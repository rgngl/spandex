#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys
import os
import optparse
import re

# ----------------------------------------------------------------------
def getFrames( str ):
    r = re.compile( '\(([0-9]*) frames\)', re.DOTALL | re.DOTALL )
    return int( re.search( r, str ).group( 1 ) )

# ----------------------------------------------------------------------
def getFrameRange( str ):
    r = re.compile( 'Use frames ([0-9]*) - ([0-9]*)', re.DOTALL | re.DOTALL )
    s = re.search( r, str )
    if not s:
        return [ 0, 0x7FFFFFFF ]
    return [ int( s.group( 1 ) ), int( s.group( 2 ) ) ]

# ----------------------------------------------------------------------
class Action:
    def __init__( self, name, type, module, attributes ):
        self.name       = name
        self.type       = type
        self.module     = module
        self.attributes = attributes
        self.created    = False

# ----------------------------------------------------------------------
class Benchmark:
    def __init__( self, name, actions, repeats = -1 ):
        self.name       = name
        self.actions    = actions
        self.repeats    = repeats

# ----------------------------------------------------------------------
def prepare( api, templateFolder, inputFolder, outputFolder, benchmarkFile = None, targaFolder = None ):
    templateFolder = templateFolder.replace( '\\', '/' )
    inputFolder    = inputFolder.replace( '\\', '/' )
    outputFolder   = outputFolder.replace( '\\', '/' )

    if templateFolder[ -1 :  ] != '/':
        templateFolder += '/'

    if inputFolder[ -1 : 0 ] != '/':
        inputFolder += '/'

    if outputFolder[ -1 : 0 ] != '/':
        outputFolder += '/'
 
    if api == 'OpenVG':
        includes = '#include <openvg.h>\n#include <vgu.h>\n'
    elif api == 'OpenGLES1':
        includes = '#include <GLES/gl.h>\n#include <GLES/egl.h>\n'
    elif api == 'OpenGLES2':
        includes = '#include <GLES2/gl2.h>\n#include <EGL/egl.h>\n'
        includes += 'typedef GLuint GLshader;\ntypedef GLuint GLprogram;\ntypedef GLuint GLattribute;\ntypedef GLuint GLuniform;\ntypedef GLuint GLframebuffer;\ntypedef GLuint GLrenderbuffer;\ntypedef void* GLshadertype;\ntypedef void* GLprecisiontype;\n'
   
    # Process template module file, change file names according to api, convert
    # $NAME and $LNAME within the files according to api. Create files to module
    # output folder.
    for suffix in [ '.h', '.c' ]:
        f = open( templateFolder + 'template_module' + suffix ).read()
        f = f.replace( '$NAME', api ).replace( '$LNAME', api.lower() ).replace( '$UNAME', api.upper() )
        open( outputFolder + 'sct_%stracemodule' % api.lower() + suffix, 'w' ).write( f )

    # Create config.py to module output folder.
    config = open( outputFolder + 'config.py', 'w' )
    config.write( "# GENERATED FILE\n" )
    config.write( "SetModuleName( '%sTrace' )\n" % api )
    config.write( "AddInclude( 'sct_%stracemodule.h' )\n\n" % api.lower() )

    # List available traces, sort them alphabetically for processing.
    traces = filter( lambda x: x[ 0 : len( 'anim' ) ] == 'anim', os.listdir( inputFolder ) )
    traces.sort()

    sourceFiles = []
    benchmarks = []

    for trace in traces:
        tr        = trace[ 0 : -4 ].capitalize()
        traceFile = outputFolder + 'sct_%strace%saction' % ( api.lower(), tr.lower(), )

        # Create an action file for each trace/.inl file.
        for suffix in [ '.h', '.c' ]:
            f = open( templateFolder + 'template_trace' + suffix ).read()
            f = f.replace( '$NAME', tr ).replace( '$LNAME', tr.lower() ).replace( '$UNAME', tr.upper() ).replace( '$API', api ).replace( '$LAPI', api.lower() ).replace( '$UAPI', api.upper() ).replace( '$INCLUDES', includes )
            open( traceFile + suffix, 'w' ).write( f )

        sourceFiles += [ traceFile + '.c' ]

        # Read the trace/.inl file and parse frame count.
        inl = open( inputFolder + trace ).read()

        totalFrames = getFrames( inl )
        frameRange  = getFrameRange( inl )

        if frameRange[ 1 ] >= totalFrames:
            frameRange[ 1 ] = totalFrames - 1
        
        if frameRange[ 0 ] < 0 or frameRange[ 0 ] > frameRange[ 1 ] or frameRange[ 1 ] >= totalFrames:
            raise 'Trace file contains invalid benchmark frame range'
       
        header  = frameRange[ 0 ]
        trailer = totalFrames - frameRange[ 1 ] - 1
        frames  = frameRange[ 1 ] - frameRange[ 0 ] + 1
                       
        # Copy trace/.inl file to output folder.
        open( outputFolder + trace, 'w' ).write( inl )

        # Add trace action definition to config.py.
        config.write( "AddActionConfig( '%s',\n" % tr )
        config.write( "                 'Init'+'Execute'+'Terminate'+'Destroy',\n" )
        config.write( "                 [ IntAttribute( 'Header',   0  ),\n" )
        config.write( "                   IntAttribute( 'Trailer',  0  ) ]\n" )
        config.write( "                 )\n\n" )

        benchmarks += [ Benchmark( tr, 
                                   [ Action( tr,
                                             tr,
                                             '%sTrace' % api, 
                                             { 'Header'  : str( header ),
                                               'Trailer' : str( trailer ) } ) ],
                                   frames ) ]

    # Config.py is done.
    config.close()

    # Write a benchmark definition (benchmark.txt) to run the traces.
    if benchmarkFile:
        bf = open( benchmarkFile, 'w' )
    else:
        bf = sys.stdout
        
    for b in benchmarks:
        actions = []
        # Write actions only once.
        for a in b.actions:
            if a.created == False:
                bf.write( '[%s:%s@%s]\n' % ( a.name, a.type, a.module, ) )
                for att in a.attributes.keys():
                    bf.write( '    %s : %s\n' % ( att, a.attributes[ att ], ) )
                bf.write( '\n' )
                a.created = True
            actions += [ a.name ]
            
        # Write benchmark
        bf.write( '[%s:BENCHMARK]\n' % b.name )
        bf.write( '    Repeats          : %d\n' % b.repeats )
        bf.write( '    InitActions      : None\n' )
        bf.write( '    BenchmarkActions : %s\n\n' % '+'.join( actions ) )

    return sourceFiles

# ----------------------------------------------------------------------
if __name__ == '__main__':

    usage = "usage: %prog [options] <api> <template folder> <input trace folder> <module output folder>"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--targaFolder',
                       type    = 'string',
                       dest    = 'targaFolder',
                       default = '',
                       help    = 'Folder to save screenshots' )

    ( options, args ) = parser.parse_args()

    if len( args ) != 4:
        parser.error( 'incorrect number of arguments' )
        sys.exit( -1 )

    api                 = args[ 0 ]
    templateFolder      = args[ 1 ]
    srcFolder           = args[ 2 ]
    dstFolder           = args[ 3 ]

    if api not in [ 'OpenGLES1', 'OpenGLES2', 'OpenVG' ]:
        raise "Unsupported API"

    prepare( api, templateFolder, srcFolder, dstFolder, None, options.targaFolder )

# ----------------------------------------------------------------------
# End of file
