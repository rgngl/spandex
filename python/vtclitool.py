#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

################################################################################
# This file is meant to be used as a base for command command line tools that
# need to run spandex.

import sys, os, fnmatch, re, glob, shutil, inspect, optparse
from sct.util import console, fileutil

################################################################################
# Argument list handling
class ArgumentParsingError( ValueError ):
    def __init__( self, message, format ):
        ValueError.__init__( self, message )
        self.format = format

class MalformedArgumentFormatError( ArgumentParsingError ): pass
class NotEnoughArgumentsError( ArgumentParsingError ): pass

def parseArgumentFormat( format ):
    argumentNames = filter( str, re.split( '\s*<(.*?)>\s*', format ) )
    # Check for argument name uniqueness.
    if len( set( argumentNames ) ) < len( argumentNames ):
        raise MalformedArgumentFormatError( 'Duplicate argument names in format', format )
    return argumentNames

def parseArgumentList( argumentList, format ):
    argumentNames = parseArgumentFormat( format )

    argDict = {}

    for argName in argumentNames:
        if isPlural( argName ):
            # Only the last argument can be plural.
            if not argName == argumentNames[-1]:
                raise MalformedArgumentFormatError( 'Only the last argument can be plural', format )
            argDict[argName] = argumentList[:]
            break
        if len( argumentList ) == 0:
            raise NotEnoughArgumentsError( 'Could not get value for argument %s' % argName, format )
        argDict[argName] = argumentList.pop( 0 )

    return camelCaseDictKeys( argDict )
   
################################################################################
# Misc utilities
def toCamelCase( string ):
    tokens = string.split( ' ' )
    return tokens[0] + ''.join( [token.capitalize() for token in tokens[1:]] )

def camelCaseDictKeys( dictionary ):
    newDict = {}
    for key, value in dictionary.items():
        newDict[toCamelCase( key )] = value
    return newDict

def isPlural( token ):
    return token[-1] == 's'

################################################################################
# Command handling
commandConfig = {}

class Command:
    def __init__( self, name, arguments, suite, options ):
        self.name = name
        if name not in commandConfig:
            console.exitWithError( 'Unknown command "%s"' % name )

        format = commandConfig[name]
        try:
            self.argDict = parseArgumentList( arguments, format )
        except ArgumentParsingError, e:
            console.exitWithError( 'Argument parsing failed: %s\nUsage: %s %s' % (e, name, e.format) )
        # Get the callers globals
        callerGlobals = inspect.stack()[2][0].f_globals
        self.handler  = getCommandHandler( name, callerGlobals )
        assert self.handler, 'Handler for command %s not found!' % name

        self.suite   = suite
        self.options = options

    def __getattr__( self, name ):
        return self.__dict__.get( name, None ) or self.argDict[name]

    def __call__( self ):
        return self.handler( self.suite, self.options, **self.argDict )
        
def getCommands():
    return commandConfig.keys()

def getCommandHandler( partialCommand, globalsDict ):
    commands = getCommands()
    if partialCommand in commands:
        command = partialCommand
    else:
        matchingCommands = fnmatch.filter( commands, partialCommand + '*' )
        if len( matchingCommands ) == 1:
            command = matchingCommands[0]
        else:
            return None
    commandHandlerName = 'handle%sCommand' % command.capitalize()
    handler = globalsDict.get( commandHandlerName, None )
    return handler or globals().get( commandHandlerName, None )

class CLI:
    def __init__( self, suite ):
        self.suite = suite
        commands   = getCommands()

        # Command line handling.
        usage  = '''usage: %prog [options] <command>

commands:
  ''' + ', '.join( commands )
        self.parser = optparse.OptionParser( usage )

    def run( self ):
        options, args = self.parser.parse_args()
        self.postInit( options )

        if len( args ) == 0:
            self.parser.error( 'No command given' )

        commandName = args[0]
        if commandName == 'init':
            initFromRelease( args[1] )
            sys.exit( 0 )
    
        return self.suite, Command( args[0], args[1:], self.suite, options )

    def postInit( self, options ):
        pass
