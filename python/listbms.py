#
# Spandex benchmark and test framework.
#
# Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, re

# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':
    usage  = "usage: %prog <benchmark file>"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--verbose',
                       action  = 'store_true',
                       dest    = 'verbose',
                       default = False,
                       help    = 'Verbose results.' )

    options, args = parser.parse_args()

    if len( args ) != 1:
        parser.error( 'Incorrect number of arguments' )
        sys.exit( -1 )

    benchmarkFile = None
    try:
        benchmarkFile = file( args[ 0 ], 'r' )
    except:
        parser.error( 'Failed to open benchmark file: %s' % args[ 1 ] );
        sys.exit( -1 )

    benchmark = benchmarkFile.readlines()
    benchmarkFile.close()

    for l in benchmark:
        if not l.startswith( '#' ) and l.endswith( 'BENCHMARK]\n' ):
            print l.split( ':' )[ 0 ][ 1: ]

