#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, os

# ------------------------------------------------------------
def limited60fps( times ):
    s      = 0.0
    misses = 0
    for i in range( len( times ) ):
        if ( s + times[ i ] ) > ( 18 * 2 ):
            misses += 1
        s = times[ i ]

    if misses > 4:
        return False

    return True

# ------------------------------------------------------------
def noload( benchmark ):
    return benchmark.find( 'no load' ) > 0

# ------------------------------------------------------------
def brief( values ):
    v = values.split( ',' )
    l = 14
    
    if len( v ) < ( l * 2 + 4 ):
        return values

    return ','.join( v[ 0 : l ] ) + ' ... ' + ','.join( v[ -l : ] )

# ------------------------------------------------------------
# tableSeparator
# ------------------------------------------------------------
def tableSeparator( format ):
    return {
        "twiki" : "|",
        "trac"  : "||",
    }[ format ]

# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] <result file>"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--verbose',
                       action  = 'store_true',
                       dest    = 'verbose',
                       default = False,
                       help    = 'Verbose results.' )
    parser.add_option( '--debug',
                       action  = 'store_true',
                       dest    = 'debug',
                       default = False,
                       help    = 'Debug results.' )
    parser.add_option( '--shiftvsync',
                       type    ='int',
                       dest    = 'shiftvsync',
                       default = 0,
                       help    = 'Shift vsync messages related to other messages.' )
    parser.add_option( '--format',
                       default = 'twiki',
                       dest    = 'format',
                       help    = 'Wiki markup format to use: twiki, trac [default: %default].' )

    ( options, args ) = parser.parse_args()
   
    # Validate command line parameters.
    if len( args ) != 1:
        parser.error( 'Incorrect number of arguments' )
        sys.exit( -1 )
        
    # Open log file.
    resultFile = None
    try:
        resultFile = file( args[ 0 ], 'r' )
    except:
        parser.error( 'Failed to open result file: %s' % args[ 0 ] );
        sys.exit( -1 )

    sep   = tableSeparator( options.format )
        
    lines = resultFile.readlines()
    resultFile.close()

    benchmarks     = {}
    benchmarkIndex = 1
    
    for i in range( len( lines ) ):
        if lines[ i ].endswith( 'BENCHMARK]\n' ):
            name = lines[ i ].split( ':' )[ 0 ][ 1: ]
            fps  = float( lines[ i + 4 ].split( ':' )[ 1 ] ) / float( lines[ i + 5 ].split( ':' )[ 1 ] )
            benchmarks[ name ] = {}
            benchmarks[ name ][ 'fps' ]   = fps
            benchmarks[ name ][ 'index' ] = '%04d' % benchmarkIndex
            benchmarkIndex += 1
    
        if lines[ i ].endswith( 'LOG_CPU_LOAD]\n' ):
            n = lines[ i ].split( ':' )[ 0 ][ 1: ]
            logtype, name = n.split( ' ', 1 )            
            cpuload = float( lines[ i + 1 ].split( ':' )[ 1 ][ 2 : -4 ] )
            benchmarks[ name ][ logtype ] = min( 100.0, max( 0,0, cpuload ) )

        if lines[ i ].endswith( 'LOG_MESSAGE]\n' ) or lines[ i ].endswith( 'LOG_SINCE]\n' ):
            n = lines[ i ].split( ':' )[ 0 ][ 1: ]
            logtype, name = n.split( ' ', 1 )
            values = map( float, lines[ i + 1 ].split( ':' )[ 1 ][ 2 : -4 ].split( ',' ) )
            benchmarks[ name ][ logtype ] = values

    maxNameLen = 0
    modBenchmarks = {}
    for b in benchmarks.keys():
        n = benchmarks[ b ][ 'index' ] + ' ' + b
        modBenchmarks[ n ] = benchmarks[ b ]
        modBenchmarks[ n ][ 'name' ] = b
        if len( b ) > maxNameLen:
            maxNameLen = len( b )

    benchmarks = modBenchmarks
    k = benchmarks.keys()
    k.sort()

    print sep + ' *Benchmark* ' + ' ' * ( maxNameLen - 10 ) + '  ' + sep + ' *fps*  ' + sep + ' *sload*  ' + sep + ' *pload*  ' + sep + ' *ploadtot*  ' + sep + ' *issue*  ' + sep + ' *vsync*  ' +  sep + ' *gload*  ' + sep 

    for b in k:
        if not benchmarks[ b ].has_key( 'fps' ):
            print b, 'error.'
            continue
        
        print sep, 
        print benchmarks[ b ][ 'name' ] ,
        print ' ' * ( maxNameLen - len( benchmarks[ b ][ 'name' ] ) ),
        print sep,

        if limited60fps( benchmarks[ b ][ 'FRAME' ] ):
            print '60*  ',
        else:
            print '%.1f  ' % benchmarks[ b ][ 'fps' ],

        print sep,
                    
        if benchmarks[ b ].has_key( 'SYSTEM_LOAD' ):
            print '%.1f%%  ' % benchmarks[ b ][ 'SYSTEM_LOAD' ],
        else:
            print ' -     ',

        print sep,            
            
        if benchmarks[ b ].has_key( 'PROCESS2BUSY_LOAD' ):
            print '%.1f%%  ' % benchmarks[ b ][ 'PROCESS2BUSY_LOAD' ],
        else:
            print ' -     ',

        print sep,            
            
        if benchmarks[ b ].has_key( 'PROCESS2TOTAL_LOAD' ):
            print '%.1f%%  ' % benchmarks[ b ][ 'PROCESS2TOTAL_LOAD' ],
        else:
            print ' -     ',

        print sep,            
            
        if benchmarks[ b ].has_key( 'GFX_ISSUE' ) and benchmarks[ b ][ 'VSYNC_DELAY' ] and benchmarks[ b ][ 'FRAME' ]:
            s  = ''
            gis = 0.0
            vds = 0.0
            gus = 0.0
            l    = len( benchmarks[ b ][ 'FRAME' ] ) - 1
            gusl = l
            for i in range( l ):
                s += '%2.1f %2.1f %2.1f,\t' % ( benchmarks[ b ][ 'GFX_ISSUE' ][ i ],
                                                benchmarks[ b ][ 'VSYNC_DELAY' ][ i + options.shiftvsync ],
                                                benchmarks[ b ][ 'FRAME' ][ i ], )
                f    = float( benchmarks[ b ][ 'FRAME' ][ i ] )
                v    = float( benchmarks[ b ][ 'VSYNC_DELAY' ][ i + options.shiftvsync ] )
                
                gis += float( benchmarks[ b ][ 'GFX_ISSUE' ][ i ] )
                vds += v
                if f > 0.0:
                    gus += max( 0.0, ( f - v ) / f )
                else:
                    gusl -= 1

            print '%.1f ms  ' % ( gis / float( l ), ),
            print sep,
            
            print '%.1f ms  ' % ( vds / float( l ), ),
            print sep,

            if limited60fps( benchmarks[ b ][ 'FRAME' ] ) and noload( b ):
                print '%.1f%%  ' % ( 100.0 * gus / float( gusl ), ),
            else:
                print ' -    ',
                
            print sep,
            
            if benchmarks[ b ].has_key( 'TOTAL' ):
                match = True

                for i in range( l ):
                    if abs( benchmarks[ b ][ 'FRAME' ][ i ] - benchmarks[ b ][ 'FRAME' ][ i ] ) > 1:
                        match = False
                if not match:
                    print 'Frame total times do not match frame times.'
                    
            print ''
                    
            if options.debug:
                print 'Frames (issue vsync total):'
                print s
                print ''
        else:
            print ' -  ' + sep + ' -  ' + sep + ' -  ' + sep
