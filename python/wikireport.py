#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# TODO:
# Average and max columns are not formatted properly with large values
# Average and max output FPS values when reporting memory benchmarks
# Redesign

import sys, optparse, os

MEMORYBENCHMARK_TAG = 'mbbytes='

# ----------------------------------------------------------------------
class Benchmark():
    errorTagString = 'err'
    nextErrorIndex = 1
    errors         = {}
    FPS            = 1
    TIME           = 2
    BPS            = 3

    def _getErrorTag( error ):
        if Benchmark.errors.has_key( error ):
            return Benchmark.errors[ error ]
        tag = Benchmark.errorTagString + str( Benchmark.nextErrorIndex )
        Benchmark.nextErrorIndex += 1
        Benchmark.errors[ error ] = tag
        return tag

    def _getMemString( bytes ):
        kb = 1024
        mb = kb * kb
        gb = kb * mb
        if bytes > gb * 10:
            return '%.1f GB/s' % ( bytes / float( gb ), )
        elif bytes > mb * 10:
            return '%.1f MB/s' % ( bytes / float( mb ), )
        elif bytes > kb * 10:
            return '%1.f KB/s' % ( bytes / float( kb ), )
        else:
            return '%d B/s' % ( bytes, )

    def __init__( self, run, name, rawResults = False ):
        self.run        = run
        self.name       = name
        self.errorTag   = None
        self.error      = None
        self.result     = None
        self.type       = None
        self.rawResults = rawResults

    def setError( self, error ):
        self.errorTag = Benchmark._getErrorTag( error )
        self.error    = error

    def hasError( self ):
        if self.result:
            return False
        else:
            return True

    def getError( self ):
        return self.error

    def getErrorTag( self ):
        return self.errorTag

    def setFpsResult( self, fps ):
        self.result = fps
        self.type   = Benchmark.FPS

    def setTimeResult( self, time ):
        self.result = time
        self.type   = Benchmark.TIME

    def setMemoryResult( self, Bps ):
        self.result = Bps
        self.type   = Benchmark.BPS

    def getResult( self ):
        return self.result

    def getFormattedResult( self ):
        if self.error:
            return self.errorTag

        if not self.result:
            return '-'

        if self.type == Benchmark.FPS:
            tp = ''
            if not self.rawResults:
                tp = ' fps'
            return "%.1f%s" % ( self.result, tp, )
        elif self.type == Benchmark.TIME:
            tp = ''
            if not self.rawResults:
                tp = ' s'
            return "%.3f%s" % ( self.result, tp, )
        elif self.type == Benchmark.BPS:
            if not self.rawResults:
                return Benchmark._getMemString( self.result )
            else:
                return "%d" % self.result
        else:
            raise 'Unexpected'

    _getErrorTag  = staticmethod( _getErrorTag )
    _getMemString = staticmethod( _getMemString )


# ----------------------------------------------------------------------
class ResultDb:

    def __init__( self, benchmarks ):
        self.benchmarks = []
        for b in benchmarks:
            if b.name not in self.benchmarks:
                self.benchmarks.append( b.name )
        self.db     = {}
        self.errors = {}
        for b in benchmarks:
            if not self.db.has_key( b.name ):
                self.db[ b.name ] = {}
            self.db[ b.name ][ b.run ] = b
            if b.hasError():
                if not self.errors.has_key( b.getErrorTag() ):
                    self.errors[ b.getErrorTag() ] = b.getError()

    def getErrors( self ):
        ekeys = self.errors.keys()
        ekeys.sort()
        e = []
        for k in ekeys:
            e.append( ( k, self.errors[ k ], ) )
        return e

    def getBenchmarks( self ):
        return self.benchmarks

    def getBenchmark( self, name, run ):
        if not self.db.has_key( name ) or not self.db[ name ].has_key( run ):
            return None
        return self.db[ name ][ run ]

    def getFormattedDiff( self, name, run1, run2 ):
        b1 = self.getBenchmark( name, run1 )
        b2 = self.getBenchmark( name, run2 )

        if not b1 or not b2:
            return '-'
        if b1.hasError() or b2.hasError():
            return '-'
        return '%.1f' % ( ( ( b2.getResult() - b1.getResult() ) / b1.getResult() ) * 100.0 )

    def getFormattedAverage( self, name ):
        if not self.db.has_key( name ):
            return '-'
        results = []
        for k in self.db[ name ].keys():
            b = self.db[ name ][ k ]
            if b.hasError():
                return '-'
            results.append( b.getResult() )

        return '%.1f' % ( sum( results ) / float( len( results ) ) )

    def getFormattedMax( self, name ):
        if not self.db.has_key( name ):
            return '-'
        results = []
        for k in self.db[ name ].keys():
            b = self.db[ name ][ k ]
            if b.hasError():
                return '-'
            results.append( b.getResult() )

        return '%.1f' % ( max( results ), )


# ------------------------------------------------------------
# tableSeparator
# ------------------------------------------------------------
def tableSeparator( format ):
    return {
        "twiki": "|",
        "trac":  "||",
    }[ format ]

# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] <result files>"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--diff',
                       action  = 'store_true',
                       dest    = 'diff',
                       default = False,
                       help    = 'Show the percentage difference between the last two result files.' )
    parser.add_option( '--average',
                       action  = 'store_true',
                       dest    = 'average',
                       default = False,
                       help    = 'Calculate the average value from the benchmark results.' )
    parser.add_option( '--max',
                       action  = 'store_true',
                       dest    = 'max',
                       default = False,
                       help    = 'Find the maximum value from benchmark results.' )
    parser.add_option( '--rawresult',
                       action  = 'store_true',
                       dest    = 'rawresult',
                       default = False,
                       help    = 'Print result value without result type (fps, s, B/s).' )
    parser.add_option( '--fps',
                       action  = 'store_true',
                       dest    = 'fps',
                       default = False,
                       help    = 'Print benchmark FPS values.' )
    parser.add_option( '--format',
                       default = 'twiki',
                       dest    = 'format',
                       help    = 'Wiki markup format to use: twiki, trac [default: %default].' )
    parser.add_option( '--errors',
                       action  = 'store_true',
                       dest    = 'errors',
                       default = False,
                       help    = 'Report benchmark errors.' )

    ( options, args ) = parser.parse_args()

    # Validate command line parameters.
    if len( args ) < 1:
        parser.error( 'Incorrect number of arguments' )
        sys.exit( -1 )

    if len( args ) < 2:
        if options.diff or options.average or options.max:
            parser.error( 'Difference/average/max require multiple result files.' )
            sys.exit( -1 )

    # Open result files.
    resultFiles = []
    try:
        currentFile = None
        for f in args:
            currentFile = f
            resultFiles.append( file( f, 'r' ) )
    except:
        parser.error( 'Failed to open the result file: %s' % currentFile );
        sys.exit( -1 )

    # Read result files
    benchmarks = []
    runs       = []
    warning    = None

    try:
        # Parse result files.
        for f in resultFiles:
            # Take the run name from the result file name, remove path and file suffix.
            benchmarkRun = f.name.replace( '\\', '/' ).split( '/' )[ -1 ].rsplit( '.', 1 )[ 0 ]
            runs.append( benchmarkRun )

            # Read result file contents
            resultLines = f.readlines()
            f.close()

            lineCount = len( resultLines )

            for i in range( lineCount ):
                line = resultLines[ i ]

                # Look for the beginning of a benchmark section.
                if line.strip().endswith( 'BENCHMARK]' ):
                    benchmarkName = line.split( ':' )[ 0 ][ 1 : ]
                    j             = 0
                    repeats       = -1
                    time          = -1.0
                    error         = None

                    while True:
                        # Start the processing from the next line after the start of a
                        # benchmark section
                        j += 1

                        if i + j >= lineCount:
                            # End of the file before end of the benchmark
                            # section.
                            raise Exception()

                        line = resultLines[ i + j ]

                        # Check for benchmark error
                        if line.find( 'Error ' ) >= 0:
                            if len( line.split() ) < 2:
                                raise Exception()
                            error = line.split( ':', 1 )[ 1 ].strip()

                        # Check for benchmark repeats
                        if line.find( 'Repeats' ) >= 0:
                            if len( line.split() ) < 2:
                                raise Exception()

                            repeats = int( line.split( ':' )[ 1 ] )

                        # Check for benchmark times
                        if line.find( 'Times' ) >= 0:
                            if len( line.split() ) < 2:
                                raise Exception()

                            t = line.split( ':' )[ 1 ].strip()
                            if t:
                                time = float( t.split()[ 0 ].replace( ',', '.' ).replace( '"', '' ) )

                        # Benchmark section done if error reported, or repeats and times parsed
                        if error or ( repeats >= 0 and time >= 0 ):
                            break

                    ######
                    assert( error or ( repeats >= 0 and time >= 0 ) )

                    # Store benchmark results
                    benchmark = Benchmark( benchmarkRun, benchmarkName, options.rawresult )

                    if error:
                        if options.errors:
                            benchmark.setError( error )
                    else:
                        if repeats == 0:
                            benchmark.setTimeResult( time )

                        elif benchmarkName.find( MEMORYBENCHMARK_TAG ) > 0 and not options.fps:
                            result = repeats / time
                            a = benchmarkName
                            result *= int( a[ a.find( MEMORYBENCHMARK_TAG[ -1 ], a.find( MEMORYBENCHMARK_TAG ) ) + 1 : ].split()[ 0 ] )
                            benchmark.setMemoryResult( result )
                        else:
                            if time == 0.0:     # TODO FIXME HACK
                                time = 0.0001

                            benchmark.setFpsResult( repeats / time )

                    benchmarks.append( benchmark )
    except:
        warning = 'WARNING: invalid/incomplete result file.'

    # ###### All benchmark results now parsed and stored in the benchmarks list.
    # Calculate column widths for the table.

    # Look for the longest benchmark name.
    longestBenchmarkName = 0
    for b in benchmarks:
        l = len( b.name )
        if l > longestBenchmarkName:
            longestBenchmarkName = l

    # Calculate the longest string for each run's result.
    longestResultForRun = []
    for r in runs:
        longestResult = len( r )
        for b in benchmarks:
            if b.run == r:
                l = len( b.getFormattedResult() )
                if l > longestResult:
                    longestResult = l
        longestResultForRun.append( longestResult )

    # Create table header and print it.
    sep = tableSeparator( options.format )

    header = '%s %s  %s' % ( sep, ' ' * longestBenchmarkName, sep )
    for i in range( len( runs ) ):
        spaces = longestResultForRun[ i ] - len( runs[ i ] )
        header += ' %s%s  %s' % ( runs[ i ], ' ' * spaces, sep )

    DIFF_COLUMN_TITLE    = 'Diff.%'
    AVERAGE_COLUMN_TITLE = 'Average'
    MAX_COLUMN_TITLE     = 'Maximum'

    # Add diff column, if required.
    if options.diff:
        header += ' ' + DIFF_COLUMN_TITLE + '  ' +  sep

    # Add average column, if required.
    if options.average:
        header += ' ' + AVERAGE_COLUMN_TITLE + '  ' + sep

    # Add maximum column, if required.
    if options.max:
        header += ' ' + MAX_COLUMN_TITLE + '  ' + sep

    print header

    # Create a simple result db for querying benchmark results
    db = ResultDb( benchmarks )

    # Print benchmark results.
    for b in db.getBenchmarks():
        spaces = longestBenchmarkName - len( b )
        print '%s %s%s  %s' % ( sep, b, ' ' * spaces, sep ),

        for i in range( len( runs ) ):
            run = runs[ i ]
            b2 = db.getBenchmark( b, run )
            if b2:
                r = b2.getFormattedResult()
            else:
                r = '-'
            d = longestResultForRun[ i ] - len( r )
            print '%s%s  %s' % ( r, ' ' * d, sep, ),

        # Add diff column, if required.
        if options.diff:
            diffString = db.getFormattedDiff( b, runs[ -2 ], runs[ -1 ] )
            spaces = len( DIFF_COLUMN_TITLE ) - len( diffString )
            print '%s%s  %s' % ( diffString, ' ' * spaces, sep, ),

        # Add average column, if required.
        if options.average:
            averageString = db.getFormattedAverage( b )
            spaces = len( AVERAGE_COLUMN_TITLE ) - len( averageString )
            print '%s%s  %s' % ( averageString, ' ' * spaces, sep, ),

        # Add max column, if required.
        if options.max:
            maxString = db.getFormattedMax( b )
            spaces = len( MAX_COLUMN_TITLE ) - len( maxString )
            print '%s%s  %s' % ( maxString, ' ' * spaces, sep, ),

        # Print new line after each row.
        print

    # #####
    # Print errors after the result table
    if options.errors:
        print
        errors = db.getErrors()
        for e in errors:
            print e[ 0 ], ':', e[ 1 ]

    if warning:
        print warning

