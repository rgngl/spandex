#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, re

# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':
    usage  = "usage: %prog [options] <benchmark list file> <benchmark file>"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--verbose',
                       action  = 'store_true',
                       dest    = 'verbose',
                       default = False,
                       help    = 'Verbose results.' )
    parser.add_option( '--op',
                       default = 'include',
                       dest    = 'op',
                       help    = 'Operation: include, exclude [default: %default].' )

    options, args = parser.parse_args()

    if len( args ) != 2:
        parser.error( 'Incorrect number of arguments' )
        sys.exit( -1 )

    benchmarkListFile = None
    try:
        benchmarkListFile = file( args[ 0 ], 'r' )
    except:
        parser.error( 'Failed to open benchmark list file: %s' % args[ 0 ] );
        sys.exit( -1 )

    benchmarkFile = None
    try:
        benchmarkFile = file( args[ 1 ], 'r' )
    except:
        parser.error( 'Failed to open benchmark file: %s' % args[ 1 ] );
        sys.exit( -1 )

    benchmarkList = benchmarkListFile.readlines()
    benchmarkListFile.close()

    for i in range( len( benchmarkList ) ):
        benchmarkList[ i ] = benchmarkList[ i ].strip()

    benchmark = benchmarkFile.readlines()
    benchmarkFile.close()

    nomatch = '#'
    match   = ''

    if options.op == 'exclude':
        nomatch = ''
        match   = '#'

    i = 0
    while True:
        if not benchmark[ i ].startswith( '#' ) and benchmark[ i ].endswith( 'BENCHMARK]\n' ):
            name  = benchmark[ i ].split( ':' )[ 0 ][ 1: ]

            c = nomatch
            if name in benchmarkList:
                benchmarkList.remove( name )
                c = match

            while True:
                print c + benchmark[ i ],
                i += 1
                if i >= len( benchmark ) or benchmark[ i ].startswith( '[' ):
                    print ''
                    break

        else:
            print benchmark[ i ],
            i += 1

        if i >= len( benchmark ):
            break

    for b in benchmarkList:
        if not b.startswith( '#' ) and b.strip():
            print '# NOT FOUND: ' + b

