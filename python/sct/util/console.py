#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, inspect, time, os, sys

__verbose = False

def exitWithError( errorMessage, exitStatus=1 ):
    caller = inspect.stack()[1]
    errorMessage = 'Error at %s line %d (function %s): %s' % (caller[1], caller[2], caller[3], errorMessage)
    sys.stderr.write( errorMessage + '\n' )
    sys.exit( exitStatus )

def setVerbose( verbose ):
    global __verbose
    __verbose = verbose

def log( message ):
    printInfo( message )
    if sys.argv[0] != '-c':
        # Only write to a log if we are not in interactive mode.
        log_message = '[%s] %s' % (time.strftime( '%H:%M' ), message)
        # Use the script name from sys.argv[0] as the base for the log file
        # name.
        filename = os.path.splitext( os.path.basename( sys.argv[0] ) )[0] + '.log'
        file( filename, 'a' ).write( log_message + '\n' )

def printInfo( message ):
    if __verbose:
        print message

def printWarning( message ):
    sys.stderr.write( 'Warning: %s\n' % message )

def printBanner( header, bannerChar='-' ):
    spaceBeforeHeader = ( 80 - len( header ) ) / 2
    spaceAfterHeader  = 80 - len( header ) - spaceBeforeHeader
    print spaceBeforeHeader * bannerChar + header + spaceAfterHeader * bannerChar

def prefixPrint( text, prefix='| ' ):
    for line in text.split( '\n' ):
        print prefix + line

def reportStandardOutput( stdout, stderr ):
    if stdout:
        printBanner( 'STDOUT' )
        prefixPrint( stdout.strip() )
    if stderr:
        printBanner( 'STDERR' )
        prefixPrint( stderr.strip() )
    if stdout or stderr:
        print

def confirm( prompt, default=True ):
    '''Prints the given prompt and waits for y/n confirmation. If default is
    True, empty input is also considered as positive response.'''
    assert default in [True, False]
    try:
        if default == True:
            if raw_input( "%s [Y/n] " % prompt ) in ["y", "Y", ""]:
                return True
        else:
            if raw_input( "%s [y/N] " % prompt ) in ["y", "Y"]:
                return True
    except KeyboardInterrupt:
        pass
    return False

