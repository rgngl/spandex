#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import os, subprocess
import console

def cleanDir( path ):
    '''Removes the contents (files and folders) of the given directory.'''
    os.system( 'rm -rf %s' % os.path.join( path, '*' ) )

def safeCreateDir( path ):
    '''Creates a directory using the given path, if it does not already
    exist. Returns True if the directory was created, False otherwise.'''
    name = os.path.normpath( path )
    if not os.path.isdir( path ):
        console.log( 'Creating directory %s...' % path )
        os.makedirs( path )
        return True
    else:
        console.log( 'Creating directory %s... exists' % path )
        return False

def safeCreateFile( path, contents='' ):
    '''Creates a file using the given path and contents, if the path does not
    already exist. Return True if the file was created, False otherwise.'''
    if not os.path.exists( path ):
        console.log( 'Creating file %s...' % path )
        file( path, 'w' ).write( contents )
        return True
    else:
        console.log( 'Creating file %s... exists' % path )
        return False

def executeCommand( command ):
    '''Runs the command and returns its exit status and data written to both
    standard out and standard error streams.'''
    console.log( 'Executing command "%s"' % command )
    
    p              = subprocess.Popen( command, stdout=subprocess.PIPE, stderr=subprocess.PIPE )
    stdout, stderr = p.communicate()

    console.log( 'Command exited with code %d' % p.returncode )

    return p.returncode, stdout, stderr

def isNewer( file1, file2 ):
    '''Returns True if file1 has been modified more recently than file2.'''
    return os.stat( file1 ).st_mtime > os.stat( file2 ).st_mtime

