#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

def getLinesStartingWith( text, prefix ):
    '''Returns the list of lines in text that start with the given prefix. White
    spaces are removed from the beginning of the lines before checking for the
    prefix. Therefore, the prefix should not start with white space either (a
    ValueError is generated if it does.)'''
    if prefix.lstrip() != prefix:
        raise ValueError( 'prefix cannot start with white space' )
    return [line for line in text.split( '\n' ) if line.lstrip().startswith( prefix )]

def stripConsecutiveEmptyLines( text, n ):
    '''Returns a copy of text with all occurances of more than n consecutive
    newlines converted to n newlines.'''
    if n < 0:
        raise ValueError( 'n must be >= 0' )
    
    searchString  = (n+1) * '\n'
    replaceString = n * '\n'
    while searchString in text:
        text = text.replace( searchString, replaceString )
    return text

def uncap( string ):
    return string[0].lower() + string[1:]

