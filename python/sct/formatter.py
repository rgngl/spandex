#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

INDENT_SIZE = 4

class FormatterBase:
    def __init__( self, stream ):
        assert hasattr( stream, 'write' ), 'Expected an object with write method'
        self.stream = stream
        
    def Output( self, text='' ):
        self.stream.write( text + '\n' )

    def OutputLines( self, lines ):
        for line in lines:
            self.Output( line )

class IndentingFormatter( FormatterBase ):
    def __init__( self, stream, initialIndentLevel=0 ):
        FormatterBase.__init__( self, stream )
        self.indentLevel = initialIndentLevel
    
    def _increaseIndent( self ):
        self.indentLevel += 1

    def _decreaseIndent( self ):
        self.indentLevel -= 1

    def Output( self, text='' ):
        FormatterBase.Output( self, self.indentLevel * INDENT_SIZE * ' ' + text )

class XMLFormatter( IndentingFormatter ):
    def __init__( self, stream, initialIndentLevel=0 ):
        IndentingFormatter.__init__( self, stream, initialIndentLevel )
        self.elements = []
        self.AddComment( '<?xml version="1.0" ?>' )
    
    def _pushElement( self, name ):
        self.elements.append( name )

    def _popElement( self ):
        return self.elements.pop()

    def AddComment( self, text ):
        self.Output( '<!-- %s -->' % text )

    def Newline( self ):
        self.Output( '' )
    
    def StartElement( self, element, **kwargs ):
        if not kwargs:
            self.Output( '<%s>' % element )
        else:
            s = ' '.join( ['%s="%s"' % (name, value) for name, value in kwargs.items()] )
            self.Output( '<%s %s>' % (element, s) )
        self._pushElement( element )
        self._increaseIndent()

    def EndElement( self ):
        name = self._popElement()
        self._decreaseIndent()
        self.Output( '</%s>' % name )

class CFormatter( IndentingFormatter ):
    def PrintParagraph( self, paragraph ):
        for line in paragraph.split( '\n' ):
            self.Output( line )

    def AddComment( self, text, width=74 ):
        self.Output( '/* %-*s */' % (width, text) )

    def StartBlock( self, text='' ):
        self.Output( '{' + text );
        self._increaseIndent()

    def EndBlock( self, text='' ):
        assert self.indentLevel > 0
        self._decreaseIndent()
        self.Output( '}' + text )

class SpandexFormatter( FormatterBase ):
    def AddComment( self, text ):
        for line in text.split( '\n' ):
            self.Output( '# ' + line )
    
    def StartSection( self, name ):
        self.Output( '[' + name + ']' )
    
    def StartActionSection( self, name, type, module ):
        self.StartSection( '%s:%s@%s' % (name, type, module) )

    def StartBenchmarkSection( self, name ):
        self.Output( '[%s:BENCHMARK]' % name )

    def OutputAttribute( self, name, value ):
        self.Output( '\t%-20s : %s' % (name, value) )

