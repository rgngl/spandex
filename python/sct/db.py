#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import types
from util import sequtil, textutil

# class EmptyProjectionError( Exception ): pass

class DataRow( dict ):
    def __init__( self, attributes={} ):
        self.update( attributes )

    def __contains__( self, item ):
        if sequtil.listOrTuple( item ):
            for subItem in item:
                if not subItem in self:
                    return False
            return True
        else:
            return dict.__contains__( self, item )

    def __getattr__( self, name ):
        #return self.get( name, None )
        return self[name]
        
    def __getitem__( self, key ):
        if sequtil.listOrTuple( key ):
            return tuple( [self[attr] for attr in key] )
        assert type( key ) == types.StringType, 'Expected a string key but got %s' % str( type( key ) )
        return dict.__getitem__( self, key )
        
    def __setitem__( self, key, value ):
        if sequtil.listOrTuple( key ):
            assert sequtil.listOrTuple( value ), 'Expected a list or tuple but got %s' % type( value )
            assert len( key ) == len( value ), 'Expected a value of length %d but got %d' % (len( key ), len( value ))
            for k, v in zip( key, value ):
                self[k] = v
        else:
            dict.__setitem__( self, key, value )
        
    def __str__( self ):
        return '\n'.join( '%-20s : %s' % item for item in self.items() )
    
    def clone( self ):
        return DataRow( self )

    def toDict( self ):
        return dict( self )

class DataTable:
    def __init__( self ):
        self.rows = []

    def __len__( self ):
        return len( self.rows )

    def __getitem__( self, key ):
        return self.rows[key]

    def __contains__( self, item ):
        raise NotImplementedError, 'DataTable does not have a key, use IndexedDataTable if you need it'

    def __iter__( self ):
        return iter( self.rows )

    def __str__( self ):
        s = ''
        for i, row in enumerate( self.rows ):
            s += 'Row %d:\n' % i
            s += str( row ) + '\n'
        return s

#     def __getattr__( self, name ):
#         if name.endswith( 's' ):
#             return self.getUniqueAttributeValues( name[:-1] )
#         raise AttributeError, name
    
    def clone( self ):
        clone = DataTable()
        for row in self:
            clone.addRow( row.clone() )
        return clone

    def addRow( self, row ):
        if not isinstance( row, DataRow ):
            raise TypeError, 'Given row object is not an instance of DataRow class'
        self.rows.append( row )

    def replaceRow( self, row, newRow ):
        self.rows[self.rows.index( row )] = newRow
        
    def addRows( self, rows ):
        for row in rows:
            self.addRow( rows )

    def by( self, attribute ):
        return self.split( attribute )
            
    def getColumn( self, columnName ):
        values = []
        for row in self:
            if columnName in row:
                values.append( row[columnName] )
        return values

    def getColumnNames( self ):
        names = []
        for row in self:
            names.extend( row.keys() )
        return sequtil.unique( names )

    def getPotentialKeys( self ):
        columnNames = self.getColumnNames()

        # Project the actions by all columns. 
        projected = {}
        for name in columnNames:
            projected[name] = self.project( name )

        # Potential keys must have at least two values in the table.
        for name, table in projected.items():
            if len( table ) < 2:
                del projected[name]
                
        return projected

    def getUniqueAttributeValues( self, attributeName ):
        projected = self.project( attributeName )
        if type( attributeName ) == types.StringType:
            return projected.getColumn( attributeName )
        return [tuple( [row[attr] for attr in attributeName] ) for row in projected]

    def join( self, table, attributeNameMap ):
        joinedTable = DataTable()
        for selfRow in self:
            for otherRow in table:
                for selfName, otherName in attributeNameMap.items():
                    if selfRow[selfName] != otherRow[otherName]:
                        break
                else:
                    joinedRow = DataRow( selfRow )
                    otherAttributes = otherRow.keys()
                    for name in attributeNameMap.values():
                        otherAttributes.remove( name )
                    for name in otherAttributes:
                        assert name not in joinedRow, 'Cannot join rows, attribute "%s" exists in both' % name
                        joinedRow[name] = otherRow[name]
                    joinedTable.addRow( joinedRow )
        return joinedTable

    def project( self, attributeNames ):
        '''Return a new data table with the rows selected from this data table
        but having only the given attributes. Identical rows are removed from
        the returned table.'''
        if type( attributeNames ) == type( '' ):
            # Convert a single string to a list.
            attributeNames = [attributeNames]

        projected = DataTable()
        for row in self:
            d = DataRow()
            for name in attributeNames:
                if name in row:
                    d[name] = row[name]
            if not d:
                continue
            for p in projected:
                if d == p:
                    break
            else:
                projected.addRow( d )
#         if len( projected ) == 0:
#             raise EmptyProjectionError, 'Attribute set %s resulted in an empty projection' % attributeNames
        return projected

    def removeColumn( self, columnName ):
        for row in self:
            if columnName in row.keys():
                del row[columnName]

    def removeColumns( self, columnNames ):
        for name in columnNames:
            self.removeColumn( name )

    def removeColumnsByPredicate( self, predicate ):
        '''Removes from the table the columns for which the given predicate
        function returs true. The predicate takes the column name as an
        argument.'''
        for row in self:
            for name in row.keys():
                if predicate( name ):
                    del row[name]

    def renameColumn( self, oldName, newName ):
        for row in self:
            if oldName in row:
                assert newName not in row, 'Cannot rename column "%s" to "%s": column with the new name exists' % (oldName, newName)
                row[newName] = row[oldName]
                del row[oldName]

    def select( self, *args ):
        if len( args ) == 1:
            expr      = args[0]
            resultSet = DataTable()
            for row in self:
                value = eval( expr, globals(), row )
                if value:
                    resultSet.addRow( row )
            return resultSet
        else:
            return self.select( ' or '.join( '%s==%s' % (args[0], repr(value)) for value in args[1:] ) )

    def selectByPredicate( self, pred ):
        resultSet = DataTable()
        for row in self:
            if pred( row ):
                resultSet.addRow( row )
        return resultSet

    def split( self, attribute ):
        values = self.getUniqueAttributeValues( attribute )
        if not values:
            raise ValueError, 'Data table does not have any values for attribute "%s"' % attribute
        return [self.select( attribute, value ) for value in values]
    
    def toIndexedTable( self, key ):
        indexedTable = IndexedDataTable( key )
        for row in self:
            indexedTable.addRow( row )
        return indexedTable

    def toMergedTable( self, key ):
        mergedTable = MergedDataTable( key )
        for row in self:
            mergedTable.addRow( row )
        return mergedTable

class IndexedDataTable( DataTable ):
    def __init__( self, key ):
        DataTable.__init__( self )
        self.key   = key
        self.index = {}
        
    def __contains__( self, item ):
        return item in self.index

    def clone( self ):
        clone = IndexedDataTable( self.key )
        for row in self:
            clone.addRow( row.clone() )
        return clone

    def addRow( self, row ):
        assert self.key in row, 'Given row does not have the key attribute'
        if row[self.key] in self:
            # Try to merge the rows
            print 'Warning: Input tables have redundant or disparate data. Trying to merge rows.'
            existingRow = self.index[row[self.key]]
            row         = mergeRows( row, existingRow )
            self.replaceRow( existingRow, row )
        else:
            #assert row[self.key] not in self, 'Given row has a key that already exists in the table'
            DataTable.addRow( self, row )
            self.index[row[self.key]] = row

    def selectByKey( self, key ):
        return self.index[key]

    def toListOfLists( self, topAttributes=[] ):
        lol = []
        # Key attribute is put at the top if there are multiple data rows.
        if len( self ) > 1:
            lol.append( [self.key] )
            for key in self.index.keys():
                lol[0].append( key )
        attributeNames = self.getColumnNames()
        if len( attributeNames ) == 0:
            return lol
        attributeNames.remove( self.key )
        attributeNames.sort()
        # Move the top attributes to the front of the list.
        for name in topAttributes[-1::-1]:
            if name in attributeNames:
                attributeNames.remove( name )
                attributeNames.insert( 0, name )
        for name in attributeNames:
            row = [name + ':']
            for r in self.index.values():
                row.append( r[name] )
            lol.append( row )
        return lol

    # TODO: Should Select be optimized for the case that the key attribute is in
    # the kwargs? In that case there can be only one row that matches.

class MergedDataTable( IndexedDataTable ):
    def addRow( self, row ):
        assert self.key in row, 'Given row does not have the key attribute'
        if row[self.key] in self:
            # Do a merge.
            existingRow = self.selectByKey( row[self.key] )
            for name, value in row.items():
                if name in existingRow:
                    # Add the value to the existing row if the existing
                    # attribute is a list and the value does not exist.
                    if type( existingRow[name] ) == types.ListType:
                        if value not in existingRow[name]:
                            existingRow[name].append( value )
                    else:
                        if existingRow[name] != value:
                            existingRow[name] = [existingRow[name], value]
                else:
                    existingRow[name] = [value]
        else:
            newRow = DataRow()
            # Embed all values to lists.
            for name, value in row.items():
                newRow[name] = [value]
            DataTable.addRow( self, newRow )
            self.index[row[self.key]] = newRow

class Database:
    def __init__( self, tableDict ):
        self.tables = dict( tableDict )

    def __contains__( self, item ):
        return item in self.tables

    def addTable( self, tableName, table ):
        if tableName in self.tables:
            raise ValueError, 'Table "%s" already exists' % tableName
        self.tables[tableName] = table

    def select( self, tableName, expr ):
        return self.tables[tableName].select( expr )

    def setAttributeInAllTables( self, name, value ):
        for table in self.tables.values():
            for row in table:
                row[name] = value

    def asDict( self ):
        return dict( self.tables )

    def getTables( self ):
        return self.tables.values()
            
    def getTable( self, name ):
        return self.tables[name]

def combineDatabases( dbs ):
    '''Return a database that has the all the data in the given databases.
    Tables with identical names are combined to single table in the resulting
    database.'''
    combinedDB = Database( dbs[0].asDict() )
    for db in dbs[1:]:
        for tableName, table in db.asDict().items():
            if tableName in combinedDB:
                targetTable = combinedDB.getTable( tableName )
                for row in table:
                    targetTable.addRow( row )
            else:
                combinedDB.addTable( tableName, table )
    return combinedDB

def makeDataTable( columnNames, rowData ):
    table = DataTable()
    for rd in rowData:
        assert len( columnNames ) == len( rd ), 'Row item count does not match column count.'
        row = DataRow( dict( zip( columnNames, rd ) ) )
        table.addRow( row )
    return table
        
def mergeRows( row1, row2 ):
    mergedRow = DataRow( row1 )
    for name, value in row2.items():
        if name not in mergedRow:
            mergedRow[name] = value
        else:
            assert mergedRow[name] == value, 'Cannot merge rows that have different values for the same attributes'
    return mergedRow
        
