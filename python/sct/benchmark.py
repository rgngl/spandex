#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import scalar, bmparser, db

def ParseScalar( string, byteUnit=False ):
    unit  = string.split()[-1]
    value = ''.join( string.split()[:-1] )

    # Return scalar value and its unit
    return scalar.Scalar( ParseFloat( value ), unit, byteUnit )

def ParseFloat( string ):
    '''Returns the floating point number contained in the string. The number can
    be in double quotes and have extra white-space characters around it. This
    function also tries to handle the typical decimal point and thousand
    separator cases.'''

    # Remove possible quotes.
    string  = bmparser.ParseQuotedString( string )

    if not string:
        raise ValueError, 'Empty string given'
    
    USFloat     = True
    thousandSep = None

    if string.find( ',' ) != -1:
        # Either a non-US float or comma is used as thousand separator.
        if string.find( '.' ) != -1:
            # Ok, US float with comma as a thousand separator.
            thousandSep = ','
        else:
            # Non-US style float, thousand separator can be space.
            thousandSep = ' '
            USFloat     = False

    if thousandSep:
        # Remove thousand separators.
        string = string.replace( thousandSep, '' )

    if not USFloat:
        # Convert decimal separator to comma.
        string = string.replace( ',', '.' )

    return float( string )

## def FindActionSection( actionSections, name ):
##     for section in actionSections:
##         if section['Name'] == name:
##             return section
##     return None

def ParseActionName( name ):
    if name.find( '$' ) != -1:
        name, id = name.split( '$', 1 )
        return name, int( id )
    return name, 0

def ParseActionSection( section ):
    action             = db.DataRow( section )
    name, ID           = ParseActionName( action.Name )
    # Save the full name of the action for later use.
    action['FullName'] = action.Name
    action['Name']     = name
    action['ID']       = ID
    if 'Workloads' in action:
        # Convert the workloads to scalars.
        workloads           = section['Workloads'].split( ',' )
        workloads           = map( ParseScalar, workloads )
        action['Workloads'] = workloads
    else:
        action['Workloads'] = []
    return action

def ParseActionSections( sections ):
    table = db.DataTable()
    for section in sections:
        table.addRow( ParseActionSection( section ) )
    return table

def GetBenchmarkMultiActionName( bm ):
    for actionAttributeName in ['InitActions','BenchmarkActions']:
        for actionName in bm[actionAttributeName]:
            if '$' in actionName:
                return actionName.split( '$' )[0]
    return None

def GetBenchmarkActionID( bm, actionName ):
    for actionAttributeName in ['InitActions','BenchmarkActions']:
        for name in bm[actionAttributeName]:
            n, ID = ParseActionName( name )
            if n == actionName:
                return ID
    return None

def ParseBenchmarkSection( section ):
    benchmark                     = db.DataRow( section )
    benchmark['InitActions']      = section['InitActions'].split( '+' )
    benchmark['BenchmarkActions'] = section['BenchmarkActions'].split( '+' )
    repeats                       = int( section['Repeats'] )
    benchmark['Repeats']          = repeats
    times = []
    if section['Times']:
        times.append( ParseFloat( section['Times'] ) )
    multiActionName = GetBenchmarkMultiActionName( benchmark )
    if multiActionName:
        benchmark['MultiActionName'] = multiActionName
        benchmark['MultiActionID']   = GetBenchmarkActionID( benchmark, multiActionName )
    benchmark['Times'] = times
    # Use -1 if time is zero.
    fps                = [repeats / (time or -1.0) for time in times]
    benchmark['FPS']   = fps
    return benchmark

def ParseBenchmarkSections( sections ):
    table = db.DataTable()
    for section in sections:
        table.addRow( ParseBenchmarkSection( section ) )
    return table

def ParseModuleSections( sections ):
    moduleTable = db.DataTable()
    for section in sections:
        del section['SectionType']
        moduleTable.addRow( section )
    return moduleTable

def _CreateResultRow( benchmark, workload, actionName=None, actionID=None ):
    resultUnit = '%s/s' % workload.GetUnit()
    resultDict = benchmark.toDict()
    # Remove SectionType so it won't cause problems later when the
    # results and actions are joined.
    del resultDict['SectionType']

    resultDict['ActionName']   = actionName
    resultDict['ActionID']     = actionID
    resultDict['ResultUnit']   = resultUnit
    resultDict['WorkloadUnit'] = workload.GetUnit()
    
    if 'Error' in benchmark:
        resultValue = 0.0
    else:
        if len( benchmark.Times ) != 1:
            raise NotImplementedError
        resultValue = workload.GetValue() * benchmark.Repeats / (benchmark.Times[0] or -1.0)

    resultDict['ResultValue'] = resultValue
    resultDict['Result']      = scalar.Scalar( resultValue, resultUnit )
    return db.DataRow( resultDict )

def CreateResultTable( benchmarkTable, actionTable ):
    fpsWorkload = scalar.Scalar( 1, 'frames' )
    resultTable = db.DataTable()
    actionTable = actionTable.toIndexedTable( 'FullName' )
    for benchmark in benchmarkTable.rows:
        actionWithWorkload = False
        for actionName in benchmark.BenchmarkActions:
            action = actionTable.selectByKey( actionName )
                
            for workload in action.Workloads:
                resultRow = _CreateResultRow( benchmark, workload, action.Name, action.ID )
                resultTable.addRow( resultRow )
                actionWithWorkload = True
        if not actionWithWorkload:
            resultRow = _CreateResultRow( benchmark, fpsWorkload )
            resultTable.addRow( resultRow )
    return resultTable

def ParseRun( data ):
    sectionTable      = bmparser.ParseOutput( data )
    benchmarkSections = sectionTable.select( "SectionType=='BENCHMARK'" ).rows
    moduleSections    = sectionTable.select( "SectionType=='MODULE'" ).rows
    actionSections    = sectionTable.selectByPredicate( lambda row: '@' in row['SectionType'] ).rows
    benchmarkTable    = ParseBenchmarkSections( benchmarkSections )
    actionTable       = ParseActionSections( actionSections )
    resultTable       = CreateResultTable( benchmarkTable, actionTable )
    moduleTable       = ParseModuleSections( moduleSections )
    return db.Database( { 'actions'    : actionTable,
                          'modules'    : moduleTable,
                          'results'    : resultTable,
                          'benchmarks' : benchmarkTable } )
