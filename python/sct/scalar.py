#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import math

significantDigits = 3

class SIPrefix:
    # Map for selecting correct prefix based on number's magnitude.
    prefixMap = { 0 : '',
                  1 : 'k',
                  2 : 'M',
                  3 : 'G',
                  4 : 'T',
                  5 : 'P' }

    def __init__( self, value, kilo=1000 ):
        self.kilo = kilo
        if value == 0.0:
            mag = 0
        else:
            mag = int( math.floor( math.log( abs( value ), kilo ) ) )
        # Truncate magnitude to the maximum magnitude present in the prefixMap.
        self.mag    = min( mag, max( SIPrefix.prefixMap.keys() ) )
        self.mag    = max( 0, self.mag )
        self.prefix = SIPrefix.prefixMap[self.mag]

    def GetPrefixChar( self ):
        return self.prefix

    def GetMagnitude( self ):
        return self.mag

    def GetScale( self ):
        return self.kilo**self.mag

class Scalar:
    def __init__( self, value, unit, byteUnit=False ):
        self.value    = value
        self.unit     = unit
        if 'bytes' == unit.split( '/' )[0]:
            self.kilo = 1024
        else:
            self.kilo = 1000
        self.prefix = SIPrefix( self.value, self.kilo )

    def __eq__( self, other ):
        if type( self ) != type( other ):
            return False
        return self.value == other.value and self.unit == other.unit

    def __float__( self ):
        return float( self.value )

    def __str__( self ):
        return '%s %s' % self.GetFormattedValueAndUnit()

    def __repr__( self ):
        return 'Scalar( %s, %s )' % (repr( self.value ), repr( self.unit ))

    def __hash__( self ):
        return self.value.__hash__() ^ self.unit.__hash__()

    def GetSIPrefix( self ):
        return self.prefix

    def GetReadingNote( self ):
        if self.kilo == 1000:
            return ''
        # Guess a singular form of the unit, might not work in some cases.
        baseUnit, rest = self.unit.split( '/', 1 )
        if baseUnit[-1] == 's':
            # Chop the trailing 's' to create (hopefully) a singular.
            baseUnit = baseUnit[:-1]
        singularUnit = baseUnit + '/' + rest
        mag = self.prefix.GetMagnitude()
        if mag != 1:
            return '1 %s%s = %d^%d %s' % (self.prefix.GetPrefixChar(), singularUnit, self.kilo, mag, self.unit)
        else:
            return '1 %s%s = %d %s' % (self.prefix.GetPrefixChar(), singularUnit, self.kilo, self.unit)

    def GetFormattedValueAndUnit( self ):
        if self.value == 0.0:
            return '0.0', self.unit
        value = self.value / self.prefix.GetScale()
        # Calculate number of digits in the integer part.
        if value < 1:
            intDigits = 0
        else:
            intDigits = int( math.floor( 1 + math.log( abs( int( value ) ), 10 ) ) )
        # Calculate number of decimal digits to get the desired number of
        # significant digits.
        decimalDigits = max( 0, int( significantDigits - intDigits ) )
        formattedValue = '%.*f' % (decimalDigits, value)
        formattedUnit = '%s%s' % (self.prefix.GetPrefixChar(), self.unit)
        return formattedValue, formattedUnit

    def GetUnit( self ):
        return self.unit

    def GetValue( self ):
        return self.value


