#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
import sys, os, re, glob, StringIO, __builtin__

######################################################################
from   util import console
import action
import formatter
import types

######################################################################
class Suite:
    def __init__( self ):
        self.benchmarks       = []
        self.builtinImport    = __builtin__.__import__
        self.targetModulePath = 'targets'

    def addBenchmark( self, benchmark, features = None ):
        if features and not isinstance( features, ( types.ListType, types.TupleType ) ):
            features = [ features ]
        for bm in self.benchmarks:
            if bm[ 0 ].name == benchmark.name:
                console.printWarning( 'Multiple benchmarks with name "%s"' % bm[ 0 ].name )
        self.benchmarks.append( ( benchmark, features, ) )
        return ( benchmark, features, )

    def getBuilder( self, target ):
        targetModule = self.getTargetModule( target )
        return BenchmarkBuilder( targetModule )
    
    def generateSpandexInput( self, target ):
        builder = self.getBuilder( target )
        
        for benchmark in self.benchmarks:
            if benchmark[ 1 ] == None or builder.targetModule.supportFeature( benchmark[ 1 ] ) == True:
                builder.build( benchmark[ 0 ] )

        return builder.stream.getvalue()

    def getTargetModule( self, name ):
        sys.path.insert( 0, self.targetModulePath )
        targetModule = importModule( name )
        sys.path.pop( 0 )
        targetModule.name = name
        return targetModule

def importModule( name ):
    module = __import__( name )
    # The following is copied from the documentation for __import__
    components = name.split( '.' )
    for comp in components[1:]:
        module = getattr( module, comp )
    return module
    
class BenchmarkBuilder:
    def __init__( self, targetModule ):
        self.targetModule       = targetModule
        self.allActions         = []
        self.stream             = StringIO.StringIO()
        self.formatter          = formatter.SpandexFormatter( self.stream )
        self.actionsByType      = {}
        self.generatedActions   = set()
        self.benchmarkCount     = 0
        self.actionModules      = loadSpandexActionModules( self.recordAction )
        targetModule.__dict__.update( self.actionModules )
        self.release            = self._getSpandexRelease()
        self.formatter.AddComment( 'This file has been generated' )
        self.formatter.AddComment( 'Target: %s' % targetModule.name )
        self.formatter.AddComment( 'Spandex release: %s' % self.release )        

    def _getSpandexRelease( self ):
        f = None
        try:
            if os.environ.has_key( "SPANDEX_HOME" ):
                f = open( os.environ[ "SPANDEX_HOME" ] + "/sct/common/include/sct_version.h", "r" )
            if not f:
                f = open( "../../../sct/common/include/sct_version.h", "r" )
        except:
            pass
        if f:
            lines = f.readlines()
            f.close()
            for l in lines:
                if l.startswith( "#define SCT_VERSION" ):
                    return l.split()[ 2 ].strip( '"' )
        return "Unspecified (define SPANDEX_HOME)"
        
    def _getAmountOfActions( self, actionType ):
        return len( self.actionsByType.get( actionType, [] ) )

    def _addAction( self, action ):
        self.allActions.append( action )
        actionType = action.config.type
        if actionType not in self.actionsByType:
            self.actionsByType[ actionType ] = [ action ]
        else:
            self.actionsByType[ actionType ].append( action )

    def recordAction( self, module, actionType, *args ):
        assert self.benchmark

        action = self.findAction( module.name, actionType, args )
        if not action:
            actionConfig = module.getActionConfig( actionType )
            if not actionConfig:
                raise ValueError( 'Could not find config for %s action in module %s' % (actionType, module.name) )
            actionName = '%s_%d' % (actionType, self._getAmountOfActions( actionType ) + 1)
            action     = Action( actionName, actionConfig, args )
            self._addAction( action )
        self.benchmark.addAction( action )
        return action

    def generateActions( self, actions ):
        for k in actions.keys():
            for action in actions[ k ]:
                if action not in self.generatedActions:
                    action.generateOutput( self.formatter )
                    self.formatter.Output()
                    self.generatedActions.add( action )

    def findAction( self, moduleName, actionType, args ):
        for action in self.allActions:
            if action.config.type == actionType and action.config.moduleName == moduleName and action.args == args:
                return action
        return None

    def build( self, benchmark ):
        self.benchmarkCount += 1
        self.benchmark       = benchmark # TODO: This is kludgey...
        # Remove possible actions from previous builds.
        benchmark.resetActions()
        benchmark.build( self.targetModule, self.actionModules )
        self.generateActions( self.benchmark.initActions )
        self.generateActions( self.benchmark.benchmarkActions )
        self.generateActions( self.benchmark.finalActions )
        self.generateActions( self.benchmark.preTerminateActions )        
        self.generateActions( self.benchmark.terminateActions )        
        self.benchmark.generateOutput( self.formatter, self.targetModule.name )
        self.formatter.Output()
        del self.benchmark
        
class Action:
    def __init__( self, name, config, args ):
        self.config     = config
        self.name       = name
        self.args       = args
        self.attributes = []
        attributeCount  = len( config.attributeConfigs )

        actionType = config.type
        if len( args ) < attributeCount:
            raise ValueError, 'Too few attributes for %s action' % actionType
        elif len( args ) > attributeCount:
            raise ValueError, 'Too many attributes for %s action' % actionType

        # Convert args to action attributes
        for i, arg in enumerate( args ):
            attrConfig = config.attributeConfigs[i]
            attrName   = attrConfig.name
            # Check argument type
            attrTypes = attrConfig.pyTypes
            if type( arg ) not in attrTypes:
                baseString = 'Invalid type for %s attribute of %s action. ' % (attrName, actionType)
                if len( attrTypes ) == 1:
                    raise TypeError, baseString + 'Expected type %s, got %s' % (attrConfig.pyTypes[0], type( arg ))
                else:
                    raise TypeError, baseString + 'Expected type to be one of %s, got %s' % (','.join( attrTypes ), type( arg ))
            # Check argument value
            if not attrConfig.IsLegalPythonValue( arg ):
                raise ValueError, attrConfig.MakeAttributeValueErrorMessage( arg )
            self.attributes.append( (attrConfig.name, arg) )

    def getAttributeConfig( self, name ):
        for config in self.config.attributeConfigs:
            if config.name == name:
                return config
        return None

    def generateOutput( self, formatter ):
        formatter.StartActionSection( self.name, self.config.type, self.config.moduleName )
        for name, value in self.attributes:
            attrConfig   = self.getAttributeConfig( name )
            spandexValue = attrConfig.FormatValueForSpandex( value )
            formatter.OutputAttribute( name, spandexValue )

class ActionModule:
    def __init__( self, name, actionConfigs, callback ):
        self.name          = name
        self.actionConfigs = actionConfigs
        self.callback      = callback

    def __repr__( self ):
        return 'ActionModule( %s, %s, %s )' % (self.name, self.actionConfigs, self.callback)

    def __getattr__( self, name ):
        return lambda *args: self.callback( self, name, *args )
    
    def getActionConfig( self, actionType ):
        for config in self.actionConfigs:
            if config.type == actionType:
                return config
        return None

NEXT_BENCHMARK_INDEX    = 1     # KJK

class Benchmark:
    def __init__( self ):
        global NEXT_BENCHMARK_INDEX
        self.name             = self.__class__.__name__
        self.repeats          = -1
        self.minRepeatTime    = -1.0
        self.benchmarkIndex   = NEXT_BENCHMARK_INDEX    # KJK
        NEXT_BENCHMARK_INDEX  += 1                      # KJK
        self.resetActions()
        if self.__doc__:
            self.description = self.__doc__
        else:
            self.description = 'No description'

    def resetActions( self ):
        self.initActions         = {}
        self.benchmarkActions    = {}
        self.finalActions        = {}
        self.preTerminateActions = {}
        self.terminateActions    = {}
        self.activeActionList    = None
        
    def addAction( self, action ):
        self.activeActionList.append( action )

    def beginInitActions( self, index = 0 ):
        self.initActions[ index ] = []
        self.activeActionList = self.initActions[ index ]

    def beginBenchmarkActions( self, index = 0 ):
        self.benchmarkActions[ index ] = []
        self.activeActionList = self.benchmarkActions[ index ]

    def beginFinalActions( self, index = 0 ):
        self.finalActions[ index ] = []
        self.activeActionList = self.finalActions[ index ]

    def beginPreTerminateActions( self, index = 0 ):
        self.preTerminateActions[ index ] = []
        self.activeActionList = self.preTerminateActions[ index ]
        
    def beginTerminateActions( self, index = 0 ):
        self.terminateActions[ index ] = []
        self.activeActionList = self.terminateActions[ index ]
        
    def generateOutput( self, formatter, targetName ):
        # Generate benchmark section header.
        if ' ' in self.name:
            # Put quotes around the benchmark name because it contains spaces.
            formatter.StartSection( '"%s":BENCHMARK' % self.name )
        else:
            formatter.StartSection( '%s:BENCHMARK' % self.name )

        assert self.repeats < 0 or self.minRepeatTime < 0.0, "Repeats and MinRepeatTime are mutually exclusive benchmark attributes"
            
        if self.repeats >= 0:
            formatter.OutputAttribute( 'Repeats', self.repeats )

        if self.minRepeatTime > 0.0 :
            formatter.OutputAttribute( "MinRepeatTime", self.minRepeatTime )
            
        # Generate InitActions attribute.
        ikeys = self.initActions.keys()
        ikeys.sort()
        for k in ikeys:
            initActionNames = [ action.name for action in self.initActions[ k ] ]
            if initActionNames:
                initActionsValue = '+'.join( initActionNames )
            else:
                initActionsValue = 'None'
            if k == 0:
                formatter.OutputAttribute( 'InitActions', initActionsValue )
            else:
                formatter.OutputAttribute( 'InitActions%d' % k, initActionsValue )

        # Generate BenchmarkActions attribute.
        bkeys = self.benchmarkActions.keys()
        bkeys.sort()
        for k in bkeys:
            benchmarkActionNames = [ action.name for action in self.benchmarkActions[ k ] ]
            assert benchmarkActionNames, 'No benchmark actions!'
            benchmarkActionsValue = '+'.join( benchmarkActionNames )            
            if k == 0:
                formatter.OutputAttribute( 'BenchmarkActions', benchmarkActionsValue )
            else:
                formatter.OutputAttribute( 'BenchmarkActions%d' % k, benchmarkActionsValue )

        # Generate FinalActions attribute.
        fkeys = self.finalActions.keys()
        fkeys.sort()
        for k in fkeys:
            finalActionNames = [ action.name for action in self.finalActions[ k ] ]
            if finalActionNames:
                finalActionsValue = '+'.join( finalActionNames )
            else:
                finalActionsValue = 'None'
            if k == 0:
                formatter.OutputAttribute( 'FinalActions', finalActionsValue )
            else:
                formatter.OutputAttribute( 'FinalActions%d' % k, finalActionsValue )

        # Generate PreTerminateActions attribute.
        ptkeys = self.preTerminateActions.keys()
        ptkeys.sort()
        for k in ptkeys:
            preTerminateActionNames = [ action.name for action in self.preTerminateActions[ k ] ]
            if preTerminateActionNames:
                preTerminateActionsValue = '+'.join( preTerminateActionNames )
            else:
                preTerminateActionsValue = 'None'
            if k == 0:
                formatter.OutputAttribute( 'PreTerminateActions', preTerminateActionsValue )
            else:
                formatter.OutputAttribute( 'PreTerminateActions%d' % k, preTerminateActionsValue )
                
        # Generate TerminateActions attribute.
        tkeys = self.terminateActions.keys()
        tkeys.sort()
        for k in tkeys:
            terminateActionNames = [ action.name for action in self.terminateActions[ k ] ]
            if terminateActionNames:
                terminateActionsValue = '+'.join( terminateActionNames )
            else:
                terminateActionsValue = 'None'
            if k == 0:
                formatter.OutputAttribute( 'TerminateActions', terminateActionsValue )
            else:
                formatter.OutputAttribute( 'TerminateActions%d' % k, terminateActionsValue )

        for k in bkeys:
            if k in ikeys:
                ikeys.remove( k )
            if k in fkeys:
                fkeys.remove( k )
            if k in ptkeys:
                ptkeys.remove( k )
            if k in tkeys:
                tkeys.remove( k )

        assert not ikeys ,"InitActions without matching benchmark actions!"
        assert not fkeys, "FinallyActions without matching benchmark actions!"
        assert not fkeys, "TerminateActions without matching benchmark actions!"                
            
        formatter.OutputAttribute( 'Target', targetName )

def loadSpandexActionModules( actionCallback ):
    actionModules = {}
    if not os.path.isdir( 'configs' ):
        console.exitWithError( 'Directory "configs" is missing' )
    configPaths = glob.glob( 'configs/*config.py' )
    if not configPaths:
        raise 'No config files found in directory configs'

    for configPath in configPaths:
        config = action.ProcessModuleConfigFile( configPath )
        for moduleName in config.GetModuleNames():
            assert moduleName not in actionModules
            module = ActionModule( moduleName, config.GetActionConfigs( moduleName ), actionCallback )
            actionModules[moduleName] = module

    return actionModules
    

