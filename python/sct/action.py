#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, random, copy, tempfile, types

maxint   = sys.maxint
minint   = -sys.maxint - 1
maxfloat = 3.402823466e+38
minfloat = -maxfloat

class LocalVariable:
    def __init__( self, name, definition ):
        self.name       = name
        self.definition = definition

    def __eq__( self, other ):
        return self.__class__ == other.__class__ and self.name == other.name and self.definition == other.definition 

    def GenerateInitialization( self, formatter ):
        pass

    def GenerateDestruction( self, formatter ):
        pass

class AttributeConfig:
    def __init__( self, name, ctype, pyTypes ):
        assert len( name ) > 0
        self.name            = name
        self.ctype           = ctype
        
        if type( pyTypes ) == types.TypeType:
            pyTypes = [ pyTypes ]
            
        self.pyTypes         = pyTypes
        self.localVariables  = []

    def __str__( self ):
        raise NotImplementedError

    def FormatValueForSpandex( self, value ):
        return str( value )

    def AddLocalVariable( self, variable ):
        self.localVariables.append( variable )

    def SetTestValues( self, values ):
        self.testValues = values

    def GetTestValues( self ):
        return self.testValues

    def GenerateAttributeMissingError( self, formatter ):
        errorMessage = 'Missing or invalid %s attribute for %s action.' % ( self.name, self.actionName, )
        formatter.GenerateError( errorMessage )

    def MakeAttributeValueErrorMessage( self, value = None ):
        if value != None:
            errorMessage = 'Invalid value %s for %s attribute in %s action.\\n' % ( value, self.name, self.actionName, )
        else:
            errorMessage = 'Invalid value for %s attribute in %s action.\\n' % ( self.name, self.actionName, )
            
        errorMessage += 'Correct value specification: %s' % str( self )
        
        return errorMessage

    def GenerateAttributeValueError( self, formatter ):
        errorMessage = self.MakeAttributeValueErrorMessage()
        formatter.GenerateError( errorMessage )

    def GetCType( self ):
        return self.ctype

    def GetCName( self ):
        return self.name[ 0 ].lower() + self.name[ 1: ]
    
    def GetStructItem( self ):
        return '%s %s;' % ( self.GetCType(), self.GetCName() )

    def SetActionConfig( self, config ):
        self.action     = config
        self.actionName = config.type
    
    def GenerateCType( self, f ):
        pass

    def IsLegalPythonValue( self, value ):
        return self.validator.IsLegalPythonValue( value )

    def GetRandomIllegalToken( self ):
        values = self.GetIllegalValues()
        
        if values:
            return str( random.choice( values ) )
        
        return None

class NumericalValidator:
    def __init__( self, minValue, maxValue ):
        self.minValue = minValue
        self.maxValue = maxValue

    def IsValidationNeeded( self ):
        return self.minValue != None or self.maxValue != None

    def IsLegalPythonValue( self, value ):
        if self.minValue != None:
            if self.maxValue != None:
                return self.minValue <= value <= self.maxValue
            return value >= self.minValue
        elif self.maxValue != None:
            return value <= self.maxValue
        return True

class IntValidator( NumericalValidator ):
    def IsLegalPythonValue( self, value ):
        if type( value ) != types.IntType:
            return False
        return NumericalValidator( self, value )

class FloatValidator( NumericalValidator ):
    def IsLegalPythonValue( self, value ):
        if type( value ) not in [types.IntType, types.FloatType]:
            return False
        return NumericalValidator( self, value )

class Hex32Validator:
    def IsLegalPythonValue( self, value ):
        if type( value ) != types.StringType or value[ :2 ] != '0x':
            return False
        try:
            numericalValue = int( value, 16 )
            if not 0 <= numericalValue <= 2**32:
                return False
        except ValueError:
            # We end up here if int() cannot convert the value.
            return False
        return True

class CIntValidator( NumericalValidator ):    
    def IsLegalPythonValue( self, value ):
        conditions = [ '<=', '<', '>=', '>', '=' ]  # MUST BE IN THIS ORDER
        
        if type( value ) == types.StringType:
            if value == '-':
                return True
            for c in conditions:
                if value.startswith( c ):
                    return NumericalValidator( self, int( value[ len( c ) : ] ) )
            return False
                
        if type( value ) != types.IntType and type( value ) not in ( types.TupleType, types.ListType ):
            return False

        if type( value ) == types.IntType:
            return NumericalValidator( self, value )

        else:
            if len( value ) != 2:
                return False

            if value[ 0 ] == '-' and value[ 1 ] == 0:
                return True
            
            if value[ 0 ] not in conditions:
                return False

            return NumericalValidator( self, value[ 1 ] )
            
class VectorValidator:
    def __init__( self, numericalValidator ):
        assert isinstance( numericalValidator, NumericalValidator )
        self.numericalValidator = numericalValidator
    
    def IsLegalPythonValue( self, value ):
        legal = type( value ) == types.ListType
        # Some list comprehension magic. Note that [] is false in Python, if
        # there is anything in a list it is not considered false in a
        # test. So the idea is to construct a list that has an item (in this
        # case number 1, but it could be anything) for each item of the
        # value that is not legal.
        return legal and not [ 1 for number in value if not self.numericalValidator.IsLegalPythonValue( number )]

class ArrayValidator:
    def __init__( self, numericalValidator, length ):
        assert isinstance( numericalValidator, NumericalValidator )
        assert length > 0
        self.numericalValidator = numericalValidator
        self.length             = length

    def IsLegalPythonValue( self, value ):
        legal = type( value ) == types.ListType
        legal = legal and len( value ) == self.length
        # Some list comprehension magic. Note that [] is false in Python, if
        # there is anything in a list it is not considered false in a
        # test. So the idea is to construct a list that has an item (in this
        # case number 1, but it could be anything) for each item of the
        # value that is not legal.
        return legal and not [ 1 for number in value if not self.numericalValidator.IsLegalPythonValue( number ) ]

class IntAttributeBase( AttributeConfig ):
    def __init__( self, name, pyTypes, minValue = None, maxValue = None ):
        AttributeConfig.__init__( self, name, 'int', pyTypes )
        self.numericalValidator = IntValidator( minValue, maxValue )

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsInt( attributes, "%s", &data->%s ) != SCT_TRUE )' % ( self.name, self.GetCName() ) )
        self.GenerateAttributeMissingError( formatter )

        formatter.AddComment( '%s attribute validation.' % self.name )
        self.DoGenerateVariableValidation( formatter, 'data->%s' % self.GetCName() )
    
    def DoGenerateVariableValidation( self, formatter, variableName ):
        minValue = self.numericalValidator.minValue
        maxValue = self.numericalValidator.maxValue
        
        if minValue != None:
            formatter.Output( 'if( %s < %d )' % ( variableName, minValue, ) )
            self.GenerateAttributeValueError( formatter )
            
        if maxValue != None:
            formatter.Output( 'if( %s > %d )' % ( variableName, maxValue, ) )
            self.GenerateAttributeValueError( formatter )
        
class IntAttribute( IntAttributeBase ):
    def __init__( self, name, minValue = None, maxValue = None ):
        IntAttributeBase.__init__( self, name, types.IntType, minValue, maxValue )
        self.validator = self.numericalValidator

        testValues = []
        if minValue != None:
            testValues.append( minValue )
        if maxValue != None:
            testValues.append( maxValue )
        if not testValues:
            testValues = [ 0, 1 ]
        self.SetTestValues( testValues )
        
    def __str__( self ):
        return 'Integer ' + RangeToStr( self.validator.minValue, self.validator.maxValue )

    def GetRandomLegalToken( self ):
        return str( saturatedRandInt( self.numericalValidator.minValue, self.numericalValidator.maxValue ) )
        
    def GetIllegalValues( self ):
        return _GetIllegalIntValues( self.numericalValidator.minValue, self.numericalValidator.maxValue )

class EnumIntAttribute( IntAttributeBase ):
    def __init__( self, name, values ):
        IntAttributeBase.__init__( self, name, types.IntType )
        self.values = values
        self.SetTestValues( values )

    def __str__( self ):
        return FormatEnumValues( map( str, self.values ) )

    def DoGenerateValidationCode( self, formatter ):
        expr = ' && '.join( map( lambda v: 'data->%s != %d' % ( self.GetCName(), v ), self.values ) )
        formatter.Output( 'if( ' + expr + ' )' )
        self.GenerateAttributeValueError( formatter )

    def IsLegalPythonValue( self, value ):
        return value in self.values

    def GetRandomLegalToken( self ):
        return str( random.choice( self.values ) )

    def GetRandomIllegalToken( self ):
        while 1:
            value = _GetIllegalIntValues( minint, maxint )
            if value not in self.values:
                return value

class FloatAttributeBase( AttributeConfig ):
    def __init__( self, name, pyTypes, minValue = None, maxValue = None, doublePrecision = False ):
        self.doublePrecision = doublePrecision
        if doublePrecision:
            ctype = 'double'
        else:
            ctype = 'float'
        AttributeConfig.__init__( self, name, ctype, pyTypes )
        self.numericalValidator = FloatValidator( minValue, maxValue )

    def FormatValueForSpandex( self, value ):
        formattedValue = str( value )
        if 'e' in formattedValue:
            # Fall back to decimal formatting if calling str create a scientific
            # result (which Spandex cannot parse).
            if self.doublePrecision:
                return '%.15f' % value
            return '%.10f' % value
        return formattedValue

    def DoGenerateVariableValidation( self, formatter, variableName ):
        minValue = self.numericalValidator.minValue
        maxValue = self.numericalValidator.maxValue
        
        if minValue != None:
            formatter.Output( 'if( %s < %f )' % ( variableName, minValue ) )
            self.GenerateAttributeValueError( formatter )
            
        if maxValue != None:
            formatter.Output( 'if( %s > %f )' % ( variableName, maxValue ) )
            self.GenerateAttributeValueError( formatter )

class FloatAttribute( FloatAttributeBase ):
    def __init__( self, name, minValue = None, maxValue = None ):
        FloatAttributeBase.__init__( self, name, [ types.IntType, types.FloatType ],
                                     minValue, maxValue )
        self.validator = self.numericalValidator
        if minValue == None:
            self.SetTestValues( [ 0.0 ] )
        else:
            self.SetTestValues( [ minValue ] )
    
    def __str__( self ):
        return 'Float ' + RangeToStr( self.numericalValidator.minValue, self.numericalValidator.maxValue )

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsFloat( attributes, "%s", &data->%s ) != SCT_TRUE )' % ( self.name, self.GetCName() ) )
        self.GenerateAttributeMissingError( formatter )
        
        if self.numericalValidator.IsValidationNeeded():
            formatter.AddComment( '%s attribute validation.' % self.name )
            self.DoGenerateVariableValidation( formatter, 'data->%s' % self.GetCName() )

    def GetRandomLegalToken( self ):
        return '%.6f' % randFloat( self.numericalValidator.minValue, self.numericalValidator.maxValue )

    def GetIllegalValues( self ):
        values = [ 'Nan', '+', '-', 'Inf', 'abc' ]
        if self.numericalValidator.minValue >= 0.0:
            values.append( '-1' )
        if self.numericalValidator.minValue > 0.0:
            values.append( '0' )
        return values

class FloatArrayAttribute( FloatAttributeBase ):
    def __init__( self, name, length, minValue = None, maxValue = None, doublePrecision = False ):
        assert length > 0
        FloatAttributeBase.__init__( self, name, types.ListType, minValue, maxValue, doublePrecision )
        self.length = length
        self.validator = ArrayValidator( self.numericalValidator, length )

        if self.numericalValidator.IsValidationNeeded():
            self.AddLocalVariable( LocalVariable( 'i', 'int i' ) )

        testValues  = []
        if minValue == None:
            testValues.append( length * ( 0.0, ) )
        else:
            testValues.append( length * ( minValue, ) )
        if maxValue == None:
            testValues.append( length * ( 1.0, ) )
        else:
            testValues.append( length * ( maxValue, ) )
        self.SetTestValues( testValues )

    def __str__( self ):
        return 'Array of %d floats ' % self.length + RangeToStr( self.numericalValidator.minValue, self.numericalValidator.maxValue )

    def FormatValueForSpandex( self, value ):
        assert type( value ) in self.pyTypes
        assert len( value ) == self.length
        return FormatSpandexArray( [ FloatAttributeBase.FormatValueForSpandex( self, item ) for item in value ] )

    def GetStructItem( self ):
        return 'float %s[%d];' % ( self.GetCName(), self.length )

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsFloatArray( attributes, "%s", data->%s, %d ) != SCT_TRUE )' % ( self.name, self.GetCName(), self.length ) )
        self.GenerateAttributeMissingError( formatter )

        if self.numericalValidator.IsValidationNeeded():
            formatter.AddComment( '%s attribute validation.' % self.name )
            # Value validation generation.
            formatter.Output( 'for( i = 0; i < %d; i++ )' % self.length )
            formatter.StartBlock()
            self.DoGenerateVariableValidation( formatter, 'data->%s[i]' % self.GetCName() )
            formatter.EndBlock()

    def GetRandomLegalToken( self ):
        return FormatSpandexArray( [ '%.6f' % randFloat( self.numericalValidator.minValue, self.numericalValidator.maxValue )
                                   for i in range( self.length ) ] )

    def GetIllegalValues( self ):
        return [ '{}', '{', '}', '[]', '"test"', '1', '-1', '{ 1; 2 }', '{ abc }' ]

class DoubleArrayAttribute( FloatArrayAttribute ):
    def __init__( self, name, length, minValue = None, maxValue = None ):
        FloatArrayAttribute.__init__( self, name, length, minValue, maxValue, True )

    def __str__( self ):
        return 'Array of %d doubles ' % self.length + RangeToStr( self.numericalValidator.minValue, self.numericalValidator.maxValue )

    def GetStructItem( self ):
        return 'double %s[%d];' % ( self.GetCName(), self.length )

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsDoubleArray( attributes, "%s", data->%s, %d ) != SCT_TRUE )' % ( self.name, self.GetCName(), self.length ) )
        self.GenerateAttributeMissingError( formatter )

        if self.numericalValidator.IsValidationNeeded():
            formatter.AddComment( '%s attribute validation.' % self.name )
            # Value validation generation.
            formatter.Output( 'for( i = 0; i < %d; i++ )' % self.length )
            formatter.StartBlock()
            self.DoGenerateVariableValidation( formatter, 'data->%s[i]' % self.GetCName() )
            formatter.EndBlock()

class IntArrayAttribute( IntAttributeBase ):
    def __init__( self, name, length, minValue = None, maxValue = None ):
        assert length > 0
        IntAttributeBase.__init__( self, name, types.ListType, minValue, maxValue )
        self.validator = ArrayValidator( self.numericalValidator, length )

        self.length = length

        testValues  = []
        if minValue == None:
            testValues.append( length * ( 0, ) )
        else:
            testValues.append( length * ( minValue, ) )
        if maxValue == None:
            testValues.append( length * ( 1, ) )
        else:
            testValues.append( length * ( maxValue, ) )
        self.SetTestValues( testValues )

    def __str__( self ):
        return 'Array of %d integers ' % self.length + RangeToStr( self.numericalValidator.minValue, self.numericalValidator.maxValue )

    def FormatValueForSpandex( self, value ):
        assert type( value ) in self.pyTypes
        assert len( value ) == self.length
        return FormatSpandexArray( map( str, value ) )

    def GetStructItem( self ):
        return 'int %s[%d];' % ( self.GetCName(), self.length )

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsIntArray( attributes, "%s", data->%s, %d ) != SCT_TRUE )' % ( self.name, self.GetCName(), self.length ) )
        self.GenerateAttributeMissingError( formatter )

        if self.numericalValidator.IsValidationNeeded():
            formatter.AddComment( '%s attribute validation.' % self.name )
            # Value validation generation.
            formatter.Output( 'for( i = 0; i < %d; i++ )' % self.length )
            formatter.StartBlock()
            self.DoGenerateVariableValidation( formatter, 'data->%s[i]' % self.GetCName() )
            formatter.EndBlock()

    def GetRandomLegalToken( self ):
        return FormatSpandexArray( [ str( saturatedRandInt( self.numericalValidator.minValue, self.numericalValidator.maxValue ) )
                                     for i in range( self.length ) ] )
    
    def GetIllegalValues( self ):
        return [ '{}', '{', '}', '[]', '"test"', '1', '-1', '{ 1; 2 }', '{ abc }, { 1.0 }' ]

class FloatVectorAttribute( FloatAttributeBase ):
    def __init__( self, name, minValue = None, maxValue = None, doublePrecision = False ):
        FloatAttributeBase.__init__( self, name, types.ListType, minValue, maxValue, doublePrecision )
        self.validator = VectorValidator( FloatValidator( minValue, maxValue ) )

        if self.numericalValidator.IsValidationNeeded():
            self.AddLocalVariable( LocalVariable( 'i', 'int i' ) )

        testValues = []
        if minValue != None:
            testValues.append( ( minValue, ) )
        if maxValue != None:
            testValues.append( ( maxValue, ) )
        if minValue == None and maxValue == None:
            testValues.append( ( 0.0, 1.0 ) )
        self.SetTestValues( testValues )
    
    def __str__( self ):
        return 'Vector of floats ' + RangeToStr( self.numericalValidator.minValue, self.numericalValidator.maxValue )

    def FormatValueForSpandex( self, value ):
        assert type( value ) in self.pyTypes
        return FormatSpandexArray( [ FloatAttributeBase.FormatValueForSpandex( self, item ) for item in value ] )

    def GetStructItem( self ):
        return 'SCTFloatVector *%s;' % self.GetCName()

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsFloatVector( attributes, "%s", &data->%s ) != SCT_TRUE )'% ( self.name, self.GetCName() ) )
        self.GenerateAttributeMissingError( formatter )

        if self.numericalValidator.IsValidationNeeded():
            formatter.AddComment( '%s attribute validation.' % self.name )
            formatter.Output( 'for( i = 0; i < data->%s->length; i++ )' % self.GetCName() )
            formatter.StartBlock()
            self.DoGenerateVariableValidation( formatter, 'data->%s->data[i]' % self.GetCName() )
            formatter.EndBlock()

    def GetRandomLegalToken( self ):
        return FormatSpandexArray( [ '%.6f' % randFloat( self.numericalValidator.minValue, self.numericalValidator.maxValue )
                                     for i in range( random.randint( 1, 10 ) ) ] )

    def GetIllegalValues( self ):
        return [ '{}', '{', '}', '[]', '"test"', '1', '-1', '{ 1; 2 }', '{ abc }' ]

class DoubleVectorAttribute( FloatVectorAttribute ):
    def __init__( self, name, minValue = None, maxValue = None ):
        FloatVectorAttribute.__init__( self, name, minValue, maxValue, True )

    def __str__( self ):
        return 'Vector of doubles ' + RangeToStr( self.numericalValidator.minValue, self.numericalValidator.maxValue )

    def GetStructItem( self ):
        return 'SCTDoubleVector *%s;' % self.GetCName()

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsDoubleVector( attributes, "%s", &data->%s ) != SCT_TRUE )'% ( self.name, self.GetCName() ) )
        self.GenerateAttributeMissingError( formatter )

        if self.numericalValidator.IsValidationNeeded():
            formatter.AddComment( '%s attribute validation.' % self.name )
            formatter.Output( 'for( i = 0; i < data->%s->length; i++ )' % self.GetCName() )
            formatter.StartBlock()
            self.DoGenerateVariableValidation( formatter, 'data->%s->data[i]' % self.GetCName() )
            formatter.EndBlock()

class IntVectorAttribute( IntAttributeBase ):
    def __init__( self, name, minValue = None, maxValue = None ):
        IntAttributeBase.__init__( self, name, types.ListType, minValue, maxValue )
        self.validator = VectorValidator( IntValidator( minValue, maxValue ) )
        if self.numericalValidator.IsValidationNeeded():
            self.AddLocalVariable( LocalVariable( 'i', 'int i' ) )

        testValues = []
        if minValue != None:
            testValues.append( ( minValue, ) )
        if maxValue != None:
            testValues.append( ( maxValue, ) )
        if minValue == None and maxValue == None:
            testValues.append( ( 0, 1 ) )
        self.SetTestValues( testValues )
    
    def __str__( self ):
        return 'Vector of ints ' + RangeToStr( self.validator.numericalValidator.minValue, self.validator.numericalValidator.maxValue )

    def FormatValueForSpandex( self, value ):
        assert type( value ) in self.pyTypes
        return FormatSpandexArray(  map( str, value ) )

    def GetStructItem( self ):
        return 'SCTIntVector *%s;' % self.GetCName()

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsIntVector( attributes, "%s", &data->%s ) != SCT_TRUE )'% ( self.name, self.GetCName() ) )
        self.GenerateAttributeMissingError( formatter )

        if self.numericalValidator.IsValidationNeeded():
            formatter.AddComment( '%s attribute validation.' % self.name )
            formatter.Output( 'for( i = 0; i < data->%s->length; i++ )' % self.GetCName() )
            formatter.StartBlock()
            self.DoGenerateVariableValidation( formatter, 'data->%s->data[i]' % self.GetCName() )
            formatter.EndBlock()

    def GetRandomLegalToken( self ):
        return FormatSpandexArray( [ str( saturatedRandInt( self.numericalValidator.minValue, self.numericalValidator.maxValue ) )
                                     for i in range( random.randint( 1, 10 ) ) ] )

    def GetIllegalValues( self ):
        return [ '{}', '{', '}', '[]', '"test"', '1', '-1', '{ 1; 2 }', '{ abc }, { 1.0 }' ]

class EnumAttribute( AttributeConfig ):
    _allValues = []

    def __init__( self, name, ctype, values ):
        assert values
        AttributeConfig.__init__( self, name, ctype, types.StringType )
        self.AddLocalVariable( LocalVariable( 'attribute', 'SCTAttribute *attribute' ) )
        
        self.values = values
        for value in values:
            if value not in EnumAttribute._allValues:
                EnumAttribute._allValues.append( value )

        self.SetTestValues( values )
        
    def __str__( self ):
        return FormatEnumValues( self.values )

    def GetEnum( self, value ):
        return value

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( ( attribute = sctFindAttribute( attributes, "%s" ) ) == NULL )' % self.name )
        self.GenerateAttributeMissingError( formatter )
        # Value validation generation.
        conditional = 'if'
        for value in self.values:
            formatter.Output( conditional + '( strcmp( attribute->value, "%s" ) == 0 )' % value )
            formatter.StartBlock()
            formatter.Output( 'data->%s = %s;' % ( self.GetCName(), self.GetEnum( value ) ) )
            formatter.EndBlock()
            # Change the conditional to "else if" after the 1st value
            conditional = 'else if'
        formatter.Output( 'else' )
        self.GenerateAttributeValueError( formatter )

    def IsLegalPythonValue( self, value ):
        return value in self.values

    def GetRandomLegalToken( self ):
        return random.choice( self.values )

    def GetIllegalValues( self ):
        values = copy.copy( EnumAttribute._allValues )
        for value in self.values:
            values.remove( value )
        return values

class EnumMaskAttribute( AttributeConfig ):
    def __init__( self, name, ctype, values ):
        assert values
        AttributeConfig.__init__( self, name, ctype, types.ListType )
        self.AddLocalVariable( LocalVariable( 'stringList', 'SCTStringList *stringList = NULL' ) )
        self.AddLocalVariable( LocalVariable( 'string', 'char *string' ) )
        self.AddLocalVariable( LocalVariable( 'iter', 'SCTStringListIterator iter' ) )

        self.values = values
        self.SetTestValues( [ (value,) for value in values ] )

    def __str__( self ):
        return 'Array of enumerations (%s)' % FormatEnumValues( self.values )

    def FormatValueForSpandex( self, value ):
        assert type( value ) in self.pyTypes
        return FormatSpandexArray( map( str, value ) )

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsStringList( attributes, "%s", &stringList ) != SCT_TRUE )' % self.name )
        self.GenerateAttributeMissingError( formatter )

        formatter.Output( 'data->%s = 0;' % self.GetCName() )
        formatter.Output( 'sctGetStringListIterator( stringList, &iter );' )
        formatter.Output( 'while( (string = sctGetNextStringListElement( &iter ) ) != NULL )' )
        formatter.StartBlock()
        conditional = 'if'
        for value in self.values:
            formatter.Output( conditional + '( strcmp( string, "%s" ) == 0 )' % value )
            formatter.StartBlock()
            formatter.Output( 'data->%s |= %s;' % ( self.GetCName(), value ) )
            formatter.EndBlock()
            # Change the conditional to "else if" after the 1st value
            conditional = 'else if'
        # Empty strings are accepted and cause no action
        formatter.Output( 'else if( strcmp( string, "" ) == 0 ) {}' )
        formatter.Output( 'else' )
        self.GenerateAttributeValueError( formatter )
        formatter.EndBlock()
        formatter.Output( 'sctDestroyStringList( stringList );' )

    def IsLegalPythonValue( self, value ):
        if not isinstance( value, list ):
            return False
        for item in value:
            if item not in self.values:
                return False
        return True

    def GetRandomLegalToken( self ):
        count  = random.randint( 1, len( self.values ) - 1 )
        values = set()
        while len( values ) < count:
            values.add( random.choice( self.values ) )
        return FormatSpandexArray( values )

class AutoEnumAttribute( EnumAttribute ):
    def __init__( self, name, values ):
        EnumAttribute.__init__( self, name, None, values )
        
    def GetCType( self ):
        return 'E%s%s' % ( self.actionName, self.name, )

    def GetEnum( self, value ):
        return '%s_%s_%s' % ( self.actionName.upper(), self.name.upper(), value.upper(), )

    def GenerateCType( self, f ):
        f.Output( 'typedef enum' )
        f.StartBlock()
        for enum in map( self.GetEnum, self.values ):
            f.Output( enum + ',' )
        f.EndBlock( ' %s;' % self.GetCType() )

class OnOffAttribute( EnumAttribute ):
    def __init__( self, name ):
        EnumAttribute.__init__( self, name, 'SCTBoolean', [ 'ON', 'OFF' ] )

    def GetEnum( self, value ):
        return { 'ON'  : 'SCT_TRUE',
                 'OFF' : 'SCT_FALSE' }[ value ]

    def GenerateCType( self, formatter ):
        pass

class StringAttribute( AttributeConfig ):
    def __init__( self, name, length ):
        assert length > 0
        AttributeConfig.__init__( self, name, 'char *', types.StringType )
        self.length = length

    def __str__( self ):
        return 'String with maximum of %d characters)' % self.length

    def FormatValueForSpandex( self, value ):
        if value.startswith( '"' ) and value.endswith( '"' ):
            return value
        else:
            return '"' + value + '"'
    
    def GetStructItem( self ):
        return 'char %s[%d];' % ( self.GetCName(), self.length, )

    def GenerateAttributeParser( self, formatter ):
        # Note: one subtracted from the length to accommodate the NULL
        # character.
        formatter.Output( 'if( sctGetAttributeAsString( attributes, "%s", data->%s, %d ) != SCT_TRUE )' % ( self.name, self.GetCName(), self.length - 1, ) )
        self.GenerateAttributeMissingError( formatter )
        # No validation code needed.

    def IsLegalPythonValue( self, value ):
        delimiters = [ '[', ']', '@', ':', '+' ]
        if [ True for delimiter in delimiters if delimiter in value ]:
            return value[ 0 ] == '"' and value[ -1 ] == '"'
        return True

    def GetRandomLegalToken( self ):
        token = '"'
        for i in range( random.randint( 1, 100 ) ):
            token += chr( random.randint( 0, 255 ) )
        token += '"'
        return token

    def GetIllegalValues( self ):
        return [ '', '0', 'None', '0, 1', '{1,2,3}' ]

class Hex32Attribute( AttributeConfig ):
    def __init__( self, name ):
        AttributeConfig.__init__( self, name, 'unsigned int', types.StringType )
        self.SetTestValues( ['0x0', '0xffffffff'] )
        self.validator = Hex32Validator()

    def __str__( self ):
        return '32-bit hexadecimal integer'

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsHex( attributes, "%s", &data->%s ) != SCT_TRUE )' % ( self.name, self.GetCName(), ) )
        self.GenerateAttributeMissingError( formatter )
        # No validation code needed.

    def FormatValueForSpandex( self, value ):
        assert value[ :2 ] == '0x'
        return value

class Hex32ArrayAttribute( AttributeConfig ):
    def __init__( self, name, length ):
        self.length = length
        AttributeConfig.__init__( self, name, 'unsigned int', types.ListType )
        self.validator = Hex32Validator()

        self.SetTestValues( [ ( '0x0', ), ( '0xffffffff', ), ( '0x0', '0xffffffff' ) ] )

    def __str__( self ):
        return 'Array of %d 32-bit hexadecimal numbers' % self.length

    def IsLegalPythonValue( self, value ):
        for item in value:
            if not self.validator.IsLegalPythonValue( item ):
                return False
        return True

    def FormatValueForSpandex( self, value ):
        assert type( value ) in self.pyTypes
        assert len( value ) == self.length
        return FormatSpandexArray( map( str, value ) )

    def GetStructItem( self ):
        return 'unsigned int %s[%d];' % ( self.GetCName(), self.length, )

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsHexArray( attributes, "%s", data->%s, %d ) != SCT_TRUE )' % ( self.name, self.GetCName(), self.length, ) )
        self.GenerateAttributeMissingError( formatter )

    def GetRandomLegalToken( self ):
        count  = expInt( 1, 2, 100 )
        values = []
        while len( values ) < count:
            values.append( '0x%x' % random.randint( 0, 2**32 - 1 ) )
        return FormatSpandexArray( values )

class Hex32VectorAttribute( AttributeConfig ):
    def __init__( self, name ):
        AttributeConfig.__init__( self, name, 'unsigned int', types.StringType )

    def __str__( self ):
        return 'Vector of 32-bit hexadecimal numbers'

    def FormatValueForSpandex( self, value ):
        assert 0, value
        assert type( value ) in self.pyTypes
        return FormatSpandexArray( map( str, value ) )

    def GetStructItem( self ):
        return 'SCTHexVector *%s;' % self.GetCName()

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsHexVector( attributes, "%s", &data->%s ) != SCT_TRUE )' % ( self.name, self.GetCName(), ) )
        self.GenerateAttributeMissingError( formatter )

class CIntAttribute( AttributeConfig ):
    def __init__( self, name, minValue = None, maxValue = None ):
        AttributeConfig.__init__( self, name, 'SCTConditionalInt', [ types.IntType, types.StringType, types.TupleType, types.ListType ] )
        self.validator = CIntValidator( minValue, maxValue )

    def __str__( self ):
        return 'Conditional (<,<=,>,>=,=) integer %s, for example =32. Use single - for dontcare'

    def GenerateAttributeParser( self, formatter ):
        formatter.Output( 'if( sctGetAttributeAsCInt( attributes, "%s", &data->%s ) != SCT_TRUE )' % ( self.name, self.GetCName(), ) )
        self.GenerateAttributeMissingError( formatter )
        formatter.AddComment( '%s attribute validation.' % self.name )

        minValue = self.validator.minValue
        maxValue = self.validator.maxValue
        
        if minValue != None:
            formatter.Output( 'if( data->%s.value < %d )' % ( self.GetCName(), minValue, ) )
            self.GenerateAttributeValueError( formatter )
            
        if maxValue != None:
            formatter.Output( 'if( data->%s.value > %d )' % ( self.GetCName(), maxValue, ) )
            self.GenerateAttributeValueError( formatter )       
        
    def FormatValueForSpandex( self, value ):
        if type( value ) == types.IntType:
            return '=%d' % ( value, )
        elif type( value ) == types.StringType:
            return value
        else:
            if value[ 0 ] == '-' and value[ 1 ] == 0:
                return '-'
            return '%s%d' % ( value[ 0 ], value[ 1 ], )
        
def ConvertAttributeReferences( string, attributeConfigs ):
    pyAttributeNames = [ config.name for config in attributeConfigs ]
    cAttributeNames  = [ config.GetCName() for config in attributeConfigs ]
    cReferences      = [ 'data->%s' % name for name in cAttributeNames ]
    for pyName, cRef in zip( pyAttributeNames, cReferences ):
        # TODO: Direct replacement does not see the difference between the real
        # variable references and any other uses of the variable names, e.g.,
        # embedded in other names. For example, if variable reference 'Color' is
        # replaced, it will get replaced in 'ColorMask' and 'EColorRGB'
        # etc. This could be fixed by making more elaborate analysis of each
        # occurance of the variable name.
        string = string.replace( pyName, cRef )
    return string

class ValidatorConfig:
    def __init__( self, expr, failureMessage ):
        self.expr           = expr
        self.failureMessage = failureMessage

    def SetActionConfig( self, config ):
        self.actionConfig = config

    def GenerateC( self, f ):
        expr = ConvertAttributeReferences( self.expr, self.actionConfig.attributeConfigs )
        f.Output( 'if( %s )' % expr )
        f.GenerateError( self.failureMessage )

class ActionConfig:
    def __init__( self, type, moduleName, methods, attributeConfigs, validatorConfigs ):
        self.type                     = type
        self.moduleName               = moduleName
        self.methods                  = methods
        self.attributeConfigs         = attributeConfigs
        self.validatorConfigs         = validatorConfigs
        self.initActionTypes          = []
        
        for config in self.attributeConfigs:
            config.SetActionConfig( self )
            
        for config in self.validatorConfigs:
            config.SetActionConfig( self )

    def GetAttributeConfigs( self ):
        return self.attributeConfigs

    def SetTestValues( self, attributeName, values ):
        for config in self.attributeConfigs:
            if config.name == attributeName:
                config.SetTestValues( values )
                break
        else:
            raise ValueError( 'Attribute %s is not defined for action type %s' % ( attributeName, self.type, ) )

    def AddInitAction( self, actionType ):
        self.initActionTypes.append( actionType )

# Configuration file handling.

class ModuleConfig:
    def __init__( self, actionConfigs, validators, includes ):
        self.actionConfigs       = actionConfigs
        self.validators          = validators
        self.moduleActionConfigs = {}
        self.includes            = includes
        
        for config in actionConfigs:
            if config.moduleName not in self.moduleActionConfigs:
                self.moduleActionConfigs[config.moduleName] = []
            self.moduleActionConfigs[config.moduleName].append( config )

        for validator in validators:
            assert self.GetActionConfig( validator.moduleName, validator.actionType ), \
                   'Validator refers to an undefined action %s' % validator.actionType

    def GetModuleNames( self ):
        return self.moduleActionConfigs.keys()

    def GetActionConfig( self, moduleName, actionType ):
        for config in self.moduleActionConfigs[ moduleName ]:
            if config.type == actionType:
                return config
        return None

    def GetActionConfigs( self, moduleName ):
        return self.moduleActionConfigs[ moduleName ]

class ValueValidator:
    def __init__( self, actionType, moduleName, expr ):
        self.actionType = actionType
        self.moduleName = moduleName
        self.expr       = expr

    def Validate( self, env, attributes, callLog ):
        try:
            if not eval( self.expr, globals(), env ):
                return 'Value validation for %s@%s action failed:\n' \
                       'Failed expression: %s' % ( self.actionType, self.moduleName, self.expr, )
        except Exception, e:
            return 'Exception while validating %s@%s action:\n' \
                   'Expression: %s\n' \
                   'Exception:\n%s' % ( self.actionType, self.moduleName, self.expr, e, )
        return None

class CallValidator:
    def __init__( self, actionType, moduleName, callName, callCount ):
        self.actionType = actionType
        self.moduleName = moduleName
        self.callName   = callName
        self.callCount  = callCount

    def FormatError( self, actualCallCount ):
        return 'Call count validation for %s@%s action failed:\n' \
               'Expected call count %d, actual %d' % ( self.actionType, self.moduleName, self.callCount, actualCallCount, )

    def Validate( self, state, attributes, callLog ):
        callNameLog     = map( lambda a: a.split( '(' )[ 0 ], callLog )
        actualCallCount = callNameLog.count( self.callName )
        if actualCallCount != self.callCount:
            return self.FormatError( actualCallCount )
        return None

def ProcessModuleConfig( configData, defines = [] ):
    handle, filename = tempfile.mkstemp()
    f = file( filename, 'w' )
    f.write( configData )
    f.close()
    # TODO: Cannot destroy the temporary file here for some reason. Try to fix
    # this.
    return ProcessModuleConfigFile( filename, defines )

def FindActionConfig( actionConfigs, actionType ):
    for config in actionConfigs:
        if config.type == actionType:
            return config
    return None

def ProcessModuleConfigFile( filename, defines = [] ):
    actionConfigs = []
    includes      = []
    fileNames     = ['']
    validators    = []
    def SetModuleName( name ):
        fileNames[ 0 ] = name
    def GetModuleName():
        assert fileNames[ 0 ], 'Module name has not been set!'
        return fileNames[ 0 ]
    def AddActionConfig( name, methods, attributeConfigs, validators = []  ):
        actionConfigs.append( ActionConfig( name, GetModuleName(), methods, attributeConfigs, validators ) )
    def AddInclude( name ):
        includes.append( name )
    def AddStateValidators( actionType, exprs ):
        if isinstance( exprs, str ):
            exprs = [ exprs ]
        for expr in exprs:
            validators.append( ValueValidator( actionType, GetModuleName(), expr ) )
    def AddCallCountValidator( actionType, callName, callCount ):
        validators.append( CallValidator( actionType, GetModuleName(), callName, callCount ) )
    def SetTestValues( actionType, attributeName, values ):
        actionConfig = FindActionConfig( actionConfigs, actionType )
        if actionConfig:
            actionConfig.SetTestValues( attributeName, values )
        else:
            raise ValueError( 'Action type %s is not defined' % actionType )
    def SetInitAction( actionType, initActionType ):
        actionConfig = FindActionConfig( actionConfigs, actionType )
        if actionConfig:
            actionConfig.AddInitAction( initActionType )
        else:
            raise ValueError( 'Action type %s is not defined' % actionType )
    execfile( filename )
    return ModuleConfig( actionConfigs, validators, includes )

# Utility functions.

def saturatedRandInt( minValue, maxValue ):
    minValue = max( minValue, -1000 )
    maxValue = min( maxValue, 1000 ) or 1000
    mean     = ( minValue + maxValue ) / 2
    if minValue >= 0 and mean > 0:
        return expInt( minValue, mean, maxValue )
    else:
        return int( random.gauss( mean, ( maxValue - minValue ) / 4 ) )

def randFloat( minValue, maxValue ):
    minValue = max( minValue, -1000 )
    maxValue = min( maxValue, 1000 ) or 1000
    return random.uniform( minValue, maxValue )

def RangeToStr( lowerBound, upperBound ):
    if lowerBound == None and upperBound == None:
        return ''
    elif lowerBound == None:
        return '<= %s' % repr( upperBound )
    elif upperBound == None:
        return '>= %s' % repr( lowerBound )
    return 'in range %s - %s' % ( repr( lowerBound ), repr( upperBound ) )

def _GetIllegalIntValues( minValue, maxValue ):
    values = []
    if minValue > minint:
        values.append( minint )
    if minValue > minint + 1:
        values.append( minValue - 1 )
    # None is less than any number.
    if maxValue != None and maxValue < maxint:
        values.append( maxint )
    if maxValue != None and maxValue < maxint - 1:
        values.append( maxValue + 1 )
    return values

def FormatEnumValues( values ):
    if len( values ) == 1:
        return values[ 0 ]
    return '%s or %s' % (', '.join( values[ :-1 ] ), values[ -1 ] )

def FormatSpandexArray( values ):
    return '{' + ','.join( values ) +  '}'

def expInt( minValue, meanValue, maxValue ):
    assert minValue <= meanValue <= maxValue
    r = max( minValue, int( random.expovariate( 1.0 / meanValue ) ) )
    return int( min( maxValue, r ) )
