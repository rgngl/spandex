#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import os, sys, time, webbrowser, Image, ImageDraw, ImageFont
from sct.report import htmlbuilder
from sct.visualtest import image

def makeBookmarkLink( pageIndex, bookmark ):
    s = ''
    if pageIndex > 0:
        s = '%d' % pageIndex
    return '<a href="index%s.html%s">%s</a> ' % ( s, '#' + bookmark, bookmark )

def makeColoredText( text, color ):
    htmlColorCodes = { 'white'      : '#FFFFFF',
                       'black'      : '#000000',
                       'light blue' : '#0000FF',
                       'red'        : '#FF0000',
                       'yellow'     : '#FFFF00',
                       'green'      : '#00FF00',
                       'pink'       : '#FF00FF' }
    return '<font color="%s">%s</font>' % ( htmlColorCodes[ color ], text )

def makeWarningText( text ):
    return makeColoredText( text, 'pink' )

def makeImagesetTable( imageSetComparison ):
    referenceSet = imageSetComparison.referenceSet
    candidateSet = imageSetComparison.candidateSet

class ImageTable:
    def __init__( self ):
        self.tableData = [ [],[] ]
    
    def addImage( self, image, caption ):
        self.tableData[ 0 ].append( image )
        caption += ' ( %dx%d %s)' % ( image.size[ 0 ], image.size[ 1 ], image.mode )
        self.tableData[ 1 ].append( caption )

def makeSummaryRow( pageIndex, testCase, rgbText, alphaText = None ):
    link = makeBookmarkLink( pageIndex, testCase.name )
    if alphaText == None:
        alphaText = rgbText
    return [ link, rgbText, alphaText ]

def createNotFoundImage( size ):
    image = Image.new( 'RGB', size, ( 255, 0, 0 ) )
    draw = ImageDraw.Draw( image )
    draw.rectangle( ( ( 5, 5 ), ( size[ 0 ] -6 , size[ 1 ] -6 ) ), fill = ( 0, 0, 0 ) )
    
    try:
        font = ImageFont.truetype( 'arial.ttf', 15 )
    except IOError:
        font = ImageFont.load_default()
        
    draw.text( ( 10, ( size[ 1 ] - 15 ) / 2 ), 'Image not found', font = font )
    
    return image

def createImageComparisonReport( name, testCases, referenceImageLoader, candidateImageLoader, comparer, summary, testsPerPage = 500 ):
    notFoundImage = createNotFoundImage( ( 128, 128 ) )
    path = os.path.join( os.getcwd(), name )
    
    summaryTable = [ [ 'Test case', 'RGB', 'Alpha' ] ]

    resultBuilder = htmlbuilder.HTMLBuilder( path )

    missingImageCount = 0.0
    failureCount       = 0.0
    passedCount        = 0.0
    lastPercent        = -1
    testIndex          = 0
    pageIndex          = 0
    
    try:
        for i, case in enumerate( testCases ):
            progressPercent = i * 100 / len( testCases )
            
            if progressPercent > lastPercent:
                sys.stdout.write( 'Progress: %d%%\r' % progressPercent )
                lastPercent = progressPercent
            
            imageTable = ImageTable()

            referenceImage = referenceImageLoader.load( case.name )
            candidateImage = candidateImageLoader.load( case.name )

            resultBuilder.addPageBreak()
            resultBuilder.addBookmark( case.name )
            resultBuilder.addHeading( case.name, 2 )
            resultBuilder.addHeading( 'Description', 3 )
            
            if case.description:
                resultBuilder.addParagraph( case.description )
            else:
                resultBuilder.addParagraph( 'Test case has no description.' )

            if not referenceImage and not candidateImage:
                summaryRow = makeSummaryRow( pageIndex, case, makeWarningText( 'No image' ) )
                
                imageTable.addImage( notFoundImage, 'Reference' )
                imageTable.addImage( notFoundImage, 'Candidate' )
                
                missingImageCount += 2.0
                failureCount      += 1.0
                
            elif not referenceImage:
                summaryRow = makeSummaryRow( pageIndex, case, makeWarningText( 'Reference image missing' ) )
                
                imageTable.addImage( notFoundImage, 'Reference' )
                imageTable.addImage( candidateImage, 'Candidate' )
                
                missingImageCount += 1.0
                failureCount      += 1.0
                
            elif not candidateImage:
                summaryRow = makeSummaryRow( pageIndex, case, makeWarningText( 'Candidate image missing' ) )
                
                imageTable.addImage( referenceImage, 'Reference' )
                imageTable.addImage( notFoundImage, 'Candidate' )
                
                missingImageCount += 1.0
                failureCount      += 1.0
                
            elif not candidateImage.mode == referenceImage.mode:
                summaryRow = makeSummaryRow( pageIndex, case, makeWarningText( 'Image mode mismatch' ) )
                
                resultBuilder.addParagraph( 'Candidate and reference image have different color depths!' )
                imageTable.addImage( referenceImage, 'Reference' )
                imageTable.addImage( candidateImage, 'Candidate' )
                
                failureCount += 1.0
                
            elif not candidateImage.size == referenceImage.size:
                summaryRow = makeSummaryRow( pageIndex, case, makeWarningText( 'Image size mismatch' ) )
                
                resultBuilder.addParagraph( 'Candidate and reference image have different sizes!' )
                imageTable.addImage( referenceImage, 'Reference' )
                imageTable.addImage( candidateImage, 'Candidate' )
                
                failureCount += 1.0
                
            else:
                failure   = False
                imagePair = image.PILImagePair( case, referenceImage, candidateImage )
                result    = image.ImageComparison( case, imagePair, comparer )
                
                if not result.isRGBDiffAcceptable():
                    resultBuilder.addParagraph( 'RGB channels of the images are different.' )
                    imageTable.addImage( imagePair.referenceRgbImage, 'Reference RGB' )
                    imageTable.addImage( imagePair.candidateRgbImage, 'Candidate RGB' )
                    imageTable.addImage( imagePair.rgbDiffImage, 'RGB difference' )
                    imageTable.addImage( imagePair.negRgbDiffImage, 'Negative RGB difference' )
                    
                    rgbSummary = makeColoredText( 'FAIL', 'red' )
                    failure    = True
                    
                else:
                    rgbSummary = makeColoredText( 'PASS', 'green' )
                    
                if not result.isAlphaDiffAcceptable():
                    resultBuilder.addParagraph( 'Alpha channels of the images are different.' )
                    imageTable.addImage( imagePair.referenceAlphaImage, 'Reference alpha' )
                    imageTable.addImage( imagePair.candidateAlphaImage, 'Candidate alpha' )
                    imageTable.addImage( imagePair.alphaDiffImage, 'Alpha difference' )
                    imageTable.addImage( imagePair.negAlphaDiffImage, 'Negative alpha difference' )
                    
                    alphaSummary = makeColoredText( 'FAIL', 'red' )
                    failure      = True
                else:
                    alphaSummary = makeColoredText( 'PASS', 'green' )
                    
                if failure:
                    resultBuilder.addParagraph( 'Test FAILED.' )
                    failureCount += 1.0
                    
                else:
                    imageTable.addImage( imagePair.referenceImage, 'Reference' )
                    resultBuilder.addParagraph( 'Test PASSED.' )
                    passedCount += 1.0
                    
                summaryRow = makeSummaryRow( pageIndex, case, rgbSummary, alphaSummary )

            resultBuilder.addImageTable( imageTable )
            summaryTable.append( summaryRow )

            testIndex += 1
            if testIndex > testsPerPage:
                resultBuilder.nextPage()
                pageIndex += 1
                testIndex  = 0
                
    finally:
        # Create the main report
        builder = htmlbuilder.HTMLBuilder( path )
        builder.startHtml()
        builder.addHeading( 'Summary', 1 )
        builder.addParagraph( 'Report creation time: %s' % time.strftime( '%d.%m.%Y %H:%M' ) )
        builder.addParagraph( 'Passed cases: %d' % passedCount  )
        builder.addParagraph( 'Failed cases: %d' % failureCount  )
        builder.addParagraph( 'Missing images: %d' % missingImageCount  )
        builder.addParagraph( 'Pass rate: %.2f' % ( ( passedCount / ( failureCount + passedCount ) ) * 100.0 ) + '%' )
        builder.addParagraph( 'Location of candidate images: %s' % candidateImageLoader.path  )
        builder.addParagraph( 'Location of reference images: %s' % referenceImageLoader.path ) 

        # Add the result summary table
        builder.addTable( summaryTable )

        if not summary:
          # Add the detailed results
          builder.addHeading( 'Test case results', 1 )
          resultBuilder.addParagraph( 'END OF RESULTS.' )
          builder.combineBuilders( resultBuilder )
        
        builder.save()

        if failureCount or missingImageCount:
            print 'FAILED: %d tests failed, %d images missing' % ( failureCount, missingImageCount )
        else:
            print 'Test PASSED'
            
        print 'Report saved to %s' % name
