#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import os, Image, numpy, fnmatch
from sct.util import console

class TestCaseImageLoader:
    def __init__( self, path ):
        self.path = path
    
    def load( self, testcase_name ):
        filename = testcase_name.replace( ' ', '_' ) + '.png'
        path = os.path.join( self.path, filename )
        return loadImage( path )

def loadImage( path ):
    try:
        image = Image.open( path )
        # Force the image data to be loaded. This seems to prevent the "Too many
        # open files" problem.
        image.load()
        return image
    except IOError:
        console.log( 'Could not load image %s' % path )
        return None

def pilImageToArray( image ):
    if image.mode == 'L':
        shape = image.size
    elif image.mode == 'RGB':
        shape = image.size + (3,)
    elif image.mode == 'RGBA':
        shape = image.size + (4,)
    else:
        raise NotImplementedError( 'Image mode %s' % image.mode )
    # Using numpy.fromstring and image.tostring is *much* faster than reshaping
    # the list resulting from list( image.getdata() ).
    return numpy.reshape( numpy.fromstring( image.tostring(), 'B' ), shape )

def pilImageFromArray( array ):
    assert array.dtype.char == 'B', 'Unexpected type for array: %c' % array.dtype.char
    size = tuple( array.shape[:2] )
    if len( array.shape ) == 3:
        channelCount = array.shape[2]
    elif len( array.shape ) == 2:
        channelCount = 1
    else:
        raise ValueError, 'Cannot handle arrays with shape %s' % array.shape
        
    if channelCount == 1:
        return Image.fromstring( 'L', size, array.tostring() )
    elif channelCount == 3:
        return Image.fromstring( 'RGB', size, array.tostring() )
    elif channelCount == 4:
        return Image.fromstring( 'RGBA', size, array.tostring() )
    else:
        raise ValueError, 'Cannot create image from %d channels' % channelCount

def splitPILImageToRGBAndAlpha( pilImage ):
    '''Given a RGBA PIL image returns two images as a tuple: an RGB image and a
    grayscale image containing the Alpha information. Given an RGB image return
    the image and None as a tuple.'''
    assert pilImage.mode in ['RGB', 'RGBA']
    if pilImage.mode == 'RGB':
        return pilImage, None
    else:
        array      = pilImageToArray( pilImage )
        rgbImage   = pilImageFromArray( array[:,:,:3] )
        alphaImage = pilImageFromArray( array[:,:,3] )
        return rgbImage, alphaImage

class PILImagePair:
    def __init__( self, testCase, referenceImage, candidateImage ):
        self.testCase       = testCase
        self.referenceImage = referenceImage
        self.candidateImage = candidateImage
        
        if self.referenceImage:
            self.referenceRgbImage, self.referenceAlphaImage = splitPILImageToRGBAndAlpha( self.referenceImage )
        if self.candidateImage:
            self.candidateRgbImage, self.candidateAlphaImage = splitPILImageToRGBAndAlpha( self.candidateImage )

        self.diffImage = self.negDiffImage = None
        
        if self.referenceImage and self.candidateImage:
            if self.referenceImage.mode != self.candidateImage.mode:
                console.printWarning( 'Difference image not available for test case %s: candidate and reference images have different color depths!' % testCase.name )
            elif self.referenceImage.size != self.candidateImage.size:
                console.printWarning( 'Difference image not available for test case %s: candidate and reference images have different size!' % testCase.name )
            else:
                self.diffImage, self.negDiffImage = calculateDiffImages( self )
                self.rgbDiffImage, self.alphaDiffImage       = splitPILImageToRGBAndAlpha( self.diffImage )
                self.negRgbDiffImage, self.negAlphaDiffImage = splitPILImageToRGBAndAlpha( self.negDiffImage )

def calculateDiffImages( imagePair ):
    if not imagePair.referenceImage or not imagePair.candidateImage:
        return None, None
    # Note: arrays are converted to signed integer type because otherwise the
    # subtraction will not be done properly, i.e., it will not create negative
    # values as expected.
    referenceArray = pilImageToArray( imagePair.referenceImage ).astype( 'i' )
    candidateArray = pilImageToArray( imagePair.candidateImage ).astype( 'i' )
    # Difference is converted back to unsigned bytes so that result will be
    # valid color values.
    diff           = abs( referenceArray - candidateArray ).astype( 'B' )
    assert diff.max() < 256    # Sanity check.
    diffImage = pilImageFromArray( diff )
    # 255 - diff returns an array of integeres, have to convert it back to
    # unsigned bytes.
    negDiffImage = pilImageFromArray( ( 255 - diff ).astype( 'B' ) )
    return diffImage, negDiffImage

class ImageComparison:
    def __init__( self, testCase, imagePair, comparer ):
        self.testCase  = testCase
        self.imagePair = imagePair
        self.comparer  = comparer
        if not imagePair.diffImage:
            self._rgbResult   = False
            self._alphaResult = False
        else:
            self._rgbResult   = None
            self._alphaResult = None

    def isRGBDiffAcceptable( self ):
        # Note! Have to check against None here because the actual result can be
        # 0 or False which would fail a simple "not"  also.
        if self._rgbResult == None:
            self._rgbResult = self.comparer( self.imagePair.rgbDiffImage )
        return self._rgbResult

    def isAlphaDiffAcceptable( self ):
        # Note! Have to check against None here because the actual result can be
        # 0 or False which would fail a simple "not" test also.
        if self._alphaResult == None:
            self._alphaResult = self.comparer( self.imagePair.alphaDiffImage )
        return self._alphaResult

################################################################################
# Image comparer functions
def _exactComparer( image ):
    return pilImageToArray( image ).max() == 0

def getImageComparerMap():
    comparerNames = fnmatch.filter( globals().keys(), '_*Comparer' )
    return dict( [(name[1:-8].lower(), globals()[name]) for name in comparerNames] )

def getImageComparer( comparerName ):
    return globals().get( '_' + comparerName + 'Comparer' )

