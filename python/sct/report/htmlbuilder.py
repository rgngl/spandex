#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import os

class HTMLBuilder:
    def __init__( self, path ):
        self.path = path
        if not os.path.isdir( path ):
            os.makedirs( path )
        self.htmls      = []
        self.activeHtml = ''       
        self.elements   = []
        self.imageCount = 0

    def startHtml( self ):
        if not self.activeHtml:
            self.activeHtml  = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">'
            self.activeHtml += '<body>'        
        
    def output( self, text ):
        self.startHtml()
        self.activeHtml += text

    def outputLine( self, text ):
        self.startHtml()
        self.output( text + '\n' )

    def pushElement( self, name ):
        self.elements.append( name )

    def popElement( self ):
        return self.elements.pop()

    def combineBuilders( self, builder ):
        if builder.activeHtml:
            builder.htmls.append( builder.activeHtml )
            builder.activeHtml = ''
            
        if len( builder.htmls ) >= 1:
            if len( self.htmls ) == 0:
                self.htmls.append( self.activeHtml + builder.htmls[ 0 ] )
            else:
                self.htmls.append( builder.htmls[ 0 ] )
                self.htmls.append( self.activeHtml )
                
            self.htmls += builder.htmls[ 1: ]
        else:
            self.htmls.append( self.activeHtml )

        self.activeHtml = ''
        self.elements += builder.elements
        self.imageCount += builder.imageCount
    
    def nextPage( self ):
        if self.activeHtml:
            self.htmls.append( self.activeHtml )
            self.activeHtml = None
    
    def startElement( self, element, **kwargs ):
        if not kwargs:
            self.outputLine( '<%s>' % element )
        else:
            s = ' '.join( [ '%s="%s"' % ( name, value ) for name, value in kwargs.items() ] )
            self.outputLine( '<%s %s>' % ( element, s ) )
        self.pushElement( element )

    def endElement( self ):
        element = self.popElement()
        self.outputLine( '</%s>' % element )

    def compactElement( self, element, text ):
        self.output( '<%s>%s</%s>' % ( element, text, element ) )

    def save( self ):
        for i in range( len( self.htmls ) ):
            if i == 0:
                s = ''
            else:
                s  = '%d' % i

            ss = '%d' % ( i + 1 )

            page = self.htmls[ i ]
            if self.activeHtml or i != len( self.htmls ) - 1:
                page += '\n<br><a href="index%s.html">%s</a>' % ( ss, 'Next page' )
            file( os.path.join( self.path, 'index%s.html' % s ), 'w' ).write( page )

        if self.activeHtml:
            if len( self.htmls ) == 0:
                s = ''
            else:
                s = '%d' % len( self.htmls )
            
            file( os.path.join( self.path, 'index%s.html' % s ), 'w' ).write( self.activeHtml )
            
    def addLineBreak( self ):
        self.output( '<br>' )

    def addPageBreak( self ):
        self.outputLine( '<hr>' )

    def addBookmark( self, name ):
        self.output( '<a name="%s"> </a>' % name ) 

    def addHeading( self, name, level ):
        self.compactElement( 'H%d' % level, name )
        self.output( '\n' )

    def addLink( self, link, name = '' ):
        self.output( '<a href="%s">%s</a>' % ( link, name ) )

    def addParagraph( self, text ):
        self.startElement( 'p' )
        self.outputLine( text )
        self.endElement()

    def addTable( self, tableData, headingRow = True, headingColumn = False ):
        self.startElement( 'table', border = "1" )
        firstDataRow = 0
        if headingColumn:
            firstRowItemElem = '<th>'
        else:
            firstRowItemElem = '<td>'
        if headingRow:
            self.outputLine( '<tr><th>' + '<th>'.join( tableData[ 0 ] ) )
            firstDataRow = 1
        for row in tableData[ firstDataRow: ]:
            self.outputLine( '<tr>' + firstRowItemElem + '<td>'.join( row ) )
        self.endElement()

    def addImageTable( self, imageTable ):
        self.startElement( 'table', border = "1" )
        self.outputLine( '<tr><td align=center>' + '<td align=center>'.join( [ '<img src="%s">' % imageName for imageName in [ self._addPILImage( image ) for image in imageTable.tableData[ 0 ] ] ] ) )
        self.outputLine( '<tr><td>' + '<td>'.join( imageTable.tableData[ 1 ] ) )
        self.endElement()

    def addImage( self, path, title ):
        self.output( '<img src="%s">' % path )
        if title:
            self.addParagraph( title )

    def _addPILImage( self, image ):
        id = self.imageCount
        self.imageCount += 1
        image.save( os.path.join( self.path, 'image%d.png' ) % id )        
        return 'image%d.png' % id

    def addPILImage( self, image, title = '' ):
        imageName = self.addPILImage( image )
        self.addImage( imageName, title )
