#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

#import chart, htmlbuilder, types
import htmlbuilder, types

class Reporter:
    def __init__( self, format ):
        if format == 'html':
            self.builder = htmlbuilder.HTMLBuilder()
        else:
            raise ValueError

#     def lineChart( self, results, valueAttribute, seriesAttribute, title='' ):
#         table = results.project( [valueAttribute, seriesAttribute] )
#         # Create a line chart from the results
#         print table

#         # Add the chart as an image to the report
#         self.builder.addImage

#     def barChart( self, dataTable, valueAttribute, groupAttribute, seriesAttribute=None, title='' ):
#         colors     = ['blue', 'red', 'green', 'yellow']
#         groupNames = dataTable.getUniqueAttributeValues( groupAttribute )
#         chartData  = chart.ChartData( title, valueAttribute, groupNames )

#         if seriesAttribute:
#             seriesNames = dataTable.getUniqueAttributeValues( seriesAttribute )
#             for name, color in zip( seriesNames, colors ):
#                 dt = dataTable.select( seriesAttribute, name )
#                 ds = makeDataSeries( name, dt, valueAttribute, color )
#                 chartData.addSeries( ds )
#         else:
#             ds = makeDataSeries( '', dataTable, valueAttribute, colors[0] )
#             chartData.addSeries( ds )
            
#         pilImage = chart.renderBarChart( chartData )
#         self.builder.addPILImage( pilImage )

    def makeColoredText( self, text, color ):
        htmlColorCodes = {
            'white'      : '#FFFFFF',
            'black'      : '#000000',
            'light blue' : '#0000FF',
            'red'        : '#FF0000',
            'yellow'     : '#FFFF00',
            'green'      : '#00FF00',
            'pink'       : '#FF00FF' }
        return '<font color="%s">%s</font>' % (htmlColorCodes[color], text )
        
    def paragraph( self, text, color='default' ):
        if color != 'default':
            text = self.makeColoredText( text, color )
        self.builder.addParagraph( text )

    def section( self, name ):
        self.builder.addHeading( name, 1 )

    def save( self, path ):
        self.builder.saveAs( path )

    def table( self, dataTable, columns ):
        columnNames = [getColumnName( col ) for col in columns]
        tableData   = [columnNames]
        for row in dataTable:
            tableData.append( [] )
            for column in columns:
                format = getColumnFormat( column )
                func   = getColumnFunc( column )
                if func:
                    value = func( row )
                else:
                    attribute = getColumnAttribute( column )
                    value     = row[attribute]
                tableData[-1].append( format % value )
        self.builder.addTable( tableData, headingRow=True, headingColumn=True )
        
def getColumnName( column ):
    if type( column ) == types.StringType:
        return column
    assert type( column ) == types.DictType
    return column['name']

def getColumnAttribute( column ):
    if type( column ) == types.StringType:
        return column
    assert type( column ) == types.DictType
    return column.get( 'attribute', column['name'] )

def getColumnFormat( column ):
    defaultFormat = '%s'
    if type( column ) == types.StringType:
        return defaultFormat
    assert type( column ) == types.DictType
    return column.get( 'format', defaultFormat )

def getColumnFunc( column ):
    if type( column ) == types.StringType:
        return None
    assert type( column ) == types.DictType
    return column.get( 'func', None )

# def makeDataSeries( name, dataTable, valueAttribute, color ):
#     values = []
#     for row in dataTable:
#         values.append( row[valueAttribute] )
#     return chart.DataSeries( name, values, color )
    
