#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import copy, re, string, types
import db

class ParsingError( Exception ):
    pass

class MultiValueAttribute:
    def __init__( self, name, values ):
        self.name   = name
        assert type( values ) == types.ListType
        self.values = []
        for value in values:
            self.AddValue( value )

    def AddValue( self, value ):
        assert value not in self.values
        self.values.append( str( value ) )

class Permutator:
    def __init__( self, limits ):
        #assert min( limits ) > 0
        self.limits = limits[:]
        self.state  = [ 0 ] * len( limits )

    def inc( self ):
        return self.rinc( self.state, self.limits, len( self.state ) - 1 );

    def rinc( self, state, limits, counter ):
        if counter < 0:
            return 0
        if state[ counter ] + 1 >= limits[ counter ]:
            state[ counter ] = 0
            return self.rinc( state, limits, counter - 1 )
        else:
            state[ counter ] += 1
            return 1

    def getState( self ):
        return self.state[:]

class Section( db.DataRow ):
    # Simple class to represent parsed sections. Behaves as a dictionary that
    # stores attribute values with attribute names as keys.
    def __init__( self, name, type, id=1 ):
        self['Name']        = name
        self['SectionType'] = type
        self.ID             = id

def ParseQuotedString( string ):
    '''Returns a copy of the string with possible surrounding double quotation
    marks and white-space removed.'''

    string = string.strip()
    if not string:
        return ''
    if string[0] == '"':
        assert string[-1] == '"', 'Quotation mark expected at the end of string ' + string
        return string[1:-1].strip()
    assert string[-1] != '"'
    return string

def ParseCSV( text ):
    # Parse a list of comma separated values. Values can be quoted with double
    # quotation marks.
    text = text.strip()
    if not text:
        return ['']
    elif text.find( '+' ) != -1:
        # Haxor: do nothing with action lists
        return [text]

    values = []
    while text:
        if text[0] == '"':
            # Leading value is quoted.
            assert text[1:].find( '"' ) != -1, 'Missing second quote'
            value, text = text[1:].split( '"', 1 )
            # There is a comma (and possibly also some white space) at the start
            # of the text if there are more values. These have to be removed.
            text = text.lstrip( ' ,' )
        elif text[0] == '{':
            # Leading value is an array.
            assert text[1:].find( '}' ) != -1, 'Missing closing brace'
            value, text = text[1:].split( '}', 1 )
            # Restore the braces around the value.
            value = '{' + value + '}'
            text  = text.lstrip( ' ,' )
        elif text.find( ',' ) != -1:
            # Leading value is not quoted but there are several values
            # left. Split the first value.
            value, text = text.split( ',', 1 )
        else:
            # Single value.
            value, text = text, ''
        values.append( value )
        text = text.strip()
    return map( string.strip, values )

def IsTuple( string ):
    return string[0] == '(' and string[-1] == ')'

def ParseTuple( expr ):
    expr   = expr.strip()
    assert expr[0] == '(' and expr[-1] == ')'
    # Remove the parentheses.
    expr   = expr[1:-1]
    
    return ParseCSV( expr )
    
def ParseValueTuples( string ):
    string = string.strip()
    # Slight trick: by removing the first opening and last closing parenthesis a
    # single regexp split can produce each tuple as a separate comma separated
    # value list.
    string = string[1:-1]
    # Split from the close-paren-comma-open-paren.
    csvLists = re.split( '\)\s*,\s*\(', string )
    return map( ParseCSV, csvLists )

class AttributeGroups:
    def __init__( self ):
        self._groups               = [[]]
        self._tupleAttributeNames  = []
        self._allowAttributeChanges = False

    def GetGroups( self ):
        return self._groups

    def GetGroupCount( self ):
        return len( self._groups )

    def AllowAttributeChanges( self ):
        self._allowAttributeChanges = True
        
    def AddAttribute( self, name, values ):
        for group in self._groups:
            if not self._allowAttributeChanges:
                for attr in group:
                    if attr.name == name:
                        raise ParsingError( 'Tried to set attribute %s multiple times' % name )
            group.append( MultiValueAttribute( name, values ) )

    def AddAttributeTuple( self, names, valueTuples ):
        if self._allowAttributeChanges and names == self._tupleAttributeNames:
            # We are modifying exactly the same set of tuple attributes that has
            # been already set. This can be done by keeping only the 1st group
            # and building a new set of groups as usual.
            self._groups = [self._groups[0]]
        elif self.GetGroupCount() > 1:
            raise ParsingError( 'Multiple attribute tuples are not supported' )
        # Duplicate the first attribute set to get enough sets.
        for i in range( len( valueTuples ) - 1 ):
            self._groups.append( copy.copy( self._groups[0] ) )
        for index, name in enumerate( names ):
            # Values are converted to strings because attributes are
            # handled as strings within the rest of the system.
            valueList = [str( t[index] ) for t in valueTuples]
            for value, group in zip( valueList, self._groups ):
                group.append( MultiValueAttribute( name, [value] ) )
        self._tupleAttributeNames = names

variantRegexp   = re.compile( """^\s*<(.*?):variant>\s*$""" )
attributeRegexp = re.compile( """^\s*(.*?)\s*:\s*(.*?)\s*$""" )

def IsVariantLine( line ):
    return re.match( variantRegexp, line ) != None

def ParseVariantLine( line ):
    return re.match( variantRegexp, line ).group( 1 )

def IsAttributeLine( line ):
    return re.match( attributeRegexp, line ) != None

def ParseAttributeLine( line ):
    m     = re.match( attributeRegexp, line )
    name  = m.group( 1 )
    # Remove the quotes that are added by Spandex
    value = m.group( 2 )
    return name, value

def ParseSectionBody( body, variant ):
    attributeGroups = AttributeGroups()
    skipLines       = False
    for line in body.split( '\n' ):
        # Skip empty lines.
        if not line.strip():
            continue
        if IsVariantLine( line ):
            variantName = ParseVariantLine( line )
            if variantName == variant:
                # Requested variant found, previously set attributes can now be
                # changed.
                attributeGroups.AllowAttributeChanges()
                skipLines = False
            else:
                # Variant name does not match the requested variant, skip it.
                skipLines = True
        elif skipLines:
            continue
        elif IsAttributeLine( line ):
            # Attribute line
            attribName, attribValue = ParseAttributeLine( line )
            if IsTuple( attribName ):
                names       = ParseTuple( attribName )
                valueTuples = ParseValueTuples( attribValue )
                attributeGroups.AddAttributeTuple( names, valueTuples )
            else:
                valueList = ParseCSV( attribValue )
                attributeGroups.AddAttribute( attribName, valueList )
        else:
            raise ParsingError( 'Cannot parse line "%s"' % line )
    return attributeGroups

def ParseOutputSectionBody( body ):
    attributes = {}
    for line in body.split( '\n' ):
        # Skip empty lines.
        if not line.strip():
            continue
        name, value = ParseAttributeLine( line )
        # Remove any quotation marks that Spandex might have added.
        value       = value.replace( '"', '' )
        attributes[name] = value
    return attributes

def ParseSectionHeader( header ):
    # Split section header from : to get name and type
    try:
        sectionName, sectionType = header.split( ':', 1 )
    except:
        print 'Exception: ' + str( header )
    else:
        sectionName = ParseQuotedString( sectionName )
        return sectionName, sectionType.strip()

def ExtractSections( s ):
    # First, we strip out all comments
    cexp = re.compile( """\s*#.*?$""", re.MULTILINE | re.DOTALL )
    s    = re.sub( cexp, '', s )

    # Second, we find sections
    exp = re.compile( """^\[([^\]]*?)\]      # Find section name, group 1
                         ([^\[]*)               # Find section contents, group 2"""
                      , re.MULTILINE | re.DOTALL | re.VERBOSE)
    return re.findall( exp, s )

def ParseInput( data, variant=None ):
    sectionTable = db.DataTable()

    for header, body in ExtractSections( data ):
        sectionName, sectionType = ParseSectionHeader( header )
        attributeGroups          = ParseSectionBody( body, variant )
        sectionId                = 1
        for group in attributeGroups.GetGroups():
            if len( group ) == 0:
                # A section with no attributes
                sect = Section( sectionName, sectionType, sectionId )
                sectionTable.addRow( sect )
                continue
            # Get counter limits for permutator
            limits = [len( attribute.values ) for attribute in group]
            # Create permutator
            p = Permutator( limits )
            # Create section from each attribute permutation
            while 1:
                sect = Section( sectionName, sectionType, sectionId )
                sectionId += 1

                # Get permutator state and use it as indices to get
                # attribute values for current permutation index

                state = p.getState()
                for attribute, valueIndex in zip( group, state ):
                    sect[attribute.name] = attribute.values[valueIndex]

                sectionTable.addRow( sect )

                # Generate new permutation, quit if permutations done

                if p.inc() == 0:
                    break;

    return sectionTable

def ParseOutput( data ):
    sectionTable = db.DataTable()

    for header, body in ExtractSections( data ):
        name, type = ParseSectionHeader( header )
        section    = Section( name, type ) 
        section.update( ParseOutputSectionBody( body ) )
        sectionTable.addRow( section )

    return sectionTable

    

