#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import os, optparse, sys
from sct import action, formatter

filenameTemplate = 'sct_%smodule_parser.%s'
WARNING          = '/* NOTE: This file has been generated, you should not edit this file directly */'
LICENSE          = '''/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */'''

class ActionParserCFormatter( formatter.CFormatter ):
    def __init__( self, stream ):
        formatter.CFormatter.__init__( self, stream )
        self.localVariables = []
    
    def GenerateError( self, errorMessage ):
        self.StartBlock()
        self.Output( 'SCT_LOG_ERROR( "%s" );' % errorMessage )
        for variable in self.localVariables:
            variable.GenerateDestruction( self )
        self.Output( 'return SCT_FALSE;' )
        self.EndBlock()

    def GenerateExternCStart( self ):
        self.Output( '#if defined( __cplusplus )' )
        self.Output( 'extern "C"' )
        self.Output( '{' )
        self.Output( '#endif  /* defined( __cplusplus ) */' )

    def GenerateExternCEnd( self ):
        self.Output( '#if defined( __cplusplus )' )
        self.Output( '}' )
        self.Output( '#endif  /* defined( __cplusplus ) */' )

    def GenerateActionParser( self, config ):
        self.AddComment( 'Generated parser for %s action in %s module' % (config.type, config.moduleName) )
        self.Output( GetParserFunctionDeclaration( config ) )
        self.StartBlock()

        # Gather unique local variables.
        localVariables = {}
        for attrConfig in config.attributeConfigs:
            for variable in attrConfig.localVariables:
                if variable.name not in localVariables:
                    localVariables[variable.name] = variable
                else:
                    existingVariable = localVariables[variable.name]
                    assert variable == existingVariable, 'Local variable %s has multiple different definitions.' % variable.name

        self.localVariables = localVariables.values()

        # Write the local variable definitions.
        for variable in localVariables.values():
            self.Output( variable.definition + ';' )

        # Put a newline after the local variable definitions (if any).
        if localVariables:
            self.Output()

        self.Output( 'SCT_ASSERT( data != NULL );' )
        self.Output( 'SCT_ASSERT( attributes != NULL );' )
        self.Output( 'SCT_USE_VARIABLE( data );' )
        self.Output( 'SCT_USE_VARIABLE( attributes );' )
        self.Output()
        self.Output( 'if( sctCountListElements( attributes->list ) != %d )' % len( config.attributeConfigs ) )
        self.StartBlock()
        self.Output( 'SCT_LOG_ERROR( "Invalid number of attributes for %s action." );' %
                 config.type );
        self.Output( 'return SCT_FALSE;' )
        self.EndBlock()
        self.Output()

        # Generate the local variable initializations.
        for variable in localVariables.values():
            variable.GenerateInitialization( self )

        # Generate attribute parsers.
        for cfg in config.attributeConfigs:
            self.AddComment( '%s attribute parsing.' % cfg.name )
            self.GenerateAttributeParser( cfg )
            self.Output()

        # Generate code for additional attribute validators (if any).
        if config.validatorConfigs:
            self.AddComment( 'Additional attribute value validation.' )
            for cfg in config.validatorConfigs:
                self.GenerateValidator( cfg )
            self.Output()

        # Generate the local variable destruction code.
        for variable in localVariables.values():
            variable.GenerateDestruction( self )

        self.Output( 'return SCT_TRUE;' )
        self.EndBlock()
        self.Output()

    def GenerateFunctionDeclaration( self, config ):
        self.AddComment( 'Parser function declaration for %s action' % config.type )
        self.GenerateExternCStart()
        self.Output( GetParserFunctionDeclaration( config ) + ';' )
        self.GenerateExternCEnd()

    def GenerateTypes( self, config ):
        self.AddComment( 'Data type definitions for %s action' % config.type )
        for attribConfig in config.attributeConfigs:
            attribConfig.GenerateCType( self )
        self.GenerateStruct( config )

    def GenerateStruct( self, config ):
        self.Output( 'typedef struct' )
        self.StartBlock()
        if not config.attributeConfigs:
            # No action attributes, insert a dummy field to satisfy the compiler.
            self.Output( 'int dummy; /* This is a dummy placeholder */' )
        else:
            for attrConfig in config.attributeConfigs:
                self.Output( attrConfig.GetStructItem() )
        self.EndBlock( ' %s;' % GetStructName( config ) )

    def GenerateIncludeGuardStart( self, headerName ):
        define = MakeIncludeGuardDefine( headerName )
        self.Output( '#if !defined( ' + define + ')' )
        self.Output( '#define ' + define )

    def GenerateIncludeGuardEnd( self, headerName ):
        define = MakeIncludeGuardDefine( headerName )
        self.Output( '#endif  /* !defined( %s ) */' % define )

    def GenerateModuleConfigString( self, moduleName, configData ):
        hexChars   = ['0x%x' % ord( char ) for char in configData]
        # Add a NULL character to the end to create a valid C string.
        hexChars.append( '0x0' )
        arrayInitializer =  '{' + ','.join( hexChars ) + '};'
        self.Output( 'const char _%s_parser_config[] = %s' % (moduleName, arrayInitializer) )

    def GenerateActionTemplate( self, config ):
        self.StartBlock( ' "%s",' % config.type )
        for prefix in ['Create', 'Destroy']:
            self.Output( 'scti%s%s%sActionContext,' % (prefix, config.moduleName, config.type) )
        for suffix in ['Init', 'Execute', 'Terminate', 'Destroy', 'GetWorkload']:
            if suffix in config.methods:
                self.Output( 'scti%s%sAction%s,' % (config.moduleName, config.type, suffix) )
            else:
                self.Output( 'NULL,' )
        self.EndBlock( ',' )

    def GenerateActionTemplateArray( self, actionConfigs ):
        moduleName = actionConfigs[0].moduleName
        self.Output( 'static const SCTActionTemplate %sActionTemplates[] =' % moduleName )
        self.StartBlock()
        for config in actionConfigs:
            self.GenerateActionTemplate( config )
        self.EndBlock( ';' )

    def GenerateAttributeParser( self, config ):
        config.GenerateAttributeParser( self )

    def GenerateValidator( self, config ):
        config.GenerateC( self )

def GetStructName( config ):
    return '%s%sActionData' % (config.moduleName, config.type)

def GetParserFunctionDeclaration( config ):
    return 'SCTBoolean sctiParse%s%sActionAttributes( %s *data, SCTAttributeList *attributes )' % (config.moduleName, config.type, GetStructName( config ))

def MakeIncludeGuardDefine( headerName ):
    return ('__%s__' % (headerName.replace( '.', '_' ))).upper()

def generateHeader( f ):
    f.Output( LICENSE )
    f.Output()
    f.Output( WARNING )
    f.Output()

def parserGenerator( configPath, sourcePath, headerPath, actionHeaderPath, defines=[] ):
    targetPath           = os.path.dirname( configPath )
    moduleName           = os.path.split( targetPath )[1]
    sourceName           = os.path.split( sourcePath )[1]
    headerName           = os.path.split( headerPath )[1]
    actionHeaderName     = os.path.split( actionHeaderPath )[1]

    try:
        parserConfig = action.ProcessModuleConfig( file( configPath ).read(), defines )
    except:
        msg = '! Action config processing failed while parsing %s !' % configPath
        print len( msg ) * '!'
        print msg
        print len( msg ) * '!'
        raise

    # Parser source generation.
    f = ActionParserCFormatter( file( sourcePath, 'w' ) )
    generateHeader( f )
    f.Output( '#include "%s"' % headerName )
    f.Output( '#include "string.h"' )
    f.Output( '#include "sct_sicommon.h"' )
    f.Output( '#include "sct_utils.h"' )
    for include in parserConfig.includes:
        f.Output( '#include "%s"' % include )
    f.Output()
    f.GenerateModuleConfigString( moduleName, file( configPath ).read() )
    f.Output()
    for config in parserConfig.actionConfigs:
        f.GenerateActionParser( config )

    # Parser header generation.
    f = ActionParserCFormatter( file( headerPath, 'w' ) )
    generateHeader( f )
    f.GenerateIncludeGuardStart( headerName )
    f.Output( '#include "sct_types.h"' )
    for include in parserConfig.includes:
        f.Output( '#include "%s"' % include )
    f.Output( 'extern const char _%s_parser_config[];' % moduleName )
    f.Output()
    for config in parserConfig.actionConfigs:
        f.GenerateTypes( config )
        f.GenerateFunctionDeclaration( config )
        f.Output()
    f.GenerateIncludeGuardEnd( headerName )

    # Action template header generation.
    f = ActionParserCFormatter( file( actionHeaderPath, 'w' ) )
    generateHeader( f )
    f.GenerateIncludeGuardStart( actionHeaderName )
    f.Output()
    f.Output( '#include "sct_action.h"' )
    for config in parserConfig.actionConfigs:
        headerName = 'sct_%s%saction.h' % (moduleName, config.type.lower())
        f.Output( '#include "%s"' % headerName )
    f.Output()
    f.GenerateActionTemplateArray( parserConfig.actionConfigs )
    f.GenerateIncludeGuardEnd( actionHeaderName )    

################################################################################
# SCons builder
def moduleGenerator( target, source, env ):
    # Find out the path to the config.py
    configPath       = str( source[0] )
    sourcePath       = str( target[0] )
    headerPath       = str( target[1] )
    actionHeaderPath = str( target[2] )

    parserGenerator( configPath, sourcePath, headerPath, actionHeaderPath, env['DEFINES'] )

    # Return None to signify success
    return None

################################################################################
# Command line interface
if __name__ == '__main__':
    usage  = 'usage: %prog <config> [<define> [<define> ...]]'
    parser = optparse.OptionParser( usage )

    parser.add_option( '--cpp', action='store_true', dest='cpp',
                       default=False, help='create the source file with .cpp extension instead of .c' )
    options, args = parser.parse_args()

    # Check arguments.
    if len( args ) < 1:
        parser.error( 'No config file given.' )

    # Parse defines.
    defines = {}
    for arg in args[1:]:
        name, value = arg.split( '=', 1 )
        defines[name] = value
        
    configPath = args[0]
    targetPath = os.path.dirname( configPath )
    moduleName = os.path.split( targetPath )[1]

    if options.cpp:
        sourceName       = 'sct_%smodule_parser.cpp' % moduleName
    else:
        sourceName       = 'sct_%smodule_parser.c' % moduleName
    headerName           = 'sct_%smodule_parser.h' % moduleName
    actionHeaderName     = 'sct_%smodule_actions.h' % moduleName

    sourcePath           = os.path.join( targetPath, sourceName )
    headerPath           = os.path.join( targetPath, headerName )
    actionHeaderPath     = os.path.join( targetPath, actionHeaderName )
    
    parserGenerator( configPath, sourcePath, headerPath, actionHeaderPath, defines )
