#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, os, shutil

# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] <candidate result> <reference file>"
    parser = optparse.OptionParser( usage )
    
    ( options, args ) = parser.parse_args()

    # Validate command line parameters.
    if len( args ) != 2:
        parser.error( 'Incorrect number of arguments' )
        sys.exit( -1 )

    resultFile = args[ 0 ]
    refFile    = args[ 1 ]
        
    f = file( resultFile, 'r' )
    resultLines = f.readlines()
    f.close()

    f = file( refFile, 'r' )
    refLines = f.readlines()
    f.close()

    # First, read in the reference data
    refs = {}
        
    for line in refLines:
        benchmark, target, min, max, type = line.split( '|' )[ 1 : -1 ]
        refs[ benchmark.strip() ] = ( float( target ), float( min ), float( max ), type.strip(), )

    status = 0
        
    # Then start going through the result file
    for line in resultLines:
        s = line.split( '|' )
        
        benchmark = s[ 1 ].strip()

        if not benchmark:
            continue

        if s[ 2 ].strip() == '-':
            result = 0
            type   = 'NOT SUPPORTED'
        else:
            result, type = s[ 2 ].split()
            result = float( result )
        
        # Check benchmark result against the reference

        if not refs.has_key( benchmark ):
            print 'FAIL : "%s" missing performance reference data.' % benchmark
            status = -1
        else:
            ( rtarget, min, max, rtype, ) = refs[ benchmark ]

            if rtype != type:
                print 'FAIL : "%s" unexpected result type.' % benchmark
                status = -1
            elif result < min:
                print '"FAIL: %s" result %.1f %s below min limit %.1f %s' % ( benchmark, result, rtype, min, rtype, )
                status = -1
            elif ( max > 0 and result > max ):
                print 'FAIL : "%s" result %.1f %s above max limit %.1f %s' % ( benchmark, result, rtype, max, rtype, )
                status = -1

    if status == 0:
        print 'Candidate performance OK.'

    sys.exit( status )
