#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, re, sha

#------------------------------------------------------------
# SpandexBenchmark
#------------------------------------------------------------
class SpandexBenchmark:

    def __init__( self, name, initActions, benchmarkActions, repeats ):
        self.name              = name
        self.initActions       = initActions
        self.benchmarkActions  = benchmarkActions
        self.repeats           = repeats

    def replaceAction( self, oldAction, newAction ):
        if oldAction in self.initActions:
            self.initActions[ self.initActions.index( oldAction ) ] = newAction
            #print '#Replace', oldAction, '->', newAction, 'in init actions'
        if oldAction in self.benchmarkActions:            
            self.benchmarkActions[ self.benchmarkActions.index( oldAction ) ] = newAction
            #print '#Replace', oldAction, '->', newAction, 'in benchmark actions'

    def renameAction( self, oldName, newName ):
        for i, e in enumerate( self.initActions ):
            if e == oldName:
                self.initActions[ i ] = newName
        for i, e in enumerate( self.benchmarkActions ):
            if e == oldName:
                self.benchmarkActions[ i ] = newName
            
        
#------------------------------------------------------------
# SpandexAction
#------------------------------------------------------------
class SpandexAction:

    def __init__( self, name, type, module, attributes ):
        self.name       = name
        self.type       = type
        self.module     = module
        self.attributes = attributes

        # strip away white spaces
        a = self.attributes.replace( ' ', '' )
        s = sha.new( type )
        s.update( a )        
        self.digest = s.hexdigest()

#------------------------------------------------------------
# SpandexInput
#------------------------------------------------------------
class SpandexInput:

    def __init__( self ):
        self.benchmarks = []
        self.actions    = []

    def addBenchmark( self, benchmark ):
        self.benchmarks.append( benchmark )

    def addAction( self, action ):
        self.actions.append( action )

    def getAction( self, name ):
        for a in self.actions:
            if a.name == name:
                return a
        return None

    def simplify( self ):
        
        # First, we replace same actions with one instance
        actions = self.actions[ : ]
        hashes = []
        for a in actions:
            h = a.digest
            if h in hashes:
                self._replaceBMA( a, actions[ hashes.index( h ) ] )
                hashes.append( None )
            else:
                hashes.append( h )

        # Secondly, convert action names into human readable format
        index = 1
        for a in self.actions:
            oldname = a.name
            newname = a.type + '_' + str ( index )
            a.name = newname
            for b in self.benchmarks:
                b.renameAction( oldname, newname )
            index += 1

    def _replaceBMA( self, src, dst ):
        for b in self.benchmarks:
            b.replaceAction( src.name, dst.name )


#------------------------------------------------------------
# GetAttribute
#------------------------------------------------------------
def GetAttribute( attribute, attributes ):
    for a in attributes:
        if not a.strip():
            continue
        kv = a.split( ':' )
        if len( kv ) != 2:
            raise Exception( 'Invalid attributes: ' + str( attributes ) )
        if kv[ 0 ].strip() == attribute:
            return kv[ 1 ].strip()
        
    return None
    
#------------------------------------------------------------
# AddInput
#------------------------------------------------------------
def AddInput( input, inputName, content ):

    lines     = content.splitlines()
    lineCount = len( lines )
    curLine   = 0
    index     = 0
    while 1:
        if curLine >= lineCount:
            break;
        
        if not lines[ curLine ].startswith( '[' ):
            curLine += 1
            continue
        
        name, type = lines[ curLine ].split( ':' )
        name = name[ 1: ]
        type = type[ : -1 ]

        curLine += 1        
        attributes = []

        while ( curLine < lineCount ) and not lines[ curLine ].startswith( '[' ):
            attributes.append( lines[ curLine ] )
            curLine += 1            

        #print '->', name, '<-, ->', type, '<- ->', attributes
            
        if type == 'BENCHMARK':
            repeats = None
            if attributes[ 0 ].split( ':' )[ 0 ].strip() == 'Repeats':
                repeats = attributes[ 0 ].split( ':' )[ 1 ].strip()
                ( initn, initv )   = attributes[ 1 ].split( ':' )
                ( benchn, benchv ) = attributes[ 2 ].split( ':' )
            else:
                ( initn, initv )   = attributes[ 0 ].split( ':' )
                ( benchn, benchv ) = attributes[ 1 ].split( ':' )        
                
            initn  = initn.strip()
            initv  = initv.strip()
            benchn = benchn.strip()
            benchv = benchv.strip()
            
            initv  = initv.split( '+' )
            if len( initv ) != 1 or initv[ 0 ] != 'None':
                for i in range( len( initv ) ):
                    initv[ i ] = str( abs( hash( initv[ i ] + inputName ) ) )
                
            benchv = benchv.split( '+' )
            if len( benchv ) != 1 or benchv[ 0 ] != 'None':
                for i in range( len( benchv ) ):
                    benchv[ i ] = str( abs( hash( benchv[ i ] + inputName ) ) )

            input.addBenchmark( SpandexBenchmark( name, initv, benchv, repeats ) )
        else:
            s      = type.split( '@' )
            type   = s[ 0 ]
            module = s[ 1 ]

            name = str( abs( hash( name + inputName ) ) )            
            attributes = '\n'.join( attributes )
            input.addAction( SpandexAction( name, type, module, attributes ) )

# ------------------------------------------------------------
# Output
# ------------------------------------------------------------
def Output( input ):

    s = ''
    actions = []
    for b in input.benchmarks:
        for a in b.initActions:
            if a == 'None':
                continue
            if not a in actions:
                action = input.getAction( a )
                s += '[%s:%s@%s]\n'              % ( action.name, action.type, action.module, )
                s += action.attributes
                actions.append( a )
        for a in b.benchmarkActions:
            if a == 'None':
                continue
            if not a in actions:
                action = input.getAction( a )
                s += '[%s:%s@%s]\n'              % ( action.name, action.type, action.module, )
                s += action.attributes
                actions.append( a )

        s += '[%s:BENCHMARK]\n'                 % b.name
        if b.repeats:
                s += 'Repeats : %s\n'           % b.repeats
        s += 'InitActions : %s\n'               % ( '+'.join( b.initActions ) )
        s += 'BenchmarkActions : %s\n'          % ( '+'.join( b.benchmarkActions ) )        
        s += '\n'
           
    return s


# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':

    usage = "usage: %prog [options] <input files>"
    parser = optparse.OptionParser( usage )
    parser.add_option( "-v", "--verbose",
                       action = 'store_true', dest = 'verbose', default = False,
                       help='Verbose output')

    ( options, args ) = parser.parse_args()

    if len( args ) < 1:
        parser.error( 'incorrect number of arguments' )
        sys.exit( -1 )

    # Open input and output files
    try:
        inputFiles = []
        for f in args:
            inputFiles.append( file( f, 'r' ) )
    except Exception, e:
        parser.error( 'Invalid input file: ' +str( e ) );
        sys.exit( -1 )

    input = SpandexInput()
    for f in inputFiles:
        AddInput( input, f.name, f.read() )

    input.simplify()

    if options.verbose == True:
        print '#Actions: ' + str( len( input.actions ) )
        print '#Benchmarks: ' + str( len( input.benchmarks ) )
          
    s = Output( input )

    if len( inputFiles ) == 1:
        inputFiles[ 0 ].seek( 0 )
        for l in inputFiles[ 0 ]:
            if l[ 0 ] == '#':
                print l
            else:
                break
    print s

    for f in inputFiles:
        f.close()

    sys.exit( 0 )

