#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, os
import Image, numpy

IMAGE_FOLDERS = [ ( 'vgout',    'OpenVG visual test', ),
                  ( 'gles1out', 'OpenGLES1 visual test', ),
                  ( 'gles2out', 'OpenGLES2 visual test', ) ]

# ------------------------------------------------------------
def getShape( i ):
    if i.mode == 'L':
        return i.size
    elif i.mode == 'RGB':
        return i.size + ( 3, )
    elif i.mode == 'RGBA':
        return i.size + ( 4, )
    else:
        raise NotImplementedError( 'Image mode %s' % i.mode )

# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] <candidate release> <reference release>"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--full',
                       action  = 'store_true',
                       dest    = 'full',
                       default = False,
                       help    = 'Show status of every image (default only failed)' )
    
    ( options, args ) = parser.parse_args()

    # Validate command line parameters.
    if len( args ) != 2:
        parser.error( 'Incorrect number of arguments' )
        sys.exit( -1 )

    status = 0
        
    for folder in IMAGE_FOLDERS:       
        candidateFolder = os.path.join( args[ 0 ], folder[ 0 ] )
        referenceFolder = os.path.join( args[ 1 ], folder[ 0 ] )
        
        candidateFiles = os.listdir( candidateFolder )
        referenceFiles = os.listdir( referenceFolder )

        if len( referenceFiles ) != len( candidateFiles ):
            print 'FAIL: image count mismatch in "%s".' % folder[ 1 ]
            status = -1
            dif = set( candidateFiles ) - set( referenceFiles )
            for d in dif:
                print 'FAIL: missing file "%s".' % d
    
        for fn in referenceFiles:
            try:
                candidate = Image.open( os.path.join( candidateFolder, fn ) )
                reference = Image.open( os.path.join( referenceFolder, fn ) )
            except:
                print 'FAIL: "%s".' % fn
                status = -1
                continue
            
            if candidate.size != reference.size or candidate.getbands() != reference.getbands():
                print 'FAIL: "%s".' % fn
                status = -1
                continue

            candidateArray = numpy.reshape( numpy.fromstring( candidate.tostring(), 'B' ), getShape( candidate ) ).astype( 'i' )
            referenceArray = numpy.reshape( numpy.fromstring( reference.tostring(), 'B' ), getShape( reference ) ).astype( 'i' )
            diff = abs( referenceArray - candidateArray ).astype( 'B' )
        
            if diff.max() == 0:
                if options.full:
                    print 'PASS: "%s".' % fn
            else:
                print 'FAIL: "%s".' % fn
                status = -1

    if status == 0:
        print 'Candidate images OK.'

    sys.exit( status )
