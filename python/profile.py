#!python
# -*- coding:utf-8 -*-
#
# Spandex benchmark and test framework.
#
# Copyright (c) 2012 Kari J. Kangas.
#
# Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import fileinput
import optparse

# -------------------------------------------------------------------
PROFILE_TAG = 'SPANDEX_PROFILE'

# -------------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] [input file]"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--exclude',
                       default = None,
                       dest    = 'exclude_filter',
                       help    = 'Exclude messages with given ids, separated by comma [default: %default].' )
    parser.add_option( '--include',
                       default = None,
                       dest    = 'include_filter',
                       help    = 'Include messages with given ids, separated by comma [default: %default].' )
    parser.add_option( '--range',
                       default = None,
                       dest    = 'index_range',
                       help    = 'Include messages within given index range, inclusive, separated by colon [default: %default].' )
    parser.add_option( '--highlight',
                       default = None,
                       dest    = 'highlight_ms',
                       help    = 'Highlight time delta threshold in milliseconds [default: %default].' )
    parser.add_option( '--start',
                       default = 'START',
                       dest    = 'start',
                       help    = 'Start tag for frames [default: %default].' )
    parser.add_option( '--separate',
                       action  = 'store_true',
                       dest    = 'separate_frames',
                       default = False,
                       help    = 'Separate frames with empty line.' )
    parser.add_option( '--noframes',
                       action  = 'store_true',
                       dest    = 'noframes',
                       default = False,
                       help    = 'Do not calculate time delta over frames [default: %default].' )
    parser.add_option( '--showfiltered',
                       action  = 'store_true',
                       dest    = 'show_filtered',
                       default = False,
                       help    = 'Show filtered lines as threads [default: %default].' )
    parser.add_option( '--truncate',
                       default = '500',
                       dest    = 'truncate_length',
                       help    = 'Truncate length for lines [default: %default].' )
    parser.add_option( '--threads',
                       action  = 'store_true',
                       dest    = 'threads',
                       default = False,
                       help    = 'Rename threads.' )
    parser.add_option( '--event',
                       default = 'SPANDEX:',
                       dest    = 'eventTag',
                       help    = 'Tag for special events [default: %default].' )
 
    ( options, args ) = parser.parse_args()
 
    first_start_ns      = 0
    latest_start_ns     = 0
    current_start_ns    = 0
    previous_ns         = 0
    index               = 1
    start_index         = 0 
    new_start           = False
    previous_message_id = None

    exclude_filter = []
    if options.exclude_filter:
        exclude_filter = options.exclude_filter.split( ',' )

    include_filter = []
    if options.include_filter:
        include_filter = options.include_filter.split( ',' )

    index_range = []
    if options.index_range:
        index_range = map( int, options.index_range.split( ':' ) )

    highlight_ns = None
    if options.highlight_ms:
        highlight_ns = float( options.highlight_ms ) * 1000 * 1000

    truncate_length = int( options.truncate_length ) 

    tags = options.start.split( '+' )

    if len( tags ) == 1:
        start_message_id = None
        start_tag        = tags[ 0 ]
    else:
        start_message_id = tags[ 0 ] 
        start_tag        = tags[ 1 ]

    threads  = {}
    for line in fileinput.input( args ):
        if options.eventTag and line.find( options.eventTag ) >= 0:
            print '\n', line.strip(), '\n' 
        elif line.find( PROFILE_TAG ) >= 0:
            payload         = line.split( ':', 1 )[ 1 ].split()
            cpu_id          = payload[ 1 ]
            process_id      = payload[ 2 ]
            thread_id       = payload[ 3 ]
            message_id      = process_id + ':' + thread_id
            message_ns      = int( payload[ 4 ] ) * 1000 * 1000 * 1000 + int( payload[ 5 ] )
            message_type    = payload[ 6 ]
            message_tag     = ' '.join( payload[ 7 : ] )

            if options.threads:
                if not threads.has_key( message_id ):
                    threads[ message_id ] = 't%02d' % len( threads.keys() )
                message_id = threads[ message_id ]

            if message_type == start_tag or message_tag.find( start_tag ) >= 0:
                if start_message_id and message_id != start_message_id:
                    continue
                if not options.noframes or first_start_ns == 0:
                    latest_start_ns = message_ns
                    if first_start_ns == 0:
                        first_start_ns = message_ns
                    if not options.noframes:
                        new_start = True
                if options.separate_frames:
                    print ''
                start_index += 1
            else:
                current_start_ns = latest_start_ns

            if not current_start_ns:
                # Skip stray messages before the initial START
                continue

            if previous_ns == 0:
                previous_ns = message_ns
            
            if include_filter and message_id not in include_filter:
                # Include messages
                if options.show_filtered:
                    print ' ' * 15 + message_id
                index += 1
                continue
            elif exclude_filter and message_id in exclude_filter:
                # Exclude messages     
                if options.show_filtered:
                    print ' ' * 14 + message_id
                index += 1
                continue

            tag = ''
            if new_start:
                tag += '>'
            
            if message_id != previous_message_id:
                tag += '.'
            
            gap = False;
            if highlight_ns and ( message_ns - previous_ns ) > highlight_ns:
                gap = True
                tag += 'G' 

            tag = tag.ljust( 3 )

            if not index_range or ( index >= index_range[ 0 ] and index <= index_range[ 1 ] ):
                s = "%s %05d %03d  %s  %s %s %s %s   %s %s" % ( tag, 
                                                                index,
                                                                start_index, 
                                                                message_id,  
                                                                cpu_id,                                                          
                                                                str( '%.3f' % ( ( message_ns - first_start_ns ) / ( 1000.0 * 1000.0 ) ) ).rjust( 8 ), 
                                                                str( '%.3f' % ( ( message_ns - current_start_ns ) / ( 1000.0 * 1000.0 ) ) ).rjust( 8 ), 
                                                                str( '+%.3f' % ( ( message_ns - previous_ns ) / ( 1000.0 * 1000.0 ) ) ).rjust( 8 ), 
                                                                message_type.ljust( 22 ), 
                                                                message_tag, )
                print s[ : truncate_length ] + ( s[ truncate_length : ] and ' ..' ), 
                
                if gap:
                    print '*GAP*'
                else:
                    print ''
                 
                new_start = False
                previous_message_id = message_id

            # Sometimes messages arrive out of order so ensure time delta does not go back and forth.
            if previous_ns < message_ns: 
                previous_ns  = message_ns
            index += 1

