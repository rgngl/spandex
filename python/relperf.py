#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, os, shutil

RESULTS = [ 'vg11_perf_result',
            'gl11_perf_result',
            'gl20_perf_result',
            'vg11_gl11_perf_result',
            'vg11_gl20_perf_result',
            'vg11_ui_result',
            'gl11_ui_result',
            'gl20_ui_result',
            'gl20_ui2_result', ]

# ------------------------------------------------------------
def getReleases( directories ):
    s = []
    for d in directories:
        s.append( os.path.split( d )[ 1 ] )
        
    return s    

# ------------------------------------------------------------
def getResultDirectory( result, directory ):
    return os.path.normcase( os.path.join( directory, result ) )

# ------------------------------------------------------------
def copyResult( directory, result, release ):
    srcfile = os.path.join( directory, result + '.txt' )
    dstfile = os.path.join( directory, release + '.txt' )
    shutil.copyfile( srcfile, dstfile )
    return dstfile

# ------------------------------------------------------------
def getWikireportOptions( options ):
    o = ' '
    if options.diff:
        o += ' --diff '
        
    return o
    
# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] <release result directories>"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--diff',
                       action  = 'store_true',
                       dest    = 'diff',
                       default = False,
                       help    = 'Show the percentage difference between the last two result files' )
    
    ( options, args ) = parser.parse_args()

    # Validate command line parameters.
    if len( args ) < 1:
        parser.error( 'Incorrect number of arguments' )
        sys.exit( -1 )

    releases           = getReleases( args )        
    releaseDirectories = args        

    spandexHome = ''
    if os.environ.has_key( 'SPANDEX_HOME' ):
        spandexHome = os.path.normcase( os.environ[ 'SPANDEX_HOME' ] )
        
    wikireport = 'python %s' % os.path.join( spandexHome, 'python', 'wikireport.py' )

    #print 'Wikireport:', wikireport
    #print 'Releases:', releases

    for result in RESULTS:
        rfs = []
        for i in range( len( releaseDirectories ) ):
            d = getResultDirectory( result, releaseDirectories[ i ] )
            rfs.append( copyResult( d, result, releases[ i ] ) )

        ops = getWikireportOptions( options )
        cmd = wikireport + ops + ' '.join( rfs )
        #print cmd
        os.system( cmd )
        
