#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, os, shutil

# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] <performance result file>"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--min',
                       default = 0.75,
                       dest    = 'min',
                       help    = 'Minimum performance level [default: %default].' )
    parser.add_option( '--max',
                       default = 0,
                       dest    = 'max',
                       help    = 'Max performance level, 0 means no upper limit [default: %default].' )
    
    ( options, args ) = parser.parse_args()

    # Validate command line parameters.
    if len( args ) != 1:
        parser.error( 'Incorrect number of arguments' )
        sys.exit( -1 )

    filename = args[ 0 ]
        
    f     = file( filename, 'r' )
    lines = f.readlines()
    f.close()
            
    count      = len( lines )
    benchmarks = []
    longest    = 0
        
    for i in range( count ):
        line = lines[ i ]

        s = line.split( '|' )
        
        benchmark = s[ 1 ].strip()

        if not benchmark:
            continue

        if s[ 2 ].strip() == '-':
            result = 0
            min    = 0
            max    = 0
            type   = 'NOT SUPPORTED'
        else:
            result, type = s[ 2 ].split()
            result = float( result )
        
            if type == 's':
                # smaller is better
                max = result / options.min
                min = result * options.max
            else:
                # bigger is better
                min = result * options.min
                max = result * options.max
        
        if len( benchmark ) > longest:
            longest = len( benchmark )

        # Allow any performance to debug benchmarks (e.g. printing EGL configs
        # to output)
        if benchmark.find( 'debug' ) >= 0:
            result = 0.0
            min    = 0.0
            max    = 0.0
            
        benchmarks.append( ( benchmark, result, min, max, type, ) )

    for b in benchmarks:
        print '| %s%s | %.1f | %.1f | %.1f | %s |' % ( b[ 0 ],
                                                       ( ' ' * ( longest - len( b[ 0 ] ) ) ),
                                                       b[ 1 ],
                                                       b[ 2 ],
                                                       b[ 3 ],
                                                       b[ 4 ], )
