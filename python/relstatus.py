#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, os, shutil

RESULTS = [ 'vg11_vt_result',
            'gl11_vt_result',
            'gl20_vt_result',
            'vg11_perf_result',
            'gl11_perf_result',
            'gl20_perf_result',
            'vg11_gl11_perf_result',
            'vg11_gl20_perf_result',
            'vg11_ui_result',
            'gl11_ui_result',
            'gl20_ui_result',
            'gl20_ui2_result', ]

# ------------------------------------------------------------
def getResultDirectory( result, directory ):
    return os.path.normcase( os.path.join( directory, result ) )

# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] <release result directory>"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--full',
                       action  = 'store_true',
                       dest    = 'full',
                       default = False,
                       help    = 'Show status of every benchmark (default only failed)' )
    parser.add_option( '--short',
                       action  = 'store_true',
                       dest    = 'short',
                       default = False,
                       help    = 'Show short error string' )
    
    ( options, args ) = parser.parse_args()

    # Validate command line parameters.
    if len( args ) < 1:
        parser.error( 'Incorrect number of arguments' )
        sys.exit( -1 )

    releaseDirectory = args[ 0 ]

    spandexHome = ''
    if os.environ.has_key( 'SPANDEX_HOME' ):
        spandexHome = os.path.normcase( os.environ[ 'SPANDEX_HOME' ] )
        
    print '#Release', releaseDirectory
       
    for result in RESULTS:        
        directory = os.path.normcase( os.path.join( releaseDirectory, result ) )
        filename  = os.path.join( directory, result + '.txt' )

        print '\n# ----------------------------------------------------------------------'
        print '#', result
        print '# ----------------------------------------------------------------------'

        f     = file( filename, 'r' )
        lines = f.readlines()
        f.close()
            
        count                = len( lines )
        outLines             = []

        try:
            for i in range( count ):
                line = lines[ i ]
            
                # Look for the beginning of a benchmark section.
                if line.strip().endswith( 'BENCHMARK]' ):                
                    benchmarkName = line.split( ':' )[ 0 ][ 1 : ]
                    j             = 0
                    repeats       = -1
                    time          = -1.0
                    error         = None
                
                    while True:
                        # Start the processing from the next line after the start of a
                        # benchmark section
                        j += 1
                    
                        if i + j >= count:  # End of the file before end of the benchmark section.
                            print '# INVALID/INCOMPLETE BENCHMARK/TEST FILE %s' % result
                            raise RuntimeError()

                        line = lines[ i + j ]

                        # Check for benchmark error
                        if line.find( 'Error ' ) >= 0:
                            error = line.split( ':', 1 )[ 1 ].strip().strip( '"' )
                            if options.short:
                                error = error.split( ':' )[ -1 ].strip()

                        # Check for benchmark repeats
                        if line.find( 'Repeats' ) >= 0:
                            if len( line.split( ':' ) ) < 2:
                                continue
                            repeats = int( line.split( ':' )[ 1 ] )

                        # Check for benchmark times
                        if line.find( 'Times' ) >= 0:
                            if len( line.split( ':' ) ) < 2:
                                continue
                            t = line.split( ':' )[ 1 ].strip()
                            if t:
                                time = float( t.split()[ 0 ].replace( ',', '.' ).replace( '"', '' ) )

                        # Benchmark section done if error reported, or repeats and times parsed
                        if error or ( repeats >= 0 and time >= 0 ):
                            break

                    ######
                    assert( error or ( repeats >= 0 and time >= 0 ) )

                    if error:
                        outLines.append( [ benchmarkName, error ] )
                    else:
                        if options.full:
                            outLines.append( [ benchmarkName, 'OK' ] )

        except:
            pass
                            
        #####
        if len( outLines ) < 1:
            print '# No errors'
        else:
            for l in outLines:
                print '"%s" : "%s"' % ( l[ 0 ], l[ 1 ], )
