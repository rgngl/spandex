#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, os

# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] <log file>"
    parser = optparse.OptionParser( usage )

    ( options, args ) = parser.parse_args()
   
    # Validate command line parameters.
    if len( args ) != 1:
        parser.error( 'Incorrect number of arguments' )
        sys.exit( -1 )

    separator = '\0'
        
    # Open log file.
    logFile = None
    try:
        logFile = file( args[ 0 ], 'rb' )
    except:
        parser.error( 'Failed to open log file: %s' % args[ 0 ] );
        sys.exit( -1 )

    log = logFile.read()
    logFile.close()

    i1 = 0
    prefix = '[SPANDEX] '
    while True:
        i1 = log.find( prefix, i1 )
        if i1 < 0:
            break
        i1 += len( prefix )
        
        i2 = log.find( separator, i1 )
        if i2 < 0:
            break

        sys.stdout.write( log[ i1 : i2 ] )
        i1 = i2
