#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
import sys, datetime, time

######################################################################
from bdclitool  import *
from sct        import bmbuilder

######################################################################
commandConfig.update( { 'generate' : '<target>' } )

def handleGenerateCommand( suite, options, target ):
    print suite.generateSpandexInput( target )
    
Benchmark = bmbuilder.Benchmark

################################################################################
class BDCLI( CLI ):
    def __init__( self ):
        CLI.__init__( self, bmbuilder.Suite() )
        self.parser.add_option( '-l', '--library-path',
                                dest='libraryPath',
                                type='string',
                                default='lib',
                                help='path to BDS Python library' )
        self.parser.add_option( '-v', '--verbose',
                                action='store_true',
                                dest='verbose',
                                default=False,
                                help='print verbose progress information' )
        self.parser.add_option( '-d', '--debug',
                                action='store_true',
                                dest='debug',
                                default=False,
                                help='run in debug mode' )

    def postInit( self, options ):
        sys.path.append( options.libraryPath )
        # Make also all the stuff located at the same place is this script available
        # (__file__ contains the relative filename of the current module.)
        sys.path.append( os.path.dirname( __file__ ) )
       
def init():
    return BDCLI().run()
