#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
# Graphics APIs
API_OPENVG                   = 1
API_OPENGLES1                = 2
API_OPENGLES2                = 3

######################################################################
# Surface types
SURFACE_WINDOW               = 1
SURFACE_PBUFFER              = 2
SURFACE_PIXMAP               = 3

######################################################################
# Pixel read method
PIXELREAD_COPYBUFFERS        = 1
PIXELREAD_READPIXELS         = 2
PIXELREAD_WAIT               = 3

######################################################################
# Color formats
COLOR_FORMAT_DEFAULT         = 1
COLOR_FORMAT_BW1             = 2
COLOR_FORMAT_L8              = 3
COLOR_FORMAT_RGBA5551        = 4
COLOR_FORMAT_RGBA4444        = 5
COLOR_FORMAT_RGB565          = 6
COLOR_FORMAT_RGB888          = 7
COLOR_FORMAT_RGBX8888        = 8
COLOR_FORMAT_RGBA8888        = 9

######################################################################
# Config attributes
CONFIG_BUFFERSIZE               = 1
CONFIG_REDSIZE                  = 2
CONFIG_GREENSIZE                = 3
CONFIG_BLUESIZE                 = 4
CONFIG_ALPHASIZE                = 5
CONFIG_DEPTHSIZE                = 6
CONFIG_ALPHAMASKSIZE            = 7
CONFIG_SAMPLE_BUFFERS           = 8
CONFIG_SAMPLES                  = 9
CONFIG_STENCILSIZE              = 10
CONFIG_SUPPORTRGBTEXTURES       = 11
CONFIG_SUPPORTRGBATEXTURES      = 12
CONFIG_SURFACETYPE              = 13
CONFIG_RENDERABLETYPE           = 14
CONFIG_CONFIGID                 = 15
CONFIG_SWAP_BEHAVIOR            = 16

######################################################################
class IndexTracker:
    def __init__( self ):
        self.indices = {}

    def allocIndex( self, key ):
        if not self.indices.has_key( key ):
            self.indices[ key ] = 0

        i = self.indices[ key ] 
        self.indices[ key ] += 1
        return i

######################################################################
def boolToSpandex( b ):
    if b:
        return 'ON'
    else:
        return 'OFF'

