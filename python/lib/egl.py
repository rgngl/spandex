#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#
 
# ----------------------------------------------------------------------
import types

from common  import *

EGL_DEFINES = { 'EGL_FALSE'                         : 0,
                'EGL_TRUE'                          : 1,
                'EGL_DONT_CARE'                     : -1,
                'EGL_BUFFER_SIZE'                   : 0x3020,
                'EGL_ALPHA_SIZE'                    : 0x3021,
                'EGL_BLUE_SIZE'                     : 0x3022,
                'EGL_GREEN_SIZE'                    : 0x3023,
                'EGL_RED_SIZE'                      : 0x3024,
                'EGL_DEPTH_SIZE'                    : 0x3025,
                'EGL_STENCIL_SIZE'                  : 0x3026,
                'EGL_CONFIG_CAVEAT'                 : 0x3027,
                'EGL_CONFIG_ID'                     : 0x3028,
                'EGL_LEVEL'                         : 0x3029,
                'EGL_MAX_PBUFFER_HEIGHT'            : 0x302A,
                'EGL_MAX_PBUFFER_PIXELS'            : 0x302B,
                'EGL_MAX_PBUFFER_WIDTH'             : 0x302C,
                'EGL_NATIVE_RENDERABLE'             : 0x302D,
                'EGL_NATIVE_VISUAL_ID'              : 0x302E,
                'EGL_NATIVE_VISUAL_TYPE'            : 0x302F,
                'EGL_PRESERVED_RESOURCES'           : 0x3030,
                'EGL_SAMPLES'                       : 0x3031,
                'EGL_SAMPLE_BUFFERS'                : 0x3032,
                'EGL_SURFACE_TYPE'                  : 0x3033,
                'EGL_TRANSPARENT_TYPE'              : 0x3034,
                'EGL_TRANSPARENT_BLUE_VALUE'        : 0x3035,
                'EGL_TRANSPARENT_GREEN_VALUE'       : 0x3036,
                'EGL_TRANSPARENT_RED_VALUE'         : 0x3037,
                'EGL_NONE'                          : 0x3038,
                'EGL_BIND_TO_TEXTURE_RGB'           : 0x3039,
                'EGL_BIND_TO_TEXTURE_RGBA'          : 0x303A,
                'EGL_MIN_SWAP_INTERVAL'             : 0x303B,
                'EGL_MAX_SWAP_INTERVAL'             : 0x303C,
                'EGL_LUMINANCE_SIZE'                : 0x303D,
                'EGL_ALPHA_MASK_SIZE'               : 0x303E,
                'EGL_COLOR_BUFFER_TYPE'             : 0x303F,
                'EGL_RENDERABLE_TYPE'               : 0x3040,
                'EGL_MATCH_NATIVE_PIXMAP'           : 0x3041,
                'EGL_CONFORMANT'                    : 0x3042,
                'EGL_SLOW_CONFIG'                   : 0x3050,
                'EGL_NON_CONFORMANT_CONFIG'         : 0x3051,
                'EGL_TRANSPARENT_RGB'               : 0x3052,                     
                'EGL_RGB_BUFFER'                    : 0x308E,
                'EGL_LUMINANCE_BUFFER'              : 0x308F,
                'EGL_NO_TEXTURE'                    : 0x305C,
                'EGL_TEXTURE_RGB'                   : 0x305D,
                'EGL_TEXTURE_RGBA'                  : 0x305E,
                'EGL_TEXTURE_2D'                    : 0x305F,                       
                'EGL_PBUFFER_BIT'                   : 0x0001,
                'EGL_PIXMAP_BIT'                    : 0x0002,
                'EGL_WINDOW_BIT'                    : 0x0004,
                'EGL_VG_COLORSPACE_LINEAR_BIT'      : 0x0020,
                'EGL_VG_ALPHA_FORMAT_PRE_BIT'       : 0x0040,
                'EGL_MULTISAMPLE_RESOLVE_BOX_BIT'   : 0x0200,
                'EGL_SWAP_BEHAVIOR_PRESERVED_BIT'   : 0x0400,
                'EGL_OPENGL_ES_BIT'                 : 0x0001,
                'EGL_OPENVG_BIT'                    : 0x0002,
                'EGL_OPENGL_ES2_BIT'                : 0x0004,
                'EGL_OPENGL_BIT'                    : 0x0008,
                'EGL_HEIGHT'                        : 0x3056,
                'EGL_WIDTH'                         : 0x3057,
                'EGL_LARGEST_PBUFFER'               : 0x3058,
                'EGL_TEXTURE_FORMAT'                : 0x3080,
                'EGL_TEXTURE_TARGET'                : 0x3081,
                'EGL_MIPMAP_TEXTURE'                : 0x3082,
                'EGL_MIPMAP_LEVEL'                  : 0x3083,
                'EGL_RENDER_BUFFER'                 : 0x3086,
                'EGL_COLORSPACE'                    : 0x3087,
                'EGL_VG_COLORSPACE'                 : 0x3087,
                'EGL_ALPHA_FORMAT'                  : 0x3088,
                'EGL_VG_ALPHA_FORMAT'               : 0x3088,
                'EGL_HORIZONTAL_RESOLUTION'         : 0x3090,
                'EGL_VERTICAL_RESOLUTION'           : 0x3091,
                'EGL_PIXEL_ASPECT_RATIO'            : 0x3092,
                'EGL_SWAP_BEHAVIOR'                 : 0x3093,
                'EGL_MULTISAMPLE_RESOLVE'           : 0x3099,
                'EGL_BACK_BUFFER'                   : 0x3084,
                'EGL_SINGLE_BUFFER'                 : 0x3085,
                'EGL_COLORSPACE_sRGB'               : 0x3089,
                'EGL_VG_COLORSPACE_sRGB'            : 0x3089,
                'EGL_COLORSPACE_LINEAR'             : 0x308A,
                'EGL_VG_COLORSPACE_LINEAR'          : 0x308A,
                'EGL_ALPHA_FORMAT_NONPRE'           : 0x308B,
                'EGL_VG_ALPHA_FORMAT_NONPRE'        : 0x308B,
                'EGL_ALPHA_FORMAT_PRE'              : 0x308C,
                'EGL_VG_ALPHA_FORMAT_PRE'           : 0x308C,
                'EGL_BUFFER_PRESERVED'              : 0x3094,
                'EGL_BUFFER_DESTROYED'              : 0x3095,
                'EGL_MULTISAMPLE_RESOLVE_DEFAULT'   : 0x309A,
                'EGL_MULTISAMPLE_RESOLVE_BOX'       : 0x309B,
                'EGL_LOCK_SURFACE_BIT_KHR'          : 0x0080,
                'EGL_OPTIMAL_FORMAT_BIT_KHR'        : 0x0100,
                'EGL_MATCH_FORMAT_KHR'              : 0x3043,
                'EGL_FORMAT_RGB_565_EXACT_KHR'      : 0x30C0,
                'EGL_FORMAT_RGB_565_KHR'            : 0x30C1,
                'EGL_FORMAT_RGBA_8888_EXACT_KHR'    : 0x30C2,
                'EGL_FORMAT_RGBA_8888_KHR'          : 0x30C3,
                'EGL_SURFACE_SCALING_NOK'           : 0x30DD,
                'EGL_TARGET_EXTENT_OFFSET_X_NOK'    : 0x3079,
                'EGL_TARGET_EXTENT_OFFSET_Y_NOK'    : 0x307A,
                'EGL_TARGET_EXTENT_WIDTH_NOK'       : 0x307B,
                'EGL_TARGET_EXTENT_HEIGHT_NOK'      : 0x307C,
                'EGL_BORDER_COLOR_RED_NOK'          : 0x307D,
                'EGL_BORDER_COLOR_GREEN_NOK'        : 0x307E,
                'EGL_BORDER_COLOR_BLUE_NOK'         : 0x30D8,
                'EGL_FIXED_WIDTH_NOK'               : 0x30DB,
                'EGL_FIXED_HEIGHT_NOK'              : 0x30DC,
                'EGL_IMAGE_PRESERVED_KHR'           : 0x30D2,
                'EGL_GL_TEXTURE_LEVEL_KHR'          : 0x30BC,
                }

# ----------------------------------------------------------------------
def getEglAttributeListAsIntegerList( attributes ):
    intlist = []

    for a in attributes:
        intlist.append( EGL_DEFINES[ a ] )

    return intlist

# ----------------------------------------------------------------------
def getEglAttributes( api, surface, attributes ):
    eglSurfaces = { SURFACE_WINDOW  : 'EGL_WINDOW_BIT',
                    SURFACE_PBUFFER : 'EGL_PBUFFER_BIT',
                    SURFACE_PIXMAP  : 'EGL_PIXMAP_BIT' }

    attribs = {}

    attribs[ CONFIG_BUFFERSIZE ]            = 32
    attribs[ CONFIG_REDSIZE ]               = 8
    attribs[ CONFIG_GREENSIZE ]             = 8
    attribs[ CONFIG_BLUESIZE ]              = 8
    attribs[ CONFIG_ALPHASIZE ]             = 8
    attribs[ CONFIG_DEPTHSIZE ]             = '-'
    attribs[ CONFIG_ALPHAMASKSIZE ]         = '-'    
    attribs[ CONFIG_SAMPLE_BUFFERS ]        = 0
    attribs[ CONFIG_SAMPLES ]               = '>=0'
    attribs[ CONFIG_STENCILSIZE ]           = '-'
    attribs[ CONFIG_SUPPORTRGBTEXTURES ]    = 'EGL_DONT_CARE'
    attribs[ CONFIG_SUPPORTRGBATEXTURES ]   = 'EGL_DONT_CARE'
    attribs[ CONFIG_SWAP_BEHAVIOR ]         = False
    
    attribs[ CONFIG_SURFACETYPE ]           = [ eglSurfaces[ surface ] ]
    attribs[ CONFIG_RENDERABLETYPE ]        = []

    if api == API_OPENGLES1:
        attribs[ CONFIG_DEPTHSIZE ]         = '>=16'
        attribs[ CONFIG_RENDERABLETYPE ]    += [ 'EGL_OPENGL_ES_BIT' ]

    if api == API_OPENGLES2:        
        attribs[ CONFIG_DEPTHSIZE ]         = '>=16'
        attribs[ CONFIG_RENDERABLETYPE ]    += [ 'EGL_OPENGL_ES2_BIT' ]
        
    if api == API_OPENVG:
        attribs[ CONFIG_ALPHAMASKSIZE ]     = '>=8'
        attribs[ CONFIG_RENDERABLETYPE ]    += [ 'EGL_OPENVG_BIT' ]

    for k in attributes.keys():
        attribs[ k ] = attributes[ k ]

    return attribs

# ----------------------------------------------------------------------
def convertCInt( a ):
    if type( a ) == types.StringType:
        if a == '-':
            return None
        str = ''
        for c in a: 
            if c in [ '<', '>', '=' ]:
                continue
            str += c
        return int( str )
    
    return a

# ----------------------------------------------------------------------
def getEglAttributeList( api, surface, attributes ):    
    eglApis     = { API_OPENGLES1  : 'EGL_OPENGL_ES_BIT',
                    API_OPENVG     : 'EGL_OPENVG_BIT',
                    API_OPENGLES2  : 'EGL_OPENGL_ES2_BIT' }

    eglSurfaces = { SURFACE_WINDOW  : 'EGL_WINDOW_BIT',
                    SURFACE_PBUFFER : 'EGL_PBUFFER_BIT',
                    SURFACE_PIXMAP  : 'EGL_PIXMAP_BIT' }

    eglAttributes = []

    if attributes and attributes.has_key( CONFIG_CONFIGID ):
        eglAttributes.append( EGL_DEFINES[ 'EGL_CONFIG_ID' ] )
        eglAttributes.append( attributes[ CONFIG_CONFIGID ] )
        
        return eglAttributes;
    
    if not eglApis.has_key( api ):
        raise 'Unexpected API ' + str( api )
    
    eglAttributes.append( EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ] )
    eglAttributes.append( EGL_DEFINES[ eglApis[ api ] ] )

    if not eglSurfaces.has_key( surface ):
        raise 'Unexpected surface ' + str( surface )

    eglAttributes.append( EGL_DEFINES[ 'EGL_SURFACE_TYPE' ] )
    eglAttributes.append( EGL_DEFINES[ eglSurfaces[ surface ] ] )
    
    if attributes:
        if attributes.has_key( CONFIG_BUFFERSIZE ) and convertCInt( attributes[ CONFIG_BUFFERSIZE ] ):
            eglAttributes.append( EGL_DEFINES[ 'EGL_BUFFER_SIZE' ] )
            eglAttributes.append( convertCInt( attributes[ CONFIG_BUFFERSIZE ] ) )
        if attributes.has_key( CONFIG_REDSIZE ) and convertCInt( attributes[ CONFIG_REDSIZE ] ):
            eglAttributes.append( EGL_DEFINES[ 'EGL_RED_SIZE' ] )
            eglAttributes.append( convertCInt( attributes[ CONFIG_REDSIZE ] ) )
        if attributes.has_key( CONFIG_GREENSIZE ) and convertCInt( attributes[ CONFIG_GREENSIZE ] ):
            eglAttributes.append( EGL_DEFINES[ 'EGL_GREEN_SIZE' ] )
            eglAttributes.append( convertCInt( attributes[ CONFIG_GREENSIZE ] ) )
        if attributes.has_key( CONFIG_BLUESIZE ) and convertCInt( attributes[ CONFIG_BLUESIZE ] ):
            eglAttributes.append( EGL_DEFINES[ 'EGL_BLUE_SIZE' ] )
            eglAttributes.append( convertCInt( attributes[ CONFIG_BLUESIZE ] ) )
        if attributes.has_key( CONFIG_ALPHASIZE ) and convertCInt( attributes[ CONFIG_ALPHASIZE ] ):
            eglAttributes.append( EGL_DEFINES[ 'EGL_ALPHA_SIZE' ] )
            eglAttributes.append( convertCInt( attributes[ CONFIG_ALPHASIZE ] ) )
        if attributes.has_key( CONFIG_DEPTHSIZE ) and convertCInt( attributes[ CONFIG_DEPTHSIZE ] ):
            eglAttributes.append( EGL_DEFINES[ 'EGL_DEPTH_SIZE' ] )
            eglAttributes.append( convertCInt( attributes[ CONFIG_DEPTHSIZE ] ) )
        if attributes.has_key( CONFIG_ALPHAMASKSIZE ) and convertCInt( attributes[ CONFIG_ALPHAMASKSIZE ] ):
            eglAttributes.append( EGL_DEFINES[ 'EGL_ALPHA_MASK_SIZE' ] )
            eglAttributes.append( convertCInt( attributes[ CONFIG_ALPHAMASKSIZE ] ) )
        if attributes.has_key( CONFIG_SAMPLES ) and convertCInt( attributes[ CONFIG_SAMPLES ] ):
            eglAttributes.append( EGL_DEFINES[ 'EGL_SAMPLES' ] )
            eglAttributes.append( convertCInt( attributes[ CONFIG_SAMPLES ] ) )
        if attributes.has_key( CONFIG_SAMPLE_BUFFERS ) and convertCInt( attributes[ CONFIG_SAMPLE_BUFFERS ] ):
            eglAttributes.append( EGL_DEFINES[ 'EGL_SAMPLE_BUFFERS' ] )
            eglAttributes.append( convertCInt( attributes[ CONFIG_SAMPLE_BUFFERS ] ) )
        if attributes.has_key( CONFIG_STENCILSIZE ) and convertCInt( attributes[ CONFIG_STENCILSIZE ] ):
            eglAttributes.append( EGL_DEFINES[ 'EGL_STENCIL_SIZE' ] )
            eglAttributes.append( convertCInt( attributes[ CONFIG_STENCILSIZE ] ) )
        if attributes.has_key( CONFIG_SWAP_BEHAVIOR ) and attributes[ CONFIG_SWAP_BEHAVIOR ]:
            i =  eglAttributes.index( EGL_DEFINES[ 'EGL_SURFACE_TYPE' ] )
            assert( i >= 0 )
            eglAttributes[ i + 1 ] = eglAttributes[ i + 1 ] | EGL_DEFINES[ 'EGL_SWAP_BEHAVIOR_PRESERVED_BIT' ]
            
    return eglAttributes;

# ----------------------------------------------------------------------
def eglInitializeWindow( Egl, indexTracker,api, windowAttributes, configAttributes, surfaceAttributes, ext = False ):
    displayIndex    = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
    configIndex     = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
    surfaceIndex    = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
    contextIndex    = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
    windowIndex     = -1

    Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' )
    if not ext:
        Egl.Initialize( displayIndex )
    else:
        Egl.InitializeExt( displayIndex )
            
    if api == API_OPENVG:        
        Egl.BindApi( 'EGL_OPENVG_API' )
    else:
        Egl.BindApi( 'EGL_OPENGL_ES_API' )        
    
    apiVersion = -1

    if api == API_OPENGLES2:    
        apiVersion = 2

    windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )       
    Egl.CreateWindow( windowIndex, 
                      'SCT_SCREEN_DEFAULT', 
                      windowAttributes[ 0 ], 
                      windowAttributes[ 1 ], 
                      windowAttributes[ 2 ], 
                      windowAttributes[ 3 ], 
                      windowAttributes[ 4 ] )    

    surface = configAttributes[ CONFIG_SURFACETYPE ]
    
    if configAttributes[ CONFIG_SWAP_BEHAVIOR ]:
        surface += [ 'EGL_SWAP_BEHAVIOR_PRESERVED_BIT' ]
    
    Egl.RGBConfig( displayIndex,
                   configIndex,
                   configAttributes[ CONFIG_BUFFERSIZE ],
                   configAttributes[ CONFIG_REDSIZE ],
                   configAttributes[ CONFIG_GREENSIZE ],
                   configAttributes[ CONFIG_BLUESIZE ],
                   configAttributes[ CONFIG_ALPHASIZE ],
                   configAttributes[ CONFIG_DEPTHSIZE ],
                   configAttributes[ CONFIG_ALPHAMASKSIZE ],
                   configAttributes[ CONFIG_SAMPLE_BUFFERS ],
                   configAttributes[ CONFIG_SAMPLES ],
                   configAttributes[ CONFIG_STENCILSIZE ],
                   configAttributes[ CONFIG_SUPPORTRGBTEXTURES ],
                   configAttributes[ CONFIG_SUPPORTRGBATEXTURES ],
                   surface,
                   configAttributes[ CONFIG_RENDERABLETYPE ] )

    Egl.CreateWindowSurface( displayIndex, 
                             surfaceIndex, 
                             configIndex, 
                             windowIndex, 
                             surfaceAttributes[ 0 ], 
                             surfaceAttributes[ 1 ], 
                             surfaceAttributes[ 2 ] )       

    Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, apiVersion )    
    Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

    return { 'Egl'          : Egl,
             'indexTracker' : indexTracker,
             'windowIndex'  : windowIndex,
             'displayIndex' : displayIndex,
             'configIndex'  : configIndex,
             'surfaceIndex' : surfaceIndex,
             'contextIndex' : contextIndex }

# ----------------------------------------------------------------------
def eglInitializeWindowFromList( Egl, indexTracker, api, windowAttributes, configAttributeList, surfaceAttributes, ext = False ):
    displayIndex    = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
    configIndex     = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
    surfaceIndex    = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
    contextIndex    = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
    windowIndex     = -1
    
    Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' )
    if not ext:
        Egl.Initialize( displayIndex )
    else:
        Egl.InitializeExt( displayIndex )

    if api == API_OPENVG:
        Egl.BindApi( 'EGL_OPENVG_API' )
    else:
        Egl.BindApi( 'EGL_OPENGL_ES_API' )        

    apiVersion = -1

    if api == API_OPENGLES2:    
        apiVersion = 2
        
    Egl.Config( displayIndex, configIndex, configAttributeList )

    windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )       
    Egl.CreateWindow( windowIndex, 
                      'SCT_SCREEN_DEFAULT', 
                      windowAttributes[ 0 ], 
                      windowAttributes[ 1 ], 
                      windowAttributes[ 2 ], 
                      windowAttributes[ 3 ], 
                      windowAttributes[ 4 ] )
    
    Egl.CreateWindowSurface( displayIndex, 
                             surfaceIndex, 
                             configIndex, 
                             windowIndex, 
                             surfaceAttributes[ 0 ], 
                             surfaceAttributes[ 1 ], 
                             surfaceAttributes[ 2 ] )
    Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, apiVersion )    
    Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

    return { 'Egl'          : Egl,
             'indexTracker' : indexTracker,
             'windowIndex'  : windowIndex,
             'displayIndex' : displayIndex,
             'configIndex'  : configIndex,
             'surfaceIndex' : surfaceIndex,
             'contextIndex' : contextIndex }

# ----------------------------------------------------------------------
gStaticDisplayIndex = -1
gStaticWindowIndex  = -1;
gStaticSurfaceIndex = -1;

# ----------------------------------------------------------------------
def eglInitializeStaticWindow( Egl, indexTracker, api, windowAttributes, configAttributes, surfaceAttributeList ):
    global gStaticDisplayIndex, gStaticWindowIndex

    configIndex  = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
    contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )

    if gStaticDisplayIndex < 0:
        assert gStaticDisplayIndex < 0
        
        gStaticDisplayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( gStaticDisplayIndex, 'SCT_SCREEN_DEFAULT' )
        
        Egl.InitializeExt( gStaticDisplayIndex )
    else:
        indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )

    assert gStaticDisplayIndex >= 0

    if api == API_OPENVG:
        Egl.BindApi( 'EGL_OPENVG_API' )
    else:
        Egl.BindApi( 'EGL_OPENGL_ES_API' )  

    if gStaticWindowIndex < 0:
        assert gStaticWindowIndex < 0

        gStaticWindowIndex  = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateStaticWindow( gStaticWindowIndex, 
                                'SCT_SCREEN_DEFAULT', 
                                windowAttributes[ 0 ], 
                                windowAttributes[ 1 ], 
                                windowAttributes[ 2 ], 
                                windowAttributes[ 3 ], 
                                windowAttributes[ 4 ] )
    else:
        indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        
    assert gStaticWindowIndex >= 0

    surface = configAttributes[ CONFIG_SURFACETYPE ]
    
    if configAttributes[ CONFIG_SWAP_BEHAVIOR ]:
        surface += [ 'EGL_SWAP_BEHAVIOR_PRESERVED_BIT' ]        
    
    Egl.RGBConfig( gStaticDisplayIndex,
                   configIndex,
                   configAttributes[ CONFIG_BUFFERSIZE ],
                   configAttributes[ CONFIG_REDSIZE ],
                   configAttributes[ CONFIG_GREENSIZE ],
                   configAttributes[ CONFIG_BLUESIZE ],
                   configAttributes[ CONFIG_ALPHASIZE ],
                   configAttributes[ CONFIG_DEPTHSIZE ],
                   configAttributes[ CONFIG_ALPHAMASKSIZE ],
                   configAttributes[ CONFIG_SAMPLE_BUFFERS ],
                   configAttributes[ CONFIG_SAMPLES ],
                   configAttributes[ CONFIG_STENCILSIZE ],
                   configAttributes[ CONFIG_SUPPORTRGBTEXTURES ],
                   configAttributes[ CONFIG_SUPPORTRGBATEXTURES ],
                   surface,
                   configAttributes[ CONFIG_RENDERABLETYPE ] )

    surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
    Egl.CreateWindowSurfaceExt( gStaticDisplayIndex, 
                                surfaceIndex, 
                                configIndex,
                                gStaticWindowIndex, 
                                surfaceAttributeList )

    apiVersion = -1

    if api == API_OPENGLES2:    
        apiVersion = 2

    Egl.CreateContext( gStaticDisplayIndex, contextIndex, configIndex, -1, apiVersion )    
    Egl.MakeCurrent( gStaticDisplayIndex, surfaceIndex, surfaceIndex, contextIndex )

    return { 'Egl'          : Egl,
             'indexTracker' : indexTracker,
             'windowIndex'  : gStaticWindowIndex,
             'displayIndex' : gStaticDisplayIndex,
             'configIndex'  : configIndex,             
             'surfaceIndex' : surfaceIndex,
             'contextIndex' : contextIndex }

# ----------------------------------------------------------------------
def eglInitializeStaticWindowSurface( Egl, indexTracker, api, windowAttributes, configAttributes, surfaceAttributeList ):
    global gStaticDisplayIndex, gStaticWindowIndex, gStaticSurfaceIndex

    configIndex  = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
    contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )

    if gStaticDisplayIndex < 0:
        assert gStaticDisplayIndex < 0

        gStaticDisplayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( gStaticDisplayIndex, 'SCT_SCREEN_DEFAULT' )
        
        Egl.InitializeExt( gStaticDisplayIndex )
    else:
        indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )

    assert gStaticDisplayIndex >= 0

    if api == API_OPENVG:
        Egl.BindApi( 'EGL_OPENVG_API' )
    else:
        Egl.BindApi( 'EGL_OPENGL_ES_API' )  

    surface = configAttributes[ CONFIG_SURFACETYPE ]
    
    if configAttributes[ CONFIG_SWAP_BEHAVIOR ]:
        surface += [ 'EGL_SWAP_BEHAVIOR_PRESERVED_BIT' ]        
        
    Egl.RGBConfig( gStaticDisplayIndex,
                   configIndex,
                   configAttributes[ CONFIG_BUFFERSIZE ],
                   configAttributes[ CONFIG_REDSIZE ],
                   configAttributes[ CONFIG_GREENSIZE ],
                   configAttributes[ CONFIG_BLUESIZE ],
                   configAttributes[ CONFIG_ALPHASIZE ],
                   configAttributes[ CONFIG_DEPTHSIZE ],
                   configAttributes[ CONFIG_ALPHAMASKSIZE ],
                   configAttributes[ CONFIG_SAMPLE_BUFFERS ],
                   configAttributes[ CONFIG_SAMPLES ],
                   configAttributes[ CONFIG_STENCILSIZE ],
                   configAttributes[ CONFIG_SUPPORTRGBTEXTURES ],
                   configAttributes[ CONFIG_SUPPORTRGBATEXTURES ],
                   surface,
                   configAttributes[ CONFIG_RENDERABLETYPE ] )

    if gStaticWindowIndex < 0:
        assert gStaticWindowIndex  < 0
        assert gStaticSurfaceIndex < 0

        gStaticWindowIndex  = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateStaticWindow( gStaticWindowIndex, 
                                'SCT_SCREEN_DEFAULT', 
                                windowAttributes[ 0 ], 
                                windowAttributes[ 1 ], 
                                windowAttributes[ 2 ], 
                                windowAttributes[ 3 ], 
                                windowAttributes[ 4 ] )

        gStaticSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateStaticWindowSurfaceExt( gStaticDisplayIndex, 
                                          gStaticSurfaceIndex, 
                                          configIndex,
                                          gStaticWindowIndex, 
                                          surfaceAttributeList )
    else:
        indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
    assert gStaticWindowIndex  >= 0
    assert gStaticSurfaceIndex >= 0

    apiVersion = -1

    if api == API_OPENGLES2:    
        apiVersion = 2

    Egl.CreateContext( gStaticDisplayIndex, contextIndex, configIndex, -1, apiVersion )    
    Egl.MakeCurrentExt( gStaticDisplayIndex, gStaticSurfaceIndex, gStaticSurfaceIndex, contextIndex )

    return { 'Egl'          : Egl,
             'indexTracker' : indexTracker,
             'windowIndex'  : gStaticWindowIndex,
             'displayIndex' : gStaticDisplayIndex,
             'configIndex'  : configIndex,
             'surfaceIndex' : gStaticSurfaceIndex,
             'contextIndex' : contextIndex }

# ----------------------------------------------------------------------
def eglTerminate( state ):
    Egl = state[ 'Egl' ]
    displayIndex = state[ 'displayIndex' ]
    contextIndex = state[ 'contextIndex' ]
    surfaceIndex = state[ 'surfaceIndex' ]
    windowIndex  = state[ 'windowIndex' ]
    Egl.MakeCurrent( displayIndex, -1, -1, -1 )
    Egl.DestroyContext( displayIndex, contextIndex )
    Egl.DestroySurface( displayIndex, surfaceIndex )
    Egl.DestroyWindow( windowIndex )

# ----------------------------------------------------------------------    
def eglSwapBuffers( state ):
    Egl = state[ 'Egl' ]
    displayIndex = state[ 'displayIndex' ]
    surfaceIndex = state[ 'surfaceIndex' ]
    Egl.SwapBuffers( displayIndex, surfaceIndex )
