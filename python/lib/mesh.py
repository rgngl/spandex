#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
# Imports
import random
import math

######################################################################
# Function parameter constants
MESH_CCW_WINDING                = 1
MESH_CW_WINDING                 = 2
MESH_ALTERNATING_WINDING        = 3

MESH_TYPE_UNSIGNED_BYTE         = 1
MESH_TYPE_BYTE                  = 2
MESH_TYPE_UNSIGNED_SHORT        = 3
MESH_TYPE_SHORT                 = 4
MESH_TYPE_FIXED                 = 5
MESH_TYPE_FLOAT                 = 6

######################################################################
# Internal constants
MESH_MAX_BYTE                   = 0x7F
MESH_MAX_UNSIGNED_BYTE          = 0xFF
MESH_MAX_SHORT                  = 0x7FFF
MESH_MAX_UNSIGNED_SHORT         = 0xFFFF

######################################################################
RAD = 360.0 / ( 2.0 * math.pi )

######################################################################
class MeshAttribute:
    def __init__( self, type ):
        self.type       = type

######################################################################
class MeshVertexAttribute( MeshAttribute ):
    def __init__( self, type, components ):
        MeshAttribute.__init__( self, type )
        self.components = components

######################################################################
class MeshTexCoordAttribute( MeshVertexAttribute ):
    def __init__( self, type, components, scale = [ 1.0, 1.0 ] ):
        MeshVertexAttribute.__init__( self, type, components )
        self.scale      = scale[ : ]

######################################################################
def _randomVertexColors( vertexCount, colorType, randomAlpha = False, seed = None ):
    colors              = []

    random.seed( seed )
    
    if colorType == MESH_TYPE_UNSIGNED_BYTE:
        for i in range( vertexCount ):
            colors += [ int( round( random.random() * MESH_MAX_UNSIGNED_BYTE ) ) ]
            colors += [ int( round( random.random() * MESH_MAX_UNSIGNED_BYTE ) ) ]
            colors += [ int( round( random.random() * MESH_MAX_UNSIGNED_BYTE ) ) ]
            if randomAlpha:
                colors += [ int( round( random.random() * MESH_MAX_UNSIGNED_BYTE ) ) ]
            else:
                colors += [ MESH_MAX_UNSIGNED_BYTE ]
    elif colorType == MESH_TYPE_FLOAT:
        for i in range( vertexCount ):
            colors += [ random.random() ]
            colors += [ random.random() ]
            colors += [ random.random() ]
            if randomAlpha:
                colors += [ random.random() ]
            else:
                colors += [ 1.0 ]
    else:
        raise 'Unexpected color type: ' + str( colorType )

    return colors

######################################################################
def _getScale( type ):
    scales = { MESH_TYPE_BYTE           : MESH_MAX_BYTE,
               MESH_TYPE_UNSIGNED_BYTE  : MESH_MAX_UNSIGNED_BYTE,
               MESH_TYPE_SHORT          : MESH_MAX_UNSIGNED_SHORT,
               MESH_TYPE_UNSIGNED_SHORT : MESH_MAX_UNSIGNED_SHORT }

    if scales.has_key( type ):
        return scales[ type ]
    else:
        return 1.0

######################################################################
def _meshTypeToGLType( meshType ):
    typeMap = { MESH_TYPE_UNSIGNED_BYTE         : 'GL_UNSIGNED_BYTE',
                MESH_TYPE_BYTE                  : 'GL_BYTE',
                MESH_TYPE_UNSIGNED_SHORT        : 'GL_UNSIGNED_SHORT',
                MESH_TYPE_SHORT                 : 'GL_SHORT',
                MESH_TYPE_FIXED                 : 'GL_FIXED',
                MESH_TYPE_FLOAT                 : 'GL_FLOAT' }                
    if not typeMap.has_key( meshType ):
        raise 'Invalid type'

    return typeMap[ meshType ]

######################################################################
def _checkRange( type, value ):
    if type == MESH_TYPE_UNSIGNED_BYTE and value > MESH_MAX_UNSIGNED_BYTE:
        return False
    elif type == MESH_TYPE_UNSIGNED_SHORT and value > MESH_MAX_UNSIGNED_SHORT:
        return False

    return True

######################################################################
class MeshPlaneBase:
    def __init__( self,
                  width,
                  height,
                  vertexAttribute,
                  colorAttribute,
                  normalAttribute,
                  texCoordAttributes,
                  indexAttribute,
                  vertexColor,
                  flipNormals ):
        # Variables to hold mesh data
        self.width                      = width
        self.height                     = height
        self.vertices                   = []
        self.vertexType                 = None
        self.vertexGLType               = None
        self.vertexComponents           = None
        self.vertexNormalized           = False
        self.colors                     = []
        self.colorType                  = None
        self.colorGLType                = None
        self.colorComponents            = None
        self.colorNormalized            = False
        self.normals                    = []
        self.normalType                 = None
        self.normalGLType               = None
        self.normalComponents           = 3     # Always 3
        self.normalNormalized           = True  # Always normalized
        self.texCoords                  = []
        self.texCoordsComponents        = []
        self.texCoordsTypes             = []
        self.texCoordsGLTypes           = []
        self.texCoordsNormalized        = []
        self.indices                    = []
        self.indexType                  = None
        self.indexGLType                = None
        self.indexComponents            = 1     # Always 1

        # ----->
        # Code
        # ----->
        
        if not texCoordAttributes:
            texCoordAttributes  = []
            
        numOfTexCoords                  = len( texCoordAttributes )
        self.texCoordsComponents        = [ None ] * numOfTexCoords
        self.texCoordsTypes             = [ None ] * numOfTexCoords
        self.texCoordsGLTypes           = [ None ] * numOfTexCoords
        self.texCoordsNormalized        = [ False ] * numOfTexCoords

        if indexAttribute.type != MESH_TYPE_UNSIGNED_BYTE and \
           indexAttribute.type != MESH_TYPE_UNSIGNED_SHORT:
            raise 'Invalid index data type'

        self.indexType                  = indexAttribute.type
        self.indexGLType                = _meshTypeToGLType( indexAttribute.type )

        if width < 2:
            raise 'Invalid width'
        if height < 2:
            raise 'Invalid height'

        # Calculate the vertex/texture coordinate increments
        if width == 2:
            incx = 1.0
        else:
            incx = 1.0 / ( width - 1 )

        if height == 2:
            incy = 1.0
        else:
            incy = 1.0 / ( height - 1 )

        # Set default components
        self.vertexComponents        = 4
        self.colorComponents         = 4
        #self.texCoordComponents      = [ 4 ] * numOfTexCoords

        # Set components from input data, if given
        if vertexAttribute:
            if vertexAttribute.components < 2 or vertexAttribute.components > 4:
                raise 'Invalid number of vertex components'
            self.vertexComponents       = vertexAttribute.components
            self.vertexType             = vertexAttribute.type
            self.vertexGLType           = _meshTypeToGLType( vertexAttribute.type )
            
        if colorAttribute:
            if colorAttribute.components != 4:
                raise 'Invalid number of color components'
            if colorAttribute.type != MESH_TYPE_UNSIGNED_BYTE and \
               colorAttribute.type != MESH_TYPE_FIXED and \
               colorAttribute.type != MESH_TYPE_FLOAT:
                raise 'Invalid color type'
            self.colorComponents        = colorAttribute.components
            self.colorType              = colorAttribute.type
            self.colorGLType            = _meshTypeToGLType( colorAttribute.type )
            
        if normalAttribute:
            if normalAttribute.type == MESH_TYPE_UNSIGNED_BYTE or \
               normalAttribute.type == MESH_TYPE_UNSIGNED_SHORT:
                raise 'Invalid normal type'          
            self.normalComponents       = 3
            self.normalType             = normalAttribute.type
            self.normalGLType           = _meshTypeToGLType( normalAttribute.type )
            
        for i in range( numOfTexCoords ):
            # It is possible for the array of texture coordinate
            # arrays to have "holes", i.e. undefined texture
            # coordinate arrays for some texture unit
            if texCoordAttributes[ i ]:
                if texCoordAttributes[ i ].components != 2:               
                    raise 'Invalid number of texture coordinate components'
                self.texCoordsComponents[ i ]   = texCoordAttributes[ i ].components
                self.texCoordsTypes[ i ]        = texCoordAttributes[ i ].type
                self.texCoordsGLTypes[ i ]      = _meshTypeToGLType( texCoordAttributes[ i ].type )
            else:
                self.texCoordsComponents[ i ]   = -1
                self.texCoordsTypes[ i ]        = -1
                self.texCoordsGLTypes[ i ]      = -1
            
        # Set default values for vertices, colors and normals data
        vertices                = [ [ 0, 0, 0 ] ] * ( width * height )
        colors                  = [ [ 0, 0, 0, 0 ] ] * ( width * height )
        if not flipNormals:
            normals             = [ [ 0, 0, 1 ] ] * ( width * height )
        else:
            normals             = [ [ 0, 0, -1 ] ] * ( width * height )
            
        # Set vertex color
        if vertexColor:
            color               = vertexColor
        else:
            color               = [ 1.0 ] * self.colorComponents

        # Map color to unsigned byte range (reject invalid types)
        if colorAttribute and colorAttribute.type == MESH_TYPE_UNSIGNED_BYTE:
            color                       = map(lambda x: int( round( x * MESH_MAX_UNSIGNED_BYTE ) ), color )
            self.colorNormalized        = True

        texCoords               = []
        # Create default (0,0) texture coordinates for each vertex        
        for i in range( numOfTexCoords ):
            texCoords.append( [] )
            if texCoordAttributes[ i ]:
                for j in range( width * height ):
                    texCoords[ i ].append( [ 0, 0 ] )

        # ----->
        # Calculate vertex data
        for y in range( height ):
            for x in range( width ):
                vertexIndex = y * width + x
                # Calculate vertex x,y coordinates
                vertex = [ x * incx, y * incy ]
                
                # Add padding to vertex according to vertexComponents
                for i in range( self.vertexComponents - 2 ):
                    vertex.append( [ 0, 1 ][ i ] )
                                   
                vertices[ vertexIndex ] = vertex

                colors[ vertexIndex ] = color[ : ]

                for i in range( numOfTexCoords ):
                    if texCoordAttributes[ i ]:
                        # Calculate texture x,y coordinates                        
                        texCoord = [ x * incx, y * incy ]

                        # Add padding to texture coordinates accordint to texture coordinate components
                        for j in range( self.texCoordsComponents[ i ] - 2 ):
                            texCoord.append( [ 0, 1 ][ j ] )

                        texCoords[ i ][ vertexIndex ] = texCoord

        # Vertex and texture coordinate data lies in [0, 1] float
        # range and normals in [-1, 1] range, so we need to defined
        # scales for integer data types to spread the data to [-RANGE,
        # RANGE] range. We then use normalized = GL_TRUE when
        # inputting the vertex data.
        vertexScale     = 1.0
        normalScale     = 1.0   
        texCoordScales  = []
        
        # Select proper vertex scaler
        if vertexAttribute:
            vertexScale = _getScale( self.vertexType )

        # Select proper normal scaler, normals are always normalized
        if normalAttribute:
            normalScale = _getScale( normalAttribute.type )

        # Scale the vertices
        for v in vertices:
            self.vertices += map( lambda x: x * vertexScale, v )

        if vertexScale != 1.0:
            self.vertexNormalized       = True

        # Colors are already correctly scaled earlier
        if colorAttribute:      
            for c in colors:
                self.colors += c

        # Calculate random vertex colors if vertex color is not given
        if colorAttribute and not vertexColor:
            self.colors = _randomVertexColors( width * height,
                                               colorAttribute.type,
                                               False,
                                               2 )

        # Scale normals
        if normalAttribute:        
            for n in normals:
                self.normals += map( lambda x: x * normalScale, n )

        floatScaler     = [ 1.0, 1.0 ]
        for i in range( numOfTexCoords ):
            self.texCoords.append( [] )

            if texCoordAttributes[ i ]:
                # Texture coordinate scale is supported only with fixed or
                # float type
                if texCoordAttributes[ i ].scale[ 0 ] != 1.0 or \
                   texCoordAttributes[ i ].scale[ 1 ] != 1.0:
                    if texCoordAttributes[ i ].type != MESH_TYPE_FIXED and \
                       texCoordAttributes[ i ].type != MESH_TYPE_FLOAT:
                        raise 'Texture coordinate scale supported only with fixed or float type'

                # Determine texture coordinate scaler
                texCoordScales          = _getScale( texCoordAttributes[ i ].type )
            
                floatScaler[ 0 ]        = texCoordAttributes[ i ].scale[ 0 ] * texCoordScales
                floatScaler[ 1 ]        = texCoordAttributes[ i ].scale[ 1 ] * texCoordScales

                # Mark texture coordinates normalized
                if texCoordAttributes[ i ].type != MESH_TYPE_FIXED and \
                   texCoordAttributes[ i ].type != MESH_TYPE_FLOAT:
                    if floatScaler[ 0 ] != 1.0 or floatScaler[ 1 ] != 1.0:
                        self.texCoordsNormalized[ i ] = True
                    
                for t in texCoords[ i ]:
                    self.texCoords[ i ] += [ t[ 0 ] * floatScaler[ 0 ], t[ 1 ] * floatScaler[ 1 ] ]

    def translate( self, vector ):
        if not self.vertices or not self.vertexComponents:
            raise 'Undefined vertices'

        if self.vertexNormalized:
            raise 'Translate not supported for normalized vertices'
        
        vertexCount      = len( self.vertices ) / self.vertexComponents
        vertexComponents = min( len( vector ), self.vertexComponents )
        
        for j in range( vertexCount ):
            for i in range( vertexComponents ):
                self.vertices[ j * self.vertexComponents + i ] += vector[ i ]
            
    def scale( self, vector ):
        if not self.vertices or not self.vertexComponents:
            raise 'Undefined vertices'

        if self.vertexNormalized:
            raise 'Scale not supported for normalized vertices'
        
        vertexCount      = len( self.vertices ) / self.vertexComponents
        vertexComponents = min( len( vector ), self.vertexComponents )
        
        for j in range( vertexCount ):
            for i in range( vertexComponents ):
                self.vertices[ j * self.vertexComponents + i ] *= vector[ i ]
       
    def zero( self ):
        if not self.vertices or not self.vertexComponents:
            raise 'Undefined vertices'

        if self.vertexNormalized:
            raise 'Zero not supported for normalized vertices'
        
        vertexCount = len( self.vertices ) / self.vertexComponents
        
        for j in range( vertexCount ):
            for i in range( self.vertexComponents ):
                self.vertices[ j * self.vertexComponents + i ] = 0

        if self.colors:
            for j in range( vertexCount ):
                for i in range( self.colorComponents ):
                    self.colors[ j * self.colorComponents + i ] = 0
                    
        if self.normals:
            for j in range( vertexCount ):
                for i in range( self.normalComponents ):
                    self.normals[ j * self.normalComponents + i ] = 0
        
        if self.texCoords:
            for t in range( len( self.texCoords ) ):
                for j in range( vertexCount ):
                    for i in range( self.texCoordsComponents[ t ] ):
                        self.texCoords[ t ][ j * self.texCoordsComponents[ t ] + i ] = 0
                
        if self.indices:
                for i in range( len( self.indices ) ):
                    self.indices[ i ] = 0

    def depthGradient( self, startZ, endZ ):
        if not self.vertices or not self.vertexComponents:
            raise 'Undefined vertices'

        if self.vertexComponents < 3:
            raise 'No z in vertex data'
        
        vertexCount = len( self.vertices ) / self.vertexComponents
        
        d = ( float( endZ ) - float( startZ ) ) / float( vertexCount )
        r = startZ

        for j in range( vertexCount ):
            self.vertices[ j * self.vertexComponents + 2 ] = r
            r += d
                
    def gradientColors( self, color1, color2 ):
        newColors = []
        r = 0.0
        multiplier = 1.0
        f = float
        if self.colorType == MESH_TYPE_UNSIGNED_BYTE:
            multiplier = MESH_MAX_UNSIGNED_BYTE
            f = int

        colorCount = len( self.colors ) / self.colorComponents
        
        for i in range( colorCount ):
            r = i / float( colorCount - 1 );
            for j in range( self.colorComponents ):
                newColors += [ f( ( color1[ j ] * ( 1.0 - r ) + color2[ j ] * ( r ) ) * multiplier ) ]

        self.colors = newColors       

                    
######################################################################
class MeshStripPlane( MeshPlaneBase ):
    def __init__( self,
                  width,
                  height,
                  vertexAttribute,
                  colorAttribute,
                  normalAttribute,
                  texCoordAttributes,
                  indexAttribute,
                  vertexColor,
                  winding,
                  flipNormals ):
        MeshPlaneBase.__init__( self,
                                width,
                                height,
                                vertexAttribute,
                                colorAttribute,
                                normalAttribute,
                                texCoordAttributes,
                                indexAttribute,
                                vertexColor,
                                flipNormals )
        self.triangleCount      = 0
        self.glMode             = 'GL_TRIANGLE_STRIP'

        if winding == MESH_CCW_WINDING:
            for x in range( width - 1 ):
                if x % 2 == 0:		# climp up the ladder
                    self.indices = self.indices[ : -1 ]
				
                    for y in range( height ):
                        i0 = y * width + x
                        i1 = y * width + x + 1

                        if not _checkRange( indexAttribute.type, i0 ) or \
                           not _checkRange( indexAttribute.type, i1 ):
                            raise 'Vertex index out of range: ' + str( i0 ) + ', ' + str( i1 )

                        self.indices.append( i0 )
                        self.indices.append( i1 )
				   
                        self.triangleCount += 2
                else:				# climb down the ladder
                    self.indices = self.indices[ : -1 ]
                    for y in range( height - 1, -1, -1 ):
                        i0 = y * width + x
                        i1 = y * width + x + 1

                        self.indices.append( i0 )
                        self.indices.append( i1 )
					   
                        self.triangleCount += 2
        elif winding == MESH_CW_WINDING:
            for x in range( width - 1, 0, -1 ):
                if width % 2 == 0:
                    mod = 1
                else:
                    mod = 0
                
                if x % 2 == mod:        # climp up the ladder
                    self.indices = self.indices[ : -1 ]
				
                    for y in range( height ):
                        i0 = y * width + x
                        i1 = y * width + x - 1

                        if not _checkRange( indexAttribute.type, i0 ) or \
                           not _checkRange( indexAttribute.type, i1 ):
                            raise 'Vertex index out of range'

                        self.indices.append( i0 )
                        self.indices.append( i1 )
				   
                        self.triangleCount += 2
                else:				# climb down the ladder
                    self.indices = self.indices[ : -1 ]
                    for y in range( height - 1, -1, -1 ):
                        i0 = y * width + x
                        i1 = y * width + x - 1

                        self.indices.append( i0 )
                        self.indices.append( i1 )
					   
                        self.triangleCount += 2
        elif winding == MESH_ALTERNATING_WINDING:
            for x in range( width - 1 ):
                if x % 2 == 0:		# climp up the ladder			
                    for y in range( height ):
                        i0 = y * width + x
                        i1 = y * width + x + 1

                        if not _checkRange( indexAttribute.type, i0 ) or \
                           not _checkRange( indexAttribute.type, i1 ):
                            raise 'Vertex index out of range'

                        self.indices.append( i0 )
                        self.indices.append( i1 )
				   
                        self.triangleCount += 2
                else:				# climb down the ladder
                    for y in range( height - 1, -1, -1 ):
                        i0 = y * width + x
                        i1 = y * width + x + 1

                        self.indices.append( i0 )
                        self.indices.append( i1 )
					   
                        self.triangleCount += 2
        else:
            raise 'Invalid winding ' + str( winding )

######################################################################
class MeshTrianglePlane( MeshPlaneBase ):
    def __init__( self,
                  width,
                  height,
                  vertexAttribute,
                  colorAttribute,
                  normalAttribute,
                  texCoordAttributes,
                  indexAttribute,
                  vertexColor,
                  winding,
                  flipNormals ):
        MeshPlaneBase.__init__( self,
                                width,
                                height,
                                vertexAttribute,
                                colorAttribute,
                                normalAttribute,
                                texCoordAttributes,
                                indexAttribute,
                                vertexColor,
                                flipNormals )
        self.triangleCount      = 0
        self.glMode             = 'GL_TRIANGLES'

        for y in range( height - 1 ):
            for x in range( width - 1 ):
                i0 = y * width + x
                i1 = y * width + x + 1
                i2 = ( y + 1 ) * width + x
                i3 = ( y + 1 ) * width + x + 1

                if not _checkRange( indexAttribute.type, i0 ) or \
                   not _checkRange( indexAttribute.type, i1 ) or \
                   not _checkRange( indexAttribute.type, i2 ) or \
                   not _checkRange( indexAttribute.type, i3 ) :
                    raise 'Vertex index out of range'
                
                if winding == MESH_CCW_WINDING:
                    self.indices.append( i0 )
                    self.indices.append( i1 )
                    self.indices.append( i3 )
                elif winding == MESH_CW_WINDING:
                    self.indices.append( i0 )
                    self.indices.append( i3 )
                    self.indices.append( i1 )
                elif winding == MESH_ALTERNATING_WINDING:
                    if x % 2 == 0:
                        self.indices.append( i0 )
                        self.indices.append( i1 )
                        self.indices.append( i3 )
                    else:
                        self.indices.append( i0 )
                        self.indices.append( i3 )
                        self.indices.append( i1 )
                else:
                    raise 'Unexpected winding ' + str( winding )

                self.triangleCount += 1

                if winding == MESH_CCW_WINDING:
                    self.indices.append( i0 )
                    self.indices.append( i3 )
                    self.indices.append( i2 )
                elif winding == MESH_CW_WINDING:
                    self.indices.append( i0 )
                    self.indices.append( i2 )
                    self.indices.append( i3 )
                elif winding == MESH_ALTERNATING_WINDING:
                    if x % 2 == 0:
                        self.indices.append( i0 )
                        self.indices.append( i3 )
                        self.indices.append( i2 )
                    else:
                        self.indices.append( i0 )
                        self.indices.append( i2 )
                        self.indices.append( i3 )
                
                self.triangleCount += 1

######################################################################
def meshLeftToRightGradientColors( mesh, color1, color2 ):
    # Create a left-to-right vertex color gradient from color1 to
    # color2
    colormultiplier = 1.0
    if mesh.colorType == MESH_TYPE_UNSIGNED_BYTE:
        colormultiplier = float( MESH_MAX_UNSIGNED_BYTE )

    for h in range( mesh.height ):
        for w in range( mesh.width ):
            c = ( w + 0.5 ) / float( mesh.width )
            index = ( h * mesh.width + w ) * 4

            mesh.colors[ index + 0 ] = c * color2[ 0 ] + ( 1.0 - c ) * color1[ 0 ]
            mesh.colors[ index + 1 ] = c * color2[ 1 ] + ( 1.0 - c ) * color1[ 1 ]
            mesh.colors[ index + 2 ] = c * color2[ 2 ] + ( 1.0 - c ) * color1[ 2 ]
            mesh.colors[ index + 3 ] = c * color2[ 3 ] + ( 1.0 - c ) * color1[ 3 ]

            if colormultiplier != 1.0:
                for i in range( 4 ):
                    assert mesh.colors[ index + i ] >= 0.0 and mesh.colors[ index + i ] <= 1.0
                    mesh.colors[ index + i ] = round( mesh.colors[ index + i ] * colormultiplier )
          
    return mesh

######################################################################
def _normalize( x, y, z, w ):
    len = math.sqrt( x * x + y * y + z * z + w * w )
    
    nx = x / len
    ny = y / len
    nz = z / len
    nw = w / len
    
    return ( nx, ny, nz, nw, )

######################################################################
def meshSpherePerturbNormals( mesh, center, radius ):

    if not mesh.normals:
        raise 'Undefined normals'
    
    for y in range( mesh.height ):
        for x in range( mesh.width ):
            index    = ( y * mesh.width + x ) * 3

            # Calculate distance from the center of the perturbance
            dx       = center[ 0 ] - x / float( mesh.width )
            dy       = center[ 1 ] - y / float( mesh.height )
            distance = math.sqrt( dx * dx + dy * dy )

            if distance > radius:
                continue
           
            dz = math.sin( math.acos( distance / radius ) ) * radius

            # Normalize
            ( nx, ny, nz, nw, ) = _normalize( dx, dy, dz, 0 )
            mesh.normals[ index + 0 ] = nx
            mesh.normals[ index + 1 ] = ny
            mesh.normals[ index + 2 ] = nz

######################################################################
def meshRandomPerturbNormals( mesh,
                              ranges = [ [ -0.5, 0.5 ], [ -0.5, 0.5 ] ],
                              seed   = None ):

    if not mesh.normals:
        raise 'Undefined normals'

    random.seed( seed )
    
    for y in range( mesh.height ):
        for x in range( mesh.width ):
            index    = ( y * mesh.width + x ) * 3

            # Random perturb
            dx       = random.randrange( ranges[ 0 ][ 0 ], ranges[ 0 ][ 1 ] )
            dy       = random.randrange( ranges[ 0 ][ 0 ], ranges[ 0 ][ 1 ] )
            dz       = mesh.normals[ index + 2 ]

            # Normalize
            ( nx, ny, nz, nw, ) = _normalize( dx, dy, dz, 0 )
            mesh.normals[ index + 0 ] = nx
            mesh.normals[ index + 1 ] = ny
            mesh.normals[ index + 2 ] = nz

######################################################################            
class Object():
    def __init__( self ):
        self.vertices    = []
        self.colors      = []
        self.normals     = []
        self.texCoords   = []
        self.indices     = []
        self.indexGLType = None
        
######################################################################
class Cube( Object ):
    def __init__( self, side ):
        Object.__init__( self )
        self.vertices = self._vertices( side )
        self.colors   = self._colors()
        self.texCoords.append( self._texCoords() )                
        self.indices  = self._indices()
        self.drawType = 'GL_TRIANGLES'
        
    def _vertices( self, side ):
        return ( [
                # Front
                -side, -side,           0.0,   -side,  side,          0.0,     side, -side,          0.0,
                 side, -side,           0.0,    side,  side,          0.0,    -side,  side,          0.0,
                 
                 # Back
                -side, -side, ( 2 * side ),   -side,  side, ( 2 * side ),     side, -side, ( 2 * side ),
                 side, -side, ( 2 * side ),    side,  side, ( 2 * side ),    -side,  side, ( 2 * side ),
                 
                 # Left
                -side, -side, 0.0,            -side,  side,         0.0,     -side, -side, ( 2 * side ),
                 -side, -side, ( 2 * side ),   -side,  side,( 2 * side ),     -side,  side,          0.0,
                 
                 # Right
                 side, -side,          0.0,     side,  side,          0.0,     side, -side, ( 2 * side ),
                 side, -side, ( 2 * side ),     side,  side, ( 2 * side ),     side,  side,           0.0,
                 
                 # Top
                -side, side,          0.0,    -side,  side, ( 2 * side ),     side,  side,          0.0,
                 side,  side,          0.0,     side,  side, ( 2 * side ),    -side,  side, ( 2 * side ),
                                
                 # Bottom
                -side, -side,         0.0,    -side, -side, ( 2 * side ),     side, -side,          0.0,
                 side, -side,         0.0,     side, -side, ( 2 * side ),    -side, -side, ( 2 * side ),
                ] );

    def _colors( self ):
        return ( [
                # Front
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                
                # Back
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                
                # Left
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                
                # Right
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                
                # Top
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                
                # Bottom
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                255, 255, 255, 255,   255, 255, 255, 255,   255, 255, 255, 255,
                ] );
    
    def _texCoords( self ):
        return ( [
                # Front
                0.0, 0.0,   0.0, 1.0,   1.0, 0.0,
                1.0, 0.0,   1.0, 1.0,   0.0, 1.0,
                
                # Back
                0.0, 0.0,   0.0, 1.0,   1.0, 0.0,
                1.0, 0.0,   1.0, 1.0,   0.0, 1.0,
                
                # Left
                0.0, 0.0,   0.0, 1.0,   1.0, 0.0,
                1.0, 0.0,   1.0, 1.0,   0.0, 1.0,
                
                # Right
                0.0, 0.0,   0.0, 1.0,   1.0, 0.0,
                1.0, 0.0,   1.0, 1.0,   0.0, 1.0,
                
                # Top
                0.0, 0.0,   0.0, 1.0,   1.0, 0.0,
                1.0, 0.0,   1.0, 1.0,   0.0, 1.0,
                
                # Bottom
                0.0, 0.0,   0.0, 1.0,   1.0, 0.0,
                1.0, 0.0,   1.0, 1.0,   0.0, 1.0,
                ] );

    def _indices( self ):
        elements = 36
        indices  = []
    
        for i in range( elements ):
            indices.extend( [ i ] )

        return indices

######################################################################    
def _normalize2( vertices ):
    newVertices = []

    for v in range( 0, len( vertices ), 3 ):
        rx = vertices[ v + 0 ]
        ry = vertices[ v + 1 ]
        rz = vertices[ v + 2 ]
                        
        mag = rx * rx + ry * ry + rz * rz
    
        if mag != 0.0:
            mag = 1.0 / math.sqrt( mag )
            rx = rx * mag
            ry = ry * mag
            rz = rz * mag
    
            newVertices.extend( [ rx, ry, rz ] )
                
    return newVertices

######################################################################
class Sphere( Object ):
    def __init__( self, long, lat, rad ):
        Object.__init__( self )
        self.vertices = self._vertices( long, lat, rad )
        self.normals  = _normalize2( self.vertices )
        self.indices  = self._indices( long, lat, rad )
        self.drawType = 'GL_TRIANGLES'
    
    def _vertices( self, long, lat, rad ):
        vertices = []

        for i in range( lat ):
            la = i * 180.0 / lat / RAD
            for j in range( long ):
                lo = j * 360.0 / long / RAD
                x = rad * math.sin( la ) * math.cos( lo )
                z = rad * math.sin( la ) * math.sin( lo )
                y = rad * math.cos( la )
                vertices.extend( [ x, y, z ] )
            
        return vertices

    def _indices( self, long, lat, rad ):
        indices = []         
        quadCount = ( long - 1 ) * lat
    
        for i in range( quadCount ):
            base = i / lat * long
            v1   = i
            v2   = base + ( v1 + 1 ) % long
            v3   = i + long
            v4   = base + long + ( v3 + 1 ) % long
            indices.extend( [v1, v2, v3, v2, v4, v3] )
            
        return indices

    def _texCoords( self, long, lat, rad, tw, th ):
        textures = []   

        if  tw >= th:
            texturex = tw / th
            texturey = 1.0
        else:
            texturey = th / tw
            texturex = 1.0
    
        for i in range( lat ):
            la = i * 180.0 / lat / RAD
            for j in range( long ):
                lo = j * 360.0 / long / RAD
                x = rad * sin( la ) * cos( lo )
                z = rad * sin( la ) * sin( lo )
                y = rad * cos( la )

                v = acos( y / radius ) / pi
                u = ( math.acos( x / rad * math.sin( v * math.pi ) ) ) / ( 2 * math.pi )           
                textures.extend( [ -u * texturex ,-v * texturey ] )
    
        return textures

    
# #################################################################################
# ############# vertex arrays for cone ############################################
# def DefineConeVertices( x, y, triangles ):

#     # Counting rotation angle
#     angle = 360.0 / triangles
    
#     vertices = []
#     indices = []

#     # Setting coordinates for the first triangle
#     x0 = y0 = w0 = y1 = w1 = x2 = w2 = 0.0
#     z0 = z2 = 0.5
#     y2 = y

#     CurrentAngle = 0
#     NewX = 0
#     NewZ = 0
#     for i in range(triangles):

#         ## Storing previous values before counting the new values
#         PreviousX = NewX
#         PreviousZ = NewZ

#         ## Counting new values by rotating around y-axis
#         NewX = cos(CurrentAngle)* x;
#         NewZ = sin(CurrentAngle)* x;

#         CurrentAngle += angle

#         #### adding new vertice ##########################
#         vertices.extend( [x0,y0,y1,NewX,y1,NewZ,x2,y2,z2] )
#         #### adding vertice based on previous vertice to close the caps on the bottom
#         vertices.extend( [PreviousX,y0,PreviousZ,NewX,y0,NewZ,x0,y0,z0] )
#         ### adding vertice based on previus vertice to close the caps on the side
#         vertices.extend( [PreviousX,y0,PreviousZ,NewX,y0,NewZ,x0,y2,z0] )
        
#     for i in range( triangles * 3 ):
#         indices.extend( [i] )
        
#     return vertices, indices

# ###########################################################################
# ############## Texture coordinates for cone ###############################
# def DefineConeTexcoords(x, y, triangles):

  
#     # Counting rotation angle
#     angle = 360.0 / triangles
    
#     textures = []

#     CurrentAngle = 0
#     NewX = 0
#     NewZ = 0
    
#     for i in range(triangles):

#         ## Storing previous values before counting the new values
#         PreviousX = NewX
#         PreviousZ = NewZ

#         CurrentAngle += angle

#         ## Counting new values by rotating around y-axis
#         NewX = cos(CurrentAngle)* x
#         NewZ = sin(CurrentAngle)* x
#         ## These does not matter, since it is inside the cone
#         textures.extend( [0.0,0.0,0.0,0.0,0.0,0.0] )
#         ## These does not matter, since it is the bottom of the cone
#         textures.extend( [0.0,0.0,0.0,0.0,0.0,0.0] )
#         ## These matters, since these are the sides of the cone
#         textures.extend( [PreviousX,0.0,NewX,0.0,0.0,1.0] )
  
#     return textures

# #################################################################################
# ############## Defining colors ##################################################
# def DefineColors(Count, InputColors):

#     ## returning Count length list of colors
#     Colors = []

#     for i in range( Count ):
#         Colors.extend( InputColors )
              
#     return Colors

# #################################################################################
# ############## Rotated points ###################################################
# def RotatedPoints2d( angle,radius ):
#     ## Function rotates the point 2d and returns the coordinate

#     x = cos( angle ) * radius
#     y = sin( angle ) * radius
    
#     return x,y
    
# #################################################################################
# ############## Randomcolors #####################################################
# def RandomColors():

#     r = random.uniform(0.0,1.0)
#     g = random.uniform(0.0,1.0)
#     b = random.uniform(0.0,1.0)
#     a = random.uniform(0.0,1.0)

#     return [r, g, b, a, r, g, b, a, r, g, b, a]

# ################################################################################
# ############# Projection matrice ###############################################
# def ProjectionMatrice( dY, dZ ):

#     matProj = [0] * 16
#     matProj[0] = 1.0
#     matProj[5] = dY
#     matProj[10] = - dZ
#     matProj[11] = - dZ
#     matProj[14] = -1
    
#     return matProj

# ################################################################################
# ############## Model View Matrice###############################################
# def ModelViewMatrice( fAngle ):

#     fAngle = fAngle /  RAD
#     # Rotate and Translate the model view matrix
#     matModelView = [0] * 16
#     matModelView[ 0] = +cos( fAngle )
#     matModelView[ 2] = +sin( fAngle )
#     matModelView[ 5] = 1.0
#     matModelView[ 8] = -sin( fAngle )
#     matModelView[10] = +cos( fAngle )
#     matModelView[14] = -5.0
#     matModelView[15] = 1.0

#     return matModelView

# ################################################################################

            
