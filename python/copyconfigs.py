#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, os

if __name__ == '__main__':
    usage = "usage: %prog [options] <config files> <output folder>"
    parser = optparse.OptionParser( usage )
   
    ( options, args ) = parser.parse_args()

    if len( args ) < 2:
        parser.error( 'incorrect number of arguments' )
        sys.exit( -1 )

    configFiles  = args[ : -1 ]
    outputFolder = args[ -1 ]

    for cf in configFiles:
        inputFile = open(  cf, 'r' )
        content = inputFile.read()
        inputFile.close()
        i = content.find( 'SetModuleName' )
        if i < 0:
            raise 'Invalid config file: ' + cf
        j = content.find( ')', i )
        moduleName = content[ i : j ].split( "'" )[ 1 ].lower()
        outputFile = file( outputFolder + '/' + moduleName + '_config.py', 'wb' )
        outputFile.write( content )
        outputFile.close()

    
