#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, shutil
from   vtclitool      import *
import tga2png
from   sct            import bmbuilder
from   sct.util       import textutil
from   sct.visualtest import image, report

######################################################################
class TestCase:
    def __init__( self, name, description = '', status = 'UNDEFINED', priority = 'UNDEFINED' ):
        self.name        = name
        self.description = description
        self.status      = status

    def __str__( self ):
        return '%-50s Status: %10s' % ( self.name, self.status, )

def createTestCases( suite ):
    testCases = []
    for benchmark in suite.benchmarks:
        testCases.append( TestCase( benchmark[ 0 ].name, description = benchmark[ 0 ].__doc__ ) )
    return testCases

class VisualTestCase( bmbuilder.Benchmark ):
    def __init__( self ):
        bmbuilder.Benchmark.__init__( self )
        self.repeats = 1

def permutate( stack, rest ):
    if not rest:
        yield stack
        return
    attr   = rest.keys()[ 0 ]
    values = rest[ attr ]
    rest.pop( attr )
    if isinstance( attr, str ):
        names = ( attr, )
    else:
        names = attr
    for value in values:
        if len( names ) > 1:
            for name, value in zip( names, value ):
                stack[ name ] = value
        else:
            stack[ names[ 0 ] ] = value
        for x in permutate( stack, rest ):
            yield x
    for name in names:
        stack.pop( name )
    rest[ attr ] = values
        
class VisualTestSuite( bmbuilder.Suite ):
    def addTestCase( self, testCaseClass, features = None ):
        if hasattr( testCaseClass, 'testValues' ):
            for kwargs in permutate( {}, testCaseClass.testValues ):
                self.addBenchmark( testCaseClass( **kwargs ), features )
        else:
            self.addBenchmark( testCaseClass(), features )

######################################################################            
# Command handling
commandConfig.update( { 'compare'  : '<candidate image path> <reference image path>',
                        'fetch'    : '<path> <target>',
                        'generate' : '<target>',
                        } )

def handleCompareCommand( suite, options, candidateImagePath, referenceImagePath ):
    testCases = createTestCases( suite )
    comparer  = image.getImageComparer( 'exact' )

    candidateImageLoader = image.TestCaseImageLoader( candidateImagePath )
    referenceImageLoader = image.TestCaseImageLoader( referenceImagePath )

    report.createImageComparisonReport( options.reportName,
                                        testCases,
                                        referenceImageLoader,
                                        candidateImageLoader,
                                        comparer,
                                        options.summary,
                                        options.testsPerPage )
    
def handleFetchCommand( suite, options, path, target ):
    srcImagePath = os.path.join( path )
    dstImagePath = os.path.join( 'images', target )
    
    if not os.path.exists( srcImagePath ):
        console.exitWithError( 'Folder %s does not exist' % srcImagePath )
        
    if os.path.exists( dstImagePath ):
        if not console.confirm( 'ALL the images at %s will DELETED and images from %s will be copied there, OK?' % ( dstImagePath, srcImagePath ) ):
            console.exitWithError( 'Stopped by user request' )
            
        shutil.rmtree( dstImagePath )
        
    os.makedirs( dstImagePath )
    tga2png.tga2png( srcImagePath, dstImagePath )
        
def handleGenerateCommand( suite, options, target ):
    sys.stdout.write( suite.generateSpandexInput( target ) )

class VTCLI( CLI ):
    def __init__( self ):
        CLI.__init__( self, VisualTestSuite() )

        self.parser.add_option( '-c', '--compare',
                                action  = 'store_true',
                                dest    = 'compare',
                                default = False,
                                help    = 'produce comparison report when running two targets' )
        self.parser.add_option( '-r', '--report',
                                action  = 'store_true',
                                dest    = 'report',
                                default = False,
                                help    = 'generate report' )
        self.parser.add_option( '--report-name',
                                dest    = 'reportName',
                                type    = 'string',
                                default = 'report',
                                help    = 'name of the report' )
        self.parser.add_option( '--report-tests-per-page',
                                dest    = 'testsPerPage',
                                default = 500,
                                help    = 'tests per report page' )
        self.parser.add_option( '-v', '--verbose',
                                action  = 'store_true',
                                dest    = 'verbose',
                                default = False,
                                help    = 'print verbose progress information' )
        self.parser.add_option( '', '--separate',
                                action  = 'store_true',
                                dest    = 'separate',
                                default = False,
                                help    = 'write benchmarks to separate files' )
        self.parser.add_option( '', '--summary',
                                action  = 'store_true',
                                dest    = 'summary',
                                default = False,
                                help    = 'Create summary without images' )
        
def init():
    return VTCLI().run()

