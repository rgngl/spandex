#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, optparse, os

UPLOAD_BENCHMARK         = ( 'OPENGLES2 tex data upload bytes=4194304 (1024x1024 RGBA8 FLOWER)', 4194304, )
UI_BENCHMARK             = ( 'OPENGLES2 ui 1080x1920 fbo draw b(RGBA8)+p(RGBA8)+g(RGBA8)+i(RGBA8)+o(RGBA8)', 1080*1920*5, )
FILL_BENCHMARK           = ( 'OPENGLES2 tex fillrate (1080x1920 fbo transparent RGBA8 FLOWER 1 smpl 42 ovrdrw nearest)', 1080*1920*42, )
TRANSFORMATION_BENCHMARK = ( 'OPENGLES2 tri throughput (480x640 fbo 757760 downscaled tris 48x32x256 RGBA8)', 757760, )
BLUR_BENCHMARK           = ( 'OPENGLES2 blur (720x1280 RGBA8 FLOWER 2 iterations nearest)', 1, )
PULS_BENCHMARK           = ( 'OPENGLES2 modified puls (480x640 fbo)', 1, )

# ------------------------------------------------------------
# tableSeparator
# ------------------------------------------------------------
def tableSeparator( format ):
    return {
        "twiki" : "|",
        "trac"  : "||",
    }[ format ]

# ------------------------------------------------------------
# getMetric
# ------------------------------------------------------------
def getMetric( value ):
    kb = 1024
    mb = kb * kb
    gb = kb * mb
    if value > gb * 10:
        return '%.1f G' % ( value / float( gb ), )
    elif value > mb * 10:
        return '%.1f M' % ( value / float( mb ), )
    elif value > kb * 10:
        return '%1.f K' % ( value / float( kb ), )
    else:
        if float( value ) == int( value ):
            return '%d ' % ( value, )
        else:
            return '%.1f ' % ( value, )

# ------------------------------------------------------------
# benchmarkCaps 
# ------------------------------------------------------------
def benchmarkCaps( fps ):
    if fps < 59 or fps > 61:
        return False
    return True

# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] <ui2 benchmark result file>"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--verbose',
                       action  = 'store_true',
                       dest    = 'verbose',
                       default = False,
                       help    = 'Verbose results.' )
    parser.add_option( '--format',
                       default = 'twiki',
                       dest    = 'format',
                       help    = 'Wiki markup format to use: twiki, trac [default: %default].' )

    ( options, args ) = parser.parse_args()

    # Validate command line parameters.
    if len( args ) != 1:
        parser.error( 'Incorrect number of arguments' )
        sys.exit( -1 )

    # Open log file.
    resultFile = None
    try:
        resultFile = file( args[ 0 ], 'r' )
    except:
        parser.error( 'Failed to open result file: %s' % args[ 0 ] );
        sys.exit( -1 )

    sep = tableSeparator( options.format )

    lines = resultFile.readlines()
    resultFile.close()

    upload   = 0
    ui       = 0
    uiCaps   = False
    fill     = 0
    fillCaps = False
    tri      = 0
    triCaps  = False
    blur     = 0
    blurCaps = False
    puls     = 0
    pulsCaps = False

    for i in range( len( lines ) ):
        if lines[ i ].endswith( 'BENCHMARK]\n' ):
            name    = lines[ i ].split( ':' )[ 0 ][ 1: ]
            repeats = float( lines[ i + 4 ].split( ':' )[ 1 ] ) # Repeats : N
            if repeats > 0:
                s = lines[ i + 5 ].split( ':' )                 # Times : M
                if len( s ) < 2 or not s[ 1 ].strip() or float( s[ 1 ] ) == 0:
                    continue
                fps = repeats / float( s[ 1 ] )

            if name == UPLOAD_BENCHMARK[ 0 ]:
                upload  = UPLOAD_BENCHMARK[ 1 ] * fps
            elif name == UI_BENCHMARK[ 0 ]:
                ui      = UI_BENCHMARK[ 1 ] * fps
                uiCaps  = benchmarkCaps( fps )
            elif name == FILL_BENCHMARK[ 0 ]:
                fill     = FILL_BENCHMARK[ 1 ] * fps
                fillCaps = benchmarkCaps( fps )
            elif name == TRANSFORMATION_BENCHMARK[ 0 ]:
                tri      = TRANSFORMATION_BENCHMARK[ 1 ] * fps
                triCaps  = benchmarkCaps( fps )
            elif name == BLUR_BENCHMARK[ 0 ]:
                blur     = BLUR_BENCHMARK[ 1 ] * fps
                blurCaps = benchmarkCaps( fps )
            elif name == PULS_BENCHMARK[ 0 ]:
                puls     = PULS_BENCHMARK[ 1 ] * fps
                pulsCaps = benchmarkCaps( fps )

    print sep + ' *Name*          ' + sep + ' *Value*     ' + sep + ' *Source*  ' + sep
    print sep + ' Upload          ' + sep + ' %sbytes/s   ' % getMetric( upload ) + sep + ' ' + UPLOAD_BENCHMARK[ 0 ] + '  ' + sep
    print sep + ' Ui              ' + sep + ' %stex/s %s  ' % ( getMetric( ui ), { False : '', True : '+' }[ uiCaps ], ) + sep + ' ' + FILL_BENCHMARK[ 0 ] + '  ' + sep
    print sep + ' Fill            ' + sep + ' %stex/s %s  ' % ( getMetric( fill ), { False : '', True : '+' }[ fillCaps ], ) + sep + ' ' + FILL_BENCHMARK[ 0 ] + '  ' + sep
    print sep + ' Transformation  ' + sep + ' %stri/s %s  ' % ( getMetric( tri ), { False : '', True : '+' }[ triCaps ], ) + sep + ' ' + TRANSFORMATION_BENCHMARK[ 0 ] + '  ' + sep
    print sep + ' Blur            ' + sep + ' %sblrs/s %s ' % ( getMetric( blur ), { False : '', True : '+' }[ blurCaps ], ) + sep + ' ' + BLUR_BENCHMARK[ 0 ] + '  ' + sep
    print sep + ' Puls            ' + sep + ' %spuls/s %s ' % ( getMetric( puls ), { False : '', True : '+' }[ pulsCaps ], ) + sep + ' ' + PULS_BENCHMARK[ 0 ] + '  ' + sep

