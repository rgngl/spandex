#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, os, Image, glob, re
from sct.util.fileutil import isNewer

imageSuffix = '_0.tga'

def tga2png( sourceDir, destDir ):
    sourcePaths = glob.glob( os.path.join( sourceDir, '*' + imageSuffix ) )
    if not sourcePaths:
        print 'No images with suffix "%s" found in %s' % (imageSuffix, sourceDir)
    for sourcePath in sourcePaths:
        filename     = os.path.basename( sourcePath )
        testCaseName = re.match( '(.*)' + imageSuffix + '$', filename ).group( 1 )
        filename     = testCaseName + '.png'
        destPath     = os.path.join( destDir, filename )
        # Skip if the destination image exists and the source image has not been
        # modified more recently.
        if os.path.exists( destPath ) and not isNewer( sourcePath, destPath ):
            continue
        image        = Image.open( sourcePath )
        image.save( destPath )
        print 'Saved %s' % destPath

if __name__ == '__main__':
    if len( sys.argv ) not in [2,3]:
        print 'Usage: tga2png <source dir> [<dest dir>]'
        sys.exit( 1 )

    sourceDir = os.path.normpath( sys.argv[1] )
    if not os.path.isdir( sourceDir ):
        print '%s is not a directory' % sourceDir
        sys.exit( 1 )

    if len( sys.argv ) == 3:
        destDir = os.path.normpath( sys.argv[2] )
        if not os.path.isdir( destDir ):
            print 'Creating ' + destDir
            os.makedirs( destDir )
    else:
        destDir = sourceDir

    tga2png( sourceDir, destDir )
