Spandex benchmark and test framework
Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies)
Contact: Kari J. Kangas <kari.j.kangas@nokia.com>

See https://www.gitorious.org/spandex/pages/Home
