#!/bin/sh
#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

if which cygpath > /dev/null 2>&1; then
    _SPANDEX_HOME=`cygpath -m \`pwd\``
    export SPANDEX_HOME=$_SPANDEX_HOME
    _SPANDEX_SYMBIAN_SDK=`cygpath -m \`pwd\` | cut -b1,2`
    export SPANDEX_SYMBIAN_SDK=$_SPANDEX_SYMBIAN_SDK
else
    export SPANDEX_HOME=$PWD
fi
