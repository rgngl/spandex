#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../..' )

import  bd

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'CPU and memory benchmark'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

#----------------------------------------------------------------------
import cpumem

totalMemory   = 16 * 1024 * 1024
blocks        = [ 1, 4, 16, 64, 256, 1024, 4096, 16384, 65536, 262144 ]
cachedBlocks  = [ 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216 ]
alignment     = 32
alignedBlocks = 16384
alignments    = [ 4, 8, 16, 32, 64 ]
sweeps        = [ 1, 8, 16, 32 ]

# ----------
for b in blocks:
    suite.addBenchmark( cpumem.BlockAccessBenchmark( 'sstnble', cpumem.BLOCK_ACCESS_WRITE, b, totalMemory / b, 1, alignment ) )
for b in blocks:
    suite.addBenchmark( cpumem.BlockAccessBenchmark( 'sstnble', cpumem.BLOCK_ACCESS_COPY, b, totalMemory / b, 1, alignment ) )
for b in blocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'sstnble', cpumem.SINGLE_ACCESS_READ, b, totalMemory / b, 4, 1, alignment, False ) )
for b in blocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'sstnble', cpumem.SINGLE_ACCESS_READ, b, totalMemory / b, 4, 1, alignment, True ) )
for b in blocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'sstnble', cpumem.SINGLE_ACCESS_WRITE, b, totalMemory / b, 4, 1, alignment, False ) )
for b in blocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'sstnble', cpumem.SINGLE_ACCESS_WRITE, b, totalMemory / b, 4, 1, alignment, True ) )
for b in blocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'sstnble', cpumem.SINGLE_ACCESS_COPY, b, totalMemory / b, 4, 1, alignment, False ) )
for b in blocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'sstnble', cpumem.SINGLE_ACCESS_COPY, b, totalMemory / b, 4, 1, alignment, True ) )
for b in blocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'sstnble', cpumem.SINGLE_ACCESS_MODIFY, b, totalMemory / b, 4, 1, alignment, False ) )
for b in blocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'sstnble', cpumem.SINGLE_ACCESS_MODIFY, b, totalMemory / b, 4, 1, alignment, True ) )
for b in blocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'sstnble', cpumem.SINGLE_ACCESS_BLEND, b, totalMemory / b, 4, 1, alignment, False ) )
for b in blocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'sstnble', cpumem.SINGLE_ACCESS_BLEND, b, totalMemory / b, 4, 1, alignment, True ) )

# ----------
for b in cachedBlocks:
    suite.addBenchmark( cpumem.BlockAccessBenchmark( 'cched', cpumem.BLOCK_ACCESS_WRITE, 1, b, 1, alignment ) )
for b in cachedBlocks:
    suite.addBenchmark( cpumem.BlockAccessBenchmark( 'cched', cpumem.BLOCK_ACCESS_COPY, 1, b, 1, alignment ) )
for b in cachedBlocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'cched', cpumem.SINGLE_ACCESS_READ, 1, b, 4, 1, alignment, True ) )
for b in cachedBlocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'cched', cpumem.SINGLE_ACCESS_WRITE, 1, b, 4, 1, alignment, True ) )
for b in cachedBlocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'cched', cpumem.SINGLE_ACCESS_COPY, 1, b, 4, 1, alignment, True ) )
for b in cachedBlocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'cched', cpumem.SINGLE_ACCESS_MODIFY, 1, b, 4, 1, alignment, True ) )
for b in cachedBlocks:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'cched', cpumem.SINGLE_ACCESS_BLEND, 1, b, 4, 1, alignment, True ) )

# ----------
for a in alignments:
    suite.addBenchmark( cpumem.BlockAccessBenchmark( 'alngmnt', cpumem.BLOCK_ACCESS_WRITE, alignedBlocks, totalMemory / alignedBlocks, 1, a ) )
for a in alignments:
    suite.addBenchmark( cpumem.BlockAccessBenchmark( 'alngmnt', cpumem.BLOCK_ACCESS_COPY, alignedBlocks, totalMemory / alignedBlocks, 1, a ) )
for a in alignments:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'alngmnt', cpumem.SINGLE_ACCESS_READ, alignedBlocks, totalMemory / alignedBlocks, 4, 1, a, True ) )
for a in alignments:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'alngmnt', cpumem.SINGLE_ACCESS_WRITE, alignedBlocks, totalMemory / alignedBlocks, 4, 1, a, True ) )
for a in alignments:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'alngmnt', cpumem.SINGLE_ACCESS_COPY, alignedBlocks, totalMemory / alignedBlocks, 4, 1, a, True ) )
for a in alignments:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'alngmnt', cpumem.SINGLE_ACCESS_MODIFY, alignedBlocks, totalMemory / alignedBlocks, 4, 1, a, True ) )
for a in alignments:
    suite.addBenchmark( cpumem.SingleAccessBenchmark( 'alngmnt', cpumem.SINGLE_ACCESS_BLEND, alignedBlocks, totalMemory / alignedBlocks, 4, 1, a, True ) )

# ----------
suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_READ, 1, totalMemory, 1, 1, alignment, True ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_READ, 1, totalMemory, 2, 1, alignment, True ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_READ, 1, totalMemory, 4, 1, alignment, True ) )

suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_WRITE, 1, totalMemory, 1, 1, alignment, True ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_WRITE, 1, totalMemory, 2, 1, alignment, True ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_WRITE, 1, totalMemory, 4, 1, alignment, True ) )

suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_COPY, 1, totalMemory, 1, 1, alignment, True ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_COPY, 1, totalMemory, 2, 1, alignment, True ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_COPY, 1, totalMemory, 4, 1, alignment, True ) )

suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_MODIFY, 1, totalMemory, 1, 1, alignment, True ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_MODIFY, 1, totalMemory, 2, 1, alignment, True ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_MODIFY, 1, totalMemory, 4, 1, alignment, True ) )

suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_BLEND, 1, totalMemory, 1, 1, alignment, True ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_BLEND, 1, totalMemory, 2, 1, alignment, True ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( 'elmntsz', cpumem.SINGLE_ACCESS_BLEND, 1, totalMemory, 4, 1, alignment, True ) )

# ----------
for s in sweeps:
    suite.addBenchmark( cpumem.SweepAccessBenchmark( cpumem.SWEEP_ACCESS_READ, s, 1, totalMemory, 4, 1, alignment, True ) )
for s in sweeps:
    suite.addBenchmark( cpumem.SweepAccessBenchmark( cpumem.SWEEP_ACCESS_WRITE, s, 1, totalMemory, 4, 1, alignment, True ) )
for s in sweeps:
    suite.addBenchmark( cpumem.SweepAccessBenchmark( cpumem.SWEEP_ACCESS_COPY, s, 1, totalMemory, 4, 1, alignment, True ) )
for s in sweeps:
    suite.addBenchmark( cpumem.SweepAccessBenchmark( cpumem.SWEEP_ACCESS_MODIFY, s, 1, totalMemory, 4, 1, alignment, True ) )
for s in sweeps:
    suite.addBenchmark( cpumem.SweepAccessBenchmark( cpumem.SWEEP_ACCESS_BLEND, s, 1, totalMemory, 4, 1, alignment, True ) )

#----------------------------------------------------------------------
command()
