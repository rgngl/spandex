#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *

#----------------------------------------------------------------------
def boolToStr( b ):
    if b:
        return 'Yes'
    else:
        return 'No'

#----------------------------------------------------------------------
def memFormat( bytes ):
    return 'mbbytes=%d' % ( bytes, )

SINGLE_ACCESS_READ   = 'READ'
SINGLE_ACCESS_WRITE  = 'FILLZ'
SINGLE_ACCESS_COPY   = 'COPY'
SINGLE_ACCESS_MODIFY = 'MODIFY'
SINGLE_ACCESS_BLEND  = 'BLEND'

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SingleAccessBenchmark( Benchmark ):
    def __init__( self, id, operation, blockCount, blockSize, elementSize, iterations, align, loopUnroll ):
        Benchmark.__init__( self )
        self.id          = id
        self.operation   = operation
        self.blockCount  = blockCount
        self.blockSize   = blockSize
        self.elementSize = elementSize
        self.iterations  = iterations
        self.align       = align
        self.loopUnroll  = loopUnroll
        self.bytes       = self.blockCount * self.blockSize * self.iterations

        if self.operation == SINGLE_ACCESS_READ:
            if not self.loopUnroll:
                self.name = "MEM %s sngle accss read ansi c %s bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                                           memFormat( self.bytes ),
                                                                                                           self.blockCount,
                                                                                                           self.blockSize,
                                                                                                           self.elementSize,
                                                                                                           self.iterations,
                                                                                                           self.align, )
            else:
                self.name = "MEM %s sngle accss read ansi c (unrlled) %s bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                                                     memFormat( self.bytes ),
                                                                                                                     self.blockCount,
                                                                                                                     self.blockSize,
                                                                                                                     self.elementSize,
                                                                                                                     self.iterations,
                                                                                                                     self.align, )
        elif self.operation == SINGLE_ACCESS_WRITE:
            if not self.loopUnroll:
                self.name = "MEM %s sngle accss write ansi c %s bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                                            memFormat( self.bytes ),
                                                                                                            self.blockCount,
                                                                                                            self.blockSize,
                                                                                                            self.elementSize,
                                                                                                            self.iterations,
                                                                                                            self.align, )
            else:
                self.name = "MEM %s sngle accss write ansi c (unrlled) %s bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                                                      memFormat( self.bytes ),
                                                                                                                      self.blockCount,
                                                                                                                      self.blockSize,
                                                                                                                      self.elementSize,
                                                                                                                      self.iterations,
                                                                                                                      self.align, )
        elif self.operation == SINGLE_ACCESS_COPY:
            if not self.loopUnroll:
                self.name = "MEM %s sngle accss copy ansi c %s bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                                           memFormat( self.bytes ),
                                                                                                           self.blockCount,
                                                                                                           self.blockSize,
                                                                                                           self.elementSize,
                                                                                                           self.iterations,
                                                                                                           self.align, )
            else:
                self.name = "MEM %s sngle accss copy ansi c (unrlled) %s bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                                                     memFormat( self.bytes ),
                                                                                                                     self.blockCount,
                                                                                                                     self.blockSize,
                                                                                                                     self.elementSize,
                                                                                                                     self.iterations,
                                                                                                                     self.align, )
        elif self.operation == SINGLE_ACCESS_MODIFY:
            if not self.loopUnroll:
                self.name = "MEM %s sngle accss modify ansi c %s bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                                             memFormat( self.bytes ),
                                                                                                             self.blockCount,
                                                                                                             self.blockSize,
                                                                                                             self.elementSize,
                                                                                                             self.iterations,
                                                                                                             self.align, )
            else:
                self.name = "MEM %s sngle accss modify ansi c (unrlled) %s bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                                                       memFormat( self.bytes ),
                                                                                                                       self.blockCount,
                                                                                                                       self.blockSize,
                                                                                                                       self.elementSize,
                                                                                                                       self.iterations,
                                                                                                                       self.align, )
        elif self.operation == SINGLE_ACCESS_BLEND:
            if not self.loopUnroll:
                self.name = "MEM %s sngle accss blend ansi c %s bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                                            memFormat( self.bytes ),
                                                                                                            self.blockCount,
                                                                                                            self.blockSize,
                                                                                                            self.elementSize,
                                                                                                            self.iterations,
                                                                                                            self.align, )
            else:
                self.name = "MEM %s sngle accss blend ansi c (unrlled) %s bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                                                      memFormat( self.bytes ),
                                                                                                                      self.blockCount,
                                                                                                                      self.blockSize,
                                                                                                                      self.elementSize,
                                                                                                                      self.iterations,
                                                                                                                      self.align, )
        else:
            raise 'Unexpected operation %s' % str( self.operation )

    def build( self, target, modules ):
        Memory = modules[ 'Memory' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Memory.Single( self.blockCount,
                       self.blockSize,
                       self.elementSize,
                       self.iterations,
                       self.align,
                       self.operation,
                       boolToSpandex( self.loopUnroll ) )


BLOCK_ACCESS_WRITE = 'FILLZ'
BLOCK_ACCESS_COPY  = 'COPY'

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class BlockAccessBenchmark( Benchmark ):
    def __init__( self, id, operation, blockCount, blockSize, iterations, align ):
        Benchmark.__init__( self )
        self.id          = id
        self.operation   = operation
        self.blockCount  = blockCount
        self.blockSize   = blockSize
        self.iterations  = iterations
        self.align       = align
        self.bytes       = self.blockCount * self.blockSize * self.iterations

        if self.operation == BLOCK_ACCESS_WRITE:
            self.name = "MEM %s blck accss write syslib %s bc=%d bsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                              memFormat( self.bytes ),
                                                                                              self.blockCount,
                                                                                              self.blockSize,
                                                                                              self.iterations,
                                                                                              self.align, )
        elif self.operation == BLOCK_ACCESS_COPY:
            self.name = "MEM %s blck accss copy syslib %s bc=%d bsz=%d itrns=%d algn=%d" % ( self.id,
                                                                                             memFormat( self.bytes ),
                                                                                             self.blockCount,
                                                                                             self.blockSize,
                                                                                             self.iterations,
                                                                                             self.align, )
        else:
            raise 'Unexpected operation %s' % str( self.operation )


    def build( self, target, modules ):
        Memory = modules[ 'Memory' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Memory.Block( self.blockCount,
                      self.blockSize,
                      self.iterations,
                      self.align,
                      self.operation )


SWEEP_ACCESS_READ   = 'READ'
SWEEP_ACCESS_WRITE  = 'FILLZ'
SWEEP_ACCESS_COPY   = 'COPY'
SWEEP_ACCESS_MODIFY = 'MODIFY'
SWEEP_ACCESS_BLEND  = 'BLEND'

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SweepAccessBenchmark( Benchmark ):
    def __init__( self, operation, interval, blockCount, blockSize, elementSize, iterations, align, loopUnroll ):
        Benchmark.__init__( self )
        self.operation   = operation
        self.interval    = interval
        self.blockCount  = blockCount
        self.blockSize   = blockSize
        self.elementSize = elementSize
        self.iterations  = iterations
        self.align       = align
        self.loopUnroll  = loopUnroll
        self.bytes       = self.blockCount * self.blockSize * self.iterations

        if self.operation == SINGLE_ACCESS_READ:
            if not self.loopUnroll:
                self.name = "MEM sweep accss read ansi c %s intrvl=%d bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( memFormat( self.bytes ),
                                                                                                                  self.interval,
                                                                                                                  self.blockCount,
                                                                                                                  self.blockSize,
                                                                                                                  self.elementSize,
                                                                                                                  self.iterations,
                                                                                                                  self.align, )
            else:
                self.name = "MEM sweep accss read ansi c (unrlled) %s intrvl=%d bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( memFormat( self.bytes ),
                                                                                                                            self.interval,
                                                                                                                            self.blockCount,
                                                                                                                            self.blockSize,
                                                                                                                            self.elementSize,
                                                                                                                            self.iterations,
                                                                                                                            self.align, )
        elif self.operation == SINGLE_ACCESS_WRITE:
            if not self.loopUnroll:
                self.name = "MEM sweep accss write ansi c %s intrvl=%d bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( memFormat( self.bytes ),
                                                                                                                   self.interval,
                                                                                                                   self.blockCount,
                                                                                                                   self.blockSize,
                                                                                                                   self.elementSize,
                                                                                                                   self.iterations,
                                                                                                                   self.align, )
            else:
                self.name = "MEM sweep accss write ansi c (unrlled) %s intrvl=%d bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( memFormat( self.bytes ),
                                                                                                                             self.interval,
                                                                                                                             self.blockCount,
                                                                                                                             self.blockSize,
                                                                                                                             self.elementSize,
                                                                                                                             self.iterations,
                                                                                                                             self.align, )
        elif self.operation == SINGLE_ACCESS_COPY:
            if not self.loopUnroll:
                self.name = "MEM sweep accss copy ansi c %s intrvl=%d bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( memFormat( self.bytes ),
                                                                                                                  self.interval,
                                                                                                                  self.blockCount,
                                                                                                                  self.blockSize,
                                                                                                                  self.elementSize,
                                                                                                                  self.iterations,
                                                                                                                  self.align, )
            else:
                self.name = "MEM sweep accss copy ansi c (unrlled) %s intrvl=%d bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( memFormat( self.bytes ),
                                                                                                                            self.interval,
                                                                                                                            self.blockCount,
                                                                                                                            self.blockSize,
                                                                                                                            self.elementSize,
                                                                                                                            self.iterations,
                                                                                                                            self.align, )
        elif self.operation == SINGLE_ACCESS_MODIFY:
            if not self.loopUnroll:
                self.name = "MEM sweep accss modify ansi c %s intrvl=%d bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( memFormat( self.bytes ),
                                                                                                                    self.interval,
                                                                                                                    self.blockCount,
                                                                                                                    self.blockSize,
                                                                                                                    self.elementSize,
                                                                                                                    self.iterations,
                                                                                                                    self.align, )
            else:
                self.name = "MEM sweep accss modify ansi c (unrlled) %s intrvl=%d bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( memFormat( self.bytes ),
                                                                                                                              self.interval,
                                                                                                                              self.blockCount,
                                                                                                                              self.blockSize,
                                                                                                                              self.elementSize,
                                                                                                                              self.iterations,
                                                                                                                              self.align, )
        elif self.operation == SINGLE_ACCESS_BLEND:
            if not self.loopUnroll:
                self.name = "MEM sweep accss blend ansi c %s intrvl=%d bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( memFormat( self.bytes ),
                                                                                                                   self.interval,
                                                                                                                   self.blockCount,
                                                                                                                   self.blockSize,
                                                                                                                   self.elementSize,
                                                                                                                   self.iterations,
                                                                                                                   self.align, )
            else:
                self.name = "MEM sweep accss blend ansi c (unrlled) %s intrvl=%d bc=%d bsz=%d elmsz=%d itrns=%d algn=%d" % ( memFormat( self.bytes ),
                                                                                                                             self.interval,
                                                                                                                             self.blockCount,
                                                                                                                             self.blockSize,
                                                                                                                             self.elementSize,
                                                                                                                             self.iterations,
                                                                                                                             self.align, )
        else:
            raise 'Unexpected operation %s' % str( self.operation )

    def build( self, target, modules ):
        Memory = modules[ 'Memory' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Memory.Sweep( self.interval,
                      self.blockCount,
                      self.blockSize,
                      self.elementSize,
                      self.iterations,
                      self.align,
                      self.operation,
                      boolToSpandex( self.loopUnroll ) )
