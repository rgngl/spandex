#
# Spandex benchmark and test framework.
#
# Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

from lib.common import  *
from lib.mesh   import  *

import struct
import math

#----------------------------------------------------------------------
BACKGROUND              = 0
PANELS                  = 1
GROUPS                  = 2
ICONS                   = 3
OVERLAY                 = 4
OVERLAY4X               = 5
 
UPLOAD                  = 4
DRAW                    = 5
UPLOAD_AND_DRAW         = 6

PANELSX                 = 2
PANELSY                 = 1
GROUPSX                 = 2
GROUPSY                 = 3
ICONSX                  = 4
ICONSY                  = 6

#----------------------------------------------------------------------
def setupOpenGLES2( modules, indexTracker, target, attributes = {} ):
    a = target.getDefaultAttributes( API_OPENGLES2 )
    if a:
        a[ CONFIG_DEPTHSIZE ] = '>=0'
    
    for k in attributes.keys():
        a[ k ] = attributes[ k ]

    return target.initialize( modules, indexTracker, API_OPENGLES2, a )

#----------------------------------------------------------------------
def stripTriangleCount( gridx, gridy, winding ):
    if gridx < 2 or gridy < 2:
        raise 'Invalid grid'

    if winding in ( MESH_CCW_WINDING, MESH_CW_WINDING ):
        if gridx == 2:
            return 2 * gridy - 2
    
        return 2 * ( gridy - 1 ) * ( gridx - 1 ) + ( gridx - 2 )
    else:
        if gridx == 2:
            return 2 * gridy - 1
    
        return 2 * ( gridy - 1 ) * ( gridx - 1 ) + ( gridx - 2 ) * 2
    
#----------------------------------------------------------------------
def megaFormat( b ):
    if b >= ( 1024 * 1024 ):
        if int( b ) % ( 1024 * 1024 ) == 0:
            return "%dM" % ( b / ( 1024 * 1024 ), )
        else:
            return "%.1fM" % ( b / ( 1024.0 * 1024.0 ), )
    elif b >= 1024:
        if int( b ) % 1024 == 0:
            return "%dK" % ( b / 1024, )
        else:
            return "%.1fK" % ( b / 1024.0, )
    else:
        return "%d" % b

#----------------------------------------------------------------------    
def memFormat( bytes ):
    return 'mbbytes=%d' % ( bytes, )    
    
#----------------------------------------------------------------------
def hasLayer( layer, layers ):
    for l in layers:
        if l[ 0 ] == layer:
            return True
    return False

#----------------------------------------------------------------------
def layerTextureFormat( layer, layers ):
    for l in layers:
        if l[ 0 ] == layer:
            return l[ 1 ]
    raise "Missing layer"
   
#----------------------------------------------------------------------
def boolToStr( b ):
    if b:
        return 'Yes'
    else:
        return 'No'

#----------------------------------------------------------------------
def toPot( v, pot ):
    if pot:
        nv = 2 ** math.ceil( math.log( v, 2 ) )
        return ( int( nv ), v / nv, )
    else:
        return ( int( v ), 1.0, )

#----------------------------------------------------------------------
def potToString( pot ):
    if pot:
        return 'pot'
    else:
        return 'npot'

#----------------------------------------------------------------------
def actionToString( fboSize, action, layers ):

    fbos = ''
    if fboSize:
        fbos = '%dx%d fbo ' % fboSize
    else:
        fbos = 'screen '
    
    if action == UPLOAD:
        return fbos + 'upload'
    if action == DRAW:
        if not hasLayer( BACKGROUND, layers ):
            return fbos + 'clear+draw'
        else:
            return fbos + 'draw'
    if action == UPLOAD_AND_DRAW:
        if not hasLayer( BACKGROUND, layers ):
            return fbos + 'upload+clear+draw'
        else:
            return fbos + 'upload+draw'
    else:
        raise 'Unexpected action %s' % str( action )

#----------------------------------------------------------------------
def layersToString( layers ):
    s = []
    i = 0
    if hasLayer( BACKGROUND, layers ):
        s += [ 'background(%s)' % layerTextureFormat( BACKGROUND, layers ) ]
        i += 1
    if hasLayer( PANELS, layers ):
        s += [ 'panels(%s)' % layerTextureFormat( PANELS, layers ) ]
        i += 1
    if hasLayer( GROUPS, layers ):
        s += [ 'groups(%s)' % layerTextureFormat( GROUPS, layers ) ]
        i += 1
    if hasLayer( ICONS, layers ):
        s += [ 'icons(%s)' % layerTextureFormat( ICONS, layers ) ]
        i += 1
    if hasLayer( OVERLAY, layers ):
        s += [ 'overlay(%s)' % layerTextureFormat( OVERLAY, layers ) ]
        i += 1
    if hasLayer( OVERLAY4X, layers ):
        s += [ 'overlay4x(%s)' % layerTextureFormat( OVERLAY4X, layers ) ]
        i += 1

    ss = '+'.join( s )

    if i == 0:
        raise 'No layers'
    elif i == 1:
        ss += ' layer'
    else:
        ss += ' layers'

    return ss

#----------------------------------------------------------------------
def formatShader( shader ):
    shader = shader.replace( "\n", "\\n" )
    while True:
        s = shader.replace( "  ", " " )
        if s == shader:
            return '"' + s.strip() + '"'
        shader = s

#----------------------------------------------------------------------
class VertexDataSetup:
    def __init__( self,
                  vertexArrayIndex,
                  vertexSize,
                  vertexArrayNormalized,
                  colorArrayIndex,
                  colorSize,
                  colorArrayNormalized,
                  normalArrayIndex,
                  texCoordArrayIndices,
                  texCoordSizes,
                  texCoordArrayNormalized,
                  indexArrayIndex,
                  indexArrayLength,
                  glMode ):
        self.vertexArrayIndex           = vertexArrayIndex
        self.vertexSize                 = vertexSize
        self.vertexArrayNormalized      = vertexArrayNormalized
        self.colorArrayIndex            = colorArrayIndex
        self.colorSize                  = colorSize
        self.colorArrayNormalized       = colorArrayNormalized
        self.normalArrayIndex           = normalArrayIndex
        self.texCoordArrayIndices       = texCoordArrayIndices
        self.texCoordSizes              = texCoordSizes[ : ]
        self.texCoordArrayNormalized    = texCoordArrayNormalized
        self.indexArrayIndex            = indexArrayIndex
        self.indexArrayLength           = indexArrayLength
        self.glMode                     = glMode

#----------------------------------------------------------------------
def setupVertexData( modules, indexTracker, mesh ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    if not mesh.vertices:
        raise 'Undefined vertices'

    if not mesh.indices:
        raise 'Undefined indices'

    vertexArrayIndex            = -1
    vertexArrayNormalized       = False
    colorArrayIndex             = -1
    colorArrayNormalized        = False
    normalArrayIndex            = -1
    texCoordArrayIndices        = []
    texCoordArrayNormalized     = []
    indexArrayIndex             = -1

    # ----->
    # Setup vertex attributes

    # Vertices are always defined
    vertexArrayIndex            = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
    vertexArrayNormalized       = mesh.vertexNormalized
    OpenGLES2.CreateArray( vertexArrayIndex, mesh.vertexGLType )
    OpenGLES2.AppendToArray( vertexArrayIndex, mesh.vertices )

    if mesh.colors:
        colorArrayIndex         = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        colorArrayNormalized    = mesh.colorNormalized
        OpenGLES2.CreateArray( colorArrayIndex, mesh.colorGLType )
        OpenGLES2.AppendToArray( colorArrayIndex, mesh.colors )

    if mesh.normals:
        normalArrayIndex        = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( normalArrayIndex, mesh.normalGLType )
        OpenGLES2.AppendToArray( normalArrayIndex, mesh.normals )
   
    if mesh.texCoords:
        for i in range( len( mesh.texCoords ) ):
            if mesh.texCoords:
                texCoordArrayIndices.append( indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' ) )
                texCoordArrayNormalized.append( mesh.texCoordsNormalized[ i ] )
                OpenGLES2.CreateArray( texCoordArrayIndices[ i ], mesh.texCoordsGLTypes[ i ] )
                OpenGLES2.AppendToArray( texCoordArrayIndices[ i ], mesh.texCoords[ i ] )
            else:
                texCoordArrayIndices.append( -1 )
                texCoordArrayNormalized.append( False )
            
    indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
    OpenGLES2.CreateArray( indexArrayIndex, mesh.indexGLType );
    OpenGLES2.AppendToArray( indexArrayIndex, mesh.indices )

    return VertexDataSetup( vertexArrayIndex,
                            mesh.vertexComponents,
                            vertexArrayNormalized,                            
                            colorArrayIndex,
                            mesh.colorComponents,
                            colorArrayNormalized,
                            normalArrayIndex,
                            texCoordArrayIndices,
                            mesh.texCoordsComponents,
                            texCoordArrayNormalized,
                            indexArrayIndex,
                            len( mesh.indices ),
                            mesh.glMode )

#----------------------------------------------------------------------
class Program:
    def __init__( self,
                  programIndex,
                  vertexLocationIndex,
                  colorLocationIndex,
                  normalLocationIndex,
                  texCoordLocationIndices,
                  textureLocationIndices ):
        self.programIndex               = programIndex
        self.vertexLocationIndex        = vertexLocationIndex
        self.colorLocationIndex         = colorLocationIndex
        self.normalLocationIndex        = normalLocationIndex
        self.texCoordLocationIndices    = texCoordLocationIndices
        self.textureLocationIndices     = textureLocationIndices

#----------------------------------------------------------------------
def createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    vsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
    fsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
    
    OpenGLES2.SetData( vsDataIndex, vertexShaderSource )
    OpenGLES2.SetData( fsDataIndex, fragmentShaderSource )
    
    vsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
    OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vsIndex )
    OpenGLES2.ShaderSource( vsIndex, vsDataIndex )
    OpenGLES2.CompileShader( vsIndex )

    fsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
    OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fsIndex )
    OpenGLES2.ShaderSource( fsIndex, fsDataIndex )
    OpenGLES2.CompileShader( fsIndex )

    programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
    OpenGLES2.CreateProgram( programIndex )

    OpenGLES2.AttachShader( programIndex, vsIndex )
    OpenGLES2.AttachShader( programIndex, fsIndex )

    vertexAttributeName         = 'gVertex'
    colorAttributeName          = 'gColor'
    normalAttributeName         = 'gNormal'
    texCoordAttributeName       = 'gTexCoord%d'
    textureName                 = 'gTexture%d'

    # Connect vertex attribute arrays to corresponding shader
    # variables
    nextLocationIndex           = -1
    vertexLocationIndex         = -1
    colorLocationIndex          = -1
    normalLocationIndex         = -1
    texCoordLocationIndices     = []
    textureLocationIndices      = []

    locationValue               = 0

    # Vertices are always defined
    vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
    OpenGLES2.SetRegister( vertexLocationIndex, locationValue )
    OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, vertexAttributeName )
    locationValue += 1

    if vertexDataSetup.colorArrayIndex >= 0:
        colorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( colorLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, colorLocationIndex, colorAttributeName )
        locationValue += 1

    if vertexDataSetup.normalArrayIndex >= 0:
        normalLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( normalLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, normalLocationIndex, normalAttributeName )
        locationValue += 1

    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices[ i ] >= 0:
                texCoordLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
                OpenGLES2.SetRegister( texCoordLocationIndices[ i ], locationValue )
                OpenGLES2.BindAttribLocation( programIndex, texCoordLocationIndices[ i ], texCoordAttributeName % i )
                locationValue += 1
            else:
                texCoordLocationIndices.append( -1 )
            
    OpenGLES2.LinkProgram( programIndex )

    # Program must be linked before we can query the uniform variable
    # (texture) locations
    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices[ i ] >= 0:
                textureLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
                OpenGLES2.GetUniformLocation( textureLocationIndices[ i ], programIndex, textureName % i )
            else:
                textureLocationIndices.append( -1 )

    OpenGLES2.DeleteShader( vsIndex )
    OpenGLES2.DeleteShader( fsIndex )

    return Program( programIndex,
                    vertexLocationIndex,
                    colorLocationIndex,
                    normalLocationIndex,
                    texCoordLocationIndices,
                    textureLocationIndices )

#----------------------------------------------------------------------
class VertexBufferDataSetup:
    def __init__( self,
                  vertexBufferIndex,
                  vertexSize,
                  vertexArrayNormalized,
                  colorBufferIndex,
                  colorSize,
                  colorArrayNormalized,
                  normalBufferIndex,
                  texCoordBufferIndices,
                  texCoordSizes,
                  texCoordArrayNormalized,
                  indexBufferIndex,
                  indexArrayLength,
                  glMode ):
        self.vertexBufferIndex       = vertexBufferIndex
        self.vertexSize              = vertexSize
        self.vertexArrayNormalized   = vertexArrayNormalized
        self.colorBufferIndex        = colorBufferIndex
        self.colorSize               = colorSize
        self.colorArrayNormalized    = colorArrayNormalized
        self.normalBufferIndex       = normalBufferIndex
        self.texCoordBufferIndices   = texCoordBufferIndices
        self.texCoordSizes           = texCoordSizes[ : ]
        self.texCoordArrayNormalized = texCoordArrayNormalized
        self.indexBufferIndex        = indexBufferIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode

#----------------------------------------------------------------------
def setupVertexBufferData( modules, indexTracker, vertexDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    vertexBufferIndex       = -1
    vertexArrayNormalized   = False
    colorBufferIndex        = -1
    colorArrayNormalized    = False
    normalBufferIndex       = -1
    texCoordBufferIndices   = []
    texCoordArrayNormalized = []
    indexArrayIndex         = -1

    vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
    OpenGLES2.GenBuffer( vertexBufferIndex )
    OpenGLES2.BufferData( vertexBufferIndex,
                          'ON',
                          'GL_ARRAY_BUFFER',
                          'GL_STATIC_DRAW',
                          vertexDataSetup.vertexArrayIndex,
                          0,
                          0 )
    vertexArrayNormalized = vertexDataSetup.vertexArrayNormalized
    
    if vertexDataSetup.colorArrayIndex >= 0:
        colorBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( colorBufferIndex )
        OpenGLES2.BufferData( colorBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              vertexDataSetup.colorArrayIndex,
                              0,
                              0 )
        colorArrayNormalized = vertexDataSetup.colorArrayNormalized
        
    if vertexDataSetup.normalArrayIndex >= 0:
        normalBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( normalBufferIndex )
        OpenGLES2.BufferData( normalBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              vertexDataSetup.normalArrayIndex,
                              0,
                              0 )

    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices[ i ] >= 0:
                texCoordBufferIndices.append( indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' ) )
                OpenGLES2.GenBuffer( texCoordBufferIndices[ i ] )
                OpenGLES2.BufferData( texCoordBufferIndices[ i ],
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      vertexDataSetup.texCoordArrayIndices[ i ],
                                      0,
                                      0 )
                texCoordArrayNormalized.append( vertexDataSetup.texCoordArrayNormalized[ i ] )
            else:
                texCoordBufferIndices.append( -1 )
                texCoordArrayNormalized.append( False )

    indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
    OpenGLES2.GenBuffer( indexBufferIndex )
    OpenGLES2.BufferData( indexBufferIndex,
                          'ON',
                          'GL_ELEMENT_ARRAY_BUFFER',
                          'GL_STATIC_DRAW',
                          vertexDataSetup.indexArrayIndex,
                          0,
                          0 )

    return VertexBufferDataSetup( vertexBufferIndex,
                                  vertexDataSetup.vertexSize,
                                  vertexArrayNormalized,
                                  colorBufferIndex,
                                  vertexDataSetup.colorSize,                                  
                                  colorArrayNormalized,
                                  normalBufferIndex,
                                  texCoordBufferIndices,
                                  vertexDataSetup.texCoordSizes,
                                  texCoordArrayNormalized,
                                  indexBufferIndex,
                                  vertexDataSetup.indexArrayLength,
                                  vertexDataSetup.glMode )

#----------------------------------------------------------------------
def useProgram( modules, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.UseProgram( program.programIndex )

#----------------------------------------------------------------------
def validateProgram( modules, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    OpenGLES2.ValidateProgram( program.programIndex )

#----------------------------------------------------------------------
def useVertexBufferData( modules, vertexBufferDataSetup, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.vertexBufferIndex,
                                         'ON',
                                         program.vertexLocationIndex,
                                         vertexBufferDataSetup.vertexSize,
                                         boolToSpandex( vertexBufferDataSetup.vertexArrayNormalized ),
                                         0 )
    OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

    if program.colorLocationIndex >= 0:
        OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.colorBufferIndex,
                                             'ON',
                                             program.colorLocationIndex,
                                             vertexBufferDataSetup.colorSize,
                                             boolToSpandex( vertexBufferDataSetup.colorArrayNormalized ),
                                             0 )
        OpenGLES2.EnableVertexAttribArray( program.colorLocationIndex )

    if program.normalLocationIndex >= 0:
        OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.normalBufferIndex,
                                             'ON',
                                             program.normalLocationIndex,
                                             3,
                                             boolToSpandex( True ),
                                             0 )
        OpenGLES2.EnableVertexAttribArray( program.normalLocationIndex )   

    if program.texCoordLocationIndices:
        for i in range( len( program.texCoordLocationIndices ) ):
            if program.texCoordLocationIndices[ i ] >= 0:
                OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.texCoordBufferIndices[ i ],
                                                     'ON',
                                                     program.texCoordLocationIndices[ i ],
                                                     vertexBufferDataSetup.texCoordSizes[ i ],
                                                     boolToSpandex( vertexBufferDataSetup.texCoordArrayNormalized[ i ] ),
                                                     0 )
                OpenGLES2.EnableVertexAttribArray( program.texCoordLocationIndices[ i ] )

    OpenGLES2.BindBuffer( 'GL_ELEMENT_ARRAY_BUFFER', vertexBufferDataSetup.indexBufferIndex )

#----------------------------------------------------------------------
def drawVertexBufferData( modules, vertexBufferDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.BufferDrawElements( vertexBufferDataSetup.indexBufferIndex,
                                  'OFF',
                                  vertexBufferDataSetup.glMode,
                                  vertexBufferDataSetup.indexArrayLength,
                                  0 )    
    
#----------------------------------------------------------------------
def useTexture( modules, textureIndex, textureUnit, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    if textureUnit < 0 or textureUnit >= len( program.textureLocationIndices ):
        raise 'Invalid texture unit'

    OpenGLES2.ActiveTexture( textureUnit )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )

    OpenGLES2.Uniformi( program.textureLocationIndices[ textureUnit ], 1, [ textureUnit ] )

#----------------------------------------------------------------------
class Material:
    def __init__( self,
                  ambient,
                  diffuse,
                  specular,
                  shininess ):
        self.ambient   = ambient
        self.diffuse   = diffuse
        self.specular  = specular
        self.shininess = shininess

#----------------------------------------------------------------------
class Light:
    def __init__( self,
                  ambient,
                  direction,
                  diffuse,
                  specular ):
        self.ambient   = ambient
        self.direction = direction
        self.diffuse   = diffuse
        self.specular  = specular

#----------------------------------------------------------------------
class LightSetup:
    def __init__( self,
                  program,
                  light,
                  material,
                  ambientLight,
                  ambientMaterial,
                  lightDirection,
                  diffuseLight,
                  diffuseMaterial,
                  specularLight,
                  specularMaterial,
                  shininess ):
        self.program          = program
        self.light            = light
        self.material         = material
        self.ambientLight     = ambientLight
        self.ambientMaterial  = ambientMaterial
        self.lightDirection   = lightDirection
        self.diffuseLight     = diffuseLight
        self.diffuseMaterial  = diffuseMaterial
        self.specularLight    = specularLight
        self.specularMaterial = specularMaterial
        self.shininess        = shininess

#----------------------------------------------------------------------
def setupLight( modules, indexTracker, light, material, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    ambientLight     = -1
    ambientMaterial  = -1
    lightDirection   = -1
    diffuseLight     = -1
    diffuseMaterial  = -1
    specularLight    = -1
    specularMaterial = -1
    shininess        = -1

    if light and light.ambient:
        ambientLight = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( ambientLight, program.programIndex, 'gAmbientLight' )

    if material and material.ambient:
        ambientMaterial = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( ambientMaterial, program.programIndex, 'gAmbientMaterial' )

    if light and light.direction:
        lightDirection = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( lightDirection, program.programIndex, 'gLightDirection' )

    if light and light.diffuse:
        diffuseLight = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( diffuseLight, program.programIndex, 'gDiffuseLight' )

    if material and material.diffuse:
        diffuseMaterial = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( diffuseMaterial, program.programIndex, 'gDiffuseMaterial' )

    if light and light.specular:
        specularLight = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( specularLight, program.programIndex, 'gSpecularLight' )

    if material and material.specular:
        specularMaterial = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( specularMaterial, program.programIndex, 'gSpecularMaterial' )

    if material and material.shininess:
        shininess = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( shininess, program.programIndex, 'gShininess' )

    return LightSetup( program,
                       light,
                       material,
                       ambientLight,
                       ambientMaterial,
                       lightDirection,
                       diffuseLight,
                       diffuseMaterial,
                       specularLight,
                       specularMaterial,
                       shininess )

#----------------------------------------------------------------------
def useLight( modules, lightSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    if lightSetup.ambientLight >= 0:
        OpenGLES2.Uniformf( lightSetup.ambientLight, 1, lightSetup.light.ambient )

    if lightSetup.ambientMaterial >= 0:
        OpenGLES2.Uniformf( lightSetup.ambientMaterial, 1, lightSetup.material.ambient )

    if lightSetup.lightDirection >= 0:
        OpenGLES2.Uniformf( lightSetup.lightDirection, 1, lightSetup.light.direction )

    if lightSetup.diffuseLight >= 0:
        OpenGLES2.Uniformf( lightSetup.diffuseLight, 1, lightSetup.light.diffuse )

    if lightSetup.diffuseMaterial >= 0:
        OpenGLES2.Uniformf( lightSetup.diffuseMaterial, 1, lightSetup.material.diffuse )
                  
    if lightSetup.specularLight >= 0:
        OpenGLES2.Uniformf( lightSetup.specularLight, 1, lightSetup.light.specular )

    if lightSetup.specularMaterial >= 0:
        OpenGLES2.Uniformf( lightSetup.specularMaterial, 1, lightSetup.material.specular )
    
    if lightSetup.shininess >= 0:
        OpenGLES2.Uniformf( lightSetup.shininess, 1, [ lightSetup.material.shininess ] )
    
