#
# Spandex benchmark and test framework.
#
# Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *
        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ContinuousAllocation( Benchmark ):
    def __init__( self, width, height, memlimit = 0 ):
        Benchmark.__init__( self )
        self.repeats    = 0
        self.width      = width
        self.height     = height
        self.memlimit   = memlimit
        self.name = "OPENGLES2 continuous allocation %dx%d RGBA8 %x" % ( self.width,
                                                                         self.height,
                                                                         self.memlimit )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # Shader source codes
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 position_in;
        attribute vec2 uv_in;
        uniform mat4 mvp;
        varying vec2 uv;

        void main()
        {
            gl_Position = mvp * position_in;
            uv = uv_in;
        }"""
        
        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D tex0;
        varying vec2 uv;

        void main()
        {
            gl_FragColor = texture2D(tex0, uv);
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )
        
        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
       
        state = setupOpenGLES2( modules, indexTracker, target )            

        targetWidth      = screenWidth
        targetHeight     = screenWidth
        backgroundWidth  = targetWidth
        backgroundHeight = targetHeight

        backgroundTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]
        
        background = MeshStripPlane( 2,
                                     2,
                                     MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                     None,
                                     None,
                                     backgroundTexCoordAttribute,
                                     MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                     None,
                                     MESH_CCW_WINDING,
                                     False )

        background.translate( [ -0.5, -0.5, 0 ] )
        background.scale( [ 2.0 * backgroundWidth / float( targetWidth ),
                            2.0 * backgroundHeight / float( targetHeight ) ] )
                   
        backgroundVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( backgroundVertexArrayIndex, background.vertexGLType )
        OpenGLES2.AppendToArray( backgroundVertexArrayIndex, background.vertices )
            
        backgroundTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( backgroundTexCoordArrayIndex, background.texCoordsGLTypes[ 0 ] )
        OpenGLES2.AppendToArray( backgroundTexCoordArrayIndex, background.texCoords[ 0 ] )
            
        backgroundIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( backgroundIndexArrayIndex, background.indexGLType );
        OpenGLES2.AppendToArray( backgroundIndexArrayIndex, background.indices )
            
        # Create shaders.
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )
        
        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )
        
        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, "position_in" )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex, texCoordLocationIndex, "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.ValidateProgram( programIndex )
        OpenGLES2.UseProgram( programIndex )        
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        mVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mVPLocationIndex, programIndex, 'mvp' )
            
        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        textureDataIndex =indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                          [ 0.5, 0.5, 0.5, 0.5 ],
                                          self.width, self.height,
                                          'OFF',
                                          'OPENGLES2_RGBA8888' )
        
        OpenGLES2.CheckError( 'Init' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES2.StashTexture( textureDataIndex, 'GL_TEXTURE_2D', '0x%x' % self.memlimit, 16384, 'ON', 'ON' )

        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
        
        OpenGLES2.UniformMatrix( mVPLocationIndex,
                                 1,
                                 [ 1.0, 0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.VertexAttribPointer( vertexLocationIndex, backgroundVertexArrayIndex, 2, 'OFF', 0 )
        OpenGLES2.VertexAttribPointer( texCoordLocationIndex, backgroundTexCoordArrayIndex, 2, 'OFF', 0 )

        OpenGLES2.DrawElements( background.glMode, len( background.indices ), 0, backgroundIndexArrayIndex )    

        target.swapBuffers( state )
        

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class FragmentedAllocation( Benchmark ):
    def __init__( self, width, height, memlimit, fWidth = 256, fHeight = 256, fwInc = 2, fhInc = 2, fwLimit = 2048, fhLimit = 2048 ):
        Benchmark.__init__( self )
        self.repeats    = 0
        self.width      = width
        self.height     = height
        self.memlimit   = memlimit
        self.fWidth     = fWidth
        self.fHeight    = fHeight
        self.fwInc      = fwInc
        self.fhInc      = fhInc
        self.fwLimit    = fwLimit
        self.fhLimit    = fhLimit
        self.name = "OPENGLES2 fragmented allocation %dx%d RGBA8 %x %d %d %d %d %d %d" % ( self.width,
                                                                                           self.height,
                                                                                           self.memlimit,
                                                                                           self.fWidth,
                                                                                           self.fHeight,
                                                                                           self.fwInc,
                                                                                           self.fhInc,
                                                                                           self.fwLimit,
                                                                                           self.fhLimit, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # Shader source codes
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 position_in;
        attribute vec2 uv_in;
        uniform mat4 mvp;
        varying vec2 uv;

        void main()
        {
            gl_Position = mvp * position_in;
            uv = uv_in;
        }"""
        
        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D tex0;
        varying vec2 uv;

        void main()
        {
            gl_FragColor = texture2D(tex0, uv);
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )
        
        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
       
        state = setupOpenGLES2( modules, indexTracker, target )            

        targetWidth      = screenWidth
        targetHeight     = screenWidth
        backgroundWidth  = targetWidth
        backgroundHeight = targetHeight

        backgroundTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]
        
        background = MeshStripPlane( 2,
                                     2,
                                     MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                     None,
                                     None,
                                     backgroundTexCoordAttribute,
                                     MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                     None,
                                     MESH_CCW_WINDING,
                                     False )

        background.translate( [ -0.5, -0.5, 0 ] )
        background.scale( [ 2.0 * backgroundWidth / float( targetWidth ),
                            2.0 * backgroundHeight / float( targetHeight ) ] )
                   
        backgroundVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( backgroundVertexArrayIndex, background.vertexGLType )
        OpenGLES2.AppendToArray( backgroundVertexArrayIndex, background.vertices )
            
        backgroundTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( backgroundTexCoordArrayIndex, background.texCoordsGLTypes[ 0 ] )
        OpenGLES2.AppendToArray( backgroundTexCoordArrayIndex, background.texCoords[ 0 ] )
            
        backgroundIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( backgroundIndexArrayIndex, background.indexGLType );
        OpenGLES2.AppendToArray( backgroundIndexArrayIndex, background.indices )
            
        # Create shaders.
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )
        
        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )
        
        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, "position_in" )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex, texCoordLocationIndex, "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.ValidateProgram( programIndex )
        OpenGLES2.UseProgram( programIndex )        
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        mVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mVPLocationIndex, programIndex, 'mvp' )
            
        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        textureDataIndex =indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                          [ 0.5, 0.5, 0.5, 0.5 ],
                                          self.width, self.height,
                                          'OFF',
                                          'OPENGLES2_RGBA8888' )
        
        OpenGLES2.CheckError( 'Init' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        fragmentTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGrowingTextureData( fragmentTextureDataIndex,
                                            [ 0.5, 0.5, 0.5, 0.5 ],
                                            self.fWidth, self.fHeight,
                                            self.fwInc, self.fhInc,
                                            self.fwLimit, self.fhLimit,
                                            'OFF',
                                            'OPENGLES2_RGBA8888' )

        fragmentTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( fragmentTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fragmentTextureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( fragmentTextureDataIndex, 'GL_TEXTURE_2D' )
        
        OpenGLES2.StashTexture( textureDataIndex, 'GL_TEXTURE_2D', '0x%x' % self.memlimit, 16384, 'ON', 'ON' )

        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
        
        OpenGLES2.UniformMatrix( mVPLocationIndex,
                                 1,
                                 [ 1.0, 0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.VertexAttribPointer( vertexLocationIndex, backgroundVertexArrayIndex, 2, 'OFF', 0 )
        OpenGLES2.VertexAttribPointer( texCoordLocationIndex, backgroundTexCoordArrayIndex, 2, 'OFF', 0 )

        OpenGLES2.DrawElements( background.glMode, len( background.indices ), 0, backgroundIndexArrayIndex )    

        target.swapBuffers( state )

        OpenGLES2.DeleteTexture( fragmentTextureIndex )
        OpenGLES2.DeleteTextureData( fragmentTextureDataIndex )

