#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#          Sami Kyostila <sami.kyostila@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from common        import *
        
SINGLE_ACCESS_READ   = 'READ'
SINGLE_ACCESS_WRITE  = 'FILLZ'
SINGLE_ACCESS_COPY   = 'COPY'
SINGLE_ACCESS_MODIFY = 'MODIFY'
SINGLE_ACCESS_BLEND  = 'BLEND'

BLOCK_ACCESS_WRITE   = 'FILLZ'
BLOCK_ACCESS_COPY    = 'COPY'

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SingleAccessBenchmark( Benchmark ):  
    def __init__( self, operation, blockCount, blockSize, elementSize, iterations, align, loopUnroll ):
        Benchmark.__init__( self )
        self.operation   = operation
        self.blockCount  = blockCount
        self.blockSize   = blockSize
        self.elementSize = elementSize
        self.iterations  = iterations
        self.align       = align
        self.loopUnroll  = loopUnroll
        self.bytes       = self.blockCount * self.blockSize * self.iterations

        if self.operation == SINGLE_ACCESS_READ:
            if not self.loopUnroll:
                self.name = "MEMORY single access c read %s (bc %d bsz %d elmsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                          self.blockCount,
                                                                                                          self.blockSize,
                                                                                                          self.elementSize,
                                                                                                          self.iterations,
                                                                                                          self.align, )
            else:
                self.name = "MEMORY single access c read unrolled %s (bc %d bsz %d elmsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                                   self.blockCount,
                                                                                                                   self.blockSize,
                                                                                                                   self.elementSize,
                                                                                                                   self.iterations,
                                                                                                                   self.align, )
        elif self.operation == SINGLE_ACCESS_WRITE:
            if not self.loopUnroll:
                self.name = "MEMORY single access c write %s (bc %d bsz %d elmsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                           self.blockCount,
                                                                                                           self.blockSize,
                                                                                                           self.elementSize,
                                                                                                           self.iterations,
                                                                                                           self.align, )
            else:
                self.name = "MEMORY single access c write unrolled %s (bc %d bsz %d elmsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                                    self.blockCount,
                                                                                                                    self.blockSize,
                                                                                                                    self.elementSize,
                                                                                                                    self.iterations,
                                                                                                                    self.align, )
        elif self.operation == SINGLE_ACCESS_COPY:
            if not self.loopUnroll:
                self.name = "MEMORY single access c copy %s (bc %d bsz %d elmsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                          self.blockCount,
                                                                                                          self.blockSize,
                                                                                                          self.elementSize,
                                                                                                          self.iterations,
                                                                                                          self.align, )
            else:
                self.name = "MEMORY single access c copy unrolled %s (bc %d bsz %d elmsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                                   self.blockCount,
                                                                                                                   self.blockSize,
                                                                                                                   self.elementSize,
                                                                                                                   self.iterations,
                                                                                                                   self.align, )
        elif self.operation == SINGLE_ACCESS_MODIFY:
            if not self.loopUnroll:
                self.name = "MEMORY single access c modify %s (bc %d bsz %d elmsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                            self.blockCount,
                                                                                                            self.blockSize,
                                                                                                            self.elementSize,
                                                                                                            self.iterations,
                                                                                                            self.align, )
            else:
                self.name = "MEMORY single access c modify unrolled %s (bc %d bsz %d elmsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                                     self.blockCount,
                                                                                                                     self.blockSize,
                                                                                                                     self.elementSize,
                                                                                                                     self.iterations,
                                                                                                                     self.align, )
        elif self.operation == SINGLE_ACCESS_BLEND:
            if not self.loopUnroll:
                self.name = "MEMORY single access c blend %s (bc %d bsz %d elmsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                           self.blockCount,
                                                                                                           self.blockSize,
                                                                                                           self.elementSize,
                                                                                                           self.iterations,
                                                                                                           self.align, )
            else:
                self.name = "MEMORY single access c blend unrolled %s (bc %d bsz %d elmsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                                    self.blockCount,
                                                                                                                    self.blockSize,
                                                                                                                    self.elementSize,
                                                                                                                    self.iterations,
                                                                                                                    self.align, )
        else:
            raise 'Unexpected operation %s' % str( self.operation )
                
    def build( self, target, modules ):
        Memory = modules[ 'Memory' ]

        indexTracker = IndexTracker()
       
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
             
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        Memory.Single( self.blockCount,
                       self.blockSize,
                       self.elementSize,
                       self.iterations,
                       self.align,
                       self.operation,
                       boolToSpandex( self.loopUnroll ) )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class BlockAccessBenchmark( Benchmark ):  
    def __init__( self, operation, blockCount, blockSize, iterations, align ):
        Benchmark.__init__( self )
        self.operation   = operation
        self.blockCount  = blockCount
        self.blockSize   = blockSize
        self.iterations  = iterations
        self.align       = align
        self.bytes       = self.blockCount * self.blockSize * self.iterations

        if self.operation == BLOCK_ACCESS_WRITE:
            self.name = "MEMORY block access syslib write %s (bc %d bsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                  self.blockCount,
                                                                                                  self.blockSize,
                                                                                                  self.iterations,
                                                                                                  self.align, )
        elif self.operation == BLOCK_ACCESS_COPY:
            self.name = "MEMORY block access syslib copy %s (bc %d bsz %d itrns %d algn %d)" % ( memFormat( self.bytes ),
                                                                                                 self.blockCount,
                                                                                                 self.blockSize,
                                                                                                 self.iterations,
                                                                                                 self.align, )
        else:
            raise 'Unexpected operation %s' % str( self.operation )

            
    def build( self, target, modules ):
        Memory = modules[ 'Memory' ]

        indexTracker = IndexTracker()
       
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
             
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        Memory.Block( self.blockCount,
                      self.blockSize,
                      self.iterations,
                      self.align,
                      self.operation )

