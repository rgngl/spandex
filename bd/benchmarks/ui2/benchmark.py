#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#          Sami Kyostila <sami.kyostila@gmail.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../..' )

import  bd
import  common

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'Spandex v2 UI benchmark'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

#----------------------------------------------------------------------
( width, height, )      = target.getScreenSize()

import cpumem
import opengles2

res_vga    = ( 480,  640,  )
res_720p   = ( 720,  1280, )
res_fullhd = ( 1080, 1920, )

bg         = ( common.BACKGROUND, 'RGBA8'  )
panels     = ( common.PANELS,     'RGBA8' )
groups     = ( common.GROUPS,     'RGBA8' )
icons      = ( common.ICONS,      'RGBA8' )
overlay    = ( common.OVERLAY,    'RGBA8' )

# ----------------------------------------------------------------------
# Texture data upload with no drawing and hence no display sync.
# ----------------------------------------------------------------------
suite.addBenchmark( opengles2.TextureDataUploadBenchmark(   64,   64, 'FLOWER', 'RGBA8',       False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark(  128,  128, 'FLOWER', 'RGBA8',       False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark(  256,  256, 'FLOWER', 'RGBA8',       False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark(  512,  512, 'FLOWER', 'RGBA8',       False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark(  360,  640, 'FLOWER', 'RGBA8',       False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark(  640,  480, 'FLOWER', 'RGBA8',       False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark(  480,  800, 'FLOWER', 'RGBA8',       False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark(  480,  854, 'FLOWER', 'RGBA8',       False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'RGBA8',       False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 2048, 'FLOWER', 'RGBA8',       False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 2048, 2048, 'FLOWER', 'RGBA8',       False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'ETC1_RGB8',   False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'ETC1_RGB8_A', False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'PVRTC_RGB4',  False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'PVRTC_RGB2',  False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'PVRTC_RGBA4', False, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'PVRTC_RGBA2', False, None, False ) )

for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER',    'RGB565', False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER',    'RGB8',   False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER',    'RGBA8',  False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'SOLID',     'RGBA8',  False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'CHECKER',   'RGBA8',  False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'VGRADIENT', 'RGBA8',  False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'HGRADIENT', 'RGBA8',  False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'DGRADIENT', 'RGBA8',  False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'RANDOM',    'RGBA8',  False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'PERLIN1',   'RGBA8',  False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'PERLIN2',   'RGBA8',  False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'PERLIN3',   'RGBA8',  False, None, False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER',    'RGBA8',  False, None, True  ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'RANDOM',    'RGBA8',  False, None, True  ) )

for sz in [ res_vga, res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD, [ bg ],                                 False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD, [ bg, panels ],                         False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD, [ bg, panels, groups ],                 False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD, [ bg, panels, groups, icons ],          False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD, [ bg, panels, groups, icons, overlay ], False, False ) )

for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD, [ bg, panels, groups, icons, overlay ], True,  False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD, [ bg, panels, groups, icons, overlay ], False, True  ) )

overdraw = 4
for sz in [ res_vga, res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.UiThroughputBenchmark( sz, common.UPLOAD, overdraw ) )

# ----------------------------------------------------------------------
# Ui cases, some reaching 60 FPS or more with display sync off.
# ----------------------------------------------------------------------
for sz in [ res_vga, res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.Clear( sz, 1 ) )
    suite.addBenchmark( opengles2.Clear( sz, 2 ) )

suite.addBenchmark( opengles2.TextureDataUploadBenchmark(  360,  640, 'FLOWER', 'RGBA8',       1, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark(  480,  800, 'FLOWER', 'RGBA8',       1, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark(  854,  480, 'FLOWER', 'RGBA8',       1, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'RGBA8',       1, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 2048, 'FLOWER', 'RGBA8',       1, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 2048, 2048, 'FLOWER', 'RGBA8',       1, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'ETC1_RGB8',   1, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'ETC1_RGB8_A', 1, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'PVRTC_RGB4',  1, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'PVRTC_RGB2',  1, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'PVRTC_RGBA4', 1, None, False ) )
suite.addBenchmark( opengles2.TextureDataUploadBenchmark( 1024, 1024, 'FLOWER', 'PVRTC_RGBA2', 1, None, False ) )

for sz in [ res_vga, res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER', 'RGB565', 1, None,     False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER', 'RGB8',   1, None,     False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER', 'RGBA8',  1, None,     False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER', 'RGBA8',  1, 'rot90',  False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER', 'RGBA8',  1, 'rot180', False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER', 'RGBA8',  1, 'rot270', False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER', 'RGBA8',  1, 'hflip',  False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER', 'RGBA8',  1, 'vflip',  False ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'FLOWER', 'RGBA8',  1, None,     True  ) )
    suite.addBenchmark( opengles2.TextureDataUploadBenchmark( sz[ 0 ], sz[ 1 ], 'RANDOM', 'RGBA8',  1, None,     True  ) )

for sz in [ res_vga, res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.DRAW,            [ bg ],                                 False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.DRAW,            [ bg, panels ],                         False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.DRAW,            [ bg, panels, groups ],                 False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.DRAW,            [ bg, panels, groups, icons ],          False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.DRAW,            [ bg, panels, groups, icons, overlay ], False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD_AND_DRAW, [ bg ],                                 False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD_AND_DRAW, [ bg, panels ],                         False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD_AND_DRAW, [ bg, panels, groups ],                 False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD_AND_DRAW, [ bg, panels, groups, icons ],          False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD_AND_DRAW, [ bg, panels, groups, icons, overlay ], False, False ) )

for sz in [ res_vga, res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.DRAW,            [ bg, panels, groups, icons, overlay ], True,  False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD_AND_DRAW, [ bg, panels, groups, icons, overlay ], True,  False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.DRAW,            [ bg, panels, groups, icons, overlay ], False, True  ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD_AND_DRAW, [ bg, panels, groups, icons, overlay ], False, True  ) )

# ----------------------------------------------------------------------
# Pixel fill throughput.
# ----------------------------------------------------------------------
overdraw = 4
for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.UiThroughputBenchmark( sz, common.DRAW,            overdraw ) )
    suite.addBenchmark( opengles2.UiThroughputBenchmark( sz, common.UPLOAD_AND_DRAW, overdraw ) )

overdraw = 42
for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 1, overdraw, 'opaque'                    ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 1, overdraw, 'transparent'               ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  1, overdraw, 'opaque'                    ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  1, overdraw, 'transparent'               ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'nearest', 1, overdraw, 'opaque'                    ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'nearest', 1, overdraw, 'transparent'               ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'linear',  1, overdraw, 'opaque'                    ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'linear',  1, overdraw, 'transparent'               ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 1, overdraw, 'transparent back-to-front' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 1, overdraw, 'opaque front-to-back'      ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  1, overdraw, 'transparent back-to-front' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  1, overdraw, 'opaque front-to-back'      ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'nearest', 1, overdraw, 'transparent back-to-front' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'nearest', 1, overdraw, 'opaque front-to-back'      ) )

for sz in [ res_720p ]:
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 5, overdraw, 'opaque'                    ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 5, overdraw, 'transparent'               ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  5, overdraw, 'opaque'                    ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  5, overdraw, 'transparent'               ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 5, overdraw, 'transparent back-to-front' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 5, overdraw, 'opaque front-to-back'      ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  5, overdraw, 'transparent back-to-front' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  5, overdraw, 'opaque front-to-back'      ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'nearest', 5, overdraw, 'transparent back-to-front' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'nearest', 5, overdraw, 'opaque front-to-back'      ) )

    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 9, overdraw, 'opaque'                    ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 9, overdraw, 'transparent'               ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  9, overdraw, 'opaque'                    ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  9, overdraw, 'transparent'               ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 9, overdraw, 'transparent back-to-front' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 9, overdraw, 'opaque front-to-back'      ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  9, overdraw, 'transparent back-to-front' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  9, overdraw, 'opaque front-to-back'      ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'nearest', 9, overdraw, 'transparent back-to-front' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'nearest', 9, overdraw, 'opaque front-to-back'      ) )

overdraw = 32
for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.MultitextureFillrateBenchmark( sz, 'RGBA8',  'nearest', 1, overdraw ) )
    suite.addBenchmark( opengles2.MultitextureFillrateBenchmark( sz, 'RGBA8',  'linear',  1, overdraw ) )
    suite.addBenchmark( opengles2.MultitextureFillrateBenchmark( sz, 'RGBA8',  'nearest', 2, overdraw ) )
    suite.addBenchmark( opengles2.MultitextureFillrateBenchmark( sz, 'RGBA8',  'linear',  2, overdraw ) )
    suite.addBenchmark( opengles2.MultitextureFillrateBenchmark( sz, 'RGBA8',  'nearest', 4, overdraw ) )
    suite.addBenchmark( opengles2.MultitextureFillrateBenchmark( sz, 'RGBA8',  'linear',  4, overdraw ) )
    suite.addBenchmark( opengles2.MultitextureFillrateBenchmark( sz, 'RGBA8',  'nearest', 8, overdraw ) )
    suite.addBenchmark( opengles2.MultitextureFillrateBenchmark( sz, 'RGBA8',  'linear',  8, overdraw ) )

for sz in [ res_720p ]:
    suite.addBenchmark( opengles2.MultitextureFillrateBenchmark( sz, 'RGB565', 'nearest', 1, overdraw ) )
    suite.addBenchmark( opengles2.MultitextureFillrateBenchmark( sz, 'RGB565', 'linear',  1, overdraw ) )

overdraw = 4
for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.DependentTexturingBenchmark( sz, 'RGB8', 'noise',  overdraw ) )
    suite.addBenchmark( opengles2.DependentTexturingBenchmark( sz, 'RGB8', 'lookup', overdraw ) )
    suite.addBenchmark( opengles2.DependentTexturingBenchmark( sz, 'RGB8', 'multi',  overdraw ) )

# ----------------------------------------------------------------------
# Vertex throughput.
# ----------------------------------------------------------------------
parameters = ( ( res_vga, 48, 32, 256, ), ) # fboSize, gridx, gridy, overdraw
for p in parameters:
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'white', ),                                      'none',  'none' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'white', ),                                      'none',  'half' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'white', ),                                      'none', 'fully' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'white', ),                                'downscaled',  'none' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'white', ),                                'downscaled',  'half' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'white', ),                                'downscaled', 'fully' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'smooth', ),                                     'none',  'none' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured', 'RGBA8', ),                          'none',  'none' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured', 'RGBA8', ),                          'none',  'half' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured', 'RGBA8', ),                          'none', 'fully' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured', 'RGBA8', ),                    'downscaled',  'none' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured', 'RGBA8', ),                    'downscaled',  'half' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured', 'RGBA8', ),                    'downscaled', 'fully' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'per-vertex light', ),                           'none',  'none' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'per-vertex light', ),                           'none',  'half' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'per-vertex light', ),                           'none', 'fully' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'per-vertex light', ),                     'downscaled',  'none' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'per-vertex light', ),                     'downscaled',  'half' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'per-vertex light', ),                     'downscaled', 'fully' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'per-fragment light', ),                         'none',  'none' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'per-fragment light', ),                         'none',  'half' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'per-fragment light', ),                         'none', 'fully' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured per-vertex light',   'RGBA8', ),       'none',  'none' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured per-vertex light',   'RGBA8', ),       'none',  'half' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured per-vertex light',   'RGBA8', ),       'none', 'fully' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured per-vertex light',   'RGBA8', ), 'downscaled',  'none' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured per-vertex light',   'RGBA8', ), 'downscaled',  'half' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured per-vertex light',   'RGBA8', ), 'downscaled', 'fully' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured per-fragment light', 'RGBA8', ),       'none',  'none' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured per-fragment light', 'RGBA8', ),       'none',  'half' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], ( 'textured per-fragment light', 'RGBA8', ),       'none', 'fully' ) )

# ----------------------------------------------------------------------
# Particle throughput.
# ----------------------------------------------------------------------
parameters = ( ( res_720p, 48, 32, 128, ), ) # fboSize, gridx, gridy, overdraw
for p in parameters:
    suite.addBenchmark( opengles2.ParticlesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], 10, 'RGBA8', False ) )
    suite.addBenchmark( opengles2.ParticlesBenchmark( p[ 0 ], p[ 1 ], p[ 2 ], p[ 3 ], 10, 'RGBA8', True  ) )

# ----------------------------------------------------------------------
# Shader effects.
# ----------------------------------------------------------------------
for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.ColorTransformationBenchmark( 'sepia', sz[ 0 ], sz[ 1 ], 'RGB8' ) )

for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.ColorTransformationBenchmark( 'bleach', sz[ 0 ], sz[ 1 ], 'RGB8' ) )

for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.ColorTransformationBenchmark( 'tint', sz[ 0 ], sz[ 1 ], 'RGB8' ) )

for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.ColorTransformationBenchmark( 'hsl', sz[ 0 ], sz[ 1 ], 'RGBA8' ) )

for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.CompositionMaskBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8' ) )

for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.BlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGBA8', 1, 'nearest' ) )
    suite.addBenchmark( opengles2.BlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGBA8', 2, 'nearest' ) )
    suite.addBenchmark( opengles2.BlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGBA8', 4, 'nearest' ) )
    suite.addBenchmark( opengles2.BlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGBA8', 8, 'nearest' ) )
    suite.addBenchmark( opengles2.BlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGBA8', 1, 'linear'  ) )
    suite.addBenchmark( opengles2.BlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGBA8', 2, 'linear'  ) )
    suite.addBenchmark( opengles2.BlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGBA8', 4, 'linear'  ) )
    suite.addBenchmark( opengles2.BlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGBA8', 8, 'linear'  ) )

for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.MotionBlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 16, 16, 2, 'nearest' ) )
    suite.addBenchmark( opengles2.MotionBlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 16, 16, 3, 'nearest' ) )
    suite.addBenchmark( opengles2.MotionBlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 16, 16, 4, 'nearest' ) )
    suite.addBenchmark( opengles2.MotionBlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 16, 16, 2, 'linear'  ) )
    suite.addBenchmark( opengles2.MotionBlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 16, 16, 3, 'linear'  ) )
    suite.addBenchmark( opengles2.MotionBlurBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 16, 16, 4, 'linear'  ) )

for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.DOFBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 1, 'nearest' ) )
    suite.addBenchmark( opengles2.DOFBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 2, 'nearest' ) )
    suite.addBenchmark( opengles2.DOFBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 4, 'nearest' ) )
    suite.addBenchmark( opengles2.DOFBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 8, 'nearest' ) )
    suite.addBenchmark( opengles2.DOFBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 1, 'linear'  ) )
    suite.addBenchmark( opengles2.DOFBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 2, 'linear'  ) )
    suite.addBenchmark( opengles2.DOFBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 4, 'linear'  ) )
    suite.addBenchmark( opengles2.DOFBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 8, 'linear'  ) )

for sz in [ res_720p, res_fullhd ]:
    suite.addBenchmark( opengles2.GlowBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 1, 'nearest' ) )
    suite.addBenchmark( opengles2.GlowBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 2, 'nearest' ) )
    suite.addBenchmark( opengles2.GlowBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 4, 'nearest' ) )
    suite.addBenchmark( opengles2.GlowBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 8, 'nearest' ) )
    suite.addBenchmark( opengles2.GlowBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 1, 'linear'  ) )
    suite.addBenchmark( opengles2.GlowBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 2, 'linear'  ) )
    suite.addBenchmark( opengles2.GlowBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 4, 'linear'  ) )
    suite.addBenchmark( opengles2.GlowBenchmark( sz[ 0 ], sz[ 1 ], 'RGB8', 8, 'linear'  ) )

# ----------------------------------------------------------------------
# Shader throughput.
# ----------------------------------------------------------------------
for sz in [ ( 128, 128, ), ( 256, 256, ), res_vga ]:
    suite.addBenchmark( opengles2.PulsBenchmark( sz ) )

for sz in [ ( 256, 256, ), res_vga ]:
    suite.addBenchmark( opengles2.MandelbrotBenchmark( sz, 64  ) )
    suite.addBenchmark( opengles2.MandelbrotBenchmark( sz, 128 ) )
    suite.addBenchmark( opengles2.MandelbrotBenchmark( sz, 256 ) )
    suite.addBenchmark( opengles2.MandelbrotBenchmark( sz, 512 ) )

#----------------------------------------------------------------------
# Memory benchmark
#----------------------------------------------------------------------
blockCount = 16
blockSize  = 4 * 512 * 512     # 16 MB total
suite.addBenchmark( cpumem.BlockAccessBenchmark(  cpumem.BLOCK_ACCESS_WRITE,   blockCount, blockSize, 1, 16 ) )
suite.addBenchmark( cpumem.BlockAccessBenchmark(  cpumem.BLOCK_ACCESS_COPY,    blockCount, blockSize, 1, 16 ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( cpumem.SINGLE_ACCESS_READ,   blockCount, blockSize, 4,  1, 16, False ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( cpumem.SINGLE_ACCESS_READ,   blockCount, blockSize, 4,  1, 16, True  ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( cpumem.SINGLE_ACCESS_WRITE,  blockCount, blockSize, 4,  1, 16, False ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( cpumem.SINGLE_ACCESS_WRITE,  blockCount, blockSize, 4,  1, 16, True  ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( cpumem.SINGLE_ACCESS_COPY,   blockCount, blockSize, 4,  1, 16, False ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( cpumem.SINGLE_ACCESS_COPY,   blockCount, blockSize, 4,  1, 16, True  ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( cpumem.SINGLE_ACCESS_MODIFY, blockCount, blockSize, 4,  1, 16, False ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( cpumem.SINGLE_ACCESS_MODIFY, blockCount, blockSize, 4,  1, 16, True  ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( cpumem.SINGLE_ACCESS_BLEND,  blockCount, blockSize, 4,  1, 16, False ) )
suite.addBenchmark( cpumem.SingleAccessBenchmark( cpumem.SINGLE_ACCESS_BLEND,  blockCount, blockSize, 4,  1, 16, True  ) )

#----------------------------------------------------------------------
# Diagnostics
# ----------------------------------------------------------------------

# Draw some of the benchmarks to full screen and to an FBO with matching size to
# allow comparing the performance of the two.
screenSizes = [ None ]

if ( width, height, ) not in [ res_vga, res_720p, res_fullhd ]:
    screenSizes += [ ( width, height, ) ]

for sz in screenSizes:
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD, [ bg, panels, groups, icons, overlay ],          False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.DRAW, [ bg, panels, groups, icons, overlay ],            False, False ) )
    suite.addBenchmark( opengles2.UiBenchmark( sz, common.UPLOAD_AND_DRAW, [ bg, panels, groups, icons, overlay ], False, False ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'nearest', 1, 42, 'transparent' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGBA8',  'linear',  1, 42, 'transparent' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'nearest', 1, 42, 'transparent' ) )
    suite.addBenchmark( opengles2.TexelFillrateBenchmark( sz, 'RGB565', 'linear',  1, 42, 'transparent' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( sz, 48, 32, 256, ( 'per-vertex light', ), 'none',       'none'  ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( sz, 48, 32, 256, ( 'per-vertex light', ), 'none',       'half'  ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( sz, 48, 32, 256, ( 'per-vertex light', ), 'none',       'fully' ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( sz, 48, 32, 256, ( 'per-vertex light', ), 'downscaled', 'none'  ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( sz, 48, 32, 256, ( 'per-vertex light', ), 'downscaled', 'half'  ) )
    suite.addBenchmark( opengles2.TrianglesBenchmark( sz, 48, 32, 256, ( 'per-vertex light', ), 'downscaled', 'fully' ) )

suite.addBenchmark( opengles2.EglInfo(), 'EGL_LIMITED' )           # REQUIRES EGL
suite.addBenchmark( opengles2.ResourceProfile(), 'EGL_LIMITED' )   # REQUIRES EGL, EGL_NOK_resource_profiling extension
suite.addBenchmark( opengles2.Info() )

#----------------------------------------------------------------------

command()
