#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#          Sami Kyostila <sami.kyostila@gmail.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

import numpy

IMAGES_TYPES  = { 'FLOWER' : 'IMAGES_FLOWER'
                  }

PATTERN_TYPES = { 'SOLID'    : '',
                  'CHECKER'  : '',
                  'GRADIENT' : ''
                  }

NOISE_TYPES = { 'RANDOM'  : 'OPENGLES2_NOISE_RANDOM',
                'PERLIN1' : 'OPENGLES2_NOISE_ORIGINAL_PERLIN',
                'PERLIN2' : 'OPENGLES2_NOISE_LOW_QUALITY_PERLIN',
                'PERLIN3' : 'OPENGLES2_NOISE_HIGH_QUALITY_PERLIN'
                }

IMAGES_FORMATS = { 'RGB565'      : 'IMAGES_RGB565',
                   'RGB8'        : 'IMAGES_RGB8',
                   'RGBA8'       : 'IMAGES_RGBA8',
                   'ETC1_RGB8'   : 'IMAGES_ETC1_RGB8',
                   'ETC1_RGB8_A' : 'IMAGES_ETC1_RGB8',
                   'PVRTC_RGB4'  : 'IMAGES_PVRTC_RGB4',
                   'PVRTC_RGB2'  : 'IMAGES_PVRTC_RGB2',
                   'PVRTC_RGBA4' : 'IMAGES_PVRTC_RGBA4',
                   'PVRTC_RGBA2' : 'IMAGES_PVRTC_RGBA2'
                   }

FORMATS = { 'RGB565' : 'OPENGLES2_RGB565',
            'RGB8'   : 'OPENGLES2_RGB888',
            'RGBA8'  : 'OPENGLES2_RGBA8888'
            }

UNCOMPRESSED_FORMATS_MAP = { 'RGB565' : 'OPENGLES2_RGB565',
                             'RGB8'   : 'OPENGLES2_RGB888',
                             'RGBA8'  : 'OPENGLES2_RGBA8888'
                             }

COMPRESSED_FORMATS_MAP = { 'ETC1_RGB8'   : 'GL_ETC1_RGB8_OES',
                           'PVRTC_RGB4'  : 'GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG',
                           'PVRTC_RGB2'  : 'GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG',
                           'PVRTC_RGBA4' : 'GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG',
                           'PVRTC_RGBA2' : 'GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG'
                           }

#----------------------------------------------------------------------
def getTextureDataSize( w, h, format ):
    if format == 'RGB565':
        b = w * h * 2
    elif format == 'RGB8':
        b = w * h * 3
    elif format == 'RGBA8':
        b = w * h * 4
    elif format == 'ETC1_RGB8':
        b = w * h / 2
    elif format == 'ETC1_RGB8_A':
        b = w * h * 3 / 2
    elif format in [ 'PVRTC_RGB4', 'PVRTC_RGBA4' ]:
        b = ( max( w, 8 ) * max( h, 8 ) * 4 + 7 ) / 8
    elif format in [ 'PVRTC_RGB2', 'PVRTC_RGBA2' ]:
        b = ( max( w, 16 ) * max( h, 8 ) * 2 + 7 ) / 8
    else:
        raise 'Unexpected format ' + format

    return 'bytes=%d' % ( b, )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#
# Upload texture data and draw to the full screen surface via FBO/texture, if
# overdraw is specified. Overdraw specifies how many times the texture is drawn
# to the FBO/texture. Blending is enabled if overdraw > 1. FBO/texture size
# matches the texture size. Drawing to FBO/texture can include an operation such
# as rotate. Texture object for upload can be reused, or re-created per upload.
class TextureDataUploadBenchmark( Benchmark ):
    def __init__( self, width, height, type, format, overdraw, op, reuseTexture ):
        Benchmark.__init__( self )
        self.width        = width
        self.height       = height
        self.type         = type
        self.format       = format
        self.overdraw     = overdraw
        self.op           = op
        self.reuseTexture = reuseTexture
        size              = getTextureDataSize( self.width, self.height, self.format )

        drawString = ''
        if self.overdraw:
            s = 'draw'
            if self.overdraw > 1:
                s = '%d draws' % self.overdraw
            drawString = 'and %s ' % ( s, )
        if self.op:
            if not self.op in [ 'rot90', 'rot180', 'rot270', 'hflip', 'vflip' ]:
                raise "Unexpected operation %d" % self.op
            drawString = drawString + '%s ' % self.op
        reuseString = ''
        if self.reuseTexture:
            reuseString = ' reused'
        self.name = 'OPENGLES2 tex data upload %s%s (%dx%d %s %s%s)' % ( drawString,
                                                                         size,
                                                                         self.width,
                                                                         self.height,
                                                                         self.format,
                                                                         self.type,
                                                                         reuseString )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;

        void main()
        {
           gl_Position = vec4( gVertex, 0, 1 );
           uv = gTexCoord0;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv );
        }"""

        rawMaskedDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        uniform sampler2D gMask0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = vec4( texture2D( gTexture0, uv ).rgb, texture2D( gMask0, uv ).a );
        }"""

        # Select shaders.
        vertexShaderSource             = formatShader( rawVertexShaderSource )
        drawFragmentShaderSource       = formatShader( rawDrawFragmentShaderSource )
        maskedDrawFragmentShaderSource = formatShader( rawMaskedDrawFragmentShaderSource )

        if self.format == 'ETC1_RGB8_A':
            fboFragmentShaderSource = maskedDrawFragmentShaderSource
            masked = True
        else:
            fboFragmentShaderSource = drawFragmentShaderSource
            masked = False

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        # Setup OpenGLES2 surface and context.
        state = setupOpenGLES2( modules, indexTracker, target )

        # Check for required extensions.
        if self.format.startswith( 'ETC1' ):
            OpenGLES2.CheckExtension( 'GL_OES_compressed_ETC1_RGB8_texture' )
        elif self.format.startswith( 'PVRTC' ):
            OpenGLES2.CheckExtension( 'GL_IMG_texture_compression_pvrtc' )

        if self.overdraw:
            # Create mesh, program, and VBOs for drawing to the fbo.
            fboTexCoordAttribute = []
            fboTexCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) )

            mesh = MeshStripPlane( 2,
                                   2,
                                   MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                   None,
                                   None,
                                   fboTexCoordAttribute,
                                   MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                   None,
                                   MESH_CCW_WINDING,
                                   False )

            mesh.translate( [ -0.5, -0.5, 0 ] )
            mesh.scale( [ 2.0, 2.0 ] )

            fboWidth  = self.width
            fboHeight = self.height

            # Manipulate mesh texture coordinates according to the drawing
            # operation.
            if not self.op:
                mesh.texCoords[ 0 ] = [ 0, 0, 1, 0, 0, 1, 1, 1 ]
            elif self.op == 'rot90':
                mesh.texCoords[ 0 ] = [ 1, 0, 1, 1, 0, 0, 0, 1 ]
                fboWidth  = self.height
                fboHeight = self.width
            elif self.op == 'rot180':
                mesh.texCoords[ 0 ] = [ 1, 1, 0, 1, 1, 0, 0, 0 ]
            elif self.op == 'rot270':
                mesh.texCoords[ 0 ] = [ 0, 1, 0, 0, 1, 1, 1, 0 ]
                fboWidth  = self.height
                fboHeight = self.width
            elif self.op == 'hflip':
                mesh.texCoords[ 0 ] = [ 0, 1, 1, 1, 0, 0, 1, 0 ]
            elif self.op == 'vflip':
                mesh.texCoords[ 0 ] = [ 1, 0, 0, 0, 1, 1, 0, 1 ]

            fboVertexDataSetup       = setupVertexData( modules, indexTracker, mesh )
            fboProgram               = createProgram( modules,
                                                      indexTracker,
                                                      vertexShaderSource,
                                                      fboFragmentShaderSource,
                                                      fboVertexDataSetup )
            useProgram( modules, fboProgram )
            fboVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, fboVertexDataSetup )
            useVertexBufferData( modules, fboVertexBufferDataSetup, fboProgram )
            validateProgram( modules, fboProgram )

            if masked:
                fboMaskLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( fboMaskLocationIndex, fboProgram.programIndex, 'gMask0' )

            # For mesh, program and VBOs for drawing to the screen.
            screenTexCoordAttribute = []
            screenTexCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) )

            plane = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    screenTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            plane.translate( [ -0.5, -0.5, 0 ] )
            plane.scale( [ 2.0, 2.0 ] )

            drawVertexDataSetup       = setupVertexData( modules, indexTracker, plane )
            drawProgram               = createProgram( modules,
                                                       indexTracker,
                                                       vertexShaderSource,
                                                       drawFragmentShaderSource,
                                                       drawVertexDataSetup )
            useProgram( modules, drawProgram )
            drawVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, drawVertexDataSetup )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            validateProgram( modules, drawProgram )

            # Create a FBO and textures used for drawing.
            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_RGBA8888' )

            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        # Create texture data.
        textureDataIndex           = -1
        compressedTextureDataIndex = -1
        maskDataIndex              = -1

        if IMAGES_TYPES.has_key( self.type):
            if self.format == 'ETC1_RGB8_A':
                # Create a separate alpha mask and initialize it with perlin noise
                maskDataFormat = 'OPENGLES2_ALPHA8'
                maskDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateNoiseTextureData( maskDataIndex,
                                                  'OPENGLES2_NOISE_ORIGINAL_PERLIN',
                                                  [ 1, 1, 1, 1, 2, 2, 8 ],
                                                  self.width, self.height,
                                                  'OFF',
                                                  maskDataFormat )
                self.format = 'ETC1_RGB8'

            textureDataFormat = IMAGES_FORMATS[ self.format ]
            imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
            Images.CreateData( imagesDataIndex,
                               IMAGES_TYPES[ self.type ],
                               textureDataFormat,
                               self.width, self.height )

            dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            Images.CopyToOpenGLES2( imagesDataIndex,
                                    dataIndex )

            if COMPRESSED_FORMATS_MAP.has_key( self.format ):
                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateCompressedTextureData( compressedTextureDataIndex,
                                                       dataIndex,
                                                       self.width, self.height,
                                                       COMPRESSED_FORMATS_MAP[ self.format ] )

            else:
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateTextureData( textureDataIndex,
                                             dataIndex,
                                             self.width, self.height,
                                             UNCOMPRESSED_FORMATS_MAP[ self.format ] )

        else:
            textureDataFormat = FORMATS[ self.format ]
            textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )

            if self.type == 'SOLID':
                OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                                  [ 0.5, 0.5, 0.5, 0.5 ],
                                                  self.width, self.height,
                                                  'OFF',
                                                  textureDataFormat )
            elif self.type == 'CHECKER':
                OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                    [ 0.5, 0.5, 0.5, 0.5 ],
                                                    [ 1.0, 1.0, 1.0, 1.0 ],
                                                    4, 4,
                                                    self.width, self.height,
                                                    'OFF',
                                                    textureDataFormat )
            elif self.type == 'VGRADIENT':
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_VERTICAL',
                                                     [ 0.5, 0.5, 0.5, 0.5 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     self.width, self.height,
                                                     'OFF',
                                                     textureDataFormat )
            elif self.type == 'HGRADIENT':
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_HORIZONTAL',
                                                     [ 0.5, 0.5, 0.5, 0.5 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     self.width, self.height,
                                                     'OFF',
                                                     textureDataFormat )
            elif self.type == 'DGRADIENT':
                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES2_GRADIENT_DIAGONAL',
                                                     [ 0.5, 0.5, 0.5, 0.5 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     self.width, self.height,
                                                     'OFF',
                                                     textureDataFormat )
            elif self.type == 'RANDOM':
                OpenGLES2.CreateNoiseTextureData( textureDataIndex,
                                                  'OPENGLES2_NOISE_RANDOM',
                                                  [1,1,1,1,1,22],
                                                  self.width, self.height,
                                                  'OFF',
                                                  textureDataFormat )
            elif self.type == 'PERLIN1':
                OpenGLES2.CreateNoiseTextureData( textureDataIndex,
                                                  'OPENGLES2_NOISE_ORIGINAL_PERLIN',
                                                  [1,1,1,1,2,2,8],
                                                  self.width, self.height,
                                                  'OFF',
                                                  textureDataFormat )
            elif self.type == 'PERLIN2':
                OpenGLES2.CreateNoiseTextureData( textureDataIndex,
                                                  'OPENGLES2_NOISE_LOW_QUALITY_PERLIN',
                                                  [1,1,1,1,0.01,0.75,8,1,1,0.5],
                                                  self.width, self.height,
                                                  'OFF',
                                                  textureDataFormat )
            elif self.type == 'PERLIN3':
                OpenGLES2.CreateNoiseTextureData( textureDataIndex,
                                                  'OPENGLES2_NOISE_HIGH_QUALITY_PERLIN',
                                                  [1,1,1,1,0.01,0.75,8,1,1,0.5],
                                                  self.width, self.height,
                                                  'OFF',
                                                  textureDataFormat )
            else:
                raise "Unexpected type " + self.type

        # Create textures, if texture reuse is defined.
        textureIndex = -1
        maskIndex    = -1

        if self.reuseTexture:
            textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( textureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            if maskDataIndex >= 0:
                maskIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( maskIndex )
                OpenGLES2.ActiveTexture( 1 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', maskIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                OpenGLES2.ActiveTexture( 0 )

        if not self.overdraw:
            # In case of just texture data upload, clear the screen/default FBO
            # to green.
            OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
            target.swapBuffers( state )
        else:
            # In case of drawing to FBO/screen, set clear color to black and
            # enable blending.
            OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
            OpenGLES2.Enable( 'GL_BLEND' )
            OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        # Create textures, if not reused.
        if textureIndex < 0:
            textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( textureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            if maskDataIndex >= 0:
                maskIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( maskIndex )
                OpenGLES2.ActiveTexture( 1 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', maskIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                OpenGLES2.ActiveTexture( 0 )

        # Upload texture data.
        if textureDataIndex >= 0:
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
            OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        if compressedTextureDataIndex >= 0:
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
            OpenGLES2.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )

        if maskDataIndex >= 0:
            OpenGLES2.ActiveTexture( 1 )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', maskIndex )
            OpenGLES2.TexImage2D( maskDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.ActiveTexture( 0 )

        if self.overdraw:
            # Draw fbo/fbo texture.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
            OpenGLES2.Viewport( 0, 0, fboWidth, fboHeight )

            useProgram( modules, fboProgram )
            useVertexBufferData( modules, fboVertexBufferDataSetup, fboProgram )
            useTexture( modules, textureIndex, 0, fboProgram )

            if masked:
                OpenGLES2.ActiveTexture( 1 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', maskIndex )
                OpenGLES2.Uniformi( fboMaskLocationIndex, 1, [ 1 ] )

            # Clear and enable blending if overdraw.
            if self.overdraw > 1:
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                OpenGLES2.Enable( 'GL_BLEND' )

            # Draw ovedraws.
            for i in range( self.overdraw ):
                drawVertexBufferData( modules, fboVertexBufferDataSetup )

            # Draw fbo texture to screen.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
            OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

            useProgram( modules, drawProgram )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            useTexture( modules, fboTextureIndex, 0, drawProgram )

            if self.overdraw > 1:
                OpenGLES2.Disable( 'GL_BLEND' )

            drawVertexBufferData( modules, drawVertexBufferDataSetup )

            target.swapBuffers( state )

        # Delete textures, if not reused.
        if not self.reuseTexture:
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )
            OpenGLES2.DeleteTexture( textureIndex )
            if maskIndex >= 0:
                OpenGLES2.ActiveTexture( 1 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )
                OpenGLES2.DeleteTexture( maskIndex )
                OpenGLES2.ActiveTexture( 0 )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class UiBenchmark( Benchmark ):
    def __init__( self, fboSize, action, layers, reuseTextures, randomTextures ):
        Benchmark.__init__( self )
        self.fboSize        = fboSize
        self.action         = action
        self.layers         = layers
        self.reuseTextures  = reuseTextures
        self.randomTextures = randomTextures
        reuseString = ''
        if self.reuseTextures:
            reuseString = 'reused'
        randomString = ''
        if self.randomTextures:
            randomString = 'random'
            if reuseString:
                reuseString += ' '

        textureString = ''
        if reuseString or randomString:
            textureString = ' %s' % ( reuseString + randomString, )

        self.name = "OPENGLES2 ui %s %s%s" % ( actionToString( self.fboSize, self.action, self.layers ),
                                               layersToString( self.layers ),
                                               textureString, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]

        indexTracker = IndexTracker()

        # Shader source codes
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 position_in;
        attribute vec2 uv_in;
        uniform mat4 mvp;
        varying vec2 uv;

        void main()
        {
            gl_Position = mvp * position_in;
            uv = uv_in;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D tex0;
        varying vec2 uv;

        void main()
        {
            gl_FragColor = texture2D(tex0, uv);
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        if not self.fboSize:
            targetWidth  = screenWidth
            targetHeight = screenWidth
        else:
            targetWidth  = self.fboSize[ 0 ]
            targetHeight = self.fboSize[ 1 ]

        backgroundWidth  = targetWidth
        backgroundHeight = targetHeight
        panelsX          = PANELSX
        panelsY          = PANELSY
        panelWidth       = targetWidth / panelsX
        panelHeight      = targetHeight / panelsY
        groupsX          = GROUPSX
        groupsY          = GROUPSY
        groupWidth       = panelWidth / groupsX
        groupHeight      = panelHeight / groupsY
        iconsX           = ICONSX
        iconsY           = ICONSY
        iconWidth        = panelWidth / iconsX
        iconHeight       = panelHeight / iconsY
        overlayWidth     = targetWidth
        overlayHeight    = targetHeight

        backgroundTextureWidth,     backgroundTextureS  = toPot( backgroundWidth, False )
        backgroundTextureHeight,    backgroundTextureT  = toPot( backgroundHeight, False )
        panelTextureWidth,          panelTextureS       = toPot( panelWidth, False )
        panelTextureHeight,         panelTextureT       = toPot( panelHeight, False )
        groupTextureWidth,          groupTextureS       = toPot( groupWidth, False )
        groupTextureHeight,         groupTextureT       = toPot( groupHeight, False )
        iconTextureWidth,           iconTextureS        = toPot( iconWidth, False )
        iconTextureHeight,          iconTextureT        = toPot( iconHeight, False )
        overlayTextureWidth,        overlayTextureS     = toPot( overlayWidth, False )
        overlayTextureHeight,       overlayTextureT     = toPot( overlayHeight, False )

        # Create texture data.
        if hasLayer( OVERLAY, self.layers ):
            overlays             = 1
            overlayTextureFormat = layerTextureFormat( OVERLAY, self.layers )
        elif hasLayer( OVERLAY4X, self.layers ):
            overlays             = 4
            overlayTextureFormat = layerTextureFormat( OVERLAY4X, self.layers )

        if hasLayer( BACKGROUND, self.layers ):
            backgroundTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            backgroundTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

            textureFormat = layerTextureFormat( BACKGROUND, self.layers )

            if not self.randomTextures:
                imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
                Images.CreateData( imagesDataIndex,
                                   'IMAGES_FLOWER',
                                   IMAGES_FORMATS[ textureFormat ],
                                   backgroundTextureWidth,
                                   backgroundTextureHeight )

                dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )

                OpenGLES2.CreateTextureData( backgroundTextureDataIndex,
                                             dataIndex,
                                             backgroundTextureWidth,
                                             backgroundTextureHeight,
                                             FORMATS[ textureFormat ] )

            else:
                OpenGLES2.CreateNoiseTextureData( backgroundTextureDataIndex,
                                                  'OPENGLES2_NOISE_RANDOM',
                                                  [1.0,1.0,1.0,1.0,1,1],
                                                  backgroundTextureWidth,
                                                  backgroundTextureHeight,
                                                  'OFF',
                                                  FORMATS[ textureFormat ] )
            OpenGLES2.GenTexture( backgroundTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            if self.action == DRAW:
                OpenGLES2.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )

        if hasLayer( PANELS, self.layers ):
            panelTextureDataIndices = []
            panelTextureIndices     = []
            panelOrigins            = []

            textureFormat = layerTextureFormat( PANELS, self.layers )

            total = panelsX * panelsY + 1
            for y in range( panelsY ):
                for x in range( panelsX ):
                    textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                    textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

                    panelTextureDataIndices += [ textureDataIndex ]
                    panelTextureIndices     += [ textureIndex ]
                    panelOrigins            += [ ( panelWidth * x, panelHeight * y, ) ]

                    r = 1.0 * ( ( x + ( y * panelsX ) + 1 ) / float( total ) )

                    if self.randomTextures:
                        OpenGLES2.CreateNoiseTextureData( textureDataIndex,
                                                          'OPENGLES2_NOISE_RANDOM',
                                                          [1.0,1.0,1.0,1.0,1,2],
                                                          panelTextureWidth,
                                                          panelTextureHeight,
                                                          'OFF',
                                                          FORMATS[ textureFormat ] )
                    else:
                        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                             'OPENGLES2_GRADIENT_HORIZONTAL',
                                                             [ 0.0, r, 0.0, 0.5 ],
                                                             [ 0.2, r, 0.2, 0.5 ],
                                                             panelTextureWidth,
                                                             panelTextureHeight,
                                                             'OFF',
                                                             FORMATS[ textureFormat ] )

                    OpenGLES2.GenTexture( textureIndex )
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                    OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

                    if self.action == DRAW:
                        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        if hasLayer( GROUPS, self.layers ):
            groupTextureDataIndices = []
            groupTextureIndices     = []
            groupOrigins            = []

            textureFormat = layerTextureFormat( GROUPS, self.layers )

            total = groupsX * groupsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( groupsY ):
                        for x in range( groupsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

                            groupTextureDataIndices += [ textureDataIndex ]
                            groupTextureIndices     += [ textureIndex ]
                            groupOrigins            += [ ( px * panelWidth + groupWidth * x,
                                                           py * panelHeight + groupHeight * y, ) ]

                            r = 1.0 * ( x + ( y * groupsX ) + 1 ) / float( total )

                            if self.randomTextures:
                                OpenGLES2.CreateNoiseTextureData( textureDataIndex,
                                                                  'OPENGLES2_NOISE_RANDOM',
                                                                  [1.0,1.0,1.0,1.0,1,3],
                                                                  groupTextureWidth,
                                                                  groupTextureHeight,
                                                                  'OFF',
                                                                  FORMATS[ textureFormat ] )
                            else:
                                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                                     'OPENGLES2_GRADIENT_HORIZONTAL',
                                                                     [ 0.0, 0.0, r, 0.5 ],
                                                                     [ 0.2, 0.2, r, 0.5 ],
                                                                     groupTextureWidth,
                                                                     groupTextureHeight,
                                                                     'OFF',
                                                                     FORMATS[ textureFormat ] )

                            OpenGLES2.GenTexture( textureIndex )
                            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

                            if self.action == DRAW:
                                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        if hasLayer( ICONS, self.layers ):
            iconTextureDataIndices  = []
            iconTextureIndices      = []
            iconOrigins             = []

            textureFormat = layerTextureFormat( ICONS, self.layers )

            total = iconsX * iconsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( iconsY ):
                        for x in range( iconsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

                            iconTextureDataIndices  += [ textureDataIndex ]
                            iconTextureIndices      += [ textureIndex ]
                            iconOrigins             += [ ( px * panelWidth + iconWidth * x,
                                                           py * panelHeight + iconHeight * y, ) ]

                            r = 1.0 * ( x + ( y * iconsX ) + 1 ) / float( total )

                            if self.randomTextures:
                                OpenGLES2.CreateNoiseTextureData( textureDataIndex,
                                                                  'OPENGLES2_NOISE_RANDOM',
                                                                  [1.0,1.0,1.0,1.0,1,4],
                                                                  iconTextureWidth,
                                                                  iconTextureHeight,
                                                                  'OFF',
                                                                  FORMATS[ textureFormat ] )
                            else:
                                OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                                    [ 0.3, 0.3, r, 0.5 ],
                                                                    [ 0.2, 0.2, r, 0.5 ],
                                                                    2, 2,
                                                                    iconTextureWidth,
                                                                    iconTextureHeight,
                                                                    'OFF',
                                                                    FORMATS[ textureFormat ] )

                            OpenGLES2.GenTexture( textureIndex )
                            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

                            if self.action == DRAW:
                                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        if hasLayer( OVERLAY, self.layers ) or hasLayer( OVERLAY4X, self.layers ):
            overlayTextureIndices = []
            for i in range( overlays ):
                overlayTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                overlayTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

                if not self.randomTextures:
                    OpenGLES2.CreateCheckerTextureData( overlayTextureDataIndex,
                                                        [ 0.7, 0.3, 0.0, 0.3 ],
                                                        [ 0.7, 0.2, 0.0, 0.7 ],
                                                        2, 2,
                                                        overlayTextureWidth,
                                                        overlayTextureHeight,
                                                        'OFF',
                                                        FORMATS[ overlayTextureFormat ] )

                else:
                    OpenGLES2.CreateNoiseTextureData( overlayTextureDataIndex,
                                                      'OPENGLES2_NOISE_RANDOM',
                                                      [1.0,1.0,1.0,1.0,1,1],
                                                      overlayTextureWidth,
                                                      overlayTextureHeight,
                                                      'OFF',
                                                      FORMATS[ overlayTextureFormat ] )

                OpenGLES2.GenTexture( overlayTextureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', overlayTextureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

                if self.action == DRAW:
                    OpenGLES2.TexImage2D( overlayTextureDataIndex, 'GL_TEXTURE_2D' )

                overlayTextureIndices.append( overlayTextureIndex )

        # Create geometry.
        if hasLayer( BACKGROUND, self.layers ):
            backgroundTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                                   2,
                                                                   [ backgroundTextureS,
                                                                     backgroundTextureT ] ) ]

            background = MeshStripPlane( 2,
                                         2,
                                         MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                         None,
                                         None,
                                         backgroundTexCoordAttribute,
                                         MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                         None,
                                         MESH_CCW_WINDING,
                                         False )

            background.translate( [ -0.5, -0.5, 0 ] )
            background.scale( [ 2.0 * backgroundWidth / float( targetWidth ),
                                2.0 * backgroundHeight / float( targetHeight ) ] )

            backgroundVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundVertexArrayIndex, background.vertexGLType )
            OpenGLES2.AppendToArray( backgroundVertexArrayIndex, background.vertices )

            backgroundTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundTexCoordArrayIndex, background.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( backgroundTexCoordArrayIndex, background.texCoords[ 0 ] )

            backgroundIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundIndexArrayIndex, background.indexGLType );
            OpenGLES2.AppendToArray( backgroundIndexArrayIndex, background.indices )

        if hasLayer( PANELS, self.layers ):
            panelTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ panelTextureS,
                                                                panelTextureT ] ) ]

            panel = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    panelTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            panel.translate( [ -0.5, -0.5, 0 ] )
            panel.scale( [ 2.0 * panelWidth / float( targetWidth ),
                           2.0 * panelHeight / float( targetHeight ) ] )

            panelVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelVertexArrayIndex, panel.vertexGLType )
            OpenGLES2.AppendToArray( panelVertexArrayIndex, panel.vertices )

            panelTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelTexCoordArrayIndex, panel.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( panelTexCoordArrayIndex, panel.texCoords[ 0 ] )

            panelIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelIndexArrayIndex, panel.indexGLType );
            OpenGLES2.AppendToArray( panelIndexArrayIndex, panel.indices )

        if hasLayer( GROUPS, self.layers ):
            groupTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ groupTextureS,
                                                                groupTextureT ] ) ]

            group = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    groupTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            group.translate( [ -0.5, -0.5, 0 ] )
            group.scale( [ 2.0 * groupWidth / float( targetWidth ),
                           2.0 * groupHeight / float( targetHeight ) ] )

            groupVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupVertexArrayIndex, group.vertexGLType )
            OpenGLES2.AppendToArray( groupVertexArrayIndex, group.vertices )

            groupTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupTexCoordArrayIndex, group.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( groupTexCoordArrayIndex, group.texCoords[ 0 ] )

            groupIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupIndexArrayIndex, group.indexGLType );
            OpenGLES2.AppendToArray( groupIndexArrayIndex, group.indices )

        if hasLayer( ICONS, self.layers ):
            iconTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                             2,
                                                             [ iconTextureS,
                                                               iconTextureT ] ) ]

            icon = MeshStripPlane( 2,
                                   2,
                                   MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                   None,
                                   None,
                                   iconTexCoordAttribute,
                                   MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                   None,
                                   MESH_CCW_WINDING,
                                   False )

            icon.translate( [ -0.5, -0.5, 0 ] )
            icon.scale( [ 2.0 * iconWidth / float( targetWidth ),
                          2.0 * iconHeight / float( targetHeight ) ] )

            iconVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconVertexArrayIndex, icon.vertexGLType )
            OpenGLES2.AppendToArray( iconVertexArrayIndex, icon.vertices )

            iconTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconTexCoordArrayIndex, icon.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( iconTexCoordArrayIndex, icon.texCoords[ 0 ] )

            iconIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconIndexArrayIndex, icon.indexGLType );
            OpenGLES2.AppendToArray( iconIndexArrayIndex, icon.indices )

        if hasLayer( OVERLAY, self.layers ) or hasLayer( OVERLAY4X, self.layers ):
            overlayTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                                2,
                                                                [ overlayTextureS,
                                                                  overlayTextureT ] ) ]

            overlay = MeshStripPlane( 2,
                                      2,
                                      MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                      None,
                                      None,
                                      overlayTexCoordAttribute,
                                      MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                      None,
                                      MESH_CCW_WINDING,
                                      False )

            overlay.translate( [ -0.5, -0.5, 0 ] )
            overlay.scale( [ 2.0 * overlayWidth / float( targetWidth ),
                             2.0 * overlayHeight / float( targetHeight ) ] )

            overlayVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( overlayVertexArrayIndex, overlay.vertexGLType )
            OpenGLES2.AppendToArray( overlayVertexArrayIndex, overlay.vertices )

            overlayTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( overlayTexCoordArrayIndex, overlay.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( overlayTexCoordArrayIndex, overlay.texCoords[ 0 ] )

            overlayIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( overlayIndexArrayIndex, overlay.indexGLType );
            OpenGLES2.AppendToArray( overlayIndexArrayIndex, overlay.indices )

        # Create shaders.
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )

        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )

        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, "position_in" )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex, texCoordLocationIndex, "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.ValidateProgram( programIndex )
        OpenGLES2.UseProgram( programIndex )
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        mVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mVPLocationIndex, programIndex, 'mvp' )

        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )

        if ( self.action == DRAW or self.action == UPLOAD_AND_DRAW ) and self.fboSize:
            # Create a FBO and textures (RGBA8) used for drawing.
            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, targetWidth, targetHeight, 'OFF', 'OPENGLES2_RGBA8888' )

            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            # Render into fbo/texture, if defined.
            if self.fboSize:
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
                OpenGLES2.Viewport( 0, 0, targetWidth, targetHeight )

            # Clear the target if drawing is requested and the layers do not include background.
            if not hasLayer( BACKGROUND, self.layers ):
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        # Do texture data upload if requested
        if self.action == UPLOAD or self.action == UPLOAD_AND_DRAW:
            if hasLayer( BACKGROUND, self.layers ):
                if not self.reuseTextures:
                    OpenGLES2.DeleteTexture( backgroundTextureIndex )
                    OpenGLES2.GenTexture( backgroundTextureIndex )
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                    OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                else:
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )

                OpenGLES2.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )

            if hasLayer( PANELS, self.layers ):
                for i in range( len( panelTextureDataIndices ) ):
                    if not self.reuseTextures:
                        OpenGLES2.DeleteTexture( panelTextureIndices[ i ] )
                        OpenGLES2.GenTexture( panelTextureIndices[ i ] )
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                    else:
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )

                    OpenGLES2.TexImage2D( panelTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if hasLayer( GROUPS, self.layers ):
                for i in range( len( groupTextureDataIndices ) ):
                    if not self.reuseTextures:
                        OpenGLES2.DeleteTexture( groupTextureIndices[ i ] )
                        OpenGLES2.GenTexture( groupTextureIndices[ i ] )
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                    else:
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )

                    OpenGLES2.TexImage2D( groupTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if hasLayer( ICONS, self.layers ):
                for i in range( len( iconTextureDataIndices ) ):
                    if not self.reuseTextures:
                        OpenGLES2.DeleteTexture( iconTextureIndices[ i ] )
                        OpenGLES2.GenTexture( iconTextureIndices[ i ] )
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                    else:
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )

                    OpenGLES2.TexImage2D( iconTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if hasLayer( OVERLAY, self.layers ) or hasLayer( OVERLAY4X, self.layers ):
                for i in range( overlays ):
                    if not self.reuseTextures:
                        OpenGLES2.DeleteTexture( overlayTextureIndices[ i ] )
                        OpenGLES2.GenTexture( overlayTextureIndices[ i ] )
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', overlayTextureIndices[ i ] )
                        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                    else:
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', overlayTextureIndices[ i ] )

                    OpenGLES2.TexImage2D( overlayTextureDataIndex, 'GL_TEXTURE_2D' )

        # Draw if drawing is requested
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            if hasLayer( BACKGROUND, self.layers ):
                OpenGLES2.Disable( 'GL_BLEND' )

                OpenGLES2.UniformMatrix( mVPLocationIndex,
                                         1,
                                         [ 1.0, 0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0, 0.0,
                                           0.0, 0.0, 1.0, 0.0,
                                           0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex, backgroundVertexArrayIndex, 2, 'OFF', 0 )
                OpenGLES2.VertexAttribPointer( texCoordLocationIndex, backgroundTexCoordArrayIndex, 2, 'OFF', 0 )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

                OpenGLES2.DrawElements( background.glMode, len( background.indices ), 0, backgroundIndexArrayIndex )

            if hasLayer( PANELS, self.layers ):
                OpenGLES2.Enable( 'GL_BLEND' )

                for i in range( len( panelTextureIndices ) ):
                    origin = panelOrigins[ i ]
                    tx = ( -1.0 + panelWidth / float( targetWidth ) ) + ( 2.0 * origin[ 0 ] / float ( targetWidth ) )
                    ty = ( -1.0 + panelHeight / float( targetHeight ) ) + ( 2.0 * origin[ 1 ] / float ( targetHeight ) )

                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex, panelVertexArrayIndex, 2, 'OFF', 0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex, panelTexCoordArrayIndex, 2, 'OFF', 0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

                    OpenGLES2.DrawElements( panel.glMode, len( panel.indices ), 0, panelIndexArrayIndex )

            if hasLayer( GROUPS, self.layers ):
                OpenGLES2.Enable( 'GL_BLEND' )

                for i in range( len( groupTextureIndices ) ):
                    origin = groupOrigins[ i ]
                    tx = ( -1.0 + groupWidth / float( targetWidth ) ) + ( 2.0 * origin[ 0 ] / float ( targetWidth ) )
                    ty = ( -1.0 + groupHeight / float( targetHeight ) ) + ( 2.0 * origin[ 1 ] / float ( targetHeight ) )

                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex, groupVertexArrayIndex, 2, 'OFF', 0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex, groupTexCoordArrayIndex, 2, 'OFF', 0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

                    OpenGLES2.DrawElements( group.glMode, len( group.indices ), 0, groupIndexArrayIndex )

            if hasLayer( ICONS, self.layers ):
                OpenGLES2.Enable( 'GL_BLEND' )

                for i in range( len( iconTextureIndices ) ):
                    origin = iconOrigins[ i ]
                    tx = ( -1.0 + iconWidth / float( targetWidth ) ) + ( 2.0 * origin[ 0 ] / float ( targetWidth ) )
                    ty = ( -1.0 + iconHeight / float( targetHeight ) ) + ( 2.0 * origin[ 1 ] / float ( targetHeight ) )

                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex, iconVertexArrayIndex, 2, 'OFF', 0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex, iconTexCoordArrayIndex, 2, 'OFF', 0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

                    OpenGLES2.DrawElements( icon.glMode, len( icon.indices ), 0, iconIndexArrayIndex )

            if hasLayer( OVERLAY, self.layers ) or hasLayer( OVERLAY4X, self.layers ):
                OpenGLES2.Enable( 'GL_BLEND' )

                OpenGLES2.UniformMatrix( mVPLocationIndex,
                                         1,
                                         [ 1.0, 0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0, 0.0,
                                           0.0, 0.0, 1.0, 0.0,
                                           0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex, overlayVertexArrayIndex, 2, 'OFF', 0 )
                OpenGLES2.VertexAttribPointer( texCoordLocationIndex, overlayTexCoordArrayIndex, 2, 'OFF', 0 )

                for i in range( overlays ):
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', overlayTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

                    OpenGLES2.DrawElements( overlay.glMode, len( overlay.indices ), 0, overlayIndexArrayIndex )

            # Render fbo/texture into screen, if defined.
            if self.fboSize:
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

                OpenGLES2.Disable( 'GL_BLEND' )

                OpenGLES2.UniformMatrix( mVPLocationIndex,
                                         1,
                                         [ 1.0, 0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0, 0.0,
                                           0.0, 0.0, 1.0, 0.0,
                                           0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex, backgroundVertexArrayIndex, 2, 'OFF', 0 )
                OpenGLES2.VertexAttribPointer( texCoordLocationIndex, backgroundTexCoordArrayIndex, 2, 'OFF', 0 )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
                OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

                OpenGLES2.DrawElements( background.glMode, len( background.indices ), 0, backgroundIndexArrayIndex )

            target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class UiThroughputBenchmark( Benchmark ):
    def __init__( self, fboSize, action, stackSize ):
        Benchmark.__init__( self )
        self.fboSize        = fboSize
        self.action         = action
        self.stackSize      = stackSize
        self.layers         = [ ( BACKGROUND, 'RGB8' ),
                                ( PANELS,     'RGBA8' ),
                                ( GROUPS,     'RGBA8' ),
                                ( ICONS,      'RGBA8' ) ]
        self.name = "OPENGLES2 ui throughput %d stacks of %s %s" % ( self.stackSize,
                                                                     actionToString( self.fboSize, self.action, self.layers ),
                                                                     layersToString( self.layers ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]

        indexTracker = IndexTracker()

        # Shader source codes
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 position_in;
        attribute vec2 uv_in;
        uniform mat4 mvp;
        varying vec2 uv;

        void main()
        {
            gl_Position = mvp * position_in;
            uv = uv_in;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D tex0;
        varying vec2 uv;

        void main()
        {
            gl_FragColor = texture2D(tex0, uv);
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        if not self.fboSize:
            targetWidth  = screenWidth
            targetHeight = screenWidth
        else:
            targetWidth  = self.fboSize[ 0 ]
            targetHeight = self.fboSize[ 1 ]

        backgroundWidth  = targetWidth
        backgroundHeight = targetHeight
        panelsX          = PANELSX
        panelsY          = PANELSY
        panelWidth       = targetWidth / panelsX
        panelHeight      = targetHeight / panelsY
        groupsX          = GROUPSX
        groupsY          = GROUPSY
        groupWidth       = panelWidth / groupsX
        groupHeight      = panelHeight / groupsY
        iconsX           = ICONSX
        iconsY           = ICONSY
        iconWidth        = panelWidth / iconsX
        iconHeight       = panelHeight / iconsY

        backgroundTextureWidth,     backgroundTextureS  = toPot( backgroundWidth, False )
        backgroundTextureHeight,    backgroundTextureT  = toPot( backgroundHeight, False )
        panelTextureWidth,          panelTextureS       = toPot( panelWidth, False )
        panelTextureHeight,         panelTextureT       = toPot( panelHeight, False )
        groupTextureWidth,          groupTextureS       = toPot( groupWidth, False )
        groupTextureHeight,         groupTextureT       = toPot( groupHeight, False )
        iconTextureWidth,           iconTextureS        = toPot( iconWidth, False )
        iconTextureHeight,          iconTextureT        = toPot( iconHeight, False )

        if hasLayer( BACKGROUND, self.layers ):
            backgroundTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            backgroundTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

            textureFormat = layerTextureFormat( BACKGROUND, self.layers )

            imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
            Images.CreateData( imagesDataIndex,
                               'IMAGES_FLOWER',
                               IMAGES_FORMATS[ textureFormat ],
                               backgroundTextureWidth,
                               backgroundTextureHeight )

            dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )

            OpenGLES2.CreateTextureData( backgroundTextureDataIndex,
                                         dataIndex,
                                         backgroundTextureWidth,
                                         backgroundTextureHeight,
                                         FORMATS[ textureFormat ] )

            OpenGLES2.GenTexture( backgroundTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            if self.action == DRAW:
                OpenGLES2.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )

        if hasLayer( PANELS, self.layers ):
            panelTextureDataIndices = []
            panelTextureIndices     = []
            panelOrigins            = []

            textureFormat = layerTextureFormat( PANELS, self.layers )

            total = panelsX * panelsY + 1
            for y in range( panelsY ):
                for x in range( panelsX ):
                    textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                    textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

                    panelTextureDataIndices += [ textureDataIndex ]
                    panelTextureIndices     += [ textureIndex ]
                    panelOrigins            += [ ( panelWidth * x, panelHeight * y, ) ]

                    r = 1.0 * ( ( x + ( y * panelsX ) + 1 ) / float( total ) )

                    OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                         'OPENGLES2_GRADIENT_HORIZONTAL',
                                                         [ 0.0, r, 0.0, 0.5 ],
                                                         [ 0.2, r, 0.2, 0.5 ],
                                                         panelTextureWidth,
                                                         panelTextureHeight,
                                                         'OFF',
                                                         FORMATS[ textureFormat ] )

                    OpenGLES2.GenTexture( textureIndex )
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                    OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

                    if self.action == DRAW:
                        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        if hasLayer( GROUPS, self.layers ):
            groupTextureDataIndices = []
            groupTextureIndices     = []
            groupOrigins            = []

            textureFormat = layerTextureFormat( GROUPS, self.layers )

            total = groupsX * groupsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( groupsY ):
                        for x in range( groupsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

                            groupTextureDataIndices += [ textureDataIndex ]
                            groupTextureIndices     += [ textureIndex ]
                            groupOrigins            += [ ( px * panelWidth + groupWidth * x,
                                                           py * panelHeight + groupHeight * y, ) ]

                            r = 1.0 * ( x + ( y * groupsX ) + 1 ) / float( total )

                            OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                                 'OPENGLES2_GRADIENT_HORIZONTAL',
                                                                 [ 0.0, 0.0, r, 0.5 ],
                                                                 [ 0.2, 0.2, r, 0.5 ],
                                                                 groupTextureWidth,
                                                                 groupTextureHeight,
                                                                 'OFF',
                                                                 FORMATS[ textureFormat ] )

                            OpenGLES2.GenTexture( textureIndex )
                            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

                            if self.action == DRAW:
                                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        if hasLayer( ICONS, self.layers ):
            iconTextureDataIndices  = []
            iconTextureIndices      = []
            iconOrigins             = []

            textureFormat = layerTextureFormat( ICONS, self.layers )

            total = iconsX * iconsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( iconsY ):
                        for x in range( iconsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

                            iconTextureDataIndices  += [ textureDataIndex ]
                            iconTextureIndices      += [ textureIndex ]
                            iconOrigins             += [ ( px * panelWidth + iconWidth * x,
                                                           py * panelHeight + iconHeight * y, ) ]

                            r = 1.0 * ( x + ( y * iconsX ) + 1 ) / float( total )

                            OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                                [ 0.3, 0.3, r, 0.5 ],
                                                                [ 0.2, 0.2, r, 0.5 ],
                                                                2, 2,
                                                                iconTextureWidth,
                                                                iconTextureHeight,
                                                                'OFF',
                                                                FORMATS[ textureFormat ] )

                            OpenGLES2.GenTexture( textureIndex )
                            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

                            if self.action == DRAW:
                                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        # Create geometry.
        if hasLayer( BACKGROUND, self.layers ):
            backgroundTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                                   2,
                                                                   [ backgroundTextureS,
                                                                     backgroundTextureT ] ) ]

            background = MeshStripPlane( 2,
                                         2,
                                         MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                         None,
                                         None,
                                         backgroundTexCoordAttribute,
                                         MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                         None,
                                         MESH_CCW_WINDING,
                                         False )

            background.translate( [ -0.5, -0.5, 0 ] )
            background.scale( [ 2.0 * backgroundWidth / float( targetWidth ),
                                2.0 * backgroundHeight / float( targetHeight ) ] )

            backgroundVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundVertexArrayIndex, background.vertexGLType )
            OpenGLES2.AppendToArray( backgroundVertexArrayIndex, background.vertices )

            backgroundTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundTexCoordArrayIndex, background.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( backgroundTexCoordArrayIndex, background.texCoords[ 0 ] )

            backgroundIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundIndexArrayIndex, background.indexGLType );
            OpenGLES2.AppendToArray( backgroundIndexArrayIndex, background.indices )

        if hasLayer( PANELS, self.layers ):
            panelTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ panelTextureS,
                                                                panelTextureT ] ) ]

            panel = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    panelTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            panel.translate( [ -0.5, -0.5, 0 ] )
            panel.scale( [ 2.0 * panelWidth / float( targetWidth ),
                           2.0 * panelHeight / float( targetHeight ) ] )

            panelVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelVertexArrayIndex, panel.vertexGLType )
            OpenGLES2.AppendToArray( panelVertexArrayIndex, panel.vertices )

            panelTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelTexCoordArrayIndex, panel.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( panelTexCoordArrayIndex, panel.texCoords[ 0 ] )

            panelIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelIndexArrayIndex, panel.indexGLType );
            OpenGLES2.AppendToArray( panelIndexArrayIndex, panel.indices )

        if hasLayer( GROUPS, self.layers ):
            groupTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ groupTextureS,
                                                                groupTextureT ] ) ]

            group = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    groupTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            group.translate( [ -0.5, -0.5, 0 ] )
            group.scale( [ 2.0 * groupWidth / float( targetWidth ),
                           2.0 * groupHeight / float( targetHeight ) ] )

            groupVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupVertexArrayIndex, group.vertexGLType )
            OpenGLES2.AppendToArray( groupVertexArrayIndex, group.vertices )

            groupTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupTexCoordArrayIndex, group.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( groupTexCoordArrayIndex, group.texCoords[ 0 ] )

            groupIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupIndexArrayIndex, group.indexGLType );
            OpenGLES2.AppendToArray( groupIndexArrayIndex, group.indices )

        if hasLayer( ICONS, self.layers ):
            iconTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                             2,
                                                             [ iconTextureS,
                                                               iconTextureT ] ) ]

            icon = MeshStripPlane( 2,
                                   2,
                                   MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                   None,
                                   None,
                                   iconTexCoordAttribute,
                                   MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                   None,
                                   MESH_CCW_WINDING,
                                   False )

            icon.translate( [ -0.5, -0.5, 0 ] )
            icon.scale( [ 2.0 * iconWidth / float( targetWidth ),
                          2.0 * iconHeight / float( targetHeight ) ] )

            iconVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconVertexArrayIndex, icon.vertexGLType )
            OpenGLES2.AppendToArray( iconVertexArrayIndex, icon.vertices )

            iconTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconTexCoordArrayIndex, icon.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( iconTexCoordArrayIndex, icon.texCoords[ 0 ] )

            iconIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconIndexArrayIndex, icon.indexGLType );
            OpenGLES2.AppendToArray( iconIndexArrayIndex, icon.indices )

        # Create shaders.
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )

        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )

        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, "position_in" )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex, texCoordLocationIndex, "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.ValidateProgram( programIndex )
        OpenGLES2.UseProgram( programIndex )
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        mVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mVPLocationIndex, programIndex, 'mvp' )

        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )

        if ( self.action == DRAW or self.action == UPLOAD_AND_DRAW ) and self.fboSize:
            # Create a FBO and textures (RGBA8) used for drawing.
            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, targetWidth, targetHeight, 'OFF', 'OPENGLES2_RGBA8888' )

            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            # Render into fbo/texture, if defined.
            if self.fboSize:
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
                OpenGLES2.Viewport( 0, 0, targetWidth, targetHeight )

            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        for s in range( self.stackSize ):
            # Do texture data upload if requested
            if self.action == UPLOAD or self.action == UPLOAD_AND_DRAW:
                if hasLayer( BACKGROUND, self.layers ):
                    OpenGLES2.DeleteTexture( backgroundTextureIndex )
                    OpenGLES2.GenTexture( backgroundTextureIndex )
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                    OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                    OpenGLES2.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )

                if hasLayer( PANELS, self.layers ):
                    for i in range( len( panelTextureDataIndices ) ):
                        OpenGLES2.DeleteTexture( panelTextureIndices[ i ] )
                        OpenGLES2.GenTexture( panelTextureIndices[ i ] )
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                        OpenGLES2.TexImage2D( panelTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

                if hasLayer( GROUPS, self.layers ):
                    for i in range( len( groupTextureDataIndices ) ):
                        OpenGLES2.DeleteTexture( groupTextureIndices[ i ] )
                        OpenGLES2.GenTexture( groupTextureIndices[ i ] )
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                        OpenGLES2.TexImage2D( groupTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

                if hasLayer( ICONS, self.layers ):
                    for i in range( len( iconTextureDataIndices ) ):
                        OpenGLES2.DeleteTexture( iconTextureIndices[ i ] )
                        OpenGLES2.GenTexture( iconTextureIndices[ i ] )
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                        OpenGLES2.TexImage2D( iconTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            # Draw if drawing is requested
            if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
                OpenGLES2.Disable( 'GL_BLEND' )

                if hasLayer( BACKGROUND, self.layers ):
                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               0.0, 0.0, 0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex, backgroundVertexArrayIndex, 2, 'OFF', 0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex, backgroundTexCoordArrayIndex, 2, 'OFF', 0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

                    OpenGLES2.DrawElements( background.glMode, len( background.indices ), 0, backgroundIndexArrayIndex )

                if hasLayer( PANELS, self.layers ):
                    for i in range( len( panelTextureIndices ) ):
                        origin = panelOrigins[ i ]
                        tx = ( -1.0 + panelWidth / float( targetWidth ) ) + ( 2.0 * origin[ 0 ] / float ( targetWidth ) )
                        ty = ( -1.0 + panelHeight / float( targetHeight ) ) + ( 2.0 * origin[ 1 ] / float ( targetHeight ) )

                        OpenGLES2.UniformMatrix( mVPLocationIndex,
                                                 1,
                                                 [ 1.0, 0.0, 0.0, 0.0,
                                                   0.0, 1.0, 0.0, 0.0,
                                                   0.0, 0.0, 1.0, 0.0,
                                                   tx,  ty,  0.0, 1.0 ] )

                        OpenGLES2.VertexAttribPointer( vertexLocationIndex, panelVertexArrayIndex, 2, 'OFF', 0 )
                        OpenGLES2.VertexAttribPointer( texCoordLocationIndex, panelTexCoordArrayIndex, 2, 'OFF', 0 )

                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

                        OpenGLES2.DrawElements( panel.glMode, len( panel.indices ), 0, panelIndexArrayIndex )

                if hasLayer( GROUPS, self.layers ):
                    for i in range( len( groupTextureIndices ) ):
                        origin = groupOrigins[ i ]
                        tx = ( -1.0 + groupWidth / float( targetWidth ) ) + ( 2.0 * origin[ 0 ] / float ( targetWidth ) )
                        ty = ( -1.0 + groupHeight / float( targetHeight ) ) + ( 2.0 * origin[ 1 ] / float ( targetHeight ) )

                        OpenGLES2.UniformMatrix( mVPLocationIndex,
                                                 1,
                                                 [ 1.0, 0.0, 0.0, 0.0,
                                                   0.0, 1.0, 0.0, 0.0,
                                                   0.0, 0.0, 1.0, 0.0,
                                                   tx,  ty,  0.0, 1.0 ] )

                        OpenGLES2.VertexAttribPointer( vertexLocationIndex, groupVertexArrayIndex, 2, 'OFF', 0 )
                        OpenGLES2.VertexAttribPointer( texCoordLocationIndex, groupTexCoordArrayIndex, 2, 'OFF', 0 )

                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

                        OpenGLES2.DrawElements( group.glMode, len( group.indices ), 0, groupIndexArrayIndex )

                if hasLayer( ICONS, self.layers ):
                    for i in range( len( iconTextureIndices ) ):
                        origin = iconOrigins[ i ]
                        tx = ( -1.0 + iconWidth / float( targetWidth ) ) + ( 2.0 * origin[ 0 ] / float ( targetWidth ) )
                        ty = ( -1.0 + iconHeight / float( targetHeight ) ) + ( 2.0 * origin[ 1 ] / float ( targetHeight ) )

                        OpenGLES2.UniformMatrix( mVPLocationIndex,
                                                 1,
                                                 [ 1.0, 0.0, 0.0, 0.0,
                                                   0.0, 1.0, 0.0, 0.0,
                                                   0.0, 0.0, 1.0, 0.0,
                                                   tx,  ty,  0.0, 1.0 ] )

                        OpenGLES2.VertexAttribPointer( vertexLocationIndex, iconVertexArrayIndex, 2, 'OFF', 0 )
                        OpenGLES2.VertexAttribPointer( texCoordLocationIndex, iconTexCoordArrayIndex, 2, 'OFF', 0 )

                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

                        OpenGLES2.DrawElements( icon.glMode, len( icon.indices ), 0, iconIndexArrayIndex )

        # Render fbo/texture into screen, if defined.
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            if self.fboSize:
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

                OpenGLES2.Disable( 'GL_BLEND' )

                OpenGLES2.UniformMatrix( mVPLocationIndex,
                                         1,
                                         [ 1.0, 0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0, 0.0,
                                           0.0, 0.0, 1.0, 0.0,
                                           0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex, backgroundVertexArrayIndex, 2, 'OFF', 0 )
                OpenGLES2.VertexAttribPointer( texCoordLocationIndex, backgroundTexCoordArrayIndex, 2, 'OFF', 0 )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
                OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

                OpenGLES2.DrawElements( background.glMode, len( background.indices ), 0, backgroundIndexArrayIndex )

            target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ColorTransformationBenchmark( Benchmark ):
    def __init__( self, operation, width, height, format ):
        Benchmark.__init__( self )
        self.operation       = operation
        self.width           = width
        self.height          = height
        self.format          = format
        self.sepiaDesat      = 0.5
        self.sepiaToned      = 1.0
        self.sepiaLightColor = [ 1.0, 0.9, 0.5 ]
        self.sepiaDarkColor  = [ 0.2, 0.05, 0.0 ]
        self.bleachOpacity   = 1.0
        self.tintColor       = [ 1.0, 0.0, 0.0, 1.0 ]
        self.hslParams       = [ 0.2, 0.1, 0.2, 1.0 ]   # h, s, l, opacity
        self.name = "OPENGLES2 color transformation %s (%dx%d %s FLOWER)" % ( self.operation,
                                                                              self.width,
                                                                              self.height,
                                                                              self.format, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # Shader source codes
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            uv = gTexCoord0;
        }"""

        # From nVidia shader library:
        # http://developer.download.nvidia.com/shaderlibrary/webpages/shader_library.html
        rawSepiaFragmentShaderSource = """
        precision mediump float;
        varying vec2 uv;
        uniform sampler2D gTexture0;
        uniform float desat;
        uniform float toned;
        uniform vec3 lightColor;
        uniform vec3 darkColor;

        void main()
        {
            vec3 color = lightColor * texture2D( gTexture0, uv ).rgb;
            const vec3 grayXfer = vec3( 0.3, 0.59, 0.11 );
            float gray = dot( grayXfer, color );
            vec3 muted = mix( color, vec3( gray ), desat );
            vec3 sepia = mix( darkColor, lightColor, gray );
            vec3 result = mix( muted, sepia, toned );
            gl_FragColor = vec4( result, 1 );
        }"""

        # From nVidia shader library:
        # http://developer.download.nvidia.com/shaderlibrary/webpages/shader_library.html
        rawBleachFragmentShaderSource = """
        precision mediump float;
        varying vec2 uv;
        uniform sampler2D gTexture0;
        uniform float opacity;

        void main()
        {
            vec4 base = texture2D( gTexture0, uv );
            const vec3 lumCoeff = vec3( 0.25, 0.65, 0.1 );
            float lum = dot( lumCoeff, base.rgb );
            vec3 blend = vec3( lum );
            float l = min( 1.0, max( 0.0, 10.0 * ( lum - 0.45 ) ) );
            vec3 result1 = 2.0 * base.rgb * blend;
            vec3 result2 = vec3( 1.0 ) - 2.0 * ( vec3( 1.0 ) - blend ) * ( vec3( 1.0 ) - base.rgb );
            vec3 newColor = mix( result1, result2, l );
            float a2 = opacity * base.a;
            vec3 mixRGB = a2 * newColor.rgb;
            mixRGB += ( ( 1.0 - a2 ) * base.rgb );
            gl_FragColor = vec4( mixRGB, base.a );
        }"""

        rawTintFragmentShaderSource = """
        precision mediump float;
        varying vec2 uv;
        uniform sampler2D gTexture0;
        uniform vec4 tintColor;

        void main()
        {
            gl_FragColor = texture2D( gTexture0, uv ) * tintColor;
        }"""

        rawHslFragmentShaderSource = """
        uniform lowp sampler2D gTexture0;
        uniform lowp vec4 params; // h, s, l, opacity
        varying lowp vec2 uv;

        lowp vec3 RGBtoHSL( lowp vec3 color )
        {
            lowp float cmin = min( color.r, min( color.g, color.b ) );
            lowp float cmax = max( color.r, max( color.g, color.b ) );
            lowp float h = 0.0;
            lowp float s = 0.0;
            lowp float l = ( cmin + cmax ) / 2.0;
            lowp float diff = cmax - cmin;

            if( dot( diff, diff ) > pow( 8.0 / 256.0, 2.0 ) )
            {
               lowp float diff = cmax - cmin;

               if( l < 0.5 )
               {
                  s = diff / ( cmin + cmax );
               }
               else
               {
                  s = diff / ( 2.0 - ( cmin + cmax ) );
               }

               if( color.r == cmax )
               {
                   h = ( color.g - color.b ) / diff;
               }
               else if( color.g == cmax )
               {
                   h = 2.0 + ( color.b - color.r ) / diff;
               }
               else
               {
                   h = 4.0 + ( color.r - color.g ) / diff;
               }

               h /= 6.0;
            }

            return vec3(h, s, l);
        }

        lowp float hueToIntensity( lowp float v1, lowp float v2, lowp float h )
        {
            h = fract( h );

            if( h < 1.0 / 6.0 )
            {
                return v1 + ( v2 - v1 ) * 6.0 * h;
            }
            else if( h < 1.0 / 2.0 )
            {
                return v2;
            }
            else if( h < 2.0 / 3.0 )
            {
                return v1 + ( v2 - v1 ) * 6.0 * ( 2.0 / 3.0 - h );
            }

            return v1;
        }

        lowp vec3 HSLtoRGB( lowp vec3 color )
        {
            lowp float h = color.x;
            lowp float s = color.y;
            lowp float l = color.z;

            if( s < 1.0 / 256.0 )
            {
                return vec3( l, l, l );
            }

            lowp float v1;
            lowp float v2;

            if( l < 0.5 )
            {
                v2 = l * ( 1.0 + s );
            }
            else
            {
                v2 = ( l + s ) - ( s * l );
            }

            v1 = 2.0 * l - v2;

            lowp float d = 1.0 / 3.0;
            lowp float r = hueToIntensity( v1, v2, h + d );
            lowp float g = hueToIntensity( v1, v2, h );
            lowp float b = hueToIntensity( v1, v2, h - d );

            return vec3( r, g, b );
        }

        void main()
        {
            lowp vec4 sample = texture2D( gTexture0, uv );
            sample.xyz = RGBtoHSL( sample.rgb );
            sample.rgb = HSLtoRGB( sample.xyz + params.xyz );
            gl_FragColor = sample * sample.a * params.w;
        }""";

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv );
        }"""

        vertexShaderSource         = formatShader( rawVertexShaderSource )
        sepiaFragmentShaderSource  = formatShader( rawSepiaFragmentShaderSource )
        bleachFragmentShaderSource = formatShader( rawBleachFragmentShaderSource )
        tintFragmentShaderSource   = formatShader( rawTintFragmentShaderSource )
        hslFragmentShaderSource    = formatShader( rawHslFragmentShaderSource )
        drawFragmentShaderSource   = formatShader( rawDrawFragmentShaderSource )

        if self.operation == 'sepia':
            effectFragmentShaderSource = sepiaFragmentShaderSource
        elif self.operation == 'bleach':
            effectFragmentShaderSource = bleachFragmentShaderSource
        elif self.operation == 'tint':
            effectFragmentShaderSource = tintFragmentShaderSource
        elif self.operation == 'hsl':
            effectFragmentShaderSource = hslFragmentShaderSource

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        # Create image texture data.
        imageTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        imageTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex, 'IMAGES_FLOWER', IMAGES_FORMATS[ self.format ], self.width, self.height )

        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )

        OpenGLES2.CreateTextureData( imageTextureDataIndex, dataIndex, self.width, self.height, FORMATS[ self.format ] )

        OpenGLES2.GenTexture( imageTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( imageTextureDataIndex, 'GL_TEXTURE_2D' )

        # Create geometry, programs, and VBOs.
        texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

        mesh = MeshStripPlane( 2,
                               2,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                               None,
                               None,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0  ] )

        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )

        effectProgram = createProgram( modules, indexTracker, vertexShaderSource, effectFragmentShaderSource, vertexDataSetup )
        useProgram( modules, effectProgram )

        useVertexBufferData( modules, vertexBufferDataSetup, effectProgram )
        validateProgram( modules, effectProgram )

        drawProgram = createProgram( modules, indexTracker, vertexShaderSource, drawFragmentShaderSource, vertexDataSetup )
        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        validateProgram( modules, drawProgram )

        useProgram( modules, effectProgram )

        # Get effect specific uniform locations.
        if self.operation == 'sepia':
            sepiaDesatLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( sepiaDesatLocationIndex, effectProgram.programIndex, 'desat' )

            sepiaTonedLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( sepiaTonedLocationIndex, effectProgram.programIndex, 'toned' )

            sepiaLightColorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( sepiaLightColorLocationIndex, effectProgram.programIndex, 'lightColor' )

            sepiaDarkColorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( sepiaDarkColorLocationIndex, effectProgram.programIndex, 'darkColor' )

        elif self.operation == 'bleach':
            bleachOpacityLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( bleachOpacityLocationIndex, effectProgram.programIndex, 'opacity' )

        elif self.operation == 'tint':
            tintColorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( tintColorLocationIndex, effectProgram.programIndex, 'tintColor' )

        elif self.operation == 'hsl':
            hslParamsLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( hslParamsLocationIndex, effectProgram.programIndex, 'params' )

        # Create a FBO and textures (RGBA8) used for color transformation.
        fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( fboTextureDataIndex, self.width, self.height, 'OFF', 'OPENGLES2_RGBA8888' )

        fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( fboTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
        OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

        framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        # Draw effect to fbo/fbo texture.
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
        OpenGLES2.Viewport( 0, 0, self.width, self.height )

        # Use effect program.
        useProgram( modules, effectProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, effectProgram )
        useTexture( modules, imageTextureIndex, 0, effectProgram )

        # Set effect specific uniforms.
        if self.operation == 'sepia':
            OpenGLES2.Uniformf( sepiaDesatLocationIndex, 1, [ self.sepiaDesat ] )
            OpenGLES2.Uniformf( sepiaTonedLocationIndex, 1, [ self.sepiaToned ] )
            OpenGLES2.Uniformf( sepiaLightColorLocationIndex, 1, self.sepiaLightColor )
            OpenGLES2.Uniformf( sepiaDarkColorLocationIndex, 1, self.sepiaDarkColor )

        elif self.operation == 'bleach':
            OpenGLES2.Uniformf( bleachOpacityLocationIndex, 1, [ self.bleachOpacity ] )

        elif self.operation == 'tint':
            OpenGLES2.Uniformf( tintColorLocationIndex, 1, self.tintColor )

        elif self.operation == 'hsl':
            OpenGLES2.Uniformf( hslParamsLocationIndex, 1, self.hslParams )

        drawVertexBufferData( modules, vertexBufferDataSetup )

        # Draw fbo texture to screen.
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        useTexture( modules, fboTextureIndex, 0, drawProgram )

        drawVertexBufferData( modules, vertexBufferDataSetup )

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CompositionMaskBenchmark( Benchmark ):
    def __init__( self, width, height, format ):
        Benchmark.__init__( self )
        self.width    = width
        self.height   = height
        self.format   = format
        self.name = 'OPENGLES2 composition mask (%dx%d %s FLOWER)' % ( self.width,
                                                                       self.height,
                                                                       self.format, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;

        void main()
        {
           gl_Position = vec4( gVertex, 0, 1 );
           uv = gTexCoord0;
        }"""

        rawCompositionFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        uniform sampler2D gMask0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv ) * texture2D( gMask0, uv ).a;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv );
        }"""

        vertexShaderSource              = formatShader( rawVertexShaderSource )
        compositionFragmentShaderSource = formatShader( rawCompositionFragmentShaderSource )
        drawFragmentShaderSource        = formatShader( rawDrawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        texCoordAttribute = []
        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) )

        mesh = MeshStripPlane( 2,
                               2,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                               None,
                               None,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )

        compProgram = createProgram( modules, indexTracker, vertexShaderSource, compositionFragmentShaderSource, vertexDataSetup )
        useProgram( modules, compProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, compProgram )
        validateProgram( modules, compProgram )

        drawProgram = createProgram( modules, indexTracker, vertexShaderSource, drawFragmentShaderSource, vertexDataSetup )
        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        validateProgram( modules, drawProgram )

        # Get composition mask uniform.
        compositionMaskLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( compositionMaskLocationIndex, compProgram.programIndex, 'gMask0' )

        # Create texture and mask.
        textureDataFormat = IMAGES_FORMATS[ self.format ]
        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex,
                           'IMAGES_FLOWER',
                           textureDataFormat,
                           self.width, self.height )

        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imagesDataIndex,
                                dataIndex )

        textureDataFormat = FORMATS[ self.format ]
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )

        OpenGLES2.CreateTextureData( textureDataIndex, dataIndex, self.width, self.height, FORMATS[ self.format ] )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        maskDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( maskDataIndex,
                                            [ 0, 0, 0, 0 ],
                                            [ 1, 1, 1, 1 ],
                                            4, 4,
                                            self.width, self.height,
                                            'OFF',
                                            'OPENGLES2_ALPHA8' )

        maskIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( maskIndex )
        OpenGLES2.ActiveTexture( 1 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', maskIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( maskDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.ActiveTexture( 0 )

        # Create a FBO and textures (RGBA8) used for masking.
        fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( fboTextureDataIndex, self.width, self.height, 'OFF', 'OPENGLES2_RGBA8888' )

        fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( fboTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
        OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

        framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        # Draw composition to fbo texture.
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
        OpenGLES2.Viewport( 0, 0, self.width, self.height )

        # Use composition program.
        useProgram( modules, compProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, compProgram )
        useTexture( modules, textureIndex, 0, compProgram )

        # Bind mask.
        OpenGLES2.ActiveTexture( 1 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', maskIndex )
        OpenGLES2.Uniformi( compositionMaskLocationIndex, 1, [ 1 ] )

        OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        OpenGLES2.Enable( 'GL_BLEND' )

        drawVertexBufferData( modules, vertexBufferDataSetup )

        # Draw fbo texture to screen.
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        useTexture( modules, fboTextureIndex, 0, drawProgram )

        drawVertexBufferData( modules, vertexBufferDataSetup )

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class BlurBenchmark( Benchmark ):
    def __init__( self, width, height, format, iterations, filtering ):
        Benchmark.__init__( self )
        self.width      = width
        self.height     = height
        self.format     = format
        self.iterations = iterations
        self.filtering  = filtering
        self.name = "OPENGLES2 blur (%dx%d %s FLOWER %d iterations %s)" % ( self.width,
                                                                            self.height,
                                                                            self.format,
                                                                            self.iterations,
                                                                            self.filtering, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # Shader source codes
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            uv = gTexCoord0;
        }"""

        rawBlurFragmentShaderSource = """
        precision lowp float;
        varying vec2 uv;
        uniform sampler2D gTexture0;
        uniform vec2 scale;

        void main()
        {
            vec4 color = vec4( 0.0, 0.0, 0.0, 0.0 );
            color += 0.0366 * texture2D( gTexture0, uv - 3.0 * scale );
            color += 0.1113 * texture2D( gTexture0, uv - 2.0 * scale );
            color += 0.2167 * texture2D( gTexture0, uv - 1.0 * scale );
            color += 0.2707 * texture2D( gTexture0, uv + 0.0 );
            color += 0.2167 * texture2D( gTexture0, uv + 1.0 * scale );
            color += 0.1113 * texture2D( gTexture0, uv + 2.0 * scale );
            color += 0.0366 * texture2D( gTexture0, uv + 3.0 * scale );
            gl_FragColor = color;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv );
        }"""

        vertexShaderSource       = formatShader( rawVertexShaderSource )
        blurFragmentShaderSource = formatShader( rawBlurFragmentShaderSource )
        drawFragmentShaderSource = formatShader( rawDrawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        # Create image texture data.
        imageTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        imageTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex, 'IMAGES_FLOWER', IMAGES_FORMATS[ self.format ], self.width, self.height )

        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )

        OpenGLES2.CreateTextureData( imageTextureDataIndex, dataIndex, self.width, self.height, FORMATS[ self.format ] )

        OpenGLES2.GenTexture( imageTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( imageTextureDataIndex, 'GL_TEXTURE_2D' )

        # Create geometry
        texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

        mesh = MeshStripPlane( 2,
                               2,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                               None,
                               None,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0  ] )

        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )

        blurProgram = createProgram( modules, indexTracker, vertexShaderSource, blurFragmentShaderSource, vertexDataSetup )
        useProgram( modules, blurProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, blurProgram )
        validateProgram( modules, blurProgram )

        drawProgram = createProgram( modules, indexTracker, vertexShaderSource, drawFragmentShaderSource, vertexDataSetup )
        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        validateProgram( modules, drawProgram )

        useProgram( modules, blurProgram )

        blurScaleLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( blurScaleLocationIndex, blurProgram.programIndex, 'scale' )

        # Create a FBO and textures used for blurring.
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( textureDataIndex, self.width, self.height, 'OFF', FORMATS[ self.format ] )

        glFiltering = 'GL_NEAREST'
        if self.filtering == 'linear':
            glFiltering = 'GL_LINEAR'

        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

        framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture1Index, 0 )

        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
        OpenGLES2.Viewport( 0, 0, self.width, self.height )

        useProgram( modules, blurProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, blurProgram )
        useTexture( modules, imageTextureIndex, 0, blurProgram )

        for i in range( self.iterations ):
            # Bind FBO with the first temp texture
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture1Index, 0 )

            # Draw source image into the first temp texture
            OpenGLES2.Uniformf( blurScaleLocationIndex, 1, [ 1.0 / self.width, 0.0 ] )
            drawVertexBufferData( modules, vertexBufferDataSetup )

            # Set the second temp texture into FBO
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture2Index, 0 )

            # Use the first temp texture
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )

            # Draw the first temp texture into the second temp texture
            OpenGLES2.Uniformf( blurScaleLocationIndex, 1, [ 0.0, 1.0 / self.height ] )
            drawVertexBufferData( modules, vertexBufferDataSetup )

            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )

        # Remove FBO and draw the second temp texture into the screen
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        useTexture( modules, texture2Index, 0, drawProgram )

        drawVertexBufferData( modules, vertexBufferDataSetup )

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class MotionBlurBenchmark( Benchmark ):
    def __init__( self, width, height, format, gridx, gridy, samples, filtering ):
        Benchmark.__init__( self )
        self.width      = width
        self.height     = height
        self.format     = format
        self.gridx      = gridx
        self.gridy      = gridy
        self.samples    = samples
        self.filtering  = filtering
        self.name = "OPENGLES2 motion blur (%dx%d %s FLOWER %dx%d %d samples %s)" % ( self.width,
                                                                                      self.height,
                                                                                      self.format,
                                                                                      self.gridx,
                                                                                      self.gridy,
                                                                                      self.samples,
                                                                                      self.filtering, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # Shader source codes
        rawBlurVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gVelocity;
        attribute vec2 gTexCoord0;
        varying vec2 uv;
        varying vec2 velocity;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            uv = gTexCoord0;
            velocity = gVelocity;
        }"""

        rawBlurFragmentShaderSource = """
        precision lowp float;
        varying vec2 uv;
        varying vec2 velocity;
        uniform sampler2D gTexture0;

        void main()
        {
            vec2 texCoord = uv;
            vec4 color = texture2D( gTexture0, texCoord );
            texCoord += velocity;
            for( int i = 1; i < %d; ++i )
            {
                color += texture2D( gTexture0, texCoord );
                texCoord += velocity;
            }

            gl_FragColor = color * %f;
        }""" % ( self.samples, 1.0 / self.samples, )

        rawDrawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            uv = gTexCoord0;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv );
        }"""


        blurVertexShaderSource   = formatShader( rawBlurVertexShaderSource )
        blurFragmentShaderSource = formatShader( rawBlurFragmentShaderSource )
        drawVertexShaderSource   = formatShader( rawDrawVertexShaderSource )
        drawFragmentShaderSource = formatShader( rawDrawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        # Create image texture data.
        imageTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        imageTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex, 'IMAGES_FLOWER', IMAGES_FORMATS[ self.format ], self.width, self.height )

        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )

        OpenGLES2.CreateTextureData( imageTextureDataIndex, dataIndex, self.width, self.height, FORMATS[ self.format ] )

        glFiltering = 'GL_NEAREST'
        if self.filtering == 'linear':
            glFiltering = 'GL_LINEAR'

        OpenGLES2.GenTexture( imageTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( imageTextureDataIndex, 'GL_TEXTURE_2D' )

        # Create geometry.
        texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

        mesh = MeshStripPlane( self.gridx,
                               self.gridy,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                               None,
                               None,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0  ] )

        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )

        blurProgram = createProgram( modules, indexTracker, blurVertexShaderSource, blurFragmentShaderSource, vertexDataSetup )
        useProgram( modules, blurProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, blurProgram )
        validateProgram( modules, blurProgram )

        drawProgram = createProgram( modules, indexTracker, drawVertexShaderSource, drawFragmentShaderSource, vertexDataSetup )
        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        validateProgram( modules, drawProgram )

        useProgram( modules, blurProgram )

        velocities = [ 0.01, 0.01 ] * self.gridx * self.gridy

        velocitiesArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( velocitiesArrayIndex, 'GL_FLOAT' )
        OpenGLES2.AppendToArray( velocitiesArrayIndex, velocities )

        velocitiesBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( velocitiesBufferIndex )
        OpenGLES2.BufferData( velocitiesBufferIndex, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', velocitiesArrayIndex, 0, 0 )

        velocitiesLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetAttribLocation( velocitiesLocationIndex, blurProgram.programIndex, 'gVelocity' )

        # Create a FBO and textures (RGBA8) used for blur.
        fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( fboTextureDataIndex, self.width, self.height, 'OFF', 'OPENGLES2_RGBA8888' )

        fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( fboTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
        OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

        framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        # Draw composition to fbo texture.
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
        OpenGLES2.Viewport( 0, 0, self.width, self.height )

        # Use composition program.
        useProgram( modules, blurProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, blurProgram )
        useTexture( modules, imageTextureIndex, 0, blurProgram )

        OpenGLES2.BufferVertexAttribPointer( velocitiesBufferIndex, 'ON', velocitiesLocationIndex, 1, 'OFF', 0 )
        OpenGLES2.EnableVertexAttribArray( velocitiesLocationIndex )

        drawVertexBufferData( modules, vertexBufferDataSetup )

        # Draw fbo texture to screen.
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        useTexture( modules, fboTextureIndex, 0, drawProgram )

        drawVertexBufferData( modules, vertexBufferDataSetup )

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DOFBenchmark( Benchmark ):
    def __init__( self, width, height, format, blurIterations, filtering ):
        Benchmark.__init__( self )
        self.width              = width
        self.height             = height
        self.format             = format
        self.blurIterations     = blurIterations
        self.filtering          = filtering
        self.downsample         = 2
        self.highlightThreshold = 0.5
        self.intensity          = [ 0.7, 0.3, 0.5 ]
        self.name = "OPENGLES2 depth-of-field (%dx%d %s FLOWER %d iterations %s)" % ( self.width,
                                                                                      self.height,
                                                                                      self.format,
                                                                                      self.blurIterations,
                                                                                      self.filtering, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # Shader source codes.
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec3 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;

        void main()
        {
            gl_Position = vec4( gVertex, 1 );
            uv = gTexCoord0;
        }"""

        rawDownsampleVertexShaderSource = """
        precision mediump float;
        attribute vec3 gVertex;
        attribute vec2 gTexCoord0;
        uniform vec2 scale;
        varying vec2 uv0;
        varying vec2 uv1;
        varying vec2 uv2;
        varying vec2 uv3;

        void main()
        {
            gl_Position = vec4( gVertex, 1 );
            uv0 = gTexCoord0;
            uv1 = gTexCoord0 + vec2( 2, 0 ) * scale;
            uv2 = gTexCoord0 + vec2( 2, 2 ) * scale;
            uv3 = gTexCoord0 + vec2( 0, 2 ) * scale;
        }"""

        rawDownsampleFragmentShaderSource = """
        precision lowp float;
        uniform sampler2D gTexture0;
        varying vec2 uv0;
        varying vec2 uv1;
        varying vec2 uv2;
        varying vec2 uv3;

        void main()
        {
            vec4 color;
            color  = 0.25 * texture2D( gTexture0, uv0 );
            color += 0.25 * texture2D( gTexture0, uv1 );
            color += 0.25 * texture2D( gTexture0, uv2 );
            color += 0.25 * texture2D( gTexture0, uv3 );

            gl_FragColor = color;
        }"""

        rawBlurFragmentShaderSource = """
        precision lowp float;
        varying vec2 uv;
        uniform sampler2D gTexture0;
        uniform vec2 scale;

        void main()
        {
            vec4 color = vec4( 0.0, 0.0, 0.0, 0.0 );
            color += 0.0366 * texture2D( gTexture0, uv - 3.0 * scale );
            color += 0.1113 * texture2D( gTexture0, uv - 2.0 * scale );
            color += 0.2167 * texture2D( gTexture0, uv - 1.0 * scale );
            color += 0.2707 * texture2D( gTexture0, uv + 0.0 );
            color += 0.2167 * texture2D( gTexture0, uv + 1.0 * scale );
            color += 0.1113 * texture2D( gTexture0, uv + 2.0 * scale );
            color += 0.0366 * texture2D( gTexture0, uv + 3.0 * scale );
            gl_FragColor = color;
        }"""

        rawFragmentShaderSource = """
        precision lowp float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv );
        }"""

        rawCompositionFragmentShaderSource = """
        precision lowp float;
        varying vec2 uv;
        uniform sampler2D gTexture0;
        uniform sampler2D gBlur0;
        uniform sampler2D gDepth0;

        void main()
        {
            vec4 color = texture2D( gTexture0, uv );
            vec4 blur  = texture2D( gBlur0, uv );
            vec4 depth = texture2D( gDepth0, uv );
            gl_FragColor = mix( color, blur, 1.0 - depth.z );
        }"""

        rawDrawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            uv = gTexCoord0;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv );
        }"""

        downsampleVertexShaderSource    = formatShader( rawDownsampleVertexShaderSource )
        downsampleFragmentShaderSource  = formatShader( rawDownsampleFragmentShaderSource )
        vertexShaderSource              = formatShader( rawVertexShaderSource )
        blurFragmentShaderSource        = formatShader( rawBlurFragmentShaderSource )
        fragmentShaderSource            = formatShader( rawFragmentShaderSource )
        compositionFragmentShaderSource = formatShader( rawCompositionFragmentShaderSource )
        drawVertexShaderSource          = formatShader( rawDrawVertexShaderSource )
        drawFragmentShaderSource        = formatShader( rawDrawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        OpenGLES2.CheckExtension( 'GL_OES_depth_texture' )

        # Create image texture data.
        imageTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        imageTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex, 'IMAGES_FLOWER', IMAGES_FORMATS[ self.format ], self.width, self.height )

        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )

        OpenGLES2.CreateTextureData( imageTextureDataIndex, dataIndex, self.width, self.height, FORMATS[ self.format ] )

        OpenGLES2.GenTexture( imageTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.TexImage2D( imageTextureDataIndex, 'GL_TEXTURE_2D' )

        # Create geometry.
        texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

        mesh = MeshStripPlane( 2,
                               2,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 3 ),
                               None,
                               None,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0  ] )

        # Set mesh vertex z values explicitly.
        mesh.vertices[ 2 ]  = 1.0
        mesh.vertices[ 5 ]  = 1.0
        mesh.vertices[ 8 ]  = -1.0
        mesh.vertices[ 11 ] = -1.0

        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )

        program = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        useProgram( modules, program )
        useVertexBufferData( modules, vertexBufferDataSetup, program )
        validateProgram( modules, program )

        downsampleProgram = createProgram( modules,
                                           indexTracker,
                                           downsampleVertexShaderSource,
                                           downsampleFragmentShaderSource,
                                           vertexDataSetup )
        useProgram( modules, downsampleProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, downsampleProgram )
        validateProgram( modules, downsampleProgram )

        downsampleScaleLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( downsampleScaleLocationIndex, downsampleProgram.programIndex, 'scale' )

        blurProgram = createProgram( modules,
                                     indexTracker,
                                     vertexShaderSource,
                                     blurFragmentShaderSource,
                                     vertexDataSetup )
        useProgram( modules, blurProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, blurProgram )
        validateProgram( modules, blurProgram )

        blurScaleLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( blurScaleLocationIndex, blurProgram.programIndex, 'scale' )

        compositionProgram = createProgram( modules,
                                            indexTracker,
                                            vertexShaderSource,
                                            compositionFragmentShaderSource,
                                            vertexDataSetup )
        useProgram( modules, compositionProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, compositionProgram )
        validateProgram( modules, compositionProgram )

        compositionBlurTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( compositionBlurTextureLocationIndex, compositionProgram.programIndex, 'gBlur0' )

        compositionDepthTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( compositionDepthTextureLocationIndex, compositionProgram.programIndex, 'gDepth0' )

        drawProgram = createProgram( modules, indexTracker, drawVertexShaderSource, drawFragmentShaderSource, vertexDataSetup )
        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        validateProgram( modules, drawProgram )

        # Create a FBO and textures used for DOF.
        fullresTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( fullresTextureDataIndex, self.width, self.height, 'OFF', 'OPENGLES2_RGBA8888' )

        fullresTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( fullresTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fullresTextureIndex )
        OpenGLES2.TexImage2D( fullresTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        fullresDepthTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( fullresDepthTextureDataIndex, self.width, self.height, 'OFF', 'OPENGLES2_DEPTH' )

        fullresDepthTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( fullresDepthTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fullresDepthTextureIndex )
        OpenGLES2.TexImage2D( fullresDepthTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        downsampleWidth  = self.width / self.downsample
        downsampleHeight = self.width / self.downsample

        downsampleTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( downsampleTextureDataIndex, downsampleWidth, downsampleHeight, 'OFF', 'OPENGLES2_RGBA8888' )

        glFiltering = 'GL_NEAREST'
        if self.filtering == 'linear':
            glFiltering = 'GL_LINEAR'

        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexImage2D( downsampleTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexImage2D( downsampleTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

        framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fullresTextureIndex, 0 )
        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_DEPTH_ATTACHMENT', 'GL_TEXTURE_2D', fullresDepthTextureIndex, 0 )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        # Create a FBO and textures (RGBA8) used for drawing final composition
        # to the screen.
        blitTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( blitTextureDataIndex, self.width, self.height, 'OFF', 'OPENGLES2_RGBA8888' )

        blitTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( blitTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', blitTextureIndex )
        OpenGLES2.TexImage2D( blitTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        blitFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( blitFramebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', blitFramebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', blitTextureIndex, 0 )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.DepthFunc( 'GL_ALWAYS' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        # First, draw full resolution image to framebuffer, store depth.
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fullresTextureIndex, 0 )
        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_DEPTH_ATTACHMENT', 'GL_TEXTURE_2D', fullresDepthTextureIndex, 0 )
        OpenGLES2.Viewport( 0, 0, self.width, self.height )

        useProgram( modules, program )
        useVertexBufferData( modules, vertexBufferDataSetup, program )
        useTexture( modules, imageTextureIndex, 0, program )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        drawVertexBufferData( modules, vertexBufferDataSetup )
        OpenGLES2.Disable( 'GL_DEPTH_TEST' )
        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_DEPTH_ATTACHMENT', 'GL_TEXTURE_2D', -1, 0 )

        # Downsample for blur.
        useProgram( modules, downsampleProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, downsampleProgram )
        useTexture( modules, imageTextureIndex, 0, downsampleProgram )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture1Index, 0 )
        OpenGLES2.Viewport( 0, 0, downsampleWidth, downsampleHeight )

        OpenGLES2.Uniformf( downsampleScaleLocationIndex, 1, [ 1.0 / self.width, 1.0 / self.height ] )
        drawVertexBufferData( modules, vertexBufferDataSetup )

        # Blur.
        useProgram( modules, blurProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, blurProgram )

        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        for i in range( self.blurIterations ):
            useTexture( modules, texture1Index, 0, blurProgram )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture2Index, 0 )

            OpenGLES2.Uniformf( blurScaleLocationIndex, 1, [ 1.0 / self.width, 0.0 ] )
            drawVertexBufferData( modules, vertexBufferDataSetup )

            useTexture( modules, texture2Index, 0, blurProgram )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture1Index, 0 )

            OpenGLES2.Uniformf( blurScaleLocationIndex, 1, [ 0.0, 1.0 / self.height ] )
            drawVertexBufferData( modules, vertexBufferDataSetup )

        # Do DOF composition to blit FBO.
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', blitFramebufferIndex )
        OpenGLES2.Viewport( 0, 0, self.width, self.height )

        useProgram( modules, compositionProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, compositionProgram )
        useTexture( modules, fullresTextureIndex, 0, compositionProgram )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        OpenGLES2.ActiveTexture( 1 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.Uniformi( compositionBlurTextureLocationIndex, 1, [ 1 ] )
        # Use linear filtering for accessing the downsampled blur texture.
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        OpenGLES2.ActiveTexture( 2 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fullresDepthTextureIndex )
        OpenGLES2.Uniformi( compositionDepthTextureLocationIndex, 1, [ 2 ] )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        drawVertexBufferData( modules, vertexBufferDataSetup )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )
        OpenGLES2.ActiveTexture( 1 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )
        OpenGLES2.ActiveTexture( 0 )

        # Draw blit FBO to screen/default FBO.
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        useTexture( modules, blitTextureIndex, 0, drawProgram )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        drawVertexBufferData( modules, vertexBufferDataSetup )

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class GlowBenchmark( Benchmark ):
    def __init__( self, width, height, format, blurIterations, filtering ):
        # Glow benchmark is based on samples from the nVidia shader library:
        # http://developer.download.nvidia.com/shaderlibrary/webpages/shader_library.html
        Benchmark.__init__( self )
        self.width              = width
        self.height             = height
        self.format             = format
        self.blurIterations     = blurIterations
        self.filtering          = filtering
        self.downsample         = 2
        self.highlightThreshold = 0.5
        self.intensity          = [ 0.7, 0.3, 0.5 ]
        self.name = "OPENGLES2 glow (%dx%d %s FLOWER %d iterations %s)" % ( self.width,
                                                                            self.height,
                                                                            self.format,
                                                                            self.blurIterations,
                                                                            self.filtering, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # Shader source codes.
        rawDownsampleVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        uniform vec2 scale;
        varying vec2 uv0;
        varying vec2 uv1;
        varying vec2 uv2;
        varying vec2 uv3;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            uv0 = gTexCoord0;
            uv1 = gTexCoord0 + vec2( 2, 0 ) * scale;
            uv2 = gTexCoord0 + vec2( 2, 2 ) * scale;
            uv3 = gTexCoord0 + vec2( 0, 2 ) * scale;
        }"""

        rawDownsampleFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        uniform float highlightThreshold;
        varying vec2 uv0;
        varying vec2 uv1;
        varying vec2 uv2;
        varying vec2 uv3;

        float luminance( vec3 c)
        {
            return dot( c, vec3( 0.3, 0.59, 0.11 ) );
        }

        float highlights( vec3 c, float highlightThreshold )
        {
            return smoothstep( highlightThreshold, 1.0, luminance( c.rgb ) );
        }

        void main()
        {
            vec4 color;
            color  = 0.25 * texture2D( gTexture0, uv0 );
            color += 0.25 * texture2D( gTexture0, uv1 );
            color += 0.25 * texture2D( gTexture0, uv2 );
            color += 0.25 * texture2D( gTexture0, uv3 );

            color.a = highlights( color.rgb, highlightThreshold );

            gl_FragColor = color;
        }"""

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;

        void main()
        {
            gl_Position = gVertex;
            uv = gTexCoord0;
        }"""

        rawBlurFragmentShaderSource = """
        precision lowp float;
        varying vec2 uv;
        uniform sampler2D gTexture0;
        uniform vec2 scale;

        void main()
        {
            vec4 color = vec4( 0.0, 0.0, 0.0, 0.0 );
            color += 0.0366 * texture2D( gTexture0, uv - 3.0 * scale );
            color += 0.1113 * texture2D( gTexture0, uv - 2.0 * scale );
            color += 0.2167 * texture2D( gTexture0, uv - 1.0 * scale );
            color += 0.2707 * texture2D( gTexture0, uv + 0.0 );
            color += 0.2167 * texture2D( gTexture0, uv + 1.0 * scale );
            color += 0.1113 * texture2D( gTexture0, uv + 2.0 * scale );
            color += 0.0366 * texture2D( gTexture0, uv + 3.0 * scale );
            gl_FragColor = color;
        }"""

        rawGlowFragmentShaderSource = """
        precision lowp float;
        varying vec2 uv;
        uniform sampler2D gTexture0;
        uniform vec2 scale;

        void main()
        {
            vec4 color = vec4( 0.0, 0.0, 0.0, 0.0 );
            color += 0.2 * texture2D( gTexture0, uv - 1.0 * scale );
            color += 0.6 * texture2D( gTexture0, uv + 0.0 );
            color += 0.2 * texture2D( gTexture0, uv + 1.0 * scale );
            gl_FragColor = color;
        }"""

        rawCompositionFragmentShaderSource = """
        precision lowp float;
        varying vec2 uv;
        uniform sampler2D gTexture0;
        uniform sampler2D gBlur0;
        uniform vec3 intensity;

        void main()
        {
            vec4 color = texture2D( gTexture0, uv );
            vec4 blur  = texture2D( gBlur0, uv );
            gl_FragColor = intensity[ 0 ] * color + intensity[ 1 ] * blur + intensity[ 2 ] * blur.a;
        }"""

        rawDrawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            uv = gTexCoord0;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv );
        }"""

        downsampleVertexShaderSource    = formatShader( rawDownsampleVertexShaderSource )
        downsampleFragmentShaderSource  = formatShader( rawDownsampleFragmentShaderSource )
        vertexShaderSource              = formatShader( rawVertexShaderSource )
        blurFragmentShaderSource        = formatShader( rawBlurFragmentShaderSource )
        glowFragmentShaderSource        = formatShader( rawGlowFragmentShaderSource )
        compositionFragmentShaderSource = formatShader( rawCompositionFragmentShaderSource )
        drawVertexShaderSource          = formatShader( rawDrawVertexShaderSource )
        drawFragmentShaderSource        = formatShader( rawDrawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        # Create image texture data.
        imageTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        imageTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex, 'IMAGES_FLOWER', IMAGES_FORMATS[ self.format ], self.width, self.height )

        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )

        OpenGLES2.CreateTextureData( imageTextureDataIndex, dataIndex, self.width, self.height, FORMATS[ self.format ] )

        OpenGLES2.GenTexture( imageTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.TexImage2D( imageTextureDataIndex, 'GL_TEXTURE_2D' )

        # Create geometry.
        texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

        mesh = MeshStripPlane( 2,
                               2,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                               None,
                               None,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0  ] )

        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )

        # Create programs.
        downsampleProgram = createProgram( modules,
                                           indexTracker,
                                           downsampleVertexShaderSource,
                                           downsampleFragmentShaderSource,
                                           vertexDataSetup )
        useProgram( modules, downsampleProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, downsampleProgram )
        validateProgram( modules, downsampleProgram )

        downsampleScaleLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( downsampleScaleLocationIndex, downsampleProgram.programIndex, 'scale' )

        downsampleHighlightThresholdLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( downsampleHighlightThresholdLocationIndex, downsampleProgram.programIndex, 'highlightThreshold' )

        blurProgram = createProgram( modules, indexTracker, vertexShaderSource, blurFragmentShaderSource, vertexDataSetup )
        useProgram( modules, blurProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, blurProgram )
        validateProgram( modules, blurProgram )

        blurScaleLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( blurScaleLocationIndex, blurProgram.programIndex, 'scale' )

        glowProgram = createProgram( modules, indexTracker, vertexShaderSource, glowFragmentShaderSource, vertexDataSetup )
        useProgram( modules, glowProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, glowProgram )
        validateProgram( modules, glowProgram )

        glowScaleLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( glowScaleLocationIndex, glowProgram.programIndex, 'scale' )

        compositionProgram = createProgram( modules, indexTracker, vertexShaderSource, compositionFragmentShaderSource, vertexDataSetup )
        useProgram( modules, compositionProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, compositionProgram )
        validateProgram( modules, compositionProgram )

        compositionBlurTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( compositionBlurTextureLocationIndex, compositionProgram.programIndex, 'gBlur0' )

        compositionIntensityLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( compositionIntensityLocationIndex, compositionProgram.programIndex, 'intensity' )

        drawProgram = createProgram( modules, indexTracker, drawVertexShaderSource, drawFragmentShaderSource, vertexDataSetup )
        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        validateProgram( modules, drawProgram )

        # Create a FBO and textures used for glow.
        downsampleWidth  = self.width / self.downsample
        downsampleHeight = self.width / self.downsample

        downsampleTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( downsampleTextureDataIndex, downsampleWidth, downsampleHeight, 'OFF', 'OPENGLES2_RGBA8888' )

        glFiltering = 'GL_NEAREST'
        if self.filtering == 'linear':
            glFiltering = 'GL_LINEAR'

        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexImage2D( downsampleTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexImage2D( downsampleTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

        framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture1Index, 0 )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        # Create a FBO and textures (RGBA8) used for drawing final composition
        # to the screen.
        blitTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( blitTextureDataIndex, self.width, self.height, 'OFF', 'OPENGLES2_RGBA8888' )

        blitTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( blitTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', blitTextureIndex )
        OpenGLES2.TexImage2D( blitTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        blitFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( blitFramebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', blitFramebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', blitTextureIndex, 0 )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        # Downsample for blur and glow.
        useProgram( modules, downsampleProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, downsampleProgram )
        useTexture( modules, imageTextureIndex, 0, downsampleProgram )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
        OpenGLES2.Viewport( 0, 0, downsampleWidth, downsampleHeight )
        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture1Index, 0 )

        OpenGLES2.Uniformf( downsampleScaleLocationIndex, 1, [ 1.0 / self.width, 1.0 / self.height ] )
        OpenGLES2.Uniformf( downsampleHighlightThresholdLocationIndex, 1, [ self.highlightThreshold ] )
        drawVertexBufferData( modules, vertexBufferDataSetup )

        # Blur.
        useProgram( modules, blurProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, blurProgram )

        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        for i in range( self.blurIterations ):
            useTexture( modules, texture1Index, 0, blurProgram )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture2Index, 0 )

            OpenGLES2.Uniformf( blurScaleLocationIndex, 1, [ 1.0 / self.width, 0.0 ] )
            drawVertexBufferData( modules, vertexBufferDataSetup )

            useTexture( modules, texture2Index, 0, blurProgram )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture1Index, 0 )

            OpenGLES2.Uniformf( blurScaleLocationIndex, 1, [ 0.0, 1.0 / self.height ] )
            drawVertexBufferData( modules, vertexBufferDataSetup )

        # Glow.
        useProgram( modules, glowProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, glowProgram )
        useTexture( modules, texture1Index, 0, glowProgram )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )
        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture2Index, 0 )

        OpenGLES2.Uniformf( glowScaleLocationIndex, 1, [ 1.0 / self.width, 0.0 ] )
        drawVertexBufferData( modules, vertexBufferDataSetup )

        useTexture( modules, texture2Index, 0, glowProgram )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )
        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture1Index, 0 )

        OpenGLES2.Uniformf( glowScaleLocationIndex, 1, [ 0.0, 1.0 / self.height ] )
        drawVertexBufferData( modules, vertexBufferDataSetup )

        # Composition to blit FBO.
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', blitFramebufferIndex )
        OpenGLES2.Viewport( 0, 0, self.width, self.height )

        useProgram( modules, compositionProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, compositionProgram )
        useTexture( modules, imageTextureIndex, 0, compositionProgram )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        OpenGLES2.ActiveTexture( 1 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        # Use linear filtering for accessing the downsampled blur texture.
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )
        OpenGLES2.Uniformi( compositionBlurTextureLocationIndex, 1, [ 1 ] )

        OpenGLES2.Uniformf( compositionIntensityLocationIndex, 1, self.intensity )

        drawVertexBufferData( modules, vertexBufferDataSetup )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )
        OpenGLES2.ActiveTexture( 0 )

        # Draw blit FBO to screen/default FBO.
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        useTexture( modules, blitTextureIndex, 0, drawProgram )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

        drawVertexBufferData( modules, vertexBufferDataSetup )

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class TexelFillrateBenchmark( Benchmark ):
    def __init__( self, fboSize, format, filtering, samples, overdraw, op ):
        Benchmark.__init__( self )
        self.fboSize   = fboSize
        self.format    = format
        self.filtering = filtering
        self.samples   = samples
        self.overdraw  = overdraw
        self.op        = op
        s = ''
        if self.samples > 1:
            s = 's'
        fboString = 'fullscreen'
        if self.fboSize:
            fboString = '%dx%d fbo' % self.fboSize

        self.name = 'OPENGLES2 tex fillrate (%s %s %s FLOWER %d smpl%s %d ovrdrw %s)' % ( fboString,
                                                                                          self.op,
                                                                                          self.format,
                                                                                          self.samples,
                                                                                          s,
                                                                                          self.overdraw,
                                                                                          self.filtering, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        if self.fboSize:
            fboWidth  = self.fboSize[ 0 ]
            fboHeight = self.fboSize[ 1 ]
        else:
            fboWidth  = screenWidth
            fboHeight = screenHeight

        if self.samples == 1:
            accessString = 'gl_FragColor = texture2D( gTexture0, uv );'
        if self.samples == 5:
            accessString = """
            vec4 color = vec4( 0.0, 0.0, 0.0, 0.0 );
            const float r = 1.0 / 5.0;
            const float d1 = %f;
            const float d2 = %f;
            color += r * texture2D( gTexture0, uv + vec2( 0.0, -d2 ) );
            color += r * texture2D( gTexture0, uv );
            color += r * texture2D( gTexture0, uv + vec2( 0.0, d2 ) );
            color += r * texture2D( gTexture0, uv + vec2( -d1, 0.0 ) );
            color += r * texture2D( gTexture0, uv + vec2( d1, 0.0 ) );
            gl_FragColor = color;
            """ % ( 1.0 / fboWidth, 1.0 / fboHeight, )
        if self.samples == 9:
            accessString = """
            vec4 color = vec4( 0.0, 0.0, 0.0, 0.0 );
            const float r = 1.0 / 9.0;
            const float d1 = %f;
            const float d2 = %f;
            color += r * texture2D( gTexture0, uv + vec2( -d1, -d2 ) );
            color += r * texture2D( gTexture0, uv + vec2( -d1, 0 ) );
            color += r * texture2D( gTexture0, uv + vec2( -d1, d2 ) );
            color += r * texture2D( gTexture0, uv + vec2( 0, -d2 ) );
            color += r * texture2D( gTexture0, uv );
            color += r * texture2D( gTexture0, uv + vec2( 0, d2 ) );
            color += r * texture2D( gTexture0, uv + vec2( d1, -d2 ) );
            color += r * texture2D( gTexture0, uv + vec2( d1, 0 ) );
            color += r * texture2D( gTexture0, uv + vec2( d1, d2 ) );
            gl_FragColor = color;
            """ % ( 1.0 / fboWidth, 1.0 / fboHeight, )

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex, gOffset, 1 );
           uv = gTexCoord0;
        }
        """

        rawFillFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           %s
        }
        """ % accessString

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv );
        }"""

        vertexShaderSource       = formatShader( rawVertexShaderSource )
        fillFragmentShaderSource = formatShader( rawFillFragmentShaderSource )
        drawFragmentShaderSource = formatShader( rawDrawFragmentShaderSource )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        a = {}
        if not self.fboSize:
            a[ CONFIG_DEPTHSIZE ] = '>=16'

        state = setupOpenGLES2( modules, indexTracker, target, a )

        useDepth = False
        if self.op in ( 'transparent back-to-front', 'opaque front-to-back', 'opaque back-to-front' ):
            useDepth = True

        useBlending = False
        if self.op in ( 'transparent', 'transparent back-to-front' ):
            useBlending = True

        # if self.fboSize and useDepth:
        #     OpenGLES2.CheckExtension( 'GL_OES_depth_texture' )

        # Create mesh and VBOs.
        texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

        mesh = MeshStripPlane( 2,
                               2,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                               None,
                               None,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0  ] )

        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )

        # Create programs.
        fillProgram = createProgram( modules, indexTracker, vertexShaderSource, fillFragmentShaderSource, vertexDataSetup )
        useProgram( modules, fillProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, fillProgram )
        validateProgram( modules, fillProgram )

        fillOffsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( fillOffsetLocationIndex, fillProgram.programIndex, 'gOffset' )

        drawProgram = createProgram( modules, indexTracker, vertexShaderSource, drawFragmentShaderSource, vertexDataSetup )
        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        validateProgram( modules, drawProgram )

        drawOffsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( drawOffsetLocationIndex, drawProgram.programIndex, 'gOffset' )

        # Create texture.
        imageTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        imageTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex, 'IMAGES_FLOWER', IMAGES_FORMATS[ self.format ], fboWidth, fboHeight )

        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )

        OpenGLES2.CreateTextureData( imageTextureDataIndex, dataIndex, fboWidth, fboHeight, FORMATS[ self.format ] )

        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.GenTexture( imageTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )

        if self.filtering == 'nearest':
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        else:
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.TexImage2D( imageTextureDataIndex, 'GL_TEXTURE_2D' )

        if useBlending:
            OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
            OpenGLES2.Enable( 'GL_BLEND' )

        clearMask = None
        if useDepth:
            clearMask = [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ]
            OpenGLES2.Enable( 'GL_DEPTH_TEST' )
            OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        if self.fboSize:
            # Create a FBO and textures (RGBA8) used for fill.
            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_RGBA8888' )

            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            if useDepth:
                # fboDepthTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                # OpenGLES2.CreateNullTextureData( fboDepthTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_DEPTH' )

                # fboDepthTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                # OpenGLES2.GenTexture( fboDepthTextureIndex )
                # OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboDepthTextureIndex )
                # OpenGLES2.TexImage2D( fboDepthTextureDataIndex, 'GL_TEXTURE_2D' )
                # OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )

                fboDepthRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( fboDepthRenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', fboDepthRenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER', 'GL_DEPTH_COMPONENT16', fboWidth, fboHeight )

            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            if useDepth:
                #OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_DEPTH_ATTACHMENT', 'GL_TEXTURE_2D', fboDepthTextureIndex, 0 )
                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER', 'GL_DEPTH_ATTACHMENT', 'GL_RENDERBUFFER', fboDepthRenderbufferIndex )

            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        useProgram( modules, fillProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, fillProgram )
        useTexture( modules, imageTextureIndex, 0, fillProgram )

        OpenGLES2.Uniformf( fillOffsetLocationIndex, 1, [ 0 ] )

        if useDepth:
            OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.fboSize:
            # Draw fill to FBO/texture.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
            OpenGLES2.Viewport( 0, 0, fboWidth, fboHeight )

            useProgram( modules, fillProgram )
            useVertexBufferData( modules, vertexBufferDataSetup, fillProgram )
            useTexture( modules, imageTextureIndex, 0, fillProgram )

            OpenGLES2.Uniformf( fillOffsetLocationIndex, 1, [ 0 ] )

            if useBlending:
                OpenGLES2.Enable( 'GL_BLEND' )

            if useDepth:
                OpenGLES2.Enable( 'GL_DEPTH_TEST' )

            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )

        # Clear target framebuffer, if needed.
        if clearMask:
            OpenGLES2.Clear( clearMask )

        delta = 1.0 / float( self.overdraw )

        if useDepth:
            if self.op in ( 'opaque front-to-back' ):
                offset      = 0.0
                delta       = delta
            elif self.op in ( 'transparent back-to-front', 'opaque back-to-front' ):
                offset      = 1.0
                delta       = -delta
            else:
                raise 'Unexpected op'

        # Draw fill.
        for i in range( self.overdraw ):
            if useDepth:
                OpenGLES2.Uniformf( fillOffsetLocationIndex, 1, [ offset ] )

            drawVertexBufferData( modules, vertexBufferDataSetup )

            if useDepth:
                offset += delta

        if self.fboSize:
            # Draw fill FBO/texture to the screen.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
            OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

            OpenGLES2.Disable( 'GL_DEPTH_TEST' )
            OpenGLES2.Disable( 'GL_BLEND' )

            useProgram( modules, drawProgram )
            useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
            useTexture( modules, fboTextureIndex, 0, drawProgram )

            OpenGLES2.Uniformf( drawOffsetLocationIndex, 1, [ 0 ] )

            drawVertexBufferData( modules, vertexBufferDataSetup )

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class MultitextureFillrateBenchmark( Benchmark ):
    def __init__( self, fboSize, format, filtering, textures, overdraw ):
        Benchmark.__init__( self )
        self.fboSize   = fboSize
        self.format    = format
        self.filtering = filtering
        self.textures  = textures
        self.overdraw  = overdraw
        fboString = 'fullscreen'
        if self.fboSize:
            fboString = '%dx%d fbo' % self.fboSize

        self.name = 'OPENGLES2 multitex fillrate (%s %d %s FLOWER ovrdrw %d %s)' % ( fboString,
                                                                                     self.textures,
                                                                                     self.format,
                                                                                     self.overdraw,
                                                                                     self.filtering, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        if self.fboSize:
            fboWidth  = self.fboSize[ 0 ]
            fboHeight = self.fboSize[ 1 ]
        else:
            fboWidth  = screenWidth
            fboHeight = screenHeight

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;

        void main()
        {
           gl_Position = vec4( gVertex, 0, 1 );
           uv = gTexCoord0;
        }
        """

        rawFillFragmentShaderSource = """
        precision mediump float;"""

        for i in range( self.textures ):
            rawFillFragmentShaderSource += ( 'uniform sampler2D gTexture%d;' % i )

        accessString = """
        vec4 color = vec4( 0.0, 0.0, 0.0, 0.0 );
        const float r = 1.0 / %f;
        """ % self.textures;

        for i in range( self.textures ):
            accessString += ( 'color += r * texture2D( gTexture%d, uv );' % i )

        accessString += 'gl_FragColor = color;'

        rawFillFragmentShaderSource += """
        varying vec2 uv;

        void main()
        {
           %s
        }
        """ % accessString

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv );
        }"""

        vertexShaderSource       = formatShader( rawVertexShaderSource )
        fillFragmentShaderSource = formatShader( rawFillFragmentShaderSource )
        drawFragmentShaderSource = formatShader( rawDrawFragmentShaderSource )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        # Create mesh and VBOs.
        texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

        mesh = MeshStripPlane( 2,
                               2,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                               None,
                               None,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0  ] )

        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )

        # Create programs.
        fillProgram = createProgram( modules, indexTracker, vertexShaderSource, fillFragmentShaderSource, vertexDataSetup )
        useProgram( modules, fillProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, fillProgram )
        validateProgram( modules, fillProgram )

        drawProgram = createProgram( modules, indexTracker, vertexShaderSource, drawFragmentShaderSource, vertexDataSetup )
        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        validateProgram( modules, drawProgram )

        # Create texture.
        imageTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex, 'IMAGES_FLOWER', IMAGES_FORMATS[ self.format ], fboWidth, fboHeight )

        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )

        OpenGLES2.CreateTextureData( imageTextureDataIndex, dataIndex, fboWidth, fboHeight, FORMATS[ self.format ] )

        # Activate fill program and create textures for multitex fill.
        useProgram( modules, fillProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, fillProgram )

        fillTextureLocationIndices = []
        for i in range( self.textures ):
            imageTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( imageTextureIndex )
            OpenGLES2.ActiveTexture( i )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )

            if self.filtering == 'nearest':
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
            else:
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            OpenGLES2.TexImage2D( imageTextureDataIndex, 'GL_TEXTURE_2D' )

            fillTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( fillTextureLocationIndex, fillProgram.programIndex, 'gTexture%d' % i )
            OpenGLES2.Uniformi( fillTextureLocationIndex, 1, [ i ] )

            fillTextureLocationIndices.append( fillTextureLocationIndex )

        if self.fboSize:
            # Create a FBO and textures (RGBA8) used for fill.
            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_RGBA8888' )

            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.ClearColor( [ 0, 0, 0, 1 ] )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.fboSize:
            # Draw fill to FBO/texture.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
            OpenGLES2.Viewport( 0, 0, fboWidth, fboHeight )

            useProgram( modules, fillProgram )
            useVertexBufferData( modules, vertexBufferDataSetup, fillProgram )

            OpenGLES2.Enable( 'GL_BLEND' )

            for i in range( self.textures ):
                OpenGLES2.ActiveTexture( i )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
                OpenGLES2.Uniformi( fillTextureLocationIndices[ i ], 1, [ i ] )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        for i in range( self.overdraw ):
            drawVertexBufferData( modules, vertexBufferDataSetup )

        if self.fboSize:
            # Draw fill FBO/texture to the screen.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
            OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

            OpenGLES2.Disable( 'GL_DEPTH_TEST' )
            OpenGLES2.Disable( 'GL_BLEND' )

            useProgram( modules, drawProgram )
            useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
            useTexture( modules, fboTextureIndex, 0, drawProgram )

            drawVertexBufferData( modules, vertexBufferDataSetup )

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class TrianglesBenchmark( Benchmark ):
    def __init__( self, fboSize, gridx, gridy, overdraw, type, scaling, culling ):
        Benchmark.__init__( self )
        self.fboSize  = fboSize
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.type     = type
        self.scaling  = scaling
        self.culling  = culling

        # Create strings for benchmark name.
        if self.scaling == 'none':
            scaleString = ''
        else:
            scaleString = ' downscaled'
        if self.culling == 'none':
            cullString = ''
        else:
            cullString = ' ' + self.culling + ' culled'

        typeString = ''
        self.usesColor     = False
        self.usesTexture   = False
        self.usesNormals   = False
        self.textureFormat = None
        if self.type[ 0 ] == 'white':
            typeString = 'constant white'
        elif self.type[ 0 ] == 'smooth':
            self.usesColor   = True
            typeString = 'per-vrtx smooth'
        elif self.type[ 0 ] == 'textured':
            self.usesTexture   = True
            self.textureFormat = self.type[ 1 ]
            typeString = '%s' % self.textureFormat
        elif self.type[ 0 ] == 'per-vertex light':
            self.usesColor   = True
            self.usesNormals = True
            typeString = 'per-vrtx light'
        elif self.type[ 0 ] == 'per-fragment light':
            self.usesColor   = True
            self.usesNormals = True
            typeString = 'per-frgmnt light'
        elif self.type[ 0 ] == 'textured per-vertex light':
            self.usesTexture = True
            self.usesNormals = True
            self.textureFormat = self.type[ 1 ]
            typeString = '%s per-vrtx light' % self.textureFormat
        elif self.type[ 0 ] == 'textured per-fragment light':
            self.usesTexture = True
            self.usesNormals = True
            self.textureFormat = self.type[ 1 ]
            typeString = '%s per-frgmnt light' % self.textureFormat
        else:
            raise 'Unexpected type ' + str( self.type )

        if self.culling == 'none':
            self.winding = MESH_CCW_WINDING
        elif self.culling == 'fully':
            self.winding = MESH_CW_WINDING
        elif self.culling == 'half':
            self.winding = MESH_ALTERNATING_WINDING
        else:
            raise 'Invalid type'

        self.gridTriangleCount = stripTriangleCount( self.gridx, self.gridy, self.winding )
        fboString = 'fullscreen'
        if self.fboSize:
            fboString = '%dx%d fbo' % self.fboSize

        # Format benchmark name.
        self.name = 'OPENGLES2 tri throughput (%s %d%s%s tris %dx%dx%d %s)' % ( fboString,
                                                                                self.gridTriangleCount * self.overdraw,
                                                                                scaleString,
                                                                                cullString,
                                                                                self.gridx,
                                                                                self.gridy,
                                                                                self.overdraw,
                                                                                typeString, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # TnL shaders.
        if self.type[ 0 ] == 'white':
            rawVertexShaderSource = """
            precision mediump float;
            attribute vec4 gVertex;
            uniform mat4 gModelView;

            void main()
            {
               gl_Position  = gModelView * gVertex;
            }"""

            rawFragmentShaderSource = """
            precision lowp float;

            void main()
            {
               gl_FragColor = vec4( 1.0, 1.0, 1.0, 1.0 );
            }"""

        elif self.type[ 0 ] == 'smooth':
            rawVertexShaderSource = """
            precision mediump float;
            attribute vec4 gVertex;
            attribute vec4 gColor;
            uniform mat4 gModelView;
            varying vec4 gVSColor;

            void main()
            {
               gl_Position  = gModelView * gVertex;
               gVSColor = gColor;
            }"""

            rawFragmentShaderSource = """
            precision mediump float;
            varying vec4 gVSColor;

            void main()
            {
                gl_FragColor = gVSColor;
            }"""

        elif self.type[ 0 ] == 'textured':
            rawVertexShaderSource = """
            precision mediump float;
            attribute vec4 gVertex;
            attribute vec2 gTexCoord0;
            varying vec2 gVSTexCoord0;
            uniform mat4 gModelView;

            void main()
            {
               gl_Position  = gModelView * gVertex;
               gVSTexCoord0 = gTexCoord0;
            }"""

            rawFragmentShaderSource = """
            precision lowp float;
            uniform sampler2D gTexture0;
            varying vec2 gVSTexCoord0;

            void main()
            {
               gl_FragColor = texture2D( gTexture0, gVSTexCoord0 );
            }"""

        elif self.type[ 0 ] == 'per-vertex light':
            rawVertexShaderSource = """
            precision mediump float;
            attribute vec4 gVertex;
            attribute vec3 gNormal;
            uniform mat4 gModelView;
            uniform mat4 gITModelView;
            uniform vec3 gLightDirection;
            uniform vec4 gAmbientLight;
            uniform vec4 gDiffuseLight;
            uniform vec4 gSpecularLight;
            uniform vec4 gAmbientMaterial;
            uniform vec4 gDiffuseMaterial;
            uniform vec4 gSpecularMaterial;
            uniform float gShininess;
            varying vec4 gVSColor;

            void main()
            {
                float NdotL;
                vec4 ambientColor;
                vec4 diffuseColor;
                vec4 specularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
                vec3 normal;
                ambientColor = gAmbientLight * gAmbientMaterial;
                normal = vec3( gITModelView * vec4( gNormal.x, gNormal.y, gNormal.z, 1.0 ) );
                NdotL = max( abs( dot( normal, gLightDirection ) ), 0.0 );
                diffuseColor = gDiffuseLight * gDiffuseMaterial * NdotL;
                if( NdotL > 0.0 )
                {
                    vec3 eye = vec3( 0.0, 0.0, -1.0 );
                    vec3 HV = normalize( gLightDirection + eye );
                    float NdotHV = max( abs( dot( normal, HV ) ), 0.0 );
                    specularColor = gSpecularLight * gSpecularMaterial * pow( NdotHV, gShininess );
                }
                gl_Position = gModelView * gVertex;
                gVSColor = ambientColor + diffuseColor + specularColor;
            }"""

            rawFragmentShaderSource = """
            precision mediump float;
            varying vec4 gVSColor;

            void main()
            {
               gl_FragColor = gVSColor;
            }"""

        elif self.type[ 0 ] == 'per-fragment light':
            rawVertexShaderSource = """
            precision mediump float;
            attribute vec4 gVertex;
            attribute vec4 gNormal;
            uniform mat4 gModelView;
            uniform mat4 gITModelView;
            uniform vec3 gLightDirection;
            uniform vec4 gDiffuseLight;
            uniform vec4 gAmbientLight;
            uniform vec4 gDiffuseMaterial;
            uniform vec4 gAmbientMaterial;
            varying vec4 gVSAmbientColor;
            varying vec4 gVSDiffuseColor;
            varying vec3 gVSNormal;
            varying vec3 gVSReflect;

            void main()
            {
                gVSNormal = vec3( gITModelView * gNormal );
                gVSAmbientColor = gAmbientLight * gAmbientMaterial;
                gVSDiffuseColor = gDiffuseLight * gDiffuseMaterial;
                gVSReflect = reflect( gLightDirection, gVSNormal );
                gl_Position = gModelView * gVertex;
            }"""

            rawFragmentShaderSource = """
            precision mediump float;
            uniform vec3 gLightDirection;
            uniform vec4 gSpecularLight;
            uniform vec4 gSpecularMaterial;
            uniform float gShininess;
            varying vec4 gVSAmbientColor;
            varying vec4 gVSDiffuseColor;
            varying vec3 gVSNormal;
            varying vec3 gVSLightDirection;
            varying vec3 gVSReflect;

            void main()
            {
                float NdotL;
                vec4 diffuseColor;
                vec4 specularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
                NdotL = max( abs( dot( gVSNormal, gLightDirection ) ), 0.0 );
                diffuseColor = gVSDiffuseColor * NdotL;
                if( NdotL > 0.0 )
                {
                    vec3 eye = vec3( 0.0, 0.0, -1.0 );
                    float RdotEye = max( abs( dot( gVSReflect, eye ) ), 0.0 );
                    specularColor = gSpecularLight * gSpecularMaterial * pow( RdotEye, gShininess );
                }
                gl_FragColor = gVSAmbientColor + diffuseColor + specularColor;
            }"""

        elif self.type[ 0 ] == 'textured per-vertex light':
            rawVertexShaderSource = """
            precision mediump float;
            attribute vec4 gVertex;
            attribute vec3 gNormal;
            attribute vec2 gTexCoord0;
            uniform mat4 gModelView;
            uniform mat4 gITModelView;
            uniform vec3 gLightDirection;
            uniform vec4 gAmbientLight;
            uniform vec4 gDiffuseLight;
            uniform vec4 gSpecularLight;
            uniform vec4 gAmbientMaterial;
            uniform vec4 gDiffuseMaterial;
            uniform vec4 gSpecularMaterial;
            uniform float gShininess;
            varying vec4 gVSDiffuseColor;
            varying vec4 gVSSpecularColor;
            varying vec2 gVSTexCoord0;

            void main()
            {
                float NdotL;
                vec4 ambientColor;
                vec3 normal;
                ambientColor = gAmbientLight * gAmbientMaterial;
                normal = vec3( gITModelView * vec4( gNormal.x, gNormal.y, gNormal.z, 1.0 ) );
                NdotL = max( abs( dot( normal, gLightDirection ) ), 0.0 );
                gVSDiffuseColor = ambientColor + gDiffuseLight * gDiffuseMaterial * NdotL;
                gVSSpecularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
                if( NdotL > 0.0 )
                {
                   vec3 eye = vec3( 0.0, 0.0, -1.0 );
                   vec3 HV = normalize( gLightDirection + eye );
                   float NdotHV = max( abs( dot( normal, HV ) ), 0.0 );
                   gVSSpecularColor = gSpecularLight * gSpecularMaterial * pow( NdotHV, gShininess );
                }
                gl_Position = gModelView * gVertex;
                gVSTexCoord0 = gTexCoord0;
             }"""

            rawFragmentShaderSource = """
            precision lowp float;
            uniform sampler2D gTexture0;
            varying vec4 gVSDiffuseColor;
            varying vec4 gVSSpecularColor;
            varying vec2 gVSTexCoord0;

            void main()
            {
                vec4 texColor = texture2D( gTexture0, gVSTexCoord0 );
                gl_FragColor = ( texColor * gVSDiffuseColor ) + gVSSpecularColor;
            }"""

        elif self.type[ 0 ] == 'textured per-fragment light':
            rawVertexShaderSource = """
            precision mediump float;
            attribute vec4 gVertex;
            attribute vec4 gNormal;
            attribute vec2 gTexCoord0;
            uniform mat4 gModelView;
            uniform mat4 gITModelView;
            uniform vec3 gLightDirection;
            uniform vec4 gDiffuseLight;
            uniform vec4 gAmbientLight;
            uniform vec4 gDiffuseMaterial;
            uniform vec4 gAmbientMaterial;
            varying vec4 gVSAmbientColor;
            varying vec4 gVSDiffuseColor;
            varying vec3 gVSNormal;
            varying vec3 gVSReflect;
            varying vec2 gVSTexCoord0;

            void main()
            {
                gVSNormal = vec3( gITModelView * vec4( gNormal.x, gNormal.y, gNormal.z, 1.0 ) );
                gVSAmbientColor = gAmbientLight * gAmbientMaterial;
                gVSDiffuseColor = gDiffuseLight * gDiffuseMaterial;
                gVSReflect = reflect( gLightDirection, gVSNormal );
                gl_Position = gModelView * gVertex;
                gVSTexCoord0 = gTexCoord0;
            }"""

            rawFragmentShaderSource = """
            precision mediump float;
            uniform sampler2D gTexture0;
            uniform vec3 gLightDirection;
            uniform vec4 gSpecularLight;
            uniform vec4 gSpecularMaterial;
            uniform float gShininess;
            varying vec2 gVSTexCoord0;
            varying vec4 gVSAmbientColor;
            varying vec4 gVSDiffuseColor;
            varying vec3 gVSNormal;
            varying vec3 gVSLightDirection;
            varying vec3 gVSReflect;

            void main()
            {
                float NdotL;
                vec4 diffuseColor;
                vec4 specularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
                vec4 texColor;
                NdotL = max( abs( dot( gVSNormal, gLightDirection ) ), 0.0 );
                diffuseColor = gVSDiffuseColor * NdotL;
                if( NdotL > 0.0 )
                {
                    vec3 eye = vec3( 0.0, 0.0, -1.0 );
                    float RdotEye = max( abs( dot( gVSReflect, eye ) ), 0.0 );
                    specularColor = gSpecularLight * gSpecularMaterial * pow( RdotEye, gShininess );
                }
                texColor = texture2D( gTexture0, gVSTexCoord0 );
                gl_FragColor = texColor * ( gVSAmbientColor + diffuseColor ) + specularColor;
             }"""

        else:
            raise 'Unexpected'

        # Draw-to-screen shaders.
        rawDrawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 gVSTexCoord0;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            gVSTexCoord0 = gTexCoord0;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0 );
        }"""

        # Format selected shader strings, escapes new line chars.
        tnlVertexShaderSource    = formatShader( rawVertexShaderSource )
        tnlFragmentShaderSource  = formatShader( rawFragmentShaderSource )
        drawVertexShaderSource   = formatShader( rawDrawVertexShaderSource )
        drawFragmentShaderSource = formatShader( rawDrawFragmentShaderSource )

        # Screen size, fboSize.
        ( screenWidth, screenHeight, ) = target.getScreenSize()

        if self.fboSize:
            fboWidth  = self.fboSize[ 0 ]
            fboHeight = self.fboSize[ 1 ]
        else:
            fboWidth  = screenWidth
            fboHeight = screenHeight

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        # Setup OpenGLES2 surface/context.
        state = setupOpenGLES2( modules, indexTracker, target )

        # Create a TnL mesh with required vertex attributes.
        colorAttribute = None
        if self.usesColor:
            colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        texCoordAttribute = None
        if self.usesTexture:
            texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

        normalAttribute = None
        if self.usesNormals:
            normalAttribute = MeshAttribute( MESH_TYPE_FLOAT )

        mesh = MeshStripPlane( self.gridx,
                               self.gridy,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 4 ),
                               colorAttribute,
                               normalAttribute,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               self.winding,
                               False )

        assert( self.gridTriangleCount == ( len( mesh.indices ) - 2 ) )

        # Create TnL program and vertex buffer objects.
        tnlVertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        tnlProgram         = createProgram( modules, indexTracker, tnlVertexShaderSource, tnlFragmentShaderSource, tnlVertexDataSetup )
        useProgram( modules, tnlProgram )
        tnlVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, tnlVertexDataSetup )
        useVertexBufferData( modules, tnlVertexBufferDataSetup, tnlProgram )
        validateProgram( modules, tnlProgram )

        if self.fboSize:
            # Create draw-to-screen plane.
            planeTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

            plane = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    planeTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            plane.translate( [ -0.5, -0.5, 0 ] )
            plane.scale( [ 2.0, 2.0  ] )

            # Create draw-to-screen program and vertex buffer objects.
            drawVertexDataSetup       = setupVertexData( modules, indexTracker, plane )
            drawProgram               = createProgram( modules, indexTracker, drawVertexShaderSource, drawFragmentShaderSource, drawVertexDataSetup )
            useProgram( modules, drawProgram )
            drawVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, drawVertexDataSetup )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            validateProgram( modules, drawProgram )

        # Activate TnL program.
        useProgram( modules, tnlProgram )

        if self.usesTexture:
            # Create a flower texture for drawing the mesh, if required.
            imageTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            imageTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

            imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
            Images.CreateData( imagesDataIndex, 'IMAGES_FLOWER', IMAGES_FORMATS[ self.textureFormat ], fboWidth, fboHeight )

            dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )

            OpenGLES2.CreateTextureData( imageTextureDataIndex, dataIndex, fboWidth, fboHeight, FORMATS[ self.textureFormat ] )

            OpenGLES2.GenTexture( imageTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
            OpenGLES2.TexImage2D( imageTextureDataIndex, 'GL_TEXTURE_2D' )

            # Use texture in TnL program.
            useTexture( modules, imageTextureIndex, 0, tnlProgram )

        # Get modelview and inverse modelview uniforms. Setup material and ligh
        # parameters, if needed.
        gModelView = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gModelView, tnlProgram.programIndex, 'gModelView' )

        if self.usesNormals:
            gITModelView = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( gITModelView, tnlProgram.programIndex, 'gITModelView' )

            material = Material( [ 1.0, 1.0, 1.0, 1.0 ],
                                 [ 1.0, 1.0, 1.0, 1.0 ],
                                 [ 1.0, 1.0, 1.0, 1.0 ],
                                 0.5 )
            light = Light( [ 0.7, 0.3, 0.3, 1.0 ],
                           [ 0.0, 0.0, -1.0 ],            # World-space
                           [ 0.3, 0.7, 0.3, 1.0 ],
                           [ 0.3, 0.3, 0.7, 1.0 ] )

            lightSetup = setupLight( modules, indexTracker, light, material, tnlProgram )
            useLight( modules, lightSetup )

        if self.culling != 'none':
            OpenGLES2.Enable( 'GL_CULL_FACE' )

        # Draw the Tnl mesh with blending to ensure overdraw acts as expected.
        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA', )

        if self.fboSize:
            # Create a FBO and textures (RGBA8, DEPTH) used for TnL drawing, if specified.
            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_RGBA8888' )

            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            fboDepthTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboDepthTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_DEPTH' )

            # Store current framebuffer for future reference; to cope with
            # systems using an FBO for default OpenGLES2 surface.
            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.fboSize:
            # Draw TnL mesh to FBO/texture.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
            OpenGLES2.Viewport( 0, 0, fboWidth, fboHeight )

            useProgram( modules, tnlProgram )
            useVertexBufferData( modules, tnlVertexBufferDataSetup, tnlProgram )

            if self.usesTexture:
                useTexture( modules, imageTextureIndex, 0, tnlProgram )

            if self.usesNormals:
                useLight( modules, lightSetup )

            OpenGLES2.Enable( 'GL_BLEND' )
            if self.culling != 'none':
                OpenGLES2.Enable( 'GL_CULL_FACE' )

        # Create buffers.
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        offset = 0.5
        scalex = 2.0
        scaley = 2.0

        if self.scaling != 'none':
            scalex = 2.0 / float( fboWidth )
            scaley = 2.0 / float( fboHeight )

        for i in range( self.overdraw ):

            # Set modelviewprojection and its inverse as uniforms.
            m = [  scalex,        0.0,       0.0,    0.0,
                      0.0,     scaley,       0.0,    0.0,
                      0.0,        0.0,       2.0,    0.0,
                     -1.0,       -1.0,    offset,    1.0 ]
            OpenGLES2.UniformMatrix( gModelView, 1, m )

            if self.usesNormals:
                OpenGLES2.UniformMatrix( gITModelView, 1, numpy.matrix( m ).I.T.tolist()[ 0 ] )

            # Draw Tnl mesh.
            drawVertexBufferData( modules, tnlVertexBufferDataSetup )

        if self.fboSize:
            # Draw TnL FBO/texture to the screen/default FBO.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
            OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

            OpenGLES2.Disable( 'GL_BLEND' )
            OpenGLES2.Disable( 'GL_CULL_FACE' )

            useProgram( modules, drawProgram )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            useTexture( modules, fboTextureIndex, 0, drawProgram )

            drawVertexBufferData( modules, drawVertexBufferDataSetup )

        target.swapBuffers( state )

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class DependentTexturingBenchmark( Benchmark ):
    def __init__( self, fboSize, format, type, overdraw ):
        Benchmark.__init__( self )
        self.fboSize  = fboSize
        self.format   = format
        self.type     = type
        self.overdraw = overdraw
        fboString = 'fullscreen'
        if self.fboSize:
            fboString = '%dx%d fbo' % self.fboSize

        self.name  = "OPENGLES2 dependent texturing (%s %s %s dependency %d overdraw)" % ( fboString,
                                                                                           self.format,
                                                                                           self.type,
                                                                                           self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 gVSTexCoord0;

        void main()
        {
           gl_Position = vec4( gVertex, 0, 1 );
           gVSTexCoord0 = gTexCoord0;
        }"""

        if self.type == 'multi':
            rawFragmentShaderSource = """
            precision mediump float;
            uniform sampler2D gTexture0;
            uniform sampler2D gTexture1;
            uniform sampler2D gTexture2;
            varying vec2 gVSTexCoord0;

            void main()
            {
                gl_FragColor = texture2D( gTexture2, vec2( texture2D( gTexture0, gVSTexCoord0 ).x, texture2D( gTexture1, gVSTexCoord0 ).y ) );
            }"""

        else:
            rawFragmentShaderSource = """
            precision mediump float;
            uniform sampler2D gTexture0;
            uniform sampler2D gTexture1;
            varying vec2 gVSTexCoord0;

            void main()
            {
                gl_FragColor = texture2D( gTexture1, vec2( texture2D( gTexture0, gVSTexCoord0 ) ) );
            }"""

        # Draw-to-screen shaders.
        rawDrawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 gVSTexCoord0;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            gVSTexCoord0 = gTexCoord0;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0 );
        }"""

        # Format selected shader strings.
        depVertexShaderSource    = formatShader( rawVertexShaderSource )
        depFragmentShaderSource  = formatShader( rawFragmentShaderSource )
        drawVertexShaderSource   = formatShader( rawDrawVertexShaderSource )
        drawFragmentShaderSource = formatShader( rawDrawFragmentShaderSource )

        # Screen size, fboSize.
        ( screenWidth, screenHeight, ) = target.getScreenSize()

        if self.fboSize:
            fboWidth  = self.fboSize[ 0 ]
            fboHeight = self.fboSize[ 1 ]
        else:
            fboWidth  = screenWidth
            fboHeight = screenHeight

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        # Setup OpenGLES2 surface and context.
        state = setupOpenGLES2( modules, indexTracker, target )

        # Create full screen mesh.
        texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

        mesh = MeshStripPlane( 2,
                               2,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                               None,
                               None,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0  ] )

        # Create vertex data from the mesh.
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )

        # Create program and VBO for dependent texturing.
        depProgram = createProgram( modules, indexTracker, depVertexShaderSource, depFragmentShaderSource, vertexDataSetup )
        useProgram( modules, depProgram )
        depVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )
        useVertexBufferData( modules, depVertexBufferDataSetup, depProgram )
        validateProgram( modules, depProgram )

        if self.fboSize:
            # Create program and VBO for draw-to-screen.
            drawProgram = createProgram( modules, indexTracker, drawVertexShaderSource, drawFragmentShaderSource, vertexDataSetup )
            useProgram( modules, drawProgram )
            drawVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            validateProgram( modules, drawProgram )

        # Create flower texture.
        useProgram( modules, depProgram )
        imageTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex, 'IMAGES_FLOWER', IMAGES_FORMATS[ self.format ], fboWidth, fboHeight )

        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )

        OpenGLES2.CreateTextureData( imageTextureDataIndex, dataIndex, fboWidth, fboHeight, FORMATS[ self.format ] )

        imageTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( imageTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( imageTextureDataIndex, 'GL_TEXTURE_2D' )

        # Create different textures depending on the dependency.
        if self.type == 'noise':
            noiseTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNoiseTextureData( noiseTextureDataIndex,
                                              'OPENGLES2_NOISE_RANDOM',
                                              [ 1, 1, 1, 1, 1, 22 ],
                                              fboWidth, fboHeight,
                                              'OFF',
                                              FORMATS[ self.format ] )

            noiseTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( noiseTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', noiseTextureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
            OpenGLES2.TexImage2D( noiseTextureDataIndex, 'GL_TEXTURE_2D' )

            noiseTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( noiseTextureLocationIndex, depProgram.programIndex, 'gTexture0' )
            OpenGLES2.Uniformi( noiseTextureLocationIndex, 1, [ 0 ] )

            OpenGLES2.ActiveTexture( 0 )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', noiseTextureIndex )

            imageTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( imageTextureLocationIndex, depProgram.programIndex, 'gTexture1' )
            OpenGLES2.Uniformi( imageTextureLocationIndex, 1, [ 1 ] )

            OpenGLES2.ActiveTexture( 1 )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )

        elif self.type == 'lookup':
            gradientTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateGradientTextureData( gradientTextureDataIndex,
                                                 'OPENGLES2_GRADIENT_DIAGONAL',
                                                 [ 0, 0, 0, 0 ],
                                                 [ 1, 1, 1, 1 ],
                                                 fboWidth, fboHeight,
                                                 'OFF',
                                                 FORMATS[ self.format ] )

            gradientTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( gradientTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', gradientTextureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
            OpenGLES2.TexImage2D( gradientTextureDataIndex, 'GL_TEXTURE_2D' )

            gradientTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( gradientTextureLocationIndex, depProgram.programIndex, 'gTexture1' )
            OpenGLES2.Uniformi( gradientTextureLocationIndex, 1, [ 1 ] )

            OpenGLES2.ActiveTexture( 1 )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', gradientTextureIndex )

            imageTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( imageTextureLocationIndex, depProgram.programIndex, 'gTexture0' )
            OpenGLES2.Uniformi( imageTextureLocationIndex, 1, [ 0 ] )

            OpenGLES2.ActiveTexture( 0 )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )

        elif self.type == 'multi':
            vgradientTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateGradientTextureData( vgradientTextureDataIndex,
                                                 'OPENGLES2_GRADIENT_HORIZONTAL',
                                                 [ 1, 0, 0, 0 ],
                                                 [ 0, 0, 0, 0 ],
                                                 fboWidth, fboHeight,
                                                 'OFF',
                                                 FORMATS[ self.format ] )

            vgradientTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( vgradientTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', vgradientTextureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
            OpenGLES2.TexImage2D( vgradientTextureDataIndex, 'GL_TEXTURE_2D' )

            hgradientTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateGradientTextureData( hgradientTextureDataIndex,
                                                 'OPENGLES2_GRADIENT_VERTICAL',
                                                 [ 0, 0, 0, 0 ],
                                                 [ 0, 1, 0, 0 ],
                                                 fboWidth, fboHeight,
                                                 'OFF',
                                                 FORMATS[ self.format ] )

            hgradientTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( hgradientTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', hgradientTextureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
            OpenGLES2.TexImage2D( hgradientTextureDataIndex, 'GL_TEXTURE_2D' )

            vgradientTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( vgradientTextureLocationIndex, depProgram.programIndex, 'gTexture0' )
            OpenGLES2.Uniformi( vgradientTextureLocationIndex, 1, [ 0 ] )

            OpenGLES2.ActiveTexture( 0 )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', vgradientTextureIndex )

            hgradientTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( hgradientTextureLocationIndex, depProgram.programIndex, 'gTexture1' )
            OpenGLES2.Uniformi( hgradientTextureLocationIndex, 1, [ 1 ] )

            OpenGLES2.ActiveTexture( 1 )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', hgradientTextureIndex )

            imageTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( imageTextureLocationIndex, depProgram.programIndex, 'gTexture2' )
            OpenGLES2.Uniformi( imageTextureLocationIndex, 1, [ 2 ] )

            OpenGLES2.ActiveTexture( 2 )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )

        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.Enable( 'GL_BLEND' )

        if self.fboSize:
            # Create a FBO and textures (RGBA8) used for dep texturing.
            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_RGBA8888' )

            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        useVertexBufferData( modules, depVertexBufferDataSetup, depProgram )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.fboSize:
            # Draw TnL mesh to FBO/texture.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
            OpenGLES2.Viewport( 0, 0, fboWidth, fboHeight )

            useProgram( modules, depProgram )
            useVertexBufferData( modules, depVertexBufferDataSetup, depProgram )

            if self.type == 'noise':
                OpenGLES2.ActiveTexture( 0 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', noiseTextureIndex )
                OpenGLES2.Uniformi( noiseTextureLocationIndex, 1, [ 0 ] )

                OpenGLES2.ActiveTexture( 1 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
                OpenGLES2.Uniformi( imageTextureLocationIndex, 1, [ 1 ] )

            elif self.type == 'lookup':
                OpenGLES2.ActiveTexture( 1 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', gradientTextureIndex )
                OpenGLES2.Uniformi( gradientTextureLocationIndex, 1, [ 1 ] )

                OpenGLES2.ActiveTexture( 0 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
                OpenGLES2.Uniformi( imageTextureLocationIndex, 1, [ 0 ] )

            elif self.type == 'multi':
                OpenGLES2.ActiveTexture( 0 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', vgradientTextureIndex )
                OpenGLES2.Uniformi( vgradientTextureLocationIndex, 1, [ 0 ] )

                OpenGLES2.ActiveTexture( 1 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', hgradientTextureIndex )
                OpenGLES2.Uniformi( hgradientTextureLocationIndex, 1, [ 1 ] )

                OpenGLES2.ActiveTexture( 2 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
                OpenGLES2.Uniformi( imageTextureLocationIndex, 1, [ 2 ] )

            OpenGLES2.Enable( 'GL_BLEND' )

        for i in range( self.overdraw ):
            drawVertexBufferData( modules, depVertexBufferDataSetup )

        if self.fboSize:
            # Draw TnL FBO/texture to the screen/default FBO.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
            OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

            OpenGLES2.Disable( 'GL_BLEND' )

            useProgram( modules, drawProgram )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            useTexture( modules, fboTextureIndex, 0, drawProgram )

            drawVertexBufferData( modules, drawVertexBufferDataSetup )

        target.swapBuffers( state )

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ParticlesBenchmark( Benchmark ):
    def __init__( self, fboSize, gridx, gridy, overdraw, size, format, rounded ):
        Benchmark.__init__( self )
        self.fboSize  = fboSize
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.size     = size
        self.format   = format
        self.rounded  = rounded
        roundedString = ''
        if self.rounded:
            roundedString = 'shader alpha-rounded'
        fboString = 'fullscreen'
        if self.fboSize:
            fboString = '%dx%d fbo' % self.fboSize

        self.name = "OPENGLES2 particles (%s %d particles %d size %s %s)" % ( fboString,
                                                                              self.gridx * self.gridy * self.overdraw,
                                                                              self.size,
                                                                              self.format,
                                                                              roundedString, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # Shader source code
        rawParticleVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute float gSize;
        uniform mat4 mvp;

        void main()
        {
           gl_Position = mvp * gVertex;
           gl_PointSize = gSize / gl_Position.w;
        }"""

        if self.rounded:
            rawParticleFragmentShaderSource = """
            precision mediump float;
            uniform sampler2D gTexture0;
            const vec2 center = vec2( 0.5, 0.5 );
            float x, y, d;

            void main()
            {
                vec4 color = texture2D( gTexture0, gl_PointCoord.xy );
                x = gl_PointCoord.x - 0.5;
                y = gl_PointCoord.y - 0.5;
                d = x * x + y * y;

                color.a = 1.0 - smoothstep( 0.22, 0.25, d );
                gl_FragColor = color;
            }"""
        else:
            rawParticleFragmentShaderSource = """
            precision mediump float;
            uniform sampler2D gTexture0;

            void main()
            {
               gl_FragColor = texture2D( gTexture0, gl_PointCoord.xy );
            }"""

        # Draw-to-screen shaders.
        rawDrawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 gVSTexCoord0;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            gVSTexCoord0 = gTexCoord0;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0 );
        }"""

        # Format selected shader strings.
        particleVertexShaderSource    = formatShader( rawParticleVertexShaderSource )
        particleFragmentShaderSource  = formatShader( rawParticleFragmentShaderSource )
        drawVertexShaderSource        = formatShader( rawDrawVertexShaderSource )
        drawFragmentShaderSource      = formatShader( rawDrawFragmentShaderSource )

        # Screen size, fboSize.
        ( screenWidth, screenHeight, ) = target.getScreenSize()

        if self.fboSize:
            fboWidth  = self.fboSize[ 0 ]
            fboHeight = self.fboSize[ 1 ]
        else:
            fboWidth  = screenWidth
            fboHeight = screenHeight

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        # Setup OpenGLES2 surface/context.
        state = setupOpenGLES2( modules, indexTracker, target )

        texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

        mesh = MeshStripPlane( self.gridx,
                               self.gridy,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 4 ),
                               None,
                               None,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0  ] )

        particleVertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        particleProgram         = createProgram( modules,
                                                 indexTracker,
                                                 particleVertexShaderSource,
                                                 particleFragmentShaderSource,
                                                 particleVertexDataSetup )
        useProgram( modules, particleProgram )
        particleVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, particleVertexDataSetup )
        useVertexBufferData( modules, particleVertexBufferDataSetup, particleProgram )
        validateProgram( modules, particleProgram )

        if self.fboSize:
            # Create draw-to-screen plane.
            planeTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

            plane = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    planeTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            plane.translate( [ -0.5, -0.5, 0 ] )
            plane.scale( [ 2.0, 2.0  ] )

            # Create draw-to-screen program and vertex buffer objects.
            drawVertexDataSetup       = setupVertexData( modules, indexTracker, plane )
            drawProgram               = createProgram( modules,
                                                       indexTracker,
                                                       drawVertexShaderSource,
                                                       drawFragmentShaderSource,
                                                       drawVertexDataSetup )
            useProgram( modules, drawProgram )
            drawVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, drawVertexDataSetup )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            validateProgram( modules, drawProgram )

            useProgram( modules, particleProgram )
            useVertexBufferData( modules, particleVertexBufferDataSetup, particleProgram )

        # Create particle texture.
        imageTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        imageTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        if self.rounded:
            OpenGLES2.CreateSolidTextureData( imageTextureDataIndex,
                                              [ 0.2, 0.7, 0.2, 1 ],
                                              self.size,
                                              self.size,
                                              'OFF',
                                              FORMATS[ self.format ] )

        else:
            OpenGLES2.CreateShapeTextureData( imageTextureDataIndex,
                                              'OPENGLES2_SHAPE_ROUND',
                                              [ 0.2, 0.7, 0.2, 1,  0, 0, 0, 0, self.size ],
                                              'OFF',
                                              FORMATS[ self.format ] )

        OpenGLES2.GenTexture( imageTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( imageTextureDataIndex, 'GL_TEXTURE_2D' )

        useTexture( modules, imageTextureIndex, 0, particleProgram )

        sizes = [ self.size ] * ( self.gridx * self.gridy )
        sizeArrayIndex  = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( sizeArrayIndex, 'GL_FLOAT' )
        OpenGLES2.AppendToArray( sizeArrayIndex, sizes )

        sizeBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( sizeBufferIndex )
        OpenGLES2.BufferData( sizeBufferIndex, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', sizeArrayIndex, 0, 0 )

        sizeLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetAttribLocation( sizeLocationIndex, particleProgram.programIndex, 'gSize' )

        OpenGLES2.BufferVertexAttribPointer( sizeBufferIndex, 'ON', sizeLocationIndex, 1, 'OFF', 0 )
        OpenGLES2.EnableVertexAttribArray( sizeLocationIndex )

        modelViewProjectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( modelViewProjectionLocationIndex, particleProgram.programIndex, 'mvp' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )

        if self.fboSize:
            # Create a FBO and textures (RGBA8, DEPTH) used for TnL drawing, if specified.
            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_RGBA8888' )

            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            # Store current framebuffer for future reference; to cope with
            # systems using an FBO for default OpenGLES2 surface.
            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.fboSize:
            # Draw TnL mesh to FBO/texture.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
            OpenGLES2.Viewport( 0, 0, fboWidth, fboHeight )

            useProgram( modules, particleProgram )
            useVertexBufferData( modules, particleVertexBufferDataSetup, particleProgram )

            OpenGLES2.BufferVertexAttribPointer( sizeBufferIndex, 'ON', sizeLocationIndex, 1, 'OFF', 0 )
            OpenGLES2.EnableVertexAttribArray( sizeLocationIndex )

            useTexture( modules, imageTextureIndex, 0, particleProgram )
            OpenGLES2.Enable( 'GL_BLEND' )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        offset = 0.0
        delta  = 0.02

        for i in range( self.overdraw ):
            OpenGLES2.UniformMatrix( modelViewProjectionLocationIndex,
                                     1,
                                     [    1.0,     0.0,   0.0,  0.0,
                                          0.0,     1.0,   0.0,  0.0,
                                          0.0,     0.0,   1.0,  0.0,
                                       offset,  offset,   0.0,  1.0 ] )

            OpenGLES2.BufferDrawElements( particleVertexBufferDataSetup.indexBufferIndex,
                                          'OFF',
                                          'GL_POINTS',
                                          particleVertexBufferDataSetup.indexArrayLength,
                                          0 )
            offset += delta

        if self.fboSize:
            # Draw TnL FBO/texture to the screen/default FBO.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
            OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

            OpenGLES2.Disable( 'GL_BLEND' )

            useProgram( modules, drawProgram )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            useTexture( modules, fboTextureIndex, 0, drawProgram )

            drawVertexBufferData( modules, drawVertexBufferDataSetup )

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PulsBenchmark( Benchmark ):
    def __init__( self, fboSize, timeInc = 4 ):
        Benchmark.__init__( self )
        self.fboSize  = fboSize
        self.timeInc  = timeInc
        fboString = 'fullscreen'
        if self.fboSize:
            fboString = '%dx%d fbo' % self.fboSize

        self.name = "OPENGLES2 modified puls (%s)" % ( fboString, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # The following shader program is based on a WebGL port of the "Puls"
        # 256-byte intro by Jan Kadlec (Rrrola). The WebGL port was made by Dag
        # Agren (WAHa.06x36)
        #
        # The WebGL version of the shader is available at
        # http://wakaba.c3.cx/w/puls.html
        #
        # The original "Puls" intro can be found at
        # http://www.pouet.net/prod.php?which=53816
        #
        # Note that the fragment shader has been modified slightly; the for loop
        # in main is simplified so that the shader can be compiled with only
        # basic loop support. This leads into visual artifacts compared to the
        # original shader, but the performance of the modified shader should be
        # quite close to the original puls pixel shader.
        #
        rawPulsVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        uniform float h;
        varying vec2 tc;

        void main()
        {
            gl_Position = vec4( gVertex, 0.0, 1.0 );
            tc = vec2( gVertex.x, gVertex.y * h );
        }"""

        rawPulsFragmentShaderSource = """
        precision mediump float;
        uniform float t;
        varying vec2 tc;

        const float BLOWUP = 66.0;      /* 86.0 */
        const float MAXSTEPSHIFT = 8.0; /* 6.0  */
        const int MAXITERS = 34;        /* 26   */

        const float pi = 3.1415926535;

        float sum( vec3 v )
        {
           return v.x+v.y+v.z;
        }

        int func( vec3 pos, float stepshift )
        {
            vec3 v2 = abs( fract( pos ) - vec3( 0.5, 0.5, 0.5 ) ) / 2.0;
            float r = 0.0769 * sin( t * -0.0708 );
            float blowup = BLOWUP / pow( 2.0, stepshift + 8.0 );

            if( sum( v2 ) - 0.1445 + r < blowup )
            {
                return 1;
            }

            v2 = vec3( 0.25, 0.25, 0.25 ) - v2;

            if( sum( v2 ) - 0.1445 - r < blowup )
            {
                return 2;
            }

            int hue;
            float width;

            if( abs( sum( v2 ) - 3.0 * r - 0.375 ) < 0.03846 )
            {
                width = 0.1445;
                hue = 4;
            }
            else
            {
                width = 0.0676;
                hue = 3;
            }

            if( sum( abs( v2.zxy - v2.xyz ) ) - width < blowup )
            {
                return hue;
            }

            return 0;
        }

        void main()
        {
            float x = tc.x * 0.5;
            float y = tc.y * 0.5;

            float sin_a = sin( t * 0.00564 );
            float cos_a = cos( t * 0.00564 );

            vec3 dir = vec3( x, -y, 0.33594 - x * x - y * y );
            dir = vec3( dir.y, dir.z * cos_a - dir.x * sin_a, dir.x * cos_a + dir.z * sin_a );
            dir = vec3( dir.y, dir.z * cos_a - dir.x * sin_a, dir.x * cos_a + dir.z * sin_a );
            dir = vec3( dir.y, dir.z * cos_a - dir.x * sin_a, dir.x * cos_a + dir.z * sin_a );

            vec3 pos = vec3( 0.5, 1.1875, 0.875 ) + vec3( 1.0, 1.0, 1.0 ) * 0.0134 * t;

            float stepshift = MAXSTEPSHIFT;

            if( fract( pow( x, y ) * t * 1000.0 ) > 0.5 )
            {
                pos += dir / pow( 2.0, stepshift );
            }
            else
            {
                pos -= dir / pow( 2.0, stepshift );
            }

            int c;
            int i2;
            stepshift -= 1.0;
            for( int i = 0 ; i < MAXITERS; i++ )
            {
                i2 = i;
                c = func( pos, stepshift );
                if( c > 0 )
                {
                    stepshift += 1.0;
                    if( stepshift >= MAXSTEPSHIFT )
                    {
                       break;
                    }
                    pos -= dir / pow( 2.0, stepshift );
                }
                else
                {
                    if( stepshift > 0.0 )
                    {
                        stepshift -= 1.0;
                    }

                    pos += dir / pow( 2.0, stepshift );
                }
           }

           vec3 col;

           if( c == 0 )
           {
                col = vec3( 0.0, 0.0, 0.0 );
           }
           else if( c == 1 )
           {
                col = vec3( 1.0, 0.5, 0.0 );
           }
           else if( c == 2 )
           {
                col = vec3( 0.0, 1.0, 0.0 );
           }
           else if( c == 3 )
           {
                col = vec3( 1.0, 1.0, 1.0 );
           }
           else if( c == 4 )
           {
                col = vec3( 0.5, 0.5, 0.5 );
           }

           gl_FragColor = vec4( col * ( 1.0 - ( float( i2 ) - stepshift ) / 32.0 ), 1.0 );
        }"""

        # Draw-to-screen shaders.
        rawDrawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 gVSTexCoord0;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            gVSTexCoord0 = gTexCoord0;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0 );
        }"""

        # Format selected shader strings.
        pulsVertexShaderSource    = formatShader( rawPulsVertexShaderSource )
        pulsFragmentShaderSource  = formatShader( rawPulsFragmentShaderSource )
        drawVertexShaderSource    = formatShader( rawDrawVertexShaderSource )
        drawFragmentShaderSource  = formatShader( rawDrawFragmentShaderSource )

        # Screen size, fboSize.
        ( screenWidth, screenHeight, ) = target.getScreenSize()

        if self.fboSize:
            fboWidth  = self.fboSize[ 0 ]
            fboHeight = self.fboSize[ 1 ]
        else:
            fboWidth  = screenWidth
            fboHeight = screenHeight

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        mesh = MeshStripPlane( 2,
                               2,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                               None,
                               None,
                               None,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        pulsVertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        pulsProgram = createProgram( modules, indexTracker, pulsVertexShaderSource, pulsFragmentShaderSource, pulsVertexDataSetup )
        useProgram( modules, pulsProgram )
        pulsVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, pulsVertexDataSetup )
        useVertexBufferData( modules, pulsVertexBufferDataSetup, pulsProgram )

        texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]
        plane = MeshStripPlane( 2,
                                2,
                                MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                None,
                                None,
                                texCoordAttribute,
                                MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                None,
                                MESH_CCW_WINDING,
                                False )

        plane.translate( [ -0.5, -0.5, 0 ] )
        plane.scale( [ 2.0, 2.0 ] )

        drawVertexDataSetup = setupVertexData( modules, indexTracker, plane )
        drawProgram = createProgram( modules, indexTracker, drawVertexShaderSource, drawFragmentShaderSource, drawVertexDataSetup )
        useProgram( modules, drawProgram )
        drawVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, drawVertexDataSetup )
        useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )

        useProgram( modules, pulsProgram )
        useVertexBufferData( modules, pulsVertexBufferDataSetup, pulsProgram )

        h = fboHeight / float( fboWidth )

        hLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( hLocationIndex, pulsProgram.programIndex, 'h' )
        OpenGLES2.Uniformf( hLocationIndex, 1, [ h ] )

        tLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( tLocationIndex, pulsProgram.programIndex, 't' )

        tValueIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( tValueIndex, 0 )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 0.0 ] )

        if self.fboSize:
            # Create a FBO and textures (RGBA8) used for drawing puls.

            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_RGBA8888' )

            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.fboSize:
            # Draw TnL mesh to FBO/texture.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
            OpenGLES2.Viewport( 0, 0, fboWidth, fboHeight )

            useProgram( modules, pulsProgram )
            useVertexBufferData( modules, pulsVertexBufferDataSetup, pulsProgram )
            OpenGLES2.Uniformf( hLocationIndex, 1, [ h ] )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES2.UniformfFromRegister( tLocationIndex, tValueIndex )
        drawVertexBufferData( modules, pulsVertexBufferDataSetup )

        if self.fboSize:
            # Draw TnL FBO/texture to the screen/default FBO.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
            OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

            OpenGLES2.Disable( 'GL_DEPTH_TEST' )
            OpenGLES2.Disable( 'GL_BLEND' )
            OpenGLES2.Disable( 'GL_CULL_FACE' )

            useProgram( modules, drawProgram )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            useTexture( modules, fboTextureIndex, 0, drawProgram )

            drawVertexBufferData( modules, drawVertexBufferDataSetup )

        target.swapBuffers( state )
        OpenGLES2.AddRegister( tValueIndex, self.timeInc )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class MandelbrotBenchmark( Benchmark ):
    def __init__( self, fboSize, iterations, timeInc = 1.0 ):
        Benchmark.__init__( self )
        self.fboSize    = fboSize
        self.iterations = iterations
        self.timeInc    = timeInc
        fboString = 'fullscreen'
        if self.fboSize:
            fboString = '%dx%d fbo' % self.fboSize
        self.name = "OPENGLES2 mandelbrot (%s %d per-fragment iterations)" % ( fboString,
                                                                               self.iterations, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # Shader source codes
        rawMandelbrotVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;

        void main()
        {
            gl_Position = vec4( gVertex, 0.0, 1.0 );
        }"""

        rawMandelbrotFragmentShaderSource = """
        precision mediump float;
        uniform vec2 resolution;
        uniform float time;

        void main( void )
        {
            vec2 p = -1.0 + 2.0 * gl_FragCoord.xy / resolution.xy;
            p.x *= ( resolution.x / resolution.y );

            float zoo = 0.62 + 0.38 * sin( 0.1 * time );
            float coa = cos( 0.1 * ( 1.0 - zoo ) * time );
            float sia = sin( 0.1 * ( 1.0 - zoo ) * time );
            zoo = pow( zoo, 8.0 );
            vec2 xy = vec2( p.x * coa - p.y * sia, p.x * sia + p.y * coa );
            vec2 cc = vec2( -0.745, 0.186 ) + xy * zoo;

            vec2 z = vec2( 0.0 );
            vec2 z2 = z*z;
            float m2;
            float co = 0.0;
            for( int i = 0; i < %d; i++ )
            {
                z = cc + vec2( z.x * z.x - z.y * z.y, 2.0 * z.x * z.y );
                m2 = dot( z, z );
                if( m2 > 1024.0 )
                {
                    break;
                }
                co += 1.0;
            }

            co = co + 1.0 - log2( 0.5 * log2( m2 ) );
            co = sqrt( co / 256.0 );
            gl_FragColor = vec4( 0.5 + 0.5 * cos( 6.2831 * co + 0.0 ),
                                 0.5 + 0.5 * cos( 6.2831 * co + 0.4 ),
                                 0.5 + 0.5 * cos( 6.2831 * co + 0.7 ),
                                 1.0 );
        }""" % ( self.iterations, )

        # Draw-to-screen shaders.
        rawDrawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 gVSTexCoord0;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            gVSTexCoord0 = gTexCoord0;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0 );
        }"""

        # Format selected shader strings.
        mandelbrotVertexShaderSource   = formatShader( rawMandelbrotVertexShaderSource )
        mandelbrotFragmentShaderSource = formatShader( rawMandelbrotFragmentShaderSource )
        drawVertexShaderSource         = formatShader( rawDrawVertexShaderSource )
        drawFragmentShaderSource       = formatShader( rawDrawFragmentShaderSource )

        # Screen size, fboSize.
        ( screenWidth, screenHeight, ) = target.getScreenSize()

        if self.fboSize:
            fboWidth  = self.fboSize[ 0 ]
            fboHeight = self.fboSize[ 1 ]
        else:
            fboWidth  = screenWidth
            fboHeight = screenHeight

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        mesh = MeshStripPlane( 2,
                               2,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                               None,
                               None,
                               None,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        mandelbrotVertexDataSetup  = setupVertexData( modules, indexTracker, mesh )
        mandelbrotProgram          = createProgram( modules,
                                                    indexTracker,
                                                    mandelbrotVertexShaderSource,
                                                    mandelbrotFragmentShaderSource,
                                                    mandelbrotVertexDataSetup )
        useProgram( modules, mandelbrotProgram )
        mandelbrotVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, mandelbrotVertexDataSetup )
        useVertexBufferData( modules, mandelbrotVertexBufferDataSetup, mandelbrotProgram )
        validateProgram( modules, mandelbrotProgram )

        if self.fboSize:
            # Create draw-to-screen plane.
            planeTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

            plane = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    planeTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            plane.translate( [ -0.5, -0.5, 0 ] )
            plane.scale( [ 2.0, 2.0  ] )

            # Create draw-to-screen program and vertex buffer objects.
            drawVertexDataSetup       = setupVertexData( modules, indexTracker, plane )
            drawProgram               = createProgram( modules,
                                                       indexTracker,
                                                       drawVertexShaderSource,
                                                       drawFragmentShaderSource,
                                                       drawVertexDataSetup )
            useProgram( modules, drawProgram )
            drawVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, drawVertexDataSetup )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            validateProgram( modules, drawProgram )

            # Activate mandelbrot program.
            useProgram( modules, mandelbrotProgram )

        # Get uniforms.
        resolutionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( resolutionLocationIndex, mandelbrotProgram.programIndex, 'resolution' )
        OpenGLES2.Uniformf( resolutionLocationIndex, 1, [ fboWidth, fboHeight ] )

        timeLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( timeLocationIndex, mandelbrotProgram.programIndex, 'time' )

        timeValueIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( timeValueIndex, 0 )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 0.0 ] )

        if self.fboSize:
            # Create a FBO and textures (RGBA8, DEPTH) used for mandelbrot.
            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_RGBA8888' )

            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            # Store current framebuffer for future reference; to cope with
            # systems using an FBO for default OpenGLES2 surface.
            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.fboSize:
            # Draw mandelbrot to FBO/texture.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
            OpenGLES2.Viewport( 0, 0, fboWidth, fboHeight )

            useProgram( modules, mandelbrotProgram )
            useVertexBufferData( modules, mandelbrotVertexBufferDataSetup, mandelbrotProgram )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES2.UniformfFromRegister( timeLocationIndex, timeValueIndex )
        drawVertexBufferData( modules, mandelbrotVertexBufferDataSetup )

        if self.fboSize:
            # Draw mandelbrot FBO/texture to the screen/default FBO.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
            OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

            useProgram( modules, drawProgram )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            useTexture( modules, fboTextureIndex, 0, drawProgram )

            drawVertexBufferData( modules, drawVertexBufferDataSetup )

        target.swapBuffers( state )
        OpenGLES2.AddRegister( timeValueIndex, self.timeInc )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class EglInfo( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "EGL print EGL information (debug)"
        self.repeats = 1

    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )
        displayIndex = state[ 'displayIndex' ]

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.Info( displayIndex )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ResourceProfile( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "EGL print GPU resource profile (debug)"
        self.repeats = 1

    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )
        displayIndex = state[ 'displayIndex' ]

        Egl.CheckExtension( displayIndex, 'EGL_NOK_resource_profiling' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.ResourceProfile( displayIndex, [ 0 ] )

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class Info( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENGLES2 print OpenGLES2 information (debug)"
        self.repeats = 1

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Info()

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class Clear( Benchmark ):
    def __init__( self, fboSize, frames ):
        Benchmark.__init__( self )
        self.fboSize = fboSize
        self.frames  = frames
        fboString = 'fullscreen'
        if self.fboSize:
            fboString = '%dx%d fbo' % self.fboSize

        self.name   = "OPENGLES2 clear %d frames (%s)" % ( self.frames,
                                                           fboString, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # Draw-to-screen shaders.
        rawDrawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 gVSTexCoord0;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            gVSTexCoord0 = gTexCoord0;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0 );
        }"""

        # Format selected shader strings.
        drawVertexShaderSource   = formatShader( rawDrawVertexShaderSource )
        drawFragmentShaderSource = formatShader( rawDrawFragmentShaderSource )

        # Screen size, fboSize.
        ( screenWidth, screenHeight, ) = target.getScreenSize()

        if self.fboSize:
            fboWidth  = self.fboSize[ 0 ]
            fboHeight = self.fboSize[ 1 ]
        else:
            fboWidth  = screenWidth
            fboHeight = screenHeight

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        if self.fboSize:
            # Create draw-to-screen plane.
            planeTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

            plane = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    planeTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            plane.translate( [ -0.5, -0.5, 0 ] )
            plane.scale( [ 2.0, 2.0  ] )

            # Create draw-to-screen program and vertex buffer objects.
            drawVertexDataSetup = setupVertexData( modules, indexTracker, plane )
            drawProgram         = createProgram( modules,
                                                 indexTracker,
                                                 drawVertexShaderSource,
                                                 drawFragmentShaderSource,
                                                 drawVertexDataSetup )
            useProgram( modules, drawProgram )
            drawVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, drawVertexDataSetup )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            validateProgram( modules, drawProgram )

            # Create a FBO and textures (RGBA8) used for clear.
            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_RGBA8888' )

            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.ClearColor( [ 1.0, 1.0, 1.0, 1.0 ] )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.frames ):

            if self.fboSize:
                # Draw fill to FBO/texture.
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
                OpenGLES2.Viewport( 0, 0, fboWidth, fboHeight )

            if i % 2 == 0:
                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
            else:
                OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

            if self.fboSize:
                # Draw fill FBO/texture to the screen.
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

                useProgram( modules, drawProgram )
                useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
                useTexture( modules, fboTextureIndex, 0, drawProgram )

                drawVertexBufferData( modules, drawVertexBufferDataSetup )

            target.swapBuffers( state )

