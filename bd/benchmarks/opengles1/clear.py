#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ClearBenchmark( Benchmark ):  
    def __init__( self, mask ):
        Benchmark.__init__( self )
        self.mask = mask
        self.name = "OPENGLES1 clear (msk=%s)" % maskToStr( self.mask )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()
            
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupGL1( modules, indexTracker, target )            

        if 'GL_DEPTH_BUFFER_BIT' in self.mask:
            OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( self.mask )

        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class SynchronizedClearBenchmark( Benchmark ):  
    def __init__( self, mask ):
        Benchmark.__init__( self )
        self.mask = mask
        self.name = "OPENGLES1 synchronized clear (msk=%s)" % maskToStr( self.mask )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupGL1( modules, indexTracker, target )            

        if 'GL_DEPTH_BUFFER_BIT' in self.mask:
            OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.CheckError( '' )

        target.swapInterval( state, 1 )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( self.mask )

        target.swapBuffers( state )
