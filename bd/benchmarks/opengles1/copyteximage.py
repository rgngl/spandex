#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
INTERNAL_FORMATS = { 'OPENGLES1_RGB565'   : 'GL_RGB',
                     'OPENGLES1_RGB888'   : 'GL_RGB',
                     'OPENGLES1_RGBA4444' : 'GL_RGBA',
                     'OPENGLES1_RGBA5551' : 'GL_RGBA',
                     'OPENGLES1_RGBA8888' : 'GL_RGBA' }

######################################################################
def createPlane( gridx, gridy, tcx, tcy ):
    vertexAttribute     = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute   = []
    indexAttribute      = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CopyTexImageBenchmark( Benchmark ):  
    def __init__( self, textureWidth, textureHeight, textureType ):
        Benchmark.__init__( self)
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType
        self.name = 'OPENGLES1 copyteximage (%dx%d txtr.tpe=%s)' % ( self.textureWidth,
                                                                     self.textureHeight,
                                                                     textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()
            
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                            [ 0.5, 0.7, 0.1, 1.0 ],
                                            [ 0.7, 0.5, 0.1, 1.0 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )        
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE',
                                'OFF' )

        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        target.swapBuffers( state )
        OpenGLES1.Finish()
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.CopyTexImage2D( 'GL_TEXTURE_2D',
                                  0,
                                  INTERNAL_FORMATS[ self.textureType ],
                                  0, 0,
                                  self.textureWidth, self.textureHeight )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES1.Finish()

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CopyAndUseBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureWidth, textureHeight, textureType, copyIndex, useIndex, frameCount ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight 
        self.textureType   = textureType
        self.copyIndex     = copyIndex
        self.useIndex      = useIndex
        self.frameCount    = frameCount
        if ( ( self.useIndex > 0 ) and ( self.copyIndex > self.useIndex ) ) or ( self.useIndex > self.frameCount ):
            raise 'Index error'
        
        self.name = 'OPENGLES1 copyteximage copy.n.use cpy=%d use=%d frm.cnt=%d (grd=%dx%d ovrdrw=%d txtr.sz=%dx%d txtr.tpe=%s)' % ( self.copyIndex,
                                                                                                                                     self.useIndex,
                                                                                                                                     self.frameCount,
                                                                                                                                     self.gridx,
                                                                                                                                     self.gridy,
                                                                                                                                     self.overdraw,
                                                                                                                                     self.textureWidth,
                                                                                                                                     self.textureHeight,
                                                                                                                                     textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                self.textureWidth,
                                                self.textureHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                            [ 0.5, 0.7, 0.1, 1.0 ],
                                            [ 0.7, 0.5, 0.1, 1.0 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )        
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE',
                                'OFF' )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )

        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
        
        for j in range( self.frameCount ):
            OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
            
            if ( j + 1 ) == self.copyIndex:
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.CopyTexImage2D( 'GL_TEXTURE_2D',
                                          0,
                                          INTERNAL_FORMATS[ self.textureType ],
                                          0, 0,
                                          self.textureWidth, self.textureHeight )
                
                if ( j + 1 ) != self.useIndex:
                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
                    
            if ( self.copyIndex != self.useIndex ) and ( ( j + 1 ) == self.useIndex ):
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )

            for i in range( self.overdraw ):
                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                drawMeshBufferData( modules, meshBufferDataSetup )
                offset -= delta

            target.swapBuffers( state )
