#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
def createPlane( gridx, gridy, tcx, tcy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_REPEAT',
                            'GL_REPEAT',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class TwiddleBenchmark( Benchmark ):
    def __init__( self, textureWidth, textureHeight, textureType ):
        Benchmark.__init__( self )
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType      
        self.name = 'OPENGLES1 texture upload and twiddle (txtr.sz=%dx%d txtr.tpe=%s)' % ( self.textureWidth,
                                                                                           self.textureHeight,
                                                                                           textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                            [ 0.5, 0.7, 0.1, 1.0 ],
                                            [ 0.7, 0.5, 0.1, 1.0 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE',
                                'OFF' )

        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        target.swapBuffers( state )
        OpenGLES1.Finish()
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES1.Finish()
        

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class UploadCompressedBenchmark( Benchmark ):
    def __init__( self, textureSize, compressedTextureType ):
        Benchmark.__init__( self )
        self.textureSize           = textureSize
        self.compressedTextureType = compressedTextureType      
        self.name = 'OPENGLES1 compressed texture upload (txtr.sz=%dx%d txtr.tpe=%s)' % ( self.textureSize,
                                                                                          self.textureSize,
                                                                                          self.compressedTextureType[ 7 : ], )
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Images    = modules[ 'Images' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        
        if self.compressedTextureType.startswith( 'IMAGES_ETC' ):
            OpenGLES1.CheckExtension( 'GL_OES_compressed_ETC1_RGB8_texture' )            
        elif self.compressedTextureType.startswith( 'IMAGES_PVRTC' ):
            OpenGLES1.CheckExtension( 'GL_IMG_texture_compression_pvrtc' )            
        else:
            raise 'Unexpected compressed texture type'

        Images.CreateData( imagesDataIndex,
                           'IMAGES_FLOWER',
                           self.compressedTextureType,
                           self.textureSize, self.textureSize )
        
        dataIndex = indexTracker.allocIndex( 'OPENGLES1_DATA_INDEX' )
        Images.CopyToOpenGLES1( imagesDataIndex,
                                dataIndex )

        etcTypeMap = { 'IMAGES_ETC1_RGB8'   : 'GL_ETC1_RGB8_OES',
                       'IMAGES_PVRTC_RGB4'  : 'GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG',
                       'IMAGES_PVRTC_RGB2'  : 'GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG',
                       'IMAGES_PVRTC_RGBA4' : 'GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG',
                       'IMAGES_PVRTC_RGBA2' : 'GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG'
                       }
        
        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES1_COMPRESSED_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateCompressedTextureData( compressedTextureDataIndex,
                                               dataIndex,
                                               self.textureSize, self.textureSize,
                                               etcTypeMap[ self.compressedTextureType ]  )

        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE',
                                'OFF' )

        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        target.swapBuffers( state )
        OpenGLES1.Finish()
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES1.Finish()
        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class UploadAndUseBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureWidth, textureHeight, textureType, uploadIndex, useIndex, frameCount ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType
        self.uploadIndex   = uploadIndex
        self.useIndex      = useIndex
        self.frameCount    = frameCount

        if ( ( self.useIndex > 0 ) and ( self.uploadIndex > self.useIndex ) ) or ( self.useIndex > self.frameCount ):
            raise 'Index error'
        
        self.name = 'OPENGLES1 texture upload and use upld=%d use=%d frm.cnt=%d (grd=%dx%d ovrdrw=%d txtr.sz=%dx%d txtr.tpe=%s)' % ( self.uploadIndex,
                                                                                                                                     self.useIndex,
                                                                                                                                     self.frameCount,
                                                                                                                                     self.gridx,
                                                                                                                                     self.gridy,
                                                                                                                                     self.overdraw,
                                                                                                                                     self.textureWidth,
                                                                                                                                     self.textureHeight,
                                                                                                                                     textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        tcx = screenWidth / float( self.textureWidth )
        tcy = screenHeight / float( self.textureHeight )

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                self.textureWidth,
                                                self.textureHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        # Create another texture
        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                            [ 0.5, 0.7, 0.1, 1.0 ],
                                            [ 0.7, 0.5, 0.1, 1.0 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_REPEAT',
                                'GL_REPEAT',
                                'OFF' )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
        
        for j in range( self.frameCount ):
            OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
            
            if ( j + 1 ) == self.uploadIndex:
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                
                if ( j + 1 ) != self.useIndex:
                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
                    
            if ( self.uploadIndex != self.useIndex ) and ( ( j + 1 ) == self.useIndex ):
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )

            for i in range( self.overdraw ):
                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                drawMeshBufferData( modules, meshBufferDataSetup )
                offset -= delta

            target.swapBuffers( state )

