#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *
  
######################################################################
def createPlane( gridx, gridy, tcx, tcy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ScissorBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, scissorRect ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.scissorRect = scissorRect
        
        self.name = 'OPENGLES1 scissor rect=%s (grd=%dx%d ovrdrw=%d txtr.tpe=%s)' % ( arrayToStr( self.scissorRect ),
                                                                                      self.gridx,
                                                                                      self.gridy,
                                                                                      self.overdraw,
                                                                                      textureTypeToStr( self.textureType ), )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.5, 0.5, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        if self.scissorRect:
            OpenGLES1.Scissor( int( self.scissorRect[ 0 ] ),
                               int( self.scissorRect[ 1 ] ),
                               int( self.scissorRect[ 2 ] ),
                               int( self.scissorRect[ 3 ] ) )

        OpenGLES1.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Disable( 'GL_SCISSOR_TEST' )        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        if self.scissorRect:
            OpenGLES1.Enable( 'GL_SCISSOR_TEST' )
        
        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ScissorSwitchBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, textureType, overdraw1, scissorRect1, overdraw2, scissorRect2 ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.textureType  = textureType
        self.overdraw1    = overdraw1
        self.scissorRect1 = scissorRect1
        self.overdraw2    = overdraw2
        self.scissorRect2 = scissorRect2   
        self.name = 'OPENGLES1 scissor switch ovrdrw1=%d rct1=%s ovrdrw2=%d rct2=%s (grd=%dx%d txtr.tpe=%s)' % ( self.overdraw1,
                                                                                                                 arrayToStr( self.scissorRect1 ),
                                                                                                                 self.overdraw2,
                                                                                                                 arrayToStr( self.scissorRect2 ),
                                                                                                                 self.gridx,
                                                                                                                 self.gridy,
                                                                                                                 textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.5, 0.5, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                            [ 0.5, 0.7, 0.1, 0.5 ],
                                            [ 0.7, 0.5, 0.1, 0.5 ],
                                            16, 16,
                                            textureW, textureH,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE',
                                'OFF' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Disable( 'GL_SCISSOR_TEST' )        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        overdraw = self.overdraw1 + self.overdraw2
        delta    = 1.0 / float( overdraw )
        offset   = 1.0

        if self.scissorRect1:
            OpenGLES1.Enable( 'GL_SCISSOR_TEST' )
            OpenGLES1.Scissor( int( self.scissorRect1[ 0 ] ),
                               int( self.scissorRect1[ 1 ] ),
                               int( self.scissorRect1[ 2 ] ),
                               int( self.scissorRect1[ 3 ] ) )
        else:
            OpenGLES1.Disable( 'GL_SCISSOR_TEST' )

        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
            
        for i in range( self.overdraw1 ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        if self.scissorRect2:
            OpenGLES1.Enable( 'GL_SCISSOR_TEST' )
            OpenGLES1.Scissor( int( self.scissorRect2[ 0 ] ),
                               int( self.scissorRect2[ 1 ] ),
                               int( self.scissorRect2[ 2 ] ),
                               int( self.scissorRect2[ 3 ] ) )
        else:
            OpenGLES1.Disable( 'GL_SCISSOR_TEST' )

        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
            
        for i in range( self.overdraw2 ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta
        
        target.swapBuffers( state )
            
