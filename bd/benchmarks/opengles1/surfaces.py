#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from lib.egl       import *
from common        import *

######################################################################
READPIXELS  = 'rdpxls'
COPYBUFFERS = 'cpbffrs'
FINISH      = 'fnsh'

######################################################################
def createPlane( gridx, gridy, tcx, tcy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class WindowSurfaceBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, buffer ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.buffer      = buffer
        self.name = 'OPENGLES1 window surface bffr=%s (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( bufferToStr( self.buffer ),
                                                                                            self.gridx,
                                                                                            self.gridy,
                                                                                            self.overdraw,
                                                                                            textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()        
        textureW, tcx = toPot( width, True )
        textureH, tcy = toPot( height, True )       
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        gles1ConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
            
        attrs = target.getDefaultAttributes( API_OPENGLES1 )
        
        Egl.RGBConfig( displayIndex,
                       gles1ConfigIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT'])

        gles1SurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 gles1SurfaceIndex,
                                 gles1ConfigIndex,
                                 windowIndex,
                                 self.buffer,
                                 'EGL_NONE',
                                 'EGL_NONE' )
       
        gles1ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           gles1ContextIndex,
                           gles1ConfigIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         gles1SurfaceIndex,
                         gles1SurfaceIndex,
                         gles1ContextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )

        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        if self.buffer == 'EGL_SINGLE_BUFFER':
            OpenGLES1.Finish()
        else:
            Egl.SwapBuffers( displayIndex, gles1SurfaceIndex )

            
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PbufferSurfaceBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, format, flush ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.format      = format
        self.flush       = flush
        self.name = 'OPENGLES1 pbuffer surface frmt=%s flsh=%s (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( pixmapFormatToStr( self.format ),
                                                                                                     self.flush,
                                                                                                     self.gridx,
                                                                                                     self.gridy,
                                                                                                     self.overdraw,
                                                                                                     textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]       

        indexTracker = IndexTracker()
      
        ( width, height, ) = target.getScreenSize()
        textureW, tcx = toPot( width, True )
        textureH, tcy = toPot( height, True )       
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        state   = setupGL1( modules, indexTracker, target, attribs )
        
        displayIndex = state[ 'displayIndex' ]

        a = mapColorFormatToAttributes( self.format )
        
        gles1ConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       gles1ConfigIndex,
                       a[ CONFIG_BUFFERSIZE ],
                       a[ CONFIG_REDSIZE ],
                       a[ CONFIG_GREENSIZE ],
                       a[ CONFIG_BLUESIZE ],
                       a[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        gles1SurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  gles1SurfaceIndex,
                                  gles1ConfigIndex,
                                  width,
                                  height,
                                  'EGL_NO_TEXTURE',
                                  'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',                                  
                                  'EGL_NONE',
                                  'EGL_NONE' )
       
        gles1ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           gles1ContextIndex,
                           gles1ConfigIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         gles1SurfaceIndex,
                         gles1SurfaceIndex,
                         gles1ContextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()

        if self.flush == READPIXELS:
            dataIndex = indexTracker.allocIndex( 'OPENGLES1_DATA_INDEX' )
            OpenGLES1.CreateData( dataIndex, width * height * 4  ) # RGBA8888
        if self.flush == COPYBUFFERS:
            pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
            Egl.CreatePixmap( pixmapIndex,
                              width,
                              height,
                              self.format,
                              -1 )

        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        if self.flush == READPIXELS:
            OpenGLES1.ReadPixels( dataIndex,
                               0, 0,
                               width, height )
            
        if self.flush == COPYBUFFERS:
            Egl.CopyBuffers( displayIndex,
                             gles1SurfaceIndex,
                             pixmapIndex )
            
        if self.flush == FINISH:
            OpenGLES1.Finish()

            
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PixmapSurfaceBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, format ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.format      = format
        self.name = 'OPENGLES1 pixmap surface frmt=%s (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( pixmapFormatToStr( self.format ),
                                                                                            self.gridx,
                                                                                            self.gridy,
                                                                                            self.overdraw,
                                                                                            textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()        
        textureW, tcx = toPot( width, True )
        textureH, tcy = toPot( height, True )       

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        state   = setupGL1( modules, indexTracker, target, attribs )
        
        displayIndex = state[ 'displayIndex' ]
        
        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex, width, height, self.format, -1  )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENGL_ES_BIT' ],
                       EGL_DEFINES[ 'EGL_DEPTH_SIZE' ],        16 ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex,
                             configIndex,
                             pixmapIndex,
                             attributes )
        
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 pixmapIndex,
                                 'EGL_NONE',
                                 'EGL_NONE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureH, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        OpenGLES1.Finish()
