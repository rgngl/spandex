#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
def createPlane( gridx, gridy, widthScale, heightScale ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    colorAttribute    = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )    
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

    mesh = MeshTrianglePlane( gridx,
                              gridy,
                              vertexAttribute,
                              colorAttribute,
                              None,
                              None,
                              indexAttribute,
                              None,
                              MESH_CCW_WINDING,
                              False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ widthScale * 2.0, heightScale * 2.0 ] )

    return mesh
      

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class VboPointsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, pointSize, overdraw ):
        Benchmark.__init__( self )
        self.gridx      = gridx
        self.gridy      = gridy
        self.pointSize  = pointSize
        self.overdraw   = overdraw
        self.pointCount = self.gridx * self.gridy                
        self.name = 'OPENGLES1 vbo points sz=%d cnt=%d ovrdrw=%d' % ( self.pointSize,
                                                                      self.pointCount,
                                                                      self.overdraw )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, 0.8, 0.8 )

        mesh.indices = []
        for i in range( self.pointCount ):
            mesh.indices.append( i )
        
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        OpenGLES1.PointSize( self.pointSize )
        OpenGLES1.BindBuffer( 'GL_ELEMENT_ARRAY_BUFFER', meshBufferDataSetup.indexBufferIndex )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        for i in range( self.overdraw ):
            OpenGLES1.BufferDrawElements( meshBufferDataSetup.indexBufferIndex,
                                          'OFF',
                                          'GL_POINTS',
                                          meshBufferDataSetup.indexArrayLength,
                                          0 )    

        target.swapBuffers( state )
        

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PointsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, pointSize, overdraw ):
        Benchmark.__init__( self )
        self.gridx      = gridx
        self.gridy      = gridy
        self.pointSize  = pointSize
        self.overdraw   = overdraw
        self.pointCount = self.gridx * self.gridy        
        self.name = 'OPENGLES1 points sz=%d cnt=%d ovrdrw=%d' % ( self.pointSize,
                                                                  self.pointCount,
                                                                  self.overdraw, )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, 0.8, 0.8 )

        mesh.indices = []
        for i in range( self.pointCount ):
            mesh.indices.append( i )
        
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        useVertexData( modules, indexTracker, vertexDataSetup )

        OpenGLES1.PointSize( self.pointSize )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        for i in range( self.overdraw ):
            OpenGLES1.DrawElements( 'GL_POINTS',
                                    vertexDataSetup.indexArrayLength,
                                    0,
                                    vertexDataSetup.indexArrayIndex )    

        target.swapBuffers( state )
        

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class VboLinesBenchmark( Benchmark ):
    def __init__( self, lineCount, lineWidth, overdraw, widthScale, lineSmooth, multisample ):
        Benchmark.__init__( self )
        self.lineCount   = lineCount
        self.lineWidth   = lineWidth
        self.overdraw    = overdraw
        self.widthScale  = widthScale
        self.lineSmooth  = lineSmooth
        self.multisample = multisample
        self.name = 'OPENGLES1 vbo lines wdth=%d cnt=%d ovrdrw=%d wdthscle=%.2f lnsmth=%s mltsmpl=%s' % ( self.lineWidth,
                                                                                                          self.lineCount,
                                                                                                          self.overdraw,
                                                                                                          self.widthScale,
                                                                                                          boolToStr( self.lineSmooth ),
                                                                                                          boolToStr( self.multisample ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( 2, self.lineCount, self.widthScale, 0.9 )

        mesh.indices = []
        for i in range( self.lineCount ):
            mesh.indices.append( i * 2 )
            mesh.indices.append( i * 2 + 1 )
        
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        OpenGLES1.LineWidth( self.lineWidth );
        if self.lineSmooth:
            OpenGLES1.Enable( 'GL_LINE_SMOOTH' )
        if self.multisample:
            OpenGLES1.Enable( 'GL_MULTISAMPLE' )

        OpenGLES1.BindBuffer( 'GL_ELEMENT_ARRAY_BUFFER', meshBufferDataSetup.indexBufferIndex )            
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        for i in range( self.overdraw ):
            OpenGLES1.BufferDrawElements( meshBufferDataSetup.indexBufferIndex,
                                          'OFF',
                                          'GL_LINES',
                                          meshBufferDataSetup.indexArrayLength,
                                          0 )    
            
        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class LinesBenchmark( Benchmark ):
    def __init__( self, lineCount, lineWidth, overdraw, widthScale, lineSmooth, multisample ):
        Benchmark.__init__( self )
        self.lineCount   = lineCount
        self.lineWidth   = lineWidth
        self.overdraw    = overdraw
        self.widthScale  = widthScale
        self.lineSmooth  = lineSmooth
        self.multisample = multisample
        self.name = 'OPENGLES1 lines wdth=%d cnt=%d ovrdrw=%d wdthscle=%.2f lnsmth=%s mltsmpl=%s' % ( self.lineWidth,
                                                                                                      self.lineCount,
                                                                                                      self.overdraw,
                                                                                                      self.widthScale,
                                                                                                      boolToStr( self.lineSmooth ),
                                                                                                      boolToStr( self.multisample ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )
        mesh                    = createPlane( 2, self.lineCount, self.widthScale, 0.9 )

        mesh.indices = []
        for i in range( self.lineCount ):
            mesh.indices.append( i * 2 )
            mesh.indices.append( i * 2 + 1 )
        
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        useVertexData( modules, indexTracker, vertexDataSetup )

        OpenGLES1.LineWidth( self.lineWidth );
        if self.lineSmooth:
            OpenGLES1.Enable( 'GL_LINE_SMOOTH' )
        if self.multisample:
            OpenGLES1.Enable( 'GL_MULTISAMPLE' )       
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        for i in range( self.overdraw ):
            OpenGLES1.DrawElements( 'GL_LINES',
                                    vertexDataSetup.indexArrayLength,
                                    0,
                                    vertexDataSetup.indexArrayIndex )    
            
        target.swapBuffers( state )
        
