#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
WINDOW  = 'wndw'
PBUFFER = 'pbffr'

######################################################################
def createPlane( gridx, gridy, tcx, tcy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ReadPixelsBenchmark( Benchmark ):  
    def __init__( self, width, height ):
        Benchmark.__init__( self)
        self.width  = width
        self.height = height
        self.name = 'OPENGLES1 readpixels (sz=%dx%d)' % ( self.width,
                                                          self.height, )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()
           
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()
        
        state                   = setupGL1( modules, indexTracker, target )            
        dataIndex               = indexTracker.allocIndex( 'OPENGLES1_DATA_INDEX' )
        OpenGLES1.CreateData( dataIndex, self.width * self.height * 4 ) # RGBA = 4

        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        target.swapBuffers( state )
        OpenGLES1.Finish()
        OpenGLES1.CheckError( '' )        
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.ReadPixels( dataIndex, 0, 0, self.width, self.height )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CopyBuffersWindowBenchmark( Benchmark ):  
    def __init__( self, width, height ):
        Benchmark.__init__( self)
        self.width  = width
        self.height = height

        self.name = 'OPENGLES1 copybuffers window (sz=%dx%d)' % ( self.width,
                                                                  self.height, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()
           
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          0,
                          0,
                          self.width,
                          self.height,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        surfaceBits = [ 'EGL_WINDOW_BIT' ]
        attrs = target.getDefaultAttributes( API_OPENGLES1 )
            
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       surfaceBits,
                       [ 'EGL_OPENGL_ES_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
            
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )
        
        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex,
                          self.width,
                          self.height,
                          mapAttributesToColorFormat( attrs ),
                          -1 )
        
        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        Egl.SwapBuffers( displayIndex, surfaceIndex )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        OpenGLES1.Finish()
        OpenGLES1.CheckError( '' )        
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CopyBuffers( displayIndex, surfaceIndex, pixmapIndex )
       

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CopyBuffersPbufferBenchmark( Benchmark ):  
    def __init__( self, width, height, format = None ):
        Benchmark.__init__( self)
        self.width  = width
        self.height = height
        self.format = format

        self.name = 'OPENGLES1 copybuffers pbuffer (frmt=%s sz=%dx%d)' % ( pixmapFormatToStr( self.format ),
                                                                           self.width,
                                                                           self.height, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()
           
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        state   = setupGL1( modules, indexTracker, target, attribs )
        
        displayIndex = state[ 'displayIndex' ]
                        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        
        surfaceBits = [ 'EGL_PBUFFER_BIT' ]
        attrs = mapColorFormatToAttributes( self.format )
            
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       surfaceBits,
                       [ 'EGL_OPENGL_ES_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )

        Egl.CreatePbufferSurface( displayIndex,
                                  surfaceIndex,
                                  configIndex,
                                  self.width,
                                  self.height,
                                  'EGL_NO_TEXTURE',
                                  'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )        
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )
        
        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex,
                          self.width,
                          self.height,
                          mapAttributesToColorFormat( attrs ),
                          -1 )
        
        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        OpenGLES1.Finish()
        OpenGLES1.CheckError( '' )        
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CopyBuffers( displayIndex, surfaceIndex, pixmapIndex )
       

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DrawAndReadPixelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, width, height, readIndex ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.width       = width
        self.height      = height
        self.readIndex   = readIndex

        if self.readIndex >= 0:
            ri = str( self.readIndex )
        else:
            ri = 'none'
            
        self.name = 'OPENGLES1 readpixels and draw sz=%dx%d rdndx=%s (grd=%dx%d ovdrw=%d txtr.tpe=%s )' % ( self.width,
                                                                                                            self.height,
                                                                                                            ri,
                                                                                                            self.gridx,
                                                                                                            self.gridy,
                                                                                                            self.overdraw,
                                                                                                            textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 0.0, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        dataIndex               = indexTracker.allocIndex( 'OPENGLES1_DATA_INDEX' )
        OpenGLES1.CreateData( dataIndex, self.width * self.height * 4 ) # RGBA = 4

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.CheckError( '' )        
      
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            if i == self.readIndex:
                OpenGLES1.ReadPixels( dataIndex,
                                      0, 0,
                                      self.width, self.height )

            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )
