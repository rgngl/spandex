#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
def createPlane( w, h, colorAttributes, texCoordAttributes ):
    vertexA   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    colorA    = None
    texCoordA = []
    indexA    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    if colorAttributes:
        colorA = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

    if texCoordAttributes:
        for i in range( len( texCoordAttributes ) ):
            texCoordA.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
        
    mesh = MeshStripPlane( w,
                           h,
                           vertexA,
                           colorA,
                           None,
                           texCoordA,
                           indexA,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh

######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, cx, cy, textureType, minF, magF ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        cx, cy,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            minF,
                            magF,
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class FlatPixelsBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES1 marketing flat pixels (grd=%dx%d ovdrw=%d)" % ( self.gridx,
                                                                               self.gridy,
                                                                               self.overdraw, )

    def build( self, target, modules ):          
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()
       
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, False, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        OpenGLES1.ShadeModel( 'GL_FLAT' )
        OpenGLES1.Color( [ 0.1, 1.0, 0.1, 1.0 ] )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SmoothPixelsBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES1 marketing smooth pixels (grd=%dx%d ovdrw=%d)" % ( self.gridx,
                                                                                 self.gridy,
                                                                                 self.overdraw, )
        
    def build( self, target, modules ):          
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()
       
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class FlatTexelsBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES1 marketing flat texels (grd=%dx%d ovrdrw=%d txtr.tpe=%s)' % ( self.gridx,
                                                                                            self.gridy,
                                                                                            self.overdraw,
                                                                                            textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, False, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                4, 4,
                                                self.textureType,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )        
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------        
class SmoothTexelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES1 marketing smooth texels (grd=%dx%d ovrdrw=%d txtr.tpe=%s)' % ( self.gridx,
                                                                                              self.gridy,
                                                                                              self.overdraw,
                                                                                              textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):          
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, True, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                4, 4,
                                                self.textureType,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )
        
        useTexture( modules, textureSetup, 0, 'GL_BLEND' )
        
        OpenGLES1.ShadeModel( 'GL_SMOOTH' )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class FlatMultitexelsBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, overdraw, texture1Type, texture2Type ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.texture1Type = texture1Type
        self.texture2Type = texture2Type
        self.name = 'OPENGLES1 marketing flat multitexels (grd=%dx%d ovrdrw=%d txtr1.tpe=%s txtr2.tpe=%s)' % ( self.gridx,
                                                                                                               self.gridy,
                                                                                                               self.overdraw,
                                                                                                               textureTypeToStr( self.texture1Type ),
                                                                                                               textureTypeToStr( self.texture2Type ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()
            
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, False, [ True, True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 1.0, 0.3, 0.3, 0.8 ],
                                                [ 0.3, 1.0, 0.3, 0.8 ],
                                                4, 4,
                                                self.texture1Type,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )
        texture1Setup           = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 0.4, 0.0, 0.0, 0.8 ],
                                                [ 0.6, 0.0, 0.0, 0.8 ],
                                                2, 2,                                                                
                                                self.texture2Type,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )

        useTexture( modules, texture0Setup, 0, 'GL_REPLACE' )
        useTexture( modules, texture1Setup, 1, 'GL_MODULATE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )        
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------               
#----------------------------------------------------------------------
class SmoothMultitexelsBenchmark( Benchmark ):  
    def __init__( self,  gridx, gridy, overdraw, texture1Type, texture2Type ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.texture1Type = texture1Type
        self.texture2Type = texture2Type
        self.name = 'OPENGLES1 marketing smooth multitexels (grd=%dx%d ovrdrw=%d txtr1.tpe=%s txtr2.tpe=%s)' % ( self.gridx,
                                                                                                                 self.gridy,
                                                                                                                 self.overdraw,
                                                                                                                 textureTypeToStr( self.texture1Type ),
                                                                                                                 textureTypeToStr( self.texture2Type ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()
            
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, True, [ True, True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 1.0, 0.3, 0.3, 0.8 ],
                                                [ 0.3, 1.0, 0.3, 0.8 ],
                                                4, 4,
                                                self.texture1Type,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )
        texture1Setup           = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 0.4, 0.0, 0.0, 0.8 ],
                                                [ 0.6, 0.0, 0.0, 0.8 ],
                                                2, 2,
                                                self.texture2Type,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )

        useTexture( modules, texture0Setup, 0, 'GL_BLEND' )
        useTexture( modules, texture1Setup, 1, 'GL_MODULATE' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------        
#----------------------------------------------------------------------
class PixelFillrateBenchmark( Benchmark ):
    def __init__( self, overdraw ):
        Benchmark.__init__( self )
        self.overdraw = overdraw       
        self.name = "OPENGLES1 marketing pixel fillrate (blended ovrdrw=%d)" % ( self.overdraw, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()
               
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( 2, 2, True, [] )

        for i in range( len( mesh.colors ) ):
            if ( i + 1 ) % 4 == 0:
                mesh.colors[ i ] = 127
        
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        OpenGLES1.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES1.Enable( 'GL_BLEND' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 0.0 ] )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        
        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )


######################################################################
def createPlane2( s, t ):
    vertexA   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordA = []
    indexA    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordA.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ s, t ] ) )
        
    mesh = MeshStripPlane( 2,
                           2,
                           vertexA,
                           None,
                           None,
                           texCoordA,
                           indexA,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh


#----------------------------------------------------------------------
#----------------------------------------------------------------------        
#----------------------------------------------------------------------
class TexelFillrateBenchmark( Benchmark ):
    def __init__( self, overdraw, textureType ):
        Benchmark.__init__( self )
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = "OPENGLES1 marketing texel fillrate (blended ovrdrw=%d txtr.tpe=%s)" % ( self.overdraw,
                                                                                             textureTypeToStr( self.textureType ), )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        textureWidth, textureS  = toPot( screenWidth, True )
        textureHeight, textureT = toPot( screenHeight, True )
       
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )
        mesh                    = createPlane2( textureS, textureT )
        
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureWidth, textureHeight,
                                                [ 1.0, 0.3, 0.3, 0.5 ],
                                                [ 0.3, 1.0, 0.3, 0.5 ],
                                                4, 4,
                                                self.textureType,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES1.Enable( 'GL_BLEND' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 0.0 ] )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        
        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------        
#----------------------------------------------------------------------         
class RotatedTexelFillrateBenchmark( Benchmark ):
    def __init__( self, overdraw, textureType ):
        Benchmark.__init__( self )
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = "OPENGLES1 marketing rotated texel fillrate (ovrdrw=%d txtr.tpe=%s)" % ( self.overdraw,
                                                                                             textureTypeToStr( self.textureType ), )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()
               
        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
       
        state = setupGL1( modules, indexTracker, target )            
       
        backgroundWidth  = screenWidth
        backgroundHeight = screenHeight

        backgroundVertexColorAttribute  = None
        backgroundTexCoordAttribute     = []
        
        backgroundTextureWidth  = 32
        backgroundTextureHeight = 32
        
        backgroundTextureS  = 1.0
        backgroundTextureT  = 1.0
        
        ######################################################################
        # Create texture data                                                #
        ######################################################################
        
        backgroundTextureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        backgroundTextureIndex     = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
            
        OpenGLES1.CreateCheckerTextureData( backgroundTextureDataIndex,
                                            [ 1.0, 0.3, 0.3, 0.5 ],
                                            [ 0.3, 1.0, 0.3, 0.5 ],
                                            4, 4,
                                            backgroundTextureWidth,
                                            backgroundTextureHeight,
                                            'OFF',
                                            self.textureType )
            
        OpenGLES1.GenTexture( backgroundTextureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST',
                                'GL_NEAREST',
                                'GL_REPEAT',
                                'GL_REPEAT',
                                'OFF' )

        OpenGLES1.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )
        
        backgroundTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                               2,
                                                               [ backgroundTextureS,
                                                                 backgroundTextureT ] ) ]

        ######################################################################
        # Create geometry                                                    #
        ######################################################################
        
        background = MeshStripPlane( 2,
                                     2,
                                     MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                     backgroundVertexColorAttribute,
                                     None,
                                     backgroundTexCoordAttribute,
                                     MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                     None,
                                     MESH_CCW_WINDING,
                                     False )
       
        background.translate( [ -0.5, -0.5, 0 ] )
        background.scale( [ 2.0 * backgroundWidth / float( screenWidth ),
                            2.0 * backgroundHeight / float( screenHeight ) ] )
                   
        backgroundVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( backgroundVertexArrayIndex,
                               background.vertexGLType )
        OpenGLES1.AppendToArray( backgroundVertexArrayIndex, background.vertices )

        backgroundTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( backgroundTexCoordArrayIndex,
                               background.texCoordsGLTypes[ 0 ] )
        OpenGLES1.AppendToArray( backgroundTexCoordArrayIndex, background.texCoords[ 0 ] )
            
        backgroundIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( backgroundIndexArrayIndex, background.indexGLType );
        OpenGLES1.AppendToArray( backgroundIndexArrayIndex, background.indices )

        #----------------------------------------------------------------------
        OpenGLES1.VertexPointer( backgroundVertexArrayIndex,
                                 2,
                                 0 )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )

        OpenGLES1.TexCoordPointer( backgroundTexCoordArrayIndex,
                                   2,
                                   0 )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )        
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )

        OpenGLES1.MatrixMode( 'GL_TEXTURE' )
        a = 45
        OpenGLES1.LoadMatrix( [   math.cos( a ), math.sin( a ), 0.0, 0.0,
                                 -math.sin( a ), math.cos( a ), 0.0, 0.0,
                                 0.0,            0.0,           1.0, 0.0,
                                 0.0,            0.0,           0.0, 1.0 ] )
        
        OpenGLES1.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES1.Enable( 'GL_BLEND' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )        
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 0.0 ] )
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        
        for i in range( self.overdraw ):
            OpenGLES1.DrawElements( background.glMode,
                                    len( background.indices ),
                                    0,
                                    backgroundIndexArrayIndex )    

        target.swapBuffers( state )


