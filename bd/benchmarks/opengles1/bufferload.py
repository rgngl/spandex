#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class BufferLoadBenchmark( Benchmark ):  
    def __init__( self, size, target, usage ):
        Benchmark.__init__( self )
        self.size = size
        self.target = target
        self.usage = usage
        self.name = 'OPENGLES1 buffer load (sz=%d trgt=%s usge=%s)' % ( self.size,
                                                                        bufferTargetToStr( self.target ),
                                                                        bufferUsageToStr( self.usage ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupGL1( modules, indexTracker, target )            

        dataIndex = indexTracker.allocIndex( 'OPENGLES1_DATA_INDEX' )
        OpenGLES1.CreateData( dataIndex, self.size )
        
        arrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( arrayIndex, 'GL_BYTE' )

        OpenGLES1.AppendDataToArray( arrayIndex, dataIndex )

        bufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
        OpenGLES1.GenBuffer( bufferIndex )
        OpenGLES1.BindBuffer( self.target, bufferIndex )

        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        target.swapBuffers( state )
        OpenGLES1.Finish()
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.BufferData( bufferIndex,
                              'OFF',
                              self.target,
                              self.usage,
                              arrayIndex,
                              0,
                              0 )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES1.Finish()

        
######################################################################
def createPlane( gridx, gridy, tcx, tcy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh

######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class LoadBufferAndDrawBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, load ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.load        = load
        
        self.name = 'OPENGLES1 load buffer and draw ld=%s (grd=%dx%d ovrdrw=%d txtr.tpe=%s)' % ( boolToStr( self.load ),
                                                                                                 self.gridx,
                                                                                                 self.gridy,
                                                                                                 self.overdraw,
                                                                                                 textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.BindBuffer( 'GL_ARRAY_BUFFER', meshBufferDataSetup.meshBufferIndex )
        OpenGLES1.BindBuffer( 'GL_ELEMENT_ARRAY_BUFFER', meshBufferDataSetup.indexBufferIndex )
        OpenGLES1.CheckError( '' )
      
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        if self.load:
            OpenGLES1.MeshBufferData( meshBufferDataSetup.meshBufferIndex,
                                      'OFF',
                                      meshDataSetup.meshIndex,
                                      'GL_STATIC_DRAW' )
            
            OpenGLES1.BufferData( meshBufferDataSetup.indexBufferIndex,
                                  'OFF',
                                  'GL_ELEMENT_ARRAY_BUFFER',
                                  'GL_STATIC_DRAW',
                                  meshDataSetup.indexArrayIndex,
                                  0,
                                  0 )

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )
