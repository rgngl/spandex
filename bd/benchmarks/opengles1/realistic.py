#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    colorAttribute    = None
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    if colorAttributes:
        colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

    if texCoordAttributes:
        for i in range( len( texCoordAttributes ) ):
            texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                             2,
                                                             [ float( tcx ),
                                                               float( tcy ) ] ) )

    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           colorAttribute,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, cx, cy, textureType, minF, magF ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        cx, cy,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            minF,
                            magF,
                            'GL_REPEAT',
                            'GL_REPEAT',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class FlatPixelsBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES1 realistic flat pixels (grd=%dx%d ovdrw=%d)" % ( self.gridx,
                                                                               self.gridy,
                                                                               self.overdraw, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, self.gridx, self.gridy, True, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SmoothPixelsBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES1 realistic smooth pixels (grd=%dx%d ovdrw=%d)" % ( self.gridx,
                                                                                 self.gridy,
                                                                                 self.overdraw, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, self.gridx, self.gridy, True, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )        

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )

        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class FlatTexelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, fsTexture ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.fsTexture   = fsTexture       
        self.name = 'OPENGLES1 realistic flat texels (grd=%dx%d ovrdrw=%d txtr.tpe=%s fll.scrn.txtre=%s)' % ( self.gridx,
                                                                                                              self.gridy,
                                                                                                              self.overdraw,
                                                                                                              textureTypeToStr( self.textureType ),
                                                                                                              boolToStr( self.fsTexture ), )
    

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()     
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        if self.fsTexture:
            textureW, tcx  = toPot( screenWidth, True )
            textureH, tcy  = toPot( screenHeight, True )
        else:
            tcx = self.gridx
            tcy = self.gridy
            textureW = toPot( screenWidth / self.gridx, True )[ 0 ]
            textureH = toPot( screenHeight / self.gridy, True )[ 0 ]

        filtering = 'GL_LINEAR'
            
        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy, False, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                4, 4,                                                               
                                                self.textureType,
                                                filtering,
                                                filtering )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------        
#----------------------------------------------------------------------
class SmoothTexelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES1 realistic smooth texels (grd=%dx%d ovrdrw=%d txtr.tpe=%s)' % ( self.gridx,
                                                                                              self.gridy,
                                                                                              self.overdraw,
                                                                                              textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, self.gridx, self.gridy, True, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureW = toPot( screenWidth / self.gridx, True )[ 0 ]
        textureH = toPot( screenHeight / self.gridy, True )[ 0 ]
        
        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                4, 4,
                                                self.textureType,
                                                'GL_LINEAR',
                                                'GL_LINEAR' )
        
        useTexture( modules, textureSetup, 0, 'GL_BLEND' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )        

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------        
#----------------------------------------------------------------------
class FlatMultitexelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, texture1Type, texture2Type ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.texture1Type = texture1Type
        self.texture2Type = texture2Type
        self.name = 'OPENGLES1 realistic flat multitexels (grd=%dx%d ovrdrw=%d txtr1.tpe=%s txtr2.tpe=%s)' % ( self.gridx,
                                                                                                               self.gridy,
                                                                                                               self.overdraw,
                                                                                                               textureTypeToStr( self.texture1Type ),
                                                                                                               textureTypeToStr( self.texture2Type ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, self.gridx, self.gridy, False, [ True, True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureW = toPot( screenWidth / self.gridx, True )[ 0 ]
        textureH = toPot( screenHeight / self.gridy, True )[ 0 ]
        
        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 0.8 ],
                                                [ 0.3, 1.0, 0.3, 0.8 ],
                                                4, 4,                                                                
                                                self.texture1Type,
                                                'GL_LINEAR',
                                                'GL_LINEAR' )
        texture1Setup           = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 0.4, 0.0, 0.0, 0.8 ],
                                                [ 0.6, 0.0, 0.0, 0.8 ],
                                                2, 2,                                                                
                                                self.texture2Type,
                                                'GL_LINEAR',
                                                'GL_LINEAR' )

        useTexture( modules, texture0Setup, 0, 'GL_REPLACE' )
        useTexture( modules, texture1Setup, 1, 'GL_MODULATE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------        
#----------------------------------------------------------------------
class SmoothMultitexelsBenchmark( Benchmark ):
    def __init__( self,  gridx, gridy, overdraw, texture1Type, texture2Type ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.texture1Type = texture1Type
        self.texture2Type = texture2Type
        self.name = 'OPENGLES1 realistic smooth multitexels (grd=%dx%d ovrdrw=%d txtr1.tpe=%s txtr2.tpe=%s)' % ( self.gridx,
                                                                                                                 self.gridy,
                                                                                                                 self.overdraw,
                                                                                                                 textureTypeToStr( self.texture1Type ),
                                                                                                                 textureTypeToStr( self.texture2Type ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, self.gridx, self.gridy, True, [ True, True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureW = toPot( screenWidth / self.gridx, True )[ 0 ]
        textureH = toPot( screenHeight / self.gridy, True )[ 0 ]
        
        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 0.8 ],
                                                [ 0.3, 1.0, 0.3, 0.8 ],
                                                4, 4,                                                                
                                                self.texture1Type,
                                                'GL_LINEAR',
                                                'GL_LINEAR' )
        texture1Setup           = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 0.4, 0.0, 0.0, 0.8 ],
                                                [ 0.6, 0.0, 0.0, 0.8 ],
                                                2, 2,                                                                
                                                self.texture2Type,
                                                'GL_LINEAR',
                                                'GL_LINEAR' )

        useTexture( modules, texture0Setup, 0, 'GL_BLEND' )
        useTexture( modules, texture1Setup, 1, 'GL_MODULATE' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )

