#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../..' )

import  bd

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'OpenGLES1.1 smoke benchmark'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

#----------------------------------------------------------------------
( width, height, )      = target.getScreenSize()

#----------------------------------------------------------------------
import clear
suite.addBenchmark( clear.ClearBenchmark( [ 'GL_COLOR_BUFFER_BIT' ] ) )
suite.addBenchmark( clear.SynchronizedClearBenchmark( [ 'GL_COLOR_BUFFER_BIT' ] ) )

#----------------------------------------------------------------------
import realistic
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( realistic.FlatPixelsBenchmark( GRIDX, GRIDY, OVERDRAW ) )
suite.addBenchmark( realistic.SmoothPixelsBenchmark( GRIDX, GRIDY, OVERDRAW ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', False ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', True ) )
suite.addBenchmark( realistic.SmoothTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( realistic.FlatMultitexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565','OPENGLES1_LUMINANCE8' ) )
suite.addBenchmark( realistic.SmoothMultitexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565','OPENGLES1_LUMINANCE8' ) )

#----------------------------------------------------------------------
import readpixels
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( readpixels.ReadPixelsBenchmark( width, height ) )

#----------------------------------------------------------------------
import texturemod
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 1
suite.addBenchmark( texturemod.TwiddleBenchmark( 512, 512, 'OPENGLES1_RGB565' ) )

#----------------------------------------------------------------------
import mipmap
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 512, 512, 'OPENGLES1_RGB565', False ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 512, 512, 'OPENGLES1_RGB565', True ) )

#----------------------------------------------------------------------
import surfaces         # REQUIRES EGL
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3

suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGBA8888', surfaces.READPIXELS ) )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGBA8888', surfaces.COPYBUFFERS ) )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGB565', surfaces.READPIXELS ) )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGB565', surfaces.COPYBUFFERS ) )

#----------------------------------------------------------------------
import ui
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 1 ) )

#----------------------------------------------------------------------
command()
