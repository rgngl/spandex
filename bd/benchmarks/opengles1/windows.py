#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
READPIXELS  = 'rdpxls'
COPYBUFFERS = 'cpbffrs'
FINISH      = 'fnsh'

######################################################################
def createPlane( gridx, gridy, tcx, tcy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )

    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ResizeWindowBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, extent1, extent2 ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.extent1     = extent1
        self.extent2     = extent2
        self.name = "OPENGLES1 resize window from=%s to=%s (grd=%dx%d ovdrw=%d txtr.tpe=%s)" % ( arrayToStr( self.extent1 ),
                                                                                                 arrayToStr( self.extent2 ),
                                                                                                 self.gridx,
                                                                                                 self.gridy,
                                                                                                 self.overdraw,
                                                                                                 textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()        
        textureW, tcx = toPot( width, True )
        textureH, tcy = toPot( height, True )       
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        x = 0
        y = 0
        w = width
        h = height
        if self.extent1:
            x = self.extent1[ 0 ]
            y = self.extent1[ 1 ]
            w = self.extent1[ 2 ]
            h = self.extent1[ 3 ]
            
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          x,
                          y,
                          w,
                          h,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES1 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
       
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )
        
        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        x = 0
        y = 0
        w = width
        h = height
        if self.extent1:
            x = self.extent1[ 0 ]
            y = self.extent1[ 1 ]
            w = self.extent1[ 2 ]
            h = self.extent1[ 3 ]

        if self.extent1 and self.extent2:
            Egl.ResizeWindow( windowIndex,
                              x,
                              y,
                              w,
                              h )
            OpenGLES1.Viewport( 0, 0, w, h )
            
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        x = 0
        y = 0
        w = width
        h = height
        if self.extent2:
            x = self.extent2[ 0 ]
            y = self.extent2[ 1 ]
            w = self.extent2[ 2 ]
            h = self.extent2[ 3 ]

        if self.extent1 and self.extent2:            
            Egl.ResizeWindow( windowIndex,
                              x,
                              y,
                              w,
                              h )
            OpenGLES1.Viewport( 0, 0, w, h )
            
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class TopWindowBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, screenGridX, screenGridY, swapInterval ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.screenGridX  = screenGridX
        self.screenGridY  = screenGridY
        self.swapInterval = swapInterval
        self.name = "OPENGLES1 top windows bg=1 scrn.grd=%dx%d swpintrvl=%d (grd=%dx%d ovdrw=%d txtr.tpe=%s)" % ( self.screenGridX,
                                                                                                                  self.screenGridY,
                                                                                                                  self.swapInterval,
                                                                                                                  self.gridx,
                                                                                                                  self.gridy,
                                                                                                                  self.overdraw,
                                                                                                                  textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()        
        textureW, tcx = toPot( width, True )
        textureH, tcy = toPot( height, True )       
        
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        parentWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( parentWindowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES1 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )       
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        parentSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 parentSurfaceIndex,
                                 configIndex,
                                 parentWindowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        childSurfaces = []
        childWidth    = 0
        childHeight   = 0
        childTextureW = 0
        childTextureH = 0
        ctcx          = 0
        ctcy          = 0
        
        if self.screenGridY != 0 and self.screenGridY != 0:
            margin              = 5
            childWidth          = ( width - margin * 2 ) / self.screenGridX
            childHeight         = ( height - margin * 2 ) / self.screenGridY
            childTextureW, ctcx = toPot( childWidth, True )
            childTextureH, ctcy = toPot( childHeight, True )       
            
            for y in range( self.screenGridY ):
                for x in range( self.screenGridX ):
                    childWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
                    Egl.CreateWindow( childWindowIndex,
                                      'SCT_SCREEN_DEFAULT',
                                      x * childWidth + margin,
                                      y * childHeight + margin,
                                      childWidth - 1, childHeight - 1,
                                      'SCT_COLOR_FORMAT_DEFAULT' )
                   
                    childSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                    Egl.CreateWindowSurface( displayIndex,
                                             childSurfaceIndex,
                                             configIndex,
                                             childWindowIndex,
                                             'EGL_BACK_BUFFER',
                                             'EGL_NONE',
                                             'EGL_NONE' )
            
                    childSurfaces.append( childSurfaceIndex )
            
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         parentSurfaceIndex,
                         parentSurfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, self.swapInterval )

        mesh                    = createPlane( self.gridx, self.gridy, 1.0, 1.0 )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        if len( childSurfaces ) > 0:        
            texture1Setup       = setupTexture( modules,
                                                indexTracker,
                                                childTextureW, childTextureH,
                                                [ 0.3, 0.3, 1.0, 1.0 ],
                                                [ 0.3, 1.0, 1.0, 1.0 ],
                                                self.textureType )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )        
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
               
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        ##########
        Egl.MakeCurrent( displayIndex,
                         parentSurfaceIndex,
                         parentSurfaceIndex,
                         contextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )        
        OpenGLES1.Viewport( 0, 0, width, height )
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        useTexture( modules, texture0Setup, 0, 'GL_REPLACE' )
       
        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, parentSurfaceIndex )

        if len( childSurfaces ) > 0:
            useTexture( modules, texture1Setup, 0, 'GL_REPLACE' )
        
        for childSurface in childSurfaces:
            Egl.MakeCurrent( displayIndex,
                             childSurface,
                             childSurface,
                             contextIndex )

            
            Egl.SwapInterval( displayIndex, self.swapInterval )
            OpenGLES1.Viewport( 0, 0, childWidth, childHeight )

            OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

            delta   = 1.0 / float( self.overdraw )
            offset  = 1.0

            for i in range( self.overdraw ):
                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                drawMeshBufferData( modules, meshBufferDataSetup )
                offset -= delta

            Egl.SwapBuffers( displayIndex, childSurface )

            
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ChildWindowBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, screenGridX, screenGridY, swapInterval ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.screenGridX  = screenGridX
        self.screenGridY  = screenGridY
        self.swapInterval = swapInterval
        self.name = "OPENGLES1 child windows bg=1 scrn.grd=%dx%d swpintrvl=%d (grd=%dx%d ovdrw=%d txtr.tpe=%s)" % ( self.screenGridX,
                                                                                                                    self.screenGridY,
                                                                                                                    self.swapInterval,
                                                                                                                    self.gridx,
                                                                                                                    self.gridy,
                                                                                                                    self.overdraw,
                                                                                                                    textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()        
        textureW, tcx = toPot( width, True )
        textureH, tcy = toPot( height, True )       
        
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        parentWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( parentWindowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES1 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        parentSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 parentSurfaceIndex,
                                 configIndex,
                                 parentWindowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        childSurfaces = []
        childWidth    = 0
        childHeight   = 0
        childTextureW = 0
        childTextureH = 0
        ctcx          = 0
        ctcy          = 0
        
        if self.screenGridY != 0 and self.screenGridY != 0:
            margin              = 5
            childWidth          = ( width - margin * 2 ) / self.screenGridX
            childHeight         = ( height - margin * 2 ) / self.screenGridY
            childTextureW, ctcx = toPot( childWidth, True )
            childTextureH, ctcy = toPot( childHeight, True )       
            
            for y in range( self.screenGridY ):
                for x in range( self.screenGridX ):
                    childWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
                    Egl.CreateChildWindow( childWindowIndex,
                                           parentWindowIndex,
                                           x * childWidth + margin,
                                           y * childHeight + margin,
                                           childWidth - 1, childHeight - 1,
                                           'SCT_COLOR_FORMAT_DEFAULT' )
                   
                    childSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                    Egl.CreateWindowSurface( displayIndex,
                                             childSurfaceIndex,
                                             configIndex,
                                             childWindowIndex,
                                             'EGL_BACK_BUFFER',
                                             'EGL_NONE',
                                             'EGL_NONE' )
            
                    childSurfaces.append( childSurfaceIndex )
            
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         parentSurfaceIndex,
                         parentSurfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, self.swapInterval )

        mesh                    = createPlane( self.gridx, self.gridy, 1.0, 1.0 )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
        
        if len( childSurfaces ) > 0:        
            texture1Setup       = setupTexture( modules,
                                                indexTracker,
                                                childTextureW, childTextureH,
                                                [ 0.3, 0.3, 1.0, 1.0 ],
                                                [ 0.3, 1.0, 1.0, 1.0 ],
                                                self.textureType )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
               
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        ##########
        Egl.MakeCurrent( displayIndex, parentSurfaceIndex,
                         parentSurfaceIndex,
                         contextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )        
        OpenGLES1.Viewport( 0, 0, width, height )
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        useTexture( modules, texture0Setup, 0, 'GL_REPLACE' )
       
        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, parentSurfaceIndex )

        if len( childSurfaces ) > 0:
            useTexture( modules, texture1Setup, 0, 'GL_REPLACE' )
        
        for childSurface in childSurfaces:
            Egl.MakeCurrent( displayIndex,childSurface,
                             childSurface,
                             contextIndex )

            Egl.SwapInterval( displayIndex, self.swapInterval )
            OpenGLES1.Viewport( 0, 0, childWidth, childHeight )

            OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

            delta   = 1.0 / float( self.overdraw )
            offset  = 1.0

            for i in range( self.overdraw ):
                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                drawMeshBufferData( modules, meshBufferDataSetup )
                offset -= delta

            Egl.SwapBuffers( displayIndex, childSurface )

            
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------        
# ----------------------------------------------------------------------
class PbuffersBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, screenGridX, screenGridY, colorFormat, flush ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.screenGridX  = screenGridX
        self.screenGridY  = screenGridY
        self.colorFormat  = colorFormat
        self.flush        = flush
        self.name = "OPENGLES1 pbuffers scrn.grd=%dx%d clrfrmt=%s flsh=%s (grd=%dx%d ovdrw=%d txtr.tpe=%s)" % ( self.screenGridX,
                                                                                                                self.screenGridY,
                                                                                                                pixmapFormatToStr( self.colorFormat ),
                                                                                                                self.flush,
                                                                                                                self.gridx,
                                                                                                                self.gridy,
                                                                                                                self.overdraw,
                                                                                                                textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        state   = setupGL1( modules, indexTracker, target, attribs )
        
        displayIndex = state[ 'displayIndex' ]
        
        a = mapColorFormatToAttributes( self.colorFormat )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       a[ CONFIG_BUFFERSIZE ],
                       a[ CONFIG_REDSIZE ],
                       a[ CONFIG_GREENSIZE ],
                       a[ CONFIG_BLUESIZE ],
                       a[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        surfaces = []

        surfaceWidth    = width / self.screenGridX
        surfaceHeight   = height / self.screenGridY
        textureW, tcx   = toPot( surfaceWidth, True )
        textureH, tcy   = toPot( surfaceHeight, True )       
        
        for y in range( self.screenGridY ):
            for x in range( self.screenGridX ):                 
                surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                Egl.CreatePbufferSurface( displayIndex,
                                          surfaceIndex,
                                          configIndex,
                                          surfaceWidth,
                                          surfaceHeight,
                                          'EGL_NO_TEXTURE',
                                          'EGL_NO_TEXTURE',
                                          'OFF',
                                          'OFF',                                          
                                          'EGL_NONE',
                                          'EGL_NONE' )
            
                surfaces.append( surfaceIndex )
            
        gles1ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           gles1ContextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaces[ 0 ],
                         surfaces[ 0 ],
                         gles1ContextIndex )

        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        
        if self.flush == READPIXELS:
            dataIndex           = indexTracker.allocIndex( 'OPENGLES1_DATA_INDEX' )
            OpenGLES1.CreateData( dataIndex, surfaceWidth * surfaceHeight * 4 ) # RGBA8888
        elif self.flush == COPYBUFFERS:
            pixmapIndex         = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
            Egl.CreatePixmap( pixmapIndex, surfaceWidth, surfaceHeight, self.colorFormat, -1 )

        OpenGLES1.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        # -----
        for s in surfaces:
            Egl.MakeCurrent( displayIndex,
                             s,
                             s,
                             gles1ContextIndex )
            OpenGLES1.Viewport( 0, 0, surfaceWidth, surfaceHeight )
            OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

            delta   = 1.0 / float( self.overdraw )
            offset  = 1.0

            for i in range( self.overdraw ):
                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                drawMeshBufferData( modules, meshBufferDataSetup )
                offset -= delta

            if self.flush == READPIXELS:
                OpenGLES1.ReadPixels( dataIndex, 0, 0, surfaceWidth, surfaceHeight )
            elif self.flush == COPYBUFFERS:
                Egl.CopyBuffers( displayIndex, s, pixmapIndex )
            elif self.flush == FINISH:
                OpenGLES1.Finish()
            else:
                raise 'Invalid flush'
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class RotateScreenBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, orientation, swapInterval ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.orientation  = orientation
        self.swapInterval = swapInterval       
        self.name = "OPENGLES1 screen orientation %s swpntrvl=%d (grd=%dx%d ovdrw=%d txtr.tpe=%s)" % ( orientationToStr( self.orientation ),
                                                                                                       self.swapInterval,
                                                                                                       self.gridx,
                                                                                                       self.gridy,
                                                                                                       self.overdraw,
                                                                                                       textureTypeToStr( self.textureType ), )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()        
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        x = 0
        y = 0
        w = width
        h = height

        if defaultOrientation( width, height ) != self.orientation:
            w = height
            h = width
            Egl.ScreenOrientation( 'SCT_SCREEN_DEFAULT', self.orientation )

        textureW, tcx = toPot( w, True )
        textureH, tcy = toPot( h, True )       
                    
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          x,
                          y,
                          w,
                          h,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )

        attrs = target.getDefaultAttributes( API_OPENGLES1 )
        
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
       
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )
        
        Egl.SwapInterval( displayIndex,
                          self.swapInterval )

        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class OpaqueWindowBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, opacity ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.opacity     = opacity
        self.name = "OPENGLES1 window opacity opacity=%.2f (grd=%dx%d ovdrw=%d txtr.tpe=%s)" % ( self.opacity,
                                                                                                 self.gridx,
                                                                                                 self.gridy,
                                                                                                 self.overdraw,
                                                                                                 textureTypeToStr( self.textureType ), )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()        
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        x = 0
        y = 0
        w = width
        h = height

        textureW, tcx = toPot( w, True )
        textureH, tcy = toPot( h, True )       
                   
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          x,
                          y,
                          w,
                          h,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )

        attrs = target.getDefaultAttributes( API_OPENGLES1 )
        
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
       
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )
        
        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 0.3 ],
                                                [ 0.3, 1.0, 0.3, 0.9 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )

        if self.opacity < 1.0:
            Egl.WindowOpacity( windowIndex, self.opacity )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )
