#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

######################################################################
def createPlane( gridx, gridy, tcx, tcy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    normalAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           normalAttribute,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class NoLightsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES1 no lights (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( self.gridx,
                                                                               self.gridy,
                                                                               self.overdraw,
                                                                               textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        if self.textureType:
            textureSetup        = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
            useTexture( modules, textureSetup, 0, 'GL_MODULATE' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )

        NEAR = 10            
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Frustum( -1,
                            1,
                            -1,
                            1,
                            NEAR,
                            100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        depths = []
        scales = []
        zdepth = -1
        for i in range( self.overdraw ):
            depths.append( zdepth )
            scales.append( ( NEAR + abs( zdepth ) ) / float( NEAR ) )
            zdepth -= 1.0

        depths.reverse()
        scales.reverse()

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ -1.0 * scales[ i ], -1.0 * scales[ i ], depths[ i ] - NEAR ] )
            OpenGLES1.Scale( [ 2.0 * scales[ i ], 2.0 * scales[ i ], 1.0 ] )
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DirectionalWhiteLightBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, lightCount, flipNormals ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.lightCount  = lightCount
        self.flipNormals = flipNormals
        self.name = 'OPENGLES1 directional white light lghtcnt=%d (grd=%dx%d ovdrw=%d txtr.tpe=%s flpnrmls=%s)' % ( self.lightCount,
                                                                                                                    self.gridx,
                                                                                                                    self.gridy,
                                                                                                                    self.overdraw,
                                                                                                                    textureTypeToStr( self.textureType ),
                                                                                                                    boolToStr( self.flipNormals ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )

        if self.flipNormals:
            for i in range( len( mesh.normals ) ):
                if ( i + 1 ) % 3 == 0:
                    mesh.normals[ i ] = -mesh.normals[ i ]
        
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        if self.textureType:
            textureSetup        = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 0.0, 1.0 ],
                                                self.textureType )
       
            useTexture( modules, textureSetup, 0, 'GL_MODULATE' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()

        OpenGLES1.Material( [ 1.0, 1.0, 1.0, 1 ],
                            [ 1.0, 1.0, 1.0, 1 ],
                            [ 1.0, 1.0, 1.0, 1 ],
                            [ 0, 0, 0, 0 ],
                            42 )

        for i in range( self.lightCount ):
            OpenGLES1.LightColor( i,
                                  [ 0, 0, 0, 0 ],
                                  [ 1, 1, 1, 1 ],
                                  [ 0, 0, 0, 0 ] )
            OpenGLES1.Enable( 'GL_LIGHT%d' % i )

        if self.lightCount > 0:
            OpenGLES1.Enable( 'GL_LIGHTING' )
            OpenGLES1.LightModelTwoSided( 'OFF' )

        NEAR = 10            
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Frustum( -1,
                            1,
                            -1,
                            1,
                            NEAR,
                            100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        depths = []
        scales = []
        zdepth = -1
        for i in range( self.overdraw ):
            depths.append( zdepth )
            scales.append( ( NEAR + abs( zdepth ) ) / float( NEAR ) )
            zdepth -= 1.0

        depths.reverse()
        scales.reverse()

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            for i in range( self.lightCount ):
                OpenGLES1.LightPosition( i, [ 0, 0, 1, 0 ] )

            OpenGLES1.Translate( [ -1.0 * scales[ i ], -1.0 * scales[ i ], depths[ i ] - NEAR ] )
            OpenGLES1.Scale( [ 2.0 * scales[ i ], 2.0 * scales[ i ], 1.0 ] )
           
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DirectionalLightBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, lightCount ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.lightCount  = lightCount
        self.name = 'OPENGLES1 directional light lghtcnt=%d (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( self.lightCount,
                                                                                                  self.gridx,
                                                                                                  self.gridy,
                                                                                                  self.overdraw,
                                                                                                  textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        if self.textureType:
            textureSetup        = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 0.0, 1.0 ],
                                                self.textureType )
       
            useTexture( modules, textureSetup, 0, 'GL_MODULATE' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()

        OpenGLES1.Material( [ 1.0, 1.0, 1.0, 1 ],
                            [ 1.0, 1.0, 1.0, 1 ],
                            [ 1.0, 1.0, 1.0, 1 ],
                            [ 0, 0, 0, 0 ],
                            42 )

        for i in range( self.lightCount ):
            OpenGLES1.LightColor( i,
                                  [ 0, 0, 0, 0 ],
                                  [ 0.3, 0.7, 0.3, 1 ],
                                  [ 0.3, 0.7, 0.3, 1 ] )
            OpenGLES1.Enable( 'GL_LIGHT%d' % i )

        if self.lightCount > 0:
            OpenGLES1.Enable( 'GL_LIGHTING' )
            OpenGLES1.LightModelTwoSided( 'OFF' )

        NEAR = 10            
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Frustum( -1,
                            1,
                            -1,
                            1,
                            NEAR,
                            100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        depths = []
        scales = []
        zdepth = -1
        for i in range( self.overdraw ):
            depths.append( zdepth )
            scales.append( ( NEAR + abs( zdepth ) ) / float( NEAR ) )
            zdepth -= 1.0

        depths.reverse()
        scales.reverse()

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            for i in range( self.lightCount ):
                OpenGLES1.LightPosition( i, [ 0, 0, 1, 0 ] )

            OpenGLES1.Translate( [ -1.0 * scales[ i ], -1.0 * scales[ i ], depths[ i ] - NEAR ] )
            OpenGLES1.Scale( [ 2.0 * scales[ i ], 2.0 * scales[ i ], 1.0 ] )
           
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PointLightBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, lightCount ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.lightCount  = lightCount
        self.name = 'OPENGLES1 point light lghtcnt=%d (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( self.lightCount,
                                                                                            self.gridx,
                                                                                            self.gridy,
                                                                                            self.overdraw,
                                                                                            textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        if self.textureType:
            textureSetup        = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 0.0, 1.0 ],
                                                self.textureType )
       
            useTexture( modules, textureSetup, 0, 'GL_MODULATE' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()

        OpenGLES1.Material( [ 1.0, 1.0, 1.0, 1 ],
                            [ 1.0, 1.0, 1.0, 1 ],
                            [ 1.0, 1.0, 1.0, 1 ],
                            [ 0, 0, 0, 0 ],
                            42 )

        for i in range( self.lightCount ):
            OpenGLES1.LightColor( i,
                                  [ 0, 0, 0, 0 ],
                                  [ 0.3, 0.7, 0.3, 1 ],
                                  [ 0.3, 0.7, 0.3, 1 ] )
            OpenGLES1.LightAttenuation( i, 0.1, 0.1, 0.1 )
            OpenGLES1.Enable( 'GL_LIGHT%d' % i )

        if self.lightCount > 0:
            OpenGLES1.Enable( 'GL_LIGHTING' )
            OpenGLES1.LightModelTwoSided( 'OFF' )

        NEAR = 10            
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Frustum( -1,
                            1,
                            -1,
                            1,
                            NEAR,
                            100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        depths = []
        scales = []
        zdepth = -1
        for i in range( self.overdraw ):
            depths.append( zdepth )
            scales.append( ( NEAR + abs( zdepth ) ) / float( NEAR ) )
            zdepth -= 1.0

        depths.reverse()
        scales.reverse()

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            for i in range( self.lightCount ):
                OpenGLES1.LightPosition( i, [ 0, 0, -NEAR, 1 ] )
                OpenGLES1.LightSpotDirection( i, [ 0, 0, -1 ] )

            OpenGLES1.Translate( [ -1.0 * scales[ i ], -1.0 * scales[ i ], depths[ i ] - NEAR ] )
            OpenGLES1.Scale( [ 2.0 * scales[ i ], 2.0 * scales[ i ], 1.0 ] )
           
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SpotLightBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, lightCount ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.lightCount  = lightCount
        self.name = 'OPENGLES1 spot light lghtcnt=%d (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( self.lightCount,
                                                                                           self.gridx,
                                                                                           self.gridy,
                                                                                           self.overdraw,
                                                                                           textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        if self.textureType:
            textureSetup        = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 0.0, 1.0 ],
                                                self.textureType )
       
            useTexture( modules, textureSetup, 0, 'GL_MODULATE' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()

        OpenGLES1.Material( [ 1.0, 1.0, 1.0, 1 ],
                            [ 1.0, 1.0, 1.0, 1 ],
                            [ 1.0, 1.0, 1.0, 1 ],
                            [ 0, 0, 0, 0 ],
                            42 )

        for i in range( self.lightCount ):
            OpenGLES1.LightColor( i,
                                  [ 0, 0, 0, 0 ],
                                  [ 0.3, 0.7, 0.3, 1 ],
                                  [ 0.3, 0.7, 0.3, 1 ] )
            OpenGLES1.LightAttenuation( i, 0.1, 0.1, 0.1 )            
            OpenGLES1.Enable( 'GL_LIGHT%d' % i )

        if self.lightCount > 0:
            OpenGLES1.Enable( 'GL_LIGHTING' )
            OpenGLES1.LightModelTwoSided( 'OFF' )

        NEAR = 10            
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Frustum( -1,
                            1,
                            -1,
                            1,
                            NEAR,
                            100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        depths = []
        scales = []
        zdepth = -1
        for i in range( self.overdraw ):
            depths.append( zdepth )
            scales.append( ( NEAR + abs( zdepth ) ) / float( NEAR ) )
            zdepth -= 1.0

        depths.reverse()
        scales.reverse()

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            for i in range( self.lightCount ):
                OpenGLES1.LightPosition( i, [ 0, 0, -NEAR, 1 ] )
                OpenGLES1.LightSpot( i,
                                     [ 0, 0, -1 ],
                                     1,
                                     45 )

            OpenGLES1.Translate( [ -1.0 * scales[ i ], -1.0 * scales[ i ], depths[ i ] - NEAR ] )
            OpenGLES1.Scale( [ 2.0 * scales[ i ], 2.0 * scales[ i ], 1.0 ] )
           
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class AmbientLightBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES1 ambient light (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( self.gridx,
                                                                                   self.gridy,
                                                                                   self.overdraw,
                                                                                   textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        if self.textureType:
            textureSetup        = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 0.0, 1.0 ],
                                                self.textureType )
       
            useTexture( modules, textureSetup, 0, 'GL_MODULATE' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )

        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()

        OpenGLES1.Material( [ 1.0, 1.0, 1.0, 1 ],
                            [ 1.0, 1.0, 1.0, 1 ],
                            [ 1.0, 1.0, 1.0, 1 ],
                            [ 0, 0, 0, 0 ],
                            42 )

        OpenGLES1.Disable( 'GL_LIGHT0')
        OpenGLES1.Enable( 'GL_LIGHTING' )
        OpenGLES1.LightModelTwoSided( 'OFF' )
        OpenGLES1.LightModelAmbient( [ 0.3, 0.8, 0.8, 1.0 ] )

        NEAR = 10            
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Frustum( -1,
                            1,
                            -1,
                            1,
                            NEAR,
                            100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        depths = []
        scales = []
        zdepth = -1
        for i in range( self.overdraw ):
            depths.append( zdepth )
            scales.append( ( NEAR + abs( zdepth ) ) / float( NEAR ) )
            zdepth -= 1.0

        depths.reverse()
        scales.reverse()

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ -1.0 * scales[ i ], -1.0 * scales[ i ], depths[ i ] - NEAR ] )
            OpenGLES1.Scale( [ 2.0 * scales[ i ], 2.0 * scales[ i ], 1.0 ] )
           
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )
        
