#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
BACKGROUND      = 0
PANELS          = 1
GROUPS          = 2
ICONS           = 3
 
UPLOAD          = 4
DRAW            = 5
UPLOAD_AND_DRAW = 6

PANELSX         = 2
PANELSY         = 1
GROUPSX         = 2
GROUPSY         = 3
ICONSX          = 4
ICONSY          = 6

######################################################################
def actionToString( action ):
    if action == UPLOAD:
        return 'UPLD'
    if action == DRAW:
        return 'DRW'
    if action == UPLOAD_AND_DRAW:
        return 'UPLD.N.DRW'
    else:
        raise 'Unexpected action %s' % str( action )

######################################################################
def layersToString( layers ):
    s = []
    if BACKGROUND in layers:
        s += [ 'BCKGRND' ]
    if PANELS in layers:
        s += [ 'PNL' ]
    if GROUPS in layers:
        s += [ 'GRPS' ]
    if ICONS in layers:
        s += [ 'ICNS' ]

    return '+'.join( s )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class UiBenchmark( Benchmark ):
    def __init__( self, action, layers, format, swapInterval ):
        Benchmark.__init__( self )
        self.action       = action
        self.layers       = layers
        self.format       = format
        self.swapInterval = swapInterval
        self.name = "OPENGLES1 ui actn=%s layers=%s frmt=%s swpntvl=%d" % ( actionToString( self.action ),
                                                                            layersToString( self.layers ),
                                                                            textureTypeToStr( self.format ),
                                                                            self.swapInterval, )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
       
        state = setupGL1( modules, indexTracker, target )            
       
        backgroundWidth  = screenWidth
        backgroundHeight = screenHeight
        panelsX          = PANELSX
        panelsY          = PANELSY
        panelWidth       = screenWidth / panelsX
        panelHeight      = screenHeight / panelsY
        groupsX          = GROUPSX
        groupsY          = GROUPSY
        groupWidth       = panelWidth / groupsX
        groupHeight      = panelHeight / groupsY
        iconsX           = ICONSX
        iconsY           = ICONSY
        iconWidth        = panelWidth / iconsX
        iconHeight       = panelHeight / iconsY

        backgroundTextureWidth,     backgroundTextureS  = toPot( backgroundWidth, True )
        backgroundTextureHeight,    backgroundTextureT  = toPot( backgroundHeight, True )
        panelTextureWidth,          panelTextureS       = toPot( panelWidth, True )
        panelTextureHeight,         panelTextureT       = toPot( panelHeight, True )
        groupTextureWidth,          groupTextureS       = toPot( groupWidth, True )
        groupTextureHeight,         groupTextureT       = toPot( groupHeight, True )
        iconTextureWidth,           iconTextureS        = toPot( iconWidth, True )
        iconTextureHeight,          iconTextureT        = toPot( iconHeight, True )

        ######################################################################
        # Create texture data                                                #
        ######################################################################
        
        if BACKGROUND in self.layers:
            backgroundTextureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
            backgroundTextureIndex     = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
            
            OpenGLES1.CreateCheckerTextureData( backgroundTextureDataIndex,
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             [ 0.9, 0.9, 0.9, 1.0 ],
                                             2, 2,
                                             backgroundTextureWidth,
                                             backgroundTextureHeight,
                                             'OFF',
                                             self.format )

            OpenGLES1.GenTexture( backgroundTextureIndex )
            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
            OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_NEAREST',
                                    'GL_NEAREST',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE',
                                    'OFF' )
            if self.action == DRAW:
                OpenGLES1.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )
                       
        if PANELS in self.layers:
            panelTextureDataIndices = []
            panelTextureIndices     = []
            panelOrigins            = []

            total = panelsX * panelsY + 1
            for y in range( panelsY ):
                for x in range( panelsX ):
                    textureDataIndex    = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                    textureIndex        = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                    
                    panelTextureDataIndices += [ textureDataIndex ]
                    panelTextureIndices     += [ textureIndex ]
                    panelOrigins            += [ ( panelWidth * x, panelHeight * y, ) ]

                    r = 1.0 * ( ( x + ( y * panelsX ) + 1 ) / float( total ) )
                   
                    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                                     [ 0.0, r, 0.0, 0.5 ],
                                                     [ 0.2, r, 0.2, 0.5 ],
                                                     2, 2, 
                                                     panelTextureWidth,
                                                     panelTextureHeight,
                                                     'OFF',
                                                     self.format )

                    OpenGLES1.GenTexture( textureIndex )
                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                            'GL_NEAREST',
                                            'GL_NEAREST',
                                            'GL_CLAMP_TO_EDGE',
                                            'GL_CLAMP_TO_EDGE',
                                            'OFF' )
                    if self.action == DRAW:
                        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )                   
                   
        if GROUPS in self.layers:
            groupTextureDataIndices = []
            groupTextureIndices     = []
            groupOrigins            = []

            total = groupsX * groupsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( groupsY ):
                        for x in range( groupsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                    
                            groupTextureDataIndices += [ textureDataIndex ]
                            groupTextureIndices     += [ textureIndex ]
                            groupOrigins            += [ ( px * panelWidth + groupWidth * x,
                                                           py * panelHeight + groupHeight * y, ) ]

                            r = 1.0 * ( x + ( y * groupsX ) + 1 ) / float( total )

                            OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                                             [ 0.0, 0.0, r, 0.5 ],
                                                             [ 0.2, 0.2, r, 0.5 ],
                                                             2, 2, 
                                                             groupTextureWidth,
                                                             groupTextureHeight,
                                                             'OFF',
                                                             self.format )

                            OpenGLES1.GenTexture( textureIndex )
                            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                                    'GL_NEAREST',
                                                    'GL_NEAREST',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'OFF' )
                            
                            if self.action == DRAW:
                                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                                                                          
        if ICONS in self.layers:
            iconTextureDataIndices  = []
            iconTextureIndices      = []
            iconOrigins             = []

            total = iconsX * iconsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( iconsY ):
                        for x in range( iconsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                    
                            iconTextureDataIndices  += [ textureDataIndex ]
                            iconTextureIndices      += [ textureIndex ]
                            iconOrigins             += [ ( px * panelWidth + iconWidth * x,
                                                           py * panelHeight + iconHeight * y, ) ]

                            r = 1.0 * ( x + ( y * iconsX ) + 1 ) / float( total )

                            OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                                             [ 0.3, 0.3, r, 0.5 ],
                                                             [ 0.2, 0.2, r, 0.5 ],
                                                             2, 2, 
                                                             iconTextureWidth,
                                                             iconTextureHeight,
                                                             'OFF',
                                                             self.format )
                            
                            OpenGLES1.GenTexture( textureIndex )
                            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                                    'GL_NEAREST',
                                                    'GL_NEAREST',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'OFF' )

                            if self.action == DRAW:
                                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )                            

        ######################################################################
        # Create geometry                                                    #
        ######################################################################
                                
        if BACKGROUND in self.layers:              
            backgroundTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                                   2,
                                                                   [ backgroundTextureS,
                                                                     backgroundTextureT ] ) ]
        
            background = MeshStripPlane( 2,
                                         2,
                                         MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                         None,
                                         None,
                                         backgroundTexCoordAttribute,
                                         MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                         None,
                                         MESH_CCW_WINDING,
                                         False )

            background.translate( [ -0.5, -0.5, 0 ] )
            background.scale( [ 2.0 * backgroundWidth / float( screenWidth ),
                                2.0 * backgroundHeight / float( screenHeight ) ] )
                   
            backgroundVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( backgroundVertexArrayIndex,
                                   background.vertexGLType )
            OpenGLES1.AppendToArray( backgroundVertexArrayIndex, background.vertices )
            
            backgroundTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( backgroundTexCoordArrayIndex,
                                   background.texCoordsGLTypes[ 0 ] )
            OpenGLES1.AppendToArray( backgroundTexCoordArrayIndex, background.texCoords[ 0 ] )
            
            backgroundIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( backgroundIndexArrayIndex, background.indexGLType );
            OpenGLES1.AppendToArray( backgroundIndexArrayIndex, background.indices )

        if PANELS in self.layers:              
            panelTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ panelTextureS,
                                                                panelTextureT ] ) ]
        
            panel = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    panelTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            panel.translate( [ -0.5, -0.5, 0 ] )
            panel.scale( [ 2.0 * panelWidth / float( screenWidth ),
                           2.0 * panelHeight / float( screenHeight ) ] )
           
            panelVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( panelVertexArrayIndex,
                                   panel.vertexGLType )
            OpenGLES1.AppendToArray( panelVertexArrayIndex, panel.vertices )
            
            panelTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( panelTexCoordArrayIndex,
                                   panel.texCoordsGLTypes[ 0 ] )
            OpenGLES1.AppendToArray( panelTexCoordArrayIndex, panel.texCoords[ 0 ] )
            
            panelIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( panelIndexArrayIndex, panel.indexGLType );
            OpenGLES1.AppendToArray( panelIndexArrayIndex, panel.indices )

        if GROUPS in self.layers:              
            groupTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ groupTextureS,
                                                                groupTextureT ] ) ]
        
            group = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    groupTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            group.translate( [ -0.5, -0.5, 0 ] )
            group.scale( [ 2.0 * groupWidth / float( screenWidth ),
                           2.0 * groupHeight / float( screenHeight ) ] )
           
            groupVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( groupVertexArrayIndex,
                                   group.vertexGLType )
            OpenGLES1.AppendToArray( groupVertexArrayIndex, group.vertices )
            
            groupTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( groupTexCoordArrayIndex,
                                   group.texCoordsGLTypes[ 0 ] )
            OpenGLES1.AppendToArray( groupTexCoordArrayIndex, group.texCoords[ 0 ] )
            
            groupIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( groupIndexArrayIndex, group.indexGLType );
            OpenGLES1.AppendToArray( groupIndexArrayIndex, group.indices )

        if ICONS in self.layers:              
            iconTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                             2,
                                                             [ iconTextureS,
                                                               iconTextureT ] ) ]
        
            icon = MeshStripPlane( 2,
                                   2,
                                   MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                   None,
                                   None,
                                   iconTexCoordAttribute,
                                   MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                   None,
                                   MESH_CCW_WINDING,
                                   False )

            icon.translate( [ -0.5, -0.5, 0 ] )
            icon.scale( [ 2.0 * iconWidth / float( screenWidth ),
                          2.0 * iconHeight / float( screenHeight ) ] )
           
            iconVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( iconVertexArrayIndex,
                                   icon.vertexGLType )
            OpenGLES1.AppendToArray( iconVertexArrayIndex, icon.vertices )
            
            iconTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( iconTexCoordArrayIndex,
                                   icon.texCoordsGLTypes[ 0 ] )
            OpenGLES1.AppendToArray( iconTexCoordArrayIndex, icon.texCoords[ 0 ] )
            
            iconIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( iconIndexArrayIndex, icon.indexGLType );
            OpenGLES1.AppendToArray( iconIndexArrayIndex, icon.indices )

        ######################################################################            
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )

        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )

        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )

        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )

        target.swapInterval( state, self.swapInterval )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        # Clear the screen if drawing is requested and the layers do not include background
        if not BACKGROUND in self.layers:
            if self.action == DRAW or self.action == UPLOAD_AND_DRAW:       
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        # Do texture data upload if requested
        if self.action == UPLOAD or self.action == UPLOAD_AND_DRAW:
            if BACKGROUND in self.layers:
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )                
                OpenGLES1.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )

            if PANELS in self.layers:
                for i in range( len( panelTextureDataIndices ) ):
                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )                    
                    OpenGLES1.TexImage2D( panelTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if GROUPS in self.layers:
                for i in range( len( groupTextureDataIndices ) ):
                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )                    
                    OpenGLES1.TexImage2D( groupTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if ICONS in self.layers:
                for i in range( len( iconTextureDataIndices ) ):
                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )                    
                    OpenGLES1.TexImage2D( iconTextureDataIndices[ i ], 'GL_TEXTURE_2D' )
                    
        # Draw if drawing is requested                
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            if BACKGROUND in self.layers:
                OpenGLES1.Disable( 'GL_BLEND' )

                OpenGLES1.LoadIdentity()

                OpenGLES1.VertexPointer( backgroundVertexArrayIndex, 2, 0 )
                OpenGLES1.TexCoordPointer( backgroundTexCoordArrayIndex, 2, 0 )

                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                
                OpenGLES1.DrawElements( background.glMode,
                                        len( background.indices ),
                                        0,
                                        backgroundIndexArrayIndex )    

            if PANELS in self.layers:
                OpenGLES1.Enable( 'GL_BLEND' )
                
                for i in range( len( panelTextureIndices ) ):
                    origin = panelOrigins[ i ]
                    tx = ( -1.0 + panelWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + panelHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )
                    
                    OpenGLES1.LoadMatrix( [ 1.0, 0.0, 0.0, 0.0,
                                            0.0, 1.0, 0.0, 0.0,
                                            0.0, 0.0, 1.0, 0.0,
                                            tx,  ty,  0.0, 1.0 ] )

                    OpenGLES1.VertexPointer( panelVertexArrayIndex, 2, 0 )
                    OpenGLES1.TexCoordPointer( panelTexCoordArrayIndex, 2, 0 )

                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                    
                    OpenGLES1.DrawElements( panel.glMode,
                                            len( panel.indices ),
                                            0,
                                            panelIndexArrayIndex )    

            if GROUPS in self.layers:
                OpenGLES1.Enable( 'GL_BLEND' )
                
                for i in range( len( groupTextureIndices ) ):
                    origin = groupOrigins[ i ]
                    tx = ( -1.0 + groupWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + groupHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )
                    
                    OpenGLES1.LoadMatrix( [ 1.0, 0.0, 0.0, 0.0,
                                            0.0, 1.0, 0.0, 0.0,
                                            0.0, 0.0, 1.0, 0.0,
                                            tx,  ty,  0.0, 1.0 ] )

                    OpenGLES1.VertexPointer( groupVertexArrayIndex, 2, 0 )
                    OpenGLES1.TexCoordPointer( groupTexCoordArrayIndex, 2, 0 )

                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                    
                    OpenGLES1.DrawElements( group.glMode,
                                            len( group.indices ),
                                            0,
                                            groupIndexArrayIndex )    

            if ICONS in self.layers:
                OpenGLES1.Enable( 'GL_BLEND' )
                
                for i in range( len( iconTextureIndices ) ):
                    origin = iconOrigins[ i ]
                    tx = ( -1.0 + iconWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + iconHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )

                    OpenGLES1.LoadMatrix( [ 1.0, 0.0, 0.0, 0.0,
                                            0.0, 1.0, 0.0, 0.0,
                                            0.0, 0.0, 1.0, 0.0,
                                            tx,  ty,  0.0, 1.0 ] )

                    OpenGLES1.VertexPointer( iconVertexArrayIndex, 2, 0 )
                    OpenGLES1.TexCoordPointer( iconTexCoordArrayIndex, 2, 0 )

                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                    
                    OpenGLES1.DrawElements( icon.glMode,
                                            len( icon.indices ),
                                            0,
                                            iconIndexArrayIndex )    
                    
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            target.swapBuffers( state )

            
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class Invert4x4MatrixBenchmark( Benchmark ):  
    def __init__( self, count, iterations ):
        Benchmark.__init__( self )
        self.count      = count
        self.iterations = iterations
        self.name = "OPENGLES1 invert 4x4 matrix mtrx.cnt=%d itrtns=%d"  % ( self.count,
                                                                             self.iterations, )
       
    def build( self, target, modules ):
        Cpu = modules[ 'CPU' ]
        
        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Cpu.Invert4x4Matrix( self.count, self.iterations )

                  
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ParallelUiBenchmark( Benchmark ):
    def __init__( self, action, layers, format, matrices ):
        Benchmark.__init__( self )
        self.action   = action
        self.layers   = layers
        self.format   = format
        self.matrices = matrices
        self.name = "OPENGLES1 parallel ui actn=%s layers=%s frmt=%s mtrcs=%d" % ( actionToString( self.action ),
                                                                                   layersToString( self.layers ),
                                                                                   textureTypeToStr( self.format ),
                                                                                   self.matrices, )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Cpu       = modules[ 'CPU' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
       
        state = setupGL1( modules, indexTracker, target )            
       
        backgroundWidth  = screenWidth
        backgroundHeight = screenHeight
        panelsX          = PANELSX
        panelsY          = PANELSY
        panelWidth       = screenWidth / panelsX
        panelHeight      = screenHeight / panelsY
        groupsX          = GROUPSX
        groupsY          = GROUPSY
        groupWidth       = panelWidth / groupsX
        groupHeight      = panelHeight / groupsY
        iconsX           = ICONSX
        iconsY           = ICONSY
        iconWidth        = panelWidth / iconsX
        iconHeight       = panelHeight / iconsY

        backgroundTextureWidth,     backgroundTextureS  = toPot( backgroundWidth, True )
        backgroundTextureHeight,    backgroundTextureT  = toPot( backgroundHeight, True )
        panelTextureWidth,          panelTextureS       = toPot( panelWidth, True )
        panelTextureHeight,         panelTextureT       = toPot( panelHeight, True )
        groupTextureWidth,          groupTextureS       = toPot( groupWidth, True )
        groupTextureHeight,         groupTextureT       = toPot( groupHeight, True )
        iconTextureWidth,           iconTextureS        = toPot( iconWidth, True )
        iconTextureHeight,          iconTextureT        = toPot( iconHeight, True )

        ######################################################################
        # Create texture data                                                #
        ######################################################################
        
        if BACKGROUND in self.layers:
            backgroundTextureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
            backgroundTextureIndex     = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
            
            OpenGLES1.CreateCheckerTextureData( backgroundTextureDataIndex,
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             [ 0.9, 0.9, 0.9, 1.0 ],
                                             2, 2,
                                             backgroundTextureWidth,
                                             backgroundTextureHeight,
                                             'OFF',
                                             self.format )

            OpenGLES1.GenTexture( backgroundTextureIndex )
            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
            OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_NEAREST',
                                    'GL_NEAREST',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE',
                                    'OFF' )
            if self.action == DRAW:
                OpenGLES1.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )
                       
        if PANELS in self.layers:
            panelTextureDataIndices = []
            panelTextureIndices     = []
            panelOrigins            = []

            total = panelsX * panelsY + 1
            for y in range( panelsY ):
                for x in range( panelsX ):
                    textureDataIndex    = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                    textureIndex        = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                    
                    panelTextureDataIndices += [ textureDataIndex ]
                    panelTextureIndices     += [ textureIndex ]
                    panelOrigins            += [ ( panelWidth * x, panelHeight * y, ) ]

                    r = 1.0 * ( ( x + ( y * panelsX ) + 1 ) / float( total ) )
                   
                    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                                     [ 0.0, r, 0.0, 0.5 ],
                                                     [ 0.2, r, 0.2, 0.5 ],
                                                     2, 2, 
                                                     panelTextureWidth,
                                                     panelTextureHeight,
                                                     'OFF',
                                                     self.format )

                    OpenGLES1.GenTexture( textureIndex )
                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                            'GL_NEAREST',
                                            'GL_NEAREST',
                                            'GL_CLAMP_TO_EDGE',
                                            'GL_CLAMP_TO_EDGE',
                                            'OFF' )
                    if self.action == DRAW:
                        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )                   
                   
        if GROUPS in self.layers:
            groupTextureDataIndices = []
            groupTextureIndices     = []
            groupOrigins            = []

            total = groupsX * groupsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( groupsY ):
                        for x in range( groupsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                    
                            groupTextureDataIndices += [ textureDataIndex ]
                            groupTextureIndices     += [ textureIndex ]
                            groupOrigins            += [ ( px * panelWidth + groupWidth * x,
                                                           py * panelHeight + groupHeight * y, ) ]

                            r = 1.0 * ( x + ( y * groupsX ) + 1 ) / float( total )

                            OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                                             [ 0.0, 0.0, r, 0.5 ],
                                                             [ 0.2, 0.2, r, 0.5 ],
                                                             2, 2, 
                                                             groupTextureWidth,
                                                             groupTextureHeight,
                                                             'OFF',
                                                             self.format )

                            OpenGLES1.GenTexture( textureIndex )
                            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                                    'GL_NEAREST',
                                                    'GL_NEAREST',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'OFF' )
                            
                            if self.action == DRAW:
                                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                                                                          
        if ICONS in self.layers:
            iconTextureDataIndices  = []
            iconTextureIndices      = []
            iconOrigins             = []

            total = iconsX * iconsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( iconsY ):
                        for x in range( iconsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                    
                            iconTextureDataIndices  += [ textureDataIndex ]
                            iconTextureIndices      += [ textureIndex ]
                            iconOrigins             += [ ( px * panelWidth + iconWidth * x,
                                                           py * panelHeight + iconHeight * y, ) ]

                            r = 1.0 * ( x + ( y * iconsX ) + 1 ) / float( total )

                            OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                                             [ 0.3, 0.3, r, 0.5 ],
                                                             [ 0.2, 0.2, r, 0.5 ],
                                                             2, 2, 
                                                             iconTextureWidth,
                                                             iconTextureHeight,
                                                             'OFF',
                                                             self.format )
                            
                            OpenGLES1.GenTexture( textureIndex )
                            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                                    'GL_NEAREST',
                                                    'GL_NEAREST',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'OFF' )

                            if self.action == DRAW:
                                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )                            

        ######################################################################
        # Create geometry                                                    #
        ######################################################################
                                
        if BACKGROUND in self.layers:              
            backgroundTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                                   2,
                                                                   [ backgroundTextureS,
                                                                     backgroundTextureT ] ) ]
        
            background = MeshStripPlane( 2,
                                         2,
                                         MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                         None,
                                         None,
                                         backgroundTexCoordAttribute,
                                         MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                         None,
                                         MESH_CCW_WINDING,
                                         False )

            background.translate( [ -0.5, -0.5, 0 ] )
            background.scale( [ 2.0 * backgroundWidth / float( screenWidth ),
                                2.0 * backgroundHeight / float( screenHeight ) ] )
                   
            backgroundVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( backgroundVertexArrayIndex,
                                   background.vertexGLType )
            OpenGLES1.AppendToArray( backgroundVertexArrayIndex, background.vertices )
            
            backgroundTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( backgroundTexCoordArrayIndex,
                                   background.texCoordsGLTypes[ 0 ] )
            OpenGLES1.AppendToArray( backgroundTexCoordArrayIndex, background.texCoords[ 0 ] )
            
            backgroundIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( backgroundIndexArrayIndex, background.indexGLType );
            OpenGLES1.AppendToArray( backgroundIndexArrayIndex, background.indices )

        if PANELS in self.layers:              
            panelTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ panelTextureS,
                                                                panelTextureT ] ) ]
        
            panel = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    panelTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            panel.translate( [ -0.5, -0.5, 0 ] )
            panel.scale( [ 2.0 * panelWidth / float( screenWidth ),
                           2.0 * panelHeight / float( screenHeight ) ] )
           
            panelVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( panelVertexArrayIndex,
                                   panel.vertexGLType )
            OpenGLES1.AppendToArray( panelVertexArrayIndex, panel.vertices )
            
            panelTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( panelTexCoordArrayIndex,
                                   panel.texCoordsGLTypes[ 0 ] )
            OpenGLES1.AppendToArray( panelTexCoordArrayIndex, panel.texCoords[ 0 ] )
            
            panelIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( panelIndexArrayIndex, panel.indexGLType );
            OpenGLES1.AppendToArray( panelIndexArrayIndex, panel.indices )

        if GROUPS in self.layers:              
            groupTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ groupTextureS,
                                                                groupTextureT ] ) ]
        
            group = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    groupTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            group.translate( [ -0.5, -0.5, 0 ] )
            group.scale( [ 2.0 * groupWidth / float( screenWidth ),
                           2.0 * groupHeight / float( screenHeight ) ] )
           
            groupVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( groupVertexArrayIndex,
                                   group.vertexGLType )
            OpenGLES1.AppendToArray( groupVertexArrayIndex, group.vertices )
            
            groupTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( groupTexCoordArrayIndex,
                                   group.texCoordsGLTypes[ 0 ] )
            OpenGLES1.AppendToArray( groupTexCoordArrayIndex, group.texCoords[ 0 ] )
            
            groupIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( groupIndexArrayIndex, group.indexGLType );
            OpenGLES1.AppendToArray( groupIndexArrayIndex, group.indices )

        if ICONS in self.layers:              
            iconTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                             2,
                                                             [ iconTextureS,
                                                               iconTextureT ] ) ]
        
            icon = MeshStripPlane( 2,
                                   2,
                                   MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                   None,
                                   None,
                                   iconTexCoordAttribute,
                                   MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                   None,
                                   MESH_CCW_WINDING,
                                   False )

            icon.translate( [ -0.5, -0.5, 0 ] )
            icon.scale( [ 2.0 * iconWidth / float( screenWidth ),
                          2.0 * iconHeight / float( screenHeight ) ] )
           
            iconVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( iconVertexArrayIndex,
                                   icon.vertexGLType )
            OpenGLES1.AppendToArray( iconVertexArrayIndex, icon.vertices )
            
            iconTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( iconTexCoordArrayIndex,
                                   icon.texCoordsGLTypes[ 0 ] )
            OpenGLES1.AppendToArray( iconTexCoordArrayIndex, icon.texCoords[ 0 ] )
            
            iconIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
            OpenGLES1.CreateArray( iconIndexArrayIndex, icon.indexGLType );
            OpenGLES1.AppendToArray( iconIndexArrayIndex, icon.indices )

        ######################################################################            
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )

        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )

        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )

        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )

        target.swapInterval( state, 1 )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.matrices > 0:
            Cpu.Invert4x4Matrix( self.matrices, 1 )
               
        # Clear the screen if drawing is requested and the layers do not include background
        if not BACKGROUND in self.layers:
            if self.action == DRAW or self.action == UPLOAD_AND_DRAW:       
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        # Do texture data upload if requested
        if self.action == UPLOAD or self.action == UPLOAD_AND_DRAW:
            if BACKGROUND in self.layers:
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )                
                OpenGLES1.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )

            if PANELS in self.layers:
                for i in range( len( panelTextureDataIndices ) ):
                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )                    
                    OpenGLES1.TexImage2D( panelTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if GROUPS in self.layers:
                for i in range( len( groupTextureDataIndices ) ):
                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )                    
                    OpenGLES1.TexImage2D( groupTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if ICONS in self.layers:
                for i in range( len( iconTextureDataIndices ) ):
                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )                    
                    OpenGLES1.TexImage2D( iconTextureDataIndices[ i ], 'GL_TEXTURE_2D' )
                    
        # Draw if drawing is requested                
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            if BACKGROUND in self.layers:
                OpenGLES1.Disable( 'GL_BLEND' )

                OpenGLES1.LoadIdentity()

                OpenGLES1.VertexPointer( backgroundVertexArrayIndex, 2, 0 )
                OpenGLES1.TexCoordPointer( backgroundTexCoordArrayIndex, 2, 0 )

                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                
                OpenGLES1.DrawElements( background.glMode,
                                        len( background.indices ),
                                        0,
                                        backgroundIndexArrayIndex )    

            if PANELS in self.layers:
                OpenGLES1.Enable( 'GL_BLEND' )
                
                for i in range( len( panelTextureIndices ) ):
                    origin = panelOrigins[ i ]
                    tx = ( -1.0 + panelWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + panelHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )
                    
                    OpenGLES1.LoadMatrix( [ 1.0, 0.0, 0.0, 0.0,
                                            0.0, 1.0, 0.0, 0.0,
                                            0.0, 0.0, 1.0, 0.0,
                                            tx,  ty,  0.0, 1.0 ] )

                    OpenGLES1.VertexPointer( panelVertexArrayIndex, 2, 0 )
                    OpenGLES1.TexCoordPointer( panelTexCoordArrayIndex, 2, 0 )

                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                    
                    OpenGLES1.DrawElements( panel.glMode,
                                            len( panel.indices ),
                                            0,
                                            panelIndexArrayIndex )    

            if GROUPS in self.layers:
                OpenGLES1.Enable( 'GL_BLEND' )
                
                for i in range( len( groupTextureIndices ) ):
                    origin = groupOrigins[ i ]
                    tx = ( -1.0 + groupWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + groupHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )
                    
                    OpenGLES1.LoadMatrix( [ 1.0, 0.0, 0.0, 0.0,
                                            0.0, 1.0, 0.0, 0.0,
                                            0.0, 0.0, 1.0, 0.0,
                                            tx,  ty,  0.0, 1.0 ] )

                    OpenGLES1.VertexPointer( groupVertexArrayIndex, 2, 0 )
                    OpenGLES1.TexCoordPointer( groupTexCoordArrayIndex, 2, 0 )

                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                    
                    OpenGLES1.DrawElements( group.glMode,
                                            len( group.indices ),
                                            0,
                                            groupIndexArrayIndex )    

            if ICONS in self.layers:
                OpenGLES1.Enable( 'GL_BLEND' )
                
                for i in range( len( iconTextureIndices ) ):
                    origin = iconOrigins[ i ]
                    tx = ( -1.0 + iconWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + iconHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )

                    OpenGLES1.LoadMatrix( [ 1.0, 0.0, 0.0, 0.0,
                                            0.0, 1.0, 0.0, 0.0,
                                            0.0, 0.0, 1.0, 0.0,
                                            tx,  ty,  0.0, 1.0 ] )

                    OpenGLES1.VertexPointer( iconVertexArrayIndex, 2, 0 )
                    OpenGLES1.TexCoordPointer( iconTexCoordArrayIndex, 2, 0 )

                    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                    
                    OpenGLES1.DrawElements( icon.glMode,
                                            len( icon.indices ),
                                            0,
                                            iconIndexArrayIndex )    
                    
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            target.swapBuffers( state )


        
