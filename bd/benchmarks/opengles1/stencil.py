#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *
  
######################################################################
def createPlane( gridx, gridy, tcx, tcy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, 0.0, 0 ] )
    mesh.scale( [ 2.0, 1.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class StencilBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, textureType, useStencil ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.textureType = textureType
        self.useStencil  = useStencil       
        self.name = 'OPENGLES1 stencil use=%s (grd=%dx%d txtr.tpe=%s)' % ( boolToStr( self.useStencil ),
                                                                           self.gridx,
                                                                           self.gridy,
                                                                           textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        if self.useStencil:
            attribs[ CONFIG_STENCILSIZE ] = '>=8'
        else:
            attribs[ CONFIG_STENCILSIZE ] = '-'

        attribs[ CONFIG_DEPTHSIZE ] = '-'     
            
        state                   = setupGL1( modules, indexTracker, target, attribs )
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 0.0, 1.0 ],
                                                self.textureType )
        
        texture1Setup           = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 1.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 1.0, 1.0 ],
                                                self.textureType )
        
        useTexture( modules, texture0Setup, 0, 'GL_REPLACE' )

        if self.useStencil:
            OpenGLES1.ClearStencil( 0 )
            OpenGLES1.Enable( 'GL_STENCIL_TEST' )
            OpenGLES1.CheckValue( 'GL_STENCIL_BITS', 'SCT_GEQUAL', [ 8 ] )
            
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        bits = [ 'GL_COLOR_BUFFER_BIT' ]
        if self.useStencil:
            bits += [ 'GL_STENCIL_BUFFER_BIT' ]

        OpenGLES1.Clear( bits )

        if self.useStencil:
            OpenGLES1.StencilFunc( 'GL_ALWAYS', 1, '0x1' )
            OpenGLES1.StencilOp( 'GL_REPLACE', 'GL_REPLACE', 'GL_REPLACE' )

        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture0Setup.textureIndex )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Translate( [ 0.0, 0.0, 0.0 ] )
        drawMeshBufferData( modules, meshBufferDataSetup )

        if self.useStencil:
            OpenGLES1.StencilFunc( 'GL_NOTEQUAL', 1, '0x1' )
            OpenGLES1.StencilOp( 'GL_KEEP', 'GL_KEEP', 'GL_KEEP' )

        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Setup.textureIndex )

        OpenGLES1.LoadIdentity()
        OpenGLES1.Translate( [ 0.0, -1.0, 0.0 ] )
        drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

