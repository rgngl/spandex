#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# Potential additional benchmarks
#    * better vbo benchmarks: two meshes with slightly different data
#    * triangle fan
#    * texture combiners
#    * palette textures
#    * ETC compressed textures
#    * texture environment
#    * projection
#    * normal normalization
#    * fog
#    * logic op
#    * perspective correction
#    * depth test
#    * clip planes
#    * hints
#    ...

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../..' )

import  bd

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'OpenGLES1.1 benchmark'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

#----------------------------------------------------------------------
( width, height, )      = target.getScreenSize()

#----------------------------------------------------------------------
import resources
suite.addBenchmark( resources.PrintResourceProfile( 'start' ), 'EGL_FULL' )

#----------------------------------------------------------------------
import info
suite.addBenchmark( info.PrintOpenGLES1Info() )

#----------------------------------------------------------------------
import clear
suite.addBenchmark( clear.ClearBenchmark( [ 'GL_COLOR_BUFFER_BIT' ] ) )
suite.addBenchmark( clear.ClearBenchmark( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] ) )
suite.addBenchmark( clear.SynchronizedClearBenchmark( [ 'GL_COLOR_BUFFER_BIT' ] ) ) 

#----------------------------------------------------------------------
import marketing
GRIDX    = 2
GRIDY    = 2
OVERDRAW = 32
suite.addBenchmark( marketing.FlatPixelsBenchmark( GRIDX, GRIDY, OVERDRAW ) )
suite.addBenchmark( marketing.SmoothPixelsBenchmark( GRIDX, GRIDY, OVERDRAW ) )
suite.addBenchmark( marketing.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( marketing.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB888' ) )
suite.addBenchmark( marketing.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( marketing.SmoothTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( marketing.FlatMultitexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565','OPENGLES1_LUMINANCE8' ) )
suite.addBenchmark( marketing.SmoothMultitexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565','OPENGLES1_LUMINANCE8' ) )
suite.addBenchmark( marketing.PixelFillrateBenchmark( OVERDRAW ) )
suite.addBenchmark( marketing.TexelFillrateBenchmark( OVERDRAW, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( marketing.RotatedTexelFillrateBenchmark( OVERDRAW, 'OPENGLES1_RGB565' ) )

#----------------------------------------------------------------------
import realistic
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( realistic.FlatPixelsBenchmark( GRIDX, GRIDY, OVERDRAW ) )
suite.addBenchmark( realistic.SmoothPixelsBenchmark( GRIDX, GRIDY, OVERDRAW ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', False ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB888', False ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGBA8888', False ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB888', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGBA8888', True ) )
suite.addBenchmark( realistic.SmoothTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( realistic.FlatMultitexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565','OPENGLES1_LUMINANCE8' ) )
suite.addBenchmark( realistic.SmoothMultitexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565','OPENGLES1_LUMINANCE8' ) )

suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 2, 'OPENGLES1_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 4, 'OPENGLES1_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 6, 'OPENGLES1_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 8, 'OPENGLES1_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 10, 'OPENGLES1_RGB565', True ) )

#----------------------------------------------------------------------
import draworder
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 3
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, OVERDRAW, 'OPENGLES1_RGB565', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, OVERDRAW, 'OPENGLES1_RGB565', draworder.FRONT_TO_BACK ) )

suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 2, 'OPENGLES1_RGB565', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 2, 'OPENGLES1_RGB565', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 4, 'OPENGLES1_RGB565', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 4, 'OPENGLES1_RGB565', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 8, 'OPENGLES1_RGB565', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 8, 'OPENGLES1_RGB565', draworder.FRONT_TO_BACK ) )

#----------------------------------------------------------------------
import transformation
GRIDX     = 48
GRIDY     = 128
OVERDRAW  = 3
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, None, transformation.NONE ) )
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, None, transformation.HALF ) )
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, None, transformation.FULLY ) )
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', transformation.NONE ) )
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', transformation.HALF ) )
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', transformation.FULLY ) )

#----------------------------------------------------------------------
import texturetype
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 3
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB888' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGBA4444' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGBA5551' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGBA8888' ) )

suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_RGB565' ) )
suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_ETC1_RGB8' ) )
suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_PVRTC_RGB2' ) )
suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_PVRTC_RGB4' ) )
suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_PVRTC_RGBA2' ) )
suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_PVRTC_RGBA4' ) )

#----------------------------------------------------------------------
import texturesize
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 3
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 32, 32, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 64, 64, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 128, 128, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES1_RGB565' ) )

suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 32, 32, 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 64, 64, 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 128, 128, 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES1_RGBA8888' ) )

#----------------------------------------------------------------------
import vertexstore
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 3
suite.addBenchmark( vertexstore.VertexStoreBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', vertexstore.ARRAYS ) )
suite.addBenchmark( vertexstore.VertexStoreBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', vertexstore.MESH ) )
suite.addBenchmark( vertexstore.VertexStoreBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', vertexstore.VBO_ARRAYS ) )
suite.addBenchmark( vertexstore.VertexStoreBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', vertexstore.VBO_MESH ) )

#----------------------------------------------------------------------
import triangles
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 3
suite.addBenchmark( triangles.TrianglesBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', triangles.STRIP ) )
suite.addBenchmark( triangles.TrianglesBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', triangles.DISCRETE ) )

#----------------------------------------------------------------------
import elements
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 1, 1 ) )
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 1, 32 ) )
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 1, 64 ) )
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 10, 1 ) )
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 10, 32 ) )
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 10, 64 ) )

suite.addBenchmark( elements.PointsBenchmark( 10, 10, 1, 1 ) )
suite.addBenchmark( elements.PointsBenchmark( 10, 10, 1, 32 ) )
suite.addBenchmark( elements.PointsBenchmark( 10, 10, 1, 64 ) )
suite.addBenchmark( elements.PointsBenchmark( 10, 10, 10, 1 ) )
suite.addBenchmark( elements.PointsBenchmark( 10, 10, 10, 32 ) )
suite.addBenchmark( elements.PointsBenchmark( 10, 10, 10, 64 ) )

suite.addBenchmark( elements.VboLinesBenchmark( 32, 1, 1, 1.0, False, False ) )
suite.addBenchmark( elements.VboLinesBenchmark( 32, 1, 32, 1.0, False, False ) )
suite.addBenchmark( elements.VboLinesBenchmark( 32, 1, 64, 1.0, False, False ) )
suite.addBenchmark( elements.VboLinesBenchmark( 32, 10, 1, 1.0, False, False ) )
suite.addBenchmark( elements.VboLinesBenchmark( 32, 10, 32, 1.0, False, False ) )
suite.addBenchmark( elements.VboLinesBenchmark( 32, 10, 64, 1.0, False, False ) )

suite.addBenchmark( elements.LinesBenchmark( 32, 1, 1, 1.0, False, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 1, 32, 1.0, False, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 1, 64, 1.0, False, False ) )
suite.addBenchmark( elements.LinesBenchmark( 256, 1, 64, 1.0, False, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 1, 1.0, False, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 32, 1.0, False, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 64, 1.0, False, False ) )

suite.addBenchmark( elements.LinesBenchmark( 32, 1, 1, 0.01, False, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 1, 32, 0.01, False, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 1, 64, 0.01, False, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 1, 0.01, False, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 32, 0.01, False, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 64, 0.01, False, False ) )

suite.addBenchmark( elements.LinesBenchmark( 32, 1, 64, 1.0, True, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 1, 64, 1.0, False, True ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 1, 64, 1.0, True, True ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 64, 1.0, True, False ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 64, 1.0, False, True ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 64, 1.0, True, True ) )

#----------------------------------------------------------------------
import texturemod
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 1
suite.addBenchmark( texturemod.TwiddleBenchmark( 128, 128, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( texturemod.TwiddleBenchmark( 256, 256, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( texturemod.TwiddleBenchmark( 512, 512, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( texturemod.TwiddleBenchmark( 1024, 1024, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( texturemod.TwiddleBenchmark( 128, 128, 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( texturemod.TwiddleBenchmark( 256, 256, 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( texturemod.TwiddleBenchmark( 512, 512, 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( texturemod.TwiddleBenchmark( 1024, 1024, 'OPENGLES1_RGBA8888' ) )

suite.addBenchmark( texturemod.UploadCompressedBenchmark( 128, 'IMAGES_ETC1_RGB8' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 256, 'IMAGES_ETC1_RGB8' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 512, 'IMAGES_ETC1_RGB8' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 1024, 'IMAGES_ETC1_RGB8' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 128, 'IMAGES_PVRTC_RGB4' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 256, 'IMAGES_PVRTC_RGB4' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 512, 'IMAGES_PVRTC_RGB4' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 1024, 'IMAGES_PVRTC_RGB4' ) )

TEXWIDTH  = 256
TEXHEIGHT = 256
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, TEXWIDTH, TEXHEIGHT, 'OPENGLES1_RGB565', 0, 0, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, TEXWIDTH, TEXHEIGHT, 'OPENGLES1_RGB565', 1, 0, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, TEXWIDTH, TEXHEIGHT, 'OPENGLES1_RGB565', 1, 1, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, TEXWIDTH, TEXHEIGHT, 'OPENGLES1_RGB565', 1, 2, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, TEXWIDTH, TEXHEIGHT, 'OPENGLES1_RGB565', 1, 3, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, TEXWIDTH, TEXHEIGHT, 'OPENGLES1_RGB565', 1, 4, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, TEXWIDTH, TEXHEIGHT, 'OPENGLES1_RGB565', 2, 2, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, TEXWIDTH, TEXHEIGHT, 'OPENGLES1_RGB565', 2, 3, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, TEXWIDTH, TEXHEIGHT, 'OPENGLES1_RGB565', 2, 4, 4  ) )

#----------------------------------------------------------------------
import vertextype
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 3
suite.addBenchmark( vertextype.VertexTypeBenchmark( GRIDX, GRIDY, OVERDRAW, vertextype.FIXED ) )
suite.addBenchmark( vertextype.VertexTypeBenchmark( GRIDX, GRIDY, OVERDRAW, vertextype.FLOAT ) )

#----------------------------------------------------------------------
import blending
GRIDX     = 20
GRIDY     = 32
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGBA8888', 0, 4, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGBA8888', 1, 3, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGBA8888', 2, 2, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGBA8888', 3, 1, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGBA8888', 4, 0, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )

suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGBA8888', 4, 0, 'GL_ONE', 'GL_ZERO' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 4, 0, 'GL_ONE', 'GL_ZERO' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 4, 0, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )

#----------------------------------------------------------------------
import scissor
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 0, 0, width, height ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 0, 0, width - 10, height - 10 ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 10, 10, width - 10, height - 10 ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 0, 0, width, height / 2 ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 0, height / 2, width, 2 ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ width / 2, 0, 2, height ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ width / 2, height / 2, 10, 10 ] ) )

suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [], 1, [] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [], 1, [ 0, 0, width, height ] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [ 0, 0, width, height ], 1, [] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [ 0, 0, width, height ], 1, [ 0, 0, width, height ] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [ 0, 0, width - 10, height - 10 ], 1, [ 10, 10, width - 10, height - 10 ] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [ 0, height / 2, width, height/ 2 ], 1, [ 0, 0, width, height / 2 ] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [ 0, height / 2, width, height/ 2 ], 1, [ 0, 0, width, height / 2 + 10 ] ) )

#----------------------------------------------------------------------
import viewport
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( viewport.ViewportBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [] ) )
suite.addBenchmark( viewport.ViewportBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 10, 10, width - 20 , height - 20 ] ) )
suite.addBenchmark( viewport.ViewportBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 0, 0, width / 2 , height / 2 ] ) )
suite.addBenchmark( viewport.ViewportBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ width / 4, height / 4, width / 2 , height / 2 ] ) )
suite.addBenchmark( viewport.ViewportBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 0, 0, height / 2, width / 2 ] ) )

suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [], 1, [] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [], 1, [ 0, 0, width, height ] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [ 0, 0, width, height ], 1, [] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [ 0, 0, width, height ], 1, [ 0, 0, width, height ] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [ 0, 0, width - 10, height - 10 ], 1, [ 10, 10, width - 10, height - 10 ] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [ 0, height / 2, width, height/ 2 ], 1, [ 0, 0, width, height / 2 ] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', 1, [ 0, height / 2, width, height/ 2 ], 1, [ 0, 0, width, height / 2 + 10 ] ) )

#----------------------------------------------------------------------
import colormask
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( colormask.ColorMaskBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', True, True, True, True ) )
suite.addBenchmark( colormask.ColorMaskBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', True, False, False, False ) )
suite.addBenchmark( colormask.ColorMaskBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', True, True, False, False ) )
suite.addBenchmark( colormask.ColorMaskBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', True, True, True, False ) )

#----------------------------------------------------------------------
import copyteximage
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 256, 256, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 256, 512, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 512, 512, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 512, 512, 'OPENGLES1_RGB888' ) )
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 512, 512, 'OPENGLES1_RGBA8888' ) )

suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 512, 'OPENGLES1_RGB565', 0, 0, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 512, 'OPENGLES1_RGB565', 1, 0, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 512, 'OPENGLES1_RGB565', 1, 1, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 512, 'OPENGLES1_RGB565', 1, 2, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 512, 'OPENGLES1_RGB565', 1, 3, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 512, 'OPENGLES1_RGB565', 1, 4, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 512, 'OPENGLES1_RGB565', 2, 2, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 512, 'OPENGLES1_RGB565', 2, 3, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 512, 'OPENGLES1_RGB565', 2, 4, 4  ) )

#----------------------------------------------------------------------
import bufferload
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( bufferload.BufferLoadBenchmark( 10 * 1024, 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW' ) )
suite.addBenchmark( bufferload.BufferLoadBenchmark( 100 * 1024, 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW' ) )
suite.addBenchmark( bufferload.BufferLoadBenchmark( 1000 * 1024, 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW' ) )
suite.addBenchmark( bufferload.BufferLoadBenchmark( 1000 * 1024, 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW' ) )
suite.addBenchmark( bufferload.BufferLoadBenchmark( 1000 * 1024, 'GL_ARRAY_BUFFER', 'GL_DYNAMIC_DRAW' ) )

suite.addBenchmark( bufferload.LoadBufferAndDrawBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', False ) )
suite.addBenchmark( bufferload.LoadBufferAndDrawBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', True ) )

#----------------------------------------------------------------------
import multisampling
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( multisampling.MultisamplingBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1  ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 4  ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 2, 2, 1, 'OPENGLES1_RGB565', 1  ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 2, 2, 1, 'OPENGLES1_RGB565', 4  ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 2, 2, 1, 'OPENGLES1_RGBA8888', 1  ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 2, 2, 1, 'OPENGLES1_RGBA8888', 4  ) )

#----------------------------------------------------------------------
import stencil
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( stencil.StencilBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', False  ) )
suite.addBenchmark( stencil.StencilBenchmark( GRIDX, GRIDY, 'OPENGLES1_RGB565', True  ) )

#----------------------------------------------------------------------
import readpixels
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( readpixels.ReadPixelsBenchmark( 128, 128 ) )
suite.addBenchmark( readpixels.ReadPixelsBenchmark( 256, 256 ) )
suite.addBenchmark( readpixels.ReadPixelsBenchmark( width, height / 2 ) )
suite.addBenchmark( readpixels.ReadPixelsBenchmark( width, height ) )

suite.addBenchmark( readpixels.CopyBuffersWindowBenchmark( 128, 128 ), 'EGL_FULL' )
suite.addBenchmark( readpixels.CopyBuffersWindowBenchmark( 256, 256 ), 'EGL_FULL' )
suite.addBenchmark( readpixels.CopyBuffersWindowBenchmark( width, height / 2 ), 'EGL_FULL' )
suite.addBenchmark( readpixels.CopyBuffersWindowBenchmark( width, height ), 'EGL_FULL' )

suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 128, 128, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 256, 256, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( width, height / 2, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( width, height, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 128, 128, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 256, 256, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( width, height / 2, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( width, height, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 128, 128, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 256, 256, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( width, height / 2, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( width, height, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )

suite.addBenchmark( readpixels.DrawAndReadPixelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height, -1  ) )
suite.addBenchmark( readpixels.DrawAndReadPixelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height, 0  ) )

#----------------------------------------------------------------------
import mipmap
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 128, 128, 'OPENGLES1_RGB565', False ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 128, 128, 'OPENGLES1_RGB565', True ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 256, 256, 'OPENGLES1_RGB565', False ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 256, 256, 'OPENGLES1_RGB565', True ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 512, 512, 'OPENGLES1_RGB565', False ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 512, 512, 'OPENGLES1_RGB565', True ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 1024, 1024, 'OPENGLES1_RGB565', False ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 1024, 1024, 'OPENGLES1_RGB565', True ) )

suite.addBenchmark( mipmap.UploadGenerateMipmapAndDrawBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 512, 512, -1, False ) )
suite.addBenchmark( mipmap.UploadGenerateMipmapAndDrawBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 512, 512, 0, False ) )
suite.addBenchmark( mipmap.UploadGenerateMipmapAndDrawBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 512, 512, 0, True ) )

#----------------------------------------------------------------------
import polygonoffset
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( polygonoffset.PolygonOffsetBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', False ) )
suite.addBenchmark( polygonoffset.PolygonOffsetBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', True ) )

#----------------------------------------------------------------------
import rendertobuffer
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3

suite.addBenchmark( rendertobuffer.RenderToWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height / 2 ), 'EGL_FULL' )
suite.addBenchmark( rendertobuffer.RenderToWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height ), 'EGL_FULL' )

suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height / 2, 'SCT_COLOR_FORMAT_RGB565', rendertobuffer.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height / 2, 'SCT_COLOR_FORMAT_RGB565', rendertobuffer.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height / 2, 'SCT_COLOR_FORMAT_RGB565', rendertobuffer.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height / 2, 'SCT_COLOR_FORMAT_RGB888', rendertobuffer.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height / 2, 'SCT_COLOR_FORMAT_RGB888', rendertobuffer.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height / 2, 'SCT_COLOR_FORMAT_RGB888', rendertobuffer.FINISH ), 'EGL_LIMITED' )

suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height, 'SCT_COLOR_FORMAT_RGB565', rendertobuffer.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height, 'SCT_COLOR_FORMAT_RGB565', rendertobuffer.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height, 'SCT_COLOR_FORMAT_RGB565', rendertobuffer.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height, 'SCT_COLOR_FORMAT_RGB888', rendertobuffer.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height, 'SCT_COLOR_FORMAT_RGB888', rendertobuffer.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height, 'SCT_COLOR_FORMAT_RGB888', rendertobuffer.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height, 'SCT_COLOR_FORMAT_RGBA8888', rendertobuffer.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height, 'SCT_COLOR_FORMAT_RGBA8888', rendertobuffer.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.RenderToPbufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', width, height, 'SCT_COLOR_FORMAT_RGBA8888', rendertobuffer.FINISH ), 'EGL_LIMITED' )

suite.addBenchmark( rendertobuffer.DrawPbufferTextureBenchmark( 2, 2, 1, 'OPENGLES1_RGB565', 512, 512, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.DrawPbufferTextureBenchmark( 2, 2, 1, 'OPENGLES1_RGB565', 512, 512, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.DrawPbufferTextureBenchmark( 2, 2, 1, 'OPENGLES1_RGB565', 512, 512, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )

suite.addBenchmark( rendertobuffer.DrawPbufferTextureBenchmark( 2, 2, 1, 'OPENGLES1_RGBA8888', 512, 512, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.DrawPbufferTextureBenchmark( 2, 2, 1, 'OPENGLES1_RGBA8888', 512, 512, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.DrawPbufferTextureBenchmark( 2, 2, 1, 'OPENGLES1_RGBA8888', 512, 512, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )

suite.addBenchmark( rendertobuffer.DrawPbufferTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGBA8888', 512, 512, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.DrawPbufferTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGBA8888', 512, 512, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( rendertobuffer.DrawPbufferTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGBA8888', 512, 512, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )

#----------------------------------------------------------------------
import light
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( light.NoLightsBenchmark( GRIDX, GRIDY, OVERDRAW, None ) )
suite.addBenchmark( light.NoLightsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565' ) )
suite.addBenchmark( light.DirectionalWhiteLightBenchmark( GRIDX, GRIDY, OVERDRAW, None, 1, False ) )
suite.addBenchmark( light.DirectionalWhiteLightBenchmark( GRIDX, GRIDY, OVERDRAW, None, 2, False ) )
suite.addBenchmark( light.DirectionalWhiteLightBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1, False ) )
suite.addBenchmark( light.DirectionalWhiteLightBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 2, False ) )
suite.addBenchmark( light.DirectionalWhiteLightBenchmark( GRIDX, GRIDY, OVERDRAW, None, 1, True ) )
suite.addBenchmark( light.DirectionalWhiteLightBenchmark( GRIDX, GRIDY, OVERDRAW, None, 2, True ) )
suite.addBenchmark( light.DirectionalWhiteLightBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1, True ) )
suite.addBenchmark( light.DirectionalWhiteLightBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 2, True ) )
suite.addBenchmark( light.DirectionalLightBenchmark( GRIDX, GRIDY, OVERDRAW, None, 1 ) )
suite.addBenchmark( light.DirectionalLightBenchmark( GRIDX, GRIDY, OVERDRAW, None, 2 ) )
suite.addBenchmark( light.DirectionalLightBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1 ) )
suite.addBenchmark( light.DirectionalLightBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 2 ) )
suite.addBenchmark( light.PointLightBenchmark( GRIDX, GRIDY, OVERDRAW, None, 1 ) )
suite.addBenchmark( light.PointLightBenchmark( GRIDX, GRIDY, OVERDRAW, None, 2 ) )
suite.addBenchmark( light.PointLightBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1 ) )
suite.addBenchmark( light.PointLightBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 2 ) )
suite.addBenchmark( light.SpotLightBenchmark( GRIDX, GRIDY, OVERDRAW, None, 1 ) )
suite.addBenchmark( light.SpotLightBenchmark( GRIDX, GRIDY, OVERDRAW, None, 2 ) )
suite.addBenchmark( light.SpotLightBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1 ) )
suite.addBenchmark( light.SpotLightBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 2 ) )
suite.addBenchmark( light.AmbientLightBenchmark( GRIDX, GRIDY, OVERDRAW, None ) )
suite.addBenchmark( light.AmbientLightBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565' ) )

#----------------------------------------------------------------------
import alphatest
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( alphatest.AlphaTestBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1.00 ) )
suite.addBenchmark( alphatest.AlphaTestBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 0.75 ) )
suite.addBenchmark( alphatest.AlphaTestBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 0.50 ) )
suite.addBenchmark( alphatest.AlphaTestBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 0.25 ) )
suite.addBenchmark( alphatest.AlphaTestBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 0.00 ) )

#----------------------------------------------------------------------
import surfaces
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( surfaces.WindowSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'EGL_SINGLE_BUFFER' ), 'EGL_FULL' )
suite.addBenchmark( surfaces.WindowSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'EGL_BACK_BUFFER' ), 'EGL_FULL' )

suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGBA8888', surfaces.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGBA8888', surfaces.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGBA8888', surfaces.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGB565', surfaces.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGB565', surfaces.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGB565', surfaces.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGB888', surfaces.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGB888', surfaces.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGB888', surfaces.FINISH ), 'EGL_LIMITED' )

suite.addBenchmark( surfaces.PixmapSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PixmapSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PixmapSurfaceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )

#----------------------------------------------------------------------
import egl
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( egl.PrintEglInfo(), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreateContextBenchmark(), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreateWindowSurfaceBenchmark(), 'EGL_FULL' )
suite.addBenchmark( egl.CreatePbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreatePbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreatePbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreatePixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreatePixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreatePixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SwapBehaviorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'EGL_BUFFER_DESTROYED' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SwapBehaviorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'EGL_BUFFER_PRESERVED' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SwapIntervalBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 0 ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SwapIntervalBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1 ), 'EGL_LIMITED')
suite.addBenchmark( egl.SwapIntervalBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 2 ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SwapIntervalBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 3 ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ width, height ], None, None ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ width / 2, height / 2 ], None, None ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ width, height ], [ width / 2, height / 2 ], [ 0, 0, width, height ] ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ width, height ], [ width, height ], [ width / 4, height / 4, width / 2, height / 2 ] ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ 240, 320 ], None, None ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ width, height ], [ 240, 320 ], [ 0, 0, width, height ] ), 'EGL_FULL' )
ar = min( width / 240.0, height / 320.0 )
w = int( 240 * ar )
h = int( 320 * ar )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ width, height ], [ 240, 320 ], [ ( width - w  ) / 2, ( height - h ) / 2 , w, h ] ), 'EGL_FULL' )

if width != 320 or height != 480:
    suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ 320, 480 ], None, None ), 'EGL_FULL' )
    suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ width, height ], [ 320, 480 ], [ 0, 0, width, height ] ), 'EGL_FULL' )
    
ar = min( width / 320.0, height / 480.0 )
w = int( 320 * ar )
h = int( 480 * ar )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ width, height ], [ 320, 480 ], [ ( width - w  ) / 2, ( height - h ) / 2 , w, h ] ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ width, height ], [ width / 2, height / 2 ], [ 0, 0, width / 2 + 1, height / 2 + 1 ] ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES1_RGB565', [ width, height ], [ width / 2, height /2 ], [ width / 2, height / 2, width / 2, height / 2 ] ), 'EGL_FULL' )

#----------------------------------------------------------------------
import windows
GRIDX      = 2
GRIDY      = 2
OVERDRAW   = 1
# REQUIRES EGL
suite.addBenchmark( windows.ResizeWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [], [] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 0, 0, width, height ], [ 0, 0, width, height ] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 0, 0, width - 1, height - 1 ], [ 1, 1, width - 1, height - 1 ] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 0, 0, width - 10, height - 10 ], [ 10, 10, width - 10, height - 10 ] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', [ 0, 0, width, height / 2 ], [ 0, 0, height / 2, width ] ), 'EGL_FULL' )

suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 0, 0, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1, 1, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1, 2, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 2, 2, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 2, 3, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 3, 3, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 4, 3, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 4, 4, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 5, 5, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 0, 0, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1, 1, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1, 2, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 2, 2, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 2, 3, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 3, 3, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 4, 3, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 4, 4, 1 ), 'EGL_FULL'  )
suite.addBenchmark( windows.TopWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 5, 5, 1 ), 'EGL_FULL'  )

suite.addBenchmark( windows.ChildWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1, 1, 0 ), 'EGL_FULL'  )
suite.addBenchmark( windows.ChildWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1, 2, 0 ), 'EGL_FULL'  )
suite.addBenchmark( windows.ChildWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1, 1, 1 ), 'EGL_FULL'  )
suite.addBenchmark( windows.ChildWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1, 2, 1 ), 'EGL_FULL'  )

suite.addBenchmark( windows.PbuffersBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1, 1, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_LIMITED'  )
suite.addBenchmark( windows.PbuffersBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 2, 2, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_LIMITED'  )
suite.addBenchmark( windows.PbuffersBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 3, 3, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_LIMITED'  )
suite.addBenchmark( windows.PbuffersBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 4, 4, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_LIMITED'  )
suite.addBenchmark( windows.PbuffersBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 4, 4, 'SCT_COLOR_FORMAT_RGB565', windows.READPIXELS ), 'EGL_LIMITED'  )
suite.addBenchmark( windows.PbuffersBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 4, 4, 'SCT_COLOR_FORMAT_RGB565', windows.COPYBUFFERS ), 'EGL_LIMITED'  )

suite.addBenchmark( windows.RotateScreenBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_SCREEN_ORIENTATION_PORTRAIT', 0 ), 'EGL_FULL'  )
suite.addBenchmark( windows.RotateScreenBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_SCREEN_ORIENTATION_LANDSCAPE', 0 ), 'EGL_FULL'  )
suite.addBenchmark( windows.RotateScreenBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_SCREEN_ORIENTATION_PORTRAIT', 1 ), 'EGL_FULL'  )
suite.addBenchmark( windows.RotateScreenBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 'SCT_SCREEN_ORIENTATION_LANDSCAPE', 1 ), 'EGL_FULL'  )

suite.addBenchmark( windows.OpaqueWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 1.0 ), 'EGL_FULL' )
suite.addBenchmark( windows.OpaqueWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES1_RGB565', 0.3 ), 'EGL_FULL' )

#----------------------------------------------------------------------
import ui
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.PANELS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.GROUPS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.ICONS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 0 ) )

suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.PANELS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.GROUPS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.ICONS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 0 ) )

suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.PANELS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.GROUPS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.ICONS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 0 ) )

suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.PANELS ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.GROUPS ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.ICONS ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 1 ) )

suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.PANELS ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.GROUPS ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.ICONS ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], 'OPENGLES1_RGBA8888', 1 ) )

matrices = [ 75, 100, 150, 200, 250, 300 ]
#- for l in matrices:
#-     suite.addBenchmark( ui.Invert4x4MatrixBenchmark( l, 1 ) )

suite.addBenchmark( ui.ParallelUiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], 'OPENGLES1_RGBA8888', 0 ) )
for m in matrices:
    suite.addBenchmark( ui.ParallelUiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], 'OPENGLES1_RGBA8888', m ) )

#----------------------------------------------------------------------
import resources
suite.addBenchmark( resources.PrintResourceProfile( 'end' ), 'EGL_FULL'  )

#----------------------------------------------------------------------
command()
