#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
READPIXELS  = 'rdpxls'
COPYBUFFERS = 'cpbffrs'
FINISH      = 'fnsh'

######################################################################
def createPlane( gridx, gridy, tcx, tcy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )

    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RenderToWindowBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, width, height ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.width       = width
        self.height      = height
        self.name = 'OPENGLES1 render to window sz=%dx%d (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( self.width,
                                                                                               self.height,
                                                                                               self.gridx,
                                                                                               self.gridy,
                                                                                               self.overdraw,
                                                                                               textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()        
        textureW, tcx = toPot( self.width, True )
        textureH, tcy = toPot( self.height, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, screenWidth, screenHeight, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES1 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )       
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT'])

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
       
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, 0 )       

        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RenderToPbufferBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, width, height, colorFormat, flush ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.width       = width
        self.height      = height
        self.colorFormat = colorFormat
        self.flush       = flush
        self.name = 'OPENGLES1 render to pbuffer sz=%dx%d clrfrmt=%s flsh=%s (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( self.width,
                                                                                                                   self.height,
                                                                                                                   pixmapFormatToStr( self.colorFormat ),
                                                                                                                   self.flush,
                                                                                                                   self.gridx,
                                                                                                                   self.gridy,
                                                                                                                   self.overdraw,
                                                                                                                   textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        textureW, tcx = toPot( self.width, True )
        textureH, tcy = toPot( self.height, True )       

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        state   = setupGL1( modules, indexTracker, target, attribs )
        
        displayIndex = state[ 'displayIndex' ]
        
        a = mapColorFormatToAttributes( self.colorFormat )
                   
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       a[ CONFIG_BUFFERSIZE ],
                       a[ CONFIG_REDSIZE ],
                       a[ CONFIG_GREENSIZE ],
                       a[ CONFIG_BLUESIZE ],
                       a[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  surfaceIndex,
                                  configIndex,
                                  self.width,
                                  self.height,
                                  'EGL_NO_TEXTURE',
                                  'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, 0 )
       
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        
        if self.flush == READPIXELS:
            dataIndex           = indexTracker.allocIndex( 'OPENGLES1_DATA_INDEX' )
            OpenGLES1.CreateData( dataIndex, self.width * self.height * 4 ) # RGBA8888
        elif self.flush == COPYBUFFERS:
            pixmapIndex         = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
            Egl.CreatePixmap( pixmapIndex,
                              self.width,
                              self.height,
                              self.colorFormat,
                              -1 )
            
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        if self.flush == READPIXELS:
            OpenGLES1.ReadPixels( dataIndex,
                                  0, 0,
                                  self.width,
                                  self.height )
        elif self.flush == COPYBUFFERS:
            Egl.CopyBuffers( displayIndex,
                             surfaceIndex,
                             pixmapIndex )
        elif self.flush == FINISH:
            OpenGLES1.Finish()
        else:
            raise 'Invalid flush'


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DrawPbufferTextureBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, width, height, colorFormat ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.width       = width
        self.height      = height
        self.colorFormat = colorFormat
        self.name = 'OPENGLES1 draw pbuffer texture sz=%dx%d clrfrmt=%s (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( self.width,
                                                                                                              self.height,
                                                                                                              pixmapFormatToStr( self.colorFormat ),
                                                                                                              self.gridx,
                                                                                                              self.gridy,
                                                                                                              self.overdraw,
                                                                                                              textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()        
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        state   = setupGL1( modules, indexTracker, target, attribs )
        
        displayIndex       = state[ 'displayIndex' ]        
        windowSurfaceIndex = state[ 'surfaceIndex' ]        
        windowContextIndex = state[ 'contextIndex' ]
        
        # Create opengles1 objects for window surface.
        windowMesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0 )
        windowVertexDataSetup = setupVertexData( modules, indexTracker, windowMesh )
        useVertexData( modules, indexTracker, windowVertexDataSetup )
        
        OpenGLES1.ActiveTexture( 0 )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )        
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )     
        
        OpenGLES1.ShadeModel( 'GL_SMOOTH' )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        
        OpenGLES1.Viewport( 0, 0, screenWidth, screenHeight )
        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenGLES1.CheckError( '' )
        
        # Create pbuffer
        pbufferAttrs = mapColorFormatToAttributes( self.colorFormat )
        
        rgbTextures  = 'EGL_DONT_CARE'
        rgbaTextures = 'EGL_DONT_CARE'
        
        rgbaMap = { 'SCT_COLOR_FORMAT_RGB565'   : False,
                    'SCT_COLOR_FORMAT_RGB888'   : False,
                    'SCT_COLOR_FORMAT_RGBX8888' : False,
                    'SCT_COLOR_FORMAT_RGBA8888' : True }

        if rgbaMap[ self.colorFormat ]:
            rgbaTextures  = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        else:
            rgbTextures   = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        
        pbufferConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufferConfigIndex,
                       pbufferAttrs[ CONFIG_BUFFERSIZE ],
                       pbufferAttrs[ CONFIG_REDSIZE ],
                       pbufferAttrs[ CONFIG_GREENSIZE ],
                       pbufferAttrs[ CONFIG_BLUESIZE ],
                       pbufferAttrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       rgbTextures,
                       rgbaTextures,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        pbufferSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufferSurfaceIndex,
                                  pbufferConfigIndex,
                                  self.width,
                                  self.height,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )
        
        pbufferContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           pbufferContextIndex,
                           pbufferConfigIndex,
                           windowContextIndex,
                           -1 )

        Egl.MakeCurrent( displayIndex, pbufferSurfaceIndex, pbufferSurfaceIndex, pbufferContextIndex )

        # Create opengles1 objects for pbuffer surface.
        pbufferMesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0 )
        pbufferVertexDataSetup = setupVertexData( modules, indexTracker, pbufferMesh )
        useVertexData( modules, indexTracker, pbufferVertexDataSetup )

        pbufferTextureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateGradientTextureData( pbufferTextureDataIndex,
                                             'OPENGLES1_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 0.5 ],
                                             [ 1.0, 1.0, 1.0, 0.5 ],
                                             self.width, self.height,
                                             'OFF',
                                             self.textureType )

        OpenGLES1.ActiveTexture( 0 )
        
        pbufferTextureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( pbufferTextureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', pbufferTextureIndex )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.TexImage2D( pbufferTextureDataIndex, 'GL_TEXTURE_2D' )

        OpenGLES1.Enable( 'GL_TEXTURE_2D' )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )
        
        OpenGLES1.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES1.Enable( 'GL_BLEND' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        
        OpenGLES1.Viewport( 0, 0, self.width, self.height )
        OpenGLES1.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        # First, draw to pbuffer-texture.
        Egl.MakeCurrent( displayIndex, pbufferSurfaceIndex, pbufferSurfaceIndex, pbufferContextIndex )

        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', pbufferTextureIndex )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        
        for i in range( self.overdraw ):
            drawVertexData( modules, pbufferVertexDataSetup )

        # Second, draw the pbuffer-texture to window surface.
        Egl.MakeCurrent( displayIndex, windowSurfaceIndex, windowSurfaceIndex, windowContextIndex )

        Egl.BindTexImage( displayIndex, pbufferSurfaceIndex, 'EGL_BACK_BUFFER' )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        
        drawVertexData( modules, windowVertexDataSetup )
        
        Egl.SwapBuffers( displayIndex, windowSurfaceIndex )

        Egl.ReleaseTexImage( displayIndex, pbufferSurfaceIndex, 'EGL_BACK_BUFFER' )
        
