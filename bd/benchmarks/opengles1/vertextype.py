#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
FIXED        = 'fixed'
FLOAT        = 'float'

######################################################################
def createPlane( gridx, gridy, type ):
    vertexAttribute     = MeshVertexAttribute( type, 3 )
    colorAttribute      = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )    
    texCoordAttribute   = []
    indexAttribute      = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           colorAttribute,
                           None,
                           None,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class VertexTypeBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, vertexType ):
        Benchmark.__init__( self )
        self.gridx      = gridx
        self.gridy      = gridy
        self.overdraw   = overdraw
        self.vertexType = vertexType
        typemap         = { FIXED : MESH_TYPE_FIXED,
                            FLOAT : MESH_TYPE_FLOAT }
        self.ivtype     = typemap[ self.vertexType ]       
        self.name = 'OPENGLES1 vertex type %s (grd=%dx%d ovrdrw=%d)' % ( self.vertexType,
                                                                         self.gridx,
                                                                         self.gridy,
                                                                         self.overdraw, )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()
           
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, self.ivtype )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        useVertexData( modules, indexTracker, vertexDataSetup )

        OpenGLES1.ShadeModel( 'GL_SMOOTH' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawVertexData( modules, vertexDataSetup )
            offset -= delta

        target.swapBuffers( state )
