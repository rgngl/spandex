#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
NONE    = 'none'
FULLY   = 'fully'
HALF    = 'half'
    
######################################################################
def createPlane( gridx, gridy, color, texCoords, tcx, tcy, winding ):
    vertexAttribute     = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    colorAttribute      = None
    texCoordAttribute   = []
    indexAttribute      = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

    if color:
        colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

    if texCoords:
        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           colorAttribute,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           winding,
                           False )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class TransformationBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, overdraw, textureType, culling ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.culling     = culling

        if self.textureType:
            self.textured = True
            self.name = 'OPENGLES1 textured transformation (grd=%dx%d ovrdrw=%d txtr.tpe=%s cllng=%s)' % ( self.gridx,
                                                                                                           self.gridy,
                                                                                                           self.overdraw,
                                                                                                           textureTypeToStr( self.textureType ),
                                                                                                           self.culling, )
        else:
            self.textured = False            
            self.name = 'OPENGLES1 smooth transformation (grd=%dx%d ovrdrw=%d cllng=%s)' % ( self.gridx,
                                                                                             self.gridy,
                                                                                             self.overdraw,
                                                                                             self.culling, )

    def build( self, target, modules ):         
        OpenGLES1 = modules[ 'OpenGLES1' ]

        indexTracker = IndexTracker()

        if self.culling == NONE:
            winding = MESH_CCW_WINDING
        elif self.culling == FULLY:
            winding = MESH_CW_WINDING
        elif self.culling == HALF:
            winding = MESH_ALTERNATING_WINDING
        else:
            raise 'Invalid type'

        if self.textured:
            color = False
            texCoords = True
        else:
            color = True
            texCoords = False

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW, tcx = toPot( screenWidth, True )
        textureH, tcy = toPot( screenHeight, True )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL1( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, color, texCoords, tcx, tcy, winding )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        if self.textured:
            textureSetup        = setupTexture( modules,
                                                indexTracker,
                                                screenWidth,
                                                screenHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
            useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        if self.culling != NONE:        
            OpenGLES1.Enable( 'GL_CULL_FACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )           

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )      
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        offset  = 0.9
        delta   = -0.9 / float( self.overdraw )

        for i in range( self.overdraw ):
            OpenGLES1.LoadMatrix( [  2.0,     0.0,    0.0,    0.0,
                                     0.0,     2.0,    0.0,    0.0,
                                     0.0,     0.0,    1.0,    0.0,
                                    -1.0,    -1.0,    offset, 1.0 ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset += delta

        target.swapBuffers( state )
