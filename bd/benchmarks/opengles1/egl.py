#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from lib.egl       import *
from common        import *

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PrintEglInfo( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENGLES1 egl info (debug)"
        self.repeats = 1

    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        state   = setupGL1( modules, indexTracker, target, attribs )
        
        displayIndex = state[ 'displayIndex' ]
               
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.Info( displayIndex )
        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CreateContextBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENGLES1 create context"

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        state   = setupGL1( modules, indexTracker, target, attribs )
        
        displayIndex = state[ 'displayIndex' ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.DestroyContext( displayIndex,
                            contextIndex )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CreateWindowSurfaceBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENGLES1 create window surface"

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
       
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        Egl.DestroySurface( displayIndex,
                            surfaceIndex )        

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CreatePbufferSurfaceBenchmark( Benchmark ):  
    def __init__( self, format ):
        Benchmark.__init__( self )
        self.format = format       
        self.name = "OPENGLES1 create pbuffer surface frmt=%s" % ( pixmapFormatToStr( self.format ), )

    def build( self, target, modules ):
        Egl = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        state   = setupGL1( modules, indexTracker, target, attribs )
        
        displayIndex = state[ 'displayIndex' ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )

        a = mapColorFormatToAttributes( self.format )
        
        configIndex = 0
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       a[ CONFIG_BUFFERSIZE ],
                       a[ CONFIG_REDSIZE ],
                       a[ CONFIG_GREENSIZE ],
                       a[ CONFIG_BLUESIZE ],
                       a[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CreatePbufferSurface( displayIndex,
                                  surfaceIndex,
                                  configIndex,
                                  width,
                                  height,
                                  'EGL_NO_TEXTURE', 'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        Egl.DestroySurface( displayIndex,
                            surfaceIndex )        

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CreatePixmapSurfaceBenchmark( Benchmark ):  
    def __init__( self, format ):
        Benchmark.__init__( self )
        self.format = format       
        self.name = "OPENGLES1 create pixmap surface frmt=%s" % ( pixmapFormatToStr( self.format ), )

    def build( self, target, modules ):
        Egl = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        state   = setupGL1( modules, indexTracker, target, attribs )
        
        displayIndex = state[ 'displayIndex' ]
        
        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex,
                          width, height,
                          self.format,
                          -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENGL_ES_BIT' ]
                       ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex,
                             configIndex,
                             pixmapIndex,
                             attributes )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CreatePixmapSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 pixmapIndex,
                                 'EGL_NONE',
                                 'EGL_NONE' )
        
        Egl.DestroySurface( displayIndex,
                            surfaceIndex )
       
######################################################################
def createPlane( gridx, gridy, tcx, tcy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
    OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
    OpenGLES1.GenTexture( textureIndex )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE',
                            'OFF' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SwapBehaviorBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, swapBehavior ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.swapBehavior = swapBehavior
        self.name = 'OPENGLES1 swap behavior bhvr=%s (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( swapBehaviorToStr( self.swapBehavior ),
                                                                                           self.gridx,
                                                                                           self.gridy,
                                                                                           self.overdraw,
                                                                                           textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()        
        textureW, tcx = toPot( width, True )
        textureH, tcy = toPot( height, True )       
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        if self.swapBehavior != 'EGL_BUFFER_DESTROYED':
            attribs[ CONFIG_SWAP_BEHAVIOR ] = self.swapBehavior
        
        state   = setupGL1( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]
                
        if self.swapBehavior != 'EGL_BUFFER_DESTROYED':
            Egl.SurfaceAttrib( displayIndex,
                               surfaceIndex,
                               EGL_DEFINES[ 'EGL_SWAP_BEHAVIOR' ],
                               EGL_DEFINES[ self.swapBehavior ] ) 
        
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SwapIntervalBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, swapInterval ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.swapInterval = swapInterval
        self.name = 'OPENGLES1 swap interval intrvl=%d (grd=%dx%d ovdrw=%d txtr.tpe=%s)' % ( self.swapInterval,
                                                                                             self.gridx,
                                                                                             self.gridy,
                                                                                             self.overdraw,
                                                                                             textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        textureW, tcx = toPot( width, True )
        textureH, tcy = toPot( height, True )       
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES1 )
        state   = setupGL1( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]
        
        Egl.SwapInterval( displayIndex, self.swapInterval )
        
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class SurfaceScalingBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, overdraw, textureType, windowSize, surfaceSize, targetExtent ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.windowSize   = windowSize
        self.surfaceSize  = surfaceSize
        self.targetExtent = targetExtent

        windowSizeStr       = '%dx%d' % ( windowSize[ 0 ], windowSize[ 1 ], )
        if surfaceSize:
            surfaceSizeStr  = '%dx%d' % ( surfaceSize[ 0 ], surfaceSize[ 1 ], )
        if not targetExtent:
            targetExtentStr = 'full window'
        else:
            targetExtentStr = '%d %d %d %d' % ( targetExtent[ 0 ],
                                                targetExtent[ 1 ],
                                                targetExtent[ 2 ],
                                                targetExtent[ 3 ], )
            
        if not self.surfaceSize:
            self.name = "OPENGLES1 surface scaling reference wndsz=%s (grd=%dx%d ovdrw=%d txtr.tpe=%s)" % ( windowSizeStr,
                                                                                                            self.gridx,
                                                                                                            self.gridy,
                                                                                                            self.overdraw,
                                                                                                            textureTypeToStr( self.textureType ), )
        else:
            self.name = "OPENGLES1 surface scaling wndsz=%s srfcsz=%s trgtxtnt=%s (grd=%dx%d ovdrw=%d txtr.tpe=%s)" % ( windowSizeStr,
                                                                                                                        surfaceSizeStr,
                                                                                                                        targetExtentStr,
                                                                                                                        self.gridx,
                                                                                                                        self.gridy,
                                                                                                                        self.overdraw,
                                                                                                                        textureTypeToStr( self.textureType ), )
            
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        if self.surfaceSize:
            width = self.surfaceSize[ 0 ]
            height = self.surfaceSize[ 1 ]
        else:
            width = self.windowSize[ 0 ]
            height = self.windowSize[ 1 ]
        
        textureW, tcx = toPot( width, True )
        textureH, tcy = toPot( height, True )       
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.CheckExtension( displayIndex, 'EGL_NOK_surface_scaling' )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
                      
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, self.windowSize[ 0 ], self.windowSize[ 1 ], 'SCT_COLOR_FORMAT_DEFAULT' )

        surfaceBits = EGL_DEFINES[ 'EGL_WINDOW_BIT' ]
        
        gles1ConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
            
        attrs = target.getDefaultAttributes( API_OPENGLES1 )

        eglConfigAttributes = [ EGL_DEFINES[ 'EGL_BUFFER_SIZE' ],         attrs[ CONFIG_BUFFERSIZE ],
                                EGL_DEFINES[ 'EGL_RED_SIZE' ],            attrs[ CONFIG_REDSIZE ],
                                EGL_DEFINES[ 'EGL_GREEN_SIZE' ],          attrs[ CONFIG_GREENSIZE ],
                                EGL_DEFINES[ 'EGL_BLUE_SIZE' ],           attrs[ CONFIG_BLUESIZE ],
                                EGL_DEFINES[ 'EGL_ALPHA_SIZE' ],          attrs[ CONFIG_ALPHASIZE ],
                                EGL_DEFINES[ 'EGL_DEPTH_SIZE' ],          16,
                                EGL_DEFINES[ 'EGL_SURFACE_TYPE' ],        surfaceBits,
                                EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],     EGL_DEFINES[ 'EGL_OPENGL_ES_BIT' ]
                                ]

        if self.surfaceSize:
            eglConfigAttributes += [ EGL_DEFINES[ 'EGL_SURFACE_SCALING_NOK' ], EGL_DEFINES[ 'EGL_TRUE' ] ]
        
        Egl.Config( displayIndex, gles1ConfigIndex, eglConfigAttributes )

        eglWindowSurfaceAttributes = [ EGL_DEFINES[ 'EGL_RENDER_BUFFER' ], EGL_DEFINES[ 'EGL_BACK_BUFFER' ] ]
        
        if self.surfaceSize:
            eglWindowSurfaceAttributes +=   [ EGL_DEFINES[ 'EGL_FIXED_WIDTH_NOK' ], self.surfaceSize[ 0 ],
                                              EGL_DEFINES[ 'EGL_FIXED_HEIGHT_NOK' ], self.surfaceSize[ 1 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_OFFSET_X_NOK' ], self.targetExtent[ 0 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_OFFSET_Y_NOK' ], self.targetExtent[ 1 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_WIDTH_NOK' ], self.targetExtent[ 2 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_HEIGHT_NOK' ], self.targetExtent[ 3 ],
                                              EGL_DEFINES[ 'EGL_BORDER_COLOR_RED_NOK' ], 127,
                                              EGL_DEFINES[ 'EGL_BORDER_COLOR_GREEN_NOK' ], 127,
                                              EGL_DEFINES[ 'EGL_BORDER_COLOR_BLUE_NOK' ], 127
                                              ]

        gles1SurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurfaceExt( displayIndex,
                                    gles1SurfaceIndex,
                                    gles1ConfigIndex,
                                    windowIndex,
                                    eglWindowSurfaceAttributes )
            
        gles1ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           gles1ContextIndex,
                           gles1ConfigIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         gles1SurfaceIndex,
                         gles1SurfaceIndex,
                         gles1ContextIndex )

        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

        OpenGLES1.ShadeModel( 'GL_FLAT' )

        OpenGLES1.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES1.LoadIdentity()
            OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, gles1SurfaceIndex )
