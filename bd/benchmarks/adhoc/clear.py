#
# Spandex benchmark and test framework.
#
# Copyright (c) 2013 Contribute.
#
# Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ClearBenchmark( Benchmark ):  
    def __init__( self, mask, swap ):
        Benchmark.__init__( self )
        self.mask    = mask
        self.swap    = swap
        self.name = "OPENGLES2 clear mask=%s %s" % ( maskToStr( self.mask ),
                                                     { True : 'swap', False : 'finish' }[ swap ], )
        self.repeats = 240

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()
            
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )            

        if 'GL_DEPTH_BUFFER_BIT' in self.mask:
            OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        timerIndex = indexTracker.allocIndex( 'TEST_TIMER_INDEX' )
        
        for i in range( 2 ):
            Test.ProfileEvent( "START" )
            Test.SetTimer( timerIndex ) 

            if i == 0:
                OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
            else:
                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

            Test.ProfileEvent( "gfx_issue_>>" )
        
            OpenGLES2.Clear( self.mask )

            Test.ProfileEvent( "<<_gfx_issue" )
 
            if self.swap:
                target.swapBuffers( state )
            else:
                OpenGLES2.Finish()
            
            Test.LogSince( timerIndex, self.name,  self.repeats * 2 + 1 )
        
            Test.ProfileEvent( "END" ) 
 

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class FBOClearBenchmark( Benchmark ):
    def __init__( self, fboSize, swap ):
        Benchmark.__init__( self )
        self.fboSize = fboSize
        self.swap    = swap
        if not self.fboSize:
            raise "Undefined FBO size"
        self.name    = "OPENGLES2 clear through fbo (%dx%d) %s" % ( self.fboSize[ 0 ], 
                                                                    self.fboSize[ 1 ], 
                                                                    { False : 'finish', True : 'swap' }[ self.swap ], )
        self.repeats = 240

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # Draw-to-screen shaders.
        rawDrawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 gVSTexCoord0;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            gVSTexCoord0 = gTexCoord0;
        }"""

        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0 );
        }"""

        # Format selected shader strings.
        drawVertexShaderSource   = formatShader( rawDrawVertexShaderSource )
        drawFragmentShaderSource = formatShader( rawDrawFragmentShaderSource )

        # Screen size, fboSize.
        ( screenWidth, screenHeight, ) = target.getScreenSize()

        fboWidth  = self.fboSize[ 0 ]
        fboHeight = self.fboSize[ 1 ]

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        # Create draw-to-screen plane.
        planeTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]

        plane = MeshStripPlane( 2,
                                2,
                                MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                None,
                                None,
                                planeTexCoordAttribute,
                                MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                None,
                                MESH_CCW_WINDING,
                                False )

        plane.translate( [ -0.5, -0.5, 0 ] )
        plane.scale( [ 2.0, 2.0  ] )

        # Create draw-to-screen program and vertex buffer objects.
        drawVertexDataSetup = setupVertexData( modules, indexTracker, plane )
        drawProgram         = createProgram( modules,
                                             indexTracker,
                                             drawVertexShaderSource,
                                             drawFragmentShaderSource,
                                             drawVertexDataSetup )
        useProgram( modules, drawProgram )
        drawVertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, drawVertexDataSetup )
        useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
        validateProgram( modules, drawProgram )

        # Create a FBO and textures (RGBA8) used for clear.
        fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( fboTextureDataIndex, fboWidth, fboHeight, 'OFF', 'OPENGLES2_RGBA8888' )

        fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( fboTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
        OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

        framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.ClearColor( [ 1.0, 1.0, 1.0, 1.0 ] )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
        
        timerIndex = indexTracker.allocIndex( 'TEST_TIMER_INDEX' )
        for i in range( 2 ):
            Test.ProfileEvent( "START" )
            Test.SetTimer( timerIndex ) 

            Test.ProfileEvent( "gfx%d_issue_>>" % self.fboSize[ 1 ] ) 
  
            # Draw fill to FBO/texture.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
            OpenGLES2.Viewport( 0, 0, fboWidth, fboHeight )

            if i % 2 == 0:
                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
            else:
                OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
 
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

            # Draw fill FBO/texture to the screen.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
            OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

            useProgram( modules, drawProgram )
            useVertexBufferData( modules, drawVertexBufferDataSetup, drawProgram )
            useTexture( modules, fboTextureIndex, 0, drawProgram )

            drawVertexBufferData( modules, drawVertexBufferDataSetup )

            Test.ProfileEvent( "<<_gfx%d_issue" % self.fboSize[ 1 ] ) 
            Test.LogSince( timerIndex, 'Issue ' + self.name,  self.repeats * 2 + 1 )

            if self.swap:
                target.swapBuffers( state )
            else:
                OpenGLES2.Finish()
 
            Test.ProfileEvent( "END" ) 
            Test.LogSince( timerIndex, 'Frame ' + self.name,  self.repeats * 2 + 1 )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ClearFboMemoryBenchmark( Benchmark ):
    def __init__( self, fboSize, fboBytes, depthBytes ):
        Benchmark.__init__( self )
        self.fboSize     = fboSize
        self.fboBytes    = fboBytes
        self.depthBytes  = depthBytes

        fboString = '%dx%d %d color' % ( self.fboSize[ 0 ], self.fboSize[ 1 ], self.fboBytes, )
        if self.depthBytes > 0:
            fboString += ' %d depth' % self.depthBytes
        fboString += ' bytes'

        self.name = "OPENGLES2 clear fbo memory (%s)" % ( fboString, )
        self.repeats = 720

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupOpenGLES2( modules, indexTracker, target )

        # Create a FBO and textures used for clear.
        COLOR_FORMATS = { 2 : 'OPENGLES2_RGB565',
                          3 : 'OPENGLES2_RGB888',
                          4 : 'OPENGLES2_RGBA8888' }
        colorFormat = COLOR_FORMATS[ self.fboBytes ]

        fboColorTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( fboColorTextureDataIndex, self.fboSize[ 0 ], self.fboSize[ 1 ], 'OFF', colorFormat )

        fboColorTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( fboColorTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboColorTextureIndex )
        OpenGLES2.TexImage2D( fboColorTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboColorTextureIndex, 0 )

        OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )

        clearMask = [ 'GL_COLOR_BUFFER_BIT' ]
 
        if self.depthBytes:
            OpenGLES2.CheckExtension( 'GL_OES_depth_texture' )

            DEPTH_FORMATS = { 2 : 'OPENGLES2_DEPTH',
                              4 : 'OPENGLES2_DEPTH32' }

            depthFormat = DEPTH_FORMATS[ self.depthBytes ]

            fboDepthTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboDepthTextureDataIndex, self.fboSize[ 0 ], self.fboSize[ 1 ], 'OFF', depthFormat )

            fboDepthTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboDepthTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboDepthTextureIndex )
            OpenGLES2.TexImage2D( fboDepthTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_DEPTH_ATTACHMENT', 'GL_TEXTURE_2D', fboDepthTextureIndex, 0 )

            OpenGLES2.Enable( 'GL_DEPTH_TEST' )
            OpenGLES2.ClearDepth( 0.5 )
            
            clearMask += [ 'GL_DEPTH_BUFFER_BIT' ] 

        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
        
        OpenGLES2.Viewport( 0, 0, self.fboSize[ 0 ], self.fboSize[ 1 ] )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( clearMask )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        OpenGLES2.Finish()

