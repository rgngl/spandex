#
# Spandex benchmark and test framework.
#
# Copyright (c) 2013 Contribute.
#
# Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# --------------------------------------------------------------------
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ScrollerBenchmark( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.gridx       = 2
        self.gridy       = 2
        self.overdraw    = 1
        self.textureType = 'OPENGLES2_RGBA8888'
        self.name = 'OPENGLES2 scroller benchmark'
        self.repeats = 600

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        uniform float gTime;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = vec4( texture2D( gTexture0, gVSTexCoord0.xy ).rgb * step( gTime, gVSTexCoord0.x ), 1.0 );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # Screen size, fboSize.
        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        timeinc = 16.0 / screenWidth;

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state     = setupOpenGLES2( modules, indexTracker, target )

        vertexA   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        texCoordA = []
        indexA    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

        texCoordA.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )

        mesh      = MeshStripPlane( self.gridx,
                                    self.gridy,
                                    vertexA,
                                    None,
                                    None,
                                    texCoordA,
                                    indexA,
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            4, 4,
                                            screenWidth, screenHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        useTexture( modules, textureIndex, 0, program )

        timeLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( timeLocationIndex, program.programIndex, 'gTime' )

        timeValueIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( timeValueIndex, 0 )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.UniformfFromRegister( timeLocationIndex, timeValueIndex )

        for i in range( self.overdraw ):
            drawVertexData( modules, vertexDataSetup )

        target.swapBuffers( state )

        OpenGLES2.AccumulateRegister( timeValueIndex, timeinc, 1.0, 0.0 )

