#
# Spandex benchmark and test framework.
#
# Copyright (c) 2013 Contribute.
#
# Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# This benchmark is a combination of some random ad hoc benchmarks/tests.

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../../' )

import  bd

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'Ad hoc'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

# #----------------------------------------------------------------------
import clear
suite.addBenchmark( clear.ClearBenchmark( [ 'GL_COLOR_BUFFER_BIT' ], True ) )
suite.addBenchmark( clear.FBOClearBenchmark( (  480,  640, ),  True ) )
suite.addBenchmark( clear.FBOClearBenchmark( (  720, 1280, ),  True ) )
suite.addBenchmark( clear.FBOClearBenchmark( ( 1080, 1920, ),  True ) )
suite.addBenchmark( clear.FBOClearBenchmark( ( 2048, 2048, ),  True ) )
suite.addBenchmark( clear.FBOClearBenchmark( (  480,  640, ), False ) )
suite.addBenchmark( clear.FBOClearBenchmark( (  720, 1280, ), False ) )
suite.addBenchmark( clear.FBOClearBenchmark( ( 1080, 1920, ), False ) )
suite.addBenchmark( clear.FBOClearBenchmark( ( 2048, 2048, ), False ) )

suite.addBenchmark( clear.ClearFboMemoryBenchmark( ( 1080, 1920 ), 2, 0 ) )
suite.addBenchmark( clear.ClearFboMemoryBenchmark( ( 1080, 1920 ), 4, 0 ) )
suite.addBenchmark( clear.ClearFboMemoryBenchmark( ( 1080, 1920 ), 2, 2 ) )
suite.addBenchmark( clear.ClearFboMemoryBenchmark( ( 1080, 1920 ), 4, 2 ) )
suite.addBenchmark( clear.ClearFboMemoryBenchmark( ( 1080, 1920 ), 2, 4 ) )
suite.addBenchmark( clear.ClearFboMemoryBenchmark( ( 1080, 1920 ), 4, 4 ) )

#----------------------------------------------------------------------
import scroller
suite.addBenchmark( scroller.ScrollerBenchmark() )

#----------------------------------------------------------------------
command()
