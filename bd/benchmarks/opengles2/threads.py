#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
def createPlane( gridx, gridy, zero = False ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    colorAttribute    = None
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )

    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           colorAttribute,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    if not zero:
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )
    else:
        mesh.zero()

    return mesh

######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )

######################################################################
def createSharedContext( modules, indexTracker, target, displayIndex, sharedContextIndex ):
    Egl = modules[ 'Egl' ]

    pbuffer = True
    a = target.getDefaultAttributes( API_OPENGLES2, SURFACE_PBUFFER )
    if not a:
        a = target.getDefaultAttributes( API_OPENGLES2, SURFACE_PIXMAP )
        pbuffer = False

    if pbuffer:
        workerThreadConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       workerThreadConfigIndex,
                       a[ CONFIG_BUFFERSIZE ],
                       a[ CONFIG_REDSIZE ],
                       a[ CONFIG_GREENSIZE ],
                       a[ CONFIG_BLUESIZE ],
                       a[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        workerThreadSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  workerThreadSurfaceIndex,
                                  workerThreadConfigIndex,
                                  1, 1,
                                  'EGL_NO_TEXTURE',
                                  'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )
    else:
        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex,
                          1,
                          1,
                          'SCT_COLOR_FORMAT_RGBA8888',
                          -1 )

        workerThreadConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       workerThreadConfigIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PIXMAP_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        workerThreadSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurfaceExt( displayIndex,
                                    workerThreadSurfaceIndex,
                                    workerThreadConfigIndex,
                                    pixmapIndex,
                                    [] )

    workerThreadContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
    Egl.CreateContext( displayIndex,
                       workerThreadContextIndex,
                       workerThreadConfigIndex,
                       sharedContextIndex,
                       2 )

    return ( workerThreadSurfaceIndex, workerThreadContextIndex, )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ParallelWindowsBenchmark( Benchmark ):
    def __init__( self, windows, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.windows          = windows
        self.gridx            = gridx
        self.gridy            = gridy
        self.overdraw         = overdraw
        self.textureType      = textureType
        self.repeats          = 0
        self.repeatsPerThread = 256
        self.name = 'OPENGLES2 parallel windows %d grd=%dx%dx%d tex=%s rpts=%d' % ( self.windows,
                                                                                    self.gridx,
                                                                                    self.gridy,
                                                                                    self.overdraw,
                                                                                    textureTypeToStr( self.textureType ),
                                                                                    self.repeatsPerThread, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW  = screenWidth
        textureH  = screenHeight

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        groupIndex = indexTracker.allocIndex( 'THREAD_GROUP_INDEX' )

        for i in range( self.windows ):
            # ------------------------------------------------------------
            self.beginInitActions( i )

            if i == 0:
                Thread.GroupSize( groupIndex, self.windows )

            displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
            Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
            Egl.Initialize( displayIndex )

            windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
            Egl.CreateWindow( windowIndex,
                              'SCT_SCREEN_DEFAULT',
                              0,
                              screenHeight / self.windows * i,
                              screenWidth,
                              screenHeight / self.windows,
                              'SCT_COLOR_FORMAT_DEFAULT' )

            configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
            Egl.RGBConfig( displayIndex,
                           configIndex,
                           attrs[ CONFIG_BUFFERSIZE ],
                           attrs[ CONFIG_REDSIZE ],
                           attrs[ CONFIG_GREENSIZE ],
                           attrs[ CONFIG_BLUESIZE ],
                           attrs[ CONFIG_ALPHASIZE ],
                           '>=16',
                           '-',
                           0,
                           '>=0',
                           '-',
                           'EGL_DONT_CARE',
                           'EGL_DONT_CARE',
                           [ 'EGL_WINDOW_BIT' ],
                           [ 'EGL_OPENGL_ES2_BIT' ] )

            surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
            Egl.CreateWindowSurface( displayIndex,
                                     surfaceIndex,
                                     configIndex,
                                     windowIndex,
                                     'EGL_BACK_BUFFER',
                                     'EGL_NONE',
                                     'EGL_NONE' )

            contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
            Egl.CreateContext( displayIndex,
                               contextIndex,
                               configIndex,
                               -1,
                               2 )

            Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
            Egl.SwapInterval( displayIndex, 0 )

            mesh                    = createPlane( self.gridx, self.gridy )
            vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
            program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
            meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
            useMeshData( modules, indexTracker, meshDataSetup, program )

            r = ( i + 1 ) / float( self.windows )
            textureSetup            = setupTexture( modules,
                                                    indexTracker,
                                                    textureW, textureH,
                                                    [ r, 0.0, 0.0, 0.7 ],
                                                    [ 0.0, r, 0.0, 0.7 ],
                                                    self.textureType )

            useTexture( modules, textureSetup, 0, program )

            offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

            OpenGLES2.Enable( 'GL_BLEND' )
            OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
            OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

            OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

            OpenGLES2.Enable( 'GL_DEPTH_TEST' )
            OpenGLES2.DepthFunc( 'GL_LEQUAL' )
            OpenGLES2.CheckError( '' )

            # ------------------------------------------------------------
            self.beginBenchmarkActions( i )

            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

            delta   = 1.0 / float( self.overdraw )
            offset  = 1.0

            for i in range( self.overdraw ):
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawMeshData( modules, meshDataSetup )
                offset -= delta

            Egl.SwapBuffers( displayIndex, surfaceIndex )

            signalIndex = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
            Thread.GroupCompleteBenchmark( self.repeatsPerThread, groupIndex, signalIndex )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SwapReferenceBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 single thread swap reference grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                                     self.gridy,
                                                                                     self.overdraw,
                                                                                     textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        textureW  = screenWidth
        textureH  = screenHeight

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )

        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          0,
                          0,
                          screenWidth,
                          screenHeight,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 0.7 ],
                                                [ 0.0, 1.0, 0.0, 0.7 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CreateWindowAndSwapFromMainDrawFromWorkerBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 create.wndw.n.swp.frm.main.thrd.drw.frm.wrker.thrd grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                                                           self.gridy,
                                                                                                           self.overdraw,
                                                                                                           textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        textureW  = screenWidth
        textureH  = screenHeight

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )

        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          0,
                          0,
                          screenWidth,
                          screenHeight,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        Egl.SwapInterval( displayIndex, 0 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginInitActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 0.7 ],
                                                [ 0.0, 1.0, 0.0, 0.7 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        # Swap from the main thread
        Thread.Wait( mainThreadSignal, 0 )

        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        Egl.SwapBuffers( displayIndex, surfaceIndex )
        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        Thread.Signal( workerThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        # Draw from the worker thread
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        Thread.Signal( mainThreadSignal )
        Thread.Wait( workerThreadSignal, 0 );

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 );

        # ------------------------------------------------------------
        self.beginPreTerminateActions( 1 )

        Thread.Wait( workerThreadSignal, 0 );
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Signal( mainThreadSignal );

        Egl.ReleaseThread()


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DrawFromMainCreateWindowAndSwapFromWorkerBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 draw.frm.main.thrd.create.wndw.n.swp.from.wrkr.thrd grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                                                            self.gridy,
                                                                                                            self.overdraw,
                                                                                                            textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        windowIndex  = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        configIndex  = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        textureW  = screenWidth
        textureH  = screenHeight

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )

        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 0.0, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        # ------------------------------------------------------------
        self.beginInitActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )

        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.InitializeExt( displayIndex )

        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          0,
                          0,
                          screenWidth,
                          screenHeight,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        Egl.SwapInterval( displayIndex, 0 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        # Draw from the main thread
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 );

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        # Swap from the worker thread
        Thread.Wait( workerThreadSignal, 0 )

        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        Egl.SwapBuffers( displayIndex, surfaceIndex )
        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Wait( mainThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        # ------------------------------------------------------------
        self.beginTerminateActions()

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Signal( workerThreadSignal )
        Egl.ReleaseThread()

        # ------------------------------------------------------------
        self.beginPreTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Signal( mainThreadSignal )
        Thread.Wait( workerThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.Terminate( displayIndex )
        Egl.ReleaseThread()


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CreateBufferAndDrawMainReadWorkerBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 create.bffr.n.drw.frm.main.thrd.rd.frm.wrkr.thrd grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                                                         self.gridy,
                                                                                                         self.overdraw,
                                                                                                         textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        textureW  = screenWidth
        textureH  = screenHeight

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )

        pbuffer = True
        a = target.getDefaultAttributes( API_OPENGLES2, SURFACE_PBUFFER )
        if not a:
            a = target.getDefaultAttributes( API_OPENGLES2, SURFACE_PIXMAP )
            pbuffer = False

        if pbuffer:
            configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
            Egl.RGBConfig( displayIndex,
                           configIndex,
                           a[ CONFIG_BUFFERSIZE ],
                           a[ CONFIG_REDSIZE ],
                           a[ CONFIG_GREENSIZE ],
                           a[ CONFIG_BLUESIZE ],
                           a[ CONFIG_ALPHASIZE ],
                           '>=16',
                           '-',
                           0,
                           '>=0',
                           '-',
                           'EGL_DONT_CARE',
                           'EGL_DONT_CARE',
                           [ 'EGL_PBUFFER_BIT' ],
                           [ 'EGL_OPENGL_ES2_BIT' ] )

            surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
            Egl.CreatePbufferSurface( displayIndex,
                                      surfaceIndex,
                                      configIndex,
                                      screenWidth, screenHeight,
                                      'EGL_NO_TEXTURE',
                                      'EGL_NO_TEXTURE',
                                      'OFF',
                                      'OFF',
                                      'EGL_NONE',
                                      'EGL_NONE' )
        else:
            pixmapFormats = { 16 : 'SCT_COLOR_FORMAT_RGB565',
                              24 : 'SCT_COLOR_FORMAT_RGB888',
                              32 : 'SCT_COLOR_FORMAT_RGBA8888' }

            pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
            Egl.CreatePixmap( pixmapIndex,
                              screenWidth, screenHeight,
                              pixmapFormats[ a[ CONFIG_BUFFERSIZE ] ],
                              -1 )

            configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
            Egl.RGBConfig( displayIndex,
                           configIndex,
                           a[ CONFIG_BUFFERSIZE ],
                           a[ CONFIG_REDSIZE ],
                           a[ CONFIG_GREENSIZE ],
                           a[ CONFIG_BLUESIZE ],
                           a[ CONFIG_ALPHASIZE ],
                           '>=16',
                           '-',
                           0,
                           '>=0',
                           '-',
                           'EGL_DONT_CARE',
                           'EGL_DONT_CARE',
                           [ 'EGL_PIXMAP_BIT' ],
                           [ 'EGL_OPENGL_ES2_BIT' ] )

            surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
            Egl.CreatePixmapSurfaceExt( displayIndex,
                                        surfaceIndex,
                                        configIndex,
                                        pixmapIndex,
                                        [] )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 0.7 ],
                                                [ 0.0, 1.0, 0.0, 0.7 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 );

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        rpDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.ReadPixels( rpDataIndex, 0, 0, screenWidth, screenHeight )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.ReleaseThread()
        Thread.Signal( mainThreadSignal )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
# Does not work in Symbian as RWindow is thread-specific
class CreateWindowAndDrawMainSwapWorkerBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 create.wndw.n.drw.frm.main.thrd.swp.frm.wrkr.thrd grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                                                          self.gridy,
                                                                                                          self.overdraw,
                                                                                                          textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        textureW  = screenWidth
        textureH  = screenHeight

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )

        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          0,
                          0,
                          screenWidth,
                          screenHeight,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.0, 0.0, 0.7 ],
                                                [ 0.0, 1.0, 0.0, 0.7 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 );

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )

        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        Egl.SwapBuffers( displayIndex, surfaceIndex )
        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        # Ensure main thread has active context during termination.
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        # ------------------------------------------------------------
        self.beginTerminateActions(1)

        Egl.ReleaseThread()


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class TextureUploadReferenceBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureWidth, textureHeight, textureType ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType
        self.name = 'OPENGLES2 single.thrd.txtr.upld.ref %dx%d %s grd=%dx%dx%d' % ( self.textureWidth,
                                                                                    self.textureHeight,
                                                                                    textureTypeToStr( self.textureType ),
                                                                                    self.gridx,
                                                                                    self.gridy,
                                                                                    self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.7 ],
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ThreadedTextureUploadBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureWidth, textureHeight, textureType ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType
        self.name = 'OPENGLES2 threaded.txtr.upld %dx%d %s grd=%dx%dx%d' % ( self.textureWidth,
                                                                             self.textureHeight,
                                                                             textureTypeToStr( self.textureType ),
                                                                             self.gridx,
                                                                             self.gridy,
                                                                             self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.7 ],
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        Thread.Wait( mainThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Signal( workerThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Signal( mainThreadSignal )
        Thread.Wait( workerThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        # ------------------------------------------------------------
        self.beginPreTerminateActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.ReleaseThread()
        Thread.Signal( mainThreadSignal )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ThreadedSharedTextureUploadBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureWidth, textureHeight, textureType ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType
        self.name = 'OPENGLES2 threaded.shrd.txtr.upld %dx%d %s grd=%dx%dx%d' % ( self.textureWidth,
                                                                                  self.textureHeight,
                                                                                  textureTypeToStr( self.textureType ),
                                                                                  self.gridx,
                                                                                  self.gridy,
                                                                                  self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Main thread init actions
        self.beginInitActions()

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.7 ],
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.DeleteProgram( program.programIndex )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( 'Main thread init' )

        workerThreadSurfaceIndex, workerThreadContextIndex = createSharedContext( modules, indexTracker, target, displayIndex, contextIndex )

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginInitActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, workerThreadSurfaceIndex, workerThreadSurfaceIndex, workerThreadContextIndex )

        uploadTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( uploadTextureDataIndex,
                                            [ 1.0, 1.0, 1.0, 0.7 ],
                                            [ 0.0, 0.0, 0.0, 0.7 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        Thread.Wait( mainThreadSignal, 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )
        Thread.Signal( workerThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( uploadTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.Finish()
        Thread.Signal( mainThreadSignal )
        Thread.Wait( workerThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.ReleaseThread()
        Thread.Signal( mainThreadSignal )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ThreadedAsyncSharedTextureUploadBenchmark( Benchmark ):
    def __init__( self, gx, gy, overdraw, tt, utw, uth, utt, utgx, utgy, sync ):
        Benchmark.__init__( self )
        self.gridx               = gx
        self.gridy               = gy
        self.overdraw            = overdraw
        self.textureType         = tt
        self.uploadTextureWidth  = utw
        self.uploadTextureHeight = uth
        self.uploadTextureType   = utt
        self.uploadTextureGridX  = utgx
        self.uploadTextureGridY  = utgy
        self.sync                = sync
        self.name = 'OPENGLES2 threaded.asnc.shrd.txtr.upld %dx%d %s div=%dx%d grd=%dx%dx%d tex=%s%s' % ( self.uploadTextureWidth,
                                                                                                          self.uploadTextureHeight,
                                                                                                          textureTypeToStr( self.uploadTextureType ),
                                                                                                          self.uploadTextureGridX,
                                                                                                          self.uploadTextureGridX,
                                                                                                          self.gridx,
                                                                                                          self.gridy,
                                                                                                          self.overdraw,
                                                                                                          textureTypeToStr( self.textureType ),
                                                                                                          { True : ' synchrnzd', False : '' }[ self.sync ], )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Main thread init actions
        self.beginInitActions()

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        if self.sync:
            Egl.SwapInterval( displayIndex, 1 )
        else:
            Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.7 ],
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            4, 4,
                                            screenWidth, screenHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.DeleteProgram( program.programIndex )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( 'Main thread init' )

        workerThreadSurfaceIndex, workerThreadContextIndex = createSharedContext( modules, indexTracker, target, displayIndex, contextIndex )

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginInitActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, workerThreadSurfaceIndex, workerThreadSurfaceIndex, workerThreadContextIndex )

        uploadTextureBaseDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( uploadTextureBaseDataIndex,
                                            [ 1.0, 1.0, 1.0, 0.7 ],
                                            [ 0.0, 0.0, 0.0, 0.7 ],
                                            4, 4,
                                            self.uploadTextureWidth, self.uploadTextureHeight,
                                            'OFF',
                                            self.uploadTextureType )

        uploadTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.GenTexture( uploadTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uploadTextureIndex )
        OpenGLES2.TexImage2D( uploadTextureBaseDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        uploadTextureDataIndices   = []
        uploadTextureDataLocations = []
        uploadTextureDataWidth     = self.uploadTextureWidth / self.uploadTextureGridX
        uploadTextureDataHeight    = self.uploadTextureWidth / self.uploadTextureGridY

        for y in range( self.uploadTextureGridY ):
            for x in range( self.uploadTextureGridX ):
                location = [ x * uploadTextureDataWidth, y * uploadTextureDataHeight ]
                r = ( y * self.uploadTextureGridY + x ) / float( self.uploadTextureGridX * self.uploadTextureGridY )
                uploadTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateCheckerTextureData( uploadTextureDataIndex,
                                                    [ r, 0.0, 1.0, 0.7 ],
                                                    [ 0.0, r, 1.0, 0.7 ],
                                                    2, 2,
                                                    uploadTextureDataWidth, uploadTextureDataHeight,
                                                    'OFF',
                                                    self.uploadTextureType )

                uploadTextureDataIndices.append( uploadTextureDataIndex )
                uploadTextureDataLocations.append( location )

        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uploadTextureIndex )

        for i in range( len( uploadTextureDataIndices ) ):
            l = uploadTextureDataLocations[ i ]
            OpenGLES2.TexSubImage2D( uploadTextureDataIndices[ i ], 'GL_TEXTURE_2D', l[ 0 ], l[ 1 ] )
        OpenGLES2.Finish()

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.ReleaseThread()
        Thread.Signal( mainThreadSignal )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RenderToTextureReferenceBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureWidth, textureHeight, textureType ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType
        self.name = 'OPENGLES2 single.thrd.rndr.to.txtr.ref %dx%d %s grd=%dx%dx%d' % ( self.textureWidth,
                                                                                       self.textureHeight,
                                                                                       textureTypeToStr( self.textureType ),
                                                                                       self.gridx,
                                                                                       self.gridy,
                                                                                       self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        workerThreadContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           workerThreadContextIndex,
                           configIndex,
                           contextIndex,
                           2 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.5 ],
                                            [ 0.0, 1.0, 0.0, 0.5 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        renderTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( renderTextureDataIndex,
                                         self.textureWidth,
                                         self.textureHeight,
                                         'OFF',
                                         self.textureType )

        renderTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( renderTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', renderTextureIndex )
        OpenGLES2.TexImage2D( renderTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

        framebufferIndex        = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                        'GL_COLOR_ATTACHMENT0',
                                        'GL_TEXTURE_2D',
                                        renderTextureIndex,
                                        0 )

        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
        OpenGLES2.Viewport( 0, 0, self.textureWidth, self.textureHeight )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 1.0 ] )
        drawMeshData( modules, meshDataSetup )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', renderTextureIndex )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ThreadedRenderToTextureBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureWidth, textureHeight, textureType ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType
        self.name = 'OPENGLES2 threaded.rndr.to.txtr %dx%d %s grd=%dx%dx%d' % ( self.textureWidth,
                                                                                self.textureHeight,
                                                                                textureTypeToStr( self.textureType ),
                                                                                self.gridx,
                                                                                self.gridy,
                                                                                self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        workerThreadContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           workerThreadContextIndex,
                           configIndex,
                           contextIndex,
                           2 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.5 ],
                                            [ 0.0, 1.0, 0.0, 0.5 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        renderTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( renderTextureDataIndex,
                                         self.textureWidth,
                                         self.textureHeight,
                                         'OFF',
                                         self.textureType )

        renderTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( renderTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', renderTextureIndex )
        OpenGLES2.TexImage2D( renderTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

        framebufferIndex        = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
        OpenGLES2.Viewport( 0, 0, self.textureWidth, self.textureHeight )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                        'GL_COLOR_ATTACHMENT0',
                                        'GL_TEXTURE_2D',
                                        renderTextureIndex,
                                        0 )

        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] );

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        Thread.Wait( mainThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', renderTextureIndex )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Signal( workerThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
        OpenGLES2.Viewport( 0, 0, self.textureWidth, self.textureHeight )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 1.0 ] )
        drawMeshData( modules, meshDataSetup )

        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Thread.Signal( mainThreadSignal )
        Thread.Wait( workerThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Egl.ReleaseThread()


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ThreadedRenderToSharedTextureBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureWidth, textureHeight, textureType ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType
        self.name = 'OPENGLES2 threaded.rndr.to.shrd.txtr grd=%dx%dx%d tex=%dx%d %s' % ( self.gridx,
                                                                                         self.gridy,
                                                                                         self.overdraw,
                                                                                         self.textureWidth,
                                                                                         self.textureHeight,
                                                                                         textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        # FBO texture
        renderTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( renderTextureDataIndex,
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            [ 0.0, 0.0, 1.0, 0.7 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        OpenGLES2.ActiveTexture( 0 )
        renderTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( renderTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', renderTextureIndex )
        OpenGLES2.TexImage2D( renderTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Main thread init' )

        # Create worker thread (shared) context
        workerThreadSurfaceIndex, workerThreadContextIndex = createSharedContext( modules, indexTracker, target, displayIndex, contextIndex )

        # Signal worker thread to do its initialization
        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginInitActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )

        Egl.MakeCurrentExt( displayIndex, workerThreadSurfaceIndex, workerThreadSurfaceIndex, workerThreadContextIndex )

        vertexDataSetup2        = setupVertexData( modules, indexTracker, mesh )
        program2                = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup2 )
        meshDataSetup2          = setupMeshData( modules, indexTracker, vertexDataSetup2 )
        useMeshData( modules, indexTracker, meshDataSetup2, program2 )

        # Texture used for rendering into FBO texture
        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.7 ],
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Uniformi( program2.textureLocationIndices[ 0 ], 1, [ 0 ] )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

        framebufferIndex        = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        # Use FBO texture from main thread
        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                        'GL_COLOR_ATTACHMENT0',
                                        'GL_TEXTURE_2D',
                                        renderTextureIndex,
                                        0 )

        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )

        workerOffsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( workerOffsetLocationIndex, program2.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )

        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Worker thread init' )

        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        # Main thread: draw full screen mesh with the FBO texture.
        Thread.Wait( mainThreadSignal, 0 )

        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', renderTextureIndex )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )
        Thread.Signal( workerThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        # Worker thread: draw full screen mesh into FBO
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
        OpenGLES2.Viewport( 0, 0, self.textureWidth, self.textureHeight )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        OpenGLES2.Uniformf( workerOffsetLocationIndex, 1, [ 1.0 ] )
        drawMeshData( modules, meshDataSetup2 )
        OpenGLES2.Finish()

        Thread.Signal( mainThreadSignal )
        Thread.Wait( workerThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        # Delete program here to avoid crash in some engine
        OpenGLES2.DeleteProgram( program.programIndex )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginPreTerminateActions( 1 )

        # Delete program here to avoid crash in some engine
        OpenGLES2.DeleteProgram( program2.programIndex )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.ReleaseThread( )
        Thread.Signal( mainThreadSignal )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ThreadedSharedVBODataUploadBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 threaded.shrd.vbo.data.upld grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                                    self.gridy,
                                                                                    self.overdraw,
                                                                                    textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Main thread init actions
        self.beginInitActions()

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        nullMesh                = createPlane( self.gridx, self.gridy, True )
        nullVertexDataSetup     = setupVertexData( modules, indexTracker, nullMesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, nullVertexDataSetup )
        nullMeshDataSetup       = setupMeshData( modules, indexTracker, nullVertexDataSetup )
        nullMeshBufferDataSetup = setupMeshBufferData( modules, indexTracker, nullMeshDataSetup )
        useMeshBufferData( modules, indexTracker, nullMeshBufferDataSetup, program )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.7 ],
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            4, 4,
                                            screenWidth, screenHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.CheckError( 'Main thread init' )

        workerThreadSurfaceIndex, workerThreadContextIndex = createSharedContext( modules, indexTracker, target, displayIndex, contextIndex )

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginInitActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, workerThreadSurfaceIndex, workerThreadSurfaceIndex, workerThreadContextIndex )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )

        OpenGLES2.CheckError( 'Worker thread init' )

        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        Thread.Wait( mainThreadSignal, 0 )

        # Bind buffers, this should use the new data uploaded in the worker thread.
        OpenGLES2.BindBuffer( 'GL_ARRAY_BUFFER', nullMeshBufferDataSetup.meshBufferIndex )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            OpenGLES2.BufferDrawElements( nullMeshBufferDataSetup.indexBufferIndex,
                                          'ON',
                                          nullMeshBufferDataSetup.glMode,
                                          nullMeshBufferDataSetup.indexArrayLength,
                                          0 )

            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        # Set buffer data back to all zeros
        OpenGLES2.MeshBufferData( nullMeshBufferDataSetup.meshBufferIndex,
                                  'ON',
                                  nullMeshDataSetup.meshIndex,
                                  'GL_STATIC_DRAW' )

        OpenGLES2.BufferData( nullMeshBufferDataSetup.indexBufferIndex,
                              'ON',
                              'GL_ELEMENT_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              nullMeshDataSetup.indexArrayIndex,
                              0,
                              0 )
        OpenGLES2.Finish()

        Thread.Signal( workerThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        OpenGLES2.MeshBufferData( nullMeshBufferDataSetup.meshBufferIndex,
                                  'ON',
                                  meshDataSetup.meshIndex,
                                  'GL_STATIC_DRAW' )

        OpenGLES2.BufferData( nullMeshBufferDataSetup.indexBufferIndex,
                              'ON',
                              'GL_ELEMENT_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              meshDataSetup.indexArrayIndex,
                              0,
                              0 )
        OpenGLES2.Finish()

        Thread.Signal( mainThreadSignal )
        Thread.Wait( workerThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        OpenGLES2.DeleteProgram( program.programIndex )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.ReleaseThread()
        Thread.Signal( mainThreadSignal )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ThreadedSharedProgramBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 threaded.shrd.prgrm grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                            self.gridy,
                                                                            self.overdraw,
                                                                            textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawDummyVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x / 2.0, gVertex.y / 2.0, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawDummyFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
           gl_FragColor = gl_FragColor.bgra;
        }"""

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        dummyVertexShaderSource   = formatShader( rawDummyVertexShaderSource )
        dummyFragmentShaderSource = formatShader( rawDummyFragmentShaderSource )

        vertexShaderSource        = formatShader( rawVertexShaderSource )
        fragmentShaderSource      = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Main thread init actions
        self.beginInitActions()

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, dummyVertexShaderSource, dummyFragmentShaderSource, vertexDataSetup, False )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.7 ],
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            4, 4,
                                            screenWidth, screenHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.CheckError( 'Main thread init' )

        workerThreadSurfaceIndex, workerThreadContextIndex = createSharedContext( modules, indexTracker, target, displayIndex, contextIndex )

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginInitActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, workerThreadSurfaceIndex, workerThreadSurfaceIndex, workerThreadContextIndex )

        workerThreadvsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        workerThreadfsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )

        OpenGLES2.SetData( workerThreadvsDataIndex, vertexShaderSource )
        OpenGLES2.SetData( workerThreadfsDataIndex, fragmentShaderSource )

        workerThreadvsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', workerThreadvsIndex )
        OpenGLES2.ShaderSource( workerThreadvsIndex, workerThreadvsDataIndex )
        OpenGLES2.CompileShader( workerThreadvsIndex )

        workerThreadfsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', workerThreadfsIndex )
        OpenGLES2.ShaderSource( workerThreadfsIndex, workerThreadfsDataIndex )
        OpenGLES2.CompileShader( workerThreadfsIndex )

        OpenGLES2.CheckError( 'Worker thread init' )

        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        Thread.Wait( mainThreadSignal, 0 )

        # Use program so it should reflect the changes done in the worker thread
        OpenGLES2.UseProgram( program.programIndex )

        textureLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, program.programIndex, 'gTexture0' )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        # Set program to use dummy shaders
        OpenGLES2.DetachShader( program.programIndex, workerThreadvsIndex )
        OpenGLES2.AttachShader( program.programIndex, program.vertexShaderIndex )
        OpenGLES2.DetachShader( program.programIndex, workerThreadfsIndex )
        OpenGLES2.AttachShader( program.programIndex, program.fragmentShaderIndex )
        OpenGLES2.LinkProgram( program.programIndex )
        OpenGLES2.ValidateProgram( program.programIndex )
        OpenGLES2.Finish()

        Thread.Signal( workerThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        # Set program to use proper shaders and relink.
        OpenGLES2.DetachShader( program.programIndex, program.vertexShaderIndex )
        OpenGLES2.AttachShader( program.programIndex, workerThreadvsIndex )
        OpenGLES2.DetachShader( program.programIndex, program.fragmentShaderIndex )
        OpenGLES2.AttachShader( program.programIndex, workerThreadfsIndex )
        OpenGLES2.LinkProgram( program.programIndex )
        OpenGLES2.ValidateProgram( program.programIndex )
        OpenGLES2.Finish()

        Thread.Signal( mainThreadSignal )
        Thread.Wait( workerThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        OpenGLES2.DeleteProgram( program.programIndex )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.ReleaseThread()
        Thread.Signal( mainThreadSignal )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ThreadedSharedShaderBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureType   = textureType
        self.name = 'OPENGLES2 threaded.shrd.shader grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                             self.gridy,
                                                                             self.overdraw,
                                                                             textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawDummyVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x / 2.0, gVertex.y / 2.0, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawDummyFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
           gl_FragColor = gl_FragColor.bgra;
        }"""

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        dummyVertexShaderSource   = formatShader( rawDummyVertexShaderSource )
        dummyFragmentShaderSource = formatShader( rawDummyFragmentShaderSource )

        vertexShaderSource        = formatShader( rawVertexShaderSource )
        fragmentShaderSource      = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Main thread init actions
        self.beginInitActions()

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, dummyVertexShaderSource, dummyFragmentShaderSource, vertexDataSetup, False )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.7 ],
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            4, 4,
                                            screenWidth, screenHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.CheckError( 'Main thread init' )

        workerThreadSurfaceIndex, workerThreadContextIndex = createSharedContext( modules, indexTracker, target, displayIndex, contextIndex )

        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginInitActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )
        Egl.MakeCurrentExt( displayIndex, workerThreadSurfaceIndex, workerThreadSurfaceIndex, workerThreadContextIndex )

        workerThreadvsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        workerThreadfsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )

        OpenGLES2.SetData( workerThreadvsDataIndex, vertexShaderSource )
        OpenGLES2.SetData( workerThreadfsDataIndex, fragmentShaderSource )

        OpenGLES2.CheckError( 'Worker thread init' )

        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        Thread.Wait( mainThreadSignal, 0 )

        # Use program so it should reflect the changes done in the worker thread
        OpenGLES2.UseProgram( program.programIndex )
        OpenGLES2.LinkProgram( program.programIndex )
        OpenGLES2.ValidateProgram( program.programIndex )

        textureLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, program.programIndex, 'gTexture0' )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        Thread.Signal( workerThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        OpenGLES2.ShaderSource( program.vertexShaderIndex, workerThreadvsDataIndex )
        OpenGLES2.CompileShader( program.vertexShaderIndex )

        OpenGLES2.ShaderSource( program.fragmentShaderIndex, workerThreadfsDataIndex )
        OpenGLES2.CompileShader( program.fragmentShaderIndex )
        OpenGLES2.Finish()

        Thread.Signal( mainThreadSignal )
        Thread.Wait( workerThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        OpenGLES2.DeleteProgram( program.programIndex )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.ReleaseThread()
        Thread.Signal( mainThreadSignal )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ThreadedSharedRenderbufferBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 threaded.shrd.renderbuffer grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                                   self.gridy,
                                                                                   self.overdraw,
                                                                                   textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        mtDefaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( mtDefaultFramebufferIndex )

        # Create a main thread FBO
        mtFramebufferIndex        = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( mtFramebufferIndex )

        # Create a shared renderbuffer
        renderbufferIndex       = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
        OpenGLES2.GenRenderbuffer( renderbufferIndex )
        OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                    renderbufferIndex )
        OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                       'GL_RGB565',
                                       screenWidth, screenHeight )
        OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                    -1 )

        # Create pixel and texture data
        mtDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.CreateData( mtDataIndex,
                              screenWidth * screenHeight * 4 )

        OpenGLES2.ActiveTexture( 0 )
        mtTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( mtTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', mtTextureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        #OpenGLES2.DeleteProgram( program.programIndex )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Main thread init' )

        # Create worker thread (shared) context
        workerThreadSurfaceIndex, workerThreadContextIndex = createSharedContext( modules, indexTracker, target, displayIndex, contextIndex )

        # Signal worker thread to do its initialization
        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginInitActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )

        Egl.MakeCurrentExt( displayIndex, workerThreadSurfaceIndex, workerThreadSurfaceIndex, workerThreadContextIndex )

        vertexDataSetup2        = setupVertexData( modules, indexTracker, mesh )
        program2                = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup2 )
        meshDataSetup2          = setupMeshData( modules, indexTracker, vertexDataSetup2 )
        useMeshData( modules, indexTracker, meshDataSetup2, program2 )

        wtDefaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( wtDefaultFramebufferIndex )

        # Create a worker thread FBO
        wtFramebufferIndex        = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( wtFramebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', wtFramebufferIndex )

        wtTextureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( wtTextureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.7 ],
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            4, 4,
                                            screenWidth, screenHeight,
                                            'OFF',
                                            self.textureType )

        wtTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.GenTexture( wtTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', wtTextureIndex )
        OpenGLES2.TexImage2D( wtTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Uniformi( program2.textureLocationIndices[ 0 ], 1, [ 0 ] )

        workerOffsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( workerOffsetLocationIndex, program2.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Worker thread init' )

        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        # Main thread: read pixels from renderbuffer, pload them as texture data
        # and use for drawing.
        Thread.Wait( mainThreadSignal, 0 )

        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', mtFramebufferIndex )
        OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                           'GL_COLOR_ATTACHMENT0',
                                           'GL_RENDERBUFFER',
                                           renderbufferIndex )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.ReadPixels( mtDataIndex,
                              0, 0,
                              screenWidth, screenHeight )

        OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                           'GL_COLOR_ATTACHMENT0',
                                           'GL_RENDERBUFFER',
                                           -1 )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', mtDefaultFramebufferIndex )

        mtTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.CreateTextureData( mtTextureDataIndex,
                                     mtDataIndex,
                                     screenWidth, screenHeight,
                                     'OPENGLES2_RGBA8888' )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', mtTextureIndex )
        OpenGLES2.TexImage2D( mtTextureDataIndex, 'GL_TEXTURE_2D' )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        OpenGLES2.CheckError( 'Main thread draw' )
        Egl.SwapBuffers( displayIndex, surfaceIndex )
        Thread.Signal( workerThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        # Worker thread: draw full screen mesh into FBO, and into a shared
        # renderbuffer.
        OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                           'GL_COLOR_ATTACHMENT0',
                                           'GL_RENDERBUFFER',
                                           renderbufferIndex )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        OpenGLES2.Uniformf( workerOffsetLocationIndex, 1, [ 1.0 ] )
        drawMeshData( modules, meshDataSetup2 )
        OpenGLES2.Finish()

        OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                           'GL_COLOR_ATTACHMENT0',
                                           'GL_RENDERBUFFER',
                                           -1 )

        OpenGLES2.CheckError( 'Worker thread draw' )
        Thread.Signal( mainThreadSignal )
        Thread.Wait( workerThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        # Delete program here to avoid crash in some engine
        OpenGLES2.DeleteProgram( program.programIndex )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginPreTerminateActions( 1 )

        # Delete program here to avoid crash in some engine
        OpenGLES2.DeleteProgram( program2.programIndex )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.ReleaseThread( )
        Thread.Signal( mainThreadSignal )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ThreadedAsyncRenderToTextureBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, bgGridx, bgGridy, bgOverdraw, fboTextureType, sync  ):
        Benchmark.__init__( self )
        self.gridx          = gridx
        self.gridy          = gridy
        self.overdraw       = overdraw
        self.textureType    = textureType
        self.bgGridx        = bgGridx
        self.bgGridy        = bgGridy
        self.bgOverdraw     = bgOverdraw
        self.fboTextureType = fboTextureType
        self.sync           = sync
        self.name = 'OPENGLES2 threaded.async.rndr.to.txtr fbotex=%s bggrd=%dx%dx%d grd=%dx%dx%d tex=%s%s' % ( textureTypeToStr( self.fboTextureType ),
                                                                                                               self.bgGridx,
                                                                                                               self.bgGridy,
                                                                                                               self.bgOverdraw,
                                                                                                               self.gridx,
                                                                                                               self.gridy,
                                                                                                               self.overdraw,
                                                                                                               textureTypeToStr( self.textureType ),
                                                                                                               { True : ' synchrnzd', False : '' }[ self.sync ], )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        mainThreadSignal   = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        workerThreadSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        if self.sync:
            Egl.SwapInterval( displayIndex, 1 )
        else:
            Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        useMeshData( modules, indexTracker, meshDataSetup, program )

        mtTextureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( mtTextureDataIndex,
                                            [ 1.0, 0.0, 0.0, 0.7 ],
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            4, 4,
                                            screenWidth, screenHeight,
                                            'OFF',
                                            self.textureType )

        mtTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.GenTexture( mtTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', mtTextureIndex )
        OpenGLES2.TexImage2D( mtTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Main thread init' )

        # Create worker thread (shared) context
        workerThreadSurfaceIndex, workerThreadContextIndex = createSharedContext( modules, indexTracker, target, displayIndex, contextIndex )

        # Signal worker thread to do its initialization
        Thread.Signal( workerThreadSignal )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginInitActions( 1 )

        Thread.Wait( workerThreadSignal, 0 )

        Egl.MakeCurrentExt( displayIndex, workerThreadSurfaceIndex, workerThreadSurfaceIndex, workerThreadContextIndex )

        mesh2                   = createPlane( self.bgGridx, self.bgGridy )
        vertexDataSetup2        = setupVertexData( modules, indexTracker, mesh2 )
        program2                = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup2 )
        meshDataSetup2          = setupMeshData( modules, indexTracker, vertexDataSetup2 )
        useMeshData( modules, indexTracker, meshDataSetup2, program2 )

        # Create a worker thread FBO
        wtFramebufferIndex        = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( wtFramebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', wtFramebufferIndex )

        renderTexture1DataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( renderTexture1DataIndex,
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            [ 0.0, 0.0, 1.0, 0.7 ],
                                            4, 4,
                                            screenWidth,
                                            screenHeight,
                                            'OFF',
                                            self.fboTextureType )

        renderTexture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( renderTexture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', renderTexture1Index )
        OpenGLES2.TexImage2D( renderTexture1DataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        renderTexture2DataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( renderTexture2DataIndex,
                                            [ 0.0, 0.0, 1.0, 0.7 ],
                                            [ 0.0, 1.0, 0.0, 0.7 ],
                                            4, 4,
                                            screenWidth,
                                            screenHeight,
                                            'OFF',
                                            self.fboTextureType )

        renderTexture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( renderTexture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', renderTexture2Index )
        OpenGLES2.TexImage2D( renderTexture2DataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                        'GL_COLOR_ATTACHMENT0',
                                        'GL_TEXTURE_2D',
                                        renderTexture1Index,
                                        0 )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

        OpenGLES2.Uniformi( program2.textureLocationIndices[ 0 ], 1, [ 0 ] )

        wtOffsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( wtOffsetLocationIndex, program2.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Worker thread init' )

        Thread.Signal( mainThreadSignal )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        # ------------------------------------------------------------
        self.beginBenchmarkActions( 1 )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                        'GL_COLOR_ATTACHMENT0',
                                        'GL_TEXTURE_2D',
                                        renderTexture1Index,
                                        0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', renderTexture2Index )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.bgOverdraw )
        offset  = 1.0

        for i in range( self.bgOverdraw ):
            OpenGLES2.Uniformf( wtOffsetLocationIndex, 1, [ offset ] )
            drawMeshData( modules, meshDataSetup2 )
            offset -= delta

            OpenGLES2.Finish()

        # ------------------------------------------------------------
        self.beginPreTerminateActions()

        # Delete program here to avoid crash in some engine
        OpenGLES2.DeleteProgram( program.programIndex )
        Thread.Wait( mainThreadSignal, 0 )

        # ------------------------------------------------------------
        self.beginPreTerminateActions( 1 )

        # Delete program here to avoid crash in some engine
        OpenGLES2.DeleteProgram( program2.programIndex )

        # ------------------------------------------------------------
        self.beginTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.ReleaseThread( )
        Thread.Signal( mainThreadSignal )


RENDERBUFFER_FORMATS = { 'OPENGLES2_RGB565'   : 'GL_RGB565',
                         'OPENGLES2_RGB888'   : 'GL_RGB8_OES',
                         'OPENGLES2_RGBA8888' : 'GL_RGBA8_OES' }

PIXMAP_FORMATS = { 'OPENGLES2_RGB565'   : 'SCT_COLOR_FORMAT_RGB565',
                   'OPENGLES2_RGB888'   : 'SCT_COLOR_FORMAT_RGBX8888',
                   'OPENGLES2_RGBA8888' : 'SCT_COLOR_FORMAT_RGBA8888' }

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class UiRender60FPSBenchmark( Benchmark ):
    def __init__( self, method, uiTextureType, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.method         = method
        self.uiTextureType  = uiTextureType
        self.gridx          = gridx
        self.gridy          = gridy
        self.overdraw       = overdraw
        self.textureType    = textureType
        self.wtRepeats      = 64
        self.minMod         = 0.3
        self.repeats        = 0
        self.log            = False
        self.name = 'OPENGLES2 ui render 60 fps %s uitex=%s grd=%dx%dx%d tex=%s' % ( self.method,
                                                                                     textureTypeToStr( self.uiTextureType ),
                                                                                     self.gridx,
                                                                                     self.gridy,
                                                                                     self.overdraw,
                                                                                     textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawMtFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        rawWtFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        uniform float gTextureMod;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = vec4( texture2D( gTexture0, gVSTexCoord0.xy ).rgb * gTextureMod, 1.0 );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        mtFragmentShaderSource = formatShader( rawMtFragmentShaderSource )
        wtFragmentShaderSource = formatShader( rawWtFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Main thread Init actions
        self.beginInitActions()

        mtSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )
        wtSignal = indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' )

        mtTimer  = indexTracker.allocIndex( 'TEST_TIMER_INDEX' )
        wtTimer1 = indexTracker.allocIndex( 'TEST_TIMER_INDEX' )
        wtTimer2 = indexTracker.allocIndex( 'TEST_TIMER_INDEX' )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex   = state[ 'displayIndex' ]
        mtConfigIndex  = state[ 'configIndex' ]
        mtSurfaceIndex = state[ 'surfaceIndex' ]
        mtContextIndex = state[ 'contextIndex' ]

        if self.method in [ 'TEXTURE-EGLIMAGE-TEXTURE',
                            'RBO-EGLIMAGE-TEXTURE',
                            'SHAREDPIXMAP-EGLIMAGE-TEXTURE',
                            'SHAREDPIXMAP-EGLIMAGE-TEXTURE-TEXTURE',
                            'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE' ]:
            Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
            Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )

        if self.method in [ 'SHAREDPIXMAP-EGLIMAGE-TEXTURE',
                            'SHAREDPIXMAP-EGLIMAGE-TEXTURE-TEXTURE',
                            'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE' ]:
            Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )

        Egl.SwapInterval( displayIndex, 1 )

        if self.method in [ 'TEXTURE-EGLIMAGE-TEXTURE',
                            'RBO-EGLIMAGE-TEXTURE',
                            'SHAREDPIXMAP-EGLIMAGE-TEXTURE',
                            'SHAREDPIXMAP-EGLIMAGE-TEXTURE-TEXTURE',
                            'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE' ]:
            OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )

        if self.uiTextureType in [ 'OPENGLES2_RGB888', 'OPENGLES2_RGBA8888' ]:
            if self.method in [ 'RBO-EGLIMAGE-TEXTURE',
                                'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE' ]:
                OpenGLES2.CheckExtension( 'GL_OES_rgb8_rgba8' )

        mtMesh                    = createPlane( 2, 2 )
        mtVertexDataSetup         = setupVertexData( modules, indexTracker, mtMesh )
        mtProgram                 = createProgram( modules, indexTracker, vertexShaderSource, mtFragmentShaderSource, mtVertexDataSetup )
        mtMeshDataSetup           = setupMeshData( modules, indexTracker, mtVertexDataSetup )
        useMeshData( modules, indexTracker, mtMeshDataSetup, mtProgram )

        flagIndex     = indexTracker.allocIndex( 'OPENGLES2_FLAG_INDEX' )

        uiTextures    = []
        uiTextures.append( indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' ) )
        uiTextures.append( indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' ) )

        eglImages      = []
        renderBuffers  = []
        renderTextures = []

        nullTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( nullTextureDataIndex,
                                         screenWidth, screenHeight,
                                         'OFF',
                                         self.uiTextureType )

        if self.method == 'TEXTURE-TEXTURE':
            # TEXTURE-TEXTURE logic: UI thread creates UI
            # textures. Subsequently, worker thread renders into UI textures
            # while UI thread draws the UI textures to the screen.

            OpenGLES2.GenTexture( uiTextures[ 0 ] )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTextures[ 0 ] )
            OpenGLES2.TexImage2D( nullTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_NEAREST',
                                    'GL_NEAREST',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE' )

            OpenGLES2.GenTexture( uiTextures[ 1 ] )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTextures[ 1 ] )
            OpenGLES2.TexImage2D( nullTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_NEAREST',
                                    'GL_NEAREST',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE' )

        elif self.method == 'TEXTURE-EGLIMAGE-TEXTURE':
            # TEXTURE-EGLIMAGE-TEXTURE logic: UI thread creates UI textures, and
            # creates EGL image from UI textures. Subsequently, worker thread
            # creates render textures from the EGL images.

            eglImages.append( indexTracker.allocIndex( 'EGL_IMAGE_INDEX' ) )
            eglImages.append( indexTracker.allocIndex( 'EGL_IMAGE_INDEX' ) )

            OpenGLES2.GenTexture( uiTextures[ 0 ] )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTextures[ 0 ] )
            OpenGLES2.TexImage2D( nullTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_NEAREST',
                                    'GL_NEAREST',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE' )

            OpenGLES2.GenTexture( uiTextures[ 1 ] )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTextures[ 1 ] )
            OpenGLES2.TexImage2D( nullTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_NEAREST',
                                    'GL_NEAREST',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE' )

            Egl.CreateImageExt( displayIndex,
                                eglImages[ 0 ],
                                mtContextIndex,
                                'EGL_GL2_TEXTURE_2D_KHR',
                                uiTextures[ 0 ],
                                [] )

            Egl.CreateImageExt( displayIndex,
                                eglImages[ 1 ],
                                mtContextIndex,
                                'EGL_GL2_TEXTURE_2D_KHR',
                                uiTextures[ 1 ],
                                [] )

        elif self.method == 'RBO-EGLIMAGE-TEXTURE':
            # RBO-EGLIMAGE-TEXTURE logic: Worker thread creates RBOs
            # (renderbuffer object). UI thread creates EGL images from the RBOs,
            # and creates UI textures from
            eglImages.append( indexTracker.allocIndex( 'EGL_IMAGE_INDEX' ) )
            eglImages.append( indexTracker.allocIndex( 'EGL_IMAGE_INDEX' ) )
            renderBuffers.append( indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' ) )
            renderBuffers.append( indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' ) )

        elif self.method in [ 'SHAREDPIXMAP-EGLIMAGE-TEXTURE',
                              'SHAREDPIXMAP-EGLIMAGE-TEXTURE-TEXTURE',
                              'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE' ]:
            eglPixmap0Index    = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
            eglPixmap1Index    = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
            eglImages.append( indexTracker.allocIndex( 'EGL_IMAGE_INDEX' ) )
            eglImages.append( indexTracker.allocIndex( 'EGL_IMAGE_INDEX' ) )

            Egl.CreateSharedPixmap( eglPixmap0Index,
                                    screenWidth, screenHeight,
                                    PIXMAP_FORMATS[ self.uiTextureType ],
                                    [ 'SCT_USAGE_OPENGLES2_TEXTURE_2D', 'SCT_USAGE_OPENGLES2_SURFACE' ],
                                    -1 )

            Egl.CreateSharedPixmap( eglPixmap1Index,
                                    screenWidth, screenHeight,
                                    PIXMAP_FORMATS[ self.uiTextureType ],
                                    [ 'SCT_USAGE_OPENGLES2_TEXTURE_2D', 'SCT_USAGE_OPENGLES2_SURFACE' ],
                                    -1 )

            Egl.CreateImageExt( displayIndex,
                                eglImages[ 0 ],
                                -1,
                                'EGL_NATIVE_PIXMAP_KHR',
                                eglPixmap0Index,
                                [] )

            Egl.CreateImageExt( displayIndex,
                                eglImages[ 1 ],
                                -1,
                                'EGL_NATIVE_PIXMAP_KHR',
                                eglPixmap1Index,
                                [] )

        if self.method in [ 'SHAREDPIXMAP-EGLIMAGE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-TEXTURE-TEXTURE' ]:
            OpenGLES2.GenTexture( uiTextures[ 0 ] )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTextures[ 0 ] )
            OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImages[ 0 ] )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            OpenGLES2.GenTexture( uiTextures[ 1 ] )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTextures[ 1 ] )
            OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImages[ 1 ] )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        if self.method == 'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE':
            renderBuffers.append( indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' ) )
            renderBuffers.append( indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' ) )

        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Main thread init phase 1' )

        # Create worker thread (shared) context
        wtSurfaceIndex, wtContextIndex = createSharedContext( modules, indexTracker, target, displayIndex, mtContextIndex )

        # Signal worker thread to do its initialization
        Thread.Signal( wtSignal )
        Thread.Wait( mtSignal, 0 )

        # -----------------------------------------
        # WORKER THREAD DOES ITS INIT AT THIS PHASE
        # -----------------------------------------

        if self.method == 'RBO-EGLIMAGE-TEXTURE':
            Egl.CreateImageExt( displayIndex, eglImages[ 0 ], wtContextIndex, 'EGL_GL2_RENDERBUFFER_KHR', renderBuffers[ 0 ], [] )
            Egl.CreateImageExt( displayIndex, eglImages[ 1 ], wtContextIndex, 'EGL_GL2_RENDERBUFFER_KHR', renderBuffers[ 1 ], [] )

        if self.method in [ 'RBO-EGLIMAGE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE' ]:
            OpenGLES2.GenTexture( uiTextures[ 0 ] )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTextures[ 0 ] )
            OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImages[ 0 ] )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            OpenGLES2.GenTexture( uiTextures[ 1 ] )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTextures[ 1 ] )
            OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImages[ 1 ] )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        # Set texture 0 as the current UI texture
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTextures[ 0 ] )

        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Main thread init' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker thread
        self.beginInitActions( 1 )

        Thread.Wait( wtSignal, 0 )

        Egl.MakeCurrentExt( displayIndex, wtSurfaceIndex, wtSurfaceIndex, wtContextIndex )

        wtMesh                   = createPlane( self.gridx, self.gridy )
        wtVertexDataSetup        = setupVertexData( modules, indexTracker, wtMesh )
        wtProgram                = createProgram( modules, indexTracker, vertexShaderSource, wtFragmentShaderSource, wtVertexDataSetup )
        wtMeshDataSetup          = setupMeshData( modules, indexTracker, wtVertexDataSetup )
        useMeshData( modules, indexTracker, wtMeshDataSetup, wtProgram )

        # Create a worker thread FBO
        wtFramebufferIndex        = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( wtFramebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', wtFramebufferIndex )

        if self.method in [ 'TEXTURE-EGLIMAGE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-TEXTURE-TEXTURE' ]:
            wtRenderTextures = []

            wtTexture0Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( wtTexture0Index )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', wtTexture0Index )
            OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImages[ 0 ] )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            wtRenderTextures.append( wtTexture0Index )

            wtTexture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( wtTexture1Index )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', wtTexture1Index )
            OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImages[ 1 ] )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            wtRenderTextures.append( wtTexture1Index )

        elif self.method == 'RBO-EGLIMAGE-TEXTURE':
            OpenGLES2.GenRenderbuffer( renderBuffers[ 0 ] )
            OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', renderBuffers[ 0 ] )
            OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER', RENDERBUFFER_FORMATS[ self.uiTextureType ], screenWidth, screenHeight )

            OpenGLES2.GenRenderbuffer( renderBuffers[ 1 ] )
            OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', renderBuffers[ 1 ] )
            OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER', RENDERBUFFER_FORMATS[ self.uiTextureType ], screenWidth, screenHeight )

            OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', -1 )

        elif self.method == 'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE':
            OpenGLES2.GenRenderbuffer( renderBuffers[ 0 ] )
            OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', renderBuffers[ 0 ] )
            OpenGLES2.EglImageTargetRenderbufferStorage( 'GL_RENDERBUFFER', eglImages[ 0 ] )

            OpenGLES2.GenRenderbuffer( renderBuffers[ 1 ] )
            OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', renderBuffers[ 1 ] )
            OpenGLES2.EglImageTargetRenderbufferStorage( 'GL_RENDERBUFFER', eglImages[ 1 ] )

            OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', -1 )

        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )
        OpenGLES2.Uniformi( wtProgram.textureLocationIndices[ 0 ], 1, [ 0 ] )

        wtTextureModLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( wtTextureModLocationIndex, wtProgram.programIndex, 'gTextureMod' )

        textureModRegisterIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
        OpenGLES2.SetRegister( textureModRegisterIndex, self.minMod )

        if self.method in [ 'TEXTURE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-TEXTURE' ]:
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', uiTextures[ 0 ], 0 )
            OpenGLES2.ClearColor( [ 0, 1, 0, 1 ] )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', uiTextures[ 1 ], 0 )
            OpenGLES2.ClearColor( [ 1, 0, 0, 1 ] )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', -1, 0 )

        elif self.method in [ 'TEXTURE-EGLIMAGE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-TEXTURE-TEXTURE' ]:
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', wtRenderTextures[ 0 ], 0 )
            OpenGLES2.ClearColor( [ 0, 1, 0, 1 ] )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', wtRenderTextures[ 1 ], 0 )
            OpenGLES2.ClearColor( [ 1, 0, 0, 1 ] )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', -1, 0 )

        if self.method in [ 'RBO-EGLIMAGE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE' ]:
            OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER','GL_COLOR_ATTACHMENT0', 'GL_RENDERBUFFER', renderBuffers[ 0 ] )
            OpenGLES2.ClearColor( [ 0, 1, 0, 1 ] )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

            OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER','GL_COLOR_ATTACHMENT0', 'GL_RENDERBUFFER', renderBuffers[ 1 ] )
            OpenGLES2.ClearColor( [ 1, 0, 0, 1 ] )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

            OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_RENDERBUFFER', -1 )

        wtTextureData0Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( wtTextureData0Index,
                                            [ 0.0, 0.0, 0.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            4, 4,
                                            screenWidth,
                                            screenHeight,
                                            'OFF',
                                            self.textureType )

        wtTexture0Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( wtTexture0Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', wtTexture0Index )
        OpenGLES2.TexImage2D( wtTextureData0Index, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST',
                                'GL_NEAREST',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        wtTextureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( wtTextureData1Index,
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 0.0, 0.0, 0.0, 1.0 ],
                                            4, 4,
                                            screenWidth,
                                            screenHeight,
                                            'OFF',
                                            self.textureType )

        wtTexture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( wtTexture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', wtTexture1Index )
        OpenGLES2.TexImage2D( wtTextureData1Index, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST',
                                'GL_NEAREST',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Enable( 'GL_BLEND' )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.ClearColor( [ 0, 0, 0, 1 ] )
        OpenGLES2.Finish()

        Thread.Signal( mtSignal )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # UI thread benchmark actions
        self.beginBenchmarkActions()

        if self.log:
            Test.SetTimer( mtTimer )

        # Set the current UI texture
        OpenGLES2.CBindTexture( flagIndex, 'GL_TEXTURE_2D', uiTextures, 'ON', wtSignal )

        drawMeshData( modules, mtMeshDataSetup )

        Egl.SwapBuffers( displayIndex, mtSurfaceIndex )

        if self.log:
            Test.LogSince( mtTimer, 'Ui thread', 4096 )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker thread benchmark actions.
        self.beginBenchmarkActions( 1 )

        if self.log:
            Test.SetTimer( wtTimer1 )

        # Set texture modulation value; makes worker thread progress
        # visible. Change texture to use for rendering.
        OpenGLES2.UniformfFromRegister( wtTextureModLocationIndex, textureModRegisterIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', wtTexture0Index )

        if self.method in [ 'TEXTURE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-TEXTURE' ]:
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', uiTextures[ 1 ], 0 )

        elif self.method in [ 'TEXTURE-EGLIMAGE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-TEXTURE-TEXTURE' ]:
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', wtRenderTextures[ 1 ], 0 )

        elif self.method in [ 'RBO-EGLIMAGE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE' ]:
            OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_RENDERBUFFER', renderBuffers[ 1 ] )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        # Draw worker thread mesh.
        for i in range( self.overdraw ):
            drawMeshData( modules, wtMeshDataSetup )

        # Ensure all commands are finished. Flag the UI thread that the texture1
        # is ready.
        OpenGLES2.Finish()

        if self.log:
            Test.LogSince( wtTimer1, 'Worker thread part 1', self.wtRepeats + 2 )

        OpenGLES2.Flag( flagIndex, 1, wtSignal )

        if self.log:
            Test.SetTimer( wtTimer2 )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', wtTexture1Index )

        if self.method in [ 'TEXTURE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-TEXTURE' ]:
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', uiTextures[ 0 ], 0 )

        elif self.method in [ 'TEXTURE-EGLIMAGE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-TEXTURE-TEXTURE' ]:
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', wtRenderTextures[ 0 ], 0 )

        elif self.method in [ 'RBO-EGLIMAGE-TEXTURE', 'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE' ]:
            OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_RENDERBUFFER', renderBuffers[ 0 ] )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        for i in range( self.overdraw ):
            drawMeshData( modules, wtMeshDataSetup )

        # Ensure all commands are finished. Flag the UI thread that the texture0
        # is ready.
        OpenGLES2.Finish()

        if self.log:
            Test.LogSince( wtTimer2, 'Worker thread part 2', self.wtRepeats + 1 )

        OpenGLES2.Flag( flagIndex, 0, wtSignal )

        # Increase texture modulation value.
        OpenGLES2.AccumulateRegister( textureModRegisterIndex, ( 1.0 - self.minMod ) / float( self.wtRepeats ), 1.0, self.minMod )

        Thread.CompleteBenchmark( self.wtRepeats )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Main thread pre-terminate actions
        self.beginPreTerminateActions()

        # Delete program here to avoid crash in some engine
        OpenGLES2.DeleteProgram( mtProgram.programIndex )
        Thread.Wait( mtSignal, 0 )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker thread pre-terminate actions
        self.beginPreTerminateActions( 1 )

        # Delete program here to avoid crash in some engine
        OpenGLES2.DeleteProgram( wtProgram.programIndex )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker thread terminate actions
        self.beginTerminateActions( 1 )

        Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
        Egl.ReleaseThread()
        Thread.Signal( mtSignal )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class MultiThreadFenceBenchmark( Benchmark ):
    def __init__( self, textureType ):
        Benchmark.__init__( self )
        self.textureType    = textureType
        self.name = 'OPENGLES2 multithread fence tex=%s' % ( textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawMtFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        rawWtFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        uniform float gTextureMod;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = vec4( texture2D( gTexture0, gVSTexCoord0.xy ).rgb * gTextureMod, 1.0 );
        }
        """

        vertexShaderSource     = formatShader( rawVertexShaderSource )
        mtFragmentShaderSource = formatShader( rawMtFragmentShaderSource )
        wtFragmentShaderSource = formatShader( rawWtFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Main thread Init actions
        self.beginInitActions()

        mtSignals = []
        wtSignals = []

        for i in range( 1 ):
            mtSignals.append( indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' ) )
            wtSignals.append( indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' ) )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex   = state[ 'displayIndex' ]
        mtConfigIndex  = state[ 'configIndex' ]
        mtSurfaceIndex = state[ 'surfaceIndex' ]
        mtContextIndex = state[ 'contextIndex' ]

        mtMesh              = createPlane( 2, 2 )
        mtVertexDataSetup   = setupVertexData( modules, indexTracker, mtMesh )
        mtProgram           = createProgram( modules, indexTracker, vertexShaderSource, mtFragmentShaderSource, mtVertexDataSetup )
        useVertexData( modules, indexTracker, mtVertexDataSetup, mtProgram )

        uiTexture = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        nullTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( nullTextureDataIndex,
                                         screenWidth, screenHeight,
                                         'OFF',
                                         self.textureType )

        OpenGLES2.GenTexture( uiTexture )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTexture )
        OpenGLES2.TexImage2D( nullTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST',
                                'GL_NEAREST',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Main thread init phase 1' )

        # Create worker thread (shared) context
        wtSurfaceContext = []
        wtSurfaceContext.append( createSharedContext( modules, indexTracker, target, displayIndex, mtContextIndex ) )

        # Signal worker thread to do its initialization, wait for completion.
        for i in range( 1 ):
            Thread.Signal( wtSignals[ i ] )

        # -----------------------------------------
        # WORKER THREADS DO THEIR INIT AT THIS PHASE
        # -----------------------------------------

        for i in range( 1 ):
            Thread.Wait( mtSignals[ 0 ], 0 )

        # Set UI texture
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTexture )

        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Main thread init' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker threads
        wtMeshes                    = []
        wtVertexDataSetups          = []
        wtPrograms                  = []
        wtFramebufferIndices        = []
        wtTextureModLocationIndices = []
        textureModRegisterIndices   = []

        for i in range( 1 ):
            self.beginInitActions( i + 1 )

            Thread.Wait( wtSignals[ i ], 0 )

            Egl.MakeCurrentExt( displayIndex, wtSurfaceContext[ i ][ 0 ], wtSurfaceContext[ i ][ 0 ], wtSurfaceContext[ i ][ 1 ] )

            wtMeshes.append( createPlane( 2, 2 ) )
            wtVertexDataSetups.append( setupVertexData( modules, indexTracker, wtMeshes[ i ] ) )
            wtPrograms.append( createProgram( modules, indexTracker, vertexShaderSource, wtFragmentShaderSource, wtVertexDataSetups[ i ] ) )
            useVertexData( modules, indexTracker, wtVertexDataSetups[ i ], wtPrograms[ i ] )

            # Create a worker thread FBO
            wtFramebufferIndices.append( indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' ) )
            OpenGLES2.GenFramebuffer( wtFramebufferIndices[ i ] )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', wtFramebufferIndices[ i ] )

            texColors = [ [ 1, 1, 0, 1 ],
                          [ 0, 1, 1, 1 ],
                          [ 0, 0, 1, 1 ] ]
            viewports = [ [                   0, 0,     screenWidth, screenHeight ],
                          [                   0, 0, screenWidth / 3, screenHeight ],
                          [ 2 * screenWidth / 3, 0, screenWidth / 3, screenHeight ] ]

            OpenGLES2.Viewport( viewports[ i ][ 0 ], viewports[ i ][ 1 ], viewports[ i ][ 2 ],viewports[ i ][ 3 ] )
            OpenGLES2.Uniformi( wtPrograms[ i ].textureLocationIndices[ 0 ], 1, [ 0 ] )

            wtTextureModLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
            OpenGLES2.GetUniformLocation( wtTextureModLocationIndices[ i ], wtPrograms[ i ].programIndex, 'gTextureMod' )

            textureModRegisterIndices.append( indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' ) )
            OpenGLES2.SetRegister( textureModRegisterIndices[ i ], 0.1 )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', uiTexture, 0 )
            OpenGLES2.ClearColor( [ 0, 1, 0, 1 ] )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

            wtTextureData0Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateCheckerTextureData( wtTextureData0Index,
                                                [ 0.0, 0.0, 0.0, 1.0 ],
                                                texColors[ i ],
                                                4, 4,
                                                screenWidth,
                                                screenHeight,
                                                'OFF',
                                                self.textureType )

            wtTexture0Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( wtTexture0Index )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', wtTexture0Index )
            OpenGLES2.TexImage2D( wtTextureData0Index, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_NEAREST',
                                    'GL_NEAREST',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE' )

            OpenGLES2.ClearColor( [ 0, 0, 0, 1 ] )
            OpenGLES2.Finish()

            Thread.Signal( mtSignals[ i ] )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # UI thread benchmark actions
        self.beginBenchmarkActions()

        # Trigger worker thread 0 to do its stuff; it will trigger other worker threads.
        Thread.Signal( wtSignals[ 0 ] )

        # Wait for the worker thread to signal back the main thread that EGL sync is in place.
        for i in range( 1 ):
            Thread.Wait( mtSignals[ i ], 0 )

        syncs = []
        for i in range( 1 ):
            syncs.append( indexTracker.allocIndex( 'EGL_SYNC_INDEX' ) )

        # At this point we have EGL fence sync from the worker thread, wait for them.
        for i in range( 1 ):
            Egl.ClientWaitSync( displayIndex, syncs[ i ], [], [ 5000, 1000000 ] )      # 5 seconds
            Egl.DestroySync( displayIndex, syncs[ i ], 'ON' )

        drawVertexData( modules, mtVertexDataSetup )

        Egl.SwapBuffers( displayIndex, mtSurfaceIndex )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker thread benchmark actions.
        for i in range( 1 ):
            self.beginBenchmarkActions( i + 1 )

            # Worker thread wait for go.
            Thread.Wait( wtSignals[ i ], 0 )

            # Set texture modulation value; makes worker thread progress
            # visible. Change texture to use for rendering.
            OpenGLES2.UniformfFromRegister( wtTextureModLocationIndices[ i ], textureModRegisterIndices[ i ] )

            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
            drawVertexData( modules, wtVertexDataSetups[ i ] )

            Egl.CreateSync( displayIndex, syncs[ i ], 'EGL_SYNC_FENCE_KHR', [] )
            OpenGLES2.Flush()

            # Worker thread signal the main thread.
            Thread.Signal( mtSignals[ i ] )

            OpenGLES2.AccumulateRegister( textureModRegisterIndices[ i ], 1.0 / 255.0, 1.0, 0.1 )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Main thread pre-terminate actions
        self.beginPreTerminateActions()

        # Delete program here to avoid crash in some engine
        OpenGLES2.DeleteProgram( mtProgram.programIndex )
        for i in range( 1 ):
            Thread.Wait( mtSignals[ i ], 0 )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker thread pre-terminate actions
        for i in range( 1 ):
           self.beginPreTerminateActions( i + 1 )

           # Delete program here to avoid crash in some engine
           OpenGLES2.DeleteProgram( wtPrograms[ i ].programIndex )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker thread terminate actions
        for i in range( 1 ):
            self.beginTerminateActions( i + 1 )

            Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
            Egl.ReleaseThread()
            Thread.Signal( mtSignals[ i ] )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ComplexMultiThreadFenceBenchmark( Benchmark ):
    def __init__( self, textureType ):
        Benchmark.__init__( self )
        self.textureType    = textureType
        self.wtCount        = 3
        self.name = 'OPENGLES2 complex multithread fence tex=%s' % ( textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Thread    = modules[ 'Thread' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawMtFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        rawWtFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        uniform float gTextureMod;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = vec4( texture2D( gTexture0, gVSTexCoord0.xy ).rgb * gTextureMod, 1.0 );
        }
        """

        vertexShaderSource     = formatShader( rawVertexShaderSource )
        mtFragmentShaderSource = formatShader( rawMtFragmentShaderSource )
        wtFragmentShaderSource = formatShader( rawWtFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Main thread Init actions
        self.beginInitActions()

        mtSignals = []
        wtSignals = []

        for i in range( self.wtCount ):
            mtSignals.append( indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' ) )
            wtSignals.append( indexTracker.allocIndex( 'THREAD_SIGNAL_INDEX' ) )

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex   = state[ 'displayIndex' ]
        mtConfigIndex  = state[ 'configIndex' ]
        mtSurfaceIndex = state[ 'surfaceIndex' ]
        mtContextIndex = state[ 'contextIndex' ]

        mtMesh              = createPlane( 2, 2 )
        mtVertexDataSetup   = setupVertexData( modules, indexTracker, mtMesh )
        mtProgram           = createProgram( modules, indexTracker, vertexShaderSource, mtFragmentShaderSource, mtVertexDataSetup )
        useVertexData( modules, indexTracker, mtVertexDataSetup, mtProgram )

        uiTexture = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        nullTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( nullTextureDataIndex,
                                         screenWidth, screenHeight,
                                         'OFF',
                                         self.textureType )

        OpenGLES2.GenTexture( uiTexture )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTexture )
        OpenGLES2.TexImage2D( nullTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST',
                                'GL_NEAREST',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Main thread init phase 1' )

        # Create worker thread (shared) contexts
        wtSurfaceContext = []
        for i in range( self.wtCount ):
            wtSurfaceContext.append( createSharedContext( modules, indexTracker, target, displayIndex, mtContextIndex ) )

        # Signal worker threads to do their initialization (concurrently), wait for completion.
        for i in range( self.wtCount ):
            Thread.Signal( wtSignals[ i ] )

        # -----------------------------------------
        # WORKER THREADS DO THEIR INIT AT THIS PHASE
        # -----------------------------------------

        for i in range( self.wtCount ):
            Thread.Wait( mtSignals[ i ], 0 )

        # Set UI texture
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', uiTexture )

        OpenGLES2.Finish()
        OpenGLES2.CheckError( 'Main thread init' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker threads
        wtMeshes                    = []
        wtVertexDataSetups          = []
        wtPrograms                  = []
        wtFramebufferIndices        = []
        wtTextureModLocationIndices = []
        textureModRegisterIndices   = []

        for i in range( self.wtCount ):
            self.beginInitActions( i + 1 )

            Thread.Wait( wtSignals[ i ], 0 )

            Egl.MakeCurrentExt( displayIndex, wtSurfaceContext[ i ][ 0 ], wtSurfaceContext[ i ][ 0 ], wtSurfaceContext[ i ][ 1 ] )

            wtMeshes.append( createPlane( 2, 2 ) )
            wtVertexDataSetups.append( setupVertexData( modules, indexTracker, wtMeshes[ i ] ) )
            wtPrograms.append( createProgram( modules, indexTracker, vertexShaderSource, wtFragmentShaderSource, wtVertexDataSetups[ i ] ) )
            useVertexData( modules, indexTracker, wtVertexDataSetups[ i ], wtPrograms[ i ] )

            # Create a worker thread FBO
            wtFramebufferIndices.append( indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' ) )
            OpenGLES2.GenFramebuffer( wtFramebufferIndices[ i ] )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', wtFramebufferIndices[ i ] )

            texColors = [ [ 1, 1, 0, 1 ],
                          [ 0, 1, 1, 1 ],
                          [ 0, 0, 1, 1 ] ]
            viewports = [ [                   0, 0,     screenWidth, screenHeight ],
                          [                   0, 0, screenWidth / 3, screenHeight ],
                          [ 2 * screenWidth / 3, 0, screenWidth / 3, screenHeight ] ]

            assert len( texColors ) == self.wtCount
            assert len( viewports ) == self.wtCount

            OpenGLES2.Viewport( viewports[ i ][ 0 ], viewports[ i ][ 1 ], viewports[ i ][ 2 ],viewports[ i ][ 3 ] )
            OpenGLES2.Uniformi( wtPrograms[ i ].textureLocationIndices[ 0 ], 1, [ 0 ] )

            wtTextureModLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
            OpenGLES2.GetUniformLocation( wtTextureModLocationIndices[ i ], wtPrograms[ i ].programIndex, 'gTextureMod' )

            textureModRegisterIndices.append( indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' ) )
            OpenGLES2.SetRegister( textureModRegisterIndices[ i ], 0.1 )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', uiTexture, 0 )
            OpenGLES2.ClearColor( [ 0, 1, 0, 1 ] )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

            wtTextureData0Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateCheckerTextureData( wtTextureData0Index,
                                                [ 0.0, 0.0, 0.0, 1.0 ],
                                                texColors[ i ],
                                                4, 4,
                                                screenWidth,
                                                screenHeight,
                                                'OFF',
                                                self.textureType )

            wtTexture0Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( wtTexture0Index )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', wtTexture0Index )
            OpenGLES2.TexImage2D( wtTextureData0Index, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_NEAREST',
                                    'GL_NEAREST',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE' )

            OpenGLES2.ClearColor( [ 0, 0, 0, 1 ] )
            OpenGLES2.Finish()

            Thread.Signal( mtSignals[ i ] )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # UI thread benchmark actions
        self.beginBenchmarkActions()

        # Trigger worker thread 0 to do its stuff; it will trigger other worker threads.
        Thread.Signal( wtSignals[ 0 ] )

        # Wait for the worker threads to signal back the main thread that EGL syncs are in place.
        for i in range( self.wtCount ):
            Thread.Wait( mtSignals[ i ], 0 )

        syncs = []
        for i in range( self.wtCount ):
            syncs.append( indexTracker.allocIndex( 'EGL_SYNC_INDEX' ) )

        # At this point we have EGL fence syncs from the worker threads 1...n, wait for them.
        for i in range( 1, self.wtCount ):
            Egl.ClientWaitSync( displayIndex, syncs[ i ], [], [ 10000, 1000000 ] )      # 10 seconds
            Egl.DestroySync( displayIndex, syncs[ i ], 'ON' )

        drawVertexData( modules, mtVertexDataSetup )

        Egl.SwapBuffers( displayIndex, mtSurfaceIndex )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker thread benchmark actions.
        for i in range( self.wtCount ):
            self.beginBenchmarkActions( i + 1 )

            # Worker thread wait for go.
            Thread.Wait( wtSignals[ i ], 0 )

            if i != 0:
                # Worker threads 1...n wait for the sync from the worker thread 0.
                Egl.ClientWaitSync( displayIndex, syncs[ 0 ], [], [ 5000, 1000000 ] )  # 5 seconds
                Egl.DestroySync( displayIndex, syncs[ 0 ], 'OFF' )

            # Set texture modulation value; makes worker thread progress
            # visible. Change texture to use for rendering.
            OpenGLES2.UniformfFromRegister( wtTextureModLocationIndices[ i ], textureModRegisterIndices[ i ] )

            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
            drawVertexData( modules, wtVertexDataSetups[ i ] )

            Egl.CreateSync( displayIndex, syncs[ i ], 'EGL_SYNC_FENCE_KHR', [] )
            OpenGLES2.Flush()

            if i == 0:
                # Worker thread 0 signals worker threads 1...n.
                for j in range( 1, self.wtCount ):
                    Thread.Signal( wtSignals[ j ] )

            # All worker threads signal the main thread.
            Thread.Signal( mtSignals[ i ] )

            OpenGLES2.AccumulateRegister( textureModRegisterIndices[ i ], 1.0 / 255.0, 1.0, 0.1 )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Main thread pre-terminate actions
        self.beginPreTerminateActions()

        # Delete program here to avoid crash in some engine
        OpenGLES2.DeleteProgram( mtProgram.programIndex )
        for i in range( self.wtCount ):
            Thread.Wait( mtSignals[ i ], 0 )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker thread pre-terminate actions
        for i in range( self.wtCount ):
           self.beginPreTerminateActions( i + 1 )

           # Delete program here to avoid crash in some engine
           OpenGLES2.DeleteProgram( wtPrograms[ i ].programIndex )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Worker thread terminate actions
        for i in range( self.wtCount ):
            self.beginTerminateActions( i + 1 )

            Egl.MakeCurrentExt( displayIndex, -1, -1, -1 )
            Egl.ReleaseThread()
            Thread.Signal( mtSignals[ i ] )

