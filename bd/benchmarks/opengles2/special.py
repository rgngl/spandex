#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

import math

######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    colorAttribute    = None
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )

    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           colorAttribute,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh

######################################################################
def createPlane2( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    colorAttribute    = None
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )

    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           colorAttribute,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 1.8, 1.8 ] )

    return mesh

######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                         'OPENGLES2_GRADIENT_HORIZONTAL',
                                         c1,
                                         c2,
                                         w, h,
                                         'OFF',
                                         textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )

######################################################################
def useTextureSpecial( modules, indexTracker, textureSetup, textureUnit, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    if textureUnit < 0:
        raise 'Invalid texture unit'

    OpenGLES2.ActiveTexture( textureUnit )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )

    index = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
    OpenGLES2.GetUniformLocation( index, program.programIndex, 'gTexture%d' % textureUnit )
    
    OpenGLES2.Uniformi( index, 1, [ textureUnit ] )

    
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                              
class DependentTextureBenchmark( Benchmark ):  
    def __init__( self, textureType ):
        Benchmark.__init__( self )
        self.textureType = textureType
        self.gridx       = 2
        self.gridy       = 2
        self.overdraw    = 1
        self.name  = "OPENGLES2 dependent texture %s" % ( textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # Shader source code
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""
        
        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        uniform sampler2D gTexture1;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture1, texture2D( gTexture0, gVSTexCoord0.xy).xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )
       
        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )
        
        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 0.0, 0.0, 0.0, 1.0 ],
                                                [ 1.0, 1.0, 1.0, 1.0 ],
                                                self.textureType )

        texture1Setup           = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 0.3, 0.3, 1.0, 1.0 ],
                                                [ 0.3, 1.0, 1.0, 1.0 ],
                                                self.textureType )
                              
        useTexture( modules, texture0Setup, 0, program )
        useTextureSpecial( modules, indexTracker, texture1Setup, 1, program )                              

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )
       
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                              
class ParticlesBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, overdraw, size, textureType, calculateRounding ):
        Benchmark.__init__( self )
        self.gridx             = gridx
        self.gridy             = gridy
        self.overdraw          = overdraw
        self.size              = size
        self.textureType       = textureType
        self.calculateRounding = calculateRounding
        self.name = "OPENGLES2 particles %dx%dx%d size=%d tex=%s%s" % ( self.gridx,
                                                                        self.gridy,
                                                                        self.overdraw,
                                                                        self.size,
                                                                        textureTypeToStr( self.textureType ),
                                                                        { True : ' rounded', False : '' }[ self.calculateRounding ], )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # Shader source code
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute float gSize;
        uniform mat4 mvp;

        void main()
        {
           gl_Position = mvp * gVertex;
           gl_PointSize = gSize / gl_Position.w;
        }"""
        
        if self.calculateRounding:
            rawFragmentShaderSource = """
            precision mediump float;
            uniform sampler2D gTexture0;
            const vec2 center=vec2(0.5,0.5);
            float x, y, d;
            void main()
            {
               x=gl_PointCoord.x-0.5;
               y=gl_PointCoord.y-0.5;
               d=x*x+y*y;
               if(d>0.25 )
               {
                  discard;
               }
               gl_FragColor=texture2D(gTexture0, gl_PointCoord.xy);
             }"""
        else:
            rawFragmentShaderSource = """
            precision mediump float;
            uniform sampler2D gTexture0;
            void main()
            {
               gl_FragColor = texture2D(gTexture0, gl_PointCoord.xy);
            }"""
                       
        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )
       
        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane2( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                self.size, self.size,
                                                [ 0.0, 0.0, 0.0, 1.0 ],
                                                [ 1.0, 1.0, 1.0, 1.0 ],
                                                self.textureType )

        useTextureSpecial( modules, indexTracker, texture0Setup, 0, program )                              


        sizes = [ self.size ] * ( self.gridx * self.gridy )
        sizeArrayIndex  = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( sizeArrayIndex, 'GL_FLOAT' )
        OpenGLES2.AppendToArray( sizeArrayIndex, sizes )

        sizeBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( sizeBufferIndex )
        OpenGLES2.BufferData( sizeBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              sizeArrayIndex,
                              0,
                              0 )
        
        sizeLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetAttribLocation( sizeLocationIndex, program.programIndex, 'gSize' )

        OpenGLES2.BufferVertexAttribPointer( sizeBufferIndex,
                                             'ON',
                                             sizeLocationIndex,
                                             1,
                                             'OFF',
                                             0 )
        OpenGLES2.EnableVertexAttribArray( sizeLocationIndex )

        modelViewProjectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( modelViewProjectionLocationIndex,
                                      program.programIndex,
                                      'mvp' )

        OpenGLES2.UniformMatrix( modelViewProjectionLocationIndex,
                                 1,
                                 [ 1.0, 0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0, 1.0 ] )
        
        if not self.calculateRounding:
            OpenGLES2.Enable( 'GL_BLEND' )
            OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )

        OpenGLES2.BindBuffer( 'GL_ELEMENT_ARRAY_BUFFER', meshBufferDataSetup.indexBufferIndex )
        OpenGLES2.CheckError( '' )        
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()
       
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        for i in range( self.overdraw ):
            OpenGLES2.BufferDrawElements( meshBufferDataSetup.indexBufferIndex,
                                          'OFF',
                                          'GL_POINTS',
                                          len( mesh.indices ),
                                          0 )    
        
        target.swapBuffers( state )

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                              
class SkyboxBenchmark( Benchmark ):  
    def __init__( self, textureType ):
        Benchmark.__init__( self )
        self.textureType = textureType
        self.gridx       = 2
        self.gridy       = 2
        self.overdraw    = 1
        self.name = "OPENGLES2 skybox %s" % ( textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # Shader source code
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        varying vec4 gVSTexCoord0;
        uniform mat4 gMVP;

        void main()
        {
           gl_Position = gMVP * gVertex;
 	   gVSTexCoord0.xyz = gVertex.xyz;
        }"""
        
        rawFragmentShaderSource = """
        precision mediump float;
        uniform samplerCube gTex0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           vec3 cube = vec3( textureCube( gTex0, gVSTexCoord0.xyz ) );
           gl_FragColor = vec4( cube, 1.0 );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )
       
        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        state = setupGL2( modules, indexTracker, target )       

        vertices = [ -10.000000,  10.000000,  10.000000,
                      10.000000,  10.000000,  10.000000,
                      10.000000, -10.000000,  10.000000,
                     -10.000000, -10.000000,  10.000000,
                      10.000000,  10.000000, -10.000000,
                     -10.000000,  10.000000, -10.000000,
                     -10.000000, -10.000000, -10.000000,
                      10.000000, -10.000000, -10.000000 ]
        
        indices = [ 1, 4, 7, 7, 2, 1,  # positive x
                    5, 0, 3, 3, 6, 5,  # negative x
                    5, 4, 1, 1, 0, 5,
                    3, 2, 7, 7, 6, 3, 
                    0, 1, 2, 2, 3, 0,
                    4, 5, 6, 6, 7, 4 ]
                      
        vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( vertexArrayIndex, 'GL_FLOAT' )
        OpenGLES2.AppendToArray( vertexArrayIndex, vertices )

        indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( indexArrayIndex, 'GL_UNSIGNED_SHORT' )
        OpenGLES2.AppendToArray( indexArrayIndex, indices )
         
        vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( vertexBufferIndex )
        OpenGLES2.BufferData( vertexBufferIndex, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', vertexArrayIndex, 0, 0 );

        indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( indexBufferIndex )
        OpenGLES2.BufferData( indexBufferIndex, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', indexArrayIndex, 0, 0 );

        colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                   [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                   [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                   [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                   [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                   [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

        textureWidth  = 512
        textureHeight = 512

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )            
        OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

        faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                  'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                  'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

        for i in range( 6 ):
            textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                              colors[ i ],
                                              textureWidth,
                                              textureHeight,
                                              'OFF',
                                              self.textureType )
             
            OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );

        OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )
       
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )
        
        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )
        
        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )
        
        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )
        
        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.ValidateProgram( programIndex )
        OpenGLES2.UseProgram( programIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
        OpenGLES2.GetAttribLocation( vertexLocationIndex,
                                     programIndex,
                                     'gVertex' )

        OpenGLES2.BufferVertexAttribPointer( vertexBufferIndex,
                                             'ON',
                                             vertexLocationIndex,
                                             3,
                                             'OFF',
                                             0 )

        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        
        mvpLocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
        OpenGLES2.GetUniformLocation( mvpLocationIndex,
                                      programIndex,
                                      'gMVP' )

        OpenGLES2.UniformMatrix( mvpLocationIndex,
                                 1,
                                 [  -0.222787,  0.000000, -0.918314, -0.918222,
                                     0.000000,  1.000000,  0.000000,  0.000000, 
                                    -0.516500,  0.000000,  0.396106,  0.396066,
                                     1.300805, -0.283331, -1.197603, -0.997493 ] )
        
        tex0LocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
        OpenGLES2.GetUniformLocation( tex0LocationIndex,
                                      programIndex,
                                      'gTex0' )
        
        OpenGLES2.Uniformi( tex0LocationIndex,
                            1,
                            [ 0 ] )

        OpenGLES2.BindBuffer( 'GL_ELEMENT_ARRAY_BUFFER', indexBufferIndex )
        OpenGLES2.CheckError( '' )
       
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES2.BufferDrawElements( indexBufferIndex,
                                      'OFF',
                                      'GL_TRIANGLES',
                                      len( indices ),
                                      0 )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                              
class MediaWallSkyboxBenchmark( Benchmark ):  
    def __init__( self, textureType ):
        Benchmark.__init__( self )
        self.textureType = textureType
        self.gridx       = 2
        self.gridy       = 2
        self.overdraw    = 1
        self.name = "OPENGLES2 mediawall skybox %s" % ( textureTypeToStr( self.textureType ), )
        self.repeats = 2048

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # Shader source code
        rawVertexShaderSource = """
        uniform mat4 proj_model_view_matrix;
        uniform vec3 camera_position;

        attribute vec4 rm_Vertex;
        varying vec3 vTexCoord;

        void main(void)
        {
           vec4 newPos = vec4(1.0);
           newPos.xyz =  rm_Vertex.xyz * 100.0 + camera_position.xyz;
           gl_Position = proj_model_view_matrix * vec4(newPos.xyz, 1.0);
           vec4 tex = normalize(rm_Vertex);
           vTexCoord.z = -tex.z;
           vTexCoord.y = tex.y;
           vTexCoord.x = tex.x;
        }"""
       
        rawFragmentShaderSource = """
        precision highp float;

        uniform samplerCube skybox;
        varying vec3 vTexCoord;

        void main(void)
        {
           gl_FragColor = textureCube( skybox, vTexCoord );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )
       
        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        state = setupGL2( modules, indexTracker, target )       

        mvp = [ -0.562500,  0.000000, -0.000000, -0.000000,
                 0.000000,  1.000000,  0.000000,  0.000000, 
                -0.000000,  0.000000,  1.000100,  1.000000,
                 0.000000, -0.450000, -4.200410, -4.000000 ]

        camerapos = [ 0.000000, 0.444600, 3.952000 ]
        
        vertices = [ -10.000000,  10.000000,  10.000000,
                      10.000000,  10.000000,  10.000000,
                      10.000000, -10.000000,  10.000000,
                     -10.000000, -10.000000,  10.000000,
                      10.000000,  10.000000, -10.000000,
                     -10.000000,  10.000000, -10.000000,
                     -10.000000, -10.000000, -10.000000,
                      10.000000, -10.000000, -10.000000 ]
        
        indices = [ 1, 4, 7, 7, 2, 1,  # positive x
                    5, 0, 3, 3, 6, 5,  # negative x
                    5, 4, 1, 1, 0, 5,
                    3, 2, 7, 7, 6, 3, 
                    0, 1, 2, 2, 3, 0,
                    4, 5, 6, 6, 7, 4 ]
                      
        vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( vertexArrayIndex, 'GL_FLOAT' )
        OpenGLES2.AppendToArray( vertexArrayIndex, vertices )

        indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( indexArrayIndex, 'GL_UNSIGNED_SHORT' )
        OpenGLES2.AppendToArray( indexArrayIndex, indices )
         
        vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( vertexBufferIndex )
        OpenGLES2.BufferData( vertexBufferIndex, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', vertexArrayIndex, 0, 0 );

        indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( indexBufferIndex )
        OpenGLES2.BufferData( indexBufferIndex, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', indexArrayIndex, 0, 0 );

        colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                   [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                   [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                   [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                   [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                   [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

        textureWidth  = 512
        textureHeight = 512

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )            
        OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

        faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                  'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                  'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

        for i in range( 6 ):
            textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                              colors[ i ],
                                              textureWidth,
                                              textureHeight,
                                              'OFF',
                                              self.textureType )
             
            OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                
        OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )
       
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )
        
        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )
        
        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )
        
        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )
        
        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.ValidateProgram( programIndex )
        OpenGLES2.UseProgram( programIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
        OpenGLES2.GetAttribLocation( vertexLocationIndex,
                                     programIndex,
                                     'rm_Vertex' )

        OpenGLES2.BufferVertexAttribPointer( vertexBufferIndex,
                                             'ON',
                                             vertexLocationIndex,
                                             3,
                                             'OFF',
                                             0 )

        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        
        mvpLocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
        OpenGLES2.GetUniformLocation( mvpLocationIndex,
                                      programIndex,
                                      'proj_model_view_matrix' )

        OpenGLES2.UniformMatrix( mvpLocationIndex,
                                 1,
                                 mvp )

        cameraposLocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
        OpenGLES2.GetUniformLocation( cameraposLocationIndex,
                                      programIndex,
                                      'camera_position' )

        OpenGLES2.Uniformf( cameraposLocationIndex,
                            1,
                            camerapos )
        
        tex0LocationIndex = indexTracker.allocIndex( 'OPENGLES2_REGISTER_INDEX' )
        OpenGLES2.GetUniformLocation( tex0LocationIndex,
                                      programIndex,
                                      'skybox' )
        
        OpenGLES2.Uniformi( tex0LocationIndex,
                            1,
                            [ 0 ] )

        OpenGLES2.BindBuffer( 'GL_ELEMENT_ARRAY_BUFFER', indexBufferIndex )
        OpenGLES2.CheckError( '' )
       
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES2.BufferDrawElements( indexBufferIndex,
                                      'OFF',
                                      'GL_TRIANGLES',
                                      len( indices ),
                                      0 )
        
        target.swapBuffers( state )

