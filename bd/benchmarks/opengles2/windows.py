#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
READPIXELS  = 'readpixels'
COPYBUFFERS = 'copybuffers'
FINISH      = 'finish'

######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ResizeWindowBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, extentR1, extentR2 ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.extentR1    = extentR1
        self.extentR2    = extentR2
        self.name = "OPENGLES2 resize window from=%s to=%s grd=%dx%dx%d tex=%s" % ( farrayToStr( self.extentR1 ),
                                                                                    farrayToStr( self.extentR2 ),
                                                                                    self.gridx,
                                                                                    self.gridy,
                                                                                    self.overdraw,
                                                                                    textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()        

        extent1 = None
        if self.extentR1:
            extent1 = [ 0, 0, 0, 0 ]
            extent1[ 0 ] = int( self.extentR1[ 0 ] * screenWidth )
            extent1[ 1 ] = int( self.extentR1[ 1 ] * screenHeight )
            extent1[ 2 ] = int( self.extentR1[ 2 ] * screenWidth )
            extent1[ 3 ] = int( self.extentR1[ 3 ] * screenHeight )

        extent2 = None
        if self.extentR2:
            extent2 = [ 0, 0, 0, 0 ]
            extent2[ 0 ] = int( self.extentR2[ 0 ] * screenWidth )
            extent2[ 1 ] = int( self.extentR2[ 1 ] * screenHeight )
            extent2[ 2 ] = int( self.extentR2[ 2 ] * screenWidth )
            extent2[ 3 ] = int( self.extentR2[ 3 ] * screenHeight )
            
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        x = 0
        y = 0
        w = screenWidth
        h = screenHeight
        if extent1:
            x = extent1[ 0 ]
            y = extent1[ 1 ]
            w = extent1[ 2 ]
            h = extent1[ 3 ]
            
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          x,
                          y,
                          w,
                          h,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
       
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )
        
        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        x = 0
        y = 0
        w = screenWidth
        h = screenHeight
        if extent1:
            x = extent1[ 0 ]
            y = extent1[ 1 ]
            w = extent1[ 2 ]
            h = extent1[ 3 ]

        if extent1 and extent2:
            Egl.ResizeWindow( windowIndex,
                              x,
                              y,
                              w,
                              h )
            OpenGLES2.Viewport( 0, 0, w, h )
            
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        x = 0
        y = 0
        w = screenWidth
        h = screenHeight
        if extent2:
            x = extent2[ 0 ]
            y = extent2[ 1 ]
            w = extent2[ 2 ]
            h = extent2[ 3 ]

        if extent1 and extent2:            
            Egl.ResizeWindow( windowIndex,
                              x,
                              y,
                              w,
                              h )
            OpenGLES2.Viewport( 0, 0, w, h )
            
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class TopWindowBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, screenGridX, screenGridY, swapInterval, fullOverlap = False ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.screenGridX  = screenGridX
        self.screenGridY  = screenGridY
        self.swapInterval = swapInterval
        self.fullOverlap  = fullOverlap
        self.name = "OPENGLES2 top windows%s wgrd=%dx%d swpntrvl=%d grd=%dx%dx%d tex=%s" % ( { True : ' fllovrlp', False : '' }[ self.fullOverlap ],
                                                                                             self.screenGridX,
                                                                                             self.screenGridY,
                                                                                             self.swapInterval,
                                                                                             self.gridx,
                                                                                             self.gridy,
                                                                                             self.overdraw,
                                                                                             textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()        
        
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        parentWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( parentWindowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        parentSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 parentSurfaceIndex,
                                 configIndex,
                                 parentWindowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        childSurfaces = []
        childWidth    = 0
        childHeight   = 0

        margin = 4
        if self.fullOverlap:
            margin = 0
            
        if self.screenGridY != 0 and self.screenGridY != 0:
            childWidth    = ( width - margin * 2 ) / self.screenGridX
            childHeight   = ( height - margin * 2 ) / self.screenGridY
            for y in range( self.screenGridY ):
                for x in range( self.screenGridX ):
                    childWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
                    Egl.CreateWindow( childWindowIndex,
                                      'SCT_SCREEN_DEFAULT',
                                      x * childWidth + margin / 2,
                                      y * childHeight + margin / 2,
                                      childWidth - margin / 2, childHeight - margin / 2,
                                      'SCT_COLOR_FORMAT_DEFAULT' )
                   
                    childSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                    Egl.CreateWindowSurface( displayIndex,
                                             childSurfaceIndex,
                                             configIndex,
                                             childWindowIndex,
                                             'EGL_BACK_BUFFER',
                                             'EGL_NONE',
                                             'EGL_NONE' )
            
                    childSurfaces.append( childSurfaceIndex )
            
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         parentSurfaceIndex,
                         parentSurfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, self.swapInterval )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        if len( childSurfaces ) > 0:        
            texture1Setup       = setupTexture( modules,
                                                indexTracker,
                                                childWidth, childHeight,
                                                [ 0.3, 0.3, 1.0, 1.0 ],
                                                [ 0.3, 1.0, 1.0, 1.0 ],
                                                self.textureType )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES2.CheckError( '' )      
               
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        ##########
        Egl.MakeCurrent( displayIndex,
                         parentSurfaceIndex,
                         parentSurfaceIndex,
                         contextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )        
        OpenGLES2.Viewport( 0, 0, width, height )
        
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        useTexture( modules, texture0Setup, 0, program )
       
        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, parentSurfaceIndex )

        if len( childSurfaces ) > 0:
            useTexture( modules, texture1Setup, 0, program )
        
        for childSurface in childSurfaces:
            Egl.MakeCurrent( displayIndex,
                             childSurface,
                             childSurface,
                             contextIndex )

            
            Egl.SwapInterval( displayIndex, self.swapInterval )
            OpenGLES2.Viewport( 0, 0, childWidth, childHeight )

            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

            delta   = 1.0 / float( self.overdraw )
            offset  = 1.0

            for i in range( self.overdraw ):
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawMeshBufferData( modules, meshBufferDataSetup )
                offset -= delta

            Egl.SwapBuffers( displayIndex, childSurface )
        

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ChildWindowBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, screenGridX, screenGridY, swapInterval ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.screenGridX  = screenGridX
        self.screenGridY  = screenGridY
        self.swapInterval = swapInterval
        self.name = "OPENGLES2 child windows wgrd=%dx%d swpntrvl=%d grd=%dx%dx%d tex=%s" % ( self.screenGridX,
                                                                                             self.screenGridY,
                                                                                             self.swapInterval,
                                                                                             self.gridx,
                                                                                             self.gridy,
                                                                                             self.overdraw,
                                                                                             textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()        
        
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        parentWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( parentWindowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        parentSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 parentSurfaceIndex,
                                 configIndex,
                                 parentWindowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        childSurfaces = []
        childWidth    = 0
        childHeight   = 0
        
        if self.screenGridY != 0 and self.screenGridY != 0:
            margin        = 5
            childWidth    = ( width - margin * 2 ) / self.screenGridX
            childHeight   = ( height - margin * 2 ) / self.screenGridY
            for y in range( self.screenGridY ):
                for x in range( self.screenGridX ):
                    childWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
                    Egl.CreateChildWindow( childWindowIndex,
                                           parentWindowIndex,
                                           x * childWidth + margin,
                                           y * childHeight + margin,
                                           childWidth - 1, childHeight - 1,
                                           'SCT_COLOR_FORMAT_DEFAULT' )
                   
                    childSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                    Egl.CreateWindowSurface( displayIndex,
                                             childSurfaceIndex,
                                             configIndex,
                                             childWindowIndex,
                                             'EGL_BACK_BUFFER',
                                             'EGL_NONE',
                                             'EGL_NONE' )
            
                    childSurfaces.append( childSurfaceIndex )
            
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         parentSurfaceIndex,
                         parentSurfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, self.swapInterval )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
        
        if len( childSurfaces ) > 0:        
            texture1Setup       = setupTexture( modules,
                                                indexTracker,
                                                childWidth, childHeight,
                                                [ 0.3, 0.3, 1.0, 1.0 ],
                                                [ 0.3, 1.0, 1.0, 1.0 ],
                                                self.textureType )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES2.CheckError( '' )      
               
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        ##########
        Egl.MakeCurrent( displayIndex,
                         parentSurfaceIndex,
                         parentSurfaceIndex,
                         contextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )        
        OpenGLES2.Viewport( 0, 0, width, height )
        
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        useTexture( modules, texture0Setup, 0, program )
       
        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, parentSurfaceIndex )

        if len( childSurfaces ) > 0:
            useTexture( modules, texture1Setup, 0, program )
        
        for childSurface in childSurfaces:
            Egl.MakeCurrent( displayIndex,
                             childSurface,
                             childSurface,
                             contextIndex )

            Egl.SwapInterval( displayIndex, self.swapInterval )
            OpenGLES2.Viewport( 0, 0, childWidth, childHeight )

            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

            delta   = 1.0 / float( self.overdraw )
            offset  = 1.0

            for i in range( self.overdraw ):
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawMeshBufferData( modules, meshBufferDataSetup )
                offset -= delta

            Egl.SwapBuffers( displayIndex, childSurface )

            
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------        
# ----------------------------------------------------------------------
class PbuffersBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, screenGridX, screenGridY, colorFormat, flush ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.screenGridX = screenGridX
        self.screenGridY = screenGridY
        self.colorFormat = colorFormat
        self.flush       = flush
        self.name = "OPENGLES2 pbuffers pbgrd=%dx%d %s flsh=%s grd=%dx%dx%d tex=%s" % ( self.screenGridX,
                                                                                        self.screenGridY,
                                                                                        pixmapFormatToStr( self.colorFormat ),
                                                                                        self.flush,
                                                                                        self.gridx,
                                                                                        self.gridy,
                                                                                        self.overdraw,
                                                                                        textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )
        
        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        
        a = mapColorFormatToAttributes( self.colorFormat )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       a[ CONFIG_BUFFERSIZE ],
                       a[ CONFIG_REDSIZE ],
                       a[ CONFIG_GREENSIZE ],
                       a[ CONFIG_BLUESIZE ],
                       a[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaces = []

        surfaceWidth    = width / self.gridx
        surfaceHeight   = height / self.gridy
        for y in range( self.gridy ):
            for x in range( self.gridx ):                 
                surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                Egl.CreatePbufferSurface( displayIndex,
                                          surfaceIndex,
                                          configIndex,
                                          surfaceWidth,
                                          surfaceHeight,
                                          'EGL_NO_TEXTURE',
                                          'EGL_NO_TEXTURE',
                                          'OFF',
                                          'OFF',                                          
                                          'EGL_NONE',
                                          'EGL_NONE' )
            
                surfaces.append( surfaceIndex )
            
        gles2ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           gles2ContextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         surfaces[ 0 ],
                         surfaces[ 0 ],
                         gles2ContextIndex )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                surfaceWidth, surfaceHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
        
        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        if self.flush == READPIXELS:
            dataIndex           = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            OpenGLES2.CreateData( dataIndex, surfaceWidth * surfaceHeight * 4 ) # RGBA8888
        elif self.flush == COPYBUFFERS:
            pixmapIndex         = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
            Egl.CreatePixmap( pixmapIndex, surfaceWidth, surfaceHeight, self.colorFormat, -1 )
        
        OpenGLES2.CheckError( '' )      
             
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        # -----
        for s in surfaces:
            Egl.MakeCurrent( displayIndex,
                             s,
                             s,
                             gles2ContextIndex )
            Egl.SwapInterval( displayIndex, 0 )
            
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

            delta   = 1.0 / float( self.overdraw )
            offset  = 1.0

            for i in range( self.overdraw ):
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawMeshBufferData( modules, meshBufferDataSetup )
                offset -= delta

            if self.flush == READPIXELS:
                OpenGLES2.ReadPixels( dataIndex,
                                      0, 0,
                                      surfaceWidth,
                                      surfaceHeight )
            elif self.flush == COPYBUFFERS:
                Egl.CopyBuffers( displayIndex, s, pixmapIndex )
            elif self.flush == FINISH:
                OpenGLES2.Finish()
            else:
                raise 'Invalid flush'

            
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class RotateScreenBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, orientation, swapInterval ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.orientation  = orientation
        self.swapInterval = swapInterval       
        self.name = "OPENGLES2 screen orientation %s swpntrvl=%d grd=%dx%dx%d tex=%s" % ( orientationToStr( self.orientation ),
                                                                                          self.swapInterval,
                                                                                          self.gridx,
                                                                                          self.gridy,
                                                                                          self.overdraw,
                                                                                          textureTypeToStr( self.textureType ), )
        
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()        
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        x = 0
        y = 0
        w = width
        h = height
       
        if defaultOrientation( width, height ) != self.orientation:
            w = height
            h = width
            Egl.ScreenOrientation( 'SCT_SCREEN_DEFAULT', self.orientation )
            
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          x,
                          y,
                          w,
                          h,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
       
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )
        
        Egl.SwapInterval( displayIndex, self.swapInterval )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                w, h,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES2.Scissor( w / 2 - 4, 0, 8, h )
        
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.ClearColor( [ 0, 0, 0, 1 ] )        
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        # Draw a up-down center line in blue to make screen rotation visible.
        OpenGLES2.Enable( 'GL_SCISSOR_TEST' )
        OpenGLES2.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        OpenGLES2.Disable( 'GL_SCISSOR_TEST' )
        
        Egl.SwapBuffers( displayIndex, surfaceIndex )

        
######################################################################
def setupRoundTexture( modules, indexTracker, sz, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    p = []
    p += c1
    p += c2
    p.append( sz )
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateShapeTextureData( textureDataIndex, 'OPENGLES2_SHAPE_ROUND', p, 'OFF', textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class WindowCompositionBenchmark( Benchmark ):
    def __init__( self, opacity, ppaEnabled, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.opacity     = opacity
        self.ppaEnabled  = ppaEnabled
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = "OPENGLES2 window composition opacity=%.2f%s grd=%dx%dx%d tex=%s" % ( self.opacity,
                                                                                          { True : ' with per-pixel alpha', False : '' }[ self.ppaEnabled ],
                                                                                          self.gridx,
                                                                                          self.gridy,
                                                                                          self.overdraw,
                                                                                          textureTypeToStr( self.textureType ), )
        
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()        
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        x = 0
        y = 0
        w = width
        h = height
                  
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          x,
                          y,
                          w,
                          h,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        configIndex = indexTracker.allocIndex( 'ELG_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
       
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )
        
        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupRoundTexture( modules,
                                                     indexTracker,
                                                     max( w, h ),
                                                     [ 0.2, 1.0, 0.2, 1.0 ],
                                                     [ 1.0, 0.2, 0.2, 0.3 ],
                                                     self.textureType )
       
        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES2.CheckError( '' )

        if self.opacity < 1.0:
            Egl.WindowOpacity( windowIndex, self.opacity )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()
           
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )      

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class WindowFormatBenchmark( Benchmark ):
    def __init__( self, format, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.format      = format
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = "OPENGLES2 window format %s grd=%dx%dx%d tex=%s" % ( pixmapFormatToStr( self.format ),
                                                                         self.gridx,
                                                                         self.gridy,
                                                                         self.overdraw,
                                                                         textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()        

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, screenWidth, screenHeight, self.format )

        if self.format == 'SCT_COLOR_FORMAT_DEFAULT':
            attrs = target.getDefaultAttributes( API_OPENGLES2  )
        else:
            attrs = mapColorFormatToAttributes( self.format )       
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
       
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )
        
        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()
           
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )
