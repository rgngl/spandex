#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
WINDOW  = 'window'
PBUFFER = 'pbuffer'

######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ReadPixelsBenchmark( Benchmark ):  
    def __init__( self, width, height ):
        Benchmark.__init__( self)
        self.width  = width
        self.height = height
        self.name = 'OPENGLES2 readpixels %dx%d' % ( self.width,
                                                     self.height, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()
           
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()
        
        state                   = setupGL2( modules, indexTracker, target )            
        dataIndex               = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.CreateData( dataIndex, self.width * self.height * 4 ) # RGBA = 4

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        target.swapBuffers( state )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )        
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.ReadPixels( dataIndex, 0, 0, self.width, self.height )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CopyBuffersWindowBenchmark( Benchmark ):  
    def __init__( self, width, height ):
        Benchmark.__init__( self)
        self.width  = width
        self.height = height

        self.name = 'OPENGLES2 copybuffers window %dx%d' % ( self.width,
                                                             self.height, )
        
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()
           
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT',
                          0,
                          0,
                          self.width,
                          self.height,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        surfaceBits = [ 'EGL_WINDOW_BIT' ]
        attrs = target.getDefaultAttributes( API_OPENGLES2 )
            
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       surfaceBits,
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )
        
        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex,
                          self.width,
                          self.height,
                          mapAttributesToColorFormat( attrs ),
                          -1 )
        
        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )        
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )        
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CopyBuffers( displayIndex, surfaceIndex, pixmapIndex )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CopyBuffersPbufferBenchmark( Benchmark ):  
    def __init__( self, width, height, format = None ):
        Benchmark.__init__( self)
        self.width  = width
        self.height = height
        self.format = format

        self.name = 'OPENGLES2 copybuffers pbuffer %dx%d %s' % ( self.width,
                                                                 self.height,
                                                                 pixmapFormatToStr( self.format ), )
        
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()
           
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        
        surfaceBits = [ 'EGL_PBUFFER_BIT' ]
        attrs = mapColorFormatToAttributes( self.format )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       surfaceBits,
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        Egl.CreatePbufferSurface( displayIndex,
                                  surfaceIndex,
                                  configIndex,
                                  self.width,
                                  self.height,
                                  'EGL_NO_TEXTURE',
                                  'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )
            
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )
        
        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex,
                          self.width,
                          self.height,
                          mapAttributesToColorFormat( attrs ),
                          -1 )
        
        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )        
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CopyBuffers( displayIndex, surfaceIndex, pixmapIndex )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DrawAndReadPixelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, width, height, readIndex ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.width       = width
        self.height      = height
        self.readIndex   = readIndex

        if self.readIndex >= 0:
            ri = str( self.readIndex ) + '/' + str( self.overdraw )
        else:
            ri = 'noread'
            
        self.name = 'OPENGLES2 readpixels.n.draw %s %dx%d grd=%dx%dx%d tex=%s' % ( ri,
                                                                                   self.width,
                                                                                   self.height,
                                                                                   self.gridx,
                                                                                   self.gridy,
                                                                                   self.overdraw,
                                                                                   textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 0.0, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )
      
        dataIndex               = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.CreateData( dataIndex, self.width * self.height * 4 ) # RGBA = 4

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.CheckError( '' )        
      
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            if i == self.readIndex:
                OpenGLES2.ReadPixels( dataIndex,
                                      0, 0,
                                      self.width, self.height )

            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )
