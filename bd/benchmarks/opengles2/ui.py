#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
BACKGROUND      = 0
PANELS          = 1
GROUPS          = 2
ICONS           = 3
 
UPLOAD          = 4
DRAW            = 5
UPLOAD_AND_DRAW = 6

PANELSX         = 2
PANELSY         = 1
GROUPSX         = 2
GROUPSY         = 3
ICONSX          = 4
ICONSY          = 6

######################################################################
def toPot( v, pot ):
    if pot:
        nv = 2 ** math.ceil( math.log( v, 2 ) )
        return ( int( nv ), v / nv, )
    else:
        return ( int( v ), 1.0, )

######################################################################
def potToString( pot ):
    if pot:
        return 'POT'
    else:
        return 'NPOT'

######################################################################
def actionToString( action ):
    if action == UPLOAD:
        return 'UPLD'
    if action == DRAW:
        return 'DRW'
    if action == UPLOAD_AND_DRAW:
        return 'UPLD.N.DRW'
    else:
        raise 'Unexpected action %s' % str( action )

######################################################################
def layersToString( layers ):
    s = []
    if BACKGROUND in layers:
        s += [ 'BCKGRND' ]
    if PANELS in layers:
        s += [ 'PNL' ]
    if GROUPS in layers:
        s += [ 'GRPS' ]
    if ICONS in layers:
        s += [ 'ICNS' ]

    return '+'.join( s )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class UiBenchmark( Benchmark ):
    def __init__( self, action, layers, pot, format, swapInterval ):
        Benchmark.__init__( self )
        self.action       = action
        self.layers       = layers
        self.pot          = pot
        self.format       = format
        self.swapInterval = swapInterval
        self.name = "OPENGLES2 ui actn=%s layers=%s pot=%s frmt=%s swpntvl=%d" % ( actionToString( self.action ),
                                                                                   layersToString( self.layers ),
                                                                                   potToString( self.pot ),
                                                                                   textureTypeToStr( self.format ),
                                                                                   self.swapInterval, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # Shader source codes
        vertexShaderSource      = '"attribute vec4 position_in;attribute vec4 uv_in;uniform mat4 mvp;varying vec4 uv;void main(){gl_Position = mvp * position_in;uv = uv_in;}"'
        fragmentShaderSource    = '"precision mediump float;uniform sampler2D tex0;varying vec4 uv;void main(){gl_FragColor = texture2D(tex0, uv.xy);}"'

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
       
        state = setupGL2( modules, indexTracker, target )            
       
        backgroundWidth  = screenWidth
        backgroundHeight = screenHeight
        panelsX          = PANELSX
        panelsY          = PANELSY
        panelWidth       = screenWidth / panelsX
        panelHeight      = screenHeight / panelsY
        groupsX          = GROUPSX
        groupsY          = GROUPSY
        groupWidth       = panelWidth / groupsX
        groupHeight      = panelHeight / groupsY
        iconsX           = ICONSX
        iconsY           = ICONSY
        iconWidth        = panelWidth / iconsX
        iconHeight       = panelHeight / iconsY

        backgroundTextureWidth,     backgroundTextureS  = toPot( backgroundWidth, self.pot )
        backgroundTextureHeight,    backgroundTextureT  = toPot( backgroundHeight, self.pot )
        panelTextureWidth,          panelTextureS       = toPot( panelWidth, self.pot )
        panelTextureHeight,         panelTextureT       = toPot( panelHeight, self.pot )
        groupTextureWidth,          groupTextureS       = toPot( groupWidth, self.pot )
        groupTextureHeight,         groupTextureT       = toPot( groupHeight, self.pot )
        iconTextureWidth,           iconTextureS        = toPot( iconWidth, self.pot )
        iconTextureHeight,          iconTextureT        = toPot( iconHeight, self.pot )

        ######################################################################
        # Create texture data                                                #
        ######################################################################
        
        if BACKGROUND in self.layers:
            backgroundTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            backgroundTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            
            OpenGLES2.CreateCheckerTextureData( backgroundTextureDataIndex,
                                                [ 1.0, 1.0, 1.0, 1.0 ],
                                                [ 0.9, 0.9, 0.9, 1.0 ],
                                                2, 2,
                                                backgroundTextureWidth,
                                                backgroundTextureHeight,
                                                'OFF',
                                                self.format )

            
            OpenGLES2.GenTexture( backgroundTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_NEAREST',
                                    'GL_NEAREST',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE' )
            if self.action == DRAW:
                OpenGLES2.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )
            
        if PANELS in self.layers:
            panelTextureDataIndices = []
            panelTextureIndices     = []
            panelOrigins            = []

            total = panelsX * panelsY + 1
            for y in range( panelsY ):
                for x in range( panelsX ):
                    textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                    textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                    
                    panelTextureDataIndices += [ textureDataIndex ]
                    panelTextureIndices     += [ textureIndex ]
                    panelOrigins            += [ ( panelWidth * x, panelHeight * y, ) ]

                    r = 1.0 * ( ( x + ( y * panelsX ) + 1 ) / float( total ) )
                   
                    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                        [ 0.0, r, 0.0, 0.5 ],
                                                        [ 0.2, r, 0.2, 0.5 ],
                                                        2, 2, 
                                                        panelTextureWidth,
                                                        panelTextureHeight,
                                                        'OFF',
                                                        self.format )

                    
                    OpenGLES2.GenTexture( textureIndex )
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                            'GL_NEAREST',
                                            'GL_NEAREST',
                                            'GL_CLAMP_TO_EDGE',
                                            'GL_CLAMP_TO_EDGE' )
                    if self.action == DRAW:
                        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                   
        if GROUPS in self.layers:
            groupTextureDataIndices = []
            groupTextureIndices     = []
            groupOrigins            = []

            total = groupsX * groupsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( groupsY ):
                        for x in range( groupsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                    
                            groupTextureDataIndices += [ textureDataIndex ]
                            groupTextureIndices     += [ textureIndex ]
                            groupOrigins            += [ ( px * panelWidth + groupWidth * x,
                                                           py * panelHeight + groupHeight * y, ) ]

                            r = 1.0 * ( x + ( y * groupsX ) + 1 ) / float( total )

                            OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                                [ 0.0, 0.0, r, 0.5 ],
                                                                [ 0.2, 0.2, r, 0.5 ],
                                                                2, 2, 
                                                                groupTextureWidth,
                                                                groupTextureHeight,
                                                                'OFF',
                                                                self.format )
                            
                            OpenGLES2.GenTexture( textureIndex )
                            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                                    'GL_NEAREST',
                                                    'GL_NEAREST',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'GL_CLAMP_TO_EDGE' )
                            
                            if self.action == DRAW:
                                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                                               
        if ICONS in self.layers:
            iconTextureDataIndices  = []
            iconTextureIndices      = []
            iconOrigins             = []

            total = iconsX * iconsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( iconsY ):
                        for x in range( iconsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                    
                            iconTextureDataIndices  += [ textureDataIndex ]
                            iconTextureIndices      += [ textureIndex ]
                            iconOrigins             += [ ( px * panelWidth + iconWidth * x,
                                                           py * panelHeight + iconHeight * y, ) ]

                            r = 1.0 * ( x + ( y * iconsX ) + 1 ) / float( total )

                            OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                                [ 0.3, 0.3, r, 0.5 ],
                                                                [ 0.2, 0.2, r, 0.5 ],
                                                                2, 2, 
                                                                iconTextureWidth,
                                                                iconTextureHeight,
                                                                'OFF',
                                                                self.format )
                            
                            OpenGLES2.GenTexture( textureIndex )
                            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                                    'GL_NEAREST',
                                                    'GL_NEAREST',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'GL_CLAMP_TO_EDGE' )
                            if self.action == DRAW:
                                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        ######################################################################
        # Create geometry                                                    #
        ######################################################################
                                
        if BACKGROUND in self.layers:              
            backgroundTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                                   2,
                                                                   [ backgroundTextureS,
                                                                     backgroundTextureT ] ) ]
        
            background = MeshStripPlane( 2,
                                         2,
                                         MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                         None,
                                         None,
                                         backgroundTexCoordAttribute,
                                         MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                         None,
                                         MESH_CCW_WINDING,
                                         False )

            background.translate( [ -0.5, -0.5, 0 ] )
            background.scale( [ 2.0 * backgroundWidth / float( screenWidth ),
                                2.0 * backgroundHeight / float( screenHeight ) ] )
                   
            backgroundVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundVertexArrayIndex,
                                   background.vertexGLType )
            OpenGLES2.AppendToArray( backgroundVertexArrayIndex, background.vertices )
            
            backgroundTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundTexCoordArrayIndex,
                                   background.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( backgroundTexCoordArrayIndex, background.texCoords[ 0 ] )
            
            backgroundIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundIndexArrayIndex, background.indexGLType );
            OpenGLES2.AppendToArray( backgroundIndexArrayIndex, background.indices )

        if PANELS in self.layers:              
            panelTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ panelTextureS,
                                                                panelTextureT ] ) ]
        
            panel = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    panelTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            panel.translate( [ -0.5, -0.5, 0 ] )
            panel.scale( [ 2.0 * panelWidth / float( screenWidth ),
                           2.0 * panelHeight / float( screenHeight ) ] )
           
            panelVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelVertexArrayIndex,
                                   panel.vertexGLType )
            OpenGLES2.AppendToArray( panelVertexArrayIndex, panel.vertices )
            
            panelTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelTexCoordArrayIndex,
                                   panel.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( panelTexCoordArrayIndex, panel.texCoords[ 0 ] )
            
            panelIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelIndexArrayIndex, panel.indexGLType );
            OpenGLES2.AppendToArray( panelIndexArrayIndex, panel.indices )

        if GROUPS in self.layers:              
            groupTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ groupTextureS,
                                                                groupTextureT ] ) ]
        
            group = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    groupTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            group.translate( [ -0.5, -0.5, 0 ] )
            group.scale( [ 2.0 * groupWidth / float( screenWidth ),
                           2.0 * groupHeight / float( screenHeight ) ] )
           
            groupVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupVertexArrayIndex,
                                   group.vertexGLType )
            OpenGLES2.AppendToArray( groupVertexArrayIndex, group.vertices )
            
            groupTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupTexCoordArrayIndex,
                                   group.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( groupTexCoordArrayIndex, group.texCoords[ 0 ] )
            
            groupIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupIndexArrayIndex, group.indexGLType );
            OpenGLES2.AppendToArray( groupIndexArrayIndex, group.indices )

        if ICONS in self.layers:              
            iconTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                             2,
                                                             [ iconTextureS,
                                                               iconTextureT ] ) ]
        
            icon = MeshStripPlane( 2,
                                   2,
                                   MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                   None,
                                   None,
                                   iconTexCoordAttribute,
                                   MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                   None,
                                   MESH_CCW_WINDING,
                                   False )

            icon.translate( [ -0.5, -0.5, 0 ] )
            icon.scale( [ 2.0 * iconWidth / float( screenWidth ),
                          2.0 * iconHeight / float( screenHeight ) ] )
           
            iconVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconVertexArrayIndex,
                                   icon.vertexGLType )
            OpenGLES2.AppendToArray( iconVertexArrayIndex, icon.vertices )
            
            iconTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconTexCoordArrayIndex,
                                   icon.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( iconTexCoordArrayIndex, icon.texCoords[ 0 ] )
            
            iconIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconIndexArrayIndex, icon.indexGLType );
            OpenGLES2.AppendToArray( iconIndexArrayIndex, icon.indices )

            
        ######################################################################
        # Shaders                                                            #
        ######################################################################
            
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )
        
        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )
        
        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      vertexLocationIndex,
                                      "position_in" )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      texCoordLocationIndex,
                                      "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.ValidateProgram( programIndex )
        OpenGLES2.UseProgram( programIndex )        
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        mVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mVPLocationIndex, programIndex, 'mvp' )
            
        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.CheckError( '' )

        target.swapInterval( state, self.swapInterval )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        # Clear the screen if drawing is requested and the layers do not include background
        if not BACKGROUND in self.layers:
            if self.action == DRAW or self.action == UPLOAD_AND_DRAW:       
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        # Do texture data upload if requested
        if self.action == UPLOAD or self.action == UPLOAD_AND_DRAW:
            if BACKGROUND in self.layers:           
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                OpenGLES2.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )

            if PANELS in self.layers:
                for i in range( len( panelTextureDataIndices ) ):
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                    OpenGLES2.TexImage2D( panelTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if GROUPS in self.layers:
                for i in range( len( groupTextureDataIndices ) ):
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                    OpenGLES2.TexImage2D( groupTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if ICONS in self.layers:
                for i in range( len( iconTextureDataIndices ) ):
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                    OpenGLES2.TexImage2D( iconTextureDataIndices[ i ], 'GL_TEXTURE_2D' )
                    
        # Draw if drawing is requested                
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            if BACKGROUND in self.layers:
                OpenGLES2.Disable( 'GL_BLEND' )
                
                OpenGLES2.UniformMatrix( mVPLocationIndex,
                                         1,
                                         [ 1.0, 0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0, 0.0,
                                           0.0, 0.0, 1.0, 0.0,
                                           0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                               backgroundVertexArrayIndex,
                                               2,
                                               'OFF',
                                               0 )
                OpenGLES2.VertexAttribPointer( texCoordLocationIndex,
                                               backgroundTexCoordArrayIndex,
                                               2,
                                               'OFF',
                                               0 )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                
                OpenGLES2.DrawElements( background.glMode,
                                        len( background.indices ),
                                        0,
                                        backgroundIndexArrayIndex )    

            if PANELS in self.layers:
                OpenGLES2.Enable( 'GL_BLEND' )
                
                for i in range( len( panelTextureIndices ) ):
                    origin = panelOrigins[ i ]
                    tx = ( -1.0 + panelWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + panelHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )
                    
                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                                   panelVertexArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex,
                                                   panelTexCoordArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                    
                    OpenGLES2.DrawElements( panel.glMode,
                                            len( panel.indices ),
                                            0,
                                            panelIndexArrayIndex )    

            if GROUPS in self.layers:
                OpenGLES2.Enable( 'GL_BLEND' )
                
                for i in range( len( groupTextureIndices ) ):
                    origin = groupOrigins[ i ]
                    tx = ( -1.0 + groupWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + groupHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )
                    
                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                                   groupVertexArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex,
                                                   groupTexCoordArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                    
                    OpenGLES2.DrawElements( group.glMode,
                                            len( group.indices ),
                                            0,
                                            groupIndexArrayIndex )    


            if ICONS in self.layers:
                OpenGLES2.Enable( 'GL_BLEND' )
                
                for i in range( len( iconTextureIndices ) ):
                    origin = iconOrigins[ i ]
                    tx = ( -1.0 + iconWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + iconHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )
                    
                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                                   iconVertexArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex,
                                                   iconTexCoordArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                    
                    OpenGLES2.DrawElements( icon.glMode,
                                            len( icon.indices ),
                                            0,
                                            iconIndexArrayIndex )    
                    
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            target.swapBuffers( state )

            
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class Invert4x4MatrixBenchmark( Benchmark ):  
    def __init__( self, count, iterations ):
        Benchmark.__init__( self )
        self.count      = count
        self.iterations = iterations
        self.name = "OPENGLES2 invert 4x4 matrix mtrx.cnt=%d itrtns=%d"  % ( self.count,
                                                                             self.iterations, )
       
    def build( self, target, modules ):
        Cpu = modules[ 'CPU' ]
        
        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Cpu.Invert4x4Matrix( self.count, self.iterations )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ParallelUiBenchmark( Benchmark ):
    def __init__( self, action, layers, pot, format, matrices ):
        Benchmark.__init__( self )
        self.action   = action
        self.layers   = layers
        self.pot      = pot
        self.format   = format
        self.matrices = matrices
        self.name = "OPENGLES2 parallel ui actn=%s layers=%s pot=%s frmt=%s mtrcs=%d" % ( actionToString( self.action ),
                                                                                          layersToString( self.layers ),
                                                                                          potToString( self.pot ),
                                                                                          textureTypeToStr( self.format ),
                                                                                          self.matrices, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Cpu       = modules[ 'CPU' ]

        indexTracker = IndexTracker()

        # Shader source codes
        vertexShaderSource      = '"attribute vec4 position_in;attribute vec4 uv_in;uniform mat4 mvp;varying vec4 uv;void main(){gl_Position = mvp * position_in;uv = uv_in;}"'
        fragmentShaderSource    = '"precision mediump float;uniform sampler2D tex0;varying vec4 uv;void main(){gl_FragColor = texture2D(tex0, uv.xy);}"'

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
       
        state = setupGL2( modules, indexTracker, target )            
       
        backgroundWidth  = screenWidth
        backgroundHeight = screenHeight
        panelsX          = PANELSX
        panelsY          = PANELSY
        panelWidth       = screenWidth / panelsX
        panelHeight      = screenHeight / panelsY
        groupsX          = GROUPSX
        groupsY          = GROUPSY
        groupWidth       = panelWidth / groupsX
        groupHeight      = panelHeight / groupsY
        iconsX           = ICONSX
        iconsY           = ICONSY
        iconWidth        = panelWidth / iconsX
        iconHeight       = panelHeight / iconsY

        backgroundTextureWidth,     backgroundTextureS  = toPot( backgroundWidth, self.pot )
        backgroundTextureHeight,    backgroundTextureT  = toPot( backgroundHeight, self.pot )
        panelTextureWidth,          panelTextureS       = toPot( panelWidth, self.pot )
        panelTextureHeight,         panelTextureT       = toPot( panelHeight, self.pot )
        groupTextureWidth,          groupTextureS       = toPot( groupWidth, self.pot )
        groupTextureHeight,         groupTextureT       = toPot( groupHeight, self.pot )
        iconTextureWidth,           iconTextureS        = toPot( iconWidth, self.pot )
        iconTextureHeight,          iconTextureT        = toPot( iconHeight, self.pot )

        ######################################################################
        # Create texture data                                                #
        ######################################################################
        
        if BACKGROUND in self.layers:
            backgroundTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            backgroundTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            
            OpenGLES2.CreateCheckerTextureData( backgroundTextureDataIndex,
                                                [ 1.0, 1.0, 1.0, 1.0 ],
                                                [ 0.9, 0.9, 0.9, 1.0 ],
                                                2, 2,
                                                backgroundTextureWidth,
                                                backgroundTextureHeight,
                                                'OFF',
                                                self.format )

            
            OpenGLES2.GenTexture( backgroundTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_NEAREST',
                                    'GL_NEAREST',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE' )
            if self.action == DRAW:
                OpenGLES2.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )
            
        if PANELS in self.layers:
            panelTextureDataIndices = []
            panelTextureIndices     = []
            panelOrigins            = []

            total = panelsX * panelsY + 1
            for y in range( panelsY ):
                for x in range( panelsX ):
                    textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                    textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                    
                    panelTextureDataIndices += [ textureDataIndex ]
                    panelTextureIndices     += [ textureIndex ]
                    panelOrigins            += [ ( panelWidth * x, panelHeight * y, ) ]

                    r = 1.0 * ( ( x + ( y * panelsX ) + 1 ) / float( total ) )
                   
                    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                        [ 0.0, r, 0.0, 0.5 ],
                                                        [ 0.2, r, 0.2, 0.5 ],
                                                        2, 2, 
                                                        panelTextureWidth,
                                                        panelTextureHeight,
                                                        'OFF',
                                                        self.format )

                    
                    OpenGLES2.GenTexture( textureIndex )
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                            'GL_NEAREST',
                                            'GL_NEAREST',
                                            'GL_CLAMP_TO_EDGE',
                                            'GL_CLAMP_TO_EDGE' )
                    if self.action == DRAW:
                        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                   
        if GROUPS in self.layers:
            groupTextureDataIndices = []
            groupTextureIndices     = []
            groupOrigins            = []

            total = groupsX * groupsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( groupsY ):
                        for x in range( groupsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                    
                            groupTextureDataIndices += [ textureDataIndex ]
                            groupTextureIndices     += [ textureIndex ]
                            groupOrigins            += [ ( px * panelWidth + groupWidth * x,
                                                           py * panelHeight + groupHeight * y, ) ]

                            r = 1.0 * ( x + ( y * groupsX ) + 1 ) / float( total )

                            OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                                [ 0.0, 0.0, r, 0.5 ],
                                                                [ 0.2, 0.2, r, 0.5 ],
                                                                2, 2, 
                                                                groupTextureWidth,
                                                                groupTextureHeight,
                                                                'OFF',
                                                                self.format )
                            
                            OpenGLES2.GenTexture( textureIndex )
                            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                                    'GL_NEAREST',
                                                    'GL_NEAREST',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'GL_CLAMP_TO_EDGE' )
                            
                            if self.action == DRAW:
                                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                                               
        if ICONS in self.layers:
            iconTextureDataIndices  = []
            iconTextureIndices      = []
            iconOrigins             = []

            total = iconsX * iconsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( iconsY ):
                        for x in range( iconsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                    
                            iconTextureDataIndices  += [ textureDataIndex ]
                            iconTextureIndices      += [ textureIndex ]
                            iconOrigins             += [ ( px * panelWidth + iconWidth * x,
                                                           py * panelHeight + iconHeight * y, ) ]

                            r = 1.0 * ( x + ( y * iconsX ) + 1 ) / float( total )

                            OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                                [ 0.3, 0.3, r, 0.5 ],
                                                                [ 0.2, 0.2, r, 0.5 ],
                                                                2, 2, 
                                                                iconTextureWidth,
                                                                iconTextureHeight,
                                                                'OFF',
                                                                self.format )
                            
                            OpenGLES2.GenTexture( textureIndex )
                            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                                    'GL_NEAREST',
                                                    'GL_NEAREST',
                                                    'GL_CLAMP_TO_EDGE',
                                                    'GL_CLAMP_TO_EDGE' )
                            if self.action == DRAW:
                                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        ######################################################################
        # Create geometry                                                    #
        ######################################################################
                                
        if BACKGROUND in self.layers:              
            backgroundTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                                   2,
                                                                   [ backgroundTextureS,
                                                                     backgroundTextureT ] ) ]
        
            background = MeshStripPlane( 2,
                                         2,
                                         MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                         None,
                                         None,
                                         backgroundTexCoordAttribute,
                                         MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                         None,
                                         MESH_CCW_WINDING,
                                         False )

            background.translate( [ -0.5, -0.5, 0 ] )
            background.scale( [ 2.0 * backgroundWidth / float( screenWidth ),
                                2.0 * backgroundHeight / float( screenHeight ) ] )
                   
            backgroundVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundVertexArrayIndex,
                                   background.vertexGLType )
            OpenGLES2.AppendToArray( backgroundVertexArrayIndex, background.vertices )
            
            backgroundTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundTexCoordArrayIndex,
                                   background.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( backgroundTexCoordArrayIndex, background.texCoords[ 0 ] )
            
            backgroundIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundIndexArrayIndex, background.indexGLType );
            OpenGLES2.AppendToArray( backgroundIndexArrayIndex, background.indices )

        if PANELS in self.layers:              
            panelTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ panelTextureS,
                                                                panelTextureT ] ) ]
        
            panel = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    panelTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            panel.translate( [ -0.5, -0.5, 0 ] )
            panel.scale( [ 2.0 * panelWidth / float( screenWidth ),
                           2.0 * panelHeight / float( screenHeight ) ] )
           
            panelVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelVertexArrayIndex,
                                   panel.vertexGLType )
            OpenGLES2.AppendToArray( panelVertexArrayIndex, panel.vertices )
            
            panelTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelTexCoordArrayIndex,
                                   panel.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( panelTexCoordArrayIndex, panel.texCoords[ 0 ] )
            
            panelIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelIndexArrayIndex, panel.indexGLType );
            OpenGLES2.AppendToArray( panelIndexArrayIndex, panel.indices )

        if GROUPS in self.layers:              
            groupTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ groupTextureS,
                                                                groupTextureT ] ) ]
        
            group = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    groupTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            group.translate( [ -0.5, -0.5, 0 ] )
            group.scale( [ 2.0 * groupWidth / float( screenWidth ),
                           2.0 * groupHeight / float( screenHeight ) ] )
           
            groupVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupVertexArrayIndex,
                                   group.vertexGLType )
            OpenGLES2.AppendToArray( groupVertexArrayIndex, group.vertices )
            
            groupTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupTexCoordArrayIndex,
                                   group.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( groupTexCoordArrayIndex, group.texCoords[ 0 ] )
            
            groupIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupIndexArrayIndex, group.indexGLType );
            OpenGLES2.AppendToArray( groupIndexArrayIndex, group.indices )

        if ICONS in self.layers:              
            iconTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                             2,
                                                             [ iconTextureS,
                                                               iconTextureT ] ) ]
        
            icon = MeshStripPlane( 2,
                                   2,
                                   MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                   None,
                                   None,
                                   iconTexCoordAttribute,
                                   MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                   None,
                                   MESH_CCW_WINDING,
                                   False )

            icon.translate( [ -0.5, -0.5, 0 ] )
            icon.scale( [ 2.0 * iconWidth / float( screenWidth ),
                          2.0 * iconHeight / float( screenHeight ) ] )
           
            iconVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconVertexArrayIndex,
                                   icon.vertexGLType )
            OpenGLES2.AppendToArray( iconVertexArrayIndex, icon.vertices )
            
            iconTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconTexCoordArrayIndex,
                                   icon.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( iconTexCoordArrayIndex, icon.texCoords[ 0 ] )
            
            iconIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconIndexArrayIndex, icon.indexGLType );
            OpenGLES2.AppendToArray( iconIndexArrayIndex, icon.indices )

            
        ######################################################################
        # Shaders                                                            #
        ######################################################################
            
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )
        
        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )
        
        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      vertexLocationIndex,
                                      "position_in" )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      texCoordLocationIndex,
                                      "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.ValidateProgram( programIndex )
        OpenGLES2.UseProgram( programIndex )        
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        mVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mVPLocationIndex, programIndex, 'mvp' )
            
        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.CheckError( '' )
        
        target.swapInterval( state, 1 )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.matrices > 0:
            Cpu.Invert4x4Matrix( self.matrices, 1 )
        
        # Clear the screen if drawing is requested and the layers do not include background
        if not BACKGROUND in self.layers:
            if self.action == DRAW or self.action == UPLOAD_AND_DRAW:       
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        # Do texture data upload if requested
        if self.action == UPLOAD or self.action == UPLOAD_AND_DRAW:
            if BACKGROUND in self.layers:           
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                OpenGLES2.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )

            if PANELS in self.layers:
                for i in range( len( panelTextureDataIndices ) ):
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                    OpenGLES2.TexImage2D( panelTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if GROUPS in self.layers:
                for i in range( len( groupTextureDataIndices ) ):
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                    OpenGLES2.TexImage2D( groupTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if ICONS in self.layers:
                for i in range( len( iconTextureDataIndices ) ):
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                    OpenGLES2.TexImage2D( iconTextureDataIndices[ i ], 'GL_TEXTURE_2D' )
                    
        # Draw if drawing is requested                
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            if BACKGROUND in self.layers:
                OpenGLES2.Disable( 'GL_BLEND' )
                
                OpenGLES2.UniformMatrix( mVPLocationIndex,
                                         1,
                                         [ 1.0, 0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0, 0.0,
                                           0.0, 0.0, 1.0, 0.0,
                                           0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                               backgroundVertexArrayIndex,
                                               2,
                                               'OFF',
                                               0 )
                OpenGLES2.VertexAttribPointer( texCoordLocationIndex,
                                               backgroundTexCoordArrayIndex,
                                               2,
                                               'OFF',
                                               0 )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                
                OpenGLES2.DrawElements( background.glMode,
                                        len( background.indices ),
                                        0,
                                        backgroundIndexArrayIndex )    

            if PANELS in self.layers:
                OpenGLES2.Enable( 'GL_BLEND' )
                
                for i in range( len( panelTextureIndices ) ):
                    origin = panelOrigins[ i ]
                    tx = ( -1.0 + panelWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + panelHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )
                    
                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                                   panelVertexArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex,
                                                   panelTexCoordArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                    
                    OpenGLES2.DrawElements( panel.glMode,
                                            len( panel.indices ),
                                            0,
                                            panelIndexArrayIndex )    

            if GROUPS in self.layers:
                OpenGLES2.Enable( 'GL_BLEND' )
                
                for i in range( len( groupTextureIndices ) ):
                    origin = groupOrigins[ i ]
                    tx = ( -1.0 + groupWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + groupHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )
                    
                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                                   groupVertexArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex,
                                                   groupTexCoordArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                    
                    OpenGLES2.DrawElements( group.glMode,
                                            len( group.indices ),
                                            0,
                                            groupIndexArrayIndex )    


            if ICONS in self.layers:
                OpenGLES2.Enable( 'GL_BLEND' )
                
                for i in range( len( iconTextureIndices ) ):
                    origin = iconOrigins[ i ]
                    tx = ( -1.0 + iconWidth / float( screenWidth ) ) + ( 2.0 * origin[ 0 ] / float ( screenWidth ) )
                    ty = ( -1.0 + iconHeight / float( screenHeight ) ) + ( 2.0 * origin[ 1 ] / float ( screenHeight ) )
                    
                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                                   iconVertexArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex,
                                                   iconTexCoordArrayIndex,
                                                   2,
                                                   'OFF',
                                                   0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                    
                    OpenGLES2.DrawElements( icon.glMode,
                                            len( icon.indices ),
                                            0,
                                            iconIndexArrayIndex )    
                    
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            target.swapBuffers( state )

