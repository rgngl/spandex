#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *
    
######################################################################
def createPlane( gridx, gridy, tcx = 1.0, tcy = 1.0 ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ tcx, tcy ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class TextureTypeBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 texture type %s grd=%dx%dx%d' % ( textureTypeToStr( self.textureType ),
                                                                 self.gridx,
                                                                 self.gridy,
                                                                 self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;        

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )
            
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupGL2( modules, indexTracker, target )

        extensions = { 'OPENGLES2_BGRA8888' : 'GL_EXT_texture_format_BGRA8888' }

        if extensions.has_key( self.textureType ):
            OpenGLES2.CheckExtension( extensions[ self.textureType ] )
        
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        screenSize              = target.getScreenSize()
        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenSize[ 0 ], screenSize[ 1 ],                                                
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
                    
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )

######################################################################
def setupCompressedTexture( modules, indexTracker, size, compressedTextureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    Images    = modules[ 'Images' ]

    if compressedTextureType.startswith( 'IMAGES_ETC' ):
        OpenGLES2.CheckExtension( 'GL_OES_compressed_ETC1_RGB8_texture' )
    elif compressedTextureType.startswith( 'IMAGES_PVRTC' ):
        OpenGLES2.CheckExtension( 'GL_IMG_texture_compression_pvrtc' )            
    
    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    
    imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
    Images.CreateData( imagesDataIndex,
                       'IMAGES_FLOWER',
                       compressedTextureType,
                       size, size )

    dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
    Images.CopyToOpenGLES2( imagesDataIndex,
                            dataIndex )

    if compressedTextureType.startswith( 'IMAGES_RGB' ):   
        rgbTypeMap = { 'IMAGES_RGB565' : 'OPENGLES2_RGB565',
                       'IMAGES_RGB8'   : 'OPENGLES2_RGB888',
                       'IMAGES_RGBA8'  : 'OPENGLES2_RGBA8888' }
        
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateTextureData( textureDataIndex,
                                     dataIndex,
                                     size, size,
                                     rgbTypeMap[ compressedTextureType ]  )
    
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        
    else:
        etcTypeMap = { 'IMAGES_ETC1_RGB8'   : 'GL_ETC1_RGB8_OES',
                       'IMAGES_PVRTC_RGB4'  : 'GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG',
                       'IMAGES_PVRTC_RGB2'  : 'GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG',
                       'IMAGES_PVRTC_RGBA4' : 'GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG',
                       'IMAGES_PVRTC_RGBA2' : 'GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG'
                       }
        
        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCompressedTextureData( compressedTextureDataIndex,
                                               dataIndex,
                                               size, size,
                                               etcTypeMap[ compressedTextureType ]  )
    
        OpenGLES2.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )

    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )       
        
    return TextureSetup( -1, textureIndex )
        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CompressedTextureTypeBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, compressedTextureType ):
        Benchmark.__init__( self )
        self.gridx                 = gridx
        self.gridy                 = gridy
        self.overdraw              = overdraw
        self.compressedTextureType = compressedTextureType
        self.name = 'OPENGLES2 compressed texture %s grd=%dx%dx%d' % ( self.compressedTextureType[ 7 : ],
                                                                       self.gridx,
                                                                       self.gridy,
                                                                       self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;        

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )
            
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        screenSize              = target.getScreenSize()
        w = toPot( screenSize[ 0 ], True )
        h = toPot( screenSize[ 1 ], True )
        textureSize = min( 1024, max( w, h ) )
        
        tcx = screenSize[ 0 ] / float( textureSize )
        tcy = screenSize[ 1 ] / float( textureSize )
        
        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupCompressedTexture( modules,
                                                          indexTracker,
                                                          textureSize,
                                                          self.compressedTextureType )
        
        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )
    
