#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from lib.common import *
from lib.mesh   import *

######################################################################
def defaultOrientation( w, h ):
    if w > h:
        return 'SCT_SCREEN_ORIENTATION_LANDSCAPE'
    else:
        return 'SCT_SCREEN_ORIENTATION_PORTRAIT'

######################################################################
def screenToStr( s ):
    return s[ 11 : ]

######################################################################
def orientationToStr( o ):
    return o[ 23 : ]

######################################################################
def elementsToStr( t ):
    return t[ 3 : ]

######################################################################
def arrayToStr( t ):
    if not t:
        return 'none'
    return str( t ).replace( ' ', '' )

######################################################################
def farrayToStr( t ):
    if not t:
        return 'none'
    s = ''
    for f in t:
        s = s + ( '%.2f' % f ).replace( '00', '0' ) + ' '
    return '[' + s.strip() + ']'

######################################################################
def bufferToStr( f ):
    return f[ 4 : ]

######################################################################
def bufferFormatToStr( format ):
    f = { 'SCT_COLOR_FORMAT_DEFAULT'      : 'DFLT',
          'SCT_COLOR_FORMAT_BW1'          : 'BW1',
          'SCT_COLOR_FORMAT_L8'           : 'L8',
          'SCT_COLOR_FORMAT_RGB565'       : 'RGB565',
          'SCT_COLOR_FORMAT_RGB888'       : 'RGB8',
          'SCT_COLOR_FORMAT_RGBX8888'     : 'RGBX8',
          'SCT_COLOR_FORMAT_RGBA8888'     : 'RGBA8',
          'SCT_COLOR_FORMAT_RGBA8888_PRE' : 'RGBA8P'
          }
    return f[ format ]

######################################################################
def fboFormatToStr( format ):
    f = { 'GL_RGBA4'                 : 'RGBA4',
          'GL_RGB5_A1'               : 'RGBA51',
          'GL_RGB565'                : 'RGB565',
          'GL_DEPTH_COMPONENT16'     : 'DPTH16',
          'GL_STENCIL_INDEX8'        : 'STNCIL8',
          'GL_RGB8_OES'              : 'RGB8',
          'GL_RGBA8_OES'             : 'RGBA8',
          'GL_DEPTH_COMPONENT24_OES' : 'DPTH24',
          'GL_DEPTH_COMPONENT32_OES' : 'DPTH32',
          'GL_STENCIL_INDEX4_OES'    : 'STNCIL4'
          }

    return f[ format ]

######################################################################
def pixmapFormatToStr( f ):
    return bufferFormatToStr( f )

######################################################################
def swapBehaviorToStr( s ):
    return s[ 11 : ]

######################################################################
def mapAttributesToColorFormat( a ):
    bits  = a[ CONFIG_REDSIZE ] + a[ CONFIG_GREENSIZE ] + a[ CONFIG_BLUESIZE ] + a[ CONFIG_ALPHASIZE ]
    abits = a[ CONFIG_ALPHASIZE ]

    if bits == 16:
        if abits == 0:
            return 'SCT_COLOR_FORMAT_RGB565'
        else:
            raise 'Invalid attributes'
    elif bits == 24:
        return 'SCT_COLOR_FORMAT_RGB888'
    elif bits == 32:
        return 'SCT_COLOR_FORMAT_RGBA8888'
    else:
        raise 'Invalid attributes'

######################################################################
def mapColorFormatToAttributes( f ):
    formats = { 'SCT_COLOR_FORMAT_RGBA5551' : [ 16, 5, 5, 5, 1 ],
                'SCT_COLOR_FORMAT_RGBA4444' : [ 16, 4, 4, 4, 4 ],
                'SCT_COLOR_FORMAT_RGB565'   : [ 16, 5, 6, 5, 0 ],
                'SCT_COLOR_FORMAT_RGB888'   : [ 24, 8, 8, 8, 0 ],
                'SCT_COLOR_FORMAT_RGBA8888' : [ 32, 8, 8, 8, 8 ] }

    format = formats[ f ]

    a = {}
    a[ CONFIG_BUFFERSIZE ] = format[ 0 ]
    a[ CONFIG_REDSIZE ]    = format[ 1 ]
    a[ CONFIG_GREENSIZE ]  = format[ 2 ]
    a[ CONFIG_BLUESIZE ]   = format[ 3 ]
    a[ CONFIG_ALPHASIZE ]  = format[ 4 ]

    return a

######################################################################
def arrayToStr( a ):
    return str( a ).replace( ' ', '' )

######################################################################
def bufferTargetToStr( b ):
    return b[ 3 : -7 ]

######################################################################
def bufferUsageToStr( b ):
    return b[ 3 : -5 ]

######################################################################
def blendToStr( b ):
    return b[ 3: ]

######################################################################
def toPot( v, pot ):
    if pot:
        nv = 2 ** math.ceil( math.log( v, 2 ) )
        return ( int( nv ), v / nv, )
    else:
        return ( int( v ), 1.0, )

######################################################################
def boolToStr( b ):
    if b:
        return '1'
    else:
        return '0'

######################################################################
def maskToStr( mask ):
    return '+'.join( mask ).replace( 'GL_', '' ).replace( '_BUFFER_BIT', '' )

######################################################################
def filteringToStr( filtering ):
    f = { 'GL_NEAREST'                : 'NRST',
          'GL_LINEAR'                 : 'LNR',
          'GL_NEAREST_MIPMAP_NEAREST' : 'NRST_MP_NRST',
          'GL_LINEAR_MIPMAP_NEAREST'  : 'LNR_MP_NRST',
          'GL_NEAREST_MIPMAP_LINEAR'  : 'NRST_MP_LNR',
          'GL_LINEAR_MIPMAP_LINEAR'   : 'LNR_MP_LNR',
          }

    return f[ filtering ]

######################################################################
def wrapToStr( wrap ):
    w = { 'GL_CLAMP_TO_EDGE'   : 'CLMP',
          'GL_REPEAT'          : 'RPT',
          'GL_MIRRORED_REPEAT' : 'MRRRD_RPT' }
    return w[ wrap ]

######################################################################
def textureTypeToStr( type ):
    t = { 'OPENGLES2_LUMINANCE8'        : 'L8',
          'OPENGLES2_ALPHA8'            : 'A8',
          'OPENGLES2_LUMINANCE_ALPHA88' : 'LA88',
          'OPENGLES2_RGB565'            : 'RGB565',
          'OPENGLES2_RGB888'            : 'RGB8',
          'OPENGLES2_RGBA4444'          : 'RGBA4',
          'OPENGLES2_RGBA5551'          : 'RGBA51',
          'OPENGLES2_RGBA8888'          : 'RGBA8',
          'OPENGLES2_BGRA8888'          : 'BGRA8',
          'OPENGLES2_DEPTH'             : 'DPTH',
          'OPENGLES2_DEPTH32'           : 'DPTH32',
          }

    return t[ type ]

######################################################################
def imageTypeToStr( type ):
    t = { 'SCT_COLOR_FORMAT_RGB565'     : 'RGB565',
          'SCT_COLOR_FORMAT_RGB888'     : 'RGB8',
          'SCT_COLOR_FORMAT_RGBA8888'   : 'RGBA8',
          'SCT_COLOR_FORMAT_YUV12'      : 'YUV12',
          }

    return t[ type ]

######################################################################
def setupGL2( modules, indexTracker, target, attributes = {} ):
    a = target.getDefaultAttributes( API_OPENGLES2 )

    for k in attributes.keys():
        a[ k ] = attributes[ k ]

    return target.initialize( modules, indexTracker, API_OPENGLES2, a )

######################################################################
def formatShader( shader ):
    shader = shader.replace( "\n", "" )
    while True:
        s = shader.replace( "  ", " " )
        if s == shader:
            return '"' + s.strip() + '"'
        shader = s

######################################################################
class VertexDataSetup:
    def __init__( self,
                  vertexArrayIndex,
                  vertexSize,
                  vertexArrayNormalized,
                  colorArrayIndex,
                  colorSize,
                  colorArrayNormalized,
                  normalArrayIndex,
                  texCoordArrayIndices,
                  texCoordSizes,
                  texCoordArrayNormalized,
                  indexArrayIndex,
                  indexArrayLength,
                  glMode ):
        self.vertexArrayIndex        = vertexArrayIndex
        self.vertexSize              = vertexSize
        self.vertexArrayNormalized   = vertexArrayNormalized
        self.colorArrayIndex         = colorArrayIndex
        self.colorSize               = colorSize
        self.colorArrayNormalized    = colorArrayNormalized
        self.normalArrayIndex        = normalArrayIndex
        self.texCoordArrayIndices    = texCoordArrayIndices
        self.texCoordSizes           = texCoordSizes[ : ]
        self.texCoordArrayNormalized = texCoordArrayNormalized
        self.indexArrayIndex         = indexArrayIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode

######################################################################
def setupVertexData( modules, indexTracker, mesh ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    if not mesh.vertices:
        raise 'Undefined vertices'

    if not mesh.indices:
        raise 'Undefined indices'

    vertexArrayIndex        = -1
    vertexArrayNormalized   = False
    colorArrayIndex         = -1
    colorArrayNormalized    = False
    normalArrayIndex        = -1
    texCoordArrayIndices    = []
    texCoordArrayNormalized = []
    indexArrayIndex         = -1

    vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
    vertexArrayNormalized = mesh.vertexNormalized
    OpenGLES2.CreateArray( vertexArrayIndex,
                           mesh.vertexGLType )
    OpenGLES2.AppendToArray( vertexArrayIndex, mesh.vertices )

    if mesh.colors:
        colorArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        colorArrayNormalized = mesh.colorNormalized
        OpenGLES2.CreateArray( colorArrayIndex,
                               mesh.colorGLType )
        OpenGLES2.AppendToArray( colorArrayIndex, mesh.colors )

    if mesh.normals:
        normalArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( normalArrayIndex,
                               mesh.normalGLType )
        OpenGLES2.AppendToArray( normalArrayIndex, mesh.normals )

    if mesh.texCoords:
        for i in range( len( mesh.texCoords ) ):
            if mesh.texCoords:
                texCoordArrayIndices.append( indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' ) )
                texCoordArrayNormalized.append( mesh.texCoordsNormalized[ i ] )
                OpenGLES2.CreateArray( texCoordArrayIndices[ i ],
                                       mesh.texCoordsGLTypes[ i ] )
                OpenGLES2.AppendToArray( texCoordArrayIndices[ i ], mesh.texCoords[ i ] )
            else:
                texCoordArrayIndices.append( -1 )
                texCoordArrayNormalized.append( False )

    indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
    OpenGLES2.CreateArray( indexArrayIndex, mesh.indexGLType );
    OpenGLES2.AppendToArray( indexArrayIndex, mesh.indices )

    return VertexDataSetup( vertexArrayIndex,
                            mesh.vertexComponents,
                            vertexArrayNormalized,
                            colorArrayIndex,
                            mesh.colorComponents,
                            colorArrayNormalized,
                            normalArrayIndex,
                            texCoordArrayIndices,
                            mesh.texCoordsComponents,
                            texCoordArrayNormalized,
                            indexArrayIndex,
                            len( mesh.indices ),
                            mesh.glMode )

######################################################################
class VertexBufferDataSetup:
    def __init__( self,
                  vertexBufferIndex,
                  vertexSize,
                  vertexArrayNormalized,
                  colorBufferIndex,
                  colorSize,
                  colorArrayNormalized,
                  normalBufferIndex,
                  texCoordBufferIndices,
                  texCoordSizes,
                  texCoordArrayNormalized,
                  indexBufferIndex,
                  indexArrayLength,
                  glMode ):
        self.vertexBufferIndex       = vertexBufferIndex
        self.vertexSize              = vertexSize
        self.vertexArrayNormalized   = vertexArrayNormalized
        self.colorBufferIndex        = colorBufferIndex
        self.colorSize               = colorSize
        self.colorArrayNormalized    = colorArrayNormalized
        self.normalBufferIndex       = normalBufferIndex
        self.texCoordBufferIndices   = texCoordBufferIndices
        self.texCoordSizes           = texCoordSizes[ : ]
        self.texCoordArrayNormalized = texCoordArrayNormalized
        self.indexBufferIndex        = indexBufferIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode

######################################################################
def setupVertexBufferData( modules, indexTracker, vertexDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    vertexBufferIndex       = -1
    vertexArrayNormalized   = False
    colorBufferIndex        = -1
    colorArrayNormalized    = False
    normalBufferIndex       = -1
    texCoordBufferIndices   = []
    texCoordArrayNormalized = []
    indexArrayIndex         = -1

    vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
    OpenGLES2.GenBuffer( vertexBufferIndex )
    OpenGLES2.BufferData( vertexBufferIndex,
                          'ON',
                          'GL_ARRAY_BUFFER',
                          'GL_STATIC_DRAW',
                          vertexDataSetup.vertexArrayIndex,
                          0,
                          0 )
    vertexArrayNormalized = vertexDataSetup.vertexArrayNormalized

    if vertexDataSetup.colorArrayIndex >= 0:
        colorBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( colorBufferIndex )
        OpenGLES2.BufferData( colorBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              vertexDataSetup.colorArrayIndex,
                              0,
                              0 )
        colorArrayNormalized = vertexDataSetup.colorArrayNormalized

    if vertexDataSetup.normalArrayIndex >= 0:
        normalBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( normalBufferIndex )
        OpenGLES2.BufferData( normalBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              vertexDataSetup.normalArrayIndex,
                              0,
                              0 )

    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices[ i ] >= 0:
                texCoordBufferIndices.append( indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' ) )
                OpenGLES2.GenBuffer( texCoordBufferIndices[ i ] )
                OpenGLES2.BufferData( texCoordBufferIndices[ i ],
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      vertexDataSetup.texCoordArrayIndices[ i ],
                                      0,
                                      0 )
                texCoordArrayNormalized.append( vertexDataSetup.texCoordArrayNormalized[ i ] )
            else:
                texCoordBufferIndices.append( -1 )
                texCoordArrayNormalized.append( False )

    indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
    OpenGLES2.GenBuffer( indexBufferIndex )
    OpenGLES2.BufferData( indexBufferIndex,
                          'ON',
                          'GL_ELEMENT_ARRAY_BUFFER',
                          'GL_STATIC_DRAW',
                          vertexDataSetup.indexArrayIndex,
                          0,
                          0 )

    return VertexBufferDataSetup( vertexBufferIndex,
                                  vertexDataSetup.vertexSize,
                                  vertexArrayNormalized,
                                  colorBufferIndex,
                                  vertexDataSetup.colorSize,
                                  colorArrayNormalized,
                                  normalBufferIndex,
                                  texCoordBufferIndices,
                                  vertexDataSetup.texCoordSizes,
                                  texCoordArrayNormalized,
                                  indexBufferIndex,
                                  vertexDataSetup.indexArrayLength,
                                  vertexDataSetup.glMode )

######################################################################
class MeshDataSetup:
    def __init__( self,
                  meshIndex,
                  vertexArrayIndex,
                  vertexSize,
                  vertexArrayNormalized,
                  colorArrayIndex,
                  colorSize,
                  colorArrayNormalized,
                  normalArrayIndex,
                  texCoordArrayIndices,
                  texCoordSizes,
                  texCoordArrayNormalized,
                  indexArrayIndex,
                  indexArrayLength,
                  glMode ):
        self.meshIndex               = meshIndex
        self.vertexArrayIndex        = vertexArrayIndex
        self.vertexSize              = vertexSize
        self.vertexArrayNormalized   = vertexArrayNormalized
        self.colorArrayIndex         = colorArrayIndex
        self.colorSize               = colorSize
        self.colorArrayNormalized    = colorArrayNormalized
        self.normalArrayIndex        = normalArrayIndex
        self.texCoordArrayIndices    = texCoordArrayIndices
        self.texCoordSizes           = texCoordSizes[ : ]
        self.texCoordArrayNormalized = texCoordArrayNormalized
        self.indexArrayIndex         = indexArrayIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode

######################################################################
def setupMeshData( modules, indexTracker, vertexDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    vertexArrayIndex         = -1
    vertexArrayNormalized    = False
    colorArrayIndex          = -1
    colorArrayNormalized     = False
    normalArrayIndex         = -1
    texCoordArrayIndices     = []
    texCoordArraysNormalized = []
    nextIndex                = 0
    meshIndices              = []
    sizes                    = []

    vertexArrayIndex = nextIndex
    nextIndex += 1
    meshIndices.append( vertexDataSetup.vertexArrayIndex )
    vertexArrayNormalized = vertexDataSetup.vertexArrayNormalized
    sizes += [ vertexDataSetup.vertexSize ]

    if vertexDataSetup.colorArrayIndex >= 0:
        colorArrayIndex = nextIndex
        nextIndex += 1
        meshIndices.append( vertexDataSetup.colorArrayIndex )
        colorArrayNormalized = vertexDataSetup.colorArrayNormalized
        sizes += [ vertexDataSetup.colorSize ]

    if vertexDataSetup.normalArrayIndex >= 0:
        normalArrayIndex = nextIndex
        nextIndex += 1
        meshIndices.append( vertexDataSetup.normalArrayIndex )
        sizes += [ 3 ]

    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices >= 0:
                texCoordArrayIndices.append( nextIndex )
                nextIndex += 1
                meshIndices.append( vertexDataSetup.texCoordArrayIndices[ i ] )
                texCoordArraysNormalized.append( vertexDataSetup.texCoordArrayNormalized[ i ] )
                sizes += [ vertexDataSetup.texCoordSizes[ i ] ]
            else:
                texCoordArrayIndices.append( -1 )
                meshIndices.append( -1 )
                texCoordArraysNormalized.append( False )
                sizes += [ 0 ]

    meshIndex = indexTracker.allocIndex( 'OPENGLES2_MESH_INDEX' )
    OpenGLES2.CreateMesh( meshIndex, meshIndices, sizes )

    return MeshDataSetup( meshIndex,
                          vertexArrayIndex,
                          vertexDataSetup.vertexSize,
                          vertexArrayNormalized,
                          colorArrayIndex,
                          vertexDataSetup.colorSize,
                          colorArrayNormalized,
                          normalArrayIndex,
                          texCoordArrayIndices,
                          vertexDataSetup.texCoordSizes,
                          texCoordArraysNormalized,
                          vertexDataSetup.indexArrayIndex,
                          vertexDataSetup.indexArrayLength,
                          vertexDataSetup.glMode )

######################################################################
class MeshBufferDataSetup:
    def __init__( self,
                  meshBufferIndex,
                  vertexArrayIndex,
                  vertexArrayNormalized,
                  colorArrayIndex,
                  colorArrayNormalized,
                  normalArrayIndex,
                  texCoordArrayIndices,
                  texCoordArrayNormalized,
                  indexBufferIndex,
                  indexArrayLength,
                  glMode ):
        self.meshBufferIndex         = meshBufferIndex
        self.vertexArrayIndex        = vertexArrayIndex
        self.vertexArrayNormalized   = vertexArrayNormalized
        self.colorArrayIndex         = colorArrayIndex
        self.colorArrayNormalized    = colorArrayNormalized
        self.normalArrayIndex        = normalArrayIndex
        self.texCoordArrayIndices    = texCoordArrayIndices
        self.texCoordArrayNormalized = texCoordArrayNormalized
        self.indexBufferIndex        = indexBufferIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode

######################################################################
def setupMeshBufferData( modules, indexTracker, meshDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    meshBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
    OpenGLES2.GenBuffer( meshBufferIndex )
    OpenGLES2.MeshBufferData( meshBufferIndex,
                              'ON',
                              meshDataSetup.meshIndex,
                              'GL_STATIC_DRAW' )

    indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
    OpenGLES2.GenBuffer( indexBufferIndex )
    OpenGLES2.BufferData( indexBufferIndex,
                          'ON',
                          'GL_ELEMENT_ARRAY_BUFFER',
                          'GL_STATIC_DRAW',
                          meshDataSetup.indexArrayIndex,
                          0,
                          0 )

    return MeshBufferDataSetup( meshBufferIndex,
                                meshDataSetup.vertexArrayIndex,
                                meshDataSetup.vertexArrayNormalized,
                                meshDataSetup.colorArrayIndex,
                                meshDataSetup.colorArrayNormalized,
                                meshDataSetup.normalArrayIndex,
                                meshDataSetup.texCoordArrayIndices[ : ],
                                meshDataSetup.texCoordArrayNormalized[ : ],
                                indexBufferIndex,
                                meshDataSetup.indexArrayLength,
                                meshDataSetup.glMode )

######################################################################
class Program:
    def __init__( self,
                  programIndex,
                  vertexLocationIndex,
                  colorLocationIndex,
                  normalLocationIndex,
                  texCoordLocationIndices,
                  textureLocationIndices,
                  vertexShaderIndex,
                  fragmentShaderIndex ):
        self.programIndex            = programIndex
        self.vertexLocationIndex     = vertexLocationIndex
        self.colorLocationIndex      = colorLocationIndex
        self.normalLocationIndex     = normalLocationIndex
        self.texCoordLocationIndices = texCoordLocationIndices
        self.textureLocationIndices  = textureLocationIndices
        self.vertexShaderIndex       = vertexShaderIndex
        self.fragmentShaderIndex     = fragmentShaderIndex

######################################################################
def createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup, deleteShaders = True ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    vsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
    fsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )

    OpenGLES2.SetData( vsDataIndex, vertexShaderSource )
    OpenGLES2.SetData( fsDataIndex, fragmentShaderSource )

    vsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
    OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vsIndex )
    OpenGLES2.ShaderSource( vsIndex, vsDataIndex )
    OpenGLES2.CompileShader( vsIndex )

    fsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
    OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fsIndex )
    OpenGLES2.ShaderSource( fsIndex, fsDataIndex )
    OpenGLES2.CompileShader( fsIndex )

    programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
    OpenGLES2.CreateProgram( programIndex )

    OpenGLES2.AttachShader( programIndex, vsIndex )
    OpenGLES2.AttachShader( programIndex, fsIndex )

    vertexAttributeName   = 'gVertex'
    colorAttributeName    = 'gColor'
    normalAttributeName   = 'gNormal'
    texCoordAttributeName = 'gTexCoord%d'
    textureName           = 'gTexture%d'

    # Connect vertex attribute arrays to corresponding shader
    # variables
    nextLocationIndex       = -1
    vertexLocationIndex     = -1
    colorLocationIndex      = -1
    normalLocationIndex     = -1
    texCoordLocationIndices = []
    textureLocationIndices  = []

    locationValue           = 0

    # Vertices are always defined
    vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
    OpenGLES2.SetRegister( vertexLocationIndex, locationValue )
    OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, vertexAttributeName )
    locationValue += 1

    if vertexDataSetup.colorArrayIndex >= 0:
        colorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( colorLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, colorLocationIndex, colorAttributeName )
        locationValue += 1

    if vertexDataSetup.normalArrayIndex >= 0:
        normalLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( normalLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, normalLocationIndex, normalAttributeName )
        locationValue += 1

    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices[ i ] >= 0:
                texCoordLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
                OpenGLES2.SetRegister( texCoordLocationIndices[ i ], locationValue )
                OpenGLES2.BindAttribLocation( programIndex, texCoordLocationIndices[ i ], texCoordAttributeName % i )
                locationValue += 1
            else:
                texCoordLocationIndices.append( -1 )

    OpenGLES2.LinkProgram( programIndex )

    # Program must be linked before we can query the uniform variable
    # (texture) locations
    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices[ i ] >= 0:
                textureLocationIndices.append( indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' ) )
                OpenGLES2.GetUniformLocation( textureLocationIndices[ i ], programIndex, textureName % i )
            else:
                textureLocationIndices.append( -1 )

    if deleteShaders:
        OpenGLES2.DeleteShader( vsIndex )
        OpenGLES2.DeleteShader( fsIndex )

    return Program( programIndex,
                    vertexLocationIndex,
                    colorLocationIndex,
                    normalLocationIndex,
                    texCoordLocationIndices,
                    textureLocationIndices,
                    vsIndex,
                    fsIndex )

######################################################################
def useVertexData( modules, indexTracker, vertexDataSetup, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.UseProgram( program.programIndex )

    OpenGLES2.VertexAttribPointer( program.vertexLocationIndex,
                                   vertexDataSetup.vertexArrayIndex,
                                   vertexDataSetup.vertexSize,
                                   boolToSpandex( vertexDataSetup.vertexArrayNormalized ),
                                   0 )
    OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

    if program.colorLocationIndex >= 0:
        OpenGLES2.VertexAttribPointer( program.colorLocationIndex,
                                       vertexDataSetup.colorArrayIndex,
                                       vertexDataSetup.colorSize,
                                       boolToSpandex( vertexDataSetup.colorArrayNormalized ),
                                       0 )
        OpenGLES2.EnableVertexAttribArray( program.colorLocationIndex )

    if program.normalLocationIndex >= 0:
        OpenGLES2.VertexAttribPointer( program.normalLocationIndex,
                                       vertexDataSetup.normalArrayIndex,
                                       3,
                                       boolToSpandex( True ),  # Normals always normalized
                                       0 )
        OpenGLES2.EnableVertexAttribArray( program.normalLocationIndex )

    if program.texCoordLocationIndices:
        for i in range( len( program.texCoordLocationIndices ) ):
            if program.texCoordLocationIndices[ i ] >= 0:
                OpenGLES2.VertexAttribPointer( program.texCoordLocationIndices[ i ],
                                               vertexDataSetup.texCoordArrayIndices[ i ],
                                               vertexDataSetup.texCoordSizes[ i ],
                                               boolToSpandex( vertexDataSetup.texCoordArrayNormalized[ i ] ),
                                               0 )
                OpenGLES2.EnableVertexAttribArray( program.texCoordLocationIndices[ i ] )

    OpenGLES2.ValidateProgram( program.programIndex )

######################################################################
def useVertexBufferData( modules, indexTracker, vertexBufferDataSetup, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.UseProgram( program.programIndex )

    OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.vertexBufferIndex,
                                         'ON',
                                         program.vertexLocationIndex,
                                         vertexBufferDataSetup.vertexSize,
                                         boolToSpandex( vertexBufferDataSetup.vertexArrayNormalized ),
                                         0 )
    OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

    if program.colorLocationIndex >= 0:
        OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.colorBufferIndex,
                                             'ON',
                                             program.colorLocationIndex,
                                             vertexBufferDataSetup.colorSize,
                                             boolToSpandex( vertexBufferDataSetup.colorArrayNormalized ),
                                             0 )
        OpenGLES2.EnableVertexAttribArray( program.colorLocationIndex )

    if program.normalLocationIndex >= 0:
        OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.normalBufferIndex,
                                             'ON',
                                             program.normalLocationIndex,
                                             3,
                                             boolToSpandex( True ),
                                             0 )
        OpenGLES2.EnableVertexAttribArray( program.normalLocationIndex )

    if program.texCoordLocationIndices:
        for i in range( len( program.texCoordLocationIndices ) ):
            if program.texCoordLocationIndices[ i ] >= 0:
                OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.texCoordBufferIndices[ i ],
                                                     'ON',
                                                     program.texCoordLocationIndices[ i ],
                                                     vertexBufferDataSetup.texCoordSizes[ i ],
                                                     boolToSpandex( vertexBufferDataSetup.texCoordArrayNormalized[ i ] ),
                                                     0 )
                OpenGLES2.EnableVertexAttribArray( program.texCoordLocationIndices[ i ] )

    OpenGLES2.ValidateProgram( program.programIndex )

######################################################################
def useMeshData( modules, indexTracker, meshDataSetup, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.UseProgram( program.programIndex )

    OpenGLES2.MeshVertexAttribPointer( program.vertexLocationIndex,
                                       meshDataSetup.meshIndex,
                                       meshDataSetup.vertexArrayIndex,
                                       boolToSpandex( meshDataSetup.vertexArrayNormalized ) )
    OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

    if program.colorLocationIndex >= 0:
        OpenGLES2.MeshVertexAttribPointer( program.colorLocationIndex,
                                           meshDataSetup.meshIndex,
                                           meshDataSetup.colorArrayIndex,
                                           boolToSpandex( meshDataSetup.colorArrayNormalized ) )
        OpenGLES2.EnableVertexAttribArray( program.colorLocationIndex )

    if program.normalLocationIndex >= 0:
        OpenGLES2.MeshVertexAttribPointer( program.normalLocationIndex,
                                           meshDataSetup.meshIndex,
                                           meshDataSetup.normalArrayIndex,
                                           boolToSpandex( True ) )
        OpenGLES2.EnableVertexAttribArray( program.normalLocationIndex )

    if program.texCoordLocationIndices:
        for i in range( len( program.texCoordLocationIndices ) ):
            if program.texCoordLocationIndices[ i ] >= 0:
                OpenGLES2.MeshVertexAttribPointer( program.texCoordLocationIndices[ i ],
                                                   meshDataSetup.meshIndex,
                                                   meshDataSetup.texCoordArrayIndices[ i ],
                                                   boolToSpandex( meshDataSetup.texCoordArrayNormalized[ i ] ) )
                OpenGLES2.EnableVertexAttribArray( program.texCoordLocationIndices[ i ] )

    OpenGLES2.ValidateProgram( program.programIndex )

######################################################################
def useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.UseProgram( program.programIndex )

    OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                             'ON',
                                             program.vertexLocationIndex,
                                             meshBufferDataSetup.vertexArrayIndex,
                                             boolToSpandex( meshBufferDataSetup.vertexArrayNormalized ) )
    OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

    if program.colorLocationIndex >= 0:
        OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                 'ON',
                                                 program.colorLocationIndex,
                                                 meshBufferDataSetup.colorArrayIndex,
                                                 boolToSpandex( meshBufferDataSetup.colorArrayNormalized ) )
        OpenGLES2.EnableVertexAttribArray( program.colorLocationIndex )

    if program.normalLocationIndex >= 0:
        OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                 'ON',
                                                 program.normalLocationIndex,
                                                 meshBufferDataSetup.normalArrayIndex,
                                                 boolToSpandex( True ) )
        OpenGLES2.EnableVertexAttribArray( program.normalLocationIndex )

    if program.texCoordLocationIndices:
        for i in range( len( program.texCoordLocationIndices ) ):
            if program.texCoordLocationIndices[ i ] >= 0:
                OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                         'ON',
                                                         program.texCoordLocationIndices[ i ],
                                                         meshBufferDataSetup.texCoordArrayIndices[ i ],
                                                         boolToSpandex( meshBufferDataSetup.texCoordArrayNormalized[ i ] ) )
                OpenGLES2.EnableVertexAttribArray( program.texCoordLocationIndices[ i ] )

    OpenGLES2.ValidateProgram( program.programIndex )

######################################################################
def drawVertexData( modules, vertexDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.DrawElements( vertexDataSetup.glMode,
                            vertexDataSetup.indexArrayLength,
                            0,
                            vertexDataSetup.indexArrayIndex )

######################################################################
def drawMeshData( modules, meshDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.DrawElements( meshDataSetup.glMode,
                            meshDataSetup.indexArrayLength,
                            0,
                            meshDataSetup.indexArrayIndex )

######################################################################
def drawVertexBufferData( modules, vertexBufferDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.BufferDrawElements( vertexBufferDataSetup.indexBufferIndex,
                                  'ON',
                                  vertexBufferDataSetup.glMode,
                                  vertexBufferDataSetup.indexArrayLength,
                                  0 )

######################################################################
def drawMeshBufferData( modules, meshBufferDataSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    OpenGLES2.BufferDrawElements( meshBufferDataSetup.indexBufferIndex,
                                  'ON',
                                  meshBufferDataSetup.glMode,
                                  meshBufferDataSetup.indexArrayLength,
                                  0 )

######################################################################
class TextureSetup:
    def __init__( self,
                  textureDataIndex,
                  textureIndex ):
        self.textureDataIndex = textureDataIndex
        self.textureIndex     = textureIndex

######################################################################
def useTexture( modules, textureSetup, textureUnit, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    if textureUnit < 0 or textureUnit >= len( program.textureLocationIndices ):
        raise 'Invalid texture unit'

    OpenGLES2.ActiveTexture( textureUnit )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )

    OpenGLES2.Uniformi( program.textureLocationIndices[ textureUnit ], 1, [ textureUnit ] )

######################################################################
class Material:
    def __init__( self,
                  ambient,
                  diffuse,
                  specular,
                  shininess ):
        self.ambient   = ambient
        self.diffuse   = diffuse
        self.specular  = specular
        self.shininess = shininess

######################################################################
class Light:
    def __init__( self,
                  ambient,
                  direction,
                  diffuse,
                  specular ):
        self.ambient   = ambient
        self.direction = direction
        self.diffuse   = diffuse
        self.specular  = specular

######################################################################
class LightSetup:
    def __init__( self,
                  light,
                  material,
                  ambientLight,
                  ambientMaterial,
                  lightDirection,
                  diffuseLight,
                  diffuseMaterial,
                  specularLight,
                  specularMaterial,
                  shininess ):
        self.light            = light
        self.material         = material
        self.ambientLight     = ambientLight
        self.ambientMaterial  = ambientMaterial
        self.lightDirection   = lightDirection
        self.diffuseLight     = diffuseLight
        self.diffuseMaterial  = diffuseMaterial
        self.specularLight    = specularLight
        self.specularMaterial = specularMaterial
        self.shininess        = shininess

######################################################################
def setupLight( modules, indexTracker, light, material, program ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    ambientLight     = -1
    ambientMaterial  = -1
    lightDirection   = -1
    diffuseLight     = -1
    diffuseMaterial  = -1
    specularLight    = -1
    specularMaterial = -1
    shininess        = -1

    if light and light.ambient:
        ambientLight = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( ambientLight, program.programIndex, 'gAmbientLight' )

    if material and material.ambient:
        ambientMaterial = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( ambientMaterial, program.programIndex, 'gAmbientMaterial' )

    if light and light.direction:
        lightDirection = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( lightDirection, program.programIndex, 'gLightDirection' )

    if light and light.diffuse:
        diffuseLight = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( diffuseLight, program.programIndex, 'gDiffuseLight' )

    if material and material.diffuse:
        diffuseMaterial = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( diffuseMaterial, program.programIndex, 'gDiffuseMaterial' )

    if light and light.specular:
        specularLight = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( specularLight, program.programIndex, 'gSpecularLight' )

    if material and material.specular:
        specularMaterial = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( specularMaterial, program.programIndex, 'gSpecularMaterial' )

    if material and material.shininess:
        shininess = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( shininess, program.programIndex, 'gShininess' )

    return LightSetup( light,
                       material,
                       ambientLight,
                       ambientMaterial,
                       lightDirection,
                       diffuseLight,
                       diffuseMaterial,
                       specularLight,
                       specularMaterial,
                       shininess )

######################################################################
def useLight( modules, lightSetup ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    if lightSetup.ambientLight >= 0:
        OpenGLES2.Uniformf( lightSetup.ambientLight, 1, lightSetup.light.ambient )

    if lightSetup.ambientMaterial >= 0:
        OpenGLES2.Uniformf( lightSetup.ambientMaterial, 1, lightSetup.material.ambient )

    if lightSetup.lightDirection >= 0:
        OpenGLES2.Uniformf( lightSetup.lightDirection, 1, lightSetup.light.direction )

    if lightSetup.diffuseLight >= 0:
        OpenGLES2.Uniformf( lightSetup.diffuseLight, 1, lightSetup.light.diffuse )

    if lightSetup.diffuseMaterial >= 0:
        OpenGLES2.Uniformf( lightSetup.diffuseMaterial, 1, lightSetup.material.diffuse )

    if lightSetup.specularLight >= 0:
        OpenGLES2.Uniformf( lightSetup.specularLight, 1, lightSetup.light.specular )

    if lightSetup.specularMaterial >= 0:
        OpenGLES2.Uniformf( lightSetup.specularMaterial, 1, lightSetup.material.specular )

    if lightSetup.shininess >= 0:
        OpenGLES2.Uniformf( lightSetup.shininess, 1, [ lightSetup.material.shininess ] )

