#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *
  
######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, 0.0, 0 ] )
    mesh.scale( [ 2.0, 1.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class StencilBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, textureType, useStencil ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.textureType = textureType
        self.useStencil  = useStencil       
        self.name = 'OPENGLES2 stencil%s grd=%dx%d tex=%s' % ( { True: ' enabled', False : ' disabled' }[ self.useStencil ],
                                                               self.gridx,
                                                               self.gridy,
                                                               textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y + gOffset, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        if self.useStencil:
            attribs[ CONFIG_STENCILSIZE ] = '>=8'
        else:
            attribs[ CONFIG_STENCILSIZE ] = '-'

        attribs[ CONFIG_DEPTHSIZE ] = '-'
            
        state                   = setupGL2( modules, indexTracker, target, attribs )
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 0.0, 1.0 ],
                                                self.textureType )
        
        texture1Setup           = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 1.0, 0.0, 1.0 ],
                                                [ 0.0, 1.0, 1.0, 1.0 ],
                                                self.textureType )
        
        useTexture( modules, texture0Setup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        if self.useStencil:
            OpenGLES2.ClearStencil( 0 )
            OpenGLES2.Enable( 'GL_STENCIL_TEST' )
            OpenGLES2.CheckValue( 'GL_STENCIL_BITS', 'SCT_GEQUAL', [ 8 ] )

        OpenGLES2.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        bits = [ 'GL_COLOR_BUFFER_BIT' ]
        if self.useStencil:
            bits += [ 'GL_STENCIL_BUFFER_BIT' ]

        OpenGLES2.Clear( bits )

        if self.useStencil:
            OpenGLES2.StencilFunc( 'GL_ALWAYS', 1, '0x1' )
            OpenGLES2.StencilOp( 'GL_REPLACE', 'GL_REPLACE', 'GL_REPLACE' )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture0Setup.textureIndex )
        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
        drawMeshBufferData( modules, meshBufferDataSetup )

        if self.useStencil:
            OpenGLES2.StencilFunc( 'GL_NOTEQUAL', 1, '0x1' )
            OpenGLES2.StencilOp( 'GL_KEEP', 'GL_KEEP', 'GL_KEEP' )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Setup.textureIndex )
        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ -1.0 ] )
        drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

