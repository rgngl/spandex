#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
FRONT_TO_BACK = 'front-to-back'
BACK_TO_FRONT = 'back-to-front'
    
######################################################################
def createPlane( gridx, gridy ):
    vertexAttributes    = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttributes  = []
    indexAttributes     = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttributes.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2,
                                                      [ 1.0,
                                                        1.0 ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttributes,
                           None,
                           None,
                           texCoordAttributes,
                           indexAttributes,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        [ 1.0, 0.3, 0.3, 1.0 ],
                                        [ 0.3, 1.0, 0.3, 1.0 ],
                                        4, 4,
                                        w, w,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DrawOrderBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, order ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.order       = order
        self.name = 'OPENGLES2 draw order %s grd=%dx%dx%d tex=%s' % ( order,
                                                                      self.gridx,
                                                                      self.gridy,
                                                                      self.overdraw,
                                                                      textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;
        uniform vec4 gModColor;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy ) * gModColor;
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules, indexTracker, screenWidth, screenHeight, self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        modColorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( modColorLocationIndex, program.programIndex, 'gModColor' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta = 1.0 / float( self.overdraw )

        if self.order == FRONT_TO_BACK:
            offset      = 0.0
            delta       = delta
        elif self.order == BACK_TO_FRONT:
            offset      = 1.0
            delta       = -delta
        else:
            raise 'Invalid draw order'

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( modColorLocationIndex, 1, [ offset, 1.0 - offset, 1.0, 1.0 ] )
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset += delta

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DrawOrderWithHslBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, order ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.hslParams   = [ 0.2, 0.1, 0.2, 1.0 ]   # h, s, l, opacity
        self.order       = order
        self.name = 'OPENGLES2 draw order with hsl.frgmnt.shdr %s grd=%dx%dx%d tex=%s' % ( order,
                                                                                           self.gridx,
                                                                                           self.gridy,
                                                                                           self.overdraw,
                                                                                           textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        uniform lowp sampler2D gTexture0;
        varying lowp vec4 gVSTexCoord0;
        uniform lowp vec4 params;
        uniform lowp vec4 gModColor;

        lowp vec3 RGBtoHSL( lowp vec3 color )
        {
            lowp float cmin = min( color.r, min( color.g, color.b ) );
            lowp float cmax = max( color.r, max( color.g, color.b ) );
            lowp float h = 0.0;
            lowp float s = 0.0;
            lowp float l = ( cmin + cmax ) / 2.0;
            lowp float diff = cmax - cmin;

            if( dot( diff, diff ) > pow( 8.0 / 256.0, 2.0 ) )
            {
               lowp float diff = cmax - cmin;

               if( l < 0.5 )
               {
                  s = diff / ( cmin + cmax );
               }
               else
               {
                  s = diff / ( 2.0 - ( cmin + cmax ) );
               }

               if( color.r == cmax )
               {
                   h = ( color.g - color.b ) / diff;
               }
               else if( color.g == cmax )
               {
                   h = 2.0 + ( color.b - color.r ) / diff;
               }
               else
               {
                   h = 4.0 + ( color.r - color.g ) / diff;
               }

               h /= 6.0;
            }

            return vec3(h, s, l);
        }

        lowp float hueToIntensity( lowp float v1, lowp float v2, lowp float h )
        {
            h = fract( h );

            if( h < 1.0 / 6.0 )
            {
                return v1 + ( v2 - v1 ) * 6.0 * h;
            }
            else if( h < 1.0 / 2.0 )
            {
                return v2;
            }
            else if( h < 2.0 / 3.0 )
            {
                return v1 + ( v2 - v1 ) * 6.0 * ( 2.0 / 3.0 - h );
            }

            return v1;
        }

        lowp vec3 HSLtoRGB( lowp vec3 color )
        {
            lowp float h = color.x;
            lowp float s = color.y;
            lowp float l = color.z;

            if( s < 1.0 / 256.0 )
            {
                return vec3( l, l, l );
            }

            lowp float v1;
            lowp float v2;

            if( l < 0.5 )
            {
                v2 = l * ( 1.0 + s );
            }
            else
            {
                v2 = ( l + s ) - ( s * l );
            }

            v1 = 2.0 * l - v2;

            lowp float d = 1.0 / 3.0;
            lowp float r = hueToIntensity( v1, v2, h + d );
            lowp float g = hueToIntensity( v1, v2, h );
            lowp float b = hueToIntensity( v1, v2, h - d );

            return vec3( r, g, b );
        }

        void main()
        {
            lowp vec4 sample = texture2D( gTexture0, gVSTexCoord0.xy );
            sample.xyz = RGBtoHSL( sample.rgb );
            sample.rgb = HSLtoRGB( sample.xyz + params.xyz );
            gl_FragColor = sample * sample.a * params.w * gModColor;
        }"""
        
        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules, indexTracker, screenWidth, screenHeight, self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        modColorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( modColorLocationIndex, program.programIndex, 'gModColor' )

        hslParamsLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( hslParamsLocationIndex, program.programIndex, 'params' )

        OpenGLES2.Uniformf( hslParamsLocationIndex, 1, self.hslParams )           

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta = 1.0 / float( self.overdraw )

        if self.order == FRONT_TO_BACK:
            offset      = 0.0
            delta       = delta
        elif self.order == BACK_TO_FRONT:
            offset      = 1.0
            delta       = -delta
        else:
            raise 'Invalid draw order'

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( modColorLocationIndex, 1, [ offset, 1.0 - offset, 1.0, 1.0 ] )
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset += delta

        target.swapBuffers( state )

