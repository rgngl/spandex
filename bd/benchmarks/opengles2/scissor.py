#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *
  
######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ScissorBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, scissorR ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.scissorR    = scissorR
        self.name = 'OPENGLES2 scissor %s grd=%dx%dx%d tex=%s' % ( farrayToStr( self.scissorR ),
                                                                   self.gridx,
                                                                   self.gridy,
                                                                   self.overdraw,
                                                                   textureTypeToStr( self.textureType ), )
        
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        scissorRect = None
        if self.scissorR:
            scissorRect = [ 0, 0, 0, 0 ]
            scissorRect[ 0 ] = int( self.scissorR[ 0 ] * screenWidth )
            scissorRect[ 1 ] = int( self.scissorR[ 1 ] * screenHeight )
            scissorRect[ 2 ] = int( self.scissorR[ 2 ] * screenWidth )
            scissorRect[ 3 ] = int( self.scissorR[ 3 ] * screenHeight )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.5, 0.5, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        
        if scissorRect:
            OpenGLES2.Scissor( scissorRect[ 0 ],
                               scissorRect[ 1 ],
                               scissorRect[ 2 ],
                               scissorRect[ 3 ] )

        OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if scissorRect:
            OpenGLES2.Disable( 'GL_SCISSOR_TEST' )
            
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        if scissorRect:
            OpenGLES2.Enable( 'GL_SCISSOR_TEST' )
        
        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ScissorSwitchBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, textureType, overdraw1, scissorR1, overdraw2, scissorR2 ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.textureType  = textureType
        self.overdraw1    = overdraw1
        self.scissorR1    = scissorR1
        self.overdraw2    = overdraw2
        self.scissorR2    = scissorR2
        self.overdraw     = overdraw1 + overdraw2
        
        self.name = 'OPENGLES2 scissor switch %d %s %d %s grd=%dx%dx%d tex=%s' % ( self.overdraw1,
                                                                                   farrayToStr( self.scissorR1 ),
                                                                                   self.overdraw2,
                                                                                   farrayToStr( self.scissorR2 ),
                                                                                   self.gridx,
                                                                                   self.gridy,
                                                                                   self.overdraw,
                                                                                   textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        scissorRect1 = None
        if self.scissorR1:
            scissorRect1 = [ 0, 0, 0, 0 ]
            scissorRect1[ 0 ] = int( self.scissorR1[ 0 ] * screenWidth )
            scissorRect1[ 1 ] = int( self.scissorR1[ 1 ] * screenHeight )
            scissorRect1[ 2 ] = int( self.scissorR1[ 2 ] * screenWidth )
            scissorRect1[ 3 ] = int( self.scissorR1[ 3 ] * screenHeight )

        scissorRect2 = None
        if self.scissorR2:
            scissorRect2 = [ 0, 0, 0, 0 ]
            scissorRect2[ 0 ] = int( self.scissorR2[ 0 ] * screenWidth )
            scissorRect2[ 1 ] = int( self.scissorR2[ 1 ] * screenHeight )
            scissorRect2[ 2 ] = int( self.scissorR2[ 2 ] * screenWidth )
            scissorRect2[ 3 ] = int( self.scissorR2[ 3 ] * screenHeight )
            
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.5, 0.5, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 0.5, 0.7, 0.1, 0.5 ],
                                            [ 0.7, 0.5, 0.1, 0.5 ],
                                            16, 16,
                                            screenWidth, screenHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if scissorRect1 or scissorRect2:
            OpenGLES2.Disable( 'GL_SCISSOR_TEST' )
            
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        overdraw = self.overdraw1 + self.overdraw2
        delta    = 1.0 / float( overdraw )
        offset   = 1.0

        if scissorRect1:
            OpenGLES2.Enable( 'GL_SCISSOR_TEST' )
            OpenGLES2.Scissor( scissorRect1[ 0 ],
                               scissorRect1[ 1 ],
                               scissorRect1[ 2 ],
                               scissorRect1[ 3 ] )
        else:
            OpenGLES2.Disable( 'GL_SCISSOR_TEST' )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
            
        for i in range( self.overdraw1 ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        if scissorRect2:
            OpenGLES2.Enable( 'GL_SCISSOR_TEST' )
            OpenGLES2.Scissor( scissorRect2[ 0 ],
                               scissorRect2[ 1 ],
                               scissorRect2[ 2 ],
                               scissorRect2[ 3 ] )
        else:
            OpenGLES2.Disable( 'GL_SCISSOR_TEST' )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
            
        for i in range( self.overdraw2 ):            
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta
        
        target.swapBuffers( state )
            
