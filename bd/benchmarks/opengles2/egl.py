#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from lib.egl       import *
from common        import *

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PrintEglInfo( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENGLES2 egl info (debug)"
        self.repeats = 1

    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.Info( displayIndex )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CreateContextBenchmark( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENGLES2 create context"

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.DestroyContext( displayIndex, contextIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DummyContextSwitchBenchmark( Benchmark ):
    def __init__( self, switch, shared, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.switch       = switch
        self.shared       = shared
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.name = 'OPENGLES2 dummy context switch%s%s grd=%dx%dx%d tex=%s' % ( { True: '', False: ' skip' }[ self.switch ],
                                                                                 { True: ' shared context', False: '' }[ self.shared ],
                                                                                 self.gridx,
                                                                                 self.gridy,
                                                                                 self.overdraw,
                                                                                 textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        if self.switch:
            context2Index = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
            index = -1
            if self.shared:
                index = contextIndex
            Egl.CreateContext( displayIndex, context2Index, configIndex, index, 2 )
            Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, context2Index )
            OpenGLES2.Scissor( width / 2 - 10, height / 2 - 10, 20, 20 )
            OpenGLES2.Enable( 'GL_SCISSOR_TEST' )
            OpenGLES2.ClearColor( [ 0, 0, 1, 1 ] )
            Egl.SwapInterval( displayIndex, 0 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.ClearColor( [ 0, 0, 0, 1 ] )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        if self.switch:
            Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, context2Index )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
            Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        Egl.SwapBuffers( displayIndex, surfaceIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CreateWindowSurfaceBenchmark( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENGLES2 create window surface"

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )

        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        a = target.getDefaultAttributes( API_OPENGLES2 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       a[ CONFIG_BUFFERSIZE ],
                       a[ CONFIG_REDSIZE ],
                       a[ CONFIG_GREENSIZE ],
                       a[ CONFIG_BLUESIZE ],
                       a[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        Egl.DestroySurface( displayIndex, surfaceIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CreatePbufferSurfaceBenchmark( Benchmark ):
    def __init__( self, format ):
        Benchmark.__init__( self )
        self.format = format
        self.name = "OPENGLES2 create pbuffer surface %s" % ( pixmapFormatToStr( self.format ), )

    def build( self, target, modules ):
        Egl = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]

        a = mapColorFormatToAttributes( self.format )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       a[ CONFIG_BUFFERSIZE ],
                       a[ CONFIG_REDSIZE ],
                       a[ CONFIG_GREENSIZE ],
                       a[ CONFIG_BLUESIZE ],
                       a[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CreatePbufferSurface( displayIndex,
                                  surfaceIndex,
                                  configIndex,
                                  width,
                                  height,
                                  'EGL_NO_TEXTURE', 'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        Egl.DestroySurface( displayIndex, surfaceIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CreatePixmapSurfaceBenchmark( Benchmark ):
    def __init__( self, format ):
        Benchmark.__init__( self )
        self.format = format
        self.name = "OPENGLES2 create pixmap surface %s" % ( pixmapFormatToStr( self.format ), )

    def build( self, target, modules ):
        Egl = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]

        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex,
                          width, height,
                          self.format,
                          -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENGL_ES2_BIT' ] ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex,
                             configIndex,
                             pixmapIndex,
                             attributes )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CreatePixmapSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 pixmapIndex,
                                 'EGL_NONE',
                                 'EGL_NONE' )

        Egl.DestroySurface( displayIndex, surfaceIndex )

######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )

    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh

######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SwapBehaviorBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, swapBehavior ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.swapBehavior = swapBehavior
        self.name = 'OPENGLES2 swap behavior %s grd=%dx%dx%d tex=%s' % ( swapBehaviorToStr( self.swapBehavior ),
                                                                         self.gridx,
                                                                         self.gridy,
                                                                         self.overdraw,
                                                                         textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        if self.swapBehavior != 'EGL_BUFFER_DESTROYED':
            attribs[ CONFIG_SWAP_BEHAVIOR ] = self.swapBehavior

        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        if self.swapBehavior != 'EGL_BUFFER_DESTROYED':
            Egl.SurfaceAttrib( displayIndex,
                               surfaceIndex,
                               EGL_DEFINES[ 'EGL_SWAP_BEHAVIOR' ],
                               EGL_DEFINES[ self.swapBehavior ] )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SwapIntervalBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, swapInterval ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.textureType  = textureType
        self.swapInterval = swapInterval
        self.name = 'OPENGLES2 swap interval %d grd=%dx%dx%d tex=%s' % ( self.swapInterval,
                                                                         self.gridx,
                                                                         self.gridy,
                                                                         self.overdraw,
                                                                         textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        Egl.SwapInterval( displayIndex, self.swapInterval )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class SurfaceScalingBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, windowR, surfaceR, targetR ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureType   = textureType

        if windowR:
            self.windowRR  = windowR[ 0 ]
            self.windowR   = windowR[ 1 : ]
        else:
            self.windowR   = None

        if surfaceR:
            assert targetR
            self.surfaceRR = surfaceR[ 0 ]
            self.surfaceR  = surfaceR[ 1 : ]
        else:
            self.surfaceR  = None

        if targetR:
            assert surfaceR
            self.targetRR  = targetR[ 0 ]
            self.targetR   = targetR[ 1 : ]
        else:
            self.targetR   = None

        self.name = "OPENGLES2 surface scaling wr=%s sr=%s tr=%s grd=%dx%dx%d tex=%s" % ( farrayToStr( self.windowR ),
                                                                                          farrayToStr( self.surfaceR ),
                                                                                          farrayToStr( self.targetR ),
                                                                                          self.gridx,
                                                                                          self.gridy,
                                                                                          self.overdraw,
                                                                                          textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        windowSize = [ screenWidth, screenHeight ]
        if self.windowR:
            if self.windowRR:
                windowSize[ 0 ] = int( self.windowR[ 0 ] * screenWidth )
                windowSize[ 1 ] = int( self.windowR[ 1 ] * screenHeight )
            else:
                windowSize[ 0 ] = int( self.windowR[ 0 ] )
                windowSize[ 1 ] = int( self.windowR[ 1 ] )

        surfaceSize = [ screenWidth, screenHeight ]
        if self.surfaceR:
            if self.surfaceRR:
                surfaceSize[ 0 ] = int( self.surfaceR[ 0 ] * screenWidth )
                surfaceSize[ 1 ] = int( self.surfaceR[ 1 ] * screenHeight )
            else:
                surfaceSize[ 0 ] = int( self.surfaceR[ 0 ] )
                surfaceSize[ 1 ] = int( self.surfaceR[ 1 ] )

        targetExtent = [ 0, 0, screenWidth, screenHeight ]
        if self.targetR:
            if self.targetRR:
                targetExtent[ 0 ] = int( self.targetR[ 0 ] * screenWidth )
                targetExtent[ 1 ] = int( self.targetR[ 1 ] * screenHeight )
                targetExtent[ 2 ] = int( self.targetR[ 2 ] * screenWidth )
                targetExtent[ 3 ] = int( self.targetR[ 3 ] * screenHeight )
            else:
                targetExtent[ 0 ] = int( self.targetR[ 0 ] )
                targetExtent[ 1 ] = int( self.targetR[ 1 ] )
                targetExtent[ 2 ] = int( self.targetR[ 2 ] )
                targetExtent[ 3 ] = int( self.targetR[ 3 ] )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )

        Egl.CheckExtension( displayIndex, 'EGL_NOK_surface_scaling' )

        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, windowSize[ 0 ], windowSize[ 1 ], 'SCT_COLOR_FORMAT_DEFAULT' )

        surfaceBits = EGL_DEFINES[ 'EGL_WINDOW_BIT' ]

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        gles2ConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        eglConfigAttributes = [ EGL_DEFINES[ 'EGL_BUFFER_SIZE' ],         attrs[ CONFIG_BUFFERSIZE ],
                                EGL_DEFINES[ 'EGL_RED_SIZE' ],            attrs[ CONFIG_REDSIZE ],
                                EGL_DEFINES[ 'EGL_GREEN_SIZE' ],          attrs[ CONFIG_GREENSIZE ],
                                EGL_DEFINES[ 'EGL_BLUE_SIZE' ],           attrs[ CONFIG_BLUESIZE ],
                                EGL_DEFINES[ 'EGL_ALPHA_SIZE' ],          attrs[ CONFIG_ALPHASIZE ],
                                EGL_DEFINES[ 'EGL_DEPTH_SIZE' ],          convertCInt( attrs[ CONFIG_DEPTHSIZE ] ),
                                EGL_DEFINES[ 'EGL_SURFACE_TYPE' ],        surfaceBits,
                                EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],     EGL_DEFINES[ 'EGL_OPENGL_ES2_BIT' ]
                                ]

        if self.surfaceR:
            eglConfigAttributes += [ EGL_DEFINES[ 'EGL_SURFACE_SCALING_NOK' ], EGL_DEFINES[ 'EGL_TRUE' ] ]

        Egl.Config( displayIndex, gles2ConfigIndex, eglConfigAttributes )

        eglWindowSurfaceAttributes = [ EGL_DEFINES[ 'EGL_RENDER_BUFFER' ], EGL_DEFINES[ 'EGL_BACK_BUFFER' ] ]

        if self.surfaceR:
            eglWindowSurfaceAttributes +=   [ EGL_DEFINES[ 'EGL_FIXED_WIDTH_NOK' ], surfaceSize[ 0 ],
                                              EGL_DEFINES[ 'EGL_FIXED_HEIGHT_NOK' ], surfaceSize[ 1 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_OFFSET_X_NOK' ], targetExtent[ 0 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_OFFSET_Y_NOK' ], targetExtent[ 1 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_WIDTH_NOK' ], targetExtent[ 2 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_HEIGHT_NOK' ], targetExtent[ 3 ],
                                              EGL_DEFINES[ 'EGL_BORDER_COLOR_RED_NOK' ], 127,
                                              EGL_DEFINES[ 'EGL_BORDER_COLOR_GREEN_NOK' ], 127,
                                              EGL_DEFINES[ 'EGL_BORDER_COLOR_BLUE_NOK' ], 127
                                              ]

        gles2SurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurfaceExt( displayIndex,
                                    gles2SurfaceIndex,
                                    gles2ConfigIndex,
                                    windowIndex,
                                    eglWindowSurfaceAttributes )

        gles2ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           gles2ContextIndex,
                           gles2ConfigIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         gles2SurfaceIndex,
                         gles2SurfaceIndex,
                         gles2ContextIndex )

        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                surfaceSize[ 0 ], surfaceSize[ 1 ],
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, gles2SurfaceIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CreateEglImageBenchmark( Benchmark ):
    def __init__( self, imageWidth, imageHeight, format ):
        Benchmark.__init__( self )
        self.imageWidth  = imageWidth
        self.imageHeight = imageHeight
        self.format      = format
        self.name = 'OPENGLES2 create egl image %dx%d %s' % ( self.imageWidth,
                                                              self.imageHeight,
                                                              imageTypeToStr( self.format ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Images    = modules[ 'Images' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )

        if self.format == 'SCT_COLOR_FORMAT_YUV12':
            imagesRawDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
            Images.CreateData( imagesRawDataIndex,
                               'IMAGES_FLOWER',
                               'IMAGES_RGB8',
                               self.imageWidth, self.imageHeight )
            Images.ConvertData( imagesRawDataIndex,
                                imagesDataIndex,
                                'IMAGES_YUV12' )
        else:
            IMAGES_FORMATS = { 'SCT_COLOR_FORMAT_RGB565'      : 'IMAGES_RGB565',
                               'SCT_COLOR_FORMAT_RGB888'      : 'IMAGES_RGB8',
                               'SCT_COLOR_FORMAT_RGBA8888'    : 'IMAGES_RGBA8',
                               }

            Images.CreateData( imagesDataIndex,
                               'IMAGES_FLOWER',
                               IMAGES_FORMATS[ self.format ],
                               self.imageWidth, self.imageHeight )

        eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
        Images.CopyToEgl( imagesDataIndex, eglDataIndex )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.imageWidth, self.imageHeight,
                                self.format,
                                [ 'SCT_USAGE_OPENGLES2_TEXTURE_2D' ],
                                eglDataIndex )

        # Create egl image from shared pixmap.
        imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            -1,
                            'EGL_NATIVE_BUFFER',
                            eglPixmapIndex,
                            imageAttributes )

        Egl.DestroyImage( displayIndex, eglImageIndex )
        Egl.DestroyPixmap( eglPixmapIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class EglImageBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, format, eglImage ):
        Benchmark.__init__( self )
        self.gridx      = gridx
        self.gridy      = gridy
        self.overdraw   = overdraw
        self.format     = format
        self.eglImage   = eglImage
        self.name = 'OPENGLES2 egl image %sgrd=%dx%dx%d img=%s' % ( { False : 'texture ', True : 'egl image ' }[ self.eglImage ],
                                                                    self.gridx,
                                                                    self.gridy,
                                                                    self.overdraw,
                                                                    imageTypeToStr( self.format ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Images    = modules[ 'Images' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        rawExtFragmentShaderSource = """
        #extension GL_OES_EGL_image_external : require\\n
        precision mediump float;
        uniform samplerExternalOES gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource = formatShader( rawVertexShaderSource )

        if self.eglImage:
            if self.format == 'SCT_COLOR_FORMAT_YUV12':
                fragmentShaderSource = formatShader( rawExtFragmentShaderSource )
                eglImageTarget = 'GL_TEXTURE_EXTERNAL_OES'
                #eglImageTarget = 'GL_TEXTURE_2D'
            else:
                fragmentShaderSource = formatShader( rawFragmentShaderSource )
                eglImageTarget = 'GL_TEXTURE_2D'
        else:
            fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )

        if self.format == 'SCT_COLOR_FORMAT_YUV12':
            imagesRawDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
            Images.CreateData( imagesRawDataIndex,
                               'IMAGES_FLOWER',
                               'IMAGES_RGB8',
                               width, height )
            Images.ConvertData( imagesRawDataIndex,
                                imagesDataIndex,
                                'IMAGES_YUV12' )
        else:
            IMAGES_FORMATS = { 'SCT_COLOR_FORMAT_RGB565'      : 'IMAGES_RGB565',
                               'SCT_COLOR_FORMAT_RGB888'      : 'IMAGES_RGB8',
                               'SCT_COLOR_FORMAT_RGBA8888'    : 'IMAGES_RGBA8',
                               }

            Images.CreateData( imagesDataIndex,
                               'IMAGES_FLOWER',
                               IMAGES_FORMATS[ self.format ],
                               width, height )

        OpenGLES2.ActiveTexture( 0 )

        if self.eglImage:
            eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
            Images.CopyToEgl( imagesDataIndex, eglDataIndex )

            eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
            Egl.CreateSharedPixmap( eglPixmapIndex,
                                    width, height,
                                    self.format,
                                    [ 'SCT_USAGE_OPENGLES2_TEXTURE_2D' ],
                                    eglDataIndex )

            # Create egl image from shared pixmap.
            imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]
            eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
            Egl.CreateImageExt( displayIndex,
                                eglImageIndex,
                                -1,
                                'EGL_NATIVE_BUFFER',
                                eglPixmapIndex,
                                imageAttributes )
        else:
            opengles2DataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            Images.CopyToOpenGLES2( imagesDataIndex, opengles2DataIndex )

            FORMATS_MAP = { 'SCT_COLOR_FORMAT_RGB565'   : 'OPENGLES2_RGB565',
                            'SCT_COLOR_FORMAT_RGB888'   : 'OPENGLES2_RGB888',
                            'SCT_COLOR_FORMAT_RGBA8888' : 'OPENGLES2_RGBA8888'
                            }

            textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateTextureData( textureDataIndex,
                                         opengles2DataIndex,
                                         width, height,
                                         FORMATS_MAP[ self.format ] )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )

        if self.eglImage:
            OpenGLES2.BindTexture( eglImageTarget, textureIndex )

            # Set texture data from egl image.
            OpenGLES2.EglImageTargetTexture2D( eglImageTarget, eglImageIndex )

            OpenGLES2.TexParameter( eglImageTarget, 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        else:
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
            OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )

        offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class TextureStreamingBenchmark( Benchmark ):
    def __init__( self, format, eglImage, stream, drawop ):
        Benchmark.__init__( self )
        self.format         = format
        self.eglImage       = eglImage
        self.stream         = stream
        self.drawop         = drawop
        self.logCpu         = False

        # For studying the difference in CPU usage between glTexImage2D and
        # egl image texture streaming.
        #self.logCpu         = True
        #self.repeats        = 180

        if not self.drawop in [ 'no draw', 'direct blit', 'rot90', 'rot180', 'rot270', 'hflip', 'vflip' ]:
            raise "Unexpected operation %d" % self.drawop
        drawString = 'with %s' % self.drawop

        if self.drawop == 'no draw' and self.stream == False:
            raise 'Empty benchmark loop'

        self.name = 'OPENGLES2 fullscreen %s %s %s %s' % ( { False : 'texture', True : 'egl image' }[ self.eglImage ],
                                                           { False : 'draw', True : 'streaming' }[ self.stream ],
                                                           drawString,
                                                           imageTypeToStr( self.format ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        rawExtFragmentShaderSource = """
        #extension GL_OES_EGL_image_external : require\\n
        precision mediump float;
        uniform samplerExternalOES gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource = formatShader( rawVertexShaderSource )

        if self.eglImage:
            if self.format == 'SCT_COLOR_FORMAT_YUV12':
                fragmentShaderSource = formatShader( rawExtFragmentShaderSource )
                eglImageTarget = 'GL_TEXTURE_EXTERNAL_OES'
                #eglImageTarget = 'GL_TEXTURE_2D'
            else:
                fragmentShaderSource = formatShader( rawFragmentShaderSource )
                eglImageTarget = 'GL_TEXTURE_2D'
        else:
            fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        mesh = createPlane( 2, 2 )

        # Manipulate mesh texture coordinates according to the drawing
        # operation.
        if self.drawop == 'rot90':
            mesh.texCoords[ 0 ] = [ 1, 0, 1, 1, 0, 0, 0, 1 ]
        elif self.drawop == 'rot180':
            mesh.texCoords[ 0 ] = [ 1, 1, 0, 1, 1, 0, 0, 0 ]
        elif self.drawop == 'rot270':
            mesh.texCoords[ 0 ] = [ 0, 1, 0, 0, 1, 1, 1, 0 ]
        elif self.drawop == 'hflip':
            mesh.texCoords[ 0 ] = [ 0, 1, 1, 1, 0, 0, 1, 0 ]
        elif self.drawop == 'vflip':
            mesh.texCoords[ 0 ] = [ 1, 0, 0, 0, 1, 1, 0, 1 ]

        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )

        if self.format == 'SCT_COLOR_FORMAT_YUV12':
            imagesRawDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
            Images.CreateData( imagesRawDataIndex,
                               'IMAGES_FLOWER',
                               'IMAGES_RGB8',
                               width, height )
            Images.ConvertData( imagesRawDataIndex,
                                imagesDataIndex,
                                'IMAGES_YUV12' )
        else:
            IMAGES_FORMATS = { 'SCT_COLOR_FORMAT_RGB565'      : 'IMAGES_RGB565',
                               'SCT_COLOR_FORMAT_RGB888'      : 'IMAGES_RGB8',
                               'SCT_COLOR_FORMAT_RGBA8888'    : 'IMAGES_RGBA8',
                               }

            Images.CreateData( imagesDataIndex,
                               'IMAGES_FLOWER',
                               IMAGES_FORMATS[ self.format ],
                               width, height )

        OpenGLES2.ActiveTexture( 0 )

        if self.eglImage:
            eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
            Images.CopyToEgl( imagesDataIndex, eglDataIndex )

            eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
            Egl.CreateSharedPixmap( eglPixmapIndex,
                                    width, height,
                                    self.format,
                                    [ 'SCT_USAGE_OPENGLES2_TEXTURE_2D' ],
                                    eglDataIndex )

            # Create egl image from shared pixmap.
            imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]
            eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
            Egl.CreateImageExt( displayIndex,
                                eglImageIndex,
                                -1,
                                'EGL_NATIVE_BUFFER',
                                eglPixmapIndex,
                                imageAttributes )
        else:
            opengles2DataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            Images.CopyToOpenGLES2( imagesDataIndex, opengles2DataIndex )

            FORMATS_MAP = { 'SCT_COLOR_FORMAT_RGB565'   : 'OPENGLES2_RGB565',
                            'SCT_COLOR_FORMAT_RGB888'   : 'OPENGLES2_RGB888',
                            'SCT_COLOR_FORMAT_RGBA8888' : 'OPENGLES2_RGBA8888'
                            }

            textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateTextureData( textureDataIndex,
                                         opengles2DataIndex,
                                         width, height,
                                         FORMATS_MAP[ self.format ] )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        if self.eglImage:
            # NOTE: Trying to modify EGL image data while the EGL image is still being 
            # used as a texture seems to cause a deadlock in some systems (e.g. Nexus7); 
            # hence, create a new texture for every frame.

            #OpenGLES2.GenTexture( textureIndex )
            #OpenGLES2.BindTexture( eglImageTarget, textureIndex )

            # Set texture data from egl image.
            #OpenGLES2.EglImageTargetTexture2D( eglImageTarget, eglImageIndex )

            #OpenGLES2.TexParameter( eglImageTarget, 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
            pass
        else:
            OpenGLES2.GenTexture( textureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
            OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.stream:
            if self.eglImage:
               Egl.SetPixmapData( eglPixmapIndex, eglDataIndex )

               # See the above NOTE.
               OpenGLES2.GenTexture( textureIndex )
               OpenGLES2.BindTexture( eglImageTarget, textureIndex )
               OpenGLES2.EglImageTargetTexture2D( eglImageTarget, eglImageIndex )
               OpenGLES2.TexParameter( eglImageTarget, 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
            else:
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        if self.drawop != 'no draw':
            drawMeshBufferData( modules, meshBufferDataSetup )
            Egl.SwapBuffers( displayIndex, surfaceIndex )

        if self.stream:
            if self.eglImage:
                # See the above NOTE.
                OpenGLES2.BindTexture( eglImageTarget, -1 )
                OpenGLES2.DeleteTexture( textureIndex )

        if self.logCpu:
            Test.LogCpuLoad( '"SYSTEM_LOAD ' + self.name + '"', 'SCT_CPULOAD_SYSTEM', self.repeats, self.repeats * 2 );
            Test.LogCpuLoad( '"PROCESS2BUSY_LOAD ' + self.name + '"', 'SCT_CPULOAD_PROCESS2BUSY', self.repeats, self.repeats * 2 );
            Test.LogCpuLoad( '"PROCESS2TOTAL_LOAD ' + self.name + '"', 'SCT_CPULOAD_PROCESS2TOTAL', self.repeats, self.repeats * 2 );


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SingleThreadFenceBenchmark( Benchmark ):
    def __init__( self, fence, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.fence       = fence
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 single thread fence %s grd=%dx%dx%d tex=%s' % ( { False : "no-sync", True : 'sync' }[ self.fence ],
                                                                               self.gridx,
                                                                               self.gridy,
                                                                               self.overdraw,
                                                                               textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        textureW  = screenWidth
        textureH  = screenHeight

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        if self.fence:
            syncIndex = indexTracker.allocIndex( 'EGL_SYNC_INDEX' )
            Egl.CreateSync( displayIndex,
                            syncIndex,
                            'EGL_SYNC_FENCE_KHR',
                            [] )
            Egl.ClientWaitSync( displayIndex,
                                syncIndex,
                                [ 'EGL_SYNC_FLUSH_COMMANDS_BIT_KHR' ],
                                [ 5000, 1000000 ] )
            Egl.DestroySync( displayIndex, syncIndex, 'ON' )

        target.swapBuffers( state )

