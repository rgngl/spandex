#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    colorAttribute    = None
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    if colorAttributes:
        colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

    if texCoordAttributes:
        for i in range( len( texCoordAttributes ) ):
            texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                             2,
                                                             [ float( tcx ),
                                                               float( tcy ) ] ) )

    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           colorAttribute,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------        
class MipmapBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureWidth, textureHeight, textureType, minFiltering, magFiltering, wrap ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType
        self.minFiltering  = minFiltering
        self.magFiltering  = magFiltering
        self.wrap          = wrap
        self.name = 'OPENGLES2 mipmap tex=%dx%d %s fltr mn=%s mg=%s wrp=%s grd=%dx%dx%d' % ( self.textureWidth,
                                                                                             self.textureHeight,
                                                                                             textureTypeToStr( self.textureType ),
                                                                                             filteringToStr( self.minFiltering ),
                                                                                             filteringToStr( self.magFiltering ),
                                                                                             wrapToStr( self.wrap ),
                                                                                             self.gridx,
                                                                                             self.gridy,
                                                                                             self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()     
        rx = 2.2 / ( self.textureWidth / float( screenWidth ) )
        ry = 2.2 / ( self.textureHeight / float( screenHeight ) )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, rx, ry, False, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
        
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                             [ 0.2, 0.2, 0.2, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ] ,
                                             self.textureWidth, self.textureHeight,
                                             'ON',
                                             self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                self.minFiltering,
                                self.magFiltering,
                                self.wrap,
                                self.wrap )

        textureUnit = 0
        OpenGLES2.ActiveTexture( textureUnit )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )

        OpenGLES2.Uniformi( program.textureLocationIndices[ textureUnit ], 1, [ textureUnit ] )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )

        
