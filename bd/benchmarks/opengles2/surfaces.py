#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from lib.egl       import *
from common        import *

######################################################################
READ1PIXEL  = 'read1pixel'
READPIXELS  = 'readpixels'
COPYBUFFERS = 'copybuffers'
FINISH      = 'finish'

######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class WindowSurfaceBenchmark( Benchmark ):
    def __init__( self, format, buffer, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.format      = format
        self.buffer      = buffer
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 window surface %s bffr=%s grd=%dx%dx%d tex=%s' % ( pixmapFormatToStr( self.format ),
                                                                                  bufferToStr( self.buffer ),
                                                                                  self.gridx,
                                                                                  self.gridy,
                                                                                  self.overdraw,
                                                                                  textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()        
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        if self.format == 'SCT_COLOR_FORMAT_DEFAULT':
            attrs = target.getDefaultAttributes( API_OPENGLES2 )
        else:
            attrs = mapColorFormatToAttributes( self.format )

        gles2ConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )       
        Egl.RGBConfig( displayIndex,
                       gles2ConfigIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT'])

        gles2SurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 gles2SurfaceIndex,
                                 gles2ConfigIndex,
                                 windowIndex,
                                 self.buffer,
                                 'EGL_NONE',
                                 'EGL_NONE' )
       
        gles2ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           gles2ContextIndex,
                           gles2ConfigIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         gles2SurfaceIndex,
                         gles2SurfaceIndex,
                         gles2ContextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        if self.buffer == 'EGL_SINGLE_BUFFER':
            OpenGLES2.Finish()
        else:
            Egl.SwapBuffers( displayIndex, gles2SurfaceIndex )

            
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PbufferSurfaceBenchmark( Benchmark ):
    def __init__( self, format, flush, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.format      = format
        self.flush       = flush
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 pbuffer surface %s flsh=%s grd=%dx%dx%d tex=%s' % ( pixmapFormatToStr( self.format ),
                                                                                   self.flush,
                                                                                   self.gridx,
                                                                                   self.gridy,
                                                                                   self.overdraw,
                                                                                   textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl    = modules[ 'Egl' ]       

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()        
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]

        a = mapColorFormatToAttributes( self.format )

        gles2ConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       gles2ConfigIndex,
                       a[ CONFIG_BUFFERSIZE ],
                       a[ CONFIG_REDSIZE ],
                       a[ CONFIG_GREENSIZE ],
                       a[ CONFIG_BLUESIZE ],
                       a[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        gles2SurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  gles2SurfaceIndex,
                                  gles2ConfigIndex,
                                  width,
                                  height,
                                  'EGL_NO_TEXTURE',
                                  'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',                                  
                                  'EGL_NONE',
                                  'EGL_NONE' )
       
        gles2ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           gles2ContextIndex,
                           gles2ConfigIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         gles2SurfaceIndex,
                         gles2SurfaceIndex,
                         gles2ContextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        if self.flush == READ1PIXEL:
            dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            OpenGLES2.CreateData( dataIndex, 1 * 1 * 4  ) # RGBA8888
        elif self.flush == READPIXELS:
            dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            OpenGLES2.CreateData( dataIndex, width * height * 4  ) # RGBA8888
        if self.flush == COPYBUFFERS:
            pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
            Egl.CreatePixmap( pixmapIndex,
                              width,
                              height,
                              self.format,
                              -1 )

        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        if self.flush == READ1PIXEL:
            OpenGLES2.ReadPixels( dataIndex,
                                  width / 2, height / 2,
                                  1, 1 )
        elif self.flush == READPIXELS:
            OpenGLES2.ReadPixels( dataIndex,
                               0, 0,
                               width, height )
        elif self.flush == COPYBUFFERS:
            Egl.CopyBuffers( displayIndex,
                             gles2SurfaceIndex,
                             pixmapIndex )
        elif self.flush == FINISH:
            OpenGLES2.Finish()
        else:
            raise 'Unexpected flush'

            
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PixmapSurfaceBenchmark( Benchmark ):
    def __init__( self, format, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.format      = format
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType

        self.name = 'OPENGLES2 pixmap surface %s grd=%dx%dx%d tex=%s' % ( pixmapFormatToStr( self.format ),
                                                                          self.gridx,
                                                                          self.gridy,
                                                                          self.overdraw,
                                                                          textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position  = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( width, height, ) = target.getScreenSize()        
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]

        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex, width, height, self.format, -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENGL_ES2_BIT' ],
                       EGL_DEFINES[ 'EGL_DEPTH_SIZE' ],        16 ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex,
                             configIndex,
                             pixmapIndex,
                             attributes )
        
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 pixmapIndex,
                                 'EGL_NONE',
                                 'EGL_NONE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
       
        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        OpenGLES2.Finish()
