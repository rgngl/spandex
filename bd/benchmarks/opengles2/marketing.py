#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
FRONT_TO_BACK = 'front-to-back'
BACK_TO_FRONT = 'back-to-front'

######################################################################
def createPlane( w, h, colorAttributes, texCoordAttributes ):
    vertexA   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    colorA    = None
    texCoordA = []
    indexA    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

    if colorAttributes:
        colorA = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

    if texCoordAttributes:
        for i in range( len( texCoordAttributes ) ):
            texCoordA.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )

    mesh = MeshStripPlane( w,
                           h,
                           vertexA,
                           colorA,
                           None,
                           texCoordA,
                           indexA,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh

######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, cx, cy, textureType, minF, magF ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        cx, cy,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            minF,
                            magF,
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SmoothPixelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES2 marketing smooth pixels grd=%dx%dx%d" % ( self.gridx,
                                                                         self.gridy,
                                                                         self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gColor;
        varying vec4 gVSColor;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSColor = gColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        varying vec4 gVSColor;

        void main()
        {
            gl_FragColor = gVSColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class FlatTexelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 marketing flat texels grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                              self.gridy,
                                                                              self.overdraw,
                                                                              textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, False, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                4, 4,
                                                self.textureType,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )

        useTexture( modules, textureSetup, 0, program )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SmoothTexelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 marketing smooth texels grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                                self.gridy,
                                                                                self.overdraw,
                                                                                textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gColor;
        attribute vec4 gTexCoord0;
        varying vec4 gVSColor;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
           gVSColor = gColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSColor;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = gVSColor * texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                4, 4,
                                                self.textureType,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )

        useTexture( modules, textureSetup, 0, program )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class FlatMultitexelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, texture1Type, texture2Type ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.texture1Type = texture1Type
        self.texture2Type = texture2Type
        self.name = 'OPENGLES2 marketing flat multitexels grd=%dx%dx%d tex1=%s tex2=%s' % ( self.gridx,
                                                                                            self.gridy,
                                                                                            self.overdraw,
                                                                                            textureTypeToStr( self.texture1Type ),
                                                                                            textureTypeToStr( self.texture2Type ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        attribute vec4 gTexCoord1;
        varying vec4 gVSTexCoord0;
        varying vec4 gVSTexCoord1;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
           gVSTexCoord1 = gTexCoord1;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        uniform sampler2D gTexture1;
        varying vec4 gVSTexCoord0;
        varying vec4 gVSTexCoord1;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy ) * texture2D( gTexture1, gVSTexCoord1.xy ).x;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, False, [ True, True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 1.0, 0.3, 0.3, 0.8 ],
                                                [ 0.3, 1.0, 0.3, 0.8 ],
                                                4, 4,
                                                self.texture1Type,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )
        texture1Setup           = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 0.4, 0.0, 0.0, 0.8 ],
                                                [ 0.6, 0.0, 0.0, 0.8 ],
                                                2, 2,
                                                self.texture2Type,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )

        useTexture( modules, texture0Setup, 0, program )
        useTexture( modules, texture1Setup, 1, program )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SmoothMultitexelsBenchmark( Benchmark ):
    def __init__( self,  gridx, gridy, overdraw, texture1Type, texture2Type ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.overdraw     = overdraw
        self.texture1Type = texture1Type
        self.texture2Type = texture2Type
        self.name = 'OPENGLES2 marketing smooth multitexels grd=%dx%dx%d tex1=%s tex2=%s' % ( self.gridx,
                                                                                              self.gridy,
                                                                                              self.overdraw,
                                                                                              textureTypeToStr( self.texture1Type ),
                                                                                              textureTypeToStr( self.texture2Type ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gColor;
        attribute vec4 gTexCoord0;
        attribute vec4 gTexCoord1;
        varying vec4 gVSTexCoord0;
        varying vec4 gVSTexCoord1;
        varying vec4 gVSColor;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSColor = gColor;
           gVSTexCoord0 = gTexCoord0;
           gVSTexCoord1 = gTexCoord1;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        uniform sampler2D gTexture1;
        varying vec4 gVSColor;
        varying vec4 gVSTexCoord0;
        varying vec4 gVSTexCoord1;

        void main()
        {
           gl_FragColor = gVSColor * texture2D( gTexture0, gVSTexCoord0.xy ) * texture2D( gTexture1, gVSTexCoord1.xy ).x;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [ True, True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        texture0Setup           = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 1.0, 0.3, 0.3, 0.8 ],
                                                [ 0.3, 1.0, 0.3, 0.8 ],
                                                4, 4,
                                                self.texture1Type,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )
        texture1Setup           = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 0.4, 0.0, 0.0, 0.8 ],
                                                [ 0.6, 0.0, 0.0, 0.8 ],
                                                2, 2,
                                                self.texture2Type,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )

        useTexture( modules, texture0Setup, 0, program )
        useTexture( modules, texture1Setup, 1, program )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PixelFillrateBenchmark( Benchmark ):
    def __init__( self, overdraw ):
        Benchmark.__init__( self )
        self.overdraw = overdraw
        self.name = "OPENGLES2 marketing pixel fillrate ovrdrw=%d" % ( self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gColor;
        varying vec4 gVSColor;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSColor = gColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        varying vec4 gVSColor;

        void main()
        {
            gl_FragColor = gVSColor;
        }
        """

        vertexShaderSource      = formatShader( rawVertexShaderSource )
        fragmentShaderSource    = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( 2, 2, True, [] )

        for i in range( len( mesh.colors ) ):
            if ( i + 1 ) % 4 == 0:
                mesh.colors[ i ] = 127

        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.Enable( 'GL_BLEND' )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 0.0 ] )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

######################################################################
def createPlane2( s, t ):
    vertexA   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordA = []
    indexA    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

    texCoordA.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ s, t ] ) )

    mesh = MeshStripPlane( 2,
                           2,
                           vertexA,
                           None,
                           None,
                           texCoordA,
                           indexA,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class TexelFillrateBenchmark( Benchmark ):
    def __init__( self, overdraw, textureType, pot ):
        Benchmark.__init__( self )
        self.overdraw    = overdraw
        self.textureType = textureType
        self.pot         = pot
        self.name = "OPENGLES2 marketing texel fillrate ovrdrw=%d tex=%s%s" % ( self.overdraw,
                                                                                { True : 'POT ', False : '' }[ self.pot ],
                                                                                textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        textureWidth, textureS  = toPot( screenWidth, self.pot )
        textureHeight, textureT = toPot( screenHeight, self.pot )

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane2( textureS, textureT )

        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureWidth, textureHeight,
                                                [ 1.0, 0.3, 0.3, 0.5 ],
                                                [ 0.3, 1.0, 0.3, 0.5 ],
                                                4, 4,
                                                self.textureType,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )

        useTexture( modules, textureSetup, 0, program )

        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.Enable( 'GL_BLEND' )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 0.0 ] )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RotatedTexelFillrateBenchmark( Benchmark ):
    def __init__( self, overdraw, textureType ):
        Benchmark.__init__( self )
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = "OPENGLES2 marketing rotated texel fillrate ovrdrw=%d tex=%s" % ( self.overdraw,
                                                                                      textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        vsSource = """
        precision mediump float;
        attribute vec4 position_in;
        attribute vec4 uv_in;
        uniform mat4 mvp;
        uniform mat4 texm;
        varying vec4 uv;

        void main()
        {
           gl_Position = mvp * position_in;
           uv = texm * uv_in;
        }"""

        fsSource = """
        precision mediump float;
        uniform sampler2D tex0;
        varying vec4 uv;

        void main()
        {
           gl_FragColor = texture2D(tex0, uv.xy) * 1.1;
        }"""

        vertexShaderSource   = formatShader( vsSource )
        fragmentShaderSource = formatShader( fsSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupGL2( modules, indexTracker, target )

        backgroundWidth  = screenWidth
        backgroundHeight = screenHeight

        backgroundVertexColorAttribute  = None
        backgroundTexCoordAttribute     = []

        backgroundTextureWidth  = 32
        backgroundTextureHeight = 32

        backgroundTextureS  = 1.0
        backgroundTextureT  = 1.0

        ######################################################################
        # Create texture data                                                #
        ######################################################################

        backgroundTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        backgroundTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        OpenGLES2.CreateCheckerTextureData( backgroundTextureDataIndex,
                                            [ 1.0, 0.3, 0.3, 0.5 ],
                                            [ 0.3, 1.0, 0.3, 0.5 ],
                                            4, 4,
                                            backgroundTextureWidth,
                                            backgroundTextureHeight,
                                            'OFF',
                                            self.textureType )

        OpenGLES2.GenTexture( backgroundTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST',
                                'GL_NEAREST',
                                'GL_REPEAT',
                                'GL_REPEAT' )

        OpenGLES2.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )

        backgroundTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                               2,
                                                               [ backgroundTextureS,
                                                                 backgroundTextureT ] ) ]

        ######################################################################
        # Create geometry                                                    #
        ######################################################################

        background = MeshStripPlane( 2,
                                     2,
                                     MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                     backgroundVertexColorAttribute,
                                     None,
                                     backgroundTexCoordAttribute,
                                     MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                     None,
                                     MESH_CCW_WINDING,
                                     False )

        background.translate( [ -0.5, -0.5, 0 ] )
        background.scale( [ 2.0 * backgroundWidth / float( screenWidth ),
                            2.0 * backgroundHeight / float( screenHeight ) ] )

        backgroundVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( backgroundVertexArrayIndex,
                               background.vertexGLType )
        OpenGLES2.AppendToArray( backgroundVertexArrayIndex, background.vertices )

        backgroundTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( backgroundTexCoordArrayIndex,
                               background.texCoordsGLTypes[ 0 ] )
        OpenGLES2.AppendToArray( backgroundTexCoordArrayIndex, background.texCoords[ 0 ] )

        backgroundIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( backgroundIndexArrayIndex, background.indexGLType );
        OpenGLES2.AppendToArray( backgroundIndexArrayIndex, background.indices )

        ######################################################################
        # Shaders                                                            #
        ######################################################################

        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )

        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      vertexLocationIndex,
                                      "position_in" )

        OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                       backgroundVertexArrayIndex,
                                       2,
                                       'OFF',
                                       0 )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      texCoordLocationIndex,
                                      "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.ValidateProgram( programIndex )
        OpenGLES2.UseProgram( programIndex )
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )

        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

        OpenGLES2.VertexAttribPointer( texCoordLocationIndex,
                                       backgroundTexCoordArrayIndex,
                                       2,
                                       'OFF',
                                       0 )

        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        mvpLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mvpLocationIndex, programIndex, 'mvp' )

        texmLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( texmLocationIndex, programIndex, 'texm' )

        OpenGLES2.UniformMatrix( mvpLocationIndex,
                                 1,
                                 [ 1.0, 0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0, 1.0 ] )

        a = 45
        OpenGLES2.UniformMatrix( texmLocationIndex,
                                 1,
                                 [  math.cos( a ), math.sin( a ), 0.0, 0.0,
                                   -math.sin( a ), math.cos( a ), 0.0, 0.0,
                                   0.0,            0.0,           1.0, 0.0,
                                   0.0,            0.0,           0.0, 1.0 ] )

        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
        OpenGLES2.Enable( 'GL_BLEND' )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 0.0 ] )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        for i in range( self.overdraw ):
            OpenGLES2.DrawElements( background.glMode,
                                    len( background.indices ),
                                    0,
                                    backgroundIndexArrayIndex )

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class FlatTexelsBgraSwizzleBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 marketing flat texels bgra swizzle grd=%dx%dx%d tex=%s' % ( self.gridx,
                                                                                           self.gridy,
                                                                                           self.overdraw,
                                                                                           textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy ).bgra;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, False, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                32, 32,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                4, 4,
                                                self.textureType,
                                                'GL_NEAREST',
                                                'GL_NEAREST' )

        useTexture( modules, textureSetup, 0, program )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

######################################################################
def createPlane3( w, h ):
    mesh = MeshTrianglePlane( w,
                              h,
                              MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                              None,
                              None,
                              None,
                              MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                              None,
                              MESH_CCW_WINDING,
                              False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class UniformPixelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, tpd, ucount, draworder ):
        Benchmark.__init__( self )
        self.gridx     = gridx
        self.gridy     = gridy
        self.overdraw  = overdraw
        self.tpd       = tpd
        self.ucount    = ucount
        self.draworder = draworder
        self.name = "OPENGLES2 marketing uniform pixels tpd=%d ucount=%d dorder=%s grd=%dx%dx%d" % ( self.tpd,
                                                                                                     self.ucount,
                                                                                                     self.draworder,
                                                                                                     self.gridx,
                                                                                                     self.gridy,
                                                                                                     self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        offsetUniform    = ''
        offsetUniformUse = '0.0'
        if self.draworder:
            offsetUniform    = 'uniform float gOffset;'
            offsetUniformUse = 'gOffset'

        uniforms   = ''
        fscode     = '( '
        for i in range( self.ucount ):
            uniforms += 'uniform vec4 gColor%d;' % i
            fscode   += 'gColor%d' % i

            if i != ( self.ucount - 1 ):
                fscode += ' + '
            else:
                if self.ucount > 1:
                    fscode += ' ) / %.1f' % self.ucount
                else:
                    fscode += ' )'

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        %s

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, %s, 1.0 );
        }
        """ % ( offsetUniform, offsetUniformUse, )

        rawFragmentShaderSource = """
        precision mediump float;
        %s

        void main()
        {
            gl_FragColor = %s;
        }
        """ % ( uniforms, fscode, )

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane3( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        colorLocationIndices = []
        for i in range( self.ucount ):
            colorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( colorLocationIndex, program.programIndex, 'gColor%d' % i )

            colorLocationIndices.append( colorLocationIndex )

        if self.draworder:
            offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

            OpenGLES2.Enable( 'GL_DEPTH_TEST' )
            OpenGLES2.DepthFunc( 'GL_LEQUAL' )

            OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        for i in range( self.ucount ):
            OpenGLES2.Uniformf( colorLocationIndices[ i ], 1, [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenGLES2.BindBuffer( 'GL_ELEMENT_ARRAY_BUFFER', meshBufferDataSetup.indexBufferIndex )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.draworder:
            OpenGLES2.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )

            depthdelta = 1.0 / float( self.overdraw )

            if self.draworder == FRONT_TO_BACK:
                depthoffset = 0.0
                depthdelta  = depthdelta
            elif self.draworder == BACK_TO_FRONT:
                depthoffset = 1.0
                depthdelta  = -depthdelta

        for i in range( self.overdraw ):
            trioffset = 0
            draws     = mesh.triangleCount / self.tpd

            if draws > 1:
                for i in range( self.ucount ):
                    OpenGLES2.Uniformf( colorLocationIndices[ i ], 1, [ 0.0, 1.0, 0.0, 1.0 ] )

            if self.draworder:
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ depthoffset ] )

            for j in range( draws ):
                OpenGLES2.BufferDrawElements( meshBufferDataSetup.indexBufferIndex,
                                              'OFF',
                                              meshBufferDataSetup.glMode,
                                              self.tpd * 3,
                                              trioffset )
                trioffset += ( 3 * self.tpd )

                if draws > 1:
                    if j % 2 == 0:
                        for i in range( self.ucount ):
                            OpenGLES2.Uniformf( colorLocationIndices[ i ], 1, [ 0.0, 0.0, 1.0, 1.0 ] )
                    else:
                        for i in range( self.ucount ):
                            OpenGLES2.Uniformf( colorLocationIndices[ i ], 1, [ 0.0, 1.0, 0.0, 1.0 ] )

            if mesh.triangleCount % self.tpd != 0:
                OpenGLES2.BufferDrawElements( meshBufferDataSetup.indexBufferIndex,
                                              'OFF',
                                              meshBufferDataSetup.glMode,
                                              ( mesh.triangleCount % self.tpd ) * 3,
                                              trioffset )

            if self.draworder:
                depthoffset += depthdelta

        target.swapBuffers( state )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class VaryingPixelsBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, tpd, vcount, draworder, vbo ):
        Benchmark.__init__( self )
        self.gridx     = gridx
        self.gridy     = gridy
        self.overdraw  = overdraw
        self.tpd       = tpd
        self.vcount    = vcount
        self.draworder = draworder
        self.vbo       = vbo
        self.name = "OPENGLES2 marketing varying pixels tpd=%d vcount=%d draworder=%s vbo=%s grd=%dx%dx%d" % ( self.tpd,
                                                                                                               self.vcount,
                                                                                                               self.draworder,
                                                                                                               { True : 'yes', False : 'no' }[ self.vbo ],
                                                                                                               self.gridx,
                                                                                                               self.gridy,
                                                                                                               self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        offsetUniform    = ''
        offsetUniformUse = '0.0'
        if self.draworder:
            offsetUniform    = 'uniform float gOffset;'
            offsetUniformUse = 'gOffset'

        attributes = ''
        varyings   = ''
        vscode     = ''
        fscode     = '( '
        for i in range( self.vcount ):
            attributes += 'attribute vec4 gColor%d;' % i
            varyings   += 'varying vec4 gVColor%d;' % i
            vscode     += 'gVColor%d = gColor%d;' % ( i, i, )
            fscode     += 'gVColor%d ' % i

            if i != ( self.vcount - 1 ):
                fscode += '+ '
            else:
                if self.vcount > 1:
                    fscode += ' ) / %.1f' % self.vcount
                else:
                    fscode += ' )'

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        %s
        %s
        %s

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, %s, 1.0 );
           %s
        }
        """ % ( offsetUniform, attributes, varyings, offsetUniformUse, vscode, )

        rawFragmentShaderSource = """
        precision mediump float;
        %s

        void main()
        {
            gl_FragColor = %s;
        }
        """ % ( varyings, fscode, )

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane3( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )

        if self.vbo:
            vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )

        vsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        fsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )

        OpenGLES2.SetData( vsDataIndex, vertexShaderSource )
        OpenGLES2.SetData( fsDataIndex, fragmentShaderSource )

        vsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vsIndex )
        OpenGLES2.ShaderSource( vsIndex, vsDataIndex )
        OpenGLES2.CompileShader( vsIndex )

        fsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fsIndex )
        OpenGLES2.ShaderSource( fsIndex, fsDataIndex )
        OpenGLES2.CompileShader( fsIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vsIndex )
        OpenGLES2.AttachShader( programIndex, fsIndex )

        locationValue = 0

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.SetRegister( vertexLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, 'gVertex' )
        locationValue += 1

        colors = []
        for i in range( self.gridx * self.gridy ):
            r = i / float( self.gridx * self.gridy )
            colors += [ 0.0, r, 1.0 - r, 1.0 ]

        vArrayIndices    = []
        vBufferIndices   = []
        vLocationIndices = []

        for i in range( self.vcount ):
            vArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( vArrayIndex, 'GL_FLOAT' )
            OpenGLES2.AppendToArray( vArrayIndex, colors )

            vArrayIndices.append( vArrayIndex )

            if self.vbo:
                vBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
                OpenGLES2.GenBuffer( vBufferIndex )
                OpenGLES2.BufferData( vBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      vArrayIndex,
                                      0,
                                      0 )

                vBufferIndices.append( vBufferIndex )

            vLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.SetRegister( vLocationIndex, locationValue )
            OpenGLES2.BindAttribLocation( programIndex, vLocationIndex, 'gVColor%d' % i )

            vLocationIndices.append( vLocationIndex )
            locationValue += 1

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.UseProgram( programIndex )

        OpenGLES2.DeleteShader( vsIndex )
        OpenGLES2.DeleteShader( fsIndex )

        if self.vbo:
            OpenGLES2.BufferVertexAttribPointer( vertexBufferDataSetup.vertexBufferIndex,
                                                 'ON',
                                                 vertexLocationIndex,
                                                 vertexBufferDataSetup.vertexSize,
                                                 'OFF',
                                                 0 )
        else:
            OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                           vertexDataSetup.vertexArrayIndex,
                                           vertexDataSetup.vertexSize,
                                           'OFF',
                                           0 )

        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )

        OpenGLES2.BindBuffer( 'GL_ARRAY_BUFFER', -1 )

        for i in range( self.vcount ):
            if self.vbo:
                OpenGLES2.BufferVertexAttribPointer( vBufferIndices[ i ],
                                                     'ON',
                                                     vLocationIndices[ i ],
                                                     4,
                                                     'OFF',
                                                     0 )
            else:
                OpenGLES2.VertexAttribPointer( vLocationIndices[ i ],
                                               vArrayIndices[ i ],
                                               4,
                                               'OFF',
                                               0 )

            OpenGLES2.EnableVertexAttribArray( vLocationIndices[ i ] )

        OpenGLES2.ValidateProgram( programIndex )

        if self.vbo:
            OpenGLES2.BindBuffer( 'GL_ELEMENT_ARRAY_BUFFER', vertexBufferDataSetup.indexBufferIndex )

        if self.draworder:
            offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( offsetLocationIndex, programIndex, 'gOffset' )

            OpenGLES2.Enable( 'GL_DEPTH_TEST' )
            OpenGLES2.DepthFunc( 'GL_LEQUAL' )

            OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.draworder:
            OpenGLES2.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )

            depthdelta = 1.0 / float( self.overdraw )

            if self.draworder == FRONT_TO_BACK:
                depthoffset = 0.0
                depthdelta  = depthdelta
            elif self.draworder == BACK_TO_FRONT:
                depthoffset = 1.0
                depthdelta  = -depthdelta

        for i in range( self.overdraw ):
            trioffset = 0
            draws     = mesh.triangleCount / self.tpd

            if self.draworder:
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ depthoffset ] )

            for j in range( draws ):
                if self.vbo:
                    OpenGLES2.BufferDrawElements( vertexBufferDataSetup.indexBufferIndex,
                                                  'OFF',
                                                  vertexBufferDataSetup.glMode,
                                                  self.tpd * 3,
                                                  trioffset )
                else:
                    OpenGLES2.DrawElements( vertexDataSetup.glMode,
                                            self.tpd * 3,
                                            trioffset,
                                            vertexDataSetup.indexArrayIndex,)

                trioffset += ( 3 * self.tpd )

            if mesh.triangleCount % self.tpd != 0:
                if self.vbo:
                    OpenGLES2.BufferDrawElements( vertexBufferDataSetup.indexBufferIndex,
                                                  'OFF',
                                                  vertexBufferDataSetup.glMode,
                                                  ( mesh.triangleCount % self.tpd ) * 3,
                                                  trioffset )
                else:
                    OpenGLES2.DrawElements( vertexDataSetup.glMode,
                                            ( mesh.triangleCount % self.tpd ) * 3,
                                            trioffset,
                                            vertexDataSetup.indexArrayIndex,)
            if self.draworder:
                depthoffset += depthdelta

        target.swapBuffers( state )

