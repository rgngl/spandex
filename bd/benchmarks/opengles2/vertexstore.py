#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
ARRAYS     = 'arrays'
MESH       = 'mesh'
VBO_ARRAYS = 'vbo arrays'
VBO_MESH   = 'vbo mesh'

######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    colorAttribute    = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
    texCoordAttribute = []
    indexAttribute      = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           colorAttribute,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class VertexStoreBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, vertexStore ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.vertexStore = vertexStore
        self.name = 'OPENGLES2 vertex store %s grd=%dx%dx%d tex=%s' % ( self.vertexStore,
                                                                        self.gridx,
                                                                        self.gridy,
                                                                        self.overdraw,
                                                                        textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gColor;
        attribute vec4 gTexCoord0;
        varying vec4 gVSColor;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;        

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
           gVSColor = gColor;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSColor;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = gVSColor * texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state           = setupGL2( modules, indexTracker, target )
        mesh            = createPlane( self.gridx, self.gridy )
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        program         = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )

        if self.vertexStore == ARRAYS:
            useVertexData( modules, indexTracker, vertexDataSetup, program )
        elif self.vertexStore == VBO_ARRAYS:
            vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )
            useVertexBufferData( modules, indexTracker, vertexBufferDataSetup, program )
        elif self.vertexStore == MESH:
            meshDataSetup = setupMeshData( modules, indexTracker, vertexDataSetup )
            useMeshData( modules, indexTracker, meshDataSetup, program )
        elif self.vertexStore == VBO_MESH:
            meshDataSetup = setupMeshData( modules, indexTracker, vertexDataSetup )
            meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
            useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup    = setupTexture( modules,
                                        indexTracker,
                                        screenWidth,
                                        screenHeight,
                                        [ 1.0, 0.3, 0.3, 1.0 ],
                                        [ 0.3, 1.0, 0.3, 1.0 ],
                                        self.textureType )
        
        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            
            if self.vertexStore == ARRAYS or self.vertexStore == MESH:
                drawVertexData( modules, vertexDataSetup )
                
            if self.vertexStore == VBO_ARRAYS:
                drawVertexBufferData( modules, vertexBufferDataSetup )

            if self.vertexStore == VBO_MESH:
                drawMeshBufferData( modules, meshBufferDataSetup )
            
            offset -= delta

        target.swapBuffers( state )
