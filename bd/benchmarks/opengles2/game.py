#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class GameBenchmark( Benchmark ):  
    def __init__( self, singleTexPlanes, dualTexPlanes, samples ):
        Benchmark.__init__( self )
        self.singleTexPlanes = singleTexPlanes
        self.dualTexPlanes   = dualTexPlanes
        self.samples         = samples
        self.name = "OPENGLES2 gamelike stx=%d dtx=%d samples=%d" % ( self.singleTexPlanes,
                                                                      self.dualTexPlanes,
                                                                      self.samples, )

    def build( self, target, modules ):
        OpenGLES2    = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # Shader source code
        singleTexPlaneVertexShaderSource = '"attribute vec4 position_in;attribute vec4 uv_in;uniform mat4 mvp;varying vec4 uv;void main(){gl_Position = mvp * position_in;uv = uv_in;}"'
        singleTexPlaneFragmentShaderSource = '"precision mediump float;uniform sampler2D tex0;varying vec4 uv;void main(){gl_FragColor = texture2D(tex0, uv.xy);}"'
        dualTexPlaneVertexShaderSource = '"attribute vec4 position_in;attribute vec4 normal_in;attribute vec4 uv_in;uniform mat4 mvp;uniform vec4 modelView[3];varying vec4 uv0;varying vec4 uv1;vec4 mul43(vec4 matrix[3], vec4 invec){vec4 outvec;outvec.x = dot(matrix[0], invec);outvec.y = dot(matrix[1], invec);outvec.z = dot(matrix[2], invec);outvec.w = invec.w;return outvec;}void main(){gl_Position = mvp * position_in;vec4 worldPos = mul43(modelView, position_in);vec4 worldNormal = mul43(modelView, vec4(normal_in.x,normal_in.y,normal_in.z,0));vec4 refl = reflect(worldPos,worldNormal);uv0 = uv_in;uv1 = 0.5 * (normalize(refl) + 1.0);}"'
        dualTexPlaneFragmentShaderSource = '"precision mediump float;uniform sampler2D tex0;uniform sampler2D tex1;varying vec4 uv0;varying vec4 uv1;void main(){gl_FragColor = texture2D(tex0, uv0.xy) * texture2D(tex1, uv1.xy);}"'

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        if self.samples == 0 or self.samples == 1:
            attribs[ CONFIG_SAMPLES ]        = '>=0'
            attribs[ CONFIG_SAMPLE_BUFFERS ] = 0
        else:
            attribs[ CONFIG_SAMPLES ]        = self.samples
            attribs[ CONFIG_SAMPLE_BUFFERS ] = 1
        
        state = setupGL2( modules, indexTracker, target, attribs )        

        if self.samples > 1:
            OpenGLES2.CheckValue( 'GL_SAMPLES', 'SCT_EQUAL', [ self.samples ] )
            OpenGLES2.CheckValue( 'GL_SAMPLE_BUFFERS', 'SCT_GREATER', [ 0 ] )
        
        ######################################################################        
        # PLANE CREATION
        ######################################################################
        PLANE_WIDTH                     = 22
        PLANE_HEIGHT                    = 26
        VERTEX_COMPONENTS               = 3
        TEX_COORD_COMPONENTS            = 2
        SINGLE_TEX_PLANE_TEXTURE_WIDTH  = 128
        SINGLE_TEX_PLANE_TEXTURE_HEIGHT = 128
        DUAL_TEX_PLANE_TEXTURE_WIDTH    = 128
        DUAL_TEX_PLANE_TEXTURE_HEIGHT   = 128
        
        ######################################################################
        # Create raw vertex data for single tex plane
        ######################################################################
        texCoordAttribute = []
        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                         TEX_COORD_COMPONENTS,
                                                         [ float( PLANE_WIDTH ),
                                                           float( PLANE_HEIGHT ) ] ) )
        
        singleTexPlane = MeshStripPlane( PLANE_WIDTH,
                                         PLANE_HEIGHT,
                                         MeshVertexAttribute( MESH_TYPE_FLOAT, VERTEX_COMPONENTS ),
                                         None,
                                         None,
                                         texCoordAttribute,
                                         MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                         None,
                                         MESH_CCW_WINDING,
                                         False )

        singleTexPlane.translate( [ -0.5, -0.5, 0 ] )
        singleTexPlane.scale( [ 2.0, 2.0 ] )

        ######################################################################
        # Create OpenGL ES 2.0 vertex arrays for single tex plane
        ######################################################################        
        singleTexPlaneVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( singleTexPlaneVertexArrayIndex,
                               singleTexPlane.vertexGLType )
        OpenGLES2.AppendToArray( singleTexPlaneVertexArrayIndex, singleTexPlane.vertices )

        singleTexPlaneTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( singleTexPlaneTexCoordArrayIndex,
                               singleTexPlane.texCoordsGLTypes[ 0 ] )
        OpenGLES2.AppendToArray( singleTexPlaneTexCoordArrayIndex, singleTexPlane.texCoords[ 0 ] )
            
        singleTexPlaneIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( singleTexPlaneIndexArrayIndex, singleTexPlane.indexGLType );
        OpenGLES2.AppendToArray( singleTexPlaneIndexArrayIndex, singleTexPlane.indices )

        ######################################################################
        # Create OpenVG ES 2.0 vertex buffers from vertex arrays for single tex plane
        ######################################################################        
        singleTexPlaneVertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( singleTexPlaneVertexBufferIndex )
        OpenGLES2.BufferData( singleTexPlaneVertexBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              singleTexPlaneVertexArrayIndex,
                              0,
                              0 )

        singleTexPlaneTexCoordBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( singleTexPlaneTexCoordBufferIndex )
        OpenGLES2.BufferData( singleTexPlaneTexCoordBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              singleTexPlaneTexCoordArrayIndex,
                              0,
                              0 )

        singleTexPlaneIndexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( singleTexPlaneIndexBufferIndex )
        OpenGLES2.BufferData( singleTexPlaneIndexBufferIndex,
                              'ON',
                              'GL_ELEMENT_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              singleTexPlaneIndexArrayIndex,
                              0,
                              0 )

        ######################################################################
        # Create raw vertex data for dual tex plane
        ######################################################################
        texCoordAttribute = []
        for i in range( 2 ):
            texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                             TEX_COORD_COMPONENTS,
                                                             [ float( PLANE_WIDTH ),
                                                               float( PLANE_HEIGHT ) ] ) )
        
        dualTexPlane = MeshStripPlane( PLANE_WIDTH,
                                       PLANE_HEIGHT,
                                       MeshVertexAttribute( MESH_TYPE_FLOAT, VERTEX_COMPONENTS ),
                                       None,
                                       MeshVertexAttribute( MESH_TYPE_FLOAT, 3 ),
                                       texCoordAttribute,
                                       MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                       None,
                                       MESH_CCW_WINDING,
                                       False )

        dualTexPlane.translate( [ -0.5, -0.5, 0 ] )
        dualTexPlane.scale( [ 2.0, 2.0 ] )

        ######################################################################
        # Create OpenGL ES 2.0 vertex arrays for dual tex plane
        ######################################################################
        dualTexPlaneVertexArrayIndex  = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( dualTexPlaneVertexArrayIndex,
                               dualTexPlane.vertexGLType )
        OpenGLES2.AppendToArray( dualTexPlaneVertexArrayIndex, dualTexPlane.vertices )

        dualTexPlaneNormalArrayIndex  = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( dualTexPlaneNormalArrayIndex,
                               dualTexPlane.normalGLType )
        OpenGLES2.AppendToArray( dualTexPlaneNormalArrayIndex, dualTexPlane.normals )
        
        dualTexPlaneTexCoord0ArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( dualTexPlaneTexCoord0ArrayIndex,
                               dualTexPlane.texCoordsGLTypes[ 0 ] )
        OpenGLES2.AppendToArray( dualTexPlaneTexCoord0ArrayIndex, dualTexPlane.texCoords[ 0 ] )

        dualTexPlaneTexCoord1ArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( dualTexPlaneTexCoord1ArrayIndex,
                               dualTexPlane.texCoordsGLTypes[ 1 ] )
        OpenGLES2.AppendToArray( dualTexPlaneTexCoord1ArrayIndex, dualTexPlane.texCoords[ 1 ] )
        
        dualTexPlaneIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( dualTexPlaneIndexArrayIndex, dualTexPlane.indexGLType );
        OpenGLES2.AppendToArray( dualTexPlaneIndexArrayIndex, dualTexPlane.indices )

        ######################################################################
        # Create OpenGL ES 2.0 vertex buffers from vertex arrays for dual tex plane
        ######################################################################
        dualTexPlaneVertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( dualTexPlaneVertexBufferIndex )
        OpenGLES2.BufferData( dualTexPlaneVertexBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              dualTexPlaneVertexArrayIndex,
                              0,
                              0 )

        dualTexPlaneNormalBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( dualTexPlaneNormalBufferIndex )
        OpenGLES2.BufferData( dualTexPlaneNormalBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              dualTexPlaneNormalArrayIndex,
                              0,
                              0 )
        
        dualTexPlaneTexCoord0BufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( dualTexPlaneTexCoord0BufferIndex )
        OpenGLES2.BufferData( dualTexPlaneTexCoord0BufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              dualTexPlaneTexCoord0ArrayIndex,
                              0,
                              0 )

        dualTexPlaneTexCoord1BufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( dualTexPlaneTexCoord1BufferIndex )
        OpenGLES2.BufferData( dualTexPlaneTexCoord1BufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              dualTexPlaneTexCoord1ArrayIndex,
                              0,
                              0 )
       
        dualTexPlaneIndexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( dualTexPlaneIndexBufferIndex )
        OpenGLES2.BufferData( dualTexPlaneIndexBufferIndex,
                              'ON',
                              'GL_ELEMENT_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              dualTexPlaneIndexArrayIndex,
                              0,
                              0 )

        ######################################################################
        # Create single tex plane shaders
        ######################################################################
        singleTexPlaneVertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( singleTexPlaneVertexShaderDataIndex, singleTexPlaneVertexShaderSource )
        
        singleTexPlaneVertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', singleTexPlaneVertexShaderIndex )
        OpenGLES2.ShaderSource( singleTexPlaneVertexShaderIndex, singleTexPlaneVertexShaderDataIndex )
        OpenGLES2.CompileShader( singleTexPlaneVertexShaderIndex )

        singleTexPlaneFragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( singleTexPlaneFragmentShaderDataIndex, singleTexPlaneFragmentShaderSource )
        
        singleTexPlaneFragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', singleTexPlaneFragmentShaderIndex )
        OpenGLES2.ShaderSource( singleTexPlaneFragmentShaderIndex, singleTexPlaneFragmentShaderDataIndex )
        OpenGLES2.CompileShader( singleTexPlaneFragmentShaderIndex )

        singleTexPlaneProgramIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( singleTexPlaneProgramIndex )

        OpenGLES2.AttachShader( singleTexPlaneProgramIndex, singleTexPlaneVertexShaderIndex )
        OpenGLES2.AttachShader( singleTexPlaneProgramIndex, singleTexPlaneFragmentShaderIndex )

        singleTexPlaneVertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        singleTexPlaneVertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( singleTexPlaneVertexLocationIndex, singleTexPlaneVertexLocationValue )
        OpenGLES2.BindAttribLocation( singleTexPlaneProgramIndex,
                                      singleTexPlaneVertexLocationIndex,
                                      "position_in" )

        singleTexPlaneTexCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        singleTexPlaneTexCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( singleTexPlaneTexCoordLocationIndex, singleTexPlaneTexCoordLocationValue )
        OpenGLES2.BindAttribLocation( singleTexPlaneProgramIndex,
                                      singleTexPlaneTexCoordLocationIndex,
                                      "uv_in" )

        OpenGLES2.LinkProgram( singleTexPlaneProgramIndex )
        OpenGLES2.DeleteShader( singleTexPlaneVertexShaderIndex )
        OpenGLES2.DeleteShader( singleTexPlaneFragmentShaderIndex )

        singleTexPlaneMVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( singleTexPlaneMVPLocationIndex, singleTexPlaneProgramIndex, 'mvp' )
            
        singleTexPlaneTextureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( singleTexPlaneTextureLocationIndex, singleTexPlaneProgramIndex, 'tex0' )

        ######################################################################
        # Create dual tex plane shaders
        ######################################################################
        dualTexPlaneVertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( dualTexPlaneVertexShaderDataIndex, dualTexPlaneVertexShaderSource )
        
        dualTexPlaneVertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', dualTexPlaneVertexShaderIndex )
        OpenGLES2.ShaderSource( dualTexPlaneVertexShaderIndex, dualTexPlaneVertexShaderDataIndex )
        OpenGLES2.CompileShader( dualTexPlaneVertexShaderIndex )

        dualTexPlaneFragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( dualTexPlaneFragmentShaderDataIndex, dualTexPlaneFragmentShaderSource )
        
        dualTexPlaneFragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', dualTexPlaneFragmentShaderIndex )
        OpenGLES2.ShaderSource( dualTexPlaneFragmentShaderIndex, dualTexPlaneFragmentShaderDataIndex )
        OpenGLES2.CompileShader( dualTexPlaneFragmentShaderIndex )

        dualTexPlaneProgramIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( dualTexPlaneProgramIndex )

        OpenGLES2.AttachShader( dualTexPlaneProgramIndex, dualTexPlaneVertexShaderIndex )
        OpenGLES2.AttachShader( dualTexPlaneProgramIndex, dualTexPlaneFragmentShaderIndex )

        dualTexPlaneVertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        dualTexPlaneVertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( dualTexPlaneVertexLocationIndex, dualTexPlaneVertexLocationValue )
        OpenGLES2.BindAttribLocation( dualTexPlaneProgramIndex,
                                      dualTexPlaneVertexLocationIndex,
                                      "position_in" )

        dualTexPlaneNormalLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        dualTexPlaneNormalLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( dualTexPlaneNormalLocationIndex, dualTexPlaneNormalLocationValue )
        OpenGLES2.BindAttribLocation( dualTexPlaneProgramIndex,
                                      dualTexPlaneNormalLocationIndex,
                                      "normal_in" )
        
        dualTexPlaneTexCoord0LocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        dualTexPlaneTexCoord0LocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( dualTexPlaneTexCoord0LocationIndex, dualTexPlaneTexCoord0LocationValue )
        OpenGLES2.BindAttribLocation( dualTexPlaneProgramIndex,
                                      dualTexPlaneTexCoord0LocationIndex,
                                      "uv0" )

        dualTexPlaneTexCoord1LocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        dualTexPlaneTexCoord1LocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( dualTexPlaneTexCoord1LocationIndex, dualTexPlaneTexCoord1LocationValue )
        OpenGLES2.BindAttribLocation( dualTexPlaneProgramIndex,
                                      dualTexPlaneTexCoord1LocationIndex,
                                      "uv1" )
       
        OpenGLES2.LinkProgram( dualTexPlaneProgramIndex )
        OpenGLES2.DeleteShader( dualTexPlaneVertexShaderIndex )
        OpenGLES2.DeleteShader( dualTexPlaneFragmentShaderIndex )

        dualTexPlaneModelViewProjectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( dualTexPlaneModelViewProjectionLocationIndex,
                                      dualTexPlaneProgramIndex,
                                      'mvp' )

        dualTexPlaneModelViewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( dualTexPlaneModelViewLocationIndex,
                                      dualTexPlaneProgramIndex,
                                      'modelView' )
        
        dualTexPlaneTexture0LocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( dualTexPlaneTexture0LocationIndex, dualTexPlaneProgramIndex, 'tex0' )
        
        dualTexPlaneTexture1LocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( dualTexPlaneTexture1LocationIndex, dualTexPlaneProgramIndex, 'tex1' )

        ######################################################################
        # Create single tex plane texture
        ######################################################################
        singleTexPlaneTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( singleTexPlaneTextureDataIndex,
                                            [ 1.0, 0.3, 0.3, 1.0 ],
                                            [ 0.3, 1.0, 0.3, 1.0 ],
                                            4, 4,
                                            SINGLE_TEX_PLANE_TEXTURE_WIDTH,
                                            SINGLE_TEX_PLANE_TEXTURE_HEIGHT,
                                            'ON',
                                            'OPENGLES2_RGBA8888' )
        
        singleTexPlaneTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( singleTexPlaneTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', singleTexPlaneTextureIndex )
        OpenGLES2.TexImage2D( singleTexPlaneTextureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST_MIPMAP_LINEAR',
                                'GL_LINEAR',
                                'GL_REPEAT',
                                'GL_REPEAT' )

        ######################################################################
        # Create dual tex plane texture
        ######################################################################
        dualTexPlaneTexture0DataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( dualTexPlaneTexture0DataIndex,
                                            [ 1.0, 0.3, 0.3, 1.0 ],
                                            [ 0.3, 0.3, 1.0, 1.0 ],
                                            4, 4,
                                            DUAL_TEX_PLANE_TEXTURE_WIDTH,
                                            DUAL_TEX_PLANE_TEXTURE_HEIGHT,
                                            'ON',
                                            'OPENGLES2_RGBA8888' )
        
        dualTexPlaneTexture0Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( dualTexPlaneTexture0Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', dualTexPlaneTexture0Index )
        OpenGLES2.TexImage2D( dualTexPlaneTexture0DataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST_MIPMAP_LINEAR',
                                'GL_LINEAR',
                                'GL_REPEAT',
                                'GL_REPEAT' )

        dualTexPlaneTexture1DataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( dualTexPlaneTexture1DataIndex,
                                            [ 0.3, 1.0, 0.3, 1.0 ],
                                            [ 0.3, 0.3, 1.0, 1.0 ],
                                            8, 8,
                                            DUAL_TEX_PLANE_TEXTURE_WIDTH,
                                            DUAL_TEX_PLANE_TEXTURE_HEIGHT,
                                            'ON',
                                            'OPENGLES2_RGBA8888' )
        
        dualTexPlaneTexture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( dualTexPlaneTexture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', dualTexPlaneTexture1Index )
        OpenGLES2.TexImage2D( dualTexPlaneTexture1DataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST_MIPMAP_LINEAR',
                                'GL_LINEAR',
                                'GL_REPEAT',
                                'GL_REPEAT' )

        ######################################################################
        # OpenGL ES 2.0 state setup
        ######################################################################
        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        if self.samples > 1:
            OpenGLES2.CheckValue( 'GL_SAMPLES', 'SCT_EQUAL', [ self.samples ] )
            OpenGLES2.CheckValue( 'GL_SAMPLE_BUFFERS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        for i in range( self.singleTexPlanes ):
            ######################################################################
            # Draw a single tex plane
            ######################################################################
            OpenGLES2.UseProgram( singleTexPlaneProgramIndex )
            
            OpenGLES2.BufferVertexAttribPointer( singleTexPlaneVertexBufferIndex,
                                                 'ON',
                                                 singleTexPlaneVertexLocationIndex,
                                                 VERTEX_COMPONENTS,
                                                 'OFF',
                                                 0 )
            OpenGLES2.EnableVertexAttribArray( singleTexPlaneVertexLocationIndex )
        
            OpenGLES2.ActiveTexture( 0 )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', singleTexPlaneTextureIndex )
            OpenGLES2.Uniformi( singleTexPlaneTextureLocationIndex, 1, [ 0 ] )
       
            OpenGLES2.BufferVertexAttribPointer( singleTexPlaneTexCoordBufferIndex,
                                                 'ON',
                                                 singleTexPlaneTexCoordLocationIndex,
                                                 TEX_COORD_COMPONENTS,
                                                 'OFF',
                                                 0 )
            OpenGLES2.EnableVertexAttribArray( singleTexPlaneTexCoordLocationIndex )

            OpenGLES2.UniformMatrix( singleTexPlaneMVPLocationIndex,
                                     1,
                                     [ 1.0, 0.0, 0.0, 0.0,
                                       0.0, 1.0, 0.0, 0.0,
                                       0.0, 0.0, 1.0, 0.0,
                                       0.0, 0.0, 0.0, 1.0 ] )

            OpenGLES2.BufferDrawElements( singleTexPlaneIndexBufferIndex,
                                          'ON',
                                          singleTexPlane.glMode,
                                          len( singleTexPlane.indices ),
                                          0 )    

            OpenGLES2.DisableVertexAttribArray( singleTexPlaneVertexLocationIndex )
            OpenGLES2.DisableVertexAttribArray( singleTexPlaneTexCoordLocationIndex )

        for i in range( self.dualTexPlanes ):
            ######################################################################
            # Draw a dual tex plane
            ######################################################################
            OpenGLES2.UseProgram( dualTexPlaneProgramIndex )
        
            OpenGLES2.BufferVertexAttribPointer( dualTexPlaneVertexBufferIndex,
                                                 'ON',
                                                 dualTexPlaneVertexLocationIndex,
                                                 dualTexPlane.vertexComponents,
                                                 boolToSpandex( dualTexPlane.vertexNormalized ),
                                                 0 )
            OpenGLES2.EnableVertexAttribArray( dualTexPlaneVertexLocationIndex )

            OpenGLES2.BufferVertexAttribPointer( dualTexPlaneNormalBufferIndex,
                                                 'ON',
                                                 dualTexPlaneNormalLocationIndex,
                                                 dualTexPlane.normalComponents,
                                                 boolToSpandex( dualTexPlane.normalNormalized ),
                                                 0 )
            OpenGLES2.EnableVertexAttribArray( dualTexPlaneNormalLocationIndex )

            OpenGLES2.ActiveTexture( 0 )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', dualTexPlaneTexture0Index )
            OpenGLES2.ActiveTexture( 1 )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', dualTexPlaneTexture1Index )

            OpenGLES2.Uniformi( dualTexPlaneTexture0LocationIndex, 1, [ 0 ] )
            OpenGLES2.Uniformi( dualTexPlaneTexture1LocationIndex, 1, [ 1 ] )
            
            OpenGLES2.BufferVertexAttribPointer( dualTexPlaneTexCoord0BufferIndex,
                                                 'ON',
                                                 dualTexPlaneTexCoord0LocationIndex,
                                                 dualTexPlane.texCoordsComponents[ 0 ],
                                                 boolToSpandex( dualTexPlane.texCoordsNormalized[ 0 ] ),
                                                 0 )
            OpenGLES2.EnableVertexAttribArray( dualTexPlaneTexCoord0LocationIndex )

            OpenGLES2.BufferVertexAttribPointer( dualTexPlaneTexCoord1BufferIndex,
                                                 'ON',
                                                 dualTexPlaneTexCoord1LocationIndex,
                                                 dualTexPlane.texCoordsComponents[ 1 ],
                                                 boolToSpandex( dualTexPlane.texCoordsNormalized[ 1 ] ),
                                                 0 )
            OpenGLES2.EnableVertexAttribArray( dualTexPlaneTexCoord1LocationIndex )

            OpenGLES2.UniformMatrix( dualTexPlaneModelViewProjectionLocationIndex,
                                     1,
                                     [ 1.0, 0.0, 0.0, 0.0,
                                       0.0, 1.0, 0.0, 0.0,
                                       0.0, 0.0, 1.0, 0.0,
                                       0.0, 0.0, 0.0, 1.0 ] )

            OpenGLES2.Uniformf( dualTexPlaneModelViewLocationIndex,
                                3,
                                [ 1.0, 0.0, 0.0, 0.0,
                                  0.0, 1.0, 0.0, 0.0,
                                  0.0, 0.0, 1.0, 0.0 ] )
        
            OpenGLES2.BufferDrawElements( dualTexPlaneIndexBufferIndex,
                                          'ON',
                                          dualTexPlane.glMode,
                                          len( dualTexPlane.indices ),
                                          0 )    

            OpenGLES2.DisableVertexAttribArray( dualTexPlaneVertexLocationIndex )
            OpenGLES2.DisableVertexAttribArray( dualTexPlaneNormalLocationIndex )            
            OpenGLES2.DisableVertexAttribArray( dualTexPlaneTexCoord0LocationIndex )
            OpenGLES2.DisableVertexAttribArray( dualTexPlaneTexCoord1LocationIndex )

        target.swapBuffers( state )

