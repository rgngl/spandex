#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
INTERNAL_FORMATS = { 'OPENGLES2_RGB565'   : 'GL_RGB',
                     'OPENGLES2_RGB888'   : 'GL_RGB',
                     'OPENGLES2_RGBA4444' : 'GL_RGBA',
                     'OPENGLES2_RGBA5551' : 'GL_RGBA',
                     'OPENGLES2_RGBA8888' : 'GL_RGBA' }

######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute     = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute   = []
    indexAttribute      = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CopyTexImageBenchmark( Benchmark ):  
    def __init__( self, textureWidth, textureHeight, textureType ):
        Benchmark.__init__( self)
        self.textureWidth   = textureWidth
        self.textureHeight  = textureHeight
        self.textureType    = textureType      
        self.name = 'OPENGLES2 copyteximage tex=%dx%d %s' % ( self.textureWidth,
                                                              self.textureHeight,
                                                              textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()
            
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 0.5, 0.7, 0.1, 1.0 ],
                                            [ 0.7, 0.5, 0.1, 1.0 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )        
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        target.swapBuffers( state )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.CopyTexImage2D( 'GL_TEXTURE_2D',
                                  0,
                                  INTERNAL_FORMATS[ self.textureType ],
                                  0, 0,
                                  self.textureWidth, self.textureHeight )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES2.Finish()

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class CopyAndUseBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureWidth, textureHeight, textureType, copyIndex, useIndex, frameCount ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight 
        self.textureType   = textureType
        self.copyIndex     = copyIndex
        self.useIndex      = useIndex
        self.frameCount    = frameCount

        if ( ( self.useIndex > 0 ) and ( self.copyIndex > self.useIndex ) ) or ( self.useIndex > self.frameCount ):
            raise 'Index error'
       
        self.name = 'OPENGLES2 copyteximage.n.use %d/%d/%d tex=%dx%d %s grd=%dx%dx%d' % ( self.copyIndex,
                                                                                          self.useIndex,
                                                                                          self.frameCount,
                                                                                          self.textureWidth,
                                                                                          self.textureHeight,
                                                                                          textureTypeToStr( self.textureType ),
                                                                                          self.gridx,
                                                                                          self.gridy,
                                                                                          self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                self.textureWidth,
                                                self.textureHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 0.5, 0.7, 0.1, 1.0 ],
                                            [ 0.7, 0.5, 0.1, 1.0 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )        
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )        
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
        
        for j in range( self.frameCount ):
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
            
            if ( j + 1 ) == self.copyIndex:
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.CopyTexImage2D( 'GL_TEXTURE_2D',
                                          0,
                                          INTERNAL_FORMATS[ self.textureType ],
                                          0, 0,
                                          self.textureWidth, self.textureHeight )
                
                if ( j + 1 ) != self.useIndex:
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
                    
            if ( self.copyIndex != self.useIndex ) and ( ( j + 1 ) == self.useIndex ):
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )

            for i in range( self.overdraw ):
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawMeshBufferData( modules, meshBufferDataSetup )
                offset -= delta

            target.swapBuffers( state )
