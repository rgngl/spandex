#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from lib.egl       import *
from common        import *

######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class GenTextureBenchmark( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = 'OPENGLES2 gentexture'
    
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupGL2( modules, indexTracker, target )            
       
        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        target.swapBuffers( state )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.DeleteTexture( textureIndex )
        
        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()
       
        # Ensure all benchmark loop actions have finished before timing
        OpenGLES2.Finish()


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SwizzleBenchmark( Benchmark ):
    def __init__( self, textureWidth, textureHeight, textureType ):
        Benchmark.__init__( self )
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType        
        self.name = 'OPENGLES2 texture upload and swizzle tex=%dx%d %s' % ( self.textureWidth,
                                                                            self.textureHeight,
                                                                            textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupGL2( modules, indexTracker, target )            

        extensions = { 'OPENGLES2_BGRA8888' : 'GL_EXT_texture_format_BGRA8888',
                       'OPENGLES2_DEPTH'    : 'GL_OES_depth_texture',
                       'OPENGLES2_DEPTH32'  : 'GL_OES_depth_texture'
                       }

        if extensions.has_key( self.textureType ):
            OpenGLES2.CheckExtension( extensions[ self.textureType ] )
        
        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 0.5, 0.7, 0.1, 1.0 ],
                                            [ 0.7, 0.5, 0.1, 1.0 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        target.swapBuffers( state )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES2.Finish()


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class EglImageUploadBenchmark( Benchmark ):
    def __init__( self, textureWidth, textureHeight, textureType ):
        Benchmark.__init__( self )
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType        
        self.name = 'OPENGLES2 texture upload through egl image tex=%dx%d %s' % ( self.textureWidth,
                                                                                  self.textureHeight,
                                                                                  textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]
        Images    = modules[ 'Images' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupGL2( modules, indexTracker, target )            

        displayIndex = state[ 'displayIndex' ]
        configIndex  = state[ 'configIndex' ]
        surfaceIndex = state[ 'surfaceIndex' ]
        contextIndex = state[ 'contextIndex' ]

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )

        IMAGES_FORMATS = { 'OPENGLES2_RGB565'      : 'IMAGES_RGB565',
                           'OPENGLES2_RGB888'      : 'IMAGES_RGB8',
                           'OPENGLES2_RGBA8888'    : 'IMAGES_RGBA8',
                          }

        Images.CreateData( imagesDataIndex,
                           'IMAGES_FLOWER',
                           IMAGES_FORMATS[ self.textureType ],
                           self.textureWidth, self.textureHeight )

        eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
        Images.CopyToEgl( imagesDataIndex, eglDataIndex )

        PIXMAP_FORMATS = { 'OPENGLES2_RGB565'      : 'SCT_COLOR_FORMAT_RGB565',
                           'OPENGLES2_RGB888'      : 'SCT_COLOR_FORMAT_RGB888',
                           'OPENGLES2_RGBA8888'    : 'SCT_COLOR_FORMAT_RGBA8888',
                          }

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.textureWidth, self.textureHeight,
                                PIXMAP_FORMATS[ self.textureType ],
                                [ 'SCT_USAGE_OPENGLES2_TEXTURE_2D' ],
                                eglDataIndex )

        # Create egl image from shared pixmap.
        imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            -1,
                            'EGL_NATIVE_BUFFER',
                            eglPixmapIndex,
                            imageAttributes )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        target.swapBuffers( state )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.SetPixmapData( eglPixmapIndex, eglDataIndex )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )
        OpenGLES2.DeleteTexture( textureIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES2.Finish()


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SubtextureSwizzleBenchmark( Benchmark ):
    def __init__( self, textureWidth, textureHeight, subtexXoffset, subtexYoffset, subtexWidth, subtexHeight, textureType ):
        Benchmark.__init__( self )
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.subtexXoffset = subtexXoffset
        self.subtexYoffset = subtexYoffset
        self.subtexWidth   = subtexWidth
        self.subtexHeight  = subtexHeight
        self.textureType   = textureType        
        self.name = 'OPENGLES2 subtexture upload and swizzle tex=%dx%d subtex=%d %d %dx%d %s' % ( self.textureWidth,
                                                                                                  self.textureHeight,
                                                                                                  self.subtexXoffset,
                                                                                                  self.subtexYoffset,
                                                                                                  self.subtexWidth,
                                                                                                  self.subtexHeight,
                                                                                                  textureTypeToStr( self.textureType ), )
    
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupGL2( modules, indexTracker, target )            

        extensions = { 'OPENGLES2_BGRA8888' : 'GL_EXT_texture_format_BGRA8888',
                       'OPENGLES2_DEPTH'    : 'GL_OES_depth_texture',
                       'OPENGLES2_DEPTH32'  : 'GL_OES_depth_texture'
                       }

        if extensions.has_key( self.textureType ):
            OpenGLES2.CheckExtension( extensions[ self.textureType ] )
        
        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 0.5, 0.7, 0.1, 1.0 ],
                                            [ 0.7, 0.5, 0.1, 1.0 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        subtexDataIndex         = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( subtexDataIndex,
                                            [ 0.5, 0.7, 0.1, 1.0 ],
                                            [ 0.7, 0.5, 0.1, 1.0 ],
                                            4, 4,
                                            self.subtexWidth, self.subtexHeight,
                                            'OFF',
                                            self.textureType )
        
        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        target.swapBuffers( state )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.TexSubImage2D( subtexDataIndex,
                                 'GL_TEXTURE_2D',
                                 self.subtexXoffset, self.subtexYoffset )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES2.Finish()

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class UploadCompressedBenchmark( Benchmark ):
    def __init__( self, textureSize, compressedTextureType ):
        Benchmark.__init__( self )
        self.textureSize           = textureSize
        self.compressedTextureType = compressedTextureType      
        self.name = 'OPENGLES2 compressed texture upload tex=%dx%d %s' % ( self.textureSize,
                                                                           self.textureSize,
                                                                           self.compressedTextureType[ 7 : ], )
    
    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]

        indexTracker = IndexTracker()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupGL2( modules, indexTracker, target )            

        if self.compressedTextureType.startswith( 'IMAGES_ETC' ):
            OpenGLES2.CheckExtension( 'GL_OES_compressed_ETC1_RGB8_texture' )
        elif self.compressedTextureType.startswith( 'IMAGES_PVRTC' ):
            OpenGLES2.CheckExtension( 'GL_IMG_texture_compression_pvrtc' )            
        else:
            raise 'Unexpected compressed texture type'
        
        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex,
                           'IMAGES_FLOWER',
                           self.compressedTextureType,
                           self.textureSize, self.textureSize )       
            
        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imagesDataIndex,
                                dataIndex )

        etcTypeMap = { 'IMAGES_ETC1_RGB8'   : 'GL_ETC1_RGB8_OES',
                       'IMAGES_PVRTC_RGB4'  : 'GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG',
                       'IMAGES_PVRTC_RGB2'  : 'GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG',
                       'IMAGES_PVRTC_RGBA4' : 'GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG',
                       'IMAGES_PVRTC_RGBA2' : 'GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG'
                       }
        
        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCompressedTextureData( compressedTextureDataIndex,
                                               dataIndex,
                                               self.textureSize, self.textureSize,
                                               etcTypeMap[ self.compressedTextureType ]  )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        target.swapBuffers( state )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES2.Finish()
        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class UploadAndUseBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureWidth, textureHeight, textureType, uploadIndex, useIndex, frameCount ):
        Benchmark.__init__( self )
        self.gridx         = gridx
        self.gridy         = gridy
        self.overdraw      = overdraw
        self.textureWidth  = textureWidth
        self.textureHeight = textureHeight
        self.textureType   = textureType
        self.uploadIndex   = uploadIndex
        self.useIndex      = useIndex
        self.frameCount    = frameCount

        if ( ( self.useIndex > 0 ) and ( self.uploadIndex > self.useIndex ) ) or ( self.useIndex > self.frameCount ):
            raise 'Index error'
        
        self.name = 'OPENGLES2 texture.upld.n.use %d/%d/%d tex=%dx%d %s grd=%dx%dx%d' % ( self.uploadIndex,
                                                                                          self.useIndex,
                                                                                          self.frameCount,
                                                                                          self.textureWidth,
                                                                                          self.textureHeight,
                                                                                          textureTypeToStr( self.textureType ),
                                                                                          self.gridx,
                                                                                          self.gridy,
                                                                                          self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth,
                                                screenHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        # Create another texture
        textureDataIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 0.5, 0.7, 0.1, 1.0 ],
                                            [ 0.7, 0.5, 0.1, 1.0 ],
                                            4, 4,
                                            self.textureWidth, self.textureHeight,
                                            'OFF',
                                            self.textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
        
        for j in range( self.frameCount ):
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
            
            if ( j + 1 ) == self.uploadIndex:
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                
                if ( j + 1 ) != self.useIndex:
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
                    
            if ( self.uploadIndex != self.useIndex ) and ( ( j + 1 ) == self.useIndex ):
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )

            for i in range( self.overdraw ):
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawMeshBufferData( modules, meshBufferDataSetup )
                offset -= delta

            target.swapBuffers( state )

