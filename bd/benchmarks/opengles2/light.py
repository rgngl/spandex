#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
def createPlane( gridx, gridy, normalAttributes, texCoordAttributes ):
    vertexAttribute = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    normalAttribute = None

    if normalAttributes:
        normalAttribute = MeshAttribute( MESH_TYPE_FIXED )

    texCoordAttribute = []
    indexAttribute = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

    if texCoordAttributes:
        for i in range( len( texCoordAttributes ) ):
            if texCoordAttributes[ i ]:
                texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
            else:
                tca.append( None )

    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           normalAttribute,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    if normalAttributes:
        meshSpherePerturbNormals( mesh, [ 0.5, 0.5 ], 0.4 )

    return mesh

######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ReferenceBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES2 light reference grd=%dx%dx%d"  % ( self.gridx,
                                                                  self.gridy,
                                                                  self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4  gVertex;
        uniform   vec4  gColor;
        uniform   float gOffset;
        varying   vec4  gVSColor;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSColor = gColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        varying vec4 gVSColor;

        void main()
        {
           gl_FragColor = gVSColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, False, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        gOffset                 = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gOffset, program.programIndex, 'gOffset' )

        gColor                  = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gColor, program.programIndex, 'gColor' )

        OpenGLES2.Uniformf( gColor, 1, [ 0.3, 1.0, 0.3, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( gOffset, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SimpleDiffuseBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES2 light simple diffuse grd=%dx%dx%d"  % ( self.gridx,
                                                                       self.gridy,
                                                                       self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec3 gNormal;
        uniform float gOffset;
        uniform vec3 gLightDirection;
        uniform vec4 gDiffuseLight;
        uniform vec4 gAmbientLight;
        uniform vec4 gDiffuseMaterial;
        uniform vec4 gAmbientMaterial;
        varying vec4 gVSColor;

        void main()
        {
           float NdotL;
           vec4 diffuseColor;
           vec4 ambientColor;

           ambientColor = gAmbientLight * gAmbientMaterial;

           NdotL = max( abs( dot( gNormal, gLightDirection ) ), 0.0 );
           diffuseColor = gDiffuseLight * gDiffuseMaterial * NdotL;

           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSColor = ambientColor + diffuseColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        varying vec4 gVSColor;

        void main()
        {
           gl_FragColor = gVSColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        gOffset                 = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gOffset, program.programIndex, 'gOffset' )

        material                = Material( [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            None,
                                            None )
        light                   = Light( [ 1.0, 0.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, -1.0 ],            # World-space
                                         [ 0.0, 1.0, 1.0, 1.0 ],
                                         None )

        lightSetup              = setupLight( modules, indexTracker, light, material, program )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        useLight( modules, lightSetup )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( gOffset, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ComplexDiffuseBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES2 light complex diffuse grd=%dx%dx%d"  % ( self.gridx,
                                                                        self.gridy,
                                                                        self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gNormal;
        uniform mat4 gModelView;
        uniform mat4 gITModelView;
        uniform vec3 gLightDirection;
        uniform vec4 gDiffuseLight;
        uniform vec4 gAmbientLight;
        uniform vec4 gDiffuseMaterial;
        uniform vec4 gAmbientMaterial;
        varying vec4 gVSColor;

        void main()
        {
           float NdotL;
           vec4 diffuseColor;
           vec4 ambientColor;
           vec3 normal;
           ambientColor = gAmbientLight * gAmbientMaterial;
           normal = ( gITModelView * vec4( gNormal.x, gNormal.y, gNormal.z, 1.0 ) ).xyz;
           NdotL = max( abs( dot( normal, gLightDirection ) ), 0.0 );
           diffuseColor = gDiffuseLight * gDiffuseMaterial * NdotL;
           gl_Position = gModelView * gVertex;
           gVSColor = ambientColor + diffuseColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        varying vec4 gVSColor;

        void main()
        {
           gl_FragColor = gVSColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        gModelView              = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gModelView, program.programIndex, 'gModelView' )

        gITModelView            = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gITModelView, program.programIndex, 'gITModelView' )

        material                = Material( [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            None,
                                            None )
        light                   = Light( [ 1.0, 0.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, -1.0 ],            # World-space
                                         [ 0.0, 1.0, 1.0, 1.0 ],
                                         None )

        lightSetup              = setupLight( modules, indexTracker, light, material, program )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        useLight( modules, lightSetup )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.UniformMatrix( gModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                      0.0,   1.0,   0.0,    0.0,
                                                      0.0,   0.0,   1.0,    0.0,
                                                      0.0,   0.0,   offset, 1.0 ] )
            OpenGLES2.UniformMatrix( gITModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                        0.0,   1.0,   0.0,    0.0,
                                                        0.0,   0.0,   1.0,    -offset,
                                                        0.0,   0.0,   0.0,    1.0 ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SimpleSpecularBlinnBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES2 light simple specular blinn grd=%dx%dx%d"  % ( self.gridx,
                                                                              self.gridy,
                                                                              self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec3 gNormal;
        uniform float gOffset;
        uniform vec3 gLightDirection;
        uniform vec4 gAmbientLight;
        uniform vec4 gDiffuseLight;
        uniform vec4 gSpecularLight;
        uniform vec4 gAmbientMaterial;
        uniform vec4 gDiffuseMaterial;
        uniform vec4 gSpecularMaterial;
        uniform float gShininess;
        varying vec4 gVSColor;

        void main()
        {
           float NdotL;
           vec4 ambientColor;
           vec4 diffuseColor;
           vec4 specularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
           ambientColor = gAmbientLight * gAmbientMaterial;
           NdotL = max( abs( dot( gNormal, gLightDirection ) ), 0.0 );
           diffuseColor = gDiffuseLight * gDiffuseMaterial * NdotL;
           if( NdotL > 0.0 )
           {
              vec3 eye = vec3( 0.0, 0.0, -1.0 );
              vec3 HV = normalize( gLightDirection + eye );
              float NdotHV = max( abs( dot( gNormal, HV ) ), 0.0 );
              specularColor = gSpecularLight * gSpecularMaterial * pow( NdotHV, gShininess );
           }
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSColor = ambientColor + diffuseColor + specularColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        varying vec4 gVSColor;

        void main()
        {
           gl_FragColor = gVSColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        gOffset                 = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gOffset, program.programIndex, 'gOffset' )

        material                = Material( [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            2.0 )
        light                   = Light( [ 1.0, 0.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, -1.0 ],            # World-space
                                         [ 0.0, 1.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, 1.0, 1.0 ] )

        lightSetup              = setupLight( modules, indexTracker, light, material, program )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        useLight( modules, lightSetup )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( gOffset, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ComplexSpecularBlinnBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES2 light complex specular blinn grd=%dx%dx%d"  % ( self.gridx,
                                                                               self.gridy,
                                                                               self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec3 gNormal;
        uniform mat4 gModelView;
        uniform mat4 gITModelView;
        uniform vec3 gLightDirection;
        uniform vec4 gAmbientLight;
        uniform vec4 gDiffuseLight;
        uniform vec4 gSpecularLight;
        uniform vec4 gAmbientMaterial;
        uniform vec4 gDiffuseMaterial;
        uniform vec4 gSpecularMaterial;
        uniform float gShininess;
        varying vec4 gVSColor;

        void main()
        {
           float NdotL;
           vec4 ambientColor;
           vec4 diffuseColor;
           vec4 specularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
           vec3 normal;
           ambientColor = gAmbientLight * gAmbientMaterial;
           normal = vec3( gITModelView * vec4( gNormal.x, gNormal.y, gNormal.z, 1.0 ) );
           NdotL = max( abs( dot( normal, gLightDirection ) ), 0.0 );
           diffuseColor = gDiffuseLight * gDiffuseMaterial * NdotL;
           if( NdotL > 0.0 )
           {
              vec3 eye = vec3( 0.0, 0.0, -1.0 );
              vec3 HV = normalize( gLightDirection + eye );
              float NdotHV = max( abs( dot( normal, HV ) ), 0.0 );
              specularColor = gSpecularLight * gSpecularMaterial * pow( NdotHV, gShininess );
           }
           gl_Position = gModelView * gVertex;
           gVSColor = ambientColor + diffuseColor + specularColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        varying vec4 gVSColor;

        void main()
        {
           gl_FragColor = gVSColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        gModelView              = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gModelView, program.programIndex, 'gModelView' )

        gITModelView            = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gITModelView, program.programIndex, 'gITModelView' )

        material                = Material( [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            2.0 )
        light                   = Light( [ 1.0, 0.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, -1.0 ],            # World-space
                                         [ 0.0, 1.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, 1.0, 1.0 ] )

        lightSetup              = setupLight( modules, indexTracker, light, material, program )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        useLight( modules, lightSetup )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.UniformMatrix( gModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                      0.0,   1.0,   0.0,    0.0,
                                                      0.0,   0.0,   1.0,    0.0,
                                                      0.0,   0.0,   offset, 1.0 ] )
            OpenGLES2.UniformMatrix( gITModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                        0.0,   1.0,   0.0,    0.0,
                                                        0.0,   0.0,   1.0,    -offset,
                                                        0.0,   0.0,   0.0,    1.0 ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ComplexSpecularBlinnWithTextureBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = "OPENGLES2 light cmplx.spclr.blinn.wth.txtr grd=%dx%dx%d tex=%s"  % ( self.gridx,
                                                                                          self.gridy,
                                                                                          self.overdraw,
                                                                                          textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec3 gNormal;
        attribute vec4 gTexCoord0;
        uniform mat4 gModelView;
        uniform mat4 gITModelView;
        uniform vec3 gLightDirection;
        uniform vec4 gAmbientLight;
        uniform vec4 gDiffuseLight;
        uniform vec4 gSpecularLight;
        uniform vec4 gAmbientMaterial;
        uniform vec4 gDiffuseMaterial;
        uniform vec4 gSpecularMaterial;
        uniform float gShininess;
        varying vec4 gVSDiffuseColor;
        varying vec4 gVSSpecularColor;
        varying vec4 gVSTexCoord0;

        void main()
        {
           float NdotL;
           vec4 ambientColor;
           vec3 normal;
           ambientColor = gAmbientLight * gAmbientMaterial;
           normal = vec3( gITModelView * vec4( gNormal.x, gNormal.y, gNormal.z, 1.0 ) );
           NdotL = max( abs( dot( normal, gLightDirection ) ), 0.0 );
           gVSDiffuseColor = ambientColor + gDiffuseLight * gDiffuseMaterial * NdotL;
           gVSSpecularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
           if( NdotL > 0.0 )
           {
              vec3 eye = vec3( 0.0, 0.0, -1.0 );
              vec3 HV = normalize( gLightDirection + eye );
              float NdotHV = max( abs( dot( normal, HV ) ), 0.0 );
              gVSSpecularColor = gSpecularLight * gSpecularMaterial * pow( NdotHV, gShininess );
           }
           gl_Position = gModelView * gVertex;
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSDiffuseColor;
        varying vec4 gVSSpecularColor;
        varying vec4 gVSTexCoord0;

        void main()
        {
           vec4 texColor = texture2D( gTexture0, gVSTexCoord0.xy );
           gl_FragColor = ( texColor * gVSDiffuseColor ) + gVSSpecularColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        gModelView              = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gModelView, program.programIndex, 'gModelView' )

        gITModelView            = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gITModelView, program.programIndex, 'gITModelView' )

        material                = Material( [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            2.0 )
        light                   = Light( [ 1.0, 0.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, -1.0 ],            # World-space
                                         [ 0.0, 1.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, 1.0, 1.0 ] )

        lightSetup              = setupLight( modules, indexTracker, light, material, program )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        useLight( modules, lightSetup )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.UniformMatrix( gModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                      0.0,   1.0,   0.0,    0.0,
                                                      0.0,   0.0,   1.0,    0.0,
                                                      0.0,   0.0,   offset, 1.0 ] )
            OpenGLES2.UniformMatrix( gITModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                        0.0,   1.0,   0.0,    0.0,
                                                        0.0,   0.0,   1.0,    -offset,
                                                        0.0,   0.0,   0.0,    1.0 ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SimpleSpecularPhongBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES2 light simple specular phong grd=%dx%dx%d"  % ( self.gridx,
                                                                              self.gridy,
                                                                              self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec3 gNormal;
        uniform float gOffset;
        uniform vec3 gLightDirection;
        uniform vec4 gAmbientLight;
        uniform vec4 gDiffuseLight;
        uniform vec4 gSpecularLight;
        uniform vec4 gAmbientMaterial;
        uniform vec4 gDiffuseMaterial;
        uniform vec4 gSpecularMaterial;
        uniform float gShininess;
        varying vec4 gVSColor;

        void main()
        {
           float NdotL;
           vec4 ambientColor;
           vec4 diffuseColor;
           vec4 specularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
           ambientColor = gAmbientLight * gAmbientMaterial;
           NdotL = max( abs( dot( gNormal, gLightDirection ) ), 0.0 );
           diffuseColor = gDiffuseLight * gDiffuseMaterial * NdotL;
           if( NdotL > 0.0 )
           {
              vec3 eye = vec3( 0.0, 0.0, -1.0 );
              vec3 R = reflect( gLightDirection, gNormal );
              float RdotEye = max( abs( dot( R, eye ) ), 0.0 );
              specularColor = gSpecularLight * gSpecularMaterial * pow( RdotEye, gShininess );
           }
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSColor = ambientColor + diffuseColor + specularColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        varying vec4 gVSColor;

        void main()
        {
           gl_FragColor = gVSColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        gOffset                 = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gOffset, program.programIndex, 'gOffset' )

        material                = Material( [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            2.0 )
        light                   = Light( [ 1.0, 0.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, -1.0 ],            # World-space
                                         [ 0.0, 1.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, 1.0, 1.0 ] )

        lightSetup              = setupLight( modules, indexTracker, light, material, program )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        useLight( modules, lightSetup )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( gOffset, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ComplexSpecularPhongBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES2 light complex specular phong grd=%dx%dx%d"  % ( self.gridx,
                                                                               self.gridy,
                                                                               self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec3 gNormal;
        uniform mat4 gModelView;
        uniform mat4 gITModelView;
        uniform vec3 gLightDirection;
        uniform vec4 gAmbientLight;
        uniform vec4 gDiffuseLight;
        uniform vec4 gSpecularLight;
        uniform vec4 gAmbientMaterial;
        uniform vec4 gDiffuseMaterial;
        uniform vec4 gSpecularMaterial;
        uniform float gShininess;
        varying vec4 gVSColor;

        void main()
        {
           float NdotL;
           vec4 ambientColor;
           vec4 diffuseColor;
           vec4 specularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
           vec3 normal;
           ambientColor = gAmbientLight * gAmbientMaterial;
           normal = vec3( gITModelView * vec4( gNormal.x, gNormal.y, gNormal.z, 1.0 ) );
           NdotL = max( abs( dot( normal, gLightDirection ) ), 0.0 );
           diffuseColor = gDiffuseLight * gDiffuseMaterial * NdotL;
           if( NdotL > 0.0 )
           {
              vec3 eye = vec3( 0.0, 0.0, -1.0 );
              vec3 R = reflect( gLightDirection, normal );
              float RdotEye = max( abs( dot( R, eye ) ), 0.0 );
              specularColor = gSpecularLight * gSpecularMaterial * pow( RdotEye, gShininess );
           }
           gl_Position = gModelView * gVertex;
           gVSColor = ambientColor + diffuseColor + specularColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        varying vec4 gVSColor;

        void main()
        {
           gl_FragColor = gVSColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        gModelView              = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gModelView, program.programIndex, 'gModelView' )

        gITModelView            = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gITModelView, program.programIndex, 'gITModelView' )

        material                = Material( [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            2.0 )
        light                   = Light( [ 1.0, 0.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, -1.0 ],            # World-space
                                         [ 0.0, 1.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, 1.0, 1.0 ] )

        lightSetup              = setupLight( modules, indexTracker, light, material, program )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        useLight( modules, lightSetup )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.UniformMatrix( gModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                      0.0,   1.0,   0.0,    0.0,
                                                      0.0,   0.0,   1.0,    0.0,
                                                      0.0,   0.0,   offset, 1.0 ] )
            OpenGLES2.UniformMatrix( gITModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                        0.0,   1.0,   0.0,    0.0,
                                                        0.0,   0.0,   1.0,    -offset,
                                                        0.0,   0.0,   0.0,    1.0 ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ComplexSpecularPhongWithTextureBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = "OPENGLES2 light cmplx.spclr.phong.wth.txtr grd=%dx%dx%d tex=%s"  % ( self.gridx,
                                                                                          self.gridy,
                                                                                          self.overdraw,
                                                                                          textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec3 gNormal;
        attribute vec4 gTexCoord0;
        uniform mat4 gModelView;
        uniform mat4 gITModelView;
        uniform vec3 gLightDirection;
        uniform vec4 gAmbientLight;
        uniform vec4 gDiffuseLight;
        uniform vec4 gSpecularLight;
        uniform vec4 gAmbientMaterial;
        uniform vec4 gDiffuseMaterial;
        uniform vec4 gSpecularMaterial;
        uniform float gShininess;
        varying vec4 gVSDiffuseColor;
        varying vec4 gVSSpecularColor;
        varying vec4 gVSTexCoord0;

        void main()
        {
           float NdotL;
           vec4 ambientColor;
           vec3 normal;
           ambientColor = gAmbientLight * gAmbientMaterial;
           normal = vec3( gITModelView * vec4( gNormal.x, gNormal.y, gNormal.z, 1.0 ) );
           NdotL = max( abs( dot( normal, gLightDirection ) ), 0.0 );
           gVSDiffuseColor = ambientColor + gDiffuseLight * gDiffuseMaterial * NdotL;
           gVSSpecularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
           if( NdotL > 0.0 )
           {
              vec3 eye = vec3( 0.0, 0.0, -1.0 );
              vec3 R = reflect( gLightDirection, normal );
              float RdotEye = max( abs( dot( R, eye ) ), 0.0 );
              gVSSpecularColor = gSpecularLight * gSpecularMaterial * pow( RdotEye, gShininess );
           }
           gl_Position = gModelView * gVertex;
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSDiffuseColor;
        varying vec4 gVSSpecularColor;
        varying vec4 gVSTexCoord0;

        void main()
        {
           vec4 texColor = texture2D( gTexture0, gVSTexCoord0.xy );
           gl_FragColor = ( texColor * gVSDiffuseColor ) + gVSSpecularColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        gModelView              = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gModelView, program.programIndex, 'gModelView' )

        gITModelView            = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gITModelView, program.programIndex, 'gITModelView' )

        material                = Material( [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            2.0 )
        light                   = Light( [ 1.0, 0.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, -1.0 ],            # World-space
                                         [ 0.0, 1.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, 1.0, 1.0 ] )

        lightSetup              = setupLight( modules, indexTracker, light, material, program )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        useLight( modules, lightSetup )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.UniformMatrix( gModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                      0.0,   1.0,   0.0,    0.0,
                                                      0.0,   0.0,   1.0,    0.0,
                                                      0.0,   0.0,   offset, 1.0 ] )
            OpenGLES2.UniformMatrix( gITModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                        0.0,   1.0,   0.0,    0.0,
                                                        0.0,   0.0,   1.0,    -offset,
                                                        0.0,   0.0,   0.0,    1.0 ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PerPixelPhongBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw ):
        Benchmark.__init__( self )
        self.gridx    = gridx
        self.gridy    = gridy
        self.overdraw = overdraw
        self.name = "OPENGLES2 light per pixel phong grd=%dx%dx%d"  % ( self.gridx,
                                                                        self.gridy,
                                                                        self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gNormal;
        uniform mat4 gModelView;
        uniform mat4 gITModelView;
        uniform vec3 gLightDirection;
        uniform vec4 gDiffuseLight;
        uniform vec4 gAmbientLight;
        uniform vec4 gDiffuseMaterial;
        uniform vec4 gAmbientMaterial;
        varying vec4 gVSAmbientColor;
        varying vec4 gVSDiffuseColor;
        varying vec3 gVSNormal;
        varying vec3 gVSReflect;

        void main()
        {
          gVSNormal = vec3( gITModelView * gNormal );
          gVSAmbientColor = gAmbientLight * gAmbientMaterial;
          gVSDiffuseColor = gDiffuseLight * gDiffuseMaterial;
          gVSReflect = reflect( gLightDirection, gVSNormal );
          gl_Position = gModelView * gVertex;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform vec3 gLightDirection;
        uniform vec4 gSpecularLight;
        uniform vec4 gSpecularMaterial;
        uniform float gShininess;
        varying vec4 gVSAmbientColor;
        varying vec4 gVSDiffuseColor;
        varying vec3 gVSNormal;
        varying vec3 gVSLightDirection;
        varying vec3 gVSReflect;

        void main()
        {
           float NdotL;
           vec4 diffuseColor;
           vec4 specularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
           NdotL = max( abs( dot( gVSNormal, gLightDirection ) ), 0.0 );
           diffuseColor = gVSDiffuseColor * NdotL;
           if( NdotL > 0.0 )
           {
              vec3 eye = vec3( 0.0, 0.0, -1.0 );
              float RdotEye = max( abs( dot( gVSReflect, eye ) ), 0.0 );
              specularColor = gSpecularLight * gSpecularMaterial * pow( RdotEye, gShininess );
           }
           gl_FragColor = gVSAmbientColor + diffuseColor + specularColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        gModelView              = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gModelView, program.programIndex, 'gModelView' )

        gITModelView            = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gITModelView, program.programIndex, 'gITModelView' )

        material                = Material( [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            2.0 )
        light                   = Light( [ 1.0, 0.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, -1.0 ],            # World-space
                                         [ 0.0, 1.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, 1.0, 1.0 ] )

        lightSetup              = setupLight( modules, indexTracker, light, material, program )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        useLight( modules, lightSetup )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.UniformMatrix( gModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                      0.0,   1.0,   0.0,    0.0,
                                                      0.0,   0.0,   1.0,    0.0,
                                                      0.0,   0.0,   offset, 1.0 ] )
            OpenGLES2.UniformMatrix( gITModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                        0.0,   1.0,   0.0,    0.0,
                                                        0.0,   0.0,   1.0,    -offset,
                                                        0.0,   0.0,   0.0,    1.0 ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PerPixelPhongWithTextureBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = "OPENGLES2 light per.pxl.phong.wth.txtr grd=%dx%dx%d tex=%s"  % ( self.gridx,
                                                                                      self.gridy,
                                                                                      self.overdraw,
                                                                                      textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gNormal;
        attribute vec4 gTexCoord0;
        uniform mat4 gModelView;
        uniform mat4 gITModelView;
        uniform vec3 gLightDirection;
        uniform vec4 gDiffuseLight;
        uniform vec4 gAmbientLight;
        uniform vec4 gDiffuseMaterial;
        uniform vec4 gAmbientMaterial;
        varying vec4 gVSAmbientColor;
        varying vec4 gVSDiffuseColor;
        varying vec3 gVSNormal;
        varying vec3 gVSReflect;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gVSNormal = vec3( gITModelView * vec4( gNormal.x, gNormal.y, gNormal.z, 1.0 ) );
           gVSAmbientColor = gAmbientLight * gAmbientMaterial;
           gVSDiffuseColor = gDiffuseLight * gDiffuseMaterial;
           gVSReflect = reflect( gLightDirection, gVSNormal );
           gl_Position = gModelView * gVertex;
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        uniform vec3 gLightDirection;
        uniform vec4 gSpecularLight;
        uniform vec4 gSpecularMaterial;
        uniform float gShininess;
        varying vec4 gVSTexCoord0;
        varying vec4 gVSAmbientColor;
        varying vec4 gVSDiffuseColor;
        varying vec3 gVSNormal;
        varying vec3 gVSLightDirection;
        varying vec3 gVSReflect;

        void main()
        {
           float NdotL;
           vec4 diffuseColor;
           vec4 specularColor = vec4( 0.0, 0.0, 0.0, 0.0 );
           vec4 texColor;
           NdotL = max( abs( dot( gVSNormal, gLightDirection ) ), 0.0 );
           diffuseColor = gVSDiffuseColor * NdotL;
           if( NdotL > 0.0 )
           {
              vec3 eye = vec3( 0.0, 0.0, -1.0 );
              float RdotEye = max( abs( dot( gVSReflect, eye ) ), 0.0 );
              specularColor = gSpecularLight * gSpecularMaterial * pow( RdotEye, gShininess );
           }
           texColor = texture2D( gTexture0, gVSTexCoord0.xy );
           gl_FragColor = texColor * ( gVSAmbientColor + diffuseColor ) + specularColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        gModelView              = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gModelView, program.programIndex, 'gModelView' )

        gITModelView            = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gITModelView, program.programIndex, 'gITModelView' )

        material                = Material( [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            [ 1.0, 1.0, 1.0, 1.0 ],
                                            2.0 )
        light                   = Light( [ 1.0, 0.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, -1.0 ],            # World-space
                                         [ 0.0, 1.0, 0.0, 1.0 ],
                                         [ 0.0, 0.0, 1.0, 1.0 ] )

        lightSetup              = setupLight( modules, indexTracker, light, material, program )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        useLight( modules, lightSetup )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.UniformMatrix( gModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                      0.0,   1.0,   0.0,    0.0,
                                                      0.0,   0.0,   1.0,    0.0,
                                                      0.0,   0.0,   offset, 1.0 ] )
            OpenGLES2.UniformMatrix( gITModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                        0.0,   1.0,   0.0,    0.0,
                                                        0.0,   0.0,   1.0,    -offset,
                                                        0.0,   0.0,   0.0,    1.0 ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SimpleTnLBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = "OPENGLES2 light simple tnl grd=%dx%dx%d tex=%s"  % ( self.gridx,
                                                                          self.gridy,
                                                                          self.overdraw,
                                                                          textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec3 gNormal;
        attribute vec4 gTexCoord0;
        uniform mat4 gModelView;
        uniform vec3 gLightDirection;
        uniform float gMaterialBias;
        uniform float gMaterialScale;

        varying vec3 gVSDiffuseColor;
        varying vec3 gVSSpecularColor;
        varying vec3 gVSNormal;
        varying vec3 gVSReflect;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = gModelView * gVertex;
           gVSDiffuseColor = vec3( max( dot( gNormal, gLightDirection ), 0.0 ) );
           gVSSpecularColor = vec3( max( ( gVSDiffuseColor.x - gMaterialBias ) * gMaterialScale, 0.0));
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;
        varying vec3 gVSDiffuseColor;
        varying vec3 gVSSpecularColor;

        void main()
        {
           vec3 texColor = texture2D( gTexture0, gVSTexCoord0.xy ).rgb;
           gl_FragColor = vec4( ( texColor * gVSDiffuseColor ) + gVSSpecularColor, 1.0 );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy, True, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        gModelView              = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gModelView, program.programIndex, 'gModelView' )

        lightDirection = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( lightDirection, program.programIndex, 'gLightDirection' )

        materialBias = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( materialBias, program.programIndex, 'gMaterialBias' )

        materialScale = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( materialScale, program.programIndex, 'gMaterialScale' )

        OpenGLES2.Uniformf( lightDirection, 1, [ 0.0, 0.0, 1.0 ] )
        OpenGLES2.Uniformf( materialBias, 1, [ 0.1 ] )
        OpenGLES2.Uniformf( materialScale, 1, [ 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.UniformMatrix( gModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                      0.0,   1.0,   0.0,    0.0,
                                                      0.0,   0.0,   1.0,    0.0,
                                                      0.0,   0.0,   offset, 1.0 ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )

