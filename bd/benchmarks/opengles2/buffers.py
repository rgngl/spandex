#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
READ1PIXEL  = 'read1pixel'
READPIXELS  = 'readpixels'
COPYBUFFERS = 'copybuffers'
FINISH      = 'finish'

######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RenderToWindowBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, widthR, heightR ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.widthR      = widthR
        self.heightR     = heightR
        self.name = 'OPENGLES2 render to window %s grd=%dx%dx%d tex=%s' % ( farrayToStr( [ self.widthR, self.heightR ] ),
                                                                            self.gridx,
                                                                            self.gridy,
                                                                            self.overdraw,
                                                                            textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):          
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()              
        width  = int( self.widthR * screenWidth )
        height = int( self.heightR * screenHeight )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENGLES2 )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )       
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT'])

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
       
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, 0 )

        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RenderToBufferBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, widthR, heightR, format, flush ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.widthR      = widthR
        self.heightR     = heightR
        self.format      = format
        self.flush       = flush
        self.name = 'OPENGLES2 render to fbo %s %s flsh=%s grd=%dx%dx%d tex=%s' % ( farrayToStr( [ self.widthR, self.heightR ] ),
                                                                                    fboFormatToStr( self.format ),
                                                                                    self.flush,
                                                                                    self.gridx,
                                                                                    self.gridy,
                                                                                    self.overdraw,
                                                                                    textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):          
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()              
        width  = int( self.widthR * screenWidth )
        height = int( self.heightR * screenHeight )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        target.swapBuffers( state )
        OpenGLES2.Finish()
        
        colorRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
        OpenGLES2.GenRenderbuffer( colorRenderbufferIndex )
        OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', colorRenderbufferIndex )
        OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER', self.format, width, height )

        depthRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
        OpenGLES2.GenRenderbuffer( depthRenderbufferIndex )
        OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', depthRenderbufferIndex )
        OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER', 'GL_DEPTH_COMPONENT16', width, height )

        framebufferIndex        = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                           'GL_COLOR_ATTACHMENT0',
                                           'GL_RENDERBUFFER',
                                           colorRenderbufferIndex )

        OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                           'GL_DEPTH_ATTACHMENT',
                                           'GL_RENDERBUFFER',
                                           depthRenderbufferIndex )

        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        if self.flush == READ1PIXEL:
            dataIndex           = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            OpenGLES2.CreateData( dataIndex, 1 * 1 * 4 ) # RGBA8888
        elif self.flush == READPIXELS:       
            dataIndex           = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            OpenGLES2.CreateData( dataIndex, width * height * 4 ) # RGBA8888

        OpenGLES2.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        if self.flush == READ1PIXEL:
            OpenGLES2.ReadPixels( dataIndex,
                                  width / 2, height / 2,
                                  1,
                                  1 )
        elif self.flush == READPIXELS:
            OpenGLES2.ReadPixels( dataIndex,
                                  0, 0,
                                  width,
                                  height )
        elif self.flush == FINISH:
            OpenGLES2.Finish()
        else:
            raise 'Invalid flush'

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RenderToPbufBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, widthR, heightR, colorFormat, flush ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.widthR      = widthR
        self.heightR     = heightR
        self.colorFormat = colorFormat
        self.flush       = flush
        self.name = 'OPENGLES2 render to pbuf %s %s flsh=%s grd=%dx%dx%d tex=%s' % ( farrayToStr( [ self.widthR, self.heightR ] ),
                                                                                     pixmapFormatToStr( self.colorFormat ),
                                                                                     self.flush,
                                                                                     self.gridx,
                                                                                     self.gridy,
                                                                                     self.overdraw,
                                                                                     textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        Egl = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()              
        width  = int( self.widthR * screenWidth )
        height = int( self.heightR * screenHeight )
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        state   = setupGL2( modules, indexTracker, target, attribs )

        displayIndex = state[ 'displayIndex' ]
        
        a = mapColorFormatToAttributes( self.colorFormat )
                   
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       a[ CONFIG_BUFFERSIZE ],
                       a[ CONFIG_REDSIZE ],
                       a[ CONFIG_GREENSIZE ],
                       a[ CONFIG_BLUESIZE ],
                       a[ CONFIG_ALPHASIZE ],
                       '>=16',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  surfaceIndex,
                                  configIndex,
                                  width,
                                  height,
                                  'EGL_NO_TEXTURE',
                                  'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           2 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, 0 )
       
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        if self.flush == READ1PIXEL:
            dataIndex           = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            OpenGLES2.CreateData( dataIndex, 1 * 1 * 4 ) # RGBA8888
        elif self.flush == READPIXELS:
            dataIndex           = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            OpenGLES2.CreateData( dataIndex, width * height * 4 ) # RGBA8888
        elif self.flush == COPYBUFFERS:
            pixmapIndex         = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
            Egl.CreatePixmap( pixmapIndex,
                              width,
                              height,
                              self.colorFormat,
                              -1 )
            
        OpenGLES2.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        if self.flush == READ1PIXEL:
            OpenGLES2.ReadPixels( dataIndex,
                                  width / 2, height / 2,
                                  1,
                                  1 )
        elif self.flush == READPIXELS:
            OpenGLES2.ReadPixels( dataIndex,
                                  0, 0,
                                  width,
                                  height )
        elif self.flush == COPYBUFFERS:
            Egl.CopyBuffers( displayIndex,
                             surfaceIndex,
                             pixmapIndex )
        elif self.flush == FINISH:
            OpenGLES2.Finish()
        else:
            raise 'Invalid flush'

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RenderToTextureBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, widthR, heightR, format, useIndex ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.widthR      = widthR
        self.heightR     = heightR
        self.format      = format
        self.useIndex    = useIndex
        
        if self.useIndex >= 0:
            ui = str( self.useIndex ) + '/' + str( self.overdraw )
        else:
            ui = 'nouse'
            
        self.name = 'OPENGLES2 render to tex %s %s %s grd=%dx%dx%d tex=%s' % ( farrayToStr( [ self.widthR, self.heightR ] ),
                                                                               textureTypeToStr( self.format ),
                                                                               ui,
                                                                               self.gridx,
                                                                               self.gridy,
                                                                               self.overdraw,
                                                                               textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()              
        width  = int( self.widthR * screenWidth )
        height = int( self.heightR * screenHeight )
       
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                width, height,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        if self.useIndex >= 0:
            textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( textureDataIndex,
                                             width,
                                             height,
                                             'OFF',
                                             self.textureType )
            
            textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( textureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
            OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_LINEAR',
                                    'GL_LINEAR',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE' )
            
            depthRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
            OpenGLES2.GenRenderbuffer( depthRenderbufferIndex )
            OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', depthRenderbufferIndex )
            OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER', 'GL_DEPTH_COMPONENT16', width, height )

            defaultFramebufferIndex  = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
            
            framebufferIndex        = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                            'GL_COLOR_ATTACHMENT0',
                                            'GL_TEXTURE_2D',
                                            textureIndex,
                                            0 )

            OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                               'GL_DEPTH_ATTACHMENT',
                                               'GL_RENDERBUFFER',
                                               depthRenderbufferIndex )

            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        if self.useIndex >= 0:
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
        else:
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        for i in range( self.overdraw ):

            if i == self.useIndex:
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
            
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


######################################################################
def setupTexture2( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_NEAREST',
                            'GL_NEAREST',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RenderTextureThroughFboPipelineBenchmark( Benchmark ):
    def __init__( self, format, pipeLen, loops, separateVbos, separatePrograms ):
        Benchmark.__init__( self )
        self.textureType      = 'OPENGLES2_RGBA8888'
        self.format           = format
        self.pipeLen          = pipeLen
        self.loops            = loops
        self.separateVbos     = separateVbos
        self.separatePrograms = separatePrograms
        self.name = 'OPENGLES2 render texture through fbo pipeline pplen=%d loops=%d fmt=%s sprtvbos=%s sprtprgrms=%s' % ( self.pipeLen,
                                                                                                                           self.loops,
                                                                                                                           textureTypeToStr( self.format ),
                                                                                                                           { False : 'no', True : 'yes' }[ self.separateVbos ], 
                                                                                                                           { False : 'no', True : 'yes' }[ self.separatePrograms ],)

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()              
        width  = screenWidth
        height = screenHeight
       
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state           = setupGL2( modules, indexTracker, target )            
        mesh            = createPlane( 2, 2 )
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )

        meshDataSetup        = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup  = setupMeshBufferData( modules, indexTracker, meshDataSetup )

        if self.separateVbos:
            screenMeshDataSetup        = setupMeshData( modules, indexTracker, vertexDataSetup )
            screenMeshBufferDataSetup  = setupMeshBufferData( modules, indexTracker, screenMeshDataSetup )

        if not self.separatePrograms:
            program = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        else:
            fboPrograms = []
            for i in range( self.pipeLen ):
                program  = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
                fboPrograms.append( program )

            screenProgram = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )

        textureSetup = setupTexture2( modules, 
                                      indexTracker,
                                      width, height,
                                      [ 1.0, 0.3, 0.3, 1.0 ],
                                      [ 0.3, 1.0, 0.3, 1.0 ],
                                      self.textureType )

        defaultFramebufferIndex = -1
        if self.pipeLen > 0:
            defaultFramebufferIndex  = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

        fboTextures  = []
        framebuffers = []

        fboTextures.append( textureSetup.textureIndex )

        for i in range( self.pipeLen ):
            textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( textureDataIndex,
                                             width,
                                             height,
                                             'OFF',
                                             self.textureType )
            
            textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( textureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
            OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                    'GL_NEAREST',
                                    'GL_NEAREST',
                                    'GL_CLAMP_TO_EDGE',
                                    'GL_CLAMP_TO_EDGE' )

            fboTextures.append( textureIndex )

            framebufferIndex        = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                            'GL_COLOR_ATTACHMENT0',
                                            'GL_TEXTURE_2D',
                                            textureIndex,
                                            0 )

            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
            framebuffers.append( framebufferIndex )

        activeProgram = program
        if self.separatePrograms:
            activeProgram = fboPrograms[ 0 ]

        OpenGLES2.UseProgram( activeProgram.programIndex )

        if not self.separateVbos: 
            OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                     'ON',
                                                     activeProgram.vertexLocationIndex,
                                                     meshBufferDataSetup.vertexArrayIndex,
                                                     boolToSpandex( meshBufferDataSetup.vertexArrayNormalized ) )
            OpenGLES2.EnableVertexAttribArray( activeProgram.vertexLocationIndex )

            OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                     'ON',
                                                     activeProgram.texCoordLocationIndices[ 0 ],
                                                     meshBufferDataSetup.texCoordArrayIndices[ 0 ],
                                                     boolToSpandex( meshBufferDataSetup.texCoordArrayNormalized[ 0 ] ) )
            OpenGLES2.EnableVertexAttribArray( activeProgram.texCoordLocationIndices[ 0 ] )
        
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        # Set first fbo program, if separate programs are used. Needed for
        # setting vbos.
        if self.separatePrograms:
            OpenGLES2.UseProgram( fboPrograms[ 0 ].programIndex )

        # Set fbo vbos.
        if self.separateVbos: 
            OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                     'ON',
                                                     activeProgram.vertexLocationIndex,
                                                     meshBufferDataSetup.vertexArrayIndex,
                                                     boolToSpandex( meshBufferDataSetup.vertexArrayNormalized ) )
            OpenGLES2.EnableVertexAttribArray( activeProgram.vertexLocationIndex )

            OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                     'ON',
                                                     activeProgram.texCoordLocationIndices[ 0 ],
                                                     meshBufferDataSetup.texCoordArrayIndices[ 0 ],
                                                     boolToSpandex( meshBufferDataSetup.texCoordArrayNormalized[ 0 ] ) )
            OpenGLES2.EnableVertexAttribArray( activeProgram.texCoordLocationIndices[ 0 ] )

        # Draw a pipeline of fbos.
        for i in range( self.loops ):
            for j in range( self.pipeLen ):
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebuffers[ j ] )
                OpenGLES2.Viewport( 0, 0, width, height )

                if j > 0 and self.separatePrograms:        
                    activeProgram = fboPrograms[ j ]
                    OpenGLES2.UseProgram( activeProgram.programIndex )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextures[ j ] )
                OpenGLES2.Uniformi( activeProgram.textureLocationIndices[ 0 ], 1, [ 0 ] )

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                drawMeshBufferData( modules, meshBufferDataSetup )

        if defaultFramebufferIndex >= 0:
            # Draw the final fbo in the pipeline to the default fbo/screen.
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
            OpenGLES2.Viewport( 0, 0, width, height )

        if self.separatePrograms:
            activeProgram = screenProgram
            OpenGLES2.UseProgram( activeProgram.programIndex )

        activeMeshBufferDataSetup = meshBufferDataSetup
        if self.separateVbos: 
            activeMeshBufferDataSetup = screenMeshBufferDataSetup
            OpenGLES2.BufferMeshVertexAttribPointer( screenMeshBufferDataSetup.meshBufferIndex,
                                                     'ON',
                                                     activeProgram.vertexLocationIndex,
                                                     screenMeshBufferDataSetup.vertexArrayIndex,
                                                     boolToSpandex( screenMeshBufferDataSetup.vertexArrayNormalized ) )
            OpenGLES2.EnableVertexAttribArray( activeProgram.vertexLocationIndex )

            OpenGLES2.BufferMeshVertexAttribPointer( screenMeshBufferDataSetup.meshBufferIndex,
                                                     'ON',
                                                     activeProgram.texCoordLocationIndices[ 0 ],
                                                     screenMeshBufferDataSetup.texCoordArrayIndices[ 0 ],
                                                     boolToSpandex( screenMeshBufferDataSetup.texCoordArrayNormalized[ 0 ] ) )
            OpenGLES2.EnableVertexAttribArray( activeProgram.texCoordLocationIndices[ 0 ] )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        if self.pipeLen > 0:
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextures[ self.pipeLen ] )
        else:
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )

        OpenGLES2.Uniformi( activeProgram.textureLocationIndices[ 0 ], 1, [ 0 ] )
        drawMeshBufferData( modules, activeMeshBufferDataSetup )

        target.swapBuffers( state )
