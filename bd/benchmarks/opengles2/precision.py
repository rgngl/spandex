#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *
  
######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    normalAttribute   = MeshAttribute( MESH_TYPE_FIXED )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           normalAttribute,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    meshSpherePerturbNormals( mesh, [ 0.5, 0.5 ], 0.4 )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ShaderPrecisionBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, precision ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.precision   = precision
        self.name = 'OPENGLES2 shader precision %s grd=%dx%dx%d tex=%s' % ( self.precision,
                                                                            self.gridx,
                                                                            self.gridy,
                                                                            self.overdraw,
                                                                            textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision %s float;
        attribute vec4 gVertex;
        attribute vec3 gNormal;
        attribute vec4 gTexCoord0;        
        uniform mat4 gModelView;
        uniform vec3 gLightDirection;
        uniform float gMaterialBias;
        uniform float gMaterialScale;
        
        varying vec3 gVSDiffuseColor;
        varying vec3 gVSSpecularColor;
        varying vec3 gVSNormal;
        varying vec3 gVSReflect;
        varying vec4 gVSTexCoord0;
        
        void main()
        {
           gl_Position = gModelView * gVertex;
           gVSDiffuseColor = vec3( max( dot( gNormal, gLightDirection ), 0.0 ) );
           gVSSpecularColor = vec3( max( ( gVSDiffuseColor.x - gMaterialBias ) * gMaterialScale, 0.0));
           gVSTexCoord0 = gTexCoord0;	
        }
        """ % ( self.precision, )

        rawFragmentShaderSource = """
        precision %s float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;
        varying vec3 gVSDiffuseColor;
        varying vec3 gVSSpecularColor;

        void main()
        {
           vec3 texColor = texture2D( gTexture0, gVSTexCoord0.xy ).rgb;
           gl_FragColor = vec4( ( texColor * gVSDiffuseColor ) + gVSSpecularColor, 1.0 );
        }
        """ % ( self.precision, )

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )
        
        useTexture( modules, textureSetup, 0, program )

        gModelView              = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( gModelView, program.programIndex, 'gModelView' )

        lightDirection = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( lightDirection, program.programIndex, 'gLightDirection' )

        materialBias = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( materialBias, program.programIndex, 'gMaterialBias' )

        materialScale = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( materialScale, program.programIndex, 'gMaterialScale' )

        OpenGLES2.Uniformf( lightDirection, 1, [ 0.0, 0.0, 1.0 ] )
        OpenGLES2.Uniformf( materialBias, 1, [ 0.1 ] )
        OpenGLES2.Uniformf( materialScale, 1, [ 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.UniformMatrix( gModelView, 1, [ 1.0,   0.0,   0.0,    0.0,
                                                      0.0,   1.0,   0.0,    0.0,
                                                      0.0,   0.0,   1.0,    0.0,
                                                      0.0,   0.0,   offset, 1.0 ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )
