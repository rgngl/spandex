#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SetupSimpleShaderBenchmark( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENGLES2 setup simple shader"

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gColor;
        varying vec4 gVSColor;
        uniform vec4 gOffset;

        void main()
        {
           gl_Position  = gVertex + gOffset;
           gVSColor = gColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        varying vec4 gVSColor;

        void main()
        {
           gl_FragColor = gVSColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )

        vsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        fsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vsDataIndex, vertexShaderSource )
        OpenGLES2.SetData( fsDataIndex, fragmentShaderSource )

        vsIndex                 = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        fsIndex                 = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        programIndex            = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        vertexLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        colorLocationIndex      = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        target.swapBuffers( state )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vsIndex )
        OpenGLES2.ShaderSource( vsIndex, vsDataIndex )

        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fsIndex )
        OpenGLES2.ShaderSource( fsIndex, fsDataIndex )

        OpenGLES2.CompileShader( vsIndex )
        OpenGLES2.CompileShader( fsIndex )

        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vsIndex )
        OpenGLES2.AttachShader( programIndex, fsIndex )

        vertexAttributeName         = 'gVertex'
        colorAttributeName          = 'gColor'

        locationValue               = 0

        OpenGLES2.SetRegister( vertexLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, vertexAttributeName )
        locationValue += 1

        OpenGLES2.SetRegister( colorLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, colorLocationIndex, colorAttributeName )
        locationValue += 1

        OpenGLES2.LinkProgram( programIndex )

        OpenGLES2.GetUniformLocation( offsetLocationIndex, programIndex, 'gOffset' )

        OpenGLES2.UseProgram( programIndex )

        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0, 0.0, 0.0, 0.0 ] )

        OpenGLES2.DeleteShader( vsIndex )
        OpenGLES2.DeleteShader( fsIndex )

        OpenGLES2.DeleteProgram( programIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES2.Finish()


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SetupMediumShaderBenchmark( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENGLES2 setup medium shader"

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gColor;
        attribute vec4 gTexCoord0;
        uniform mat4 gModelView;
        varying vec4 gVSColor;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = gModelView * gVertex;
           gVSColor = gColor;
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSColor;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = gVSColor * texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )

        vsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        fsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vsDataIndex, vertexShaderSource )
        OpenGLES2.SetData( fsDataIndex, fragmentShaderSource )

        vsIndex                 = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        fsIndex                 = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        programIndex            = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        vertexLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        colorLocationIndex      = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoord0LocationIndex  = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        modelViewLocationIndex  = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        textureLocationIndex    = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        target.swapBuffers( state )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vsIndex )
        OpenGLES2.ShaderSource( vsIndex, vsDataIndex )

        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fsIndex )
        OpenGLES2.ShaderSource( fsIndex, fsDataIndex )

        OpenGLES2.CompileShader( vsIndex )
        OpenGLES2.CompileShader( fsIndex )

        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vsIndex )
        OpenGLES2.AttachShader( programIndex, fsIndex )

        vertexAttributeName         = 'gVertex'
        colorAttributeName          = 'gColor'
        texCoord0AttributeName      = 'gTexCoord0'

        locationValue               = 0

        OpenGLES2.SetRegister( vertexLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, vertexAttributeName )
        locationValue += 1

        OpenGLES2.SetRegister( colorLocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, colorLocationIndex, colorAttributeName )
        locationValue += 1

        OpenGLES2.SetRegister( texCoord0LocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( programIndex, texCoord0LocationIndex, texCoord0AttributeName )
        locationValue += 1

        OpenGLES2.LinkProgram( programIndex )

        OpenGLES2.GetUniformLocation( modelViewLocationIndex, programIndex, 'gModelView' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'gTexture0' )

        OpenGLES2.UseProgram( programIndex )

        OpenGLES2.UniformMatrix( modelViewLocationIndex,
                                 1,
                                 [ 1.0, 0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )

        OpenGLES2.DeleteShader( vsIndex )
        OpenGLES2.DeleteShader( fsIndex )

        OpenGLES2.DeleteProgram( programIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES2.Finish()


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SwitchSimpleProgramBenchmark( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENGLES2 switch sample program"

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gColor;
        varying vec4 gVSColor;
        uniform vec4 gOffset;

        void main()
        {
           gl_Position  = gVertex + gOffset;
           gVSColor = gColor;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        varying vec4 gVSColor;

        void main()
        {
           gl_FragColor = gVSColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )

        vsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        fsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vsDataIndex, vertexShaderSource )
        OpenGLES2.SetData( fsDataIndex, fragmentShaderSource )

        vs0Index                = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        fs0Index                = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        program0Index           = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        vertex0LocationIndex    = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        color0LocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        offset0LocationIndex    = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )

        vs1Index                = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        fs1Index                = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        program1Index           = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        vertex1LocationIndex    = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        color1LocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        offset1LocationIndex    = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )

        vertexAttributeName     = 'gVertex'
        colorAttributeName      = 'gColor'
        locationValue           = 0

        # Create program 0
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vs0Index )
        OpenGLES2.ShaderSource( vs0Index, vsDataIndex )

        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fs0Index )
        OpenGLES2.ShaderSource( fs0Index, fsDataIndex )

        OpenGLES2.CompileShader( vs0Index )
        OpenGLES2.CompileShader( fs0Index )

        OpenGLES2.CreateProgram( program0Index )

        OpenGLES2.AttachShader( program0Index, vs0Index )
        OpenGLES2.AttachShader( program0Index, fs0Index )

        OpenGLES2.SetRegister( vertex0LocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( program0Index, vertex0LocationIndex, vertexAttributeName )
        locationValue += 1

        OpenGLES2.SetRegister( color0LocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( program0Index, color0LocationIndex, colorAttributeName )
        locationValue += 1

        OpenGLES2.LinkProgram( program0Index )

        OpenGLES2.GetUniformLocation( offset0LocationIndex, program0Index, 'gOffset' )

        OpenGLES2.DeleteShader( vs0Index )
        OpenGLES2.DeleteShader( fs0Index )

        # Create
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vs1Index )
        OpenGLES2.ShaderSource( vs1Index, vsDataIndex )

        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fs1Index )
        OpenGLES2.ShaderSource( fs1Index, fsDataIndex )

        OpenGLES2.CompileShader( vs1Index )
        OpenGLES2.CompileShader( fs1Index )

        OpenGLES2.CreateProgram( program1Index )

        OpenGLES2.AttachShader( program1Index, vs1Index )
        OpenGLES2.AttachShader( program1Index, fs1Index )

        OpenGLES2.SetRegister( vertex1LocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( program1Index, vertex1LocationIndex, vertexAttributeName )
        locationValue += 1

        OpenGLES2.SetRegister( color1LocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( program1Index, color1LocationIndex, colorAttributeName )
        locationValue += 1

        OpenGLES2.LinkProgram( program1Index )

        OpenGLES2.GetUniformLocation( offset1LocationIndex, program1Index, 'gOffset' )

        OpenGLES2.DeleteShader( vs1Index )
        OpenGLES2.DeleteShader( fs1Index )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        target.swapBuffers( state )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.UseProgram( program0Index )
        OpenGLES2.Uniformf( offset0LocationIndex, 1, [ 0.0, 0.0, 0.0, 0.0 ] )

        OpenGLES2.UseProgram( program1Index )
        OpenGLES2.Uniformf( offset1LocationIndex, 1, [ 0.0, 0.0, 0.0, 0.0 ] )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES2.Finish()


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SwitchMediumProgramBenchmark( Benchmark ):
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENGLES2 switch medium program"

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gColor;
        attribute vec4 gTexCoord0;
        uniform mat4 gModelView;
        varying vec4 gVSColor;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = gModelView * gVertex;
           gVSColor = gColor;
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSColor;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = gVSColor * texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )

        vsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        fsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vsDataIndex, vertexShaderSource )
        OpenGLES2.SetData( fsDataIndex, fragmentShaderSource )

        vertexAttributeName     = 'gVertex'
        colorAttributeName      = 'gColor'
        texCoord0AttributeName  = 'gTexCoord0'
        locationValue           = 0

        # Create program 0
        vs0Index                = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        fs0Index                = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        program0Index           = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        vertex0LocationIndex    = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        color0LocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoord0LocationIndex  = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        modelView0LocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texture0LocationIndex   = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )

        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vs0Index )
        OpenGLES2.ShaderSource( vs0Index, vsDataIndex )

        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fs0Index )
        OpenGLES2.ShaderSource( fs0Index, fsDataIndex )

        OpenGLES2.CompileShader( vs0Index )
        OpenGLES2.CompileShader( fs0Index )

        OpenGLES2.CreateProgram( program0Index )

        OpenGLES2.AttachShader( program0Index, vs0Index )
        OpenGLES2.AttachShader( program0Index, fs0Index )

        OpenGLES2.SetRegister( vertex0LocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( program0Index, vertex0LocationIndex, vertexAttributeName )
        locationValue += 1

        OpenGLES2.SetRegister( color0LocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( program0Index, color0LocationIndex, colorAttributeName )
        locationValue += 1

        OpenGLES2.SetRegister( texCoord0LocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( program0Index, texCoord0LocationIndex, texCoord0AttributeName )
        locationValue += 1

        OpenGLES2.LinkProgram( program0Index )

        OpenGLES2.GetUniformLocation( modelView0LocationIndex, program0Index, 'gModelView' )
        OpenGLES2.GetUniformLocation( texture0LocationIndex, program0Index, 'gTexture0' )

        OpenGLES2.DeleteShader( vs0Index )
        OpenGLES2.DeleteShader( fs0Index )

        # Create program 1
        vs1Index                = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        fs1Index                = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        program1Index           = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        vertex1LocationIndex    = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        color1LocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoord1LocationIndex  = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        modelView1LocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texture1LocationIndex   = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )

        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vs1Index )
        OpenGLES2.ShaderSource( vs1Index, vsDataIndex )

        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fs1Index )
        OpenGLES2.ShaderSource( fs1Index, fsDataIndex )

        OpenGLES2.CompileShader( vs1Index )
        OpenGLES2.CompileShader( fs1Index )

        OpenGLES2.CreateProgram( program1Index )

        OpenGLES2.AttachShader( program1Index, vs1Index )
        OpenGLES2.AttachShader( program1Index, fs1Index )

        OpenGLES2.SetRegister( vertex1LocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( program1Index, vertex1LocationIndex, vertexAttributeName )
        locationValue += 1

        OpenGLES2.SetRegister( color1LocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( program1Index, color1LocationIndex, colorAttributeName )
        locationValue += 1

        OpenGLES2.SetRegister( texCoord1LocationIndex, locationValue )
        OpenGLES2.BindAttribLocation( program1Index, texCoord1LocationIndex, texCoord0AttributeName )
        locationValue += 1

        OpenGLES2.LinkProgram( program1Index )

        OpenGLES2.GetUniformLocation( modelView1LocationIndex, program1Index, 'gModelView' )
        OpenGLES2.GetUniformLocation( texture1LocationIndex, program1Index, 'gTexture0' )

        OpenGLES2.DeleteShader( vs1Index )
        OpenGLES2.DeleteShader( fs1Index )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        target.swapBuffers( state )
        OpenGLES2.Finish()
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.UseProgram( program0Index )
        OpenGLES2.UniformMatrix( modelView0LocationIndex,
                                 1,
                                 [ 1.0, 0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.Uniformi( texture0LocationIndex, 1, [ 0 ] )

        OpenGLES2.UseProgram( program1Index )
        OpenGLES2.UniformMatrix( modelView1LocationIndex,
                                 1,
                                 [ 1.0, 0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.Uniformi( texture1LocationIndex, 1, [ 0 ] )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenGLES2.Finish()


######################################################################
def createPlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED, 2, [ 1.0, 1.0 ] ) )

    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh

######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, textureType ):
    OpenGLES2 = modules[ 'OpenGLES2' ]

    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        4, 4,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            'GL_LINEAR',
                            'GL_LINEAR',
                            'GL_CLAMP_TO_EDGE',
                            'GL_CLAMP_TO_EDGE' )

    return TextureSetup( textureDataIndex, textureIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RealisticProgramSwitchBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, textureType, switchIndex ):
        Benchmark.__init__( self )
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.switchIndex = switchIndex

        if self.switchIndex >= 0:
            si = str( self.switchIndex ) + '/' + str( self.overdraw )
        else:
            si = 'noswitch'

        self.name = 'OPENGLES2 realistic program switch %s grd=%dx%dx%d tex=%s' % ( si,
                                                                                    self.gridx,
                                                                                    self.gridy,
                                                                                    self.overdraw,
                                                                                    textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program0                = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        program1                = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )

        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program1 )
        offset1LocationIndex    = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offset1LocationIndex, program1.programIndex, 'gOffset' )

        OpenGLES2.UseProgram( program0.programIndex )
        OpenGLES2.ValidateProgram( program0.programIndex )
        offset0LocationIndex    = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offset0LocationIndex, program0.programIndex, 'gOffset' )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                screenWidth, screenHeight,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                self.textureType )

        useTexture( modules, textureSetup, 0, program0 )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        activeOffsetLocationIndex = offset0LocationIndex
        if self.switchIndex >= 0:
            OpenGLES2.UseProgram( program0.programIndex )
            OpenGLES2.Uniformi( program0.textureLocationIndices[ 0 ], 1, [ 0 ] )
            activeOffsetLocationIndex = offset0LocationIndex

        for i in range( self.overdraw ):
            if i == self.switchIndex:
                OpenGLES2.UseProgram( program0.programIndex )
                OpenGLES2.Uniformi( program1.textureLocationIndices[ 0 ], 1, [ 0 ] )
                activeOffsetLocationIndex = offset1LocationIndex

            OpenGLES2.Uniformf( activeOffsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


######################################################################
def createTrianglePlane( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) )

    mesh = MeshTrianglePlane( gridx,
                              gridy,
                              vertexAttribute,
                              None,
                              None,
                              texCoordAttribute,
                              indexAttribute,
                              None,
                              MESH_CCW_WINDING,
                              False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ShaderCacheBenchmark( Benchmark ):
    def __init__( self, draws, shaders, blend, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.draws       = draws
        self.shaders     = shaders
        self.blend       = blend
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType

        blendstr = 'no-blend'
        if self.blend == 1:
            blendstr = 'flip-blend'
        elif self.blend > 1:
            blendstr = 'cycle%d-blend' % ( self.blend, )

        self.name = 'OPENGLES2 shader cache drws=%d shdrs=%d %s grd=%dx%dx%d tex=%s' % ( self.draws,
                                                                                        self.shaders,
                                                                                        blendstr,
                                                                                        self.gridx,
                                                                                        self.gridy,
                                                                                        self.overdraw,
                                                                                        textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        attribute highp vec4 gVertex;
        attribute mediump vec4 gTexCoord0;
        varying mediump vec4 gVSTexCoord0;
        uniform highp mat4 gModelView;

        void main()
        {
           gl_Position = gModelView * gVertex;
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        uniform sampler2D gTexture0;
        varying mediump vec4 gVSTexCoord0;
        const mediump vec4 modColor = vec4( %.3f, %.3f, %.3f, %.3f );

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy ) * modColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createTrianglePlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )

        programs = []
        modelViewLocations = []
        textureLocations = []
        for i in range( self.shaders ):
            fs = fragmentShaderSource % ( ( i + 1 ) / float( self.shaders ), float( i + 1 ) / self.shaders, 1.0, 1.0, )
            program = createProgram( modules, indexTracker, vertexShaderSource, fs, vertexDataSetup )
            programs.append( program )

            modelViewLocation = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( modelViewLocation, program.programIndex, 'gModelView' )
            modelViewLocations.append( modelViewLocation )

            textureLocation = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( textureLocation, program.programIndex, 'gTexture0' )
            textureLocations.append( textureLocation )

            OpenGLES2.UseProgram( program.programIndex )
            OpenGLES2.ValidateProgram( program.programIndex )

        textureSetup = setupTexture( modules,
                                     indexTracker,
                                     screenWidth, screenHeight,
                                     [ 1.0, 0.3, 0.3, 0.3 ],
                                     [ 0.3, 1.0, 0.3, 0.7 ],
                                     self.textureType )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 1.0, 0.5 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        # How many triangles to draw per draw call. Leftover triangles are added
        # to the last draw.
        triangleBatch     = ( meshBufferDataSetup.indexArrayLength / 3 ) / self.draws
        leftOverTriangles = ( meshBufferDataSetup.indexArrayLength / 3 ) % self.draws

        for i in range( self.overdraw ):
            for j in range( self.draws ):

                pi = j % len( programs )

                # Offset to the start of the current triangle batch.
                triangleOffset = triangleBatch * j * 3

                if self.blend > 0 :
                    blends = [ [ 'GL_SRC_ALPHA',           'GL_ONE_MINUS_SRC_ALPHA' ],
                               [ 'GL_SRC_COLOR',           'GL_ONE_MINUS_SRC_COLOR' ],
                               [ 'GL_DST_COLOR',           'GL_ONE_MINUS_SRC_ALPHA' ],
                               [ 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE' ],
                               [ 'GL_ONE',                 'GL_ONE' ],
                               [ 'GL_ZERO',                'GL_ONE' ],
                               [ 'GL_ZERO',                'GL_SRC_COLOR' ],
                               [ 'GL_ONE',                 'GL_ONE_MINUS_SRC_ALPHA' ],
                               [ 'GL_ONE_MINUS_SRC_ALPHA', 'GL_SRC_ALPHA' ],
                               [ 'GL_DST_ALPHA',           'GL_ONE_MINUS_DST_ALPHA' ],
                             ]

                    bi = j % ( self.blend + 1 )

                    if bi >= ( len( blends ) + 1 ):
                        raise 'Too long blend cycle'

                    if bi == 0:
                        OpenGLES2.Disable( 'GL_BLEND' )
                    else:
                        OpenGLES2.Enable( 'GL_BLEND' )
                        OpenGLES2.BlendFunc( blends[ bi - 1 ][ 0 ], blends[ bi - 1 ][ 1 ] )

                program = programs[ pi ]
                OpenGLES2.UseProgram( program.programIndex )
                OpenGLES2.UniformMatrix( modelViewLocations[ pi ], 1, [ 1.0,   0.0,   0.0,    0.0,
                                                                        0.0,   1.0,   0.0,    0.0,
                                                                        0.0,   0.0,   1.0,    0.0,
                                                                        0.0,   0.0,   offset, 1.0 ] )
                OpenGLES2.Uniformi( textureLocations[ pi ], 1, [ 0 ] )

                OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                         'ON',
                                                         program.vertexLocationIndex,
                                                         meshBufferDataSetup.vertexArrayIndex,
                                                         boolToSpandex( meshBufferDataSetup.vertexArrayNormalized ) )
                OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

                OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                         'ON',
                                                         program.texCoordLocationIndices[ 0 ],
                                                         meshBufferDataSetup.texCoordArrayIndices[ 0 ],
                                                         boolToSpandex( meshBufferDataSetup.texCoordArrayNormalized[ 0 ] ) )
                OpenGLES2.EnableVertexAttribArray( program.texCoordLocationIndices[ 0 ] )

                lo = 0
                if j == ( self.draws - 1 ):
                    lo = leftOverTriangles

                OpenGLES2.BufferDrawElements( meshBufferDataSetup.indexBufferIndex,
                                              'ON',
                                              meshBufferDataSetup.glMode,
                                              ( triangleBatch + lo ) * 3,
                                              triangleOffset )

            offset -= delta

        target.swapBuffers( state )


######################################################################
def createTrianglePlane2( gridx, gridy ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
    normalAttribute   = MeshAttribute( MESH_TYPE_FLOAT )
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) )

    mesh = MeshTrianglePlane( gridx,
                              gridy,
                              vertexAttribute,
                              None,
                              normalAttribute,
                              texCoordAttribute,
                              indexAttribute,
                              None,
                              MESH_CCW_WINDING,
                              False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    meshSpherePerturbNormals( mesh, [ 0.5, 0.5 ], 0.4 )

    return mesh


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ComplexShaderCacheBenchmark( Benchmark ):
    def __init__( self, draws, shaders, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.draws       = draws
        self.shaders     = shaders
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 complex shader cache drws=%d shdrs=%d grd=%dx%dx%d tex=%s' % ( self.draws,
                                                                                              self.shaders,
                                                                                              self.gridx,
                                                                                              self.gridy,
                                                                                              self.overdraw,
                                                                                              textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        attribute highp vec4 gVertex;
        attribute highp vec3 gNormal;
        attribute mediump vec4 gTexCoord0;
        uniform highp mat4 gModelView;
        uniform highp vec3 gLightDirection;
        uniform highp float gMaterialBias;
        uniform highp float gMaterialScale;

        varying mediump vec3 gVSDiffuseColor;
        varying mediump vec3 gVSSpecularColor;
        varying mediump vec3 gVSNormal;
        varying mediump vec3 gVSReflect;
        varying mediump vec4 gVSTexCoord0;

        void main()
        {
           gl_Position = gModelView * gVertex;
           gVSDiffuseColor = vec3( max( dot( gNormal, gLightDirection ), 0.0 ) );
           gVSSpecularColor = vec3( max( ( gVSDiffuseColor.x - gMaterialBias ) * gMaterialScale, 0.0));
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        uniform sampler2D gTexture0;
        varying mediump vec4 gVSTexCoord0;
        varying mediump vec3 gVSDiffuseColor;
        varying mediump vec3 gVSSpecularColor;
        const mediump vec4 modColor = vec4( %.3f, %.3f, %.3f, %.3f );

        void main()
        {
           mediump vec3 texColor = texture2D( gTexture0, gVSTexCoord0.xy ).rgb;
           gl_FragColor = vec4( ( texColor * gVSDiffuseColor ) + gVSSpecularColor, 1.0 ) * modColor;
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createTrianglePlane2( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )

        programs           = []
        modelViewLocations = []
        lightDirections    = []
        materialBiases     = []
        materialScales     = []
        textureLocations   = []
        for i in range( self.shaders ):
            fs = fragmentShaderSource % ( ( i + 1 ) / float( self.shaders ), float( i + 1 ) / self.shaders, 1.0, 1.0, )
            program = createProgram( modules, indexTracker, vertexShaderSource, fs, vertexDataSetup )
            programs.append( program )

            modelViewLocation = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( modelViewLocation, program.programIndex, 'gModelView' )
            modelViewLocations.append( modelViewLocation )

            lightDirection = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( lightDirection, program.programIndex, 'gLightDirection' )
            lightDirections.append( lightDirection )

            materialBias = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( materialBias, program.programIndex, 'gMaterialBias' )
            materialBiases.append( materialBias )

            materialScale = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( materialScale, program.programIndex, 'gMaterialScale' )
            materialScales.append( materialScale )

            textureLocation = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( textureLocation, program.programIndex, 'gTexture0' )
            textureLocations.append( textureLocation )

            OpenGLES2.UseProgram( program.programIndex )
            OpenGLES2.ValidateProgram( program.programIndex )

        textureSetup = setupTexture( modules,
                                     indexTracker,
                                     screenWidth, screenHeight,
                                     [ 1.0, 0.3, 0.3, 0.3 ],
                                     [ 0.3, 1.0, 0.3, 0.7 ],
                                     self.textureType )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 1.0, 0.5 ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        # How many triangles to draw per draw call. Leftover triangles are added
        # to the last draw.
        triangleBatch     = ( meshBufferDataSetup.indexArrayLength / 3 ) / self.draws
        leftOverTriangles = ( meshBufferDataSetup.indexArrayLength / 3 ) % self.draws

        for i in range( self.overdraw ):
            for j in range( self.draws ):

                pi = j % len( programs )

                # Offset to the start of the current triangle batch.
                triangleOffset = triangleBatch * j * 3

                program = programs[ pi ]
                OpenGLES2.UseProgram( program.programIndex )
                OpenGLES2.UniformMatrix( modelViewLocations[ pi ], 1, [ 1.0,   0.0,   0.0,    0.0,
                                                                        0.0,   1.0,   0.0,    0.0,
                                                                        0.0,   0.0,   1.0,    0.0,
                                                                        0.0,   0.0,   offset, 1.0 ] )

                OpenGLES2.Uniformf( lightDirections[ pi ], 1, [ 0.0, 0.0, 1.0 ] )
                OpenGLES2.Uniformf( materialBiases[ pi ], 1, [ 0.1 ] )
                OpenGLES2.Uniformf( materialScales[ pi ], 1, [ 1.0 ] )
                OpenGLES2.Uniformi( textureLocations[ pi ], 1, [ 0 ] )

                OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                         'ON',
                                                         program.vertexLocationIndex,
                                                         meshBufferDataSetup.vertexArrayIndex,
                                                         boolToSpandex( meshBufferDataSetup.vertexArrayNormalized ) )
                OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

                OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                         'ON',
                                                         program.normalLocationIndex,
                                                         meshBufferDataSetup.normalArrayIndex,
                                                         'ON' )
                OpenGLES2.EnableVertexAttribArray( program.normalLocationIndex )

                OpenGLES2.BufferMeshVertexAttribPointer( meshBufferDataSetup.meshBufferIndex,
                                                         'ON',
                                                         program.texCoordLocationIndices[ 0 ],
                                                         meshBufferDataSetup.texCoordArrayIndices[ 0 ],
                                                         boolToSpandex( meshBufferDataSetup.texCoordArrayNormalized[ 0 ] ) )
                OpenGLES2.EnableVertexAttribArray( program.texCoordLocationIndices[ 0 ] )

                lo = 0
                if j == ( self.draws - 1 ):
                    lo = leftOverTriangles

                OpenGLES2.BufferDrawElements( meshBufferDataSetup.indexBufferIndex,
                                              'ON',
                                              meshBufferDataSetup.glMode,
                                              ( triangleBatch + lo ) * 3,
                                              triangleOffset )

            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ShaderBranchReferenceBenchmark( Benchmark ):
    def __init__( self, overdraw ):
        Benchmark.__init__( self )
        self.gridx       = 2
        self.gridy       = 2
        self.overdraw    = overdraw
        self.name = 'OPENGLES2 shader branch no-branch reference ovrdrw=%d' % ( self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW  = screenWidth
        textureH  = screenHeight

        rawVertexShaderSource = """
        attribute highp vec4 gVertex;
        attribute mediump vec4 gTexCoord0;
        varying mediump vec4 gVSTexCoord0;
        uniform mediump float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        rawFragmentShaderSource = """
        uniform sampler2D gTexture0;
        varying mediump vec4 gVSTexCoord0;

        void main()
        {
            gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }
        """

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                'OPENGLES2_RGBA8888', )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ShaderBranchBenchmark( Benchmark ):
    def __init__( self, bratio, condition, complexity, overdraw ):
        Benchmark.__init__( self )
        self.bratio      = bratio
        self.condition   = condition  # const, uniform
        self.complexity  = complexity
        self.gridx       = 2
        self.gridy       = 2
        self.overdraw    = overdraw
        self.name = 'OPENGLES2 shader branch brtio=%.2f cndtion=%s cmplxty=%d ovrdrw=%d' % ( self.bratio,
                                                                                             self.condition,
                                                                                             self.complexity,
                                                                                             self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW  = screenWidth
        textureH  = screenHeight

        rawVertexShaderSource = """
        attribute highp vec4 gVertex;
        attribute mediump vec4 gTexCoord0;
        varying mediump vec4 gVSTexCoord0;
        uniform mediump float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        if self.condition == 'const':
            us = ''
            cs = 'gVSTexCoord0.x > %f' % ( self.bratio, )
        elif self.condition == 'uniform':
            us = 'uniform mediump float threshold;'
            cs = 'gVSTexCoord0.x > threshold'
        else:
            raise 'Unexpected condition'

        rawFragmentShaderSource = """
        uniform sampler2D gTexture0;
        varying mediump vec4 gVSTexCoord0;
        %s

        void main()
        {
           mediump vec4 color;

           if( %s )
           {
               color = texture2D( gTexture0, gVSTexCoord0.xy );
           }
           else
           {
               const mediump float time = 16.0;
               const mediump vec2 resolution = vec2( %d, %d );
               mediump vec2 p = -1.0 + 2.0 * gl_FragCoord.xy / resolution.xy;
               p.x *= ( resolution.x / resolution.y );

               mediump float zoo = 0.62 + 0.38 * sin( 0.1 * time );
               mediump float coa = cos( 0.1 * ( 1.0 - zoo ) * time );
               mediump float sia = sin( 0.1 * ( 1.0 - zoo ) * time );

               zoo = pow( zoo, 8.0 );
               mediump vec2 xy = vec2( p.x * coa - p.y * sia, p.x * sia + p.y * coa );
               mediump vec2 cc = vec2( -0.745, 0.186 ) + xy * zoo;

               mediump vec2 z = vec2( 0.0 );
               mediump vec2 z2 = z*z;
               mediump float m2;
               mediump float co = 0.0;
               for( int i = 0; i < %d; i++ )
               {
                   z = cc + vec2( z.x * z.x - z.y * z.y, 2.0 * z.x * z.y );
                   m2 = dot( z, z );
                   if( m2 > 1024.0 )
                   {
                       break;
                   }
                   co += 1.0;
               }
               co = co + 1.0 - log2( 0.5 * log2( m2 ) );
               co = sqrt( co / 256.0 );
               color = vec4( 0.5 + 0.5 * cos( 6.2831 * co + 0.0 ),
                             0.5 + 0.5 * cos( 6.2831 * co + 0.4 ),
                             0.5 + 0.5 * cos( 6.2831 * co + 0.7 ),
                             1.0 );
           }
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy ) * color;
        }
        """ % ( us, cs, screenWidth, screenHeight, self.complexity, )

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                'OPENGLES2_RGBA8888', )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        if self.condition == 'uniform':
            thresholdLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( thresholdLocationIndex, program.programIndex, 'threshold' )
            OpenGLES2.Uniformf( thresholdLocationIndex, 1, [ self.bratio ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ShaderWithDiscardBranchBenchmark( Benchmark ):
    def __init__( self, dratio, condition, overdraw ):
        Benchmark.__init__( self )
        self.dratio      = dratio
        self.condition   = condition  # const, uniform
        self.gridx       = 2
        self.gridy       = 2
        self.overdraw    = overdraw
        self.name = 'OPENGLES2 shader with discard branch drtio=%.2f cndtion=%s ovrdrw=%d' % ( self.dratio,
                                                                                               self.condition,
                                                                                               self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()

        rawVertexShaderSource = """
        attribute highp vec4 gVertex;
        attribute mediump vec4 gTexCoord0;
        varying mediump vec4 gVSTexCoord0;
        uniform mediump float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }
        """

        if self.condition == 'const':
            us = ''
            cs = 'gVSTexCoord0.x > %f' % ( self.dratio, )
        elif self.condition == 'uniform':
            us = 'uniform mediump float threshold;'
            cs = 'gVSTexCoord0.x > threshold'
        else:
            raise 'Unexpected condition'

        rawFragmentShaderSource = """
        uniform sampler2D gTexture0;
        varying mediump vec4 gVSTexCoord0;
        %s

        void main()
        {
           if( %s )
           {
               discard;
           }
           else
           {
               gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
           }
        }
        """ % ( us, cs, )

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        textureW  = screenWidth
        textureH  = screenHeight

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )
        mesh                    = createPlane( self.gridx, self.gridy )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                'OPENGLES2_RGBA8888', )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        if self.condition == 'uniform':
            thresholdLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
            OpenGLES2.GetUniformLocation( thresholdLocationIndex, program.programIndex, 'threshold' )
            OpenGLES2.Uniformf( thresholdLocationIndex, 1, [ self.dratio ] )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )

        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
            drawMeshBufferData( modules, meshBufferDataSetup )
            offset -= delta

        target.swapBuffers( state )

