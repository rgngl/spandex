#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
    vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
    colorAttribute    = None
    texCoordAttribute = []
    indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
    if colorAttributes:
        colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

    if texCoordAttributes:
        for i in range( len( texCoordAttributes ) ):
            texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                             2,
                                                             [ float( tcx ),
                                                               float( tcy ) ] ) )

    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           colorAttribute,
                           None,
                           texCoordAttribute,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2, cx, cy, textureType, minF, magF ):
    OpenGLES2 = modules[ 'OpenGLES2' ]
    
    textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
    OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                        c1,
                                        c2,
                                        cx, cy,
                                        w, h,
                                        'OFF',
                                        textureType )

    textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
    OpenGLES2.GenTexture( textureIndex )
    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
    OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
    OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                            minF,
                            magF,
                            'GL_REPEAT',
                            'GL_REPEAT' )

    return TextureSetup( textureDataIndex, textureIndex )

       
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------        
class DepthBenchmark( Benchmark ):
    def __init__( self, depthBits, gridx, gridy, overdraw, textureType ):
        Benchmark.__init__( self )
        self.depthBits   = depthBits
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.textureType = textureType
        self.name = 'OPENGLES2 depth test benchmark %s grd=%dx%dx%d tex=%s' % ( { 0 : 'no depth', 16 : '16b', 24 : '24b' }[ self.depthBits ],
                                                                                self.gridx,
                                                                                self.gridy,
                                                                                self.overdraw,
                                                                                textureTypeToStr( self.textureType ), )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;
        attribute vec4 gTexCoord0;
        varying vec4 gVSTexCoord0;
        uniform float gOffset;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
           gVSTexCoord0 = gTexCoord0;
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec4 gVSTexCoord0;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )

        ( screenWidth, screenHeight, ) = target.getScreenSize()     
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        tcx       = 1.0
        tcy       = 1.0
        textureW  = screenWidth
        textureH  = screenHeight
        filtering = 'GL_LINEAR'

        attribs = target.getDefaultAttributes( API_OPENGLES2 )
        if self.depthBits == 0:
            attribs[ CONFIG_DEPTHSIZE ] = '-'
        else:
            attribs[ CONFIG_DEPTHSIZE ] = self.depthBits            
       
        state                   = setupGL2( modules, indexTracker, target, attribs )
        mesh                    = createPlane( self.gridx, self.gridy, tcx, tcy, False, [ True ] )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        meshDataSetup           = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup     = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )

        textureSetup            = setupTexture( modules,
                                                indexTracker,
                                                textureW, textureH,
                                                [ 1.0, 0.3, 0.3, 1.0 ],
                                                [ 0.3, 1.0, 0.3, 1.0 ],
                                                4, 4,                                                               
                                                self.textureType,
                                                filtering,
                                                filtering )

        useTexture( modules, textureSetup, 0, program )

        offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

        if self.depthBits > 0:
            OpenGLES2.Enable( 'GL_DEPTH_TEST' )
            OpenGLES2.DepthFunc( 'GL_LEQUAL' )

            OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_EQUAL', [ self.depthBits ] )
            
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        mask = [ 'GL_COLOR_BUFFER_BIT' ]

        if self.depthBits > 0:        
            mask.append( 'GL_DEPTH_BUFFER_BIT' )
            
        OpenGLES2.Clear( mask )
        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.5 ] )
        
        for i in range( self.overdraw ):
            drawMeshBufferData( modules, meshBufferDataSetup )

        target.swapBuffers( state )

