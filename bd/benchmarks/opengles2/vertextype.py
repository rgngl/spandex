#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

######################################################################
FIXED        = 'fixed'
FLOAT        = 'float'

######################################################################
def createPlane( gridx, gridy, type ):
    vertexAttribute     = MeshVertexAttribute( type, 3 )
    texCoordAttribute   = []
    indexAttribute      = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
        
    mesh = MeshStripPlane( gridx,
                           gridy,
                           vertexAttribute,
                           None,
                           None,
                           None,
                           indexAttribute,
                           None,
                           MESH_CCW_WINDING,
                           False )

    mesh.translate( [ -0.5, -0.5, 0 ] )
    mesh.scale( [ 2.0, 2.0 ] )

    return mesh


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class VertexTypeBenchmark( Benchmark ):
    def __init__( self, gridx, gridy, overdraw, vertexType ):
        Benchmark.__init__( self )
        self.gridx      = gridx
        self.gridy      = gridy
        self.overdraw   = overdraw
        self.vertexType = vertexType

        typemap         = { FIXED : MESH_TYPE_FIXED,
                            FLOAT : MESH_TYPE_FLOAT }
        self.ivtype     = typemap[ self.vertexType ]
        
        self.name = 'OPENGLES2 vertex type %s grd=%dx%dx%d' % ( self.vertexType,
                                                                self.gridx,
                                                                self.gridy,
                                                                self.overdraw, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]

        indexTracker = IndexTracker()

        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 gVertex;

        void main()
        {
           gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
        }"""

        rawFragmentShaderSource = """
        precision mediump float;
        uniform vec4 gColor;

        void main()
        {
           gl_FragColor = gColor;
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )
            
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state                   = setupGL2( modules, indexTracker, target )            
        mesh                    = createPlane( self.gridx, self.gridy, self.ivtype )
        vertexDataSetup         = setupVertexData( modules, indexTracker, mesh )
        program                 = createProgram( modules, indexTracker, vertexShaderSource, fragmentShaderSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        colorLocationIndex      = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( colorLocationIndex, program.programIndex, 'gColor' )

        OpenGLES2.CheckValue( 'GL_DEPTH_BITS', 'SCT_GREATER', [ 0 ] )
        
        OpenGLES2.Enable( 'GL_DEPTH_TEST' )
        OpenGLES2.DepthFunc( 'GL_LEQUAL' )
        OpenGLES2.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        delta   = 1.0 / float( self.overdraw )
        offset  = 1.0

        for i in range( self.overdraw ):
            OpenGLES2.Uniformf( colorLocationIndex, 1, [ 0.3, offset, 0.3, 1.0 ] )
            drawVertexData( modules, vertexDataSetup )
            offset -= delta

        target.swapBuffers( state )
