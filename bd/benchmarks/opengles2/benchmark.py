#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# Potential additional benchmarks
#    * better vbo benchmarks: two meshes with slightly different data
#    * triangle fan
#    ...

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../../' )

import  bd

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'OpenGLES2.0 benchmark'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

#----------------------------------------------------------------------
import resources
suite.addBenchmark( resources.PrintResourceProfile( 'start' ), 'EGL_FULL' )

#----------------------------------------------------------------------
import info
suite.addBenchmark( info.PrintOpenGLES2Info() )

#----------------------------------------------------------------------
import clear
suite.addBenchmark( clear.ClearBenchmark( [ 'GL_COLOR_BUFFER_BIT' ] ) )
suite.addBenchmark( clear.ClearBenchmark( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] ) )
suite.addBenchmark( clear.SynchronizedClearBenchmark( [ 'GL_COLOR_BUFFER_BIT' ] ) )

#----------------------------------------------------------------------
import marketing
GRIDX    = 2
GRIDY    = 2
OVERDRAW = 32
suite.addBenchmark( marketing.SmoothPixelsBenchmark( GRIDX, GRIDY, OVERDRAW ) )
suite.addBenchmark( marketing.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( marketing.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB888' ) )
suite.addBenchmark( marketing.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( marketing.SmoothTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( marketing.FlatMultitexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565','OPENGLES2_LUMINANCE8' ) )
suite.addBenchmark( marketing.SmoothMultitexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565','OPENGLES2_LUMINANCE8' ) )
suite.addBenchmark( marketing.PixelFillrateBenchmark( OVERDRAW ) )
suite.addBenchmark( marketing.TexelFillrateBenchmark( OVERDRAW, 'OPENGLES2_RGB565', False ) )
suite.addBenchmark( marketing.TexelFillrateBenchmark( OVERDRAW, 'OPENGLES2_RGB565', True ) )
suite.addBenchmark( marketing.TexelFillrateBenchmark( OVERDRAW, 'OPENGLES2_RGBA8888', False ) )
suite.addBenchmark( marketing.TexelFillrateBenchmark( OVERDRAW, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( marketing.RotatedTexelFillrateBenchmark( OVERDRAW, 'OPENGLES2_RGB565' ) )

suite.addBenchmark( marketing.FlatTexelsBgraSwizzleBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( marketing.UniformPixelsBenchmark(  2,  2, 4, 2, 1, None ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark(  2,  2, 4, 2, 3, None ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark(  2,  2, 4, 2, 7, None ) )

suite.addBenchmark( marketing.UniformPixelsBenchmark(  2,  2, 4, 2, 1, 'front-to-back' ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark(  2,  2, 4, 2, 3, 'front-to-back' ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark(  2,  2, 4, 2, 7, 'front-to-back' ) )

suite.addBenchmark( marketing.UniformPixelsBenchmark(  2,  2, 4, 2, 1, 'back-to-front' ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark(  2,  2, 4, 2, 3, 'back-to-front' ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark(  2,  2, 4, 2, 7, 'back-to-front' ) )

suite.addBenchmark( marketing.UniformPixelsBenchmark( 20, 32, 4, 8, 1, None ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark( 20, 32, 4, 8, 3, None ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark( 20, 32, 4, 8, 7, None ) )

suite.addBenchmark( marketing.UniformPixelsBenchmark( 20, 32, 4, 8, 1, 'front-to-back' ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark( 20, 32, 4, 8, 3, 'front-to-back' ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark( 20, 32, 4, 8, 7, 'front-to-back' ) )

suite.addBenchmark( marketing.UniformPixelsBenchmark( 20, 32, 4, 8, 1, 'back-to-front' ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark( 20, 32, 4, 8, 3, 'back-to-front' ) )
suite.addBenchmark( marketing.UniformPixelsBenchmark( 20, 32, 4, 8, 7, 'back-to-front' ) )

suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 1, None, True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 3, None, True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 7, None, True ) )

suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 1, 'front-to-back', True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 3, 'front-to-back', True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 7, 'front-to-back', True ) )

suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 1, 'back-to-front', True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 3, 'back-to-front', True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 7, 'back-to-front', True ) )

suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 1, None, True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 3, None, True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 7, None, True ) )

suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 1, 'front-to-back', True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 3, 'front-to-back', True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 7, 'front-to-back', True ) )

suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 1, 'back-to-front', True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 3, 'back-to-front', True ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 7, 'back-to-front', True ) )

suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 1, 'back-to-front', False ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 3, 'back-to-front', False ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 2, 2, 4, 2, 7, 'back-to-front', False ) )

suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 1, 'back-to-front', False ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 3, 'back-to-front', False ) )
suite.addBenchmark( marketing.VaryingPixelsBenchmark( 20, 32, 4, 8, 7, 'back-to-front', False ) )

#----------------------------------------------------------------------
import realistic
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( realistic.SmoothPixelsBenchmark( GRIDX, GRIDY, OVERDRAW ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', False ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB888', False ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888', False ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB888', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( realistic.SmoothTexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( realistic.FlatMultitexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565','OPENGLES2_LUMINANCE8' ) )
suite.addBenchmark( realistic.SmoothMultitexelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565','OPENGLES2_LUMINANCE8' ) )

suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 2, 'OPENGLES2_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 4, 'OPENGLES2_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 6, 'OPENGLES2_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 8, 'OPENGLES2_RGB565', True ) )
suite.addBenchmark( realistic.FlatTexelsBenchmark( GRIDX, GRIDY, 10, 'OPENGLES2_RGB565', True ) )

suite.addBenchmark( realistic.FlatTexelsBgraSwizzleBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888', False ) )

#----------------------------------------------------------------------
import depth
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( depth.DepthBenchmark( 0, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( depth.DepthBenchmark( 16, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( depth.DepthBenchmark( 24, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )

#----------------------------------------------------------------------
import draworder
GRIDX     = 20
GRIDY     = 32
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 2, 'OPENGLES2_RGB565', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 2, 'OPENGLES2_RGB565', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 4, 'OPENGLES2_RGB565', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 4, 'OPENGLES2_RGB565', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 8, 'OPENGLES2_RGB565', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 8, 'OPENGLES2_RGB565', draworder.FRONT_TO_BACK ) )

suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 2, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 2, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 4, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 4, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 8, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( GRIDX, GRIDY, 8, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )

suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 2, 'OPENGLES2_RGB565', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 2, 'OPENGLES2_RGB565', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 4, 'OPENGLES2_RGB565', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 4, 'OPENGLES2_RGB565', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 8, 'OPENGLES2_RGB565', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 8, 'OPENGLES2_RGB565', draworder.FRONT_TO_BACK ) )

suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 2, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 2, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 4, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 4, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 8, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderBenchmark( 2, 2, 8, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )

suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( GRIDX, GRIDY, 2, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( GRIDX, GRIDY, 2, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( GRIDX, GRIDY, 4, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( GRIDX, GRIDY, 4, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( GRIDX, GRIDY, 8, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( GRIDX, GRIDY, 8, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )

suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( 2, 2, 2, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( 2, 2, 2, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( 2, 2, 4, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( 2, 2, 4, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )
suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( 2, 2, 8, 'OPENGLES2_RGBA8888', draworder.BACK_TO_FRONT ) )
suite.addBenchmark( draworder.DrawOrderWithHslBenchmark( 2, 2, 8, 'OPENGLES2_RGBA8888', draworder.FRONT_TO_BACK ) )

#----------------------------------------------------------------------
import transformation
GRIDX     = 48
GRIDY     = 128
OVERDRAW  = 3
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, None, transformation.NONE ) )
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, None, transformation.HALF ) )
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, None, transformation.FULLY ) )
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', transformation.NONE ) )
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', transformation.HALF ) )
suite.addBenchmark( transformation.TransformationBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', transformation.FULLY ) )

#----------------------------------------------------------------------
import texturetype
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 3
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_LUMINANCE8' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_ALPHA8' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_LUMINANCE_ALPHA88' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB888' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA4444' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA5551' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturetype.TextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_BGRA8888' ) )

suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_RGB565' ) )
suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_ETC1_RGB8' ) )
suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_PVRTC_RGB2' ) )
suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_PVRTC_RGB4' ) )
suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_PVRTC_RGBA2' ) )
suite.addBenchmark( texturetype.CompressedTextureTypeBenchmark( GRIDX, GRIDY, OVERDRAW, 'IMAGES_PVRTC_RGBA4' ) )

#----------------------------------------------------------------------
import texturesize
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 3
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 32, 32, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 64, 64, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 127, 127, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 128, 128, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 255, 255, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 511, 511, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565' ) )

suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 32, 32, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 64, 64, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 127, 127, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 128, 128, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 255, 255, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 511, 511, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturesize.TextureSizeBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGBA8888' ) )

#----------------------------------------------------------------------
import texturemipmap
suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 512, 512, 'OPENGLES2_RGBA8888', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ) )
suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 512, 512, 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ) )
suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 512, 512, 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_LINEAR', 'GL_REPEAT' ) )
suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 512, 512, 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ) )

suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 360, 640, 'OPENGLES2_RGBA8888', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ) )
suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 360, 640, 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ) )
suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 360, 640, 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_LINEAR', 'GL_REPEAT' ) )
suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 360, 640, 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ) )

suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 320, 480, 'OPENGLES2_RGBA8888', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ) )
suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 320, 480, 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ) )
suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 320, 480, 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_LINEAR', 'GL_REPEAT' ) )
suite.addBenchmark( texturemipmap.MipmapBenchmark( 2, 2, 1, 320, 480, 'OPENGLES2_RGBA8888', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ) )

#----------------------------------------------------------------------
import vertexstore
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 3
suite.addBenchmark( vertexstore.VertexStoreBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', vertexstore.ARRAYS ) )
suite.addBenchmark( vertexstore.VertexStoreBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', vertexstore.MESH ) )
suite.addBenchmark( vertexstore.VertexStoreBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', vertexstore.VBO_ARRAYS ) )
suite.addBenchmark( vertexstore.VertexStoreBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', vertexstore.VBO_MESH ) )

#----------------------------------------------------------------------
import triangles
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 3
suite.addBenchmark( triangles.TrianglesBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', triangles.STRIP ) )
suite.addBenchmark( triangles.TrianglesBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', triangles.DISCRETE ) )

#----------------------------------------------------------------------
import elements
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 1, 1 ) )
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 1, 32 ) )
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 1, 64 ) )
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 10, 1 ) )
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 10, 32 ) )
suite.addBenchmark( elements.VboPointsBenchmark( 10, 10, 10, 64 ) )

suite.addBenchmark( elements.PointsBenchmark( 10, 10, 1, 1 ) )
suite.addBenchmark( elements.PointsBenchmark( 10, 10, 1, 32 ) )
suite.addBenchmark( elements.PointsBenchmark( 10, 10, 1, 64 ) )
suite.addBenchmark( elements.PointsBenchmark( 10, 10, 10, 1 ) )
suite.addBenchmark( elements.PointsBenchmark( 10, 10, 10, 32 ) )
suite.addBenchmark( elements.PointsBenchmark( 10, 10, 10, 64 ) )

suite.addBenchmark( elements.LinesBenchmark( 32, 1, 1, 1.0 ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 1, 32, 1.0 ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 1, 64, 1.0 ) )
suite.addBenchmark( elements.LinesBenchmark( 256, 1, 64, 0.01 ) )
suite.addBenchmark( elements.LinesBenchmark( 256, 1, 64, 1.0 ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 1, 1.0 ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 32, 1.0 ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 64, 1.0 ) )

suite.addBenchmark( elements.LinesBenchmark( 32, 1, 1, 0.5 ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 1, 32, 0.5 ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 1, 64, 0.5 ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 1, 0.5 ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 32, 0.5 ) )
suite.addBenchmark( elements.LinesBenchmark( 32, 10, 64, 0.5 ) )

suite.addBenchmark( elements.VboLinesBenchmark( 32, 1, 1, 1.0 ) )
suite.addBenchmark( elements.VboLinesBenchmark( 32, 1, 32, 1.0 ) )
suite.addBenchmark( elements.VboLinesBenchmark( 32, 1, 64, 1.0 ) )
suite.addBenchmark( elements.VboLinesBenchmark( 32, 10, 1, 1.0 ) )
suite.addBenchmark( elements.VboLinesBenchmark( 32, 10, 32, 1.0 ) )
suite.addBenchmark( elements.VboLinesBenchmark( 32, 10, 64, 1.0 ) )

suite.addBenchmark( elements.SlantedLinesBenchmark( 32, 1, 1 ) )
suite.addBenchmark( elements.SlantedLinesBenchmark( 32, 1, 32 ) )
suite.addBenchmark( elements.SlantedLinesBenchmark( 32, 1, 64 ) )

#----------------------------------------------------------------------
import texturemod
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 1

suite.addBenchmark( texturemod.GenTextureBenchmark() )

suite.addBenchmark( texturemod.SwizzleBenchmark( 128, 128, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 255, 255, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 256, 256, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 511, 511, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 512, 512, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1023, 1023, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1024, 1024, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 128, 128, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 256, 256, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 512, 512, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1024, 1024, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( texturemod.SwizzleBenchmark( 512, 512, 'OPENGLES2_LUMINANCE8' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1024, 1024, 'OPENGLES2_LUMINANCE8' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 512, 512, 'OPENGLES2_ALPHA8' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1024, 1024, 'OPENGLES2_ALPHA8' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 512, 512, 'OPENGLES2_LUMINANCE_ALPHA88' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1024, 1024, 'OPENGLES2_LUMINANCE_ALPHA88' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 512, 512, 'OPENGLES2_RGB888' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1024, 1024, 'OPENGLES2_RGB888' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 512, 512, 'OPENGLES2_RGBA4444' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1024, 1024, 'OPENGLES2_RGBA4444' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 512, 512, 'OPENGLES2_RGBA5551' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1024, 1024, 'OPENGLES2_RGBA5551' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 512, 512, 'OPENGLES2_BGRA8888' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1024, 1024, 'OPENGLES2_BGRA8888' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 512, 512, 'OPENGLES2_DEPTH' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1024, 1024, 'OPENGLES2_DEPTH' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 512, 512, 'OPENGLES2_DEPTH32' ) )
suite.addBenchmark( texturemod.SwizzleBenchmark( 1024, 1024, 'OPENGLES2_DEPTH32' ) )

suite.addBenchmark( texturemod.EglImageUploadBenchmark( 128, 128, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( texturemod.EglImageUploadBenchmark( 256, 256, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( texturemod.EglImageUploadBenchmark( 512, 512, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( texturemod.EglImageUploadBenchmark( 1024, 1024, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )

suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 512, 512, 'OPENGLES2_LUMINANCE8' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 1024, 1024, 'OPENGLES2_LUMINANCE8' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 512, 512, 'OPENGLES2_ALPHA8' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 1024, 1024, 'OPENGLES2_ALPHA8' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 512, 512, 'OPENGLES2_LUMINANCE_ALPHA88' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 1024, 1024, 'OPENGLES2_LUMINANCE_ALPHA88' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 256, 256, 'OPENGLES2_RGB888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 511, 511, 'OPENGLES2_RGB888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 512, 512, 'OPENGLES2_RGB888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 1024, 1024, 'OPENGLES2_RGB888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 256, 512, 'OPENGLES2_RGBA4444' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 512, 512, 'OPENGLES2_RGBA4444' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 1024, 1024, 'OPENGLES2_RGBA4444' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 512, 512, 'OPENGLES2_RGBA5551' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 1024, 1024, 'OPENGLES2_RGBA5551' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 256, 256, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 1, 0, 256, 256, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 1, 256, 256, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 1, 1, 256, 256, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 511, 511, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 512, 512, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 513, 513, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 1024, 1024, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 512, 512, 'OPENGLES2_BGRA8888' ) )
suite.addBenchmark( texturemod.SubtextureSwizzleBenchmark( 1024, 1024, 0, 0, 1024, 1024, 'OPENGLES2_BGRA8888' ) )

suite.addBenchmark( texturemod.UploadCompressedBenchmark( 128, 'IMAGES_ETC1_RGB8' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 256, 'IMAGES_ETC1_RGB8' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 512, 'IMAGES_ETC1_RGB8' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 1024, 'IMAGES_ETC1_RGB8' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 128, 'IMAGES_PVRTC_RGB4' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 256, 'IMAGES_PVRTC_RGB4' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 512, 'IMAGES_PVRTC_RGB4' ) )
suite.addBenchmark( texturemod.UploadCompressedBenchmark( 1024, 'IMAGES_PVRTC_RGB4' ) )

suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565', 0, 0, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565', 1, 0, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565', 1, 1, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565', 1, 2, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565', 1, 3, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565', 1, 4, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565', 2, 2, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565', 2, 3, 4  ) )
suite.addBenchmark( texturemod.UploadAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565', 2, 4, 4  ) )

#----------------------------------------------------------------------
import vertextype
GRIDX     = 20
GRIDY     = 32
OVERDRAW  = 3
suite.addBenchmark( vertextype.VertexTypeBenchmark( GRIDX, GRIDY, OVERDRAW, vertextype.FIXED ) )
suite.addBenchmark( vertextype.VertexTypeBenchmark( GRIDX, GRIDY, OVERDRAW, vertextype.FLOAT ) )

#----------------------------------------------------------------------
import blending
GRIDX     = 20
GRIDY     = 32
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGBA8888', 0, 4, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGBA8888', 1, 3, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGBA8888', 2, 2, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGBA8888', 3, 1, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGBA8888', 4, 0, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )

suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGBA8888', 4, 0, 'GL_ONE', 'GL_ZERO' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 4, 0, 'GL_ONE', 'GL_ZERO' ) )
suite.addBenchmark( blending.BlendBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 4, 0, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ) )

suite.addBenchmark( blending.BlendOverdrawBenchmark( 2, 2, 1, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( blending.BlendOverdrawBenchmark( 2, 2, 2, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( blending.BlendOverdrawBenchmark( 2, 2, 4, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( blending.BlendOverdrawBenchmark( 2, 2, 8, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( blending.BlendOverdrawBenchmark( 2, 2, 16, 'OPENGLES2_RGBA8888', True ) )

suite.addBenchmark( blending.BlendOverdrawBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( blending.BlendOverdrawBenchmark( GRIDX, GRIDY, 2, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( blending.BlendOverdrawBenchmark( GRIDX, GRIDY, 4, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( blending.BlendOverdrawBenchmark( GRIDX, GRIDY, 8, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( blending.BlendOverdrawBenchmark( GRIDX, GRIDY, 16, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( blending.BlendOverdrawBenchmark( GRIDX, GRIDY, 16, 'OPENGLES2_RGBA8888', False ) )

#----------------------------------------------------------------------
import scissor
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [ 0.00, 0.00, 1.00, 1.00 ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [ 0.00, 0.00, 0.90, 0.90 ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [ 0.10, 0.10, 0.90, 0.90 ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [ 0.00, 0.00, 1.00, 0.50 ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [ 0.00, 0.50, 1.00, 0.01 ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [ 0.50, 0.00, 0.01, 1.00 ] ) )
suite.addBenchmark( scissor.ScissorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [ 0.50, 0.50, 0.01, 0.01 ] ) )

suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [], 1, [] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [], 1, [ 0.00, 0.00, 1.00, 1.00 ] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [ 0.00, 0.00, 1.00, 1.00 ], 1, [] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [ 0.00, 0.00, 1.00, 1.00 ], 1, [ 0.00, 0.00, 1.00, 1.00 ] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [ 0.00, 0.00, 0.90, 0.90 ], 1, [ 0.10, 0.10, 0.90, 0.90 ] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [ 0.00, 0.50, 1.00, 0.50 ], 1, [ 0.00, 0.00, 1.00, 0.50 ] ) )
suite.addBenchmark( scissor.ScissorSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [ 0.00, 0.50, 1.00, 0.50 ], 1, [ 0.00, 0.00, 1.00, 0.60 ] ) )

#----------------------------------------------------------------------
import viewport
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( viewport.ViewportBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [] ) )
suite.addBenchmark( viewport.ViewportBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [ 0.00, 0.00, 1.00, 1.00 ] ) )
suite.addBenchmark( viewport.ViewportBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [ 0.10, 0.10, 0.80, 0.80 ] ) )
suite.addBenchmark( viewport.ViewportBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [ 0.00, 0.00, 0.50, 0.50 ] ) )
suite.addBenchmark( viewport.ViewportBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', [ 0.25, 0.25, 0.50, 0.50 ] ) )

suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [], 1, [] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [], 1, [ 0.00, 0.00, 1.00, 1.00 ] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [ 0.00, 0.00, 1.00, 1.00 ], 1, [] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [ 0.00, 0.00, 1.00, 1.00 ], 1, [ 0.00, 0.00, 1.00, 1.00 ] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [ 0.00, 0.00, 0.90, 0.90 ], 1, [ 0.10, 0.10, 0.90, 0.90 ] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [ 0.00, 0.50, 1.00, 0.50 ], 1, [ 0.00, 0.00, 1.00, 0.50 ] ) )
suite.addBenchmark( viewport.ViewportSwitchBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', 1, [ 0.00, 0.50, 1.00, 0.50 ], 1, [ 0.00, 0.00, 1.00, 0.60 ] ) )

#----------------------------------------------------------------------
import colormask
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( colormask.ColorMaskBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', True, True, True, True ) )
suite.addBenchmark( colormask.ColorMaskBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', True, False, False, False ) )
suite.addBenchmark( colormask.ColorMaskBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', True, True, False, False ) )
suite.addBenchmark( colormask.ColorMaskBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', True, True, True, False ) )

#----------------------------------------------------------------------
import copyteximage
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 256, 256, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 256, 512, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 512, 512, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 360, 640, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 360, 640, 'OPENGLES2_RGB888' ) )
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 360, 640, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 320, 480, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 320, 480, 'OPENGLES2_RGB888' ) )
suite.addBenchmark( copyteximage.CopyTexImageBenchmark( 320, 480, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES2_RGB565', 0, 0, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES2_RGB565', 1, 0, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES2_RGB565', 1, 1, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES2_RGB565', 1, 2, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES2_RGB565', 1, 3, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES2_RGB565', 1, 4, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES2_RGB565', 2, 2, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES2_RGB565', 2, 3, 4  ) )
suite.addBenchmark( copyteximage.CopyAndUseBenchmark( GRIDX, GRIDY, OVERDRAW, 256, 256, 'OPENGLES2_RGB565', 2, 4, 4  ) )

#----------------------------------------------------------------------
import bufferload
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( bufferload.BufferLoadBenchmark( 10 * 1024, 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW' ) )
suite.addBenchmark( bufferload.BufferLoadBenchmark( 100 * 1024, 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW' ) )
suite.addBenchmark( bufferload.BufferLoadBenchmark( 1000 * 1024, 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW' ) )
suite.addBenchmark( bufferload.BufferLoadBenchmark( 1000 * 1024, 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW' ) )
suite.addBenchmark( bufferload.BufferLoadBenchmark( 1000 * 1024, 'GL_ARRAY_BUFFER', 'GL_DYNAMIC_DRAW' ) )
suite.addBenchmark( bufferload.BufferLoadBenchmark( 1000 * 1024, 'GL_ARRAY_BUFFER', 'GL_STREAM_DRAW' ) )

suite.addBenchmark( bufferload.LoadBufferAndDrawBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', False ) )
suite.addBenchmark( bufferload.LoadBufferAndDrawBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', True ) )

#----------------------------------------------------------------------
import multisampling
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( multisampling.MultisamplingBenchmark( 1, 'GL_NEAREST', 'GL_NEAREST', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 4, 'GL_NEAREST', 'GL_NEAREST', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )

suite.addBenchmark( multisampling.MultisamplingBenchmark( 1, 'GL_LINEAR', 'GL_LINEAR', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 4, 'GL_LINEAR', 'GL_LINEAR', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )

suite.addBenchmark( multisampling.MultisamplingBenchmark( 1, 'GL_LINEAR', 'GL_LINEAR', GRIDX * 2, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 4, 'GL_LINEAR', 'GL_LINEAR', GRIDX * 2, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )

suite.addBenchmark( multisampling.MultisamplingBenchmark( 1, 'GL_LINEAR_MIPMAP_LINEAR', 'GL_LINEAR', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 4, 'GL_LINEAR_MIPMAP_LINEAR', 'GL_LINEAR', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ) )

suite.addBenchmark( multisampling.MultisamplingBenchmark( 1, 'GL_NEAREST', 'GL_NEAREST', 2, 2, 1, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 4, 'GL_NEAREST', 'GL_NEAREST', 2, 2, 1, 'OPENGLES2_RGB565' ) )

suite.addBenchmark( multisampling.MultisamplingBenchmark( 1, 'GL_LINEAR', 'GL_LINEAR', 2, 2, 1, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 4, 'GL_LINEAR', 'GL_LINEAR', 2, 2, 1, 'OPENGLES2_RGB565' ) )

suite.addBenchmark( multisampling.MultisamplingBenchmark( 1, 'GL_NEAREST', 'GL_NEAREST', 2, 2, 1, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 4, 'GL_NEAREST', 'GL_NEAREST', 2, 2, 1, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( multisampling.MultisamplingBenchmark( 1, 'GL_LINEAR', 'GL_LINEAR', 2, 2, 1, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( multisampling.MultisamplingBenchmark( 4, 'GL_LINEAR', 'GL_LINEAR', 2, 2, 1, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( multisampling.MultisamplingWithBlendedHslBenchmark( 1, 2, 2, 1, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( multisampling.MultisamplingWithBlendedHslBenchmark( 4, 2, 2, 1, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( multisampling.MultisampleFillrateBenchmark( 1, 32, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( multisampling.MultisampleFillrateBenchmark( 4, 32, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( multisampling.MultisampleFillrateBenchmark( 1, 32, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( multisampling.MultisampleFillrateBenchmark( 4, 32, 'OPENGLES2_RGBA8888' ) )

#----------------------------------------------------------------------
import stencil
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( stencil.StencilBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', False  ) )
suite.addBenchmark( stencil.StencilBenchmark( GRIDX, GRIDY, 'OPENGLES2_RGB565', True  ) )

#----------------------------------------------------------------------
import readpixels
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( readpixels.ReadPixelsBenchmark( 128, 128 ) )
suite.addBenchmark( readpixels.ReadPixelsBenchmark( 256, 256 ) )
suite.addBenchmark( readpixels.ReadPixelsBenchmark( 320, 480 ) )
suite.addBenchmark( readpixels.ReadPixelsBenchmark( 360, 640 ) )

suite.addBenchmark( readpixels.CopyBuffersWindowBenchmark( 128, 128 ), 'EGL_FULL' )
suite.addBenchmark( readpixels.CopyBuffersWindowBenchmark( 256, 256 ), 'EGL_FULL' )
suite.addBenchmark( readpixels.CopyBuffersWindowBenchmark( 320, 480 ), 'EGL_FULL' )
suite.addBenchmark( readpixels.CopyBuffersWindowBenchmark( 360, 640 ), 'EGL_FULL' )

suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 128, 128, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 256, 256, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 320, 480, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 360, 640, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 128, 128, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 256, 256, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 320, 480, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 360, 640, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 128, 128, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 256, 256, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 320, 480, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( readpixels.CopyBuffersPbufferBenchmark( 360, 640, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )

suite.addBenchmark( readpixels.DrawAndReadPixelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 320, 480, -1  ) )
suite.addBenchmark( readpixels.DrawAndReadPixelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 360, 640, -1  ) )
suite.addBenchmark( readpixels.DrawAndReadPixelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 320, 480, 0  ) )
suite.addBenchmark( readpixels.DrawAndReadPixelsBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 360, 640, 0  ) )

#----------------------------------------------------------------------
import mipmap
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 128, 128, 'OPENGLES2_RGB565', False ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 128, 128, 'OPENGLES2_RGB565', True ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 256, 256, 'OPENGLES2_RGB565', False ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 256, 256, 'OPENGLES2_RGB565', True ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 512, 512, 'OPENGLES2_RGB565', False ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 512, 512, 'OPENGLES2_RGB565', True ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 1024, 1024, 'OPENGLES2_RGB565', False ) )
suite.addBenchmark( mipmap.UploadAndGenerateMipmapBenchmark( 1024, 1024, 'OPENGLES2_RGB565', True ) )

suite.addBenchmark( mipmap.UploadGenerateMipmapAndDrawBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 512, 512, -1, False ) )
suite.addBenchmark( mipmap.UploadGenerateMipmapAndDrawBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 512, 512, 0, False ) )
suite.addBenchmark( mipmap.UploadGenerateMipmapAndDrawBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 512, 512, 0, True ) )

#----------------------------------------------------------------------
import polygonoffset
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 1
suite.addBenchmark( polygonoffset.PolygonOffsetBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', False ) )
suite.addBenchmark( polygonoffset.PolygonOffsetBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', True ) )

#----------------------------------------------------------------------
import shader
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( shader.SetupSimpleShaderBenchmark() )
suite.addBenchmark( shader.SetupMediumShaderBenchmark() )
suite.addBenchmark( shader.SwitchSimpleProgramBenchmark() )
suite.addBenchmark( shader.SwitchMediumProgramBenchmark() )

suite.addBenchmark( shader.RealisticProgramSwitchBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', -1 ) )
suite.addBenchmark( shader.RealisticProgramSwitchBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1 ) )

suite.addBenchmark( shader.ShaderCacheBenchmark( 1,   1,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 4,   4,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 8,   4,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 8,   8,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 16,  4,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 16, 16,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 32,  4,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 32, 32,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64,  1,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64,  1,  1, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64,  1,  2, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64,  1,  3, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64,  1,  4, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64,  1,  5, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64,  1,  6, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64,  1,  7, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64,  4,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64,  4,  1, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64,  4,  7, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64, 16,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64, 16,  1, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64, 16,  7, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64, 64,  0, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64, 64,  1, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64, 64,  2, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64, 64,  3, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64, 64,  4, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64, 64,  5, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64, 64,  6, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ShaderCacheBenchmark( 64, 64,  7, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( shader.ComplexShaderCacheBenchmark(  1,  1, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ComplexShaderCacheBenchmark(  4,  1, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ComplexShaderCacheBenchmark(  4,  4, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ComplexShaderCacheBenchmark( 16,  1, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ComplexShaderCacheBenchmark( 16, 16, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ComplexShaderCacheBenchmark( 64,  1, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ComplexShaderCacheBenchmark( 64,  8, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ComplexShaderCacheBenchmark( 64, 32, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( shader.ComplexShaderCacheBenchmark( 64, 64, 20, 32, 3, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( shader.ShaderBranchReferenceBenchmark( OVERDRAW ) )

suite.addBenchmark( shader.ShaderBranchBenchmark( 0.0, 'const', 1, OVERDRAW ) )
suite.addBenchmark( shader.ShaderBranchBenchmark( 0.1, 'const', 1, OVERDRAW ) )
suite.addBenchmark( shader.ShaderBranchBenchmark( 0.9, 'const', 1, OVERDRAW ) )

suite.addBenchmark( shader.ShaderBranchBenchmark( 0.0, 'const', 8, OVERDRAW ) )
suite.addBenchmark( shader.ShaderBranchBenchmark( 0.1, 'const', 8, OVERDRAW ) )
suite.addBenchmark( shader.ShaderBranchBenchmark( 0.9, 'const', 8, OVERDRAW ) )

suite.addBenchmark( shader.ShaderBranchBenchmark( 0.0, 'const', 16, OVERDRAW ) )
suite.addBenchmark( shader.ShaderBranchBenchmark( 0.1, 'const', 16, OVERDRAW ) )
suite.addBenchmark( shader.ShaderBranchBenchmark( 0.9, 'const', 16, OVERDRAW ) )

suite.addBenchmark( shader.ShaderBranchBenchmark( 0.0, 'uniform', 16, OVERDRAW ) )
suite.addBenchmark( shader.ShaderBranchBenchmark( 0.1, 'uniform', 16, OVERDRAW ) )
suite.addBenchmark( shader.ShaderBranchBenchmark( 0.9, 'uniform', 16, OVERDRAW ) )

suite.addBenchmark( shader.ShaderWithDiscardBranchBenchmark( 0.0, 'const', OVERDRAW ) )
suite.addBenchmark( shader.ShaderWithDiscardBranchBenchmark( 0.1, 'const', OVERDRAW ) )
suite.addBenchmark( shader.ShaderWithDiscardBranchBenchmark( 0.9, 'const', OVERDRAW ) )
suite.addBenchmark( shader.ShaderWithDiscardBranchBenchmark( 1.0, 'const', OVERDRAW ) )
suite.addBenchmark( shader.ShaderWithDiscardBranchBenchmark( 0.0, 'uniform', OVERDRAW ) )
suite.addBenchmark( shader.ShaderWithDiscardBranchBenchmark( 0.1, 'uniform', OVERDRAW ) )
suite.addBenchmark( shader.ShaderWithDiscardBranchBenchmark( 0.9, 'uniform', OVERDRAW ) )
suite.addBenchmark( shader.ShaderWithDiscardBranchBenchmark( 1.0, 'uniform', OVERDRAW ) )

#---------------------------------------------------------------------
import precision
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( precision.ShaderPrecisionBenchmark(  GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 'mediump' ) )
suite.addBenchmark( precision.ShaderPrecisionBenchmark(  GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 'highp' ) )

#----------------------------------------------------------------------
import buffers
GRIDX      = 20
GRIDY      = 32
OVERDRAW   = 3
suite.addBenchmark( buffers.RenderToWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 0.50, 0.50 ), 'EGL_FULL' )
suite.addBenchmark( buffers.RenderToWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 0.50, 1.00 ), 'EGL_FULL' )
suite.addBenchmark( buffers.RenderToWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50 ), 'EGL_FULL' )
suite.addBenchmark( buffers.RenderToWindowBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00 ), 'EGL_FULL' )

suite.addBenchmark( buffers.RenderToBufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50, 'GL_RGB565', buffers.READPIXELS ) )
suite.addBenchmark( buffers.RenderToBufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50, 'GL_RGB565', buffers.FINISH ) )
suite.addBenchmark( buffers.RenderToBufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'GL_RGB565', buffers.READPIXELS ) )
suite.addBenchmark( buffers.RenderToBufferBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'GL_RGB565', buffers.FINISH ) )

suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50, 'SCT_COLOR_FORMAT_RGB565', buffers.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50, 'SCT_COLOR_FORMAT_RGB565', buffers.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50, 'SCT_COLOR_FORMAT_RGB565', buffers.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50, 'SCT_COLOR_FORMAT_RGB888', buffers.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50, 'SCT_COLOR_FORMAT_RGB888', buffers.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50, 'SCT_COLOR_FORMAT_RGB888', buffers.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50, 'SCT_COLOR_FORMAT_RGBA8888', buffers.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50, 'SCT_COLOR_FORMAT_RGBA8888', buffers.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 0.50, 'SCT_COLOR_FORMAT_RGBA8888', buffers.FINISH ), 'EGL_LIMITED' )

suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'SCT_COLOR_FORMAT_RGB565', buffers.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'SCT_COLOR_FORMAT_RGB565', buffers.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'SCT_COLOR_FORMAT_RGB565', buffers.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'SCT_COLOR_FORMAT_RGB888', buffers.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'SCT_COLOR_FORMAT_RGB888', buffers.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'SCT_COLOR_FORMAT_RGB888', buffers.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'SCT_COLOR_FORMAT_RGBA8888', buffers.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'SCT_COLOR_FORMAT_RGBA8888', buffers.COPYBUFFERS ), 'EGL_LIMITED' )
suite.addBenchmark( buffers.RenderToPbufBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'SCT_COLOR_FORMAT_RGBA8888', buffers.FINISH ), 'EGL_LIMITED' )

suite.addBenchmark( buffers.RenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'OPENGLES2_RGB565', -1 ) )
suite.addBenchmark( buffers.RenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'OPENGLES2_RGB565', 0 ) )
suite.addBenchmark( buffers.RenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'OPENGLES2_RGB565', 1 ) )
suite.addBenchmark( buffers.RenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'OPENGLES2_RGB888', -1 ) )
suite.addBenchmark( buffers.RenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'OPENGLES2_RGB888', 0 ) )
suite.addBenchmark( buffers.RenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'OPENGLES2_RGB888', 1 ) )
suite.addBenchmark( buffers.RenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'OPENGLES2_RGBA8888', -1 ) )
suite.addBenchmark( buffers.RenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( buffers.RenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00, 1.00, 'OPENGLES2_RGBA8888', 1 ) )

suite.addBenchmark( buffers.RenderTextureThroughFboPipelineBenchmark( 'OPENGLES2_RGBA8888', 0, 0, False, False ) )
suite.addBenchmark( buffers.RenderTextureThroughFboPipelineBenchmark( 'OPENGLES2_RGBA8888', 1, 1, False, False ) )
suite.addBenchmark( buffers.RenderTextureThroughFboPipelineBenchmark( 'OPENGLES2_RGBA8888', 2, 1, False, False ) )
suite.addBenchmark( buffers.RenderTextureThroughFboPipelineBenchmark( 'OPENGLES2_RGBA8888', 3, 1, False, False ) )
suite.addBenchmark( buffers.RenderTextureThroughFboPipelineBenchmark( 'OPENGLES2_RGBA8888', 4, 1, False, False ) )
suite.addBenchmark( buffers.RenderTextureThroughFboPipelineBenchmark( 'OPENGLES2_RGBA8888', 2, 2, False, False ) )
suite.addBenchmark( buffers.RenderTextureThroughFboPipelineBenchmark( 'OPENGLES2_RGBA8888', 2, 2, True,  False ) )
suite.addBenchmark( buffers.RenderTextureThroughFboPipelineBenchmark( 'OPENGLES2_RGBA8888', 2, 2, False,  True ) )
suite.addBenchmark( buffers.RenderTextureThroughFboPipelineBenchmark( 'OPENGLES2_RGBA8888', 2, 2, True,   True ) )
suite.addBenchmark( buffers.RenderTextureThroughFboPipelineBenchmark( 'OPENGLES2_RGBA8888', 2, 4, False, False ) )

#----------------------------------------------------------------------
import light
GRID1    = ( 10, 16, )
GRID2    = ( 20, 32, )
OVERDRAW = 3
suite.addBenchmark( light.ReferenceBenchmark( GRID1[ 0 ], GRID1[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.ReferenceBenchmark( GRID2[ 0 ], GRID2[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.SimpleDiffuseBenchmark( GRID1[ 0 ], GRID1[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.SimpleDiffuseBenchmark( GRID2[ 0 ], GRID2[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.ComplexDiffuseBenchmark( GRID1[ 0 ], GRID1[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.ComplexDiffuseBenchmark( GRID2[ 0 ], GRID2[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.SimpleSpecularBlinnBenchmark( GRID1[ 0 ], GRID1[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.SimpleSpecularBlinnBenchmark( GRID2[ 0 ], GRID2[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.ComplexSpecularBlinnBenchmark( GRID1[ 0 ], GRID1[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.ComplexSpecularBlinnBenchmark( GRID2[ 0 ], GRID2[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.ComplexSpecularBlinnWithTextureBenchmark( GRID1[ 0 ], GRID1[ 1 ], OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( light.ComplexSpecularBlinnWithTextureBenchmark( GRID2[ 0 ], GRID2[ 1 ], OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( light.SimpleSpecularPhongBenchmark( GRID1[ 0 ], GRID1[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.SimpleSpecularPhongBenchmark( GRID2[ 0 ], GRID2[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.ComplexSpecularPhongBenchmark( GRID1[ 0 ], GRID1[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.ComplexSpecularPhongBenchmark( GRID2[ 0 ], GRID2[ 1 ], OVERDRAW ) )
suite.addBenchmark( light.ComplexSpecularPhongWithTextureBenchmark( GRID1[ 0 ], GRID1[ 1 ], OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( light.ComplexSpecularPhongWithTextureBenchmark( GRID2[ 0 ], GRID2[ 1 ], OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( light.PerPixelPhongBenchmark( GRID1[ 0 ], GRID1[ 1 ] , OVERDRAW ) )
suite.addBenchmark( light.PerPixelPhongBenchmark( GRID2[ 0 ], GRID2[ 1 ] , OVERDRAW ) )
suite.addBenchmark( light.PerPixelPhongWithTextureBenchmark( GRID1[ 0 ], GRID1[ 1 ], OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( light.PerPixelPhongWithTextureBenchmark( GRID2[ 0 ], GRID2[ 1 ], OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( light.SimpleTnLBenchmark( GRID1[ 0 ], GRID1[ 1 ], OVERDRAW, 'OPENGLES2_RGB565' ) )
suite.addBenchmark( light.SimpleTnLBenchmark( GRID2[ 0 ], GRID2[ 1 ], OVERDRAW, 'OPENGLES2_RGB565' ) )

#----------------------------------------------------------------------
import alphatest
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( alphatest.AlphaTestBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1.00 ) )
suite.addBenchmark( alphatest.AlphaTestBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 0.75 ) )
suite.addBenchmark( alphatest.AlphaTestBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 0.50 ) )
suite.addBenchmark( alphatest.AlphaTestBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 0.25 ) )
suite.addBenchmark( alphatest.AlphaTestBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 0.00 ) )

#----------------------------------------------------------------------
import game

suite.addBenchmark( game.GameBenchmark( 1, 0, 1 ) )
suite.addBenchmark( game.GameBenchmark( 1, 0, 4 ) )
suite.addBenchmark( game.GameBenchmark( 0, 1, 1 ) )
suite.addBenchmark( game.GameBenchmark( 0, 1, 4 ) )
suite.addBenchmark( game.GameBenchmark( 1, 1, 1 ) )
suite.addBenchmark( game.GameBenchmark( 1, 1, 4 ) )

#----------------------------------------------------------------------
import surfaces
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( surfaces.WindowSurfaceBenchmark( 'SCT_COLOR_FORMAT_DEFAULT', 'EGL_SINGLE_BUFFER', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( surfaces.WindowSurfaceBenchmark( 'SCT_COLOR_FORMAT_DEFAULT', 'EGL_BACK_BUFFER', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( surfaces.WindowSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB565', 'EGL_BACK_BUFFER', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( surfaces.WindowSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', 'EGL_BACK_BUFFER', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_FULL' )

suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', surfaces.READPIXELS, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', surfaces.COPYBUFFERS, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', surfaces.FINISH, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB565', surfaces.READPIXELS, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB565', surfaces.COPYBUFFERS, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB565', surfaces.FINISH, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB888', surfaces.READPIXELS, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB888', surfaces.COPYBUFFERS, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB888', surfaces.FINISH, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565',  ), 'EGL_LIMITED' )

suite.addBenchmark( surfaces.PixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565',  ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB565', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565',  ), 'EGL_LIMITED' )
suite.addBenchmark( surfaces.PixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB888', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )

#----------------------------------------------------------------------
import egl
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( egl.PrintEglInfo(), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreateContextBenchmark(), 'EGL_LIMITED' )
suite.addBenchmark( egl.DummyContextSwitchBenchmark( False, False, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.DummyContextSwitchBenchmark( True, False, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.DummyContextSwitchBenchmark( True, True, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreateWindowSurfaceBenchmark(), 'EGL_FULL' )
suite.addBenchmark( egl.CreatePbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreatePbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreatePbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreatePixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreatePixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreatePixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SwapBehaviorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 'EGL_BUFFER_DESTROYED' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SwapBehaviorBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 'EGL_BUFFER_PRESERVED' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SwapIntervalBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 0 ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SwapIntervalBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 1 ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SwapIntervalBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 2 ), 'EGL_LIMITED' )
suite.addBenchmark( egl.SwapIntervalBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 3 ), 'EGL_LIMITED' )

suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', [ 1, 1.00, 1.00 ], None, None ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', [ 1, 0.50, 0.50 ], None, None ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', [ 1, 1.00, 1.00 ], [ 1, 0.50, 0.50 ], [ 1, 0.00, 0.00, 1.00, 1.00 ] ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', [ 1, 1.00, 1.00 ], [ 1, 1.00, 1.00 ], [ 1, 0.25, 0.25, 0.50, 0.50 ] ), 'EGL_FULL' )
# 240*320 reference, upscaled
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', [ 0,  240,  320 ], None, None ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', [ 1, 1.00, 1.00 ], [ 0,  240,  320 ], [ 1, 0.00, 0.00, 1.00, 1.00 ] ), 'EGL_FULL' )
# 320*480 reference, upscaled
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', [ 0,  320,  480 ], None, None ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', [ 1, 1.00, 1.00 ], [ 0,  320,  480 ], [ 1, 0.00, 0.00, 1.00, 1.00 ] ), 'EGL_FULL' )
# Small up- and downscaling
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', [ 1, 1.00, 1.00 ], [ 1, 0.50, 0.50 ], [ 1, 0.00, 0.00, 0.51, 0.51 ] ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', [ 1, 1.00, 1.00 ], [ 1, 0.50, 0.50 ], [ 1, 0.00, 0.00, 0.49, 0.49 ] ), 'EGL_FULL' )
suite.addBenchmark( egl.SurfaceScalingBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', [ 1, 1.00, 1.00 ], [ 1, 0.50, 0.50 ], [ 1, 0.50, 0.50, 0.50, 0.50 ] ), 'EGL_FULL' )

suite.addBenchmark( egl.CreateEglImageBenchmark( 1024, 1024, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreateEglImageBenchmark( 1024, 1024, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreateEglImageBenchmark( 1024, 1024, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreateEglImageBenchmark( 1024, 1024, 'SCT_COLOR_FORMAT_YUV12' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.CreateEglImageBenchmark( 1280, 1280, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_LIMITED' )

suite.addBenchmark( egl.EglImageBenchmark( GRIDX, GRIDY, OVERDRAW, 'SCT_COLOR_FORMAT_RGB565', False ), 'EGL_LIMITED' )
suite.addBenchmark( egl.EglImageBenchmark( GRIDX, GRIDY, OVERDRAW, 'SCT_COLOR_FORMAT_RGB888', False ), 'EGL_LIMITED' )
suite.addBenchmark( egl.EglImageBenchmark( GRIDX, GRIDY, OVERDRAW, 'SCT_COLOR_FORMAT_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( egl.EglImageBenchmark( GRIDX, GRIDY, OVERDRAW, 'SCT_COLOR_FORMAT_RGB565', True ), 'EGL_LIMITED' )
suite.addBenchmark( egl.EglImageBenchmark( GRIDX, GRIDY, OVERDRAW, 'SCT_COLOR_FORMAT_RGB888', True ), 'EGL_LIMITED' )
suite.addBenchmark( egl.EglImageBenchmark( GRIDX, GRIDY, OVERDRAW, 'SCT_COLOR_FORMAT_RGBA8888', True ), 'EGL_LIMITED' )
suite.addBenchmark( egl.EglImageBenchmark( GRIDX, GRIDY, OVERDRAW, 'SCT_COLOR_FORMAT_YUV12', True ), 'EGL_LIMITED' )

suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGB565', False, True, 'direct blit' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGB565',  True, True, 'direct blit' ), 'EGL_LIMITED' )

suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False, False, 'direct blit' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True, False, 'direct blit' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False, False, 'rot90' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True, False, 'rot90' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False, False, 'rot180' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True, False, 'rot180' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False, False, 'rot270' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True, False, 'rot270' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False, False, 'hflip' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True, False, 'hflip' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False, False, 'vflip' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True, False, 'vflip' ), 'EGL_LIMITED' )

suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False,  True, 'direct blit' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True,  True, 'direct blit' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False,  True, 'rot90' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True,  True, 'rot90' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False,  True, 'rot180' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True,  True, 'rot180' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False,  True, 'rot270' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True,  True, 'rot270' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False,  True, 'hflip' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True,  True, 'hflip' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False,  True, 'vflip' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True,  True, 'vflip' ), 'EGL_LIMITED' )

suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', False,  True, 'no draw' ), 'EGL_LIMITED' )
suite.addBenchmark( egl.TextureStreamingBenchmark( 'SCT_COLOR_FORMAT_RGBA8888',  True,  True, 'no draw' ), 'EGL_LIMITED' )

suite.addBenchmark( egl.SingleThreadFenceBenchmark( False, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( egl.SingleThreadFenceBenchmark(  True, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888' ) )

#----------------------------------------------------------------------
import windows
suite.addBenchmark( windows.ResizeWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', [], [] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', [ 0.00, 0.00, 1.00, 1.00 ], [ 0.00, 0.00, 1.00, 1.00 ] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', [ 0.00, 0.00, 0.99, 0.99 ], [ 0.01, 0.01, 0.99, 0.99 ] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', [ 0.00, 0.00, 0.90, 0.90 ], [ 0.10, 0.10, 0.90, 0.90 ] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', [ 0.00, 0.00, 1.00, 0.50 ], [ 0.00, 0.00, 0.50, 1.00 ] ), 'EGL_FULL' )

suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 0, 0, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1, 1, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1, 2, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 2, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 3, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 3, 3, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 3, 4, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 4, 4, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 5, 5, 0, False ), 'EGL_FULL' )

suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1, 1, 0, True ), 'EGL_FULL' )

suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 0, 0, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1, 1, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1, 2, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 2, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 3, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 3, 3, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 3, 4, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 4, 4, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 5, 5, 1, False ), 'EGL_FULL' )

suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1, 1, 1, True ), 'EGL_FULL' )

suite.addBenchmark( windows.ChildWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1, 1, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.ChildWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1, 2, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.ChildWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1, 1, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.ChildWindowBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1, 2, 1 ), 'EGL_FULL' )

suite.addBenchmark( windows.PbuffersBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1, 1, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( windows.PbuffersBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 2, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( windows.PbuffersBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 3, 3, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( windows.PbuffersBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 4, 4, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_LIMITED' )
suite.addBenchmark( windows.PbuffersBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 4, 4, 'SCT_COLOR_FORMAT_RGB565', windows.READPIXELS ), 'EGL_LIMITED' )
suite.addBenchmark( windows.PbuffersBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 4, 4, 'SCT_COLOR_FORMAT_RGB565', windows.COPYBUFFERS ), 'EGL_LIMITED' )

suite.addBenchmark( windows.RotateScreenBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 'SCT_SCREEN_ORIENTATION_PORTRAIT', 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.RotateScreenBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 'SCT_SCREEN_ORIENTATION_LANDSCAPE', 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.RotateScreenBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 'SCT_SCREEN_ORIENTATION_PORTRAIT', 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.RotateScreenBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 'SCT_SCREEN_ORIENTATION_LANDSCAPE', 1 ), 'EGL_FULL' )

suite.addBenchmark( windows.WindowCompositionBenchmark( 1.0, False, 2, 2, 1, 'OPENGLES2_RGBA8888' ), 'EGL_FULL' )
suite.addBenchmark( windows.WindowCompositionBenchmark( 0.3, False, 2, 2, 1, 'OPENGLES2_RGBA8888' ), 'EGL_FULL' )
suite.addBenchmark( windows.WindowCompositionBenchmark( 1.0, True, 2, 2, 1, 'OPENGLES2_RGBA8888' ), 'EGL_FULL' )
suite.addBenchmark( windows.WindowCompositionBenchmark( 0.3, True, 2, 2, 1, 'OPENGLES2_RGBA8888' ), 'EGL_FULL' )

suite.addBenchmark( windows.WindowFormatBenchmark( 'SCT_COLOR_FORMAT_DEFAULT', 2, 2, 1, 'OPENGLES2_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( windows.WindowFormatBenchmark( 'SCT_COLOR_FORMAT_RGB565', 2, 2, 1, 'OPENGLES2_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( windows.WindowFormatBenchmark( 'SCT_COLOR_FORMAT_RGB888', 2, 2, 1, 'OPENGLES2_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( windows.WindowFormatBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', 2, 2, 1, 'OPENGLES2_RGB565' ), 'EGL_FULL' )

#----------------------------------------------------------------------
import ui

suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.PANELS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.GROUPS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.ICONS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], False, 'OPENGLES2_RGBA8888', 0 ) )

suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.PANELS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.GROUPS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.ICONS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], False, 'OPENGLES2_RGBA8888', 0 ) )

suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.PANELS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.GROUPS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.ICONS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], False, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], False, 'OPENGLES2_RGBA8888', 0 ) )

suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.PANELS ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.GROUPS ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.ICONS ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], False, 'OPENGLES2_RGBA8888', 1 ) )

suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.PANELS ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.GROUPS ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.ICONS ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], False, 'OPENGLES2_RGBA8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], False, 'OPENGLES2_RGBA8888', 1 ) )

suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.PANELS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.GROUPS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.ICONS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], True, 'OPENGLES2_RGBA8888', 0 ) )

suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.PANELS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.GROUPS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.ICONS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], True, 'OPENGLES2_RGBA8888', 0 ) )

suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.PANELS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.GROUPS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.ICONS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS  ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS  ], True, 'OPENGLES2_RGBA8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS  ], True, 'OPENGLES2_RGBA8888', 0 ) )

matrices = [ 75, 100, 150, 200, 250, 300 ]
suite.addBenchmark( ui.ParallelUiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], False, 'OPENGLES2_RGBA8888', 0 ) )
for m in matrices:
    suite.addBenchmark( ui.ParallelUiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], False, 'OPENGLES2_RGBA8888', m ) )

#----------------------------------------------------------------------
import special
suite.addBenchmark( special.DependentTextureBenchmark( 'OPENGLES2_RGB565' ) )
suite.addBenchmark( special.DependentTextureBenchmark( 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( special.ParticlesBenchmark( 20, 32, 32, 16, 'OPENGLES2_RGB565', False ) )
suite.addBenchmark( special.ParticlesBenchmark( 20, 32, 32, 16, 'OPENGLES2_RGB565', True ) )
suite.addBenchmark( special.ParticlesBenchmark( 20, 32, 32, 16, 'OPENGLES2_RGBA8888', False ) )
suite.addBenchmark( special.ParticlesBenchmark( 20, 32, 32, 16, 'OPENGLES2_RGBA8888', True ) )
suite.addBenchmark( special.SkyboxBenchmark( 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( special.MediaWallSkyboxBenchmark( 'OPENGLES2_RGBA8888' ) )

#----------------------------------------------------------------------
import screen
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( screen.ScreenBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 'SCT_SCREEN_DEFAULT' ), 'EGL_FULL' )
suite.addBenchmark( screen.ScreenBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 'SCT_SCREEN_HDMI' ), 'EGL_FULL' )

#----------------------------------------------------------------------
import threads
GRIDX    = 20
GRIDY    = 32
OVERDRAW = 3
suite.addBenchmark( threads.ParallelWindowsBenchmark( 1, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( threads.ParallelWindowsBenchmark( 2, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( threads.ParallelWindowsBenchmark( 3, GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_FULL' )

suite.addBenchmark( threads.SwapReferenceBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( threads.CreateWindowAndSwapFromMainDrawFromWorkerBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( threads.DrawFromMainCreateWindowAndSwapFromWorkerBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( threads.CreateBufferAndDrawMainReadWorkerBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_FULL' )

suite.addBenchmark( threads.TextureUploadReferenceBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedTextureUploadBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedSharedTextureUploadBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )

suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 32, 32, 'OPENGLES2_RGBA8888', 1, 1, False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 64, 64, 'OPENGLES2_RGBA8888', 1, 1, False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 128, 128, 'OPENGLES2_RGBA8888', 1, 1, False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 256, 256, 'OPENGLES2_RGBA8888', 1, 1, False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 512, 512, 'OPENGLES2_RGBA8888', 1, 1, False ), 'EGL_LIMITED' )

suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1024, 1024, 'OPENGLES2_RGBA8888', 1, 1, False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1024, 1024, 'OPENGLES2_RGBA8888', 4, 4, False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1024, 1024, 'OPENGLES2_RGBA8888', 8, 8, False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 1024, 1024, 'OPENGLES2_RGBA8888', 16, 16, False ), 'EGL_LIMITED' )

suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', 32, 32, 'OPENGLES2_RGBA8888', 1, 1, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', 64, 64, 'OPENGLES2_RGBA8888', 1, 1, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', 128, 128, 'OPENGLES2_RGBA8888', 1, 1, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', 256, 256, 'OPENGLES2_RGBA8888', 1, 1, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', 512, 512, 'OPENGLES2_RGBA8888', 1, 1, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', 1024, 1024, 'OPENGLES2_RGBA8888', 1, 1, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', 1024, 1024, 'OPENGLES2_RGBA8888', 4, 4, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', 1024, 1024, 'OPENGLES2_RGBA8888', 8, 8, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGB565', 1024, 1024, 'OPENGLES2_RGBA8888', 16, 16, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 2, 'OPENGLES2_RGB565', 1024, 1024, 'OPENGLES2_RGBA8888', 16, 16, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 4, 'OPENGLES2_RGB565', 1024, 1024, 'OPENGLES2_RGBA8888', 16, 16, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( GRIDX, GRIDY, 6, 'OPENGLES2_RGB565', 1024, 1024, 'OPENGLES2_RGBA8888', 16, 16, True ), 'EGL_LIMITED' )

suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 512, 512, 'OPENGLES2_RGBA8888', 16, 16, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 2, 'OPENGLES2_RGB565', 512, 512, 'OPENGLES2_RGBA8888', 16, 16, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 4, 'OPENGLES2_RGB565', 512, 512, 'OPENGLES2_RGBA8888', 16, 16, True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncSharedTextureUploadBenchmark( 2, 2, 6, 'OPENGLES2_RGB565', 512, 512, 'OPENGLES2_RGBA8888', 16, 16, True ), 'EGL_LIMITED' )

suite.addBenchmark( threads.RenderToTextureReferenceBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedRenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedRenderToSharedTextureBenchmark( 2, 2, 1, 512, 512, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedRenderToSharedTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 512, 512, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )

suite.addBenchmark( threads.ThreadedSharedVBODataUploadBenchmark( GRIDX, GRIDY, 1, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedSharedProgramBenchmark( 2, 2, 1, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedSharedShaderBenchmark( 2, 2, 1, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedSharedRenderbufferBenchmark( 2, 2, 1, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )

suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 2, 1, 'OPENGLES2_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 2, 2, 'OPENGLES2_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 2, 4, 'OPENGLES2_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 2, 6, 'OPENGLES2_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 2, 1, 'OPENGLES2_RGBA8888', True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 2, 2, 'OPENGLES2_RGBA8888', True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 2, 4, 'OPENGLES2_RGBA8888', True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 1, 'OPENGLES2_RGB565', 2, 2, 6, 'OPENGLES2_RGBA8888', True ), 'EGL_LIMITED' )

suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 2, 'OPENGLES2_RGB565', 2, 2, 1, 'OPENGLES2_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 4, 'OPENGLES2_RGB565', 2, 2, 1, 'OPENGLES2_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 6, 'OPENGLES2_RGB565', 2, 2, 1, 'OPENGLES2_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 2, 'OPENGLES2_RGB565', 2, 2, 1, 'OPENGLES2_RGBA8888', True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 4, 'OPENGLES2_RGB565', 2, 2, 1, 'OPENGLES2_RGBA8888', True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( 2, 2, 6, 'OPENGLES2_RGB565', 2, 2, 1, 'OPENGLES2_RGBA8888', True ), 'EGL_LIMITED' )

suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 2, 2, 1, 'OPENGLES2_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 2, 2, 2, 'OPENGLES2_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 2, 2, 4, 'OPENGLES2_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 2, 2, 6, 'OPENGLES2_RGBA8888', False ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 2, 2, 1, 'OPENGLES2_RGBA8888', True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 2, 2, 2, 'OPENGLES2_RGBA8888', True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 2, 2, 4, 'OPENGLES2_RGBA8888', True ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ThreadedAsyncRenderToTextureBenchmark( GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565', 2, 2, 6, 'OPENGLES2_RGBA8888', True ), 'EGL_LIMITED' )

suite.addBenchmark( threads.UiRender60FPSBenchmark( 'TEXTURE-TEXTURE', 'OPENGLES2_RGB565', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.UiRender60FPSBenchmark( 'TEXTURE-EGLIMAGE-TEXTURE', 'OPENGLES2_RGB565', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.UiRender60FPSBenchmark( 'RBO-EGLIMAGE-TEXTURE', 'OPENGLES2_RGB565', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.UiRender60FPSBenchmark( 'SHAREDPIXMAP-EGLIMAGE-TEXTURE', 'OPENGLES2_RGB565', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.UiRender60FPSBenchmark( 'SHAREDPIXMAP-EGLIMAGE-TEXTURE-TEXTURE', 'OPENGLES2_RGB565', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.UiRender60FPSBenchmark( 'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE', 'OPENGLES2_RGB565', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGB565' ), 'EGL_LIMITED' )

suite.addBenchmark( threads.UiRender60FPSBenchmark( 'TEXTURE-TEXTURE', 'OPENGLES2_RGBA8888', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.UiRender60FPSBenchmark( 'TEXTURE-EGLIMAGE-TEXTURE', 'OPENGLES2_RGBA8888', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.UiRender60FPSBenchmark( 'RBO-EGLIMAGE-TEXTURE', 'OPENGLES2_RGBA8888', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.UiRender60FPSBenchmark( 'SHAREDPIXMAP-EGLIMAGE-TEXTURE', 'OPENGLES2_RGBA8888', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.UiRender60FPSBenchmark( 'SHAREDPIXMAP-EGLIMAGE-TEXTURE-TEXTURE', 'OPENGLES2_RGBA8888', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.UiRender60FPSBenchmark( 'SHAREDPIXMAP-EGLIMAGE-RBO-TEXTURE', 'OPENGLES2_RGBA8888', GRIDX, GRIDY, OVERDRAW, 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )

suite.addBenchmark( threads.MultiThreadFenceBenchmark( 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )
suite.addBenchmark( threads.ComplexMultiThreadFenceBenchmark( 'OPENGLES2_RGBA8888' ), 'EGL_LIMITED' )

#----------------------------------------------------------------------
import resources
suite.addBenchmark( resources.PrintResourceProfile( 'end' ), 'EGL_FULL' )

#----------------------------------------------------------------------
command()
