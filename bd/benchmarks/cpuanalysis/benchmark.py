#
# Spandex benchmark and test framework.
#
# Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../..' )

import  bd
import  common

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'Spandex cpu analysis'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

#----------------------------------------------------------------------
( width, height, )      = target.getScreenSize()

import opengles2

bg         = ( common.BACKGROUND, 'RGB8'  )
panels     = ( common.PANELS,     'RGBA8' )
groups     = ( common.GROUPS,     'RGBA8' )
icons      = ( common.ICONS,      'RGBA8' )
overlay    = ( common.OVERLAY,    'RGBA8' )


# # UI benchmark
res = ( width,  height,  )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg ],                        0, '', '' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels ],                0, '', '' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 0, '', '' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg ],                        16, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg ],                        18, 'adaptive', 'before' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels ],                16, 'adaptive', 'before' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 12, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 13, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 14, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 15, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 16, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 17, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 18, 'adaptive', 'before' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg ],                        16, 'adaptive', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels ],                16, 'adaptive', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 16, 'adaptive', 'after' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg ],                        16, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg ],                        18, 'fixed', 'after' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels ],                16, 'fixed', 'after' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 12, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 13, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 14, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 15, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 16, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 17, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.DRAW,            [ bg, panels, groups, icons ], 18, 'fixed', 'after' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        0, '', '' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg, panels ],                0, '', '' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg, panels, groups, icons ], 0, '', '' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        10, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        11, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        12, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        13, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        14, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        15, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        16, 'adaptive', 'before' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg, panels ],                16, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg, panels, groups, icons ], 16, 'adaptive', 'before' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        16, 'adaptive', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg, panels ],                16, 'adaptive', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg, panels, groups, icons ], 16, 'adaptive', 'after' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        10, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        11, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        12, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        13, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        14, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        15, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg ],                        16, 'fixed', 'after' ) )

suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg, panels ],                16, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisUiBenchmark( res, common.UPLOAD_AND_DRAW, [ bg, panels, groups, icons ], 16, 'fixed', 'after' ) )

# Blur effect
res = ( 240,  240,  )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 0, '', '' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 2, 0, '', '' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 4, 0, '', '' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 8, 0, '', '' ) )

suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 10, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 11, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 12, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 13, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 14, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 15, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 16, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 17, 'adaptive', 'before' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 18, 'adaptive', 'before' ) )

suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 10, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 11, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 12, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 13, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 14, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 15, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 16, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 17, 'fixed', 'after' ) )
suite.addBenchmark( opengles2.AnalysisBlurBenchmark( res[ 0 ], res[ 1 ], 1, 18, 'fixed', 'after' ) )


#----------------------------------------------------------------------

command()
