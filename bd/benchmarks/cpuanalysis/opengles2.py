#
# Spandex benchmark and test framework.
#
# Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

import numpy

REPEATS     = 120
START_FRAME = 0

IMAGES_TYPES  = { 'FLOWER' : 'IMAGES_FLOWER'
                  }

PATTERN_TYPES = { 'SOLID'    : '',
                  'CHECKER'  : '',
                  'GRADIENT' : ''
                  }

NOISE_TYPES = { 'RANDOM'  : 'OPENGLES2_NOISE_RANDOM',
                'PERLIN1' : 'OPENGLES2_NOISE_ORIGINAL_PERLIN',
                'PERLIN2' : 'OPENGLES2_NOISE_LOW_QUALITY_PERLIN',
                'PERLIN3' : 'OPENGLES2_NOISE_HIGH_QUALITY_PERLIN'
                }

IMAGES_FORMATS = { 'RGB565'      : 'IMAGES_RGB565',
                   'RGB8'        : 'IMAGES_RGB8',
                   'RGBA8'       : 'IMAGES_RGBA8',
                   'ETC1_RGB8'   : 'IMAGES_ETC1_RGB8',
                   'ETC1_RGB8_A' : 'IMAGES_ETC1_RGB8',
                   'PVRTC_RGB4'  : 'IMAGES_PVRTC_RGB4',
                   'PVRTC_RGB2'  : 'IMAGES_PVRTC_RGB2',
                   'PVRTC_RGBA4' : 'IMAGES_PVRTC_RGBA4',
                   'PVRTC_RGBA2' : 'IMAGES_PVRTC_RGBA2'
                   }

FORMATS = { 'RGB565' : 'OPENGLES2_RGB565',
            'RGB8'   : 'OPENGLES2_RGB888',
            'RGBA8'  : 'OPENGLES2_RGBA8888'
            }

UNCOMPRESSED_FORMATS_MAP = { 'RGB565' : 'OPENGLES2_RGB565',
                             'RGB8'   : 'OPENGLES2_RGB888',
                             'RGBA8'  : 'OPENGLES2_RGBA8888'
                             }

COMPRESSED_FORMATS_MAP = { 'ETC1_RGB8'   : 'GL_ETC1_RGB8_OES',
                           'PVRTC_RGB4'  : 'GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG',
                           'PVRTC_RGB2'  : 'GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG',
                           'PVRTC_RGBA4' : 'GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG',
                           'PVRTC_RGBA2' : 'GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG'
                           }
        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class AnalysisUiBenchmark( Benchmark ):
    def __init__( self, fboSize, action, layers, busyTime, busyType, busyLocation ):
        Benchmark.__init__( self )
        self.log                    = True
        self.repeats                = REPEATS
        self.fboSize                = fboSize
        self.action                 = action
        self.layers                 = layers
        self.busyTime               = busyTime      # millis
        self.busyType               = busyType      # adaptive|fixed
        self.busyLocation           = busyLocation  # before|after
        self.logMessageStartFrame   = START_FRAME
        self.logTotal               = True        
        self.reuseTextures          = False
        self.randomTextures         = False
        reuseString = ''
        if self.reuseTextures:
            reuseString = 'reused'
        randomString = ''
        if self.randomTextures:
            randomString = 'random'
            if reuseString:
                reuseString += ' '

        textureString = ''
        if reuseString or randomString:
            textureString = ' with %s textures' % ( reuseString + randomString, )

        bs = 'no load '
        if self.busyTime > 0:
            bs = '%d ms %s load %s gfx issue ' % ( self.busyTime, self.busyType, self.busyLocation, )
            
        self.name = "OPENGLES2 ui %s%s %s%s" % ( bs,
                                                 actionToString( self.fboSize, self.action, self.layers ),
                                                 layersToString( self.layers ),
                                                 textureString, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # Shader source codes
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec4 position_in;
        attribute vec2 uv_in;
        uniform mat4 mvp;
        varying vec2 uv;

        void main()
        {
            gl_Position = mvp * position_in;
            uv = uv_in;
        }"""
        
        rawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D tex0;
        varying vec2 uv;

        void main()
        {
            gl_FragColor = texture2D(tex0, uv);
        }"""

        vertexShaderSource   = formatShader( rawVertexShaderSource )
        fragmentShaderSource = formatShader( rawFragmentShaderSource )
        
        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
       
        state = setupOpenGLES2( modules, indexTracker, target )            

        if not self.fboSize:
            targetWidth  = screenWidth
            targetHeight = screenWidth
        else:
            targetWidth  = self.fboSize[ 0 ]
            targetHeight = self.fboSize[ 1 ]
        
        backgroundWidth  = targetWidth
        backgroundHeight = targetHeight
        panelsX          = PANELSX
        panelsY          = PANELSY
        panelWidth       = targetWidth / panelsX
        panelHeight      = targetHeight / panelsY
        groupsX          = GROUPSX
        groupsY          = GROUPSY
        groupWidth       = panelWidth / groupsX
        groupHeight      = panelHeight / groupsY
        iconsX           = ICONSX
        iconsY           = ICONSY
        iconWidth        = panelWidth / iconsX
        iconHeight       = panelHeight / iconsY
        overlayWidth     = targetWidth
        overlayHeight    = targetHeight

        backgroundTextureWidth,     backgroundTextureS  = toPot( backgroundWidth, False )
        backgroundTextureHeight,    backgroundTextureT  = toPot( backgroundHeight, False )
        panelTextureWidth,          panelTextureS       = toPot( panelWidth, False )
        panelTextureHeight,         panelTextureT       = toPot( panelHeight, False )
        groupTextureWidth,          groupTextureS       = toPot( groupWidth, False )
        groupTextureHeight,         groupTextureT       = toPot( groupHeight, False )
        iconTextureWidth,           iconTextureS        = toPot( iconWidth, False )
        iconTextureHeight,          iconTextureT        = toPot( iconHeight, False )
        overlayTextureWidth,        overlayTextureS     = toPot( overlayWidth, False )
        overlayTextureHeight,       overlayTextureT     = toPot( overlayHeight, False )

        # Create texture data.
        if hasLayer( OVERLAY, self.layers ):
            overlays             = 1
            overlayTextureFormat = layerTextureFormat( OVERLAY, self.layers )
        elif hasLayer( OVERLAY4X, self.layers ):
            overlays             = 4
            overlayTextureFormat = layerTextureFormat( OVERLAY4X, self.layers )
            
        if hasLayer( BACKGROUND, self.layers ):
            backgroundTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            backgroundTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            
            textureFormat = layerTextureFormat( BACKGROUND, self.layers )            

            if not self.randomTextures:
                imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
                Images.CreateData( imagesDataIndex,
                                   'IMAGES_FLOWER',
                                   IMAGES_FORMATS[ textureFormat ],
                                   backgroundTextureWidth,
                                   backgroundTextureHeight )       
            
                dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )
                
                OpenGLES2.CreateTextureData( backgroundTextureDataIndex,
                                             dataIndex,
                                             backgroundTextureWidth,
                                             backgroundTextureHeight,
                                             FORMATS[ textureFormat ] )
                
            else:
                OpenGLES2.CreateNoiseTextureData( backgroundTextureDataIndex,
                                                  'OPENGLES2_NOISE_RANDOM',
                                                  [1.0,1.0,1.0,1.0,1,1],
                                                  backgroundTextureWidth,
                                                  backgroundTextureHeight,
                                                  'OFF',
                                                  FORMATS[ textureFormat ] )
                            
            OpenGLES2.GenTexture( backgroundTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
            
            if self.action == DRAW:
                OpenGLES2.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )
            
        if hasLayer( PANELS, self.layers ):
            panelTextureDataIndices = []
            panelTextureIndices     = []
            panelOrigins            = []

            textureFormat = layerTextureFormat( PANELS, self.layers )
            
            total = panelsX * panelsY + 1
            for y in range( panelsY ):
                for x in range( panelsX ):
                    textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                    textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                    
                    panelTextureDataIndices += [ textureDataIndex ]
                    panelTextureIndices     += [ textureIndex ]
                    panelOrigins            += [ ( panelWidth * x, panelHeight * y, ) ]

                    r = 1.0 * ( ( x + ( y * panelsX ) + 1 ) / float( total ) )

                    if self.randomTextures:
                        OpenGLES2.CreateNoiseTextureData( textureDataIndex,
                                                          'OPENGLES2_NOISE_RANDOM',
                                                          [1.0,1.0,1.0,1.0,1,2],
                                                          panelTextureWidth,
                                                          panelTextureHeight,
                                                          'OFF',
                                                          FORMATS[ textureFormat ] )
                    else:
                        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                             'OPENGLES2_GRADIENT_HORIZONTAL',
                                                             [ 0.0, r, 0.0, 0.5 ],
                                                             [ 0.2, r, 0.2, 0.5 ],
                                                             panelTextureWidth,
                                                             panelTextureHeight,
                                                             'OFF',
                                                             FORMATS[ textureFormat ] )

                    OpenGLES2.GenTexture( textureIndex )
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                    OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                    
                    if self.action == DRAW:
                        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                   
        if hasLayer( GROUPS, self.layers ):
            groupTextureDataIndices = []
            groupTextureIndices     = []
            groupOrigins            = []

            textureFormat = layerTextureFormat( GROUPS, self.layers )
            
            total = groupsX * groupsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( groupsY ):
                        for x in range( groupsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                    
                            groupTextureDataIndices += [ textureDataIndex ]
                            groupTextureIndices     += [ textureIndex ]
                            groupOrigins            += [ ( px * panelWidth + groupWidth * x,
                                                           py * panelHeight + groupHeight * y, ) ]

                            r = 1.0 * ( x + ( y * groupsX ) + 1 ) / float( total )

                            if self.randomTextures:
                                OpenGLES2.CreateNoiseTextureData( textureDataIndex,
                                                                  'OPENGLES2_NOISE_RANDOM',
                                                                  [1.0,1.0,1.0,1.0,1,3],
                                                                  groupTextureWidth,
                                                                  groupTextureHeight,
                                                                  'OFF',
                                                                  FORMATS[ textureFormat ] )
                            else:
                                OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                                                     'OPENGLES2_GRADIENT_HORIZONTAL',
                                                                     [ 0.0, 0.0, r, 0.5 ],
                                                                     [ 0.2, 0.2, r, 0.5 ],
                                                                     groupTextureWidth,
                                                                     groupTextureHeight,
                                                                     'OFF',
                                                                     FORMATS[ textureFormat ] )
                            
                            OpenGLES2.GenTexture( textureIndex )
                            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                            
                            if self.action == DRAW:
                                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                                               
        if hasLayer( ICONS, self.layers ):
            iconTextureDataIndices  = []
            iconTextureIndices      = []
            iconOrigins             = []

            textureFormat = layerTextureFormat( ICONS, self.layers )
            
            total = iconsX * iconsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( iconsY ):
                        for x in range( iconsX ):
                            textureDataIndex    = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                            textureIndex        = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                    
                            iconTextureDataIndices  += [ textureDataIndex ]
                            iconTextureIndices      += [ textureIndex ]
                            iconOrigins             += [ ( px * panelWidth + iconWidth * x,
                                                           py * panelHeight + iconHeight * y, ) ]

                            r = 1.0 * ( x + ( y * iconsX ) + 1 ) / float( total )

                            if self.randomTextures:
                                OpenGLES2.CreateNoiseTextureData( textureDataIndex,
                                                                  'OPENGLES2_NOISE_RANDOM',
                                                                  [1.0,1.0,1.0,1.0,1,4],
                                                                  iconTextureWidth,
                                                                  iconTextureHeight,
                                                                  'OFF',
                                                                  FORMATS[ textureFormat ] )
                            else:
                                OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                                    [ 0.3, 0.3, r, 0.5 ],
                                                                    [ 0.2, 0.2, r, 0.5 ],
                                                                    2, 2, 
                                                                    iconTextureWidth,
                                                                    iconTextureHeight,
                                                                    'OFF',
                                                                    FORMATS[ textureFormat ] )
                                                        
                            OpenGLES2.GenTexture( textureIndex )
                            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                            
                            if self.action == DRAW:
                                OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        if hasLayer( OVERLAY, self.layers ) or hasLayer( OVERLAY4X, self.layers ):
            overlayTextureIndices = []
            for i in range( overlays ):
                overlayTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                overlayTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            
                if not self.randomTextures:
                    OpenGLES2.CreateCheckerTextureData( overlayTextureDataIndex,
                                                        [ 0.7, 0.3, 0.0, 0.3 ],
                                                        [ 0.7, 0.2, 0.0, 0.7 ],
                                                        2, 2, 
                                                        overlayTextureWidth,
                                                        overlayTextureHeight,
                                                        'OFF',
                                                        FORMATS[ overlayTextureFormat ] )
                
                else:
                    OpenGLES2.CreateNoiseTextureData( overlayTextureDataIndex,
                                                      'OPENGLES2_NOISE_RANDOM',
                                                      [1.0,1.0,1.0,1.0,1,1],
                                                      overlayTextureWidth,
                                                      overlayTextureHeight,
                                                      'OFF',
                                                      FORMATS[ overlayTextureFormat ] )
                            
                OpenGLES2.GenTexture( overlayTextureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', overlayTextureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
            
                if self.action == DRAW:
                    OpenGLES2.TexImage2D( overlayTextureDataIndex, 'GL_TEXTURE_2D' )

                overlayTextureIndices.append( overlayTextureIndex )
                                
        # Create geometry.
        if hasLayer( BACKGROUND, self.layers ):
            backgroundTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                                   2,
                                                                   [ backgroundTextureS,
                                                                     backgroundTextureT ] ) ]
        
            background = MeshStripPlane( 2,
                                         2,
                                         MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                         None,
                                         None,
                                         backgroundTexCoordAttribute,
                                         MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                         None,
                                         MESH_CCW_WINDING,
                                         False )

            background.translate( [ -0.5, -0.5, 0 ] )
            background.scale( [ 2.0 * backgroundWidth / float( targetWidth ),
                                2.0 * backgroundHeight / float( targetHeight ) ] )
                   
            backgroundVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundVertexArrayIndex, background.vertexGLType )
            OpenGLES2.AppendToArray( backgroundVertexArrayIndex, background.vertices )
            
            backgroundTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundTexCoordArrayIndex, background.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( backgroundTexCoordArrayIndex, background.texCoords[ 0 ] )
            
            backgroundIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( backgroundIndexArrayIndex, background.indexGLType );
            OpenGLES2.AppendToArray( backgroundIndexArrayIndex, background.indices )

        if hasLayer( PANELS, self.layers ):
            panelTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ panelTextureS,
                                                                panelTextureT ] ) ]
        
            panel = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    panelTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            panel.translate( [ -0.5, -0.5, 0 ] )
            panel.scale( [ 2.0 * panelWidth / float( targetWidth ),
                           2.0 * panelHeight / float( targetHeight ) ] )
           
            panelVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelVertexArrayIndex, panel.vertexGLType )
            OpenGLES2.AppendToArray( panelVertexArrayIndex, panel.vertices )
            
            panelTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelTexCoordArrayIndex, panel.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( panelTexCoordArrayIndex, panel.texCoords[ 0 ] )
            
            panelIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( panelIndexArrayIndex, panel.indexGLType );
            OpenGLES2.AppendToArray( panelIndexArrayIndex, panel.indices )

        if hasLayer( GROUPS, self.layers ):
            groupTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                              2,
                                                              [ groupTextureS,
                                                                groupTextureT ] ) ]
        
            group = MeshStripPlane( 2,
                                    2,
                                    MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                    None,
                                    None,
                                    groupTexCoordAttribute,
                                    MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                    None,
                                    MESH_CCW_WINDING,
                                    False )

            group.translate( [ -0.5, -0.5, 0 ] )
            group.scale( [ 2.0 * groupWidth / float( targetWidth ),
                           2.0 * groupHeight / float( targetHeight ) ] )
           
            groupVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupVertexArrayIndex, group.vertexGLType )
            OpenGLES2.AppendToArray( groupVertexArrayIndex, group.vertices )
            
            groupTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupTexCoordArrayIndex, group.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( groupTexCoordArrayIndex, group.texCoords[ 0 ] )
            
            groupIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( groupIndexArrayIndex, group.indexGLType );
            OpenGLES2.AppendToArray( groupIndexArrayIndex, group.indices )

        if hasLayer( ICONS, self.layers ):
            iconTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                             2,
                                                             [ iconTextureS,
                                                               iconTextureT ] ) ]
        
            icon = MeshStripPlane( 2,
                                   2,
                                   MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                   None,
                                   None,
                                   iconTexCoordAttribute,
                                   MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                   None,
                                   MESH_CCW_WINDING,
                                   False )

            icon.translate( [ -0.5, -0.5, 0 ] )
            icon.scale( [ 2.0 * iconWidth / float( targetWidth ),
                          2.0 * iconHeight / float( targetHeight ) ] )
           
            iconVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconVertexArrayIndex, icon.vertexGLType )
            OpenGLES2.AppendToArray( iconVertexArrayIndex, icon.vertices )
            
            iconTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconTexCoordArrayIndex, icon.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( iconTexCoordArrayIndex, icon.texCoords[ 0 ] )
            
            iconIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( iconIndexArrayIndex, icon.indexGLType );
            OpenGLES2.AppendToArray( iconIndexArrayIndex, icon.indices )

        if hasLayer( OVERLAY, self.layers ) or hasLayer( OVERLAY4X, self.layers ):            
            overlayTexCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                                2,
                                                                [ overlayTextureS,
                                                                  overlayTextureT ] ) ]
        
            overlay = MeshStripPlane( 2,
                                      2,
                                      MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                                      None,
                                      None,
                                      overlayTexCoordAttribute,
                                      MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                      None,
                                      MESH_CCW_WINDING,
                                      False )

            overlay.translate( [ -0.5, -0.5, 0 ] )
            overlay.scale( [ 2.0 * overlayWidth / float( targetWidth ),
                             2.0 * overlayHeight / float( targetHeight ) ] )
                   
            overlayVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( overlayVertexArrayIndex, overlay.vertexGLType )
            OpenGLES2.AppendToArray( overlayVertexArrayIndex, overlay.vertices )
            
            overlayTexCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( overlayTexCoordArrayIndex, overlay.texCoordsGLTypes[ 0 ] )
            OpenGLES2.AppendToArray( overlayTexCoordArrayIndex, overlay.texCoords[ 0 ] )
            
            overlayIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
            OpenGLES2.CreateArray( overlayIndexArrayIndex, overlay.indexGLType );
            OpenGLES2.AppendToArray( overlayIndexArrayIndex, overlay.indices )
            
        # Create shaders.
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )
        
        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )
        
        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, "position_in" )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex, texCoordLocationIndex, "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.ValidateProgram( programIndex )
        OpenGLES2.UseProgram( programIndex )        
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        mVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mVPLocationIndex, programIndex, 'mvp' )
            
        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )

        if ( self.action == DRAW or self.action == UPLOAD_AND_DRAW ) and self.fboSize:
            # Create a FBO and textures (RGBA8) used for drawing.
            fboTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
            OpenGLES2.CreateNullTextureData( fboTextureDataIndex, targetWidth, targetHeight, 'OFF', 'OPENGLES2_RGBA8888' )
            
            fboTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
            OpenGLES2.GenTexture( fboTextureIndex )
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
            OpenGLES2.TexImage2D( fboTextureDataIndex, 'GL_TEXTURE_2D' )
            OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

            defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
            
            framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
            OpenGLES2.GenFramebuffer( framebufferIndex )
            OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', fboTextureIndex, 0 )
            OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )            
        
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.log:
            timerIndex = indexTracker.allocIndex( 'TEST_TIMER_INDEX' )
            Test.SetTimer( timerIndex )

        if self.log and self.busyTime > 0:
            if self.busyLocation == 'before':
                if self.busyType == 'adaptive':
                    Test.BusyDelay( timerIndex, self.busyTime )
                elif self.busyType == 'fixed':
                    Test.CpuLoad( self.busyTime )
                else:
                    raise 'Unexpected busy type'
            
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            # Render into fbo/texture, if defined.
            if self.fboSize:
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
                OpenGLES2.Viewport( 0, 0, targetWidth, targetHeight )                    
        
            # Clear the target if drawing is requested and the layers do not include background.
            if not hasLayer( BACKGROUND, self.layers ):
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                
        # Do texture data upload if requested
        if self.action == UPLOAD or self.action == UPLOAD_AND_DRAW:
            if hasLayer( BACKGROUND, self.layers ):
                if not self.reuseTextures:
                    OpenGLES2.DeleteTexture( backgroundTextureIndex )
                    OpenGLES2.GenTexture( backgroundTextureIndex )
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                    OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                else:
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                    
                OpenGLES2.TexImage2D( backgroundTextureDataIndex, 'GL_TEXTURE_2D' )

            if hasLayer( PANELS, self.layers ):
                for i in range( len( panelTextureDataIndices ) ):
                    if not self.reuseTextures:
                        OpenGLES2.DeleteTexture( panelTextureIndices[ i ] )
                        OpenGLES2.GenTexture( panelTextureIndices[ i ] )
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                    else:
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                        
                    OpenGLES2.TexImage2D( panelTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if hasLayer( GROUPS, self.layers ):
                for i in range( len( groupTextureDataIndices ) ):
                    if not self.reuseTextures:
                        OpenGLES2.DeleteTexture( groupTextureIndices[ i ] )
                        OpenGLES2.GenTexture( groupTextureIndices[ i ] )
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                    else:                   
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                        
                    OpenGLES2.TexImage2D( groupTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if hasLayer( ICONS, self.layers ):
                for i in range( len( iconTextureDataIndices ) ):
                    if not self.reuseTextures:
                        OpenGLES2.DeleteTexture( iconTextureIndices[ i ] )
                        OpenGLES2.GenTexture( iconTextureIndices[ i ] )
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                    else:                   
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                        
                    OpenGLES2.TexImage2D( iconTextureDataIndices[ i ], 'GL_TEXTURE_2D' )

            if hasLayer( OVERLAY, self.layers ) or hasLayer( OVERLAY4X, self.layers ):
                for i in range( overlays ):
                    if not self.reuseTextures:
                        OpenGLES2.DeleteTexture( overlayTextureIndices[ i ] )
                        OpenGLES2.GenTexture( overlayTextureIndices[ i ] )
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', overlayTextureIndices[ i ] )
                        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
                    else:
                        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', overlayTextureIndices[ i ] )
                    
                    OpenGLES2.TexImage2D( overlayTextureDataIndex, 'GL_TEXTURE_2D' )
                    
        # Draw if drawing is requested                
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
            if hasLayer( BACKGROUND, self.layers ):
                OpenGLES2.Disable( 'GL_BLEND' )
                
                OpenGLES2.UniformMatrix( mVPLocationIndex,
                                         1,
                                         [ 1.0, 0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0, 0.0,
                                           0.0, 0.0, 1.0, 0.0,
                                           0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex, backgroundVertexArrayIndex, 2, 'OFF', 0 )
                OpenGLES2.VertexAttribPointer( texCoordLocationIndex, backgroundTexCoordArrayIndex, 2, 'OFF', 0 )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', backgroundTextureIndex )
                OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                
                OpenGLES2.DrawElements( background.glMode, len( background.indices ), 0, backgroundIndexArrayIndex )    

            if hasLayer( PANELS, self.layers ):
                OpenGLES2.Enable( 'GL_BLEND' )
                
                for i in range( len( panelTextureIndices ) ):
                    origin = panelOrigins[ i ]
                    tx = ( -1.0 + panelWidth / float( targetWidth ) ) + ( 2.0 * origin[ 0 ] / float ( targetWidth ) )
                    ty = ( -1.0 + panelHeight / float( targetHeight ) ) + ( 2.0 * origin[ 1 ] / float ( targetHeight ) )
                    
                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex, panelVertexArrayIndex, 2, 'OFF', 0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex, panelTexCoordArrayIndex, 2, 'OFF', 0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', panelTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                    
                    OpenGLES2.DrawElements( panel.glMode, len( panel.indices ), 0, panelIndexArrayIndex )    

            if hasLayer( GROUPS, self.layers ):
                OpenGLES2.Enable( 'GL_BLEND' )
                
                for i in range( len( groupTextureIndices ) ):
                    origin = groupOrigins[ i ]
                    tx = ( -1.0 + groupWidth / float( targetWidth ) ) + ( 2.0 * origin[ 0 ] / float ( targetWidth ) )
                    ty = ( -1.0 + groupHeight / float( targetHeight ) ) + ( 2.0 * origin[ 1 ] / float ( targetHeight ) )
                    
                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex, groupVertexArrayIndex, 2, 'OFF', 0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex, groupTexCoordArrayIndex, 2, 'OFF', 0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', groupTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                    
                    OpenGLES2.DrawElements( group.glMode, len( group.indices ), 0, groupIndexArrayIndex )    

            if hasLayer( ICONS, self.layers ):
                OpenGLES2.Enable( 'GL_BLEND' )
                
                for i in range( len( iconTextureIndices ) ):
                    origin = iconOrigins[ i ]
                    tx = ( -1.0 + iconWidth / float( targetWidth ) ) + ( 2.0 * origin[ 0 ] / float ( targetWidth ) )
                    ty = ( -1.0 + iconHeight / float( targetHeight ) ) + ( 2.0 * origin[ 1 ] / float ( targetHeight ) )
                    
                    OpenGLES2.UniformMatrix( mVPLocationIndex,
                                             1,
                                             [ 1.0, 0.0, 0.0, 0.0,
                                               0.0, 1.0, 0.0, 0.0,
                                               0.0, 0.0, 1.0, 0.0,
                                               tx,  ty,  0.0, 1.0 ] )

                    OpenGLES2.VertexAttribPointer( vertexLocationIndex, iconVertexArrayIndex, 2, 'OFF', 0 )
                    OpenGLES2.VertexAttribPointer( texCoordLocationIndex, iconTexCoordArrayIndex, 2, 'OFF', 0 )

                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', iconTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                    
                    OpenGLES2.DrawElements( icon.glMode, len( icon.indices ), 0, iconIndexArrayIndex )    

            if hasLayer( OVERLAY, self.layers ) or hasLayer( OVERLAY4X, self.layers ):
                OpenGLES2.Enable( 'GL_BLEND' )
                
                OpenGLES2.UniformMatrix( mVPLocationIndex,
                                         1,
                                         [ 1.0, 0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0, 0.0,
                                           0.0, 0.0, 1.0, 0.0,
                                           0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex, overlayVertexArrayIndex, 2, 'OFF', 0 )
                OpenGLES2.VertexAttribPointer( texCoordLocationIndex, overlayTexCoordArrayIndex, 2, 'OFF', 0 )

                for i in range( overlays ):                
                    OpenGLES2.BindTexture( 'GL_TEXTURE_2D', overlayTextureIndices[ i ] )
                    OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                
                    OpenGLES2.DrawElements( overlay.glMode, len( overlay.indices ), 0, overlayIndexArrayIndex )

            # Render fbo/texture into screen, if defined.
            if self.fboSize:
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

                OpenGLES2.Disable( 'GL_BLEND' )
                
                OpenGLES2.UniformMatrix( mVPLocationIndex,
                                         1,
                                         [ 1.0, 0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0, 0.0,
                                           0.0, 0.0, 1.0, 0.0,
                                           0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.VertexAttribPointer( vertexLocationIndex, backgroundVertexArrayIndex, 2, 'OFF', 0 )
                OpenGLES2.VertexAttribPointer( texCoordLocationIndex, backgroundTexCoordArrayIndex, 2, 'OFF', 0 )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', fboTextureIndex )
                OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
                
                OpenGLES2.DrawElements( background.glMode, len( background.indices ), 0, backgroundIndexArrayIndex )    

            if self.log and self.busyTime > 0:
                if self.busyLocation == 'after':
                    if self.busyType == 'adaptive':
                        Test.BusyDelay( timerIndex, self.busyTime )
                    elif self.busyType == 'fixed':
                        Test.CpuLoad( self.busyTime )
                    else:
                        raise 'Unexpected busy type'

            if self.log:
                Test.LogSince( timerIndex, '"GFX_ISSUE ' + self.name + '"', self.repeats * 2 )
                
            target.swapBuffers( state )

            if self.log:
                Test.LogSince( timerIndex, '"FRAME ' + self.name + '"', self.repeats * 2 )
                Test.LogMessage( '"VSYNC_DELAY ' + self.name + '"', self.logMessageStartFrame, "SCT_MESSAGE_GET_BUFFER", 0, self.repeats * 2 )
                Test.LogCpuLoad( '"SYSTEM_LOAD ' + self.name + '"', 'SCT_CPULOAD_SYSTEM', self.repeats, self.repeats * 2 );
                Test.LogCpuLoad( '"PROCESS2BUSY_LOAD ' + self.name + '"', 'SCT_CPULOAD_PROCESS2BUSY', self.repeats, self.repeats * 2 );
                Test.LogCpuLoad( '"PROCESS2TOTAL_LOAD ' + self.name + '"', 'SCT_CPULOAD_PROCESS2TOTAL', self.repeats, self.repeats * 2 );
                if self.logTotal:
                    Test.LogSince( timerIndex, '"TOTAL ' + self.name + '"', self.repeats * 2 )
            
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class AnalysisBlurBenchmark( Benchmark ):
    def __init__( self, width, height, iterations, busyTime, busyType, busyLocation  ):
        Benchmark.__init__( self )
        self.log                    = True
        self.repeats                = REPEATS
        self.width                  = width
        self.height                 = height
        self.format                 = 'RGBA8'
        self.iterations             = iterations
        self.busyTime               = busyTime      # millis
        self.busyType               = busyType      # adaptive|fixed
        self.busyLocation           = busyLocation  # before|after
        self.logMessageStartFrame   = START_FRAME
        self.logTotal               = True        
        self.filtering  = 'nearest'

        bs = 'no load '
        if self.busyTime > 0:
            bs = '%d ms %s load %s gfx issue ' % ( self.busyTime, self.busyType, self.busyLocation, )
        
        self.name = "OPENGLES2 blur %s%dx%d %s %d iterations %s filtering" % ( bs,
                                                                               self.width,
                                                                               self.height,
                                                                               self.format,
                                                                               self.iterations,
                                                                               self.filtering, )

    def build( self, target, modules ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]
        Test      = modules[ 'Test' ]

        indexTracker = IndexTracker()

        # Shader source codes
        rawVertexShaderSource = """
        precision mediump float;
        attribute vec2 gVertex;
        attribute vec2 gTexCoord0;
        varying vec2 uv;

        void main()
        {
            gl_Position = vec4( gVertex, 0, 1 );
            uv = gTexCoord0;
        }"""

        rawBlurFragmentShaderSource = """
        precision lowp float;
        varying vec2 uv;
        uniform sampler2D gTexture0;
        uniform vec2 scale;

        void main()
        {
            vec4 color = vec4( 0.0, 0.0, 0.0, 0.0 );
            color += 0.0366 * texture2D( gTexture0, uv - 3.0 * scale );
            color += 0.1113 * texture2D( gTexture0, uv - 2.0 * scale );
            color += 0.2167 * texture2D( gTexture0, uv - 1.0 * scale );
            color += 0.2707 * texture2D( gTexture0, uv + 0.0 );
            color += 0.2167 * texture2D( gTexture0, uv + 1.0 * scale );
            color += 0.1113 * texture2D( gTexture0, uv + 2.0 * scale );
            color += 0.0366 * texture2D( gTexture0, uv + 3.0 * scale );
            gl_FragColor = color;
        }"""
        
        rawDrawFragmentShaderSource = """
        precision mediump float;
        uniform sampler2D gTexture0;
        varying vec2 uv;

        void main()
        {
           gl_FragColor = texture2D( gTexture0, uv );
        }"""
        
        vertexShaderSource       = formatShader( rawVertexShaderSource )
        blurFragmentShaderSource = formatShader( rawBlurFragmentShaderSource )
        drawFragmentShaderSource = formatShader( rawDrawFragmentShaderSource )
        
        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()
       
        state = setupOpenGLES2( modules, indexTracker, target )            

        # Create image texture data.
        imageTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        imageTextureIndex     = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )

        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex, 'IMAGES_FLOWER', IMAGES_FORMATS[ self.format ], self.width, self.height )       
            
        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imagesDataIndex, dataIndex )
            
        OpenGLES2.CreateTextureData( imageTextureDataIndex, dataIndex, self.width, self.height, FORMATS[ self.format ] )
            
        OpenGLES2.GenTexture( imageTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', imageTextureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )            
        OpenGLES2.TexImage2D( imageTextureDataIndex, 'GL_TEXTURE_2D' )
            
        # Create geometry                                
        texCoordAttribute = [ MeshTexCoordAttribute( MESH_TYPE_FLOAT, 2, [ 1.0, 1.0 ] ) ]
        
        mesh = MeshStripPlane( 2,
                               2,
                               MeshVertexAttribute( MESH_TYPE_FLOAT, 2 ),
                               None,
                               None,
                               texCoordAttribute,
                               MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                               None,
                               MESH_CCW_WINDING,
                               False )

        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0  ] )

        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )
        
        blurProgram = createProgram( modules, indexTracker, vertexShaderSource, blurFragmentShaderSource, vertexDataSetup )
        useProgram( modules, blurProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, blurProgram )
        validateProgram( modules, blurProgram )

        drawProgram = createProgram( modules, indexTracker, vertexShaderSource, drawFragmentShaderSource, vertexDataSetup )
        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        validateProgram( modules, drawProgram )

        useProgram( modules, blurProgram )

        blurScaleLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( blurScaleLocationIndex, blurProgram.programIndex, 'scale' )
       
        # Create a FBO and textures used for blurring.        
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( textureDataIndex, self.width, self.height, 'OFF', FORMATS[ self.format ] )

        glFiltering = 'GL_NEAREST'
        if self.filtering == 'linear':
            glFiltering = 'GL_LINEAR'
        
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', glFiltering, glFiltering, 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
        
        framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture1Index, 0 )

        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
        OpenGLES2.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.log:
            timerIndex = indexTracker.allocIndex( 'TEST_TIMER_INDEX' )
            Test.SetTimer( timerIndex )

        if self.log and self.busyTime > 0:
            if self.busyLocation == 'before':
                if self.busyType == 'adaptive':
                    Test.BusyDelay( timerIndex, self.busyTime )
                elif self.busyType == 'fixed':
                    Test.CpuLoad( self.busyTime )
                else:
                    raise 'Unexpected busy type'
        
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )
        OpenGLES2.Viewport( 0, 0, self.width, self.height )
        
        useProgram( modules, blurProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, blurProgram )
        useTexture( modules, imageTextureIndex, 0, blurProgram )
        
        for i in range( self.iterations ):
            # Bind FBO with the first temp texture
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture1Index, 0 )

            # Draw source image into the first temp texture 
            OpenGLES2.Uniformf( blurScaleLocationIndex, 1, [ 1.0 / self.width, 0.0 ] )
            drawVertexBufferData( modules, vertexBufferDataSetup )           

            # Set the second temp texture into FBO
            OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_TEXTURE_2D', texture2Index, 0 )

            # Use the first temp texture
            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )

            # Draw the first temp texture into the second temp texture
            OpenGLES2.Uniformf( blurScaleLocationIndex, 1, [ 0.0, 1.0 / self.height ] )
            drawVertexBufferData( modules, vertexBufferDataSetup )            

            OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
            
        # Remove FBO and draw the second temp texture into the screen
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
        OpenGLES2.Viewport( 0, 0, screenWidth, screenHeight )

        useProgram( modules, drawProgram )
        useVertexBufferData( modules, vertexBufferDataSetup, drawProgram )
        useTexture( modules, texture2Index, 0, drawProgram )

        drawVertexBufferData( modules, vertexBufferDataSetup )        

        if self.log and self.busyTime > 0:
            if self.busyLocation == 'after':
                if self.busyType == 'adaptive':
                    Test.BusyDelay( timerIndex, self.busyTime )
                elif self.busyType == 'fixed':
                    Test.CpuLoad( self.busyTime )
                else:
                    raise 'Unexpected busy type'

        if self.log:
            Test.LogSince( timerIndex, '"GFX_ISSUE ' + self.name + '"', self.repeats * 2 )
        
        target.swapBuffers( state )

        if self.log:
            Test.LogSince( timerIndex, '"FRAME ' + self.name + '"', self.repeats * 2 )
            Test.LogMessage( '"VSYNC_DELAY ' + self.name + '"', self.logMessageStartFrame, "SCT_MESSAGE_GET_BUFFER", 0, self.repeats * 2 )
            Test.LogCpuLoad( '"SYSTEM_LOAD ' + self.name + '"', 'SCT_CPULOAD_SYSTEM', self.repeats, self.repeats * 2 );
            Test.LogCpuLoad( '"PROCESS2BUSY_LOAD ' + self.name + '"', 'SCT_CPULOAD_PROCESS2BUSY', self.repeats, self.repeats * 2 );
            Test.LogCpuLoad( '"PROCESS2TOTAL_LOAD ' + self.name + '"', 'SCT_CPULOAD_PROCESS2TOTAL', self.repeats, self.repeats * 2 );
            if self.logTotal:
                Test.LogSince( timerIndex, '"TOTAL ' + self.name + '"', self.repeats * 2 )
        
