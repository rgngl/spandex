#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreatePaintBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG create paint"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )

        # Try to ensure the paint is actually created.
        OpenVG.Finish()

        OpenVG.DestroyPaint( paintIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ColorPaintBenchmark( Benchmark ):  
    def __init__( self, set, use, swap ):
        Benchmark.__init__( self )
        self.set  = set
        self.use  = use
        self.swap = swap
        if self.use:
            self.set = True           
        self.name = "OPENVG color paint set=%s use=%s swap=%s" % ( boolToStr( self.set ),
                                                                   boolToStr( self.use ),
                                                                   boolToStr( self.swap ) )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        if self.use:
            pathDataIndex = 0
            OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

            scale = 65535.0
            OpenVG.Lines( pathDataIndex,
                          [ 0,          1 * scale,
                            1 * scale,  1 * scale,
                            1 * scale,  0,
                            0,          0 ],
                          'OFF' )
        
            OpenVG.ClosePath( pathDataIndex )

            pathIndex = 0
            OpenVG.CreatePath( pathIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1.0 / scale,
                               0,
                               1,
                               1,
                               [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.swap:
            OpenVG.Clear( 0, 0, w, h )
            
        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 1.0, 1.0, 1.0, 1.0 ] )
        
        if self.set:
            OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        if self.use:
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        if self.swap:
            target.swapBuffers( state )
            
        OpenVG.DestroyPaint( paintIndex )

        if not self.swap:
            # ------------------------------------------------------------
            # Start final actions
            self.beginFinalActions()

            # Ensure all benchmark loop actions have finished before timing
            OpenVG.Finish()           
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class LinearGradientPaintBenchmark( Benchmark ):  
    def __init__( self, stops, set, use, swap, angle ):
        Benchmark.__init__( self )
        self.stops = stops
        self.set   = set
        self.use   = use
        self.swap  = swap
        self.angle = angle
        if self.use:
            self.set = True           
        self.name  = "OPENVG linear gradient paint %d stops set=%s use=%s swap=%s angle=%.2f" % ( self.stops,
                                                                                                  boolToStr( self.set ),
                                                                                                  boolToStr( self.use ),
                                                                                                  boolToStr( self.swap ),
                                                                                                  self.angle, )
        
    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        if self.use:
            pathDataIndex = 0
            OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

            scale = 65535.0
            OpenVG.Lines( pathDataIndex,
                          [ 0,          1 * scale,
                            1 * scale,  1 * scale,
                            1 * scale,  0,
                            0,          0 ],
                          'OFF' )
        
            OpenVG.ClosePath( pathDataIndex )

            pathIndex = 0
            OpenVG.CreatePath( pathIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1.0 / scale,
                               0,
                               1,
                               1,
                               [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.angle:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.angle )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.swap:
            OpenVG.Clear( 0, 0, w, h )
        
        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0.0, 0.0 ], [ 100.0, 100.0 ] )

        stops = []
        for i in range( self.stops ):
            r = i / float( self.stops - 1 )
            stops += [ r, r, r, r, r ]
        
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           stops )

        if self.set:
            OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        if self.use:
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        if self.swap:
            target.swapBuffers( state )
            
        OpenVG.DestroyPaint( paintIndex )

        if not self.swap:
            # ------------------------------------------------------------
            # Start final actions
            self.beginFinalActions()

            # Ensure all benchmark loop actions have finished before timing
            OpenVG.Finish()
            
        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class RadialGradientPaintBenchmark( Benchmark ):  
    def __init__( self, stops, set, use, swap ):
        Benchmark.__init__( self )
        self.stops = stops
        self.set   = set
        self.use   = use
        self.swap  = swap
        if self.use:
            self.set = True
        self.name = "OPENVG radial gradient paint %d stops set=%s use=%s swap=%s" % ( self.stops,
                                                                                      boolToStr( self.set ),
                                                                                      boolToStr( self.use ),
                                                                                      boolToStr( self.swap ) )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        if self.use:
            pathDataIndex = 0
            OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

            scale = 65535.0
            OpenVG.Lines( pathDataIndex,
                          [ 0,          1 * scale,
                            1 * scale,  1 * scale,
                            1 * scale,  0,
                            0,          0 ],
                          'OFF' )
        
            OpenVG.ClosePath( pathDataIndex )

            pathIndex = 0
            OpenVG.CreatePath( pathIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1.0 / scale,
                               0,
                               1,
                               1,
                               [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.swap:
            OpenVG.Clear( 0, 0, w, h )

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.RadialGradientPaint( paintIndex, [ 0.0, 0.0 ], [ 100.0, 100.0 ], 50 )

        stops = []
        for i in range( self.stops ):
            r = i / float( self.stops - 1 )
            stops += [ r, r, r, r, r ]
        
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           stops )

        if self.set:
            OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        if self.use:
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        if self.swap:
            target.swapBuffers( state )
            
        OpenVG.DestroyPaint( paintIndex )

        if not self.swap:
            # ------------------------------------------------------------
            # Start final actions
            self.beginFinalActions()

            # Ensure all benchmark loop actions have finished before timing
            OpenVG.Finish()
            

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class PatternPaintBenchmark( Benchmark ):  
    def __init__( self, format, quality, set, use, swap ):
        Benchmark.__init__( self )
        self.format  = format
        self.quality = quality       
        self.set     = set
        self.use     = use
        self.swap    = swap
        if self.use:
            self.set = True
            
        self.name = "OPENVG pattern paint frmat=%s qlty=%s set=%s draw=%s swap=%s" % ( imageFormatToStr( self.format ),
                                                                                       imageQualityToStr( self.quality ),
                                                                                       boolToStr( self.set ),
                                                                                       boolToStr( self.use ),
                                                                                       boolToStr( self.swap ) )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        if self.use:
            pathDataIndex = 0
            OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

            scale = 65535.0
            OpenVG.Lines( pathDataIndex,
                          [ 0,          1 * scale,
                            1 * scale,  1 * scale,
                            1 * scale,  0,
                            0,          0 ],
                          'OFF' )
        
            OpenVG.ClosePath( pathDataIndex )

            pathIndex = 0
            OpenVG.CreatePath( pathIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1.0 / scale,
                               0,
                               1,
                               1,
                               [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            w, h,
                            [ self.quality ] )
        OpenVG.ClearImage( imageIndex, 0, 0, w, h )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.swap:
            OpenVG.Clear( 0, 0, w, h )
       
        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.PatternPaint( paintIndex, imageIndex, 'VG_TILE_REPEAT' )

        if self.set:
            OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        if self.use:
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        if self.swap:
            target.swapBuffers( state )
            
        OpenVG.DestroyPaint( paintIndex )

        if not self.swap:
            # ------------------------------------------------------------
            # Start final actions
            self.beginFinalActions()

            # Ensure all benchmark loop actions have finished before timing
            OpenVG.Finish()
