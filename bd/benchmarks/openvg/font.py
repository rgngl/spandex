#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class FontCreationBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG create font"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        fontIndex = 0
        OpenVG.CreateFont( fontIndex, 256 )

        # Try to ensure the font is actually created.
        OpenVG.Finish()

        OpenVG.DestroyFont( fontIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class GlyphToPathBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG glyph to path"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        glyphSize = 40

        # A
        A_pathIndex = 0
        OpenVG.CreatePath( A_pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1, 0,
                           1, 0,
                           [ 'VG_PATH_CAPABILITY_ALL' ] )
        A_pathDataIndex = 0
        OpenVG.CreatePathData( A_pathDataIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1,
                               1 )

        OpenVG.MoveTo( A_pathDataIndex, [ 0,0 ], 'OFF')        
        OpenVG.Lines(  A_pathDataIndex, [ glyphSize / 2,            glyphSize ],            'ON' )
        OpenVG.Lines(  A_pathDataIndex, [ glyphSize / 2,            -glyphSize ],           'ON' )
        OpenVG.MoveTo( A_pathDataIndex, [ -glyphSize / 4,           glyphSize / 2 ],        'ON' )
        OpenVG.Lines(  A_pathDataIndex, [ -glyphSize / 2,           0 ],                    'ON' )

        OpenVG.AppendPathData( A_pathIndex, A_pathDataIndex)

        fontIndex = 0
        OpenVG.CreateFont( fontIndex, 1 )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.SetGlyphToPath( fontIndex,
                               0,
                               A_pathIndex,
                               'OFF',
                               [ 0, 0 ],
                               [ glyphSize + glyphSize / 8, 0 ] )
        OpenVG.ClearGlyph( fontIndex, 0 )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class DrawPathBenchmark( Benchmark ):   
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG draw font path"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        glyphSize = 40

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.7, 0.7, 0.7, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.Stroke( glyphSize / 10, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0.0 )

        # A
        A_pathIndex = 0
        OpenVG.CreatePath( A_pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1, 0,
                           1, 0,
                           [ 'VG_PATH_CAPABILITY_ALL' ] )
        A_pathDataIndex = 0
        OpenVG.CreatePathData( A_pathDataIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1,
                               1 )

        OpenVG.MoveTo( A_pathDataIndex, [ 0,0 ], 'OFF')        
        OpenVG.Lines(  A_pathDataIndex, [ glyphSize / 2,            glyphSize ],            'ON' )
        OpenVG.Lines(  A_pathDataIndex, [ glyphSize / 2,            -glyphSize ],           'ON' )
        OpenVG.MoveTo( A_pathDataIndex, [ -glyphSize / 4,           glyphSize / 2 ],        'ON' )
        OpenVG.Lines(  A_pathDataIndex, [ -glyphSize / 2,           0 ],                    'ON' )

        OpenVG.AppendPathData( A_pathIndex, A_pathDataIndex)

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )

        OpenVG.LoadIdentity()
        OpenVG.Translate( 10, 10 )
        OpenVG.DrawPath( A_pathIndex, [ 'VG_STROKE_PATH' ] )

        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class DrawGlyphBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG draw glyph"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        glyphSize = 40

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.7, 0.7, 0.7, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.Stroke( glyphSize / 10, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0.0 )

        # A
        A_pathIndex = 0
        OpenVG.CreatePath( A_pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1, 0,
                           1, 0,
                           [ 'VG_PATH_CAPABILITY_ALL' ] )
        A_pathDataIndex = 0
        OpenVG.CreatePathData( A_pathDataIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1,
                               1 )

        OpenVG.MoveTo( A_pathDataIndex, [ 0,0 ], 'OFF')        
        OpenVG.Lines(  A_pathDataIndex, [ glyphSize / 2,            glyphSize ],            'ON' )
        OpenVG.Lines(  A_pathDataIndex, [ glyphSize / 2,            -glyphSize ],           'ON' )
        OpenVG.MoveTo( A_pathDataIndex, [ -glyphSize / 4,           glyphSize / 2 ],        'ON' )
        OpenVG.Lines(  A_pathDataIndex, [ -glyphSize / 2,           0 ],                    'ON' )

        OpenVG.AppendPathData( A_pathIndex, A_pathDataIndex)

        fontIndex = 0
        OpenVG.CreateFont( fontIndex, 1 )

        OpenVG.SetGlyphToPath( fontIndex,
                               0,
                               A_pathIndex,
                               'OFF',
                               [ 0, 0 ],
                               [ glyphSize + glyphSize / 8, 0 ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )

        OpenVG.GlyphOrigin( [ 10, 10 ] )
        OpenVG.DrawGlyph( fontIndex, 0, [ 'VG_STROKE_PATH' ], 'OFF' )

        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class DrawPathsBenchmark( Benchmark ):   
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG draw font paths"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        glyphSize = 40
        strokeWidth = glyphSize / 10

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.7, 0.7, 0.7, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.Stroke( strokeWidth, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0.0 )

        # A
        A_pathIndex = 0
        OpenVG.CreatePath( A_pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1, 0,
                           1, 0,
                           [ 'VG_PATH_CAPABILITY_ALL' ] )
        A_pathDataIndex = 0
        OpenVG.CreatePathData( A_pathDataIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1,
                               1 )

        OpenVG.MoveTo( A_pathDataIndex, [ 0,0 ], 'OFF')        
        OpenVG.Lines(  A_pathDataIndex, [ glyphSize / 2,            glyphSize ],            'ON' )
        OpenVG.Lines(  A_pathDataIndex, [ glyphSize / 2,            -glyphSize ],           'ON' )
        OpenVG.MoveTo( A_pathDataIndex, [ -glyphSize / 4,           glyphSize / 2 ],        'ON' )
        OpenVG.Lines(  A_pathDataIndex, [ -glyphSize / 2,           0 ],                    'ON' )

        OpenVG.AppendPathData( A_pathIndex, A_pathDataIndex)

        # B
        B_pathIndex = 1
        OpenVG.CreatePath( B_pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1, 0,
                           1, 0,
                           [ 'VG_PATH_CAPABILITY_ALL' ] )
        B_pathDataIndex = 1
        OpenVG.CreatePathData( B_pathDataIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1,
                               1 )

        OpenVG.MoveTo( B_pathDataIndex, [ 0,0 ], 'OFF')        
        OpenVG.Lines(  B_pathDataIndex, [ 0,                        glyphSize ],            'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ glyphSize / 2,            0 ],                    'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ glyphSize / 8,            -glyphSize / 4 ],       'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ -glyphSize / 8,           -glyphSize / 4 ],       'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ glyphSize / 8,            -glyphSize / 4 ],       'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ -glyphSize / 8,           -glyphSize / 4 ],       'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ -glyphSize / 2,           0 ],                    'ON' )
        OpenVG.ClosePath( B_pathDataIndex )
        OpenVG.MoveTo( B_pathDataIndex, [ 0,                        glyphSize / 2 ],        'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ glyphSize / 2,            0 ],                    'ON' )

        OpenVG.AppendPathData( B_pathIndex, B_pathDataIndex)

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )

        a = glyphSize + glyphSize / 8
        b = glyphSize - glyphSize * 2 / 8
        c = b + glyphSize / 4
       
        OpenVG.LoadIdentity()
        OpenVG.Translate( strokeWidth, 10 )

        OpenVG.DrawPath( A_pathIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.Translate( a, 0 )
        
        OpenVG.DrawPath( B_pathIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.Translate( c, 0 )

        OpenVG.DrawPath( A_pathIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.Translate( a, 0 )

        OpenVG.DrawPath( B_pathIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.Translate( b, 0 )

        OpenVG.DrawPath( B_pathIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.Translate( b, 0 )

        OpenVG.DrawPath( A_pathIndex, [ 'VG_STROKE_PATH' ] )

        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class DrawGlyphsBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG draw glyphs"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        glyphSize = 40
        strokeWidth = glyphSize / 10

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.7, 0.7, 0.7, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.Stroke( strokeWidth, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0.0 )

        # A
        A_pathIndex = 0
        OpenVG.CreatePath( A_pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1, 0,
                           1, 0,
                           [ 'VG_PATH_CAPABILITY_ALL' ] )
        A_pathDataIndex = 0
        OpenVG.CreatePathData( A_pathDataIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1,
                               1 )

        OpenVG.MoveTo( A_pathDataIndex, [ 0,0 ], 'OFF')        
        OpenVG.Lines(  A_pathDataIndex, [ glyphSize / 2,            glyphSize ],            'ON' )
        OpenVG.Lines(  A_pathDataIndex, [ glyphSize / 2,            -glyphSize ],           'ON' )
        OpenVG.MoveTo( A_pathDataIndex, [ -glyphSize / 4,           glyphSize / 2 ],        'ON' )
        OpenVG.Lines(  A_pathDataIndex, [ -glyphSize / 2,           0 ],                    'ON' )

        OpenVG.AppendPathData( A_pathIndex, A_pathDataIndex)

        # B
        B_pathIndex = 1
        OpenVG.CreatePath( B_pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1, 0,
                           1, 0,
                           [ 'VG_PATH_CAPABILITY_ALL' ] )
        B_pathDataIndex = 1
        OpenVG.CreatePathData( B_pathDataIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1,
                               1 )

        OpenVG.MoveTo( B_pathDataIndex, [ 0,0 ], 'OFF')        
        OpenVG.Lines(  B_pathDataIndex, [ 0,                        glyphSize ],            'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ glyphSize / 2,            0 ],                    'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ glyphSize / 8,            -glyphSize / 4 ],       'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ -glyphSize / 8,           -glyphSize / 4 ],       'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ glyphSize / 8,            -glyphSize / 4 ],       'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ -glyphSize / 8,           -glyphSize / 4 ],       'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ -glyphSize / 2,           0 ],                    'ON' )
        OpenVG.ClosePath( B_pathDataIndex )
        OpenVG.MoveTo( B_pathDataIndex, [ 0,                        glyphSize / 2 ],        'ON' )
        OpenVG.Lines(  B_pathDataIndex, [ glyphSize / 2,            0 ],                    'ON' )

        OpenVG.AppendPathData( B_pathIndex, B_pathDataIndex)

        fontIndex = 0
        OpenVG.CreateFont( fontIndex, 2 )

        OpenVG.SetGlyphToPath( fontIndex,
                               0,
                               A_pathIndex,
                               'OFF',
                               [ 0, 0 ],
                               [ glyphSize + glyphSize / 8, 0 ] )

        OpenVG.SetGlyphToPath( fontIndex,
                               1,
                               B_pathIndex,
                               'OFF',
                               [ 0, 0 ],
                               [ glyphSize - glyphSize * 2 / 8, 0 ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )
       
        OpenVG.GlyphOrigin( [ strokeWidth, 10 ] )

        OpenVG.DrawGlyphs( fontIndex,
                           6,
                           [ 0, 1, 0, 1, 1, 0 ],
                           [ 0, glyphSize / 4, 0, 0, 0, 0 ],
                           [ 0, 0, 0, 0, 0, 0 ],
                           [ 'VG_STROKE_PATH' ],
                           'OFF' )

        target.swapBuffers( state )
