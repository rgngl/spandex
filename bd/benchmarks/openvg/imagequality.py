#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ImageQualityBenchmark( Benchmark ):  
    def __init__( self, width, height, overdraw, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.overdraw    = overdraw
        self.format      = format
        self.quality     = quality
        self.name = "OPENVG image quality %s %dx%d ovdrw=%d frmat=%s" % ( imageQualityToStr( self.quality ),
                                                                          self.imageWidth,
                                                                          self.imageHeight,
                                                                          self.overdraw,
                                                                          imageFormatToStr( self.format ), )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )
        OpenVG.ImageQuality( self.quality )
        
        imageIndex = 0
        OpenVG.CreateImage( imageIndex, self.format, self.imageWidth, self.imageHeight, [ self.quality ] )

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.RadialGradientPaint( paintIndex, [ 20, 20 ], [ 50, 50 ], 30 )       
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )

        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * height,
                        scale * width,  scale * height,
                        scale * width,  0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           4,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.Clear( 0, 0, width, height )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        OpenVG.GetPixels( imageIndex,
                          0, 0,
                          0, 0,
                          self.imageWidth, self.imageHeight )
        
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            OpenVG.DrawImage( imageIndex )
        
        target.swapBuffers( state )

