#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *

######################################################################
def setupVG( modules, indexTracker, target, attributes = {} ):
    a = target.getDefaultAttributes( API_OPENVG )

    for k in attributes.keys():
        a[ k ] = attributes[ k ]

    return target.initialize( modules, indexTracker, API_OPENVG, a )        

######################################################################
def defaultOrientation( w, h ):
    if w > h:
        return 'SCT_SCREEN_ORIENTATION_LANDSCAPE'
    else:
        return 'SCT_SCREEN_ORIENTATION_PORTRAIT'

######################################################################
def orientationToStr( o ):
    return o[ 23 : ]
    
######################################################################
def boolToStr( b ):
    if b:
        return 'Yes'
    else:
        return 'No'

######################################################################
def usageHintToStr( hint ):
    if not hint:
        return 'NONE'
    return '+'.join( hint ).replace( 'EGL_READ_', 'READ' ).replace( 'EGL_WRITE_', 'WRITE' ).replace( 'SURFACE_BIT_KHR', '' )
    
######################################################################
def lockSurfaceFormatToString( f ):
    return f[ 11 : -4 ]
    
######################################################################
def bufferToStr( f ):
    return f[ 4 : ]
        
######################################################################
def imageFormatToStr( f ):
    return f[ 3 : ]
    
######################################################################
def imageQualityToStr( q ):
    return q[ 17 : ]
    
######################################################################
def renderingQualityToStr( q ):
    return q[ 21 : ]

######################################################################
def channelMaskToStr( mask ):
    return '+'.join( mask ).replace( 'VG_', '' )

######################################################################
def tilingToStr( t ):
    return t[ 8 : ]

######################################################################
def filterFormatToStr( f ):
    return f[ 17 : ]

######################################################################
def blendModeToStr( b ):
    return b[ 9 : ]

######################################################################
def pixelLayoutToStr( p ):
    return p[ 16 : ]

######################################################################
def strokeCapStyleToStr( s ):
    return s[ 7 : ]

######################################################################
def strokeJoinStyleToStr( s ):
    return s[ 8 : ]

######################################################################
def pathDataTypeToStr( p ):
    return p[ 17 : ]

######################################################################
def pathCapabilitiesToStr( pcs ):
    cs = []
    for pc in pcs:
        cs.append( pc[ 19 : ] )
    return str( cs ).replace( ' ', '' )
        
######################################################################
def maskOperationToStr( m ):
    return m[ 3: ]

######################################################################
def transformToStr( t ):
    return str( t ).replace( ' ', '' )

######################################################################
def arrayToStr( t ):
    return str( t ).replace( ' ', '' )

######################################################################
def matrixModeToStr( m ):
    return m[ 10 : ]

######################################################################
def pixmapUsageToStr( usage ):
    us = []
    for u in usage:
        us.append( u[ 10 : ] )
    return str( us ).replace( ' ', '' ).replace( "'", '' ).replace( ',', '|' ).replace( '[', '' ).replace( ']', '' )

######################################################################
def imagesFormatToStr( f ):
    return f[ 7 : ]

######################################################################
def pixmapFormatToStr( f ):
    return f[ 17 : ]

######################################################################
def colorSpaceToStr( c ):
    return c[ 18 : ]

######################################################################
def surfaceAlphaToStr( a ):
    return a[ 20 : ] 

######################################################################
def multisampleResolveToStr( m ):
    return m[ 24 : ]

######################################################################
def swapBehaviorToStr( s ):
    return s[ 11 : ]
