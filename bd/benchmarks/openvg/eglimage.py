#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.egl       import *
from common        import *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreatePixmapBenchmark( Benchmark ):  
    def __init__( self, width, height, imageFormat, pixmapFormat ):
        Benchmark.__init__( self )
        self.width        = width
        self.height       = height
        self.imageFormat  = imageFormat
        self.pixmapFormat = pixmapFormat
        imageDataString = ''
        if self.imageFormat:
            imageDataString = ' initdt=%s' % imagesFormatToStr( self.imageFormat )
        self.name = "OPENVG create pixmap benchmark sz=%dx%d frmt=%s%s" % ( self.width,
                                                                            self.height,
                                                                            pixmapFormatToStr( self.pixmapFormat ),
                                                                            imageDataString, )

    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        Images = modules[ 'Images' ]        

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, w, h, 'SCT_COLOR_FORMAT_DEFAULT' )

        eglDataIndex = -1        
        if self.imageFormat:
            imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
            Images.CreateData( imagesDataIndex,
                               'IMAGES_FLOWER',
                               self.imageFormat,
                               self.width, self.height )

            eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
            Images.CopyToEgl( imagesDataIndex,
                              eglDataIndex )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( eglPixmapIndex,
                          self.width, self.height,
                          self.pixmapFormat,
                          eglDataIndex )
        
        Egl.DestroyPixmap( eglPixmapIndex )
        
            
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreateSharedPixmapBenchmark( Benchmark ):  
    def __init__( self, width, height, imageFormat, pixmapFormat, usage ):
        Benchmark.__init__( self )
        self.width        = width
        self.height       = height
        self.imageFormat  = imageFormat
        self.pixmapFormat = pixmapFormat
        self.usage        = usage
        imageDataString = ''
        if self.imageFormat:
            imageDataString = ' initdt=%s' % imagesFormatToStr( self.imageFormat )
        self.name = "OPENVG create shared pixmap benchmark sz=%dx%d frmt=%s usge=%s%s" % ( self.width,
                                                                                           self.height,
                                                                                           self.pixmapFormat,
                                                                                           pixmapUsageToStr( self.usage),
                                                                                           imageDataString, )

    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        Images = modules[ 'Images' ]        

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, w, h, 'SCT_COLOR_FORMAT_DEFAULT' )

        eglDataIndex = -1
        if self.imageFormat:
            imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
            Images.CreateData( imagesDataIndex,
                               'IMAGES_FLOWER',
                               self.imageFormat,
                               self.width, self.height )

            eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
            Images.CopyToEgl( imagesDataIndex,
                              eglDataIndex )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.pixmapFormat,
                                self.usage,
                                eglDataIndex )
        
        Egl.DestroyPixmap( eglPixmapIndex )

            
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreateVgImageFromEglImageBenchmark( Benchmark ):  
    def __init__( self, width, height, imageFormat, pixmapFormat ):
        Benchmark.__init__( self )
        self.width        = width
        self.height       = height
        self.imageFormat  = imageFormat
        self.pixmapFormat = pixmapFormat
        imageDataString = ''
        if self.imageFormat:
            imageDataString = ' initdt=%s' % imagesFormatToStr( self.imageFormat )
        self.name = "OPENVG create vg image from egl image sz=%dx%d frmt=%s%s" % ( self.width,
                                                                                   self.height,
                                                                                   self.pixmapFormat,
                                                                                   imageDataString, )

    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Images = modules[ 'Images' ]        

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )

	Egl.BindApi( 'EGL_OPENVG_API' );
        
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, w, h, 'SCT_COLOR_FORMAT_DEFAULT' )

        surfaceBits = [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ]            
        attrs = target.getDefaultAttributes( API_OPENVG )

        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       surfaceBits,
                       [ 'EGL_OPENVG_BIT' ] )

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           vgConfigIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         vgSurfaceIndex,
                         vgSurfaceIndex,
                         vgContextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        eglDataIndex = -1
        if self.imageFormat:
            imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
            Images.CreateData( imagesDataIndex,
                               'IMAGES_FLOWER',
                               self.imageFormat,
                               self.width, self.height )

            eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
            Images.CopyToEgl( imagesDataIndex,
                              eglDataIndex )

        OpenVG.CheckExtension( 'VG_KHR_EGL_image' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.pixmapFormat,
                                [ 'SCT_USAGE_OPENVG_IMAGE' ],
                                eglDataIndex )

        imageAttributes = []
        if self.imageFormat:
            imageAttribs = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ],
                             EGL_DEFINES[ 'EGL_TRUE' ] ]

        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            -1,
                            'EGL_NATIVE_PIXMAP_KHR',
                            eglPixmapIndex,
                            imageAttributes )

        vgImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateEglImageTarget( vgImageIndex,
                                     eglImageIndex )

        OpenVG.DrawImage( vgImageIndex )

        Egl.SwapBuffers( displayIndex,
                         vgSurfaceIndex )
        
        OpenVG.DestroyImage( vgImageIndex )
        
        Egl.DestroyImage( displayIndex,
                          eglImageIndex )
            
        Egl.DestroyPixmap( eglPixmapIndex )

            
