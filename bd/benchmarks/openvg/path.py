#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreatePathBenchmark( Benchmark ):  
    def __init__( self, segments, coordinates ):
        Benchmark.__init__( self)
        self.segments    = segments
        self.coordinates = coordinates
        self.name = "OPENVG create path sgmnts=%d crdntes=%d" % ( self.segments,
                                                                  self.coordinates, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        scale     = 65535.0
        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           self.segments,
                           self.coordinates,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        # Try to ensure the path is actually created.
        OpenVG.Finish()
        
        OpenVG.DestroyPath( pathIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class AppendConcatenatedPathDataBenchmark( Benchmark ):  
    def __init__( self, lines ):
        Benchmark.__init__( self )
        self.lines = lines        
        self.name = "OPENVG append concatenated path data %d lines" % ( self.lines, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale           = 65535.0
        yoffset         = h / float( self.lines );
        lineCoordinates = []
        for i in range( self.lines ):
            lineCoordinates += [ 0,             scale * i * yoffset,
                                 scale * w,     scale * i * yoffset ]

        OpenVG.Lines( pathDataIndex,
                      lineCoordinates,
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           self.lines * 1 + 1,
                           self.lines * 4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.AppendPathData( pathIndex, pathDataIndex )
        OpenVG.ClearPath( pathIndex, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class AppendDiscretePathDataBenchmark( Benchmark ):  
    def __init__( self, lines ):
        Benchmark.__init__( self )
        self.lines = lines
        self.name = "OPENVG append discrete path data %d lines" % ( self.lines, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale     = 65535.0
        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           self.lines * 1 + 1,
                           self.lines * 4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        yoffset = h / float( self.lines );
        for i in range( self.lines ):
            OpenVG.LinesDirect( pathIndex,
                                [ 0,             scale * i * yoffset,
                                  scale * w,     scale * i * yoffset ],
                                'OFF' )
            
        OpenVG.ClosePathDirect( pathIndex )
        OpenVG.ClearPath( pathIndex, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class AppendPathBenchmark( Benchmark ):  
    def __init__( self, lines ):
        Benchmark.__init__( self )
        self.lines = lines
        self.name = "OPENVG append path %d lines" % ( self.lines, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale           = 65535.0
        yoffset         = h / float( self.lines );
        lineCoordinates = []
        for i in range( self.lines ):
            lineCoordinates += [ 0,             scale * i * yoffset,
                                 scale * w,     scale * i * yoffset ]

        OpenVG.Lines( pathDataIndex,
                      lineCoordinates,
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        path1Index = 0
        OpenVG.CreatePath( path1Index,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           self.lines * 1,
                           self.lines * 4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO',
                             'VG_PATH_CAPABILITY_APPEND_FROM' ] )

        path2Index = 1
        OpenVG.CreatePath( path2Index,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           self.lines * 1,
                           self.lines * 4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( path1Index, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.AppendPath( path2Index, path1Index )
        OpenVG.ClearPath( path2Index, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ModifyPathCoordinatesBenchmark( Benchmark ):  
    def __init__( self, lines ):
        Benchmark.__init__( self )
        self.lines = lines
        self.name = "OPENVG modify path coordinates %d lines" % ( self.lines, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        pathData1Index = 0
        pathData2Index = 1
        OpenVG.CreatePathData( pathData1Index, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        OpenVG.CreatePathData( pathData2Index, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale                   = 65535.0
        yoffset                 = h / float( self.lines );
        originalLineCoordinates = []
        modifiedLineCoordinates = []
        for i in range( self.lines ):
            originalLineCoordinates += [ 0,                     scale * i * yoffset,
                                         scale * w,             scale * i * yoffset ]
            modifiedLineCoordinates += [ scale * 1,             scale * ( i * yoffset + 1 ),
                                         scale * ( w + 1 ),     scale * ( i * yoffset + 1 ) ]

        OpenVG.Lines( pathData1Index,
                      originalLineCoordinates,
                      'OFF' )       
        OpenVG.ClosePath( pathData1Index )

        OpenVG.Lines( pathData2Index,
                      modifiedLineCoordinates,
                      'OFF' )
        OpenVG.ClosePath( pathData2Index )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           self.lines * 1 + 1,
                           self.lines * 4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO',
                             'VG_PATH_CAPABILITY_MODIFY' ] )

        OpenVG.AppendPathData( pathIndex, pathData1Index )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.ModifyPathCoords( pathIndex, pathData2Index, 0, self.lines + 1 )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class TransformPathBenchmark( Benchmark ):  
    def __init__( self, lines ):
        Benchmark.__init__( self )
        self.lines = lines
        self.name = "OPENVG transform path %d lines" % ( self.lines, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale           = 65535.0
        yoffset         = h / float( self.lines );
        lineCoordinates = []
        for i in range( self.lines ):
            lineCoordinates += [ 0,                     scale * i * yoffset,
                                 scale * w,             scale * i * yoffset ]

        OpenVG.Lines( pathDataIndex,
                      lineCoordinates,
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        path1Index = 0
        path2Index = 1
        OpenVG.CreatePath( path1Index,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           self.lines * 1 + 1,
                           self.lines * 4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO',
                             'VG_PATH_CAPABILITY_TRANSFORM_FROM' ] )

        OpenVG.AppendPathData( path1Index, pathDataIndex )

        OpenVG.CreatePath( path2Index,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           self.lines * 1 + 1,
                           self.lines * 4,
                           [ 'VG_PATH_CAPABILITY_TRANSFORM_TO' ] )

        OpenVG.LoadIdentity()
        OpenVG.Translate( 10, 10 )
        OpenVG.Scale( 4.2, 2.2 )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, w, h )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.TransformPath( path2Index, path1Index )
        OpenVG.ClearPath( path2Index, [ 'VG_PATH_CAPABILITY_INTERPOLATE_TO' ] )
        
        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class InterpolatePathBenchmark( Benchmark ):  
    def __init__( self, beziers ):
        Benchmark.__init__( self )
        self.beziers = beziers
        self.name = "OPENVG interpolate path %d beziers" % ( self.beziers, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        pathData1Index = 0
        pathData2Index = 1
        OpenVG.CreatePathData( pathData1Index, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        OpenVG.CreatePathData( pathData2Index, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / float( ( self.beziers + 1 ) )
        y       = yoffset
        OpenVG.MoveTo( pathData1Index, [ scale * xoffset, y * scale ], 'OFF' )
        pathCoordinates = []
        for i in range( self.beziers ):
            pathCoordinates += [ scale * ( width / 3 ), scale * ( y - yoffset ),
                                 scale * ( width - xoffset ), scale * y ]
            y += yoffset
            if i < ( self.beziers - 1 ):
                pathCoordinates += [ scale * ( width / 3 ) , scale * ( y + yoffset ),
                                     scale * xoffset, scale * y ]
                
        OpenVG.QuadraticBeziers( pathData1Index,
                                 pathCoordinates,
                                 'OFF' )
        OpenVG.ClosePath( pathData1Index )

        for i in range( len( pathCoordinates ) ):
            pathCoordinates[ i ] *= 2

        OpenVG.MoveTo( pathData2Index, [ scale * xoffset, y * scale ], 'OFF' )
        OpenVG.QuadraticBeziers( pathData2Index,
                                 pathCoordinates,
                                 'OFF' )
        OpenVG.ClosePath( pathData2Index )

        path1Index = 0
        path2Index = 1
        path3Index = 2
        OpenVG.CreatePath( path1Index,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO',
                             'VG_PATH_CAPABILITY_INTERPOLATE_FROM'] )

        OpenVG.CreatePath( path2Index,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO',
                             'VG_PATH_CAPABILITY_INTERPOLATE_FROM'] )

        OpenVG.AppendPathData( path1Index, pathData1Index )
        OpenVG.AppendPathData( path2Index, pathData2Index )

        OpenVG.CreatePath( path3Index,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_INTERPOLATE_TO' ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.InterpolatePath( path3Index, path1Index, path2Index, 0.5 )
        OpenVG.ClearPath( path3Index, [ 'VG_PATH_CAPABILITY_INTERPOLATE_TO' ] )
        
        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class PathLengthBenchmark( Benchmark ):  
    def __init__( self, beziers ):
        Benchmark.__init__( self )
        self.beziers = beziers
        self.name = "OPENVG path length %d beziers" % ( self.beziers, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / float( ( self.beziers + 1 ) )
        y       = yoffset
        OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
        pathCoordinates = []
        for i in range( self.beziers ):
            pathCoordinates += [ scale * ( width / 3 ), scale * ( y - yoffset ),
                                 scale * ( width - xoffset ), scale * y ]
            y += yoffset
            if i < ( self.beziers - 1 ):
                pathCoordinates += [ scale * ( width / 3 ) , scale * ( y + yoffset ),
                                     scale * xoffset, scale * y ]
                
        OpenVG.QuadraticBeziers( pathDataIndex,
                                 pathCoordinates,
                                 'OFF' )
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO',
                             'VG_PATH_CAPABILITY_PATH_LENGTH' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        lengthIndex = 0
        OpenVG.PathLength( pathIndex, 0, self.beziers + 2, lengthIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class PointAlongPathBenchmark( Benchmark ):  
    def __init__( self, beziers ):
        Benchmark.__init__( self )
        self.beziers = beziers
        self.name = "OPENVG point along path %d beziers" % ( self.beziers, )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / float( ( self.beziers + 1 ) )
        y       = yoffset
        OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
        pathCoordinates = []
        for i in range( self.beziers ):
            pathCoordinates += [ scale * ( width / 3 ), scale * ( y - yoffset ),
                                 scale * ( width - xoffset ), scale * y ]
            y += yoffset
            if i < ( self.beziers - 1 ):
                pathCoordinates += [ scale * ( width / 3 ) , scale * ( y + yoffset ),
                                     scale * xoffset, scale * y ]
                
        OpenVG.QuadraticBeziers( pathDataIndex,
                                 pathCoordinates,
                                 'OFF' )
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO',
                             'VG_PATH_CAPABILITY_POINT_ALONG_PATH',
                             'VG_PATH_CAPABILITY_TANGENT_ALONG_PATH' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        distance      = ( self.beziers + 2 ) / 2.0
        xIndex        = 1
        yIndex        = 2
        tangentXIndex = 3
        tangentYIndex = 4
        OpenVG.PointAlongPath( pathIndex,
                               0, self.beziers + 2,
                               distance,
                               'ON', 'ON',
                               xIndex, yIndex,
                               tangentXIndex, tangentYIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class PathBoundsBenchmark( Benchmark ):  
    def __init__( self, beziers  ):
        Benchmark.__init__( self )
        self.beziers = beziers
        self.name = "OPENVG path bounds %d beziers" % ( self.beziers, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / float( ( self.beziers + 1 ) )
        y       = yoffset
        OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
        pathCoordinates = []
        for i in range( self.beziers ):
            pathCoordinates += [ scale * ( width / 3 ), scale * ( y - yoffset ),
                                 scale * ( width - xoffset ), scale * y ]
            y += yoffset
            if i < ( self.beziers - 1 ):
                pathCoordinates += [ scale * ( width / 3 ) , scale * ( y + yoffset ),
                                     scale * xoffset, scale * y ]
                
        OpenVG.QuadraticBeziers( pathDataIndex,
                                 pathCoordinates,
                                 'OFF' )
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO',
                             'VG_PATH_CAPABILITY_PATH_BOUNDS' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        minXIndex       = 1
        minYIndex       = 2
        widthIndex      = 3
        heightIndex     = 4
        OpenVG.PathBounds( pathIndex,
                           minXIndex, minYIndex,
                           widthIndex, heightIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class PathTransformedBoundsBenchmark( Benchmark ):  
    def __init__( self, beziers ):
        Benchmark.__init__( self )
        self.beziers = beziers
        self.name = "OPENVG path transformed bounds %d beziers" % ( self.beziers, )

    def build( self, target, modules ):         
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / float( ( self.beziers + 1 ) )
        y       = yoffset
        OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
        pathCoordinates = []
        for i in range( self.beziers ):
            pathCoordinates += [ scale * ( width / 3 ), scale * ( y - yoffset ),
                                 scale * ( width - xoffset ), scale * y ]
            y += yoffset
            if i < ( self.beziers - 1 ):
                pathCoordinates += [ scale * ( width / 3 ) , scale * ( y + yoffset ),
                                     scale * xoffset, scale * y ]
                
        OpenVG.QuadraticBeziers( pathDataIndex,
                                 pathCoordinates,
                                 'OFF' )
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO',
                             'VG_PATH_CAPABILITY_PATH_TRANSFORMED_BOUNDS' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.LoadIdentity()
        OpenVG.Translate( 1.0, 1.0 )
        OpenVG.Scale( 1.1, 1.2 )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        minXIndex       = 1
        minYIndex       = 2
        widthIndex      = 3
        heightIndex     = 4
        OpenVG.PathTransformedBounds( pathIndex,
                                      minXIndex, minYIndex,
                                      widthIndex, heightIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()

        
