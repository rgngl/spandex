#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class PathDatatypeBenchmark( Benchmark ):  
    def __init__( self, lineCount, pathDatatype ):
        Benchmark.__init__( self)
        self.lineCount    = lineCount
        self.pathDatatype = pathDatatype
        self.name = "OPENVG path data type %s %d lines" % ( pathDataTypeToStr( self.pathDatatype ),
                                                            self.lineCount, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.Stroke( 1, 'VG_CAP_BUTT', 'VG_JOIN_ROUND', 0 )        

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, self.pathDatatype, 1, 1 )

        ranges      = { 'VG_PATH_DATATYPE_S_8'       : 127.0,
                        'VG_PATH_DATATYPE_S_16'      : 32767.0,
                        'VG_PATH_DATATYPE_S_32'      : 65535.0,
                        'VG_PATH_DATATYPE_F'         : 65535.0 }

        rangeScale  = ranges[ self.pathDatatype ]        
        xoffset     = 20
        yoffset     = height / ( self.lineCount + 1 )
        y           = yoffset
        scale       = max( width / rangeScale, height / rangeScale )
        
        paths  = []        
        for i in range( self.lineCount ):
            OpenVG.ClearPathData( pathDataIndex )
            OpenVG.MoveTo( pathDataIndex, [ xoffset / scale, y / scale ], 'OFF' )
            OpenVG.Lines( pathDataIndex,
                          [ ( width - xoffset * 2 ) / scale, 0 ],
                          'ON' )
            OpenVG.ClosePath( pathDataIndex )
            y += yoffset
            
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex,
                               self.pathDatatype,
                               scale,
                               0,
                               3,
                               4,
                               [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            OpenVG.AppendPathData( pathIndex, pathDataIndex )
            paths.append( pathIndex )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for p in paths:
            OpenVG.DrawPath( p, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )

