#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.egl       import *
from common        import *

######################################################################
WINDOW  = 'wndw'
PBUFFER = 'pbffr'

######################################################################
CONFIGS = { 'SCT_COLOR_FORMAT_RGBA5551'  : ( 16, 5, 5, 5, 1 ),
            'SCT_COLOR_FORMAT_RGBA4444'  : ( 16, 4, 4, 4, 4 ),
            'SCT_COLOR_FORMAT_RGB565'    : ( 16, 5, 6, 5, 0 ),
            'SCT_COLOR_FORMAT_RGB888'    : ( 24, 8, 8, 8, 0 ),
            'SCT_COLOR_FORMAT_RGBA8888'  : ( 32, 8, 8, 8, 8 ) }


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PrintEglInfo( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG egl info (debug)"
        self.repeats = 1

    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
               
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.Info( displayIndex )

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreateContextBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG create context"

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )

        attrs = target.getDefaultAttributes( API_OPENVG )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.DestroyContext( displayIndex, contextIndex )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class DummyContextSwitchBenchmark( Benchmark ):  
    def __init__( self, switch, shared, overdraw ):
        Benchmark.__init__( self )
        self.switch    = switch
        self.shared    = shared
        self.overdraw  = overdraw
        self.name = 'OPENVG dummy context switch%s%s ovdrw=%d' % ( { True: '', False: ' skip' }[ self.switch ],
                                                                   { True: ' shared context', False: '' }[ self.shared ],
                                                                   self.overdraw, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )
           
        attrs = target.getDefaultAttributes( API_OPENVG )

        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT'])

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )

        vgContext1Index = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, vgContext1Index, vgConfigIndex, -1, -1 )

        if self.switch:
            vgContext2Index = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
            index = -1
            if self.shared:
                index = vgContext1Index
            Egl.CreateContext( displayIndex, vgContext2Index, vgConfigIndex, index, -1 )
            Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContext2Index )
            OpenVG.ClearColor( [ 0, 0, 1, 1 ] )
            Egl.SwapInterval( displayIndex, 0 )
            
        Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContext1Index )
        Egl.SwapInterval( displayIndex, 0 )
        
        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,               scale * height,
                        scale * width,   scale * height,
                        scale * width,   0,
                        0,               0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )
        
        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        if self.switch:
            Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContext2Index )
            OpenVG.Clear( width / 2 - 10, height / 2 - 10, 20, 20 )
            Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContext1Index )            
            
        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )            

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreateWindowSurfaceBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG create window surface"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        windowIndex = 0
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        Egl.BindApi( 'EGL_OPENVG_API' )

        attrs = target.getDefaultAttributes( API_OPENVG )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )

        Egl.DestroySurface( displayIndex, surfaceIndex )        
        
        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreatePbufferSurfaceBenchmark( Benchmark ):  
    def __init__( self, format ):
        Benchmark.__init__( self )
        self.format = format
        self.name = "OPENVG create pbuffer surface frmt=%s" % pixmapFormatToStr( self.format )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )

        config = CONFIGS[ self.format ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       config[ 0 ] ,
                       config[ 1 ],
                       config[ 2 ],
                       config[ 3 ],
                       config[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CreatePbufferSurface( displayIndex,
                                  surfaceIndex,
                                  configIndex,
                                  width,
                                  height,
                                  'EGL_NO_TEXTURE', 'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',
                                  'EGL_COLORSPACE_sRGB',
                                  'EGL_ALPHA_FORMAT_PRE' )

        Egl.DestroySurface( displayIndex, surfaceIndex )        


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreatePixmapSurfaceBenchmark( Benchmark ):  
    def __init__( self, format ):
        Benchmark.__init__( self )
        self.format = format
        self.name = "OPENVG create pixmap surface frmt=%s" % pixmapFormatToStr( self.format )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex,
                          width, height,
                          self.format,
                          -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENVG_BIT' ]
                       ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex,
                             configIndex,
                             pixmapIndex,
                             attributes )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.CreatePixmapSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 pixmapIndex,
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )
        
        Egl.DestroySurface( displayIndex, surfaceIndex )        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreatePbufferFromImageBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality ):
        Benchmark.__init__( self)
        self.width          = width
        self.height         = height
        self.format         = format
        self.quality        = quality
        self.name = "OPENVG create pbuffer from image %dx%d frmt=%s qlty=%s" % ( self.width,
                                                                                 self.height,
                                                                                 imageFormatToStr( self.format ),
                                                                                 imageQualityToStr( self.quality ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]
        
        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENVG )
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )       
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT'])

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )
       
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        formats = { 'VG_sRGBX_8888'      : ( 24, 8, 8, 8, 0 ),
                    'VG_sRGBA_8888'      : ( 32, 8, 8, 8, 8 ),
                    'VG_sRGBA_8888_PRE'  : ( 32, 8, 8, 8, 8 ),
                    'VG_sRGB_565'        : ( 16, 5, 6, 5, 0 ),
                    'VG_sRGBA_5551'      : ( 16, 5, 5, 5, 1 ),
                    'VG_sRGBA_4444'      : ( 16, 4, 4, 4, 4 ) }

        f = formats[ self.format ]
        
        imageConfigIndex  = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       imageConfigIndex,
                       f[ 0 ],
                       f[ 1 ],
                       f[ 2 ],
                       f[ 3 ],
                       f[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )
        
        imageSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )

        imageContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           imageContextIndex,
                           imageConfigIndex,
                           -1,
                           -1 )

        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )

        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
            
        Egl.CreatePbufferFromClientBufferExt( displayIndex,
                                              imageSurfaceIndex,
                                              imageConfigIndex,
                                              'EGL_OPENVG_IMAGE',
                                              imageIndex,
                                              [] )
        
        Egl.DestroySurface( displayIndex, imageSurfaceIndex )

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class MultisampleResolveBenchmark( Benchmark ):  
    def __init__( self, overdraw, samples, filtering ):
        Benchmark.__init__( self )
        self.overdraw  = overdraw
        self.samples   = samples
        self.filtering = filtering
        self.name = "OPENVG multisample resolve ovdrw=%d smpls=%d fltrng=%s" % ( self.overdraw,
                                                                                 self.samples,
                                                                                 multisampleResolveToStr( self.filtering ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        surfaceBits = [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ]
        if self.filtering != 'EGL_MULTISAMPLE_RESOLVE_DEFAULT':
            bits = { 'EGL_MULTISAMPLE_RESOLVE_BOX' : 'EGL_MULTISAMPLE_RESOLVE_BOX_BIT' }
            surfaceBits += [ bits[ self.filtering ] ]
            
        attrs = target.getDefaultAttributes( API_OPENVG )

        sampleBuffers = 0
        if self.samples > 1:
            sampleBuffers = 1
        
        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       sampleBuffers,
                       self.samples,
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       surfaceBits,
                       [ 'EGL_OPENVG_BIT'])

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           vgConfigIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         vgSurfaceIndex,
                         vgSurfaceIndex,
                         vgContextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        if self.filtering != 'EGL_MULTISAMPLE_RESOLVE_DEFAULT':
            Egl.SurfaceAttrib( displayIndex,
                               vgSurfaceIndex,
                               EGL_DEFINES[ 'EGL_MULTISAMPLE_RESOLVE' ],
                               EGL_DEFINES[ self.filtering ] )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,               scale * height,
                        scale * width,   scale * height,
                        scale * width,   0,
                        0,               0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        if self.samples <= 1:
            OpenVG.RenderingQuality( 'VG_RENDERING_QUALITY_NONANTIALIASED' )
        else:
            OpenVG.RenderingQuality( 'VG_RENDERING_QUALITY_BETTER' )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()
               
        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )            


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class SwapBehaviorBenchmark( Benchmark ):  
    def __init__( self, overdraw, swapBehavior ):
        Benchmark.__init__( self )
        self.overdraw     = overdraw
        self.swapBehavior = swapBehavior
        self.name = "OPENVG swap behavior %s ovrdrw=%d" % ( swapBehaviorToStr( self.swapBehavior ),
                                                            self.overdraw, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        surfaceBits = [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ]
        if self.swapBehavior != 'EGL_BUFFER_DESTROYED':
            bits = { 'EGL_BUFFER_PRESERVED' : 'EGL_SWAP_BEHAVIOR_PRESERVED_BIT' }
            surfaceBits += [ bits[ self.swapBehavior ] ]

        attrs = target.getDefaultAttributes( API_OPENVG )

        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex, vgConfigIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       surfaceBits,
                       [ 'EGL_OPENVG_BIT'])

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           vgConfigIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         vgSurfaceIndex,
                         vgSurfaceIndex,
                         vgContextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        if self.swapBehavior != 'EGL_BUFFER_DESTROYED':        
            Egl.SurfaceAttrib( displayIndex,
                               vgSurfaceIndex,
                               EGL_DEFINES[ 'EGL_SWAP_BEHAVIOR' ],
                               EGL_DEFINES[ self.swapBehavior ] ) 

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,               scale * height,
                        scale * width,   scale * height,
                        scale * width,   0,
                        0,               0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()
               
        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )                 
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class SwapIntervalBenchmark( Benchmark ):  
    def __init__( self, overdraw, swapInterval ):
        Benchmark.__init__( self )
        self.overdraw     = overdraw
        self.swapInterval = swapInterval
        self.name = "OPENVG swap interval ovdrw=%d intrvl=%d" % ( self.overdraw,
                                                                  self.swapInterval, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * h,
                        scale * w,      scale * h,
                        scale * w,      0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        target.swapInterval( state, self.swapInterval )
            
        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        target.swapBuffers( state )


lockClearColor = [ 0.0, 1.0, 0.0, 1.0 ]
lockFormats = { 'EGL_FORMAT_RGB_565_EXACT_KHR'   : 'SCT_COLOR_FORMAT_RGB565',
                'EGL_FORMAT_RGB_565_KHR'         : 'SCT_COLOR_FORMAT_RGB565',
                'EGL_FORMAT_RGBA_8888_EXACT_KHR' : 'SCT_COLOR_FORMAT_RGBA8888',
                'EGL_FORMAT_RGBA_8888_KHR'       : 'SCT_COLOR_FORMAT_RGBA8888' }

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class LockSurfaceV1Benchmark( Benchmark ):  
    def __init__( self, surface, format, optimal, preservePixels, usageHint ):
        Benchmark.__init__( self )
        self.surface        = surface
        self.format         = format
        self.optimal        = optimal
        self.preservePixels = preservePixels
        self.usageHint      = usageHint
        self.name = "OPENVG lock surface v1 %s frmt=%s optml=%s prsrvepxls=%s usghnt=%s" % ( self.surface,
                                                                                             lockSurfaceFormatToString( self.format ),
                                                                                             boolToStr( self.optimal ),
                                                                                             boolToStr( self.preservePixels ),
                                                                                             usageHintToStr( self.usageHint ), )

    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.CheckExtension( displayIndex, 'EGL_KHR_lock_surface' )
        
        surfaceType = EGL_DEFINES[ 'EGL_LOCK_SURFACE_BIT_KHR' ]
        
        if self.surface == WINDOW:
            windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
            Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, w, h, 'SCT_COLOR_FORMAT_DEFAULT' )
            surfaceType = surfaceType | EGL_DEFINES[ 'EGL_WINDOW_BIT' ]
        elif self.surface == PBUFFER:
            surfaceType = surfaceType | EGL_DEFINES[ 'EGL_PBUFFER_BIT' ]
        else:
            raise 'Unexpected surface ' + self.surface
        
        if self.optimal:
            surfaceType = surfaceType | EGL_DEFINES[ 'EGL_OPTIMAL_FORMAT_BIT_KHR' ]

        eglConfigAttributes = [ EGL_DEFINES[ 'EGL_SURFACE_TYPE' ],        surfaceType,
                                EGL_DEFINES[ 'EGL_MATCH_FORMAT_KHR' ],    EGL_DEFINES[ self.format ],
                                EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],     EGL_DEFINES[ 'EGL_DONT_CARE' ],
                                ]
            
        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )           
        Egl.Config( displayIndex, vgConfigIndex, eglConfigAttributes )

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        if self.surface == WINDOW:
            Egl.CreateWindowSurface( displayIndex,
                                     vgSurfaceIndex,
                                     vgConfigIndex,
                                     windowIndex,
                                     'EGL_BACK_BUFFER',
                                     'EGL_VG_COLORSPACE_LINEAR',
                                     'EGL_VG_ALPHA_FORMAT_NONPRE' )
        elif self.surface == PBUFFER:
            Egl.CreatePbufferSurface( displayIndex,
                                      vgSurfaceIndex,
                                      vgConfigIndex,
                                      w,
                                      h,
                                      'EGL_NO_TEXTURE', 'EGL_NO_TEXTURE',
                                      'OFF',
                                      'OFF',
                                      'EGL_COLORSPACE_LINEAR',
                                      'EGL_ALPHA_FORMAT_NONPRE' )

        if self.preservePixels:
            Egl.SurfaceAttrib( displayIndex,
                               vgSurfaceIndex,
                               EGL_DEFINES[ 'EGL_SWAP_BEHAVIOR' ],
                               EGL_DEFINES[ 'EGL_BUFFER_PRESERVED' ] )
        else:
            Egl.SurfaceAttrib( displayIndex,
                               vgSurfaceIndex,
                               EGL_DEFINES[ 'EGL_SWAP_BEHAVIOR' ],
                               EGL_DEFINES[ 'EGL_BUFFER_DESTROYED' ] )
           
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.LockSurface( displayIndex,
                         vgSurfaceIndex,
                         boolToSpandex( self.preservePixels ),
                         self.usageHint )
        Egl.ClearLock1Surface( displayIndex,
                               vgSurfaceIndex,
                               0, 0,
                               w, h,
                               h,
                               self.format,
                               lockClearColor )

        Egl.UnlockSurface( displayIndex,
                           vgSurfaceIndex )

        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class LockSurfaceV2Benchmark( Benchmark ):  
    def __init__( self, surface, format, optimal, preservePixels, usageHint ):
        Benchmark.__init__( self )
        self.surface        = surface
        self.format         = format
        self.optimal        = optimal
        self.preservePixels = preservePixels
        self.usageHint      = usageHint
        self.name = "OPENVG lock surface v2 %s frmt=%s optml=%s prsrvepxls=%s usghnt=%s" % ( self.surface,
                                                                                             lockSurfaceFormatToString( self.format ),
                                                                                             boolToStr( self.optimal ),
                                                                                             boolToStr( self.preservePixels ),
                                                                                             usageHintToStr( self.usageHint ), )

    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.CheckExtension( displayIndex, 'EGL_KHR_lock_surface2' )
        
        surfaceType = EGL_DEFINES[ 'EGL_LOCK_SURFACE_BIT_KHR' ]
        
        if self.surface == WINDOW:
            windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
            Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, w, h, 'SCT_COLOR_FORMAT_DEFAULT' )
            surfaceType = surfaceType | EGL_DEFINES[ 'EGL_WINDOW_BIT' ]
        elif self.surface == PBUFFER:
            surfaceType = surfaceType | EGL_DEFINES[ 'EGL_PBUFFER_BIT' ]
        else:
            raise 'Unexpected surface ' + self.surface
        
        if self.optimal:
            surfaceType = surfaceType | EGL_DEFINES[ 'EGL_OPTIMAL_FORMAT_BIT_KHR' ]

        eglConfigAttributes = [ EGL_DEFINES[ 'EGL_SURFACE_TYPE' ],        surfaceType,
                                EGL_DEFINES[ 'EGL_MATCH_FORMAT_KHR' ],    EGL_DEFINES[ self.format ],
                                EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],     EGL_DEFINES[ 'EGL_DONT_CARE' ],
                                ]
            
        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.Config( displayIndex, vgConfigIndex, eglConfigAttributes )

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        
        if self.surface == WINDOW:
            Egl.CreateWindowSurface( displayIndex,
                                     vgSurfaceIndex,
                                     vgConfigIndex,
                                     windowIndex,
                                     'EGL_BACK_BUFFER',
                                     'EGL_VG_COLORSPACE_LINEAR',
                                     'EGL_VG_ALPHA_FORMAT_NONPRE' )
        elif self.surface == PBUFFER:
            Egl.CreatePbufferSurface( displayIndex,
                                      vgSurfaceIndex,
                                      vgConfigIndex,
                                      w,
                                      h,
                                      'EGL_NO_TEXTURE', 'EGL_NO_TEXTURE',
                                      'OFF',
                                      'OFF',
                                      'EGL_COLORSPACE_LINEAR',
                                      'EGL_ALPHA_FORMAT_NONPRE' )

        if self.preservePixels:
            Egl.SurfaceAttrib( displayIndex,
                               vgSurfaceIndex,
                               EGL_DEFINES[ 'EGL_SWAP_BEHAVIOR' ],
                               EGL_DEFINES[ 'EGL_BUFFER_PRESERVED' ] )
        else:
            Egl.SurfaceAttrib( displayIndex,
                               vgSurfaceIndex,
                               EGL_DEFINES[ 'EGL_SWAP_BEHAVIOR' ],
                               EGL_DEFINES[ 'EGL_BUFFER_DESTROYED' ] )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Egl.LockSurface( displayIndex,
                         vgSurfaceIndex,
                         boolToSpandex( self.preservePixels ),
                         self.usageHint )
        Egl.ClearLock2Surface( displayIndex,
                               vgSurfaceIndex,
                               0, 0,
                               -1, -1,
                               lockClearColor )

        Egl.UnlockSurface( displayIndex,
                           vgSurfaceIndex )

        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class SurfaceScalingBenchmark( Benchmark ):  
    def __init__( self, overdraw, windowSize, surfaceSize, targetExtent ):
        Benchmark.__init__( self )
        self.overdraw     = overdraw
        self.windowSize   = windowSize
        self.surfaceSize  = surfaceSize
        self.targetExtent = targetExtent

        windowSizeStr       = '%dx%d' % ( windowSize[ 0 ], windowSize[ 1 ], )
        if surfaceSize:
            surfaceSizeStr  = '%dx%d' % ( surfaceSize[ 0 ], surfaceSize[ 1 ], )
        if not targetExtent:
            targetExtentStr = 'full window'
        else:
            targetExtentStr = '%d %d %d %d' % ( targetExtent[ 0 ],
                                                targetExtent[ 1 ],
                                                targetExtent[ 2 ],
                                                targetExtent[ 3 ], )
            
        if not self.surfaceSize:
            self.name = "OPENVG surface scaling reference wndsz=%s ovrdrw=%d" % ( windowSizeStr,
                                                                                  self.overdraw, )
        else:
            self.name = "OPENVG surface scaling wndsz=%s srfcsz=%s trgtxtnt=%s ovrdrw=%d" % ( windowSizeStr,
                                                                                              surfaceSizeStr,
                                                                                              targetExtentStr,
                                                                                              self.overdraw, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.CheckExtension( displayIndex, 'EGL_NOK_surface_scaling' )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, self.windowSize[ 0 ], self.windowSize[ 1 ], 'SCT_COLOR_FORMAT_DEFAULT' )

        surfaceBits = EGL_DEFINES[ 'EGL_WINDOW_BIT' ] | EGL_DEFINES[ 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ]
                    
        attrs = target.getDefaultAttributes( API_OPENVG )

        eglConfigAttributes = [ EGL_DEFINES[ 'EGL_BUFFER_SIZE' ],         attrs[ CONFIG_BUFFERSIZE ],
                                EGL_DEFINES[ 'EGL_RED_SIZE' ],            attrs[ CONFIG_REDSIZE ],
                                EGL_DEFINES[ 'EGL_GREEN_SIZE' ],          attrs[ CONFIG_GREENSIZE ],
                                EGL_DEFINES[ 'EGL_BLUE_SIZE' ],           attrs[ CONFIG_BLUESIZE ],
                                EGL_DEFINES[ 'EGL_ALPHA_SIZE' ],          attrs[ CONFIG_ALPHASIZE ],                       
                                EGL_DEFINES[ 'EGL_SURFACE_TYPE' ],        surfaceBits,
                                EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],     EGL_DEFINES[ 'EGL_OPENVG_BIT' ]
                                ]

        if self.surfaceSize:
            eglConfigAttributes += [ EGL_DEFINES[ 'EGL_SURFACE_SCALING_NOK' ], EGL_DEFINES[ 'EGL_TRUE' ] ]

        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )            
        Egl.Config( displayIndex, vgConfigIndex, eglConfigAttributes )

        eglWindowSurfaceAttributes = [ EGL_DEFINES[ 'EGL_RENDER_BUFFER' ], EGL_DEFINES[ 'EGL_BACK_BUFFER' ],
                                       EGL_DEFINES[ 'EGL_COLORSPACE' ],    EGL_DEFINES[ 'EGL_VG_COLORSPACE_sRGB' ],
                                       EGL_DEFINES[ 'EGL_ALPHA_FORMAT' ],  EGL_DEFINES[ 'EGL_VG_ALPHA_FORMAT_PRE' ]
                                       ]
        
        if self.surfaceSize:
            eglWindowSurfaceAttributes +=   [ EGL_DEFINES[ 'EGL_FIXED_WIDTH_NOK' ], self.surfaceSize[ 0 ],
                                              EGL_DEFINES[ 'EGL_FIXED_HEIGHT_NOK' ], self.surfaceSize[ 1 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_OFFSET_X_NOK' ], self.targetExtent[ 0 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_OFFSET_Y_NOK' ], self.targetExtent[ 1 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_WIDTH_NOK' ], self.targetExtent[ 2 ],
                                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_HEIGHT_NOK' ], self.targetExtent[ 3 ],
                                              EGL_DEFINES[ 'EGL_BORDER_COLOR_RED_NOK' ], 127,
                                              EGL_DEFINES[ 'EGL_BORDER_COLOR_GREEN_NOK' ], 127,
                                              EGL_DEFINES[ 'EGL_BORDER_COLOR_BLUE_NOK' ], 127
                                              ]

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )           
        Egl.CreateWindowSurfaceExt( displayIndex,
                                    vgSurfaceIndex,
                                    vgConfigIndex,
                                    windowIndex,
                                    eglWindowSurfaceAttributes )
            
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           vgConfigIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         vgSurfaceIndex,
                         vgSurfaceIndex,
                         vgContextIndex )

        Egl.SwapInterval( displayIndex, 0 )

        if self.surfaceSize:
            w = self.surfaceSize[ 0 ]
            h = self.surfaceSize[ 1 ]
        else:
            w = self.windowSize[ 0 ]
            h = self.windowSize[ 1 ]
        
        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ w / 10, h / 10 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
           
        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,          scale * h,
                        scale * w,  scale * h,
                        scale * w,  0,
                        0,          0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()
               
        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )                 
