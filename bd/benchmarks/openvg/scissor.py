#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ScissorBenchmark( Benchmark ):  
    def __init__( self, overdraw, scissorRects ):
        Benchmark.__init__( self )
        self.overdraw     = overdraw
        self.scissorRects = scissorRects
        self.name = "OPENVG scissor rects=%s ovrdrw=%d " % ( arrayToStr( self.scissorRects ),
                                                             self.overdraw, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 0.3 ] )

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * h,
                        scale * w,      scale * h,
                        scale * w,      0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.Clear( 0, 0, w, h )
        
        if self.scissorRects:
            OpenVG.ScissorRects( self.scissorRects )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.scissorRects:        
            OpenVG.Disable( 'VG_SCISSORING' )
        
        OpenVG.Clear( 0, 0, w, h )

        if self.scissorRects:        
            OpenVG.Enable( 'VG_SCISSORING' )
        
        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ScissorDivBenchmark( Benchmark ):  
    def __init__( self, overdraw, divx, divy ):
        Benchmark.__init__( self )
        self.overdraw = overdraw
        self.divx     = divx
        self.divy     = divy
        self.name = "OPENVG scissor divs=%dx%d ovrdrw=%d" % ( self.divx,
                                                              self.divy,
                                                              self.overdraw, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 0.3 ] )

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * h,
                        scale * w,      scale * h,
                        scale * w,      0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.Clear( 0, 0, w, h )
        
        scissorRects = []
        if self.divx > 0 and self.divy > 0:
            sw = int( w / self.divx )
            sh = int( h / self.divy )
            for y in range( self.divy ):
                for x in range( self.divx ):
                    scissorRects += [ x * sw, y * sh, sw, sh ]

            OpenVG.ScissorRects( scissorRects )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if scissorRects:
            OpenVG.Disable( 'VG_SCISSORING' )
        
        OpenVG.Clear( 0, 0, w, h )

        if scissorRects:        
            OpenVG.Enable( 'VG_SCISSORING' )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ScissoredDrawBenchmark( Benchmark ):  
    def __init__( self, overdraw, rect, useScissor ):
        Benchmark.__init__( self )
        self.overdraw   = overdraw
        self.rect       = rect
        self.useScissor = useScissor
        self.name = "OPENVG scissored draw rect=%s ovrdrw=%d scssr=%s" % ( arrayToStr( self.rect ),
                                                                           self.overdraw,
                                                                           boolToStr( self.useScissor ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 0.3 ] )

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.MoveTo( pathDataIndex,
                       [ scale * self.rect[ 0 ],    scale * self.rect[ 1 ] ],
                       'OFF' )
        OpenVG.Lines( pathDataIndex,
                      [ 0,                          scale * self.rect[ 3 ],
                        scale * self.rect[ 2 ],     0,
                        0,                          -scale * self.rect[ 3 ] ],
                      'ON' )
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.Clear( 0, 0, w, h )

        if self.useScissor:
            OpenVG.ScissorRects( self.rect )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.useScissor:        
            OpenVG.Disable( 'VG_SCISSORING' )
        
        OpenVG.Clear( 0, 0, w, h )

        if self.useScissor:        
            OpenVG.Enable( 'VG_SCISSORING' )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        target.swapBuffers( state )
