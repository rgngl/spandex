#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class RenderingQualityLinesBenchmark( Benchmark ):
    def __init__( self, quality, lineCount, strokeWidth, strokeCapStyle ):
        Benchmark.__init__( self )
        self.quality        = quality
        self.lineCount      = lineCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.name = "OPENVG rendering quality %s %d lines strkwdth=%d strkcpstle=%s" % ( renderingQualityToStr( self.quality ),
                                                                                         self.lineCount,
                                                                                         self.strokeWidth,
                                                                                         strokeCapStyleToStr( self.strokeCapStyle ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.RenderingQuality( self.quality )
        
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.lineCount + 1 )
        y       = yoffset

        pathCoordinates = []
        for i in range( self.lineCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )            
            OpenVG.Lines( pathDataIndex,
                          [ scale * ( width - xoffset ), y * scale ],
                          'OFF' )
            OpenVG.ClosePath( pathDataIndex )
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           2,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class RenderingQualityQuadraticBeziersBenchmark( Benchmark ):
    def __init__( self, quality, bezierCount, strokeWidth, strokeCapStyle ):
        Benchmark.__init__( self )
        self.quality        = quality
        self.bezierCount    = bezierCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.name = "OPENVG rendering quality %s %d quadratic beziers strkwdth=%d strkcpstle=%s" % ( renderingQualityToStr( self.quality ),
                                                                                                     self.bezierCount,
                                                                                                     self.strokeWidth,
                                                                                                     strokeCapStyleToStr( self.strokeCapStyle ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.RenderingQuality( self.quality )
        
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.bezierCount + 1 )
        y       = yoffset

        for i in range( self.bezierCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex,
                                     [ scale * ( width / 3 ),  scale * yoffset / 2,
                                       scale * ( width - xoffset * 2 ),  0 ],                                     
                                     'ON' )
            y += yoffset
            
        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class RenderingQualityCubicBeziersBenchmark( Benchmark ):
    def __init__( self, quality, bezierCount, strokeWidth, strokeCapStyle ):
        Benchmark.__init__( self )
        self.quality        = quality
        self.bezierCount    = bezierCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.name = "OPENVG rendering quality %s %d cubic beziers strkwdth=%d strkcpstle=%s" % ( renderingQualityToStr( self.quality ),
                                                                                                 self.bezierCount,
                                                                                                 self.strokeWidth,
                                                                                                 strokeCapStyleToStr( self.strokeCapStyle ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.RenderingQuality( self.quality )
        
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.bezierCount + 1 )
        y       = yoffset

        for i in range( self.bezierCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
            OpenVG.CubicBeziers( pathDataIndex,
                                 [ scale * ( width / 4 ),            scale * yoffset / 2,
                                   scale * ( width / 4 ),            0, 
                                   scale * ( width - xoffset * 2 ),  0 ],
                                 'ON' )           
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class RenderingQualitySQuadBeziersBenchmark( Benchmark ):
    def __init__( self, quality, bezierCount, strokeWidth, strokeCapStyle ):
        Benchmark.__init__( self )
        self.quality        = quality
        self.bezierCount    = bezierCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.name = "OPENVG rendering quality %s %d squad beziers strkwdth=%d strkcpstle=%s" % ( renderingQualityToStr( self.quality ),
                                                                                                 self.bezierCount,
                                                                                                 self.strokeWidth,
                                                                                                 strokeCapStyleToStr( self.strokeCapStyle ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.RenderingQuality( self.quality )
        
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.bezierCount + 1 )
        y       = yoffset

        for i in range( self.bezierCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
            OpenVG.SQuadBeziers( pathDataIndex,
                                 [ scale * ( width / 3 ),         scale * yoffset / 2,
                                   scale * ( width / 7 * 4 ),     0 ],
                                 'ON' )
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class RenderingQualitySCubicBeziersBenchmark( Benchmark ):
    def __init__( self, quality, bezierCount, strokeWidth, strokeCapStyle ):
        Benchmark.__init__( self )
        self.quality        = quality
        self.bezierCount    = bezierCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.name = "OPENVG rendering quality %s %d scubic beziers strkwdth=%d strkcpstle=%s" % ( renderingQualityToStr( self.quality ),
                                                                                                  self.bezierCount,
                                                                                                  self.strokeWidth,
                                                                                                  strokeCapStyleToStr( self.strokeCapStyle ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.RenderingQuality( self.quality )
        
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.bezierCount + 1 )
        y       = yoffset

        for i in range( self.bezierCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
            OpenVG.SCubicBeziers( pathDataIndex,
                                  [ scale * ( width / 3 ),         scale * yoffset / 2,
                                    scale * ( width - xoffset * 2 ),     0 ],
                                  'ON' )           
            y += yoffset
        
        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class RenderingQualityTigerBenchmark( Benchmark ):  
    def __init__( self, quality, overdraw ):
        Benchmark.__init__( self )
        self.quality    = quality
        self.overdraw   = overdraw
        self.name = "OPENVG rendering quality %s %d tigers" % ( renderingQualityToStr( self.quality ),
                                                                self.overdraw, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.RenderingQuality( self.quality )
        
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        tigerIndex = 0
        OpenVG.CreateTiger( tigerIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            OpenVG.DrawTiger( tigerIndex, 1, width, height )
        
        target.swapBuffers( state )

