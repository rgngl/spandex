#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class StrokeBenchmark( Benchmark ):  
    def __init__( self, overdraw, lineWidth, capStyle, joinStyle, miterLimit ):
        Benchmark.__init__( self )
        self.overdraw   = overdraw
        self.lineWidth  = lineWidth
        self.capStyle   = capStyle
        self.joinStyle  = joinStyle
        self.miterLimit = miterLimit
        self.name = "OPENVG stroke ovdrw=%d lnwdth=%d strkcpstle=%s strkjnstle=%s mtrlmt=%d" % ( self.overdraw,
                                                                                                 self.lineWidth,
                                                                                                 strokeCapStyleToStr( self.capStyle ),
                                                                                                 strokeJoinStyleToStr( self.joinStyle ),
                                                                                                 self.miterLimit, )
    def build( self, target, modules ):         
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.Stroke( self.lineWidth, self.capStyle, self.joinStyle, self.miterLimit )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale    = 65535.0
        elements = 6
        margin   = 10
        offsetX  = ( width - margin * ( elements - 1 ) ) / elements
        offsetY  = ( height - margin * ( elements - 1 ) ) / elements
        OpenVG.Lines( pathDataIndex,
                      [ scale * offsetX,        scale * offsetY,
                        scale * offsetX,        0,
                        0,                      scale * offsetY ],
                      'OFF' )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           3,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            OpenVG.LoadIdentity()
            OpenVG.Translate( margin, margin * 2 )
            for h in range( elements - 1 ):
                for w in range( elements - 1 ):
                    OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
                    OpenVG.Translate( offsetX + margin, 0 )
                OpenVG.Translate( -( w + 1 ) * ( offsetX + margin ), offsetY + margin * 2 )
        
        target.swapBuffers( state )

