#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DashedLinesBenchmark( Benchmark ):
    def __init__( self, lineCount, strokeWidth, strokeCapStyle, patterns, phase, reset  ):
        Benchmark.__init__( self )
        self.lineCount      = lineCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.patterns       = patterns
        self.phase          = phase
        self.reset          = reset
        self.name = "OPENVG %d dashed lines strkwdth=%d strkcpstle=%s pttrns=%s phse=%.2f rset=%s" % ( self.lineCount,
                                                                                                       self.strokeWidth,
                                                                                                       strokeCapStyleToStr( self.strokeCapStyle ),
                                                                                                       arrayToStr( self.patterns ),
                                                                                                       self.phase,
                                                                                                       boolToStr( self.reset ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.lineCount + 1 )
        y       = yoffset

        pathCoordinates = []
        for i in range( self.lineCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )            
            OpenVG.Lines( pathDataIndex,
                          [ scale * ( width - xoffset ), y * scale ],
                          'OFF' )
            OpenVG.ClosePath( pathDataIndex )
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           2,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.patterns:
            r = 'OFF'
            if self.reset:
                r = 'ON'
            OpenVG.DashPattern( self.patterns, self.phase, r )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DashedQuadraticBeziersBenchmark( Benchmark ):
    def __init__( self, bezierCount, strokeWidth, strokeCapStyle, patterns, phase, reset  ):
        Benchmark.__init__( self )
        self.bezierCount    = bezierCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.patterns       = patterns
        self.phase          = phase
        self.reset          = reset
        self.name = "OPENVG %d dashed quadratic beziers strkwdth=%d strkcpstle=%s pttrns=%s phse=%.2f rset=%s" % ( self.bezierCount,
                                                                                                                   self.strokeWidth,
                                                                                                                   strokeCapStyleToStr( self.strokeCapStyle ),
                                                                                                                   arrayToStr( self.patterns ),
                                                                                                                   self.phase,
                                                                                                                   boolToStr( self.reset ), )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.bezierCount + 1 )
        y       = yoffset

        for i in range( self.bezierCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex,
                                     [ scale * ( width / 3 ),  scale * yoffset / 2,
                                       scale * ( width - xoffset * 2 ),  0 ],                                     
                                     'ON' )
            y += yoffset
            
        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.patterns:
            r = 'OFF'
            if self.reset:
                r = 'ON'
            OpenVG.DashPattern( self.patterns, self.phase, r )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DashedCubicBeziersBenchmark( Benchmark ):
    def __init__( self, bezierCount, strokeWidth, strokeCapStyle, patterns, phase, reset  ):
        Benchmark.__init__( self )
        self.bezierCount    = bezierCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.patterns       = patterns
        self.phase          = phase
        self.reset          = reset      
        self.name = "OPENVG %d dashed cubic beziers strkwdth=%d strkcpstle=%s pttrns=%s phse=%.2f rset=%s" % ( self.bezierCount,
                                                                                                               self.strokeWidth,
                                                                                                               strokeCapStyleToStr( self.strokeCapStyle ),
                                                                                                               arrayToStr( self.patterns ),
                                                                                                               self.phase,
                                                                                                               boolToStr( self.reset ), )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.bezierCount + 1 )
        y       = yoffset

        for i in range( self.bezierCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
            OpenVG.CubicBeziers( pathDataIndex,
                                 [ scale * ( width / 4 ),            scale * yoffset / 2,
                                   scale * ( width / 4 ),            0, 
                                   scale * ( width - xoffset * 2 ),  0 ],
                                 'ON' )           
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.patterns:
            r = 'OFF'
            if self.reset:
                r = 'ON'
            OpenVG.DashPattern( self.patterns, self.phase, r )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DashedSQuadBeziersBenchmark( Benchmark ):
    def __init__( self, bezierCount, strokeWidth, strokeCapStyle, patterns, phase, reset  ):
        Benchmark.__init__( self )
        self.bezierCount    = bezierCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.patterns       = patterns
        self.phase          = phase
        self.reset          = reset
        self.name = "OPENVG %d dashed squad beziers strkwdth=%d strkcpstle=%s pttrns=%s phse=%.2f rset=%s" % ( self.bezierCount,
                                                                                                               self.strokeWidth,
                                                                                                               strokeCapStyleToStr( self.strokeCapStyle ),
                                                                                                               arrayToStr( self.patterns ),
                                                                                                               self.phase,
                                                                                                               boolToStr( self.reset ), )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.bezierCount + 1 )
        y       = yoffset

        for i in range( self.bezierCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
            OpenVG.SQuadBeziers( pathDataIndex,
                                 [ scale * ( width / 3 ),         scale * yoffset / 2,
                                   scale * ( width / 7 * 4 ),     0 ],
                                 'ON' )
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.patterns:
            r = 'OFF'
            if self.reset:
                r = 'ON'
            OpenVG.DashPattern( self.patterns, self.phase, r )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DashedSCubicBeziersBenchmark( Benchmark ):
    def __init__( self, bezierCount, strokeWidth, strokeCapStyle, patterns, phase, reset  ):
        Benchmark.__init__( self )
        self.bezierCount    = bezierCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.patterns       = patterns
        self.phase          = phase
        self.reset          = reset
        self.name = "OPENVG %d dashed scubic beziers strkwdth=%d strkcpstle=%s pttrns=%s phse=%.2f rset=%s" % ( self.bezierCount,
                                                                                                                self.strokeWidth,
                                                                                                                strokeCapStyleToStr( self.strokeCapStyle ),
                                                                                                                arrayToStr( self.patterns ),
                                                                                                                self.phase,
                                                                                                                boolToStr( self.reset ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.bezierCount + 1 )
        y       = yoffset

        for i in range( self.bezierCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
            OpenVG.SCubicBeziers( pathDataIndex,
                                  [ scale * ( width / 3 ),         scale * yoffset / 2,
                                    scale * ( width - xoffset * 2 ),     0 ],
                                  'ON' )           
            y += yoffset
        
        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.patterns:
            r = 'OFF'
            if self.reset:
                r = 'ON'
            OpenVG.DashPattern( self.patterns, self.phase, r )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )

