#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class PathCapabilitiesBenchmark( Benchmark ):
    def __init__( self, capabilities, lineCount, strokeWidth, strokeCapStyle ):
        Benchmark.__init__( self )
        self.capabilities   = capabilities
        self.lineCount      = lineCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.name = "OPENVG path capabilities %s %d lines strkwdth=%d strkcpstle=%s" % ( pathCapabilitiesToStr( self.capabilities ),
                                                                                         self.lineCount,
                                                                                         self.strokeWidth,
                                                                                         strokeCapStyleToStr( self.strokeCapStyle ), )
        

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.lineCount + 1 )
        y       = yoffset

        pathCoordinates = []
        for i in range( self.lineCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )            
            OpenVG.Lines( pathDataIndex,
                          [ scale * ( width - xoffset ), y * scale ],
                          'OFF' )
            OpenVG.ClosePath( pathDataIndex )
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           2,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        caps = [ 'VG_PATH_CAPABILITY_APPEND_FROM',
                 'VG_PATH_CAPABILITY_APPEND_TO',
                 'VG_PATH_CAPABILITY_MODIFY',
                 'VG_PATH_CAPABILITY_TRANSFORM_FROM',
                 'VG_PATH_CAPABILITY_TRANSFORM_TO',
                 'VG_PATH_CAPABILITY_INTERPOLATE_FROM',
                 'VG_PATH_CAPABILITY_INTERPOLATE_TO',
                 'VG_PATH_CAPABILITY_PATH_LENGTH',
                 'VG_PATH_CAPABILITY_POINT_ALONG_PATH',
                 'VG_PATH_CAPABILITY_TANGENT_ALONG_PATH',
                 'VG_PATH_CAPABILITY_PATH_BOUNDS',
                 'VG_PATH_CAPABILITY_PATH_TRANSFORMED_BOUNDS' ]

        if 'VG_PATH_CAPABILITY_ALL' not in self.capabilities:
            if not self.capabilities:
                OpenVG.RemovePathCapabilities( pathIndex, [ 'VG_PATH_CAPABILITY_ALL' ] )
            else:
                for c in self.capabilities:
                    caps.remove( c )
                OpenVG.RemovePathCapabilities( pathIndex, caps )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )
