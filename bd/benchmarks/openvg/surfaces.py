#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from lib.egl       import  *
from common        import  *

######################################################################
READ1PIXEL  = 'RD1PXL'
READPIXELS  = 'RDPXLS'
COPYBUFFERS = 'CPBFFRS'
FINISH      = 'FNSH'

CONFIGS     = { 'SCT_COLOR_FORMAT_RGBA5551'  : ( 16, 5, 5, 5, 1, 'VG_sRGBA_5551' ),
                'SCT_COLOR_FORMAT_RGBA4444'  : ( 16, 4, 4, 4, 4, 'VG_sRGBA_4444' ),
                'SCT_COLOR_FORMAT_RGB565'    : ( 16, 5, 6, 5, 0, 'VG_sRGB_565' ),
                'SCT_COLOR_FORMAT_RGB888'    : ( 24, 8, 8, 8, 0, 'VG_sRGBX_8888' ),
                'SCT_COLOR_FORMAT_RGBA8888'  : ( 32, 8, 8, 8, 8, 'VG_sRGBA_8888_PRE' ) }


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class WindowSurfaceBenchmark( Benchmark ):  
    def __init__( self, overdraw, buffer ):
        Benchmark.__init__( self )
        self.overdraw = overdraw
        self.buffer   = buffer
        self.name = "OPENVG window surface ovrdrw=%d bffr=%s" % ( self.overdraw,
                                                                  bufferToStr( self.buffer ), )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]       

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENVG )
               
        configIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 self.buffer,
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,               scale * height,
                        scale * width,   scale * height,
                        scale * width,   0,
                        0,               0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
       
        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        if self.buffer == 'EGL_SINGLE_BUFFER':
            OpenVG.Finish()
        else:
            Egl.SwapBuffers( displayIndex, surfaceIndex )
            

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class PbufferSurfaceBenchmark( Benchmark ):  
    def __init__( self, overdraw, format, flush ):
        Benchmark.__init__( self )
        self.overdraw = overdraw
        self.format   = format
        self.flush    = flush
        self.name = "OPENVG pbuffer surface ovrdrw=%d frmt=%s flsh=%s" % ( self.overdraw,
                                                                           pixmapFormatToStr( self.format ),
                                                                           self.flush, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]       

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )

        config = CONFIGS[ self.format ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       config[ 0 ] ,
                       config[ 1 ],
                       config[ 2 ],
                       config[ 3 ],
                       config[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  surfaceIndex,
                                  configIndex,
                                  width,
                                  height,
                                  'EGL_NO_TEXTURE',
                                  'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',                                  
                                  'EGL_COLORSPACE_sRGB',
                                  'EGL_ALPHA_FORMAT_PRE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * height,
                        scale * width,  scale * height,
                        scale * width,  0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        pixmapIndex    = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        if self.flush == READ1PIXEL:
            OpenVG.CreateNullImageData( imageDataIndex,
                                        config[ 5 ],
                                        1,
                                        1 )
        elif self.flush == READPIXELS:
            OpenVG.CreateNullImageData( imageDataIndex,
                                        config[ 5 ],
                                        width,
                                        height )
        elif self.flush == COPYBUFFERS:
            Egl.CreatePixmap( pixmapIndex,
                              width,
                              height,
                              self.format,
                              -1 )
            
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
       
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
       
        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        if self.flush == READ1PIXEL:
            OpenVG.ReadPixels( imageDataIndex,
                               width / 2, height / 2,
                               1, 1 )
        elif self.flush == READPIXELS:
            OpenVG.ReadPixels( imageDataIndex,
                               0, 0,
                               width, height )
        elif self.flush == COPYBUFFERS:
            Egl.CopyBuffers( displayIndex,
                             surfaceIndex,
                             pixmapIndex )
        elif self.flush == FINISH:
            OpenVG.Finish()
        else:
            raise 'Unexpected flush'

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class PixmapSurfaceBenchmark( Benchmark ):  
    def __init__( self, overdraw, format ):
        Benchmark.__init__( self )
        self.overdraw   = overdraw
        self.format     = format
        self.name = "OPENVG pixmap surface ovrdrw=%d frmt=%s" % ( self.overdraw,
                                                                  pixmapFormatToStr( self.format ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]       

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )

        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex, width, height, self.format, -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENVG_BIT' ]
                       ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex,
                             configIndex,
                             pixmapIndex,
                             attributes )
        
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 pixmapIndex,
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * height,
                        scale * width,  scale * height,
                        scale * width,  0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )
           
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
       
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
       
        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        OpenVG.Finish()


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class SgImageSurfaceBenchmark( Benchmark ):  
    def __init__( self, overdraw, format ):
        Benchmark.__init__( self )
        self.overdraw   = overdraw
        self.format     = format
        self.name = "OPENVG sgimage surface ovrdrw=%d frmt=%s" % ( self.overdraw,
                                                                   pixmapFormatToStr( self.format ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]       

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )

        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )
        
        Egl.BindApi( 'EGL_OPENVG_API' )

        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( pixmapIndex,
                                width, height,
                                self.format,
                                [ 'SCT_USAGE_OPENVG_SURFACE' ],
                                -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENVG_BIT' ]
                       ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex,
                             configIndex,
                             pixmapIndex,
                             attributes )

        alphas = { 'SCT_COLOR_FORMAT_A8'           : 'EGL_ALPHA_FORMAT_NONPRE',
                   'SCT_COLOR_FORMAT_RGB565'       : 'EGL_ALPHA_FORMAT_NONPRE',
                   'SCT_COLOR_FORMAT_RGBX8888'     : 'EGL_ALPHA_FORMAT_NONPRE',
                   'SCT_COLOR_FORMAT_RGBA8888'     : 'EGL_ALPHA_FORMAT_NONPRE',
                   'SCT_COLOR_FORMAT_RGBA8888_PRE' : 'EGL_ALPHA_FORMAT_PRE' }
        
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 pixmapIndex,
                                 'EGL_COLORSPACE_sRGB',
                                 alphas[ self.format ] )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * height,
                        scale * width,  scale * height,
                        scale * width,  0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )
           
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
       
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
       
        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        OpenVG.Finish()


