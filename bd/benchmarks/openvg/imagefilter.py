#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

######################################################################
class ColorMatrixBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality ):
        Benchmark.__init__( self )
        self.width   = width
        self.height  = height
        self.format  = format
        self.quality = quality
        
        self.name = "OPENVG clr mtrix img flter %dx%d frmt=%s qlty=%s" % ( self.width,
                                                                           self.height,
                                                                           imageFormatToStr( self.format ),
                                                                           imageQualityToStr( self.quality ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        image1Index = 0
        image2Index = 1
        OpenVG.CreateImage( image1Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.CreateImage( image2Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.5, 0.5, 1.0, 1.0 ] )
        OpenVG.ClearImage( image1Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( image2Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )        

        OpenVG.ImageQuality( self.quality )
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.ColorMatrix( image2Index,
                            image1Index,
                            [ 1.1, 0.0, 0.0, 0.0, 0.1,
                              0.0, 1.1, 0.0, 0.0, 0.1,
                              0.0, 0.0, 1.1, 0.0, 0.1,
                              0.0, 0.0, 0.0, 1.1, 1.0 ] )
        
        OpenVG.DrawImage( image2Index )
        target.swapBuffers( state )        

        
######################################################################
class ConvolveBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, fsize, tiling ):
        Benchmark.__init__( self )
        self.width   = width
        self.height  = height
        self.format  = format
        self.quality = quality
        self.fsize   = fsize
        self.tiling  = tiling
        
        self.name = "OPENVG cnvlve img flter %d %dx%d frmt=%s qlty=%s tlng=%s" % ( self.fsize,
                                                                                   self.width,
                                                                                   self.height,
                                                                                   imageFormatToStr( self.format ),
                                                                                   imageQualityToStr( self.quality ),
                                                                                   tilingToStr( self.tiling ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        image1Index = 0
        image2Index = 1
        OpenVG.CreateImage( image1Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.CreateImage( image2Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.1, 0.1, 1.0, 1.0 ] )
        OpenVG.ClearImage( image1Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( image2Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )        

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        kernel = []
        for y in range( self.fsize ):
            row = [ 0 ] * self.fsize
            row[ y ] = 1
            kernel += row
        OpenVG.Convolve( image2Index,
                         image1Index,
                         0, 0,
                         kernel,
                         self.fsize, self.fsize,
                         2, 0,
                         self.tiling )

        OpenVG.DrawImage( image2Index )
        target.swapBuffers( state )        
       

######################################################################
class SeparableConvolveBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, fsize, tiling  ):
        Benchmark.__init__( self )
        self.width   = width
        self.height  = height
        self.format  = format
        self.quality = quality
        self.fsize   = fsize
        self.tiling  = tiling
        
        self.name = "OPENVG sprble cnvlve img flter %d %dx%d frmt=%s qlty=%s tlng=%s" % ( self.fsize,
                                                                                          self.width,
                                                                                          self.height,
                                                                                          imageFormatToStr( self.format ),
                                                                                          imageQualityToStr( self.quality ),
                                                                                          tilingToStr( self.tiling ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        image1Index = 0
        image2Index = 1
        OpenVG.CreateImage( image1Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.CreateImage( image2Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.5, 0.5, 1.0, 1.0 ] )
        OpenVG.ClearImage( image1Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( image2Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )        

        OpenVG.ImageQuality( self.quality )                
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        kernel1 = []
        kernel2 = []
        for y in range( self.fsize ):
            row = [ 0 ] * self.fsize
            row[ y ] = 1
            kernel1 += row
            row[ y ] = 1
            kernel2 += row            
        
        OpenVG.SeparableConvolve( image2Index,
                                  image1Index,
                                  0, 0,
                                  kernel1,
                                  kernel2,
                                  self.fsize, self.fsize,
                                  1.3, 0.0,
                                  self.tiling )

        OpenVG.DrawImage( image2Index )
        target.swapBuffers( state )        
        

######################################################################
class GaussianBlurBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, tiling ):
        Benchmark.__init__( self )
        self.width   = width
        self.height  = height
        self.format  = format
        self.quality = quality
        self.tiling  = tiling
        
        self.name = "OPENVG gssian blr img flter (%dx%d frmt=%s qlty=%s tlng=%s" % ( self.width,
                                                                                     self.height,
                                                                                     imageFormatToStr( self.format ),
                                                                                     imageQualityToStr( self.quality ),
                                                                                     tilingToStr( self.tiling ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )

        image1Index = 0
        image2Index = 1
        OpenVG.CreateImage( image1Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.CreateImage( image2Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.5, 0.5, 1.0, 1.0 ] )
        OpenVG.ClearImage( image1Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( image2Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )        

        OpenVG.ImageQuality( self.quality )                        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.GaussianBlur( image2Index,
                             image1Index,
                             1.1, 1.1,
                             self.tiling )

        OpenVG.DrawImage( image2Index )
        target.swapBuffers( state )        
        

######################################################################
class LookupBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, outputLinear, outputPremultiplied ):
        Benchmark.__init__( self )
        self.width               = width
        self.height              = height
        self.format              = format
        self.quality             = quality
        self.outputLinear        = outputLinear
        self.outputPremultiplied = outputPremultiplied
        
        self.name = "OPENVG img flter lkup %dx%d frmt=%s qlty=%s oupttlnr=%s outptprmltpld=%s" % ( self.width,
                                                                                                   self.height,
                                                                                                   imageFormatToStr( self.format ),
                                                                                                   imageQualityToStr( self.quality ),
                                                                                                   boolToStr( self.outputLinear ),
                                                                                                   boolToStr( self.outputPremultiplied ), )
       
    def build( self, target, modules ):
        OpenVG  = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        image1Index = 0
        image2Index = 1
        OpenVG.CreateImage( image1Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.CreateImage( image2Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        lutIndex = 0
        OpenVG.CreateLut( lutIndex, 'SCT_OPENVG_LUT_UBYTE8' )

        lutLength = 256
        lutData   = []
        for i in range( lutLength ):
            lutData += [ str( hex( int( i / float( lutLength ) * lutLength ) ) ) ] 

        OpenVG.LutData( lutIndex, lutData )

        OpenVG.ClearColor( [ 0.5, 0.5, 1.0, 1.0 ] )
        OpenVG.ClearImage( image1Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( image2Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )        

        OpenVG.ImageQuality( self.quality )
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
        
        ls = 'OFF'
        if self.outputLinear:
            ls = 'ON'
        ps = 'OFF'
        if self.outputPremultiplied:
            ps = 'ON'
            
        OpenVG.Lookup( image2Index,
                       image1Index,
                       lutIndex, lutIndex, lutIndex, lutIndex,
                       ls, ps )

        OpenVG.DrawImage( image2Index )
        target.swapBuffers( state )        

        
######################################################################
class LookupSingleBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, outputLinear, outputPremultiplied ):
        Benchmark.__init__( self )
        self.width               = width
        self.height              = height
        self.format              = format
        self.quality             = quality
        self.outputLinear        = outputLinear
        self.outputPremultiplied = outputPremultiplied
        
        self.name = "OPENVG lkup sgle img flter %dx%d frmt=%s qlty=%s outptlnr=%s outptprmltpld=%s" % ( self.width,
                                                                                                        self.height,
                                                                                                        imageFormatToStr( self.format ),
                                                                                                        imageQualityToStr( self.quality ),
                                                                                                        boolToStr( self.outputLinear ),
                                                                                                        boolToStr( self.outputPremultiplied ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        image1Index = 0
        image2Index = 1
        OpenVG.CreateImage( image1Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.CreateImage( image2Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        lutIndex = 0
        OpenVG.CreateLut( lutIndex, 'SCT_OPENVG_LUT_UINT32' )

        lutLength = 256
        lutData = []
        for i in range( lutLength ):
            r = i << 24
            g = i << 16
            b = i << 8
            a = 0xff  
            value = r + g + b + a
            s = str( hex( value ) )
            if s[ -1 ] == 'L':
                s = s[ : -1 ]
            lutData += [ s ] 
           
        OpenVG.LutData( lutIndex, lutData )

        OpenVG.ClearColor( [ 0.1, 0.5, 1.0, 1.0 ] )
        OpenVG.ClearImage( image1Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( image2Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )        

        OpenVG.ImageQuality( self.quality )
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        ls = 'OFF'
        if self.outputLinear:
            ls = 'ON'
        ps = 'OFF'
        if self.outputPremultiplied:
            ps = 'ON'
            
        OpenVG.LookupSingle( image2Index,
                             image1Index,
                             'VG_GREEN',
                             lutIndex,
                             ls, ps )

        OpenVG.DrawImage( image2Index )
        target.swapBuffers( state )        

        
######################################################################
class FilterChannelMaskBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, channels ):
        Benchmark.__init__( self )
        self.width    = width
        self.height   = height
        self.format   = format
        self.quality  = quality 
        self.channels = channels
        
        self.name = "OPENVG flter chnnl msk %s %dx%d frmt=%s qlty=%s" % ( channelMaskToStr( self.channels ),
                                                                          self.width,
                                                                          self.height,
                                                                          imageFormatToStr( self.format ),
                                                                          imageQualityToStr( self.quality ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        image1Index = 0
        image2Index = 1
        OpenVG.CreateImage( image1Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.CreateImage( image2Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        if self.channels:
            OpenVG.FilterChannelMask( self.channels )

        OpenVG.ClearColor( [ 0.5, 0.5, 1.0, 1.0 ] )
        OpenVG.ClearImage( image1Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( image2Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )        

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.GaussianBlur( image2Index,
                             image1Index,
                             1.1, 1.1,
                             'VG_TILE_REPEAT' )

        OpenVG.DrawImage( image2Index )
        target.swapBuffers( state )        
        

######################################################################
class FilterFormatBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, filterFormat ):
        Benchmark.__init__( self )
        self.width        = width
        self.height       = height
        self.format       = format
        self.quality      = quality 
        self.filterFormat = filterFormat
        
        self.name = "OPENVG flter frmt %s %dx%d frmt=%s qlty=%s" % ( filterFormatToStr( self.filterFormat ),
                                                                     self.width,
                                                                     self.height,
                                                                     imageFormatToStr( self.format ),
                                                                     imageQualityToStr( self.quality ), )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        image1Index = 0
        image2Index = 1
        OpenVG.CreateImage( image1Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.CreateImage( image2Index,
                            self.format,
                            self.width,
                            self.height,
                            [ self.quality ] )

        OpenVG.Enable( self.filterFormat )

        OpenVG.ClearColor( [ 0.5, 0.5, 1.0, 1.0 ] )
        OpenVG.ClearImage( image1Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( image2Index, 0, 0, self.width, self.height )
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )        

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.GaussianBlur( image2Index,
                             image1Index,
                             1.1, 1.1,
                             'VG_TILE_REPEAT' )

        OpenVG.DrawImage( image2Index )
        target.swapBuffers( state )        
