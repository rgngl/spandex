#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../..' )

import  bd

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'OpenVG benchmark'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

#----------------------------------------------------------------------
( width, height, )      = target.getScreenSize()

#----------------------------------------------------------------------
import resources
suite.addBenchmark( resources.PrintResourceProfile( 'start' ), 'EGL_FULL' )

#----------------------------------------------------------------------
import info
suite.addBenchmark( info.PrintOpenVGInfo() )

#----------------------------------------------------------------------
import fullscreen
OVERDRAW    = 3
MOSAIC_DIVX = 6
MOSAIC_DIVY = 8
suite.addBenchmark( fullscreen.ClearBenchmark() )
suite.addBenchmark( fullscreen.SynchronizedClearBenchmark() )

suite.addBenchmark( fullscreen.ColorPaintBenchmark( OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.ColorPaintBenchmark( OVERDRAW, 25 ) )
suite.addBenchmark( fullscreen.ColorPaintBenchmark( OVERDRAW, 45 ) )
suite.addBenchmark( fullscreen.ColorPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.ColorPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 45 ) )
suite.addBenchmark( fullscreen.ColorPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.ColorPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 45 ) )

suite.addBenchmark( fullscreen.LinearGradientPaintBenchmark( OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.LinearGradientPaintBenchmark( OVERDRAW, 25 ) )
suite.addBenchmark( fullscreen.LinearGradientPaintBenchmark( OVERDRAW, 45 ) )
suite.addBenchmark( fullscreen.LinearGradientPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.LinearGradientPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY,OVERDRAW, 45 ) )
suite.addBenchmark( fullscreen.LinearGradientPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.LinearGradientPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 45 ) )

suite.addBenchmark( fullscreen.RadialGradientPaintBenchmark( OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.RadialGradientPaintBenchmark( OVERDRAW, 25 ) )
suite.addBenchmark( fullscreen.RadialGradientPaintBenchmark( OVERDRAW, 45 ) )
suite.addBenchmark( fullscreen.RadialGradientPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.RadialGradientPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 45 ) )
suite.addBenchmark( fullscreen.RadialGradientPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.RadialGradientPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 45 ) )

suite.addBenchmark( fullscreen.PatternPaintBenchmark( OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0 ) )
suite.addBenchmark( fullscreen.PatternPaintBenchmark( OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 25 ) )
suite.addBenchmark( fullscreen.PatternPaintBenchmark( OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 45 ) )
suite.addBenchmark( fullscreen.PatternPaintBenchmark( OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', 0 ) )
suite.addBenchmark( fullscreen.PatternPaintBenchmark( OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0 ) )
suite.addBenchmark( fullscreen.PatternPaintBenchmark( OVERDRAW, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0 ) )
suite.addBenchmark( fullscreen.PatternPaintBenchmark( OVERDRAW, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0 ) )
suite.addBenchmark( fullscreen.PatternPaintBenchmark( OVERDRAW, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0 ) )

suite.addBenchmark( fullscreen.PatternPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0 ) )
suite.addBenchmark( fullscreen.PatternPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 45 ) )
suite.addBenchmark( fullscreen.PatternPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', 45 ) )
suite.addBenchmark( fullscreen.PatternPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 45 ) )
suite.addBenchmark( fullscreen.PatternPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0 ) )
suite.addBenchmark( fullscreen.PatternPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 45 ) )
suite.addBenchmark( fullscreen.PatternPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', 0 ) )
suite.addBenchmark( fullscreen.PatternPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0 ) )

#----------------------------------------------------------------------
import paths
suite.addBenchmark( paths.DiscreteLinesBenchmark( 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( paths.ConnectedLinesBenchmark( 32, 10, 'VG_CAP_ROUND' ) )

suite.addBenchmark( paths.DiscreteQuadraticBeziersBenchmark( 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( paths.ConnectedQuadraticBeziersBenchmark( 32, 10, 'VG_CAP_ROUND' ) )

suite.addBenchmark( paths.DiscreteCubicBeziersBenchmark( 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( paths.ConnectedCubicBeziersBenchmark( 32, 10, 'VG_CAP_ROUND' ) )

suite.addBenchmark( paths.DiscreteSQuadBeziersBenchmark( 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( paths.ConnectedSQuadBeziersBenchmark( 32, 10, 'VG_CAP_ROUND' ) )

suite.addBenchmark( paths.DiscreteSCubicBeziersBenchmark( 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( paths.ConnectedSCubicBeziersBenchmark( 32, 10, 'VG_CAP_ROUND' ) )

#----------------------------------------------------------------------
import dashpattern
suite.addBenchmark( dashpattern.DashedLinesBenchmark( 32, 10, 'VG_CAP_SQUARE', [], 0, False ) )
suite.addBenchmark( dashpattern.DashedLinesBenchmark( 32, 10, 'VG_CAP_SQUARE', [ 10, 20, 30, 40 ], 35, False ) )
suite.addBenchmark( dashpattern.DashedLinesBenchmark( 32, 10, 'VG_CAP_SQUARE', [ 10, 20, 30, 40 ], 35, True ) )
suite.addBenchmark( dashpattern.DashedLinesBenchmark( 32, 10, 'VG_CAP_BUTT', [ 10, 20, 30, 40 ], 35, True ) )
suite.addBenchmark( dashpattern.DashedLinesBenchmark( 32, 10, 'VG_CAP_ROUND', [ 10, 20, 30, 40 ], 35, True ) )

suite.addBenchmark( dashpattern.DashedQuadraticBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [], 0, False ) )
suite.addBenchmark( dashpattern.DashedQuadraticBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [ 10, 20, 30, 40 ], 35, False ) )
suite.addBenchmark( dashpattern.DashedQuadraticBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [ 10, 20, 30, 40 ], 35, True ) )
suite.addBenchmark( dashpattern.DashedQuadraticBeziersBenchmark( 32, 10, 'VG_CAP_BUTT', [ 10, 20, 30, 40 ], 35, True ) )
suite.addBenchmark( dashpattern.DashedQuadraticBeziersBenchmark( 32, 10, 'VG_CAP_ROUND', [ 10, 20, 30, 40 ], 35, True ) )

suite.addBenchmark( dashpattern.DashedCubicBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [], 0, False ) )
suite.addBenchmark( dashpattern.DashedCubicBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [ 10, 20, 30, 40 ], 35, False ) )
suite.addBenchmark( dashpattern.DashedCubicBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [ 10, 20, 30, 40 ], 35, True ) )
suite.addBenchmark( dashpattern.DashedCubicBeziersBenchmark( 32, 10, 'VG_CAP_BUTT', [ 10, 20, 30, 40 ], 35, True ) )
suite.addBenchmark( dashpattern.DashedCubicBeziersBenchmark( 32, 10, 'VG_CAP_ROUND', [ 10, 20, 30, 40 ], 35, True ) )

suite.addBenchmark( dashpattern.DashedSQuadBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [], 0, False ) )
suite.addBenchmark( dashpattern.DashedSQuadBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [ 10, 20, 30, 40 ], 35, False ) )
suite.addBenchmark( dashpattern.DashedSQuadBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [ 10, 20, 30, 40 ], 35, True ) )
suite.addBenchmark( dashpattern.DashedSQuadBeziersBenchmark( 32, 10, 'VG_CAP_BUTT', [ 10, 20, 30, 40 ], 35, True ) )
suite.addBenchmark( dashpattern.DashedSQuadBeziersBenchmark( 32, 10, 'VG_CAP_ROUND', [ 10, 20, 30, 40 ], 35, True ) )

suite.addBenchmark( dashpattern.DashedSCubicBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [], 0, False ) )
suite.addBenchmark( dashpattern.DashedSCubicBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [ 10, 20, 30, 40 ], 35, False ) )
suite.addBenchmark( dashpattern.DashedSCubicBeziersBenchmark( 32, 10, 'VG_CAP_SQUARE', [ 10, 20, 30, 40 ], 35, True ) )
suite.addBenchmark( dashpattern.DashedSCubicBeziersBenchmark( 32, 10, 'VG_CAP_BUTT', [ 10, 20, 30, 40 ], 35, True ) )
suite.addBenchmark( dashpattern.DashedSCubicBeziersBenchmark( 32, 10, 'VG_CAP_ROUND', [ 10, 20, 30, 40 ], 35, True ) )

#----------------------------------------------------------------------
import imagequality
OVERDRAW     = 3
suite.addBenchmark( imagequality.ImageQualityBenchmark( width, height, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( imagequality.ImageQualityBenchmark( width, height, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER' ) )
suite.addBenchmark( imagequality.ImageQualityBenchmark( width, height, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( imagequality.ImageQualityBenchmark( width, height, OVERDRAW, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( imagequality.ImageQualityBenchmark( width, height, OVERDRAW, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ) )
suite.addBenchmark( imagequality.ImageQualityBenchmark( width, height, OVERDRAW, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( imagequality.ImageQualityBenchmark( width, height, OVERDRAW, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( imagequality.ImageQualityBenchmark( width, height, OVERDRAW, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_FASTER' ) )
suite.addBenchmark( imagequality.ImageQualityBenchmark( width, height, OVERDRAW, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_BETTER' ) )

#----------------------------------------------------------------------
import renderingquality
suite.addBenchmark( renderingquality.RenderingQualityLinesBenchmark( 'VG_RENDERING_QUALITY_NONANTIALIASED', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualityLinesBenchmark( 'VG_RENDERING_QUALITY_FASTER', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualityLinesBenchmark( 'VG_RENDERING_QUALITY_BETTER', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualityQuadraticBeziersBenchmark( 'VG_RENDERING_QUALITY_NONANTIALIASED', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualityQuadraticBeziersBenchmark( 'VG_RENDERING_QUALITY_FASTER', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualityQuadraticBeziersBenchmark( 'VG_RENDERING_QUALITY_BETTER', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualityCubicBeziersBenchmark( 'VG_RENDERING_QUALITY_NONANTIALIASED', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualityCubicBeziersBenchmark( 'VG_RENDERING_QUALITY_FASTER', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualityCubicBeziersBenchmark( 'VG_RENDERING_QUALITY_BETTER', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualitySQuadBeziersBenchmark( 'VG_RENDERING_QUALITY_NONANTIALIASED', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualitySQuadBeziersBenchmark( 'VG_RENDERING_QUALITY_FASTER', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualitySQuadBeziersBenchmark( 'VG_RENDERING_QUALITY_BETTER', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualitySCubicBeziersBenchmark( 'VG_RENDERING_QUALITY_NONANTIALIASED', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualitySCubicBeziersBenchmark( 'VG_RENDERING_QUALITY_FASTER', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualitySCubicBeziersBenchmark( 'VG_RENDERING_QUALITY_BETTER', 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( renderingquality.RenderingQualityTigerBenchmark( 'VG_RENDERING_QUALITY_NONANTIALIASED', 1 ) )
suite.addBenchmark( renderingquality.RenderingQualityTigerBenchmark( 'VG_RENDERING_QUALITY_FASTER', 1 ) )
suite.addBenchmark( renderingquality.RenderingQualityTigerBenchmark( 'VG_RENDERING_QUALITY_BETTER', 1 ) )

#----------------------------------------------------------------------
import blendmode
OVEDRAW = 3
suite.addBenchmark( blendmode.BlendModeBenchmark( OVEDRAW, 'VG_BLEND_SRC' ) )
suite.addBenchmark( blendmode.BlendModeBenchmark( OVEDRAW, 'VG_BLEND_SRC_OVER' ) )
suite.addBenchmark( blendmode.BlendModeBenchmark( OVEDRAW, 'VG_BLEND_DST_OVER' ) )
suite.addBenchmark( blendmode.BlendModeBenchmark( OVEDRAW, 'VG_BLEND_SRC_IN' ) )
suite.addBenchmark( blendmode.BlendModeBenchmark( OVEDRAW, 'VG_BLEND_DST_IN' ) )
suite.addBenchmark( blendmode.BlendModeBenchmark( OVEDRAW, 'VG_BLEND_MULTIPLY' ) )
suite.addBenchmark( blendmode.BlendModeBenchmark( OVEDRAW, 'VG_BLEND_SCREEN' ) )
suite.addBenchmark( blendmode.BlendModeBenchmark( OVEDRAW, 'VG_BLEND_DARKEN' ) )
suite.addBenchmark( blendmode.BlendModeBenchmark( OVEDRAW, 'VG_BLEND_LIGHTEN' ) )
suite.addBenchmark( blendmode.BlendModeBenchmark( OVEDRAW, 'VG_BLEND_ADDITIVE' ) )

#----------------------------------------------------------------------
import pixellayout
OVERDRAW = 3
suite.addBenchmark( pixellayout.PixelLayoutBenchmark( OVERDRAW, 'VG_PIXEL_LAYOUT_UNKNOWN' ) )
suite.addBenchmark( pixellayout.PixelLayoutBenchmark( OVERDRAW, 'VG_PIXEL_LAYOUT_RGB_VERTICAL' ) )
suite.addBenchmark( pixellayout.PixelLayoutBenchmark( OVERDRAW, 'VG_PIXEL_LAYOUT_BGR_VERTICAL' ) )
suite.addBenchmark( pixellayout.PixelLayoutBenchmark( OVERDRAW, 'VG_PIXEL_LAYOUT_RGB_HORIZONTAL' ) )
suite.addBenchmark( pixellayout.PixelLayoutBenchmark( OVERDRAW, 'VG_PIXEL_LAYOUT_BGR_HORIZONTAL' ) )

#----------------------------------------------------------------------
# Scissoring
import scissor
OVERDRAW = 3
suite.addBenchmark( scissor.ScissorBenchmark( OVERDRAW, [] ) )
suite.addBenchmark( scissor.ScissorBenchmark( OVERDRAW, [0,0,0,0 ] ) )
suite.addBenchmark( scissor.ScissorDivBenchmark( OVERDRAW, 1, 1 ) )
suite.addBenchmark( scissor.ScissorDivBenchmark( OVERDRAW, 1, 2 ) )
suite.addBenchmark( scissor.ScissorDivBenchmark( OVERDRAW, 2, 2 ) )
suite.addBenchmark( scissor.ScissorDivBenchmark( OVERDRAW, 2, 3 ) )
suite.addBenchmark( scissor.ScissorDivBenchmark( OVERDRAW, 3, 3 ) )
suite.addBenchmark( scissor.ScissoredDrawBenchmark( 1, [ 0, 0, width, 100 ], False ) )
suite.addBenchmark( scissor.ScissoredDrawBenchmark( 1, [ 0, 0, width, 100 ], True ) )
suite.addBenchmark( scissor.ScissoredDrawBenchmark( 1, [ 0, 0, width, 200 ], False ) )
suite.addBenchmark( scissor.ScissoredDrawBenchmark( 1, [ 0, 0, width, 200 ], True ) )
suite.addBenchmark( scissor.ScissoredDrawBenchmark( 1, [ 0, 0, width, 300 ], False ) )
suite.addBenchmark( scissor.ScissoredDrawBenchmark( 1, [ 0, 0, width, 300 ], True ) )

#----------------------------------------------------------------------
import tiger
suite.addBenchmark( tiger.TigerBenchmark( [ 100, 100 ] ) )
suite.addBenchmark( tiger.TigerBenchmark( [ 200, 200 ] ) )
suite.addBenchmark( tiger.TigerBenchmark( [ 240, 320 ] ) )
suite.addBenchmark( tiger.TigerBenchmark( [ width, height ] ) )

#----------------------------------------------------------------------
import stroke
OVERDRAW = 3
suite.addBenchmark( stroke.StrokeBenchmark( OVERDRAW, 10, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 20 ) )
suite.addBenchmark( stroke.StrokeBenchmark( OVERDRAW, 10, 'VG_CAP_ROUND', 'VG_JOIN_BEVEL', 20 ) )
suite.addBenchmark( stroke.StrokeBenchmark( OVERDRAW, 10, 'VG_CAP_SQUARE', 'VG_JOIN_BEVEL', 20 ) )
suite.addBenchmark( stroke.StrokeBenchmark( OVERDRAW, 10, 'VG_CAP_BUTT', 'VG_JOIN_MITER', 20 ) )
suite.addBenchmark( stroke.StrokeBenchmark( OVERDRAW, 10, 'VG_CAP_BUTT', 'VG_JOIN_ROUND', 20 ) )

#----------------------------------------------------------------------
import path
SEGMENTS    = 200
COORDINATES = 400
LINES       = 100
BEZIERS     = 100
suite.addBenchmark( path.CreatePathBenchmark( 1, 1 ) )
suite.addBenchmark( path.CreatePathBenchmark( SEGMENTS, COORDINATES ) )
suite.addBenchmark( path.AppendConcatenatedPathDataBenchmark( LINES ) )
suite.addBenchmark( path.AppendDiscretePathDataBenchmark( LINES ) )
suite.addBenchmark( path.AppendPathBenchmark( LINES ) )
suite.addBenchmark( path.ModifyPathCoordinatesBenchmark( LINES ) )
suite.addBenchmark( path.TransformPathBenchmark( LINES ) )
suite.addBenchmark( path.InterpolatePathBenchmark( BEZIERS ) )
suite.addBenchmark( path.PathLengthBenchmark( BEZIERS ) )
suite.addBenchmark( path.PointAlongPathBenchmark( BEZIERS ) )
suite.addBenchmark( path.PathBoundsBenchmark( BEZIERS ) )
suite.addBenchmark( path.PathTransformedBoundsBenchmark( BEZIERS ) )

#----------------------------------------------------------------------
import pathdatatype
suite.addBenchmark( pathdatatype.PathDatatypeBenchmark( 32, 'VG_PATH_DATATYPE_S_8' ) )
suite.addBenchmark( pathdatatype.PathDatatypeBenchmark( 32, 'VG_PATH_DATATYPE_S_16' ) )
suite.addBenchmark( pathdatatype.PathDatatypeBenchmark( 32, 'VG_PATH_DATATYPE_S_32' ) )
suite.addBenchmark( pathdatatype.PathDatatypeBenchmark( 32, 'VG_PATH_DATATYPE_F' ) )

#----------------------------------------------------------------------
import pathcapabilities
suite.addBenchmark( pathcapabilities.PathCapabilitiesBenchmark( [ 'VG_PATH_CAPABILITY_ALL' ], 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( pathcapabilities.PathCapabilitiesBenchmark( [ 'VG_PATH_CAPABILITY_MODIFY' ], 32, 10, 'VG_CAP_ROUND' ) )
suite.addBenchmark( pathcapabilities.PathCapabilitiesBenchmark( [], 32, 10, 'VG_CAP_ROUND' ) )

#----------------------------------------------------------------------
import paint
suite.addBenchmark( paint.CreatePaintBenchmark() )

suite.addBenchmark( paint.ColorPaintBenchmark( False, False, False ) )
suite.addBenchmark( paint.ColorPaintBenchmark( False, False, True ) )
suite.addBenchmark( paint.ColorPaintBenchmark( True, False, False ) )
suite.addBenchmark( paint.ColorPaintBenchmark( True, True, False ) )
suite.addBenchmark( paint.ColorPaintBenchmark( True, True, True ) )

suite.addBenchmark( paint.LinearGradientPaintBenchmark( 2, False, False, False, 0 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 2, False, False, True, 0 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 2, True, False, False, 0 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 2, True, True, False, 0 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 2, True, True, True, 0 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 2, True, True, True, 45 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 8, False, False, False, 0 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 8, False, False, True, 0 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 8, True, False, False, 0 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 8, True, True, False, 0 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 8, True, True, True, 0 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 8, True, True, True, 45 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 16, True, True, True, 0 ) )
suite.addBenchmark( paint.LinearGradientPaintBenchmark( 32, True, True, True, 0 ) )

suite.addBenchmark( paint.RadialGradientPaintBenchmark( 2, False, False, False ) )
suite.addBenchmark( paint.RadialGradientPaintBenchmark( 2, False, False, True ) )
suite.addBenchmark( paint.RadialGradientPaintBenchmark( 2, True, False, False ) )
suite.addBenchmark( paint.RadialGradientPaintBenchmark( 2, True, True, False ) )
suite.addBenchmark( paint.RadialGradientPaintBenchmark( 2, True, True, True ) )
suite.addBenchmark( paint.RadialGradientPaintBenchmark( 8, False, False, False ) )
suite.addBenchmark( paint.RadialGradientPaintBenchmark( 8, False, False, True ) )
suite.addBenchmark( paint.RadialGradientPaintBenchmark( 8, True, False, False ) )
suite.addBenchmark( paint.RadialGradientPaintBenchmark( 8, True, True, False ) )
suite.addBenchmark( paint.RadialGradientPaintBenchmark( 8, True, True, True ) )
suite.addBenchmark( paint.RadialGradientPaintBenchmark( 16, True, True, True ) )
suite.addBenchmark( paint.RadialGradientPaintBenchmark( 32, True, True, True ) )

suite.addBenchmark( paint.PatternPaintBenchmark( 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False, False, False ) )
suite.addBenchmark( paint.PatternPaintBenchmark( 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False, False, True ) )
suite.addBenchmark( paint.PatternPaintBenchmark( 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', True, False, False ) )
suite.addBenchmark( paint.PatternPaintBenchmark( 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', True, True, False ) )
suite.addBenchmark( paint.PatternPaintBenchmark( 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', True, True, True ) )
suite.addBenchmark( paint.PatternPaintBenchmark( 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED', False, False, False ) )
suite.addBenchmark( paint.PatternPaintBenchmark( 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED', False, False, True ) )
suite.addBenchmark( paint.PatternPaintBenchmark( 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED', True, False, False ) )
suite.addBenchmark( paint.PatternPaintBenchmark( 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED', True, True, False ) )
suite.addBenchmark( paint.PatternPaintBenchmark( 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED', True, True, True ) )

#----------------------------------------------------------------------
import pathreuse
suite.addBenchmark( pathreuse.LinesReuseBenchmark( 32, 10, 'VG_CAP_SQUARE', pathreuse.CONST ) )
suite.addBenchmark( pathreuse.LinesReuseBenchmark( 32, 10, 'VG_CAP_SQUARE', pathreuse.CREATE ) )
suite.addBenchmark( pathreuse.LinesReuseBenchmark( 32, 10, 'VG_CAP_SQUARE', pathreuse.CLEAR ) )
suite.addBenchmark( pathreuse.LinesReuseBenchmark( 32, 10, 'VG_CAP_SQUARE', pathreuse.MODIFY ) )

suite.addBenchmark( pathreuse.ListboxReuseBenchmark( 32, pathreuse.CONST ) )
suite.addBenchmark( pathreuse.ListboxReuseBenchmark( 32, pathreuse.CREATE ) )
suite.addBenchmark( pathreuse.ListboxReuseBenchmark( 32, pathreuse.CLEAR ) )
suite.addBenchmark( pathreuse.ListboxReuseBenchmark( 32, pathreuse.MODIFY ) )

#----------------------------------------------------------------------
import image
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ) )
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', False ) )
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', True ) )
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', False ) )
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', True ) )
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ) )
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_A_8', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )
suite.addBenchmark( image.CreateImageBenchmark( width, height, 'VG_A_8', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ) )

suite.addBenchmark( image.ClearImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.ClearImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER' ) )
suite.addBenchmark( image.ClearImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.ClearImageBenchmark( width, height, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.ClearImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.ClearImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )

suite.addBenchmark( image.ImageSubDataBenchmark( 32, 32, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.ImageSubDataBenchmark( 64, 64, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.ImageSubDataBenchmark( 128, 128, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.ImageSubDataBenchmark( 256, 256, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )

suite.addBenchmark( image.ImageSubDataBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.ImageSubDataBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER' ) )
suite.addBenchmark( image.ImageSubDataBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.ImageSubDataBenchmark( width, height, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.ImageSubDataBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.ImageSubDataBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )

suite.addBenchmark( image.GetImageSubDataBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.GetImageSubDataBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER' ) )
suite.addBenchmark( image.GetImageSubDataBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.GetImageSubDataBenchmark( width, height, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.GetImageSubDataBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.GetImageSubDataBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )

suite.addBenchmark( image.ChildImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.ChildImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER' ) )
suite.addBenchmark( image.ChildImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.ChildImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )

suite.addBenchmark( image.CopyImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )
suite.addBenchmark( image.CopyImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', False ) )
suite.addBenchmark( image.CopyImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', False ) )
suite.addBenchmark( image.CopyImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', True ) )
suite.addBenchmark( image.CopyImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )

suite.addBenchmark( image.SetPixelsBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.SetPixelsBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER' ) )
suite.addBenchmark( image.SetPixelsBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.SetPixelsBenchmark( width, height, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.SetPixelsBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.SetPixelsBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )

suite.addBenchmark( image.WritePixelsBenchmark( width, height, 'VG_sRGBX_8888' ) )
suite.addBenchmark( image.WritePixelsBenchmark( width, height, 'VG_sRGBA_8888' ) )
suite.addBenchmark( image.WritePixelsBenchmark( width, height, 'VG_sRGB_565' ) )
suite.addBenchmark( image.WritePixelsBenchmark( width, height, 'VG_sRGBA_8888_PRE' ) )
suite.addBenchmark( image.WritePixelsBenchmark( width, height, 'VG_lRGBX_8888' ) )
suite.addBenchmark( image.WritePixelsBenchmark( width, height, 'VG_lRGBA_8888' ) )
suite.addBenchmark( image.WritePixelsBenchmark( width, height, 'VG_lRGBA_8888_PRE' ) )

suite.addBenchmark( image.ReadPixelsBenchmark( 1, 1, 'VG_sRGBA_8888' ) )
suite.addBenchmark( image.ReadPixelsBenchmark( 1, 1, 'VG_sRGBA_8888_PRE' ) )
suite.addBenchmark( image.ReadPixelsBenchmark( 32, 32, 'VG_sRGBA_8888' ) )
suite.addBenchmark( image.ReadPixelsBenchmark( 64, 64, 'VG_sRGBA_8888' ) )
suite.addBenchmark( image.ReadPixelsBenchmark( 128, 128, 'VG_sRGBA_8888' ) )
suite.addBenchmark( image.ReadPixelsBenchmark( 256, 256, 'VG_sRGBA_8888' ) )

suite.addBenchmark( image.ReadPixelsBenchmark( width, height, 'VG_sRGBA_8888' ) )
suite.addBenchmark( image.ReadPixelsBenchmark( width, height, 'VG_sRGBX_8888' ) )
suite.addBenchmark( image.ReadPixelsBenchmark( width, height, 'VG_sRGBA_8888_PRE' ) )
suite.addBenchmark( image.ReadPixelsBenchmark( width, height, 'VG_sRGB_565' ) )

suite.addBenchmark( image.GetPixelsBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.GetPixelsBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER' ) )
suite.addBenchmark( image.GetPixelsBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.GetPixelsBenchmark( width, height, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.GetPixelsBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.GetPixelsBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_BETTER' ) )

suite.addBenchmark( image.CopyPixelsBenchmark( 0, 0, width / 2, 0, width / 2, height, False ) )
suite.addBenchmark( image.CopyPixelsBenchmark( 0, 0, 0, height / 2, width, height / 2, False ) )
suite.addBenchmark( image.CopyPixelsBenchmark( width / 2, 0, 0, 0, width / 2, height, False ) )
suite.addBenchmark( image.CopyPixelsBenchmark( 0, height / 2, 0, 0, width, height / 2, False ) )
suite.addBenchmark( image.CopyPixelsBenchmark( 0, 0, 0, 10, width, height - 10, False ) )
suite.addBenchmark( image.CopyPixelsBenchmark( 0, 0, 10, 0, width - 10, height, False ) )
suite.addBenchmark( image.CopyPixelsBenchmark( 0, 0, 0, 10, width, height - 10, True ) )
suite.addBenchmark( image.CopyPixelsBenchmark( 0, 0, 10, 0, width - 10, height, True ) )
suite.addBenchmark( image.CopyPixelsBenchmark( 0, 0, 0, 0, width, height, False ) )

suite.addBenchmark( image.DrawImageBenchmark( 16, 16, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( 32, 32, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( 64, 64, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( 128, 128, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( 256, 256, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( 512, 512, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0, 1 ) )

suite.addBenchmark( image.DrawImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', 0, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( width, height, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( width, height, 'VG_sRGBA_8888','VG_IMAGE_QUALITY_BETTER', 45, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 90, 1 ) )

suite.addBenchmark( image.DrawImageBenchmark( width / 2, height / 2, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0, 1 ) )
suite.addBenchmark( image.DrawImageBenchmark( width / 3, height / 3, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0, 1 ) )
s1 = 3.0 / 4.0
s2 = 4.0 / 3.0
suite.addBenchmark( image.DrawImageBenchmark( int( width / s1 ), int( height / s1 ), 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0, s1 ) )  
suite.addBenchmark( image.DrawImageBenchmark( int( width / s1 ), int( height / s1 ), 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', 0, s1 ) )  
suite.addBenchmark( image.DrawImageBenchmark( int( width / s1 ), int( height / s1 ), 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0, s1 ) )
suite.addBenchmark( image.DrawImageBenchmark( int( width / s2 ), int( height / s2 ), 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0, s2 ) )  
suite.addBenchmark( image.DrawImageBenchmark( int( width / s2 ), int( height / s2 ), 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', 0, s2 ) )  
suite.addBenchmark( image.DrawImageBenchmark( int( width / s2 ), int( height / s2 ), 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 0, s2 ) )  

suite.addBenchmark( image.DrawChildImageBenchmark( width, height, width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.DrawChildImageBenchmark( width, height, width / 2, height / 2, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.DrawChildImageBenchmark( width, height, width / 3, height / 3, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )

suite.addBenchmark( image.CreateAndDrawChildImageBenchmark( width, height, width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.CreateAndDrawChildImageBenchmark( width, height, width / 2, height / 2, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.CreateAndDrawChildImageBenchmark( width, height, width / 3, height / 3, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )

suite.addBenchmark( image.CreateAndDrawScaledChildImageBenchmark( width, height, width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.CreateAndDrawScaledChildImageBenchmark( width, height, width / 2, height / 2, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.CreateAndDrawScaledChildImageBenchmark( width, height, width / 3, height / 3, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.CreateAndDrawScaledChildImageBenchmark( width, height, width / 3, height / 3, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER' ) )
suite.addBenchmark( image.CreateAndDrawScaledChildImageBenchmark( width, height, width / 3, height / 3, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )

suite.addBenchmark( image.ImageSubDataWithDrawBenchmark( width, height, 1, 1, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )        
suite.addBenchmark( image.ImageSubDataWithDrawBenchmark( width, height, 1, 1, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', False ) )        
suite.addBenchmark( image.ImageSubDataWithDrawBenchmark( width, height, 1, 1, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', False ) )        
suite.addBenchmark( image.ImageSubDataWithDrawBenchmark( width, height, 2, 2, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', False ) )        
suite.addBenchmark( image.ImageSubDataWithDrawBenchmark( width, height, 8, 4, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', False ) )        
suite.addBenchmark( image.ImageSubDataWithDrawBenchmark( width, height, 8, 4, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', True ) )         
suite.addBenchmark( image.ImageSubDataWithDrawBenchmark( width, height, 16, 8, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', False ) )       
suite.addBenchmark( image.ImageSubDataWithDrawBenchmark( width, height, 16, 8, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', False ) )       
suite.addBenchmark( image.ImageSubDataWithDrawBenchmark( width, height, 16, 8, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', True ) )        

suite.addBenchmark( image.DrawImageSubRegionsBenchmark( width, height, 1, 1, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.DrawImageSubRegionsBenchmark( width, height, 2, 2, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.DrawImageSubRegionsBenchmark( width, height, 3, 3, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.DrawImageSubRegionsBenchmark( width, height, 6, 4, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.DrawImageSubRegionsBenchmark( width, height, 6, 6, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )

suite.addBenchmark( image.DrawOverlappingImageSubRegionsBenchmark( width, height, 1, 1, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.DrawOverlappingImageSubRegionsBenchmark( width, height, 2, 2, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.DrawOverlappingImageSubRegionsBenchmark( width, height, 3, 3, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.DrawOverlappingImageSubRegionsBenchmark( width, height, 6, 4, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( image.DrawOverlappingImageSubRegionsBenchmark( width, height, 6, 6, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ) )

suite.addBenchmark( image.ImageModeBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 'VG_DRAW_IMAGE_NORMAL' ) )
suite.addBenchmark( image.ImageModeBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 'VG_DRAW_IMAGE_MULTIPLY' ) )
suite.addBenchmark( image.ImageModeBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 'VG_DRAW_IMAGE_STENCIL' ) )

suite.addBenchmark( image.TextAtlasBenchmark( 512, 512, 4, 4, 1, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ) )
suite.addBenchmark( image.TextAtlasBenchmark( 512, 512, 8, 8, 1, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ) )
suite.addBenchmark( image.TextAtlasBenchmark( 512, 512, 16, 16, 1, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ) )
suite.addBenchmark( image.TextAtlasBenchmark( 512, 512, 32, 32, 1, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ) )
suite.addBenchmark( image.TextAtlasBenchmark( 512, 512, 4, 4, 1, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )
suite.addBenchmark( image.TextAtlasBenchmark( 512, 512, 8, 8, 1, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )
suite.addBenchmark( image.TextAtlasBenchmark( 512, 512, 16, 16, 1, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )
suite.addBenchmark( image.TextAtlasBenchmark( 512, 512, 32, 32, 1, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )

#----------------------------------------------------------------------
import imagefilter
suite.addBenchmark( imagefilter.ColorMatrixBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( imagefilter.ConvolveBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 3, 'VG_TILE_PAD' ) )
suite.addBenchmark( imagefilter.SeparableConvolveBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 3, 'VG_TILE_PAD'  ) )
suite.addBenchmark( imagefilter.GaussianBlurBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_TILE_PAD' ) )
suite.addBenchmark( imagefilter.LookupBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False, True ) )
suite.addBenchmark( imagefilter.LookupBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', True, False ) )
suite.addBenchmark( imagefilter.LookupBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', True, True ) )
suite.addBenchmark( imagefilter.LookupSingleBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False, True ) )
suite.addBenchmark( imagefilter.LookupSingleBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', True, False ) )
suite.addBenchmark( imagefilter.LookupSingleBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', True, True ) )

suite.addBenchmark( imagefilter.FilterChannelMaskBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', [] ) )
suite.addBenchmark( imagefilter.FilterChannelMaskBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', [ 'VG_RED' ] ) )
suite.addBenchmark( imagefilter.FilterChannelMaskBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', [ 'VG_RED', 'VG_GREEN', 'VG_BLUE' ] ) )
suite.addBenchmark( imagefilter.FilterFormatBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_FILTER_FORMAT_LINEAR' ) )
suite.addBenchmark( imagefilter.FilterFormatBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_FILTER_FORMAT_PREMULTIPLIED' ) )

#----------------------------------------------------------------------
import masking
# OpenVG 1.0
suite.addBenchmark( masking.RenderUnmaskedBenchmark( 32, 10, 'VG_CAP_SQUARE' ) )
suite.addBenchmark( masking.RenderWithImageMaskBenchmark( 32, 10, 'VG_CAP_SQUARE', 'VG_A_1', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( masking.RenderWithImageMaskBenchmark( 32, 10, 'VG_CAP_SQUARE', 'VG_A_8', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
# OpenVG 1.1
suite.addBenchmark( masking.RenderToMaskBenchmark( 32, 10, 'VG_CAP_SQUARE', 'VG_SET_MASK' ) )
suite.addBenchmark( masking.RenderToMaskBenchmark( 32, 10, 'VG_CAP_SQUARE', 'VG_UNION_MASK' ) )
suite.addBenchmark( masking.RenderToMaskBenchmark( 32, 10, 'VG_CAP_SQUARE', 'VG_INTERSECT_MASK' ) )
suite.addBenchmark( masking.RenderToMaskBenchmark( 32, 10, 'VG_CAP_SQUARE', 'VG_SUBTRACT_MASK' ) )
suite.addBenchmark( masking.RenderWithMaskBenchmark( 32, 10, 'VG_CAP_SQUARE' ) )
suite.addBenchmark( masking.RenderWithDynamicMaskBenchmark( 32, 10, 'VG_CAP_SQUARE' ) )
suite.addBenchmark( masking.CreateMaskLayerBenchmark() )
suite.addBenchmark( masking.FillMaskLayerBenchmark() )
suite.addBenchmark( masking.CopyMaskLayerBenchmark() )

#----------------------------------------------------------------------
import font
suite.addBenchmark( font.FontCreationBenchmark() )
suite.addBenchmark( font.GlyphToPathBenchmark() )
suite.addBenchmark( font.DrawPathBenchmark() )
suite.addBenchmark( font.DrawGlyphBenchmark() )
suite.addBenchmark( font.DrawPathsBenchmark() )
suite.addBenchmark( font.DrawGlyphsBenchmark() )

#----------------------------------------------------------------------
import colortransform   # REQUIRES OPENVG 1.1
suite.addBenchmark( colortransform.ColorTransformBenchmark( 3, False ) )
suite.addBenchmark( colortransform.ColorTransformBenchmark( 3, True ) )

suite.addBenchmark( colortransform.ColorTransformDrawImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )
suite.addBenchmark( colortransform.ColorTransformDrawImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ) )
suite.addBenchmark( colortransform.ColorTransformDrawImageBenchmark( width, height, 'VG_A_1', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ) )
suite.addBenchmark( colortransform.ColorTransformDrawImageBenchmark( width, height, 'VG_A_1', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ) )

#----------------------------------------------------------------------
import matrix
suite.addBenchmark( matrix.LoadMatrixBenchmark( 'VG_MATRIX_PATH_USER_TO_SURFACE' ) )
suite.addBenchmark( matrix.LoadMatrixBenchmark( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' ) )
suite.addBenchmark( matrix.MultMatrixBenchmark( 'VG_MATRIX_PATH_USER_TO_SURFACE' ) )

#----------------------------------------------------------------------
import rendertoimage    # REQUIRES EGL
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', False ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', False ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_FASTER', False ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_BETTER', False ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_FASTER', False ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_BETTER', False ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', True ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, 32, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ), 'EGL_FULL' )
suite.addBenchmark( rendertoimage.RenderToImageBenchmark( width, 16, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', False ), 'EGL_FULL' )

#----------------------------------------------------------------------
import surfaces         # REQUIRES EGL
OVERDRAW = 3
suite.addBenchmark( surfaces.WindowSurfaceBenchmark( OVERDRAW, 'EGL_SINGLE_BUFFER' ), 'EGL_FULL' )
suite.addBenchmark( surfaces.WindowSurfaceBenchmark( OVERDRAW, 'EGL_BACK_BUFFER' ), 'EGL_FULL' )

suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGBA8888', surfaces.READ1PIXEL ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGBA8888', surfaces.READPIXELS ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGBA8888', surfaces.COPYBUFFERS ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGBA8888', surfaces.FINISH ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB565', surfaces.READ1PIXEL ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB565', surfaces.READPIXELS ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB565', surfaces.COPYBUFFERS ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB565', surfaces.FINISH ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB888', surfaces.READ1PIXEL ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB888', surfaces.READPIXELS ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB888', surfaces.COPYBUFFERS ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB888', surfaces.FINISH ), 'EGL_FULL' )

suite.addBenchmark( surfaces.PixmapSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PixmapSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( surfaces.PixmapSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_FULL' )

suite.addBenchmark( surfaces.SgImageSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGBA8888_PRE' ), 'EGL_FULL' )
suite.addBenchmark( surfaces.SgImageSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_FULL' )
suite.addBenchmark( surfaces.SgImageSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_FULL' )

#----------------------------------------------------------------------
import surfacealpha     # REQUIRES EGL
OVERDRAW = 3 
suite.addBenchmark( surfacealpha.FillBenchmark( OVERDRAW, 'EGL_VG_COLORSPACE_sRGB', 'EGL_VG_ALPHA_FORMAT_PRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.FillBenchmark( OVERDRAW, 'EGL_VG_COLORSPACE_LINEAR', 'EGL_VG_ALPHA_FORMAT_PRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.FillBenchmark( OVERDRAW, 'EGL_VG_COLORSPACE_sRGB', 'EGL_VG_ALPHA_FORMAT_NONPRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.FillBenchmark( OVERDRAW, 'EGL_VG_COLORSPACE_LINEAR', 'EGL_VG_ALPHA_FORMAT_NONPRE' ), 'EGL_FULL' )

suite.addBenchmark( surfacealpha.TigerBenchmark( 'EGL_VG_COLORSPACE_sRGB', 'EGL_VG_ALPHA_FORMAT_PRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.TigerBenchmark( 'EGL_VG_COLORSPACE_LINEAR', 'EGL_VG_ALPHA_FORMAT_PRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.TigerBenchmark( 'EGL_VG_COLORSPACE_sRGB', 'EGL_VG_ALPHA_FORMAT_NONPRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.TigerBenchmark( 'EGL_VG_COLORSPACE_LINEAR', 'EGL_VG_ALPHA_FORMAT_NONPRE' ), 'EGL_FULL' )

suite.addBenchmark( surfacealpha.ImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'EGL_VG_COLORSPACE_sRGB', 'EGL_VG_ALPHA_FORMAT_PRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.ImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'EGL_VG_COLORSPACE_LINEAR', 'EGL_VG_ALPHA_FORMAT_PRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.ImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'EGL_VG_COLORSPACE_sRGB', 'EGL_VG_ALPHA_FORMAT_NONPRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.ImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'EGL_VG_COLORSPACE_sRGB', 'EGL_VG_ALPHA_FORMAT_PRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.ImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'EGL_VG_COLORSPACE_LINEAR', 'EGL_VG_ALPHA_FORMAT_PRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.ImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'EGL_VG_COLORSPACE_sRGB', 'EGL_VG_ALPHA_FORMAT_NONPRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.ImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER', 'EGL_VG_COLORSPACE_sRGB', 'EGL_VG_ALPHA_FORMAT_PRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.ImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER', 'EGL_VG_COLORSPACE_sRGB', 'EGL_VG_ALPHA_FORMAT_PRE' ), 'EGL_FULL' )
suite.addBenchmark( surfacealpha.ImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'EGL_VG_COLORSPACE_sRGB', 'EGL_VG_ALPHA_FORMAT_PRE' ), 'EGL_FULL' )

#----------------------------------------------------------------------
import surfaceantialiasing  # REQUIRES EGL
OVERDRAW = 3
suite.addBenchmark( surfaceantialiasing.FillBenchmark( OVERDRAW, 0 ), 'EGL_FULL' )
suite.addBenchmark( surfaceantialiasing.FillBenchmark( OVERDRAW, 4 ), 'EGL_FULL' )

suite.addBenchmark( surfaceantialiasing.TigerBenchmark( 0 ), 'EGL_FULL' )
suite.addBenchmark( surfaceantialiasing.TigerBenchmark( 4 ), 'EGL_FULL' )

suite.addBenchmark( surfaceantialiasing.ImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0 ), 'EGL_FULL' )
suite.addBenchmark( surfaceantialiasing.ImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 4 ), 'EGL_FULL' )

# ----------------------------------------------------------------------
import windows              # REQUIRES EGL
OVERDRAW = 3
suite.addBenchmark( windows.ResizeWindowBenchmark( OVERDRAW, [], [] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( OVERDRAW, [ 0, 0, width, height ], [ 0, 0, width, height ] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( OVERDRAW, [ 0, 0, width - 1, height - 1 ], [ 1, 1, width - 1, height - 1 ] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( OVERDRAW,[ 0, 0, width - 10, height - 10 ], [ 10, 10, width - 10, height - 10 ] ), 'EGL_FULL' )
suite.addBenchmark( windows.ResizeWindowBenchmark( OVERDRAW,[ 0, 0, width - 10, height ], [ 0, 0, width, height - 10 ] ), 'EGL_FULL' )

suite.addBenchmark( windows.OverlappingWindowsBenchmark( 2, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlappingWindowsBenchmark( 3, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlappingWindowsBenchmark( 4, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlappingWindowsBenchmark( 2, 0, True ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlappingWindowsBenchmark( 3, 0, True ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlappingWindowsBenchmark( 4, 0, True ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlappingWindowsBenchmark( 2, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlappingWindowsBenchmark( 3, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlappingWindowsBenchmark( 4, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlappingWindowsBenchmark( 2, 1, True ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlappingWindowsBenchmark( 3, 1, True ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlappingWindowsBenchmark( 4, 1, True ), 'EGL_FULL' )

suite.addBenchmark( windows.TopWindowBenchmark( 0, 0, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 1, 1, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 1, 2, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 3, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 3, 3, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 4, 3, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 4, 4, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 5, 5, 0 ), 'EGL_FULL' )

suite.addBenchmark( windows.TopWindowBenchmark( 0, 0, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 1, 1, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 1, 2, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 2, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 2, 3, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 3, 3, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 4, 3, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 4, 4, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.TopWindowBenchmark( 5, 5, 1 ), 'EGL_FULL' )

suite.addBenchmark( windows.ChildWindowBenchmark( 1, 1, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.ChildWindowBenchmark( 1, 2, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.ChildWindowBenchmark( 1, 1, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.ChildWindowBenchmark( 1, 2, 1 ), 'EGL_FULL' )

suite.addBenchmark( windows.PbuffersBenchmark( 1, 1, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_FULL' )
suite.addBenchmark( windows.PbuffersBenchmark( 2, 2, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_FULL' )
suite.addBenchmark( windows.PbuffersBenchmark( 3, 3, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_FULL' )
suite.addBenchmark( windows.PbuffersBenchmark( 4, 4, 'SCT_COLOR_FORMAT_RGB565', windows.FINISH ), 'EGL_FULL' )
suite.addBenchmark( windows.PbuffersBenchmark( 4, 4, 'SCT_COLOR_FORMAT_RGB565', windows.READPIXELS ), 'EGL_FULL' )
suite.addBenchmark( windows.PbuffersBenchmark( 4, 4, 'SCT_COLOR_FORMAT_RGB565', windows.COPYBUFFERS ), 'EGL_FULL' )

suite.addBenchmark( windows.RotateScreenBenchmark( OVERDRAW, 'SCT_SCREEN_ORIENTATION_PORTRAIT', 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.RotateScreenBenchmark( OVERDRAW, 'SCT_SCREEN_ORIENTATION_LANDSCAPE', 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.RotateScreenBenchmark( OVERDRAW, 'SCT_SCREEN_ORIENTATION_PORTRAIT', 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.RotateScreenBenchmark( OVERDRAW, 'SCT_SCREEN_ORIENTATION_LANDSCAPE', 1 ), 'EGL_FULL' )

suite.addBenchmark( windows.OpaqueWindowBenchmark( OVERDRAW, 1.0 ), 'EGL_FULL' )
suite.addBenchmark( windows.OpaqueWindowBenchmark( OVERDRAW, 0.3 ), 'EGL_FULL' )

suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 1.0, True, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 0.5, True, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 0.0, True, 1 ), 'EGL_FULL' )

suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 1.0, True, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 0.5, True, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 0.0, True, 0 ), 'EGL_FULL' )

suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 1.0, False, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 0.5, False, 1 ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 0.0, False, 1 ), 'EGL_FULL' )

suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 1.0, False, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 0.5, False, 0 ), 'EGL_FULL' )
suite.addBenchmark( windows.OverlayWindowBenchmark( OVERDRAW, 0.0, False, 0 ), 'EGL_FULL' )

suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', '720p', 1, False, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', '720p', 1, True, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', '720p', 1, False, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', '720p', 1, True, 1, False ), 'EGL_FULL' )

suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', '720p', 1, False, 0, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', '720p', 1, True, 0, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', '720p', 1, False, 1, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', '720p', 1, True, 1, True ), 'EGL_FULL' )

suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', '720p', 1, False, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', '720p', 1, True, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', '720p', 1, False, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', '720p', 1, True, 1, False ), 'EGL_FULL' )

suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', '720p', 1, False, 0, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', '720p', 1, True, 0, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', '720p', 1, False, 1, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', '720p', 1, True, 1, True ), 'EGL_FULL' )

suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', 'fullhd', 1, False, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', 'fullhd', 1, True, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', 'fullhd', 1, False, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', 'fullhd', 1, True, 1, False ), 'EGL_FULL' )

suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', 'fullhd', 1, False, 0, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', 'fullhd', 1, True, 0, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', 'fullhd', 1, False, 1, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGBA8888', 'fullhd', 1, True, 1, True ), 'EGL_FULL' )

suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', 'fullhd', 1, False, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', 'fullhd', 1, True, 0, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', 'fullhd', 1, False, 1, False ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', 'fullhd', 1, True, 1, False ), 'EGL_FULL' )

suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', 'fullhd', 1, False, 0, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', 'fullhd', 1, True, 0, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', 'fullhd', 1, False, 1, True ), 'EGL_FULL' )
suite.addBenchmark( windows.ScaledLandscapeBenchmark( 'SCT_COLOR_FORMAT_RGB565', 'fullhd', 1, True, 1, True ), 'EGL_FULL' )

#----------------------------------------------------------------------
import eglmisc
OVERDRAW = 3
suite.addBenchmark( eglmisc.PrintEglInfo(), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreateContextBenchmark(), 'EGL_FULL' )
suite.addBenchmark( eglmisc.DummyContextSwitchBenchmark( False, False, OVERDRAW ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.DummyContextSwitchBenchmark( True, False, OVERDRAW ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.DummyContextSwitchBenchmark( True, True, OVERDRAW ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreateWindowSurfaceBenchmark(), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePbufferSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePixmapSurfaceBenchmark( 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_FULL' )

suite.addBenchmark( eglmisc.CreatePbufferFromImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePbufferFromImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePbufferFromImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePbufferFromImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePbufferFromImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_FASTER' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePbufferFromImageBenchmark( width, height, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_BETTER' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.CreatePbufferFromImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ), 'EGL_FULL' )

suite.addBenchmark( eglmisc.MultisampleResolveBenchmark( OVERDRAW, 0, 'EGL_MULTISAMPLE_RESOLVE_DEFAULT' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.MultisampleResolveBenchmark( OVERDRAW, 0, 'EGL_MULTISAMPLE_RESOLVE_BOX' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.MultisampleResolveBenchmark( OVERDRAW, 4, 'EGL_MULTISAMPLE_RESOLVE_DEFAULT' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.MultisampleResolveBenchmark( OVERDRAW, 4, 'EGL_MULTISAMPLE_RESOLVE_BOX' ), 'EGL_FULL' )

suite.addBenchmark( eglmisc.SwapBehaviorBenchmark( OVERDRAW, 'EGL_BUFFER_DESTROYED' ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SwapBehaviorBenchmark( OVERDRAW, 'EGL_BUFFER_PRESERVED' ), 'EGL_FULL' )

suite.addBenchmark( eglmisc.SwapIntervalBenchmark( OVERDRAW, 0 ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SwapIntervalBenchmark( OVERDRAW, 1 ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SwapIntervalBenchmark( OVERDRAW, 2 ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SwapIntervalBenchmark( OVERDRAW, 3 ), 'EGL_FULL' )

buffers = [ eglmisc.WINDOW, eglmisc.PBUFFER ]

for b in buffers:
    suite.addBenchmark( eglmisc.LockSurfaceV1Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', False, False, [] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV1Benchmark( b, 'EGL_FORMAT_RGB_565_KHR', False, False, [] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV1Benchmark( b, 'EGL_FORMAT_RGBA_8888_EXACT_KHR', False, False, [] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV1Benchmark( b, 'EGL_FORMAT_RGBA_8888_KHR', False, False, [] ), 'EGL_FULL' )
    
    suite.addBenchmark( eglmisc.LockSurfaceV1Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', True, False, [] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV1Benchmark( b, 'EGL_FORMAT_RGBA_8888_EXACT_KHR', True, False, [] ), 'EGL_FULL' )

    suite.addBenchmark( eglmisc.LockSurfaceV1Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', False, True, [] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV1Benchmark( b, 'EGL_FORMAT_RGBA_8888_EXACT_KHR', False, True, [] ), 'EGL_FULL' )

    suite.addBenchmark( eglmisc.LockSurfaceV1Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', True, True, [ 'EGL_READ_SURFACE_BIT_KHR' ] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV1Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', True, False, [ 'EGL_WRITE_SURFACE_BIT_KHR' ] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV1Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', True, True, [ 'EGL_READ_SURFACE_BIT_KHR','EGL_WRITE_SURFACE_BIT_KHR' ] ), 'EGL_FULL' )

for b in buffers:
    suite.addBenchmark( eglmisc.LockSurfaceV2Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', False, False, [] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV2Benchmark( b, 'EGL_FORMAT_RGB_565_KHR', False, False, [] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV2Benchmark( b, 'EGL_FORMAT_RGBA_8888_EXACT_KHR', False, False, [] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV2Benchmark( b, 'EGL_FORMAT_RGBA_8888_KHR', False, False, [] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV2Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', True, False, [] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV2Benchmark( b, 'EGL_FORMAT_RGBA_8888_EXACT_KHR', True, False, [] ), 'EGL_FULL' )

    suite.addBenchmark( eglmisc.LockSurfaceV2Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', False, True, [] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV2Benchmark( b, 'EGL_FORMAT_RGBA_8888_EXACT_KHR', False, True, [] ), 'EGL_FULL' )

    suite.addBenchmark( eglmisc.LockSurfaceV2Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', True, True, [ 'EGL_READ_SURFACE_BIT_KHR' ] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV2Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', True, False, [ 'EGL_WRITE_SURFACE_BIT_KHR' ] ), 'EGL_FULL' )
    suite.addBenchmark( eglmisc.LockSurfaceV2Benchmark( b, 'EGL_FORMAT_RGB_565_EXACT_KHR', True, True, [ 'EGL_READ_SURFACE_BIT_KHR','EGL_WRITE_SURFACE_BIT_KHR' ] ), 'EGL_FULL' )

suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ width, height ], None, None ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ width / 2, height / 2 ], None, None ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ width, height ], [ width / 2, height / 2 ], [ 0, 0, width, height ] ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ width, height ], [ width, height ], [ width / 4, height / 4, width / 2, height / 2 ] ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ 240, 320 ], None, None ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ width, height ], [ 240, 320 ], [ 0, 0, width, height ] ), 'EGL_FULL' )
ar = min( width / 240.0, height / 320.0 )
w = int( 240 * ar )
h = int( 320 * ar )
suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ width, height ], [ 240, 320 ], [ ( width - w  ) / 2, ( height - h ) / 2 , w, h ] ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ 320, 480 ], None, None ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ width, height ], [ 320, 480 ], [ 0, 0, width, height ] ), 'EGL_FULL' )
ar = min( width / 320.0, height / 480.0 )
w = int( 320 * ar )
h = int( 480 * ar )
suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ width, height ], [ 320, 480 ], [ ( width - w  ) / 2, ( height - h ) / 2 , w, h ] ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ width, height ], [ width / 2, height / 2 ], [ 0, 0, width / 2 + 1, height / 2 + 1 ] ), 'EGL_FULL' )
suite.addBenchmark( eglmisc.SurfaceScalingBenchmark( 1, [ width, height ], [ width / 2, height /2 ], [ width / 2, height / 2, width / 2, height / 2 ] ), 'EGL_FULL' )


#----------------------------------------------------------------------
import eglimage

suite.addBenchmark( eglimage.CreatePixmapBenchmark( 360, 640, None, 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_FULL' )

suite.addBenchmark( eglimage.CreatePixmapBenchmark( 360, 640, 'IMAGES_RGB565', 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreatePixmapBenchmark( 360, 640, 'IMAGES_BGR8', 'SCT_COLOR_FORMAT_RGB888' ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreatePixmapBenchmark( 360, 640, 'IMAGES_BGRA8', 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_FULL' )

suite.addBenchmark( eglimage.CreateSharedPixmapBenchmark( 360, 640, None, 'SCT_COLOR_FORMAT_A8', [ 'SCT_USAGE_OPENVG_IMAGE' ] ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreateSharedPixmapBenchmark( 360, 640, None, 'SCT_COLOR_FORMAT_RGB565', [ 'SCT_USAGE_OPENVG_IMAGE' ] ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreateSharedPixmapBenchmark( 360, 640, None, 'SCT_COLOR_FORMAT_RGBX8888', [ 'SCT_USAGE_OPENVG_IMAGE' ] ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreateSharedPixmapBenchmark( 360, 640, None, 'SCT_COLOR_FORMAT_RGBA8888', [ 'SCT_USAGE_OPENVG_IMAGE' ] ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreateSharedPixmapBenchmark( 360, 640, None, 'SCT_COLOR_FORMAT_RGBA8888_PRE', [ 'SCT_USAGE_OPENVG_IMAGE' ] ), 'EGL_FULL' )

suite.addBenchmark( eglimage.CreateSharedPixmapBenchmark( 360, 640, 'IMAGES_A8', 'SCT_COLOR_FORMAT_A8', [ 'SCT_USAGE_OPENVG_IMAGE' ] ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreateSharedPixmapBenchmark( 360, 640, 'IMAGES_RGB565', 'SCT_COLOR_FORMAT_RGB565', [ 'SCT_USAGE_OPENVG_IMAGE' ] ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreateSharedPixmapBenchmark( 360, 640, 'IMAGES_BGRX8', 'SCT_COLOR_FORMAT_RGBX8888', [ 'SCT_USAGE_OPENVG_IMAGE' ] ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreateSharedPixmapBenchmark( 360, 640, 'IMAGES_BGRA8', 'SCT_COLOR_FORMAT_RGBA8888', [ 'SCT_USAGE_OPENVG_IMAGE' ] ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreateSharedPixmapBenchmark( 360, 640, 'IMAGES_BGRA8', 'SCT_COLOR_FORMAT_RGBA8888_PRE', [ 'SCT_USAGE_OPENVG_IMAGE' ] ), 'EGL_FULL' )

suite.addBenchmark( eglimage.CreateVgImageFromEglImageBenchmark( 360, 640, 'IMAGES_RGB565', 'SCT_COLOR_FORMAT_RGB565' ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreateVgImageFromEglImageBenchmark( 360, 640, 'IMAGES_BGRX8', 'SCT_COLOR_FORMAT_RGBX8888' ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreateVgImageFromEglImageBenchmark( 360, 640, 'IMAGES_BGRA8', 'SCT_COLOR_FORMAT_RGBA8888' ), 'EGL_FULL' )
suite.addBenchmark( eglimage.CreateVgImageFromEglImageBenchmark( 360, 640, 'IMAGES_BGRA8', 'SCT_COLOR_FORMAT_RGBA8888_PRE' ), 'EGL_FULL' )

#----------------------------------------------------------------------
import ui
suite.addBenchmark( ui.ListboxBenchmark( ui.REUSE_PATH, 1.0, False ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.MODIFY_PATH, 1.0, False ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.CLEAR_PATH, 1.0, False ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.NEW_PATH, 1.0, False ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.REUSE_PATH, 1.0, True ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.MODIFY_PATH, 1.0, True ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.CLEAR_PATH, 1.0, True ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.NEW_PATH, 1.0, True ) )

suite.addBenchmark( ui.ListboxBenchmark( ui.REUSE_PATH, 0.5, False ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.MODIFY_PATH, 0.5, False ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.CLEAR_PATH, 0.5, False ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.NEW_PATH, 0.5, False ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.REUSE_PATH, 0.5, True ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.MODIFY_PATH, 0.5, True ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.CLEAR_PATH, 0.5, True ) )
suite.addBenchmark( ui.ListboxBenchmark( ui.NEW_PATH, 0.5, True ) )

suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND ], 'VG_sRGBA_8888_PRE', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.PANELS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.GROUPS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.ICONS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.ICONS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888_PRE', 0 ) )

suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND ], 'VG_sRGBA_8888_PRE', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.PANELS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.GROUPS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.ICONS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888_PRE', 0 ) )

suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND ], 'VG_sRGBA_8888_PRE', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.PANELS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.GROUPS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.ICONS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888_PRE', 0 ) )

suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.PANELS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.GROUPS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.ICONS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888', 1 ) )

suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.PANELS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.GROUPS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.ICONS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.ICONS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888', 1 ) )

matrices = [ 75, 100, 150, 200, 250, 300 ]
#- for l in matrices:
#-     suite.addBenchmark( ui.Invert4x4MatrixBenchmark( l, 1 ) )

suite.addBenchmark( ui.ParallelUiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], 'VG_sRGBA_8888', 0 ) )
for m in matrices:    
    suite.addBenchmark( ui.ParallelUiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS ], 'VG_sRGBA_8888', m ) )

#----------------------------------------------------------------------
import vgu
suite.addBenchmark( vgu.VguPathBenchmark( 'line' ), 'VGU' )
suite.addBenchmark( vgu.VguPathBenchmark( 'polygon' ), 'VGU' )
suite.addBenchmark( vgu.VguPathBenchmark( 'rect' ), 'VGU' )
suite.addBenchmark( vgu.VguPathBenchmark( 'round rect' ), 'VGU' )
suite.addBenchmark( vgu.VguPathBenchmark( 'ellipse' ), 'VGU' )
suite.addBenchmark( vgu.VguPathBenchmark( 'open arc' ), 'VGU' )
suite.addBenchmark( vgu.VguPathBenchmark( 'chord arc' ), 'VGU' )
suite.addBenchmark( vgu.VguPathBenchmark( 'pie arc' ), 'VGU' )

suite.addBenchmark( vgu.VguWarpBenchmark( 'quad-to-square' ), 'VGU' )
suite.addBenchmark( vgu.VguWarpBenchmark( 'square-to-quad' ), 'VGU' )
suite.addBenchmark( vgu.VguWarpBenchmark( 'quad-to-quad' ), 'VGU' )
    
#----------------------------------------------------------------------
import resources
suite.addBenchmark( resources.PrintResourceProfile( 'end' ), 'EGL_FULL' )

#----------------------------------------------------------------------
command()
