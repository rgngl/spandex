#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from common        import *

######################################################################
REUSE_PATH      = 1
MODIFY_PATH     = 2
CLEAR_PATH      = 3
NEW_PATH        = 4

REUSE_STRINGS   = { REUSE_PATH  : 'REUSE_PATH',
                    MODIFY_PATH : 'MODIFY_PATH',
                    CLEAR_PATH  : 'CLEAR_PATH',
                    NEW_PATH    : 'NEW_PATH' } ;

BACKGROUND              = 0
PANELS                  = 1
GROUPS                  = 2
ICONS                   = 3
 
UPLOAD                  = 4
DRAW                    = 5
UPLOAD_AND_DRAW         = 6

PANELSX                 = 2
PANELSY                 = 1
GROUPSX                 = 2
GROUPSY                 = 3
ICONSX                  = 4
ICONSY                  = 6

######################################################################
def actionToString( action ):
    if action == UPLOAD:
        return 'UPLOAD'
    if action == DRAW:
        return 'DRAW'
    if action == UPLOAD_AND_DRAW:
        return 'UPLOAD AND DRAW'
    else:
        raise 'Unexpected action %s' % str( action )

    
######################################################################
def layersToString( layers ):
    s = []
    if BACKGROUND in layers:
        s += [ 'BACKGROUND' ]
    if PANELS in layers:
        s += [ 'PANEL' ]
    if GROUPS in layers:
        s += [ 'GROUPS' ]
    if ICONS in layers:
        s += [ 'ICONS' ]

    return '+'.join( s )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ListboxBenchmark( Benchmark ):   
    def __init__( self, reuse, visible, matrix ):
        Benchmark.__init__( self )
        self.reuse   = reuse
        self.visible = visible
        self.matrix  = matrix
        self.name = "OPENVG listbox ui %s vsble=%.2f, mtrx=%s" % ( REUSE_STRINGS[ self.reuse ],
                                                                   self.visible,
                                                                   boolToStr( self.matrix ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        
        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        listItemHeight         = 16
        listItemGraphicsHeight = listItemHeight * self.visible
        listItems              = height / listItemHeight
        
        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_F', 1, 1 )

        OpenVG.Lines( pathDataIndex,
                      [ 0,              listItemGraphicsHeight,
                        width,          listItemGraphicsHeight,
                        width,          0,
                        0,              0 ],
                      'OFF' )
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_F',
                           1.0,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_ALL' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )       

        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )
        
        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( listItems ):
            r = i / float( listItems )
            
            OpenVG.ColorPaint( paintIndex, [ 0.7 * r, 1.0, 0.7 * r, 1.0 ] )
            y = listItemHeight * i

            OpenVG.LoadIdentity()

            if self.reuse != REUSE_PATH:
                y = y / 2.0
            
            if self.matrix:
                OpenVG.LoadMatrix( [ 1.0,  0.0, 0.0,
                                     0.0,  1.0, 0.0,
                                     0.0,    y, 1.0 ] )
            else:
                OpenVG.Translate( 0, y )

            if self.reuse != REUSE_PATH:                   
                OpenVG.ClearPathData( pathDataIndex )
                OpenVG.Lines( pathDataIndex,
                              [ 0,              y + listItemGraphicsHeight,
                                width,          y + listItemGraphicsHeight,
                                width,          y,
                                0,              y ],
                              'OFF' )
                OpenVG.ClosePath( pathDataIndex )

                if self.reuse == MODIFY_PATH:
                    OpenVG.ModifyPathCoords( pathIndex, pathDataIndex, 0, 0 )
                elif self.reuse == CLEAR_PATH:
                    OpenVG.ClearPath( pathIndex, [ 'VG_PATH_CAPABILITY_ALL' ] )
                    OpenVG.AppendPathData( pathIndex, pathDataIndex )
                else:
                    pathIndex += 1
                    OpenVG.CreatePath( pathIndex,
                                       'VG_PATH_DATATYPE_F',
                                       1.0,
                                       0,
                                       5,
                                       8,
                                       [ 'VG_PATH_CAPABILITY_ALL' ] )

                    OpenVG.AppendPathData( pathIndex, pathDataIndex )
                
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

            if self.reuse == NEW_PATH:
                OpenVG.DestroyPath( pathIndex )
            
        target.swapBuffers( state )

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class UiBenchmark( Benchmark ):  
    def __init__( self, action, layers, format, swapInterval ):
        Benchmark.__init__( self )
        self.action       = action
        self.layers       = layers
        self.format       = format
        self.swapInterval = swapInterval        
        self.name = "OPENVG ui actn=%s layers=%s frmt=%s swpntvl=%d" % ( actionToString( self.action ),
                                                                         layersToString( self.layers ),
                                                                         imageFormatToStr( self.format ),
                                                                         self.swapInterval, )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        
        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        backgroundWidth  = screenWidth
        backgroundHeight = screenHeight
        panelsX          = PANELSX
        panelsY          = PANELSY
        panelWidth       = screenWidth / panelsX
        panelHeight      = screenHeight / panelsY
        groupsX          = GROUPSX
        groupsY          = GROUPSY
        groupWidth       = panelWidth / groupsX
        groupHeight      = panelHeight / groupsY
        iconsX           = ICONSX
        iconsY           = ICONSY
        iconWidth        = panelWidth / iconsX
        iconHeight       = panelHeight / iconsY
        
        if BACKGROUND in self.layers:
            backgroundImageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
            backgroundImageIndex     = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
            
            OpenVG.CreateNullImageData( backgroundImageDataIndex,
                                        self.format,
                                        backgroundWidth,
                                        backgroundHeight )

            OpenVG.CreateImage( backgroundImageIndex,
                                self.format,
                                backgroundWidth,
                                backgroundHeight,
                                [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )
            OpenVG.ClearColor( [ 1.0, 1.0, 1.0, 1.0 ] )
            OpenVG.ClearImage( backgroundImageIndex,
                               0, 0,
                               backgroundWidth,
                               backgroundHeight )
            OpenVG.GetImageSubData( backgroundImageIndex,
                                    backgroundImageDataIndex,
                                    0, 0,
                                    backgroundWidth,
                                    backgroundHeight )

        if PANELS in self.layers:
            panelImageDataIndices  = []
            panelImageIndices      = []
            panelImageOrigins      = []

            total = panelsX * panelsY + 1
            for y in range( panelsY ):
                for x in range( panelsX ):
                    dataIndex  = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                    imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                    
                    panelImageDataIndices   += [ dataIndex ]
                    panelImageIndices       += [ imageIndex ]
                    panelImageOrigins       += [ ( panelWidth * x, panelHeight * y, ) ]
                    
                    OpenVG.CreateNullImageData( dataIndex,
                                                self.format,
                                                panelWidth,
                                                panelHeight )
                    
                    OpenVG.CreateImage( imageIndex,
                                        self.format,
                                        panelWidth,
                                        panelHeight,
                                        [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )

                    r = 1.0 * ( ( x + ( y * panelsX ) + 1 ) / float( total ) )
                    OpenVG.ClearColor( [ 0.0, r, 0.0, 0.5 ] )
                    OpenVG.ClearImage( imageIndex,
                                       0, 0,
                                       panelWidth,
                                       panelHeight )
                    OpenVG.GetImageSubData( imageIndex,
                                            dataIndex,
                                            0, 0,
                                            panelWidth,
                                            panelHeight )

        if GROUPS in self.layers:
            groupImageDataIndices   = []
            groupImageIndices       = []
            groupImageOrigins       = []

            total = groupsX * groupsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( groupsY ):
                        for x in range( groupsX ):
                            dataIndex  = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                            imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                    
                            groupImageDataIndices   += [ dataIndex ]
                            groupImageIndices       += [ imageIndex ]
                            groupImageOrigins       += [ ( px * panelWidth + groupWidth * x,
                                                           py * panelHeight + groupHeight * y, ) ]
                    
                            OpenVG.CreateNullImageData( dataIndex,
                                                        self.format,
                                                        groupWidth,
                                                        groupHeight )
                    
                            OpenVG.CreateImage( imageIndex,
                                                self.format,
                                                groupWidth,
                                                groupHeight,
                                                [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )

                            r = 1.0 * ( x + ( y * groupsX ) + 1 ) / float( total )
                            OpenVG.ClearColor( [ 0.0, 0.0, r, 0.5 ] )
                            OpenVG.ClearImage( imageIndex,
                                               0, 0,
                                               groupWidth,
                                               groupHeight )
                            OpenVG.GetImageSubData( imageIndex,
                                                    dataIndex,
                                                    0, 0,
                                                    groupWidth,
                                                    groupHeight )
                    
        if ICONS in self.layers:
            iconImageDataIndices    = []
            iconImageIndices        = []
            iconImageOrigins        = []

            total = iconsX * iconsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( iconsY ):
                        for x in range( iconsX ):
                            dataIndex  = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                            imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                    
                            iconImageDataIndices    += [ dataIndex ]
                            iconImageIndices        += [ imageIndex ]
                            iconImageOrigins        += [ ( px * panelWidth + iconWidth * x,
                                                           py * panelHeight + iconHeight * y, ) ]
                    
                            OpenVG.CreateNullImageData( dataIndex,
                                                        self.format,
                                                        iconWidth,
                                                        iconHeight )
                    
                            OpenVG.CreateImage( imageIndex,
                                                self.format,
                                                iconWidth,
                                                iconHeight,
                                                [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )

                            r = 1.0 * ( x + ( y * iconsX ) + 1 ) / float( total )
                            OpenVG.ClearColor( [ 0.3, 0.3, r, 0.5 ] )
                            OpenVG.ClearImage( imageIndex,
                                               0, 0,
                                               iconWidth,
                                               iconHeight )
                            OpenVG.GetImageSubData( imageIndex,
                                                    dataIndex,
                                                    0, 0,
                                                    iconWidth,
                                                    iconHeight )
                                    
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, screenWidth, screenHeight )
        target.swapBuffers( state )

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )        
        blending = True 

        OpenVG.ImageQuality( 'VG_IMAGE_QUALITY_NONANTIALIASED' )        
        OpenVG.RenderingQuality( 'VG_RENDERING_QUALITY_NONANTIALIASED' )

        target.swapInterval( state, self.swapInterval )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        # Clear the screen if drawing is requested and the layers do not include background
        if not BACKGROUND in self.layers:       
            if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
                OpenVG.Clear( 0, 0, screenWidth, screenHeight )

        # Do image data upload if requested
        if self.action == UPLOAD or self.action == UPLOAD_AND_DRAW:
            if BACKGROUND in self.layers:           
                OpenVG.ImageSubData( backgroundImageDataIndex,
                                     backgroundImageIndex,
                                     0, 0,
                                     backgroundWidth, backgroundHeight )

            if PANELS in self.layers:
                for i in range( len( panelImageDataIndices ) ):
                    OpenVG.ImageSubData( panelImageDataIndices[ i ],
                                         panelImageIndices[ i ],
                                         0, 0,
                                         panelWidth, panelHeight )

            if GROUPS in self.layers:
                for i in range( len( groupImageDataIndices ) ):
                    OpenVG.ImageSubData( groupImageDataIndices[ i ],
                                         groupImageIndices[ i ],
                                         0, 0,
                                         groupWidth, groupHeight )
                    
            if ICONS in self.layers:
                for i in range( len( iconImageDataIndices ) ):
                    OpenVG.ImageSubData( iconImageDataIndices[ i ],
                                         iconImageIndices[ i ],
                                         0, 0,
                                         iconWidth, iconHeight )

        # Draw the images if drawing is requested
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:            
            if BACKGROUND in self.layers:
                if blending:
                    OpenVG.BlendMode( 'VG_BLEND_SRC' )
                    blending = False               
                OpenVG.LoadIdentity()
                OpenVG.DrawImage( backgroundImageIndex )

            if PANELS in self.layers:
                for i in range( len( panelImageIndices ) ):
                    origin = panelImageOrigins[ i ]
                    if not blending:
                        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )
                        blending = True
                    OpenVG.LoadIdentity()                    
                    OpenVG.Translate( origin[ 0 ], origin[ 1 ] )               
                    OpenVG.DrawImage( panelImageIndices[ i ] )

            if GROUPS in self.layers:
                for i in range( len( groupImageIndices ) ):
                    origin = groupImageOrigins[ i ]
                    if not blending:
                        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )
                        blending = True                    
                    OpenVG.LoadIdentity()                                    
                    OpenVG.Translate( origin[ 0 ], origin[ 1 ] )               
                    OpenVG.DrawImage( groupImageIndices[ i ] )
                    
            if ICONS in self.layers:
                for i in range( len( iconImageIndices ) ):
                    origin = iconImageOrigins[ i ]
                    if not blending:
                        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )
                        blending = True                                        
                    OpenVG.LoadIdentity()                                    
                    OpenVG.Translate( origin[ 0 ], origin[ 1 ] )               
                    OpenVG.DrawImage( iconImageIndices[ i ] )

            target.swapBuffers( state )
                    

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class Invert4x4MatrixBenchmark( Benchmark ):  
    def __init__( self, count, iterations ):
        Benchmark.__init__( self )
        self.count      = count
        self.iterations = iterations
        self.name = "OPENVG invert 4x4 matrix mtrx.cnt=%d itrtns=%d"  % ( self.count,
                                                                          self.iterations, )
       
    def build( self, target, modules ):
        Cpu = modules[ 'CPU' ]
        
        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        Cpu.Invert4x4Matrix( self.count, self.iterations )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ParallelUiBenchmark( Benchmark ):  
    def __init__( self, action, layers, format, matrices ):
        Benchmark.__init__( self )
        self.action   = action
        self.layers   = layers
        self.format   = format
        self.matrices = matrices
        self.name = "OPENVG parallel ui actn=%s layers=%s frmt=%s mtrcs=%d" % ( actionToString( self.action ),
                                                                                layersToString( self.layers ),
                                                                                imageFormatToStr( self.format ),
                                                                                self.matrices, )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Cpu    = modules[ 'CPU' ]
        
        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        backgroundWidth  = screenWidth
        backgroundHeight = screenHeight
        panelsX          = PANELSX
        panelsY          = PANELSY
        panelWidth       = screenWidth / panelsX
        panelHeight      = screenHeight / panelsY
        groupsX          = GROUPSX
        groupsY          = GROUPSY
        groupWidth       = panelWidth / groupsX
        groupHeight      = panelHeight / groupsY
        iconsX           = ICONSX
        iconsY           = ICONSY
        iconWidth        = panelWidth / iconsX
        iconHeight       = panelHeight / iconsY
        
        if BACKGROUND in self.layers:
            backgroundImageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
            backgroundImageIndex     = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
            
            OpenVG.CreateNullImageData( backgroundImageDataIndex,
                                        self.format,
                                        backgroundWidth,
                                        backgroundHeight )

            OpenVG.CreateImage( backgroundImageIndex,
                                self.format,
                                backgroundWidth,
                                backgroundHeight,
                                [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )
            OpenVG.ClearColor( [ 1.0, 1.0, 1.0, 1.0 ] )
            OpenVG.ClearImage( backgroundImageIndex,
                               0, 0,
                               backgroundWidth,
                               backgroundHeight )
            OpenVG.GetImageSubData( backgroundImageIndex,
                                    backgroundImageDataIndex,
                                    0, 0,
                                    backgroundWidth,
                                    backgroundHeight )

        if PANELS in self.layers:
            panelImageDataIndices  = []
            panelImageIndices      = []
            panelImageOrigins      = []

            total = panelsX * panelsY + 1
            for y in range( panelsY ):
                for x in range( panelsX ):
                    dataIndex  = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                    imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                    
                    panelImageDataIndices   += [ dataIndex ]
                    panelImageIndices       += [ imageIndex ]
                    panelImageOrigins       += [ ( panelWidth * x, panelHeight * y, ) ]
                    
                    OpenVG.CreateNullImageData( dataIndex,
                                                self.format,
                                                panelWidth,
                                                panelHeight )
                    
                    OpenVG.CreateImage( imageIndex,
                                        self.format,
                                        panelWidth,
                                        panelHeight,
                                        [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )

                    r = 1.0 * ( ( x + ( y * panelsX ) + 1 ) / float( total ) )
                    OpenVG.ClearColor( [ 0.0, r, 0.0, 0.5 ] )
                    OpenVG.ClearImage( imageIndex,
                                       0, 0,
                                       panelWidth,
                                       panelHeight )
                    OpenVG.GetImageSubData( imageIndex,
                                            dataIndex,
                                            0, 0,
                                            panelWidth,
                                            panelHeight )

        if GROUPS in self.layers:
            groupImageDataIndices   = []
            groupImageIndices       = []
            groupImageOrigins       = []

            total = groupsX * groupsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( groupsY ):
                        for x in range( groupsX ):
                            dataIndex  = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                            imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                    
                            groupImageDataIndices   += [ dataIndex ]
                            groupImageIndices       += [ imageIndex ]
                            groupImageOrigins       += [ ( px * panelWidth + groupWidth * x,
                                                           py * panelHeight + groupHeight * y, ) ]
                    
                            OpenVG.CreateNullImageData( dataIndex,
                                                        self.format,
                                                        groupWidth,
                                                        groupHeight )
                    
                            OpenVG.CreateImage( imageIndex,
                                                self.format,
                                                groupWidth,
                                                groupHeight,
                                                [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )

                            r = 1.0 * ( x + ( y * groupsX ) + 1 ) / float( total )
                            OpenVG.ClearColor( [ 0.0, 0.0, r, 0.5 ] )
                            OpenVG.ClearImage( imageIndex,
                                               0, 0,
                                               groupWidth,
                                               groupHeight )
                            OpenVG.GetImageSubData( imageIndex,
                                                    dataIndex,
                                                    0, 0,
                                                    groupWidth,
                                                    groupHeight )
                    
        if ICONS in self.layers:
            iconImageDataIndices    = []
            iconImageIndices        = []
            iconImageOrigins        = []

            total = iconsX * iconsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( iconsY ):
                        for x in range( iconsX ):
                            dataIndex  = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                            imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                    
                            iconImageDataIndices    += [ dataIndex ]
                            iconImageIndices        += [ imageIndex ]
                            iconImageOrigins        += [ ( px * panelWidth + iconWidth * x,
                                                           py * panelHeight + iconHeight * y, ) ]
                    
                            OpenVG.CreateNullImageData( dataIndex,
                                                        self.format,
                                                        iconWidth,
                                                        iconHeight )
                    
                            OpenVG.CreateImage( imageIndex,
                                                self.format,
                                                iconWidth,
                                                iconHeight,
                                                [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )

                            r = 1.0 * ( x + ( y * iconsX ) + 1 ) / float( total )
                            OpenVG.ClearColor( [ 0.3, 0.3, r, 0.5 ] )
                            OpenVG.ClearImage( imageIndex,
                                               0, 0,
                                               iconWidth,
                                               iconHeight )
                            OpenVG.GetImageSubData( imageIndex,
                                                    dataIndex,
                                                    0, 0,
                                                    iconWidth,
                                                    iconHeight )
                                    
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, screenWidth, screenHeight )
        target.swapBuffers( state )

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )       
        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )        
        blending = True 

        OpenVG.ImageQuality( 'VG_IMAGE_QUALITY_NONANTIALIASED' )        
        OpenVG.RenderingQuality( 'VG_RENDERING_QUALITY_NONANTIALIASED' )

        target.swapInterval( state, 1 )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.matrices > 0:
            Cpu.Invert4x4Matrix( self.matrices, 1 )
        
        # Clear the screen if drawing is requested and the layers do not include background
        if not BACKGROUND in self.layers:       
            if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
                OpenVG.Clear( 0, 0, screenWidth, screenHeight )

        # Do image data upload if requested
        if self.action == UPLOAD or self.action == UPLOAD_AND_DRAW:
            if BACKGROUND in self.layers:           
                OpenVG.ImageSubData( backgroundImageDataIndex,
                                     backgroundImageIndex,
                                     0, 0,
                                     backgroundWidth, backgroundHeight )

            if PANELS in self.layers:
                for i in range( len( panelImageDataIndices ) ):
                    OpenVG.ImageSubData( panelImageDataIndices[ i ],
                                         panelImageIndices[ i ],
                                         0, 0,
                                         panelWidth, panelHeight )

            if GROUPS in self.layers:
                for i in range( len( groupImageDataIndices ) ):
                    OpenVG.ImageSubData( groupImageDataIndices[ i ],
                                         groupImageIndices[ i ],
                                         0, 0,
                                         groupWidth, groupHeight )
                    
            if ICONS in self.layers:
                for i in range( len( iconImageDataIndices ) ):
                    OpenVG.ImageSubData( iconImageDataIndices[ i ],
                                         iconImageIndices[ i ],
                                         0, 0,
                                         iconWidth, iconHeight )

        # Draw the images if drawing is requested
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:            
            if BACKGROUND in self.layers:
                if blending:
                    OpenVG.BlendMode( 'VG_BLEND_SRC' )
                    blending = False
                OpenVG.LoadIdentity()
                OpenVG.DrawImage( backgroundImageIndex )

            if PANELS in self.layers:
                for i in range( len( panelImageIndices ) ):
                    origin = panelImageOrigins[ i ]
                    if not blending:
                        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )
                        blending = True
                    OpenVG.LoadIdentity()                    
                    OpenVG.Translate( origin[ 0 ], origin[ 1 ] )               
                    OpenVG.DrawImage( panelImageIndices[ i ] )

            if GROUPS in self.layers:
                for i in range( len( groupImageIndices ) ):
                    origin = groupImageOrigins[ i ]
                    if not blending:
                        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )
                        blending = True                   
                    OpenVG.LoadIdentity()                                    
                    OpenVG.Translate( origin[ 0 ], origin[ 1 ] )               
                    OpenVG.DrawImage( groupImageIndices[ i ] )
                    
            if ICONS in self.layers:
                for i in range( len( iconImageIndices ) ):
                    origin = iconImageOrigins[ i ]
                    if not blending:
                        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )
                        blending = True                                      
                    OpenVG.LoadIdentity()                                    
                    OpenVG.Translate( origin[ 0 ], origin[ 1 ] )               
                    OpenVG.DrawImage( iconImageIndices[ i ] )

            target.swapBuffers( state )
                    
        
