#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import   Benchmark
from lib.common    import   *
from common        import   *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class VguPathBenchmark( Benchmark ):  
    def __init__( self, path ):
        Benchmark.__init__( self )
        self.path = path
        self.name = "OPENVG vgu path %s" % ( self.path, )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgu    = modules[ 'Vgu' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            
       
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_F',
                           1.0,
                           0,
                           16,
                           16,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.path == 'line':
            Vgu.Line( pathIndex, 0, 0, width, height )
        elif self.path == 'polygon':
            Vgu.Polygon( pathIndex, [ 0, 0, width, 0, width, height, 0, height ], 'ON' )
        elif self.path == 'rect':
            Vgu.Rect( pathIndex, 0, 0, width, height )
        elif self.path == 'round rect':
            Vgu.RoundRect( pathIndex, 0, 0, width, height, 10, 10 )
        elif self.path == 'ellipse':
            Vgu.Ellipse( pathIndex, width / 2, height / 2, width, height )
        elif self.path == 'open arc':
            Vgu.Arc( pathIndex, 0, 0, width, height, 0, 90, 'VGU_ARC_OPEN' )
        elif self.path == 'chord arc':
            Vgu.Arc( pathIndex, 0, 0, width, height, 0, 90, 'VGU_ARC_CHORD' )
        elif self.path == 'pie arc':
            Vgu.Arc( pathIndex, 0, 0, width, height, 0, 90, 'VGU_ARC_PIE' )
        else:
            raise 'Unexpected path'

        # Try to ensure the path data is actually appended.
        OpenVG.Finish()
        
        OpenVG.ClearPath( pathIndex, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        
        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class VguWarpBenchmark( Benchmark ):  
    def __init__( self, warp ):
        Benchmark.__init__( self )
        self.warp = warp
        self.name = "OPENVG vgu warp %s" % ( self.warp, )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgu    = modules[ 'Vgu' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            
       
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.warp == 'quad-to-square':
            Vgu.ComputeWarpQuadToSquare( 0, 0.1, 0.1, 0.9, 0.1, 0.1, 0.9, 0.9, 0.9 )
        elif self.warp == 'square-to-quad':
            Vgu.ComputeWarpSquareToQuad( 0, 0.1, 0.1, 0.9, 0.1, 0.1, 0.9, 0.9, 0.9 )
        elif self.warp == 'quad-to-quad':
            Vgu.ComputeWarpQuadToQuad( 0, 0.1, 0.1, 0.9, 0.1, 0.1, 0.9, 0.9, 0.9, 0.2, 0.2, 0.8, 0.2, 0.2, 0.8, 0.8, 0.8 )
        else:
            raise 'Unexpected warp'
       
        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        
