#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class RenderUnmaskedBenchmark( Benchmark ):  
    def __init__( self, lineCount, strokeWidth, strokeCapStyle ):
        Benchmark.__init__( self )
        self.lineCount      = lineCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.name = "OPENVG render unmasked %d lines strkwdth=%d strkcpstle=%s" % ( self.lineCount,
                                                                                    self.strokeWidth,
                                                                                    strokeCapStyleToStr( self.strokeCapStyle ), )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.lineCount + 1 )
        y       = yoffset

        pathCoordinates = []
        for i in range( self.lineCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )            
            OpenVG.Lines( pathDataIndex,
                          [ scale * ( width - xoffset ), y * scale ],
                          'OFF' )
            OpenVG.ClosePath( pathDataIndex )
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           2,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class RenderWithImageMaskBenchmark( Benchmark ):
    def __init__( self, lineCount, strokeWidth, strokeCapStyle, maskFormat, maskQuality,  ):
        Benchmark.__init__( self )
        self.lineCount      = lineCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.maskFormat     = maskFormat
        self.maskQuality    = maskQuality
        self.name = "OPENVG render with image mask %d lines strkwdth=%d strkcpstle=%s mskfrmat=%s mskqlty=%s" % ( self.lineCount,
                                                                                                                  self.strokeWidth,
                                                                                                                  strokeCapStyleToStr( self.strokeCapStyle ),
                                                                                                                  imageFormatToStr( self.maskFormat ),
                                                                                                                  imageQualityToStr( self.maskQuality ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENVG )
        attribs[ CONFIG_ALPHAMASKSIZE ] = '>=8'

        state = setupVG( modules, indexTracker, target, attribs )

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.maskFormat,
                            width,
                            height,
                            [ self.maskQuality ] )
        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenVG.ClearImage( imageIndex, 0, 0, width, height )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.lineCount + 1 )
        y       = yoffset

        pathCoordinates = []
        for i in range( self.lineCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )            
            OpenVG.Lines( pathDataIndex,
                          [ scale * ( width - xoffset ), y * scale ],
                          'OFF' )
            OpenVG.ClosePath( pathDataIndex )
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           2,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.GetPixels( imageIndex, 0, 0, 0, 0, width, height )

        OpenVG.Mask( imageIndex, 'IMAGE', 'VG_SET_MASK', 0, 0, width, height )
        OpenVG.Enable( 'VG_MASKING' )

        strokePaintIndex = 1
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class RenderToMaskBenchmark( Benchmark ):  
    def __init__( self, lineCount, strokeWidth, strokeCapStyle, operation ):
        Benchmark.__init__( self )
        self.lineCount      = lineCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.operation      = operation
        self.name = "OPENVG render to mask with %s %d lines strkwdth=%d strkcpstle=%s" % ( maskOperationToStr( self.operation ),
                                                                                           self.lineCount,
                                                                                           self.strokeWidth,
                                                                                           strokeCapStyleToStr( self.strokeCapStyle ), )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENVG )
        attribs[ CONFIG_ALPHAMASKSIZE ] = '>=8'

        state = setupVG( modules, indexTracker, target, attribs )

        maskLayerIndex = 0
        OpenVG.CreateMaskLayer( maskLayerIndex, width, height )
        OpenVG.Enable( 'VG_MASKING' )
        
        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.lineCount + 1 )
        y       = yoffset

        pathCoordinates = []
        for i in range( self.lineCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )            
            OpenVG.Lines( pathDataIndex,
                          [ scale * ( width - xoffset ), y * scale ],
                          'OFF' )
            OpenVG.ClosePath( pathDataIndex )
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           2,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.RenderToMask( pathIndex, [ 'VG_STROKE_PATH' ], self.operation )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class RenderWithMaskBenchmark( Benchmark ):
    def __init__( self, lineCount, strokeWidth, strokeCapStyle ):
        Benchmark.__init__( self )
        self.lineCount      = lineCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.name = "OPENVG render with mask %d lines strkwdth=%d strkcpstle=%s" % ( self.lineCount,
                                                                                     self.strokeWidth,
                                                                                     strokeCapStyleToStr( self.strokeCapStyle ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENVG )
        attribs[ CONFIG_ALPHAMASKSIZE ] = '>=8'

        state = setupVG( modules, indexTracker, target, attribs )

        maskLayerIndex = 0
        OpenVG.CreateMaskLayer( maskLayerIndex, width, height )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.lineCount + 1 )
        y       = yoffset

        pathCoordinates = []
        for i in range( self.lineCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )            
            OpenVG.Lines( pathDataIndex,
                          [ scale * ( width - xoffset ), y * scale ],
                          'OFF' )
            OpenVG.ClosePath( pathDataIndex )
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           2,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.Mask( maskLayerIndex, 'MASK_LAYER', 'VG_CLEAR_MASK', 0, 0, width, height )
        OpenVG.RenderToMask( pathIndex, [ 'VG_STROKE_PATH' ], 'VG_SET_MASK' )

        OpenVG.Enable( 'VG_MASKING' )

        strokePaintIndex = 1
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.5, 0.5, 0.5, 0.5 ] )

        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class RenderWithDynamicMaskBenchmark( Benchmark ):
    def __init__( self, lineCount, strokeWidth, strokeCapStyle ):
        Benchmark.__init__( self )
        self.lineCount      = lineCount
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.name = "OPENVG render with dynamic mask %d lines strkwdth=%d strkcpstle=%s" % ( self.lineCount,
                                                                                             self.strokeWidth,
                                                                                             strokeCapStyleToStr( self.strokeCapStyle ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENVG )
        attribs[ CONFIG_ALPHAMASKSIZE ] = '>=8'

        state = setupVG( modules, indexTracker, target, attribs )

        maskLayerIndex = 0
        OpenVG.CreateMaskLayer( maskLayerIndex, width, height )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.lineCount + 1 )
        y       = yoffset

        pathCoordinates = []
        for i in range( self.lineCount ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )            
            OpenVG.Lines( pathDataIndex,
                          [ scale * ( width - xoffset ), y * scale ],
                          'OFF' )
            OpenVG.ClosePath( pathDataIndex )
            y += yoffset

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           2,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.Mask( maskLayerIndex, 'MASK_LAYER', 'VG_CLEAR_MASK', 0, 0, width, height )
        OpenVG.RenderToMask( pathIndex, [ 'VG_STROKE_PATH' ], 'VG_SET_MASK' )

        OpenVG.Enable( 'VG_MASKING' )

        strokePaint2Index = 1
        OpenVG.CreatePaint( strokePaint2Index )
        OpenVG.ColorPaint( strokePaint2Index, [ 0.5, 0.5, 0.5, 0.5 ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )        
        OpenVG.Mask( maskLayerIndex, 'MASK_LAYER', 'VG_CLEAR_MASK', 0, 0, width, height )        
        OpenVG.RenderToMask( pathIndex, [ 'VG_STROKE_PATH' ], 'VG_SET_MASK' )
        
        OpenVG.Clear( 0, 0, width, height )

        OpenVG.SetPaint( strokePaint2Index, [ 'VG_STROKE_PATH' ] )                
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class CreateMaskLayerBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG create mask layer"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENVG )
        attribs[ CONFIG_ALPHAMASKSIZE ] = '>=8'
        
        state = setupVG( modules, indexTracker, target, attribs )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        maskLayerIndex = 0
        OpenVG.CreateMaskLayer( maskLayerIndex, width, height )
        OpenVG.DestroyMaskLayer( maskLayerIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class FillMaskLayerBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG fill mask layer"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENVG )
        attribs[ CONFIG_ALPHAMASKSIZE ] = '>=8'
        
        state = setupVG( modules, indexTracker, target, attribs )

        maskLayerIndex = 0
        OpenVG.CreateMaskLayer( maskLayerIndex, width, height )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.FillMaskLayer( maskLayerIndex, 0, 0, width, height, 0.5 )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class CopyMaskLayerBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )
        self.name = "OPENVG copy mask layer"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        attribs = target.getDefaultAttributes( API_OPENVG )
        attribs[ CONFIG_ALPHAMASKSIZE ] = '>=8'

        state = setupVG( modules, indexTracker, target, attribs )

        maskLayerIndex = 0
        OpenVG.CreateMaskLayer( maskLayerIndex, width, height )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.CopyMask( maskLayerIndex, 0, 0, 0, 0, width, height )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        
