#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.egl       import *
from common        import *

######################################################################
READPIXELS  = 'RDPXLS'
COPYBUFFERS = 'CPBFFRS'
FINISH      = 'FNSH'

CONFIGS     = { 'SCT_COLOR_FORMAT_RGBA5551'  : ( 16, 5, 5, 5, 1, 'VG_sRGBA_5551' ),
                'SCT_COLOR_FORMAT_RGBA4444'  : ( 16, 4, 4, 4, 4, 'VG_sRGBA_4444' ),
                'SCT_COLOR_FORMAT_RGB565'    : ( 16, 5, 6, 5, 0, 'VG_sRGB_565' ),
                'SCT_COLOR_FORMAT_RGB888'    : ( 24, 8, 8, 8, 0, 'VG_sRGBX_8888' ),
                'SCT_COLOR_FORMAT_RGBA8888'  : ( 32, 8, 8, 8, 8, 'VG_sRGBA_8888_PRE' ) }


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ResizeWindowBenchmark( Benchmark ):  
    def __init__( self, overdraw, extent1, extent2 ):
        Benchmark.__init__( self )
        self.overdraw = overdraw
        self.extent1  = extent1
        self.extent2  = extent2
        self.name = "OPENVG resize window ovdrw=%d from=%s to=%s" % ( self.overdraw,
                                                                      transformToStr( self.extent1 ),
                                                                      transformToStr( self.extent2 ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )

        x = 0
        y = 0
        w = width
        h = height
        if self.extent1:
            x = self.extent1[ 0 ]
            y = self.extent1[ 1 ]
            w = self.extent1[ 2 ]
            h = self.extent1[ 3 ]
            
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT', 
                          x,
                          y,
                          w,
                          h,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENVG )

        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           vgConfigIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         vgSurfaceIndex,
                         vgSurfaceIndex,
                         vgContextIndex )
        
        Egl.SwapInterval( displayIndex, 0 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,               scale * height,
                        scale * width,   scale * height,
                        scale * width,   0,
                        0,               0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        x = 0
        y = 0
        w = width
        h = height
        if self.extent1:
            x = self.extent1[ 0 ]
            y = self.extent1[ 1 ]
            w = self.extent1[ 2 ]
            h = self.extent1[ 3 ]

        if self.extent1 and self.extent2:
            Egl.ResizeWindow( windowIndex,
                              x,
                              y,
                              w,
                              h )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )

        x = 0
        y = 0
        w = width
        h = height
        if self.extent2:
            x = self.extent2[ 0 ]
            y = self.extent2[ 1 ]
            w = self.extent2[ 2 ]
            h = self.extent2[ 3 ]

        if self.extent1 and self.extent2:            
            Egl.ResizeWindow( windowIndex,
                              x,
                              y,
                              w,
                              h )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class OverlappingWindowsBenchmark( Benchmark ):  
    def __init__( self, count, swapInterval, fullOverlap ):
        Benchmark.__init__( self )
        self.count        = count
        self.swapInterval = swapInterval
        self.fullOverlap  = fullOverlap
        self.name = "OPENVG overlapping windows count=%d swpintrvl=%d fullvrlp=%s" % ( self.count,
                                                                                       self.swapInterval,
                                                                                       boolToStr( self.fullOverlap ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' )
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' ) 

        attrs = target.getDefaultAttributes( API_OPENVG )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        windowIndices  = []
        surfaceIndices = []

        shrink = 1
        if self.fullOverlap:
            shrink = 0
        
        for i in range( self.count ):
            windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
            Egl.CreateWindow( windowIndex,
                              'SCT_SCREEN_DEFAULT',
                               i * shrink, i * shrink,
                              width - 2 * i * shrink, height - 2 * i * shrink,
                              'SCT_COLOR_FORMAT_DEFAULT' )

            surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
            Egl.CreateWindowSurface( displayIndex,
                                     surfaceIndex,
                                     configIndex,
                                     windowIndex,
                                     'EGL_BACK_BUFFER',
                                     'EGL_VG_COLORSPACE_sRGB',
                                     'EGL_ALPHA_FORMAT_PRE' )

            Egl.WindowToFront( windowIndex )
            
            windowIndices.append( windowIndex )
            surfaceIndices.append( surfaceIndex )
                    
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndices[ 0 ],
                         surfaceIndices[ 0 ],
                         vgContextIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        
        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * height,
                        scale * width,  scale * height,
                        scale * width,  0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.CheckError( '' )           

        Egl.SwapInterval( displayIndex, self.swapInterval )
               
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.count ):
            Egl.MakeCurrent( displayIndex,
                             surfaceIndices[ i ],
                             surfaceIndices[ i ],
                             vgContextIndex )
            Egl.SwapInterval( displayIndex, self.swapInterval )        
        
            OpenVG.Clear( 0, 0, width, height )        
            OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )        
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )        
            Egl.SwapBuffers( displayIndex, surfaceIndices[ i ] )
            

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class TopWindowBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, swapInterval ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.swapInterval = swapInterval
        self.name = "OPENVG top windows bg=1 grid=%dx%d swpintrvl=%d" % ( self.gridx,
                                                                          self.gridy,
                                                                          self.swapInterval )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' )
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' ) 
        
        parentWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( parentWindowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENVG )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        parentSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 parentSurfaceIndex,
                                 configIndex,
                                 parentWindowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )

        childSurfaces = []

        if self.gridx != 0 and self.gridy != 0:
            margin        = 5
            childWidth    = ( width - margin * 2 ) / self.gridx
            childHeight   = ( height - margin * 2 ) / self.gridy
            for y in range( self.gridy ):
                for x in range( self.gridx ):
                    childWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
                    Egl.CreateWindow( childWindowIndex,
                                      'SCT_SCREEN_DEFAULT',
                                      x * childWidth + margin,
                                      y * childHeight + margin,
                                      childWidth - 1, childHeight - 1,
                                      'SCT_COLOR_FORMAT_DEFAULT' )
                   
                    childSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                    Egl.CreateWindowSurface( displayIndex,
                                             childSurfaceIndex,
                                             configIndex,
                                             childWindowIndex,
                                             'EGL_BACK_BUFFER',
                                             'EGL_VG_COLORSPACE_sRGB',
                                             'EGL_ALPHA_FORMAT_PRE' )
            
                    childSurfaces.append( childSurfaceIndex )
            
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         parentSurfaceIndex,
                         parentSurfaceIndex,
                         vgContextIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        parentPaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( parentPaintIndex )
        OpenVG.LinearGradientPaint( parentPaintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( parentPaintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( parentPaintIndex, [ 'VG_FILL_PATH' ] )

        childPaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( childPaintIndex )
        OpenVG.LinearGradientPaint( childPaintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( childPaintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 0, 1, 1 ] )
        
        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * height,
                        scale * width,  scale * height,
                        scale * width,  0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.CheckError( '' )           

        Egl.SwapInterval( displayIndex, self.swapInterval )
               
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        ##########
        Egl.MakeCurrent( displayIndex,
                         parentSurfaceIndex,
                         parentSurfaceIndex,
                         vgContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )        
        
        OpenVG.Clear( 0, 0, width, height )        
        OpenVG.SetPaint( parentPaintIndex, [ 'VG_FILL_PATH' ] )        
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )        
        Egl.SwapBuffers( displayIndex, parentSurfaceIndex )
        
        for childSurface in childSurfaces:
            Egl.MakeCurrent( displayIndex,
                             childSurface,
                             childSurface,
                             vgContextIndex )

            Egl.SwapInterval( displayIndex, self.swapInterval )
            
            OpenVG.Clear( 0, 0, width, height )
            OpenVG.SetPaint( childPaintIndex, [ 'VG_FILL_PATH' ] )
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )        
            Egl.SwapBuffers( displayIndex, childSurface )
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ChildWindowBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, swapInterval ):
        Benchmark.__init__( self )
        self.gridx        = gridx
        self.gridy        = gridy
        self.swapInterval = swapInterval
        self.name = "OPENVG child windows bg=1 grid=%dx%d swpintrvl=%d" % ( self.gridx,
                                                                            self.gridy,
                                                                            self.swapInterval )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' )
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' ) 
        
        parentWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( parentWindowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )
        
        attrs = target.getDefaultAttributes( API_OPENVG )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        parentSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 parentSurfaceIndex,
                                 configIndex,
                                 parentWindowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )

        childSurfaces = []

        if self.gridx != 0 and self.gridy != 0:
            margin        = 5
            childWidth    = ( width - margin * 2 ) / self.gridx
            childHeight   = ( height - margin * 2 ) / self.gridy
            for y in range( self.gridy ):
                for x in range( self.gridx ):
                    childWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
                    Egl.CreateChildWindow( childWindowIndex,
                                           parentWindowIndex,
                                           x * childWidth + margin,
                                           y * childHeight + margin,
                                           childWidth - 1, childHeight - 1,
                                           'SCT_COLOR_FORMAT_DEFAULT' )
                   
                    childSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                    Egl.CreateWindowSurface( displayIndex,
                                             childSurfaceIndex,
                                             configIndex,
                                             childWindowIndex,
                                             'EGL_BACK_BUFFER',
                                             'EGL_VG_COLORSPACE_sRGB',
                                             'EGL_ALPHA_FORMAT_PRE' )
            
                    childSurfaces.append( childSurfaceIndex )
            
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         parentSurfaceIndex,
                         parentSurfaceIndex,
                         vgContextIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        parentPaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( parentPaintIndex )
        OpenVG.LinearGradientPaint( parentPaintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( parentPaintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( parentPaintIndex, [ 'VG_FILL_PATH' ] )

        childPaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( childPaintIndex )
        OpenVG.LinearGradientPaint( childPaintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( childPaintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 0, 1, 1 ] )
        
        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * height,
                        scale * width,  scale * height,
                        scale * width,  0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.CheckError( '' )           

        Egl.SwapInterval( displayIndex, self.swapInterval )
               
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        ##########
        Egl.MakeCurrent( displayIndex,
                         parentSurfaceIndex,
                         parentSurfaceIndex,
                         vgContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )
        
        OpenVG.Clear( 0, 0, width, height )        
        OpenVG.SetPaint( parentPaintIndex, [ 'VG_FILL_PATH' ] )        
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )        
        Egl.SwapBuffers( displayIndex, parentSurfaceIndex )
        
        for childSurface in childSurfaces:
            Egl.MakeCurrent( displayIndex,
                             childSurface,
                             childSurface,
                             vgContextIndex )

            Egl.SwapInterval( displayIndex, self.swapInterval )
            
            OpenVG.Clear( 0, 0, width, height )
            OpenVG.SetPaint( childPaintIndex, [ 'VG_FILL_PATH' ] )
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )        
            Egl.SwapBuffers( displayIndex, childSurface )

            
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------        
class PbuffersBenchmark( Benchmark ):  
    def __init__( self, gridx, gridy, format, flush ):
        Benchmark.__init__( self )
        self.gridx  = gridx
        self.gridy  = gridy
        self.format = format
        self.flush  = flush
        if self.gridx <= 0 or self.gridy <= 0:
            raise 'Invalid grid'
        self.name = "OPENVG pbuffers grid=%dx%d frmt=%s flsh=%s" % ( self.gridx,
                                                                     self.gridy,
                                                                     pixmapFormatToStr( self.format ),
                                                                     self.flush, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' )
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' ) 

        config = CONFIGS[ self.format ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       config[ 0 ] ,
                       config[ 1 ],
                       config[ 2 ],
                       config[ 3 ],
                       config[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        surfaces = []

        childWidth    = width / self.gridx
        childHeight   = height / self.gridy
        for y in range( self.gridy ):
            for x in range( self.gridx ):
                surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                Egl.CreatePbufferSurface( displayIndex,
                                          surfaceIndex,
                                          configIndex,
                                          childWidth,
                                          childHeight,
                                          'EGL_NO_TEXTURE',
                                          'EGL_NO_TEXTURE',
                                          'OFF',
                                          'OFF',
                                          'EGL_VG_COLORSPACE_sRGB',
                                          'EGL_ALPHA_FORMAT_PRE' )
                
                surfaces.append( surfaceIndex )
            
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaces[ 0 ],
                         surfaces[ 0 ],
                         vgContextIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * height,
                        scale * width,  scale * height,
                        scale * width,  0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        pixmapIndex    = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        if self.flush == READPIXELS:
            OpenVG.CreateNullImageData( imageDataIndex, config[ 5 ], childWidth, childHeight )
        if self.flush == COPYBUFFERS:
            Egl.CreatePixmap( pixmapIndex, childWidth, childHeight, self.format, -1 )
        
        OpenVG.CheckError( '' )           
               
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        ##########
        for surface in surfaces:
            Egl.MakeCurrent( displayIndex,
                             surface,
                             surface,
                             vgContextIndex )
            Egl.SwapInterval( displayIndex, 0 )
            
            OpenVG.Clear( 0, 0, width, height )
            OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

            if self.flush == READPIXELS:
                OpenVG.ReadPixels( imageDataIndex, 0, 0, childWidth, childHeight )
            
            if self.flush == COPYBUFFERS:
                Egl.CopyBuffers( displayIndex, surface, pixmapIndex )
            
            if self.flush == FINISH:
                OpenVG.Finish()
            
       
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class RotateScreenBenchmark( Benchmark ):  
    def __init__( self, overdraw, orientation, swapInterval ):
        Benchmark.__init__( self )
        self.overdraw     = overdraw
        self.orientation  = orientation
        self.swapInterval = swapInterval
        self.name = "OPENVG screen orientation %s ovdrw=%d swpntrvl=%d" % ( orientationToStr( self.orientation ),
                                                                            self.overdraw,
                                                                            self.swapInterval, )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' )
        Egl.Initialize( displayIndex )
        
        x = 0
        y = 0
        w = width
        h = height

        if defaultOrientation( width, height ) != self.orientation:
            w = height
            h = width
            Egl.ScreenOrientation( 'SCT_SCREEN_DEFAULT', self.orientation )
                      
        Egl.BindApi( 'EGL_OPENVG_API' )
            
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT', 
                          x,
                          y,
                          w,
                          h,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENVG )

        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           vgConfigIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         vgSurfaceIndex,
                         vgSurfaceIndex,
                         vgContextIndex )
        
        Egl.SwapInterval( displayIndex, self.swapInterval )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ w, h ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,           scale * h,
                        scale * w,   scale * h,
                        scale * w,   0,
                        0,           0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )
        
        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class OpaqueWindowBenchmark( Benchmark ):  
    def __init__( self, overdraw, opacity ):
        Benchmark.__init__( self )
        self.overdraw = overdraw
        self.opacity  = opacity  
        self.name = "OPENVG opaque window opacity=%.2f ovdrw=%d" % ( self.opacity,
                                                                     self.overdraw, )
        #self.repeats = 512
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' )
        Egl.Initialize( displayIndex )
        
        x = 0
        y = 0
        w = width
        h = height
                      
        Egl.BindApi( 'EGL_OPENVG_API' )
            
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT', 
                          x,
                          y,
                          w,
                          h,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENVG )

        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           vgConfigIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         vgSurfaceIndex,
                         vgSurfaceIndex,
                         vgContextIndex )
        
        Egl.SwapInterval( displayIndex, 0 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ w, h ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,           scale * h,
                        scale * w,   scale * h,
                        scale * w,   0,
                        0,           0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        if self.opacity < 1.0:
            Egl.WindowOpacity( windowIndex, self.opacity )

        OpenVG.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )
        
        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class OverlayWindowBenchmark( Benchmark ):  
    def __init__( self, overdraw, opacity, ppalpha, swapInterval ):
        Benchmark.__init__( self )
        self.overdraw     = overdraw
        self.opacity      = opacity
        self.ppalpha      = ppalpha
        self.swapInterval = swapInterval
        self.name = "OPENVG overlay window ovdrw=%d opacity=%.2f pr.pxl.alpha=%s swpntrvl=%d " % ( self.overdraw,
                                                                                                   self.opacity,
                                                                                                   { True : 'yes', False : 'no' }[ self.ppalpha ],
                                                                                                   self.swapInterval, )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        x = 0
        y = 0
        w = width
        h = height
                                      
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' )
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
            
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex,
                          'SCT_SCREEN_DEFAULT', 
                          x,
                          y,
                          w,
                          h,
                          'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENVG )

        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, vgContextIndex, vgConfigIndex, -1, -1 )

        Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )

        # OVERLAY WINDOW
        overlayWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( overlayWindowIndex, 'SCT_SCREEN_DEFAULT', x, y, w, h, 'SCT_COLOR_FORMAT_DEFAULT' )

        # Create a window surface to overlay window using surface scaling.
        overlaySurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )           
        Egl.CreateWindowSurface( displayIndex,
                                 overlaySurfaceIndex,
                                 vgConfigIndex,
                                 overlayWindowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )
        
        overlayContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, overlayContextIndex, vgConfigIndex, -1, -1 )

        Egl.MakeCurrent( displayIndex, overlaySurfaceIndex, overlaySurfaceIndex, overlayContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )

        if not self.ppalpha:
            if self.opacity < 1.0:
                Egl.WindowOpacity( overlayWindowIndex, self.opacity )
            self.opacity = 1.0
        else:
            Egl.WindowAlpha( overlayWindowIndex, 'ON' )
        
        # Clear the overlay surface to semitransparent black and swap a couple of times.
        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, self.opacity ] )
        
        for i in range( 3 ):
            OpenVG.Clear( 0, 0, w, h )
            Egl.SwapBuffers( displayIndex, overlaySurfaceIndex )           

        Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContextIndex )          
        
        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ w, h ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,           scale * h,
                        scale * w,   scale * h,
                        scale * w,   0,
                        0,           0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenVG.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )
        
        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )


CONFIGS     = { 'SCT_COLOR_FORMAT_RGBA5551'  : ( 16, 5, 5, 5, 1, 'VG_sRGBA_5551' ),
                'SCT_COLOR_FORMAT_RGBA4444'  : ( 16, 4, 4, 4, 4, 'VG_sRGBA_4444' ),
                'SCT_COLOR_FORMAT_RGB565'    : ( 16, 5, 6, 5, 0, 'VG_sRGB_565' ),
                'SCT_COLOR_FORMAT_RGB888'    : ( 24, 8, 8, 8, 0, 'VG_sRGBX_8888' ),
                'SCT_COLOR_FORMAT_RGBA8888'  : ( 32, 8, 8, 8, 8, 'VG_sRGBA_8888_PRE' ) }
        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ScaledLandscapeBenchmark( Benchmark ):  
    def __init__( self, format, size, overdraw, overlay, swapInterval, shrink ):
        Benchmark.__init__( self )
        self.format       = format
        self.size         = size
        self.overdraw     = overdraw
        self.overlay      = overlay
        self.swapInterval = swapInterval
        self.shrink       = shrink
        if self.size == 'fullhd':
            self.surfaceSize  = [ 1920, 1080 ]
        elif self.size == '720p':
            self.surfaceSize  = [ 1280, 720 ]
        else:
            raise "Unexpected"
        #self.repeats  = 240
        self.name = "OPENVG scaled %s %s surface%s ovdrw=%d swpntrvl=%d%s" % ( pixmapFormatToStr( self.format ),
                                                                               self.size,
                                                                               { True : ' with transparent overlay', False : '' }[ self.overlay ],
                                                                               self.overdraw,
                                                                               self.swapInterval,
                                                                               { True : ' with shrink', False : '' }[ self.shrink ], )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' )
        Egl.Initialize( displayIndex )

        Egl.CheckExtension( displayIndex, 'EGL_NOK_surface_scaling' )

        # Go to landscape mode.
        x = 0
        y = 0        
        w = height
        h = width
        Egl.ScreenOrientation( 'SCT_SCREEN_DEFAULT', 'SCT_SCREEN_ORIENTATION_LANDSCAPE' )

        Egl.BindApi( 'EGL_OPENVG_API' )

        # Create main window 
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, w, h, 'SCT_COLOR_FORMAT_DEFAULT' )

        # Create main window surface using surface scaling.
        config      = CONFIGS[ self.format ]
        surfaceBits = EGL_DEFINES[ 'EGL_WINDOW_BIT' ] | EGL_DEFINES[ 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ]
        
        configAttributes = [ EGL_DEFINES[ 'EGL_BUFFER_SIZE' ],         config[ 0 ],
                             EGL_DEFINES[ 'EGL_RED_SIZE' ],            config[ 1 ],
                             EGL_DEFINES[ 'EGL_GREEN_SIZE' ],          config[ 2 ],
                             EGL_DEFINES[ 'EGL_BLUE_SIZE' ],           config[ 3 ],
                             EGL_DEFINES[ 'EGL_ALPHA_SIZE' ],          config[ 4 ],
                             EGL_DEFINES[ 'EGL_SURFACE_TYPE' ],        surfaceBits,
                             EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],     EGL_DEFINES[ 'EGL_OPENVG_BIT' ],
                             EGL_DEFINES[ 'EGL_SURFACE_SCALING_NOK' ], EGL_DEFINES[ 'EGL_TRUE' ],
                             ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )            
        Egl.Config( displayIndex, configIndex, configAttributes )

        surfaceAttributes = [ EGL_DEFINES[ 'EGL_RENDER_BUFFER' ],              EGL_DEFINES[ 'EGL_BACK_BUFFER' ],
                              EGL_DEFINES[ 'EGL_COLORSPACE' ],                 EGL_DEFINES[ 'EGL_VG_COLORSPACE_sRGB' ],
                              EGL_DEFINES[ 'EGL_ALPHA_FORMAT' ],               EGL_DEFINES[ 'EGL_VG_ALPHA_FORMAT_PRE' ],
                              EGL_DEFINES[ 'EGL_FIXED_WIDTH_NOK' ],            self.surfaceSize[ 0 ],
                              EGL_DEFINES[ 'EGL_FIXED_HEIGHT_NOK' ],           self.surfaceSize[ 1 ],
                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_OFFSET_X_NOK' ], 0,
                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_OFFSET_Y_NOK' ], 0,
                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_WIDTH_NOK' ],    w,
                              EGL_DEFINES[ 'EGL_TARGET_EXTENT_HEIGHT_NOK' ],   h,
                              EGL_DEFINES[ 'EGL_BORDER_COLOR_RED_NOK' ],       127,
                              EGL_DEFINES[ 'EGL_BORDER_COLOR_GREEN_NOK' ],     127,
                              EGL_DEFINES[ 'EGL_BORDER_COLOR_BLUE_NOK' ],      127,
                              ]
        
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )           
        Egl.CreateWindowSurfaceExt( displayIndex, surfaceIndex, configIndex, windowIndex, surfaceAttributes )
      
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, -1 )
        
        if self.overlay:            
            # Create a transparent overlay window.
            overlayWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )

            sh = 0
            if self.shrink:
                sh = 1
                
            Egl.CreateWindow( overlayWindowIndex, 'SCT_SCREEN_DEFAULT', sh, sh, w - ( 2 * sh ), h - ( 2 * sh ), 'SCT_COLOR_FORMAT_DEFAULT' )

            # Enable per pixel alpha and ensure the window is in front.
            Egl.WindowAlpha( overlayWindowIndex, 'ON' )
            Egl.WindowToFront( overlayWindowIndex ) 

            overlaySurfaceAttributes = [ EGL_DEFINES[ 'EGL_RENDER_BUFFER' ],              EGL_DEFINES[ 'EGL_BACK_BUFFER' ],
                                         EGL_DEFINES[ 'EGL_COLORSPACE' ],                 EGL_DEFINES[ 'EGL_VG_COLORSPACE_sRGB' ],
                                         EGL_DEFINES[ 'EGL_ALPHA_FORMAT' ],               EGL_DEFINES[ 'EGL_VG_ALPHA_FORMAT_PRE' ],
                                         ]
            
            # Create a window surface to overlay window using surface scaling.
            overlaySurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )           
            Egl.CreateWindowSurfaceExt( displayIndex, overlaySurfaceIndex, configIndex, overlayWindowIndex, overlaySurfaceAttributes )
            
            overlayContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
            Egl.CreateContext( displayIndex, overlayContextIndex, configIndex, -1, -1 )

            Egl.MakeCurrent( displayIndex, overlaySurfaceIndex, overlaySurfaceIndex, overlayContextIndex )
            
            # Clear the overlay surface to semitransparent black and swap a couple of times.
            for i in range( 3 ):
                OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
                OpenVG.Clear( 0, 0, self.surfaceSize[ 0 ], self.surfaceSize[ 1 ] )           
                Egl.SwapBuffers( displayIndex, overlaySurfaceIndex )           

        #
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )

        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, 'VG_sRGBA_8888_PRE', self.surfaceSize[ 0 ], self.surfaceSize[ 1 ], [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( imageIndex, 0, 0, self.surfaceSize[ 0 ], self.surfaceSize[ 1 ] );
        
        OpenVG.CheckError( '' )

        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()
       
        for i in range( self.overdraw ):
            OpenVG.DrawImage( imageIndex )

        dx = self.surfaceSize[ 0 ] / 4
        dy = self.surfaceSize[ 1 ] / 4
        
        OpenVG.ClearColor( [ 1.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( self.surfaceSize[ 0 ] / 2 - 4, 0, 4, self.surfaceSize[ 1 ] )
        
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, self.surfaceSize[ 1 ] - dy, dx, dy )

        OpenVG.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )
        OpenVG.Clear( self.surfaceSize[ 0 ] - dx, 0, dx, dy )
        
        Egl.SwapBuffers( displayIndex, surfaceIndex )
