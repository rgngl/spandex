#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class RenderToImageBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, benchmarkSetup ):
        Benchmark.__init__( self)
        self.width          = width
        self.height         = height
        self.format         = format
        self.quality        = quality
        self.benchmarkSetup = benchmarkSetup
        self.name = "OPENVG render to image %dx%d frmt=%s qlty=%s bmrkstup=%s" % ( self.width,
                                                                                   self.height,
                                                                                   imageFormatToStr( self.format ),
                                                                                   imageQualityToStr( self.quality ),
                                                                                   boolToStr( self.benchmarkSetup ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]
        
        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENVG )
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )       
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT'])

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_VG_COLORSPACE_sRGB',
                                 'EGL_VG_ALPHA_FORMAT_PRE' )
       
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        formats = { 'VG_sRGBX_8888'      : ( 24, 8, 8, 8, 0 ),
                    'VG_sRGBA_8888'      : ( 32, 8, 8, 8, 8 ),
                    'VG_sRGBA_8888_PRE'  : ( 32, 8, 8, 8, 8 ),
                    'VG_sRGB_565'        : ( 16, 5, 6, 5, 0 ),
                    'VG_sRGBA_5551'      : ( 16, 5, 5, 5, 1 ),
                    'VG_sRGBA_4444'      : ( 16, 4, 4, 4, 4 ) }

        f = formats[ self.format ]
        
        imageConfigIndex  = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       imageConfigIndex,
                       f[ 0 ],
                       f[ 1 ],
                       f[ 2 ],
                       f[ 3 ],
                       f[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )
        
        imageSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )

        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        if not self.benchmarkSetup:        
            OpenVG.CreateImage( imageIndex,
                                self.format,
                                self.width,
                                self.height,
                                [ self.quality ] )
            
            Egl.CreatePbufferFromClientBufferExt( displayIndex,
                                                  imageSurfaceIndex,
                                                  imageConfigIndex,
                                                  'EGL_OPENVG_IMAGE',
                                                  imageIndex,
                                                  [] )

        imageContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           imageContextIndex,
                           imageConfigIndex,
                           -1,
                           -1 )
            
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )

        if self.benchmarkSetup:
            OpenVG.CreateImage( imageIndex,
                                self.format,
                                self.width,
                                self.height,
                                [ self.quality ] )
            
            Egl.CreatePbufferFromClientBufferExt( displayIndex,
                                                  imageSurfaceIndex,
                                                  imageConfigIndex,
                                                  'EGL_OPENVG_IMAGE',
                                                  imageIndex,
                                                  [] )
        
        Egl.MakeCurrent( displayIndex, imageSurfaceIndex, imageSurfaceIndex, imageContextIndex )
        OpenVG.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )
        OpenVG.Clear( 0, 0, self.width, self.height )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        OpenVG.DrawImage( imageIndex )
                                           
        Egl.SwapBuffers( displayIndex, surfaceIndex )

        if self.benchmarkSetup:
            Egl.DestroySurface( displayIndex, imageSurfaceIndex )
            OpenVG.DestroyImage( imageIndex )

