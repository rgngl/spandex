#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ColorTransformBenchmark( Benchmark ):  
    def __init__( self, overdraw, enabled ):
        Benchmark.__init__( self )
        self.overdraw  = overdraw
        self.enabled   = enabled
        self.transform = [ 1.0, 0.5, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0 ]
        self.name = "OPENVG color transform enbld=%s ovrdrw=%d" % ( boolToStr( self.enabled ),
                                                                    self.overdraw, )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * h,
                        scale * w,      scale * h,
                        scale * w,      0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.enabled:
            OpenVG.ColorTransform( self.transform )
            OpenVG.Enable( 'VG_COLOR_TRANSFORM' )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ColorTransformDrawImageBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, enabled ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.format      = format
        self.quality     = quality
        self.enabled     = enabled
        self.transform   = [ 1.0, 0.5, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0 ]
        self.name = "OPENVG draw image with color transform enbld=%s %dx%d frmt=%s qlty=%s" % ( self.enabled,
                                                                                                self.imageWidth,
                                                                                                self.imageHeight,
                                                                                                imageFormatToStr( self.format ),
                                                                                                imageQualityToStr( self.quality ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth, self.imageHeight,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( imageIndex, 0, 0, self.imageWidth, self.imageHeight );

        if self.enabled:
            OpenVG.ColorTransform( self.transform )
            OpenVG.Enable( 'VG_COLOR_TRANSFORM' )
        
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ImageQuality( self.quality )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.DrawImage( imageIndex )
        
        target.swapBuffers( state )
