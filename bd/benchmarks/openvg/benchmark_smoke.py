#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../..' )

import  bd

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'OpenVG smoke benchmark'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

#----------------------------------------------------------------------
( width, height, )      = target.getScreenSize()

#----------------------------------------------------------------------
import  fullscreen
OVERDRAW    = 3
MOSAIC_DIVX = 6
MOSAIC_DIVY = 8
suite.addBenchmark( fullscreen.ClearBenchmark() )
suite.addBenchmark( fullscreen.SynchronizedClearBenchmark() )

suite.addBenchmark( fullscreen.ColorPaintBenchmark( OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.ColorPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.ColorPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )

suite.addBenchmark( fullscreen.LinearGradientPaintBenchmark( OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.LinearGradientPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.LinearGradientPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )

suite.addBenchmark( fullscreen.RadialGradientPaintBenchmark( OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.RadialGradientPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )
suite.addBenchmark( fullscreen.RadialGradientPaintDiscreteBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 0 ) )

suite.addBenchmark( fullscreen.PatternPaintBenchmark( OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0 ) )
suite.addBenchmark( fullscreen.PatternPaintMosaicBenchmark( MOSAIC_DIVX, MOSAIC_DIVY, OVERDRAW, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0 ) )

#----------------------------------------------------------------------
import tiger
suite.addBenchmark( tiger.TigerBenchmark( [ 100, 100 ] ) )                                                                      
suite.addBenchmark( tiger.TigerBenchmark( [ 200, 200 ] ) )                                                                      
suite.addBenchmark( tiger.TigerBenchmark( [ 240, 320 ] ) )
suite.addBenchmark( tiger.TigerBenchmark( [ width, height ] ) )

#----------------------------------------------------------------------
import image

suite.addBenchmark( image.ImageSubDataBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( image.ReadPixelsBenchmark( width, height, 'VG_sRGBA_8888' ) )
suite.addBenchmark( image.DrawImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0, 1 ) )

#----------------------------------------------------------------------
import surfaces         # REQUIRES EGL
OVERDRAW = 3

suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGBA8888', surfaces.READPIXELS ) )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGBA8888', surfaces.COPYBUFFERS ) )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB565', surfaces.READPIXELS ) )
suite.addBenchmark( surfaces.PbufferSurfaceBenchmark( OVERDRAW, 'SCT_COLOR_FORMAT_RGB565', surfaces.COPYBUFFERS ) )

#----------------------------------------------------------------------
import ui

suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888_PRE', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888_PRE', 0 ) )
suite.addBenchmark( ui.UiBenchmark( ui.DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888', 1 ) )
suite.addBenchmark( ui.UiBenchmark( ui.UPLOAD_AND_DRAW, [ ui.BACKGROUND, ui.PANELS, ui.GROUPS, ui.ICONS ], 'VG_sRGBA_8888', 1 ) )

#----------------------------------------------------------------------
command()



