#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import   Benchmark
from lib.common    import   *
from common        import   *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreateImageBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, upload ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.format      = format
        self.quality     = quality
        self.upload      = upload
        self.name = "OPENVG create image%s %dx%d frmat=%s qlty=%s" % (  { True : ' and upload pixel', False : '' }[ self.upload ],
                                                                        self.imageWidth,
                                                                        self.imageHeight,
                                                                        imageFormatToStr( self.format ),
                                                                        imageQualityToStr( self.quality ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.ImageQuality( self.quality )

        if self.upload:
            imageDataIndex = 0
            OpenVG.CreateNullImageData( imageDataIndex,
                                        self.format,
                                        1,
                                        1 )

        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )

        if self.upload:
            OpenVG.ImageSubData( imageDataIndex,
                                 imageIndex,
                                 self.imageWidth / 2, self.imageHeight / 2,
                                 1, 1 )

        # Try to ensure the image is actually created.
        OpenVG.Finish()
            
        OpenVG.DestroyImage( imageIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ClearImageBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.format      = format
        self.quality     = quality
        self.name = "OPENVG clear image %dx%d frmat=%s qlty=%s" % ( self.imageWidth,
                                                                    self.imageHeight,
                                                                    imageFormatToStr( self.format ),
                                                                    imageQualityToStr( self.quality ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.ClearImage( imageIndex,
                           0, 0,
                           self.imageWidth, self.imageHeight )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ImageSubDataBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.format      = format
        self.quality     = quality       
        self.name = "OPENVG image subdata %dx%d frmat=%s qlty=%s" % ( self.imageWidth,
                                                                      self.imageHeight,
                                                                      imageFormatToStr( self.format ),
                                                                      imageQualityToStr( self.quality ), )
       
    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageDataIndex = 0
        OpenVG.CreateNullImageData( imageDataIndex,
                                    self.format,
                                    self.imageWidth,
                                    self.imageHeight )

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )
        
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.ImageSubData( imageDataIndex,
                             imageIndex,
                             0, 0,
                             self.imageWidth, self.imageHeight )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class GetImageSubDataBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.format      = format
        self.quality     = quality
        self.name = "OPENVG get image subdata %dx%d frmat=%s qlty=%s" % ( self.imageWidth,
                                                                          self.imageHeight,
                                                                          imageFormatToStr( self.format ),
                                                                          imageQualityToStr( self.quality ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageDataIndex = 0
        OpenVG.CreateNullImageData( imageDataIndex,
                                    self.format,
                                    self.imageWidth,
                                    self.imageHeight )

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )
        
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )      
        target.swapBuffers( state )

        OpenVG.ClearImage( imageIndex,
                           0, 0,
                           self.imageWidth, self.imageHeight )

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.GetImageSubData( imageIndex,
                                imageDataIndex,
                                0, 0,
                                self.imageWidth, self.imageHeight )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ChildImageBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.format      = format
        self.quality     = quality
        self.name = "OPENVG child image %dx%d frmat=%s qlty=%s" % ( self.imageWidth,
                                                                    self.imageHeight,
                                                                    imageFormatToStr( self.format ),
                                                                    imageQualityToStr( self.quality ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )
        
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )      
        target.swapBuffers( state )

        OpenVG.ClearImage( imageIndex,
                           0, 0,
                           self.imageWidth, self.imageHeight )

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        childImageIndex = 1
        OpenVG.ChildImage( childImageIndex,
                           imageIndex,
                           0, 0,
                           self.imageWidth, self.imageHeight )
                          
        OpenVG.DestroyImage( childImageIndex )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CopyImageBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, dither ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height       
        self.format      = format
        self.quality     = quality
        self.dither      = dither
        self.name = "OPENVG image copy %dx%d frmat=%s qlty=%s dthr=%s" % ( self.imageWidth,
                                                                           self.imageHeight,
                                                                           imageFormatToStr( self.format ),
                                                                           imageQualityToStr( self.quality ),
                                                                           boolToStr( self.dither ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        image1Index = 0
        OpenVG.CreateImage( image1Index,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )

        image2Index = 1
        OpenVG.CreateImage( image2Index,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )      
        target.swapBuffers( state )

        OpenVG.ClearImage( image1Index,
                           0, 0,
                           self.imageWidth, self.imageHeight )

        OpenVG.ClearImage( image2Index,
                           0, 0,
                           self.imageWidth, self.imageHeight )

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        s = 'OFF'
        if self.dither:
            s = 'ON'

        OpenVG.CopyImage( image2Index,
                          0, 0,
                          image1Index,
                          0, 0,
                          self.imageWidth, self.imageHeight,
                          s )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class SetPixelsBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height       
        self.format      = format
        self.quality     = quality
        self.name = "OPENVG set pixels %dx%d frmat=%s qlty=%s" % ( self.imageWidth,
                                                                   self.imageHeight,
                                                                   imageFormatToStr( self.format ),
                                                                   imageQualityToStr( self.quality ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )
        
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )      
        target.swapBuffers( state )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenVG.ClearImage( imageIndex,
                           0, 0,
                           self.imageWidth, self.imageHeight )

        OpenVG.ImageQuality( self.quality )
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.SetPixels( 0, 0,
                          imageIndex,
                          0, 0,
                          self.imageWidth, self.imageHeight )
        
        target.swapBuffers( state )
      

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class WritePixelsBenchmark( Benchmark ):  
    def __init__( self, width, height, format ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height       
        self.format      = format
        self.name = "OPENVG write pixels %dx%d frmat=%s" % ( self.imageWidth,
                                                             self.imageHeight,
                                                             imageFormatToStr( self.format ), )
       
    def build( self, target, modules ):         
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageDataIndex = 0
        OpenVG.CreateNullImageData( imageDataIndex,
                                    self.format,
                                    self.imageWidth, self.imageHeight )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )

        OpenVG.ReadPixels( imageDataIndex,
                           0, 0,
                           self.imageWidth, self.imageHeight )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )      
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.WritePixels( imageDataIndex,
                            0, 0,
                            self.imageWidth, self.imageHeight )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ReadPixelsBenchmark( Benchmark ):  
    def __init__( self, width, height, format ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height                     
        self.format      = format
        self.name = "OPENVG read pixels %dx%d frmat=%s" % ( self.imageWidth,
                                                            self.imageHeight,
                                                            imageFormatToStr( self.format ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageDataIndex = 0
        OpenVG.CreateNullImageData( imageDataIndex,
                                    self.format,
                                    self.imageWidth, self.imageHeight )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.ReadPixels( imageDataIndex,
                           0, 0,
                           self.imageWidth, self.imageHeight  )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class GetPixelsBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height              
        self.format      = format
        self.quality     = quality
        self.name = "OPENVG get pixels %dx%d frmat=%s qlty=%s" % ( self.imageWidth,
                                                                   self.imageHeight,
                                                                   imageFormatToStr( self.format ),
                                                                   imageQualityToStr( self.quality ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )
        
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )      
        target.swapBuffers( state )

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.GetPixels( imageIndex,
                          0, 0,
                          0, 0, 
                          self.imageWidth, self.imageHeight )

        # ------------------------------------------------------------
        # Start final actions
        self.beginFinalActions()

        # Ensure all benchmark loop actions have finished before timing
        OpenVG.Finish()
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CopyPixelsBenchmark( Benchmark ):  
    def __init__( self, dx, dy, sx, sy, width, height, clears ):
        Benchmark.__init__( self )
        self.dx      = dx
        self.dy      = dy
        self.sx      = sx
        self.sy      = sy
        self.cwidth  = width
        self.cheight = height
        self.clears  = clears
        self.name = "OPENVG copy pixels dx=%d dy=%d sx=%d sy=%d wdth=%d hght=%d clrs=%s" % ( self.dx,
                                                                                             self.dy,
                                                                                             self.sx,
                                                                                             self.sy,
                                                                                             self.cwidth,
                                                                                             self.cheight,
                                                                                             boolToStr( self.clears ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
       
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )

        if self.clears:
            OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )        
            OpenVG.Clear( self.dx, self.dy, self.cwidth, self.cheight )
        
            OpenVG.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )        
            OpenVG.Clear( self.sx, self.sy, self.cwidth, self.cheight )
            
        OpenVG.CopyPixels( self.dx, self.dy,
                           self.sx, self.sy,
                           self.cwidth, self.cheight )
                                                                             
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class DrawImageBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, rotation, scale ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.format      = format
        self.quality     = quality
        self.rotation    = rotation
        self.scale       = scale
        self.name = "OPENVG draw image %dx%d frmat=%s qlty=%s rot=%d scle=%.2f" % ( self.imageWidth,
                                                                                    self.imageHeight,
                                                                                    imageFormatToStr( self.format ),
                                                                                    imageQualityToStr( self.quality ),
                                                                                    self.rotation,
                                                                                    self.scale, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        cx = width / 2
        cy = height / 2
        
        icx = self.imageWidth / 2
        icy = self.imageHeight / 2
                                                                             
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth, self.imageHeight,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( imageIndex, 0, 0, self.imageWidth, self.imageHeight );

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        OpenVG.LoadIdentity()
        if self.rotation != 0.0 or self.scale != 1.0:
            OpenVG.Translate( icx, icy )
            if self.rotation != 0.0:
                OpenVG.Rotate( self.rotation ) 
            OpenVG.Translate( -icx, -icy )
            if self.scale != 1.0:
                OpenVG.Scale( self.scale, self.scale )              
            
        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )            

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.DrawImage( imageIndex )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class DrawChildImageBenchmark( Benchmark ):  
    def __init__( self, width, height, childWidth, childHeight, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth       = width
        self.imageHeight      = height
        self.childImageWidth  = childWidth
        self.childImageHeight = childHeight
        self.format             = format
        self.quality            = quality
        self.name = "OPENVG draw child, parent %dx%d child %dx%d frmat=%s qlty=%s" % ( self.imageWidth,
                                                                                       self.imageHeight,
                                                                                       self.childImageWidth,
                                                                                       self.childImageHeight,
                                                                                       imageFormatToStr( self.format ),
                                                                                       imageQualityToStr( self.quality ), )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
                                                                                    
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth, self.imageHeight,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( imageIndex, 0, 0, self.imageWidth, self.imageHeight );

        offsetx = ( self.imageWidth - self.childImageWidth ) / 2
        offsety = ( self.imageHeight - self.childImageHeight ) / 2
        
        childImageIndex = 1
        OpenVG.ChildImage( childImageIndex,
                           imageIndex,
                           offsetx, offsety,
                           self.childImageWidth, self.childImageHeight )       

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.DrawImage( childImageIndex )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreateAndDrawChildImageBenchmark( Benchmark ):  
    def __init__( self, width, height, childWidth, childHeight, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth       = width
        self.imageHeight      = height
        self.childImageWidth  = childWidth
        self.childImageHeight = childHeight
        self.format             = format
        self.quality            = quality
        self.name = "OPENVG create and draw child, parent %dx%d child %dx%d frmat=%s qlty=%s" % ( self.imageWidth,
                                                                                                  self.imageHeight,
                                                                                                  self.childImageWidth,
                                                                                                  self.childImageHeight,
                                                                                                  imageFormatToStr( self.format ),
                                                                                                  imageQualityToStr( self.quality ), )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
                                                                                    
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth, self.imageHeight,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( imageIndex, 0, 0, self.imageWidth, self.imageHeight );

        offsetx = ( self.imageWidth - self.childImageWidth ) / 2
        offsety = ( self.imageHeight - self.childImageHeight ) / 2
        
        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        childImageIndex = 1
        OpenVG.ChildImage( childImageIndex,
                           imageIndex,
                           offsetx, offsety,
                           self.childImageWidth, self.childImageHeight )       
        
        OpenVG.DrawImage( childImageIndex )
        
        target.swapBuffers( state )

        OpenVG.DestroyImage( childImageIndex )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class CreateAndDrawScaledChildImageBenchmark( Benchmark ):  
    def __init__( self, width, height, childWidth, childHeight, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth       = width
        self.imageHeight      = height
        self.childImageWidth  = childWidth
        self.childImageHeight = childHeight
        self.format             = format
        self.quality            = quality
        self.name = "OPENVG create and draw scaled child, parent %dx%d child %dx%d frmat=%s qlty=%s" % ( self.imageWidth,
                                                                                                         self.imageHeight,
                                                                                                         self.childImageWidth,
                                                                                                         self.childImageHeight,
                                                                                                         imageFormatToStr( self.format ),
                                                                                                         imageQualityToStr( self.quality ), )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
                                                                                    
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth, self.imageHeight,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( imageIndex, 0, 0, self.imageWidth, self.imageHeight );
        
        OpenVG.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )
        OpenVG.ClearImage( imageIndex, 0, 0, self.imageWidth / 2, self.imageHeight / 2 );

        OpenVG.ClearColor( [ 1.0, 0.0, 1.0, 1.0 ] )
        OpenVG.ClearImage( imageIndex, self.imageWidth / 2, self.imageHeight / 2, self.imageWidth / 2, self.imageHeight / 2 );
        
        offsetx = ( self.imageWidth - self.childImageWidth ) / 2
        offsety = ( self.imageHeight - self.childImageHeight ) / 2

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        childImageIndex = 1
        OpenVG.ChildImage( childImageIndex,
                           imageIndex,
                           offsetx, offsety,
                           self.childImageWidth, self.childImageHeight )       

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        OpenVG.LoadIdentity()
        OpenVG.Scale( width / float( self.childImageWidth ), height / float( self.childImageHeight ) )
        OpenVG.DrawImage( childImageIndex )
        
        target.swapBuffers( state )

        OpenVG.DestroyImage( childImageIndex )

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ImageSubDataWithDrawBenchmark( Benchmark ):  
    def __init__( self, width, height, divx, divy, format, quality, interleave ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.divx        = divx
        self.divy        = divy
        self.format      = format
        self.quality     = quality
        self.interleave  = interleave
        self.name = "OPENVG image subdata with draw %dx%d div. %dx%d frmat=%s qlty=%s intrlv=%s" % ( self.imageWidth,
                                                                                                     self.imageHeight,
                                                                                                     self.divx,
                                                                                                     self.divy,
                                                                                                     imageFormatToStr( self.format ),
                                                                                                     imageQualityToStr( self.quality ),
                                                                                                     boolToStr( self.interleave ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageCount          = self.divx * self.divy;
        imageWidth          = self.imageWidth / self.divx;
        imageHeight         = self.imageHeight / self.divy;
        imageDataIndices    = []
        imageIndices        = []

        for i in range( imageCount ):
            OpenVG.CreateNullImageData( i,
                                        self.format,
                                        imageWidth,
                                        imageHeight )
            imageDataIndices.append( i )
            
            OpenVG.CreateImage( i,
                                self.format,
                                imageWidth,
                                imageHeight,
                                [ self.quality ] )
            imageIndices.append( i )
            
        for i in range( imageCount ):
            OpenVG.ClearColor( [ 0.0, 0.0, 1.0 * ( i + 1 ) / float( imageCount ), 1.0 ] )
            OpenVG.Clear( 0, 0, width, height )
            OpenVG.ReadPixels( imageDataIndices[ i ],
                               0, 0,
                               imageWidth, imageHeight )
            
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, width, height )
        target.swapBuffers( state )

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        i = 0
        for y in range( self.divy ):
            for x in range( self.divx ):
                OpenVG.ImageSubData( imageDataIndices[ i ],
                                     imageIndices[ i ],
                                     0, 0,
                                     imageWidth, imageHeight )

                if self.interleave:
                    OpenVG.LoadIdentity()
                    OpenVG.Translate( imageWidth * x, imageHeight * y )
                    OpenVG.DrawImage( imageIndices[ i ] )
                    
                i += 1

        i = 0                
        if not self.interleave:
            for y in range( self.divy ):
                for x in range( self.divx ):
                    OpenVG.LoadIdentity()
                    OpenVG.Translate( imageWidth * x, imageHeight * y )
                    OpenVG.DrawImage( imageIndices[ i ] )

                    i+= 1

        target.swapBuffers( state )
        

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class DrawImageSubRegionsBenchmark( Benchmark ):  
    def __init__( self, width, height, dimx, dimy, format, quality ):
        Benchmark.__init__( self )
        self.width   = width
        self.height  = height
        self.dimx    = dimx
        self.dimy    = dimy
        self.format  = format
        self.quality = quality
        self.name = "OPENVG draw image subregions %d%d div. %dx%d frmat=%s qlty=%s" % ( self.width,
                                                                                        self.height,
                                                                                        self.dimx,
                                                                                        self.dimy,
                                                                                        imageFormatToStr( self.format ),
                                                                                        imageQualityToStr( self.quality ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
                                                                                    
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.width, self.height,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( imageIndex, 0, 0, self.width, self.height );

        childImageIndex = 1
        ox              = 0
        oy              = 0
        cw              = self.width / self.dimx
        ch              = self.height / self.dimy
        offsets         = []
        imageIndices    = []
        
        for y in range( self.dimy ):
            for x in range( self.dimx ):
                OpenVG.ChildImage( childImageIndex,
                                   imageIndex,
                                   ox, oy,
                                   cw, ch )
                offsets.append( ( ox, oy, ) )
                imageIndices.append( childImageIndex )
                ox += cw                
                childImageIndex += 1
            ox = 0
            oy += ch
            
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( len( imageIndices ) ):
            OpenVG.LoadIdentity()            
            OpenVG.Translate( offsets[ i ][ 0 ], offsets[ i ][ 1 ] )
            OpenVG.DrawImage( imageIndices[ i ] )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class DrawOverlappingImageSubRegionsBenchmark( Benchmark ):  
    def __init__( self, width, height, dimx, dimy, format, quality ):
        Benchmark.__init__( self )
        self.width   = width
        self.height  = height
        self.dimx    = dimx
        self.dimy    = dimy
        self.format  = format
        self.quality = quality
        
        self.name = "OPENVG draw overlapping image subregions %dx%d div. %dx%d frmat=%s qlty=%s" % ( self.width,
                                                                                                     self.height,
                                                                                                     self.dimx,
                                                                                                     self.dimy,
                                                                                                     imageFormatToStr( self.format ),
                                                                                                     imageQualityToStr( self.quality ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
                                                                                    
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.width, self.height,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( imageIndex, 0, 0, self.width, self.height );

        childImageIndex = 1
        ox              = 0
        oy              = 0
        cw              = self.width / self.dimx
        ch              = self.height / self.dimy
        offsets         = []
        imageIndices    = []
        
        for y in range( self.dimy ):
            for x in range( self.dimx ):
                OpenVG.ChildImage( childImageIndex,
                                   imageIndex,
                                   ox, oy,
                                   cw, ch )
                offsets.append( ( ox, oy, ) )
                imageIndices.append( childImageIndex )
                ox += cw                
                childImageIndex += 1
            ox = 0
            oy += ch
            
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( len( imageIndices ) ):
            ox = offsets[ i ][ 0 ]
            oy = offsets[ i ][ 1 ]
            if ox > 0:
                ox -= 1
            OpenVG.LoadIdentity()               
            OpenVG.Translate( ox, oy )
            OpenVG.DrawImage( imageIndices[ i ] )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------                
# ----------------------------------------------------------------------        
class ImageModeBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, imageMode ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.format      = format
        self.quality     = quality       
        self.imageMode   = imageMode
        self.name = "OPENVG image mode %s %dx%d frmat=%s qlty=%s" % ( self.imageMode,
                                                                      self.imageWidth,
                                                                      self.imageHeight,
                                                                      imageFormatToStr( self.format ),
                                                                      imageQualityToStr( self.quality ), )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        cx = width / 2
        cy = height / 2
       
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth, self.imageHeight,
                            [ self.quality ] )

        paint1Index = 0
        OpenVG.CreatePaint( paint1Index )
        OpenVG.RadialGradientPaint( paint1Index, [ cx, cy ], [ 0, 0 ], max( width, height ) )
        OpenVG.ColorRamps( paint1Index,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )

        OpenVG.SetPaint( paint1Index, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * height,
                        scale * width,  scale * height,
                        scale * width,  0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.Clear( 0, 0, width, height )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        OpenVG.GetPixels( imageIndex,
                          0, 0,
                          0, 0,
                          self.imageWidth, self.imageHeight )

        OpenVG.ImageMode( self.imageMode )

        paint2Index = 1
        OpenVG.CreatePaint( paint2Index )
        OpenVG.RadialGradientPaint( paint2Index, [ 0, 0 ], [ cx, cy ], max( width, height ) )
        OpenVG.ColorRamps( paint2Index,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )

        OpenVG.SetPaint( paint2Index, [ 'VG_FILL_PATH' ] )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        OpenVG.ImageQuality( self.quality )        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )        
        OpenVG.DrawImage( imageIndex )
        
        target.swapBuffers( state )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class TextAtlasBenchmark( Benchmark ):  
    def __init__( self, width, height, gridx, gridy, overdraw, format, quality, reuse ):
        Benchmark.__init__( self )
        self.atlasWidth  = width
        self.atlasHeight = height
        self.gridx       = gridx
        self.gridy       = gridy
        self.overdraw    = overdraw
        self.format      = format
        self.quality     = quality
        self.reuse       = reuse
        self.name = "OPENVG text atlas %dx%d grd=%dx%d ovrdrw=%d frmat=%s qlty=%s reuse=%s" % ( self.atlasWidth,
                                                                                                self.atlasHeight,
                                                                                                self.gridx,
                                                                                                self.gridy,
                                                                                                self.overdraw,
                                                                                                imageFormatToStr( self.format ),
                                                                                                imageQualityToStr( self.quality ),
                                                                                                boolToStr( self.reuse ), )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
                                                                                    
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageDataIndex = 0
        OpenVG.CreateGradientImageData( imageDataIndex,
                                        'OPENVG_GRADIENT_DIAGONAL',
                                        [ 0, 0, 0, 1 ],
                                        [ 1, 1, 1, 1 ],
                                        self.atlasWidth, self.atlasHeight,
                                        self.format )
        
        imageIndex = 0
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.atlasWidth, self.atlasHeight,
                            [ self.quality ] )

        OpenVG.ImageSubData( imageDataIndex,
                             imageIndex,
                             0, 0,
                             self.atlasWidth, self.atlasHeight )

        OpenVG.ImageQuality( self.quality )

        childImages     = []
        childImageIndex = 1
        w = self.atlasWidth / self.gridx
        h = self.atlasHeight / self.gridy;       
        if self.reuse:
            for y in range( self.gridy ):
                for x in range( self.gridx ):
                    OpenVG.ChildImage( childImageIndex,
                                       imageIndex,
                                       w * x, h * y,
                                       w, h )
                    childImages.append( childImageIndex )
                    childImageIndex += 1
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if not self.reuse:
            for y in range( self.gridy ):
                for x in range( self.gridx ):
                    OpenVG.ChildImage( childImageIndex,
                                        imageIndex,
                                        w * x, h * y,
                                        w, h )
                    childImages.append( childImageIndex )
                    childImageIndex += 1

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )

        for j in range( self.overdraw ):
            i = 0
            cw = width / float( self.gridx )
            ch = height / float( self.gridy )
            for y in range( self.gridy ):
                for x in range( self.gridy ):
                    OpenVG.LoadIdentity()
                    OpenVG.Translate( x * cw, y * ch )
                    OpenVG.Scale( cw / w, ch / h )
                    OpenVG.DrawImage( childImages[ i ] )
                    i += 1
        
        target.swapBuffers( state )

        if not self.reuse:
            for c in childImages:
                OpenVG.DestroyImage( c )
