#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ClearBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )       
        self.name = "OPENVG clear"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()
        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )       
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class SynchronizedClearBenchmark( Benchmark ):  
    def __init__( self ):
        Benchmark.__init__( self )       
        self.name = "OPENVG synchronized clear"

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()
        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.CheckError( '' )
        target.swapInterval( state, 1 )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )       
        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ColorPaintBenchmark( Benchmark ):  
    def __init__( self, overdraw, rotatePaint ):
        Benchmark.__init__( self )
        self.rotatePaint = rotatePaint
        self.overdraw    = overdraw        
        self.name = "OPENVG fullscreen color paint ovrdrw=%d rot.pnt=%d" % ( self.overdraw,
                                                                             self.rotatePaint, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()
        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * h,
                        scale * w,      scale * h,
                        scale * w,      0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ColorPaintMosaicBenchmark( Benchmark ):  
    def __init__( self, divx, divy, overdraw, rotatePaint ):
        Benchmark.__init__( self )
        self.rotatePaint = rotatePaint
        self.divx        = divx
        self.divy        = divy
        self.overdraw    = overdraw
        self.name = "OPENVG fullscreen color paint mosaic=%dx%d ovrdrw=%d rot.pnt=%d" % ( self.divx,
                                                                                          self.divy,
                                                                                          self.overdraw,
                                                                                          self.rotatePaint, )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        scale         = 65535.0
        paintIndex    = 0      
        pathDataIndex = 0
        pathIndex     = 0

        w = scale * width / float( self.divx )
        h = scale * height / float( self.divy )

        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        for hi in range( self.divy ):
            x1 = 0
            y1 = hi * h
            for wi in range( self.divx ):
                OpenVG.Lines( pathDataIndex,
                              [ 0,      h,
                                w,      0,
                                0,      -h,
                                -w,     0 ],
                              'ON' )
                OpenVG.MoveTo( pathDataIndex, [ w, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ -w * self.divx, h ], 'ON' )
        
        OpenVG.ClosePath( pathDataIndex )
        
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           6,
                           10,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )
        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):        
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
                
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class ColorPaintDiscreteBenchmark( Benchmark ):  
    def __init__( self, divx, divy, overdraw, rotatePaint ):
        Benchmark.__init__( self )
        self.rotatePaint = rotatePaint
        self.divx        = divx
        self.divy        = divy
        self.overdraw    = overdraw
        self.name = "OPENVG fullscreen color paint discrete=%dx%d ovrdrw=%d rot.pnt=%d" % ( self.divx,
                                                                                            self.divy,
                                                                                            self.overdraw,
                                                                                            self.rotatePaint, )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        scale         = 65535.0
        paintIndex    = 0      
        pathDataIndex = 0

        w = scale * width / float( self.divx )
        h = scale * height / float( self.divy )

        paths = []
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        for hi in range( self.divy ):
            x1 = 0
            y1 = hi * h
            for wi in range( self.divx ):
                OpenVG.ClearPathData( pathDataIndex )
                OpenVG.MoveTo( pathDataIndex, [ x1, y1 ], 'OFF' )
                OpenVG.Lines( pathDataIndex,
                              [ 0,      h,
                                w,      0,
                                0,      -h,
                                -w,     0 ],
                              'ON' )
                OpenVG.ClosePath( pathDataIndex )
                x1 += w
                
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex,
                                   'VG_PATH_DATATYPE_S_32',
                                   1.0 / scale,
                                   0,
                                   6,
                                   10,
                                   [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                OpenVG.AppendPathData( pathIndex, pathDataIndex )
                paths.append( pathIndex )
        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            for p in paths:
                OpenVG.DrawPath( p, [ 'VG_FILL_PATH' ] )
                
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class LinearGradientPaintBenchmark( Benchmark ):  
    def __init__( self, overdraw, rotatePaint ):
        Benchmark.__init__( self )
        self.rotatePaint = rotatePaint
        self.overdraw    = overdraw
        self.name = "OPENVG fullscreen linear gradient paint ovrdrw=%d rot.pnt=%d" % ( self.overdraw,
                                                                                       self.rotatePaint, )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * h,
                        scale * w,      scale * h,
                        scale * w,      0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class LinearGradientPaintMosaicBenchmark( Benchmark ):  
    def __init__( self, divx, divy, overdraw, rotatePaint ):
        Benchmark.__init__( self )
        self.divx        = divx
        self.divy        = divy
        self.overdraw    = overdraw
        self.rotatePaint = rotatePaint
        self.name = "OPENVG fullscreen linear gradient paint mosaic=%dx%d ovrdrw=%d rot.pnt=%d" % ( self.divx,
                                                                                                    self.divy,
                                                                                                    self.overdraw,
                                                                                                    self.rotatePaint, )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        scale         = 65535.0
        paintIndex    = 0      
        pathDataIndex = 0
        pathIndex     = 0

        w = scale * width / float( self.divx )
        h = scale * height / float( self.divy )

        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        for hi in range( self.divy ):
            x1 = 0
            y1 = hi * h
            for wi in range( self.divx ):
                OpenVG.Lines( pathDataIndex,
                              [ 0,      h,
                                w,      0,
                                0,      -h,
                                -w,     0 ],
                              'ON' )
                OpenVG.MoveTo( pathDataIndex, [ w, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ -w * self.divx, h ], 'ON' )
        
        OpenVG.ClosePath( pathDataIndex )
        
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           6,
                           10,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )
        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 10, 10 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):        
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        target.swapBuffers( state )

        
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class LinearGradientPaintDiscreteBenchmark( Benchmark ):  
    def __init__( self, divx, divy, overdraw, rotatePaint ):
        Benchmark.__init__( self )
        self.divx        = divx
        self.divy        = divy
        self.overdraw    = overdraw
        self.rotatePaint = rotatePaint
        self.name = "OPENVG fullscreen linear gradient paint discrete=%dx%d ovrdrw=%d rot.pnt=%d" % ( self.divx,
                                                                                                      self.divy,
                                                                                                      self.overdraw,
                                                                                                      self.rotatePaint, )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        scale         = 65535.0
        paintIndex    = 0      
        pathDataIndex = 0
        pathIndex     = 0

        w = scale * width / float( self.divx )
        h = scale * height / float( self.divy )

        paths = []
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        for hi in range( self.divy ):
            x1 = 0
            y1 = hi * h
            for wi in range( self.divx ):
                OpenVG.ClearPathData( pathDataIndex )
                OpenVG.MoveTo( pathDataIndex, [ x1, y1 ], 'OFF' )
                OpenVG.Lines( pathDataIndex,
                              [ 0,      h,
                                w,      0,
                                0,      -h,
                                -w,     0 ],
                              'ON' )
                OpenVG.ClosePath( pathDataIndex )
                x1 += w
                
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex,
                                   'VG_PATH_DATATYPE_S_32',
                                   1.0 / scale,
                                   0,
                                   6,
                                   10,
                                   [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                OpenVG.AppendPathData( pathIndex, pathDataIndex )
                paths.append( pathIndex )
        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 10, 10 ], [ 30, 30 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            for p in paths:
                OpenVG.DrawPath( p, [ 'VG_FILL_PATH' ] )

        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RadialGradientPaintBenchmark( Benchmark ):  
    def __init__( self, overdraw, rotatePaint ):
        Benchmark.__init__( self )
        self.rotatePaint = rotatePaint
        self.overdraw    = overdraw
        self.name = "OPENVG fullscreen radial gradient paint ovrdrw=%d rot.pnt=%s" % ( self.overdraw,
                                                                                       self.rotatePaint, )
        
    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.RadialGradientPaint( paintIndex, [ 20, 20 ], [ 50, 50 ], 30 )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * h,
                        scale * w,      scale * h,
                        scale * w,      0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RadialGradientPaintMosaicBenchmark( Benchmark ):  
    def __init__( self, divx, divy, overdraw, rotatePaint ):
        Benchmark.__init__( self )
        self.divx        = divx
        self.divy        = divy
        self.overdraw    = overdraw
        self.rotatePaint = rotatePaint
        self.name = "OPENVG fullscreen radial gradient paint mosaic=%dx%d ovrdrw=%d rot.pnt=%d" % ( self.divx,
                                                                                                    self.divy,
                                                                                                    self.overdraw,
                                                                                                    self.rotatePaint, )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        scale         = 65535.0
        paintIndex    = 0      
        pathDataIndex = 0
        pathIndex     = 0

        w = scale * width / float( self.divx )
        h = scale * height / float( self.divy )

        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        for hi in range( self.divy ):
            x1 = 0
            y1 = hi * h
            for wi in range( self.divx ):
                OpenVG.Lines( pathDataIndex,
                      [ 0,      h,
                        w,      0,
                        0,      -h,
                        -w,     0 ],
                      'ON' )
                OpenVG.MoveTo( pathDataIndex, [ w, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ -w * self.divx, h ], 'ON' )
        
        OpenVG.ClosePath( pathDataIndex )
        
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           6,
                           10,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )
        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.RadialGradientPaint( paintIndex, [ 20, 20 ], [ 50, 50 ], 30 )        
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):        
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
                
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class RadialGradientPaintDiscreteBenchmark( Benchmark ):  
    def __init__( self, divx, divy, overdraw, rotatePaint ):
        Benchmark.__init__( self )
        self.divx        = divx
        self.divy        = divy
        self.overdraw    = overdraw
        self.rotatePaint = rotatePaint
        self.name = "OPENVG fullscreen radial gradient paint discrete=%dx%d ovrdrw=%d pnt.rot=%d" % ( self.divx,
                                                                                                      self.divy,
                                                                                                      self.overdraw,
                                                                                                      self.rotatePaint, )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        scale         = 65535.0
        paintIndex    = 0      
        pathDataIndex = 0
        pathIndex     = 0

        w = scale * width / float( self.divx )
        h = scale * height / float( self.divy )

        paths = []
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        for hi in range( self.divy ):
            x1 = 0
            y1 = hi * h
            for wi in range( self.divx ):
                OpenVG.ClearPathData( pathDataIndex )
                OpenVG.MoveTo( pathDataIndex, [ x1, y1 ], 'OFF' )
                OpenVG.Lines( pathDataIndex,
                              [ 0,      h,
                                w,      0,
                                0,      -h,
                                -w,     0 ],
                              'ON' )
                OpenVG.ClosePath( pathDataIndex )
                x1 += w
                
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex,
                                   'VG_PATH_DATATYPE_S_32',
                                   1.0 / scale,
                                   0,
                                   6,
                                   10,
                                   [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                OpenVG.AppendPathData( pathIndex, pathDataIndex )
                paths.append( pathIndex )
        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.RadialGradientPaint( paintIndex, [ 20, 20 ], [ 50, 50 ], 30 )        
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            for p in paths:
                OpenVG.DrawPath( p, [ 'VG_FILL_PATH' ] )
                
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PatternPaintBenchmark( Benchmark ):  
    def __init__( self, overdraw, format, quality, rotatePaint ):
        Benchmark.__init__( self )
        self.rotatePaint = rotatePaint
        self.overdraw    = overdraw
        self.format      = format
        self.quality     = quality
        self.name = "OPENVG fullscreen pattern paint ovrdrw=%d frmat=%s qlty=%s pnt.rot=%d" % ( self.overdraw,
                                                                                                imageFormatToStr( self.format ),
                                                                                                imageQualityToStr( self.quality ),
                                                                                                self.rotatePaint, )

    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( w, h, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 0.3, 0.3, 0.3, 0.5 ] )

        imageIndex  = 0
        imageWidth  = int( w / 4 )
        imageHeight = int( h / 4 )
        OpenVG.CreateImage( imageIndex, self.format, imageWidth, imageHeight, [ self.quality ] )

        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.RadialGradientPaint( paintIndex, [ 0, 0 ], [ 50, 50 ], 30 )       
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )

        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * h,
                        scale * w,      scale * h,
                        scale * w,      0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.Clear( 0, 0, w, h )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        OpenVG.GetPixels( imageIndex,
                          0, 0,
                          0, 0,
                          imageWidth, imageHeight )
        
        OpenVG.PatternPaint( paintIndex, imageIndex, 'VG_TILE_REPEAT' )     

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ImageQuality( self.quality )
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, w, h )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PatternPaintMosaicBenchmark( Benchmark ):  
    def __init__( self, divx, divy, overdraw, format, quality, rotatePaint ):
        Benchmark.__init__( self )
        self.divx        = divx
        self.divy        = divy
        self.overdraw    = overdraw
        self.format      = format
        self.quality     = quality
        self.rotatePaint = rotatePaint
        self.name = "OPENVG fullscreen pattern paint mosaic=%dx%d ovrdrw=%d frmat=%s qlty=%s pnt.rot=%d" % ( self.divx,
                                                                                                             self.divy,
                                                                                                             self.overdraw,
                                                                                                             imageFormatToStr( self.format ),
                                                                                                             imageQualityToStr( self.quality ),
                                                                                                             self.rotatePaint, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 0.3, 0.3, 0.3, 0.5 ] )

        scale         = 65535.0
        paintIndex    = 0      
        pathDataIndex = 0
        pathIndex     = 0

        OpenVG.CreatePaint( paintIndex )
        OpenVG.RadialGradientPaint( paintIndex, [ 0, 0 ], [ 50, 50 ], 30 )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )

        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        w = scale * width / float( self.divx )
        h = scale * height / float( self.divy )

        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        for hi in range( self.divy ):
            x1 = 0
            y1 = hi * h
            for wi in range( self.divx ):
                OpenVG.Lines( pathDataIndex,
                      [ 0,      h,
                        w,      0,
                        0,      -h,
                        -w,     0 ],
                      'ON' )
                OpenVG.MoveTo( pathDataIndex, [ w, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ -w * self.divx, h ], 'ON' )
        
        OpenVG.ClosePath( pathDataIndex )
        
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           6,
                           10,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        path1DataIndex = 1
        OpenVG.CreatePathData( path1DataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )        
        OpenVG.Lines( path1DataIndex,
                      [ 0,              scale * height,
                        scale * width,  scale * height,
                        scale * width,  0,
                        0,              0 ],
                      'OFF' )
        
        OpenVG.ClosePath( path1DataIndex )

        path1Index = 1
        OpenVG.CreatePath( path1Index,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( path1Index, path1DataIndex )

        OpenVG.Clear( 0, 0, width, height )
        OpenVG.DrawPath( path1Index, [ 'VG_FILL_PATH' ] )

        imageIndex = 0
        imageWidth  = int( width / 4 )
        imageHeight = int( height / 4 )
        OpenVG.CreateImage( imageIndex, self.format, imageWidth, imageHeight, [ self.quality ] )

        OpenVG.GetPixels( imageIndex,
                          0, 0,
                          0, 0,
                          imageWidth, imageHeight )
        
        OpenVG.PatternPaint( paintIndex, imageIndex, 'VG_TILE_REPEAT' )     

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ImageQuality( self.quality )
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):        
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
                
        target.swapBuffers( state )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class PatternPaintDiscreteBenchmark( Benchmark ):  
    def __init__( self, divx, divy, overdraw, format, quality, rotatePaint ):
        Benchmark.__init__( self )
        self.divx        = divx
        self.divy        = divy
        self.overdraw    = overdraw
        self.format      = format
        self.quality     = quality
        self.rotatePaint = rotatePaint
        self.name = "OPENVG fullscreen pattern paint discrete=%dx%d ovrdrw=%d frmat=%s qlty=%s pnt.rot=%d" % ( self.divx,
                                                                                                               self.divy,
                                                                                                               self.overdraw,
                                                                                                               imageFormatToStr( self.format ),
                                                                                                               imageQualityToStr( self.quality ),
                                                                                                               self.rotatePaint, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 0.3, 0.3, 0.3, 0.5 ] )

        scale         = 65535.0
        paintIndex    = 0      
        pathDataIndex = 0

        OpenVG.CreatePaint( paintIndex )
        OpenVG.RadialGradientPaint( paintIndex, [ 0, 0 ], [ 50, 50 ], 30 )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )

        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        w = scale * width / float( self.divx )
        h = scale * height / float( self.divy )

        paths = []
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        for hi in range( self.divy ):
            x1 = 0
            y1 = hi * h
            for wi in range( self.divx ):
                OpenVG.ClearPathData( pathDataIndex )
                OpenVG.MoveTo( pathDataIndex, [ x1, y1 ], 'OFF' )
                OpenVG.Lines( pathDataIndex,
                              [ 0,      h,
                                w,      0,
                                0,      -h,
                                -w,     0 ],
                              'ON' )
                OpenVG.ClosePath( pathDataIndex )
                x1 += w
                
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex,
                                   'VG_PATH_DATATYPE_S_32',
                                   1.0 / scale,
                                   0,
                                   6,
                                   10,
                                   [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                OpenVG.AppendPathData( pathIndex, pathDataIndex )
                paths.append( pathIndex )

        OpenVG.ClearPathData( pathDataIndex )        
        OpenVG.Lines( pathDataIndex,
                      [ 0,              scale * height,
                        scale * width,  scale * height,
                        scale * width,  0,
                        0,              0 ],
                      'OFF' )
        OpenVG.ClosePath( pathDataIndex )

        path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
        OpenVG.CreatePath( path1Index,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( path1Index, pathDataIndex )

        OpenVG.Clear( 0, 0, width, height )
        OpenVG.DrawPath( path1Index, [ 'VG_FILL_PATH' ] )

        imageIndex = 0
        imageWidth  = int( width / 4 )
        imageHeight = int( height / 4 )
        OpenVG.CreateImage( imageIndex, self.format, imageWidth, imageHeight, [ self.quality ] )

        OpenVG.GetPixels( imageIndex,
                          0, 0,
                          0, 0,
                          imageWidth, imageHeight )
        
        OpenVG.PatternPaint( paintIndex, imageIndex, 'VG_TILE_REPEAT' )     

        if self.rotatePaint:
            OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )
            OpenVG.LoadIdentity()
            OpenVG.Rotate( self.rotatePaint )
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenVG.ImageQuality( self.quality )
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            for p in paths:
                OpenVG.DrawPath( p, [ 'VG_FILL_PATH' ] )
                
        target.swapBuffers( state )
        
