#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import  Benchmark
from lib.common    import  *
from common        import  *

######################################################################
CONST  = 1
CREATE = 2
CLEAR  = 3
MODIFY = 4

REUSES = { CONST  : 'CONST',
           CREATE : 'CREATE',
           CLEAR  : 'CLEAR',
           MODIFY : 'MODIFY' }


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class LinesReuseBenchmark( Benchmark ):  
    def __init__( self, lines, strokeWidth, strokeCapStyle, reuse ):
        Benchmark.__init__( self )       
        self.lines          = lines
        self.strokeWidth    = strokeWidth
        self.strokeCapStyle = strokeCapStyle
        self.reuse          = reuse       
        self.name = "OPENVG reuse %s %d lines strkwdth=%d strkcpstle=%s" % ( REUSES[ reuse ],
                                                                             self.lines,
                                                                             self.strokeWidth,
                                                                             strokeCapStyleToStr( self.strokeCapStyle ), )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        strokePaintIndex = 0
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( self.strokeWidth )
        OpenVG.StrokeCapStyle( self.strokeCapStyle )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale   = 65535.0
        xoffset = 20
        yoffset = height / ( self.lines + 1 )
        y       = yoffset

        pathCoordinates = []
        for i in range( self.lines ):
            OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )            
            OpenVG.Lines( pathDataIndex,
                          [ scale * ( width - xoffset ), y * scale ],
                          'OFF' )
            OpenVG.ClosePath( pathDataIndex )
            y += yoffset
       
        pathIndex = 0

        if self.reuse != CREATE:
            OpenVG.CreatePath( pathIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1.0 / scale,
                               0,
                               2,
                               4,
                               [ 'VG_PATH_CAPABILITY_ALL' ] )

            OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, width, height )

        if self.reuse == CREATE:
            OpenVG.CreatePath( pathIndex,
                               'VG_PATH_DATATYPE_S_32',
                               1.0 / scale,
                               0,
                               2,
                               4,
                               [ 'VG_PATH_CAPABILITY_ALL' ] )
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.reuse == CLEAR:
            OpenVG.ClearPath( pathIndex,
                              [ 'VG_PATH_CAPABILITY_ALL' ] )
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

        if self.reuse == MODIFY:
            OpenVG.ModifyPathCoords( pathIndex,
                                     pathDataIndex,
                                     0, 0 )
            
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
        
        target.swapBuffers( state )

        if self.reuse == CREATE:        
            OpenVG.DestroyPath( pathIndex )

            
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ListboxReuseBenchmark( Benchmark ):  
    def __init__( self, listElements, reuse ):
        Benchmark.__init__( self )       
        self.listElements = listElements
        self.reuse        = reuse
        self.name = "OPENVG reuse %s %d list box elements" % ( REUSES[ reuse ],
                                                               self.listElements, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )

        listElementHeight = height / self.listElements
        
        paintIndex = 0
        OpenVG.CreatePaint( paintIndex )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = 0
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_F', 1, 1 )

        OpenVG.Lines( pathDataIndex,
                      [ 0,              listElementHeight,
                        width,          listElementHeight,
                        width,          0,
                        0,              0 ],
                      'OFF' )
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = 0
        
        if self.reuse != CREATE:
            OpenVG.CreatePath( pathIndex,
                               'VG_PATH_DATATYPE_F',
                               1.0,
                               0,
                               5,
                               8,
                               [ 'VG_PATH_CAPABILITY_ALL' ] )

            OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )       

        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )
        
        OpenVG.CheckError( '' )
            
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        for i in range( self.listElements ):
            r = i / float( self.listElements )
            
            OpenVG.ColorPaint( paintIndex, [ 0.7 * r, 1.0, 0.7 * r, 1.0 ] )
            y = listElementHeight * i

            OpenVG.LoadIdentity()

            if self.reuse != CONST:
                y = y / 2.0
            
            OpenVG.LoadMatrix( [ 1.0,  0.0, 0.0,
                                 0.0,  1.0, 0.0,
                                 0.0,    y, 1.0 ] )

            if self.reuse == CREATE:
                OpenVG.CreatePath( pathIndex,
                                   'VG_PATH_DATATYPE_F',
                                   1.0,
                                   0,
                                   5,
                                   8,
                                   [ 'VG_PATH_CAPABILITY_ALL' ] )
               
            if self.reuse != CONST:
                OpenVG.ClearPathData( pathDataIndex )
                OpenVG.Lines( pathDataIndex,
                              [ 0,              y + listElementHeight,
                                width,          y + listElementHeight,
                                width,          y,
                                0,              y ],
                              'OFF' )
                OpenVG.ClosePath( pathDataIndex )

                if self.reuse == CREATE:
                    OpenVG.AppendPathData( pathIndex, pathDataIndex )
                if self.reuse == MODIFY:
                    OpenVG.ModifyPathCoords( pathIndex, pathDataIndex, 0, 0 )
                elif self.reuse == CLEAR:
                    OpenVG.ClearPath( pathIndex, [ 'VG_PATH_CAPABILITY_ALL' ] )
                    OpenVG.AppendPathData( pathIndex, pathDataIndex )
                
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

            if self.reuse == CREATE:
                OpenVG.DestroyPath( pathIndex )
            
        target.swapBuffers( state )
        
