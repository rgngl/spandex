#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from common        import *

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class FillBenchmark( Benchmark ):  
    def __init__( self, overdraw, samples ):
        Benchmark.__init__( self )
        self.overdraw = overdraw
        self.samples  = samples
        self.name = "OPENVG surface antialising fill ovdrv=%d smples=%d" % ( self.overdraw,
                                                                             self.samples, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]       

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = 0
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )
                      
        attrs = target.getDefaultAttributes( API_OPENVG )

        sampleBuffers = 0
        if self.samples > 1:
            sampleBuffers = 1
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       sampleBuffers,
                       self.samples,
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )       
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )

        contextIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        scale = 65535.0
        OpenVG.Lines( pathDataIndex,
                      [ 0,               scale * height,
                        scale * width,   scale * height,
                        scale * width,   0,
                        0,               0 ],
                      'OFF' )
        
        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           5,
                           8,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        if self.samples <= 1:
            OpenVG.RenderingQuality( 'VG_RENDERING_QUALITY_NONANTIALIASED' )
        else:
            OpenVG.RenderingQuality( 'VG_RENDERING_QUALITY_BETTER' )
               
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
       
        OpenVG.Clear( 0, 0, width, height )

        for i in range( self.overdraw ):
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class TigerBenchmark( Benchmark ):
    def __init__( self, samples ):
        Benchmark.__init__( self )
        self.samples  = samples
        self.name = "OPENVG surface antialising tiger smples=%d" % ( self.samples, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]       

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = 0
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENVG )

        sampleBuffers = 0
        if self.samples > 1:
            sampleBuffers = 1
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       sampleBuffers,
                       self.samples,
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           contextIndex,
                           configIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         surfaceIndex,
                         surfaceIndex,
                         contextIndex )

        Egl.SwapInterval( displayIndex, 0 )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        if self.samples <= 1:
            OpenVG.RenderingQuality( 'VG_RENDERING_QUALITY_NONANTIALIASED' )
        else:
            OpenVG.RenderingQuality( 'VG_RENDERING_QUALITY_BETTER' )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
       
        OpenVG.Clear( 0, 0, width, height )

        OpenVG.DrawTiger( tigerIndex, 1, width, height )

        Egl.SwapBuffers( displayIndex, surfaceIndex )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class ImageBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality, samples ):
        Benchmark.__init__( self )
        self.imageWidth     = width
        self.imageHeight    = height
        self.format         = format
        self.quality        = quality
        self.samples        = samples
        self.name = "OPENVG surface antialising image %dx%d frmt=%s qlty=%s smples=%d" % ( self.imageWidth,
                                                                                           self.imageHeight,
                                                                                           imageFormatToStr( self.format ),
                                                                                           imageQualityToStr( self.quality ),
                                                                                           self.samples, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Egl    = modules[ 'Egl' ]
        
        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        attrs = target.getDefaultAttributes( API_OPENVG )

        sampleBuffers = 0
        if self.samples > 1:
            sampleBuffers = 1
        
        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )        
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       attrs[ CONFIG_BUFFERSIZE ],
                       attrs[ CONFIG_REDSIZE ],
                       attrs[ CONFIG_GREENSIZE ],
                       attrs[ CONFIG_BLUESIZE ],
                       attrs[ CONFIG_ALPHASIZE ],
                       '-',
                       '-',
                       sampleBuffers,
                       self.samples,
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT'])

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex,
                           vgContextIndex,
                           vgConfigIndex,
                           -1,
                           -1 )

        Egl.MakeCurrent( displayIndex,
                         vgSurfaceIndex,
                         vgSurfaceIndex,
                         vgContextIndex )

        Egl.SwapInterval( displayIndex, 0 )
       
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.imageWidth, self.imageHeight,
                            [ self.quality ] )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.ClearImage( imageIndex, 0, 0, self.imageWidth, self.imageHeight );

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

        OpenVG.ImageQuality( self.quality )
        
        if self.samples <= 1:
            OpenVG.RenderingQuality( 'VG_RENDERING_QUALITY_NONANTIALIASED' )
        else:
            OpenVG.RenderingQuality( 'VG_RENDERING_QUALITY_BETTER' )
        
        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.DrawImage( imageIndex )

        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )

        
