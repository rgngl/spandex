#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class OpenVGAndOpenGLES1SameSurfaceBenchmark( Benchmark ):  
    def __init__( self, swapInterval, drawOpenVG, drawOpenGLES1 ):
        Benchmark.__init__( self )
        self.swapInterval  = swapInterval
        self.drawOpenVG    = drawOpenVG
        self.drawOpenGLES1 = drawOpenGLES1
        self.name = "OPENVG-OPENGLES1 same surface swpntrvl=%d openvg=%s opengles1=%s" % ( self.swapInterval,
                                                                                           str( self.drawOpenVG ),
                                                                                           str( self.drawOpenGLES1 ), )
        
    def build( self, target, modules ):
        OpenVG       = modules[ 'OpenVG' ]
        OpenGLES1    = modules[ 'OpenGLES1' ]
        Egl          = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-', 
                       '-',
                       '>=0',
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT',
                         'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT', 'EGL_OPENGL_ES_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )

        # First, setup OpenVG
        Egl.BindApi( 'EGL_OPENVG_API' )     
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, vgContextIndex, configIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, vgContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )

        widthOffset  = ( width / 4 )
        heightOffset = ( height / 4 )
        imageWidth   = width - widthOffset
        imageHeight  = height - heightOffset
        vgImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( vgImageIndex,
                            'VG_sRGBA_8888',
                            imageWidth,
                            imageHeight,
                            [ 'VG_IMAGE_QUALITY_BETTER' ] )

        OpenVG.ClearColor( [ 0.0, 0.0, 1.0, 0.7 ] );
        OpenVG.ClearImage( vgImageIndex,
                           0,
                           0,
                            imageWidth,
                            imageHeight )

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        OpenVG.CheckError( '' )
        
        # Second, setup OpenGLES
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        gles1ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, gles1ContextIndex, configIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, gles1ContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )
        
        # PLANE CREATION
        PLANE_WIDTH                     = 2
        PLANE_HEIGHT                    = 2
        VERTEX_COMPONENTS               = 3
        TEX_COORD_COMPONENTS            = 2
        TEXTURE_WIDTH                   = 128
        TEXTURE_HEIGHT                  = 128
               
        # Create raw vertex data for plane
        texCoordAttribute = []
        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                         TEX_COORD_COMPONENTS,
                                                         [ float( PLANE_WIDTH ),
                                                           float( PLANE_HEIGHT ) ] ) )
        
        plane = MeshStripPlane( PLANE_WIDTH,
                                PLANE_HEIGHT,
                                MeshVertexAttribute( MESH_TYPE_FLOAT, VERTEX_COMPONENTS ),
                                None,
                                None,
                                texCoordAttribute,
                                MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                None,
                                MESH_CCW_WINDING,
                                False )

        plane.translate( [ -0.5, -0.5, 0 ] )
        plane.scale( [ 2.0, 2.0 ] )

        # Create OpenGLES data
        vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( vertexArrayIndex, plane.vertexGLType )
        OpenGLES1.AppendToArray( vertexArrayIndex, plane.vertices )
        OpenGLES1.VertexPointer( vertexArrayIndex, VERTEX_COMPONENTS, 0 )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )

        texCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( texCoordArrayIndex, plane.texCoordsGLTypes[ 0 ] )
        OpenGLES1.AppendToArray( texCoordArrayIndex, plane.texCoords[ 0 ] )
        OpenGLES1.TexCoordPointer( texCoordArrayIndex, TEX_COORD_COMPONENTS, 0 )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        
        indexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( indexArrayIndex, plane.indexGLType );
        OpenGLES1.AppendToArray( indexArrayIndex, plane.indices )

        # Create texture
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )        
        OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.3, 0.3, 0.7 ],
                                            [ 0.3, 1.0, 0.3, 0.7 ],
                                            4, 4,
                                            TEXTURE_WIDTH,
                                            TEXTURE_HEIGHT,
                                            'ON',
                                            'OPENGLES1_RGBA8888' )

        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST_MIPMAP_LINEAR',
                                'GL_LINEAR',
                                'GL_REPEAT',
                                'GL_REPEAT',
                                'OFF' )
        
        # OpenGLES state setup
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        OpenGLES1.Finish()
        OpenGLES1.CheckError( '' )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.drawOpenGLES1:
            Egl.BindApi( 'EGL_OPENGL_ES_API' )
            Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, gles1ContextIndex )       

            OpenGLES1.Viewport( 0, 0, width, height )
            OpenGLES1.DrawElements( plane.glMode, len( plane.indices ), 0, indexArrayIndex )    
            OpenGLES1.Finish()
        
        if self.drawOpenVG:
            Egl.BindApi( 'EGL_OPENVG_API' )
            Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, vgContextIndex )       

            if not self.drawOpenGLES1:
                OpenVG.Clear( 0, 0, 0, 0 )
            
            OpenVG.LoadIdentity()
            OpenVG.DrawImage( vgImageIndex )
            OpenVG.Translate( widthOffset, heightOffset )
            OpenVG.DrawImage( vgImageIndex )
            OpenVG.Finish()
            
        Egl.SwapBuffers( displayIndex, surfaceIndex )
       
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class OpenVGAndOpenGLES1SeparateSurfaceBenchmark( Benchmark ):  
    def __init__( self, swapInterval, drawOpenVG, drawOpenGLES1 ):
        Benchmark.__init__( self )
        self.swapInterval  = swapInterval
        self.drawOpenVG    = drawOpenVG
        self.drawOpenGLES1 = drawOpenGLES1       
        self.name   = "OPENVG-OPENGLES1 separate surface swpntrvl=%d drawOpenVG=%s drawOpenGLES1=%s" % ( self.swapInterval,
                                                                                                         str( self.drawOpenVG ),
                                                                                                         str( self.drawOpenGLES1 ), )

    def build( self, target, modules ):
        OpenVG    = modules[ 'OpenVG' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )      
               
        gles1WindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( gles1WindowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        vgMargin = 32
        vgWidth  = width  - vgMargin * 2
        vgHeight = height - vgMargin * 2
        vgWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( vgWindowIndex, 'SCT_SCREEN_DEFAULT', vgMargin, vgMargin, vgWidth, vgHeight, 'SCT_COLOR_FORMAT_DEFAULT' )
        
        Egl.WindowToFront( vgWindowIndex )
        
        ##########
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-', 
                       '-',
                       '>=0',
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT',
                         'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 vgWindowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )
      
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, vgContextIndex, vgConfigIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )

        widthOffset  = ( vgWidth / 4 )
        heightOffset = ( vgHeight / 4 )
        imageWidth   = vgWidth - widthOffset
        imageHeight  = vgHeight - heightOffset

        vgImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( vgImageIndex,
                            'VG_sRGBA_8888',
                            imageWidth,
                            imageHeight,
                            [ 'VG_IMAGE_QUALITY_BETTER' ] )

        OpenVG.ClearColor( [ 0.3, 0.3, 1.0, 0.7 ] );
        OpenVG.ClearImage( vgImageIndex,
                           0,
                           0,
                            imageWidth,
                            imageHeight )

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 0.7 ] );
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.CheckError( '' )
        
        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )
        
        ##########
        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        gles1ConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       gles1ConfigIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-', 
                       '-',
                       '>=0',
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT' ] )

        gles1SurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 gles1SurfaceIndex,
                                 gles1ConfigIndex,
                                 gles1WindowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
        
        gles1ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, gles1ContextIndex, gles1ConfigIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, gles1SurfaceIndex, gles1SurfaceIndex, gles1ContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )        
       
        # PLANE CREATION
        PLANE_WIDTH                     = 2
        PLANE_HEIGHT                    = 2
        VERTEX_COMPONENTS               = 3
        TEX_COORD_COMPONENTS            = 2
        TEXTURE_WIDTH                   = 128
        TEXTURE_HEIGHT                  = 128
               
        # Create raw vertex data for plane
        texCoordAttribute = []
        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                         TEX_COORD_COMPONENTS,
                                                         [ float( PLANE_WIDTH ),
                                                           float( PLANE_HEIGHT ) ] ) )
        
        plane = MeshStripPlane( PLANE_WIDTH,
                                PLANE_HEIGHT,
                                MeshVertexAttribute( MESH_TYPE_FLOAT, VERTEX_COMPONENTS ),
                                None,
                                None,
                                texCoordAttribute,
                                MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                None,
                                MESH_CCW_WINDING,
                                False )

        plane.translate( [ -0.5, -0.5, 0 ] )
        plane.scale( [ 2.0, 2.0 ] )

        # Create OpenGLES1 data
        vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( vertexArrayIndex, plane.vertexGLType )
        OpenGLES1.AppendToArray( vertexArrayIndex, plane.vertices )
        OpenGLES1.VertexPointer( vertexArrayIndex, VERTEX_COMPONENTS, 0 )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )

        texCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( texCoordArrayIndex, plane.texCoordsGLTypes[ 0 ] )
        OpenGLES1.AppendToArray( texCoordArrayIndex, plane.texCoords[ 0 ] )
        OpenGLES1.TexCoordPointer( texCoordArrayIndex, TEX_COORD_COMPONENTS, 0 )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        
        indexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( indexArrayIndex, plane.indexGLType );
        OpenGLES1.AppendToArray( indexArrayIndex, plane.indices )

        # Create texture
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.3, 0.3, 1.0 ],
                                            [ 0.3, 1.0, 0.3, 1.0 ],
                                            4, 4,
                                            TEXTURE_WIDTH,
                                            TEXTURE_HEIGHT,
                                            'ON',
                                            'OPENGLES1_RGBA8888' )

        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST_MIPMAP_LINEAR',
                                'GL_LINEAR',
                                'GL_REPEAT',
                                'GL_REPEAT',
                                'OFF' )
        
        # OpenGLES state setup
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 0.7 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        OpenGLES1.CheckError( '' )
        
        Egl.SwapBuffers( displayIndex, gles1SurfaceIndex )            
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.drawOpenVG:
            Egl.BindApi( 'EGL_OPENVG_API' )
            Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContextIndex )       

            OpenVG.Clear( 0, 0, 0, 0 )
            OpenVG.LoadIdentity()
            OpenVG.DrawImage( vgImageIndex )
            OpenVG.Translate( widthOffset, heightOffset )
            OpenVG.DrawImage( vgImageIndex )

            Egl.SwapBuffers( displayIndex, vgSurfaceIndex )

        if self.drawOpenGLES1:
            Egl.BindApi( 'EGL_OPENGL_ES_API' )
            Egl.MakeCurrent( displayIndex, gles1SurfaceIndex, gles1SurfaceIndex, gles1ContextIndex )       

            OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
            OpenGLES1.Viewport( 0, 0, width, height )
            OpenGLES1.DrawElements( plane.glMode,
                                 len( plane.indices ),
                                 0,
                                 indexArrayIndex )    

            Egl.SwapBuffers( displayIndex, gles1SurfaceIndex )

