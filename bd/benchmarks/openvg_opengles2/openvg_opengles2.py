#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from lib.egl       import *
from common        import *

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class OpenVGAndOpenGLES2SameSurfaceBenchmark( Benchmark ):  
    def __init__( self, swapInterval, drawOpenVG, drawOpenGLES2 ):
        Benchmark.__init__( self )
        self.swapInterval  = swapInterval
        self.drawOpenVG    = drawOpenVG
        self.drawOpenGLES2 = drawOpenGLES2
        self.name = "OPENVG-OPENGLES2 same surface swpntrvl=%d openvg=%s opengles2=%s" % ( self.swapInterval,
                                                                                           str( self.drawOpenVG ),
                                                                                           str( self.drawOpenGLES2 ), )

    def build( self, target, modules ):
        OpenVG     = modules[ 'OpenVG' ]
        OpenGLES2  = modules[ 'OpenGLES2' ]        
        Egl        = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-', 
                       '-',
                       '>=0',
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT',
                         'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT', 'EGL_OPENGL_ES2_BIT' ] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )

        # First, setup OpenVG
        Egl.BindApi( 'EGL_OPENVG_API' )     
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, vgContextIndex, configIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, vgContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )

        widthOffset  = ( width / 4 )
        heightOffset = ( height / 4 )
        imageWidth   = width - widthOffset
        imageHeight  = height - heightOffset
        vgImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( vgImageIndex,
                            'VG_sRGBA_8888',
                            imageWidth,
                            imageHeight,
                            [ 'VG_IMAGE_QUALITY_BETTER' ] )

        OpenVG.ClearColor( [ 0.0, 0.0, 1.0, 0.7 ] );
        OpenVG.ClearImage( vgImageIndex,
                           0,
                           0,
                            imageWidth,
                            imageHeight )

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        OpenVG.CheckError( '' )

        # Second, setup OpenGLES
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        gles2ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, gles2ContextIndex, configIndex, -1, 2 )
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, gles2ContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )

        OpenGLES2.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )

        # Shader source code
        vertexShaderSource = '"attribute vec4 position_in;attribute vec4 uv_in;uniform mat4 mvp;varying vec4 uv;void main(){gl_Position = mvp * position_in;uv = uv_in;}"'
        fragmentShaderSource = '"precision mediump float;uniform sampler2D tex0;varying vec4 uv;void main(){gl_FragColor = texture2D(tex0, uv.xy);}"'

        # PLANE CREATION
        PLANE_WIDTH                     = 2
        PLANE_HEIGHT                    = 2
        VERTEX_COMPONENTS               = 3
        TEX_COORD_COMPONENTS            = 2
        TEXTURE_WIDTH                   = 128
        TEXTURE_HEIGHT                  = 128
        
        # Create raw vertex data for plane
        texCoordAttribute = []
        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                         TEX_COORD_COMPONENTS,
                                                         [ 1.0,
                                                           1.0 ] ) )
        
        plane = MeshStripPlane( PLANE_WIDTH,
                                PLANE_HEIGHT,
                                MeshVertexAttribute( MESH_TYPE_FLOAT, VERTEX_COMPONENTS ),
                                None,
                                None,
                                texCoordAttribute,
                                MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                None,
                                MESH_CCW_WINDING,
                                False )

        plane.translate( [ -0.5, -0.5, 0 ] )
        plane.scale( [ 2.0, 2.0 ] )

        # Create OpenGL ES 2.0 vertex arrays for plane
        vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( vertexArrayIndex,
                               plane.vertexGLType )
        OpenGLES2.AppendToArray( vertexArrayIndex, plane.vertices )

        texCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( texCoordArrayIndex,
                               plane.texCoordsGLTypes[ 0 ] )
        OpenGLES2.AppendToArray( texCoordArrayIndex, plane.texCoords[ 0 ] )
            
        indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( indexArrayIndex, plane.indexGLType );
        OpenGLES2.AppendToArray( indexArrayIndex, plane.indices )

        # Create shaders
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )
        
        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )
        
        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      vertexLocationIndex,
                                      "position_in" )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      texCoordLocationIndex,
                                      "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        mVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mVPLocationIndex, programIndex, 'mvp' )
            
        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        # Create texture
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.3, 0.3, 1.0 ],
                                            [ 0.3, 1.0, 0.3, 1.0 ],
                                            4, 4,
                                            TEXTURE_WIDTH,
                                            TEXTURE_HEIGHT,
                                            'ON',
                                            'OPENGLES2_RGBA8888' )
        
        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST_MIPMAP_LINEAR',
                                'GL_LINEAR',
                                'GL_REPEAT',
                                'GL_REPEAT' )

        OpenGLES2.UseProgram( programIndex )
            
        OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                       vertexArrayIndex,
                                       VERTEX_COMPONENTS,
                                       'OFF',
                                       0 )
        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        
        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
       
        OpenGLES2.VertexAttribPointer( texCoordLocationIndex,
                                       texCoordArrayIndex,
                                       TEX_COORD_COMPONENTS,
                                       'OFF',
                                       0 )
        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        OpenGLES2.UniformMatrix( mVPLocationIndex,
                                 1,
                                 [ 1.0, 0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0, 1.0 ] )
       
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.drawOpenGLES2:
            Egl.BindApi( 'EGL_OPENGL_ES_API' )
            Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, gles2ContextIndex )       

            OpenGLES2.Viewport( 0, 0, width, height )
            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
            OpenGLES2.DrawElements( plane.glMode,
                                 len( plane.indices ),
                                 0,
                                 indexArrayIndex )    
            OpenGLES2.Finish()
        
        if self.drawOpenVG:
            Egl.BindApi( 'EGL_OPENVG_API' )
            Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, vgContextIndex )       

            if not self.drawOpenGLES2:            
                OpenVG.Clear( 0,0, 0, 0 )
                
            OpenVG.LoadIdentity()
            OpenVG.DrawImage( vgImageIndex )
            OpenVG.Translate( widthOffset, heightOffset )
            OpenVG.DrawImage( vgImageIndex )       
            OpenVG.Finish()
            
        Egl.SwapBuffers( displayIndex, surfaceIndex )


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class OpenVGAndOpenGLES2SeparateSurfaceBenchmark( Benchmark ):  
    def __init__( self, swapInterval, drawOpenVG, drawOpenGLES2 ):
        Benchmark.__init__( self )
        self.swapInterval  = swapInterval
        self.drawOpenVG    = drawOpenVG
        self.drawOpenGLES2 = drawOpenGLES2
        self.name = "OPENVG-OPENGLES2 separate surface swpntrvl=%d openvg=%s opengles2=%s" % ( self.swapInterval,
                                                                                               str( self.drawOpenVG ),
                                                                                               str( self.drawOpenGLES2 ), )

    def build( self, target, modules ):
        OpenVG     = modules[ 'OpenVG' ]
        OpenGLES2  = modules[ 'OpenGLES2' ]        
        Egl        = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        ( width, height, ) = target.getScreenSize()

        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )

        gles2WindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( gles2WindowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        vgMargin = 32
        vgWidth  = width  - vgMargin * 2
        vgHeight = height - vgMargin * 2       
        vgWindowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( vgWindowIndex, 'SCT_SCREEN_DEFAULT', vgMargin, vgMargin, vgWidth, vgHeight, 'SCT_COLOR_FORMAT_DEFAULT' )
        
        Egl.WindowToFront( vgWindowIndex )

        ##########
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-', 
                       '-',
                       '>=0',
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT',
                         'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT' ] )

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 vgWindowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )
      
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, vgContextIndex, vgConfigIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )

        widthOffset  = ( vgWidth / 4 )
        heightOffset = ( vgHeight / 4 )
        imageWidth   = vgWidth - widthOffset
        imageHeight  = vgHeight - heightOffset
        vgImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( vgImageIndex,
                            'VG_sRGBA_8888',
                            imageWidth,
                            imageHeight,
                            [ 'VG_IMAGE_QUALITY_BETTER' ] )

        OpenVG.ClearColor( [ 0.0, 0.0, 1.0, 0.7 ] );
        OpenVG.ClearImage( vgImageIndex,
                           0,
                           0,
                            imageWidth,
                            imageHeight )

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 0.7 ] )
        OpenVG.Clear( 0, 0, 0, 0 )        
        OpenVG.CheckError( '' )

        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )
        
        # Second, setup OpenGLES
        Egl.BindApi( 'EGL_OPENGL_ES_API' )

        gles2ConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       gles2ConfigIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-', 
                       '-',
                       '>=0',
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        gles2SurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 gles2SurfaceIndex,
                                 gles2ConfigIndex,
                                 gles2WindowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )
        
        gles2ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, gles2ContextIndex, gles2ConfigIndex, -1, 2 )
        Egl.MakeCurrent( displayIndex, gles2SurfaceIndex, gles2SurfaceIndex, gles2ContextIndex )
        Egl.SwapInterval( displayIndex, self.swapInterval )        

        # Shader source code
        vertexShaderSource = '"attribute vec4 position_in;attribute vec4 uv_in;uniform mat4 mvp;varying vec4 uv;void main(){gl_Position = mvp * position_in;uv = uv_in;}"'
        fragmentShaderSource = '"precision mediump float;uniform sampler2D tex0;varying vec4 uv;void main(){gl_FragColor = texture2D(tex0, uv.xy);}"'

        # PLANE CREATION
        PLANE_WIDTH                     = 2
        PLANE_HEIGHT                    = 2
        VERTEX_COMPONENTS               = 3
        TEX_COORD_COMPONENTS            = 2
        TEXTURE_WIDTH                   = 128
        TEXTURE_HEIGHT                  = 128
        
        # Create raw vertex data for plane
        texCoordAttribute = []
        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                         TEX_COORD_COMPONENTS,
                                                         [ 1.0,
                                                           1.0 ] ) )
        
        plane = MeshStripPlane( PLANE_WIDTH,
                                PLANE_HEIGHT,
                                MeshVertexAttribute( MESH_TYPE_FLOAT, VERTEX_COMPONENTS ),
                                None,
                                None,
                                texCoordAttribute,
                                MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                None,
                                MESH_CCW_WINDING,
                                False )

        plane.translate( [ -0.5, -0.5, 0 ] )
        plane.scale( [ 2.0, 2.0 ] )

        # Create OpenGL ES 2.0 vertex arrays for plane
        vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( vertexArrayIndex,
                               plane.vertexGLType )
        OpenGLES2.AppendToArray( vertexArrayIndex, plane.vertices )

        texCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( texCoordArrayIndex,
                               plane.texCoordsGLTypes[ 0 ] )
        OpenGLES2.AppendToArray( texCoordArrayIndex, plane.texCoords[ 0 ] )
            
        indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( indexArrayIndex, plane.indexGLType );
        OpenGLES2.AppendToArray( indexArrayIndex, plane.indices )

        # Create shaders
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )
        
        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )
        
        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      vertexLocationIndex,
                                      "position_in" )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      texCoordLocationIndex,
                                      "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        mVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mVPLocationIndex, programIndex, 'mvp' )
            
        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        # Create texture
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.3, 0.3, 1.0 ],
                                            [ 0.3, 1.0, 0.3, 1.0 ],
                                            4, 4,
                                            TEXTURE_WIDTH,
                                            TEXTURE_HEIGHT,
                                            'ON',
                                            'OPENGLES2_RGBA8888' )
        
        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_NEAREST_MIPMAP_LINEAR',
                                'GL_LINEAR',
                                'GL_REPEAT',
                                'GL_REPEAT' )

        OpenGLES2.UseProgram( programIndex )
            
        OpenGLES2.VertexAttribPointer( vertexLocationIndex,
                                       vertexArrayIndex,
                                       VERTEX_COMPONENTS,
                                       'OFF',
                                       0 )
        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        
        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
       
        OpenGLES2.VertexAttribPointer( texCoordLocationIndex,
                                       texCoordArrayIndex,
                                       TEX_COORD_COMPONENTS,
                                       'OFF',
                                       0 )
        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        OpenGLES2.UniformMatrix( mVPLocationIndex,
                                 1,
                                 [ 1.0, 0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )        
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        OpenGLES2.CheckError( '' )
        
        Egl.SwapBuffers( displayIndex, gles2SurfaceIndex )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        if self.drawOpenVG:
            Egl.BindApi( 'EGL_OPENVG_API' )
            Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContextIndex )       

            OpenVG.Clear( 0,0, 0, 0 )
            OpenVG.LoadIdentity()
            OpenVG.DrawImage( vgImageIndex )
            OpenVG.Translate( widthOffset, heightOffset )
            OpenVG.DrawImage( vgImageIndex )

            Egl.SwapBuffers( displayIndex, vgSurfaceIndex )            

        if self.drawOpenGLES2:
            Egl.BindApi( 'EGL_OPENGL_ES_API' )
            Egl.MakeCurrent( displayIndex, gles2SurfaceIndex, gles2SurfaceIndex, gles2ContextIndex )       

            OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
            OpenGLES2.Viewport( 0, 0, width, height )
            OpenGLES2.DrawElements( plane.glMode,
                                    len( plane.indices ),
                                    0,
                                    indexArrayIndex )    

            Egl.SwapBuffers( displayIndex, gles2SurfaceIndex )            
         

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------            
class OpenGLES2ToOpenVGImageBenchmark( Benchmark ):  
    def __init__( self, width, height, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.format      = format
        self.quality     = quality       
        self.name = "OPENGLES2-TO-OPENVG-IMAGE img.sz=%dx%d frmat=%s qlty=%s" % ( self.imageWidth,
                                                                                  self.imageHeight,
                                                                                  imageFormatToStr( self.format ),
                                                                                  imageQualityToStr( self.quality ), )

    def build( self, target, modules ):
        OpenVG    = modules[ 'OpenVG' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        # Shader source code
        vertexShaderSource = '"attribute vec4 position_in;attribute vec4 uv_in;uniform mat4 mvp;varying vec4 uv;void main(){gl_Position = mvp * position_in;uv = uv_in;}"'
        fragmentShaderSource = '"precision mediump float;uniform sampler2D tex0;varying vec4 uv;void main(){gl_FragColor = texture2D(tex0, uv.xy);}"'

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        # First, setup OpenVG
        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )       
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-',
                       '-',
                       '>=0',
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT'])
        
        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, vgContextIndex, vgConfigIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContextIndex )
        Egl.SwapInterval( displayIndex, 0 )
        
        # Create OpenVG image background image
        vgImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( vgImageIndex,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )

        # Create OpenVG images modeling UI icons 
        ICON_XCOUNT = 4
        ICON_YCOUNT = 3
        ICON_WIDTH  = 88
        ICON_HEIGHT = 88

        iconCount = ICON_YCOUNT * ICON_XCOUNT
        vgIconImageIndices = [ -1 ] * iconCount
        vgIconCoordinates  = [ 0 ] * iconCount
        for j in range( ICON_YCOUNT ):
            for i in range( ICON_XCOUNT ):
                index = j * ICON_XCOUNT + i
                vgIconImageIndices[ index ] = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                vgIconCoordinates[ index ] = ( i * ICON_WIDTH, j * ICON_HEIGHT, )                
                OpenVG.CreateImage( vgIconImageIndices[ index ],
                                    'VG_sRGBA_8888_PRE',
                                    ICON_WIDTH,
                                    ICON_HEIGHT,
                                    [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )
                OpenVG.ClearColor( [ float( i + 1 ) / ICON_XCOUNT, float( j + 1 ) / ICON_YCOUNT, 1.0, 1.0 ] )
                OpenVG.ClearImage( vgIconImageIndices[ index ], 0, 0, ICON_WIDTH, ICON_HEIGHT )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        
        formats = { 'VG_sRGBX_8888'      : ( 24, 8, 8, 8, 0, False ),
                    'VG_sRGBA_8888'      : ( 32, 8, 8, 8, 8, False ),
                    'VG_sRGBA_8888_PRE'  : ( 32, 8, 8, 8, 8, True ),
                    'VG_sRGB_565'        : ( 16, 5, 6, 5, 0, False ),
                    'VG_sRGBA_5551'      : ( 16, 5, 5, 5, 1, False ),
                    'VG_sRGBA_4444'      : ( 16, 4, 4, 4, 4, False ) }

        f = formats[ self.format ]

        surfaceType = [ 'EGL_PBUFFER_BIT' ]
        if f[ 5 ]:
            surfaceType += [ 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ]
        
        gles2ConfigIndex  = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       gles2ConfigIndex,
                       f[ 0 ],
                       f[ 1 ],
                       f[ 2 ],
                       f[ 3 ],
                       f[ 4 ],
                       '-',
                       '-',
                       '>=0',
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       surfaceType,
                       [ 'EGL_OPENGL_ES2_BIT' ] )       

        attribs = [ EGL_DEFINES[ 'EGL_COLORSPACE' ],    EGL_DEFINES[ 'EGL_COLORSPACE_sRGB' ] ]

        if f[ 5 ]:
            attribs += [ EGL_DEFINES[ 'EGL_ALPHA_FORMAT' ],  EGL_DEFINES[ 'EGL_ALPHA_FORMAT_PRE' ] ]
        else:
            attribs += [ EGL_DEFINES[ 'EGL_ALPHA_FORMAT' ],  EGL_DEFINES[ 'EGL_ALPHA_FORMAT_NONPRE' ] ]
        
        gles2SurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferFromClientBufferExt( displayIndex,
                                              gles2SurfaceIndex,
                                              gles2ConfigIndex,
                                              'EGL_OPENVG_IMAGE',
                                              vgImageIndex,
                                              attribs )

        # Setup OpenGL ES 2
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        gles2ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, gles2ContextIndex, gles2ConfigIndex, -1, 2 )
        Egl.MakeCurrent( displayIndex, gles2SurfaceIndex, gles2SurfaceIndex, gles2ContextIndex )
        Egl.SwapInterval( displayIndex, 0 )
        
        # PLANE CREATION
        PLANE_WIDTH                     = 2
        PLANE_HEIGHT                    = 2
        VERTEX_COMPONENTS               = 3
        TEX_COORD_COMPONENTS            = 2
        TEXTURE_WIDTH                   = self.imageWidth
        TEXTURE_HEIGHT                  = self.imageHeight
        
        # Create raw vertex data for plane
        texCoordAttribute = []
        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT, TEX_COORD_COMPONENTS, [ 1.0, 1.0 ] ) )
        
        plane = MeshStripPlane( PLANE_WIDTH,
                                PLANE_HEIGHT,
                                MeshVertexAttribute( MESH_TYPE_FLOAT, VERTEX_COMPONENTS ),
                                None,
                                None,
                                texCoordAttribute,
                                MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                None,
                                MESH_CCW_WINDING,
                                False )

        plane.translate( [ -0.5, -0.5, 0 ] )
        plane.scale( [ 2.0, 2.0 ] )

        # Create OpenGL ES 2.0 vertex arrays for plane
        vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( vertexArrayIndex,
                               plane.vertexGLType )
        OpenGLES2.AppendToArray( vertexArrayIndex, plane.vertices )

        texCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( texCoordArrayIndex,
                               plane.texCoordsGLTypes[ 0 ] )
        OpenGLES2.AppendToArray( texCoordArrayIndex, plane.texCoords[ 0 ] )
            
        indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( indexArrayIndex, plane.indexGLType );
        OpenGLES2.AppendToArray( indexArrayIndex, plane.indices )

        # Create OpenGL ES 2.0 vertex buffers from vertex arrays plane
        vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( vertexBufferIndex )
        OpenGLES2.BufferData( vertexBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              vertexArrayIndex,
                              0,
                              0 )

        texCoordBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( texCoordBufferIndex )
        OpenGLES2.BufferData( texCoordBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              texCoordArrayIndex,
                              0,
                              0 )

        indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( indexBufferIndex )
        OpenGLES2.BufferData( indexBufferIndex,
                              'ON',
                              'GL_ELEMENT_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              indexArrayIndex,
                              0,
                              0 )

        # Create shaders
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )
        
        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )
        
        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      vertexLocationIndex,
                                      "position_in" )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      texCoordLocationIndex,
                                      "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        mVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mVPLocationIndex, programIndex, 'mvp' )
            
        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        # Create texture
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                            [ 1.0, 0.3, 0.3, 1.0 ],
                                            [ 0.3, 1.0, 0.3, 1.0 ],
                                            4, 4,
                                            TEXTURE_WIDTH,
                                            TEXTURE_HEIGHT,
                                            'OFF',
                                            'OPENGLES2_RGBA8888' )
        
        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        OpenGLES2.UseProgram( programIndex )
            
        OpenGLES2.BufferVertexAttribPointer( vertexBufferIndex,
                                             'ON',
                                             vertexLocationIndex,
                                             VERTEX_COMPONENTS,
                                             'OFF',
                                             0 )
        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        
        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
       
        OpenGLES2.BufferVertexAttribPointer( texCoordBufferIndex,
                                             'ON',
                                             texCoordLocationIndex,
                                             TEX_COORD_COMPONENTS,
                                             'OFF',
                                             0 )
        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        OpenGLES2.UniformMatrix( mVPLocationIndex,
                                 1,
                                 [ 1.0, 0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0, 1.0 ] )
        
        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        # First, draw OpenGL ES 2 graphics to the pbuffer surface bound to the
        # OpenVG image
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        Egl.MakeCurrent( displayIndex, gles2SurfaceIndex, gles2SurfaceIndex, gles2ContextIndex )       

        OpenGLES2.Viewport( 0, 0, self.imageWidth, self.imageHeight ) 
        OpenGLES2.BufferDrawElements( indexBufferIndex, 'ON', plane.glMode, len( plane.indices ), 0 )    
        OpenGLES2.Finish()
        
        Egl.MakeCurrent( displayIndex, -1, -1, -1 )       
        
        # Second, draw OpenVG
        Egl.BindApi( 'EGL_OPENVG_API' )
        Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContextIndex )       
                
        OpenVG.Clear( 0, 0, 0, 0 );

        # Draw background image (with OpenGL ES 2 content)
        OpenVG.LoadIdentity()        
        OpenVG.DrawImage( vgImageIndex )

        # Draw images emulating UI icons
        for i in range( iconCount ):
            OpenVG.LoadIdentity()
            OpenVG.Translate( vgIconCoordinates[ i ][ 0 ], vgIconCoordinates[ i ][ 1 ] )
            OpenVG.DrawImage( vgIconImageIndices[ i ] )
        
        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )


# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
class OpenGLES2ToTwoOpenVGImagesBenchmark( Benchmark ):          
    def __init__( self, width, height, format, quality ):
        Benchmark.__init__( self )
        self.imageWidth  = width
        self.imageHeight = height
        self.format      = format
        self.quality     = quality       
        self.name = "OPENGLES2-TO-TWO-OPENVG-IMAGES img.sz=%dx%d frmat=%s qlty=%s" % ( self.imageWidth,
                                                                                       self.imageHeight,
                                                                                       imageFormatToStr( self.format ),
                                                                                       imageQualityToStr( self.quality ), )
        
    def build( self, target, modules ):
        OpenVG    = modules[ 'OpenVG' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Egl       = modules[ 'Egl' ]

        indexTracker = IndexTracker()

        # Shader source code
        vertexShaderSource = '"attribute vec4 position_in;attribute vec4 uv_in;uniform mat4 mvp;varying vec4 uv;void main(){gl_Position = mvp * position_in;uv = uv_in;}"'
        fragmentShaderSource = '"precision mediump float;uniform sampler2D tex0;varying vec4 uv;void main(){gl_FragColor = texture2D(tex0, uv.xy);}"'

        ( width, height, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Init actions       
        self.beginInitActions()

        ######################################################################
        # EGL SETUP
        ######################################################################

        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        windowIndex = indexTracker.allocIndex( 'EGL_WINDOW_INDEX' )
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, width, height, 'SCT_COLOR_FORMAT_DEFAULT' )

        # First, setup OpenVG
        vgConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )       
        Egl.RGBConfig( displayIndex,
                       vgConfigIndex,
                       32,
                       8,
                       8,
                       8,
                       8,
                       '-',
                       '-',
                       '>=0',
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT'])

        vgSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreateWindowSurface( displayIndex,
                                 vgSurfaceIndex,
                                 vgConfigIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )
       
        vgContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, vgContextIndex, vgConfigIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContextIndex )
        Egl.SwapInterval( displayIndex, 0 )
        
        # Create OpenVG image to be used for OpenGL ES 2 background image
        # rendering
        vgImage0Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( vgImage0Index,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )

        vgImage1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( vgImage1Index,
                            self.format,
                            self.imageWidth,
                            self.imageHeight,
                            [ self.quality ] )
        
        # Create OpenVG images modelling UI icons 
        ICON_XCOUNT = 4
        ICON_YCOUNT = 3
        ICON_WIDTH  = 88
        ICON_HEIGHT = 88

        iconCount = ICON_YCOUNT * ICON_XCOUNT
        vgIconImageIndices = [ -1 ] * iconCount
        vgIconCoordinates  = [ 0 ] * iconCount
        for j in range( ICON_YCOUNT ):
            for i in range( ICON_XCOUNT ):
                index = j * ICON_XCOUNT + i
                vgIconImageIndices[ index ] = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                vgIconCoordinates[ index ] = ( i * ICON_WIDTH, j * ICON_HEIGHT, )                
                OpenVG.CreateImage( vgIconImageIndices[ index ],
                                    'VG_sRGBA_8888_PRE',
                                    ICON_WIDTH,
                                    ICON_HEIGHT,
                                    [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )
                OpenVG.ClearColor( [ float( i + 1 ) / ICON_XCOUNT, float( j + 1 ) / ICON_YCOUNT, 1.0, 1.0 ] )
                OpenVG.ClearImage( vgIconImageIndices[ index ], 0, 0, ICON_WIDTH, ICON_HEIGHT )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        
        formats = { 'VG_sRGBX_8888'      : ( 24, 8, 8, 8, 0, False ),
                    'VG_sRGBA_8888'      : ( 32, 8, 8, 8, 8, False ),
                    'VG_sRGBA_8888_PRE'  : ( 32, 8, 8, 8, 8, True ),
                    'VG_sRGB_565'        : ( 16, 5, 6, 5, 0, False ),
                    'VG_sRGBA_5551'      : ( 16, 5, 5, 5, 1, False ),
                    'VG_sRGBA_4444'      : ( 16, 4, 4, 4, 4, False ) }

        f = formats[ self.format ]

        surfaceType = [ 'EGL_PBUFFER_BIT' ]
        if f[ 5 ]:
            surfaceType += [ 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ]
        else:
            attribs += [ EGL_DEFINES[ 'EGL_ALPHA_FORMAT' ],  EGL_DEFINES[ 'EGL_ALPHA_FORMAT_NONPRE' ] ]
        
        gles2ConfigIndex  = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       gles2ConfigIndex,
                       f[ 0 ],
                       f[ 1 ],
                       f[ 2 ],
                       f[ 3 ],
                       f[ 4 ],
                       '-',
                       '-',
                       '>=0',
                       '>=0',
                       '-',                       
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       surfaceType,
                       [ 'EGL_OPENGL_ES2_BIT' ] )

        attribs = [ EGL_DEFINES[ 'EGL_COLORSPACE' ],    EGL_DEFINES[ 'EGL_COLORSPACE_sRGB' ] ]

        if f[ 5 ]:
            attribs += [ EGL_DEFINES[ 'EGL_ALPHA_FORMAT' ],  EGL_DEFINES[ 'EGL_ALPHA_FORMAT_PRE' ] ]
        
        gles2Surface0Index = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferFromClientBufferExt( displayIndex,
                                              gles2Surface0Index,
                                              gles2ConfigIndex,
                                              'EGL_OPENVG_IMAGE',
                                              vgImage0Index,
                                              attribs )

        gles2Surface1Index = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferFromClientBufferExt( displayIndex,
                                              gles2Surface1Index,
                                              gles2ConfigIndex,
                                              'EGL_OPENVG_IMAGE',
                                              vgImage1Index,
                                              attribs )

        # Setup OpenGL ES 2
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        
        gles2ContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, gles2ContextIndex, gles2ConfigIndex, -1, 2 )
        Egl.MakeCurrent( displayIndex, gles2Surface0Index, gles2Surface0Index, gles2ContextIndex )

        # PLANE CREATION
        PLANE_WIDTH                     = 2
        PLANE_HEIGHT                    = 2
        VERTEX_COMPONENTS               = 3
        TEX_COORD_COMPONENTS            = 2
        TEXTURE_WIDTH                   = self.imageWidth
        TEXTURE_HEIGHT                  = self.imageHeight
        
        # Create raw vertex data for plane
        texCoordAttribute = []
        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT, TEX_COORD_COMPONENTS, [ 1.0, 1.0 ] ) )
        
        plane = MeshStripPlane( PLANE_WIDTH,
                                PLANE_HEIGHT,
                                MeshVertexAttribute( MESH_TYPE_FLOAT, VERTEX_COMPONENTS ),
                                None,
                                None,
                                texCoordAttribute,
                                MeshAttribute( MESH_TYPE_UNSIGNED_SHORT ),
                                None,
                                MESH_CCW_WINDING,
                                False )

        plane.translate( [ -0.5, -0.5, 0 ] )
        plane.scale( [ 2.0, 2.0 ] )

        # Create OpenGL ES 2.0 vertex arrays for plane
        vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( vertexArrayIndex,
                               plane.vertexGLType )
        OpenGLES2.AppendToArray( vertexArrayIndex, plane.vertices )

        texCoordArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( texCoordArrayIndex,
                               plane.texCoordsGLTypes[ 0 ] )
        OpenGLES2.AppendToArray( texCoordArrayIndex, plane.texCoords[ 0 ] )
            
        indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
        OpenGLES2.CreateArray( indexArrayIndex, plane.indexGLType );
        OpenGLES2.AppendToArray( indexArrayIndex, plane.indices )

        # Create OpenGL ES 2.0 vertex buffers from vertex arrays plane
        vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( vertexBufferIndex )
        OpenGLES2.BufferData( vertexBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              vertexArrayIndex,
                              0,
                              0 )

        texCoordBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( texCoordBufferIndex )
        OpenGLES2.BufferData( texCoordBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              texCoordArrayIndex,
                              0,
                              0 )

        indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
        OpenGLES2.GenBuffer( indexBufferIndex )
        OpenGLES2.BufferData( indexBufferIndex,
                              'ON',
                              'GL_ELEMENT_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              indexArrayIndex,
                              0,
                              0 )

        # Create shaders
        vertexShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( vertexShaderDataIndex, vertexShaderSource )
        
        vertexShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vertexShaderIndex )
        OpenGLES2.ShaderSource( vertexShaderIndex, vertexShaderDataIndex )
        OpenGLES2.CompileShader( vertexShaderIndex )

        fragmentShaderDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        OpenGLES2.SetData( fragmentShaderDataIndex, fragmentShaderSource )
        
        fragmentShaderIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
        OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fragmentShaderIndex )
        OpenGLES2.ShaderSource( fragmentShaderIndex, fragmentShaderDataIndex )
        OpenGLES2.CompileShader( fragmentShaderIndex )

        programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
        OpenGLES2.CreateProgram( programIndex )

        OpenGLES2.AttachShader( programIndex, vertexShaderIndex )
        OpenGLES2.AttachShader( programIndex, fragmentShaderIndex )

        vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        vertexLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( vertexLocationIndex, vertexLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      vertexLocationIndex,
                                      "position_in" )

        texCoordLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        texCoordLocationValue = indexTracker.allocIndex( 'OPENGLES2_LOCATION_VALUE' )
        OpenGLES2.SetRegister( texCoordLocationIndex, texCoordLocationValue )
        OpenGLES2.BindAttribLocation( programIndex,
                                      texCoordLocationIndex,
                                      "uv_in" )

        OpenGLES2.LinkProgram( programIndex )
        OpenGLES2.DeleteShader( vertexShaderIndex )
        OpenGLES2.DeleteShader( fragmentShaderIndex )

        mVPLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( mVPLocationIndex, programIndex, 'mvp' )
            
        textureLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
        OpenGLES2.GetUniformLocation( textureLocationIndex, programIndex, 'tex0' )

        # Create texture
        textureData0Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureData0Index,
                                            [ 1.0, 0.3, 0.3, 1.0 ],
                                            [ 0.3, 1.0, 0.3, 1.0 ],
                                            4, 4,
                                            TEXTURE_WIDTH,
                                            TEXTURE_HEIGHT,
                                            'OFF',
                                            'OPENGLES2_RGBA8888' )
        
        texture0Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture0Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture0Index )
        OpenGLES2.TexImage2D( textureData0Index, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateCheckerTextureData( textureData1Index,
                                            [ 0.3, 1.0, 0.3, 1.0 ],
                                            [ 1.0, 0.3, 0.3, 1.0 ],                                            
                                            4, 4,
                                            TEXTURE_WIDTH,
                                            TEXTURE_HEIGHT,
                                            'OFF',
                                            'OPENGLES2_RGBA8888' )
        
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )
        
        OpenGLES2.UseProgram( programIndex )
            
        OpenGLES2.BufferVertexAttribPointer( vertexBufferIndex,
                                             'ON',
                                             vertexLocationIndex,
                                             VERTEX_COMPONENTS,
                                             'OFF',
                                             0 )
        OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )
        
        OpenGLES2.ActiveTexture( 0 )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture0Index )
        OpenGLES2.Uniformi( textureLocationIndex, 1, [ 0 ] )
       
        OpenGLES2.BufferVertexAttribPointer( texCoordBufferIndex,
                                             'ON',
                                             texCoordLocationIndex,
                                             TEX_COORD_COMPONENTS,
                                             'OFF',
                                             0 )
        OpenGLES2.EnableVertexAttribArray( texCoordLocationIndex )

        OpenGLES2.UniformMatrix( mVPLocationIndex,
                                 1,
                                 [ 1.0, 0.0, 0.0, 0.0,
                                   0.0, 1.0, 0.0, 0.0,
                                   0.0, 0.0, 1.0, 0.0,
                                   0.0, 0.0, 0.0, 1.0 ] )
       
        # OpenGL ES 2.0 state setup
        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # ------------------------------------------------------------
        # ------------------------------------------------------------        
        # Start benchmark actions
        self.beginBenchmarkActions()

        # FIRST IMAGE/SURFACE
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        Egl.MakeCurrent( displayIndex, gles2Surface1Index, gles2Surface1Index, gles2ContextIndex )       
        
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        OpenGLES2.Viewport( 0, 0, self.imageWidth, self.imageHeight )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture0Index )        
        OpenGLES2.BufferDrawElements( indexBufferIndex, 'ON', plane.glMode, len( plane.indices ), 0 )    
        OpenGLES2.Finish()
        
        # Second, draw OpenVG
        Egl.BindApi( 'EGL_OPENVG_API' )
        Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContextIndex )       
        OpenVG.Clear( 0, 0, 0, 0 );

        # Draw background image (with OpenGL ES 2 content)
        OpenVG.LoadIdentity()        
        OpenVG.DrawImage( vgImage0Index )

        # Draw images emulating UI icons
        for i in range( iconCount ):
            OpenVG.LoadIdentity()
            OpenVG.Translate( vgIconCoordinates[ i ][ 0 ], vgIconCoordinates[ i ][ 1 ] )
            OpenVG.DrawImage( vgIconImageIndices[ i ] )
        
        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )

        # SECOND IMAGE/SURFACE
        Egl.BindApi( 'EGL_OPENGL_ES_API' )
        Egl.MakeCurrent( displayIndex, gles2Surface0Index, gles2Surface0Index, gles2ContextIndex )       
        
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        OpenGLES2.Viewport( 0, 0, self.imageWidth, self.imageHeight )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.BufferDrawElements( indexBufferIndex, 'ON', plane.glMode, len( plane.indices ), 0 )    
        OpenGLES2.Finish()
        
        # Second, draw OpenVG
        Egl.BindApi( 'EGL_OPENVG_API' )
        Egl.MakeCurrent( displayIndex, vgSurfaceIndex, vgSurfaceIndex, vgContextIndex )       
        
        OpenVG.Clear( 0, 0, 0, 0 );

        # Draw background image (with OpenGL ES 2 content)
        OpenVG.LoadIdentity()        
        OpenVG.DrawImage( vgImage1Index )

        # Draw images emulating UI icons
        for i in range( iconCount ):
            OpenVG.LoadIdentity()
            OpenVG.Translate( vgIconCoordinates[ i ][ 0 ], vgIconCoordinates[ i ][ 1 ] )
            OpenVG.DrawImage( vgIconImageIndices[ i ] )
        
        Egl.SwapBuffers( displayIndex, vgSurfaceIndex )

