#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../..' )

import  bd

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'OpenVG/OpenGLES2 cross api benchmark'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

#----------------------------------------------------------------------
( width, height, )      = target.getScreenSize()

#----------------------------------------------------------------------
import openvg_opengles2
suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SameSurfaceBenchmark( 0, True, False ) )
suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SameSurfaceBenchmark( 0, False, True ) )
suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SameSurfaceBenchmark( 0, True, True ) )

suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SameSurfaceBenchmark( 1, True, False ) )
suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SameSurfaceBenchmark( 1, False, True ) )
suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SameSurfaceBenchmark( 1, True, True ) )

suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SeparateSurfaceBenchmark( 0, True, False ) )
suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SeparateSurfaceBenchmark( 0, False, True ) )
suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SeparateSurfaceBenchmark( 0, True, True ) )

suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SeparateSurfaceBenchmark( 1, True, False ) )
suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SeparateSurfaceBenchmark( 1, False, True ) )
suite.addBenchmark( openvg_opengles2.OpenVGAndOpenGLES2SeparateSurfaceBenchmark( 1, True, True ) )

suite.addBenchmark( openvg_opengles2.OpenGLES2ToOpenVGImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )
suite.addBenchmark( openvg_opengles2.OpenGLES2ToOpenVGImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ) )
suite.addBenchmark( openvg_opengles2.OpenGLES2ToOpenVGImageBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ) )
suite.addBenchmark( openvg_opengles2.OpenGLES2ToOpenVGImageBenchmark( width, height, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )

suite.addBenchmark( openvg_opengles2.OpenGLES2ToTwoOpenVGImagesBenchmark( width, height, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) )

#----------------------------------------------------------------------

command()
