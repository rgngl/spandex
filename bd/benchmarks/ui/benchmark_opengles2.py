#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../..' )

import  bd
import  common

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'OpenGLES2 UI benchmark'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

#----------------------------------------------------------------------
( width, height, )      = target.getScreenSize()

#----------------------------------------------------------------------
# OpenGL ES 2.0 UI benchmarks
import gl2ui

suite.addBenchmark( gl2ui.DataUploadBenchmark( 64, 64,   'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.DataUploadBenchmark( 128, 128, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.DataUploadBenchmark( 256, 256, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.DataUploadBenchmark( 512, 512, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.DataUploadBenchmark( 640, 480, 'OPENGLES2_RGBA8888' ) )

# NPOT
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.PANELS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.GROUPS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.ICONS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS, common.GROUPS  ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS, common.ICONS  ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS, common.GROUPS, common.ICONS  ], False, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.BACKGROUND ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.PANELS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.GROUPS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.ICONS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS  ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS, common.ICONS  ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS, common.ICONS  ], False, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.PANELS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.GROUPS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.ICONS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS  ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS, common.ICONS  ], False, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS, common.ICONS  ], False, 'OPENGLES2_RGBA8888' ) )

# POT
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.PANELS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.GROUPS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.ICONS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS, common.GROUPS  ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS, common.ICONS  ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS, common.GROUPS, common.ICONS  ], True, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.BACKGROUND ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.PANELS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.GROUPS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.ICONS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS  ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS, common.ICONS  ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS, common.ICONS  ], True, 'OPENGLES2_RGBA8888' ) )

suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.PANELS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.GROUPS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.ICONS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS  ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS, common.ICONS  ], True, 'OPENGLES2_RGBA8888' ) )
suite.addBenchmark( gl2ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS, common.ICONS  ], True, 'OPENGLES2_RGBA8888' ) )

#----------------------------------------------------------------------

command()



