#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

#----------------------------------------------------------------------
import  sys
sys.path.append( '../../../python' )
sys.path.append( '../..' )

import  bd
import  common

#----------------------------------------------------------------------
suite, command          = bd.init()

suite.targetModulePath  = '../../lib/targets'
suite.name              = 'OpenGLES1 UI benchmark'

builder                 = suite.getBuilder( command.target )
target                  = suite.getTargetModule( command.target )
modules                 = builder.actionModules

#----------------------------------------------------------------------
( width, height, )      = target.getScreenSize()

#----------------------------------------------------------------------
# ES 1.1 UI benchmarks
import gl1ui

suite.addBenchmark( gl1ui.DataUploadBenchmark( 64, 64,   'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.DataUploadBenchmark( 128, 128, 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.DataUploadBenchmark( 256, 256, 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.DataUploadBenchmark( 512, 512, 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.DataUploadBenchmark( 640, 480, 'OPENGLES1_RGBA8888' ) )

suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD, [ common.PANELS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD, [ common.GROUPS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD, [ common.ICONS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS, common.GROUPS  ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS, common.ICONS  ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD, [ common.BACKGROUND, common.PANELS, common.GROUPS, common.ICONS  ], 'OPENGLES1_RGBA8888' ) )

suite.addBenchmark( gl1ui.UiBenchmark( common.DRAW, [ common.BACKGROUND ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.DRAW, [ common.PANELS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.DRAW, [ common.GROUPS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.DRAW, [ common.ICONS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS  ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS, common.ICONS  ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS, common.ICONS  ], 'OPENGLES1_RGBA8888' ) )

suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.PANELS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.GROUPS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.ICONS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS  ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS, common.ICONS  ], 'OPENGLES1_RGBA8888' ) )
suite.addBenchmark( gl1ui.UiBenchmark( common.UPLOAD_AND_DRAW, [ common.BACKGROUND, common.PANELS, common.GROUPS, common.ICONS  ], 'OPENGLES1_RGBA8888' ) )

#----------------------------------------------------------------------

command()



