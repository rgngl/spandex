#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from sct.bmbuilder import Benchmark
from lib.common    import *
from lib.mesh      import *
from common        import *

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class DataUploadBenchmark( Benchmark ):  
    def __init__( self, width, height, format ):
        Benchmark.__init__( self )
        self.width  = width
        self.height = height
        self.format = format
        self.name = "OPENVG data upload (%dx%d, %s)" % ( self.width, self.height, self.format )
       
    def build( self, target, modules ):          
        OpenVG = modules[ 'OpenVG' ]
        
        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex,
                                    self.format,
                                    self.width,
                                    self.height )

        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex,
                            self.format,
                            self.width,
                            self.height,
                            [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )

        OpenVG.ImageQuality( 'VG_IMAGE_QUALITY_NONANTIALIASED' )
        
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, screenWidth, screenHeight )
        target.swapBuffers( state )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.ImageSubData( imageDataIndex,
                             imageIndex,
                             0, 0,
                             self.width, self.height )

#----------------------------------------------------------------------
#----------------------------------------------------------------------
#----------------------------------------------------------------------
class UiBenchmark( Benchmark ):  
    def __init__( self, action, layers, format ):
        Benchmark.__init__( self )
        self.action = action
        self.layers = layers
        self.format = format
        self.name = "OPENVG ui actn=%s layers=%s frmt=%s" % ( actionToString( self.action ),
                                                              layersToString( self.layers ),
                                                              self.format, )
       
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        
        indexTracker = IndexTracker()

        ( screenWidth, screenHeight, ) = target.getScreenSize()
        
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        state = setupVG( modules, indexTracker, target )            

        backgroundWidth  = screenWidth
        backgroundHeight = screenHeight
        panelsX          = PANELSX
        panelsY          = PANELSY
        panelWidth       = screenWidth / panelsX
        panelHeight      = screenHeight / panelsY
        groupsX          = GROUPSX
        groupsY          = GROUPSY
        groupWidth       = panelWidth / groupsX
        groupHeight      = panelHeight / groupsY
        iconsX           = ICONSX
        iconsY           = ICONSY
        iconWidth        = panelWidth / iconsX
        iconHeight       = panelHeight / iconsY
        
        if BACKGROUND in self.layers:
            backgroundImageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
            backgroundImageIndex     = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
            
            OpenVG.CreateNullImageData( backgroundImageDataIndex,
                                        self.format,
                                        backgroundWidth,
                                        backgroundHeight )

            OpenVG.CreateImage( backgroundImageIndex,
                                self.format,
                                backgroundWidth,
                                backgroundHeight,
                                [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )
            OpenVG.ClearColor( [ 1.0, 1.0, 1.0, 1.0 ] )
            OpenVG.ClearImage( backgroundImageIndex,
                               0, 0,
                               backgroundWidth,
                               backgroundHeight )
            OpenVG.GetImageSubData( backgroundImageIndex,
                                    backgroundImageDataIndex,
                                    0, 0,
                                    backgroundWidth,
                                    backgroundHeight )

        if PANELS in self.layers:
            panelImageDataIndices  = []
            panelImageIndices      = []
            panelImageOrigins      = []

            total = panelsX * panelsY + 1
            for y in range( panelsY ):
                for x in range( panelsX ):
                    dataIndex  = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                    imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                    
                    panelImageDataIndices   += [ dataIndex ]
                    panelImageIndices       += [ imageIndex ]
                    panelImageOrigins       += [ ( panelWidth * x, panelHeight * y, ) ]
                    
                    OpenVG.CreateNullImageData( dataIndex,
                                                self.format,
                                                panelWidth,
                                                panelHeight )
                    
                    OpenVG.CreateImage( imageIndex,
                                        self.format,
                                        panelWidth,
                                        panelHeight,
                                        [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )

                    r = 1.0 * ( ( x + ( y * panelsX ) + 1 ) / float( total ) )
                    OpenVG.ClearColor( [ 0.0, r, 0.0, 0.5 ] )
                    OpenVG.ClearImage( imageIndex,
                                       0, 0,
                                       panelWidth,
                                       panelHeight )
                    OpenVG.GetImageSubData( imageIndex,
                                            dataIndex,
                                            0, 0,
                                            panelWidth,
                                            panelHeight )

        if GROUPS in self.layers:
            groupImageDataIndices   = []
            groupImageIndices       = []
            groupImageOrigins       = []

            total = groupsX * groupsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( groupsY ):
                        for x in range( groupsX ):
                            dataIndex  = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                            imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                    
                            groupImageDataIndices   += [ dataIndex ]
                            groupImageIndices       += [ imageIndex ]
                            groupImageOrigins       += [ ( px * panelWidth + groupWidth * x,
                                                           py * panelHeight + groupHeight * y, ) ]
                    
                            OpenVG.CreateNullImageData( dataIndex,
                                                        self.format,
                                                        groupWidth,
                                                        groupHeight )
                    
                            OpenVG.CreateImage( imageIndex,
                                                self.format,
                                                groupWidth,
                                                groupHeight,
                                                [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )

                            r = 1.0 * ( x + ( y * groupsX ) + 1 ) / float( total )
                            OpenVG.ClearColor( [ 0.0, 0.0, r, 0.5 ] )
                            OpenVG.ClearImage( imageIndex,
                                               0, 0,
                                               groupWidth,
                                               groupHeight )
                            OpenVG.GetImageSubData( imageIndex,
                                                    dataIndex,
                                                    0, 0,
                                                    groupWidth,
                                                    groupHeight )
                    
        if ICONS in self.layers:
            iconImageDataIndices    = []
            iconImageIndices        = []
            iconImageOrigins        = []

            total = iconsX * iconsY + 1
            for py in range( panelsY ):
                for px in range( panelsX ):
                    for y in range( iconsY ):
                        for x in range( iconsX ):
                            dataIndex  = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                            imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                    
                            iconImageDataIndices    += [ dataIndex ]
                            iconImageIndices        += [ imageIndex ]
                            iconImageOrigins        += [ ( px * panelWidth + iconWidth * x,
                                                           py * panelHeight + iconHeight * y, ) ]
                    
                            OpenVG.CreateNullImageData( dataIndex,
                                                        self.format,
                                                        iconWidth,
                                                        iconHeight )
                    
                            OpenVG.CreateImage( imageIndex,
                                                self.format,
                                                iconWidth,
                                                iconHeight,
                                                [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )

                            r = 1.0 * ( x + ( y * iconsX ) + 1 ) / float( total )
                            OpenVG.ClearColor( [ 0.3, 0.3, r, 0.5 ] )
                            OpenVG.ClearImage( imageIndex,
                                               0, 0,
                                               iconWidth,
                                               iconHeight )
                            OpenVG.GetImageSubData( imageIndex,
                                                    dataIndex,
                                                    0, 0,
                                                    iconWidth,
                                                    iconHeight )
                                    
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, screenWidth, screenHeight )
        target.swapBuffers( state )

        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )        
        blending = True 

        OpenVG.ImageQuality( 'VG_IMAGE_QUALITY_NONANTIALIASED' )        
        OpenVG.RenderingQuality( 'VG_RENDERING_QUALITY_NONANTIALIASED' )

        OpenVG.CheckError( '' )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        # Clear the screen if drawing is requested and the layers do not include background
        if not BACKGROUND in self.layers:       
            if self.action == DRAW or self.action == UPLOAD_AND_DRAW:
                OpenVG.Clear( 0, 0, screenWidth, screenHeight )

        # Do image data upload if requested
        if self.action == UPLOAD or self.action == UPLOAD_AND_DRAW:
            if BACKGROUND in self.layers:           
                OpenVG.ImageSubData( backgroundImageDataIndex,
                                     backgroundImageIndex,
                                     0, 0,
                                     backgroundWidth, backgroundHeight )

            if PANELS in self.layers:
                for i in range( len( panelImageDataIndices ) ):
                    OpenVG.ImageSubData( panelImageDataIndices[ i ],
                                         panelImageIndices[ i ],
                                         0, 0,
                                         panelWidth, panelHeight )

            if GROUPS in self.layers:
                for i in range( len( groupImageDataIndices ) ):
                    OpenVG.ImageSubData( groupImageDataIndices[ i ],
                                         groupImageIndices[ i ],
                                         0, 0,
                                         groupWidth, groupHeight )
                    
            if ICONS in self.layers:
                for i in range( len( iconImageDataIndices ) ):
                    OpenVG.ImageSubData( iconImageDataIndices[ i ],
                                         iconImageIndices[ i ],
                                         0, 0,
                                         iconWidth, iconHeight )

        # Draw the images if drawing is requested
        if self.action == DRAW or self.action == UPLOAD_AND_DRAW:            
            if BACKGROUND in self.layers:
                if blending:
                    OpenVG.BlendMode( 'VG_BLEND_SRC' )
                    blending = False               
                OpenVG.LoadIdentity()
                OpenVG.DrawImage( backgroundImageIndex )

            if PANELS in self.layers:
                for i in range( len( panelImageIndices ) ):
                    origin = panelImageOrigins[ i ]
                    if not blending:
                        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )
                        blending = True
                    OpenVG.LoadIdentity()                    
                    OpenVG.Translate( origin[ 0 ], origin[ 1 ] )               
                    OpenVG.DrawImage( panelImageIndices[ i ] )

            if GROUPS in self.layers:
                for i in range( len( groupImageIndices ) ):
                    origin = groupImageOrigins[ i ]
                    if not blending:
                        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )
                        blending = True                    
                    OpenVG.LoadIdentity()                                    
                    OpenVG.Translate( origin[ 0 ], origin[ 1 ] )               
                    OpenVG.DrawImage( groupImageIndices[ i ] )
                    
            if ICONS in self.layers:
                for i in range( len( iconImageIndices ) ):
                    origin = iconImageOrigins[ i ]
                    if not blending:
                        OpenVG.BlendMode( 'VG_BLEND_SRC_OVER' )
                        blending = True                                        
                    OpenVG.LoadIdentity()                                    
                    OpenVG.Translate( origin[ 0 ], origin[ 1 ] )               
                    OpenVG.DrawImage( iconImageIndices[ i ] )

            target.swapBuffers( state )
                    
