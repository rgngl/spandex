#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# ----------------------------------------------------------------------
from lib.common import *
from lib.egl    import *

# ----------------------------------------------------------------------
def supportFeature( features ):
    return set( [ 'EGL_LIMITED',
                  'EGL_FULL' ] ).issuperset( features )

# ----------------------------------------------------------------------
def getScreenSize():
    return [ 360, 640, ]

# ----------------------------------------------------------------------
def getScreenFormat():
    return 'SCT_COLOR_FORMAT_DEFAULT'

# ----------------------------------------------------------------------
def initialize( modules, indexTracker, api, attributes = {} ):
    Egl = modules[ 'Egl' ]

    if not attributes:
        attributes = getDefaultAttributes( api )

    colorSpace  = 'EGL_NONE'
    alphaFormat = 'EGL_NONE'
    if api == API_OPENVG:
        colorSpace  = 'EGL_VG_COLORSPACE_sRGB'
        alphaFormat = 'EGL_ALPHA_FORMAT_PRE'

    windowAttributes  = [ 0, 0, ] + getScreenSize() + [ getScreenFormat() ]         
    configAttributes  = getEglAttributes( api, SURFACE_WINDOW, attributes )
    surfaceAttributes = [ 'EGL_BACK_BUFFER', colorSpace, alphaFormat ]
        
    state = eglInitializeWindow( Egl, indexTracker, api, windowAttributes, configAttributes, surfaceAttributes )
    Egl.SwapInterval( state[ 'displayIndex' ], 0 )

    return state

# ----------------------------------------------------------------------
def swapInterval( state, interval ):
    Egl = state[ 'Egl' ]
    Egl.SwapInterval( state[ 'displayIndex' ], interval )

# ----------------------------------------------------------------------    
def swapBuffers( state ):
    eglSwapBuffers( state )

# ----------------------------------------------------------------------        
def getDefaultAttributes( api, surface  = SURFACE_WINDOW ):
    attributes = {}

    if api != API_OPENGLES1:
        raise 'Unsupported'
    
    attributes[ CONFIG_BUFFERSIZE ]     = 32
    attributes[ CONFIG_REDSIZE ]        = 8
    attributes[ CONFIG_GREENSIZE ]      = 8
    attributes[ CONFIG_BLUESIZE ]       = 8
    attributes[ CONFIG_ALPHASIZE ]      = 8
    attributes[ CONFIG_DEPTHSIZE ]      = '>=16'
    attributes[ CONFIG_ALPHAMASKSIZE ]  = '-'
    attributes[ CONFIG_SAMPLE_BUFFERS ] = 0
 
    return attributes

