#
# Spandex benchmark and test framework.
#
# Copyright (c) 2013 Contribute.
#
# Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

ENDFRAME = 'swapbuffers'
#ENDFRAME = 'finish'
#ENDFRAME = 'readpixels'

# ----------------------------------------------------------------------
from lib.common import *
from lib.egl    import *

# ----------------------------------------------------------------------
def supportFeature( features ):
    return set( [ 'EGL_LIMITED',
                  'EGL_FULL' ] ).issuperset( features )

# ----------------------------------------------------------------------
def getScreenSize():
    return [ 800, 1280, ]

# ----------------------------------------------------------------------
def getScreenFormat():
    return 'SCT_COLOR_FORMAT_DEFAULT'

# ----------------------------------------------------------------------
def initialize( modules, indexTracker, api, attributes = {} ):
    Egl       = modules[ 'Egl' ]
    OpenGLES2 = modules[ 'OpenGLES2' ]

    if not attributes:
        attributes = getDefaultAttributes( api )

    windowAttributes    = [ 0, 0, ] + getScreenSize() + [ getScreenFormat() ]         
    configAttributeList = getEglAttributeList( api, SURFACE_WINDOW, attributes )
    surfaceAttributes   = [ 'EGL_BACK_BUFFER', 'EGL_NONE', 'EGL_NONE' ]
        
    state = eglInitializeWindowFromList( Egl, indexTracker, api, windowAttributes, configAttributeList, surfaceAttributes )

    # Probably not supported properly.
    #Egl.SwapInterval( state[ 'displayIndex' ], 0 )

    if ENDFRAME != 'swapbuffers':
        state[ 'OpenGLES2' ] = OpenGLES2

        if ENDFRAME == 'readpixels':
            dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
            OpenGLES2.CreateData( dataIndex, 4 )
            state[ 'dataIndex' ] = dataIndex
    
    return state

# ----------------------------------------------------------------------
def swapInterval( state, interval ):
    Egl = state[ 'Egl' ]

    # Probably not supported properly
    #Egl.SwapInterval( state[ 'displayIndex' ], interval )

# ----------------------------------------------------------------------
def swapBuffers( state ):
    if ENDFRAME == 'swapbuffers':
        eglSwapBuffers( state )
    elif ENDFRAME == 'finish':
        OpenGLES2 = state[ 'OpenGLES2' ]
        OpenGLES2.Finish()
    elif ENDFRAME == 'readpixels':
        OpenGLES2 = state[ 'OpenGLES2' ]
        dataIndex = state[ 'dataIndex' ]
        OpenGLES2.ReadPixels( dataIndex, getScreenSize()[ 0 ] / 2, getScreenSize()[ 1 ] / 2, 1, 1 )            

# ----------------------------------------------------------------------
def getDefaultAttributes( api, surface = SURFACE_WINDOW ):
    attributes = {}

    if api != API_OPENGLES2:
        raise 'Unsupported'
    
    attributes[ CONFIG_BUFFERSIZE ]     = 32
    attributes[ CONFIG_REDSIZE ]        = 8
    attributes[ CONFIG_GREENSIZE ]      = 8
    attributes[ CONFIG_BLUESIZE ]       = 8
    attributes[ CONFIG_ALPHASIZE ]      = 8
    attributes[ CONFIG_DEPTHSIZE ]      = '>=16'
 
    return attributes
