#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# ----------------------------------------------------------------------
from lib.common import *

# ----------------------------------------------------------------------
def supportFeature( features ):
    return False

# ----------------------------------------------------------------------
def getScreenSize():
    return [ 768, 1024, ]

# ----------------------------------------------------------------------
def getScreenFormat():
    return 'SCT_COLOR_FORMAT_RGBA8888'

# ----------------------------------------------------------------------
def initialize( modules, indexTracker, api, attributes = {} ):
    Agl = modules[ 'Agl' ]
    Agl.Setup()
    
    return Agl

# ----------------------------------------------------------------------
def swapInterval( state, interval ):
    pass

# ----------------------------------------------------------------------    
def swapBuffers( state ):
    Agl = state
    #Agl.Present()   # Limited to 60 FPS
    Agl.Flush()    # Use readpixels to force render, do not draw to the screen
    
# ----------------------------------------------------------------------        
def getDefaultAttributes( api, surface  = SURFACE_WINDOW ):
    if api != API_OPENGLES2:
        raise 'Unsupported'
    
    return {}
