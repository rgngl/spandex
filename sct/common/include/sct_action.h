/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_ACTION_H__ )
#define __SCT_ACTION_H__

#include "sct_types.h"

/*!
 *
 *
 */
typedef void* ( *SCTCreateActionContextMethod )( void* moduleContext, SCTAttributeList* attributes );
typedef void  ( *SCTDestroyActionContextMethod )( void* actionContext );

/*!
 *
 *
 */
typedef struct
{
    const char*                     name;
    SCTCreateActionContextMethod    createActionContextMethod;
    SCTDestroyActionContextMethod   destroyActionContextMethod;

    SCTActionInitMethod             initMethod;
    SCTActionExecuteMethod          executeMethod;
    SCTActionTerminateMethod        terminateMethod;
    SCTActionDestroyMethod          destroyMethod;
    SCTActionGetWorkloadMethod      getWorkloadMethod;
} SCTActionTemplate;

#if defined( __cplusplus )
extern "C" {
#endif /* defined( __cplusplus ) */

    /* Action handling functions. */
    SCTAction*                  sctCreateAction( const char* name,
                                                 const char* type,
                                                 const char* module,
                                                 SCTAttributeList* attributes,
                                                 void* context,
                                                 SCTActionInitMethod init,
                                                 SCTActionExecuteMethod execute,
                                                 SCTActionTerminateMethod terminate,
                                                 SCTActionDestroyMethod destroy,
                                                 SCTActionGetWorkloadMethod getWorkload );
    void                        sctDestroyAction( SCTAction* action );

    /* Action list handling functions. */
    SCTActionList*              sctCreateActionList();
    SCTBoolean                  sctAddActionToList( SCTActionList* list, SCTAction* action );
    void                        sctGetActionListIterator( SCTActionList* list, SCTActionListIterator* iterator );
    SCTAction*                  sctGetNextActionListElement( SCTActionListIterator *iterator );
    void                        sctGetActionListReverseIterator( const SCTActionList* list, SCTActionListReverseIterator* iterator );
    SCTAction*                  sctGetPrevActionListElement( SCTActionListReverseIterator* iterator );
    void                        sctDestroyActionList( SCTActionList* list );
    void                        sctDestroyActionListAndElements( SCTActionList* list );

    /* Creating action from action template list. */
    SCTAction*                  sctCreateActionFromTemplate( const char* actionName, const char* actionType, const char* moduleName, void* moduleContext, SCTAttributeList* attributes, const SCTActionTemplate* templates, int templateCount );

#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif /* !defined( __SCT_ACTION_H__ ) */
