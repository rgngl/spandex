/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_TYPES_H__ )
#define __SCT_TYPES_H__

/*!
 * The required benchmark timing accuracy in percents. For example, 0.01 means
 * that benchmark time measurement should have at most 1% of error. Requiring
 * higher accuracy currently increases the running time of benchmarks linearily,
 * which is especially important to take into account on platforms that have low
 * resolution timers (such as early Symbian).
 */
#define SCTMaxRelativeTimingError               0.01

#define SCT_INLINE __inline

#if !defined( NULL )
# if defined( __cplusplus )
#  define NULL                                  0
# else  /* defined( __cplusplus ) */
#  define NULL                                  ( ( void* )( 0 ) )
# endif /* defined( __cplusplus ) */
#endif  /* !defined( NULL ) */

#if defined( SCT_DEBUG )
# define SCT_ASSERT( exp )                      ( void )( ( exp ) || ( siCommonAssert( ( void* )( NULL ), ( void* )( #exp ), ( void* )( __FILE__ ), ( unsigned int )( __LINE__ ) ), 0 ) )
#else   /* defined( SCT_DEBUG ) */
# define SCT_ASSERT( exp )                      ( ( void )( 0 ) )
#endif  /* defined( SCT_DEBUG ) */

#if !defined( SCT_REMOVE_ASSERTS )
# define SCT_ASSERT_ALWAYS( exp )               ( void )( ( exp ) || ( siCommonAssert( ( void* )( NULL ), ( void* )( #exp ), ( void* )( __FILE__ ), ( unsigned int )( __LINE__ ) ), 0 ) )
#else   /* !defined( SCT_REMOVE_ASSERTS ) */
# define SCT_ASSERT_ALWAYS( exp )               ( void )( exp )
#endif  /* !defined( SCT_REMOVE_ASSERTS ) */

#define SCT_LOG_ERROR( msg )                    ( sctLogErrorFileLine( msg, ( const char* )( __FILE__ ), ( unsigned int )( __LINE__ ) ) )
#define SCT_ASSERT_ERROR_REPORTED               ( SCT_ASSERT( strlen( sctGetErrorLog()->error ) > 0 ) )

/* Useful macro for getting array length in for-loops etc. */
#define SCT_ARRAY_LENGTH( x )                   ( sizeof( x ) / sizeof( x[ 0 ] ) )

/* Macro to disable compiler warning of unused function argument */
#define SCT_USE_VARIABLE( X )                   ( void )( X )

/* Macro for converting floating point value to 16.16 fixed point */
#define SCT_FLOAT_TO_FIXED( F )                 ( ( int )( ( F ) * 65536.0f ) )

/*Strings. */
#define SCT_BENCHMARK_TYPE_STRING               "BENCHMARK"
#define SCT_MODULE_TYPE_STRING                  "MODULE"
#define SCT_BENCHMARK_REPEATS_ATTRIBUTE         "Repeats"
#define SCT_BENCHMARK_MIN_REPEAT_TIME_ATTRIBUTE "MinRepeatTime"
#define SCT_BENCHMARK_TIMES_RESULT              "Times"
#define SCT_BENCHMARK_WORKLOADS_RESULT          "Workloads"
#define SCT_BENCHMARK_EMPTY_ACTIONS             "None"
#define SCT_BENCHMARK_INIT_ACTIONS              "InitActions"
#define SCT_BENCHMARK_BENCHMARK_ACTIONS         "BenchmarkActions"
#define SCT_BENCHMARK_FINAL_ACTIONS             "FinalActions"
#define SCT_BENCHMARK_PRETERMINATE_ACTIONS      "PreTerminateActions"
#define SCT_BENCHMARK_TERMINATE_ACTIONS         "TerminateActions"
#define SCT_BENCHMARK_WARNINGS_RESULT           "Warnings"
#define SCT_BENCHMARK_ERROR_RESULT              "Error"
#define SCT_RELEASE_PREAMPLE                    " Spandex release: "
#define SCT_MAX_RELEASE_LENGTH                  63
#define SCT_REPORT_NAME_FIELD_WIDTH		        30

/* Input format delimeters. */
#define SCT_SECTION_HEADER_START                '['
#define SCT_SECTION_HEADER_END                  ']'
#define SCT_NAME_VALUE_SEPARATOR                ':'
#define SCT_QUOTATION_MARK                      '"'
#define SCT_TYPE_MODULE_SEPARATOR               '@'
#define SCT_ACTION_LIST_SEPARATOR               '+'
#define SCT_ARRAY_START_CHAR                    '{'
#define SCT_ARRAY_END_CHAR                      '}'
#define SCT_COMMENT_CHAR                        '#'
#define SCT_ESCAPE_CHAR                         '\\'

#define SCT_MAX_ERROR_MSG_LEN                   255
#define SCT_MIN_ITERATIONS                      4
#define SCT_MIN_BYTE                            -0x7F
#define SCT_MAX_BYTE                            0x7F
#define SCT_MIN_SHORT                           -0x7FFF
#define SCT_MAX_SHORT                           0x7FFF
#define SCT_STATIC_ARRAY_BUFFER_LEN             128
#define SCT_MAX_WORKER_THREADS                  3
#define SCT_MAX_THREADS                         1 + SCT_MAX_WORKER_THREADS
#define SCT_THREAD_SIGNAL_TIMEOUT_MILLIS        3000

#if 0
#define SCT_USE_PRECYCLE_SLEEP
#define SCT_PRECYCLE_SLEEP_MILLIS               2000
#endif

/*!
 * Condition enumerations.
 * 
 */
typedef enum
{
    SCT_DONTCARE = -1,    
    SCT_LESS,
    SCT_GREATER,
    SCT_LEQUAL,
    SCT_GEQUAL,
    SCT_EQUAL,
    SCT_NOTEQUAL,
    SCT_FOUND,
} SCTCondition;

/*!
 * Message types.
 *
 */
typedef enum
{
    SCT_MESSAGE_POST_BUFFER,    /* Signal posting a buffer to the display; message value is not used. */
    SCT_MESSAGE_GET_BUFFER,     /* Signal getting a buffer for drawing; message value stores milliseconds it took to acquire the buffer. */
} SCTMessageType;

/*!
 * CPU load types.
 *
 */
typedef enum
{
    SCT_CPULOAD_SYSTEM,         /* CPU load for the system; ratio of the system
                                 * busy CPU time compared to the total system
                                 * CPU time (busy + idle). */
    SCT_CPULOAD_PROCESS2BUSY,   /* CPU load for the current process; ratio of
                                 * the current process CPU time compared to the
                                 * system busy CPU time. */
    SCT_CPULOAD_PROCESS2TOTAL,  /* CPU load for the current process; ratio of
                                 * the current process CPU time compared to the
                                 * total system CPU time (busy + idle). */
} SCTCpuLoad;

/*******************************************************************************
 * Type definitions.
 *******************************************************************************/

/*!
 * Message struct. If supported by the underlying system, messages can be used
 * to receive timed events from the graphics system. These events can be used
 * for performance profiling.
 *
 */
struct _SCTMessage
{
    SCTMessageType  type;           /* Message type. */
    unsigned long   millis;         /* Message timestamp in milliseconds. Used
                                     * to drop abandoned messages from earlier
                                     * benchmarks. */
    double          value;          /* Message value. */
};

typedef struct _SCTMessage SCTMessage;

/*!
 * Conditional int.
 *
 */
struct _SCTConditionalInt
{
    SCTCondition    condition;
    int             value;
};

typedef struct _SCTConditionalInt SCTConditionalInt;

/*!
 * List element structure.
 *
 */
struct _SCTListElement
{
    void*                               element;
    struct _SCTListElement*             prev;
    struct _SCTListElement*             next;
};

typedef struct _SCTListElement SCTListElement;

/*!
 * List structure. List is a double linked list with arbitrary list
 * elements. List should not be used directly but instead through type
 * safe access functions.
 *
 */
typedef struct
{
    SCTListElement*                     head;
    SCTListElement*                     tail;
} SCTList;

/*!
 * List iterator structure.
 *
 */
typedef struct
{
    SCTListElement*                     current;
    SCTListElement*                     next;
} SCTListIterator;

/*!
 * List reverse iterator structure.
 *
 */
typedef struct
{
    SCTListElement*                     current;
    SCTListElement*                     prev;
} SCTReverseListIterator;

/*!
 * String list. List of strings.
 *
 */
typedef struct
{
    SCTList*                            list;
} SCTStringList;

/*!
 * String list iterator.
 *
 */
typedef struct
{
    SCTListIterator                     listIterator;
} SCTStringListIterator;

/*!
 * Log used for reporting errors and warnings. Note that log always has storage
 * for storing one error message so reporting an error cannot fail. The first
 * error is logged and subsequent error messages are reported only using debug
 * print until the log is reseted. Reporting a warning may fail due to a memory
 * allocation failure. Access to log is possible from several threads, hence the
 * access is controlled by mutex. However, destroying log should be done only in
 * situation where it cannot be accessed from multiple threads.
 *
 */
struct SCTLog
{
    void*                               mutex;
    char                                error[ SCT_MAX_ERROR_MSG_LEN + 1 ];
    SCTStringList*                      warnings;
};

/*!
 *
 */
typedef struct SCTLog SCTLog;

/*!
 * Boolean typedef.
 *
 */
typedef enum
{
	SCT_FALSE,
	SCT_TRUE
} SCTBoolean;

/*!
 * Integer array with length.
 *
 */
typedef struct
{
    int                                 length;
    int*                                data;
} SCTIntVector;

/*!
 * Floating point array with length.
 *
 */
typedef struct
{
    int                                 length;
    float*                              data;
} SCTFloatVector;

/*!
 * Double-precision floating point array with length.
 *
 */
typedef struct
{
    int                                 length;
    double*                             data;
} SCTDoubleVector;

/*!
 * Hex array with length.
 *
 */
typedef struct
{
    int                                 length;
    unsigned int*                       data;
} SCTHexVector;

/*!
 * 
 * 
 */
typedef enum
{
    SCT_ERROR_LOG_POINTER,
    SCT_THREAD_MODULE_POINTER,
    SCT_EGL_MODULE_POINTER,    
    SCT_OPENVG_MODULE_POINTER,
    SCT_OPENGLES1_MODULE_POINTER,
    SCT_OPENGLES2_MODULE_POINTER,
} SCTPointerID;

/* ************************************************** */
/*                      ATTRIBUTE                     */
/* ************************************************** */

/*!
 * Attribute. Attribute has name and value.
 *
 */
typedef struct
{
    char*                               name;
    char*                               value;
} SCTAttribute;

/*!
 * Attribute list. Use attribute list through type-safe functions.
 *
 */
typedef struct
{
    SCTList*                            list;
} SCTAttributeList;

/*!
 * Attribute list iterator.
 *
 */
typedef struct
{
    SCTListIterator                     listIterator;
} SCTAttributeListIterator;

/* ************************************************** */
/*                      ACTION                        */
/* ************************************************** */

struct _SCTAction;
struct _SCTBenchmark;

/*! Action init method function pointer type. */
typedef SCTBoolean ( *SCTActionInitMethod )( struct _SCTAction* action, struct _SCTBenchmark* benchmark );

/*! Action execute method function pointer type. */
typedef SCTBoolean ( *SCTActionExecuteMethod )( struct _SCTAction* action, int frameNumber );

/*! Action terminate method function pointer type. */
typedef void ( *SCTActionTerminateMethod )( struct _SCTAction* action );

/*! Action destroy function pointer type. */
typedef void ( *SCTActionDestroyMethod )( struct _SCTAction* action );

/*! Action get result method function pointer type. */
typedef SCTStringList* ( *SCTActionGetWorkloadMethod )( struct _SCTAction* action );

/*!
 * Action structure.
 *
 * This structure is the interface for Action. Action contains name,
 * type, and module strings which uniquely identify the particular
 * action. In addition, action contains its attributes, context (if
 * any), and function pointers to methods exported by the particular
 * Action.
 *
 */
struct _SCTAction
{
    char*                               name;
    char*                               type;
    char*                               moduleName;
    SCTAttributeList*                   attributes;
    void*                               context;
    SCTActionInitMethod                 init;
    SCTActionExecuteMethod              execute;
    SCTActionTerminateMethod            terminate;
    SCTActionDestroyMethod              destroy;
    SCTActionGetWorkloadMethod          getWorkload;
};

typedef struct _SCTAction SCTAction;

/*!
 * Action list structure.
 *
 */
typedef struct
{
    SCTList*                            list;
} SCTActionList;

/*!
 * Action list iterator.
 *
 */
typedef struct
{
    SCTListIterator                     listIterator;
} SCTActionListIterator;

/*!
 * Action list reverse iterator.
 *
 */
typedef struct
{
    SCTReverseListIterator              listIterator;
} SCTActionListReverseIterator;

/* ************************************************** */
/*                      MODULE                        */
/* ************************************************** */

struct _SCTModule;

/*! Module information function pointer type. */
typedef SCTAttributeList* ( *SCTModuleInfoMethod )( struct _SCTModule* module );

/*! Module create action method function pointer type. */
typedef SCTAction* ( *SCTModuleCreateActionMethod )( struct _SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );

/*! Module destructor function pointer type. */
typedef void ( *SCTModuleDestroyMethod )( struct _SCTModule* module );

/*!
 * Module structure. This structure is the interface for a
 * Module. Module contains a name, context, config, and pointers to
 * exported module functions.
 */
struct _SCTModule
{
    char*                               name;         /*!< Module name. */
    void*                               context;      /*!< Module context. */
    const char*                         config;       /*!< Module config. */
    SCTModuleInfoMethod                 info;         /*!< Pointer to module info method. */
    SCTModuleCreateActionMethod         createAction; /*!< Pointer to module createAction method. */
    SCTModuleDestroyMethod              destroy;      /*!< Pointer to module destroy method. */
};

typedef struct _SCTModule SCTModule;

/*!
 * Module list structure.
 */
typedef struct
{
    SCTList*                            list;
} SCTModuleList;

/*!
 * Module list iterator.
 */
typedef struct
{
    SCTListIterator                     listIterator;
} SCTModuleListIterator;

/* ************************************************** */
/*                    BENCHMARK                       */
/* ************************************************** */

/*!
 * Benchmark result.
 *
 */
typedef struct
{
    double                              time;
} SCTBenchmarkResult;

/*!
 * Benchmark result list.
 *
 */
typedef struct
{
    SCTList*                            list;
} SCTBenchmarkResultList;

/*!
 * Benchmark result list iterator.
 *
 */
typedef struct
{
    SCTListIterator                     listIterator;
} SCTBenchmarkResultListIterator;

/*!
 * Benchmark thread actions.
 *
 */
struct _SCTBenchmarkThreadActions
{
    SCTActionList*                      initActions;         /*!< List of init actions. */
    SCTActionList*                      benchmarkActions;    /*!< List of benchmark actions. */
    SCTActionList*                      finalActions;        /*!< List of final actions. */
    SCTActionList*                      preTerminateActions; /*!< List of pre-terminate actions. */
    SCTActionList*                      terminateActions;    /*!< List of terminate actions. */        
};

typedef struct _SCTBenchmarkThreadActions SCTBenchmarkThreadActions;

/*! 
 * Benchmark structure.
 *
 */
struct _SCTBenchmark
{
    char*                               name;                                    /*!< Benchmark name. */
    SCTBenchmarkThreadActions           mainThread;                              /*!< Benchmark actions for main thread. */    
    SCTBenchmarkThreadActions           workerThreads[ SCT_MAX_WORKER_THREADS ]; /*!< Benchmark actions per worker thread. */
    SCTAttributeList*                   attributes;                              /*!< List of benchmark attributes. */
    int                                 repeats;                                 /*!< Number of times the benchmark actions are executed by the runner. */
    volatile int                        completed;                               /*!< Flag to allow benchmark actions to trigger benchmark completion. */
    float                               minRepeatTime;                           /*!< Minimum time to repeat the benchmark actions. */
#if defined( SCT_INFINITE_BENCHMARK_LOOP )
    int                                 originalRepeats;                         /*!< Original repeat count. */
#endif  /* defined( SCT_INFINITE_BENCHMARK_LOOP ) */
    SCTBenchmarkResultList*             benchmarkResultList;                     /*!< List of benchmark results. */
    SCTLog*                             log;                                     /*!< Log for warning and error reporting. */
};

typedef struct _SCTBenchmark SCTBenchmark;


/*!
 * Benchmark worker thread commands.
 * 
 */
typedef enum
{
    SCT_WORKER_THREAD_COMMAND_INITIALIZE = 0,   /* Trigger worker thread to initialize. */
    SCT_WORKER_THREAD_COMMAND_EXECUTE    = 1,   /* Thrigger worker thread to execute the actions. */
    SCT_WORKER_THREAD_COMMAND_SUSPEND    = 2,   /* Thrigger worker thread to suspend executing action. */
    SCT_WORKER_THREAD_COMMAND_TERMINATE  = 3,   /* Trigger worker thread to terminate. */
} SCTWorkerThreadCommand;

/*!
 * Benchmark worker thread state enum.
 * 
 */
typedef enum
{
    SCT_WORKER_THREAD_STATE_CREATED      = 0,   /* Worker thread has been created by has not started execution. */
    SCT_WORKER_THREAD_STATE_STARTED      = 1,   /* Worker thread has started execution. */
    SCT_WORKER_THREAD_STATE_INITIALIZED  = 2,   /* Worker thread has initialized the actions. */
    SCT_WORKER_THREAD_STATE_EXECUTING    = 3,   /* Worker thread is executing its actions. */    
    SCT_WORKER_THREAD_STATE_SUSPENDED    = 4,   /* Worker thread has suspended. */    
    SCT_WORKER_THREAD_STATE_TERMINATED   = 5,   /* Worker thread has terminated. */
    SCT_WORKER_THREAD_STATE_INERROR      = 6    /* Worker thread is in error state and should be terminated. */
} SCTWorkerThreadState;

/*!
 * Benchmark worker thread context structure.
 *
 */
struct _SCTBenchmarkWorkerThreadContext
{
    SCTBenchmark*                       benchmark;      /*!< Reference to the thread benchmark. Not owned. */
    int                                 index;          /*!< Thread index for benchmark actions. */
    void*                               mutex;          /*!< Thread mutex. */
    volatile SCTWorkerThreadState       state;          /*!< Thread state control variable. */    
    void*                               signal;         /*!< Signal for the main thread to command the thread. */
    volatile SCTWorkerThreadCommand     command;        /*!< Thread command variable. Use with signal. */
    void*                               mainSignal;     /*!< Reference to the the main thread signal. Not owned. */    
};
    
typedef struct _SCTBenchmarkWorkerThreadContext SCTBenchmarkWorkerThreadContext;

/*!
 * Benchmark list.
 *
 */
typedef struct
{
    SCTList*                            list;
} SCTBenchmarkList;

/*!
 * Benchmark list iterator.
 *
 */
typedef struct
{
    SCTListIterator                     listIterator;
} SCTBenchmarkListIterator;

#endif /* !defined( __SCT_TYPES_H__ ) */
