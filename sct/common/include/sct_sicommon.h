/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_SISYSTEM_H__ )
#define __SCT_SISYSTEM_H__

#include "sct_types.h"

#include <stddef.h>

/*!
 * Character value that marks the end of system input stream.
 *
 */
#define SCT_END_OF_INPUT '\0'

/*!
 * System string IDs to be used with siQuerySystemString.
 *
 */
typedef enum
{
    /*! Internal marker, not mean to be used. */
    SCT_SYSSTRING_INVALID,
    /*! Name of the device. */
    SCT_SYSSTRING_DEVICE_TYPE,
    /*! CPU information (type and speed). */
    SCT_SYSSTRING_CPU,
    /*! Name of the operating system. */
    SCT_SYSSTRING_OS,
} SCTSystemString;

/*!
 * Memory attribute IDs to be used with siQueryMemoryAttribute. Note that
 * querying the graphics memory attributes might have a dependency to underlying
 * graphics system. For example, in a system supporting EGL, you might be able
 * to query the correct attribute values only when EGL has been initialized for
 * the calling thread.
 *
 */
typedef enum
{
    /*! Internal marker, not mean to be used. */
    SCT_MEMATTRIB_INVALID,
    /*! Total amount of installed system RAM memory, in bytes. */
    SCT_MEMATTRIB_TOTAL_SYSTEM_MEMORY,
    /*! Amount of free system RAM memory, in bytes. */
    SCT_MEMATTRIB_USED_SYSTEM_MEMORY,
    /*! Total amount of installed graphics memory, in bytes. */
    SCT_MEMATTRIB_TOTAL_GRAPHICS_MEMORY,
    /*! Amount of free graphics memory, in bytes. */
    SCT_MEMATTRIB_USED_GRAPHICS_MEMORY,
    /*! Amount of used private graphics memory, in bytes. */
    SCT_MEMATTRIB_USED_PRIVATE_GRAPHICS_MEMORY,
    /*! Amount of used shared graphics memory, in bytes. */
    SCT_MEMATTRIB_USED_SHARED_GRAPHICS_MEMORY,
} SCTMemoryAttribute;

/*!
 * Thread priorities.
 *
 */
typedef enum
{
    SCT_THREAD_PRIORITY_LOW,        
    SCT_THREAD_PRIORITY_NORMAL,
    SCT_THREAD_PRIORITY_HIGH,    
} SCTThreadPriority;

/*!
 *
 *
 */
typedef void ( *SCTExtensionProc )( void );
typedef int ( *SCTBenchmarkWorkerThreadFunc )( SCTBenchmarkWorkerThreadContext* context );

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    /*!
     * Returns the context pointer for common system interface (which can
     * be NULL on some platforms).
     *
     * This function can be used for example to cache the common system
     * interface context pointer to a local variable. The cached pointer
     * can then be used with common system interface calls to ensure the
     * maximum execution speed on all platforms.
     *
     * \return Pointer to the internal common system interface context.
     */
    void* siCommonGetContext( void );

    /*********************************************************************************
     * Functions for querying system information.
     ********************************************************************************/

    /*!
     * Returns a string describing the requested aspect of the underlying
     * system.
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param name      Id of the requested string.
     *
     * \return Pointer to a static NULL-terminated string.
     */
    const char* siCommonQuerySystemString( void* context, SCTSystemString name );

    /*!
     * Returns a size_t describing the requested aspect of the underlying
     * memory system.
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param name      Id of the requested memory attribute.
     *
     * \return Memory attribute value as size_t.
     */
    size_t siCommonQueryMemoryAttribute( void* context, SCTMemoryAttribute name );

    /*!
     * Get a CPU load sample that can be subsequently used for calculating the
     * average CPU load. Return NULL if CPU load profiling is not supported by
     * the underlying system.
     *
     * \param context   Pointer to common system interface context, or NULL.
     *
     * \return A CPU load sample, or NULL in case of failure.
     */
    void* siCommonSampleCpuLoad( void* context );

    /*!
     * Calculate the average CPU load since the given CPU load sample.
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param sample    Pointer to a earlier cpu load sample. Must be defined.
     * \param load      CPU load type to calculate.
     *
     * \return A floating point value between 0.0 and 1.0 denoting the average
     * CPU load of a given load type. A negative value is returned in case of a
     * failure.
     */    
    float siCommonCalculateCpuLoad( void* context, void* sample, SCTCpuLoad load );

    /*!
     * Destroy a CPU load sample.
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param sample    Pointer to a cpu load sample to release. Must be defined.
     *
     */
    void siCommonDestroyCpuLoadSample( void* context, void* sample );
    
    /*!
     * Retreives the system's timer frequency.
     *
     * \param context Pointer to common system interface context, or NULL.
     *
     * \return The system's timer frequency.
     */
    unsigned long siCommonGetTimerFrequency( void* context );

    /*!
     * Retreives the current value of the system's timer.
     *
     * \param context Pointer to common system interface context, or NULL.
     *
     * \return The current value of the system's timer.
     */
    unsigned long siCommonGetTimerTick( void* context );

    /********************************************************************************
     * Information reporting functions.
     *******************************************************************************/

    /*!
     * Debug print function. The arguments follow the conventions of a standard c
     * library printf.
     *
     * \param context Pointer to common system interface context, or NULL
     * \param fmt message format.
     */
    void siCommonDebugPrintf( void* context, const char* fmt, ... );
    
    /*!
     * Error callback function.
     *
     * \param context Pointer to common system interface context, or NULL
     * \param errorMessage Error string.
     */
    void siCommonErrorMessage( void* context, const char* errorMessage );
    
    /*!
     * Warning callback function.
     *
     * \param context Pointer to common system interface context, or NULL.
     * \param warningMessage Warning string.
     */
    void siCommonWarningMessage( void* context, const char* warningMessage );
    
    /*!
     * Returns the value of global singleton pointer. The singleton pointer is
     * used by the SCT core to store globally accessible data. The singleton is
     * guaranteed to be NULL if it has not been set with siCommonSetSingleton.
     *
     * Note: it is possible that this function is relatively inefficient on some
     * environments, so it is advisable to not call this function or the
     * functions that use this function where the performance matters. E.g.,
     * caching should be used in those cases.
     *
     * \param context Pointer to the common system interface context, or NULL.
     */
    void* siCommonGetSingleton( void* context );

    /*!
     * Sets the value of global singleton pointer. 
     *
     * \param context Pointer to the common system interface context, or NULL.
     */
    void siCommonSetSingleton( void* context, void* ptr );

    /*!
     * Displays a benchmark running progress message.
     *
     * This function is called every time when a new benchmark is
     * started. The implementation can choose how to display the
     * information or even completely ignore it.
     *
     * \param context Pointer to common system interface context, or NULL.
     * \param totalBenchmarks Total number of benchmarks.
     * \param currentBenchmark Current benchmark number.
     * \param currentBenchmark Current benchmark repeat, 0 means benchmark initialization.
     * \param benchmarkName Current benchmark name.
     */
    void siCommonProgressMessage( void* context, int totalBenchmark, int currentBenchmark, int currentRepeat, const char* benchmarkName );

    /*!
     * Function for displaying an assertion message.
     *
     * \param context Pointer to common system interface context, or NULL.
     * \param exp Pointer to the assert exression (as a string).
     * \param file Pointer to the name of the file where the assert failed .
     * \param line Line number where the assert failed.
     */
    void siCommonAssert( void* context, void* exp, void* file, unsigned int line );

    /*!
     * Send profile event.
     *
     * \param context Pointer to common system interface context, or NULL
     * \param event Profile event.
     */
    void siCommonProfileEvent( void* context, const char* event );

    /********************************************************************************
     * Memory management functions
     *******************************************************************************/

    /*!
     * Allocates a memory block and clears it (contents are set to zero).
     * 
     * \param context Pointer to common system interface context, or NULL.
     * \param size Amount of bytes to allocate.
     * 
     * \return Pointer to allocated memory block if successful, NULL if
     * fails.
     */
#if defined( SCT_CHECK_LEAKS )
# define siCommonMemoryAlloc( context, size ) siCommonMemoryAllocDbg( context, size, __FILE__, __LINE__)
    void* siCommonMemoryAllocDbg( void* context, unsigned int size, const char* filename, int linenumber );
#else   /* defined( SCT_CHECK_LEAKS ) */
    void* siCommonMemoryAlloc( void* context, unsigned int size );
#endif  /* defined( SCT_CHECK_LEAKS ) */

    /*!
     * Reallocate a memory block. 
     *
     * \param context Pointer to common system interface context, or NULL.
     * \param data Pointer to memory block to be reallocated. Must be defined.
     * \param size Amount of bytes to allocate.
     *
     * \return Pointer to reallocated memory block if successful, or
     * NULL if failure.
     */
#if defined(SCT_CHECK_LEAKS)
# define siCommonMemoryRealloc( context, data, size ) siCommonMemoryReallocDbg( context, data, size, __FILE__, __LINE__)
    void* siCommonMemoryReallocDbg( void* context, void* data, unsigned int size, const char* filename, int linenumber );
#else
    void* siCommonMemoryRealloc( void* context, void* data, unsigned int size );
#endif

    /*!
     * Frees a memory block.
     * 
     * \param context Pointer to common system interface context, or NULL.
     * \param memblock Previously allocated memory block to be freed.
     */
    void siCommonMemoryFree( void* context, void* memblock );

    /*********************************************************************************
     * IO functions.
     ********************************************************************************/

    /*!
     * Reads one characted from the system input stream.
     *
     * \param context Pointer to common system interface context, or NULL.
     *
     * \return Character value, which is SCT_END_OF_INPUT when there is not
     * more data to read.
     */
    char siCommonReadChar( void* context );

    /*!
     * Write one character to the system output stream.
     *
     * \param context Pointer to common system interface context, or NULL.
     * \param ch Character to write.
     * 
     * \return Non-zero if succesful, zero if fails.
     */
    int siCommonWriteChar( void* context, char ch );    

    /*!
     * Flush system output stream.
     *
     * \param context Pointer to common system interface context, or NULL.
     *
     * \return Non-zero if succesful, zero if fails.
     */
    int siCommonFlushOutput( void* context );  

    /*********************************************************************************
     * Thread functions
     ********************************************************************************/
    
    /*!
     * Create a benchmark thread and set it running. Implement to return a dummy
     * object in platforms not supporting multithreading.
     *
     * \param context       Pointer to common system interface context, or NULL.
     * \param func          Benchmark thread function.
     * \param threadContext Benchmark thread context. The caller retains data ownership.
     *
     * \return Platform-specific thread object, or NULL if the function failed.
     */
    void* siCommonCreateThread( void* context, SCTBenchmarkWorkerThreadFunc func, SCTBenchmarkWorkerThreadContext* threadContext );

    /*!
     * Do system specific per-thread cleanup when the calling thread is being
     * terminated; e.g. call CloseSTDLIB() in Symbian.
     *
     * \param context   Pointer to common system interface context, or NULL.
     *
     */
    void siCommonThreadCleanup( void* context );
    
    /*!
     * Wait for a benchmark thread to terminate and release the thread object. 
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param thread    Pointer to a platform-specific thread object. Must be defined.
     *
     */
    void siCommonJoinThread( void* context, void* thread );

    /*!
     * Set thread priority.
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param priority  Thread priority.
     *
     * \return SCT_TRUE if success, SCT_FALSE if failure.
     */
    SCTBoolean siCommonSetThreadPriority( void* context, SCTThreadPriority priority );
    
    /*!
     * Create mutex. Initially, mutex is unlocked. Implement to return a dummy
     * object in platforms not supporting multithreading.
     *
     * \param context   Pointer to common system interface context, or NULL.
     *
     * \return Platform-specific mutex object, or NULL if the function failed.
     */
    void* siCommonCreateMutex( void* context );

    /*!
     * Lock mutex.
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param mutex     Pointer to a platform-specific mutex object. Must be defined.     
     *
     */
    void siCommonLockMutex( void* context, void* mutex );

    /*!
     * Unlock mutex.
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param mutex     Pointer to a platform-specific mutex object. Must be defined.     
     *
     */
    void siCommonUnlockMutex( void* context, void* mutex );
        
    /*!
     * Destroy mutex.
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param mutex     Pointer to a platform-specific mutex object. Must be defined.     
     *
     */
    void siCommonDestroyMutex( void* context, void* mutex );

    /*!
     * Create signal. Initially, signal is not triggered, i.e. wait for signal
     * will block. Note that the thread module signals might be created and
     * destroyed several times per benchmark to prevent signal state
     * leaking. Implement to return a dummy object in platforms not supporting
     * multithreading.
     *
     * \param context   Pointer to common system interface context, or NULL.
     *
     * \return Platform-specific signal object, or NULL if the function failed.
     */
    void* siCommonCreateSignal( void* context );

    /*!
     * Destroy signal.
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param signal    Pointer to a platform-specific signal object. Must be defined.     
     *
     */
    void siCommonDestroySignal( void* context, void* signal );    
    
    /*!
     * Wait for signal.
     *
     * \param context        Pointer to common system interface context, or NULL.
     * \param signal         Pointer to a platform-specific signal object. Must be defined.
     * \param timeoutMillis  Timeout for wait, in milliseconds. Use infinite timeout if zero.
     *
     * \return Return 1 when signal received ok, 0 if timeout occurred, -1 if
     * wait was aborted from external thread, or if the signal is disabled.
     */
    int siCommonWaitForSignal( void* context, void* signal, long timeoutMillis );

    /*!
     * Trigger signal. Set signal as triggered, releasing a single thread
     * waiting for the signal. Ignored for disabled signals.
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param signal    Pointer to a platform-specific signal object. Must be defined.
     *
     */
    void siCommonTriggerSignal( void* context, void* signal );   
    
    /*!
     * Cancel a signal. Wake up all threads waiting on the signal so that
     * siCommonWaitForSignal returns -1. Signal is disabled after the call, all
     * waits return immediately with -1.
     *
     * \param context   Pointer to common system interface context, or NULL.
     * \param signal    Pointer to a platform-specific signal object. Must be defined.
     *
     */
    void siCommonCancelSignal( void* context, void* signal );
    
    /*********************************************************************************
     * Other functions
     ********************************************************************************/

    /*!
     * Get a list of modules supported by the current system
     * configuration. Caller must destroy the module list.
     *
     * \param context Pointer to common system interface context, or NULL.
     *
     * \return List of modules, or NULL if failure.
     *
     */
    SCTModuleList* siCommonGetModules( void* context );

    /*!
     * Sleep specified amount of time.
     *
     * \param context Pointer to common system interface context, or NULL
     * \param micros Microseconds to sleep
     *
     */
    void siCommonSleep( void* context, unsigned long micros );

    /*!
     * Yield for other processes for example in long running loops.
     *
     * \param context Pointer to common system interface context, or NULL
     */
    void siCommonYield( void* context );

    /*!
     * Get time. Time string is returned in yyyy-mm-dd hh:mm:ss
     * format.
     *
     * \param context Pointer to common system interface context, or NULL.
     * \param buffer Time string buffer.
     * \param length Length of the time string buffer.
     */
    void siCommonGetTime( void* context, char* buffer, int length );

    /*!
     * Get the address of a named function.
     *
     * \param context Pointer to common system interface context, or NULL.
     * \param procname Function name. Must be defined.
     *
     * \return Address to function, or null if no such function exists.
     */
    SCTExtensionProc siCommonGetProcAddress( void* context, const char* procname );

    /*!
     * Get pending message. Implement to return -1 in platforms not supporting
     * messages.
     *
     * \param context       Pointer to common system interface context, or NULL.
     * \param message       Pointer to a struct used to store the message. Must be defined.
     * \param timeoutMillis Timeout for wait, in milliseconds. Use infinite timeout if zero.
     *
     * \return Return 1 when message received ok, 0 if timeout occurred, -1 if
     * message receive failed. Message is not modified in case of timeout or failure.
     */
    int siCommonGetMessage( void* context, SCTMessage* message, long timeoutMillis );

#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif /* !defined( __SCT_SISYSTEM_H__ ) */
