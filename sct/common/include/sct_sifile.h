/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_SIFILE_H__ )
#define __SCT_SIFILE_H__

#include "sct_types.h"

typedef void*   SCTFile;

#if defined( __cplusplus )
extern "C" {
#endif /* defined( __cplusplus ) */

    /* NOTE: File system interface should follow the convention of C stdlib file
     * functions where applicable. */
    
    /*!
     * Create file system interface context. 
     *
     * \return file system interface context, or NULL if failure.
     *
     */
    void*               siFileCreateContext();

    /*!
     * Destroy file sustem interface context. 
     *
     * \param context   File system interface context. Ignored if NULL.
     *
     */
    void                siFileDestroyContext( void* context );

    /*!
     * Open file. Parameters follow the convention of C stdlib fopen.
     *
     * \param context   File system interface context. Must be defined.
     * \param name      File name. Must be defined.
     * \param mode      File mode. Must be defined. Follows the convention of C stdlib fopen.
     *
     * \return opened SCTFile, or NULL if failure.
     */    
    SCTFile             siFileOpen( void* context, const char* name, const char* mode );

    /*!
     * Close file.
     *
     * \param context   File system interface context. Must be defined.
     * \param file      Opened file. Must be defined.
     *
     */
    void                siFileClose( void* context, SCTFile file );

    /*!
     * Read data from the opened file.
     *
     * \param context   File system interface context. Must be defined.
     * \param file      Opened file. Must be defined.
     * \param data      Buffer for read data. Must be defined.
     * \param length    Length of the buffer.
     *
     * \return return number of bytes read. 
     */
    unsigned int        siFileRead( void* context, SCTFile file, char* data, unsigned int length );

    /*!
     * Read a single byte from the opened file.
     *
     * \param context   File system interface context. Can be NULL.
     * \param file      Opened file. Must be defined.
     *
     * \return read byte, or EOF if failure.
     */
    
    int                 siFileReadByte( void* context, SCTFile file );

    /*!
     * Write data into an opened file.
     *
     * \param context   File system interface context. Must be defined.
     * \param file      Opened file. Must be defined.
     * \param data      Data to be written. Must be defined.
     * \param length    Length of the data.
     *
     * \return return number of bytes written.      
     */
    unsigned int        siFileWrite( void* context, SCTFile file, const char* data, unsigned int length );

    /*!
     * Check if the file has reached end of file.
     *
     * \param context   File system interface context. Must be defined.
     * \param file      Opened file. Must be defined.
     *
     * \return SCT_TRUE if the file has reached end of file, SCT_FALSE otherwise.   
     */    
    SCTBoolean          siFileEOF( void* context, SCTFile file );

    /*!
     * Get file size in bytes.
     *
     * \param context   File system interface context. Must be defined.
     * \param file      Opened file. Must be defined.
     *
     * \return number of bytes in the file.   
     */
    int                 siFileSize( void* context, SCTFile file );

    /*!
     * Create directory. Directory name can contain / or \ characters
     * as directory separator. Directory name must end with directory
     * separator if it points to a directory. If the name points to a
     * file, directories will be created up to the directory in which
     * the file is located.
     *
     * \param context    File system interface context. Must be defined.
     * \param name       Path name. Must be defined.
     * 
     * \return SCT_TRUE if success, SCT_FALSE if failure.
     */
    SCTBoolean          siFileMkdir( void* context, const char* name );

#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif /* !defined( __SCT_SIFILE_H__ ) */
