/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_SIMEMORY_H__ )
#define __SCT_SIMEMORY_H__

#if defined( __cplusplus )
extern "C" {
#endif /* defined( __cplusplus ) */

    /*!
     * Fills blocks of memory with a zero value, using the most efficient
     * memory fill function in the system.
     *
     * \param buffers Memory buffers to fill.
     * \param bufferCount Number of buffers.
     * \param lengthInBytes Length of the memory buffer, in bytes.
     * \param iterations Number of times to repeat the block fill.
     *
     * \return Non-zero if success, zero if failure.
     *
     */
    int siMemoryBlockFillZ( void** buffers, const int bufferCount, const int lengthInBytes, int iterations );


    /*!
     * Copies blocks of memory to another memory buffers, using the most
     * efficient memory copy function in the system.
     *
     * \param destination Destination memory buffers.
     * \param source Source memory buffers.
     * \param bufferCount Number of buffers.
     * \param lengthInBytes Length of the memory buffers, in bytes.
     * \param iterations Number of times to repeat the block copy.
     *
     * \return Non-zero if success, zero if failure.
     */
    int siMemoryBlockCopy( void** destinations, void** sources, const int bufferCount, const int lengthInBytes, int iterations );

#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif /* !defined( __SCT_SIMEMORY_H__ ) */
