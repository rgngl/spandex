/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_H__ )
#define __SCT_H__

#include "sct_version.h" 
#define SCT_VERSION_STRING SCT_VERSION " " SCT_RELEASE_DESCRIPTION

/*! The exit status of sctCycle. */
typedef enum
{
    /*! The benchmarking has not been completed yet. sctCycle should be called
     *  again. */
    SCT_CYCLE_CONTINUE      = 0,
    /*! The benchmarks have been rendered and sctCycle should not be called
     *  anymore. */
    SCT_CYCLE_FINISHED      = 1,
    /*! Benchmark error occured during the last cycle. sctCycle can still be
     *  called. */
    SCT_CYCLE_ERROR         = 2,
    /*! Fatal error occured during the last cycle. sctCycle should not be called
     *  anymore. */
    SCT_CYCLE_FATAL_ERROR   = 3
} SCTCycleStatus;

#if defined( __cplusplus )
extern "C" {
#endif /* defined( __cplusplus ) */

    /* Public functions. */
    void*          sctInitialize( void );
    SCTCycleStatus sctCycle     ( void* context );
    void           sctTerminate ( void* content );

#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif /* !defined( __SCT_H__ ) */
