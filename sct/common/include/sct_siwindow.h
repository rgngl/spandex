/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_SIWINDOW_H__ )
#define __SCT_SIWINDOW_H__

#include "sct_types.h"

/*!
 * Spandex color formats.
 *
 */
typedef enum
{
    SCT_COLOR_FORMAT_INVALID,
    SCT_COLOR_FORMAT_DEFAULT,
    SCT_COLOR_FORMAT_BW1,
    SCT_COLOR_FORMAT_L8,
    SCT_COLOR_FORMAT_A8,    
    SCT_COLOR_FORMAT_LA88,    
    SCT_COLOR_FORMAT_RGBA5551,
    SCT_COLOR_FORMAT_RGBA4444,
    SCT_COLOR_FORMAT_RGB565,
    SCT_COLOR_FORMAT_RGB888,
    SCT_COLOR_FORMAT_RGBX8888, 
    SCT_COLOR_FORMAT_RGBA8888,
    SCT_COLOR_FORMAT_BGRA8888,
    SCT_COLOR_FORMAT_RGBA8888_PRE,
    SCT_COLOR_FORMAT_YUV12,
} SCTColorFormat;

/*!
 *
 *
 */
typedef enum
{
    SCT_USAGE_UNDEFINED              = 0x0000,
    SCT_USAGE_OPENVG_IMAGE           = 0x0001,
    SCT_USAGE_OPENGLES1_TEXTURE_2D   = 0x0002,
    SCT_USAGE_OPENGLES2_TEXTURE_2D   = 0x0004,
    SCT_USAGE_OPENGLES1_RENDERBUFFER = 0x0100,
    SCT_USAGE_OPENGLES2_RENDERBUFFER = 0x0200,
    SCT_USAGE_OPENVG_SURFACE         = 0x0010,
    SCT_USAGE_OPENGLES1_SURFACE      = 0x0020,
    SCT_USAGE_OPENGLES2_SURFACE      = 0x0040,
    SCT_USAGE_OPENGL_SURFACE         = 0x0080
} SCTUsage;

/*!
 *
 *
 */
typedef enum
{
    SCT_SCREEN_ORIENTATION_INVALID,
    SCT_SCREEN_ORIENTATION_DEFAULT,    
    SCT_SCREEN_ORIENTATION_PORTRAIT,
    SCT_SCREEN_ORIENTATION_LANDSCAPE
} SCTScreenOrientation;

/*!
 *
 *
 */
typedef enum
{
    SCT_SCREEN_INVALID,
    SCT_SCREEN_DEFAULT,
    SCT_SCREEN_HDMI,
} SCTScreen;

#if defined( __cplusplus )
extern "C" {
#endif /* defined( __cplusplus ) */

    /*!
     * Create window system context.
     *
     * \return Pointer to the window system context.
     */
    void*               siWindowCreateContext( void );

    /*!
     * Destroy window system context.
     *
     * \param context   Pointer to window system interface context. Ignored if NULL.
     */    
    void                siWindowDestroyContext( void* context );

    /*!
     * Check if a particular screen is supported by the window system.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param screen    Screen.
     *
     * \return SCT_TRUE if the screen is supported, SCT_FALSE if not.
     */       
    SCTBoolean          siWindowIsScreenSupported( void* context, SCTScreen screen );

    /*!
     * Get screen size.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param screen    Screen. Must be supported by the window system.
     * \param width     Pointer to variable to store screen width. Must be defined.
     * \param height    Pointer to variable to store screen height. Must be defined.     
     *
     */       
    void                siWindowGetScreenSize( void* context, SCTScreen screen, int* width, int* height );

    /*!
     * Get the EGL display ID for the screen. The display ID will be used by
     * eglGetDisplay to get the matching EGL display.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param screen    Screen. Must be supported by the window system.
     *
     * \return display ID compatible with eglGetDisplay.
     */          
    void*               siWindowGetScreenEglDisplayId( void* context, SCTScreen screen );

    /*!
     * Set screen orientation.
     *
     * \param context       Pointer to window system interface context. Must be defined.
     * \param screen        Screen. Must be supported by the window system.
     * \param orientation   Screen orientation.     
     *
     * \return SCT_TRUE if success, SCT_FALSE if failure.
     */              
    SCTBoolean          siWindowSetScreenOrientation( void* context, SCTScreen screen, SCTScreenOrientation orientation );

    /*!
     * Create a window. The window will be created into matching screen, with
     * matching dimensions, format, and a parent window.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param screen    Screen. Must be supported by the window system.
     * \param x         Window X location.
     * \param y         Window Y location.
     * \param width     Window width.
     * \param heigth    Window heigth.
     * \param format    Window color format.
     * \param parent    Window parent window. NULL means no parent window.
     *
     * \return newly created window, or NULL if failure.
     */                 
    void*               siWindowCreateWindow( void* context, SCTScreen screen, int x, int y, int width, int height, SCTColorFormat format, void* parent );

    /*!
     * Destroy window.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param window    Pointer to window. Must be defined.
     *
     */              
    void                siWindowDestroyWindow( void* context, void* window );

    /*!
     * Get an EGL window from the window. The EGL window is compatible with
     * eglCreateWindowSurface.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param window    Pointer to window. Must be defined.
     *
     * \return EGL window that is compatible with eglCreateWindowSurface.     
     */                  
    void*               siWindowGetEglWindow( void* context, void* window );

    /*!
     * Get the screen in which the window is currently presented.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param window    Pointer to window. Must be defined.
     *
     * \return screen in which the window is currently presented.     
     */                  
    SCTScreen           siWindowGetWindowScreen( void* context, void* window );

    /*!
     * Bring the window to front.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param window    Pointer to window. Must be defined.
     *
     * \return SCT_TRUE if success, SCT_FALSE if failure.     
     */                     
    SCTBoolean          siWindowToFront( void* context, void* window );

    /*!
     * Resize window.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param window    Pointer to window. Must be defined.
     * \param x         New window X location.
     * \param y         New Window Y location.
     * \param width     New window width.
     * \param heigth    New window heigth.
     *
     * \return SCT_TRUE if success, SCT_FALSE if failure.     
     */                         
    SCTBoolean          siWindowResize( void* context, void* window, int x, int y, int width, int height );

    /*!
     * Set window opacity. Default opaque value for a window should be 1.0, if
     * feasible. Window opacity is used when doing the final window composition
     * to the screen.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param window    Pointer to window. Must be defined.
     * \param opacity   Window opacity. 1.0 means completely opaque, 0.0 means completely transparent.
     *
     * \return SCT_TRUE if success, SCT_FALSE if failure.     
     */                         
    SCTBoolean          siWindowSetOpacity( void* context, void* window, float opacity );

    /*!
     * Enable/disable window per-pixel alpha. When enabled, the per-pixel alpha
     * taken from window surface alpha channel is used when doing the final
     * window composition to the screen. By default, per-pixel alpha should be
     * disabled, if feasible.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param window    Pointer to window. Must be defined.
     * \param enable    Enable/disable per-pixel alpha.
     *
     * \return SCT_TRUE if success, SCT_FALSE if failure.     
     */                         
    SCTBoolean          siWindowSetAlpha( void* context, void* window, SCTBoolean enable );
    
    /*!
     * Refresh window, might for example cause a redraw event.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param window    Pointer to window. Must be defined.
     *
     * \return SCT_TRUE if success, SCT_FALSE if failure.     
     */                             
    SCTBoolean          siWindowRefresh( void* context, void* window );

    /*!
     * Create a pixmap. Initial pixmap contents is taken from data, if
     * defined. Otherwise, initial pixmap contents is undefined. Note that
     * implementation might not support initial data, in which case NULL is
     * always returned if data is not NULL.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param width     Pixmap width. Must be >= 0.
     * \param heigth    Pixmap heigth. Must be >= 0.
     * \param format    Pixmap color format.
     * \param data      Pixmap data. Initial pixmap contents should be taken verbatim from data,
     *                  if defined; i.e. the caller must ensure the data is in correct format for
     *                  particular platform. Data points to lower-left pixel.
     * \param data      Pixmap data length. Must be >= 0 if data is defined. Pixel data length must
     *                  match the pixmap size and format.
     *
     * \return newly created pixmap, or NULL if failure. 
     */                                 
    void*               siWindowCreatePixmap( void* context, int width, int height, SCTColorFormat format, void* data, int length );

    /*!
     * Create a shared pixmap; shared pixmaps are like SgImages in Symbian or
     * native buffers in some unix systems. Initial pixmap contents is taken
     * from data, if defined. Otherwise, the pixmap contents is undefined. Note
     * that implementation might not support initial data, in which case NULL is
     * always returned if data is not NULL.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param width     Pixmap width. Must be >= 0.
     * \param heigth    Pixmap heigth. Must be >= 0.
     * \param format    Pixmap color format.
     * \param usage     Pixmap usage mask, combination of SCTUsage flags.
     * \param data      Pixmap data. Initial pixmap contents should be taken verbatim from data,
     *                  if defined; i.e. the caller must ensure the data is in correct format for
     *                  particular platform.
     * \param data      Pixmap data length. Must be >= 0 if data is defined. Pixel data length must
     *                  match the pixmap size and format.
     *
     * \return newly created shared pixmap, or NULL if failure.
     */                                 
    void*               siWindowCreateSharedPixmap( void* context, int width, int height, SCTColorFormat format, int usage, void* data, int length );

    /*!
     * Set data for pixmap/shared pixmap. Note that some implementations might not support setting
     *                  pixmap data, in which case SCT_FALSE is returned.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param pixmap    Pointer to pixmap. Must be defined.
     * \param data      Pixmap data. The caller must ensure the data is in correct format for
     *                  the particular pixmap, i.e. no data conversion is done.
     * \param data      Pixmap data length. Must be >= 0 and must match the pixmap size and format.
     *
     * \return SCT_TRUE if success, SCT_FALSE if failure.
     */                                 
    SCTBoolean         siWindowSetPixmapData( void* context, void* pixmap, void* data, int length );

    /*!
     * Get an EGL pixmap from pixmap/shared pixmap. The EGL pixmap is compatible
     * with eglCreatePixmapSurface. Output variable target defines the
     * platform specific target for eglCreateImageKhr.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param window    Pointer to pixmap. Must be defined.
     * \param target    Pointer to variable to store a platform spefici pixmap target. Ignored if NULL.
     *
     * \return EGL pixmap, or NULL if failure.     
     */                                     
    void*               siWindowGetEglPixmap( void* context, void* pixmap, unsigned int* target );

    /*!
     * Blit the pixmap/shared pixmap to the window.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param window    Window into which the bitmap should be blitted. Must be defined.
     * \param pixmap    Pixmap. Must be defined.
     * \param x         Blit X location.
     * \param y         Blit Y location.          
     *
     * \return SCT_TRUE if success, SCT_FALSE if failure.
     */                                         
    SCTBoolean          siWindowBlitPixmap( void* context, void* window, void* pixmap, int x, int y );

    /*!
     * Destroy pixmap/shared pixmap.
     *
     * \param context   Pointer to window system interface context. Must be defined.
     * \param pixmap    Pixmap. Must be defined.
     *
     */                                             
    void                siWindowDestroyPixmap( void* context, void* pixmap );

#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif /* !defined( __SCT_SIWINDOW_H__ ) */
