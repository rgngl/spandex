/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_EGLWRAPPER_H__ )
#define __SCT_EGLWRAPPER_H__

#if defined( SCT_KHR_COMPLIANT_INCLUDES )
# include <EGL/egl.h>
#else
# include <egl.h>
#endif

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                   sctCreateEglWrapperContext();
    void                    sctDestroyEglSwapperContext( void* context );
    
    EGLConfig               createEGLConfig28( void*  context,
                                               EGLint config_id,
                                               EGLint buffer_size,
                                               EGLint red_size,
                                               EGLint green_size,
                                               EGLint blue_size,
                                               EGLint alpha_size,
                                               EGLint bind_to_texture_rgb,
                                               EGLint bind_to_texture_rgba,
                                               EGLint config_caveat,
                                               EGLint depth_size,
                                               EGLint level,
                                               EGLint max_swap_interval,
                                               EGLint min_swap_interval,
                                               EGLint native_renderable,
                                               EGLint sample_buffers,
                                               EGLint samples,
                                               EGLint stencil_size,
                                               EGLint surface_type,
                                               EGLint transparent_type,
                                               EGLint transparent_red,
                                               EGLint transparent_green,
                                               EGLint transparent_blue,
                                               EGLint luminance_size,
                                               EGLint alpha_mask_size,
                                               EGLint color_buffer_type,
                                               EGLint conformant,
                                               EGLint renderable_type );
    void                    destroyEGLConfig2( void* context, EGLConfig config );
    NativeDisplayType       createEGLNativeDisplayType1( void* context );
    void                    destroyEGLNativeDisplayType2( void* context, NativeDisplayType displayType );
    void*                   createEGLNativeWindowType1( void* context );    
    void*                   createEGLNativeWindowType6( void* context, int x, int y, int width, int height, int mode );
    void                    destroyEGLNativeWindowType2( void* context, void* window );
    NativeWindowType        modifyEGLNativeWindowType7( void* context,
                                                        NativeWindowType nativeWindow,
                                                        int x,
                                                        int y,
                                                        int width,
                                                        int height,
                                                        int mode );

    void*                   createEGLNativePixmapType4( void* context, int width, int height, int mode );
    void*                   modifyEGLNativePixmapType5( void* context, void* pixmap, int width, int height, int mode );
    void                    destroyEGLNativePixmapType2( void* context, void* pixmap );
    
    /* Hacks for broken traces; calling OpenVG functions without active
     * context. */
    int         createVGImage1( void* context );
    void        destroyVGImage2( void* context, int image );
    int         createVGPath1( void* context );
    void        destroyVGPath2( void* context, int path );
    int         createVGPaint1( void* context );
    void        destroyVGPaint2( void* context, int paint );
    int         createGLframebuffer1( void* context );
    void        destroyGLframebuffer2( void* context, int fb );
    int         createGLrenderbuffer1( void* context );
    void        destroyGLrenderbuffer2( void* context, int fb );
    EGLConfig   modifyEGLConfig13( void* context, EGLConfig config, EGLint config_id, EGLint buffer_size, EGLint red_size, EGLint green_size, EGLint blue_size, EGLint alpha_size, EGLint bind_to_texture_rgb, EGLint bind_to_texture_rgba, EGLint config_caveat, EGLint depth_size, EGLint level );
    EGLConfig   modifyEGLConfig29( void* context, EGLConfig config, EGLint config_id, EGLint buffer_size, EGLint red_size, EGLint green_size, EGLint blue_size, EGLint alpha_size, EGLint bind_to_texture_rgb, EGLint bind_to_texture_rgba, EGLint config_caveat, EGLint depth_size, EGLint level, EGLint max_swap_interval, EGLint min_swap_interval, EGLint native_renderable, EGLint sample_buffers, EGLint samples, EGLint stencil_size, EGLint surface_type, EGLint transparent_type, EGLint transparent_red, EGLint transparent_green, EGLint transparent_blue, EGLint luminance_size, EGLint alpha_mask_size, EGLint color_buffer_type, EGLint conformant, EGLint renderable_type );
    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */
    
#endif /* !defined( __SCT_EGLWRAPPER_H__ ) */
