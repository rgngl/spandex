/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_UTILS_H__ )
#define __SCT_UTILS_H__

#if defined( _MSC_VER )
# pragma warning(disable:4514)
#endif  /* defined( _MSC_VER ) */

#include "sct_types.h"

#include <stddef.h>

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    /* Get spandex core modules. */      
    SCTModuleList*      sctGetCoreModules();
    
    /* Attribute functions. */
    SCTAttribute*       sctCreateAttribute( const char* name, const char* value );
    void                sctDestroyAttribute( SCTAttribute* attribute );

    /* List handling functions */
    SCTList*            sctCreateList();
    SCTBoolean          sctAddToList( SCTList* list, void* element );
    SCTBoolean          sctRemoveFromList( SCTList* list, void* element );
    void                sctGetListIterator( SCTList* list, SCTListIterator* iterator );
    void                sctGetReverseListIterator( SCTList* list, SCTReverseListIterator* iterator );
    void*               sctGetNextListElement( SCTListIterator* listIterator );
    void*               sctGetPrevListElement( SCTReverseListIterator* listIterator );
    int                 sctCountListElements( SCTList* list );
    void                sctDestroyListElements( SCTList* list );
    void                sctDestroyList( SCTList* list );

    /* Attribute list handling functions */
    SCTAttributeList*   sctCreateAttributeList();
    SCTBoolean          sctAddAttributeToList( SCTAttributeList* list, SCTAttribute* attribute );
    SCTBoolean          sctAddNameValueToList( SCTAttributeList* list, const char* name, const char* value );
    void                sctGetAttributeListIterator( const SCTAttributeList* list, SCTAttributeListIterator* iterator );
    SCTAttribute*       sctGetNextAttributeListElement( SCTAttributeListIterator* iterator );
    void                sctDestroyAttributeList( SCTAttributeList* list );

    /* Attribute access functions. */
    SCTAttribute*       sctFindAttribute( SCTAttributeList* attributes, const char* name );
    SCTBoolean          sctHasAttribute( SCTAttributeList* attributes, const char* attributeName );
    SCTBoolean          sctGetAttributeAsInt( SCTAttributeList* attributes, const char* attributeName, int* value );
    SCTBoolean          sctGetAttributeAsHex( SCTAttributeList* attributes, const char* attributeName, unsigned int* value );
    SCTBoolean          sctGetAttributeAsFloat( SCTAttributeList* attributes, const char* attributeName, float* value );
    SCTBoolean          sctGetAttributeAsString( SCTAttributeList* attributes, const char* attributeName, char* buffer, int count );
    SCTBoolean          sctGetAttributeAsXY( SCTAttributeList* attributes, const char* attributeName, int* xvalue, int* yvalue );
    SCTBoolean          sctGetAttributeAsIntArray( SCTAttributeList* attributes, const char* attributeName, int* values, int count );
    SCTBoolean          sctGetAttributeAsIntVector( SCTAttributeList* attributes, const char* attributeName, SCTIntVector** vector );
    SCTBoolean          sctGetAttributeAsFloatArray( SCTAttributeList* attributes, const char* attributeName, float* values, int count );
    SCTBoolean          sctGetAttributeAsFloatVector( SCTAttributeList* attributes, const char* attributeName, SCTFloatVector** vector );
    SCTBoolean          sctGetAttributeAsDoubleArray( SCTAttributeList* attributes, const char* attributeName, double* values, int count );
    SCTBoolean          sctGetAttributeAsDoubleVector( SCTAttributeList* attributes, const char* attributeName, SCTDoubleVector** vector );
    SCTBoolean          sctGetAttributeAsHexArray( SCTAttributeList* attributes, const char* attributeName, unsigned int* values, int count );
    SCTBoolean          sctGetAttributeAsHexVector( SCTAttributeList* attributes, const char* attributeName, SCTHexVector** vector );
    SCTBoolean          sctGetAttributeAsStringList( SCTAttributeList* attributes, const char* attributeName, SCTStringList** stringList );
    SCTBoolean          sctGetAttributeAsCInt( SCTAttributeList* attributes, const char* attributeName, SCTConditionalInt* cint );

    /* String list handling functions. */
    SCTStringList*      sctCreateStringList();
    SCTBoolean          sctAddStringToList( SCTStringList* list, char* string );
    SCTBoolean          sctAddStringCopyToList( SCTStringList* list, const char* string );
    void                sctGetStringListIterator( const SCTStringList* list, SCTStringListIterator* iterator );
    char*               sctGetNextStringListElement( SCTStringListIterator* iterator );
    char*               sctGetStringAtIndex( SCTStringList* list, int index );
    void                sctDestroyStringListElements( SCTStringList* list );
    void                sctDestroyStringList( SCTStringList* list );

    /* String processing functions. */
    char*               sctDuplicateString( const char* string );
    SCTStringList*      sctSplitString( const char* string, int len, char sep );

    /* Vector functions. */
    SCTIntVector*       sctCreateIntVector( int length );
    SCTFloatVector*     sctCreateFloatVector( int length ); 
    SCTDoubleVector*    sctCreateDoubleVector( int length );
    SCTHexVector*       sctCreateHexVector( int length );
    void                sctDestroyIntVector( SCTIntVector* vector );
    void                sctDestroyFloatVector( SCTFloatVector* vector );
    void                sctDestroyDoubleVector( SCTDoubleVector* vector );
    void                sctDestroyHexVector( SCTHexVector* vector );

    /* Log functions */
    SCTLog*             sctCreateLog( void );
    void                sctLogError( const char* message );
    void                sctLogErrorFileLine( const char* message, const char* file, unsigned int line );
    SCTBoolean          sctLogWarning( const char* message );
    int                 sctCountWarnings( SCTLog* log );
    void                sctResetLog( SCTLog* log );
    void                sctDestroyLog( SCTLog* log );
    SCTLog*             sctGetErrorLog( void );
    SCTBoolean          sctSetErrorLog( SCTLog* log );

    /* Generic utility functions. */
    unsigned long       sctGetMinTicks( void );
    SCTBoolean          sctStrncat( char* destination, const char* source, int count );
    SCTBoolean          sctStrncopy( char* destination, const char* source, int count );
    float               sctAtof( const char* str );
    int                 sctMapStringToBoolean( const char* str );
    SCTBoolean          sctFormatSize_t( char* buf, int length, size_t v );
    
    /* Pointer registry functions. */
    SCTBoolean          sctRegisterPointer( SCTPointerID id, void* pointer );
    void                sctUnregisterPointer( SCTPointerID id );
    void*               sctGetRegisteredPointer( SCTPointerID id, SCTBoolean* pointerRegistered );
    void                sctCleanupRegistry( void );

    /* */
    SCTBoolean          sctReportSection( const char* name, const char* type, SCTAttributeList* attributes );
    SCTBoolean          sctReportComment( const char* commentString );

    /* */
    int                 sctSignificantDigitsUINT( unsigned int value );
    void                sctJoinStrings( char* str1, const char* str2, const char* sep );

    /* */
    void*               sctCreateMerseneTwisterContext();
    void                sctDestroyMerseneTwisterContext( void* context );
    double              sctMerseneTwister( int* mtIndex, void* context );
        
    /* Inline helper functions */

    /*!
     * Computes the minimum of two integers
     *
     * \param a	First integer.
     * \param b Second integer.
     *
     * \return Minimum of a and b.
     */
    SCT_INLINE static int sctMin( int a, int b )
    {
        return ( a < b ) ? a : b;
    }

   /*!
    * Computes the maximum of two intergers
    *
    * \param a	First integer.
    * \param b Second integer.
    *
    * \return Maximum of a and b.
    */
    SCT_INLINE static int sctMax( int a, int b )
    {
        return ( a > b ) ? a : b;
    }

    /*!
     * Computes the minimum of two floats
     *
     * \param a	First float.
     * \param b Second float.
     *
     * \return Minimum of a and b.
     */
    SCT_INLINE static float sctMinf( float a, float b )
    {
        return ( a < b ) ? a : b;
    }

   /*!
    * Computes the maximum of two floats
    *
    * \param a	First float.
    * \param b Second float.
    *
    * \return Maximum of a and b.
    */
    SCT_INLINE static float sctMaxf( float a, float b )
    {
        return ( a > b ) ? a : b;
    }

   /*!
    * Returns the absolute value of an integer
    *
    * \param v Integer.
    *
    * \return Absolute value of v.
    */
    SCT_INLINE static int sctAbs( int v )
    {
        return ( v >= 0  ) ? v : -v;
    }

   /*!
    * Returns the absolute value of a float
    *
    * \param v Float.
    *
    * \return Absolute value of v.
    */
    SCT_INLINE static float sctAbsf( float v )
    {
        return ( v >= 0.0f  ) ? v : -v;
    }

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_UTILS_H__ ) */
