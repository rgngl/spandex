/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_BENCHMARK_H__ )
#define __SCT_BENCHMARK_H__

#include "sct_types.h"

#if defined( __cplusplus )
extern "C" {
#endif /* defined( __cplusplus ) */

    /* Benchmark functions. */
    SCTBenchmark*               sctCreateBenchmark( const char*       name, 
                                                    SCTAttributeList* attributes,
                                                    int               threadCount,
                                                    SCTActionList*    initActions[],
                                                    SCTActionList*    benchmarkActions[],
                                                    SCTActionList*    finalActions[],
                                                    SCTActionList*    preTerminateActions[],
                                                    SCTActionList*    terminateActions[] );

    SCTBoolean                  sctBenchmarkCanComplete( SCTBenchmark* benchmark );    
    void                        sctBenchmarkComplete( SCTBenchmark* benchmark );
    void                        sctDestroyBenchmark( SCTBenchmark* benchmark );
    
    /* Benchmark list functions. */
    SCTBenchmarkList*           sctCreateBenchmarkList();
    SCTBoolean                  sctAddBenchmarkToList( SCTBenchmarkList* list, SCTBenchmark* benchmark );
    void                        sctGetBenchmarkListIterator( SCTBenchmarkList* list, SCTBenchmarkListIterator* iterator );
    SCTBenchmark*               sctGetNextBenchmarkListElement( SCTBenchmarkListIterator* listIterator );
    void                        sctDestroyBenchmarkList( SCTBenchmarkList* list );

#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif /* !defined( __SCT_BENCHMARK_H__ ) */
