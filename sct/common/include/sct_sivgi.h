/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_SIVGI_H__ )
#define __SCT_SIVGI_H__

#include "sct_types.h"
#include <vg/openvg.h>
#include <vg/vgcontext.h>

/*!
 *
 *
 */
typedef struct
{
    VGIColorBufferFormat        format;
    int                         width;
    int                         height;
    void*                       bitmap;
    unsigned char*              bitmapData;
    int                         bitmapStride;
    void*                       mask;
    unsigned char*              maskData;
    int                         maskStride;
} SCTVgiTarget;

#if defined( __cplusplus )
extern "C" {
#endif /* defined( __cplusplus ) */

    void*                       siVgiGetContext();
    void                        siVgiReleaseContext( void* context );

    SCTVgiTarget*               siVgiGetTarget( void* context,
                                                int width,
                                                int height,
                                                VGIColorBufferFormat targetFormat,
                                                SCTBoolean targetMask );
    SCTBoolean                  siVgiLockTarget( void* context, SCTVgiTarget* target );
    void                        siVgiUnlockTarget( void* context, SCTVgiTarget* target );
    void                        siVgiReleaseTarget( void* context, SCTVgiTarget* target );

    SCTBoolean                  siVgiBlitTarget( void* context, SCTVgiTarget* target );

#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif /* !defined( __SCT_SIVGI_H__ ) */
