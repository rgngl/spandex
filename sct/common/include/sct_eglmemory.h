/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_EGLMEMORY_H__ )
#define __SCT_EGLMEMORY_H__

#include <stddef.h>

/* Platform-independent support code for querying graphics memory using EGL
 * resource profile extension (EGL_NOK_resource_profiling). */

/*!
 *
 *
 */
struct _SCTEGLMemory
{
    size_t  totalGraphicsMemory;
    size_t  usedGraphicsMemory;
    size_t  usedPrivateGraphicsMemory;
    size_t  usedSharedGraphicsMemory;
};

typedef struct _SCTEGLMemory SCTEGLMemory;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*       sctCreateEglMemoryContext();
    void        sctDestroyEglMemoryContext( void* context );

    SCTBoolean  sctGetEglMemoryStatus( void* context, SCTEGLMemory* memory );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif  /* !defined( __SCT_EGLMEMORY_H__ ) */

