/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma warning (disable: 4305)
#pragma warning (disable: 4100)
#pragma warning (disable: 4505)
#pragma warning (disable: 4245)
#pragma warning (disable: 4090)

#include "sct_vgctrace$NAMEaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#include <openvg.h>
#include <vgu.h>

#include "sct_eglwrapper.h"
#include "sct_vgctrace_$NAME.inl"

/*!
 *
 */
void* sctiCreateVgCtrace$NAMEActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVgCtrace$NAMEActionContext*  context;

    SCT_USE_VARIABLE( moduleContext );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTVgCtrace$NAMEActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVgCtrace$NAMEActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in $NAME@VgCtrace context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVgCtrace$NAMEActionContext ) );

    if( sctiParseVgCtrace$NAMEActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVgCtrace$NAMEActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVgCtrace$NAMEActionContext( void* context )
{
    SCTVgCtrace$NAMEActionContext* c = ( SCTVgCtrace$NAMEActionContext* )( context );

    if( c == NULL )
    {
        return;
    }

    SCT_ASSERT_ALWAYS( c->wrapperContext == NULL );
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 */
SCTBoolean sctiVgCtrace$NAMEActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTVgCtrace$NAMEActionContext*  context;
    VgCtrace$NAMEActionData*        data;    
    int                             i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTVgCtrace$NAMEActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT_ALWAYS( context->wrapperContext == NULL );
    
    context->wrapperContext = sctCreateEglWrapperContext();
    if( context->wrapperContext == NULL )
    {
        SCT_LOG_ERROR( "Creating wrapper context failed in $NAME@VgCtrace action init." );
        return SCT_FALSE;
    }
    
    for( i = 0; i < data->header; ++i )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: vgctrace init, playing frame %d", i );
#endif  /* defined( SCT_DEBUG ) */
        
        playFrame( context->wrapperContext, i );

#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: vgctrace init, frame %d done", i );
#endif  /* defined( SCT_DEBUG ) */       
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiVgCtrace$NAMEActionExecute( SCTAction* action, int frameNumber )
{
    SCTVgCtrace$NAMEActionContext*  context;
    VgCtrace$NAMEActionData*        data;
    VGErrorCode                     err;
    int                             f;
   
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTVgCtrace$NAMEActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT_ALWAYS( context->wrapperContext != NULL );
    
    f = data->header + frameNumber;
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: vgctrace execute, playing frame %d", f );
#endif  /* defined( SCT_DEBUG ) */
    
    playFrame( context->wrapperContext, f );
    context->lastFrame = f;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: vgctrace execute, frame %d done", f );
#endif  /* defined( SCT_DEBUG ) */
    
#if defined( SCT_VGCTRACE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 128 ];
        sprintf( buf, "VG error 0x%x in $NAME@VgCtrace action execute.", err );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
#else   /* defined( SCT_VGCTRACE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_VGCTRACE_CHECK_ERRORS ) */
    
    return SCT_TRUE;
}

/*!
 *
 */
void sctiVgCtrace$NAMEActionTerminate( SCTAction* action )
{
    SCTVgCtrace$NAMEActionContext*  context;
    VgCtrace$NAMEActionData*        data;    
    int                             i;
    int                             f;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTVgCtrace$NAMEActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT_ALWAYS( context->wrapperContext != NULL );
    
    for( i = 1; i <= data->trailer; ++i )
    {
        f = context->lastFrame + i;
        
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: vgctrace terminate, playing frame %d", f );
#endif  /* defined( SCT_DEBUG ) */
        
        playFrame( context->wrapperContext, f );
        
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: vgctrace terminate, frame %d done", f );
#endif  /* defined( SCT_DEBUG ) */
        
    }

    sctDestroyEglSwapperContext( context->wrapperContext );
    context->wrapperContext = NULL;
}

/*!
 *
 *
 */
void sctiVgCtrace$NAMEActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyVgCtrace$NAMEActionContext( ( SCTVgCtrace$NAMEActionContext* )( action->context ) );
    action->context = NULL;
}

