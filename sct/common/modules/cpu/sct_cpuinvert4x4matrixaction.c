/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_cpumodule.h"
#include "sct_cpuinvert4x4matrixaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#include "sct_cpumodule_parser.h"

/*!
 *
 *
 */
static void Invert4x4Matrix( const float m[ 16 ], float out[ 16 ] )
{
    float   inv[ 16 ];
    float   det;
    int     i;

    inv[0]  = m[5]*m[10]*m[15] - m[5]*m[11]*m[14] - m[9]*m[6]*m[15] + m[9]*m[7]*m[14] + m[13]*m[6]*m[11] - m[13]*m[7]*m[10];
    inv[4]  = -m[4]*m[10]*m[15] + m[4]*m[11]*m[14] + m[8]*m[6]*m[15] - m[8]*m[7]*m[14] - m[12]*m[6]*m[11] + m[12]*m[7]*m[10];
    inv[8]  = m[4]*m[9]*m[15] - m[4]*m[11]*m[13] - m[8]*m[5]*m[15] + m[8]*m[7]*m[13] + m[12]*m[5]*m[11] - m[12]*m[7]*m[9];
    inv[12] = -m[4]*m[9]*m[14] + m[4]*m[10]*m[13] + m[8]*m[5]*m[14] - m[8]*m[6]*m[13] - m[12]*m[5]*m[10] + m[12]*m[6]*m[9];
    inv[1]  = -m[1]*m[10]*m[15] + m[1]*m[11]*m[14] + m[9]*m[2]*m[15] - m[9]*m[3]*m[14] - m[13]*m[2]*m[11] + m[13]*m[3]*m[10];
    inv[5]  = m[0]*m[10]*m[15] - m[0]*m[11]*m[14] - m[8]*m[2]*m[15] + m[8]*m[3]*m[14] + m[12]*m[2]*m[11] - m[12]*m[3]*m[10];
    inv[9]  = -m[0]*m[9]*m[15] + m[0]*m[11]*m[13] + m[8]*m[1]*m[15] - m[8]*m[3]*m[13] - m[12]*m[1]*m[11] + m[12]*m[3]*m[9];
    inv[13] = m[0]*m[9]*m[14] - m[0]*m[10]*m[13] - m[8]*m[1]*m[14] + m[8]*m[2]*m[13] + m[12]*m[1]*m[10] - m[12]*m[2]*m[9];
    inv[2]  = m[1]*m[6]*m[15] - m[1]*m[7]*m[14] - m[5]*m[2]*m[15] + m[5]*m[3]*m[14] + m[13]*m[2]*m[7] - m[13]*m[3]*m[6];
    inv[6]  = -m[0]*m[6]*m[15] + m[0]*m[7]*m[14] + m[4]*m[2]*m[15] - m[4]*m[3]*m[14] - m[12]*m[2]*m[7] + m[12]*m[3]*m[6];
    inv[10] = m[0]*m[5]*m[15] - m[0]*m[7]*m[13] - m[4]*m[1]*m[15] + m[4]*m[3]*m[13] + m[12]*m[1]*m[7] - m[12]*m[3]*m[5];
    inv[14] = -m[0]*m[5]*m[14] + m[0]*m[6]*m[13] + m[4]*m[1]*m[14] - m[4]*m[2]*m[13] - m[12]*m[1]*m[6] + m[12]*m[2]*m[5];
    inv[3]  = -m[1]*m[6]*m[11] + m[1]*m[7]*m[10] + m[5]*m[2]*m[11] - m[5]*m[3]*m[10] - m[9]*m[2]*m[7] + m[9]*m[3]*m[6];
    inv[7]  = m[0]*m[6]*m[11] - m[0]*m[7]*m[10] - m[4]*m[2]*m[11] + m[4]*m[3]*m[10] + m[8]*m[2]*m[7] - m[8]*m[3]*m[6];
    inv[11] = -m[0]*m[5]*m[11] + m[0]*m[7]*m[9] + m[4]*m[1]*m[11] - m[4]*m[3]*m[9] - m[8]*m[1]*m[7] + m[8]*m[3]*m[5];
    inv[15] = m[0]*m[5]*m[10] - m[0]*m[6]*m[9] - m[4]*m[1]*m[10] + m[4]*m[2]*m[9] + m[8]*m[1]*m[6] - m[8]*m[2]*m[5];
    
    det = 1.0f / ( m[0]*inv[0] + m[1]*inv[4] + m[2]*inv[8] + m[3]*inv[12] );

    for( i = 0; i < 16; i++ )
    {
        out[ i ] = inv[ i ] * det;
    }
}

/*!
 *
 *
 */
void* sctiCreateCPUInvert4x4MatrixActionContext( void *moduleContext, SCTAttributeList* attributes )
{
    SCTCPUInvert4x4MatrixActionContext* context;
    
    SCT_USE_VARIABLE( moduleContext == NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTCPUInvert4x4MatrixActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTCPUInvert4x4MatrixActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Invert4x4Matrix@CPU action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTCPUInvert4x4MatrixActionContext ) );

    /* Parse action attributes. */
    if( sctiParseCPUInvert4x4MatrixActionAttributes( &context->data, attributes ) == SCT_FALSE )
    {
        siCommonMemoryFree( NULL, context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyCPUInvert4x4MatrixActionContext( void* context )
{
    SCTCPUInvert4x4MatrixActionContext* c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTCPUInvert4x4MatrixActionContext* )( context );
    
    SCT_ASSERT_ALWAYS( c->inMatrices == NULL );
    SCT_ASSERT_ALWAYS( c->outMatrices == NULL );
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiCPUInvert4x4MatrixActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTCPUInvert4x4MatrixActionContext* context;
    int                                 count;
    int                                 i;
    float*                              p;

    SCT_USE_VARIABLE( benchmark );
    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTCPUInvert4x4MatrixActionContext* )( action->context );
    count   = context->data.count;
    
    if( context->inMatrices == NULL )
    {
        context->inMatrices = ( float* )( siCommonMemoryAlloc( NULL, sizeof( float ) * 16 * count ) );
    }

    if( context->outMatrices == NULL )
    {
        context->outMatrices = ( float* )( siCommonMemoryAlloc( NULL, sizeof( float ) * 16 * count ) );
    }

    if( context->inMatrices == NULL || context->outMatrices == NULL )
    {
        if( context->inMatrices != NULL )
        {
            siCommonMemoryFree( NULL, context->inMatrices );
            context->inMatrices = NULL;
        }

        if( context->outMatrices != NULL )
        {
            siCommonMemoryFree( NULL, context->outMatrices );
            context->outMatrices = NULL;
        }
                
        SCT_LOG_ERROR( "Allocation failed in Invert4x4Matrix@CPU action init." );
        return SCT_FALSE;
    }

    /* Initialize input matrices. */
    p = context->inMatrices;
    for( i = 0; i < count; ++i )
    {
        p[ 0 ]  = 2;
        p[ 5 ]  = 2;
        p[ 10 ] = 2;
        p[ 15 ] = 2;

        p += 16;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiCPUInvert4x4MatrixActionExecute( SCTAction* action, int frameNumber )
{
    SCTCPUInvert4x4MatrixActionContext* context;
    int                                 i;
    int                                 j;
    int                                 count;
    int                                 iterations;
    float*                              in;
    float*                              out;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTCPUInvert4x4MatrixActionContext* )( action->context );

    count      = context->data.count;
    iterations = context->data.iterations;

    SCT_ASSERT( context->inMatrices != NULL );
    SCT_ASSERT( context->outMatrices != NULL );    
    
    for( j = 0; j < iterations; ++j )
    {
        in  = context->inMatrices;
        out = context->outMatrices;
        
        for( i = 0; i < count; ++i )
        {
            Invert4x4Matrix( in, out );
            in  += 16;
            out += 16;
        }
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiCPUInvert4x4MatrixActionTerminate( SCTAction* action )
{
    SCTCPUInvert4x4MatrixActionContext* context;    

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTCPUInvert4x4MatrixActionContext* )( action->context );

    if( context->inMatrices != NULL )
    {
        siCommonMemoryFree( NULL, context->inMatrices );
        context->inMatrices = NULL;
    }

    if( context->outMatrices != NULL )
    {
        siCommonMemoryFree( NULL, context->outMatrices );
        context->outMatrices = NULL;
    }
}

/*!
 *
 *
 */
void sctiCPUInvert4x4MatrixActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyCPUInvert4x4MatrixActionContext( action->context );
    action->context = NULL;
}
