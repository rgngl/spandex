/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_cpumodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_cpumodule_parser.h"
#include "sct_cpumodule_actions.h"

#include <stdio.h>
#include <string.h>

/* Local function declarations. */
SCTAttributeList*       sctiCpuModuleInfo( SCTModule* module );
SCTAction*              sctiCpuModuleCreateAction( SCTModule* module,
                                                   const char* name,
                                                   const char* type,
                                                   SCTAttributeList* attributes );
void                    sctiCpuModuleDestroy( SCTModule* module );

/*!
 * Create CPU module data structure which defines the name of the
 * module, CPU module context, and sets up the module interface
 * functions.
 *
 * \return Module structure for CPU module, or NULL if failure.
 */
SCTModule* sctCreateCpuModule( void )
{
    SCTModule*                  module;

    /* Create module data structure. */
    module = sctCreateModule( "CPU",
                              NULL,
#if defined( _WIN32 )
                              _cpu_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiCpuModuleInfo,
                              sctiCpuModuleCreateAction,
                              sctiCpuModuleDestroy );

    return module;
}

/*!
 * Create attribute list containing Cpu module information. 
 *
 * \param module Cpu module. Must be defined.
 *
 * \return Cpu module information as attribute list, or NULL if
 * failure.
 */
SCTAttributeList* sctiCpuModuleInfo( SCTModule* module )
{
    SCTAttributeList*           attributes;

    SCT_ASSERT_ALWAYS( module != NULL );

    attributes = sctCreateAttributeList();

    return attributes;
}

/*!
 * Create cpu module action. 
 *
 * \param module Cpu module. Must be defined.
 * \param name Action name. Must be defined.
 * \param type Action type. Must be defined.
 * \param attributes List of action attributes. Must be defined.
 *
 * \return Action data structure, or NULL if failure.
 *
 */
SCTAction* sctiCpuModuleCreateAction( SCTModule* module,
                                      const char* name,
                                      const char* type,
                                      SCTAttributeList* attributes )
{
    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    return sctCreateActionFromTemplate( name,
                                        type,
                                        "CPU",
                                        NULL,
                                        attributes,
                                        CPUActionTemplates,
                                        SCT_ARRAY_LENGTH( CPUActionTemplates ) );
}

/*!
 * Destroy cpu module. This function does not deallocate module
 * data structure but instead simply does the needed module cleanup,
 * if any.
 *
 * \param module Module to destroy. Must be defined.
 */
void sctiCpuModuleDestroy( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );
}
