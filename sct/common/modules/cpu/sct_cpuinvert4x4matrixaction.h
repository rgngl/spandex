/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_CPUINVERT4x4MATRIXACTION_H__ )
#define __SCT_CPUINVERT4x4MATRIXACTION_H__

#include "sct_types.h"
#include "sct_cpumodule.h"
#include "sct_cpumodule_parser.h"

/*!
 *
 */
typedef struct
{
    CPUInvert4x4MatrixActionData    data;
    float*                          inMatrices;
    float*                          outMatrices;
} SCTCPUInvert4x4MatrixActionContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*           sctiCreateCPUInvert4x4MatrixActionContext( void* moduleContext, SCTAttributeList* attributes );
    void            sctiDestroyCPUInvert4x4MatrixActionContext( void* actionData );

    SCTBoolean      sctiCPUInvert4x4MatrixActionInit( SCTAction* action, SCTBenchmark* benchmark );
    SCTBoolean      sctiCPUInvert4x4MatrixActionExecute( SCTAction* action, int frameNumber );
    void            sctiCPUInvert4x4MatrixActionTerminate( SCTAction* action );    
    void            sctiCPUInvert4x4MatrixActionDestroy( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_CPUINVERT4x4MATRIXACTION_H__ ) */
