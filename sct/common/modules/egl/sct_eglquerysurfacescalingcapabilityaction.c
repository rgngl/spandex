/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglquerysurfacescalingcapabilityaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_eglmodule.h"

#include <stdio.h>
#include <string.h>

#if defined( _WIN32 )
# pragma warning ( disable : 4127 )
#endif  /* defined( _WIN32 ) */

/*!
 *
 *
 */
void* sctiCreateEglQuerySurfaceScalingCapabilityActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglQuerySurfaceScalingCapabilityActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglQuerySurfaceScalingCapabilityActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglQuerySurfaceScalingCapabilityActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in QuerySurfaceScalingCapability@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglQuerySurfaceScalingCapabilityActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglQuerySurfaceScalingCapabilityActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglQuerySurfaceScalingCapabilityActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in QuerySurfaceScalingCapability@Egl context creation." );
        sctiDestroyEglQuerySurfaceScalingCapabilityActionContext( context );
        return NULL;
    }
    
    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in QuerySurfaceScalingCapability@Egl context creation." );
        sctiDestroyEglQuerySurfaceScalingCapabilityActionContext( context );        
        return NULL;
    }
    
    context->func          = NULL;
    context->funcValidated = SCT_FALSE;   
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglQuerySurfaceScalingCapabilityActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglQuerySurfaceScalingCapabilityActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglQuerySurfaceScalingCapabilityActionContext*   context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglQuerySurfaceScalingCapabilityActionContext* )( action->context );
   
    context->func = ( SCTEglQuerySurfaceScalingCapabilityNOKProc )( siCommonGetProcAddress( NULL, "eglQuerySurfaceScalingCapabilityNOK" ) );
    if( context->func == NULL )
    {
        SCT_LOG_ERROR( "Query surface scaling capability function not found in QuerySurfaceScalingCapability@Egl action execute." );
        return SCT_FALSE;
    }
    context->funcValidated = SCT_FALSE;   
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglQuerySurfaceScalingCapabilityActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglQuerySurfaceScalingCapabilityActionContext*   context;
    EglQuerySurfaceScalingCapabilityActionData*         data;    
    EGLDisplay                                          display;
    EGLConfig                                           config;    
    const char*                                         extString;
    EGLint                                              value;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( frameNumber );
    
    context = ( SCTEglQuerySurfaceScalingCapabilityActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in UnlockSurface@Egl action execute." );
        return SCT_FALSE;
    }
       
    if( context->funcValidated == SCT_FALSE )
    {
        extString = ( const char* )( eglQueryString( display, EGL_EXTENSIONS ) );
        if( extString == NULL || strstr( extString, "EGL_NOK_surface_scaling" ) == NULL )
        {
            SCT_LOG_ERROR( "EGL_NOK_surface_scaling extension not supported in QuerySurfaceScalingCapability@Egl action execute." );
            return SCT_FALSE;
        }
        context->funcValidated = SCT_TRUE;
    }

    config = sctiEglModuleGetConfig( context->moduleContext, data->configIndex );

    SCT_ASSERT( context->func != NULL );
    
    if( context->func( display,
                       config,
                       data->surfaceWidth,
                       data->surfaceHeight,
                       data->targetWidth,
                       data->targetHeight,
                       &value ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Querying surface scaling capability call failed in QuerySurfaceScalingCapability@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

    if( value != data->expectedCapability )
    {
        char    buf[ 256 ];
        sprintf( buf, "Unexpected capability 0x%x in QuerySurfaceScalingCapability@Egl action execute.", value );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglQuerySurfaceScalingCapabilityActionTerminate( SCTAction* action )
{
    SCTEglQuerySurfaceScalingCapabilityActionContext*   context;
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    
    context = ( SCTEglQuerySurfaceScalingCapabilityActionContext* )( action->context );

    context->func          = NULL;
    context->funcValidated = SCT_FALSE;  
}

/*!
 *
 *
 */
void sctiEglQuerySurfaceScalingCapabilityActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglQuerySurfaceScalingCapabilityActionContext( ( SCTEglQuerySurfaceScalingCapabilityActionContext* )( action->context ) );
    action->context = NULL;
}

