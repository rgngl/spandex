#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
SetModuleName( 'Egl' )

######################################################################
AddInclude( 'sct_eglmodule.h' )
AddInclude( 'sct_egl.h' )

######################################################################
MAX_DATA_COUNT              = 256
MAX_DISPLAY_COUNT           = 256
MAX_CONFIG_COUNT            = 256
MAX_WINDOW_COUNT            = 256
MAX_PIXMAP_COUNT            = 256
MAX_CONTEXT_COUNT           = 256
MAX_SURFACE_COUNT           = 256
MAX_IMAGE_COUNT             = 256
MAX_SYNC_COUNT              = 256

MAX_EXTENSION_NAME_LENGTH   = 512
MAX_FUNCTION_NAME_LENGTH    = 512

######################################################################
# EGL DATA
######################################################################
# DestroyData. Destroy the EGL data specified by the data index, and set the
# data index as free. Currently, the only way to create EGL data is to copy it
# from Images module.
AddActionConfig( 'DestroyData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_COUNT - 1 )
                   ]
                 )

######################################################################
######################################################################
# WINDOW SYSTEM ACTIONS
######################################################################
######################################################################

# NOTE: some of the window system actions might not be supported/implemented in
# every window system, in which case an error is reported. Also, some systems
# might require that windows are accessed only from the thread from which they
# have been created.

######################################################################
# CreateWindow. Create a window to WindowIndex. Window will be created into the
# specified screen. The default screen should always be supported if the
# underlying platform supports windows. The action uses the width or height of
# the specified screen if Width or Height is defined as -1. It might be possible
# to create a window with 0,0 size. Define
# SCT_EGL_MODULE_ALLOW_NULL_WINDOW_HANDLE preprocessor macro if you want to
# allow the window system returning a NULL window handle. Window is
# automatically destroyed during action terminate, if still valid. All child
# windows should be destroyed before the parent window is destroyed. Note that
# this is not enforced nor checked at the moment.
AddActionConfig( 'CreateWindow',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'WindowIndex',                          0, MAX_WINDOW_COUNT - 1 ),
                   EnumAttribute(               'Screen', 'SCTScreen',                  [ 'SCT_SCREEN_DEFAULT',
                                                                                          'SCT_SCREEN_HDMI' ] ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                -1 ),
                   IntAttribute(                'Height',                               -1 ),
                   EnumAttribute(               'Format', 'SCTColorFormat',             [ 'SCT_COLOR_FORMAT_DEFAULT',
                                                                                          'SCT_COLOR_FORMAT_BW1',
                                                                                          'SCT_COLOR_FORMAT_L8',
                                                                                          'SCT_COLOR_FORMAT_RGB565',
                                                                                          'SCT_COLOR_FORMAT_RGB888',
                                                                                          'SCT_COLOR_FORMAT_RGBX8888',
                                                                                          'SCT_COLOR_FORMAT_RGBA8888' ] )
                   ]
                 )

######################################################################
# CreateStaticWindow. Create a static window to WindowIndex. A static window is
# NOT destroyed automatically during action terminate but instead during action
# destroy, if still valid. Hence this action may be useful in systems with
# limited/imcomplete window system support. Window will be created into the
# specified screen. The default screen should always be supported if the
# underlying platform supports windows. The action uses the width or height of
# the specified screen if Width or Height is defined as -1. It might be possible
# to create a window with 0,0 size. Define
# SCT_EGL_MODULE_ALLOW_NULL_WINDOW_HANDLE preprocessor macro if you want to
# allow the window system returning a NULL window handle.
AddActionConfig( 'CreateStaticWindow',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'WindowIndex',                          0, MAX_WINDOW_COUNT - 1 ),
                   EnumAttribute(               'Screen', 'SCTScreen',                  [ 'SCT_SCREEN_DEFAULT',
                                                                                          'SCT_SCREEN_HDMI' ] ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                -1 ),
                   IntAttribute(                'Height',                               -1 ),
                   EnumAttribute(               'Format', 'SCTColorFormat',             [ 'SCT_COLOR_FORMAT_DEFAULT',
                                                                                          'SCT_COLOR_FORMAT_BW1',
                                                                                          'SCT_COLOR_FORMAT_L8',
                                                                                          'SCT_COLOR_FORMAT_RGB565',
                                                                                          'SCT_COLOR_FORMAT_RGB888',
                                                                                          'SCT_COLOR_FORMAT_RGBX8888',
                                                                                          'SCT_COLOR_FORMAT_RGBA8888' ] )
                   ]
                 )

######################################################################
# CreateChildWindow. Create a child window to WindowIndex. The parent window is
# taken from ParentWindowIndex. Use the parent window screen width or height if
# Width or Height is defined as -1. It might be possible to create a child
# window with 0,0 size. Child window is automatically destroyed during action
# terminate, if still valid. All child windows should be destroyed before a
# parent window can be destroyed. Note that this is not enforced nor checked at
# the moment.
AddActionConfig( 'CreateChildWindow',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'WindowIndex',                          0, MAX_WINDOW_COUNT - 1 ),
                   IntAttribute(                'ParentWindowIndex',                    0, MAX_WINDOW_COUNT - 1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                -1 ),
                   IntAttribute(                'Height',                               -1 ),
                   EnumAttribute(               'Format', 'SCTColorFormat',             [ 'SCT_COLOR_FORMAT_DEFAULT',
                                                                                          'SCT_COLOR_FORMAT_BW1',
                                                                                          'SCT_COLOR_FORMAT_L8',
                                                                                          'SCT_COLOR_FORMAT_RGB565',
                                                                                          'SCT_COLOR_FORMAT_RGB888',
                                                                                          'SCT_COLOR_FORMAT_RGBX8888',
                                                                                          'SCT_COLOR_FORMAT_RGBA8888' ] )
                   ]
                 )

######################################################################
# WindowToFront. Bring the window defined in WindowIndex to topmost.
AddActionConfig( 'WindowToFront',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'WindowIndex',                          0, MAX_WINDOW_COUNT - 1 )
                   ]
                 )

######################################################################
# ResizeWindow. Resize the window defined in WindowIndex. It might be possible
# to resize the window to size 0,0. Note that this action does not restore the
# original window size.
AddActionConfig( 'ResizeWindow',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'WindowIndex',                          0, MAX_WINDOW_COUNT - 1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 )
                   ]
                 )

######################################################################
# WindowOpacity. Set the opacity for the window defined in WindowIndex. Opacity
# 0.0 means a completely transparent window. Window opacity is used when doing
# the final composition to the screen, i.e. it can be used to make windows
# semitransparent regarding the alpha values in the window surface alpha
# channel. Note that this action does not restore the original window opacity.
AddActionConfig( 'WindowOpacity',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'WindowIndex',                          0, MAX_WINDOW_COUNT - 1 ),
                   FloatAttribute(              'Opacity',                              0, 1 )
                   ]
                 )

######################################################################
# WindowAlpha. Enable/Disable window per-pixel alpha for the window defined in
# WindowIndex. Per-pixel alpha is used when doing the final composition to the
# screen; the alpha values are taken from the window surface alpha channel. Note
# that this action does not restore the original window alpha.
AddActionConfig( 'WindowAlpha',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'WindowIndex',                          0, MAX_WINDOW_COUNT - 1 ),
                   OnOffAttribute(              'Alpha' )
                   ]
                 )

######################################################################
# DestroyWindow. Destroy the window defined in WindowIndex.
AddActionConfig( 'DestroyWindow',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'WindowIndex',                          0,  MAX_WINDOW_COUNT - 1 )
                   ]
                 )

######################################################################
# CreatePixmap. Create a pixmap to PixmapIndex. It might be possible to create a
# pixmap with size 0,0. SCT_COLOR_FORMAT_DEFAULT means the screen format of the
# default screen. The initial pixmap contents is copied from the EGL data
# verbatim; the contents is undefined if DataIndex is -1. Pixmap is
# automatically destroyed during action terminate, if still valid. NOTE: some
# systems may not support initial pixmap data at all, or support a subset of
# initial data formats.
AddActionConfig( 'CreatePixmap',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PixmapIndex',                          0, MAX_PIXMAP_COUNT - 1 ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 ),
                   EnumAttribute(               'Format', 'SCTColorFormat',             [ 'SCT_COLOR_FORMAT_DEFAULT',
                                                                                          'SCT_COLOR_FORMAT_BW1',
                                                                                          'SCT_COLOR_FORMAT_L8',
                                                                                          'SCT_COLOR_FORMAT_RGB565',
                                                                                          'SCT_COLOR_FORMAT_RGB888',
                                                                                          'SCT_COLOR_FORMAT_RGBX8888',
                                                                                          'SCT_COLOR_FORMAT_RGBA8888',
                                                                                          'SCT_COLOR_FORMAT_RGBA8888_PRE' ] ),
                   IntAttribute(                'DataIndex',                            -1, MAX_DATA_COUNT - 1 ),
                   ]
                 )

######################################################################
# CreateSharedPixmap. Create a native buffer/shared pixmap to PixmapIndex;
# shared pixmaps are SgImages in Symbian. It might be possible to create a
# shared pixmap with size 0,0. The pixmap contents is copied from the EGL data
# verbatim; the contents is undefined if DataIndex is -1. Shared pixmap is
# automatically destroyed during action terminate, if still valid. Usage
# specifies the purpose in which the shared pixmap, or derived EGL image can be
# used. NOTE: some systems may not support initial pixmap data at all, or
# support a subset of initial data formats.
AddActionConfig( 'CreateSharedPixmap',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PixmapIndex',                          0, MAX_PIXMAP_COUNT - 1 ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 ),
                   EnumAttribute(               'Format', 'SCTColorFormat',             [ 'SCT_COLOR_FORMAT_A8',
                                                                                          'SCT_COLOR_FORMAT_RGB565',
                                                                                          'SCT_COLOR_FORMAT_RGB888',
                                                                                          'SCT_COLOR_FORMAT_RGBX8888',
                                                                                          'SCT_COLOR_FORMAT_RGBA8888',
                                                                                          'SCT_COLOR_FORMAT_RGBA8888_PRE',
                                                                                          'SCT_COLOR_FORMAT_YUV12' ] ),
                   EnumMaskAttribute(           'Usage', 'int',                         [ 'SCT_USAGE_UNDEFINED',
                                                                                          'SCT_USAGE_OPENVG_IMAGE',
                                                                                          'SCT_USAGE_OPENGLES1_TEXTURE_2D',
                                                                                          'SCT_USAGE_OPENGLES2_TEXTURE_2D',
                                                                                          'SCT_USAGE_OPENGLES1_RENDERBUFFER',
                                                                                          'SCT_USAGE_OPENGLES2_RENDERBUFFER',
                                                                                          'SCT_USAGE_OPENVG_SURFACE',
                                                                                          'SCT_USAGE_OPENGLES1_SURFACE',
                                                                                          'SCT_USAGE_OPENGLES2_SURFACE',
                                                                                          'SCT_USAGE_OPENGL_SURFACE' ] ),
                   IntAttribute(                'DataIndex',                            -1, MAX_DATA_COUNT - 1 ),
                   ]
                 )

######################################################################
# SetPixmapData. Set pixmap/native buffer/shared pixmap data; the pixmap data is
# copied from the EGL data verbatim, hence it must match the pixmap size and
# format exactly. NOTE: some systems may not support setting pixmap data at all,
# or support only a subset of pixmap data formats.
AddActionConfig( 'SetPixmapData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PixmapIndex',                          0, MAX_PIXMAP_COUNT - 1 ),
                   IntAttribute(                'DataIndex',                            0, MAX_DATA_COUNT - 1 ),
                   ]
                 )

######################################################################
# BlitPixmap. Blit the pixmap to the specified window.
AddActionConfig( 'BlitPixmap',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PixmapIndex',                          0, MAX_PIXMAP_COUNT - 1 ),
                   IntAttribute(                'WindowIndex',                          0, MAX_WINDOW_COUNT - 1 ),
                   IntArrayAttribute(           'Origin',                               2 )
                   ]
                 )

######################################################################
# DestroyPixmap. Destroy the pixmap in PixmapIndex.
AddActionConfig( 'DestroyPixmap',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PixmapIndex',                          0, MAX_PIXMAP_COUNT - 1 )
                   ]
                 )

######################################################################
# ScreenOrientation. Set orientation of the specified screen. Note that this
# action sets the screen orientation back to SCT_SCREEN_ORIENTATION_DEFAULT
# during action termination.
AddActionConfig( 'ScreenOrientation',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Screen', 'SCTScreen',                  [ 'SCT_SCREEN_DEFAULT',
                                                                                          'SCT_SCREEN_HDMI' ] ),
                   EnumAttribute(               'Orientation', 'SCTScreenOrientation',  [ 'SCT_SCREEN_ORIENTATION_PORTRAIT',
                                                                                          'SCT_SCREEN_ORIENTATION_LANDSCAPE' ] )
                   ]
                 )

######################################################################
######################################################################
# EGL ACTIONS
######################################################################
######################################################################

######################################################################
# GetDisplay. Get EGL display. Releases the earlier display at the same index,
# if any. Note that the EGL display value remains accessible between benchmarks
# and it must be explicitly released, if needed. This is usually not a problem
# in systems where EGL displays remain static, but benchmarks should be designed
# so that they always get their display to avoid problems when running
# benchmarks separately or in different order.
AddActionConfig( 'GetDisplay',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   EnumAttribute(               'Screen', 'SCTScreen',                  [ 'SCT_SCREEN_DEFAULT',
                                                                                          'SCT_SCREEN_HDMI' ] ),
                   ]
                 )

######################################################################
# ReleaseDisplay. Release EGL display.
AddActionConfig( 'ReleaseDisplay',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   ]
                 )

######################################################################
# InitializeExt. Do EGL initialize WITHOUT doing automatic termination, thread
# releasing and EGL error clearing during action terminate.
AddActionConfig( 'InitializeExt',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   ]
                 )

######################################################################
# Initialize. Do EGL initialize. Egl is automatically terminated and the thread
# is released during action terminate.
AddActionConfig( 'Initialize',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   ]
                 )

######################################################################
# Terminate. Do EGL terminate.
AddActionConfig( 'Terminate',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   ]
                 )

######################################################################
# Info. Print information about the EGL implementation to the output. Output is
# usually the report file. Info is printed only once per benchmark run.
AddActionConfig( 'Info',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   ]
                 )

######################################################################
# CheckExtension. Check the availability of the given EGL extension. The action
# reports an error if the extension is not present.
AddActionConfig( 'CheckExtension',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   StringAttribute(             'Extension',                            MAX_EXTENSION_NAME_LENGTH )
                   ]
                 )

######################################################################
# CheckFunction. Check the availability of the given function. The action
# reports an error if the function is not present.
AddActionConfig( 'CheckFunction',
                 'Execute'+'Destroy',
                 [ StringAttribute(             'Function',                            MAX_FUNCTION_NAME_LENGTH )
                   ]
                 )

######################################################################
# Config. Create an EGL config from arbitrary EGL config attributes to
# ConfigIndex. EGL_NONE is added automatically at the end of the attribute
# array. Config is automatically destroyed during action terminate, if still
# valid.
AddActionConfig( 'Config',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntVectorAttribute(          'Attributes' )
                   ]
                 )

######################################################################
# ConfigExact. Create an EGL config from arbitrary EGL config attributes to
# ConfigIndex. EGL_NONE is added automatically at the end of the attribute
# array. Config is automatically destroyed during action terminate, if still
# valid. All attributes in the selected EGL config must match the given
# attribute values exactly, unless specified as EGL_DONT_CARE.
AddActionConfig( 'ConfigExact',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntVectorAttribute(          'Attributes' )
                   ]
                 )

######################################################################
# RGBConfig. Create an EGL config with the specified attribute values to
# ConfigIndex. Conditional integer attributes operate as follows. Supported
# conditions are <, <=, >, >=, =, default condition is =. For example plain
# integer value 32 is interpreted as '=32'. String values must specify both the
# condition and the value, e.g. '>=8'. '-' is used to specify
# EGL_DONT_CARE. Some attributes are used only if the version of the EGL
# implementation supports them, e.g. EGL_SWAP_BEHAVIOR_PRESERVED_BIT requires
# EGL 1.4. Config is automatically destroyed during action terminate, if still
# valid.
AddActionConfig( 'RGBConfig',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   CIntAttribute(               'BufferSize',                           0, 32 ),
                   CIntAttribute(               'RedSize',                              0, 8 ),
                   CIntAttribute(               'GreenSize',                            0, 8 ),
                   CIntAttribute(               'BlueSize',                             0, 8 ),
                   CIntAttribute(               'AlphaSize',                            0, 8 ),
                   CIntAttribute(               'DepthSize',                            0, 32 ),
                   CIntAttribute(               'AlphaMaskSize',                        0, 32 ),
                   CIntAttribute(               'SampleBuffers',                        0, 1 ),
                   CIntAttribute(               'Samples',                              0, 32 ),
                   CIntAttribute(               'StencilSize',                          0, 32 ),
                   EnumAttribute(               'SupportRGBTextures', 'EGLint',         [ 'EGL_FALSE',
                                                                                          'EGL_TRUE',
                                                                                          'EGL_DONT_CARE' ] ),
                   EnumAttribute(               'SupportRGBATextures', 'EGLint',        [ 'EGL_FALSE',
                                                                                          'EGL_TRUE',
                                                                                          'EGL_DONT_CARE' ] ),
                   EnumMaskAttribute(           'SurfaceType', 'EGLint',                [ 'EGL_WINDOW_BIT',
                                                                                          'EGL_PIXMAP_BIT',
                                                                                          'EGL_PBUFFER_BIT',
                                                                                          'EGL_MULTISAMPLE_RESOLVE_BOX_BIT',
                                                                                          'EGL_SWAP_BEHAVIOR_PRESERVED_BIT',
                                                                                          'EGL_VG_COLORSPACE_LINEAR_BIT',
                                                                                          'EGL_VG_ALPHA_FORMAT_PRE_BIT' ] ),
                   EnumMaskAttribute(           'RenderableType', 'EGLint',             [ 'EGL_OPENGL_ES_BIT',
                                                                                          'EGL_OPENVG_BIT',
                                                                                          'EGL_OPENGL_ES2_BIT',
                                                                                          'EGL_OPENGL_BIT' ] )
                   ]
                 )

######################################################################
# PixmapConfigExt. Create an EGL config matching pixmap to ConfigIndex, with the
# given EGL config attributes. Config is automatically destroyed during action
# terminate, if still valid.
AddActionConfig( 'PixmapConfigExt',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntAttribute(                'PixmapIndex',                          0, MAX_PIXMAP_COUNT - 1 ),
                   IntVectorAttribute(          'Attributes' )
                   ]
                 )

######################################################################
# PixmapConfig. Create an EGL config matching given pixmap to
# ConfigIndex. Config is automatically destroyed during action terminate, if
# still valid.
AddActionConfig( 'PixmapConfig',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntAttribute(                'PixmapIndex',                          0, MAX_PIXMAP_COUNT - 1 )
                   ]
                 )

######################################################################
# PrintConfig. Print EGL config information using siCommonDebugPrintf.
AddActionConfig( 'PrintConfig',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   ]
                 )

######################################################################
# CreateWindowSurfaceExt. Create an EGL window surface to
# SurfaceIndex. ConfigIndex defines the EGL config for the window
# surface. WindowIndex defines the window for the window surface. Attributes
# defines the egl attributes for the eglCreateWindowSurface call. Surface is
# automatically destroyed during action terminate, if still valid.
AddActionConfig( 'CreateWindowSurfaceExt',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntAttribute(                'WindowIndex',                          -1, MAX_WINDOW_COUNT - 1 ),
                   IntVectorAttribute(          'Attributes' )
                   ]
                 )

######################################################################
# CreateStaticWindowSurfaceExt. Create a static EGL window surface to
# SurfaceIndex. A static surface is NOT destroyed automatically during action
# terminate, but instead during action destroy, if still valid. Hence this
# action may be useful in systems with limited/imcomplete window system
# support. ConfigIndex defines the EGL config for the window
# surface. WindowIndex defines the window for the window surface. Attributes
# defines the egl attributes for the eglCreateWindowSurface call.
AddActionConfig( 'CreateStaticWindowSurfaceExt',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntAttribute(                'WindowIndex',                          -1, MAX_WINDOW_COUNT - 1 ),
                   IntVectorAttribute(          'Attributes' )
                   ]
                 )

######################################################################
# CreateWindowSurface. Create an EGL window surface to SurfaceIndex. ConfigIndex
# defines the EGL config for the window surface. WindowIndex defines the window
# for the window surface. ColorSpace and AlphaFormat parameters are valid only
# for OpenVG. A NULL window handle is used if WindowIndex equals -1; this may
# work in some special environments without a window system. Surface is
# automatically destroyed during action terminate, if still valid.
AddActionConfig( 'CreateWindowSurface',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntAttribute(                'WindowIndex',                          -1, MAX_WINDOW_COUNT - 1 ),
                   EnumAttribute(               'RenderBuffer', 'EGLint',               [ 'EGL_NONE',
                                                                                          'EGL_SINGLE_BUFFER',
                                                                                          'EGL_BACK_BUFFER' ] ),
                   EnumAttribute(               'ColorSpace', 'EGLint',                 [ 'EGL_NONE',
                                                                                          'EGL_COLORSPACE_sRGB',
                                                                                          'EGL_COLORSPACE_LINEAR',
                                                                                          'EGL_VG_COLORSPACE_sRGB',
                                                                                          'EGL_VG_COLORSPACE_LINEAR' ] ),
                   EnumAttribute(               'AlphaFormat','EGLint',                 [ 'EGL_NONE',
                                                                                          'EGL_ALPHA_FORMAT_NONPRE',
                                                                                          'EGL_ALPHA_FORMAT_PRE',
                                                                                          'EGL_VG_ALPHA_FORMAT_NONPRE',
                                                                                          'EGL_VG_ALPHA_FORMAT_PRE' ] )
                   ]
                 )

######################################################################
# CreatePbufferSurfaceExt. Create a pbuffer surface to SurfaceIndex. ConfigIndex
# defines the EGL config for the pbuffer surface. Attributes defines the egl
# attributes for the eglCreatePbufferSurface call. Surface is automatically
# destroyed during action terminate, if still valid.
AddActionConfig( 'CreatePbufferSurfaceExt',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntVectorAttribute(          'Attributes' )
                   ]
                 )

######################################################################
# CreatePbufferSurface. Create a pbuffer surface to SurfaceIndex. ConfigIndex
# defines the EGL config for the pbuffer surface. ColorSpace and AlphaFormat
# parameters are valid only for OpenVG. Texture parameters are valid only for
# OpenGL/ES. Surface is automatically destroyed during action terminate, if
# still valid.
AddActionConfig( 'CreatePbufferSurface',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 ),
                   EnumAttribute(               'TextureFormat', 'EGLint',              [ 'EGL_NO_TEXTURE',
                                                                                          'EGL_TEXTURE_RGB',
                                                                                          'EGL_TEXTURE_RGBA' ] ),
                   EnumAttribute(               'TextureTarget', 'EGLint',              [ 'EGL_NO_TEXTURE',
                                                                                          'EGL_TEXTURE_2D' ] ),
                   OnOffAttribute(              'MipmapTexture' ),
                   OnOffAttribute(              'LargestPbuffer' ),
                   EnumAttribute(               'ColorSpace', 'EGLint',                 [ 'EGL_NONE',
                                                                                          'EGL_COLORSPACE_sRGB',
                                                                                          'EGL_COLORSPACE_LINEAR',
                                                                                          'EGL_VG_COLORSPACE_sRGB',
                                                                                          'EGL_VG_COLORSPACE_LINEAR' ] ),
                   EnumAttribute(               'AlphaFormat','EGLint',                 [ 'EGL_NONE',
                                                                                          'EGL_ALPHA_FORMAT_NONPRE',
                                                                                          'EGL_ALPHA_FORMAT_PRE',
                                                                                          'EGL_VG_ALPHA_FORMAT_NONPRE',
                                                                                          'EGL_VG_ALPHA_FORMAT_PRE' ] )
                   ]
                 )

######################################################################
# CreatePixmapSurfaceExt. Create a pixmap surface to SurfaceIndex. Attributes
# defines the egl attributes for the eglCreatePixmapSurface call. Surface is
# automatically destroyed during action terminate, if still valid.
AddActionConfig( 'CreatePixmapSurfaceExt',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntAttribute(                'PixmapIndex',                          0, MAX_PIXMAP_COUNT - 1 ),
                   IntVectorAttribute(          'Attributes' )
                   ]
                 )

######################################################################
# CreatePixmapSurface. Create a pixmap surface to SurfaceIndex. ColorSpace and
# AlphaFormat parameters are valid only for OpenVG. Surface is automatically
# destroyed during action terminate, if still valid.
AddActionConfig( 'CreatePixmapSurface',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntAttribute(                'PixmapIndex',                          0, MAX_PIXMAP_COUNT - 1 ),
                   EnumAttribute(               'ColorSpace', 'EGLint',                 [ 'EGL_NONE',
                                                                                          'EGL_COLORSPACE_sRGB',
                                                                                          'EGL_COLORSPACE_LINEAR',
                                                                                          'EGL_VG_COLORSPACE_sRGB',
                                                                                          'EGL_VG_COLORSPACE_LINEAR' ] ),
                   EnumAttribute(               'AlphaFormat','EGLint',                 [ 'EGL_NONE',
                                                                                          'EGL_ALPHA_FORMAT_NONPRE',
                                                                                          'EGL_ALPHA_FORMAT_PRE',
                                                                                          'EGL_VG_ALPHA_FORMAT_NONPRE',
                                                                                          'EGL_VG_ALPHA_FORMAT_PRE' ] )
                   ]
                 )

######################################################################
# CreatePbufferFromClientBufferExt. Create a pbuffer surface from an OpenVG
# image. BufferIndex refers to OpenVG image index; see CreateImage@OpenVG
# action. Attributes defines the egl attributes for the
# eglCreatePbufferFromClientBuffer call. Surface is automatically destroyed
# during action terminate, if still valid.
AddActionConfig( 'CreatePbufferFromClientBufferExt',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   EnumAttribute(               'BufType', 'EGLenum',                   [ 'EGL_OPENVG_IMAGE' ] ),
                   IntAttribute(                'BufferIndex',                          0 ),
                   IntVectorAttribute(          'Attributes' )
                   ]
                 )

######################################################################
# CreatePbufferFromClientBuffer. Create a pbuffer surface from an OpenVG
# image. BufferIndex refers to OpenVG image index; see CreateImage@OpenVG
# action. Surface is automatically destroyed during action terminate, if still
# valid.
AddActionConfig( 'CreatePbufferFromClientBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   EnumAttribute(               'BufType', 'EGLenum',                   [ 'EGL_OPENVG_IMAGE' ] ),
                   IntAttribute(                'BufferIndex',                          0 ),
                   EnumAttribute(               'TextureFormat', 'EGLint',              [ 'EGL_NO_TEXTURE',
                                                                                          'EGL_TEXTURE_RGB',
                                                                                          'EGL_TEXTURE_RGBA' ] ),
                   EnumAttribute(               'TextureTarget', 'EGLint',              [ 'EGL_NO_TEXTURE',
                                                                                          'EGL_TEXTURE_2D' ] ),
                   OnOffAttribute(              'MipmapTexture' ),
                   ]
                 )

######################################################################
# CurrentSurface. Get current read or draw egl surface. Reports an error if the
# surface is not valid. Surface is automatically set to EGL_NO_SURFACE during
# action terminate.
AddActionConfig( 'CurrentSurface',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   EnumAttribute(               'Surface', 'EGLenum',                   [ 'EGL_READ',
                                                                                          'EGL_DRAW' ] )
                   ]
                 )

######################################################################
# DestroySurface. Destroy an EGL surface in SurfaceIndex.
AddActionConfig( 'DestroySurface',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 )
                   ]
                 )

######################################################################
# SurfaceAttrib. Set surface attribute.
AddActionConfig( 'SurfaceAttrib',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'Attribute' ),
                   IntAttribute(                'Value' )
                   ]
                 )

######################################################################
# BindTexImage. Bind the EGL surface as OpenGL/ES texture. TexImage is
# automatically released during action terminate, if the surface is still valid.
AddActionConfig( 'BindTexImage',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   EnumAttribute(               'Buffer', 'EGLint',                     [ 'EGL_BACK_BUFFER' ] )
                   ]
                 )

######################################################################
# ReleaseTexImage. Release the EGL surface from being used as OpenGL/ES texture.
AddActionConfig( 'ReleaseTexImage',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   EnumAttribute(               'Buffer', 'EGLint',                     [ 'EGL_BACK_BUFFER' ] )
                   ]
                 )

######################################################################
# CreateContextExt. Create an EGL context to ContextIndex. ConfigIndex defines
# the EGL config for the context. ShareContextIndex -1 means
# EGL_NO_CONTEXT. Context is destroyed automatically during action terminate, if
# still valid.
AddActionConfig( 'CreateContextExt',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ContextIndex',                         0, MAX_CONTEXT_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntAttribute(                'ShareContextIndex',                    -1, MAX_CONTEXT_COUNT - 1 ),
                   IntVectorAttribute(          'Attributes' )
                   ]
                 )

######################################################################
# CreateContext. Create an EGL context to ContextIndex. ConfigIndex defines the
# EGL config for the context. ShareContextIndex -1 means EGL_NO_CONTEXT. Client
# version is not defined in the attribute list when -1. Context is destroyed
# automatically during action terminate, if still valid.
AddActionConfig( 'CreateContext',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ContextIndex',                         0, MAX_CONTEXT_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntAttribute(                'ShareContextIndex',                    -1, MAX_CONTEXT_COUNT - 1 ),
                   IntAttribute(                'ClientVersion',                        -1 ),
                   ]
                 )

######################################################################
# DestroyContext. Destroy the context in ContextIndex.
AddActionConfig( 'DestroyContext',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ContextIndex',                         0, MAX_CONTEXT_COUNT - 1 )
                   ]
                 )

######################################################################
# MakeCurrent. Make the draw and read surfaces and context current. -1 as
# surface means EGL_NO_SURFACE, -1 as context means EGL_NO_CONTEXT. Contexts are
# set to EGL_NO_CONTEXT with the matching API automatically during action
# terminate. Also, any potential EGL error is cleared. Do not use with
# multithreaded benchmark loops; use MakeCurrentExt instead.
AddActionConfig( 'MakeCurrent',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'DrawSurfaceIndex',                     -1, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ReadSurfaceIndex',                     -1, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ContextIndex',                         -1, MAX_CONTEXT_COUNT - 1 ),
                   ]
                 )

######################################################################
# MakeCurrentExt. Make the draw and read surfaces and context current. -1 as
# surface means EGL_NO_SURFACE, -1 as context means EGL_NO_CONTEXT. Nothing is
# done during action terminate. Use with multithreaded benchmark loops.
AddActionConfig( 'MakeCurrentExt',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'DrawSurfaceIndex',                     -1, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ReadSurfaceIndex',                     -1, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'ContextIndex',                         -1, MAX_CONTEXT_COUNT - 1 ),
                   ]
                 )

######################################################################
# BindApi. Set active API.
AddActionConfig( 'BindApi',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Api', 'EGLenum',                       [ 'EGL_OPENGL_ES_API',
                                                                                          'EGL_OPENVG_API' ] ),
                   ]
                 )

######################################################################
# CopyBuffers. Copy surface color buffer contents to a pixmap.
AddActionConfig( 'CopyBuffers',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'PixmapIndex',                          0, MAX_PIXMAP_COUNT - 1 )
                   ]
                 )

######################################################################
# SwapBuffers. Swap egl surface buffers. Swap works only with EGL window
# surfaces.
AddActionConfig( 'SwapBuffers',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 )
                   ]
                 )

######################################################################
# SwapInterval. Set swap interval for active EGL surface.
AddActionConfig( 'SwapInterval',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'Interval',                             0 )
                   ]
                 )

######################################################################
# WaitClient.
AddActionConfig( 'WaitClient',
                 'Execute'+'Destroy',
                 []
                 )

######################################################################
# WaitNative.
AddActionConfig( 'WaitNative',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Engine', 'EGLint',                     [ 'EGL_CORE_NATIVE_ENGINE' ] )
                   ]
                 )

######################################################################
# ReleaseThread.
AddActionConfig( 'ReleaseThread',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 []
                 )

######################################################################
# EGL resource profile
######################################################################

######################################################################
# ResourceProfile. Requires EGL_NOK_resource_profiling extension. Print resource
# profile data using siCommonDebugPrintf and output file. Use FrameNumbers
# vector to define specific frames from which the data should be reported. Empty
# vector means that every frame reports the data.
AddActionConfig( 'ResourceProfile',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntVectorAttribute(          'FrameNumbers' )
                   ]
                 )

######################################################################
# EGL lock surface
######################################################################

######################################################################
# LockSurface. Requires EGL_KHR_lock_surface or EGL_KHR_lock_surface2
# extension. Surface is unlocked automatically during action terminate, if still
# valid.
AddActionConfig( 'LockSurface',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   OnOffAttribute(              'PreservePixels' ),
                   EnumMaskAttribute(           'UsageHint', 'EGLint',                  [ 'EGL_READ_SURFACE_BIT_KHR',
                                                                                          'EGL_WRITE_SURFACE_BIT_KHR' ] ),
                   ]
                 )

######################################################################
# ClearLock1Surface. Surface must have been locked. Requires
# EGL_KHR_lock_surface extension. X,Y specify the top left corner of the cleared
# surface area. Width extends to the right, Height extends to the bottom. It is
# possible to clear an area of size 0,0. Surface height must be provided
# explicitly for the action to deal with different bitmap origins. NOTE:
# specifying invalid clear area might cause a crash as the action does not
# validate properly that the clear area fits into surface area.
AddActionConfig( 'ClearLock1Surface',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 ),
                   IntAttribute(                'SurfaceHeight',                        0 ),
                   EnumAttribute(               'Format', 'EGLint',                     [ 'EGL_FORMAT_RGB_565_EXACT_KHR',
                                                                                          'EGL_FORMAT_RGB_565_KHR',
                                                                                          'EGL_FORMAT_RGBA_8888_EXACT_KHR',
                                                                                          'EGL_FORMAT_RGBA_8888_KHR' ] ),
                   FloatArrayAttribute(         'Color',                                4, 0, 1 ),
                   ]
                 )

######################################################################
# ClearLock2Surface. Surface must have been locked. Requires
# EGL_KHR_lock_surface2 extension. X,Y specify the top left corner of the
# cleared surface area. Width extends to the right, Height extends to the
# bottom. It is possible to clear an area of size 0,0. If equal to -1, clear
# area width and height are taken from the surface width and height.
AddActionConfig( 'ClearLock2Surface',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                -1 ),
                   IntAttribute(                'Height',                               -1 ),
                   FloatArrayAttribute(         'Color',                                4, 0, 1 ),
                   ]
                 )

######################################################################
# DrawLock2Surface. Draws EGL data into surface. Surface must have been
# locked. Requires EGL_KHR_lock_surface2 extension. EGL data size must match the
# surface.
AddActionConfig( 'DrawLock2Surface',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'DataIndex',                            0, MAX_DATA_COUNT - 1 ),
                   ]
                 )

######################################################################
# UnlockSurface. Unlock surface. Requires EGL_KHR_lock_surface or
# EGL_KHR_lock_surface2 extension.
AddActionConfig( 'UnlockSurface',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   ]
                 )

######################################################################
# EGL surface scaling
######################################################################

######################################################################
# QuerySurfaceScalingCapability. Requires EGL_NOK_surface_scaling extension.
AddActionConfig( 'QuerySurfaceScalingCapability',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ConfigIndex',                          0, MAX_CONFIG_COUNT - 1 ),
                   IntAttribute(                'SurfaceWidth',                         1 ),
                   IntAttribute(                'SurfaceHeight',                        1 ),
                   IntAttribute(                'TargetWidth',                          1 ),
                   IntAttribute(                'TargetHeight',                         1 ),
                   EnumAttribute(               'ExpectedCapability','EGLint',          [ 'EGL_NOT_SUPPORTED_NOK',
                                                                                          'EGL_SUPPORTED_NOK',
                                                                                          'EGL_SLOW_NOK' ] ),
                   ]
                 )

######################################################################
# SetSurfaceScaling. Requires EGL_NOK_surface_scaling extension.
AddActionConfig( 'SetSurfaceScaling',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SurfaceIndex',                         0, MAX_SURFACE_COUNT - 1 ),
                   IntAttribute(                'TargetOffsetX',                        0 ),
                   IntAttribute(                'TargetOffsetY',                        0 ),
                   IntAttribute(                'TargetWidth',                          1 ),
                   IntAttribute(                'TargetHeight',                         1 )
                   ]
                 )

######################################################################
# EGL image
######################################################################

######################################################################
# CreateImageExt. Requires EGL_KHR_image_base extension. BufferIndex is used to
# select buffer. Attributes defines the egl attributes for the eglCreateImageKhr
# call.
#
# Target EGL_NATIVE_BUFFER, EGL_NATIVE_PIXMAP_KHR: buffer index specifies EGL
# module pixmap index. Shared pixmaps in Symbian require
# EGL_NOK_pixmap_type_rsgimage extension. EGL_NATIVE_BUFFER uses the
# platform-specific target of the pixmap.
#
# Target EGL_VG_PARENT_IMAGE_KHR: requires EGL_KHR_vg_parent_image
# extension. Buffer index specifies OpenVG module image index.
#
# Targets EGL_GL1_TEXTURE*: require EGL_KHR_gl_texture_2D_image and
# EGL_KHR_gl_texture_cubemap_image extensions, respectively. Buffer index
# specifies OpenGLES1 module texture index.
#
# Targets EGL_GL2_TEXTURE*: require EGL_KHR_gl_texture_2D_image and
# EGL_KHR_gl_texture_cubemap_image extensions, respectively. Buffer index
# specifies OpenGLES2 module texture index.
#
# Target EGL_GL2_RENDERBUFFER_KHR requires EGL_KHR_gl_renderbuffer_image
# extension.
AddActionConfig( 'CreateImageExt',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ImageIndex',                           0, MAX_IMAGE_COUNT - 1 ),
                   IntAttribute(                'ContextIndex',                        -1, MAX_CONTEXT_COUNT - 1 ),
                   EnumAttribute(               'Target', 'EGLint',                     [ 'EGL_NATIVE_BUFFER',
                                                                                          'EGL_NATIVE_PIXMAP_KHR',
                                                                                          'EGL_VG_PARENT_IMAGE_KHR',
                                                                                          'EGL_GL1_TEXTURE_2D_KHR',
                                                                                          'EGL_GL1_TEXTURE_CUBE_MAP_POSITIVE_X_KHR',
                                                                                          'EGL_GL1_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR',
                                                                                          'EGL_GL1_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR',
                                                                                          'EGL_GL1_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR',
                                                                                          'EGL_GL1_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR',
                                                                                          'EGL_GL1_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR',
                                                                                          'EGL_GL2_TEXTURE_2D_KHR',
                                                                                          'EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_X_KHR',
                                                                                          'EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR',
                                                                                          'EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR',
                                                                                          'EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR',
                                                                                          'EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR',
                                                                                          'EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR',
                                                                                          'EGL_GL2_RENDERBUFFER_KHR' ] ),
                   IntAttribute(                'BufferIndex',                          0 ),
                   IntVectorAttribute(          'Attributes' )
                   ]
                 )

######################################################################
# DestroyImage. Requires EGL_KHR_image_base extension.
AddActionConfig( 'DestroyImage',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'ImageIndex',                           0, MAX_IMAGE_COUNT - 1 ),
                   ]
                 )

######################################################################
# EGL fence sync
######################################################################

######################################################################
# CreateSync. Create EGL sync object. Requires EGL_KHR_fence_sync extension.
AddActionConfig( 'CreateSync',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SyncIndex',                            0, MAX_SYNC_COUNT - 1 ),
                   EnumAttribute(               'Type','EGLint',                        [ 'EGL_SYNC_FENCE_KHR' ] ),
                   IntVectorAttribute(          'Attributes' )
                   ]
                 )

######################################################################
# DestroySync. Destroy EGL sync object. Requires EGL_KHR_fence_sync extension. The action
# does not trigger an error from an invalid sync when CheckError is not defined; use this
# in situations where multiple threads are waiting for a single sync event; the first thread
# woken destroys the sync and other threads ignore the error.
AddActionConfig( 'DestroySync',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SyncIndex',                            0, MAX_SYNC_COUNT - 1 ),
                   OnOffAttribute(              'CheckError' ),
                   ]
                 )

######################################################################
# ClientWaitSync. Wait for a sync object to become signaled. Empty timeout vector
# implies EGL_FOREVER_KHR, the vector values are multiplied together to get the
# timeout in nanoseconds; negative elements are not allowed, overflow is
# detected and reported as an error. Requires EGL_KHR_fence_sync extension.
AddActionConfig( 'ClientWaitSync',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DisplayIndex',                         0, MAX_DISPLAY_COUNT - 1 ),
                   IntAttribute(                'SyncIndex',                            0, MAX_SYNC_COUNT - 1 ),
                   EnumMaskAttribute(           'Flags', 'EGLint',                      [ 'EGL_SYNC_FLUSH_COMMANDS_BIT_KHR' ] ),
                   IntVectorAttribute(          'Timeout' )
                   ]
                 )

######################################################################
# END OF FILE
######################################################################
