/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglprintconfigaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglPrintConfigActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglPrintConfigActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglPrintConfigActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglPrintConfigActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PrintConfig@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglPrintConfigActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglPrintConfigActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglPrintConfigActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in PrintConfig@Egl context creation." );
        sctiDestroyEglPrintConfigActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglPrintConfigActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglPrintConfigActionExecute( SCTAction* action, int framenumber )
{
    SCTEglPrintConfigActionContext* context;
    EglPrintConfigActionData*       data;
    EGLDisplay                      display;
    EGLConfig                       config                         = 0;
    char                            name[ 32 ];
    char                            value[ 1024 ];
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglPrintConfigActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in PrintConfig@Egl action execute." );
        return SCT_FALSE;
    }

    config = sctiEglModuleGetConfig( context->moduleContext, data->configIndex );
    
    name[ 0 ]  = '\0';
    value[ 0 ] = '\0';

    if( sctiEglGetConfigNameValue( display, config, name, sizeof( name ), value, sizeof( value ) ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Get config data failed in PrintConfig@Egl action execute." );
        return SCT_FALSE;
    }
    
    siCommonDebugPrintf( NULL, "SPANDEX: PrintConfig@Egl %s: %s", name, value );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglPrintConfigActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglPrintConfigActionContext( ( SCTEglPrintConfigActionContext* )( action->context ) );
    action->context = NULL;
}

