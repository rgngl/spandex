/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglinitializeaction.h"
#include "sct_sicommon.h"
#include "sct_siwindow.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglInitializeActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglInitializeActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglInitializeActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglInitializeActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Initialize@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglInitializeActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglInitializeActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglInitializeActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in Initialize@Egl context creation." );
        sctiDestroyEglInitializeActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglInitializeActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglInitializeActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglInitializeActionExecute( SCTAction* action, int framenumber )
{
    SCTEglInitializeActionContext*  context;
    EglInitializeActionData*        data;
    EGLDisplay                      display;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglInitializeActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in Initialize@Egl action execute." );
        return SCT_FALSE;
    }

    if( eglInitialize( display, NULL, NULL ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Egl initialization failed in Initialize@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        
        return SCT_FALSE;
    }           

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglInitializeActionTerminate( SCTAction* action )
{
    SCTEglInitializeActionContext*  context;
    EGLDisplay                      display;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT( action->context != NULL );    

    context = ( SCTEglInitializeActionContext* )( action->context );    

    display = sctiEglModuleGetDisplay( context->moduleContext, context->data.displayIndex );
    if( display != EGL_NO_DISPLAY )
    {
        eglTerminate( display );
    }
    
#if defined( EGL_VERSION_1_2 )      
    eglReleaseThread();
#endif  /* defined( EGL_VERSION_1_2 ) */    
}

/*!
 *
 *
 */
void sctiEglInitializeActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglInitializeActionContext( ( SCTEglInitializeActionContext* )( action->context ) );
    action->context = NULL;
}

