/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_EGL_H__ )
#define __SCT_EGL_H__

#if defined( SCT_KHR_COMPLIANT_INCLUDES )
# include <EGL/egl.h>
#else
# include <egl.h>
#endif

#include "sct_siwindow.h"

/* ************************************************************ */
#if !defined( EGL_MATCH_NATIVE_PIXMAP )
# define EGL_MATCH_NATIVE_PIXMAP			        0x3041
#endif  /* !defined( EGL_MATCH_NATIVE_PIXMAP ) */

/* ************************************************************ */
/* EGL_NOK_surface_scaling defines */

#if !defined( EGL_NOT_SUPPORTED_NOK )
# define EGL_NOT_SUPPORTED_NOK                      0
#endif  /* !defined( EGL_NOT_SUPPORTED_NOK ) */
#if !defined( EGL_SUPPORTED_NOK )
# define EGL_SUPPORTED_NOK                          1
#endif  /* !defined( EGL_SUPPORTED_NOK ) */
#if !defined( EGL_SLOW_NOK )
# define EGL_SLOW_NOK                               3
#endif  /* !defined( EGL_SLOW_NOK ) */
#if !defined( EGL_FIXED_WIDTH_NOK )
# define EGL_FIXED_WIDTH_NOK                        0x30DB
#endif  /* !defined( EGL_FIXED_WIDTH_NOK ) */
#if !defined( EGL_FIXED_HEIGHT_NOK )
# define EGL_FIXED_HEIGHT_NOK                       0x30DC
#endif  /* !defined( EGL_FIXED_HEIGHT_NOK ) */
#if !defined( EGL_TARGET_EXTENT_OFFSET_X_NOK )
# define EGL_TARGET_EXTENT_OFFSET_X_NOK             0x3079
#endif  /* !defined( EGL_TARGET_EXTENT_OFFSET_X_NOK ) */
#if !defined( EGL_TARGET_EXTENT_OFFSET_Y_NOK )
# define EGL_TARGET_EXTENT_OFFSET_Y_NOK             0x307A
#endif  /* !defined( EGL_TARGET_EXTENT_OFFSET_Y_NOK ) */
#if !defined( EGL_TARGET_EXTENT_WIDTH_NOK )
# define EGL_TARGET_EXTENT_WIDTH_NOK                0x307B
#endif  /* !defined( EGL_TARGET_EXTENT_WIDTH_NOK ) */
#if !defined( EGL_TARGET_EXTENT_HEIGHT_NOK )
# define EGL_TARGET_EXTENT_HEIGHT_NOK               0x307C
#endif  /* !defined( EGL_TARGET_EXTENT_HEIGHT_NOK ) */
#if !defined( EGL_BORDER_COLOR_RED_NOK )
# define EGL_BORDER_COLOR_RED_NOK                   0x307D
#endif  /* !defined( EGL_BORDER_COLOR_RED_NOK ) */
#if !defined( EGL_BORDER_COLOR_GREEN_NOK )
# define EGL_BORDER_COLOR_GREEN_NOK                 0x307E
#endif  /* !defined( EGL_BORDER_COLOR_GREEN_NOK ) */
#if !defined( EGL_BORDER_COLOR_BLUE_NOK )
# define EGL_BORDER_COLOR_BLUE_NOK                  0x30D8
#endif  /* !defined( EGL_BORDER_COLOR_BLUE_NOK ) */

/* ************************************************************ */
/* EGL LOCK SURFACE DEFINES */
#if !defined( EGL_LOCK_SURFACE_BIT_KHR )
# define EGL_LOCK_SURFACE_BIT_KHR                   0x0080
#endif  /* !defined( EGL_LOCK_SURFACE_BIT_KHR ) */

#if !defined( EGL_OPTIMAL_FORMAT_BIT_KHR )
# define EGL_OPTIMAL_FORMAT_BIT_KHR                 0x0100
#endif  /* !defined( EGL_OPTIMAL_FORMAT_BIT_KHR ) */

#if !defined( EGL_MATCH_FORMAT_KHR )
# define EGL_MATCH_FORMAT_KHR                       0x3043
#endif  /* !defined( EGL_MATCH_FORMAT_KHR ) */

#if !defined( EGL_FORMAT_RGB_565_EXACT_KHR )
# define EGL_FORMAT_RGB_565_EXACT_KHR               0x30C0
#endif  /* !defined( EGL_FORMAT_RGB_565_EXACT_KHR ) */

#if !defined( EGL_FORMAT_RGB_565_KHR )
# define EGL_FORMAT_RGB_565_KHR                     0x30C1
#endif  /* !defined( EGL_FORMAT_RGB_565_KHR ) */

#if !defined( EGL_FORMAT_RGBA_8888_EXACT_KHR )
# define EGL_FORMAT_RGBA_8888_EXACT_KHR             0x30C2
#endif  /* !defined( EGL_FORMAT_RGBA_8888_EXACT_KHR ) */

#if !defined( EGL_FORMAT_RGBA_8888_KHR )
# define EGL_FORMAT_RGBA_8888_KHR                   0x30C3
#endif  /* !defined( EGL_FORMAT_RGBA_8888_KHR ) */

#if !defined( EGL_MAP_PRESERVE_PIXELS_KHR )
# define EGL_MAP_PRESERVE_PIXELS_KHR                0x30C4
#endif  /* !defined( EGL_MAP_PRESERVE_PIXELS_KHR ) */

#if !defined( EGL_LOCK_USAGE_HINT_KHR )
# define EGL_LOCK_USAGE_HINT_KHR                    0x30C5
#endif  /* !defined( EGL_LOCK_USAGE_HINT_KHR ) */

#if !defined( EGL_READ_SURFACE_BIT_KHR )
# define EGL_READ_SURFACE_BIT_KHR                   0x0001
#endif  /* !defined( EGL_READ_SURFACE_BIT_KHR ) */

#if !defined( EGL_WRITE_SURFACE_BIT_KHR )
# define EGL_WRITE_SURFACE_BIT_KHR                  0x0002
#endif  /* !defined( EGL_WRITE_SURFACE_BIT_KHR ) */

/* ************************************************************ */
#if !defined( EGL_VERSION_1_4 )
# if !defined( EGL_SWAP_BEHAVIOR )
#define EGL_SWAP_BEHAVIOR                           0x3093
# endif /* !defined( EGL_SWAP_BEHAVIOR ) */
# if !defined( EGL_MULTISAMPLE_RESOLVE )
#  define EGL_MULTISAMPLE_RESOLVE                   0x3099
# endif /* !defined( EGL_MULTISAMPLE_RESOLVE ) */
# if !defined( EGL_MULTISAMPLE_RESOLVE_BOX_BIT )
#  define EGL_MULTISAMPLE_RESOLVE_BOX_BIT           0x0200
# endif /* !defined( EGL_MULTISAMPLE_RESOLVE_BOX_BIT ) */
# if !defined( EGL_SWAP_BEHAVIOR_PRESERVED_BIT )
#  define EGL_SWAP_BEHAVIOR_PRESERVED_BIT           0x0400
# endif /* !defined( EGL_SWAP_BEHAVIOR_PRESERVED_BIT ) */
# if !defined( EGL_MULTISAMPLE_RESOLVE_DEFAULT )
#  define EGL_MULTISAMPLE_RESOLVE_DEFAULT           0x309A
# endif /* !defined( EGL_MULTISAMPLE_RESOLVE_DEFAULT ) */
# if !defined( EGL_MULTISAMPLE_RESOLVE_BOX )
#  define EGL_MULTISAMPLE_RESOLVE_BOX               0x309B
# endif /* !defined( EGL_MULTISAMPLE_RESOLVE_BOX ) */
# if !defined( EGL_BUFFER_PRESERVED )
#  define EGL_BUFFER_PRESERVED                      0x3094
# endif /* !defined( EGL_BUFFER_PRESERVED ) */
# if !defined( EGL_BUFFER_DESTROYED )
#  define EGL_BUFFER_DESTROYED                      0x3095
# endif /* !defined( EGL_BUFFER_DESTROYED ) */
# if !defined( EGL_OPENGL_BIT )
#  define EGL_OPENGL_BIT                            0x0008
# endif  /* !defined( EGL_OPENGL_BIT ) */
#endif  /* !defined( EGL_VERSION_1_4 ) */

#if !defined( EGL_VERSION_1_3 )
# if !defined( EGL_OPENGL_ES2_BIT )
#  define EGL_OPENGL_ES2_BIT                        0x0004
# endif /* !defined( EGL_OPENGL_ES2_BIT ) */
# if !defined( EGL_VG_COLORSPACE_LINEAR_BIT )
#  define EGL_VG_COLORSPACE_LINEAR_BIT              0x0020
# endif /* !defined( EGL_VG_COLORSPACE_LINEAR_BIT ) */
# if !defined( EGL_VG_ALPHA_FORMAT_PRE_BIT )
#  define EGL_VG_ALPHA_FORMAT_PRE_BIT               0x0040
# endif /* !defined( EGL_VG_ALPHA_FORMAT_PRE_BIT ) */
# if !defined( EGL_MATCH_NATIVE_PIXMAP )
#  define EGL_MATCH_NATIVE_PIXMAP                   0x3041
# endif /* !defined( EGL_MATCH_NATIVE_PIXMAP ) */
# if !defined( EGL_VG_COLORSPACE )
#  define EGL_VG_COLORSPACE                         EGL_COLORSPACE
# endif /* !defined( EGL_VG_COLORSPACE ) */
# if !defined( EGL_VG_ALPHA_FORMAT )
#  define EGL_VG_ALPHA_FORMAT                       EGL_ALPHA_FORMAT
# endif /* !defined( EGL_VG_ALPHA_FORMAT ) */
# if !defined( EGL_VG_COLORSPACE_sRGB )
#  define EGL_VG_COLORSPACE_sRGB                    EGL_COLORSPACE_sRGB
# endif /* !defined( EGL_VG_COLORSPACE_sRGB ) */
# if !defined( EGL_VG_COLORSPACE_LINEAR )
#  define EGL_VG_COLORSPACE_LINEAR                  EGL_COLORSPACE_LINEAR
# endif /* !defined( EGL_VG_COLORSPACE_LINEAR ) */
# if !defined( EGL_VG_ALPHA_FORMAT_NONPRE )
#  define EGL_VG_ALPHA_FORMAT_NONPRE                EGL_ALPHA_FORMAT_NONPRE
# endif /* !defined( EGL_VG_ALPHA_FORMAT_NONPRE ) */
# if !defined( EGL_VG_ALPHA_FORMAT_PRE )
#  define EGL_VG_ALPHA_FORMAT_PRE                   EGL_ALPHA_FORMAT_PRE
# endif /* !defined( EGL_VG_ALPHA_FORMAT_PRE ) */
#endif  /* !defined( EGL_VERSION_1_3 ) */

#if !defined( EGL_VERSION_1_2 )
typedef int                             EGLenum;
# if !defined( EGL_TEXTURE_FORMAT )
#  define EGL_TEXTURE_FORMAT            0x3080
# endif /* !defined( EGL_TEXTURE_FORMAT ) */
# if !defined( EGL_TEXTURE_TARGET )
#  define EGL_TEXTURE_TARGET            0x3081
# endif /* !defined( EGL_TEXTURE_TARGET ) */
# if !defined( EGL_MIPMAP_TEXTURE )
#  define EGL_MIPMAP_TEXTURE            0x3082
# endif /* !defined( EGL_MIPMAP_TEXTURE ) */
# if !defined( EGL_BIND_TO_TEXTURE_RGB )
#  define EGL_BIND_TO_TEXTURE_RGB       0x3039
# endif /* !defined( EGL_BIND_TO_TEXTURE_RGB ) */
# if !defined( EGL_BIND_TO_TEXTURE_RGBA )
#  define EGL_BIND_TO_TEXTURE_RGBA      0x303A
# endif /* !defined( EGL_BIND_TO_TEXTURE_RGBA ) */
# if !defined( EGL_MIN_SWAP_INTERVAL )
#  define EGL_MIN_SWAP_INTERVAL         0x303B
# endif /* !defined( EGL_MIN_SWAP_INTERVAL ) */
# if !defined( EGL_MAX_SWAP_INTERVAL )
#  define EGL_MAX_SWAP_INTERVAL         0x303C
# endif /* !defined( EGL_MAX_SWAP_INTERVAL ) */
# if !defined( EGL_MIPMAP_LEVEL )
#  define EGL_MIPMAP_LEVEL              0x3083
# endif	/* !defined( EGL_MIPMAP_LEVEL ) */
# define EGL_RENDER_BUFFER              0x3086
# define EGL_BACK_BUFFER                0x3084
# define EGL_SINGLE_BUFFER              0x3085
# define EGL_COLORSPACE                 0x3087
# define EGL_COLORSPACE_sRGB            0x3089
# define EGL_COLORSPACE_LINEAR          0x308A
# define EGL_ALPHA_FORMAT               0x3088
# define EGL_ALPHA_FORMAT_NONPRE        0x308B
# define EGL_ALPHA_FORMAT_PRE           0x308C
# define EGL_NO_TEXTURE                 0x305C
# define EGL_TEXTURE_RGB                0x305D
# define EGL_TEXTURE_RGBA               0x305E
# define EGL_TEXTURE_2D                 0x305F
# define EGL_OPENGL_ES_API              0x30A0
# define EGL_OPENVG_API                 0x30A1
# define EGL_RENDERABLE_TYPE            0x3040
# define EGL_MATCH_NATIVE_PIXMAP        0x3041
# define EGL_OPENGL_ES_BIT              0x01
# define EGL_OPENVG_BIT                 0x02
# define EGL_OPENVG_IMAGE               0x3096
#endif  /* EGL_VERSION_1_2 */

#if !defined( EGL_VERSION_1_1 )
# define EGL_CORE_NATIVE_ENGINE         0x305B
#endif  /* EGL_VERSION_1_1 */

/* Assorted hacks for getting EGL working in windows mobile */
#if defined( SCT_WINDOWS_MOBILE )

# define eglSurfaceAttrib( a, b, c, d )		EGL_FALSE
# define eglSwapInterval( a, b )			EGL_TRUE
# define eglBindTexImage( a, b, c )			EGL_FALSE
# define eglReleaseTexImage( a, b, c )      EGL_FALSE

# if !defined( EGL_TEXTURE_FORMAT )
#  define EGL_TEXTURE_FORMAT				0x3080
# endif	/* !defined( EGL_TEXTURE_FORMAT ) */

# if !defined( EGL_TEXTURE_TARGET )
#  define EGL_TEXTURE_TARGET				0x3081
# endif /* !defined( EGL_TEXTURE_TARGET ) */

# if !defined( EGL_MIPMAP_TEXTURE )
#  define EGL_MIPMAP_TEXTURE				0x3082
# endif /* !defined( EGL_MIPMAP_TEXTURE ) */

# if !defined( EGL_MIPMAP_LEVEL )
#  define EGL_MIPMAP_LEVEL					0x3083
# endif	/* !defined( EGL_MIPMAP_LEVEL ) */

# if !defined( EGL_BIND_TO_TEXTURE_RGB )
#  define EGL_BIND_TO_TEXTURE_RGB			0x3039
# endif	/* !defined( EGL_BIND_TO_TEXTURE_RGB ) */

# if !defined( EGL_BIND_TO_TEXTURE_RGBA )
#  define EGL_BIND_TO_TEXTURE_RGBA			0x303A
# endif	/* !defined( EGL_BIND_TO_TEXTURE_RGBA ) */

# if !defined( EGL_MAX_SWAP_INTERVAL )
#  define EGL_MAX_SWAP_INTERVAL				0x303C
# endif	/* !defined( EGL_MAX_SWAP_INTERVAL ) */

# if !defined( EGL_MIN_SWAP_INTERVAL )
#  define EGL_MIN_SWAP_INTERVAL				0x303B
# endif /* !defined( EGL_MIN_SWAP_INTERVAL ) */

#endif	/* defined( SCT_WINDOWS_MOBILE ) */

/* ************************************************************ */
/* EGL image */

#if !defined( EGL_KHR_image )   /* TODO: This may backfire in some environment. */
typedef void* EGLImageKHR;
#endif  /* !defined( EGL_KHR_image ) */

#if !defined( EGL_VERSION_1_3 )
typedef void* EGLClientBuffer;
#endif  /* !defined( EGL_VERSION_1_3 ) */

typedef EGLImageKHR ( *SCTPfnEglCreateImageKhr )( EGLDisplay dpy, EGLContext ctx, EGLenum target, EGLClientBuffer buffer, const EGLint* attrib_list );
typedef EGLBoolean  ( *SCTPfnEglDestroyImageKhr )( EGLDisplay dpy, EGLImageKHR image );

#if !defined( EGL_NATIVE_BUFFER )
#define EGL_NATIVE_BUFFER                           0x99FF
#endif  /* !defined( EGL_NATIVE_BUFFER ) */

#if !defined( EGL_NO_IMAGE_KHR )
# define EGL_NO_IMAGE_KHR                           ( ( EGLImageKHR )( 0 ) )
#endif /* !defined( EGL_NO_IMAGE_KHR ) */

#if !defined( EGL_IMAGE_PRESERVED_KHR )
# define EGL_IMAGE_PRESERVED_KHR                    0x30D2
#endif  /* !defined( EGL_IMAGE_PRESERVED_KHR ) */

#if !defined(  EGL_NATIVE_PIXMAP_KHR )
# define EGL_NATIVE_PIXMAP_KHR                      0x30B0
#endif  /* !defined(  EGL_NATIVE_PIXMAP_KHR ) */

#if !defined( EGL_VG_PARENT_IMAGE_KHR )
# define EGL_VG_PARENT_IMAGE_KHR                    0x30BA
#endif  /* !defined( EGL_VG_PARENT_IMAGE_KHR ) */

/* A small hack to separate EGL_GL_TEXTURE* into EGL_GL1_TEXTURE* and
 * EGL_GL2_TEXTURE*. */
#define EGL_GL_TEXTURE_MASK                         0xFFFF
#define EGL_GL1_TEXTURE_BITS                        0x10000
#define EGL_GL2_TEXTURE_BITS                        0x20000

#if !defined( EGL_GL_TEXTURE_2D_KHR )
# define EGL_GL_TEXTURE_2D_KHR                      0x30B1
#endif  /* !defined( EGL_GL_TEXTURE_2D_KHR ) */
# define EGL_GL1_TEXTURE_2D_KHR                     ( EGL_GL_TEXTURE_2D_KHR | EGL_GL1_TEXTURE_BITS )
# define EGL_GL2_TEXTURE_2D_KHR                     ( EGL_GL_TEXTURE_2D_KHR | EGL_GL2_TEXTURE_BITS )

#if !defined( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_X_KHR )
# define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_X_KHR     0x30B3
#endif  /* !defined( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_X_KHR ) */
# define EGL_GL1_TEXTURE_CUBE_MAP_POSITIVE_X_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_X_KHR | EGL_GL1_TEXTURE_BITS )
# define EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_X_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_X_KHR | EGL_GL2_TEXTURE_BITS )

#if !defined( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR )
# define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR     0x30B4
#endif  /* !defined( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR ) */
# define EGL_GL1_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR | EGL_GL1_TEXTURE_BITS )
# define EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR | EGL_GL2_TEXTURE_BITS )

#if !defined( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR )
# define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR     0x30B5
#endif  /* !defined( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR ) */
# define EGL_GL1_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR | EGL_GL1_TEXTURE_BITS )
# define EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR | EGL_GL2_TEXTURE_BITS )

#if !defined( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR )
# define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR     0x30B6
#endif  /* !defined( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR ) */
# define EGL_GL1_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR | EGL_GL1_TEXTURE_BITS )
# define EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR | EGL_GL2_TEXTURE_BITS )

#if !defined( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR )
# define EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR     0x30B7
#endif  /* !defined( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR ) */
# define EGL_GL1_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR | EGL_GL1_TEXTURE_BITS )
# define EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR | EGL_GL2_TEXTURE_BITS )

#if !defined( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR )
# define EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR     0x30B8
#endif  /* !defined( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR ) */
# define EGL_GL1_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR | EGL_GL1_TEXTURE_BITS )
# define EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR    ( EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR | EGL_GL2_TEXTURE_BITS )

#if !defined( EGL_GL_RENDERBUFFER_KHR )
#define EGL_GL_RENDERBUFFER_KHR                     0x30B9
#endif  /* !defined( EGL_GL_RENDERBUFFER_KHR ) */
#define EGL_GL1_RENDERBUFFER_KHR                    ( EGL_GL_RENDERBUFFER_KHR | EGL_GL1_TEXTURE_BITS )
#define EGL_GL2_RENDERBUFFER_KHR                    ( EGL_GL_RENDERBUFFER_KHR | EGL_GL2_TEXTURE_BITS )

/* ************************************************************ */
/* EGL fence sync */

#if !defined( EGL_SYNC_FENCE_KHR )  /* TODO: This may backfire in some environment. */
typedef void* EGLSyncKHR;
typedef unsigned long long EGLTimeKHR;
#endif  /* !defined( EGL_SYNC_FENCE_KHR ) */

typedef EGLSyncKHR ( *SCTPfnEglCreateSyncKhr )( EGLDisplay dpy, EGLenum type, const EGLint* attrib_list );
typedef EGLBoolean ( *SCTPfnEglDestroySyncKhr )( EGLDisplay dpy, EGLSyncKHR sync );
typedef EGLint     ( *SCTPfnEglClientWaitSyncKhr )( EGLDisplay dpy, EGLSyncKHR sync, EGLint flags, EGLTimeKHR timeout );

#if !defined( EGL_SYNC_FENCE_KHR )
# define EGL_SYNC_FENCE_KHR                        0x30F9 
#endif  /* !defined( EGL_SYNC_FENCE_KHR ) */

#if !defined( EGL_SYNC_FLUSH_COMMANDS_BIT_KHR )
# define EGL_SYNC_FLUSH_COMMANDS_BIT_KHR           0x0001
#endif  /* !defined( EGL_SYNC_FLUSH_COMMANDS_BIT_KHR ) */

#if !defined( EGL_FOREVER_KHR )
# define EGL_FOREVER_KHR                           0xFFFFFFFFFFFFFFFFull
#endif  /* !defined( EGL_FOREVER_KHR ) */

#if !defined( EGL_TIMEOUT_EXPIRED_KHR )
# define EGL_TIMEOUT_EXPIRED_KHR                   0x30F5
#endif  /* !defined( EGL_TIMEOUT_EXPIRED_KHR ) */

#if !defined( EGL_CONDITION_SATISFIED_KHR )
# define EGL_CONDITION_SATISFIED_KHR               0x30F6
#endif  /* !defined( EGL_CONDITION_SATISFIED_KHR ) */

#if !defined( EGL_NO_SYNC_KHR )
# define EGL_NO_SYNC_KHR                           ( ( EGLSyncKHR )( 0 ) )
#endif  /* !defined( EGL_NO_SYNC_KHR ) */

#endif /* !defined( __SCT_EGL_H__ ) */
