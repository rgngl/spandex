/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcurrentsurfaceaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCurrentSurfaceActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCurrentSurfaceActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCurrentSurfaceActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCurrentSurfaceActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CurrentSurface@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCurrentSurfaceActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCurrentSurfaceActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCurrentSurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CurrentSurface@Egl context creation." );
        sctiDestroyEglCurrentSurfaceActionContext( context );
        return NULL;
    }
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in CurrentSurface@Egl context creation." );
        sctiDestroyEglCurrentSurfaceActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCurrentSurfaceActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCurrentSurfaceActionInit( SCTAction* action, SCTBenchmark* benchmark )
{    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglCurrentSurfaceActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCurrentSurfaceActionContext*  context;
    EglCurrentSurfaceActionData*        data;
    EGLDisplay                          display;
    EGLSurface                          surface;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCurrentSurfaceActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CurrentSurface@Egl action execute." );
        return SCT_FALSE;
    }

    if( sctiEglModuleGetSurface( context->moduleContext, data->surfaceIndex ) != EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Surface index already in use in CurrentSurface@Egl action execute." );
        return SCT_FALSE;
    }

    surface = eglGetCurrentSurface( data->surface );

    if( surface == EGL_NO_SURFACE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Undefined current surface in CurrentSurface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    sctiEglModuleSetSurface( context->moduleContext, data->surfaceIndex, surface );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCurrentSurfaceActionTerminate( SCTAction* action )
{
    SCTEglCurrentSurfaceActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCurrentSurfaceActionContext* )( action->context );

    sctiEglModuleSetSurface( context->moduleContext, context->data.surfaceIndex, EGL_NO_SURFACE );
}

/*!
 *
 *
 */
void sctiEglCurrentSurfaceActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCurrentSurfaceActionContext( ( SCTEglCurrentSurfaceActionContext* )( action->context ) );
    action->context = NULL;
}

