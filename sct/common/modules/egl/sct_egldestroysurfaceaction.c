/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_egldestroysurfaceaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglDestroySurfaceActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglDestroySurfaceActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglDestroySurfaceActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglDestroySurfaceActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroySurface@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglDestroySurfaceActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglDestroySurfaceActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglDestroySurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in DestroySurface@Egl context creation." );
        sctiDestroyEglDestroySurfaceActionContext( context );        
        return NULL;
    }   
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in DestroySurface@Egl context creation." );
        sctiDestroyEglDestroySurfaceActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglDestroySurfaceActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglDestroySurfaceActionExecute( SCTAction* action, int framenumber )
{
    SCTEglDestroySurfaceActionContext*  context;
    EglDestroySurfaceActionData*        data;
    int                                 surfaceIndex;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglDestroySurfaceActionContext* )( action->context );
    data    = &( context->data );

    surfaceIndex = data->surfaceIndex;
    if( sctiEglModuleGetSurface( context->moduleContext, surfaceIndex ) == EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Invalid surface in DestroySurface@Egl action execute." );
        return SCT_FALSE;
    }

    sctiEglModuleDeleteSurface( context->moduleContext,
                                context->data.displayIndex,
                                surfaceIndex );    

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglDestroySurfaceActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglDestroySurfaceActionContext( ( SCTEglDestroySurfaceActionContext* )( action->context ) );
    action->context = NULL;
}

