/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_egldestroydataaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_egl.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglDestroyDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglDestroyDataActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglDestroyDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglDestroyDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyData@Egl action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglDestroyDataActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglDestroyDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglDestroyDataActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyEglDestroyDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in DestroyData@Egl context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglDestroyDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglDestroyDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglDestroyDataActionContext*  context;
    EglDestroyDataActionData*        data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTEglDestroyDataActionContext* )( action->context );
    data    = &( context->data );

    sctiEglModuleDeleteData( context->moduleContext, data->dataIndex );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglDestroyDataActionDestroy( SCTAction* action )
{
    SCTEglDestroyDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTEglDestroyDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyEglDestroyDataActionContext( context );
}
