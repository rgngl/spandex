/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglreleasedisplayaction.h"
#include "sct_sicommon.h"
#include "sct_siwindow.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglReleaseDisplayActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglReleaseDisplayActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglReleaseDisplayActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglReleaseDisplayActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ReleaseDisplay@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglReleaseDisplayActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglReleaseDisplayActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglReleaseDisplayActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in ReleaseDisplay@Egl context creation." );
        sctiDestroyEglReleaseDisplayActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglReleaseDisplayActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglReleaseDisplayActionExecute( SCTAction* action, int framenumber )
{
    SCTEglReleaseDisplayActionContext*  context;
    EglReleaseDisplayActionData*        data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglReleaseDisplayActionContext* )( action->context );
    data    = &( context->data );

    sctiEglModuleDeleteDisplay( context->moduleContext, data->displayIndex );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglReleaseDisplayActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglReleaseDisplayActionContext( ( SCTEglReleaseDisplayActionContext* )( action->context ) );
    action->context = NULL;
}
