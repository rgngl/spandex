/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreateimageextaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_OPENVG_MODULE )
# include "sct_openvgmodule.h"
#endif  /* defined( INCLUDE_OPENVG_MODULE ) */

#if defined( INCLUDE_OPENGLES1_MODULE )
# include "sct_opengles1module.h"
#endif  /* defined( INCLUDE_OPENGLES1_MODULE ) */

#if defined( INCLUDE_OPENGLES2_MODULE )
# include "sct_opengles2module.h"
#endif  /* defined( INCLUDE_OPENGLES2_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateEglCreateImageExtActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreateImageExtActionContext*  context;
    int                                 len;
    int                                 i;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreateImageExtActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreateImageExtActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateImageExt@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreateImageExtActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreateImageExtActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreateImageExtActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CreateImageExt@Egl context creation." );
        sctiDestroyEglCreateImageExtActionContext( context );
        return NULL;
    }   
    
    if( sctiEglModuleIsValidImageIndex( context->moduleContext, context->data.imageIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid image index in CreateImageExt@Egl context creation." );
        sctiDestroyEglCreateImageExtActionContext( context );
        return NULL;
    }

    if( context->data.contextIndex != -1 &&
        sctiEglModuleIsValidImageIndex( context->moduleContext, context->data.contextIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid context index in CreateImageExt@Egl context creation." );
        sctiDestroyEglCreateImageExtActionContext( context );
        return NULL;
    }

    if( context->data.target == EGL_NATIVE_PIXMAP_KHR &&
        sctiEglModuleIsValidPixmapIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in CreateImageExt@Egl context creation." );
        sctiDestroyEglCreateImageExtActionContext( context );        
        return NULL;
    }
    
    len = context->data.attributes->length;

    /* Reserve space for an EGL_NONE at the end. */
    context->eglAttributes = ( EGLint* )( siCommonMemoryAlloc( NULL, ( len + 1 ) * sizeof( EGLint ) ) );
    if( context->eglAttributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateImageExt@Egl context creation." );
        sctiDestroyEglCreateImageExtActionContext( context );        
        return NULL;
    }

    for( i = 0; i < len; ++i )
    {
        context->eglAttributes[ i ] = ( ( EGLint* )( context->data.attributes->data ) )[ i ];
    }

    context->eglAttributes[ len ] = EGL_NONE;   
    context->validated = SCT_FALSE;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreateImageExtActionContext( void* context )
{
    SCTEglCreateImageExtActionContext* c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTEglCreateImageExtActionContext* )( context );

    if( c->eglAttributes != NULL )
    {
        siCommonMemoryFree( NULL, c->eglAttributes );
        c->eglAttributes = NULL;
    }
    
    if( c->data.attributes != NULL )
    {
        sctDestroyIntVector( c->data.attributes );
        c->data.attributes = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateImageExtActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglCreateImageExtActionContext* context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglCreateImageExtActionContext* )( action->context );

    context->validated         = SCT_FALSE;
    context->eglCreateImageKhr = NULL;

#if !defined( EGL_VERSION_1_3 )
    SCT_LOG_ERROR( "CreateImageExt@Egl action not supported in the used EGL version." );
    return SCT_FALSE;
#else   /* !defined( EGL_VERSION_1_3 ) */   
    return SCT_TRUE;
#endif  /* !defined( EGL_VERSION_1_3 ) */    
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateImageExtActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreateImageExtActionContext*  context;
    EglCreateImageExtActionData*        data;
    EGLDisplay                          display;    
    EGLContext                          eglContext  = EGL_NO_CONTEXT;
    EGLClientBuffer                     buffer      = ( EGLClientBuffer )( 0 );
    unsigned int                        target;
    void*                               p;
    EGLImageKHR                         eglImage;
    EGLint*                             attribsPointer;
    const char*                         extString;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreateImageExtActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CreateImageExt@Egl action execute." );
        return SCT_FALSE;
    }
    
    if( context->validated == SCT_FALSE )
    {
        context->eglCreateImageKhr = ( SCTPfnEglCreateImageKhr )( siCommonGetProcAddress( NULL, "eglCreateImageKHR" ) );

        if( context->eglCreateImageKhr == NULL )
        {
            SCT_LOG_ERROR( "eglCreateImageKHR function not found in CreateImageExt@Egl action execute." );
            return SCT_FALSE;
        }

        extString = ( const char* )( eglQueryString( display, EGL_EXTENSIONS ) );
        if( extString == NULL || strstr( extString, "EGL_KHR_image_base" ) == NULL )
        {
            SCT_LOG_ERROR( "EGL_KHR_image_base extension not supported in CreateImageExt@Egl action execute." );
            return SCT_FALSE;
        }

        context->validated = SCT_TRUE;
    }
    
    if( sctiEglModuleGetImage( context->moduleContext, data->imageIndex ) != EGL_NO_IMAGE_KHR )
    {
        SCT_LOG_ERROR( "Image index already in use in CreateImageExt@Egl action execute." );        
        return SCT_FALSE;
    }
    
    if( data->contextIndex >= 0 )
    {
        eglContext = sctiEglModuleGetContext( context->moduleContext, data->contextIndex );
        
        if( eglContext == EGL_NO_CONTEXT )
        {
            SCT_LOG_ERROR( "Undefined context in CreateImageExt@Egl action execute." );
            return SCT_FALSE;
        }
    }

    target = ( unsigned int )( data->target ); 
    
    switch( data->target )
    {
    case EGL_NATIVE_BUFFER:
        p = sctiEglModuleGetPixmap( context->moduleContext, data->bufferIndex );
        if( p != NULL )
        {
            SCT_ASSERT( context->moduleContext->siWindowContext != NULL );
            buffer = ( EGLClientBuffer )( siWindowGetEglPixmap( context->moduleContext->siWindowContext, p, &target ) );
        }
        break;
        
    case EGL_NATIVE_PIXMAP_KHR:
        p = sctiEglModuleGetPixmap( context->moduleContext, data->bufferIndex );
        if( p != NULL )
        {
            SCT_ASSERT( context->moduleContext->siWindowContext != NULL );
            buffer = ( EGLClientBuffer )( siWindowGetEglPixmap( context->moduleContext->siWindowContext, p, NULL ) );
        }
        target &= EGL_GL_TEXTURE_MASK;
        break;
        
    case EGL_VG_PARENT_IMAGE_KHR:
#if defined( INCLUDE_OPENVG_MODULE )        
        buffer = ( EGLClientBuffer )( sctOpenVGModuleGetImageExt( data->bufferIndex ) );
        target &= EGL_GL_TEXTURE_MASK;        
        break;        
#else   /* defined( INCLUDE_OPENVG_MODULE ) */
        SCT_LOG_ERROR( "EGL_VG_PARENT_IMAGE_KHR target requires OpenVG module in CreateImageExt@Egl action execute." );
        return SCT_FALSE;
#endif  /* defined( INCLUDE_OPENVG_MODULE ) */

    case EGL_GL1_TEXTURE_2D_KHR:
    case EGL_GL1_TEXTURE_CUBE_MAP_POSITIVE_X_KHR:
    case EGL_GL1_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR:
    case EGL_GL1_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR:
    case EGL_GL1_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR:
    case EGL_GL1_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR:
    case EGL_GL1_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR:
#if defined( INCLUDE_OPENGLES1_MODULE )        
        buffer = ( EGLClientBuffer )( sctOpenGLES1ModuleGetTextureExt( data->bufferIndex ) );
        target &= EGL_GL_TEXTURE_MASK;        
        break;        
#else   /* defined( INCLUDE_OPENGLES1_MODULE ) */
        SCT_LOG_ERROR( "EGL_GL1* targets require OpenGLES1 module in CreateImageExt@Egl action execute." );
        return SCT_FALSE;
#endif  /* defined( INCLUDE_OPENGLES1_MODULE ) */

    case EGL_GL2_TEXTURE_2D_KHR:
    case EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_X_KHR:
    case EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR:
    case EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR:
    case EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR:
    case EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR:
    case EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR:
#if defined( INCLUDE_OPENGLES2_MODULE )        
        buffer = ( EGLClientBuffer )( sctOpenGLES2ModuleGetTextureExt( data->bufferIndex ) );
        target &= EGL_GL_TEXTURE_MASK;        
        break;        
#else   /* defined( INCLUDE_OPENGLES2_MODULE ) */
        SCT_LOG_ERROR( "EGL_GL2* targets require OpenGLES2 module in CreateImageExt@Egl action execute." );
        return SCT_FALSE;
#endif  /* defined( INCLUDE_OPENGLES2_MODULE ) */

    case EGL_GL2_RENDERBUFFER_KHR:
#if defined( INCLUDE_OPENGLES2_MODULE )        
        buffer = ( EGLClientBuffer )( sctOpenGLES2ModuleGetRenderbufferExt( data->bufferIndex ) );
        target &= EGL_GL_TEXTURE_MASK;        
        break;        
#else   /* defined( INCLUDE_OPENGLES2_MODULE ) */
        SCT_LOG_ERROR( "EGL_GL2* targets require OpenGLES2 module in CreateImageExt@Egl action execute." );
        return SCT_FALSE;
#endif  /* defined( INCLUDE_OPENGLES2_MODULE ) */
        
    default:
        SCT_LOG_ERROR( "Unexpected target in CreateImageExt@Egl action execute." );
        return SCT_FALSE;
    }

    if( buffer == ( EGLClientBuffer )( 0 ) )
    {
        SCT_LOG_ERROR( "Undefined buffer in CreateImageExt@Egl action execute." );
        return SCT_FALSE;
    }

    /* Some egl implementations might not support attributes at all so use NULL
     * pointer as attributes with default values.  */
    if( data->attributes->length == 0 )
    {
        attribsPointer = NULL;
    }
    else
    {
        SCT_ASSERT( context->eglAttributes != NULL );
        attribsPointer = context->eglAttributes;
    }
    
    SCT_ASSERT( context->eglCreateImageKhr != NULL );

    eglImage = context->eglCreateImageKhr( display, eglContext, ( EGLenum )( target ), buffer, attribsPointer );
    if( eglImage == EGL_NO_IMAGE_KHR )
    {
        char    buf[ 256 ];
        sprintf( buf, "Creating image failed in CreateImageExt@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    sctiEglModuleSetImage( context->moduleContext, data->imageIndex, eglImage );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreateImageExtActionTerminate( SCTAction* action )
{
    SCTEglCreateImageExtActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreateImageExtActionContext* )( action->context );

    sctiEglModuleDeleteImage( context->moduleContext,
                              context->data.displayIndex,
                              context->data.imageIndex,
                              NULL );

    context->validated         = SCT_FALSE;   
    context->eglCreateImageKhr = NULL;
}

/*!
 *
 *
 */
void sctiEglCreateImageExtActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreateImageExtActionContext( ( SCTEglCreateImageExtActionContext* )( action->context ) );
    action->context = NULL;
}

