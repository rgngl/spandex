/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglwaitclientaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_eglmodule.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglWaitClientActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglWaitClientActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglWaitClientActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglWaitClientActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in WaitClient@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglWaitClientActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglWaitClientActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglWaitClientActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglWaitClientActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglWaitClientActionExecute( SCTAction* action, int frameNumber )
{
    EGLBoolean  status;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );

#if !defined( EGL_VERSION_1_2 )
    status = eglWaitGL();
#else   /* !defined( EGL_VERSION_1_2 ) */
    status = eglWaitClient();
#endif  /* !defined( EGL_VERSION_1_2 ) */

    if( status != EGL_TRUE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Failure in WaitClient@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglWaitClientActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglWaitClientActionContext( ( SCTEglWaitClientActionContext* )( action->context ) );
    action->context = NULL;
}

