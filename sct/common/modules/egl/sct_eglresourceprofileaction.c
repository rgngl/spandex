/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglresourceprofileaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_eglmodule.h"

#include <stdio.h>
#include <string.h>

#if defined( _WIN32 )
# pragma warning ( disable : 4127 )
#endif  /* defined( _WIN32 ) */

#if !defined( EGL_PROF_QUERY_GLOBAL_BIT_NOK )
typedef unsigned int EGLNativeProcessIdTypeNOK;
#endif  /* !defined( EGL_PROF_QUERY_GLOBAL_BIT_NOK ) */

#if !defined( EGL_PROF_QUERY_GLOBAL_BIT_NOK )
# define EGL_PROF_QUERY_GLOBAL_BIT_NOK              0x0001
#endif  /* !defined( EGL_PROF_QUERY_GLOBAL_BIT_NOK ) */

#if !defined( EGL_PROF_QUERY_MEMORY_USAGE_BIT_NOK )
# define EGL_PROF_QUERY_MEMORY_USAGE_BIT_NOK        0x0002
#endif  /* !defined( EGL_PROF_QUERY_MEMORY_USAGE_BIT_NOK ) */

#if !defined( EGL_PROF_TOTAL_MEMORY_NOK )
# define EGL_PROF_TOTAL_MEMORY_NOK                  0x3070
#endif  /* !defined( EGL_PROF_TOTAL_MEMORY_NOK ) */

#if !defined( EGL_PROF_USED_MEMORY_NOK )
# define EGL_PROF_USED_MEMORY_NOK                   0x3071
#endif  /* !defined( EGL_PROF_USED_MEMORY_NOK ) */

#if !defined( EGL_PROF_PROCESS_ID_NOK )
# define EGL_PROF_PROCESS_ID_NOK                    0x3072
#endif  /* !defined( EGL_PROF_PROCESS_ID_NOK ) */

#if !defined( EGL_PROF_PROCESS_USED_PRIVATE_MEMORY_NOK )
# define EGL_PROF_PROCESS_USED_PRIVATE_MEMORY_NOK   0x3073
#endif  /* !defined( EGL_PROF_PROCESS_USED_PRIVATE_MEMORY_NOK ) */

#if !defined( EGL_PROF_PROCESS_USED_SHARED_MEMORY_NOK )
# define EGL_PROF_PROCESS_USED_SHARED_MEMORY_NOK    0x3074
#endif  /* !defined( EGL_PROF_PROCESS_USED_SHARED_MEMORY_NOK ) */

/*!
 *
 *
 */
void* sctiCreateEglResourceProfileActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglResourceProfileActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglResourceProfileActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglResourceProfileActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ResourceProfile@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglResourceProfileActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglResourceProfileActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglResourceProfileActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in ResourceProfile@Egl context creation." );
        sctiDestroyEglResourceProfileActionContext( context );        
        return NULL;
    }
    
    context->func          = NULL;
    context->funcValidated = SCT_FALSE;
        
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglResourceProfileActionContext( void* context )
{
    SCTEglResourceProfileActionContext* c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTEglResourceProfileActionContext* )( context );
    
    if( c->data.frameNumbers != NULL )
    {
        sctDestroyIntVector( c->data.frameNumbers );
        c->data.frameNumbers = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglResourceProfileActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglResourceProfileActionContext* context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglResourceProfileActionContext* )( action->context );
    
    if( sizeof( EGLNativeProcessIdTypeNOK ) != 4 &&
        sizeof( EGLNativeProcessIdTypeNOK ) != 8 )
    {
        SCT_LOG_ERROR( "ResourceProfiling@Egl action init unexpected native process id type." );
        return SCT_FALSE;
    }

    context->func = ( SCTEglQueryProfilingDataFunc )( siCommonGetProcAddress( NULL, "eglQueryProfilingDataNOK" ) );
    if( context->func == NULL )
    {
        SCT_LOG_ERROR( "Query profiling data function not found in ResourceProfile@Egl action init." );
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglResourceProfileActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglResourceProfileActionContext* context;
    EglResourceProfileActionData*       data;    
    EGLDisplay                          display;    
    EGLint*                             profileData;
    EGLint                              profileDataCount;
    const char*                         extString;
    int                                 i;
    SCTBoolean                          found;    
    SCTAttributeList*                   attributes;
    char                                message[ 256 ];

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    

    context = ( SCTEglResourceProfileActionContext* )( action->context );
    data    = &( context->data );

    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ResourceProfile@Egl action execute." );
        return SCT_FALSE;
    }
    
    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in ResourceProfile@Egl action execute." );
        return SCT_FALSE;
    }
           
    if( context->funcValidated == SCT_FALSE )
    {
        extString = ( const char* )( eglQueryString( display, EGL_EXTENSIONS ) );
        if( extString == NULL || strstr( extString, "EGL_NOK_resource_profiling" ) == NULL )
        {
            SCT_LOG_ERROR( "EGL_NOK_resource_profiling extension not supported in ResourceProfile@Egl action execute." );
            return SCT_FALSE;
        }
        context->funcValidated = SCT_TRUE;
    }

    if( data->frameNumbers->length > 0 )
    {
        /* Check if the current framenumber is in the framenumber vector. Use brute
           force search with the assumption that the number of elements in
           framenumber vector is limited. TODO: more efficient search?  */
        found = SCT_FALSE;
        for( i = 0; i < data->frameNumbers->length; ++i )
        {
            if( data->frameNumbers->data[ i ] == frameNumber )
            {
                found = SCT_TRUE;
                break;
            }
        }
    
        if( found == SCT_FALSE )
        {
            /* The current framenumber was not in the framenumber vector. */
            return SCT_TRUE;
        }
    }

    SCT_ASSERT( context->func != NULL );
    
    if( context->func( display,
                       EGL_PROF_QUERY_GLOBAL_BIT_NOK | EGL_PROF_QUERY_MEMORY_USAGE_BIT_NOK,
                       NULL,
                       0,
                       &profileDataCount ) == EGL_FALSE )
    {
        SCT_LOG_ERROR( "Getting profiling data count failed in ResourceProfile@Egl action execute." );
        return SCT_FALSE;
    }
    
    if( profileDataCount <= 0 )
    {
        SCT_LOG_ERROR( "Invalid profiling data count in ResourceProfile@Egl action execute." );
        return SCT_FALSE;
    }
    
    profileData = ( EGLint* )( siCommonMemoryAlloc( NULL, sizeof( EGLint ) * profileDataCount ) );
    if( profileData == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ResourceProfile@Egl action execute." );
        return SCT_FALSE;
    }

    if( context->func( display,
                       EGL_PROF_QUERY_GLOBAL_BIT_NOK | EGL_PROF_QUERY_MEMORY_USAGE_BIT_NOK,
                       profileData,
                       profileDataCount,
                       &profileDataCount ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        siCommonMemoryFree( NULL, profileData );
        sprintf( buf, "Getting profiling data failed in ResourceProfile@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

    sprintf( message, "%d", frameNumber );
    siCommonDebugPrintf( NULL, "SPANDEX ResourceProfile@Egl frame: %s", message );
    sctAddNameValueToList( attributes, "Frame", message );
    
    for( i = 0; i < profileDataCount; )
    {
        switch( profileData[ i ] )
        {
        case EGL_PROF_TOTAL_MEMORY_NOK:
            sprintf( message, "%d", profileData[ i + 1 ] );
            siCommonDebugPrintf( NULL, "SPANDEX ResourceProfile@Egl total memory: %s", message );
            sctAddNameValueToList( attributes, "Total memory", message );
            i += 2;
            break;
            
        case EGL_PROF_USED_MEMORY_NOK:
            sprintf( message, "%d", profileData[ i + 1 ] );
            siCommonDebugPrintf( NULL, "SPANDEX ResourceProfile@Egl used memory: %s", message );
            sctAddNameValueToList( attributes, "Used memory", message );
            i += 2;
            break;
            
        case EGL_PROF_PROCESS_ID_NOK:
            if( sizeof( EGLNativeProcessIdTypeNOK ) == 4 )
            {
                sprintf( message, "%08x", profileData[ i + 1 ] );
                siCommonDebugPrintf( NULL, "SPANDEX ResourceProfile@Egl process id: %s", message );
                sctAddNameValueToList( attributes, "Process id", message );
                i += 2;
            }
            else if( sizeof( EGLNativeProcessIdTypeNOK ) == 8 )
            {
                sprintf( message, "%08x%08x", profileData[ i + 2 ], profileData[ i + 1 ] );
                siCommonDebugPrintf( NULL, "SPANDEX ResourceProfile@Egl process id: %s", message );
                sctAddNameValueToList( attributes, "Process id", message );
                i += 3;
            }
            break;
            
        case EGL_PROF_PROCESS_USED_PRIVATE_MEMORY_NOK:
            sprintf( message, "%d", profileData[ i + 1 ] );
            siCommonDebugPrintf( NULL, "SPANDEX ResourceProfile@Egl private memory: %s", message );
            sctAddNameValueToList( attributes, "Private memory", message );
            i += 2;
            break;
            
        case EGL_PROF_PROCESS_USED_SHARED_MEMORY_NOK:
            sprintf( message, "%d", profileData[ i + 1 ] );
            siCommonDebugPrintf( NULL, "SPANDEX ResourceProfile@Egl shared memory: %s", message );
            sctAddNameValueToList( attributes, "Shared memory", message );
            i += 2;            
            break;
            
        default:
            sctDestroyAttributeList( attributes );
            siCommonMemoryFree( NULL, profileData );
            SCT_LOG_ERROR( "Unexpected profiling data token in ResourceProfile@Egl action execute." );
            return SCT_FALSE;
        }
    }

    if( attributes != NULL )
    {
        sctReportSection( "ResourceProfile", "Egl", attributes );                
        sctDestroyAttributeList( attributes );
    }
    
    siCommonMemoryFree( NULL, profileData );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglResourceProfileActionTerminate( SCTAction* action )
{
    SCTEglResourceProfileActionContext* context;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    

    context = ( SCTEglResourceProfileActionContext* )( action->context );
    
    context->func          = NULL;
    context->funcValidated = SCT_FALSE;   
}

/*!
 *
 *
 */
void sctiEglResourceProfileActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglResourceProfileActionContext( ( SCTEglResourceProfileActionContext* )( action->context ) );
    action->context = NULL;
}

