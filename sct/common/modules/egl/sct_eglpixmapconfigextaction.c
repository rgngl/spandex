/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglpixmapconfigextaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglPixmapConfigExtActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglPixmapConfigExtActionContext* context;
    int                                 len;
    int                                 i;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglPixmapConfigExtActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglPixmapConfigExtActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PixmapConfigExt@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglPixmapConfigExtActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglPixmapConfigExtActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglPixmapConfigExtActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in PixmapConfigExt@Egl context creation." );
        sctiDestroyEglPixmapConfigExtActionContext( context );
        return NULL;
    }
    
    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in PixmapConfigExt@Egl context creation." );
        sctiDestroyEglPixmapConfigExtActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidPixmapIndex( context->moduleContext, context->data.pixmapIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in PixmapConfigExt@Egl context creation." );
        sctiDestroyEglPixmapConfigExtActionContext( context );        
        return NULL;
    }

    len                          = context->data.attributes->length;    
    context->eglAttributesLength = len + 3;
    
    /* Reserve space for a EGL_MATCH_NATIVE_PIXMAP + pointer, and for EGL_NONE at the end. */
    context->eglAttributes = ( EGLint* )( siCommonMemoryAlloc( NULL, context->eglAttributesLength * sizeof( EGLint ) ) );
    if( context->eglAttributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PixmapConfigExt@Egl context creation." );
        sctiDestroyEglPixmapConfigExtActionContext( context );        
        return NULL;
    }

    /* Put pixmap pointer to the beginning to handle EGL_NONE in the attribute
     * list correctly. */
    context->pixmapIndex         = 1;
    context->eglAttributes[ 0 ] = EGL_MATCH_NATIVE_PIXMAP;
    context->eglAttributes[ 1 ] = ( EGLint )( NULL );
    
    for( i = 0; i < len; ++i )
    {
        context->eglAttributes[ i + 2 ] = ( ( EGLint* )( context->data.attributes->data ) )[ i ];
    }

    context->eglAttributes[ len + 2 ] = EGL_NONE;
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglPixmapConfigExtActionContext( void* context )
{
    SCTEglPixmapConfigExtActionContext* c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTEglPixmapConfigExtActionContext* )( context );
    
    if( c->eglAttributes != NULL )
    {
        siCommonMemoryFree( NULL, c->eglAttributes );
        c->eglAttributes = NULL;
    }
    
    if( c->data.attributes != NULL )
    {
        sctDestroyIntVector( c->data.attributes );
        c->data.attributes = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglPixmapConfigExtActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( EGL_VERSION_1_3 )
    SCT_LOG_ERROR( "PixmapConfigExt@Egl action init action not supported in the used EGL version." );
    return SCT_FALSE;
#else   /* !defined( EGL_VERSION_1_3 ) */   
    return SCT_TRUE;
#endif  /* !defined( EGL_VERSION_1_3 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiEglPixmapConfigExtActionExecute( SCTAction* action, int framenumber )
{
    SCTEglPixmapConfigExtActionContext* context;
    EglPixmapConfigExtActionData*       data;
    EGLint                              numConfigs;
    EGLDisplay                          display;
    EGLConfig                           config;
    void*                               pixmap;
    void*                               eglPixmap;    
    EGLint*                             eglAttributes;
    EGLBoolean                          s;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglPixmapConfigExtActionContext* )( action->context );
    data    = &( context->data );

    pixmap = sctiEglModuleGetPixmap( context->moduleContext, data->pixmapIndex );
    if( pixmap == NULL )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in CreatePixmapSurface@Egl action execute." );
        return SCT_FALSE;
    }
    eglPixmap = siWindowGetEglPixmap( sctEglModuleGetWindowContext( context->moduleContext ), pixmap, NULL );
    
    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in PixmapConfigExt@Egl action execute." );
        return SCT_FALSE;
    }
        
    SCT_ASSERT( context->eglAttributes != NULL );

    /* We make a copy from the eglAttributes at context to avoid potential
     * problems with multithreaded benchmarks. */
    eglAttributes = ( EGLint* )( siCommonMemoryAlloc( NULL, context->eglAttributesLength * sizeof( EGLint ) ) );
    if( eglAttributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PixmapConfigExt@Egl action execute." );
        return SCT_FALSE;
    }
    memcpy( eglAttributes, context->eglAttributes, context->eglAttributesLength * sizeof( EGLint ) );
        
    eglAttributes[ context->pixmapIndex ] = ( EGLint )( eglPixmap );
    
    s = eglChooseConfig( display, 
                         eglAttributes, 
                         &( config ), 
                         1, 
                         &numConfigs );

    siCommonMemoryFree( NULL, eglAttributes );

    if( s == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Choosing config failed in PixmapConfigExt@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
    
    if( numConfigs == 0 )
    {
        SCT_LOG_ERROR( "No config in PixmapConfigExt@Egl action execute." );
        return SCT_FALSE;
    }

    sctiEglModuleSetConfig( context->moduleContext, data->configIndex, config );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglPixmapConfigExtActionTerminate( SCTAction* action )
{
    SCTEglPixmapConfigExtActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTEglPixmapConfigExtActionContext* )( action->context );

    sctiEglModuleDeleteConfig( context->moduleContext, context->data.configIndex );
}

/*!
 *
 *
 */
void sctiEglPixmapConfigExtActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglPixmapConfigExtActionContext( ( SCTEglPixmapConfigExtActionContext* )( action->context ) );
    action->context = NULL;
}

