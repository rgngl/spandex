/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatechildwindowaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_siwindow.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreateChildWindowActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreateChildWindowActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreateChildWindowActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreateChildWindowActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateChildWindow@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreateChildWindowActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreateChildWindowActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreateChildWindowActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidWindowIndex( context->moduleContext, context->data.windowIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid window index in CreateChildWindow@Egl context creation." );
        sctiDestroyEglCreateChildWindowActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidWindowIndex( context->moduleContext, context->data.parentWindowIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid window index in CreateChildWindow@Egl context creation." );
        sctiDestroyEglCreateChildWindowActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreateChildWindowActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateChildWindowActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateChildWindowActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreateChildWindowActionContext*   context;
    EglCreateChildWindowActionData*         data;
    int                                     index;
    void*                                   parentWindow;    
    void*                                   window;
    int                                     width       = -1;
    int                                     height      = -1;
    void*                                   siWindowContext;
    SCTScreen                               screen;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreateChildWindowActionContext* )( action->context );
    data    = &( context->data );

    index = data->windowIndex;
    if( sctiEglModuleGetWindow( context->moduleContext, index ) != NULL )
    {
        SCT_LOG_ERROR( "Window index already in use in CreateChildWindow@Egl action execute." );
        return SCT_FALSE;
    }

    parentWindow = sctiEglModuleGetWindow( context->moduleContext, data->parentWindowIndex );
    if( parentWindow == NULL )
    {
        SCT_LOG_ERROR( "Undefined parent window in CreateChildWindow@Egl action execute." );
        return SCT_FALSE;
    }

    siWindowContext = sctEglModuleGetWindowContext( context->moduleContext );
    screen          = siWindowGetWindowScreen( siWindowContext, parentWindow );
    
    if( data->width < 0 || data->height < 0 )
    {
        siWindowGetScreenSize( siWindowContext,
                               screen,
                               &width, &height );
    }
    
    if( data->width >= 0 )
    {
        width  = data->width;
    }

    if( data->height >= 0 )
    {
        height  = data->height;
    }  
    
    window = siWindowCreateWindow( siWindowContext,
                                   screen,
                                   data->x, 
                                   data->y, 
                                   width, 
                                   height, 
                                   data->format,
                                   parentWindow );
    
#if !defined( SCT_EGL_MODULE_ALLOW_NULL_WINDOW_HANDLE )
    if( window == NULL )
    {
        SCT_LOG_ERROR( "Window creation failed in CreateChildWindow@Egl action execute." );
        return SCT_FALSE;
    }
#endif  /* !defined( SCT_EGL_MODULE_ALLOW_NULL_WINDOW_HANDLE ) */
    
    sctiEglModuleSetWindow( context->moduleContext, index, window );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreateChildWindowActionTerminate( SCTAction* action )
{
    SCTEglCreateChildWindowActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTEglCreateChildWindowActionContext* )( action->context );

    sctiEglModuleDeleteWindow( context->moduleContext, context->data.windowIndex );
}

/*!
 *
 *
 */
void sctiEglCreateChildWindowActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreateChildWindowActionContext( ( SCTEglCreateChildWindowActionContext* )( action->context ) );
    action->context = NULL;
}

