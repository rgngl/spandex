/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatepixmapsurfaceextaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreatePixmapSurfaceExtActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreatePixmapSurfaceExtActionContext*  context;
    int                                         len;
    int                                         i;      

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreatePixmapSurfaceExtActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreatePixmapSurfaceExtActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePixmapSurfaceExt@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreatePixmapSurfaceExtActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreatePixmapSurfaceExtActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreatePixmapSurfaceExtActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CreatePixmapSurfaceExt@Egl context creation." );
        sctiDestroyEglCreatePixmapSurfaceExtActionContext( context );        
        return NULL;
    }
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in CreatePixmapSurfaceExt@Egl context creation." );
        sctiDestroyEglCreatePixmapSurfaceExtActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in CreatePixmapSurfaceExt@Egl context creation." );
        sctiDestroyEglCreatePixmapSurfaceExtActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidPixmapIndex( context->moduleContext, context->data.pixmapIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in CreatePixmapSurfaceExt@Egl context creation." );
        sctiDestroyEglCreatePixmapSurfaceExtActionContext( context );
        return NULL;
    }

    len = context->data.attributes->length;

    /* Reserve space for an EGL_NONE at the end. */
    context->eglAttributes = ( EGLint* )( siCommonMemoryAlloc( NULL, ( len + 1 ) * sizeof( EGLint ) ) );
    if( context->eglAttributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePixmapSurfaceExt@Egl context creation." );
        sctiDestroyEglCreatePixmapSurfaceExtActionContext( context );
        return NULL;
    }

    for( i = 0; i < len; ++i )
    {
        context->eglAttributes[ i ] = ( ( EGLint* )( context->data.attributes->data ) )[ i ];
    }

    context->eglAttributes[ len ] = EGL_NONE;
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreatePixmapSurfaceExtActionContext( void* context )
{
    SCTEglCreatePixmapSurfaceExtActionContext* c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTEglCreatePixmapSurfaceExtActionContext* )( context );

    if( c->eglAttributes != NULL )
    {
        siCommonMemoryFree( NULL, c->eglAttributes );
        c->eglAttributes = NULL;
    }
    
    if( c->data.attributes != NULL )
    {
        sctDestroyIntVector( c->data.attributes );
        c->data.attributes = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePixmapSurfaceExtActionInit( SCTAction* action, SCTBenchmark* benchmark )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );
   
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePixmapSurfaceExtActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreatePixmapSurfaceExtActionContext*  context;
    EglCreatePixmapSurfaceExtActionData*        data;
    int                                         surfaceIndex;
    int                                         configIndex;
    int                                         pixmapIndex;
    EGLDisplay                                  display;
    EGLConfig                                   config;
    EGLSurface                                  surface;
    void*                                       pixmap;
    void*                                       eglPixmap;
    EGLint*                                     attribsPointer;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreatePixmapSurfaceExtActionContext* )( action->context );
    data    = &( context->data );

    surfaceIndex = data->surfaceIndex;
    configIndex  = data->configIndex;
    pixmapIndex  = data->pixmapIndex;

    /* Some egl implementations do not support attributes at all so use NULL
     * pointer as attributes with default values.  */
    if( data->attributes->length == 0 )
    {
        attribsPointer = NULL;
    }
    else
    {
        SCT_ASSERT( context->eglAttributes != NULL );
        attribsPointer = context->eglAttributes;
    }

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CreatePixmapSurfaceExt@Egl action execute." );
        return SCT_FALSE;
    }

    if( sctiEglModuleGetSurface( context->moduleContext, surfaceIndex ) != EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Surface index already in use in CreatePixmapSurfaceExt@Egl action execute." );
        return SCT_FALSE;
    }

    config = sctiEglModuleGetConfig( context->moduleContext, configIndex );

    pixmap = sctiEglModuleGetPixmap( context->moduleContext, pixmapIndex );
    if( pixmap == NULL )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in CreatePixmapSurfaceExt@Egl action execute." );
        return SCT_FALSE;
    }
    eglPixmap = siWindowGetEglPixmap( sctEglModuleGetWindowContext( context->moduleContext ), pixmap, NULL );
    
    surface = eglCreatePixmapSurface( display, config, ( NativePixmapType )( eglPixmap ), attribsPointer );
    if( surface == EGL_NO_SURFACE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Creating pixmap surface failed in CreatePixmapSurfaceExt@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    sctiEglModuleSetSurface( context->moduleContext, surfaceIndex, surface );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreatePixmapSurfaceExtActionTerminate( SCTAction* action )
{
    SCTEglCreatePixmapSurfaceExtActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreatePixmapSurfaceExtActionContext* )( action->context );

    sctiEglModuleDeleteSurface( context->moduleContext,
                                context->data.displayIndex,
                                context->data.surfaceIndex );    
}

/*!
 *
 *
 */
void sctiEglCreatePixmapSurfaceExtActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreatePixmapSurfaceExtActionContext( ( SCTEglCreatePixmapSurfaceExtActionContext* )( action->context ) );
    action->context = NULL;
}

