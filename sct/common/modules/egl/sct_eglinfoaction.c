/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglinfoaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglInfoActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglInfoActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglInfoActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglInfoActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Info@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglInfoActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglInfoActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglInfoActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in Info@Egl context creation." );
        sctiDestroyEglInfoActionContext( context );
        return NULL;
    }

    context->reported = SCT_FALSE;
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglInfoActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglInfoActionExecute( SCTAction* action, int framenumber )
{
    SCTEglInfoActionContext*    context;
    EglInfoActionData*          data;
    EGLDisplay                  display;
    SCTAttributeList*           attributes;
    EGLConfig*                  configIds;     
    EGLint                      maxNumConfigs               = 0;
    EGLint                      numConfigs                  = 0;
    int                         i                           = 0;
    char                        name[ 32 ];
    char                        value[ 1024 ];
    const char*                 str;    
    SCTBoolean                  status                      = SCT_TRUE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context       = ( SCTEglInfoActionContext* )( action->context );
    data          = &( context->data );

    if( context->reported == SCT_TRUE )
    {
        return SCT_TRUE;
    }
        
    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in Info@Egl action execute." );
        return SCT_FALSE;
    }

    /* Get the number of EGL configs. */
    if( eglGetConfigs( display, NULL, 0, &maxNumConfigs ) == EGL_FALSE )
    {
        SCT_LOG_ERROR( "Getting the number of configs failed in Info@Egl action execute." );
        return SCT_FALSE;
    }

    if( maxNumConfigs == 0 )
    {
        SCT_LOG_ERROR( "No configs in Info@Egl action execute." );
        return SCT_FALSE;
    }

    /* Allocate space for EGL configs. */
    configIds  = ( EGLConfig* )( siCommonMemoryAlloc( NULL, maxNumConfigs * sizeof( EGLConfig ) ) );
    if( configIds == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Info@Egl action execute." );        
        return SCT_FALSE;
    }
    memset( configIds, 0, maxNumConfigs * sizeof( EGLConfig ) );

    /* Get all EGL configs. */
    if( eglGetConfigs( display, configIds, maxNumConfigs, &numConfigs ) == EGL_FALSE )
    {
        siCommonMemoryFree( NULL, configIds );
        SCT_LOG_ERROR( "Getting the number of matching configs failed in Info@Egl action execute." );        
        return SCT_FALSE;
    }
    SCT_ASSERT_ALWAYS( numConfigs > 0 );

    /* Start collecting data to attribute list. */
    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        siCommonMemoryFree( NULL, configIds );
        SCT_LOG_ERROR( "Allocation failed in Info@Egl action execute." );        
        return SCT_FALSE;
    }

    /* Store EGL vendor, version, extension, and client api strings to attribute list. */
    str = eglQueryString( display, EGL_VENDOR );
    if( str == NULL )
    {
        str = "";
    }
    status = ( sctAddNameValueToList( attributes, "Vendor", str )       == SCT_FALSE ? SCT_FALSE : status );

    str = eglQueryString( display, EGL_VERSION );
    if( str == NULL )
    {
        str = "";
    }
    status = ( sctAddNameValueToList( attributes, "Version", str )      == SCT_FALSE ? SCT_FALSE : status );

    str = eglQueryString( display, EGL_EXTENSIONS );
    if( str == NULL )
    {
        str = "";
    }
    status = ( sctAddNameValueToList( attributes, "Extensions", str )   == SCT_FALSE ? SCT_FALSE : status );

#if defined( EGL_VERSION_1_2 )
    str = eglQueryString( display, EGL_CLIENT_APIS );
    if( str == NULL )
    {
        str = "";
    }
    status = ( sctAddNameValueToList( attributes, "Client APIs", str )  == SCT_FALSE ? SCT_FALSE : status );
#endif  /* EGL_VERSION_1_2 */

    /*  Store all EGL configs to attribute list. */
    if( status == SCT_TRUE )
    {
        for( i = 0; i < numConfigs; ++i )
        {
            if( sctiEglGetConfigNameValue( display, ( EGLConfig )( configIds[ i ] ), name, sizeof( name ), value, sizeof( value ) ) == SCT_FALSE )
            {
                status = SCT_FALSE;
                break;
            }

            if( sctAddNameValueToList( attributes, name, value ) == SCT_FALSE )
            {
                status = SCT_FALSE;
                break;
            }
        }
    }

    siCommonMemoryFree( NULL, configIds );

    if( status == SCT_FALSE )
    {
        sctDestroyAttributeList( attributes );
        SCT_LOG_ERROR( "Getting egl information failed in Info@EGL action execute." );        
        return SCT_FALSE;
    }

    if( sctReportSection( "EGL", "MODULE", attributes ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Report section failed in Info@EGL action execute." );
        status = SCT_FALSE;
    }

    sctDestroyAttributeList( attributes );

    context->reported = SCT_TRUE;
    
    return status;
}

/*!
 *
 *
 */
void sctiEglInfoActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT( action->context != NULL );    

    sctiDestroyEglInfoActionContext( ( SCTEglInfoActionContext* )( action->context ) );
    action->context = NULL;
}

