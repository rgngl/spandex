/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatecontextaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreateContextActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreateContextActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreateContextActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreateContextActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateContext@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreateContextActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreateContextActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreateContextActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CreateContext@Egl context creation." );
        sctiDestroyEglCreateContextActionContext( context );
        return NULL;
    }   
    
    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in CreateContext@Egl context creation." );
        sctiDestroyEglCreateContextActionContext( context );
        return NULL;
    }

    if( context->data.contextIndex != -1 &&
        sctiEglModuleIsValidContextIndex( context->moduleContext, context->data.contextIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid context index in CreateContext@Egl context creation." );
        sctiDestroyEglCreateContextActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreateContextActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateContextActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglCreateContextActionContext*   context;
    EglCreateContextActionData*         data;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglCreateContextActionContext* )( action->context );
    data    = &( context->data );
    
#if !defined( EGL_VERSION_1_3 )
    if( data->clientVersion > 1 )
    {
        SCT_LOG_ERROR( "CreateContext@Egl action init ClientVersion attribute not supported in the used EGL version." );
        return SCT_FALSE;
    }
#else   /* !defined( EGL_VERSION_1_3 ) */
    SCT_USE_VARIABLE( data );
#endif  /* !defined( EGL_VERSION_1_3 ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateContextActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreateContextActionContext*   context;
    EglCreateContextActionData*         data;
    EGLDisplay                          display;
    int                                 configIndex;
    EGLConfig                           config;
    int                                 contextIndex;
    int                                 shareContextIndex;
    EGLContext                          shareContext        = EGL_NO_CONTEXT;
    EGLContext                          eglContext;
    EGLint                              attributes[ 3 ];

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreateContextActionContext* )( action->context );
    data    = &( context->data );

    configIndex       = data->configIndex;
    contextIndex      = data->contextIndex;
    shareContextIndex = data->shareContextIndex;

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CreateContextExt@Egl action execute." );
        return SCT_FALSE;
    }

    config = sctiEglModuleGetConfig( context->moduleContext, configIndex );
    
    if( sctiEglModuleGetContext( context->moduleContext, contextIndex ) != EGL_NO_CONTEXT )
    {
        SCT_LOG_ERROR( "Context index already in use in CreateContext@Egl action execute." );
        return SCT_FALSE;
    }

    if( shareContextIndex >= 0 )
    {
        shareContext = sctiEglModuleGetContext( context->moduleContext, shareContextIndex );
        if( shareContext == EGL_NO_CONTEXT )
        {
            SCT_LOG_ERROR( "Invalid share context index in CreateContext@Egl action execute." );
            return SCT_FALSE;
        }
    }

#if defined( EGL_VERSION_1_3 )
    if( data->clientVersion < 0 )
    {
        attributes[ 0 ] = EGL_NONE;
    }
    else
    {
        attributes[ 0 ] = EGL_CONTEXT_CLIENT_VERSION;
        attributes[ 1 ] = data->clientVersion;
        attributes[ 2 ] = EGL_NONE;
    }
#else   /* defined( EGL_VERSION_1_3 ) */
    attributes[ 0 ] = EGL_NONE;
#endif  /* defined( EGL_VERSION_1_3 ) */

    eglContext = eglCreateContext( display, config, shareContext, attributes );

    if( eglContext == EGL_NO_CONTEXT )
    {
        char    buf[ 256 ];
        sprintf( buf, "Creating context failed in CreateContext@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    sctiEglModuleSetContext( context->moduleContext, contextIndex, eglContext );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreateContextActionTerminate( SCTAction* action )
{
    SCTEglCreateContextActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreateContextActionContext* )( action->context );

    sctiEglModuleDeleteContext( context->moduleContext,
                                context->data.displayIndex,
                                context->data.contextIndex );    
}

/*!
 *
 *
 */
void sctiEglCreateContextActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreateContextActionContext( ( SCTEglCreateContextActionContext* )( action->context ) );
    action->context = NULL;
}

