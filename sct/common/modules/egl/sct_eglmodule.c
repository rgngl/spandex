/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglmodule.h"
#include "sct_siwindow.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_egl.h"

#include "sct_eglmodule_parser.h"
#include "sct_eglmodule_actions.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
SCTAttributeList*   sctiEglModuleInfo( SCTModule* module );
SCTAction*          sctiEglModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                sctiEglModuleDestroy( SCTModule* module );

/*!
 *
 */
SCTBoolean sctEglModuleSetDataExt( int index, SCTEglData* data )
{
    SCTEglModuleContext*    context;
    SCTBoolean              pointerRegistered   = SCT_FALSE;

    context = ( SCTEglModuleContext* )( sctGetRegisteredPointer( SCT_EGL_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || context == NULL )
    {
        return SCT_FALSE;
    }

    if( sctiEglModuleIsValidDataIndex( context, index ) == SCT_FALSE )    
    {
        return SCT_FALSE;
    }
    
    sctiEglModuleDeleteData( context, index );
    sctiEglModuleSetData( context, index, data );

    return SCT_TRUE;
}

/*!
 *
 */
EGLImageKHR sctEglModuleGetImageExt( int index )
{
    SCTEglModuleContext*    context;
    SCTBoolean              pointerRegistered   = SCT_FALSE;

    if( index < 0 || index >= SCT_MAX_EGL_IMAGES )
    {
        return EGL_NO_IMAGE_KHR;
    }

    context = ( SCTEglModuleContext* )( sctGetRegisteredPointer( SCT_EGL_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || context == NULL )
    {
        return EGL_NO_IMAGE_KHR;
    }
   
    return context->eglImages[ index ];
}

/*!
 *
 *
 */
SCTModule* sctCreateEglModule( void )
{
    SCTModule*              module;
    SCTEglModuleContext*    context;
    int                     i;

    context = ( SCTEglModuleContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglModuleContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglModuleContext ) );

    for( i = 0; i < SCT_MAX_EGL_DISPLAYS; ++i )
    {
        context->eglDisplays[ i ] = EGL_NO_DISPLAY;
    }
    
    for( i = 0; i < SCT_MAX_EGL_CONFIGS; ++i )
    {
        context->eglConfigs[ i ] = ( EGLConfig )( 0 );
    }

    for( i = 0; i < SCT_MAX_EGL_WINDOWS; ++i )
    {
        context->eglWindows[ i ] = NULL;
    }
    
    for( i = 0; i < SCT_MAX_EGL_PIXMAPS; ++i )
    {
        context->eglPixmaps[ i ] = NULL;
    }

    for( i = 0; i < SCT_MAX_EGL_SURFACES; ++i )
    {
        context->eglSurfaces[ i ] = EGL_NO_SURFACE;
    }

    for( i = 0; i < SCT_MAX_EGL_CONTEXTS; ++i )
    {
        context->eglContexts[ i ] = EGL_NO_CONTEXT;
    }

    for( i = 0; i < SCT_MAX_EGL_CONTEXTS; ++i )
    {
        context->eglContexts[ i ] = EGL_NO_CONTEXT;
    }

    for( i = 0; i < SCT_MAX_EGL_IMAGES; ++i )
    {
        context->eglImages[ i ] = EGL_NO_IMAGE_KHR;
    }

    for( i = 0; i < SCT_MAX_EGL_SYNCS; ++i )
    {
        context->eglSyncs[ i ] = EGL_NO_SYNC_KHR;
    }

    module = sctCreateModule( "Egl",
                              context,
#if defined( _WIN32 )
                              _egl_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiEglModuleInfo,
                              sctiEglModuleCreateAction,
                              sctiEglModuleDestroy );

    if( module == NULL )
    {
        siCommonMemoryFree( NULL, context );
        return NULL;
    }

    context->mutex = siCommonCreateMutex( NULL );
    if( context->mutex == NULL )
    {
        sctiEglModuleDestroy( module );
        siCommonMemoryFree( NULL, module );
        
        return NULL;
    }
    
    context->siWindowContext = siWindowCreateContext();
    if( context->siWindowContext == NULL )
    {
        sctiEglModuleDestroy( module );
        siCommonMemoryFree( NULL, module );
        return NULL;
    }

    if( sctRegisterPointer( SCT_EGL_MODULE_POINTER, context ) == SCT_FALSE )
    {
        sctiEglModuleDestroy( module );
        sctDestroyModule( module );
        return NULL;
    }
    
    return module;
}

/*!
 *
 *
 */
void sctEglModuleLock( SCTEglModuleContext* moduleContext )
{
    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    siCommonLockMutex( NULL, moduleContext->mutex );
}

/*!
 *
 *
 */
void sctEglModuleUnlock( SCTEglModuleContext* moduleContext )
{
    SCT_ASSERT_ALWAYS( moduleContext != NULL );    
    siCommonUnlockMutex( NULL, moduleContext->mutex );
}

/*!
 *
 *
 */
SCTAttributeList* sctiEglModuleInfo( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );

    return sctCreateAttributeList();
}

/*!
 *
 *
 */
SCTAction* sctiEglModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCTEglModuleContext*    moduleContext;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    moduleContext = ( SCTEglModuleContext* )( module->context );

    SCT_ASSERT_ALWAYS( moduleContext != NULL );

    return sctCreateActionFromTemplate( name, type, "Egl", moduleContext, attributes, EglActionTemplates, SCT_ARRAY_LENGTH( EglActionTemplates ) );
}

/*!
 *
 *
 */
void sctiEglModuleDestroy( SCTModule* module )
{
    SCTEglModuleContext*    context;
    int                     i;

    SCT_ASSERT_ALWAYS( module != NULL );

    context = ( SCTEglModuleContext* )( module->context );

    if( context == NULL )
    {
        return;
    }

    sctUnregisterPointer( SCT_EGL_MODULE_POINTER );
    
    for( i = 0; i < SCT_MAX_EGL_DISPLAYS; ++i )
    {
        sctiEglModuleDeleteDisplay( context, i );
        SCT_ASSERT_ALWAYS( context->eglDisplays[ i ] == EGL_NO_DISPLAY );
    }
    
    for( i = 0; i < SCT_MAX_EGL_CONFIGS; ++i )
    {
        SCT_ASSERT_ALWAYS( context->eglConfigs[ i ] == ( EGLConfig )( 0 ) );
    }
    for( i = 0; i < SCT_MAX_EGL_WINDOWS; ++i )
    {
        SCT_ASSERT_ALWAYS( context->eglWindows[ i ] == NULL );
    }   
    for( i = 0; i < SCT_MAX_EGL_PIXMAPS; ++i )
    {
        SCT_ASSERT_ALWAYS( context->eglPixmaps[ i ] == NULL );
    }
    for( i = 0; i < SCT_MAX_EGL_SURFACES; ++i )
    {
        SCT_ASSERT_ALWAYS( context->eglSurfaces[ i ] == EGL_NO_SURFACE );
    }
    for( i = 0; i < SCT_MAX_EGL_CONTEXTS; ++i )
    {
        SCT_ASSERT_ALWAYS( context->eglContexts[ i ] == EGL_NO_CONTEXT );
    }
    for( i = 0; i < SCT_MAX_EGL_IMAGES; ++i )
    {
        SCT_ASSERT_ALWAYS( context->eglImages[ i ] == EGL_NO_IMAGE_KHR );
    }
    for( i = 0; i < SCT_MAX_EGL_SYNCS; ++i )
    {
        SCT_ASSERT_ALWAYS( context->eglSyncs[ i ] == EGL_NO_SYNC_KHR );
    }

    if( context->siWindowContext != NULL )
    {
        siWindowDestroyContext( context->siWindowContext );
        context->siWindowContext = NULL;
    }

    if( context->mutex != NULL )
    {
        siCommonDestroyMutex( NULL, context->mutex );
        context->mutex = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
    module->context = NULL;
}

/*!
 *
 *
 */
SCTEglGetProcAddressFunc sctEglGetProcAddress( const char* procname )
{
#if !defined( SCT_SKIP_EGL_PROG_ADDRESS )
    return ( SCTEglGetProcAddressFunc )( eglGetProcAddress( procname ) );
#else   /* !defined( SCT_SKIP_EGL_PROG_ADDRESS ) */
    return NULL;
#endif  /* !defined( SCT_SKIP_EGL_PROG_ADDRESS ) */

}

/*!
 *
 *
 */
SCTEglData* sctEglCreateData( void* data, int length, SCTBoolean copy )
{
    SCTEglData* d;

    SCT_ASSERT_ALWAYS( data != NULL );
    SCT_ASSERT_ALWAYS( length > 0 );

    d = ( SCTEglData* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglData ) ) );
    if( d == NULL )
    {
        return NULL;
    }
    
    if( copy == SCT_TRUE )
    {
        d->data = siCommonMemoryAlloc( NULL, length );
        if( d->data == NULL )
        {
            siCommonMemoryFree( NULL, d );
            return NULL;
        }
        memcpy( d->data, data, length );
    }
    else
    {
        d->data = data;
    }

    d->length = length;

    return d;
}

/*!
 *
 *
 */
void sctEglDestroyData( SCTEglData* data )
{
    if( data != NULL )
    {
        if( data->data != NULL )
        {
            siCommonMemoryFree( NULL, data->data );
            data->data = NULL;
        }
        siCommonMemoryFree( NULL, data );
    }
}

/*!
 *
 *
 */
SCTBoolean sctiEglModuleIsValidDataIndex( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_EGL_DATA_ELEMENTS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTEglData* sctiEglModuleGetData( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_EGL_DATA_ELEMENTS );

    if( index >= 0 && index < SCT_MAX_EGL_DATA_ELEMENTS )
    {
        return moduleContext->dataElements[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiEglModuleSetData( SCTEglModuleContext* moduleContext, int index, SCTEglData* data )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_EGL_DATA_ELEMENTS );

    if( index >= 0 && index < SCT_MAX_EGL_DATA_ELEMENTS )
    {
        moduleContext->dataElements[ index ] = data;
    }
}

/*!
 *
 *
 */
void sctiEglModuleDeleteData( SCTEglModuleContext* moduleContext, int index )
{
    SCTEglData* data;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_DATA_ELEMENTS )
    {
        data = moduleContext->dataElements[ index ];
        if( data != NULL )
        {
            sctEglDestroyData( data );
            moduleContext->dataElements[ index ] = NULL;
        }
    }
}

/*!
 *
 */
void* sctEglModuleGetWindowContext( SCTEglModuleContext* context )
{
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( context->siWindowContext != NULL );

    return context->siWindowContext;
}

/*!
 *
 */
SCTBoolean sctiEglModuleIsValidWindowIndex( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index < 0 || index >= SCT_MAX_EGL_WINDOWS )
    {
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 */
void* sctiEglModuleGetWindow( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_WINDOWS )
    {
        return moduleContext->eglWindows[ index ];
    }

    SCT_ASSERT_ALWAYS( 0 );
    return NULL; /* Never reached */
}

/*!
 *
 */
void sctiEglModuleSetWindow( SCTEglModuleContext* moduleContext, int index, void* window )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_EGL_WINDOWS );

    if( index >= 0 && index < SCT_MAX_EGL_WINDOWS )
    {
        moduleContext->eglWindows[ index ] = window;
    }
}

/*!
 *
 */
void sctiEglModuleDeleteWindow( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_WINDOWS )
    {
        if( moduleContext->eglWindows[ index ] != NULL )
        {
            siWindowDestroyWindow( moduleContext->siWindowContext, 
                                   moduleContext->eglWindows[ index ] );
            moduleContext->eglWindows[ index ] = NULL;            
        }
    }
}

/*!
 *
 */
SCTBoolean sctiEglModuleIsValidPixmapIndex( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index < 0 || index >= SCT_MAX_EGL_PIXMAPS )
    {
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 */
void* sctiEglModuleGetPixmap( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_PIXMAPS )
    {
        return moduleContext->eglPixmaps[ index ];
    }

    SCT_ASSERT_ALWAYS( 0 );
    return NULL; /* Never reached */
}

/*!
 *
 */
void sctiEglModuleSetPixmap( SCTEglModuleContext* moduleContext, int index, void* pixmap )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_EGL_PIXMAPS );

    if( index >= 0 && index < SCT_MAX_EGL_PIXMAPS )
    {
        moduleContext->eglPixmaps[ index ] = pixmap;
    }
}

/*!
 *
 */
void sctiEglModuleDeletePixmap( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_PIXMAPS )
    {
        if( moduleContext->eglPixmaps[ index ] != NULL )
        {
            siWindowDestroyPixmap( moduleContext->siWindowContext, 
                                   moduleContext->eglPixmaps[ index ] );
            moduleContext->eglPixmaps[ index ] = NULL;
        }
    }
}

/*!
 *
 */
SCTBoolean sctiEglModuleIsValidDisplayIndex( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index < 0 || index >= SCT_MAX_EGL_DISPLAYS )
    {
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 */
EGLDisplay sctiEglModuleGetDisplay( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_DISPLAYS )
    {
        return moduleContext->eglDisplays[ index ];
    }

    SCT_ASSERT_ALWAYS( 0 );
    return EGL_NO_DISPLAY; /* Never reached */
}

/*!
 *
 */
void sctiEglModuleSetDisplay( SCTEglModuleContext* moduleContext, int index, EGLDisplay eglDisplay )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_DISPLAYS )
    {
        sctiEglModuleDeleteDisplay( moduleContext, index );
        moduleContext->eglDisplays[ index ] = eglDisplay;
    }
    else
    {
        SCT_ASSERT_ALWAYS( 0 );
    }
}

/*!
 *
 */
void sctiEglModuleDeleteDisplay( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_DISPLAYS )
    {
        moduleContext->eglDisplays[ index ] = EGL_NO_DISPLAY;
    }
}

/*!
 *
 */
SCTBoolean sctiEglModuleIsValidConfigIndex( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index < 0 || index >= SCT_MAX_EGL_CONFIGS )
    {
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 */
EGLConfig sctiEglModuleGetConfig( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_CONFIGS )
    {
        return moduleContext->eglConfigs[ index ];
    }

    SCT_ASSERT_ALWAYS( 0 );
    return ( EGLConfig )( 0 ); /* Never reached */
}

/*!
 *
 */
void sctiEglModuleSetConfig( SCTEglModuleContext* moduleContext, int index, EGLConfig eglConfig )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_CONFIGS )
    {
        moduleContext->eglConfigs[ index ] = eglConfig;
    }
    else
    {
        SCT_ASSERT_ALWAYS( 0 );
    }
}

/*!
 *
 */
void sctiEglModuleDeleteConfig( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_CONFIGS )
    {
        moduleContext->eglConfigs[ index ] = ( EGLConfig )( 0 );
    }
}

/*!
 *
 */
SCTBoolean sctiEglModuleIsValidSurfaceIndex( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index < 0 || index >= SCT_MAX_EGL_SURFACES )
    {
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 */
EGLSurface sctiEglModuleGetSurface( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_SURFACES )
    {
        return moduleContext->eglSurfaces[ index ];
    }

    SCT_ASSERT_ALWAYS( 0 );
    return EGL_NO_SURFACE; /* Never reached */
}

/*!
 *
 */
void sctiEglModuleSetSurface( SCTEglModuleContext* moduleContext, int index, EGLSurface surface )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_EGL_SURFACES );

    if( index >= 0 && index < SCT_MAX_EGL_SURFACES )
    {
        moduleContext->eglSurfaces[ index ] = surface;
    }
}

/*!
 *
 */
void sctiEglModuleDeleteSurface( SCTEglModuleContext* moduleContext, int displayIndex, int surfaceIndex )
{
    EGLDisplay  eglDisplay   = EGL_NO_DISPLAY;
    
    SCT_ASSERT( moduleContext != NULL );

    if( sctiEglModuleIsValidDisplayIndex( moduleContext, displayIndex ) == SCT_TRUE )
    {
        eglDisplay = sctiEglModuleGetDisplay( moduleContext, displayIndex );
    }

    if( surfaceIndex >= 0 && surfaceIndex < SCT_MAX_EGL_SURFACES )    
    {
        if( moduleContext->eglSurfaces[ surfaceIndex ] != EGL_NO_SURFACE  )
        {
            if( eglDisplay != EGL_NO_DISPLAY )
            {
                eglDestroySurface( eglDisplay, moduleContext->eglSurfaces[ surfaceIndex ] );
            }
            moduleContext->eglSurfaces[ surfaceIndex ] = EGL_NO_SURFACE;
        }
    }
}

/*!
 *
 */
SCTBoolean sctiEglModuleIsValidContextIndex( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index < 0 || index >= SCT_MAX_EGL_CONTEXTS )
    {
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 */
EGLContext sctiEglModuleGetContext( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_CONTEXTS )
    {
        return moduleContext->eglContexts[ index ];
    }

    SCT_ASSERT_ALWAYS( 0 );
    return EGL_NO_CONTEXT; /* Never reached */
}

/*!
 *
 */
void sctiEglModuleSetContext( SCTEglModuleContext* moduleContext, int index, EGLContext context )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_EGL_CONTEXTS );

    if( index >= 0 && index < SCT_MAX_EGL_CONTEXTS )
    {
        moduleContext->eglContexts[ index ] = context;
    }
}

/*!
 *
 */
void sctiEglModuleDeleteContext( SCTEglModuleContext* moduleContext, int displayIndex, int contextIndex )
{
    EGLDisplay  eglDisplay   = EGL_NO_DISPLAY;
    
    SCT_ASSERT( moduleContext != NULL );

    if( sctiEglModuleIsValidDisplayIndex( moduleContext, displayIndex ) == SCT_TRUE )
    {
        eglDisplay = sctiEglModuleGetDisplay( moduleContext, displayIndex );
    }

    if( contextIndex >= 0 && contextIndex < SCT_MAX_EGL_CONTEXTS )    
    {
        if( moduleContext->eglContexts[ contextIndex ] != EGL_NO_CONTEXT  )
        {
            if( eglDisplay != EGL_NO_DISPLAY )
            {
                eglDestroyContext( eglDisplay, moduleContext->eglContexts[ contextIndex ] );
            }
            moduleContext->eglContexts[ contextIndex ] = EGL_NO_CONTEXT;
        }
    }   
}

/*!
 *
 */
SCTBoolean sctiEglModuleIsValidImageIndex( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index < 0 || index >= SCT_MAX_EGL_IMAGES )
    {
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 */
EGLImageKHR sctiEglModuleGetImage( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_IMAGES )
    {
        return moduleContext->eglImages[ index ];
    }

    SCT_ASSERT_ALWAYS( 0 );
    return EGL_NO_CONTEXT; /* Never reached */
}

/*!
 *
 */
void sctiEglModuleSetImage( SCTEglModuleContext* moduleContext, int index, EGLImageKHR image )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_EGL_IMAGES );

    if( index >= 0 && index < SCT_MAX_EGL_IMAGES )
    {
        moduleContext->eglImages[ index ] = image;
    }
}

/*!
 *
 */
void sctiEglModuleDeleteImage( SCTEglModuleContext* moduleContext, int displayIndex, int imageIndex, SCTPfnEglDestroyImageKhr func )
{
    EGLDisplay  eglDisplay   = EGL_NO_DISPLAY;
    
    SCT_ASSERT( moduleContext != NULL );

    if( sctiEglModuleIsValidDisplayIndex( moduleContext, displayIndex ) == SCT_TRUE )
    {
        eglDisplay = sctiEglModuleGetDisplay( moduleContext, displayIndex );
    }
    
    if( imageIndex >= 0 && imageIndex < SCT_MAX_EGL_IMAGES )    
    {
        if( moduleContext->eglImages[ imageIndex ] != EGL_NO_IMAGE_KHR )
        {
            if( func == NULL )
            {
                if( eglDisplay != EGL_NO_DISPLAY )
                {
                    func = ( SCTPfnEglDestroyImageKhr )( siCommonGetProcAddress( NULL, "eglDestroyImageKHR" ) );

                    if( func != NULL )
                    {
                        /* Paranoid-mode; to be on the safe side, this needs to be done 
                         * per-call as the used egl context may change in between the calls, 
                         * and apparently it is at least in theory possible that some
                         * contexts don't support all the extensions, hence the
                         * extension function pointers might not always work as
                         * expected. */
                        const char* extString;
                        extString = ( const char* )( eglQueryString( eglDisplay, EGL_EXTENSIONS ) );
                        if( extString == NULL || ( strstr( extString, "EGL_KHR_image_base" ) == NULL ) )
                        {
                           func = NULL;
                        }
                    }
                }
            }

            /* Something is wrong if we arrive here without proper func pointer. */
            SCT_ASSERT_ALWAYS( func != NULL );

            func( eglDisplay, moduleContext->eglImages[ imageIndex ] );
            moduleContext->eglImages[ imageIndex ] = EGL_NO_IMAGE_KHR;
        }
    }
}

/*!
 *
 */
SCTBoolean sctiEglModuleIsValidSyncIndex( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index < 0 || index >= SCT_MAX_EGL_SYNCS )
    {
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 */
EGLSyncKHR sctiEglModuleGetSync( SCTEglModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_EGL_SYNCS )
    {
        return moduleContext->eglSyncs[ index ];
    }

    SCT_ASSERT_ALWAYS( 0 );
    return EGL_NO_CONTEXT; /* Never reached */
}

/*!
 *
 */
void sctiEglModuleSetSync( SCTEglModuleContext* moduleContext, int index, EGLSyncKHR sync )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_EGL_SYNCS );

    if( index >= 0 && index < SCT_MAX_EGL_SYNCS )
    {
        moduleContext->eglSyncs[ index ] = sync;
    }
}

/*!
 *
 */
void sctiEglModuleDeleteSync( SCTEglModuleContext* moduleContext, int displayIndex, int syncIndex, SCTPfnEglDestroySyncKhr func )
{
    EGLDisplay  eglDisplay   = EGL_NO_DISPLAY;
    
    SCT_ASSERT( moduleContext != NULL );

    if( sctiEglModuleIsValidDisplayIndex( moduleContext, displayIndex ) == SCT_TRUE )
    {
        eglDisplay = sctiEglModuleGetDisplay( moduleContext, displayIndex );
    }
    
    if( syncIndex >= 0 && syncIndex < SCT_MAX_EGL_SYNCS )
    {
        if( moduleContext->eglSyncs[ syncIndex ] != EGL_NO_SYNC_KHR )
        {
            if( func == NULL )
            {
                if( eglDisplay != EGL_NO_DISPLAY )
                {
                    func = ( SCTPfnEglDestroySyncKhr )( siCommonGetProcAddress( NULL, "eglDestroySyncKHR" ) );
 
                    if( func != NULL )
                    {
                        /* Paranoid-mode; to be on the safe side, this needs to be done 
                         * per-call as the used egl context may change in between the 
                         * calls, and apparently it is at least in theory possible that 
                         * some contexts don't support all the extensions, hence the
                         * extension function pointers might not always work as
                         * expected. */
                        const char* extString;
                        extString = ( const char* )( eglQueryString( eglDisplay, EGL_EXTENSIONS ) );
                        if( extString == NULL || ( strstr( extString, "EGL_KHR_fence_sync" ) == NULL ) )
                        {
                            func = NULL;
                        }
                    }
                }
            }

            /* Something is very wrong if we arrive here without proper func pointer . */
            SCT_ASSERT_ALWAYS( func != NULL );

            func( eglDisplay, moduleContext->eglSyncs[ syncIndex ] );
            moduleContext->eglSyncs[ syncIndex ] = EGL_NO_SYNC_KHR;
        }
    }   
}

/*!
 *
 *
 */
const char* sctiEglGetErrorString( EGLint err )
{
    switch( err )
    {
    case EGL_SUCCESS:
        return "EGL_SUCCESS";
    case EGL_NOT_INITIALIZED:
		return "EGL_NOT_INITIALIZED";       
    case EGL_BAD_ACCESS:
        return "EGL_BAD_ACCESS";
    case EGL_BAD_ALLOC:
        return "EGL_BAD_ALLOC";
    case EGL_BAD_ATTRIBUTE:
        return "EGL_BAD_ATTRIBUTE";
    case EGL_BAD_CONFIG:
        return "EGL_BAD_CONFIG";
    case EGL_BAD_CONTEXT:
        return "EGL_BAD_CONTEXT";
    case EGL_BAD_CURRENT_SURFACE:
		return "EGL_BAD_CURRENT_SURFACE";
    case EGL_BAD_DISPLAY:
        return "EGL_BAD_DISPLAY";
    case EGL_BAD_MATCH:
        return "EGL_BAD_MATCH";
    case EGL_BAD_NATIVE_PIXMAP:
		return "EGL_BAD_NATIVE_PIXMAP";
    case EGL_BAD_NATIVE_WINDOW:
		return "EGL_BAD_NATIVE_WINDOW";
    case EGL_BAD_PARAMETER:
		return "EGL_BAD_PARAMETER";
    case EGL_BAD_SURFACE:
        return "EGL_BAD_SURFACE";
    case EGL_CONTEXT_LOST:
		return "EGL_CONTEXT_LOST";
    default:
        return "UNEXPECTED";
    }
}

/*!
 *  Get EGL config as name,value strings. namelen >= 32, valuelen >= 1024
 *
 */
SCTBoolean sctiEglGetConfigNameValue( EGLDisplay display, EGLConfig config, char* name, int namelen, char* value, int valuelen )
{
    EGLint                      bufferSize              = -1;
    EGLint                      redSize                 = -1;
    EGLint                      greenSize               = -1;
    EGLint                      blueSize                = -1;
    EGLint                      alphaSize               = -1;
    EGLint                      configCaveat            = 0;
    EGLint                      configId                = 0;
    EGLint                      depthSize               = -1; 
    EGLint                      level                   = -1; 
    EGLint                      maxPBufferWidth         = -1;
    EGLint                      maxPBufferHeight        = -1;
    EGLint                      maxPBufferPixels        = -1;
    EGLint                      nativeRenderable        = 0;
    EGLint                      nativeVisualId          = -1;
    EGLint                      nativeVisualType        = -1;
    EGLint                      sampleBuffers           = -1;
    EGLint                      samples                 = -1;
    EGLint                      stencilSize             = -1;
    EGLint                      surfaceType             = 0;
    EGLint                      transparentType         = 0;
    EGLint                      transparentRedValue     = -1;
    EGLint                      transparentGreenValue   = -1;
    EGLint                      transparentBlueValue    = -1;

#if defined( EGL_VERSION_1_1 )
    EGLint                      bindToTextureRGB        = 0;
    EGLint                      bindToTextureRGBA       = 0;
    EGLint                      maxSwapInterval         = -1;
    EGLint                      minSwapInterval         = -1;
#endif  /* defined( EGL_VERSION_1_1 ) */

#if defined( EGL_VERSION_1_2 )
    EGLint                      luminanceSize           = -1;
    EGLint                      alphaMaskSize           = -1;
    EGLint                      colorBufferType         = 0;
    EGLint                      renderableType          = 0;
#endif  /* defined( EGL_VERSION_1_2 ) */

#if defined( EGL_VERSION_1_3 )
    EGLint                      conformant              = 0;
#endif  /* defined( EGL_VERSION_1_3 ) */

    const char*                 caveatStr;
    char                        surfaceBuffer[ 512 ];
    char                        apiBuffer[ 64 ];
    char                        conformantBuffer[ 32 ];
    
    if( namelen < 32 || valuelen < 1024 )
    {
        return SCT_FALSE;
    }
   
    eglGetConfigAttrib( display, config, EGL_BUFFER_SIZE,               &bufferSize );
    eglGetConfigAttrib( display, config, EGL_RED_SIZE,                  &redSize );
    eglGetConfigAttrib( display, config, EGL_GREEN_SIZE,                &greenSize );
    eglGetConfigAttrib( display, config, EGL_BLUE_SIZE,                 &blueSize );
    eglGetConfigAttrib( display, config, EGL_ALPHA_SIZE,                &alphaSize );
    eglGetConfigAttrib( display, config, EGL_CONFIG_CAVEAT,             &configCaveat );
    eglGetConfigAttrib( display, config, EGL_CONFIG_ID,                 &configId );
    eglGetConfigAttrib( display, config, EGL_DEPTH_SIZE,                &depthSize );
    eglGetConfigAttrib( display, config, EGL_LEVEL,                     &level );
    eglGetConfigAttrib( display, config, EGL_MAX_PBUFFER_WIDTH,         &maxPBufferWidth );
    eglGetConfigAttrib( display, config, EGL_MAX_PBUFFER_HEIGHT,        &maxPBufferHeight );
    eglGetConfigAttrib( display, config, EGL_MAX_PBUFFER_PIXELS,        &maxPBufferPixels );
    eglGetConfigAttrib( display, config, EGL_NATIVE_RENDERABLE,         &nativeRenderable );
    eglGetConfigAttrib( display, config, EGL_NATIVE_VISUAL_ID,          &nativeVisualId );
    eglGetConfigAttrib( display, config, EGL_NATIVE_VISUAL_TYPE,        &nativeVisualType );
    eglGetConfigAttrib( display, config, EGL_SAMPLE_BUFFERS,            &sampleBuffers );
    eglGetConfigAttrib( display, config, EGL_SAMPLES,                   &samples );
    eglGetConfigAttrib( display, config, EGL_STENCIL_SIZE,              &stencilSize );
    eglGetConfigAttrib( display, config, EGL_SURFACE_TYPE,              &surfaceType );
    eglGetConfigAttrib( display, config, EGL_TRANSPARENT_TYPE,          &transparentType );
    eglGetConfigAttrib( display, config, EGL_TRANSPARENT_RED_VALUE,     &transparentRedValue );
    eglGetConfigAttrib( display, config, EGL_TRANSPARENT_GREEN_VALUE,   &transparentGreenValue );
    eglGetConfigAttrib( display, config, EGL_TRANSPARENT_BLUE_VALUE,    &transparentBlueValue );

#if defined( EGL_VERSION_1_1 )
    eglGetConfigAttrib( display, config, EGL_BIND_TO_TEXTURE_RGB,       &bindToTextureRGB );
    eglGetConfigAttrib( display, config, EGL_BIND_TO_TEXTURE_RGBA,      &bindToTextureRGBA );
    eglGetConfigAttrib( display, config, EGL_MAX_SWAP_INTERVAL,         &maxSwapInterval );
    eglGetConfigAttrib( display, config, EGL_MIN_SWAP_INTERVAL,         &minSwapInterval );
#endif  /* defined( EGL_VERSION_1_1 ) */

#if defined( EGL_VERSION_1_2 )
    eglGetConfigAttrib( display, config, EGL_RENDERABLE_TYPE,           &renderableType );
    eglGetConfigAttrib( display, config, EGL_LUMINANCE_SIZE,            &luminanceSize );
    eglGetConfigAttrib( display, config, EGL_ALPHA_MASK_SIZE,           &alphaMaskSize );
    eglGetConfigAttrib( display, config, EGL_COLOR_BUFFER_TYPE,         &colorBufferType );
#endif  /* defined( EGL_VERSION_1_2 ) */

#if defined( EGL_VERSION_1_3 )
    eglGetConfigAttrib( display, config, EGL_CONFORMANT,                &conformant );
#endif  /* defined( EGL_VERSION_1_3 ) */

    /* Format config name. */
    sprintf( name, "ConfigId%d", configId );
    
    /* Format caveat string. */
    switch( configCaveat )
    {
    default:
    case EGL_NONE:
        caveatStr = "None";
        break;
                
    case EGL_SLOW_CONFIG:
        caveatStr = "Slow";
        break;

    case EGL_NON_CONFORMANT_CONFIG:
        caveatStr = "Non-conformant";
        break;
    }

    /* Format surface string. */
    surfaceBuffer[ 0 ] = '\0';
    if( surfaceType & EGL_WINDOW_BIT )
    {
        strcpy( surfaceBuffer, "Window" );
    }
    if( surfaceType & EGL_PIXMAP_BIT )
    {
        sctJoinStrings( surfaceBuffer, "Pixmap", "|" );
    }
    if( surfaceType & EGL_PBUFFER_BIT )
    {
        sctJoinStrings( surfaceBuffer, "Pbuffer", "|" );
    }
    if( surfaceType & EGL_VG_COLORSPACE_LINEAR_BIT )
    {
        sctJoinStrings( surfaceBuffer, "VG colorspace linear", "|" );
    }
    if( surfaceType & EGL_VG_ALPHA_FORMAT_PRE_BIT )
    {
        sctJoinStrings( surfaceBuffer, "VG alpha format pre", "|" );
    }
    if( surfaceType & EGL_MULTISAMPLE_RESOLVE_BOX_BIT )
    {
        sctJoinStrings( surfaceBuffer, "Multisample resolve box", "|" );
    }
    if( surfaceType & EGL_SWAP_BEHAVIOR_PRESERVED_BIT )
    {
        sctJoinStrings( surfaceBuffer, "Swap behavior preserved", "|" );
    }
    if( surfaceType & EGL_LOCK_SURFACE_BIT_KHR )
    {
        sctJoinStrings( surfaceBuffer, "Lock surface", "|" );
    }
    if( surfaceType & EGL_OPTIMAL_FORMAT_BIT_KHR )
    {
        sctJoinStrings( surfaceBuffer, "Optimal format", "|" );
    }

    if( strlen( surfaceBuffer ) == 0 )
    {
        strcat( surfaceBuffer, "0" );
    }
    
    apiBuffer[ 0 ] = '\0';

#if defined( EGL_VERSION_1_2 )
    
    /* Format API string. */
    if( renderableType & EGL_OPENGL_ES_BIT )
    {
        strcpy( apiBuffer, "OpenGL ES" );
    }
    if( renderableType & EGL_OPENGL_ES2_BIT )
    {
        sctJoinStrings( apiBuffer, "OpenGL ES2", "|" );
    }
    if( renderableType & EGL_OPENVG_BIT )
    {
        sctJoinStrings( apiBuffer, "OpenVG", "|" );
    }
    if( renderableType & EGL_OPENGL_BIT )
    {
        sctJoinStrings( apiBuffer, "OpenGL", "|" );
    }
    
#endif  /* defined( EGL_VERSION_1_2 ) */

    if( strlen( apiBuffer ) == 0 )
    {
        strcat( apiBuffer, "0" );
    }

    conformantBuffer[ 0 ] = '\0';
    
#if defined( EGL_VERSION_1_3 )   
    
    /* Format conformant string. */
    if( conformant & EGL_OPENGL_ES_BIT )
    {
        strcpy( conformantBuffer, "OpenGL ES" );
    }
    if( conformant & EGL_OPENGL_ES2_BIT )
    {
        sctJoinStrings( conformantBuffer, "OpenGL ES2", "|" );
    }
    if( conformant & EGL_OPENVG_BIT )
    {
        sctJoinStrings( conformantBuffer, "OpenVG", "|" );
    }
    if( conformant & EGL_OPENGL_BIT )
    {
        sctJoinStrings( conformantBuffer, "OpenGL", "|" );
    }
    
#endif  /* defined( EGL_VERSION_1_3 ) */

    if( strlen( conformantBuffer ) == 0 )
    {
        strcat( conformantBuffer, "0" );
    }
    
#if !defined( EGL_VERSION_1_1 )
    /* EGL 1.0 */
    sprintf( value, 
             "RGB: BufferSize=%d RGBA=(%d/%d/%d/%d) DepthSize=%d "
             "Caveat=%s "
             "Level=%d "
             "MaxPbufferWidth=%d MaxPbufferHeight=%d MaxPbufferPixels=%d "
             "NativeRenderable=%s NativeVisualId=%s NativeVisualType=%d "
             "SampleBuffers=%d Samples=%d StencilSize=%d "
             "SurfaceType=%s "
             "TransparentType=%s TransparentColors=(%d/%d/%d)",
             bufferSize, redSize, greenSize, blueSize, alphaSize, depthSize,
             caveatStr,
             level,
             maxPBufferWidth, maxPBufferHeight, maxPBufferPixels,
             nativeRenderable == EGL_TRUE ? "Yes" : "No", nativeVisualId, nativeVisualType,
             sampleBuffers, samples, stencilSize,
             surfaceBuffer,
             transparentType == EGL_TRANSPARENT_RGB ? "TransparentRGB" : "None",
             transparentRedValue, transparentGreenValue, transparentBlueValue );
#endif  /* !defined( EGL_VERSION_1_1 ) */
    
#if defined( EGL_VERSION_1_1 ) && !defined( EGL_VERSION_1_2 )   
    /* EGL 1.1 */
    sprintf( value, 
             "RGB: BufferSize=%d RGBA=(%d/%d/%d/%d) DepthSize=%d "
             "BindToTextureRGB=%d BindToTextureRGBA=%d "
             "Caveat=%s "
             "Level=%d "
             "MaxPBufferWidth=%d MaxPBufferHeight=%d MaxPBufferPixels=%d "
             "MaxSwap=%d MinSwap=%d "
             "NativeRenderable=%s NativeVisualId=%d NativeVisualType=%d "
             "SampleBuffers=%d Samples=%d StencilSize=%d "
             "SurfaceType=%s "
             "TransparentType=%s TransparentColors=(%d/%d/%d)",
             bufferSize, redSize, greenSize, blueSize, alphaSize, depthSize,
             bindToTextureRGB, bindToTextureRGBA,
             caveatStr,
             level,
             maxPBufferWidth, maxPBufferHeight, maxPBufferPixels,
             maxSwapInterval, minSwapInterval,
             nativeRenderable == EGL_TRUE ? "Yes" : "No", nativeVisualId, nativeVisualType,
             sampleBuffers, samples, stencilSize,
             surfaceBuffer,
             transparentType == EGL_TRANSPARENT_RGB ? "TransparentRGB" : "None",
             transparentRedValue, transparentGreenValue, transparentBlueValue );
#endif  /* defined( EGL_VERSION_1_1 ) && !defined( EGL_VERSION_1_2 ) */

#if defined( EGL_VERSION_1_2 ) && !defined( EGL_VERSION_1_3 )
            /* EGL 1.2 */
    if( colorBufferType == EGL_RGB_BUFFER )
    {
        sprintf( value, 
                 "RGB: BufferSize=%d RGBA=(%d/%d/%d/%d) AlphaMaskSize=%d DepthSize=%d "
                 "BindToTextureRGB=%d BindToTextureRGBA=%d "
                 "Caveat=%s "
                 "Level=%d "
                 "MaxPBufferWidth=%d MaxPBufferHeight=%d MaxPBufferPixels=%d "
                 "MaxSwap=%d MinSwap=%d "
                 "NativeRenderable=%s NativeVisualId=%d NativeVisualType=%d "
                 "RenderableType=%s "
                 "SampleBuffers=%d Samples=%d StencilSize=%d "
                 "SurfaceType=%s "
                 "TransparentType=%s TransparentColors=(%d/%d/%d)",
                 bufferSize, redSize, greenSize, blueSize, alphaSize, alphaMaskSize, depthSize,
                 bindToTextureRGB, bindToTextureRGBA,
                 caveatStr,
                 level,
                 maxPBufferWidth, maxPBufferHeight, maxPBufferPixels,
                 maxSwapInterval, minSwapInterval,
                 nativeRenderable == EGL_TRUE ? "Yes" : "No", nativeVisualId, nativeVisualType,
                 apiBuffer,
                 sampleBuffers, samples, stencilSize,
                 surfaceBuffer,
                 transparentType == EGL_TRANSPARENT_RGB ? "TransparentRGB" : "None",
                 transparentRedValue, transparentGreenValue, transparentBlueValue );
    }
    else
    {
        sprintf( value, 
                 "LUMINANCE: BufferSize=%d Luminance=(%d/%d) AlphaMaskSize=%d DepthSize=%d "
                 "BindToTextureRGB=%d BindToTextureRGBA=%d "
                 "Caveat=%s "
                 "Level=%d "
                 "MaxPBufferWidth=%d MaxPBufferHeight=%d MaxPBufferPixels=%d "
                 "MaxSwap=%d MinSwap=%d "
                 "NativeRenderable=%s NativeVisualId=%d NativeVisualType=%d "
                 "RenderableType=%s "
                 "SampleBuffers=%d Samples=%d StencilSize=%d "
                 "SurfaceType=%s "
                 "TransparentType=%s TransparentColors=(%d/%d/%d)",
                 bufferSize, luminanceSize, alphaSize, alphaMaskSize, depthSize,
                 bindToTextureRGB, bindToTextureRGBA,
                 caveatStr,
                 level,
                 maxPBufferWidth, maxPBufferHeight, maxPBufferPixels,
                 maxSwapInterval, minSwapInterval,
                 nativeRenderable == EGL_TRUE ? "Yes" : "No", nativeVisualId, nativeVisualType,
                 apiBuffer,
                 sampleBuffers, samples, stencilSize,
                 surfaceBuffer,
                 transparentType == EGL_TRANSPARENT_RGB ? "TransparentRGB" : "None",
                 transparentRedValue, transparentGreenValue, transparentBlueValue );
    }
#endif  /* defined( EGL_VERSION_1_2 ) && !defined( EGL_VERSION_1_3 ) */

#if defined( EGL_VERSION_1_3 )
    /* EGL 1.3 */
    if( colorBufferType == EGL_RGB_BUFFER )
    {
        sprintf( value, 
                 "RGB: BufferSize=%d RGBA=(%d/%d/%d/%d) AlphaMaskSize=%d DepthSize=%d "
                 "BindToTextureRGB=%d BindToTextureRGBA=%d "
                 "Caveat=%s "
                 "Conformant=%s "
                 "Level=%d "
                 "MaxPBufferWidth=%d MaxPBufferHeight=%d MaxPBufferPixels=%d "
                 "MaxSwap=%d MinSwap=%d "
                 "NativeRenderable=%s NativeVisualId=%d NativeVisualType=%d "
                 "RenderableType=%s "
                 "SampleBuffers=%d Samples=%d StencilSize=%d "
                 "SurfaceType=%s "
                 "TransparentType=%s TransparentColors=(%d/%d/%d)",
                 bufferSize, redSize, greenSize, blueSize, alphaSize, alphaMaskSize, depthSize,
                 bindToTextureRGB, bindToTextureRGBA,
                 caveatStr,
                 conformantBuffer,
                 level,
                 maxPBufferWidth, maxPBufferHeight, maxPBufferPixels,
                 maxSwapInterval, minSwapInterval,
                 nativeRenderable == EGL_TRUE ? "Yes" : "No", nativeVisualId, nativeVisualType,
                 apiBuffer,
                 sampleBuffers, samples, stencilSize,
                 surfaceBuffer,
                 transparentType == EGL_TRANSPARENT_RGB ? "TransparentRGB" : "None",
                 transparentRedValue, transparentGreenValue, transparentBlueValue );
    }
    else
    {
        sprintf( value, 
                 "LUMINANCE: BufferSize=%d Luminance=(%d/%d) AlphaMaskSize=%d DepthSize=%d "
                 "BindToTextureRGB=%d BindToTextureRGBA=%d "
                 "Caveat=%s "
                 "Conformant=%s "
                 "Level=%d "
                 "MaxPBufferWidth=%d MaxPBufferHeight=%d MaxPBufferPixels=%d "
                 "MaxSwap=%d MinSwap=%d "
                 "NativeRenderable=%s NativeVisualId=%d NativeVisualType=%d "
                 "RenderableType=%s "
                 "SampleBuffers=%d Samples=%d StencilSize=%d "
                 "SurfaceType=%s "
                 "TransparentType=%s TransparentColors=(%d/%d/%d)",
                 bufferSize, luminanceSize, alphaSize, alphaMaskSize, depthSize,
                 bindToTextureRGB, bindToTextureRGBA,
                 caveatStr,
                 conformantBuffer,
                 level,
                 maxPBufferWidth, maxPBufferHeight, maxPBufferPixels,
                 maxSwapInterval, minSwapInterval,
                 nativeRenderable == EGL_TRUE ? "Yes" : "No", nativeVisualId, nativeVisualType,
                 apiBuffer,
                 sampleBuffers, samples, stencilSize,
                 surfaceBuffer,
                 transparentType == EGL_TRANSPARENT_RGB ? "TransparentRGB" : "None",
                 transparentRedValue, transparentGreenValue, transparentBlueValue );
    }
#endif  /* defined( EGL_VERSION_1_3 ) */

    return SCT_TRUE;
}
