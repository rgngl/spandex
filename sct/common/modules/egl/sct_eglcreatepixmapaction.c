/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatepixmapaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreatePixmapActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreatePixmapActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreatePixmapActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreatePixmapActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePixmap@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreatePixmapActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreatePixmapActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreatePixmapActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidPixmapIndex( context->moduleContext, context->data.pixmapIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in CreatePixmap@Egl context creation." );
        sctiDestroyEglCreatePixmapActionContext( context );
        return NULL;
    }

    if( ( context->data.dataIndex != -1 ) &&
        ( sctiEglModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE ) )
    {
        SCT_LOG_ERROR( "Invalid data index in CreatePixmap@Egl context creation." );
        sctiDestroyEglCreatePixmapActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreatePixmapActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePixmapActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePixmapActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreatePixmapActionContext*    context;
    EglCreatePixmapActionData*          data;
    int                                 index;
    void*                               pixmap;
    SCTEglData*                         eglData;    
    void*                               pixmapData          = NULL;
    int                                 pixmapDataLength    = 0;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreatePixmapActionContext* )( action->context );
    data    = &( context->data );

    index = data->pixmapIndex;
    if( sctiEglModuleGetPixmap( context->moduleContext, index ) != NULL  )
    {
        SCT_LOG_ERROR( "Pixmap index already in use in CreatePixmap@Egl action execute." );
        return SCT_FALSE;
    }

    if( data->dataIndex >= 0 )
    {
        eglData = sctiEglModuleGetData( context->moduleContext, data->dataIndex );
        if( eglData == NULL )
        {
            SCT_LOG_ERROR( "Undefined data in CreatePixmap@Egl action execute." );
            return SCT_FALSE;
        }

        pixmapData       = eglData->data;
        pixmapDataLength = eglData->length;        
    }
    
    pixmap = siWindowCreatePixmap( sctEglModuleGetWindowContext( context->moduleContext ), 
                                   data->width, 
                                   data->height, 
                                   data->format,
                                   pixmapData,
                                   pixmapDataLength );
    if( pixmap == NULL )
    {
        SCT_LOG_ERROR( "Pixmap creation failed in CreatePixmap@Egl action execute." );
        return SCT_FALSE;
    }
    
    sctiEglModuleSetPixmap( context->moduleContext, index, pixmap );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreatePixmapActionTerminate( SCTAction* action )
{
    SCTEglCreatePixmapActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreatePixmapActionContext* )( action->context );

    sctiEglModuleDeletePixmap( context->moduleContext, context->data.pixmapIndex );
}

/*!
 *
 *
 */
void sctiEglCreatePixmapActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreatePixmapActionContext( ( SCTEglCreatePixmapActionContext* )( action->context ) );
    action->context = NULL;
}

