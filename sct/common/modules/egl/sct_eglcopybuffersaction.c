/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcopybuffersaction.h"
#include "sct_eglmodule.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCopyBuffersActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCopyBuffersActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCopyBuffersActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCopyBuffersActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CopyBuffers@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCopyBuffersActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCopyBuffersActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCopyBuffersActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CopyBuffers@Egl context creation." );
        sctiDestroyEglCopyBuffersActionContext( context );
        return NULL;
    }
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in CopyBuffers@Egl context creation." );
        sctiDestroyEglCopyBuffersActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidPixmapIndex( context->moduleContext, context->data.pixmapIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in CopyBuffers@Egl context creation." );
        sctiDestroyEglCopyBuffersActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCopyBuffersActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCopyBuffersActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglCopyBuffersActionContext* context;
    EglCopyBuffersActionData*       data;
    EGLDisplay                      display;
    EGLSurface                      surface;
    void*                           pixmap;
    void*                           eglPixmap;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTEglCopyBuffersActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CreateWindowSurface@Egl action execute." );
        return SCT_FALSE;
    }

    surface = sctiEglModuleGetSurface( context->moduleContext, data->surfaceIndex );
    if( surface == EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Undefined surface in CopyBuffers@Egl action execute." );
        return SCT_FALSE;
    }

    pixmap = sctiEglModuleGetPixmap( context->moduleContext, data->pixmapIndex );
    if( pixmap == NULL )
    {
        SCT_LOG_ERROR( "Undefined pixmap in CopyBuffers@Egl action execute." );
        return SCT_FALSE;
    }
    eglPixmap = siWindowGetEglPixmap( sctEglModuleGetWindowContext( context->moduleContext ), pixmap, NULL );
    
    if( eglCopyBuffers( display, surface, ( NativePixmapType )( eglPixmap ) ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Copy buffers failed in CopyBuffers@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCopyBuffersActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglCopyBuffersActionContext( ( SCTEglCopyBuffersActionContext* )( action->context ) );
    action->context = NULL;
}

