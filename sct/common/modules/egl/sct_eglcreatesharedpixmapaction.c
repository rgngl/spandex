/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatesharedpixmapaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreateSharedPixmapActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreateSharedPixmapActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreateSharedPixmapActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreateSharedPixmapActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateSharedPixmap@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreateSharedPixmapActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreateSharedPixmapActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreateSharedPixmapActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidPixmapIndex( context->moduleContext, context->data.pixmapIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in CreateSharedPixmap@Egl context creation." );
        sctiDestroyEglCreateSharedPixmapActionContext( context );
        return NULL;
    }

    if( ( context->data.dataIndex != -1 ) &&
        ( sctiEglModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE ) )
    {
        SCT_LOG_ERROR( "Invalid data index in CreateSharedPixmap@Egl context creation." );
        sctiDestroyEglCreateSharedPixmapActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreateSharedPixmapActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateSharedPixmapActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateSharedPixmapActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreateSharedPixmapActionContext*  context;
    EglCreateSharedPixmapActionData*        data;
    int                                     index;
    void*                                   pixmap;
    SCTEglData*                             eglData;    
    void*                                   pixmapData       = NULL;
    int                                     pixmapDataLength = 0;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreateSharedPixmapActionContext* )( action->context );
    data    = &( context->data );

    index = data->pixmapIndex;
    if( sctiEglModuleGetPixmap( context->moduleContext, index ) != NULL  )
    {
        SCT_LOG_ERROR( "Pixmap index already in use in CreateSharedPixmap@Egl action execute." );
        return SCT_FALSE;
    }

    if( data->dataIndex >= 0 )
    {
        eglData = sctiEglModuleGetData( context->moduleContext, data->dataIndex );
        if( eglData == NULL )
        {
            SCT_LOG_ERROR( "Undefined data in CreateSharedPixmap@Egl action execute." );
            return SCT_FALSE;
        }

        pixmapData       = eglData->data;
        pixmapDataLength = eglData->length;
    }
    
    pixmap = siWindowCreateSharedPixmap( sctEglModuleGetWindowContext( context->moduleContext ), 
                                         data->width, 
                                         data->height, 
                                         data->format,
                                         data->usage,
                                         pixmapData,
                                         pixmapDataLength );
    if( pixmap == NULL )
    {
        SCT_LOG_ERROR( "Pixmap creation failed in CreateSharedPixmap@Egl action execute." );
        return SCT_FALSE;
    }
    
    sctiEglModuleSetPixmap( context->moduleContext, index, pixmap );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreateSharedPixmapActionTerminate( SCTAction* action )
{
    SCTEglCreateSharedPixmapActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreateSharedPixmapActionContext* )( action->context );

    sctiEglModuleDeletePixmap( context->moduleContext, context->data.pixmapIndex );
}

/*!
 *
 *
 */
void sctiEglCreateSharedPixmapActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreateSharedPixmapActionContext( ( SCTEglCreateSharedPixmapActionContext* )( action->context ) );
    action->context = NULL;
}

