/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglrgbconfigaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
static SCTBoolean matchCondition( SCTConditionalInt cint, EGLint value );

/*!
 *
 *
 */
void* sctiCreateEglRGBConfigActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglRGBConfigActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglRGBConfigActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglRGBConfigActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in RGBConfig@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglRGBConfigActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglRGBConfigActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglRGBConfigActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in RGBConfig@Egl context creation." );
        sctiDestroyEglRGBConfigActionContext( context );
        return NULL;
    }
    
    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in RGBConfig@Egl context creation." );
        sctiDestroyEglRGBConfigActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglRGBConfigActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglRGBConfigActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglRGBConfigActionContext*   context;
    EglRGBConfigActionData*         data;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglRGBConfigActionContext* )( action->context );
    data    = &( context->data );

#if! defined( EGL_VERSION_1_1 )
    if( data->supportRGBTextures == SCT_TRUE )
    {
        SCT_LOG_ERROR( "RGBConfig@Egl action init SupportRGBTexture attribute not supported in the used EGL version." );
        return SCT_FALSE;
    }
    if( data->supportRGBATextures == SCT_TRUE )
    {
        SCT_LOG_ERROR( "RGBConfig@Egl action init SupportRGBATextures attribute not supported in the used EGL version." );
        return SCT_FALSE;
    }
#endif  /* defined( EGL_VERSION_1_1 ) */

#if! defined( EGL_VERSION_1_2 )
    if( data->alphaMaskSize.condition != SCT_DONTCARE )
    {
        SCT_LOG_ERROR( "RGBConfig@Egl action init AlphaMaskSize attribute not supported in the used EGL version." );
        return SCT_FALSE;
    }

    if( ( data->renderableType & EGL_OPENVG_BIT ) != 0 )
    {
        SCT_LOG_ERROR( "RGBConfig@Egl action init EGL_OPENVG_BIT attribute supported in the used EGL version." );
        return SCT_FALSE;
    }
#endif  /* defined( EGL_VERSION_1_2 ) */

#if! defined( EGL_VERSION_1_3 )
    if( ( data->renderableType & EGL_OPENGL_ES2_BIT ) != 0 )
    {
        SCT_LOG_ERROR( "RGBConfig@Egl action init EGL_OPENGL_ES2_BIT RenderableType not supported in the used EGL version." );
        return SCT_FALSE;
    }
#endif  /* defined( EGL_VERSION_1_3 ) */

#if! defined( EGL_VERSION_1_4 )
    if( ( data->surfaceType & EGL_MULTISAMPLE_RESOLVE_BOX_BIT ) != 0 )
    {
        SCT_LOG_ERROR( "RGBConfig@Egl action init EGL_MULTISAMPLE_RESOLVE_BOX_BIT SurfaceType not supported in the used EGL version." );
        return SCT_FALSE;
    }
    if( ( data->surfaceType & EGL_SWAP_BEHAVIOR_PRESERVED_BIT ) != 0 )
    {
        SCT_LOG_ERROR( "RGBConfig@Egl action init EGL_SWAP_BEHAVIOR_PRESERVED_BIT SurfaceType not supported in the used EGL version." );
        return SCT_FALSE;
    }
    if( ( data->surfaceType & EGL_VG_COLORSPACE_LINEAR_BIT ) != 0 )
    {
        SCT_LOG_ERROR( "RGBConfig@Egl action init EGL_VG_COLORSPACE_LINEAR_BIT SurfaceType not supported in the used EGL version." );
        return SCT_FALSE;
    }
    if( ( data->surfaceType & EGL_VG_ALPHA_FORMAT_PRE_BIT ) != 0 )
    {
        SCT_LOG_ERROR( "RGBConfig@Egl action init EGL_VG_ALPHA_FORMAT_PRE_BIT SurfaceType not supported in the used EGL version." );
        return SCT_FALSE;
    }
    if( ( data->renderableType & EGL_OPENGL_BIT ) != 0 )
    {
        SCT_LOG_ERROR( "RGBConfig@Egl action init EGL_OPENGL_BIT RenderableType not supported in the used EGL version." );
        return SCT_FALSE;
    }   
#endif  /* defined( EGL_VERSION_1_4 ) */

    SCT_USE_VARIABLE( data );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglRGBConfigActionExecute( SCTAction* action, int framenumber )
{
    SCTEglRGBConfigActionContext*   context;
    EglRGBConfigActionData*         data;
    EGLint                          maxNumConfigs;
    EGLint                          numConfigs;
    EGLConfig*                      configs;                           
    EGLint                          temp;
    EGLDisplay                      display;
    EGLConfig                       config                         = 0;
    SCTBoolean                      foundConfig                    = SCT_FALSE;

    const int                       _BUFFER_SIZE_INDEX             = 0;
    const int                       _RED_SIZE_INDEX                = 2;
    const int                       _GREEN_SIZE_INDEX              = 4;
    const int                       _BLUE_SIZE_INDEX               = 6;
    const int                       _ALPHA_SIZE_INDEX              = 8;
    const int                       _DEPTH_SIZE_INDEX              = 10;
    const int                       _SAMPLE_BUFFERS_INDEX          = 12;
    const int                       _SAMPLES_INDEX                 = 14;
    const int                       _STENCIL_SIZE_INDEX            = 16;
    const int                       _SURFACE_TYPE_INDEX            = 18;

#if !defined( EGL_VERSION_1_1 )
/* EGL 1.0 */
# define                            _FINAL_INDEX                   20
#endif  /* !defined( EGL_VERSION_1_1 ) */

#if defined( EGL_VERSION_1_1 ) && !defined( SCT_WINDOWS_MOBILE )
    const int                       _BIND_TO_TEXTURE_RGB_INDEX     = 20;
    const int                       _BIND_TO_TEXTURE_RGBA_INDEX    = 22;
# if !defined( EGL_VERSION_1_2 )
/* EGL 1.1 */
#  define                           _FINAL_INDEX                   24
# endif /* !defined( EGL_VERSION_1_2 ) */
#else   /* defined( EGL_VERSION_1_1 ) && !defined( SCT_WINDOWS_MOBILE ) */
	# define                        _FINAL_INDEX                   20
#endif  /* defined( EGL_VERSION_1_1 ) && !defined( SCT_WINDOWS_MOBILE ) */

#if defined( EGL_VERSION_1_2 )
 /* EGL 1.2+ */    
    const int                       _RENDERABLE_TYPE_INDEX         = 24;
    const int                       _COLOR_BUFFER_TYPE_INDEX       = 26;
    const int                       _ALPHA_MASK_SIZE_INDEX         = 28;
# define                            _FINAL_INDEX                   30
#endif  /* defined( EGL_VERSION_1_2 ) */

    EGLint                          attribs[ _FINAL_INDEX + 1 ];
    int                             i;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglRGBConfigActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in RGBConfig@Egl action execute." );
        return SCT_FALSE;
    }

    attribs[ _BUFFER_SIZE_INDEX ]               = EGL_BUFFER_SIZE;
    attribs[ _BUFFER_SIZE_INDEX + 1 ]           = data->bufferSize.condition != SCT_DONTCARE    ?   data->bufferSize.value    : EGL_DONT_CARE;

    attribs[ _RED_SIZE_INDEX ]                  = EGL_RED_SIZE;
    attribs[ _RED_SIZE_INDEX + 1 ]              = data->redSize.condition != SCT_DONTCARE       ?   data->redSize.value       : EGL_DONT_CARE;

    attribs[ _GREEN_SIZE_INDEX ]                = EGL_GREEN_SIZE;
    attribs[ _GREEN_SIZE_INDEX + 1 ]            = data->greenSize.condition != SCT_DONTCARE     ?   data->greenSize.value     : EGL_DONT_CARE;

    attribs[ _BLUE_SIZE_INDEX ]                 = EGL_BLUE_SIZE;
    attribs[ _BLUE_SIZE_INDEX + 1 ]             = data->blueSize.condition != SCT_DONTCARE      ?   data->blueSize.value      : EGL_DONT_CARE;

    attribs[ _ALPHA_SIZE_INDEX ]                = EGL_ALPHA_SIZE;
    attribs[ _ALPHA_SIZE_INDEX + 1 ]            = data->alphaSize.condition != SCT_DONTCARE     ?   data->alphaSize.value     : EGL_DONT_CARE;

    attribs[ _DEPTH_SIZE_INDEX ]                = EGL_DEPTH_SIZE;
    attribs[ _DEPTH_SIZE_INDEX + 1 ]            = data->depthSize.condition != SCT_DONTCARE     ?   data->depthSize.value     : EGL_DONT_CARE;

    attribs[ _SAMPLE_BUFFERS_INDEX ]            = EGL_SAMPLE_BUFFERS;
    attribs[ _SAMPLE_BUFFERS_INDEX + 1 ]        = data->sampleBuffers.condition != SCT_DONTCARE ?  data->sampleBuffers.value  : EGL_DONT_CARE;

    attribs[ _SAMPLES_INDEX ]                   = EGL_SAMPLES;
    attribs[ _SAMPLES_INDEX + 1 ]               = data->samples.condition != SCT_DONTCARE       ?  data->samples.value        : EGL_DONT_CARE;

    attribs[ _STENCIL_SIZE_INDEX ]              = EGL_STENCIL_SIZE;
    attribs[ _STENCIL_SIZE_INDEX + 1 ]          = data->stencilSize.condition != SCT_DONTCARE   ? data->stencilSize.value     : EGL_DONT_CARE;

    attribs[ _SURFACE_TYPE_INDEX ]              = EGL_SURFACE_TYPE;
    attribs[ _SURFACE_TYPE_INDEX + 1 ]          = data->surfaceType;

#if defined( EGL_VERSION_1_1 ) && !defined( SCT_WINDOWS_MOBILE )
    attribs[ _BIND_TO_TEXTURE_RGB_INDEX ]       = EGL_BIND_TO_TEXTURE_RGB;
    attribs[ _BIND_TO_TEXTURE_RGB_INDEX + 1 ]   = data->supportRGBTextures;

    attribs[ _BIND_TO_TEXTURE_RGBA_INDEX ]      = EGL_BIND_TO_TEXTURE_RGBA;
    attribs[ _BIND_TO_TEXTURE_RGBA_INDEX + 1 ]  = data->supportRGBATextures;
#endif	/* defined( EGL_VERSION_1_1 ) && !defined( SCT_WINDOWS_MOBILE ) */

#if defined( EGL_VERSION_1_2 )
    attribs[ _COLOR_BUFFER_TYPE_INDEX ]         = EGL_COLOR_BUFFER_TYPE;
    attribs[ _COLOR_BUFFER_TYPE_INDEX + 1 ]     = EGL_RGB_BUFFER;

    attribs[ _ALPHA_MASK_SIZE_INDEX ]           = EGL_ALPHA_MASK_SIZE;
    attribs[ _ALPHA_MASK_SIZE_INDEX + 1 ]       = data->alphaMaskSize.condition != SCT_DONTCARE ? data->alphaMaskSize.value   : EGL_DONT_CARE;

    attribs[ _RENDERABLE_TYPE_INDEX ]           = EGL_RENDERABLE_TYPE;
    attribs[ _RENDERABLE_TYPE_INDEX + 1 ]       = data->renderableType;
#endif  /* defined( EGL_VERSION_1_2 ) */

    attribs[ _FINAL_INDEX ]                     = EGL_NONE;

    if( eglChooseConfig( display, attribs, NULL, 0, &maxNumConfigs ) == EGL_FALSE)
    {
        char    buf[ 256 ];
        sprintf( buf, "Choosing config failed in RGBConfig@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    if( maxNumConfigs == 0 )
    {
        SCT_LOG_ERROR( "No matching config in RGBConfig@Egl action execute." );
        return SCT_FALSE;
    }

    configs  = ( EGLConfig* )( siCommonMemoryAlloc( NULL, maxNumConfigs * sizeof( EGLConfig ) ) );
    if( configs == NULL )
    {
        SCT_LOG_ERROR( "Config allocation failed in RGBConfig@Egl action execute." );
        return SCT_FALSE;
    }
    memset( configs, 0, maxNumConfigs * sizeof( EGLConfig ) );

    if( eglChooseConfig( display, attribs, configs, maxNumConfigs, &numConfigs ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Choosing config failed in RGBConfig@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        siCommonMemoryFree( NULL, configs );

        return SCT_FALSE;
    }

    for( i = 0; i < numConfigs; ++i )
    {
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_BUFFER_SIZE, &temp );
        if( matchCondition( data->bufferSize, temp ) == SCT_FALSE )
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_RED_SIZE, &temp );
        if( matchCondition( data->redSize, temp ) == SCT_FALSE )
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_GREEN_SIZE, &temp );
        if( matchCondition( data->greenSize, temp ) == SCT_FALSE )
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_BLUE_SIZE, &temp );
        if( matchCondition( data->blueSize, temp ) == SCT_FALSE )
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_ALPHA_SIZE, &temp );
        if( matchCondition( data->alphaSize, temp ) == SCT_FALSE )
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_DEPTH_SIZE, &temp );
        if( matchCondition( data->depthSize, temp ) == SCT_FALSE )
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_SAMPLE_BUFFERS, &temp );
        if( matchCondition( data->sampleBuffers, temp ) == SCT_FALSE )
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_SAMPLES, &temp );
        if( matchCondition( data->samples, temp ) == SCT_FALSE )
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_STENCIL_SIZE, &temp );
        if( matchCondition( data->stencilSize, temp ) == SCT_FALSE )
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_SURFACE_TYPE, &temp );
        if( ( attribs[ _SURFACE_TYPE_INDEX + 1 ] & temp ) == 0 )
        {
            continue;
        }

#if defined( EGL_VERSION_1_1 ) && !defined( SCT_WINDOWS_MOBILE )
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_BIND_TO_TEXTURE_RGB, &temp );
        if( attribs[ _BIND_TO_TEXTURE_RGB_INDEX + 1 ] != EGL_DONT_CARE && temp != attribs[ _BIND_TO_TEXTURE_RGB_INDEX + 1 ] )
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_BIND_TO_TEXTURE_RGBA, &temp );
        if( attribs[ _BIND_TO_TEXTURE_RGBA_INDEX + 1 ] != EGL_DONT_CARE && temp != attribs[ _BIND_TO_TEXTURE_RGBA_INDEX + 1 ] )
        {
            continue;
        }
#endif  /* defined( EGL_VERSION_1_1 ) && !defined( SCT_WINDOWS_MOBILE ) */

#if defined( EGL_VERSION_1_2 )
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_COLOR_BUFFER_TYPE, &temp );
        if( temp != attribs[ _COLOR_BUFFER_TYPE_INDEX + 1 ] )
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_ALPHA_MASK_SIZE, &temp );
        if( matchCondition( data->alphaMaskSize, temp ) == SCT_FALSE )        
        {
            continue;
        }
        temp = -1;
        eglGetConfigAttrib( display, configs[ i ], EGL_RENDERABLE_TYPE, &temp );
        if( ( attribs[ _RENDERABLE_TYPE_INDEX + 1 ] & temp ) == 0 )
        {
            continue;
        }
#endif  /* defined( EGL_VERSION_1_2 ) */
        
        config      = configs[ i ];
        foundConfig = SCT_TRUE;
        break;
    }
    
    siCommonMemoryFree( NULL, configs );

    if( foundConfig == SCT_FALSE )
    {
        SCT_LOG_ERROR( "No matching config found in RGBConfig@Egl action execute." );
        return SCT_FALSE;
    }

#if defined( SCT_DEBUG )
    {
        char name[ 32 ];
        char value[ 1024 ];

        name[ 0 ]  = '\0';
        value[ 0 ] = '\0';
        sctiEglGetConfigNameValue( display, config, name, sizeof( name ), value, sizeof( value ) );
        siCommonDebugPrintf( NULL, "SPANDEX: RGBConfig@Egl selected %s: %s", name, value );
    }
#endif  /* defined( SCT_DEBUG ) */   
    
    sctiEglModuleSetConfig( context->moduleContext, data->configIndex, config );    

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglRGBConfigActionTerminate( SCTAction* action )
{
    SCTEglRGBConfigActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTEglRGBConfigActionContext* )( action->context );

    sctiEglModuleDeleteConfig( context->moduleContext, context->data.configIndex );
}

/*!
 *
 *
 */
void sctiEglRGBConfigActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglRGBConfigActionContext( ( SCTEglRGBConfigActionContext* )( action->context ) );
    action->context = NULL;
}

/*!
 *
 *
 */
static SCTBoolean matchCondition( SCTConditionalInt cint, EGLint value )
{
    switch( cint.condition )
    {
    case SCT_DONTCARE:
        return SCT_TRUE;
        
    case SCT_LESS:
        return ( ( int )( value ) < cint.value ? SCT_TRUE : SCT_FALSE );
        
    case SCT_GREATER:
        return ( ( int )( value ) > cint.value ? SCT_TRUE : SCT_FALSE );
        
    case SCT_LEQUAL:
        return ( ( int )( value ) <= cint.value ? SCT_TRUE : SCT_FALSE );
        
    case SCT_GEQUAL:
        return ( ( int )( value ) >= cint.value ? SCT_TRUE : SCT_FALSE );
        
    case SCT_EQUAL:
        return ( ( int )( value ) == cint.value ? SCT_TRUE : SCT_FALSE );

    default:
        SCT_ASSERT( 0 );
        return SCT_FALSE;
    }
}
