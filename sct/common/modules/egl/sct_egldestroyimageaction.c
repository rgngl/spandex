/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_egldestroyimageaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglDestroyImageActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglDestroyImageActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglDestroyImageActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglDestroyImageActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyImage@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglDestroyImageActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglDestroyImageActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglDestroyImageActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in DestroyImage@Egl context creation." );
        sctiDestroyEglDestroyImageActionContext( context );
        return NULL;
    }   
    
    if( sctiEglModuleIsValidImageIndex( context->moduleContext, context->data.imageIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid image index in DestroyImage@Egl context creation." );
        sctiDestroyEglDestroyImageActionContext( context );
        return NULL;
    }

    context->validated = SCT_FALSE;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglDestroyImageActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglDestroyImageActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglDestroyImageActionContext* context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglDestroyImageActionContext* )( action->context ); 

    context->validated          = SCT_FALSE;   
    context->eglDestroyImageKhr = NULL;

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglDestroyImageActionExecute( SCTAction* action, int framenumber )
{
    SCTEglDestroyImageActionContext*    context;
    EglDestroyImageActionData*          data;
    EGLDisplay                          display;    
    int                                 imageIndex;
    const char*                         extString;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglDestroyImageActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in DestroyImage@Egl action execute." );
        return SCT_FALSE;
    }

    if( context->validated == SCT_FALSE )
    {
        context->eglDestroyImageKhr = ( SCTPfnEglDestroyImageKhr )( siCommonGetProcAddress( NULL, "eglDestroyImageKHR" ) );

        if( context->eglDestroyImageKhr == NULL )
        {
            SCT_LOG_ERROR( "eglDestroyImageKHR function not found in DestroyImage@Egl action execute." );
            return SCT_FALSE;
        }

        extString = ( const char* )( eglQueryString( display, EGL_EXTENSIONS ) );
        if( extString == NULL || strstr( extString, "EGL_KHR_image_base" ) == NULL )
        {
            SCT_LOG_ERROR( "EGL_KHR_image_base extension not supported in DestroyImage@Egl action execute." );
            return SCT_FALSE;
        }

        context->validated = SCT_TRUE;
    }
 
    imageIndex = data->imageIndex;
    if( sctiEglModuleGetImage( context->moduleContext, imageIndex ) == EGL_NO_IMAGE_KHR )
    {
        SCT_LOG_ERROR( "Invalid image in DestroyImage@Egl action execute." );
        return SCT_FALSE;
    }

    sctiEglModuleDeleteImage( context->moduleContext,
                              context->data.displayIndex,
                              imageIndex,
                              context->eglDestroyImageKhr );    

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglDestroyImageActionTerminate( SCTAction* action )
{
    SCTEglDestroyImageActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglDestroyImageActionContext* )( action->context );

    context->validated          = SCT_FALSE;
    context->eglDestroyImageKhr = NULL;
}

/*!
 *
 *
 */
void sctiEglDestroyImageActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglDestroyImageActionContext( ( SCTEglDestroyImageActionContext* )( action->context ) );
    action->context = NULL;
}

