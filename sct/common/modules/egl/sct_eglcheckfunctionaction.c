/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcheckfunctionaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_egl.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCheckFunctionActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCheckFunctionActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCheckFunctionActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCheckFunctionActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CheckFunction@Egl action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCheckFunctionActionContext ) );

    if( sctiParseEglCheckFunctionActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCheckFunctionActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCheckFunctionActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCheckFunctionActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglCheckFunctionActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTEglCheckFunctionActionContext* )( action->context );

    if( siCommonGetProcAddress( NULL, context->data.function ) == NULL )
    {
        char buf[ 1024 ];
        sprintf( buf, "Function %s not found in CheckFunction@Egl action execute.", context->data.function );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }  

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCheckFunctionActionDestroy( SCTAction* action )
{
    SCTEglCheckFunctionActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTEglCheckFunctionActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyEglCheckFunctionActionContext( context );
}

