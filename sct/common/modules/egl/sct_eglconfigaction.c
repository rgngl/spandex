/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglconfigaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglConfigActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglConfigActionContext*  context;
    int                         len;
    int                         i;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglConfigActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglConfigActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Config@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglConfigActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglConfigActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglConfigActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in Config@Egl context creation." );
        sctiDestroyEglConfigActionContext( context );
        return NULL;
    }
   
    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in Config@Egl context creation." );
        sctiDestroyEglConfigActionContext( context );
        return NULL;
    }

    len = context->data.attributes->length;    
    if( len < 2 )
    {
        SCT_LOG_ERROR( "Invalid Attributes in Config@Egl context creation." );
        sctiDestroyEglConfigActionContext( context );
        return NULL;
    }

    /* Reserve space for an EGL_NONE at the end. */
    context->eglAttributes = ( EGLint* )( siCommonMemoryAlloc( NULL, ( len + 1 ) * sizeof( EGLint ) ) );
    if( context->eglAttributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Config@Egl context creation." );
        sctiDestroyEglConfigActionContext( context );        
        return NULL;
    }

    for( i = 0; i < len; ++i )
    {
        context->eglAttributes[ i ] = ( ( EGLint* )( context->data.attributes->data ) )[ i ];
    }

    context->eglAttributes[ len ] = EGL_NONE;
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglConfigActionContext( void* context )
{
    SCTEglConfigActionContext*  c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTEglConfigActionContext* )( context );

    if( c->eglAttributes != NULL )
    {
        siCommonMemoryFree( NULL, c->eglAttributes );
        c->eglAttributes = NULL;
    }
    
    if( c->data.attributes != NULL )
    {
        sctDestroyIntVector( c->data.attributes );
        c->data.attributes = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglConfigActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglConfigActionExecute( SCTAction* action, int framenumber )
{
    SCTEglConfigActionContext*  context;
    EglConfigActionData*        data;
    EGLint                      numConfigs;
    EGLDisplay                  display;
    EGLConfig                   config;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglConfigActionContext* )( action->context );
    data    = &( context->data );
    
    SCT_ASSERT( context->eglAttributes != NULL );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in Config@Egl action execute." );
        return SCT_FALSE;
    }
    
    if( eglChooseConfig( display, 
                         context->eglAttributes, 
                         &( config ), 
                         1, 
                         &numConfigs ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Choosing config failed in Config@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
    
    if( numConfigs == 0 )
    {
        SCT_LOG_ERROR( "No config in Config@Egl action execute." );
        return SCT_FALSE;
    }

#if defined( SCT_DEBUG )
    {
        char name[ 32 ];
        char value[ 1024 ];

        name[ 0 ]  = '\0';
        value[ 0 ] = '\0';        
        sctiEglGetConfigNameValue( display, config, name, sizeof( name ), value, sizeof( value ) );
        siCommonDebugPrintf( NULL, "SPANDEX: Config@Egl selected %s: %s", name, value );
    }
#endif  /* defined( SCT_DEBUG ) */   
    
    sctiEglModuleSetConfig( context->moduleContext, data->configIndex, config );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglConfigActionTerminate( SCTAction* action )
{
    SCTEglConfigActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTEglConfigActionContext* )( action->context );

    sctiEglModuleDeleteConfig( context->moduleContext, context->data.configIndex );
}

/*!
 *
 *
 */
void sctiEglConfigActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglConfigActionContext( ( SCTEglConfigActionContext* )( action->context ) );
    action->context = NULL;
}

