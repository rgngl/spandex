/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatepbuffersurfaceaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreatePbufferSurfaceActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreatePbufferSurfaceActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreatePbufferSurfaceActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreatePbufferSurfaceActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePbufferSurface@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreatePbufferSurfaceActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreatePbufferSurfaceActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreatePbufferSurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CreatePbufferSurface@Egl context creation." );
        sctiDestroyEglCreatePbufferSurfaceActionContext( context );
        return NULL;
    }
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in CreatePbufferSurface@Egl context creation." );
        sctiDestroyEglCreatePbufferSurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in CreatePbufferSurface@Egl context creation." );
        sctiDestroyEglCreatePbufferSurfaceActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreatePbufferSurfaceActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePbufferSurfaceActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglCreatePbufferSurfaceActionContext*    context;
    EglCreatePbufferSurfaceActionData*          data;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglCreatePbufferSurfaceActionContext* )( action->context );
    data    = &( context->data );

#if !defined( EGL_VERSION_1_1 )
    if( data->textureFormat != EGL_NO_TEXTURE )
    {
        SCT_LOG_ERROR( "CreatePbufferSurface@Egl action init TextureFormat attribute not supported in the current EGL version." );
        return SCT_FALSE;
    }
    if( data->textureTarget != EGL_NO_TEXTURE )
    {
        SCT_LOG_ERROR( "CreatePbufferSurface@Egl action init TextureTarget attribute not supported in the current EGL version." );
        return SCT_FALSE;
    }
    if( data->mipmapTexture != SCT_FALSE )
    {
        SCT_LOG_ERROR( "CreatePbufferSurface@Egl action init MipmapTexture attribute not supported in the current EGL." );
        return SCT_FALSE;
    }
#endif  /* !defined( EGL_VERSION_1_1 ) */

#if !defined( EGL_VERSION_1_2 )
    if( data->colorSpace != EGL_NONE && data->colorSpace != EGL_COLORSPACE_sRGB )
    {
        SCT_LOG_ERROR( "CreatePbufferSurface@Egl action init ColorSpace attribute not supported in the current EGL." );
        return SCT_FALSE;
    }
    if( data->alphaFormat != EGL_NONE && data->alphaFormat != EGL_ALPHA_FORMAT_NONPRE )
    {
        SCT_LOG_ERROR( "CreatePbufferSurface@Egl action init AlphaFormat attribute not supported in the current EGL." );
        return SCT_FALSE;
    }
#endif  /* !defined( EGL_VERSION_1_2 ) */

#if !defined( EGL_VERSION_1_4 )
    if( data->largestPbuffer != SCT_FALSE )
    {
        SCT_LOG_ERROR( "CreatePbufferSurface@Egl action init LargestPbuffer attribute not supported in the current EGL." );
        return SCT_FALSE;
    }
#endif  /* !defined( EGL_VERSION_1_4 ) */

    SCT_USE_VARIABLE( data );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePbufferSurfaceActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreatePbufferSurfaceActionContext*    context;
    EglCreatePbufferSurfaceActionData*          data;
    int                                         surfaceIndex;
    int                                         configIndex;
    EGLDisplay                                  display;
    EGLConfig                                   config;
    EGLSurface                                  surface;
    EGLint                                      renderableType  = 0;
    int                                         index           = 0;
    EGLint                                      attribs[]       = { EGL_WIDTH,           0,
                                                                    EGL_HEIGHT,          0,
#if defined( EGL_VERSION_1_1 )
                                                                    EGL_TEXTURE_FORMAT,  EGL_NO_TEXTURE,
                                                                    EGL_TEXTURE_TARGET,  EGL_NO_TEXTURE,
                                                                    EGL_MIPMAP_TEXTURE,  EGL_FALSE,
#endif  /* defined( EGL_VERSION_1_1 ) */

#if defined( EGL_VERSION_1_2 )
                                                                    EGL_COLORSPACE,      EGL_COLORSPACE_sRGB,
                                                                    EGL_ALPHA_FORMAT,    EGL_ALPHA_FORMAT_NONPRE,
#endif  /* defined( EGL_VERSION_1_2 ) */

#if defined( EGL_VERSION_1_4 )
                                                                    EGL_LARGEST_PBUFFER, EGL_FALSE,
#endif  /* defined( EGL_VERSION_1_4 ) */
                                                                    
                                                                    EGL_NONE };

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );
    SCT_USE_VARIABLE( renderableType );

    context = ( SCTEglCreatePbufferSurfaceActionContext* )( action->context );
    data    = &( context->data );

    surfaceIndex = data->surfaceIndex;
    configIndex  = data->configIndex;

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CreatePbufferSurface@Egl action execute." );
        return SCT_FALSE;
    }

    if( sctiEglModuleGetSurface( context->moduleContext, surfaceIndex ) != EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Surface index already in use in CreatePbufferSurface@Egl action execute." );
        return SCT_FALSE;
    }

    config = sctiEglModuleGetConfig( context->moduleContext, configIndex );

    attribs[ index++ ] = EGL_WIDTH;
    attribs[ index++ ] = data->width;
    attribs[ index++ ] = EGL_HEIGHT;
    attribs[ index++ ] = data->height;

#if defined( EGL_VERSION_1_1 )
# if defined( EGL_VERSION_1_2 )
    if( eglGetConfigAttrib( display, config, EGL_RENDERABLE_TYPE, &renderableType ) == EGL_FALSE )
    {
        SCT_LOG_ERROR( "Getting config attrib failed in CreatePbufferSurface@Egl action execute." );
        return SCT_FALSE;
    }

    if( ( renderableType & EGL_OPENGL_ES_BIT ) != 0 || ( renderableType & EGL_OPENGL_ES2_BIT ) != 0 ) 
# endif /* defined( EGL_VERSION_1_2 ) */
    {
        attribs[ index++ ] = EGL_TEXTURE_FORMAT;
        attribs[ index++ ] = data->textureFormat;
        attribs[ index++ ] = EGL_TEXTURE_TARGET;
        attribs[ index++ ] = data->textureTarget;
        attribs[ index++ ] = EGL_MIPMAP_TEXTURE;
        attribs[ index++ ] = data->mipmapTexture == SCT_FALSE ? EGL_FALSE : EGL_TRUE;
    }
#endif  /* defined( EGL_VERSION_1_1 ) */

#if defined( EGL_VERSION_1_2 )
    if( ( renderableType & EGL_OPENVG_BIT ) != 0 )
    {
        if( data->colorSpace != EGL_NONE )
        {
            attribs[ index++ ] = EGL_COLORSPACE;
            attribs[ index++ ] = data->colorSpace;
        }
        if( data->alphaFormat != EGL_NONE )
        {
            attribs[ index++ ] = EGL_ALPHA_FORMAT;
            attribs[ index++ ] = data->alphaFormat;
        }
    }
#endif  /* defined( EGL_VERSION_1_2 ) */

#if defined( EGL_VERSION_1_4 )
    attribs[ index++ ] = EGL_LARGEST_PBUFFER;
    attribs[ index++ ] = data->largestPbuffer == SCT_FALSE ? EGL_FALSE : EGL_TRUE;
#endif  /* defined( EGL_VERSION_1_4 ) */
    
    attribs[ index ] = EGL_NONE;

    surface = eglCreatePbufferSurface( display, config, attribs );
    if( surface == EGL_NO_SURFACE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Creating pbuffer surface failed in CreatePbufferSurface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    sctiEglModuleSetSurface( context->moduleContext, surfaceIndex, surface );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreatePbufferSurfaceActionTerminate( SCTAction* action )
{
    SCTEglCreatePbufferSurfaceActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreatePbufferSurfaceActionContext* )( action->context );

    sctiEglModuleDeleteSurface( context->moduleContext,
                                context->data.displayIndex,
                                context->data.surfaceIndex );    
}

/*!
 *
 *
 */
void sctiEglCreatePbufferSurfaceActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreatePbufferSurfaceActionContext( ( SCTEglCreatePbufferSurfaceActionContext* )( action->context ) );
    action->context = NULL;
}
