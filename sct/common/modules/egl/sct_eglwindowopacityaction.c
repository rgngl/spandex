/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglwindowopacityaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglWindowOpacityActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglWindowOpacityActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglWindowOpacityActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglWindowOpacityActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in WindowOpacity@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglWindowOpacityActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglWindowOpacityActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglWindowOpacityActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidWindowIndex( context->moduleContext, context->data.windowIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid window index in WindowOpacity@Egl context creation." );
        sctiDestroyEglWindowOpacityActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglWindowOpacityActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglWindowOpacityActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglWindowOpacityActionExecute( SCTAction* action, int framenumber )
{
    SCTEglWindowOpacityActionContext*   context;
    EglWindowOpacityActionData*         data;
    void*                               window;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglWindowOpacityActionContext* )( action->context );
    data    = &( context->data );

    window = sctiEglModuleGetWindow( context->moduleContext, data->windowIndex );
    if( window == NULL )
    {
        SCT_LOG_ERROR( "Undefined window in WindowOpacity@Egl action execute." );
        return SCT_FALSE;
    }

    if( siWindowSetOpacity( sctEglModuleGetWindowContext( context->moduleContext ),
                            window,
                            data->opacity ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Setting window opacity failed in WindowOpacity@Egl action execute." );
        return SCT_FALSE;
    }   
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglWindowOpacityActionTerminate( SCTAction* action )
{
    SCTEglWindowOpacityActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTEglWindowOpacityActionContext* )( action->context );

    SCT_USE_VARIABLE( context );
}

/*!
 *
 *
 */
void sctiEglWindowOpacityActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglWindowOpacityActionContext( ( SCTEglWindowOpacityActionContext* )( action->context ) );
    action->context = NULL;
}

