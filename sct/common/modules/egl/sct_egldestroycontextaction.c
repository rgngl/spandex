/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_egldestroycontextaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglDestroyContextActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglDestroyContextActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglDestroyContextActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglDestroyContextActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyContext@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglDestroyContextActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglDestroyContextActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglDestroyContextActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in DestroyContext@Egl context creation." );
        sctiDestroyEglDestroyContextActionContext( context );
        return NULL;
    }   
    
    if( sctiEglModuleIsValidContextIndex( context->moduleContext, context->data.contextIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid context index in DestroyContext@Egl context creation." );
        sctiDestroyEglDestroyContextActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglDestroyContextActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglDestroyContextActionExecute( SCTAction* action, int framenumber )
{
    SCTEglDestroyContextActionContext*  context;
    EglDestroyContextActionData*        data;
    int                                 contextIndex;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglDestroyContextActionContext* )( action->context );
    data    = &( context->data );

    contextIndex = data->contextIndex;
    if( sctiEglModuleGetContext( context->moduleContext, contextIndex ) == EGL_NO_CONTEXT )
    {
        SCT_LOG_ERROR( "Invalid context in DestroyContext@Egl action execute." );
        return SCT_FALSE;
    }

    sctiEglModuleDeleteContext( context->moduleContext,
                                context->data.displayIndex,
                                contextIndex );    

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglDestroyContextActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglDestroyContextActionContext( ( SCTEglDestroyContextActionContext* )( action->context ) );
    action->context = NULL;
}

