/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglclientwaitsyncaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglClientWaitSyncActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglClientWaitSyncActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglClientWaitSyncActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglClientWaitSyncActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ClientWaitSync@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglClientWaitSyncActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglClientWaitSyncActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglClientWaitSyncActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in ClientWaitSync@Egl context creation." );
        sctiDestroyEglClientWaitSyncActionContext( context );
        return NULL;
    }   
    
    if( sctiEglModuleIsValidSyncIndex( context->moduleContext, context->data.syncIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid sync index in ClientWaitSync@Egl context creation." );
        sctiDestroyEglClientWaitSyncActionContext( context );
        return NULL;
    }

#if defined( KHRONOS_SUPPORT_INT64 )
    {
        int len = context->data.timeout->length;

        if( len == 0 )
        {
            context->timeout = ( EGLTimeKHR )( EGL_FOREVER_KHR );
        }
        else
        {
            EGLTimeKHR  prev;
            int         i;

            context->timeout = 1;
            for( i = 0; i < len; ++i )
            {
                if( context->data.timeout->data[ i ] < 0 )
                {
                    SCT_LOG_ERROR( "Invalid timeout in ClientWaitSync@Egl context creation." );
                    sctiDestroyEglClientWaitSyncActionContext( context );
                    return NULL; 
                }

                prev = context->timeout;
                context->timeout *= ( EGLTimeKHR )( context->data.timeout->data[ i ] );
                if( context->timeout == 0 )
                {
                    break;
                }
                if( context->timeout < prev )
                {
                    SCT_LOG_ERROR( "Timeout overflow in ClientWaitSync@Egl context creation." );
                    sctiDestroyEglClientWaitSyncActionContext( context );
                    return NULL; 
                }
            }
        }
    }
#endif  /* defined( KHRONOS_SUPPORT_INT64 ) */ 

    context->eglClientWaitSyncKhr = NULL;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglClientWaitSyncActionContext( void* context )
{
    SCTEglClientWaitSyncActionContext* c = ( SCTEglClientWaitSyncActionContext* )( context );

    if( context == NULL )
    {
        return;
    }

    if( c->data.timeout != NULL )
    {
        sctDestroyIntVector( c->data.timeout );
        c->data.timeout = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglClientWaitSyncActionExecute( SCTAction* action, int framenumber )
{
#if !defined( KHRONOS_SUPPORT_INT64 )

    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );

    SCT_LOG_ERROR( "INT64 not supported for EGL_KHR_fence_sync in ClientWaitSync@Egl action execute." );
    return SCT_FALSE;

#else   /* !defined( KHRONOS_SUPPORT_INT64 ) */

    SCTEglClientWaitSyncActionContext*  context;
    EglClientWaitSyncActionData*        data;
    EGLDisplay                          display;
    EGLSyncKHR                          eglSync;
    EGLint                              status;
    const char*                         extString;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglClientWaitSyncActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in ClientWaitSync@Egl action execute." );
        return SCT_FALSE;
    }
 
    if( context->validated == SCT_FALSE )
    {
        context->eglClientWaitSyncKhr = ( SCTPfnEglClientWaitSyncKhr )( siCommonGetProcAddress( NULL, "eglClientWaitSyncKHR" ) );

        if( context->eglClientWaitSyncKhr == NULL )
        {
            SCT_LOG_ERROR( "eglClientWaitSyncKHR function not found in ClientWaitSync@Egl action execute." );
            return SCT_FALSE;
        }

        extString = ( const char* )( eglQueryString( display, EGL_EXTENSIONS ) );
        if( extString == NULL || strstr( extString, "EGL_KHR_fence_sync" ) == NULL )
        {
            SCT_LOG_ERROR( "EGL_KHR_fence_sync extension not supported in ClientWaitSyncSync@Egl action execute." );
            return SCT_FALSE;
        }

        context->validated = SCT_TRUE;
    }

    eglSync = sctiEglModuleGetSync( context->moduleContext, data->syncIndex );  
    if( eglSync == EGL_NO_SYNC_KHR )
    {
        SCT_LOG_ERROR( "Invalid sync in ClientWaitSync@Egl action execute." );
        return SCT_FALSE;
    }

    SCT_ASSERT( context->eglClientWaitSyncKhr != NULL );

    status = context->eglClientWaitSyncKhr( display, eglSync, data->flags, context->timeout );
    
    if( status == EGL_FALSE )   
    {
        char    buf[ 256 ];
        sprintf( buf, "Wait failed in ClientWaitSync@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
    else if( status == EGL_TIMEOUT_EXPIRED_KHR )
    {
        SCT_LOG_ERROR( "Wait timeout in ClientWaitSyncSync@Egl action execute." );
        return SCT_FALSE;
    }
    
    SCT_ASSERT( status == EGL_CONDITION_SATISFIED_KHR );

    return SCT_TRUE;

#endif  /* !defined( KHRONOS_SUPPORT_INT64 ) */
}

/*!
 *
 *
 */
void sctiEglClientWaitSyncActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglClientWaitSyncActionContext( ( SCTEglClientWaitSyncActionContext* )( action->context ) );
    action->context = NULL;
}

