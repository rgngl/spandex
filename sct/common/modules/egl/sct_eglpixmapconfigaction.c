/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglpixmapconfigaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglPixmapConfigActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglPixmapConfigActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglPixmapConfigActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglPixmapConfigActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PixmapConfig@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglPixmapConfigActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglPixmapConfigActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglPixmapConfigActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in PixmapConfig@Egl context creation." );
        sctiDestroyEglPixmapConfigActionContext( context );
        return NULL;
    }
    
    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in PixmapConfig@Egl context creation." );
        sctiDestroyEglPixmapConfigActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidPixmapIndex( context->moduleContext, context->data.pixmapIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in PixmapConfig@Egl context creation." );
        sctiDestroyEglPixmapConfigActionContext( context );        
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglPixmapConfigActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglPixmapConfigActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( EGL_VERSION_1_3 )
    SCT_LOG_ERROR( "PixmapConfig@Egl action init action not supported in the used EGL version." );
    return SCT_FALSE;
#else   /* !defined( EGL_VERSION_1_3 ) */   
    return SCT_TRUE;
#endif  /* !defined( EGL_VERSION_1_3 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiEglPixmapConfigActionExecute( SCTAction* action, int framenumber )
{
    SCTEglPixmapConfigActionContext*    context;
    EglPixmapConfigActionData*          data;
    EGLint                              numConfigs;
    EGLDisplay                          display;
    EGLConfig                           config;
    EGLint                              attribs[]       = { EGL_MATCH_NATIVE_PIXMAP, 0,
                                                            EGL_NONE };
    void*                               pixmap;
    void*                               eglPixmap;    

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglPixmapConfigActionContext* )( action->context );
    data    = &( context->data );

    pixmap = sctiEglModuleGetPixmap( context->moduleContext, data->pixmapIndex );
    if( pixmap == NULL )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in CreatePixmapSurface@Egl action execute." );
        return SCT_FALSE;
    }
    eglPixmap = siWindowGetEglPixmap( sctEglModuleGetWindowContext( context->moduleContext ), pixmap, NULL );
    
    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in PixmapConfig@Egl action execute." );
        return SCT_FALSE;
    }

    attribs[ 1 ] = ( EGLint )( eglPixmap );
    
    if( eglChooseConfig( display, 
                         attribs, 
                         &( config ), 
                         1, 
                         &numConfigs ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Choosing config failed in PixmapConfig@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
    if( numConfigs == 0 )
    {
        SCT_LOG_ERROR( "No config in PixmapConfig@Egl action execute." );
        return SCT_FALSE;
    }

    sctiEglModuleSetConfig( context->moduleContext, data->configIndex, config );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglPixmapConfigActionTerminate( SCTAction* action )
{
    SCTEglPixmapConfigActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTEglPixmapConfigActionContext* )( action->context );

    sctiEglModuleDeleteConfig( context->moduleContext, context->data.configIndex );
}

/*!
 *
 *
 */
void sctiEglPixmapConfigActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglPixmapConfigActionContext( ( SCTEglPixmapConfigActionContext* )( action->context ) );
    action->context = NULL;
}

