/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcheckextensionaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_egl.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCheckExtensionActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCheckExtensionActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCheckExtensionActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCheckExtensionActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CheckExtension@Egl action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCheckExtensionActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );
    
    if( sctiParseEglCheckExtensionActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCheckExtensionActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CheckExtension@Egl context creation." );
        sctiDestroyEglCheckExtensionActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCheckExtensionActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCheckExtensionActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglCheckExtensionActionContext*  context;
    EGLDisplay                          display;    
    const char*                         str;
    const char*                         s;
    int                                 len;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTEglCheckExtensionActionContext* )( action->context );

    display = sctiEglModuleGetDisplay( context->moduleContext, context->data.displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CheckExtension@Egl action execute." );
        return SCT_FALSE;
    }
        
    /* Get EGL extension string. */
    str = ( const char* )( eglQueryString( display, EGL_EXTENSIONS ) );
    if( str == NULL )
    {
        SCT_LOG_ERROR( "Getting extension string failed in CheckExtension@Egl action execute." );
        return SCT_FALSE;
    }

    len = strlen( context->data.extension );
    
    if( ( s = strstr( str, context->data.extension ) ) == NULL ||
        ( s[ len ] != ' ' &&
          s[ len ] != '\0' ) )
    {
        char buf[ 1024 ];
        sprintf( buf, "Extension %s not found in CheckExtension@Egl action execute.", context->data.extension );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;       
    }  

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCheckExtensionActionDestroy( SCTAction* action )
{
    SCTEglCheckExtensionActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTEglCheckExtensionActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyEglCheckExtensionActionContext( context );
}

