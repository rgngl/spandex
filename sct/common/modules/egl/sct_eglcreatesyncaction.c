/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatesyncaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreateSyncActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreateSyncActionContext*  context;
    int                             len;
    int                             i;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreateSyncActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreateSyncActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateSync@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreateSyncActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreateSyncActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreateSyncActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CreateSync@Egl context creation." );
        sctiDestroyEglCreateSyncActionContext( context );
        return NULL;
    }   
    
    if( sctiEglModuleIsValidSyncIndex( context->moduleContext, context->data.syncIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid sync index in CreateSync@Egl context creation." );
        sctiDestroyEglCreateSyncActionContext( context );
        return NULL;
    }
    
    len = context->data.attributes->length;

    /* Reserve space for an EGL_NONE at the end. */
    context->eglAttributes = ( EGLint* )( siCommonMemoryAlloc( NULL, ( len + 1 ) * sizeof( EGLint ) ) );
    if( context->eglAttributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateSync@Egl context creation." );
        sctiDestroyEglCreateSyncActionContext( context );        
        return NULL;
    }

    for( i = 0; i < len; ++i )
    {
        context->eglAttributes[ i ] = ( ( EGLint* )( context->data.attributes->data ) )[ i ];
    }

    context->eglAttributes[ len ] = EGL_NONE;   
    context->validated            = SCT_FALSE;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreateSyncActionContext( void* context )
{
    SCTEglCreateSyncActionContext*  c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTEglCreateSyncActionContext* )( context );

    if( c->eglAttributes != NULL )
    {
        siCommonMemoryFree( NULL, c->eglAttributes );
        c->eglAttributes = NULL;
    }
    
    if( c->data.attributes != NULL )
    {
        sctDestroyIntVector( c->data.attributes );
        c->data.attributes = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateSyncActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglCreateSyncActionContext*  context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglCreateSyncActionContext* )( action->context );  
    context->validated        = SCT_FALSE;   
    context->eglCreateSyncKhr = NULL;

#if !defined( EGL_VERSION_1_1 )
    SCT_LOG_ERROR( "CreateSync@Egl action not supported in the used EGL version." );
    return SCT_FALSE;
#else   /* !defined( EGL_VERSION_1_1 ) */   
    return SCT_TRUE;
#endif  /* !defined( EGL_VERSION_1_1 ) */    
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateSyncActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreateSyncActionContext*  context;
    EglCreateSyncActionData*        data;
    EGLDisplay                      display;    
    EGLSyncKHR                      eglSync;
    EGLint*                         attribsPointer;
    const char*                     extString;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreateSyncActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CreateSync@Egl action execute." );
        return SCT_FALSE;
    }
    
    if( context->validated == SCT_FALSE )
    {
        context->eglCreateSyncKhr = ( SCTPfnEglCreateSyncKhr )( siCommonGetProcAddress( NULL, "eglCreateSyncKHR" ) );

        if( context->eglCreateSyncKhr == NULL )
        {
            SCT_LOG_ERROR( "eglCreateSyncKHR function not found in CreateSync@Egl action execute." );
            return SCT_FALSE;
        }

        extString = ( const char* )( eglQueryString( display, EGL_EXTENSIONS ) );
        if( extString == NULL || strstr( extString, "EGL_KHR_fence_sync" ) == NULL )
        {
            SCT_LOG_ERROR( "EGL_KHR_fence_sync extension not supported in CreateSync@Egl action execute." );
            return SCT_FALSE;
        }

        context->validated = SCT_TRUE;
    }
     
    /* Some egl implementations might not support attributes at all so use NULL
     * pointer as attributes with default values.  */
    if( data->attributes->length == 0 )
    {
        attribsPointer = NULL;
    }
    else
    {
        SCT_ASSERT( context->eglAttributes != NULL );
        attribsPointer = context->eglAttributes;
    }
    
    SCT_ASSERT( context->eglCreateSyncKhr != NULL );

    eglSync = context->eglCreateSyncKhr( display, data->type, attribsPointer );
    if( eglSync == EGL_NO_SYNC_KHR )
    {
        char    buf[ 256 ];
        sprintf( buf, "Creating sync failed in CreateSync@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    sctiEglModuleSetSync( context->moduleContext, data->syncIndex, eglSync );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreateSyncActionTerminate( SCTAction* action )
{
    SCTEglCreateSyncActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreateSyncActionContext* )( action->context );

    sctiEglModuleDeleteSync( context->moduleContext,
                             context->data.displayIndex,
                             context->data.syncIndex,
                             NULL );

    context->validated        = SCT_FALSE;   
    context->eglCreateSyncKhr = NULL;
}

/*!
 *
 *
 */
void sctiEglCreateSyncActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreateSyncActionContext( ( SCTEglCreateSyncActionContext* )( action->context ) );
    action->context = NULL;
}

