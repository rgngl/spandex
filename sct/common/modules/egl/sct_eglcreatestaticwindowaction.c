/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatestaticwindowaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_siwindow.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreateStaticWindowActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreateStaticWindowActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreateStaticWindowActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreateStaticWindowActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateStaticWindow@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreateStaticWindowActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreateStaticWindowActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreateStaticWindowActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidWindowIndex( context->moduleContext, context->data.windowIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid window index in CreateStaticWindow@Egl context creation." );
        sctiDestroyEglCreateStaticWindowActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreateStaticWindowActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateStaticWindowActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreateStaticWindowActionContext*  context;
    EglCreateStaticWindowActionData*        data;
    int                                     index;
    void*                                   window;
    int                                     width               = -1;
    int                                     height              = -1;
    void*                                   siWindowContext;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreateStaticWindowActionContext* )( action->context );
    data    = &( context->data );

    index = data->windowIndex;
    if( sctiEglModuleGetWindow( context->moduleContext, index ) != NULL )
    {
        SCT_LOG_ERROR( "Window index already in use in CreateStaticWindow@Egl action execute." );
        return SCT_FALSE;
    }

    siWindowContext = sctEglModuleGetWindowContext( context->moduleContext );

    if( siWindowIsScreenSupported( siWindowContext, data->screen ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Screen not supported in CreateStaticWindow@Egl action execute." );
        return SCT_FALSE;
    }
    
    if( data->width < 0 || data->height < 0 )
    {
        siWindowGetScreenSize( siWindowContext, data->screen, &width, &height );        
    }
    
    if( data->width >= 0 )
    {
        width = data->width;
    }

    if( data->height >= 0 )
    {
        height = data->height;
    }  

    window = siWindowCreateWindow( siWindowContext,
                                   data->screen,
                                   data->x, 
                                   data->y, 
                                   width, 
                                   height, 
                                   data->format,
                                   NULL );
       
#if !defined( SCT_EGL_MODULE_ALLOW_NULL_WINDOW_HANDLE )
    if( window == NULL )
    {
        SCT_LOG_ERROR( "Window creation failed in CreateStaticWindow@Egl action execute." );
        return SCT_FALSE;
    }
#endif  /* !defined( SCT_EGL_MODULE_ALLOW_NULL_WINDOW_HANDLE ) */
    
    sctiEglModuleSetWindow( context->moduleContext, index, window );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreateStaticWindowActionDestroy( SCTAction* action )
{
    SCTEglCreateStaticWindowActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTEglCreateStaticWindowActionContext* )( action->context );

    sctiEglModuleDeleteWindow( context->moduleContext, context->data.windowIndex );

    sctiDestroyEglCreateStaticWindowActionContext( ( SCTEglCreateStaticWindowActionContext* )( action->context ) );
    action->context = NULL;
}

