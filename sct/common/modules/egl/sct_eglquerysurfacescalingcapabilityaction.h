/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_EGLQUERYSURFACESCALINGCAPABILITYACTION_H__ )
#define __SCT_EGLQUERYSURFACESCALINGCAPABILITYACTION_H__

#include "sct_types.h"
#include "sct_eglmodule.h"
#include "sct_eglmodule_parser.h"

/*!
 *
 *
 */

typedef EGLBoolean ( *SCTEglQuerySurfaceScalingCapabilityNOKProc )( EGLDisplay dpy, EGLConfig config, EGLint surface_width, EGLint surface_height, EGLint target_width, EGLint target_height, EGLint *value );

/*!
 *
 *
 */
typedef struct
{
    SCTEglModuleContext*                        moduleContext;
    SCTEglQuerySurfaceScalingCapabilityNOKProc  func;    
    SCTBoolean                                  funcValidated;    
    EglQuerySurfaceScalingCapabilityActionData  data;
} SCTEglQuerySurfaceScalingCapabilityActionContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                               sctiCreateEglQuerySurfaceScalingCapabilityActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroyEglQuerySurfaceScalingCapabilityActionContext( void* context );

    SCTBoolean                          sctiEglQuerySurfaceScalingCapabilityActionInit( SCTAction *action, SCTBenchmark *benchmark );
    SCTBoolean                          sctiEglQuerySurfaceScalingCapabilityActionExecute( SCTAction* action, int frameNumber );
    void                                sctiEglQuerySurfaceScalingCapabilityActionTerminate( SCTAction* action );
    void                                sctiEglQuerySurfaceScalingCapabilityActionDestroy( SCTAction* action );
    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_EGLQUERYSURFACESCALINGCAPABILITYACTION_H__ ) */
