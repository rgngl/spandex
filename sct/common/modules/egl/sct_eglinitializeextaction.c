/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglinitializeextaction.h"
#include "sct_sicommon.h"
#include "sct_siwindow.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglInitializeExtActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglInitializeExtActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglInitializeExtActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglInitializeExtActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in InitializeExt@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglInitializeExtActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglInitializeExtActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglInitializeExtActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in InitializeExt@Egl context creation." );
        sctiDestroyEglInitializeExtActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglInitializeExtActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglInitializeExtActionExecute( SCTAction* action, int framenumber )
{
    SCTEglInitializeExtActionContext*   context;
    EglInitializeExtActionData*         data;
    EGLDisplay                          display;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglInitializeExtActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in InitializeExt@Egl action execute." );
        return SCT_FALSE;
    }

    if( eglInitialize( display, NULL, NULL ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Egl initialization failed in InitializeExt@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        
        return SCT_FALSE;
    }           

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglInitializeExtActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglInitializeExtActionContext( ( SCTEglInitializeExtActionContext* )( action->context ) );
    action->context = NULL;
}

