/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_egldestroysyncaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglDestroySyncActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglDestroySyncActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglDestroySyncActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglDestroySyncActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroySync@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglDestroySyncActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglDestroySyncActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglDestroySyncActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in DestroySync@Egl context creation." );
        sctiDestroyEglDestroySyncActionContext( context );
        return NULL;
    }   
    
    if( sctiEglModuleIsValidSyncIndex( context->moduleContext, context->data.syncIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid sync index in DestroySync@Egl context creation." );
        sctiDestroyEglDestroySyncActionContext( context );
        return NULL;
    }

    context->validated = SCT_FALSE;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglDestroySyncActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglDestroySyncActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglDestroySyncActionContext* context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglDestroySyncActionContext* )( action->context );

    context->validated         = SCT_FALSE;
    context->eglDestroySyncKhr = NULL;

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglDestroySyncActionExecute( SCTAction* action, int framenumber )
{
    SCTEglDestroySyncActionContext* context;
    EglDestroySyncActionData*       data;
    EGLDisplay                      display; 
    int                             syncIndex;
    const char*                     extString;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglDestroySyncActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in DestroySync@Egl action execute." );
        return SCT_FALSE;
    }

    if( context->validated == SCT_FALSE )
    {
        context->eglDestroySyncKhr = ( SCTPfnEglDestroySyncKhr )( siCommonGetProcAddress( NULL, "eglDestroySyncKHR" ) );

        if( context->eglDestroySyncKhr == NULL )
        {
            SCT_LOG_ERROR( "eglDestroySyncKHR function not found in DestroySync@Egl action execute." );
            return SCT_FALSE;
        }

        extString = ( const char* )( eglQueryString( display, EGL_EXTENSIONS ) );
        if( extString == NULL || strstr( extString, "EGL_KHR_fence_sync" ) == NULL )
        {
            SCT_LOG_ERROR( "EGL_KHR_fence_sync extension not supported in DestroySync@Egl action execute." );
            return SCT_FALSE;
        }

        context->validated = SCT_TRUE;
    }

    syncIndex = data->syncIndex;
    if( data->checkError != SCT_FALSE )
    {
        if( sctiEglModuleGetSync( context->moduleContext, syncIndex ) == EGL_NO_SYNC_KHR )
        {
            SCT_LOG_ERROR( "Invalid sync in DestroySync@Egl action execute." );
            return SCT_FALSE;
        }
    }

    /* Potential race condition, but sctiEglModuleDeleteSync works even if the same sync 
     * is destroyed by two threads simultaneously. */
    sctiEglModuleDeleteSync( context->moduleContext,
                             context->data.displayIndex,
                             syncIndex,
                             context->eglDestroySyncKhr );    

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglDestroySyncActionTerminate( SCTAction* action )
{
    SCTEglDestroySyncActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglDestroySyncActionContext* )( action->context );

    context->validated         = SCT_FALSE;
    context->eglDestroySyncKhr = NULL;
}

/*!
 *
 *
 */
void sctiEglDestroySyncActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglDestroySyncActionContext( ( SCTEglDestroySyncActionContext* )( action->context ) );
    action->context = NULL;
}

