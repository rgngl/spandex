/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglconfigexactaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglConfigExactActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglConfigExactActionContext* context;
    int                             len;
    int                             i;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglConfigExactActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglConfigExactActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ConfigExact@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglConfigExactActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglConfigExactActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglConfigExactActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in ConfigExact@Egl context creation." );
        sctiDestroyEglConfigExactActionContext( context );
        return NULL;
    }
   
    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in ConfigExact@Egl context creation." );
        sctiDestroyEglConfigExactActionContext( context );
        return NULL;
    }

    len = context->data.attributes->length;    
    if( len < 2 )
    {
        SCT_LOG_ERROR( "Invalid Attributes in ConfigExact@Egl context creation." );
        sctiDestroyEglConfigExactActionContext( context );
        return NULL;
    }

    /* Reserve space for an EGL_NONE at the end. */
    context->eglAttributes = ( EGLint* )( siCommonMemoryAlloc( NULL, ( len + 1 ) * sizeof( EGLint ) ) );
    if( context->eglAttributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ConfigExact@Egl context creation." );
        sctiDestroyEglConfigExactActionContext( context );        
        return NULL;
    }

    for( i = 0; i < len; ++i )
    {
        context->eglAttributes[ i ] = ( ( EGLint* )( context->data.attributes->data ) )[ i ];
    }

    context->eglAttributes[ len ] = EGL_NONE;
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglConfigExactActionContext( void* context )
{
    SCTEglConfigExactActionContext* c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTEglConfigExactActionContext* )( context );

    if( c->eglAttributes != NULL )
    {
        siCommonMemoryFree( NULL, c->eglAttributes );
        c->eglAttributes = NULL;
    }
    
    if( c->data.attributes != NULL )
    {
        sctDestroyIntVector( c->data.attributes );
        c->data.attributes = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglConfigExactActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglConfigExactActionExecute( SCTAction* action, int framenumber )
{
    SCTEglConfigExactActionContext* context;
    EglConfigExactActionData*       data;
    EGLDisplay                      display;
    EGLint                          maxNumConfigs;
    EGLint                          numConfigs;
    EGLConfig*                      configs;
    EGLConfig                       config          = 0;
    SCTBoolean                      foundConfig     = SCT_FALSE;
    int                             i;
    int                             j;
    EGLint                          t;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglConfigExactActionContext* )( action->context );
    data    = &( context->data );
    
    SCT_ASSERT( context->eglAttributes != NULL );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in ConfigExact@Egl action execute." );
        return SCT_FALSE;
    }

    if( eglChooseConfig( display, context->eglAttributes, NULL, 0, &maxNumConfigs ) == EGL_FALSE)
    {
        char    buf[ 256 ];
        sprintf( buf, "Choosing config failed in ConfigExact@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    if( maxNumConfigs == 0 )
    {
        SCT_LOG_ERROR( "No matching config in ConfigExact@Egl action execute." );
        return SCT_FALSE;
    }

    configs  = ( EGLConfig* )( siCommonMemoryAlloc( NULL, maxNumConfigs * sizeof( EGLConfig ) ) );
    if( configs == NULL )
    {
        SCT_LOG_ERROR( "Config allocation failed in ConfigExact@Egl action execute." );
        return SCT_FALSE;
    }
    memset( configs, 0, maxNumConfigs * sizeof( EGLConfig ) );
    
    if( eglChooseConfig( display, context->eglAttributes, configs, maxNumConfigs, &numConfigs ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Choosing config failed in ConfigExact@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        siCommonMemoryFree( NULL, configs );

        return SCT_FALSE;
    }  
    
    for( i = 0; i < numConfigs; ++i )
    {
        for( j = 0; context->eglAttributes[ j ] != EGL_NONE; j += 2 )
        {
            if( context->eglAttributes[ j + 1 ] == EGL_DONT_CARE )
            {
                /* Skip input attributes with EGL_DONT_CARE value. */
                continue;
            }
            
            eglGetConfigAttrib( display, configs[ i ], context->eglAttributes[ j ], &t );
            if( context->eglAttributes[ j + 1 ] != t )
            {
                /* Skip configs that have mismatching attribute values. */
                continue;
            }
        }
        
        /* Reached the end of egl attributes succesfully, configs match
         * exactly enough. */
        if( context->eglAttributes[ j ] == EGL_NONE )
        {
            config      = configs[ i ];
            foundConfig = SCT_TRUE;
            break;
        }
    }

    siCommonMemoryFree( NULL, configs );

    if( foundConfig == SCT_FALSE )
    {
        SCT_LOG_ERROR( "No matching config found in ConfigExact@Egl action execute." );
        return SCT_FALSE;
    }

#if defined( SCT_DEBUG )
    {
        char name[ 32 ];
        char value[ 1024 ];

        name[ 0 ]  = '\0';
        value[ 0 ] = '\0';        
        sctiEglGetConfigNameValue( display, config, name, sizeof( name ), value, sizeof( value ) );
        siCommonDebugPrintf( NULL, "SPANDEX: ConfigExact@Egl selected %s: %s", name, value );
    }
#endif  /* defined( SCT_DEBUG ) */   
    
    sctiEglModuleSetConfig( context->moduleContext, data->configIndex, config );    

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglConfigExactActionTerminate( SCTAction* action )
{
    SCTEglConfigExactActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTEglConfigExactActionContext* )( action->context );

    sctiEglModuleDeleteConfig( context->moduleContext, context->data.configIndex );
}

/*!
 *
 *
 */
void sctiEglConfigExactActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglConfigExactActionContext( ( SCTEglConfigExactActionContext* )( action->context ) );
    action->context = NULL;
}

