/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglterminateaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglTerminateActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglTerminateActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglTerminateActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglTerminateActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Terminate@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglTerminateActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglTerminateActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglTerminateActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in Terminate@Egl context creation." );
        sctiDestroyEglTerminateActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglTerminateActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglTerminateActionExecute( SCTAction* action, int framenumber )
{
    SCTEglTerminateActionContext*   context;
    EglTerminateActionData*         data;
    EGLDisplay                      display;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglTerminateActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in Terminate@Egl action execute." );
        return SCT_FALSE;
    }

    eglTerminate( display );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglTerminateActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
        
    sctiDestroyEglTerminateActionContext( ( SCTEglTerminateActionContext* )( action->context ) );
    action->context = NULL;
}

