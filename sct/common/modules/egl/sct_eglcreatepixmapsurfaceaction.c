/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatepixmapsurfaceaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreatePixmapSurfaceActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreatePixmapSurfaceActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreatePixmapSurfaceActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreatePixmapSurfaceActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePixmapSurface@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreatePixmapSurfaceActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreatePixmapSurfaceActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreatePixmapSurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CreatePixmapSurface@Egl context creation." );
        sctiDestroyEglCreatePixmapSurfaceActionContext( context );        
        return NULL;
    }   
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in CreatePixmapSurface@Egl context creation." );
        sctiDestroyEglCreatePixmapSurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in CreatePixmapSurface@Egl context creation." );
        sctiDestroyEglCreatePixmapSurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidPixmapIndex( context->moduleContext, context->data.pixmapIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in CreatePixmapSurface@Egl context creation." );
        sctiDestroyEglCreatePixmapSurfaceActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreatePixmapSurfaceActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePixmapSurfaceActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglCreatePixmapSurfaceActionContext* context;
    EglCreatePixmapSurfaceActionData*       data;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglCreatePixmapSurfaceActionContext* )( action->context );
    data    = &( context->data );
   
#if !defined( EGL_VERSION_1_2 )
    if( data->colorSpace != EGL_NONE && data->colorSpace != EGL_COLORSPACE_sRGB )
    {
        SCT_LOG_ERROR( "CreatePixmapSurface@Egl action init ColorSpace attribute not supported in the current EGL version." );
        return SCT_FALSE;
    }
    if( data->alphaFormat != EGL_NONE && data->alphaFormat != EGL_ALPHA_FORMAT_NONPRE )
    {
        SCT_LOG_ERROR( "CreatePixmapSurface@Egl action init AlphaFormat attribute not supported in the current EGL version." );
        return SCT_FALSE;
    }
#endif  /* !defined( EGL_VERSION_1_2 ) */

    SCT_USE_VARIABLE( data );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePixmapSurfaceActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreatePixmapSurfaceActionContext* context;
    EglCreatePixmapSurfaceActionData*       data;
    int                                     surfaceIndex;
    int                                     configIndex;
    int                                     pixmapIndex;
    EGLDisplay                              display;
    EGLConfig                               config;
    EGLSurface                              surface;
    void*                                   pixmap;
    void*                                   eglPixmap;    
    int                                     attribIndex;

#if !defined( EGL_VERSION_1_2 )
    EGLint                                  attribs[]       = { EGL_NONE };
#else   /* !defined( EGL_VERSION_1_2 ) */
    EGLint                                  attribs[]       = { EGL_COLORSPACE,    EGL_COLORSPACE_sRGB,
                                                                EGL_ALPHA_FORMAT,  EGL_ALPHA_FORMAT_NONPRE,
                                                                EGL_NONE };
#endif  /* !defined( EGL_VERSION_1_2 ) */

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreatePixmapSurfaceActionContext* )( action->context );
    data    = &( context->data );

    surfaceIndex = data->surfaceIndex;
    configIndex  = data->configIndex;
    pixmapIndex  = data->pixmapIndex;

#if defined( EGL_VERSION_1_2 )
    attribIndex = 0;
    if( data->colorSpace != EGL_NONE )
    {
        attribs[ attribIndex++ ] = EGL_COLORSPACE;
        attribs[ attribIndex++ ] = data->colorSpace;
    }
    if( data->alphaFormat != EGL_NONE )
    {
        attribs[ attribIndex++ ] = EGL_ALPHA_FORMAT;
        attribs[ attribIndex++ ] = data->alphaFormat;
    }
    attribs[ attribIndex ] = EGL_NONE;
#else   /* defined( EGL_VERSION_1_2 ) */
    SCT_USE_VARIABLE( attribIndex );
#endif  /* defined( EGL_VERSION_1_2 ) */

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CreatePixmapSurface@Egl action execute." );
        return SCT_FALSE;
    }

    if( sctiEglModuleGetSurface( context->moduleContext, surfaceIndex ) != EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Surface index already in use in CreatePixmapSurface@Egl action execute." );
        return SCT_FALSE;
    }

    config = sctiEglModuleGetConfig( context->moduleContext, configIndex );

    pixmap = sctiEglModuleGetPixmap( context->moduleContext, pixmapIndex );
    if( pixmap == NULL )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in CreatePixmapSurface@Egl action execute." );
        return SCT_FALSE;
    }
    eglPixmap = siWindowGetEglPixmap( sctEglModuleGetWindowContext( context->moduleContext ), pixmap, NULL );
    
    surface = eglCreatePixmapSurface( display, config, ( NativePixmapType )( eglPixmap ), attribs );
    if( surface == EGL_NO_SURFACE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Creating pixmap surface failed in CreatePixmapSurface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    sctiEglModuleSetSurface( context->moduleContext, surfaceIndex, surface );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreatePixmapSurfaceActionTerminate( SCTAction* action )
{
    SCTEglCreatePixmapSurfaceActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreatePixmapSurfaceActionContext* )( action->context );

    sctiEglModuleDeleteSurface( context->moduleContext,
                                context->data.displayIndex,
                                context->data.surfaceIndex );    
}

/*!
 *
 *
 */
void sctiEglCreatePixmapSurfaceActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreatePixmapSurfaceActionContext( ( SCTEglCreatePixmapSurfaceActionContext* )( action->context ) );
    action->context = NULL;
}

