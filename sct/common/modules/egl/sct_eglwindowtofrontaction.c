/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglwindowtofrontaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglWindowToFrontActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglWindowToFrontActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglWindowToFrontActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglWindowToFrontActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in WindowToFront@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglWindowToFrontActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglWindowToFrontActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglWindowToFrontActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidWindowIndex( context->moduleContext, context->data.windowIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid window index in WindowToFront@Egl context creation." );
        sctiDestroyEglWindowToFrontActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglWindowToFrontActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglWindowToFrontActionExecute( SCTAction* action, int framenumber )
{
    SCTEglWindowToFrontActionContext*   context;
    EglWindowToFrontActionData*         data;
    void*                               window;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglWindowToFrontActionContext* )( action->context );
    data    = &( context->data );

    window = sctiEglModuleGetWindow( context->moduleContext, data->windowIndex );
    if( window == NULL )
    {
        SCT_LOG_ERROR( "Undefined window in WindowToFront@Egl action execute." );
        return SCT_FALSE;
    }

    if( siWindowToFront( sctEglModuleGetWindowContext( context->moduleContext ), window ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Moving window to front failed in WindowToFront@Egl action execute." );
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglWindowToFrontActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglWindowToFrontActionContext( ( SCTEglWindowToFrontActionContext* )( action->context ) );
    action->context = NULL;
}

