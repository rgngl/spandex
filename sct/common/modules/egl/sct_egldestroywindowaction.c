/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_egldestroywindowaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglDestroyWindowActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglDestroyWindowActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglDestroyWindowActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglDestroyWindowActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyWindow@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglDestroyWindowActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglDestroyWindowActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglDestroyWindowActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidWindowIndex( context->moduleContext, context->data.windowIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid window index in DestroyWindow@Egl context creation." );
        sctiDestroyEglDestroyWindowActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglDestroyWindowActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglDestroyWindowActionExecute( SCTAction* action, int framenumber )
{
    SCTEglDestroyWindowActionContext*   context;
    EglDestroyWindowActionData*         data;
    int                                 index;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglDestroyWindowActionContext* )( action->context );
    data    = &( context->data );

    index = data->windowIndex;
    if( sctiEglModuleGetWindow( context->moduleContext, index ) == NULL )
    {
        SCT_LOG_ERROR( "Undefined window in DestroyWindow@Egl action execute." );
        return SCT_FALSE;
    }

    sctiEglModuleDeleteWindow( context->moduleContext, context->data.windowIndex );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglDestroyWindowActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglDestroyWindowActionContext( ( SCTEglDestroyWindowActionContext* )( action->context ) );
    action->context = NULL;
}

