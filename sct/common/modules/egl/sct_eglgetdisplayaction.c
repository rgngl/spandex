/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglgetdisplayaction.h"
#include "sct_sicommon.h"
#include "sct_siwindow.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglGetDisplayActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglGetDisplayActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglGetDisplayActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglGetDisplayActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in GetDisplay@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglGetDisplayActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglGetDisplayActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglGetDisplayActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in GetDisplay@Egl context creation." );
        sctiDestroyEglGetDisplayActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglGetDisplayActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglGetDisplayActionExecute( SCTAction* action, int framenumber )
{
    SCTEglGetDisplayActionContext*  context;
    EglGetDisplayActionData*        data;
    NativeDisplayType               displayId;
    EGLDisplay                      display;
    void*                           siWindowContext;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglGetDisplayActionContext* )( action->context );
    data    = &( context->data );

    siWindowContext = sctEglModuleGetWindowContext( context->moduleContext );

    if( siWindowIsScreenSupported( siWindowContext, data->screen ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Screen not supported in GetDisplay@Egl action execute." );
        return SCT_FALSE;
    }

    displayId = ( NativeDisplayType )( siWindowGetScreenEglDisplayId( siWindowContext, data->screen ) );
    
    display = eglGetDisplay( displayId );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Getting screen EGL display ID failed in GetDisplay@Egl action execute." );
        return SCT_FALSE;
    }
        
    sctiEglModuleSetDisplay( context->moduleContext, data->displayIndex, display );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglGetDisplayActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglGetDisplayActionContext( ( SCTEglGetDisplayActionContext* )( action->context ) );
    action->context = NULL;
}
