/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatewindowsurfaceaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreateWindowSurfaceActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreateWindowSurfaceActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreateWindowSurfaceActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreateWindowSurfaceActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateWindowSurface@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreateWindowSurfaceActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreateWindowSurfaceActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreateWindowSurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CreateWindowSurface@Egl context creation." );
        sctiDestroyEglCreateWindowSurfaceActionContext( context );
        return NULL;
    }
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in CreateWindowSurface@Egl context creation." );
        sctiDestroyEglCreateWindowSurfaceActionContext( context );
        return NULL;
    }

    if( context->data.windowIndex != -1 &&
        sctiEglModuleIsValidWindowIndex( context->moduleContext, context->data.windowIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid window index in CreateWindowSurface@Egl context creation." );
        sctiDestroyEglCreateWindowSurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in CreateWindowSurface@Egl context creation." );
        sctiDestroyEglCreateWindowSurfaceActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreateWindowSurfaceActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateWindowSurfaceActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglCreateWindowSurfaceActionContext* context;
    EglCreateWindowSurfaceActionData*       data;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglCreateWindowSurfaceActionContext* )( action->context );
    data    = &( context->data );

#if !defined( EGL_VERSION_1_2 )
    if( data->renderBuffer != EGL_NONE && data->renderBuffer != EGL_BACK_BUFFER )
    {
        SCT_LOG_ERROR( "CreateWindowSurface@Egl action init RenderBuffer attribute not supported in the current EGL version." );
        return SCT_FALSE;
    }
    if( data->colorSpace != EGL_NONE && data->colorSpace != EGL_COLORSPACE_sRGB )
    {
        SCT_LOG_ERROR( "CreateWindowSurface@Egl action init ColorSpace attribute not supported in the current EGL version." );
        return SCT_FALSE;
    }
    if( data->alphaFormat != EGL_NONE && data->alphaFormat != EGL_ALPHA_FORMAT_NONPRE )
    {
        SCT_LOG_ERROR( "CreateWindowSurface@Egl action init AlphaFormat attribute not supported in the current EGL version." );
        return SCT_FALSE;
    }
#endif  /* !defined( EGL_VERSION_1_2 ) */

    SCT_USE_VARIABLE( data );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateWindowSurfaceActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreateWindowSurfaceActionContext* context;
    EglCreateWindowSurfaceActionData*       data;
    int                                     surfaceIndex;
    int                                     windowIndex;
    int                                     configIndex;
    EGLDisplay                              display;
    void*                                   window          = NULL;
    void*                                   eglWindow       = NULL;
    EGLConfig                               config;
    int                                     attribIndex;
    EGLSurface                              surface;
    EGLint*                                 attribsPointer;
    
#if !defined( EGL_VERSION_1_2 )
    EGLint                                  attribs[]       = { EGL_NONE };
#else   /* !defined( EGL_VERSION_1_2 ) */
    EGLint                                  attribs[]       = { EGL_RENDER_BUFFER, EGL_BACK_BUFFER,
                                                                EGL_COLORSPACE,    EGL_COLORSPACE_sRGB,
                                                                EGL_ALPHA_FORMAT,  EGL_ALPHA_FORMAT_NONPRE,
                                                                EGL_NONE };
#endif  /* !defined( EGL_VERSION_1_2 ) */

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreateWindowSurfaceActionContext* )( action->context );
    data    = &( context->data );

    surfaceIndex = data->surfaceIndex;
    windowIndex  = data->windowIndex;
    configIndex  = data->configIndex;

    attribIndex  = 0;    
#if defined( EGL_VERSION_1_2 )
    if( data->renderBuffer != EGL_NONE )
    {
        attribs[ attribIndex++ ] = EGL_RENDER_BUFFER;
        attribs[ attribIndex++ ] = data->renderBuffer;
    }
    if( data->colorSpace != EGL_NONE )
    {
        attribs[ attribIndex++ ] = EGL_COLORSPACE;
        attribs[ attribIndex++ ] = data->colorSpace;
    }
    if( data->alphaFormat != EGL_NONE )
    {
        attribs[ attribIndex++ ] = EGL_ALPHA_FORMAT;
        attribs[ attribIndex++ ] = data->alphaFormat;
    }
    attribs[ attribIndex ] = EGL_NONE;
#endif  /* defined( EGL_VERSION_1_2 ) */

    /* Some egl implementations do not support attributes at all so use NULL
     * pointer as attributes with default values.  */
    if( attribIndex == 0 )
    {
        attribsPointer = NULL;
    }
    else
    {
        attribsPointer = attribs;
    }

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CreateWindowSurface@Egl action execute." );
        return SCT_FALSE;
    }

    if( sctiEglModuleGetSurface( context->moduleContext, surfaceIndex ) != EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Surface index already in use in CreateWindowSurface@Egl action execute." );
        return SCT_FALSE;
    }

    config = sctiEglModuleGetConfig( context->moduleContext, configIndex );

    if( windowIndex >= 0 )
    {
        window = sctiEglModuleGetWindow( context->moduleContext, windowIndex );

#if !defined( SCT_EGL_MODULE_ALLOW_NULL_WINDOW_HANDLE )
        if( window == NULL )
        {
            SCT_LOG_ERROR( "Invalid window index in CreateWindowSurface@Egl action execute." );
            return SCT_FALSE;
        }
        eglWindow = siWindowGetEglWindow( sctEglModuleGetWindowContext( context->moduleContext ), window );
#endif  /* !defined( SCT_EGL_MODULE_ALLOW_NULL_WINDOW_HANDLE ) */
    }

    surface = eglCreateWindowSurface( display, config, ( NativeWindowType )( eglWindow ), attribsPointer );
    if( surface == EGL_NO_SURFACE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Creating window surface failed in CreateWindowSurface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    sctiEglModuleSetSurface( context->moduleContext, surfaceIndex, surface );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreateWindowSurfaceActionTerminate( SCTAction* action )
{
    SCTEglCreateWindowSurfaceActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreateWindowSurfaceActionContext* )( action->context );

    sctiEglModuleDeleteSurface( context->moduleContext,
                                context->data.displayIndex,
                                context->data.surfaceIndex );    
}

/*!
 *
 *
 */
void sctiEglCreateWindowSurfaceActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreateWindowSurfaceActionContext( ( SCTEglCreateWindowSurfaceActionContext* )( action->context ) );
    action->context = NULL;
}

