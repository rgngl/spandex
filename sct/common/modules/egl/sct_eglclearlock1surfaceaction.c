/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglclearlock1surfaceaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_eglmodule.h"

#include <stdio.h>
#include <string.h>

#if !defined( EGL_BITMAP_POINTER_KHR )
# define EGL_BITMAP_POINTER_KHR             0x30C6
#endif  /* !defined( EGL_BITMAP_POINTER_KHR ) */

#if !defined( EGL_BITMAP_PITCH_KHR )
# define EGL_BITMAP_PITCH_KHR               0x30C7
#endif  /* !defined( EGL_BITMAP_PITCH_KHR ) */

#if !defined( EGL_BITMAP_ORIGIN_KHR )
# define EGL_BITMAP_ORIGIN_KHR              0x30C8
#endif  /* !defined( EGL_BITMAP_ORIGIN_KHR ) */

#if !defined( EGL_LOWER_LEFT_KHR )
# define EGL_LOWER_LEFT_KHR                 0x30CE
#endif  /* !defined( EGL_LOWER_LEFT_KHR ) */

#if !defined( EGL_UPPER_LEFT_KHR )
# define EGL_UPPER_LEFT_KHR                 0x30CF
#endif  /* !defined( EGL_UPPER_LEFT_KHR ) */

#if !defined( EGL_BITMAP_PIXEL_RED_OFFSET_KHR )
# define EGL_BITMAP_PIXEL_RED_OFFSET_KHR    0x30C9
#endif  /* !defined( EGL_BITMAP_PIXEL_RED_OFFSET_KHR ) */

#if !defined( EGL_BITMAP_PIXEL_GREEN_OFFSET_KHR )
# define EGL_BITMAP_PIXEL_GREEN_OFFSET_KHR  0x30CA
#endif  /* !defined( EGL_BITMAP_PIXEL_GREEN_OFFSET_KHR ) */

#if !defined( EGL_BITMAP_PIXEL_BLUE_OFFSET_KHR )
# define EGL_BITMAP_PIXEL_BLUE_OFFSET_KHR  0x30CB
#endif  /* !defined( EGL_BITMAP_PIXEL_BLUE_OFFSET_KHR ) */

#if !defined( EGL_BITMAP_PIXEL_ALPHA_OFFSET_KHR )
# define EGL_BITMAP_PIXEL_ALPHA_OFFSET_KHR  0x30CC
#endif  /* !defined( EGL_BITMAP_PIXEL_ALPHA_OFFSET_KHR ) */

/*!
 *
 *
 */
void* sctiCreateEglClearLock1SurfaceActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglClearLock1SurfaceActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglClearLock1SurfaceActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglClearLock1SurfaceActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ClearLock1Surface@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglClearLock1SurfaceActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglClearLock1SurfaceActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglClearLock1SurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in ClearLock1Surface@Egl context creation." );
        sctiDestroyEglClearLock1SurfaceActionContext( context );                
        return NULL;
    }
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in ClearLock1Surface@Egl context creation." );
        sctiDestroyEglClearLock1SurfaceActionContext( context );        
        return NULL;
    }
   
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglClearLock1SurfaceActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglClearLock1SurfaceActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglClearLock1SurfaceActionContext*   context;
    EglClearLock1SurfaceActionData*         data;    
    EGLDisplay                              display;
    EGLSurface                              surface;
	unsigned char*                          pixels;
    unsigned short*                         sPixels;
    unsigned short                          sColor;
    unsigned int*                           lPixels;
    unsigned int                            lColor;
    EGLint                                  pitch;
    EGLint                                  origin;
    int                                     width;
    int                                     height;
    int                                     surfaceHeight;
    int                                     pixelSize;
    EGLint                                  redOffset;
    EGLint                                  greenOffset;
    EGLint                                  blueOffset;
    EGLint                                  alphaOffset;
    int                                     x;
    int                                     y;
    int                                     ex;
    int                                     ey;
    char                                    buf[ 256 ];
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTEglClearLock1SurfaceActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in ClearLock1Surface@Egl action execute." );
        return SCT_FALSE;
    }

    surface = sctiEglModuleGetSurface( context->moduleContext, data->surfaceIndex );
    if( surface == EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Invalid surface index in ClearLock1Surface@Egl action execute." );
        return SCT_FALSE;
    }

    width         = data->width;
    height        = data->height;
    surfaceHeight = data->surfaceHeight;

    if( ( data->y + height ) > surfaceHeight )
    {
        SCT_LOG_ERROR( "Clear out of surface area ClearLock1Surface@Egl action execute." );
        return SCT_FALSE;
    }       
    
    if( data->format == EGL_FORMAT_RGB_565_EXACT_KHR || data->format == EGL_FORMAT_RGB_565_KHR )
    {
        pixelSize = 16;
    }
    else
    {
        pixelSize = 32;
    }
    
    /* Get locked surface attributes. */
    if( eglQuerySurface( display, surface, EGL_BITMAP_POINTER_KHR, ( EGLint* )( &pixels )) == EGL_FALSE )
    {
        sprintf( buf, "Query surface bitmap pointer failed in ClearLock1Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

	if( eglQuerySurface( display, surface, EGL_BITMAP_PITCH_KHR, &pitch ) == EGL_FALSE )
    {
        sprintf( buf, "Query surface bitmap pitch failed in ClearLock1Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

	if( eglQuerySurface( display, surface, EGL_BITMAP_ORIGIN_KHR, &origin ) == EGL_FALSE )
    {
        sprintf( buf, "Query surface bitmap origin failed in ClearLock1Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }
        
	if( eglQuerySurface( display, surface, EGL_BITMAP_PIXEL_RED_OFFSET_KHR, &redOffset ) == EGL_FALSE )
    {
        sprintf( buf, "Query bitmap pixel red offset failed in ClearLock1Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

	if( eglQuerySurface( display, surface, EGL_BITMAP_PIXEL_GREEN_OFFSET_KHR, &greenOffset ) == EGL_FALSE )
    {
        sprintf( buf, "Query bitmap pixel green offset failed in ClearLock1Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

	if( eglQuerySurface( display, surface, EGL_BITMAP_PIXEL_BLUE_OFFSET_KHR, &blueOffset ) == EGL_FALSE )
    {
        sprintf( buf, "Query bitmap pixel blue offset failed in ClearLock1Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

	if( eglQuerySurface( display, surface, EGL_BITMAP_PIXEL_ALPHA_OFFSET_KHR, &alphaOffset ) == EGL_FALSE )
    {
        sprintf( buf, "Query bitmap pixel alpha offset failed in ClearLock1Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

    /* Move the pixels pointer to upper-left. */
    if( ( origin == EGL_LOWER_LEFT_KHR ) && ( surfaceHeight > 0 ) )
    {
        pixels = &pixels[ ( surfaceHeight - 1 ) * pitch ];
        pitch  = -pitch;
    }

    ex = data->x + width;
    ey = data->y + height;    
    
    /* Finally, clear the frame. */
    if( pixelSize == 16 )
    {
        sColor = ( unsigned short )( ( unsigned char )( ( ( 1 << 5 ) - 1 ) * data->color[ 0 ] ) << redOffset   |
                                     ( unsigned char )( ( ( 1 << 6 ) - 1 ) * data->color[ 1 ] ) << greenOffset |
                                     ( unsigned char )( ( ( 1 << 5 ) - 1 ) * data->color[ 2 ] ) << blueOffset );

        for( y = data->y; y < ey; ++y )        
        {
            sPixels = ( unsigned short* )( &pixels[ y * pitch ] );
            for( x = data->x; x < ex; ++x )
            {
                sPixels[ x ] = sColor;
            }
        }
    }
    else
    {
        lColor = ( unsigned int )( ( unsigned char )( 255 * data->color[ 0 ] ) << redOffset   |
                                   ( unsigned char )( 255 * data->color[ 1 ] ) << greenOffset |
                                   ( unsigned char )( 255 * data->color[ 2 ] ) << blueOffset  |
                                   ( unsigned char )( 255 * data->color[ 3 ] ) << alphaOffset );

        for( y = data->y; y < ey; ++y )                
        {
            lPixels = ( unsigned int* )( &pixels[ y * pitch ] ); 
            for( x = data->x; x < ex; ++x )
            {
                lPixels[ x ] = lColor;
            }
        }
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglClearLock1SurfaceActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglClearLock1SurfaceActionContext( ( SCTEglClearLock1SurfaceActionContext* )( action->context ) );
    action->context = NULL;
}
