/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglmakecurrentextaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglMakeCurrentExtActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglMakeCurrentExtActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglMakeCurrentExtActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglMakeCurrentExtActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in MakeCurrentExt@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglMakeCurrentExtActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglMakeCurrentExtActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglMakeCurrentExtActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in MakeCurrentExt@Egl context creation." );
        sctiDestroyEglMakeCurrentExtActionContext( context );
        return NULL;
    }   
    
    if( context->data.drawSurfaceIndex != -1 &&
        sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.drawSurfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid draw surface index in MakeCurrentExt@Egl context creation." );
        sctiDestroyEglMakeCurrentExtActionContext( context );
        return NULL;
    }

    if( context->data.readSurfaceIndex != -1 &&
        sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.readSurfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid read surface index in MakeCurrentExt@Egl context creation." );
        sctiDestroyEglMakeCurrentExtActionContext( context );
        return NULL;
    }

    if( context->data.contextIndex != -1 &&
        sctiEglModuleIsValidContextIndex( context->moduleContext, context->data.contextIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid context index in MakeCurrentExt@Egl context creation." );
        sctiDestroyEglMakeCurrentExtActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglMakeCurrentExtActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglMakeCurrentExtActionExecute( SCTAction* action, int framenumber )
{
    SCTEglMakeCurrentExtActionContext*  context;
    EglMakeCurrentExtActionData*        data;
    EGLDisplay                          display;
    EGLSurface                          drawSurface = EGL_NO_SURFACE;
    EGLSurface                          readSurface = EGL_NO_SURFACE;
    EGLContext                          eglContext  = EGL_NO_CONTEXT;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglMakeCurrentExtActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in MakeCurrentExt@Egl action execute." );
        return SCT_FALSE;
    }

    if( data->drawSurfaceIndex >= 0 )
    {
        drawSurface = sctiEglModuleGetSurface( context->moduleContext, data->drawSurfaceIndex );
        if( drawSurface == EGL_NO_SURFACE )
        {
            SCT_LOG_ERROR( "Undefined draw surface in MakeCurrentExt@Egl action execute." );
            return SCT_FALSE;
        }
    }

    if( data->readSurfaceIndex >= 0 )
    {
        readSurface = sctiEglModuleGetSurface( context->moduleContext, data->readSurfaceIndex );
        if( readSurface == EGL_NO_SURFACE )
        {
            SCT_LOG_ERROR( "Undefined read surface in MakeCurrentExt@Egl action execute." );
            return SCT_FALSE;
        }
    }

    if( data->contextIndex >= 0 )
    {
        eglContext = sctiEglModuleGetContext( context->moduleContext, data->contextIndex );
        if( eglContext == EGL_NO_CONTEXT )
        {
            SCT_LOG_ERROR( "Undefined context in MakeCurrentExt@Egl action execute." );
            return SCT_FALSE;
        }
    }

    if( eglMakeCurrent( display, drawSurface, readSurface, eglContext ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Make current failed in MakeCurrentExt@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglMakeCurrentExtActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglMakeCurrentExtActionContext( ( SCTEglMakeCurrentExtActionContext* )( action->context ) );
    action->context = NULL;
}

