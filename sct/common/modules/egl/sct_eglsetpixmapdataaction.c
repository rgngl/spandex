/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglsetpixmapdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglSetPixmapDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglSetPixmapDataActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglSetPixmapDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglSetPixmapDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SetPixmapData@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglSetPixmapDataActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglSetPixmapDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglSetPixmapDataActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidPixmapIndex( context->moduleContext, context->data.pixmapIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in SetPixmapData@Egl context creation." );
        sctiDestroyEglSetPixmapDataActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid data index in SetPixmapData@Egl context creation." );
        sctiDestroyEglSetPixmapDataActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglSetPixmapDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglSetPixmapDataActionExecute( SCTAction* action, int framenumber )
{
    SCTEglSetPixmapDataActionContext*   context;
    EglSetPixmapDataActionData*         data;
    void*                               pixmap;
    SCTEglData*                         eglData;    
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglSetPixmapDataActionContext* )( action->context );
    data    = &( context->data );

    pixmap = sctiEglModuleGetPixmap( context->moduleContext, data->pixmapIndex );
    if( pixmap == NULL  )
    {
        SCT_LOG_ERROR( "Undefined pixmap in SetPixmapData@Egl action execute." );
        return SCT_FALSE;
    }

    eglData = sctiEglModuleGetData( context->moduleContext, data->dataIndex );
    if( eglData == NULL )
    {
        SCT_LOG_ERROR( "Undefined data in SetPixmapData@Egl action execute." );
        return SCT_FALSE;
    }

    if( siWindowSetPixmapData( sctEglModuleGetWindowContext( context->moduleContext ),
                               pixmap,
                               eglData->data,
                               eglData->length ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Setting pixmap data failed in SetPixmapData@Egl action execute." );
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglSetPixmapDataActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglSetPixmapDataActionContext( ( SCTEglSetPixmapDataActionContext* )( action->context ) );
    action->context = NULL;
}

