/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_egllocksurfaceaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_eglmodule.h"

#include <stdio.h>
#include <string.h>

#if defined( _WIN32 )
# pragma warning ( disable : 4127 )
#endif  /* defined( _WIN32 ) */

/*!
 *
 *
 */
void* sctiCreateEglLockSurfaceActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglLockSurfaceActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglLockSurfaceActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglLockSurfaceActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LockSurface@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglLockSurfaceActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglLockSurfaceActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglLockSurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in LockSurface@Egl context creation." );
        sctiDestroyEglLockSurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in LockSurface@Egl context creation." );
        sctiDestroyEglLockSurfaceActionContext( context );        
        return NULL;
    }
    
    context->lockFunc       = NULL;
    context->unlockFunc     = NULL;    
    context->funcsValidated = SCT_FALSE;   
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglLockSurfaceActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglLockSurfaceActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglLockSurfaceActionContext* context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglLockSurfaceActionContext* )( action->context );

    context->lockFunc   = ( SCTEglLockSurfaceKHRProc )( siCommonGetProcAddress( NULL, "eglLockSurfaceKHR" ) );
    context->unlockFunc = ( _SCTEglUnlockSurfaceKHRProc )( siCommonGetProcAddress( NULL, "eglUnlockSurfaceKHR" ) );        
    if( context->lockFunc == NULL )
    {
        SCT_LOG_ERROR( "Lock surface function not found in LockSurface@Egl action init." );
        return SCT_FALSE;
    }
    if( context->unlockFunc == NULL )
    {
        SCT_LOG_ERROR( "Unlock surface function not found in LockSurface@Egl action init." );
        return SCT_FALSE;
    }
    context->funcsValidated = SCT_FALSE;   
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglLockSurfaceActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglLockSurfaceActionContext* context;
    EglLockSurfaceActionData*       data;    
    EGLDisplay                      display;
    EGLSurface                      surface;    
    const char*                     extString;
    int                             i;
    EGLint                          attribs[]       = { EGL_NONE,     EGL_DONT_CARE,
                                                        EGL_NONE,     EGL_DONT_CARE,
                                                        EGL_NONE };

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( frameNumber );
    
    context = ( SCTEglLockSurfaceActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in LockSurface@Egl action execute." );
        return SCT_FALSE;
    }

    if( context->funcsValidated == SCT_FALSE )
    {
        extString = ( const char* )( eglQueryString( display, EGL_EXTENSIONS ) );
        if( extString == NULL ||
            ( strstr( extString, "EGL_KHR_lock_surface" ) == NULL &&
              strstr( extString, "EGL_KHR_lock_surface2" ) == NULL ) )
        {
            SCT_LOG_ERROR( "EGL_KHR_lock_surface/EGL_KHR_lock_surface2 extension not supported in LockSurface@Egl action execute." );
            return SCT_FALSE;
        }
        context->funcsValidated = SCT_TRUE;
    }

    surface = sctiEglModuleGetSurface( context->moduleContext, data->surfaceIndex );
    if( surface == EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Invalid surface index in LockSurface@Egl action execute." );
        return SCT_FALSE;
    }

    i = 0;
    if( data->preservePixels == SCT_TRUE )
    {
        attribs[ i++ ] = EGL_MAP_PRESERVE_PIXELS_KHR;
        attribs[ i++ ] = EGL_TRUE;
    }
    if( data->usageHint != 0 )
    {
        attribs[ i++ ] = EGL_LOCK_USAGE_HINT_KHR;
        attribs[ i++ ] = data->usageHint;
    }
    attribs[ i ] = EGL_NONE;

    SCT_ASSERT( context->lockFunc != NULL );
    
    if( context->lockFunc( display, surface, attribs ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Lock surface call failed in LockSurface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglLockSurfaceActionTerminate( SCTAction* action )
{
    SCTEglLockSurfaceActionContext* context;
    EGLDisplay                      display;   
    EGLSurface                      surface;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    
    context = ( SCTEglLockSurfaceActionContext* )( action->context );

    /* Apparently we need to try to unlock the surface just in case there was an
     * error in the benchmark loop after the surface was locked. */

    display = sctiEglModuleGetDisplay( context->moduleContext, context->data.displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        return;
    }
    
    surface = sctiEglModuleGetSurface( context->moduleContext, context->data.surfaceIndex );
    if( surface == EGL_NO_SURFACE )
    {
        return;
    }

    if( context->funcsValidated == SCT_TRUE && context->unlockFunc != NULL )
    {
        context->unlockFunc( display, surface );
    }

    context->lockFunc       = NULL;
    context->unlockFunc     = NULL;    
    context->funcsValidated = SCT_FALSE;   
}

/*!
 *
 *
 */
void sctiEglLockSurfaceActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglLockSurfaceActionContext( ( SCTEglLockSurfaceActionContext* )( action->context ) );
    action->context = NULL;
}

