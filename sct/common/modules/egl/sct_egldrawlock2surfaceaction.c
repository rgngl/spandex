/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_egldrawlock2surfaceaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_eglmodule.h"

#include <stdio.h>
#include <string.h>

#if !defined( EGL_BITMAP_POINTER_KHR )
# define EGL_BITMAP_POINTER_KHR             0x30C6
#endif  /* !defined( EGL_BITMAP_POINTER_KHR ) */

#if !defined( EGL_BITMAP_PITCH_KHR )
# define EGL_BITMAP_PITCH_KHR               0x30C7
#endif  /* !defined( EGL_BITMAP_PITCH_KHR ) */

#if !defined( EGL_BITMAP_ORIGIN_KHR )
# define EGL_BITMAP_ORIGIN_KHR              0x30C8
#endif  /* !defined( EGL_BITMAP_ORIGIN_KHR ) */

#if !defined( EGL_LOWER_LEFT_KHR )
# define EGL_LOWER_LEFT_KHR                 0x30CE
#endif  /* !defined( EGL_LOWER_LEFT_KHR ) */

#if !defined( EGL_UPPER_LEFT_KHR )
# define EGL_UPPER_LEFT_KHR                 0x30CF
#endif  /* !defined( EGL_UPPER_LEFT_KHR ) */

#if !defined( EGL_BITMAP_PIXEL_RED_OFFSET_KHR )
# define EGL_BITMAP_PIXEL_RED_OFFSET_KHR    0x30C9
#endif  /* !defined( EGL_BITMAP_PIXEL_RED_OFFSET_KHR ) */

#if !defined( EGL_BITMAP_PIXEL_GREEN_OFFSET_KHR )
# define EGL_BITMAP_PIXEL_GREEN_OFFSET_KHR  0x30CA
#endif  /* !defined( EGL_BITMAP_PIXEL_GREEN_OFFSET_KHR ) */

#if !defined( EGL_BITMAP_PIXEL_BLUE_OFFSET_KHR )
# define EGL_BITMAP_PIXEL_BLUE_OFFSET_KHR  0x30CB
#endif  /* !defined( EGL_BITMAP_PIXEL_BLUE_OFFSET_KHR ) */

#if !defined( EGL_BITMAP_PIXEL_ALPHA_OFFSET_KHR )
# define EGL_BITMAP_PIXEL_ALPHA_OFFSET_KHR  0x30CC
#endif  /* !defined( EGL_BITMAP_PIXEL_ALPHA_OFFSET_KHR ) */

#if !defined( EGL_BITMAP_PIXEL_SIZE_KHR )
# define EGL_BITMAP_PIXEL_SIZE_KHR          0x3110
#endif  /* !defined( EGL_BITMAP_PIXEL_SIZE_KHR ) */

/*!
 *
 *
 */
void* sctiCreateEglDrawLock2SurfaceActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglDrawLock2SurfaceActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglDrawLock2SurfaceActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglDrawLock2SurfaceActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DrawLock2Surface@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglDrawLock2SurfaceActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglDrawLock2SurfaceActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglDrawLock2SurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in DrawLock2Surface@Egl context creation." );
        sctiDestroyEglDrawLock2SurfaceActionContext( context );                
        return NULL;
    }
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in DrawLock2Surface@Egl context creation." );
        sctiDestroyEglDrawLock2SurfaceActionContext( context );        
        return NULL;
    }

    if( sctiEglModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid data index in DrawLock2Surface@Egl context creation." );
        sctiDestroyEglDrawLock2SurfaceActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglDrawLock2SurfaceActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglDrawLock2SurfaceActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglDrawLock2SurfaceActionContext*    context;
    EglDrawLock2SurfaceActionData*          data;    
    EGLDisplay                              display;
    EGLSurface                              surface;
    EGLint                                  pixelSize;
	unsigned char*                          pixels;
	unsigned char*                          dataPixels;
    EGLint                                  pitch;
    EGLint                                  origin;    
    EGLint                                  surfaceWidth;
    EGLint                                  surfaceHeight;
    EGLint                                  redOffset;
    EGLint                                  greenOffset;
    EGLint                                  blueOffset;
    EGLint                                  alphaOffset;
    int                                     y;
    int                                     ex;
    char                                    buf[ 256 ];
    SCTEglData*                             eglData;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTEglDrawLock2SurfaceActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in DrawLock2Surface@Egl action execute." );
        return SCT_FALSE;
    }

    surface = sctiEglModuleGetSurface( context->moduleContext, data->surfaceIndex );
    if( surface == EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Invalid surface index in DrawLock2Surface@Egl action execute." );
        return SCT_FALSE;
    }

    eglData = sctiEglModuleGetData( context->moduleContext, data->dataIndex );
    if( eglData == NULL )
    {
        SCT_LOG_ERROR( "Undefined data in DrawLock2Surface@Egl action execute." );
        return SCT_FALSE;
    }    
    
    /* Get surface attributes. */
    if( eglQuerySurface( display, surface, EGL_WIDTH, &surfaceWidth ) == EGL_FALSE )
    {
        sprintf( buf, "Query surface width failed in DrawLock2Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

    if( eglQuerySurface( display, surface, EGL_HEIGHT, &surfaceHeight ) == EGL_FALSE )
    {
        sprintf( buf, "Query surface height failed in DrawLock2Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }
   
    /* Get locked surface attributes. */
    if( eglQuerySurface( display, surface, EGL_BITMAP_PIXEL_SIZE_KHR, ( EGLint* )( &pixelSize )) == EGL_FALSE )
    {
        sprintf( buf, "Query surface pixel size failed in DrawLock2Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

    if( pixelSize != 16 && pixelSize != 32 )
    {
        sprintf( buf, "Unexpected surface pixel size %d in DrawLock2Surface@Egl action execute.", pixelSize );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

    if( ( surfaceWidth * surfaceHeight * pixelSize / 8 ) != eglData->length )
    {
        SCT_LOG_ERROR( "Invalid EGL data in DrawLock2Surface@Egl action execute." );
        return SCT_FALSE;
    }
    
    if( eglQuerySurface( display, surface, EGL_BITMAP_POINTER_KHR, ( EGLint* )( &pixels )) == EGL_FALSE )
    {
        sprintf( buf, "Query surface bitmap pointer failed in DrawLock2Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }
    
	if( eglQuerySurface( display, surface, EGL_BITMAP_PITCH_KHR, &pitch ) == EGL_FALSE )
    {
        sprintf( buf, "Query surface bitmap pitch failed in DrawLock2Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

	if( eglQuerySurface( display, surface, EGL_BITMAP_ORIGIN_KHR, &origin ) == EGL_FALSE )
    {
        sprintf( buf, "Query surface bitmap origin failed in DrawLock1Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }
    
	if( eglQuerySurface( display, surface, EGL_BITMAP_PIXEL_RED_OFFSET_KHR, &redOffset ) == EGL_FALSE )
    {
        sprintf( buf, "Query bitmap pixel red offset failed in DrawLock2Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

	if( eglQuerySurface( display, surface, EGL_BITMAP_PIXEL_GREEN_OFFSET_KHR, &greenOffset ) == EGL_FALSE )
    {
        sprintf( buf, "Query bitmap pixel green offset failed in DrawLock2Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

	if( eglQuerySurface( display, surface, EGL_BITMAP_PIXEL_BLUE_OFFSET_KHR, &blueOffset ) == EGL_FALSE )
    {
        sprintf( buf, "Query bitmap pixel blue offset failed in DrawLock2Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

	if( eglQuerySurface( display, surface, EGL_BITMAP_PIXEL_ALPHA_OFFSET_KHR, &alphaOffset ) == EGL_FALSE )
    {
        sprintf( buf, "Query bitmap pixel alpha offset failed in DrawLock2Surface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

    dataPixels = ( unsigned char* )( eglData->data );    
    
    /* Move the pixels pointer to upper-left. */
    if( ( origin == EGL_LOWER_LEFT_KHR ) && ( surfaceHeight > 0 ) )
    {
        pixels     = &pixels[ ( surfaceHeight - 1 ) * pitch ];
        dataPixels = &dataPixels[ ( surfaceHeight - 1 ) * pitch ];        
        pitch      = -pitch;
    }

    /* Finally, draw data. */
    if( pixelSize == 16 )
    {
        ex = surfaceWidth * 2;        
        for( y = 0; y < surfaceHeight; ++y )
        {
            memcpy( &pixels[ y * pitch ], &dataPixels[ y * pitch ], ex );
        }
    }
    else
    {
        ex = surfaceWidth * 4;        

        for( y = 0; y < surfaceHeight; ++y )
        {
            memcpy( &pixels[ y * pitch ], &dataPixels[ y * pitch ], ex );
        }
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglDrawLock2SurfaceActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglDrawLock2SurfaceActionContext( ( SCTEglDrawLock2SurfaceActionContext* )( action->context ) );
    action->context = NULL;
}
