/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatecontextextaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreateContextExtActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreateContextExtActionContext*    context;
    int                                     len;
    int                                     i;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreateContextExtActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreateContextExtActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateContextExt@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreateContextExtActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreateContextExtActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreateContextExtActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CreateContextExt@Egl context creation." );
        sctiDestroyEglCreateContextExtActionContext( context );
        return NULL;
    }   
    
    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in CreateContextExt@Egl context creation." );
        sctiDestroyEglCreateContextExtActionContext( context );
        return NULL;
    }

    if( context->data.contextIndex != -1 &&
        sctiEglModuleIsValidContextIndex( context->moduleContext, context->data.contextIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid context index in CreateContextExt@Egl context creation." );
        sctiDestroyEglCreateContextExtActionContext( context );
        return NULL;
    }

    len = context->data.attributes->length;

    /* Reserve space for an EGL_NONE at the end. */
    context->eglAttributes = ( EGLint* )( siCommonMemoryAlloc( NULL, ( len + 1 ) * sizeof( EGLint ) ) );
    if( context->eglAttributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateContextExt@Egl context creation." );
        sctiDestroyEglCreateContextExtActionContext( context );        
        return NULL;
    }

    for( i = 0; i < len; ++i )
    {
        context->eglAttributes[ i ] = ( ( EGLint* )( context->data.attributes->data ) )[ i ];
    }

    context->eglAttributes[ len ] = EGL_NONE;
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreateContextExtActionContext( void* context )
{
    SCTEglCreateContextExtActionContext*    c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTEglCreateContextExtActionContext* )( context );

    if( c->eglAttributes != NULL )
    {
        siCommonMemoryFree( NULL, c->eglAttributes );
        c->eglAttributes = NULL;
    }
    
    if( c->data.attributes != NULL )
    {
        sctDestroyIntVector( c->data.attributes );
        c->data.attributes = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateContextExtActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateContextExtActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreateContextExtActionContext*    context;
    EglCreateContextExtActionData*          data;
    EGLDisplay                              display;
    int                                     configIndex;
    EGLConfig                               config;
    int                                     contextIndex;
    int                                     shareContextIndex;
    EGLContext                              shareContext       = EGL_NO_CONTEXT;
    EGLContext                              eglContext;
    EGLint*                                 attribsPointer;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreateContextExtActionContext* )( action->context );
    data    = &( context->data );

    configIndex       = data->configIndex;
    contextIndex      = data->contextIndex;
    shareContextIndex = data->shareContextIndex;

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CreateContextExt@Egl action execute." );
        return SCT_FALSE;
    }

    config = sctiEglModuleGetConfig( context->moduleContext, configIndex );
    
    if( sctiEglModuleGetContext( context->moduleContext, contextIndex ) != EGL_NO_CONTEXT )
    {
        SCT_LOG_ERROR( "Context index already in use in CreateContextExt@Egl action execute." );
        return SCT_FALSE;
    }

    if( shareContextIndex >= 0 )
    {
        shareContext = sctiEglModuleGetContext( context->moduleContext, shareContextIndex );
        if( shareContext == EGL_NO_CONTEXT )
        {
            SCT_LOG_ERROR( "Invalid share context index in CreateContextExt@Egl action execute." );
            return SCT_FALSE;
        }
    }

    /* Some egl implementations might not support attributes at all so use NULL
     * pointer as attributes with default values.  */
    if( data->attributes->length == 0 )
    {
        attribsPointer = NULL;
    }
    else
    {
        SCT_ASSERT( context->eglAttributes != NULL );
        attribsPointer = context->eglAttributes;
    }
    
    eglContext = eglCreateContext( display, config, shareContext, attribsPointer );

    if( eglContext == EGL_NO_CONTEXT )
    {
        char    buf[ 256 ];
        sprintf( buf, "Creating context failed in CreateContextExt@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    sctiEglModuleSetContext( context->moduleContext, contextIndex, eglContext );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreateContextExtActionTerminate( SCTAction* action )
{
    SCTEglCreateContextExtActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreateContextExtActionContext* )( action->context );

    sctiEglModuleDeleteContext( context->moduleContext,
                                context->data.displayIndex,
                                context->data.contextIndex );    
}

/*!
 *
 *
 */
void sctiEglCreateContextExtActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreateContextExtActionContext( ( SCTEglCreateContextExtActionContext* )( action->context ) );
    action->context = NULL;
}

