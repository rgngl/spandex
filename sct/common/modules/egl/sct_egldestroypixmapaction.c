/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_egldestroypixmapaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglDestroyPixmapActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglDestroyPixmapActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglDestroyPixmapActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglDestroyPixmapActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyPixmap@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglDestroyPixmapActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglDestroyPixmapActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglDestroyPixmapActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidPixmapIndex( context->moduleContext, context->data.pixmapIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in DestroyPixmap@Egl context creation." );
        sctiDestroyEglDestroyPixmapActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglDestroyPixmapActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglDestroyPixmapActionExecute( SCTAction* action, int framenumber )
{
    SCTEglDestroyPixmapActionContext*   context;
    EglDestroyPixmapActionData*         data;
    int                                 index;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglDestroyPixmapActionContext* )( action->context );
    data    = &( context->data );

    index = data->pixmapIndex;
    if( sctiEglModuleGetPixmap( context->moduleContext, index ) == NULL )
    {
        SCT_LOG_ERROR( "Undefined pixmap in DestroyPixmap@Egl action execute." );
        return SCT_FALSE;
    }

    sctiEglModuleDeletePixmap( context->moduleContext, index );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglDestroyPixmapActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglDestroyPixmapActionContext( ( SCTEglDestroyPixmapActionContext* )( action->context ) );
    action->context = NULL;
}
