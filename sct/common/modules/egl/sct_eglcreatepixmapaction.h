/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_EGLCREATEPIXMAPACTION_H__ )
#define __SCT_EGLCREATEPIXMAPACTION_H__

#include "sct_types.h"
#include "sct_eglmodule.h"
#include "sct_eglmodule_parser.h"

/*!
 *
 */
typedef struct
{
    SCTEglModuleContext*                moduleContext;
    EglCreatePixmapActionData           data;
} SCTEglCreatePixmapActionContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*								sctiCreateEglCreatePixmapActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroyEglCreatePixmapActionContext( void* context );

    SCTBoolean                          sctiEglCreatePixmapActionInit( SCTAction *action, SCTBenchmark *benchmark );
    SCTBoolean                          sctiEglCreatePixmapActionExecute( SCTAction *action, int framenumber );
    void                                sctiEglCreatePixmapActionTerminate( SCTAction* action );
    void                                sctiEglCreatePixmapActionDestroy( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_EGLCREATEPIXMAPACTION_H__ ) */
