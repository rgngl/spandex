/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatewindowaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_siwindow.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglCreateWindowActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreateWindowActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreateWindowActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreateWindowActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateWindow@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreateWindowActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreateWindowActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreateWindowActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidWindowIndex( context->moduleContext, context->data.windowIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid window index in CreateWindow@Egl context creation." );
        sctiDestroyEglCreateWindowActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreateWindowActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateWindowActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreateWindowActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreateWindowActionContext*    context;
    EglCreateWindowActionData*          data;
    int                                 index;
    void*                               window;
    int                                 width               = -1;
    int                                 height              = -1;
    void*                               siWindowContext;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreateWindowActionContext* )( action->context );
    data    = &( context->data );

    index = data->windowIndex;
    if( sctiEglModuleGetWindow( context->moduleContext, index ) != NULL )
    {
        SCT_LOG_ERROR( "Window index already in use in CreateWindow@Egl action execute." );
        return SCT_FALSE;
    }

    siWindowContext = sctEglModuleGetWindowContext( context->moduleContext );

    if( siWindowIsScreenSupported( siWindowContext, data->screen ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Screen not supported in CreateWindow@Egl action execute." );
        return SCT_FALSE;
    }
    
    if( data->width < 0 || data->height < 0 )
    {
        siWindowGetScreenSize( siWindowContext, data->screen, &width, &height );        
    }
    
    if( data->width >= 0 )
    {
        width = data->width;
    }

    if( data->height >= 0 )
    {
        height = data->height;
    }  

    window = siWindowCreateWindow( siWindowContext,
                                   data->screen,
                                   data->x, 
                                   data->y, 
                                   width, 
                                   height, 
                                   data->format,
                                   NULL );
       
#if !defined( SCT_EGL_MODULE_ALLOW_NULL_WINDOW_HANDLE )
    if( window == NULL )
    {
        SCT_LOG_ERROR( "Window creation failed in CreateWindow@Egl action execute." );
        return SCT_FALSE;
    }
#endif  /* !defined( SCT_EGL_MODULE_ALLOW_NULL_WINDOW_HANDLE ) */
    
    sctiEglModuleSetWindow( context->moduleContext, index, window );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreateWindowActionTerminate( SCTAction* action )
{
    SCTEglCreateWindowActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTEglCreateWindowActionContext* )( action->context );

    sctiEglModuleDeleteWindow( context->moduleContext, context->data.windowIndex );
}

/*!
 *
 *
 */
void sctiEglCreateWindowActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreateWindowActionContext( ( SCTEglCreateWindowActionContext* )( action->context ) );
    action->context = NULL;
}

