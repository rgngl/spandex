/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglmakecurrentaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglMakeCurrentActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglMakeCurrentActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglMakeCurrentActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglMakeCurrentActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in MakeCurrent@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglMakeCurrentActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglMakeCurrentActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglMakeCurrentActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in MakeCurrent@Egl context creation." );
        sctiDestroyEglMakeCurrentActionContext( context );
        return NULL;
    }   
    
    if( context->data.drawSurfaceIndex != -1 &&
        sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.drawSurfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid draw surface index in MakeCurrent@Egl context creation." );
        sctiDestroyEglMakeCurrentActionContext( context );
        return NULL;
    }

    if( context->data.readSurfaceIndex != -1 &&
        sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.readSurfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid read surface index in MakeCurrent@Egl context creation." );
        sctiDestroyEglMakeCurrentActionContext( context );
        return NULL;
    }

    if( context->data.contextIndex != -1 &&
        sctiEglModuleIsValidContextIndex( context->moduleContext, context->data.contextIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid context index in MakeCurrent@Egl context creation." );
        sctiDestroyEglMakeCurrentActionContext( context );
        return NULL;
    }

    context->api = EGL_NONE;
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglMakeCurrentActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglMakeCurrentActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglMakeCurrentActionExecute( SCTAction* action, int framenumber )
{
    SCTEglMakeCurrentActionContext* context;
    EglMakeCurrentActionData*       data;
    EGLDisplay                      display;
    EGLSurface                      drawSurface  = EGL_NO_SURFACE;
    EGLSurface                      readSurface  = EGL_NO_SURFACE;
    EGLContext                      eglContext   = EGL_NO_CONTEXT;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglMakeCurrentActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in MakeCurrent@Egl action execute." );
        return SCT_FALSE;
    }

    if( data->drawSurfaceIndex >= 0 )
    {
        drawSurface = sctiEglModuleGetSurface( context->moduleContext, data->drawSurfaceIndex );
        if( drawSurface == EGL_NO_SURFACE )
        {
            SCT_LOG_ERROR( "Undefined draw surface in MakeCurrent@Egl action execute." );
            return SCT_FALSE;
        }
    }

    if( data->readSurfaceIndex >= 0 )
    {
        readSurface = sctiEglModuleGetSurface( context->moduleContext, data->readSurfaceIndex );
        if( readSurface == EGL_NO_SURFACE )
        {
            SCT_LOG_ERROR( "Undefined read surface in MakeCurrent@Egl action execute." );
            return SCT_FALSE;
        }
    }

    if( data->contextIndex >= 0 )
    {
        eglContext = sctiEglModuleGetContext( context->moduleContext, data->contextIndex );
        if( eglContext == EGL_NO_CONTEXT )
        {
            SCT_LOG_ERROR( "Undefined context in MakeCurrent@Egl action execute." );
            return SCT_FALSE;
        }
    }

#if defined( EGL_VERSION_1_2 )
    context->api = eglQueryAPI();
#endif  /* defined( EGL_VERSION_1_2 ) */
    
    if( eglMakeCurrent( display, drawSurface, readSurface, eglContext ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Make current failed in MakeCurrent@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglMakeCurrentActionTerminate( SCTAction* action )
{
    SCTEglMakeCurrentActionContext* context;
    EGLDisplay                      display;
    int                             i;
    
#if defined( EGL_VERSION_1_2 )   
    EGLenum                         api         = EGL_NONE;    
#endif  /* defined( EGL_VERSION_1_2 ) */
    
    SCT_ASSERT_ALWAYS( action != NULL );
    if( action->context == NULL )
    {
        return;
    }

    context = ( SCTEglMakeCurrentActionContext* )( action->context );

    display = sctiEglModuleGetDisplay( context->moduleContext, context->data.displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        return;
    }
   
    /* Following code removes the current context for the API it was set in
     * execute. */
    
#if !defined( SCT_SKIP_FINAL_EGL_MAKE_CURRENT )
# if defined( EGL_VERSION_1_2 )
    if( context->api != EGL_NONE )
    {
        api = eglQueryAPI();
        if( api != EGL_NONE && api != context->api )
        {
            eglBindAPI( context->api );
        }
        else
        {
            api = EGL_NONE;
        }
    }
# endif  /* defined( EGL_VERSION_1_2 ) */    
    
    eglMakeCurrent( display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT );

    /* Above eglMakeCurrent call seems to generate EGL error sometimes
     * so we clear any pending errors. Beware infinite loop. */
    for( i = 0; i < 16; ++i )
    {
        if( eglGetError() == EGL_SUCCESS )
        {
            break;
        }
    }
    
# if defined( EGL_VERSION_1_2 )
    if( api != EGL_NONE )
    {
        eglBindAPI( api );
    }
# endif /* defined( EGL_VERSION_1_2 ) */
    
#else   /* !defined( SCT_SKIP_FINAL_EGL_MAKE_CURRENT ) */
    SCT_USE_VARIABLE( i );
#endif  /* !defined( SCT_SKIP_FINAL_EGL_MAKE_CURRENT ) */    
}

/*!
 *
 *
 */
void sctiEglMakeCurrentActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglMakeCurrentActionContext( ( SCTEglMakeCurrentActionContext* )( action->context ) );
    action->context = NULL;
}

