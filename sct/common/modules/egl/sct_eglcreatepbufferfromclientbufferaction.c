/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatepbufferfromclientbufferaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_OPENVG_MODULE )
# include "sct_openvgmodule.h"
#endif  /* defined( INCLUDE_OPENVG_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateEglCreatePbufferFromClientBufferActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreatePbufferFromClientBufferActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreatePbufferFromClientBufferActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreatePbufferFromClientBufferActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePbufferFromClientBuffer@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreatePbufferFromClientBufferActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreatePbufferFromClientBufferActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreatePbufferFromClientBufferActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CreatePbufferFromClientBuffer@Egl context creation." );
        sctiDestroyEglCreatePbufferFromClientBufferActionContext( context );        
        return NULL;
    }   
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in CreatePbufferFromClientBuffer@Egl context creation." );
        sctiDestroyEglCreatePbufferFromClientBufferActionContext( context );       
        return NULL;
    }

    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid config index in CreatePbufferFromClientBuffer@Egl context creation." );
        sctiDestroyEglCreatePbufferFromClientBufferActionContext( context );        
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreatePbufferFromClientBufferActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePbufferFromClientBufferActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglCreatePbufferFromClientBufferActionContext*   context;
    EglCreatePbufferFromClientBufferActionData*         data;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglCreatePbufferFromClientBufferActionContext* )( action->context );
    data    = &( context->data );

#if defined( EGL_VERSION_1_2 ) && !defined( INCLUDE_OPENVG_MODULE )
    if( data->bufType == EGL_OPENVG_IMAGE )
    {
        SCT_LOG_ERROR( "CreatePbufferFromClientBuffer@Egl action init OpenVG module not available." );
        return SCT_FALSE;
    }
#else   /* defined( EGL_VERSION_1_2 ) && !defined( INCLUDE_OPENVG_MODULE ) */
    SCT_USE_VARIABLE( data );
#endif  /* defined( EGL_VERSION_1_2 ) && !defined( INCLUDE_OPENVG_MODULE ) */
   
#if !defined( EGL_VERSION_1_2 )
    SCT_LOG_ERROR( "CreatePbufferFromClientBuffer@Egl action init action not supported in the used EGL version." );
    return SCT_FALSE;
#else  /* !defined( EGL_VERSION_1_2 ) */   
    return SCT_TRUE;
#endif  /* !defined( EGL_VERSION_1_2 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePbufferFromClientBufferActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreatePbufferFromClientBufferActionContext*   context;
    EglCreatePbufferFromClientBufferActionData*         data;
    int                                                 surfaceIndex;
    int                                                 configIndex;
    EGLDisplay                                          display;
    EGLConfig                                           config;
    EGLSurface                                          surface;
    EGLint                                              attribs[]       = { EGL_TEXTURE_FORMAT,  EGL_NO_TEXTURE,
                                                                            EGL_TEXTURE_TARGET,  EGL_NO_TEXTURE,
                                                                            EGL_MIPMAP_TEXTURE,  EGL_FALSE,
                                                                            EGL_NONE };
#if defined( EGL_VERSION_1_2 )
    EGLClientBuffer                                     buffer          = 0;
#endif  /* defined( EGL_VERSION_1_2 ) */
    int                                                 index           = 0;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreatePbufferFromClientBufferActionContext* )( action->context );
    data    = &( context->data );

    surfaceIndex = data->surfaceIndex;
    configIndex  = data->configIndex;

#if defined( EGL_VERSION_1_2 ) && defined( INCLUDE_OPENVG_MODULE )
    if( data->bufType == EGL_OPENVG_IMAGE )
    {
        buffer = ( EGLClientBuffer )( sctOpenVGModuleGetImageExt( data->bufferIndex ) );
        if( ( VGImage )( buffer ) == VG_INVALID_HANDLE )
        {
            SCT_LOG_ERROR( "Invalid OpenVG image index in CreatePbufferFromClientBuffer@Egl action execute." );
            return SCT_FALSE;
        }
    }
#endif /* defined( EGL_VERSION_1_2 ) && defined( INCLUDE_OPENVG_MODULE ) */

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CreatePbufferFromClientBuffer@Egl action execute." );
        return SCT_FALSE;
    }

    if( sctiEglModuleGetSurface( context->moduleContext, surfaceIndex ) != EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Surface index already in use in CreatePbufferFromClientBuffer@Egl action execute." );
        return SCT_FALSE;
    }

    config = sctiEglModuleGetConfig( context->moduleContext, configIndex );

    attribs[ index++ ] = EGL_TEXTURE_FORMAT;
    attribs[ index++ ] = data->textureFormat;
    attribs[ index++ ] = EGL_TEXTURE_TARGET;
    attribs[ index++ ] = data->textureTarget;
    attribs[ index++ ] = EGL_MIPMAP_TEXTURE;
    attribs[ index++ ] = data->mipmapTexture == SCT_FALSE ? EGL_FALSE : EGL_TRUE;
    attribs[ index ]   = EGL_NONE;

#if defined( EGL_VERSION_1_2 )   
    surface = eglCreatePbufferFromClientBuffer( display, data->bufType, buffer, config, attribs );
    if( surface == EGL_NO_SURFACE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Creating pbuffer from client buffer failed in CreatePbufferFromClientBuffer@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
    
    sctiEglModuleSetSurface( context->moduleContext, surfaceIndex, surface );   
#else   /* defined( EGL_VERSION_1_2 ) */
    SCT_USE_VARIABLE( display );
    SCT_USE_VARIABLE( config );
    SCT_USE_VARIABLE( attribs );
    SCT_USE_VARIABLE( surface );
#endif  /* defined( EGL_VERSION_1_2 ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreatePbufferFromClientBufferActionTerminate( SCTAction* action )
{
    SCTEglCreatePbufferFromClientBufferActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreatePbufferFromClientBufferActionContext* )( action->context );

    sctiEglModuleDeleteSurface( context->moduleContext,
                                context->data.displayIndex,
                                context->data.surfaceIndex );    
}

/*!
 *
 *
 */
void sctiEglCreatePbufferFromClientBufferActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreatePbufferFromClientBufferActionContext( ( SCTEglCreatePbufferFromClientBufferActionContext* )( action->context ) );
    action->context = NULL;
}
