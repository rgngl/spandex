/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglblitpixmapaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglBlitPixmapActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglBlitPixmapActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglBlitPixmapActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglBlitPixmapActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BlitPixmap@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglBlitPixmapActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglBlitPixmapActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglBlitPixmapActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidPixmapIndex( context->moduleContext, context->data.pixmapIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in BlitPixmap@Egl context creation." );
        sctiDestroyEglBlitPixmapActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidWindowIndex( context->moduleContext, context->data.windowIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid window index in BlitPixmap@Egl context creation." );
        sctiDestroyEglBlitPixmapActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglBlitPixmapActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglBlitPixmapActionExecute( SCTAction* action, int framenumber )
{
    SCTEglBlitPixmapActionContext*  context;
    EglBlitPixmapActionData*        data;
    void*                           pixmap;
    void*                           window;
    void*                           siWindowContext;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglBlitPixmapActionContext* )( action->context );
    data    = &( context->data );

    pixmap = sctiEglModuleGetPixmap( context->moduleContext, data->pixmapIndex );
    if( pixmap == NULL )
    {
        SCT_LOG_ERROR( "Invalid pixmap index in BlitPixmap@Egl action execute." );
        return SCT_FALSE;
    }

    window = sctiEglModuleGetWindow( context->moduleContext, data->windowIndex );
    if( window == NULL )
    {
        SCT_LOG_ERROR( "Invalid window index in BlitPixmap@Egl action execute." );
        return SCT_FALSE;
    }
    
    siWindowContext = sctEglModuleGetWindowContext( context->moduleContext );
    
    if( siWindowBlitPixmap( siWindowContext,
                            window,
                            pixmap,
                            data->origin[ 0 ], data->origin[ 1 ] ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Blitting failed in BlitPixmap@Egl action execute." );
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglBlitPixmapActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglBlitPixmapActionContext( ( SCTEglBlitPixmapActionContext* )( action->context ) );
    action->context = NULL;
}

