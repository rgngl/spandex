/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglswapintervalaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_eglmodule.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglSwapIntervalActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglSwapIntervalActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglSwapIntervalActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglSwapIntervalActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SwapInterval@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglSwapIntervalActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglSwapIntervalActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglSwapIntervalActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in SwapInterval@Egl context creation." );
        sctiDestroyEglSwapIntervalActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglSwapIntervalActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglSwapIntervalActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark );

#if !defined( EGL_VERSION_1_1 )
    SCT_LOG_ERROR( "SwapInterval@Egl action init action not supported in the used EGL version." );
    return SCT_FALSE;
#else   /* !defined( EGL_VERSION_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( EGL_VERSION_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiEglSwapIntervalActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglSwapIntervalActionContext*    context;
    EglSwapIntervalActionData*          data;
    EGLDisplay                          display;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTEglSwapIntervalActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in SwapInterval@Egl action execute." );
        return SCT_FALSE;
    }

#if defined( EGL_VERSION_1_1 )
    if( eglSwapInterval( display, data->interval ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Setting swap interval failed in SwapInterval@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
#else   /* defined( EGL_VERSION_1_1 ) */
    SCT_USE_VARIABLE( data );
#endif  /* defined( EGL_VERSION_1_1 ) */
    
	return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglSwapIntervalActionTerminate( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
}

/*!
 *
 *
 */
void sctiEglSwapIntervalActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglSwapIntervalActionContext( ( SCTEglSwapIntervalActionContext* )( action->context ) );
    action->context = NULL;
}

