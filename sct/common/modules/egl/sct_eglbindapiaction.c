/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglmodule.h"
#include "sct_eglbindapiaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglBindApiActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglBindApiActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglBindApiActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglBindApiActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BindApi@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglBindApiActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglBindApiActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglBindApiActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglBindApiActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglBindApiActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglBindApiActionContext* context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglBindApiActionContext* )( action->context );
    
#if !defined( EGL_VERSION_1_2 )
    if( context->data.api != EGL_OPENGL_ES_API )
    {
        SCT_LOG_ERROR( "BindApi@Egl only OpenGLES API supported in the used EGL version." );
        return SCT_FALSE;
    }
#else   /* !defined( EGL_VERSION_1_2 ) */
    SCT_USE_VARIABLE( context );
#endif  /* !defined( EGL_VERSION_1_2 ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglBindApiActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglBindApiActionContext* context;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTEglBindApiActionContext* )( action->context );
    
#if defined( EGL_VERSION_1_2 )
    if( eglBindAPI( context->data.api ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Binding API failed in BindApi@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
#else   /* defined( EGL_VERSION_1_2 ) */
    SCT_USE_VARIABLE( context );
#endif  /* defined( EGL_VERSION_1_2 ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglBindApiActionTerminate( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
}

/*!
 *
 *
 */
void sctiEglBindApiActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglBindApiActionContext( ( SCTEglBindApiActionContext* )( action->context ) );
    action->context = NULL;
}

