/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglcreatepbufferfromclientbufferextaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_OPENVG_MODULE )
# include "sct_openvgmodule.h"
#endif  /* defined( INCLUDE_OPENVG_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateEglCreatePbufferFromClientBufferExtActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglCreatePbufferFromClientBufferExtActionContext*    context;
    int                                                     len;
    int                                                     i;
    
    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglCreatePbufferFromClientBufferExtActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglCreatePbufferFromClientBufferExtActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePbufferFromClientBufferExt@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglCreatePbufferFromClientBufferExtActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglCreatePbufferFromClientBufferExtActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglCreatePbufferFromClientBufferExtActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in CreatePbufferFromClientBufferExt@Egl context creation." );
        sctiDestroyEglCreatePbufferFromClientBufferExtActionContext( context );        
        return NULL;
    }   
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        sctiDestroyEglCreatePbufferFromClientBufferExtActionContext( context );
        SCT_LOG_ERROR( "Invalid surface index in CreatePbufferFromClientBufferExt@Egl context creation." );
        return NULL;
    }

    if( sctiEglModuleIsValidConfigIndex( context->moduleContext, context->data.configIndex ) == SCT_FALSE )
    {
        sctiDestroyEglCreatePbufferFromClientBufferExtActionContext( context );
        SCT_LOG_ERROR( "Invalid config index in CreatePbufferFromClientBufferExt@Egl context creation." );
        return NULL;
    }

    len = context->data.attributes->length;

    /* Reserve space for an EGL_NONE at the end. */
    context->eglAttributes = ( EGLint* )( siCommonMemoryAlloc( NULL, ( len + 1 ) * sizeof( EGLint ) ) );
    if( context->eglAttributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePbufferFromClientBufferExt@Egl context creation." );
        sctiDestroyEglCreatePbufferFromClientBufferExtActionContext( context );
        return NULL;
    }

    for( i = 0; i < len; ++i )
    {
        context->eglAttributes[ i ] = ( ( EGLint* )( context->data.attributes->data ) )[ i ];
    }

    context->eglAttributes[ len ] = EGL_NONE;
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglCreatePbufferFromClientBufferExtActionContext( void* context )
{
    SCTEglCreatePbufferFromClientBufferExtActionContext*    c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTEglCreatePbufferFromClientBufferExtActionContext* )( context );

    if( c->eglAttributes != NULL )
    {
        siCommonMemoryFree( NULL, c->eglAttributes );
        c->eglAttributes = NULL;
    }
    
    if( c->data.attributes != NULL )
    {
        sctDestroyIntVector( c->data.attributes );
        c->data.attributes = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePbufferFromClientBufferExtActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglCreatePbufferFromClientBufferExtActionContext*    context;
    EglCreatePbufferFromClientBufferExtActionData*          data;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglCreatePbufferFromClientBufferExtActionContext* )( action->context );
    data    = &( context->data );

#if defined( EGL_VERSION_1_2 ) && !defined( INCLUDE_OPENVG_MODULE )
    if( data->bufType == EGL_OPENVG_IMAGE )
    {
        SCT_LOG_ERROR( "CreatePbufferFromClientBufferExt@Egl action init OpenVG module not available." );
        return SCT_FALSE;
    }
#else   /* defined( EGL_VERSION_1_2 ) && !defined( INCLUDE_OPENVG_MODULE ) */
    SCT_USE_VARIABLE( data );
#endif  /* defined( EGL_VERSION_1_2 ) && !defined( INCLUDE_OPENVG_MODULE ) */
   
#if !defined( EGL_VERSION_1_2 )
    SCT_LOG_ERROR( "CreatePbufferFromClientBufferExt@Egl action init action not supported in the used EGL version." );
    return SCT_FALSE;
#else  /* !defined( EGL_VERSION_1_2 ) */   
    return SCT_TRUE;
#endif  /* !defined( EGL_VERSION_1_2 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiEglCreatePbufferFromClientBufferExtActionExecute( SCTAction* action, int framenumber )
{
    SCTEglCreatePbufferFromClientBufferExtActionContext*    context;
    EglCreatePbufferFromClientBufferExtActionData*          data;
    int                                                     surfaceIndex;
    int                                                     configIndex;
    EGLDisplay                                              display;
    EGLConfig                                               config;
    EGLSurface                                              surface;
    EGLint*                                                 attribsPointer;   
#if defined( EGL_VERSION_1_2 )
    EGLClientBuffer                                         buffer          = 0;
#endif  /* defined( EGL_VERSION_1_2 ) */
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglCreatePbufferFromClientBufferExtActionContext* )( action->context );
    data    = &( context->data );

    surfaceIndex = data->surfaceIndex;
    configIndex  = data->configIndex;

#if defined( EGL_VERSION_1_2 ) && defined( INCLUDE_OPENVG_MODULE )
    if( data->bufType == EGL_OPENVG_IMAGE )
    {
        buffer = ( EGLClientBuffer )( sctOpenVGModuleGetImageExt( data->bufferIndex ) );
        if( ( VGImage )( buffer ) == VG_INVALID_HANDLE )
        {
            SCT_LOG_ERROR( "Invalid OpenVG image index in CreatePbufferFromClientBufferExt@Egl action execute." );
            return SCT_FALSE;
        }
    }
#endif /* defined( EGL_VERSION_1_2 ) && defined( INCLUDE_OPENVG_MODULE ) */

    /* Some egl implementations do not support attributes at all so use NULL
     * pointer as attributes with default values.  */
    if( data->attributes->length == 0 )
    {
        attribsPointer = NULL;
    }
    else
    {
        SCT_ASSERT( context->eglAttributes != NULL );
        attribsPointer = context->eglAttributes;
    }

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in CreatePbufferFromClientBufferExt@Egl action execute." );
        return SCT_FALSE;
    }

    if( sctiEglModuleGetSurface( context->moduleContext, surfaceIndex ) != EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Surface index already in use in CreatePbufferFromClientBufferExt@Egl action execute." );
        return SCT_FALSE;
    }

    config = sctiEglModuleGetConfig( context->moduleContext, configIndex );

#if defined( EGL_VERSION_1_2 )   
    surface = eglCreatePbufferFromClientBuffer( display, data->bufType, buffer, config, attribsPointer );
    if( surface == EGL_NO_SURFACE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Creating pbuffer from client buffer failed in CreatePbufferFromClientBufferExt@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
    
    sctiEglModuleSetSurface( context->moduleContext, surfaceIndex, surface );   
#else   /* defined( EGL_VERSION_1_2 ) */
    SCT_USE_VARIABLE( display );
    SCT_USE_VARIABLE( config );
    SCT_USE_VARIABLE( surface );
    SCT_USE_VARIABLE( attribsPointer );
    SCT_ASSERT_ALWAYS( 0 );
#endif  /* defined( EGL_VERSION_1_2 ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglCreatePbufferFromClientBufferExtActionTerminate( SCTAction* action )
{
    SCTEglCreatePbufferFromClientBufferExtActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTEglCreatePbufferFromClientBufferExtActionContext* )( action->context );

    sctiEglModuleDeleteSurface( context->moduleContext,
                                context->data.displayIndex,
                                context->data.surfaceIndex );    
}

/*!
 *
 *
 */
void sctiEglCreatePbufferFromClientBufferExtActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglCreatePbufferFromClientBufferExtActionContext( ( SCTEglCreatePbufferFromClientBufferExtActionContext* )( action->context ) );
    action->context = NULL;
}
