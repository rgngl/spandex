/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglswapbuffersaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_eglmodule.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglSwapBuffersActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglSwapBuffersActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglSwapBuffersActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglSwapBuffersActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SwapBuffers@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglSwapBuffersActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglSwapBuffersActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglSwapBuffersActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in SwapBuffers@Egl context creation." );
        sctiDestroyEglSwapBuffersActionContext( context );
        return NULL;
    }
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in SwapBuffers@Egl context creation." );
        sctiDestroyEglSwapBuffersActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglSwapBuffersActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglSwapBuffersActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglSwapBuffersActionContext* context;
    EglSwapBuffersActionData*       data;
    EGLDisplay                      display;
    EGLSurface                      surface;
    EGLBoolean                      status;
#if defined( SCT_EGL_MODULE_PRINT_SWAP_DURATION )
    unsigned long                   start;
    unsigned long                   end;
    unsigned long                   duration;
#endif  /* defined( SCT_EGL_MODULE_PRINT_SWAP_DURATION ) */
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTEglSwapBuffersActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in SwapBuffers@Egl action execute." );
        return SCT_FALSE;
    }

    surface = sctiEglModuleGetSurface( context->moduleContext, data->surfaceIndex );
    if( surface == EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Invalid surface index in SwapBuffers@Egl action execute." );
        return SCT_FALSE;
    }

#if defined( SCT_EGL_MODULE_PRINT_SWAP_DURATION )
    start = siCommonGetTimerTick( NULL );
#endif  /* defined( SCT_EGL_MODULE_PRINT_SWAP_DURATION ) */
    
    status = eglSwapBuffers( display, surface );

#if defined( SCT_EGL_MODULE_PRINT_SWAP_DURATION )
    end = siCommonGetTimerTick( NULL );
    duration = ( end - start ) * 1000 / siCommonGetTimerFrequency( NULL );
    siCommonDebugPrintf( NULL, "SPANDEX: swap duration %d ms\n", duration );    
#endif  /* defined( SCT_EGL_MODULE_PRINT_SWAP_DURATION ) */

#if !defined( SCT_EGL_MODULE_SKIP_SWAP_ERROR_CHECKING ) 
    if( status == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Swap buffers failed in SwapBuffers@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

#else   /* !defined( SCT_EGL_MODULE_SKIP_SWAP_ERROR_CHECKING ) */
    SCT_USE_VARIABLE( status );
#endif  /* !defined( SCT_EGL_MODULE_SKIP_SWAP_ERROR_CHECKING ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglSwapBuffersActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglSwapBuffersActionContext( ( SCTEglSwapBuffersActionContext* )( action->context ) );
    action->context = NULL;
}

