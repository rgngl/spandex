/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglsurfaceattribaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglSurfaceAttribActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglSurfaceAttribActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglSurfaceAttribActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglSurfaceAttribActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SurfaceAttrib@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglSurfaceAttribActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglSurfaceAttribActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglSurfaceAttribActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in SurfaceAttrib@Egl context creation." );
        sctiDestroyEglSurfaceAttribActionContext( context );        
        return NULL;
    }   
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in SurfaceAttrib@Egl context creation." );
        sctiDestroyEglSurfaceAttribActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglSurfaceAttribActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglSurfaceAttribActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( EGL_VERSION_1_1 )
    SCT_LOG_ERROR( "SurfaceAttrib@Egl not supported in the used EGL version." );
    return SCT_FALSE;
#else   /* !defined( EGL_VERSION_1_1 ) */
    return SCT_TRUE;
#endif   /* !defined( EGL_VERSION_1_1 ) */    
}
   
/*!
 *
 *
 */
SCTBoolean sctiEglSurfaceAttribActionExecute( SCTAction* action, int framenumber )
{
    SCTEglSurfaceAttribActionContext*   context;
    EglSurfaceAttribActionData*         data;
    EGLDisplay                          display;
    EGLSurface                          surface;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglSurfaceAttribActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in SurfaceAttrib@Egl action execute." );
        return SCT_FALSE;
    }

    surface = sctiEglModuleGetSurface( context->moduleContext, data->surfaceIndex );
    if( surface == EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Invalid surface in SurfaceAttrib@Egl action execute." );
        return SCT_FALSE;
    }

#if defined( EGL_VERSION_1_1 ) && !defined( HG_EGL_VERSION_1_1 )
    if( eglSurfaceAttrib( display, surface, ( EGLint )( data->attribute ), ( EGLint )( data->value ) ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Setting surface attribute failed in SurfaceAttrib@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
#endif  /* defined( EGL_VERSION_1_1 ) && !defined( HG_EGL_VERSION_1_1 ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglSurfaceAttribActionTerminate( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
}

/*!
 *
 *
 */
void sctiEglSurfaceAttribActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglSurfaceAttribActionContext( ( SCTEglSurfaceAttribActionContext* )( action->context ) );
    action->context = NULL;
}

