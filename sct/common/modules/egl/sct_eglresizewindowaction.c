/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglresizewindowaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglResizeWindowActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglResizeWindowActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglResizeWindowActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglResizeWindowActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ResizeWindow@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglResizeWindowActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglResizeWindowActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglResizeWindowActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidWindowIndex( context->moduleContext, context->data.windowIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid window index in ResizeWindow@Egl context creation." );
        sctiDestroyEglResizeWindowActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglResizeWindowActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglResizeWindowActionExecute( SCTAction* action, int framenumber )
{
    SCTEglResizeWindowActionContext*    context;
    EglResizeWindowActionData*          data;
    void*                               window;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglResizeWindowActionContext* )( action->context );
    data    = &( context->data );

    window = sctiEglModuleGetWindow( context->moduleContext, data->windowIndex );
    if( window == NULL )
    {
        SCT_LOG_ERROR( "Undefined window in ResizeWindow@Egl action execute." );
        return SCT_FALSE;
    }

    if( siWindowResize( sctEglModuleGetWindowContext( context->moduleContext ),
                        window,
                        data->x, data->y,
                        data->width, data->height ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Window resize failed in ResizeWindow@Egl action execute." );
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglResizeWindowActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglResizeWindowActionContext( ( SCTEglResizeWindowActionContext* )( action->context ) );
    action->context = NULL;
}

