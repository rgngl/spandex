/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglwaitnativeaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_eglmodule.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglWaitNativeActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglWaitNativeActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglWaitNativeActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglWaitNativeActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in WaitNative@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglWaitNativeActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglWaitNativeActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglWaitNativeActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglWaitNativeActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglWaitNativeActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( EGL_VERSION_1_1 )
    SCT_LOG_ERROR( "WaitNative@Egl action init action not supported in the used EGL version." );
    return SCT_FALSE;
#else   /* !defined( EGL_VERSION_1_1 ) */    
    return SCT_TRUE;
#endif  /* !defined( EGL_VERSION_1_1 ) */   
}

/*!
 *
 *
 */
SCTBoolean sctiEglWaitNativeActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglWaitNativeActionContext*  context;
    EglWaitNativeActionData*        data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTEglWaitNativeActionContext* )( action->context );
    data    = &( context->data );

#if defined( EGL_VERSION_1_1 )
    if( eglWaitNative( data->engine ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Waiting native failed in WaitNative@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
#else   /* defined( EGL_VERSION_1_1 ) */
    SCT_USE_VARIABLE( data );
#endif  /* defined( EGL_VERSION_1_1 ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglWaitNativeActionTerminate( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
}

/*!
 *
 *
 */
void sctiEglWaitNativeActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglWaitNativeActionContext( ( SCTEglWaitNativeActionContext* )( action->context ) );
    action->context = NULL;
}

