/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglscreenorientationaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglScreenOrientationActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglScreenOrientationActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglScreenOrientationActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglScreenOrientationActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ScreenOrientation@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglScreenOrientationActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglScreenOrientationActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglScreenOrientationActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglScreenOrientationActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglScreenOrientationActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglScreenOrientationActionExecute( SCTAction* action, int framenumber )
{
    SCTEglScreenOrientationActionContext*   context;
    EglScreenOrientationActionData*         data;
    SCTBoolean                              status;
    void*                                   siWindowContext;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTEglScreenOrientationActionContext* )( action->context );
    data    = &( context->data );

    siWindowContext = sctEglModuleGetWindowContext( context->moduleContext );
    
    if( siWindowIsScreenSupported( siWindowContext, data->screen ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Screen not supported in ScreenOrientation@Egl action execute." );
        return SCT_FALSE;
    }
    
    status = siWindowSetScreenOrientation( siWindowContext,
                                           data->screen,
                                           data->orientation );

    if( status == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Setting screen orientation failed in ScreenOrientation@Egl action execute." );
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglScreenOrientationActionTerminate( SCTAction* action )
{
    SCTEglScreenOrientationActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTEglScreenOrientationActionContext* )( action->context );

    siWindowSetScreenOrientation( sctEglModuleGetWindowContext( context->moduleContext ),
                                  context->data.screen,
                                  SCT_SCREEN_ORIENTATION_DEFAULT );
}

/*!
 *
 *
 */
void sctiEglScreenOrientationActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyEglScreenOrientationActionContext( ( SCTEglScreenOrientationActionContext* )( action->context ) );
    action->context = NULL;
}

