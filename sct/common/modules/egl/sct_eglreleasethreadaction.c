/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglreleasethreadaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_eglmodule.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateEglReleaseThreadActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglReleaseThreadActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglReleaseThreadActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglReleaseThreadActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ReleaseThread@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglReleaseThreadActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglReleaseThreadActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglReleaseThreadActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglReleaseThreadActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglReleaseThreadActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( EGL_VERSION_1_2 )
    SCT_LOG_ERROR( "ReleaseThread@Egl action init action not supported in the used EGL version." );
    return SCT_FALSE;
#else   /* !defined( EGL_VERSION_1_2 ) */
    return SCT_TRUE;
#endif  /* !defined( EGL_VERSION_1_2 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiEglReleaseThreadActionExecute( SCTAction* action, int frameNumber )
{
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );

#if defined( EGL_VERSION_1_2 )   
    if( eglReleaseThread() != EGL_TRUE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Releasing thread failed in ReleaseThread@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
#endif  /* defined( EGL_VERSION_1_2 ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglReleaseThreadActionTerminate( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
}

/*!
 *
 *
 */
void sctiEglReleaseThreadActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglReleaseThreadActionContext( ( SCTEglReleaseThreadActionContext* )( action->context ) );
    action->context = NULL;
}

