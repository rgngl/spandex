/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_EGLMODULE_H__ )
#define __SCT_EGLMODULE_H__

#include "sct_types.h"
#include "sct_egl.h"

/*!
 *
 *
 */
#define SCT_MAX_EGL_DATA_ELEMENTS   256
#define SCT_MAX_EGL_DISPLAYS        256
#define SCT_MAX_EGL_CONFIGS         256
#define SCT_MAX_EGL_WINDOWS         256
#define SCT_MAX_EGL_PIXMAPS         256
#define SCT_MAX_EGL_SURFACES        256
#define SCT_MAX_EGL_CONTEXTS        256
#define SCT_MAX_EGL_IMAGES          256
#define SCT_MAX_EGL_SYNCS           256

/*
 *
 */
typedef struct
{
    void*                           data;
    int                             length;
} SCTEglData;

/*!
 *
 *
 */
typedef struct
{
    void*                           mutex;
    void*                           siWindowContext;
    SCTEglData*                     dataElements[ SCT_MAX_EGL_DATA_ELEMENTS ];
    EGLDisplay                      eglDisplays[ SCT_MAX_EGL_DISPLAYS ];
    EGLConfig                       eglConfigs[ SCT_MAX_EGL_CONFIGS ];
    void*                           eglWindows[ SCT_MAX_EGL_WINDOWS ];
    void*                           eglPixmaps[ SCT_MAX_EGL_PIXMAPS ];
    EGLSurface                      eglSurfaces[ SCT_MAX_EGL_SURFACES ];
    EGLContext                      eglContexts[ SCT_MAX_EGL_CONTEXTS ];
    EGLImageKHR                     eglImages[ SCT_MAX_EGL_IMAGES ];
    EGLSyncKHR                      eglSyncs[ SCT_MAX_EGL_SYNCS ];
} SCTEglModuleContext;

/*!
 *
 *
 */
typedef void ( *SCTEglGetProcAddressFunc )();

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    SCTModule*                      sctCreateEglModule( void );

    /* External Egl module data access. */
    SCTBoolean                      sctEglModuleSetDataExt( int index, SCTEglData* data );
    EGLImageKHR                     sctEglModuleGetImageExt( int index );
    
    void                            sctEglModuleLock( SCTEglModuleContext* moduleContext );
    void                            sctEglModuleUnlock( SCTEglModuleContext* moduleContext );
    
    SCTEglGetProcAddressFunc        sctEglGetProcAddress( const char* procname );
    void*                           sctEglModuleGetWindowContext( SCTEglModuleContext* moduleContext );

    SCTEglData*                     sctEglCreateData( void* data, int length, SCTBoolean copy );
    void                            sctEglDestroyData( SCTEglData* data );

    /* Data */
    SCTBoolean                      sctiEglModuleIsValidDataIndex( SCTEglModuleContext* moduleContext, int index );
    SCTEglData*                     sctiEglModuleGetData( SCTEglModuleContext* moduleContext, int index );
    void                            sctiEglModuleSetData( SCTEglModuleContext* moduleContext, int index, SCTEglData* data );
    void                            sctiEglModuleDeleteData( SCTEglModuleContext* moduleContext, int index );
    
    /* Windows */
    SCTBoolean                      sctiEglModuleIsValidWindowIndex( SCTEglModuleContext* moduleContext, int index );
    void*                           sctiEglModuleGetWindow( SCTEglModuleContext* moduleContext, int index );
    void                            sctiEglModuleSetWindow( SCTEglModuleContext* moduleContext, int index, void* window );
    void                            sctiEglModuleDeleteWindow( SCTEglModuleContext* moduleContext, int index );

    /* Pixmaps */
    SCTBoolean                      sctiEglModuleIsValidPixmapIndex( SCTEglModuleContext* moduleContext, int index );
    void*                           sctiEglModuleGetPixmap( SCTEglModuleContext* moduleContext, int index );
    void                            sctiEglModuleSetPixmap( SCTEglModuleContext* moduleContext, int index, void* pixmap );
    void                            sctiEglModuleDeletePixmap( SCTEglModuleContext* moduleContext, int index );

    /* EGL display */
    SCTBoolean                      sctiEglModuleIsValidDisplayIndex( SCTEglModuleContext* moduleContext, int index );
    EGLDisplay                      sctiEglModuleGetDisplay( SCTEglModuleContext* moduleContext, int index );
    void                            sctiEglModuleSetDisplay( SCTEglModuleContext* moduleContext, int index, EGLDisplay eglDisplay );
    void                            sctiEglModuleDeleteDisplay( SCTEglModuleContext* moduleContext, int index );
    
    /* EGL configs */
    SCTBoolean                      sctiEglModuleIsValidConfigIndex( SCTEglModuleContext* moduleContext, int index );
    EGLConfig                       sctiEglModuleGetConfig( SCTEglModuleContext* moduleContext, int index );
    void                            sctiEglModuleSetConfig( SCTEglModuleContext* moduleContext, int index, EGLConfig eglConfig );
    void                            sctiEglModuleDeleteConfig( SCTEglModuleContext* moduleContext, int index );
    
    /* EGL surfaces */
    SCTBoolean                      sctiEglModuleIsValidSurfaceIndex( SCTEglModuleContext* moduleContext, int index );
    EGLSurface                      sctiEglModuleGetSurface( SCTEglModuleContext* moduleContext, int index );
    void                            sctiEglModuleSetSurface( SCTEglModuleContext* moduleContext, int index, EGLSurface surface );
    void                            sctiEglModuleDeleteSurface( SCTEglModuleContext* moduleContext, int displayIndex, int surfaceIndex );

    /* EGL contexts */
    SCTBoolean                      sctiEglModuleIsValidContextIndex( SCTEglModuleContext* moduleContext, int index );
    EGLContext                      sctiEglModuleGetContext( SCTEglModuleContext* moduleContext, int index );
    void                            sctiEglModuleSetContext( SCTEglModuleContext* moduleContext, int index, EGLContext context );
    void                            sctiEglModuleDeleteContext( SCTEglModuleContext* moduleContext, int displayIndex, int contextIndex );

    /* EGL images */
    SCTBoolean                      sctiEglModuleIsValidImageIndex( SCTEglModuleContext* moduleContext, int index );
    EGLImageKHR                     sctiEglModuleGetImage( SCTEglModuleContext* moduleContext, int index );
    void                            sctiEglModuleSetImage( SCTEglModuleContext* moduleContext, int index, EGLImageKHR image );
    void                            sctiEglModuleDeleteImage( SCTEglModuleContext* moduleContext, int displayIndex, int imageIndex, SCTPfnEglDestroyImageKhr func );
    
    /* EGL syncs */
    SCTBoolean                      sctiEglModuleIsValidSyncIndex( SCTEglModuleContext* moduleContext, int index );
    EGLImageKHR                     sctiEglModuleGetSync( SCTEglModuleContext* moduleContext, int index );
    void                            sctiEglModuleSetSync( SCTEglModuleContext* moduleContext, int index, EGLSyncKHR sync );
    void                            sctiEglModuleDeleteSync( SCTEglModuleContext* moduleContext, int displayIndex, int syncIndex, SCTPfnEglDestroySyncKhr func );

    /* EGL error string */
    const char*                     sctiEglGetErrorString( EGLint err );

    /* Get EGL config as name,value strings */
    SCTBoolean                      sctiEglGetConfigNameValue( EGLDisplay display, EGLConfig config, char* name, int namelen, char* value, int valuelen );
    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_EGLMODULE_H__ ) */
