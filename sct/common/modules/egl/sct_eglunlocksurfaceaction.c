/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_eglunlocksurfaceaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_egl.h"
#include "sct_eglmodule_parser.h"
#include "sct_eglmodule.h"

#include <stdio.h>
#include <string.h>

#if defined( _WIN32 )
# pragma warning ( disable : 4127 )
#endif  /* defined( _WIN32 ) */

/*!
 *
 *
 */
void* sctiCreateEglUnlockSurfaceActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTEglUnlockSurfaceActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTEglUnlockSurfaceActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEglUnlockSurfaceActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in UnlockSurface@Egl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTEglUnlockSurfaceActionContext ) );

    context->moduleContext = ( SCTEglModuleContext* )( moduleContext );

    if( sctiParseEglUnlockSurfaceActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyEglUnlockSurfaceActionContext( context );
        return NULL;
    }

    if( sctiEglModuleIsValidDisplayIndex( context->moduleContext, context->data.displayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid display index in UnlockSurface@Egl context creation." );
        sctiDestroyEglUnlockSurfaceActionContext( context );                
        return NULL;
    }
    
    if( sctiEglModuleIsValidSurfaceIndex( context->moduleContext, context->data.surfaceIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid surface index in UnlockSurface@Egl context creation." );
        sctiDestroyEglUnlockSurfaceActionContext( context );
        return NULL;
    }
    
    context->func          = NULL;
    context->funcValidated = SCT_FALSE;   
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyEglUnlockSurfaceActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiEglUnlockSurfaceActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTEglUnlockSurfaceActionContext*   context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTEglUnlockSurfaceActionContext* )( action->context );

    context->func = ( SCTEglUnlockSurfaceKHRProc )( siCommonGetProcAddress( NULL, "eglUnlockSurfaceKHR" ) );        
    if( context->func == NULL )
    {
        SCT_LOG_ERROR( "eglUnlockSurfaceKHR function not found in UnlockSurface@Egl action init." );
        return SCT_FALSE;
    }
    context->funcValidated = SCT_FALSE;   
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiEglUnlockSurfaceActionExecute( SCTAction* action, int frameNumber )
{
    SCTEglUnlockSurfaceActionContext*   context;
    EglUnlockSurfaceActionData*         data;    
    EGLDisplay                          display;
    EGLSurface                          surface;    
    const char*                         extString;   
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTEglUnlockSurfaceActionContext* )( action->context );
    data    = &( context->data );

    display = sctiEglModuleGetDisplay( context->moduleContext, data->displayIndex );
    if( display == EGL_NO_DISPLAY )
    {
        SCT_LOG_ERROR( "Undefined display in UnlockSurface@Egl action execute." );
        return SCT_FALSE;
    }
       
    if( context->funcValidated == SCT_FALSE )
    {
        extString = ( const char* )( eglQueryString( display, EGL_EXTENSIONS ) );
        if( extString == NULL ||
            ( strstr( extString, "EGL_KHR_lock_surface" ) == NULL &&
              strstr( extString, "EGL_KHR_lock_surface2" ) == NULL ) )
        {
            SCT_LOG_ERROR( "EGL_KHR_lock_surface/EGL_KHR_lock_surface2 extension not supported in UnlockSurface@Egl action execute." );
            return SCT_FALSE;
        }
        context->funcValidated = SCT_TRUE;
    }

    surface = sctiEglModuleGetSurface( context->moduleContext, data->surfaceIndex );
    if( surface == EGL_NO_SURFACE )
    {
        SCT_LOG_ERROR( "Invalid surface index in UnlockSurface@Egl action execute." );
        return SCT_FALSE;
    }

    SCT_ASSERT( context->func != NULL );
    
    if( context->func( display, surface ) == EGL_FALSE )
    {
        char    buf[ 256 ];
        sprintf( buf, "Unlocking surface failed in UnlockSurface@Egl action execute, error %s.",
                 sctiEglGetErrorString( eglGetError() ) );
        SCT_LOG_ERROR( buf );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiEglUnlockSurfaceActionTerminate( SCTAction* action )
{
    SCTEglUnlockSurfaceActionContext*   context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    
    context = ( SCTEglUnlockSurfaceActionContext* )( action->context );

    context->func          = NULL;    
    context->funcValidated = SCT_FALSE;   
}

/*!
 *
 *
 */
void sctiEglUnlockSurfaceActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyEglUnlockSurfaceActionContext( ( SCTEglUnlockSurfaceActionContext* )( action->context ) );
    action->context = NULL;
}

