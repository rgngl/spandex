/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_MEMORYBLOCKACTION_H__ )
#define __SCT_MEMORYBLOCKACTION_H__

#include "sct_types.h"
#include "sct_memorymodule_parser.h"

/*!
 *
 *
 */
typedef struct
{
    SCTBoolean              initialized;
    MemoryBlockActionData*  actionData;
    unsigned char**         buffers1;           /*!< Scratch memory buffers 1 */
    unsigned char**         buffers2;           /*!< Scratch memory buffers 2 */
    unsigned char**         alignedBuffers1;    /*!< Aligned memory buffers 1 */
    unsigned char**         alignedBuffers2;    /*!< Aligned memory buffers 2 */
} SCTMemoryBlockActionContext;    

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

   /* Block memory action functions. */
    void*                   sctiCreateMemoryBlockActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                    sctiDestroyMemoryBlockActionContext( void* context );
    SCTBoolean              sctiMemoryBlockActionInit( SCTAction* action, SCTBenchmark* benchmark );
    SCTBoolean              sctiMemoryBlockActionExecute( SCTAction* action, int frameNumber );
    void                    sctiMemoryBlockActionTerminate( SCTAction* action );
    void                    sctiMemoryBlockActionDestroy( SCTAction* action );
    SCTStringList*          sctiMemoryBlockActionGetWorkload( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */
    
#endif  /* !defined( __SCT_MEMORYBLOCKACTION_H__ ) */
