/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_memorymodule.h"
#include "sct_utils.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"

#include <string.h>

#include "sct_memorymodule_parser.h"
#include "sct_memorymodule_actions.h"

/*!
 * Internal data structure for memory actions. This same structure is
 * used in both block and single actions. Block action does not use
 * elementSize and unroll attributes as it operates always at byte
 * granularity and unroll may be implemented within system interface
 * implementation.
 *
 */

/* Memory module functions. */
SCTAttributeList*       sctiMemoryModuleInfo( SCTModule* module );
SCTAction*              sctiMemoryModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    sctiMemoryModuleDestroy( SCTModule* module );

/*!
 * Create memory module data structure which defines the name of the
 * module, its context, and sets up the module interface
 * functions. Memory module does not use module context.
 *
 * \return Module structure for memory module, or NULL if failure.
 */
SCTModule* sctCreateMemoryModule( void )
{
    SCTModule* module = sctCreateModule( "Memory",
                                         NULL,
#if defined( _WIN32 )
                                         _memory_parser_config,
#else   /* defined( _WIN32 ) */
                                         "",
#endif  /* defined( _WIN32 ) */
                                         sctiMemoryModuleInfo,
                                         sctiMemoryModuleCreateAction,
                                         sctiMemoryModuleDestroy );
    return module;
}

/*!
 * Create memory module information. Currently, memory module does not
 * use module information, i.e. the returned attribute list does not
 * contain any attributes.
 *
 * \param module Current memory module. Must be defined.
 *
 * \return Empty attribute list, or NULL if failure.
 */
SCTAttributeList* sctiMemoryModuleInfo( SCTModule* module )
{
    SCTAttributeList*   infoAttributes;
    
    SCT_ASSERT_ALWAYS( module != NULL );

    infoAttributes = sctCreateAttributeList();
    if( infoAttributes == NULL )
    {
        return NULL;
    }

    return infoAttributes;
}

/*!
 * Create memory module action. This function should be used only
 * through module interface data structure.
 *
 * \param module Current module. Must be defined.
 * \param name Name of the action to create. Must be defined.
 * \param type Action type. Must be defined.
 * \param attributes List of action attributes. Must be defined.
 *
 * \return Newly created action, or NULL if failure.
 */
SCTAction* sctiMemoryModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    return sctCreateActionFromTemplate( name, type, "Memory", NULL, attributes, MemoryActionTemplates, SCT_ARRAY_LENGTH( MemoryActionTemplates ) );
}

/*!
 * Destroy memory module. This function does not deallocate module
 * data structure but instead simply does the needed module cleanup,
 * if any.
 *
 * \param module Module to destroy. Must be defined.
 */
void sctiMemoryModuleDestroy( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );
}
