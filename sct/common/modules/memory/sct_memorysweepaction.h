/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_MEMORYSWEEPACTION_H__ )
#define __SCT_MEMORYSWEEPACTION_H__

#include "sct_memorymodule_parser.h"

/*!
 *
 *
 */
typedef struct
{
    SCTBoolean              initialized;
    MemorySweepActionData*  actionData;
    unsigned char**         buffers1;           /*!< Scratch memory buffers 1 */
    unsigned char**         buffers2;           /*!< Scratch memory buffers 2 */
    unsigned char**         alignedBuffers1;    /*!< Aligned memory buffers 1 */
    unsigned char**         alignedBuffers2;    /*!< Aligned memory buffers 2 */
} SCTMemorySweepActionContext;    

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    /* Sweep memory action functions. */
    void*                   sctiCreateMemorySweepActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                    sctiDestroyMemorySweepActionContext( void* context );
    SCTBoolean              sctiMemorySweepActionInit( SCTAction* action, SCTBenchmark* benchmark );
    SCTBoolean              sctiMemorySweepActionExecute( SCTAction* action, int frameNumber );
    void                    sctiMemorySweepActionTerminate( SCTAction* action );
    void                    sctiMemorySweepActionDestroy( SCTAction* action );
    SCTStringList*          sctiMemorySweepActionGetWorkload( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */
    
#endif  /* !defined( __SCT_MEMORYSWEEPACTION_H__ ) */
