/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_memoryblockaction.h"
#include "sct_memorymodule_core.h"
#include "sct_memorymodule_utils.h"
#include "sct_simemory.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

/* ---------------------------------------------------------------------- */
/*!
 * Create block memory action context.
 *
 * \param attributes Attribute list. Must be defined.
 *
 * \return Block action context, or NULL if failure.
 *
 */
void* sctiCreateMemoryBlockActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTMemoryBlockActionContext* context;
    MemoryBlockActionData*       actionData;
    void*                        systemContext = siCommonGetContext();

    SCT_USE_VARIABLE( moduleContext );
    
    /* Allocate memory for memory data structure. */
    context = ( SCTMemoryBlockActionContext* )( siCommonMemoryAlloc( systemContext, sizeof( SCTMemoryBlockActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Block@Memory action context creation." );
        return NULL;
    }

    /* Initialize memory data structure to zero. */
    memset( context, 0, sizeof( SCTMemoryBlockActionContext ) );

    actionData = ( MemoryBlockActionData* )( siCommonMemoryAlloc( systemContext, sizeof( MemoryBlockActionData ) ) );
    if( actionData == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Block@Memory action context creation." );
        siCommonMemoryFree( systemContext, context );
        return NULL;
    }

    context->actionData = actionData;
    
    /* Parse block memory attributes. */
    if( sctiParseMemoryBlockActionAttributes( actionData, attributes ) == SCT_FALSE )
    {
        siCommonMemoryFree( systemContext, context );
        siCommonMemoryFree( systemContext, actionData );
        return NULL;
    }

    return context;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void sctiDestroyMemoryBlockActionContext( void* context )
{
    SCTMemoryBlockActionContext* c;

    SCT_ASSERT_ALWAYS( context != NULL );

    c = ( SCTMemoryBlockActionContext* )( context );

    if( c->actionData != NULL )
    {
        siCommonMemoryFree( NULL, c->actionData );
    }
    siCommonMemoryFree( NULL, c );
}

/* ---------------------------------------------------------------------- */
/*!
 * Initializes block memory action.
 *
 * \param action Action to initialize.
 *
 * \return SCT_TRUE if initialization succeeded, SCT_FALSE if failed.
 *
 */
SCTBoolean sctiMemoryBlockActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTMemoryBlockActionContext* context;
    MemoryBlockActionData*       actionData;

    SCT_USE_VARIABLE( benchmark );
    SCT_ASSERT_ALWAYS( action != NULL );

    context    = ( SCTMemoryBlockActionContext* )( action->context );
    actionData = ( MemoryBlockActionData* )( context->actionData );

    if( context->initialized == SCT_TRUE )
    {
        return SCT_TRUE;
    }
    
    context->initialized = SCT_TRUE;

    SCT_ASSERT( context->buffers1 == NULL );
    SCT_ASSERT( context->buffers2 == NULL );
    SCT_ASSERT( context->alignedBuffers1 == NULL );
    SCT_ASSERT( context->alignedBuffers2 == NULL );

    /* Create scratch buffers 1. Make sure the buffers contain extra
        space for setting up the alignment. */
    context->buffers1 = sctiCreateBlocks( actionData->blockCount, actionData->blockSize + ( actionData->align * 3 - 1 ) );
    if( context->buffers1 == NULL )
    {
        SCT_LOG_ERROR( "Memory allocation failed in Block@Memory action init." );
        sctiMemoryBlockActionTerminate( action );
        return SCT_FALSE;
    }
    
    /* Get aligned addresses to scratch buffers 1. */
    context->alignedBuffers1 = sctiGetAlignedBlocks( context->buffers1, actionData->blockCount, actionData->align );
    if( context->alignedBuffers1 == NULL )
    {
        SCT_LOG_ERROR( "Memory allocation failed in Block@Memory action init." );
        sctiMemoryBlockActionTerminate( action );
        return SCT_FALSE;
    }

    /* Buffers2 are only needed for copy, blend is not supported by
     * block memory benchmark. */
    if( actionData->operation == BLOCK_OPERATION_COPY )
    {
        context->buffers2 = sctiCreateBlocks( actionData->blockCount, actionData->blockSize + ( actionData->align * 3 - 1 ) );
        if( context->buffers2 == NULL )
        {
            SCT_LOG_ERROR( "Memory allocation failed in Block@Memory action init." );
            sctiMemoryBlockActionTerminate( action );
            return SCT_FALSE;
        }

        /* Get aligned addresses to scratch buffers 2. */
        context->alignedBuffers2 = sctiGetAlignedBlocks( context->buffers2, actionData->blockCount, actionData->align );
        if( context->alignedBuffers2 == NULL )
        {
            SCT_LOG_ERROR( "Memory allocation failed in Block@Memory action init." );
            sctiMemoryBlockActionTerminate( action );
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 * Run block memory action.
 *
 * \param action Block memory action to run. Must be defined.
 * \param frameNumber Not used by block memory action.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctiMemoryBlockActionExecute( SCTAction* action, int frameNumber )
{
    SCTMemoryBlockActionContext*    context;
    MemoryBlockActionData*          actionData;

    SCT_ASSERT( action != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context    = ( SCTMemoryBlockActionContext* )( action->context );
    actionData = ( MemoryBlockActionData* )( context->actionData );

    SCT_ASSERT( context->initialized == SCT_TRUE );
    SCT_ASSERT( context->alignedBuffers1 != NULL );
    SCT_ASSERT( actionData->operation == BLOCK_OPERATION_FILLZ ||
                actionData->operation == BLOCK_OPERATION_COPY );
    
    /* Run the correct block memory operation in the memory system
        interface. */
    switch( actionData->operation )
    {
    case BLOCK_OPERATION_FILLZ:
       return siMemoryBlockFillZ( ( void ** )( context->alignedBuffers1 ),
                                   actionData->blockCount,
                                   actionData->blockSize,
                                   actionData->iterations ) == 0 ? SCT_FALSE : SCT_TRUE;
       
    case BLOCK_OPERATION_COPY:
        SCT_ASSERT( context->alignedBuffers2 != NULL );
        return siMemoryBlockCopy( ( void ** )( context->alignedBuffers1 ),
                                  ( void ** )( context->alignedBuffers2 ),
                                  actionData->blockCount,
                                  actionData->blockSize,
                                  actionData->iterations ) == 0 ? SCT_FALSE : SCT_TRUE;
    }

    /* If we end up here then it was an unknown operation. */
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 * Terminate block memory action and release all resources allocated
 * in init.
 *
 * \param action Block memory action to terminate. Must be defined.
 *
 */
void sctiMemoryBlockActionTerminate( SCTAction* action )
{
    SCTMemoryBlockActionContext*    context;
    MemoryBlockActionData*          actionData;
    void*                           systemContext  = siCommonGetContext();

    SCT_ASSERT_ALWAYS( action != NULL );

    context    = ( SCTMemoryBlockActionContext* )( action->context );
    actionData = ( MemoryBlockActionData* )( context->actionData );
    
    if( context->alignedBuffers1 != NULL )
    {
        siCommonMemoryFree( systemContext, context->alignedBuffers1 );
        context->alignedBuffers1 = NULL;
    }
    if( context->alignedBuffers2 != NULL )
    {
        siCommonMemoryFree( systemContext, context->alignedBuffers2 );
        context->alignedBuffers2 = NULL;
    }

    sctiDestroyBlocks( context->buffers1, actionData->blockCount );
    context->buffers1 = NULL;

    sctiDestroyBlocks( context->buffers2, actionData->blockCount );
    context->buffers2 = NULL;

    context->initialized = SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 * Destroy block memory action context. This function does not destroy
 * the action data structure.
 *
 * \param action Block memory action to destroy. Must be defined.
 *
 */
void sctiMemoryBlockActionDestroy( SCTAction* action )
{
    SCTMemoryBlockActionContext*    data;

    SCT_ASSERT_ALWAYS( action != NULL );

    data = ( SCTMemoryBlockActionContext* )( action->context );

    SCT_ASSERT( data->buffers1 == NULL );
    SCT_ASSERT( data->buffers2 == NULL );
    SCT_ASSERT( data->alignedBuffers1 == NULL );
    SCT_ASSERT( data->alignedBuffers2 == NULL );

    sctiDestroyMemoryBlockActionContext( data );
}

/* ---------------------------------------------------------------------- */
/*!
 * Get block memory action workload, i.e. number of bytes in each iteration.
 *
 * \param action Block memory action. Must be defined.
 *
 * \return List of workload string, or NULL if failure.
 *
 */
SCTStringList* sctiMemoryBlockActionGetWorkload( SCTAction* action )
{
    SCTMemoryBlockActionContext*    context;
    MemoryBlockActionData*          actionData;
    SCTStringList*                  workloads;
    char                            workloadString[ 64 ];
    int                             workloadValue;

    SCT_ASSERT_ALWAYS( action != NULL );

    context    = ( SCTMemoryBlockActionContext* )( action->context );
    actionData = ( MemoryBlockActionData* )( context->actionData );

    workloads = sctCreateStringList();
    if( workloads == NULL )
    {
        return NULL;
    }

    /* Calculate workload value and format workload string. */
    workloadValue = actionData->blockCount * actionData->blockSize * actionData->iterations;
    sprintf( workloadString, "%d bytes", workloadValue );

    if( sctAddStringCopyToList( workloads, workloadString ) == SCT_FALSE )
    {
        sctDestroyStringList( workloads );
        return NULL;
    }

    return workloads;
}
