/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_MEMORYMODULE_CORE_H__ )
#define __SCT_MEMORYMODULE_CORE_H__

#include "sct_types.h"

/*!
 *  Mask to use with XOR in modify memory operation
 *
 */
#define FILTER_MASK                      0xAAAAAAAA

/*!
 * UNROLL COUNT and UNROLL macro are used in loop unrolling. UNROLL_COUNT
 * defines how many operations are unrolled in a loop. UNROLL macro implements
 * the loop unroll.  These must be changed in sync.
 */
#define UNROLL_COUNT                     8
#define UNROLL( op ) op; op; op; op; op; op; op; op;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    SCTBoolean runRead( unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations );
    SCTBoolean runFillZ( unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations );
    SCTBoolean runModify( unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations );
    SCTBoolean runCopy( const unsigned char** buffers1, unsigned char** buffers2, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations );
    SCTBoolean runBlend( unsigned char** buffers1, const unsigned char** buffers2, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations );

    SCTBoolean runSweepRead( int interval, unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations );
    SCTBoolean runSweepFillZ( int interval, unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations );
    SCTBoolean runSweepModify( int interval, unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations );
    SCTBoolean runSweepCopy( int interval, const unsigned char** buffers1, unsigned char** buffers2, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations );
    SCTBoolean runSweepBlend( int interval, unsigned char** buffers1, const unsigned char** buffers2, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations );

    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_MEMORYMODULE_CORE_H__ ) */
