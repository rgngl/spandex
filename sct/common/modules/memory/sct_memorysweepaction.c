/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_memorysweepaction.h"
#include "sct_memorymodule_core.h"
#include "sct_memorymodule_utils.h"
#include "sct_simemory.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

/* ---------------------------------------------------------------------- */
/*!
 * Create sweep action context.
 *
 * \param attributes Attribute list. Must be defined.
 *
 * \return Sweep action context, or NULL if failure.
 *
 * \note The memory block size needs to be evenly divided by the element size
 * times iterations, i.e. BlockSize % ( ElementSize * Iterations ) == 0.
 *
 */
void* sctiCreateMemorySweepActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTMemorySweepActionContext*    context;
    MemorySweepActionData*          actionData;
    void*                           systemContext   = siCommonGetContext();

    SCT_USE_VARIABLE( moduleContext );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    /* Allocate memory for sweep action context.  */
    context = ( SCTMemorySweepActionContext* )( siCommonMemoryAlloc( systemContext, sizeof( SCTMemorySweepActionContext ) ) );

    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Sweep@Memory context creation." );
        return NULL;
    }

    /* Initialize memory data structure to zero. */
    memset( context, 0, sizeof( SCTMemorySweepActionContext ) );

    actionData = ( MemorySweepActionData* )( siCommonMemoryAlloc( systemContext, sizeof( MemorySweepActionData ) ) );
    if( actionData == NULL )
    {
        siCommonMemoryFree( systemContext, context );
        SCT_LOG_ERROR( "Allocation failed in Sweep@Memory context creation." );
        return NULL;
    }

    context->actionData = actionData;

    /* Parse memory benchmark attributes. */
    if( sctiParseMemorySweepActionAttributes( actionData, attributes ) == SCT_FALSE )
    {
        siCommonMemoryFree( systemContext, context );
        siCommonMemoryFree( systemContext, actionData );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyMemorySweepActionContext( void* context )
{
    SCTMemorySweepActionContext*    data;

    SCT_ASSERT_ALWAYS( context != NULL );

    data = ( SCTMemorySweepActionContext* )( context );

    if( data->actionData != NULL )
    {
        siCommonMemoryFree( NULL, data->actionData );
    }
    siCommonMemoryFree( NULL, data );
}

/* ---------------------------------------------------------------------- */
/*!
 * Initialize sweep memory action.
 *
 * \param action Action to initialize.
 *
 * \return SCT_TRUE if initialization succeeded, SCT_FALSE if failed.
 *
 */
SCTBoolean sctiMemorySweepActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTMemorySweepActionContext*    context;
    MemorySweepActionData*          actionData;

    SCT_USE_VARIABLE( benchmark );
    SCT_ASSERT_ALWAYS( action != NULL );

    context    = ( SCTMemorySweepActionContext* )( action->context );
    actionData = ( MemorySweepActionData* )( context->actionData );

    if( context->initialized == SCT_TRUE )
    {
        return SCT_TRUE;
    }
    context->initialized = SCT_TRUE;

    SCT_ASSERT( context->buffers1 == NULL );
    SCT_ASSERT( context->buffers2 == NULL );
    SCT_ASSERT( context->alignedBuffers1 == NULL );
    SCT_ASSERT( context->alignedBuffers2 == NULL );

    /* Create scratch buffers 1. Make sure the buffers contain extra space for
        setting up the alignment. */
    context->buffers1 = sctiCreateBlocks( actionData->blockCount, actionData->blockSize + ( actionData->align * 3 - 1 ) );
    if( context->buffers1 == NULL )
    {
        SCT_LOG_ERROR( "Memory allocation failed in Sweep@Memory action init." );
        sctiMemorySweepActionTerminate( action );
        return SCT_FALSE;
    }

    /* Get aligned addresses to scratch buffers 1. */
    context->alignedBuffers1 = sctiGetAlignedBlocks( context->buffers1, actionData->blockCount, actionData->align );
    if( context->alignedBuffers1 == NULL )
    {
        SCT_LOG_ERROR( "Memory allocation failed in Sweep@Memory action init." );
        sctiMemorySweepActionTerminate( action );
        return SCT_FALSE;
    }

    /* Buffers 2 are only needed for copy and blend. */
    if( actionData->operation == SWEEP_OPERATION_COPY ||
        actionData->operation == SWEEP_OPERATION_BLEND )
    {
        context->buffers2 = sctiCreateBlocks( actionData->blockCount, actionData->blockSize + ( actionData->align * 3 - 1 ) );
        if( context->buffers2 == NULL )
        {
            SCT_LOG_ERROR( "Memory allocation failed in Sweep@Memory action init." );
            sctiMemorySweepActionTerminate( action );
            return SCT_FALSE;
        }

        /* Get aligned addresses to scratch buffers 2. */
        context->alignedBuffers2 = sctiGetAlignedBlocks( context->buffers2, actionData->blockCount, actionData->align );
        if( context->alignedBuffers2 == NULL )
        {
            SCT_LOG_ERROR( "Memory allocation failed in Sweep@Memory action init." );
            sctiMemorySweepActionTerminate( action );
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 * Run sweep memory action.
 *
 * \param action Sweep memory action to run. Must be defined.
 * \param frameNumber Not used by sweep memory action.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctiMemorySweepActionExecute( SCTAction* action, int frameNumber )
{
    SCTMemorySweepActionContext*    context;
    MemorySweepActionData*          actionData;

    SCT_ASSERT( action != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context    = ( SCTMemorySweepActionContext* )( action->context );
    actionData = ( MemorySweepActionData* )( context->actionData );

    SCT_ASSERT( context->initialized == SCT_TRUE );
    SCT_ASSERT( context->alignedBuffers1 != NULL );
    SCT_ASSERT( actionData->operation == SWEEP_OPERATION_READ   ||
                actionData->operation == SWEEP_OPERATION_FILLZ  ||
                actionData->operation == SWEEP_OPERATION_MODIFY ||
                actionData->operation == SWEEP_OPERATION_COPY   ||
                actionData->operation == SWEEP_OPERATION_BLEND  );

    /* Call the correct memory benchmark function. */
    switch( actionData->operation )
    {
    case SWEEP_OPERATION_READ:
        return runSweepRead( actionData->interval, context->alignedBuffers1, actionData->blockCount, actionData->blockSize, actionData->elementSize, actionData->loopUnroll, actionData->iterations );

    case SWEEP_OPERATION_FILLZ:
        return runSweepFillZ( actionData->interval, context->alignedBuffers1, actionData->blockCount, actionData->blockSize, actionData->elementSize, actionData->loopUnroll, actionData->iterations );

    case SWEEP_OPERATION_MODIFY:
        return runSweepModify( actionData->interval, context->alignedBuffers1, actionData->blockCount, actionData->blockSize, actionData->elementSize, actionData->loopUnroll, actionData->iterations);

    case SWEEP_OPERATION_COPY:
        return runSweepCopy( actionData->interval, ( const unsigned char ** )( context->alignedBuffers1 ), context->alignedBuffers2, actionData->blockCount, actionData->blockSize, actionData->elementSize, actionData->loopUnroll, actionData->iterations );

    case SWEEP_OPERATION_BLEND:
        SCT_ASSERT( context->alignedBuffers2 != NULL );
        return runSweepBlend( actionData->interval, context->alignedBuffers1, ( const unsigned char ** )( context->alignedBuffers2 ), actionData->blockCount, actionData->blockSize, actionData->elementSize, actionData->loopUnroll, actionData->iterations );
    }

    /* If we end up here then it was an unknown operation. */
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 * Terminate sweep memory action and release all resources allocated
 * in init.
 *
 * \param action Sweep memory action to terminate. Must be defined.
 *
 */
void sctiMemorySweepActionTerminate( SCTAction* action )
{
    SCTMemorySweepActionContext*    context;
    MemorySweepActionData*          actionData;
    void*                           systemContext  = siCommonGetContext();

    SCT_ASSERT_ALWAYS( action != NULL );

    context    = ( SCTMemorySweepActionContext* )( action->context );
    actionData = ( MemorySweepActionData* )( context->actionData );

    if( context->alignedBuffers1 != NULL )
    {
        siCommonMemoryFree( systemContext, context->alignedBuffers1 );
        context->alignedBuffers1 = NULL;
    }
    if( context->alignedBuffers2 != NULL )
    {
        siCommonMemoryFree( systemContext, context->alignedBuffers2 );
        context->alignedBuffers2 = NULL;
    }

    sctiDestroyBlocks( context->buffers1, actionData->blockCount );
    context->buffers1 = NULL;

    sctiDestroyBlocks( context->buffers2, actionData->blockCount );
    context->buffers2 = NULL;

    context->initialized = SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 * Destroy sweep memory action context. This function does not
 * destroy the action data structure.
 *
 * \param action Sweep memory action to destroy. Must be defined.
 *
 */
void sctiMemorySweepActionDestroy( SCTAction* action )
{
    SCTMemorySweepActionContext*   data;

    SCT_ASSERT_ALWAYS( action != NULL );

    data = ( SCTMemorySweepActionContext* )( action->context );

    SCT_ASSERT( data->buffers1 == NULL );
    SCT_ASSERT( data->buffers2 == NULL );
    SCT_ASSERT( data->alignedBuffers1 == NULL );
    SCT_ASSERT( data->alignedBuffers2 == NULL );

    sctiDestroyMemorySweepActionContext( data );
}

/* ---------------------------------------------------------------------- */
/*!
 * Get sweep memory action workload, i.e. number of bytes in each iteration.
 *
 * \param action Sweep memory action. Must be defined.
 *
 * \return List of workload string, or NULL if failure.
 *
 */
SCTStringList* sctiMemorySweepActionGetWorkload( SCTAction* action )
{
    SCTMemorySweepActionContext*    context;
    MemorySweepActionData*          actionData;
    SCTStringList*                  workloads;
    char                            workloadString[ 64 ];
    int                             workloadValue;

    SCT_ASSERT_ALWAYS( action != NULL );

    context    = ( SCTMemorySweepActionContext* )( action->context );
    actionData = ( MemorySweepActionData* )( context->actionData );

    workloads = sctCreateStringList();
    if( workloads == NULL )
    {
        return NULL;
    }

    /* Calculate result value and format result string. */
    workloadValue = actionData->blockCount * actionData->blockSize * actionData->iterations / actionData->interval;
    sprintf( workloadString, "%d bytes", workloadValue );

    if( sctAddStringCopyToList( workloads, workloadString ) == SCT_FALSE )
    {
        sctDestroyStringList( workloads );
        return NULL;
    }

    return workloads;
}
