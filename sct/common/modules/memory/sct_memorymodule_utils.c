/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_memorymodule_utils.h"
#include "sct_sicommon.h"

#include <string.h>

/*!
 * Create an array of memory blocks with specified size in bytes.
 *
 * \param count Number of blocks.
 * \param length Length of one block, in bytes.
 *
 * \return Array of memory blocks, or NULL in failure.
 *
 */
unsigned char** sctiCreateBlocks( int count, int length )
{
    void*               context = siCommonGetContext();
    unsigned char**     b       = NULL;
    int                 i;
    SCTBoolean          status  = SCT_TRUE;

    /* First, reserve memory for pointer array to separate blocks. */
    b = ( unsigned char ** )siCommonMemoryAlloc( context, count * sizeof( unsigned char* ) );
    if( b == NULL )
    {
        return NULL;
    }
    memset( b, 0, count * sizeof( unsigned char * ) );

    /* Secondly, reserve memory for blocks. */
    for( i = 0; i < count; i++ )
    {
        b[ i ] = ( unsigned char * )siCommonMemoryAlloc( context, length );
        if( b[ i ] == NULL )
        {
            status = SCT_FALSE;
        }
        else
        {
            /* Initialize memory. */
            memset( b[ i ], 0x1F, length );
        }
    }

    /* Free reserved memory if allocation failed. */
    if( status == SCT_FALSE )
    {
        sctiDestroyBlocks( b, count );
        return NULL;
    }

    /* Return an array of memory blocks. */
    return b;
}


/*!
 * Destroy an array of memory blocks.
 *
 * \param blocks Array of memory blocks to destroy. Ignored if NULL.
 * \param count Number of blocks in the array.
 *
 * \note  NULL array and NULL elements in the array are permitted.
 *
 */
void sctiDestroyBlocks( unsigned char** blocks, int count )
{
    void*               context = siCommonGetContext();
    int                 i;

    if( blocks != NULL )
    {
        for( i = 0; i < count; i++ )
        {
            if( blocks[ i ] != NULL )
            {
                siCommonMemoryFree( context, blocks[ i ] );
                blocks[ i ] = NULL;
            }
        }
        siCommonMemoryFree( context, blocks );
    }
}


/*!
 * Get memory block aligned addresses.
 *
 * \param blocks Pointer to unaligned blocks. Must be defined.
 * \param count Number of blocks.
 * \param align Block start address alignment.
 *
 * \return Array of aligned block addresses, or NULL in failure.  Note
 * that aligned block addresses point within original blocks, i.e.
 * this function reserves memory only for the addressa array and not
 * for the actual blocks.
 *
 */
unsigned char** sctiGetAlignedBlocks( unsigned char** blocks, int count, int align )
{
    unsigned char**     aligned;
    unsigned char*      p;
    int                 i;
    int                 alignx2     = align * 2;

    SCT_ASSERT( blocks != NULL );

    /* Reserve memory for pointer array to aligned memory locations. */
    aligned = ( unsigned char** )siCommonMemoryAlloc( NULL, count * sizeof( unsigned char* ) );
    if( aligned == NULL )
    {
        return NULL;
    }

    /* Search first memory location within each block that is multiple
        of align but NOT multiple of align * 2. */
    for( i = 0; i < count; ++i )
    {
        for( p = blocks[ i ]; ; ++p )
        {
            if( ( unsigned int )( p ) % align == 0 &&  ( unsigned int )( p ) % alignx2 != 0 )
            {
                aligned[ i ] = p;
                break;
            }
        }
    }

    return aligned;
}
