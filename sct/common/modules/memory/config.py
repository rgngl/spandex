#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
SetModuleName( 'Memory' )

######################################################################
AddInclude( 'sct_memorymodule_core.h' )

######################################################################
AddActionConfig( 'Block',
                 'Init'+'Execute'+'Terminate'+'Destroy'+'GetWorkload',
                 [ IntAttribute(                'BlockCount',                           1 ),
                   IntAttribute(                'BlockSize',                            1 ),
                   IntAttribute(                'Iterations',                           1 ),
                   IntAttribute(                'Align',                                1 ),
                   AutoEnumAttribute(           'Operation',                            [ 'FILLZ',
                                                                                          'COPY' ] )
                   ]
                 )

######################################################################
AddActionConfig( 'Single',
                 'Init'+'Execute'+'Terminate'+'Destroy'+'GetWorkload',
                 [ IntAttribute(                'BlockCount',                           1 ),
                   IntAttribute(                'BlockSize',                            1 ),
                   EnumIntAttribute(            'ElementSize',                          [ 1,
                                                                                          2 ,
                                                                                          4 ] ), 
                   IntAttribute(                'Iterations',                           1 ),
                   IntAttribute(                'Align',                                1 ),
                   AutoEnumAttribute(           'Operation',                            [ 'READ',
                                                                                          'FILLZ',
                                                                                          'MODIFY',
                                                                                          'COPY',
                                                                                          'BLEND' ] ),
                   OnOffAttribute(              'LoopUnroll' )
                   ],
                 [ ValidatorConfig( 'LoopUnroll == SCT_TRUE && ( ( BlockSize / ElementSize ) < UNROLL_COUNT || ( BlockSize / ElementSize ) % UNROLL_COUNT != 0 )',
                                    'BlockSize and ElementSize values are incompatible with unrolling' ),
                   ValidatorConfig( '(( Align - 1 ) & Align) != 0',
                                    'Align has to be power of two' ) ]
                 )

######################################################################
AddActionConfig( 'Sweep',
                 'Init'+'Execute'+'Terminate'+'Destroy'+'GetWorkload',
                 [ IntAttribute(                'Interval',                             1 ),
                   IntAttribute(                'BlockCount',                           1 ),
                   IntAttribute(                'BlockSize',                            1 ),
                   EnumIntAttribute(            'ElementSize',                          [ 1,
                                                                                          2,
                                                                                          4 ] ), 
                   IntAttribute(                'Iterations',                           1 ),
                   IntAttribute(                'Align',                                1 ),
                   AutoEnumAttribute(           'Operation',                            [ 'READ',
                                                                                          'FILLZ',
                                                                                          'MODIFY',
                                                                                          'COPY',
                                                                                          'BLEND' ] ),
                   OnOffAttribute(              'LoopUnroll' )
                   ],
                 [ ValidatorConfig( 'LoopUnroll == SCT_TRUE && ( ( BlockSize / ElementSize / Interval ) < UNROLL_COUNT || ( BlockSize / ElementSize / Interval ) % UNROLL_COUNT != 0 )',
                                    'BlockSize, ElementSize and Interval values are incompatible with unrolling' ),
                   ValidatorConfig( '(( Align - 1 ) & Align) != 0',
                                    'Align has to be power of two' ),
                   ValidatorConfig( 'BlockSize % ( Interval * ElementSize ) != 0',
                                    'Incompatible Interval and BlockSize' ) ]
                 )

######################################################################
# END OF FILE
######################################################################
