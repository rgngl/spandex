/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_memorymodule_core.h"
#include "sct_sicommon.h"

/*!
 * MACRO ALERT! The macros below are used to create memory benchmark functions
 * so that they are the optimal for the particular element size and data type,
 * without having to write a separate function for each of them
 */

/* Read function, without loop unrolling. */

#define READFUNC( ELEMENTSIZE, DATATYPE )                                           \
static SCTBoolean runRead##ELEMENTSIZE( DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                                   \
    unsigned int        i, j, k;                                                    \
    volatile DATATYPE   **top, *b;                                                  \
    const int           length = bytes / ELEMENTSIZE;                               \
    int                 accum = 0;                                                  \
    volatile DATATYPE   *s;                                                         \
                                                                                    \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                                \
    SCT_ASSERT( bytes > 0 );                                                        \
    SCT_ASSERT( iterations >= 0 );                                                  \
    SCT_ASSERT( count > 0 );                                                        \
    SCT_ASSERT( buffers != NULL );                                                  \
                                                                                    \
    s = ( volatile DATATYPE* )*buffers;                                             \
                                                                                    \
    k = iterations;                                                                 \
    do                                                                              \
    {                                                                               \
        top = ( volatile DATATYPE** )buffers;                                       \
        j = count;                                                                  \
        do                                                                          \
        {                                                                           \
            b = ( volatile DATATYPE* )*( top++ );                                   \
            i = length;                                                             \
            do                                                                      \
            {                                                                       \
                accum += *b++;                                                      \
            } while( --i );                                                         \
        } while( --j );                                                             \
    } while( --k );                                                                 \
                                                                                    \
    *s = ( DATATYPE )( accum );                                                     \
                                                                                    \
    return SCT_TRUE;                                                                \
}                                                                                   \

/* Read benchmark function, with loop unrolling. */

#define READFUNC_UNROLL( ELEMENTSIZE, DATATYPE )                                    \
static SCTBoolean runRead##ELEMENTSIZE##_Unroll( DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                                   \
    unsigned int        i, j, k;                                                    \
    volatile DATATYPE   **top, *b;                                                  \
    const int           length = bytes / ELEMENTSIZE / UNROLL_COUNT;                \
    int                 accum = 0;                                                  \
    volatile DATATYPE   *s;                                                         \
                                                                                    \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                                \
    SCT_ASSERT( ( bytes / sizeof( DATATYPE ) ) % UNROLL_COUNT == 0 );               \
    SCT_ASSERT( bytes > 0 );                                                        \
    SCT_ASSERT( iterations >= 0 );                                                  \
    SCT_ASSERT( count > 0 );                                                        \
    SCT_ASSERT( buffers != NULL );                                                  \
                                                                                    \
    s = ( volatile DATATYPE* )*buffers;                                             \
                                                                                    \
    k = iterations;                                                                 \
    do                                                                              \
    {                                                                               \
        top = ( volatile DATATYPE** )buffers;                                       \
        j = count;                                                                  \
        do                                                                          \
        {                                                                           \
            b = ( volatile DATATYPE* )*( top++ );                                   \
            i = length;                                                             \
            do                                                                      \
            {                                                                       \
                UNROLL( accum += *b++ );                                            \
            } while( --i );                                                         \
        } while( --j );                                                             \
    } while( --k );                                                                 \
                                                                                    \
    *s = ( DATATYPE )( accum );                                                     \
                                                                                    \
    return SCT_TRUE;                                                                \
}                                                                                   \

/* Fillz benchmark function, without loop unrolling. */

#define FILLZFUNC( ELEMENTSIZE, DATATYPE )                                          \
static SCTBoolean runFillZ##ELEMENTSIZE( DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                                   \
    unsigned int        i, j, k;                                                    \
    volatile DATATYPE   **top, *b;                                                  \
    const int           length = bytes / ELEMENTSIZE;                               \
                                                                                    \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                                \
    SCT_ASSERT( bytes > 0 );                                                        \
    SCT_ASSERT( iterations >= 0 );                                                  \
    SCT_ASSERT( count > 0 );                                                        \
    SCT_ASSERT( buffers != NULL );                                                  \
                                                                                    \
    k = iterations;                                                                 \
    do                                                                              \
    {                                                                               \
        top = ( volatile DATATYPE** )buffers;                                       \
        j = count;                                                                  \
        do                                                                          \
        {                                                                           \
            b = ( volatile DATATYPE* )*( top++ );                                   \
            i = length;                                                             \
            do                                                                      \
            {                                                                       \
                *b++ = 0;                                                           \
            } while( --i );                                                         \
        } while( --j );                                                             \
    } while( --k );                                                                 \
                                                                                    \
    return SCT_TRUE;                                                                \
}                                                                                   \

/* Fillz benchmark function, with loop unrolling. */

#define FILLZFUNC_UNROLL( ELEMENTSIZE, DATATYPE )                                   \
static SCTBoolean runFillZ##ELEMENTSIZE##_Unroll( DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                                   \
    unsigned int        i, j, k;                                                    \
    volatile DATATYPE   **top, *b;                                                  \
    const int           length = bytes / ELEMENTSIZE / UNROLL_COUNT;                \
                                                                                    \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                                \
    SCT_ASSERT( ( bytes / sizeof( DATATYPE ) ) % UNROLL_COUNT == 0 );               \
    SCT_ASSERT( bytes > 0 );                                                        \
    SCT_ASSERT( iterations >= 0 );                                                  \
    SCT_ASSERT( count > 0 );                                                        \
    SCT_ASSERT( buffers != NULL );                                                  \
                                                                                    \
    k = iterations;                                                                 \
    do                                                                              \
    {                                                                               \
        top = ( volatile DATATYPE** )buffers;                                       \
        j = count;                                                                  \
        do                                                                          \
        {                                                                           \
            b = ( volatile DATATYPE* )*( top++ );                                   \
            i = length;                                                             \
            do                                                                      \
            {                                                                       \
                UNROLL( *b++ = 0 );                                                 \
            } while( --i );                                                         \
        } while( --j );                                                             \
    } while( --k );                                                                 \
                                                                                    \
    return SCT_TRUE;                                                                \
}                                                                                   \

/* Modify benchmark function, without loop unrolling. */

#define MODIFYFUNC( ELEMENTSIZE, DATATYPE )                                         \
static SCTBoolean runModify##ELEMENTSIZE( DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                                   \
    unsigned int        i, j, k;                                                    \
    volatile DATATYPE   **top, *b;                                                  \
    const int           length = bytes / ELEMENTSIZE;                               \
                                                                                    \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                                \
    SCT_ASSERT( iterations >= 0 );                                                  \
    SCT_ASSERT( count > 0 );                                                        \
    SCT_ASSERT( buffers != NULL );                                                  \
                                                                                    \
    k = iterations;                                                                 \
    do                                                                              \
    {                                                                               \
        top = ( volatile DATATYPE** )buffers;                                       \
        j = count;                                                                  \
        do                                                                          \
        {                                                                           \
            b = ( volatile DATATYPE* )*( top++ );                                   \
            i = length;                                                             \
            do                                                                      \
            {                                                                       \
                *b = ( DATATYPE ) ( *b ^ FILTER_MASK );                             \
                b++;                                                                \
            } while( --i );                                                         \
        } while( --j );                                                             \
    } while( --k );                                                                 \
                                                                                    \
    return SCT_TRUE;                                                                \
}                                                                                   \

/* Modify benchmark function, with loop unrolling. */

#define MODIFYFUNC_UNROLL( ELEMENTSIZE, DATATYPE )                                  \
static SCTBoolean runModify##ELEMENTSIZE##_Unroll( DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                                   \
    unsigned int        i, j, k;                                                    \
    volatile DATATYPE   **top, *b;                                                  \
    const int           length = bytes / ELEMENTSIZE / UNROLL_COUNT;                \
                                                                                    \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                                \
    SCT_ASSERT( ( bytes / sizeof( DATATYPE ) ) % UNROLL_COUNT == 0 );               \
    SCT_ASSERT( bytes > 0 );                                                        \
    SCT_ASSERT( iterations >= 0 );                                                  \
    SCT_ASSERT( count > 0 );                                                        \
    SCT_ASSERT( buffers != NULL );                                                  \
                                                                                    \
    k = iterations;                                                                 \
    do                                                                              \
    {                                                                               \
        top = ( volatile DATATYPE** )buffers;                                       \
        j = count;                                                                  \
        do                                                                          \
        {                                                                           \
            b = ( volatile DATATYPE* )*( top++ );                                   \
            i = length;                                                             \
            do                                                                      \
            {                                                                       \
                UNROLL( *b = ( DATATYPE ) ( *b ^ FILTER_MASK ); b++; );             \
            } while( --i );                                                         \
        } while( --j );                                                             \
    } while( --k );                                                                 \
                                                                                    \
    return SCT_TRUE;                                                                \
}                                                                                   \

/* Copy benchmark function, without loop unrolling. */

#define COPYFUNC( ELEMENTSIZE, DATATYPE )                                           \
static SCTBoolean runCopy##ELEMENTSIZE( const DATATYPE ** buffers1, DATATYPE ** buffers2, const int count, const int bytes, const int iterations ) \
{                                                                                   \
    unsigned int        i, j, k;                                                    \
    const DATATYPE      **top1, *b1;                                                \
    volatile DATATYPE   **top2, *b2;                                                \
    const int           length = bytes / ELEMENTSIZE;                               \
                                                                                    \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                                \
    SCT_ASSERT( iterations >= 0 );                                                  \
    SCT_ASSERT( count > 0 );                                                        \
    SCT_ASSERT( buffers1 != NULL );                                                 \
    SCT_ASSERT( buffers2 != NULL );                                                 \
                                                                                    \
    k = iterations;                                                                 \
    do                                                                              \
    {                                                                               \
        top1 = ( const DATATYPE** )buffers1;                                        \
        top2 = ( volatile DATATYPE** )buffers2;                                     \
        j = count;                                                                  \
        do                                                                          \
        {                                                                           \
            b1 = ( const DATATYPE* )*( top1++ );                                    \
            b2 = ( volatile DATATYPE* )*( top2++ );                                 \
            i = length;                                                             \
            do                                                                      \
            {                                                                       \
                *b2++ = *b1++;                                                      \
            } while( --i );                                                         \
        } while( --j );                                                             \
    } while( --k );                                                                 \
                                                                                    \
    return SCT_TRUE;                                                                \
}                                                                                   \

/* Copy benchmark function, with loop unrolling. */

#define COPYFUNC_UNROLL( ELEMENTSIZE, DATATYPE )                                    \
static SCTBoolean runCopy##ELEMENTSIZE##_Unroll( const DATATYPE ** buffers1, DATATYPE ** buffers2, const int count, const int bytes, const int iterations ) \
{ \
    unsigned int        i, j, k;                                                    \
    const DATATYPE      **top1, *b1;                                                \
    volatile DATATYPE   **top2, *b2;                                                \
    const int           length = bytes / ELEMENTSIZE / UNROLL_COUNT;                \
                                                                                    \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                                \
    SCT_ASSERT( iterations >= 0 );                                                  \
    SCT_ASSERT( count > 0 );                                                        \
    SCT_ASSERT( buffers1 != NULL );                                                 \
    SCT_ASSERT( buffers2 != NULL );                                                 \
                                                                                    \
    k = iterations;                                                                 \
    do                                                                              \
    {                                                                               \
        top1 = ( const DATATYPE** )buffers1;                                        \
        top2 = ( volatile DATATYPE** )buffers2;                                     \
        j = count;                                                                  \
        do                                                                          \
        {                                                                           \
            b1 = ( const DATATYPE* )*( top1++ );                                    \
            b2 = ( volatile DATATYPE* )*( top2++ );                                 \
            i = length;                                                             \
            do                                                                      \
            {                                                                       \
                UNROLL( *b2++ = *b1++ );                                            \
            } while( --i );                                                         \
        } while( --j );                                                             \
    } while( --k );                                                                 \
                                                                                    \
    return SCT_TRUE;                                                                \
}                                                                                   \

/* Blend benchmark function, without loop unrolling. */

#define BLENDFUNC( ELEMENTSIZE, DATATYPE )                                          \
static SCTBoolean runBlend##ELEMENTSIZE( DATATYPE ** buffers1, const DATATYPE ** buffers2, const int count, const int bytes, const int iterations ) \
{ \
    unsigned int        i, j, k;                                                    \
    volatile DATATYPE   **top1, *b1;                                                \
    const DATATYPE      **top2, *b2;                                                \
    const int           length = bytes / ELEMENTSIZE;                               \
                                                                                    \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                                \
    SCT_ASSERT( iterations >= 0 );                                                  \
    SCT_ASSERT( count > 0 );                                                        \
    SCT_ASSERT( buffers1 != NULL );                                                 \
    SCT_ASSERT( buffers2 != NULL );                                                 \
                                                                                    \
    k = iterations;                                                                 \
    do                                                                              \
    {                                                                               \
        top1 = ( volatile DATATYPE** )buffers1;                                     \
        top2 = ( const DATATYPE** )buffers2;                                        \
        j = count;                                                                  \
        do                                                                          \
        {                                                                           \
            b1 = ( volatile DATATYPE* )*( top1++ );                                 \
            b2 = ( const DATATYPE* )*( top2++ );                                    \
            i = length;                                                             \
            do                                                                      \
            {                                                                       \
                *b1 = ( DATATYPE )( ( *b1 + *b2 ) / 2 ); b1++, b2++;                \
            } while( --i );                                                         \
        } while( --j );                                                             \
    } while( --k );                                                                 \
                                                                                    \
    return SCT_TRUE;                                                                \
}                                                                                   \

/* Blend benchmark function, with loop unrolling. */

#define BLENDFUNC_UNROLL( ELEMENTSIZE, DATATYPE )                                   \
static SCTBoolean runBlend##ELEMENTSIZE##_Unroll( DATATYPE ** buffers1, const DATATYPE ** buffers2, const int count, const int bytes, const int iterations ) \
{                                                                                   \
    unsigned int        i, j, k;                                                    \
    volatile DATATYPE   **top1, *b1;                                                \
    const DATATYPE      **top2, *b2;                                                \
    const int           length = bytes / ELEMENTSIZE / UNROLL_COUNT;                \
                                                                                    \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                                \
    SCT_ASSERT( iterations >= 0 );                                                  \
    SCT_ASSERT( count > 0 );                                                        \
    SCT_ASSERT( buffers1 != NULL );                                                 \
    SCT_ASSERT( buffers2 != NULL );                                                 \
                                                                                    \
    k = iterations;                                                                 \
    do                                                                              \
    {                                                                               \
        top1 = ( volatile DATATYPE** )buffers1;                                     \
        top2 = ( const DATATYPE** )buffers2;                                        \
        j = count;                                                                  \
        do                                                                          \
        {                                                                           \
            b1 = ( volatile DATATYPE* )*( top1++ );                                 \
            b2 = ( const DATATYPE* )*( top2++ );                                    \
            i = length;                                                             \
            do                                                                      \
            {                                                                       \
                UNROLL( ( *b1 = ( DATATYPE )( ( *b1 + *b2 ) / 2 ) ); b1++; b2++; ); \
            } while( --i );                                                         \
        } while( --j );                                                             \
    } while( --k );                                                                 \
                                                                                    \
    return SCT_TRUE;                                                                \
}                                                                                   \


/* Instantiate above macros to create memory benchmark functions */
/* Note: you should not use 0 as an element size as it will cause problems */

READFUNC( 1, unsigned char )
READFUNC( 2, unsigned short )
READFUNC( 4, unsigned int )

READFUNC_UNROLL( 1,unsigned char )
READFUNC_UNROLL( 2,unsigned short )
READFUNC_UNROLL( 4,unsigned int )

FILLZFUNC( 1,unsigned char )
FILLZFUNC( 2,unsigned short )
FILLZFUNC( 4,unsigned int )

FILLZFUNC_UNROLL( 1,unsigned char )
FILLZFUNC_UNROLL( 2,unsigned short )
FILLZFUNC_UNROLL( 4,unsigned int )

MODIFYFUNC( 1,unsigned char )
MODIFYFUNC( 2,unsigned short )
MODIFYFUNC( 4,unsigned int )

MODIFYFUNC_UNROLL( 1,unsigned char )
MODIFYFUNC_UNROLL( 2,unsigned short )
MODIFYFUNC_UNROLL( 4,unsigned int )

COPYFUNC( 1, unsigned char )
COPYFUNC( 2, unsigned short )
COPYFUNC( 4, unsigned int )

COPYFUNC_UNROLL( 1, unsigned char )
COPYFUNC_UNROLL( 2, unsigned short )
COPYFUNC_UNROLL( 4, unsigned int )

BLENDFUNC( 1, unsigned char )
BLENDFUNC( 2, unsigned short )
BLENDFUNC( 4, unsigned int )

BLENDFUNC_UNROLL( 1, unsigned char )
BLENDFUNC_UNROLL( 2, unsigned short )
BLENDFUNC_UNROLL( 4, unsigned int )

/*!
 * Run read memory benchmark function. Wrapper to call correct
 * benchmark functions instantiated by above macros.
 *
 * \param buffers Memory buffers to read.
 * \param count Memory buffers count.
 * \param bytes Length of the memory buffer, in bytes.
 * \param elementSize Size of the element in memory buffer. 1, 2, or 4 are accepted.
 * \param unroll Use unrolled loop or not.
 * \param iterations Number of times to repeat the memory benchmark.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean runRead( unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations )
{
    SCT_ASSERT( elementSize == 1 || elementSize == 2 || elementSize == 4 );
    SCT_ASSERT( unroll == SCT_FALSE || ( bytes / elementSize ) % UNROLL_COUNT == 0 );

    switch( elementSize )
    {
    case 1:
        return unroll ? runRead1_Unroll( buffers, count, bytes, iterations ) : runRead1( buffers, count, bytes, iterations );

    case 2:
        return unroll ? runRead2_Unroll( ( unsigned short** )buffers, count, bytes, iterations ) : runRead2( ( unsigned short** )buffers, count, bytes, iterations );

    case 4:
    default:
        return unroll ? runRead4_Unroll( ( unsigned int** )buffers, count, bytes, iterations ) : runRead4( ( unsigned int** )buffers, count, bytes, iterations );
    }
}

/*!
 * Run fill zero memory benchmark function. Wrapper to call correct
 * benchmark functions instantiated by above macros.
 *
 * \param buffers Memory buffers to zero fill.
 * \param count Memory buffers count.
 * \param bytes Length of the memory buffer, in bytes.
 * \param elementSize Size of the element in memory buffer. 1, 2, or 4 are accepted.
 * \param unroll Use unrolled loop or not.
 * \param iterations Number of times to repeat the memory benchmark.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean runFillZ( unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations )
{
    SCT_ASSERT( elementSize == 1 || elementSize == 2 || elementSize == 4 );
    SCT_ASSERT( unroll == SCT_FALSE || ( bytes / elementSize ) % UNROLL_COUNT == 0 );

    switch( elementSize )
    {
    case 1:
        return unroll ? runFillZ1_Unroll( buffers, count, bytes, iterations ) : runFillZ1( buffers, count, bytes, iterations );

    case 2:
        return unroll ? runFillZ2_Unroll( ( unsigned short** )buffers, count, bytes, iterations ) : runFillZ2( ( unsigned short** )buffers, count, bytes, iterations );

    case 4:
    default:
        return unroll ? runFillZ4_Unroll( ( unsigned int** )buffers, count, bytes, iterations ) : runFillZ4( ( unsigned int** )buffers, count, bytes, iterations );
    }
}

/*!
 * Run modify memory benchmark function. Wrapper to call correct
 * benchmark functions instantiated by above macros.
 *
 * \param buffers Memory buffers to modify.
 * \param count Memory buffers count.
 * \param bytes Length of the memory buffer, in bytes.
 * \param elementSize Size of the element in memory buffer. 1, 2, or 4 are accepted.
 * \param unroll Use unrolled loop or not.
 * \param iterations Number of times to repeat the memory benchmark.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean runModify( unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations )
{
    SCT_ASSERT( elementSize == 1 || elementSize == 2 || elementSize == 4 );
    SCT_ASSERT( unroll == SCT_FALSE || ( bytes / elementSize ) % UNROLL_COUNT == 0 );

    switch( elementSize )
    {
    case 1:
        return unroll ? runModify1_Unroll( buffers, count, bytes, iterations ) : runModify1( buffers, count, bytes, iterations );

    case 2:
        return unroll ? runModify2_Unroll( ( unsigned short** )buffers, count, bytes, iterations ) : runModify2( ( unsigned short** )buffers, count, bytes, iterations );

    case 4:
    default:
        return unroll ? runModify4_Unroll( ( unsigned int** )buffers, count, bytes, iterations ) : runModify4( ( unsigned int** )buffers, count, bytes, iterations );
    }
}

/*!
 * Run copy memory benchmark function. Wrapper to call correct
 * benchmark functions instantiated by above macros.
 *
 * \param buffer1 Memory buffers to use as copy source.
 * \param buffer2 Memory buffers to use as copy destination.
 * \param count Memory buffers count.
 * \param bytes Length of the memory buffers, in bytes.
 * \param elementSize Size of the element in memory buffer. 1, 2, or 4 are accepted.
 * \param unroll Use unrolled loop or not.
 * \param iterations Number of times to repeat the memory benchmark.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean runCopy( const unsigned char** buffers1, unsigned char** buffers2, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations )
{
    SCT_ASSERT( elementSize == 1 || elementSize == 2 || elementSize == 4 );
    SCT_ASSERT( unroll == SCT_FALSE || ( bytes / elementSize ) % UNROLL_COUNT == 0 );

    switch( elementSize )
    {
    case 1:
        return unroll ? runCopy1_Unroll( buffers1, buffers2, count, bytes, iterations ) : runCopy1( buffers1, buffers2, count, bytes, iterations );

    case 2:
        return unroll ? runCopy2_Unroll( ( const unsigned short** )buffers1, ( unsigned short** )buffers2, count, bytes, iterations ) : runCopy2( ( const unsigned short** )buffers1, ( unsigned short** )buffers2, count, bytes, iterations );

    case 4:
    default:
        return unroll ? runCopy4_Unroll( ( const unsigned int** )buffers1, ( unsigned int** )buffers2, count, bytes, iterations ) : runCopy4( ( const unsigned int** )buffers1, ( unsigned int** )buffers2, count, bytes, iterations );
    }
}

/*!
 * Run blend memory benchmark function. Wrapper to call correct
 * benchmark functions instantiated by above macros.
 *
 * \param buffer1 Memory buffers to use as blend source.
 * \param buffer2 Memory buffers to use as blend source and destination.
 * \param count Memory buffers count.
 * \param bytes Length of the memory buffers, in bytes.
 * \param elementSize Size of the element in memory buffer. 1, 2, or 4 are accepted.
 * \param unroll Use unrolled loop or not.
 * \param iterations Number of times to repeat the memory benchmark.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean runBlend( unsigned char** buffers1, const unsigned char** buffers2, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations )
{
    SCT_ASSERT( elementSize == 1 || elementSize == 2 || elementSize == 4 );
    SCT_ASSERT( unroll == SCT_FALSE || ( bytes / elementSize ) % UNROLL_COUNT == 0 );

    switch( elementSize )
    {
    case 1:
        return unroll ? runBlend1_Unroll( buffers1, buffers2, count, bytes, iterations ) : runBlend1( buffers1, buffers2, count, bytes, iterations );

    case 2:
        return unroll ? runBlend2_Unroll( ( unsigned short** )buffers1, ( const unsigned short** )buffers2, count, bytes, iterations ) : runBlend2( ( unsigned short** )buffers1, ( const unsigned short** )buffers2, count, bytes, iterations );

    case 4:
    default:
        return unroll ? runBlend4_Unroll( ( unsigned int** )buffers1, ( const unsigned int** )buffers2, count, bytes, iterations ) : runBlend4( ( unsigned int** )buffers1, ( const unsigned int** )buffers2, count, bytes, iterations );
    }
}

// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
// ----------------------------------------------------------------------


/* Sweep read function, without loop unrolling. */

#define SWEEPREADFUNC( ELEMENTSIZE, DATATYPE )                                  \
static SCTBoolean runSweepRead##ELEMENTSIZE( int interval, DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                               \
    unsigned int        i, j, k;                                                \
    volatile DATATYPE   **top, *b;                                              \
    const int           length = bytes / ELEMENTSIZE / interval;                \
    int                 accum = 0;                                              \
    volatile DATATYPE   *s;                                                     \
                                                                                \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                            \
    SCT_ASSERT( bytes > 0 );                                                    \
    SCT_ASSERT( bytes % ( ELEMENTSIZE * interval ) == 0 );                      \
    SCT_ASSERT( iterations >= 0 );                                              \
    SCT_ASSERT( count > 0 );                                                    \
    SCT_ASSERT( buffers != NULL );                                              \
                                                                                \
    s = ( volatile DATATYPE* )*buffers;                                         \
                                                                                \
    k = iterations;                                                             \
    do                                                                          \
    {                                                                           \
        top = ( volatile DATATYPE** )buffers;                                   \
        j = count;                                                              \
        do                                                                      \
        {                                                                       \
            b = ( volatile DATATYPE* )*( top++ );                               \
            i = length;                                                         \
            do                                                                  \
            {                                                                   \
                accum += *b;                                                    \
                b += interval;                                                  \
            } while( --i );                                                     \
        } while( --j );                                                         \
    } while( --k );                                                             \
                                                                                \
    *s = ( DATATYPE )( accum );                                                 \
                                                                                \
    return SCT_TRUE;                                                            \
}                                                                               \

/* Read benchmark function, with loop unrolling. */

#define SWEEPREADFUNC_UNROLL( ELEMENTSIZE, DATATYPE )                           \
static SCTBoolean runSweepRead##ELEMENTSIZE##_Unroll( int interval, DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                               \
    unsigned int        i, j, k;                                                \
    volatile DATATYPE   **top, *b;                                              \
    const int           length = bytes / ELEMENTSIZE / interval / UNROLL_COUNT; \
    int                 accum = 0;                                              \
    volatile DATATYPE   *s;                                                     \
                                                                                \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                            \
    SCT_ASSERT( bytes > 0 );                                                    \
    SCT_ASSERT( bytes % ( ELEMENTSIZE * interval ) == 0 );                      \
    SCT_ASSERT( ( bytes / ( ELEMENTSIZE * interval ) ) % UNROLL_COUNT == 0 );   \
    SCT_ASSERT( iterations >= 0 );                                              \
    SCT_ASSERT( count > 0 );                                                    \
    SCT_ASSERT( buffers != NULL );                                              \
                                                                                \
    s = ( volatile DATATYPE* )*buffers;                                         \
                                                                                \
    k = iterations;                                                             \
    do                                                                          \
    {                                                                           \
        top = ( volatile DATATYPE** )buffers;                                   \
        j = count;                                                              \
        do                                                                      \
        {                                                                       \
            b = ( volatile DATATYPE* )*( top++ );                               \
            i = length;                                                         \
            do                                                                  \
            {                                                                   \
                UNROLL( accum += *b; b += interval );                           \
            } while( --i );                                                     \
        } while( --j );                                                         \
    } while( --k );                                                             \
                                                                                \
    *s = ( DATATYPE )( accum );                                                 \
                                                                                \
    return SCT_TRUE;                                                            \
}                                                                               \

/* Sweep fillz benchmark function, without loop unrolling. */

#define SWEEPFILLZFUNC( ELEMENTSIZE, DATATYPE )                                 \
static SCTBoolean runSweepFillZ##ELEMENTSIZE( int interval, DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                               \
    unsigned int        i, j, k;                                                \
    volatile DATATYPE   **top, *b;                                              \
    const int           length = bytes / ELEMENTSIZE / interval;                \
                                                                                \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                            \
    SCT_ASSERT( bytes > 0 );                                                    \
    SCT_ASSERT( bytes % ( ELEMENTSIZE * interval ) == 0 );                      \
    SCT_ASSERT( iterations >= 0 );                                              \
    SCT_ASSERT( count > 0 );                                                    \
    SCT_ASSERT( buffers != NULL );                                              \
                                                                                \
    k = iterations;                                                             \
    do                                                                          \
    {                                                                           \
        top = ( volatile DATATYPE** )buffers;                                   \
        j = count;                                                              \
        do                                                                      \
        {                                                                       \
            b = ( volatile DATATYPE* )*( top++ );                               \
            i = length;                                                         \
            do                                                                  \
            {                                                                   \
                *b = 0;                                                         \
                b += interval;                                                  \
            } while( --i );                                                     \
        } while( --j );                                                         \
    } while( --k );                                                             \
                                                                                \
    return SCT_TRUE;                                                            \
}                                                                               \

/* Sweep fillz benchmark function, with loop unrolling. */

#define SWEEPFILLZFUNC_UNROLL( ELEMENTSIZE, DATATYPE )                          \
static SCTBoolean runSweepFillZ##ELEMENTSIZE##_Unroll( int interval, DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                               \
    unsigned int        i, j, k;                                                \
    volatile DATATYPE   **top, *b;                                              \
    const int           length = bytes / ELEMENTSIZE / interval / UNROLL_COUNT; \
                                                                                \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                            \
    SCT_ASSERT( bytes > 0 );                                                    \
    SCT_ASSERT( bytes % ( ELEMENTSIZE * interval ) == 0 );                      \
    SCT_ASSERT( ( bytes / ( ELEMENTSIZE * interval ) ) % UNROLL_COUNT == 0 );   \
    SCT_ASSERT( iterations >= 0 );                                              \
    SCT_ASSERT( count > 0 );                                                    \
    SCT_ASSERT( buffers != NULL );                                              \
                                                                                \
    k = iterations;                                                             \
    do                                                                          \
    {                                                                           \
        top = ( volatile DATATYPE** )buffers;                                   \
        j = count;                                                              \
        do                                                                      \
        {                                                                       \
            b = ( volatile DATATYPE* )*( top++ );                               \
            i = length;                                                         \
            do                                                                  \
            {                                                                   \
                UNROLL( *b = 0; b += interval );                                \
            } while( --i );                                                     \
        } while( --j );                                                         \
    } while( --k );                                                             \
                                                                                \
    return SCT_TRUE;                                                            \
}                                                                               \

/* Sweep modify benchmark function, without loop unrolling. */

#define SWEEPMODIFYFUNC( ELEMENTSIZE, DATATYPE )                                \
static SCTBoolean runSweepModify##ELEMENTSIZE( int interval, DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                               \
    unsigned int        i, j, k;                                                \
    volatile DATATYPE   **top, *b;                                              \
    const int           length = bytes / ELEMENTSIZE / interval;                \
                                                                                \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                            \
    SCT_ASSERT( bytes > 0 );                                                    \
    SCT_ASSERT( bytes % ( ELEMENTSIZE * interval ) == 0 );                      \
    SCT_ASSERT( iterations >= 0 );                                              \
    SCT_ASSERT( count > 0 );                                                    \
    SCT_ASSERT( buffers != NULL );                                              \
                                                                                \
    k = iterations;                                                             \
    do                                                                          \
    {                                                                           \
        top = ( volatile DATATYPE** )buffers;                                   \
        j = count;                                                              \
        do                                                                      \
        {                                                                       \
            b = ( volatile DATATYPE* )*( top++ );                               \
            i = length;                                                         \
            do                                                                  \
            {                                                                   \
                *b = ( DATATYPE ) ( *b ^ FILTER_MASK );                         \
                b += interval;                                                  \
            } while( --i );                                                     \
        } while( --j );                                                         \
    } while( --k );                                                             \
                                                                                \
    return SCT_TRUE;                                                            \
}                                                                               \


/* Sweep filter benchmark function, with loop unrolling. */

#define SWEEPMODIFYFUNC_UNROLL( ELEMENTSIZE, DATATYPE )                         \
static SCTBoolean runSweepModify##ELEMENTSIZE##_Unroll( int interval, DATATYPE ** buffers, const int count, const int bytes, const int iterations ) \
{                                                                               \
    unsigned int        i, j, k;                                                \
    volatile DATATYPE   **top, *b;                                              \
    const int           length = bytes / ELEMENTSIZE / interval / UNROLL_COUNT; \
                                                                                \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                            \
    SCT_ASSERT( bytes > 0 );                                                    \
    SCT_ASSERT( bytes % ( ELEMENTSIZE * interval ) == 0 );                      \
    SCT_ASSERT( ( bytes / ( ELEMENTSIZE * interval ) ) % UNROLL_COUNT == 0 );   \
    SCT_ASSERT( iterations >= 0 );                                              \
    SCT_ASSERT( count > 0 );                                                    \
    SCT_ASSERT( buffers != NULL );                                              \
                                                                                \
    k = iterations;                                                             \
    do                                                                          \
    {                                                                           \
        top = ( volatile DATATYPE** )buffers;                                   \
        j = count;                                                              \
        do                                                                      \
        {                                                                       \
            b = ( volatile DATATYPE* )*( top++ );                               \
            i = length;                                                         \
            do                                                                  \
            {                                                                   \
                UNROLL( *b = ( DATATYPE ) ( *b ^ FILTER_MASK ); b+= interval ); \
            } while( --i );                                                     \
        } while( --j );                                                         \
    } while( --k );                                                             \
                                                                                \
    return SCT_TRUE;                                                            \
}                                                                               \


/* Sweep copy benchmark function, without loop unrolling. */

#define SWEEPCOPYFUNC( ELEMENTSIZE, DATATYPE )                                  \
static SCTBoolean runSweepCopy##ELEMENTSIZE( int interval, const DATATYPE ** buffers1, DATATYPE ** buffers2, const int count, const int bytes, const int iterations ) \
{                                                                               \
    unsigned int        i, j, k;                                                \
    const DATATYPE      **top1, *b1;                                            \
    volatile DATATYPE   **top2, *b2;                                            \
    const int           length = bytes / ELEMENTSIZE / interval;                \
                                                                                \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                            \
    SCT_ASSERT( bytes > 0 );                                                    \
    SCT_ASSERT( bytes % ( ELEMENTSIZE * interval ) == 0 );                      \
    SCT_ASSERT( iterations >= 0 );                                              \
    SCT_ASSERT( count > 0 );                                                    \
    SCT_ASSERT( buffers1 != NULL );                                             \
    SCT_ASSERT( buffers2 != NULL );                                             \
                                                                                \
    k = iterations;                                                             \
    do                                                                          \
    {                                                                           \
        top1 = ( const DATATYPE** )buffers1;                                    \
        top2 = ( volatile DATATYPE** )buffers2;                                 \
        j = count;                                                              \
        do                                                                      \
        {                                                                       \
            b1 = ( const DATATYPE* )*( top1++ );                                \
            b2 = ( volatile DATATYPE* )*( top2++ );                             \
            i = length;                                                         \
            do                                                                  \
            {                                                                   \
                *b2 = *b1;                                                      \
                b1 += interval;                                                 \
                b2 += interval;                                                 \
            } while( --i );                                                     \
        } while( --j );                                                         \
    } while( --k );                                                             \
                                                                                \
    return SCT_TRUE;                                                            \
}                                                                               \


/* Sweep copy benchmark function, with loop unrolling. */

#define SWEEPCOPYFUNC_UNROLL( ELEMENTSIZE, DATATYPE )                           \
static SCTBoolean runSweepCopy##ELEMENTSIZE##_Unroll( int interval, const DATATYPE ** buffers1, DATATYPE ** buffers2, const int count, const int bytes, const int iterations ) \
{ \
    unsigned int        i, j, k;                                                \
    const DATATYPE      **top1, *b1;                                            \
    volatile DATATYPE   **top2, *b2;                                            \
    const int           length = bytes / ELEMENTSIZE / interval / UNROLL_COUNT; \
                                                                                \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                            \
    SCT_ASSERT( bytes % ( ELEMENTSIZE * interval ) == 0 );                      \
    SCT_ASSERT( ( bytes / ( ELEMENTSIZE * interval ) ) % UNROLL_COUNT == 0 );   \
    SCT_ASSERT( iterations >= 0 );                                              \
    SCT_ASSERT( count > 0 );                                                    \
    SCT_ASSERT( buffers1 != NULL );                                             \
    SCT_ASSERT( buffers2 != NULL );                                             \
                                                                                \
    k = iterations;                                                             \
    do                                                                          \
    {                                                                           \
        top1 = ( const DATATYPE** )buffers1;                                    \
        top2 = ( volatile DATATYPE** )buffers2;                                 \
        j = count;                                                              \
        do                                                                      \
        {                                                                       \
            b1 = ( const DATATYPE* )*( top1++ );                                \
            b2 = ( volatile DATATYPE* )*( top2++ );                             \
            i = length;                                                         \
            do                                                                  \
            {                                                                   \
                UNROLL( *b2 = *b1; b1 += interval; b2 += interval );            \
            } while( --i );                                                     \
        } while( --j );                                                         \
    } while( --k );                                                             \
                                                                                \
    return SCT_TRUE;                                                            \
}                                                                               \

/* Sweep blend benchmark function, without loop unrolling. */

#define SWEEPBLENDFUNC( ELEMENTSIZE, DATATYPE )                                      \
static SCTBoolean runSweepBlend##ELEMENTSIZE( int interval, DATATYPE ** buffers1, const DATATYPE ** buffers2, const int count, const int bytes, const int iterations ) \
{ \
    unsigned int        i, j, k;                                                \
    volatile DATATYPE   **top1, *b1;                                            \
    const DATATYPE      **top2, *b2;                                            \
    const int           length = bytes / ELEMENTSIZE / interval;                \
                                                                                \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                            \
    SCT_ASSERT( bytes > 0 );                                                    \
    SCT_ASSERT( bytes % ( ELEMENTSIZE * interval ) == 0 );                      \
    SCT_ASSERT( iterations >= 0 );                                              \
    SCT_ASSERT( count > 0 );                                                    \
    SCT_ASSERT( buffers1 != NULL );                                             \
    SCT_ASSERT( buffers2 != NULL );                                             \
                                                                                \
    k = iterations;                                                             \
    do                                                                          \
    {                                                                           \
        top1 = ( volatile DATATYPE** )buffers1;                                 \
        top2 = ( const DATATYPE** )buffers2;                                    \
        j = count;                                                              \
        do                                                                      \
        {                                                                       \
            b1 = ( volatile DATATYPE* )*( top1++ );                             \
            b2 = ( const DATATYPE* )*( top2++ );                                \
            i = length;                                                         \
            do                                                                  \
            {                                                                   \
                *b1 = ( DATATYPE )( ( *b1 + *b2 ) / 2 );                        \
                b1 += interval;                                                 \
                b2 += interval;                                                 \
            } while( --i );                                                     \
        } while( --j );                                                         \
    } while( --k );                                                             \
                                                                                \
    return SCT_TRUE;                                                            \
}                                                                               \

/* Sweep blend benchmark function, with loop unrolling. */

#define SWEEPBLENDFUNC_UNROLL( ELEMENTSIZE, DATATYPE )                          \
static SCTBoolean runSweepBlend##ELEMENTSIZE##_Unroll( int interval, DATATYPE ** buffers1, const DATATYPE ** buffers2, const int count, const int bytes, const int iterations ) \
{                                                                               \
    unsigned int        i, j, k;                                                \
    volatile DATATYPE   **top1, *b1;                                            \
    const DATATYPE      **top2, *b2;                                            \
    const int           length = bytes / ELEMENTSIZE / interval / UNROLL_COUNT; \
                                                                                \
    SCT_ASSERT( sizeof( DATATYPE ) == ELEMENTSIZE );                            \
    SCT_ASSERT( bytes % ( ELEMENTSIZE * interval ) == 0 );                      \
    SCT_ASSERT( ( bytes / ( ELEMENTSIZE * interval ) ) % UNROLL_COUNT == 0 );   \
    SCT_ASSERT( iterations >= 0 );                                              \
    SCT_ASSERT( count > 0 );                                                    \
    SCT_ASSERT( buffers1 != NULL );                                             \
    SCT_ASSERT( buffers2 != NULL );                                             \
                                                                                \
    k = iterations;                                                             \
    do                                                                          \
    {                                                                           \
        top1 = ( volatile DATATYPE** )buffers1;                                 \
        top2 = ( const DATATYPE** )buffers2;                                    \
        j = count;                                                              \
        do                                                                      \
        {                                                                       \
            b1 = ( volatile DATATYPE* )*( top1++ );                             \
            b2 = ( const DATATYPE* )*( top2++ );                                \
            i = length;                                                         \
            do                                                                  \
            {                                                                   \
                UNROLL( *b1 = ( DATATYPE )( ( *b1 + *b2 ) / 2 ); b1 += interval; b2 += interval ); \
            } while( --i );                                                     \
        } while( --j );                                                         \
    } while( --k );                                                             \
                                                                                \
    return SCT_TRUE;                                                            \
}                                                                               \

/* Instantiate above macros to create memory benchmark functions */
/* Note: you should not use 0 as an element size as it will cause problems */

SWEEPREADFUNC( 1, unsigned char )
SWEEPREADFUNC( 2, unsigned short )
SWEEPREADFUNC( 4, unsigned int )

SWEEPREADFUNC_UNROLL( 1,unsigned char )
SWEEPREADFUNC_UNROLL( 2,unsigned short )
SWEEPREADFUNC_UNROLL( 4,unsigned int )

SWEEPFILLZFUNC( 1,unsigned char )
SWEEPFILLZFUNC( 2,unsigned short )
SWEEPFILLZFUNC( 4,unsigned int )

SWEEPFILLZFUNC_UNROLL( 1,unsigned char )
SWEEPFILLZFUNC_UNROLL( 2,unsigned short )
SWEEPFILLZFUNC_UNROLL( 4,unsigned int )

SWEEPMODIFYFUNC( 1,unsigned char )
SWEEPMODIFYFUNC( 2,unsigned short )
SWEEPMODIFYFUNC( 4,unsigned int )

SWEEPMODIFYFUNC_UNROLL( 1,unsigned char )
SWEEPMODIFYFUNC_UNROLL( 2,unsigned short )
SWEEPMODIFYFUNC_UNROLL( 4,unsigned int )

SWEEPCOPYFUNC( 1, unsigned char )
SWEEPCOPYFUNC( 2, unsigned short )
SWEEPCOPYFUNC( 4, unsigned int )

SWEEPCOPYFUNC_UNROLL( 1, unsigned char )
SWEEPCOPYFUNC_UNROLL( 2, unsigned short )
SWEEPCOPYFUNC_UNROLL( 4, unsigned int )

SWEEPBLENDFUNC( 1, unsigned char )
SWEEPBLENDFUNC( 2, unsigned short )
SWEEPBLENDFUNC( 4, unsigned int )

SWEEPBLENDFUNC_UNROLL( 1, unsigned char )
SWEEPBLENDFUNC_UNROLL( 2, unsigned short )
SWEEPBLENDFUNC_UNROLL( 4, unsigned int )

/*!
 * Run sweep read memory benchmark function. Wrapper to call correct
 * benchmark functions instantiated by above macros.
 *
 * \param interval sweep interval, > 0.
 * \param buffers Memory buffers to read.
 * \param count Memory buffers count.
 * \param bytes Length of the memory buffer, in bytes.
 * \param elementSize Size of the element in memory buffer. 1, 2, or 4 are accepted.
 * \param unroll Use unrolled loop or not.
 * \param iterations Number of times to repeat the memory benchmark.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean runSweepRead( int interval, unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations )
{
    SCT_ASSERT( interval > 0 );
    SCT_ASSERT( elementSize == 1 || elementSize == 2 || elementSize == 4 );
    SCT_ASSERT( unroll == SCT_TRUE || ( bytes % ( elementSize / iterations ) == 0 ) );    
    SCT_ASSERT( unroll == SCT_FALSE || ( bytes / elementSize / iterations ) % UNROLL_COUNT == 0 );

    switch( elementSize )
    {
    case 1:
        return unroll ? runSweepRead1_Unroll( interval, buffers, count, bytes, iterations ) : runSweepRead1( interval, buffers, count, bytes, iterations );

    case 2:
        return unroll ? runSweepRead2_Unroll( interval, ( unsigned short** )buffers, count, bytes, iterations ) : runSweepRead2( interval, ( unsigned short** )buffers, count, bytes, iterations );

    case 4:
    default:
        return unroll ? runSweepRead4_Unroll( interval, ( unsigned int** )buffers, count, bytes, iterations ) : runSweepRead4( interval, ( unsigned int** )buffers, count, bytes, iterations );
    }
}

/*!
 * Run sweep fill zero memory benchmark function. Wrapper to call correct
 * benchmark functions instantiated by above macros.
 *
 * \param interval sweep interval, > 0. 
 * \param buffers Memory buffers to zero fill.
 * \param count Memory buffers count.
 * \param bytes Length of the memory buffer, in bytes.
 * \param elementSize Size of the element in memory buffer. 1, 2, or 4 are accepted.
 * \param unroll Use unrolled loop or not.
 * \param iterations Number of times to repeat the memory benchmark.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean runSweepFillZ( int interval, unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations )
{
    SCT_ASSERT( interval > 0 );
    SCT_ASSERT( elementSize == 1 || elementSize == 2 || elementSize == 4 );
    SCT_ASSERT( unroll == SCT_TRUE || ( bytes % ( elementSize / iterations ) == 0 ) );    
    SCT_ASSERT( unroll == SCT_FALSE || ( bytes / elementSize / iterations ) % UNROLL_COUNT == 0 );

    switch( elementSize )
    {
    case 1:
        return unroll ? runSweepFillZ1_Unroll( interval, buffers, count, bytes, iterations ) : runSweepFillZ1( interval, buffers, count, bytes, iterations );

    case 2:
        return unroll ? runSweepFillZ2_Unroll( interval, ( unsigned short** )buffers, count, bytes, iterations ) : runSweepFillZ2( interval, ( unsigned short** )buffers, count, bytes, iterations );

    case 4:
    default:
        return unroll ? runSweepFillZ4_Unroll( interval, ( unsigned int** )buffers, count, bytes, iterations ) : runSweepFillZ4( interval, ( unsigned int** )buffers, count, bytes, iterations );
    }
}

/*!
 * Run sweep modify memory benchmark function. Wrapper to call correct
 * benchmark functions instantiated by above macros.
 *
 * \param interval sweep interval, > 0. 
 * \param buffers Memory buffers to modify.
 * \param count Memory buffers count.
 * \param bytes Length of the memory buffer, in bytes.
 * \param elementSize Size of the element in memory buffer. 1, 2, or 4 are accepted.
 * \param unroll Use unrolled loop or not.
 * \param iterations Number of times to repeat the memory benchmark.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean runSweepModify( int interval, unsigned char** buffers, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations )
{
    SCT_ASSERT( interval > 0 );
    SCT_ASSERT( elementSize == 1 || elementSize == 2 || elementSize == 4 );
    SCT_ASSERT( unroll == SCT_TRUE || ( bytes % ( elementSize / iterations ) == 0 ) );    
    SCT_ASSERT( unroll == SCT_FALSE || ( bytes / elementSize / iterations ) % UNROLL_COUNT == 0 );

    switch( elementSize )
    {
    case 1:
        return unroll ? runSweepModify1_Unroll( interval, buffers, count, bytes, iterations ) : runSweepModify1( interval, buffers, count, bytes, iterations );

    case 2:
        return unroll ? runSweepModify2_Unroll( interval, ( unsigned short** )buffers, count, bytes, iterations ) : runSweepModify2( interval, ( unsigned short** )buffers, count, bytes, iterations );

    case 4:
    default:
        return unroll ? runSweepModify4_Unroll( interval, ( unsigned int** )buffers, count, bytes, iterations ) : runSweepModify4( interval, ( unsigned int** )buffers, count, bytes, iterations );
    }
}

/*!
 * Run sweep copy memory benchmark function. Wrapper to call correct
 * benchmark functions instantiated by above macros.
 *
 * \param interval sweep interval, > 0.  
 * \param buffer1 Memory buffers to use as copy source.
 * \param buffer2 Memory buffers to use as copy destination.
 * \param count Memory buffers count.
 * \param bytes Length of the memory buffers, in bytes.
 * \param elementSize Size of the element in memory buffer. 1, 2, or 4 are accepted.
 * \param unroll Use unrolled loop or not.
 * \param iterations Number of times to repeat the memory benchmark.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean runSweepCopy( int interval, const unsigned char** buffers1, unsigned char** buffers2, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations )
{
    SCT_ASSERT( elementSize == 1 || elementSize == 2 || elementSize == 4 );
    SCT_ASSERT( unroll == SCT_TRUE || ( bytes % ( elementSize / iterations ) == 0 ) );    
    SCT_ASSERT( unroll == SCT_FALSE || ( bytes / elementSize / iterations ) % UNROLL_COUNT == 0 );

    switch( elementSize )
    {
    case 1:
        return unroll ? runSweepCopy1_Unroll( interval, buffers1, buffers2, count, bytes, iterations ) : runSweepCopy1( interval, buffers1, buffers2, count, bytes, iterations );

    case 2:
        return unroll ? runSweepCopy2_Unroll( interval, ( const unsigned short** )buffers1, ( unsigned short** )buffers2, count, bytes, iterations ) : runSweepCopy2( interval, ( const unsigned short** )buffers1, ( unsigned short** )buffers2, count, bytes, iterations );

    case 4:
    default:
        return unroll ? runSweepCopy4_Unroll( interval, ( const unsigned int** )buffers1, ( unsigned int** )buffers2, count, bytes, iterations ) : runSweepCopy4( interval, ( const unsigned int** )buffers1, ( unsigned int** )buffers2, count, bytes, iterations );
    }
}

/*!
 * Run sweep blend memory benchmark function. Wrapper to call correct
 * benchmark functions instantiated by above macros.
 *
  * \param interval sweep interval, > 0.  
 * \param buffer1 Memory buffers to use as blend source.
 * \param buffer2 Memory buffers to use as blend source and destination.
 * \param count Memory buffers count.
 * \param bytes Length of the memory buffers, in bytes.
 * \param elementSize Size of the element in memory buffer. 1, 2, or 4 are accepted.
 * \param unroll Use unrolled loop or not.
 * \param iterations Number of times to repeat the memory benchmark.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean runSweepBlend( int interval, unsigned char** buffers1, const unsigned char** buffers2, const int count, const int bytes, int elementSize, SCTBoolean unroll, const int iterations )
{
    SCT_ASSERT( elementSize == 1 || elementSize == 2 || elementSize == 4 );
    SCT_ASSERT( unroll == SCT_TRUE || ( bytes % ( elementSize / iterations ) == 0 ) );    
    SCT_ASSERT( unroll == SCT_FALSE || ( bytes / elementSize / iterations ) % UNROLL_COUNT == 0 );

    switch( elementSize )
    {
    case 1:
        return unroll ? runSweepBlend1_Unroll( interval, buffers1, buffers2, count, bytes, iterations ) : runSweepBlend1( interval, buffers1, buffers2, count, bytes, iterations );

    case 2:
        return unroll ? runSweepBlend2_Unroll( interval, ( unsigned short** )buffers1, ( const unsigned short** )buffers2, count, bytes, iterations ) : runSweepBlend2( interval, ( unsigned short** )buffers1, ( const unsigned short** )buffers2, count, bytes, iterations );

    case 4:
    default:
        return unroll ? runSweepBlend4_Unroll( interval, ( unsigned int** )buffers1, ( const unsigned int** )buffers2, count, bytes, iterations ) : runSweepBlend4( interval, ( unsigned int** )buffers1, ( const unsigned int** )buffers2, count, bytes, iterations );
    }
}

