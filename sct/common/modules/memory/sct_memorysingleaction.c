/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_memorysingleaction.h"
#include "sct_memorymodule_core.h"
#include "sct_memorymodule_utils.h"
#include "sct_simemory.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

/* ---------------------------------------------------------------------- */
/*!
 * Create single action context.
 *
 * \param attributes Attribute list. Must be defined.
 *
 * \return Single action context, or NULL if failure.
 *
 * \note The memory block size needs to be evenly divided by the
 * element size, i.e. BlockSize % ElementSize == 0.
 *
 */
void* sctiCreateMemorySingleActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTMemorySingleActionContext*   context;
    MemorySingleActionData*         actionData;
    void*                           systemContext   = siCommonGetContext();

    SCT_USE_VARIABLE( moduleContext );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    /* Allocate memory for single action context.  */
    context = ( SCTMemorySingleActionContext* )( siCommonMemoryAlloc( systemContext, sizeof( SCTMemorySingleActionContext ) ) );

    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Single@Memory context creation." );
        return NULL;
    }

    /* Initialize memory data structure to zero. */
    memset( context, 0, sizeof( SCTMemorySingleActionContext ) );

    actionData = ( MemorySingleActionData* )( siCommonMemoryAlloc( systemContext, sizeof( MemorySingleActionData ) ) );
    if( actionData == NULL )
    {
        siCommonMemoryFree( systemContext, context );
        SCT_LOG_ERROR( "Allocation failed in Single@Memory context creation." );
        return NULL;
    }

    context->actionData = actionData;

    /* Parse memory benchmark attributes. */
    if( sctiParseMemorySingleActionAttributes( actionData, attributes ) == SCT_FALSE )
    {
        siCommonMemoryFree( systemContext, context );
        siCommonMemoryFree( systemContext, actionData );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyMemorySingleActionContext( void* context )
{
    SCTMemorySingleActionContext*   data;

    SCT_ASSERT_ALWAYS( context != NULL );

    data = ( SCTMemorySingleActionContext* )( context );

    if( data->actionData != NULL )
    {
        siCommonMemoryFree( NULL, data->actionData );
    }
    siCommonMemoryFree( NULL, data );
}

/* ---------------------------------------------------------------------- */
/*!
 * Initialize single memory action.
 *
 * \param action Action to initialize.
 *
 * \return SCT_TRUE if initialization succeeded, SCT_FALSE if failed.
 *
 */
SCTBoolean sctiMemorySingleActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTMemorySingleActionContext*   context;
    MemorySingleActionData*         actionData;

    SCT_USE_VARIABLE( benchmark );
    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTMemorySingleActionContext* )( action->context );
    actionData = ( MemorySingleActionData* )( context->actionData );

    if( context->initialized == SCT_TRUE )
    {
        return SCT_TRUE;
    }
    context->initialized = SCT_TRUE;

    SCT_ASSERT( context->buffers1 == NULL );
    SCT_ASSERT( context->buffers2 == NULL );
    SCT_ASSERT( context->alignedBuffers1 == NULL );
    SCT_ASSERT( context->alignedBuffers2 == NULL );

    /* Create scratch buffers 1. Make sure the buffers contain extra space for
        setting up the alignment. */
    context->buffers1 = sctiCreateBlocks( actionData->blockCount, actionData->blockSize + ( actionData->align * 3 - 1 ) );
    if( context->buffers1 == NULL )
    {
        SCT_LOG_ERROR( "Memory allocation failed in Single@Memory action init." );
        sctiMemorySingleActionTerminate( action );
        return SCT_FALSE;
    }

    /* Get aligned addresses to scratch buffers 1. */
    context->alignedBuffers1 = sctiGetAlignedBlocks( context->buffers1, actionData->blockCount, actionData->align );
    if( context->alignedBuffers1 == NULL )
    {
        SCT_LOG_ERROR( "Memory allocation failed in Single@Memory action init." );
        sctiMemorySingleActionTerminate( action );
        return SCT_FALSE;
    }

    /* Buffers 2 are only needed for copy and blend. */
    if( actionData->operation == SINGLE_OPERATION_COPY ||
        actionData->operation == SINGLE_OPERATION_BLEND )
    {
        context->buffers2 = sctiCreateBlocks( actionData->blockCount, actionData->blockSize + ( actionData->align * 3 - 1 ) );
        if( context->buffers2 == NULL )
        {
            SCT_LOG_ERROR( "Memory allocation failed in Single@Memory action init." );
            sctiMemorySingleActionTerminate( action );
            return SCT_FALSE;
        }

        /* Get aligned addresses to scratch buffers 2. */
        context->alignedBuffers2 = sctiGetAlignedBlocks( context->buffers2, actionData->blockCount, actionData->align );
        if( context->alignedBuffers2 == NULL )
        {
            SCT_LOG_ERROR( "Memory allocation failed in Single@Memory action init." );
            sctiMemorySingleActionTerminate( action );
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 * Run single memory action.
 *
 * \param action Single memory action to run. Must be defined.
 * \param frameNumber Not used by single memory action.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctiMemorySingleActionExecute( SCTAction* action, int frameNumber )
{
    SCTMemorySingleActionContext*   context;
    MemorySingleActionData*         actionData;

    SCT_ASSERT( action != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context    = ( SCTMemorySingleActionContext* )( action->context );
    actionData = ( MemorySingleActionData* )( context->actionData );

    SCT_ASSERT( context->initialized == SCT_TRUE );
    SCT_ASSERT( context->alignedBuffers1 != NULL );
    SCT_ASSERT( actionData->operation == SINGLE_OPERATION_READ   ||
                actionData->operation == SINGLE_OPERATION_FILLZ  ||
                actionData->operation == SINGLE_OPERATION_MODIFY ||
                actionData->operation == SINGLE_OPERATION_COPY   ||
                actionData->operation == SINGLE_OPERATION_BLEND  );

    /* Call the correct memory benchmark function. */
    switch( actionData->operation )
    {
    case SINGLE_OPERATION_READ:
        return runRead( context->alignedBuffers1, actionData->blockCount, actionData->blockSize, actionData->elementSize, actionData->loopUnroll, actionData->iterations );

    case SINGLE_OPERATION_FILLZ:
        return runFillZ( context->alignedBuffers1, actionData->blockCount, actionData->blockSize, actionData->elementSize, actionData->loopUnroll, actionData->iterations );

    case SINGLE_OPERATION_MODIFY:
        return runModify( context->alignedBuffers1, actionData->blockCount, actionData->blockSize, actionData->elementSize, actionData->loopUnroll, actionData->iterations);

    case SINGLE_OPERATION_COPY:
        return runCopy( ( const unsigned char ** )( context->alignedBuffers1 ), context->alignedBuffers2, actionData->blockCount, actionData->blockSize, actionData->elementSize, actionData->loopUnroll, actionData->iterations );

    case SINGLE_OPERATION_BLEND:
        SCT_ASSERT( context->alignedBuffers2 != NULL );
        return runBlend( context->alignedBuffers1, ( const unsigned char ** )( context->alignedBuffers2 ), actionData->blockCount, actionData->blockSize, actionData->elementSize, actionData->loopUnroll, actionData->iterations );
    }

    /* If we end up here then it was an unknown operation. */
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 * Terminate single memory action and release all resources allocated
 * in init.
 *
 * \param action Single memory action to terminate. Must be defined.
 *
 */
void sctiMemorySingleActionTerminate( SCTAction* action )
{
    SCTMemorySingleActionContext*   context;
    MemorySingleActionData*         actionData;
    void*                           systemContext  = siCommonGetContext();

    SCT_ASSERT_ALWAYS( action != NULL );

    context    = ( SCTMemorySingleActionContext* )( action->context );
    actionData = (MemorySingleActionData* )( context->actionData );

    if( context->alignedBuffers1 != NULL )
    {
        siCommonMemoryFree( systemContext, context->alignedBuffers1 );
        context->alignedBuffers1 = NULL;
    }
    if( context->alignedBuffers2 != NULL )
    {
        siCommonMemoryFree( systemContext, context->alignedBuffers2 );
        context->alignedBuffers2 = NULL;
    }

    sctiDestroyBlocks( context->buffers1, actionData->blockCount );
    context->buffers1 = NULL;

    sctiDestroyBlocks( context->buffers2, actionData->blockCount );
    context->buffers2 = NULL;

    context->initialized = SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 * Destroy single memory action context. This function does not
 * destroy the action data structure.
 *
 * \param action Single memory action to destroy. Must be defined.
 *
 */
void sctiMemorySingleActionDestroy( SCTAction* action )
{
    SCTMemorySingleActionContext*   data;

    SCT_ASSERT_ALWAYS( action != NULL );

    data = ( SCTMemorySingleActionContext* )( action->context );

    SCT_ASSERT( data->buffers1 == NULL );
    SCT_ASSERT( data->buffers2 == NULL );
    SCT_ASSERT( data->alignedBuffers1 == NULL );
    SCT_ASSERT( data->alignedBuffers2 == NULL );

    sctiDestroyMemorySingleActionContext( data );
}

/* ---------------------------------------------------------------------- */
/*!
 * Get single memory action workload, i.e. number of bytes in each iteration.
 *
 * \param action Single memory action. Must be defined.
 *
 * \return List of workload string, or NULL if failure.
 *
 */
SCTStringList* sctiMemorySingleActionGetWorkload( SCTAction* action )
{
    SCTMemorySingleActionContext*   context;
    MemorySingleActionData*         actionData;
    SCTStringList*                  workloads;
    char                            workloadString[ 64 ];
    int                             workloadValue;

    SCT_ASSERT_ALWAYS( action != NULL );

    context    = ( SCTMemorySingleActionContext* )( action->context );
    actionData = ( MemorySingleActionData* )( context->actionData );

    workloads = sctCreateStringList();
    if( workloads == NULL )
    {
        return NULL;
    }

    /* Calculate result value and format result string. */
    workloadValue = actionData->blockCount * actionData->blockSize * actionData->iterations;
    sprintf( workloadString, "%d bytes", workloadValue );

    if( sctAddStringCopyToList( workloads, workloadString ) == SCT_FALSE )
    {
        sctDestroyStringList( workloads );
        return NULL;
    }

    return workloads;
}
