/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_IMAGESCROPDATAACTION_H__ )
#define __SCT_IMAGESCROPDATAACTION_H__

#include "sct_types.h"
#include "sct_imagesmodule.h"
#include "sct_imagesmodule_parser.h"

/*!
 *
 */
typedef struct
{
    SCTImagesModuleContext*             moduleContext;
    ImagesCropDataActionData            data;
} SCTImagesCropDataActionContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                               sctiCreateImagesCropDataActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroyImagesCropDataActionContext( void* context );

    SCTBoolean                          sctiImagesCropDataActionInit( SCTAction* action, SCTBenchmark* benchmark );    
    SCTBoolean                          sctiImagesCropDataActionExecute( SCTAction* action, int frameNumber );
    void                                sctiImagesCropDataActionTerminate( SCTAction* action );
    void                                sctiImagesCropDataActionDestroy( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_IMAGESCROPDATAACTION_H__ ) */
