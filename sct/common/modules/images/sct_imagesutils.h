/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_IMAGESUTILS_H__ )
#define __SCT_IMAGESUTILS_H__

#include "sct_types.h"
#include "sct_utils.h"

/*!
 *
 */
typedef enum
{
    IMAGES_FLOWER,
} SCTImagesType;

/*!
 *
 */
typedef enum
{
    IMAGES_A8,
    IMAGES_L8,
    IMAGES_RGBA5551,
    IMAGES_RGBA4444,    
    IMAGES_RGB565,
    IMAGES_BGR565,    
    IMAGES_RGB8,
    IMAGES_BGR8,  
    IMAGES_RGBX8,
    IMAGES_XBGR8,
    IMAGES_RGBA8,
    IMAGES_ABGR8,
    IMAGES_RGBA8_PRE,
    IMAGES_ABGR8_PRE,     
    IMAGES_XRGB8,
    IMAGES_BGRX8,  
    IMAGES_ARGB8,
    IMAGES_BGRA8, 
    IMAGES_ARGB8_PRE,
    IMAGES_BGRA8_PRE,
    IMAGES_ETC1_RGB8,
    IMAGES_PVRTC_RGB4,
    IMAGES_PVRTC_RGB2,
    IMAGES_PVRTC_RGBA4,
    IMAGES_PVRTC_RGBA2,
    IMAGES_YUV12,
} SCTImagesFormat;
   
/*
 *
 */
typedef struct
{
    SCTImagesFormat         format;
    int                     width;
    int                     height;
    void*                   data;
    int                     length;
} SCTImagesData;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */
   
    SCTImagesData*          sctImagesCreateData( SCTImagesFormat format, int width, int height, void* data, int length, SCTBoolean copy );
    SCTImagesData*          sctImagesCreateRescaledRGBData( SCTImagesFormat srcFormat, int srcWidth, int srcHeight, const void* srcData, SCTImagesFormat dstFormat, int dstWidth, int dstHeight );
    SCTImagesData*          sctImagesCreateConvertedData( SCTImagesFormat srcFormat, int srcWidth, int srcHeight, const void* srcData, SCTImagesFormat dstFormat );
    SCTImagesData*          sctImagesCreateCroppedRGBData( SCTImagesData* data, int x, int y, int width, int height );
    SCTImagesData*          sctImagesCreateCroppedETC1Data( SCTImagesData* data, int x, int y, int width, int height );
    SCTImagesData*          sctImagesCreateCroppedPVRTCData( SCTImagesData* data, int x, int y, int width, int height );
    void                    sctImagesDestroyData( SCTImagesData* data );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_IMAGESUTILS_H__ ) */
