/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_imagesdeletedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateImagesDeleteDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTImagesDeleteDataActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTImagesDeleteDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTImagesDeleteDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DeleteData@Images action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTImagesDeleteDataActionContext ) );

    context->moduleContext = ( SCTImagesModuleContext* )( moduleContext );

    if( sctiParseImagesDeleteDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyImagesDeleteDataActionContext( context );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesDeleteDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in DeleteData@Images context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyImagesDeleteDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiImagesDeleteDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTImagesDeleteDataActionContext*   context;
    ImagesDeleteDataActionData*         data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTImagesDeleteDataActionContext* )( action->context );
    data    = &( context->data );

    sctiImagesModuleDeleteData( context->moduleContext, data->dataIndex );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiImagesDeleteDataActionDestroy( SCTAction* action )
{
    SCTImagesDeleteDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTImagesDeleteDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyImagesDeleteDataActionContext( context );
}
