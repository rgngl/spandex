/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_imagesmodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"
#include "sct_imagesmodule_parser.h"
#include "sct_imagesmodule_actions.h"

#include <stdio.h>
#include <string.h>

/* Local function declarations. */
SCTAttributeList*   sctiImagesModuleInfo( SCTModule* module );
SCTAction*          sctiImagesModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                sctiImagesModuleDestroy( SCTModule* module );

/*!
 * Create images module data structure which defines the name of the module and
 * sets up the module interface functions.
 *
 * \return Module structure for images module, or NULL if failure.
 */
SCTModule* sctCreateImagesModule( void )
{
    SCTModule*              module;
    SCTImagesModuleContext* context;
    int                     i;

    /* Create Images module context and zero memory. */
    context = ( SCTImagesModuleContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTImagesModuleContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTImagesModuleContext ) );

    for( i = 0; i < SCT_MAX_IMAGES_DATAS; ++i )
    {
        context->datas[ i ] = NULL;
    }

    /* Create module data structure. */
    module = sctCreateModule( "Images",
                              context,
#if defined( _WIN32 )
                              _images_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiImagesModuleInfo,
                              sctiImagesModuleCreateAction,
                              sctiImagesModuleDestroy );

    if( module == NULL )
    {
        siCommonMemoryFree( NULL, context );
        return NULL;
    }

    return module;
}

/*!
 * Create attribute list containing Images module information.
 *
 * \param module Images module. Must be defined.
 *
 * \return Images system information as attribute list, or NULL if
 * failure.
 */
SCTAttributeList* sctiImagesModuleInfo( SCTModule* module )
{
    SCTAttributeList*   attributes;

    SCT_ASSERT_ALWAYS( module != NULL );

    /* Create attribute list for storing Images module information. */
    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        return NULL;
    }
    
    return attributes;
}

/*!
 * Create Images module action. 
 *
 * \param module Images module. Must be defined.
 * \param name Action name. Must be defined.
 * \param type Action type. Must be defined.
 * \param attributes List of action attributes. Must be defined.
 * \param log Log to use for warning and error reporting.
 *
 * \return Action data structure, or NULL if failure.
 *
 */
SCTAction* sctiImagesModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCTImagesModuleContext* moduleContext;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    moduleContext = ( SCTImagesModuleContext* )( module->context );

    SCT_ASSERT_ALWAYS( moduleContext != NULL );

    return sctCreateActionFromTemplate( name, type, "Images", moduleContext, attributes, ImagesActionTemplates, SCT_ARRAY_LENGTH( ImagesActionTemplates ) );
}

/*!
 * Destroy Images module context. This function does not deallocate
 * module data structure but instead simply does the needed module
 * cleanup, if any.
 *
 * \param module Module to destroy. Must be defined.
 */
void sctiImagesModuleDestroy( SCTModule* module )
{
    SCTImagesModuleContext* context;
    int                     i;

    SCT_ASSERT_ALWAYS( module != NULL );

    context = ( SCTImagesModuleContext* )( module->context );
    if( context != NULL )
    {
        for( i = 0; i < SCT_MAX_IMAGES_DATAS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->datas[ i ] == NULL );
        }

        siCommonMemoryFree( NULL, context );
        module->context = NULL;
    }
}

/*!
 *
 *
 */
SCTBoolean sctiImagesModuleIsValidDataIndex( SCTImagesModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_IMAGES_DATAS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTImagesData* sctiImagesModuleGetData( SCTImagesModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_IMAGES_DATAS );

    if( index >= 0 && index < SCT_MAX_IMAGES_DATAS )
    {
        return moduleContext->datas[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiImagesModuleSetData( SCTImagesModuleContext* moduleContext, int index, SCTImagesData* data )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_IMAGES_DATAS );

    if( index >= 0 && index < SCT_MAX_IMAGES_DATAS )
    {
        moduleContext->datas[ index ] = data;
    }
}

/*!
 *
 *
 */
void sctiImagesModuleDeleteData( SCTImagesModuleContext* moduleContext, int index )
{
    SCTImagesData*  data;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_IMAGES_DATAS )
    {
        data = moduleContext->datas[ index ];
        if( data != NULL )
        {
            sctImagesDestroyData( data );
            moduleContext->datas[ index ] = NULL;
        }
    }
}
