/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_imagescopytoopengles2action.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_OPENGLES2_MODULE )
# include "sct_opengles2module.h"
#endif  /* defined( INCLUDE_OPENGLES2_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateImagesCopyToOpenGLES2ActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTImagesCopyToOpenGLES2ActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTImagesCopyToOpenGLES2ActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTImagesCopyToOpenGLES2ActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CopyToOpenGLES2@Images action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTImagesCopyToOpenGLES2ActionContext ) );

    context->moduleContext = ( SCTImagesModuleContext* )( moduleContext );

    if( sctiParseImagesCopyToOpenGLES2ActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyImagesCopyToOpenGLES2ActionContext( context );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesCopyToOpenGLES2ActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in CopyToOpenGLES2@Images context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyImagesCopyToOpenGLES2ActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCopyToOpenGLES2ActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( INCLUDE_OPENGLES2_MODULE )
    SCT_LOG_ERROR( "CopyToOpenGLES2@Images action init OpenGLES2 module not available." );
    return SCT_FALSE;
#else   /* !defined( INCLUDE_OPENGLES2_MODULE ) */
    return SCT_TRUE;    
#endif  /* !defined( INCLUDE_OPENGLES2_MODULE ) */  
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCopyToOpenGLES2ActionExecute( SCTAction* action, int frameNumber )
{
#if defined( INCLUDE_OPENGLES2_MODULE )
    SCTImagesCopyToOpenGLES2ActionContext*  context;
    ImagesCopyToOpenGLES2ActionData*        data;
    SCTImagesData*                          srcData;    
    SCTOpenGLES2Data*                       dstData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTImagesCopyToOpenGLES2ActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiImagesModuleGetData( context->moduleContext, data->dataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data index in CopyToOpenGLES2@Images action execute." );
        return SCT_FALSE;
    }

    dstData = sctOpenGLES2CreateData( srcData->data, srcData->length, SCT_TRUE );
    if( dstData == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CopyToOpenGLES2@Images action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES2ModuleSetDataExt( data->openGLES2DataIndex, dstData ) == SCT_FALSE )
    {
        sctOpenGLES2DestroyData( dstData );        
        SCT_LOG_ERROR( "Copying data to OpenGLES2 failed in CopyToOpenGLES2@Images action execute." );
        return SCT_FALSE;
    }   

    return SCT_TRUE;
#else   /* defined( INCLUDE_OPENGLES2_MODULE ) */
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );
    SCT_ASSERT_ALWAYS( 0 );
    
    return SCT_FALSE;      
#endif  /* defined( INCLUDE_OPENGLES2_MODULE ) */     
}

/*!
 *
 *
 */
void sctiImagesCopyToOpenGLES2ActionTerminate( SCTAction* action )
{
    SCTImagesCopyToOpenGLES2ActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTImagesCopyToOpenGLES2ActionContext* )( action->context );

#if defined( INCLUDE_OPENGLES2_MODULE )
    sctOpenGLES2ModuleSetDataExt( context->data.openGLES2DataIndex, NULL );    
#else   /* defined( INCLUDE_OPENGLES2_MODULE ) */
    SCT_USE_VARIABLE( context );
#endif  /* defined( INCLUDE_OPENGLES2_MODULE ) */
}

/*!
 *
 *
 */
void sctiImagesCopyToOpenGLES2ActionDestroy( SCTAction* action )
{
    SCTImagesCopyToOpenGLES2ActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTImagesCopyToOpenGLES2ActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyImagesCopyToOpenGLES2ActionContext( context );
}
