/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_imagesutils.h"
#include "sct_utils.h"
#include "sct_sicommon.h"

#include <string.h>
#include <math.h>

/*!
 *
 *
 */
static SCTBoolean       sctiIsValidImagesFormat( SCTImagesFormat format );
static int              sctiGetImagesPixelSize( SCTImagesFormat format );
static void             sctiFilterRGBA8Pixel( double u, double v, int width, int height, const void* src, unsigned char* color );
static void             sctiAssignPixel( SCTImagesFormat format, void* dest, const unsigned char* color );
static int              sctiGetMortonOrderOffset( int x, int y );
static SCTImagesData*   sctiCreateYUV12Data( SCTImagesFormat srcFormat, int srcWidth, int srcHeight, const void* srcData );

/*!
 *
 *
 */
SCTImagesData* sctImagesCreateData( SCTImagesFormat format, int width, int height, void* data, int length, SCTBoolean copy )
{
    SCTImagesData*  d;

    SCT_ASSERT_ALWAYS( sctiIsValidImagesFormat( format ) == SCT_TRUE );
    SCT_ASSERT_ALWAYS( width > 0 );
    SCT_ASSERT_ALWAYS( height > 0 );
    SCT_ASSERT_ALWAYS( data != NULL );
    SCT_ASSERT_ALWAYS( length > 0 );

    d = ( SCTImagesData* )( siCommonMemoryAlloc( NULL, sizeof( SCTImagesData ) ) );
    if( d == NULL )
    {
        return NULL;
    }
    
    if( copy == SCT_TRUE )
    {
        d->data = siCommonMemoryAlloc( NULL, length );
        if( d->data == NULL )
        {
            siCommonMemoryFree( NULL, d );
            return NULL;
        }
        memcpy( d->data, data, length );
    }
    else
    {
        d->data = data;
    }

    d->format = format;
    d->width  = width;
    d->height = height;
    d->length = length;

    return d;
}

/*!
 *
 *
 */
SCTImagesData* sctImagesCreateRescaledRGBData( SCTImagesFormat srcFormat, int srcWidth, int srcHeight, const void* srcData, SCTImagesFormat dstFormat, int dstWidth, int dstHeight )
{
    int             pixelSize;
    int             dataLength;
    void*           rawData;
    SCTImagesData*  imagesData;
    int             x;
    int             y;
	double          xs;
    double          ys;
    unsigned char*  p;
    unsigned char   color[ 4 ];
    
    SCT_ASSERT_ALWAYS( srcWidth > 0 );
    SCT_ASSERT_ALWAYS( srcHeight > 0 );
    SCT_ASSERT_ALWAYS( dstWidth > 0 );
    SCT_ASSERT_ALWAYS( dstHeight > 0 );

    if( srcFormat != IMAGES_RGBA8 )
    {
        return NULL;
    }

    if( dstFormat != IMAGES_A8        &&
        dstFormat != IMAGES_L8        &&
        dstFormat != IMAGES_RGBA5551  &&
        dstFormat != IMAGES_RGBA4444  &&
        dstFormat != IMAGES_RGB565    &&
        dstFormat != IMAGES_BGR565    &&        
        dstFormat != IMAGES_RGB8      &&
        dstFormat != IMAGES_BGR8      &&  
        dstFormat != IMAGES_RGBX8     &&
        dstFormat != IMAGES_XBGR8     &&
        dstFormat != IMAGES_RGBA8     &&
        dstFormat != IMAGES_ABGR8     &&
        dstFormat != IMAGES_RGBA8_PRE &&
        dstFormat != IMAGES_ABGR8_PRE &&     
        dstFormat != IMAGES_XRGB8     &&
        dstFormat != IMAGES_BGRX8     &&  
        dstFormat != IMAGES_ARGB8     &&
        dstFormat != IMAGES_BGRA8     && 
        dstFormat != IMAGES_ARGB8_PRE &&
        dstFormat != IMAGES_BGRA8_PRE )
    {
        return NULL;
    }

    pixelSize = sctiGetImagesPixelSize( dstFormat );
    dataLength = dstWidth * dstHeight * pixelSize;
    
    rawData = siCommonMemoryAlloc( NULL, dataLength );
    if( rawData == NULL )
    {
        return NULL;
    }

    imagesData = sctImagesCreateData( dstFormat, dstWidth, dstHeight, rawData, dataLength, SCT_FALSE );
    if( imagesData == NULL )
    {
        siCommonMemoryFree( NULL, rawData );
        return NULL;
    }

    rawData = NULL; /* Raw data moved into imagesData */

    /* Do bilinear filtering; yes, inefficient. */
    xs = ( double )( srcWidth - 1 ) / ( double )( dstWidth );
    ys = ( double )( srcHeight - 1 ) / ( double )( dstHeight );
    
    p  = ( unsigned char* )( imagesData->data );

    for( y = 0; y < dstHeight; ++y )
    {
        for( x = 0; x < dstWidth; ++x )
        {
            sctiFilterRGBA8Pixel( x * xs, y * ys, srcWidth, srcHeight, srcData, color );
            sctiAssignPixel( dstFormat, p, color );
            p += pixelSize;
        }
    }

    return imagesData;
}

/*!
 *
 *
 */
SCTImagesData* sctImagesCreateConvertedData( SCTImagesFormat srcFormat, int srcWidth, int srcHeight, const void* srcData, SCTImagesFormat dstFormat )
{
    SCT_ASSERT_ALWAYS( srcData != NULL );
    
    if( dstFormat == IMAGES_YUV12 )
    {
        return sctiCreateYUV12Data( srcFormat, srcWidth, srcHeight, srcData );
    }
    
    return NULL;
}

/*!
 *
 *
 */
SCTImagesData* sctImagesCreateCroppedRGBData( SCTImagesData* data, int x, int y, int width, int height )
{
    SCTImagesData*  imagesData;    
    int             pixelSize;
    int             dataLength;
    unsigned char*  buf;
    int             sy;
    int             dy;
    unsigned char*  pSrc;
    unsigned char*  pDst;
    int             rowLen;
        
    SCT_ASSERT_ALWAYS( data != NULL );
    SCT_ASSERT_ALWAYS( x >= 0 );    
    SCT_ASSERT_ALWAYS( y >= 0 );    
    SCT_ASSERT_ALWAYS( width > 0 );    
    SCT_ASSERT_ALWAYS( height > 0 );    
    SCT_ASSERT_ALWAYS( ( x + width ) <= data->width );    
    SCT_ASSERT_ALWAYS( ( y + height ) <= data->height );
    SCT_ASSERT_ALWAYS( data->format == IMAGES_A8        ||
                       data->format == IMAGES_L8        ||
                       data->format == IMAGES_RGBA5551  ||
                       data->format == IMAGES_RGBA4444  ||
                       data->format == IMAGES_RGB565    ||
                       data->format == IMAGES_BGR565    ||                       
                       data->format == IMAGES_RGB8      ||
                       data->format == IMAGES_BGR8      ||  
                       data->format == IMAGES_RGBX8     ||
                       data->format == IMAGES_XBGR8     ||
                       data->format == IMAGES_RGBA8     ||
                       data->format == IMAGES_ABGR8     ||
                       data->format == IMAGES_RGBA8_PRE ||
                       data->format == IMAGES_ABGR8_PRE ||     
                       data->format == IMAGES_XRGB8     ||
                       data->format == IMAGES_BGRX8     ||  
                       data->format == IMAGES_ARGB8     ||
                       data->format == IMAGES_BGRA8     || 
                       data->format == IMAGES_ARGB8_PRE ||
                       data->format == IMAGES_BGRA8_PRE );
    
    pixelSize = sctiGetImagesPixelSize( data->format );
    dataLength = pixelSize * width * height;

    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, dataLength  ) );
    if( buf == NULL )
    {
        return NULL;
    }

    imagesData = sctImagesCreateData( data->format, width, height, buf, dataLength, SCT_FALSE );
    if( imagesData == NULL )
    {
        siCommonMemoryFree( NULL, buf );
        return NULL;
    }

    buf = NULL; /* buf moved into imagesData */
    
    rowLen = width * pixelSize;
    for( sy = y, dy = 0; dy < height; ++sy, ++dy )
    {
        pDst = &( ( ( unsigned char* )( imagesData->data ) )[ dy * rowLen ] );
        pSrc = &( ( ( unsigned char* )( data->data ) )[ ( sy * data->width + x ) * pixelSize ] );
       
        memcpy( pDst, pSrc, rowLen );
    }
    
    return imagesData;
}

/*!
 *
 *
 */
SCTImagesData* sctImagesCreateCroppedETC1Data( SCTImagesData* data, int x, int y, int width, int height )
{
    SCTImagesData*  imagesData;    
    int             dataLength;
    unsigned char*  buf;
    int             sy;
    int             dy;
    unsigned char*  pSrc;
    unsigned char*  pDst;
    int             xd4;
    int             yd4;
    int             hd4;
    int             srcBlocksW;
    int             dstBlocksW;
    
    SCT_ASSERT_ALWAYS( data != NULL );
    SCT_ASSERT_ALWAYS( ( x >= 0 ) && ( x % 4 == 0 ) );    
    SCT_ASSERT_ALWAYS( ( y >= 0 ) && ( y % 4 == 0 ) );    
    SCT_ASSERT_ALWAYS( ( width > 0 ) && ( width % 4 == 0 ) );    
    SCT_ASSERT_ALWAYS( ( height > 0 ) && ( height % 4 == 0 ) );
    SCT_ASSERT_ALWAYS( ( x + width ) <= data->width );    
    SCT_ASSERT_ALWAYS( ( y + height ) <= data->height );
    SCT_ASSERT_ALWAYS( data->format == IMAGES_ETC1_RGB8 );
    
    dataLength = width * height / 2;

    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, dataLength  ) );
    if( buf == NULL )
    {
        return NULL;
    }

    imagesData = sctImagesCreateData( data->format, width, height, buf, dataLength, SCT_FALSE );
    if( imagesData == NULL )
    {
        siCommonMemoryFree( NULL, buf );
        return NULL;
    }

    buf = NULL; /* buf moved into imagesData */
    
    xd4        = x / 4;
    yd4        = y / 4;
    hd4        = height / 4;
    srcBlocksW = data->width / 4;
    dstBlocksW = width / 4;
    for( sy = yd4, dy = 0; dy < hd4; ++sy, ++dy )
    {
        pDst = &( ( ( unsigned char* )( imagesData->data ) )[ dy * dstBlocksW * 8 ] );
        pSrc = &( ( ( unsigned char* )( data->data ) )[ ( sy * srcBlocksW + xd4 ) * 8 ] );
       
        memcpy( pDst, pSrc, dstBlocksW * 8 );
    }
    
    return imagesData;
}

/*!
 *
 *
 */
SCTImagesData* sctImagesCreateCroppedPVRTCData( SCTImagesData* data, int x, int y, int width, int height )
{
    SCTImagesData*  imagesData;    
    int             dataLength;
    unsigned char*  buf;
    int             sx;
    int             dx;
    int             sy;
    int             dy;
    unsigned char*  pSrc;
    unsigned char*  pDst;
    int             xd4;
    int             yd4;
    int             hd4;
    int             wd4;
    int             srcOffset;
    int             dstOffset;
    int             blockSize;
    
    SCT_ASSERT_ALWAYS( data != NULL );
    SCT_ASSERT_ALWAYS( data->width == data->height );        
    SCT_ASSERT_ALWAYS( width == height );
    SCT_ASSERT_ALWAYS( ( x + width ) <= data->width );    
    SCT_ASSERT_ALWAYS( ( y + height ) <= data->height );
    SCT_ASSERT_ALWAYS( data->format == IMAGES_PVRTC_RGB4 ||
                       data->format == IMAGES_PVRTC_RGBA4 ||
                       data->format == IMAGES_PVRTC_RGB2 ||
                       data->format == IMAGES_PVRTC_RGBA2 );

    if( data->format == IMAGES_PVRTC_RGB4 ||
        data->format == IMAGES_PVRTC_RGBA4 )
    {
        SCT_ASSERT_ALWAYS( ( x >= 0 ) && ( x % 4 == 0 ) );    
        SCT_ASSERT_ALWAYS( ( y >= 0 ) && ( y % 4 == 0 ) );    
        SCT_ASSERT_ALWAYS( ( width > 0 ) && ( width % 4 == 0 ) );    
        SCT_ASSERT_ALWAYS( ( height > 0 ) && ( height % 4 == 0 ) );
        
        dataLength = width * height / 2;
        blockSize  = 8;
    }
    else
    {
        SCT_ASSERT_ALWAYS( ( x >= 0 ) && ( x % 8 == 0 ) );    
        SCT_ASSERT_ALWAYS( ( y >= 0 ) && ( y % 4 == 0 ) );    
        SCT_ASSERT_ALWAYS( ( width > 0 ) && ( width % 8 == 0 ) );    
        SCT_ASSERT_ALWAYS( ( height > 0 ) && ( height % 4 == 0 ) );
        
        dataLength = width * height / 4;
        blockSize  = 4;        
    }           

    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, dataLength  ) );
    if( buf == NULL )
    {
        return NULL;
    }

    imagesData = sctImagesCreateData( data->format, width, height, buf, dataLength, SCT_FALSE );
    if( imagesData == NULL )
    {
        siCommonMemoryFree( NULL, buf );
        return NULL;
    }

    buf = NULL; /* buf moved into imagesData */

    /* PVRTC blocks are in morton order. Therefore, we do per block copy. */
    xd4  = x / 4;
    wd4 = width / 4;    
    yd4 = y / 4;
    hd4 = height / 4;
    for( sy = yd4, dy = 0; dy < hd4; ++sy, ++dy )
    {
        for( sx = xd4, dx = 0; dx < wd4; ++sx, ++dx )
        {
            srcOffset = sctiGetMortonOrderOffset( sx, sy );
            dstOffset = sctiGetMortonOrderOffset( dx, dy );

            SCT_ASSERT( ( ( dstOffset + 1 ) * blockSize ) <= imagesData->length );
            SCT_ASSERT( ( ( srcOffset + 1 ) * blockSize ) <= data->length );
            
            pDst = &( ( ( unsigned char* )( imagesData->data ) )[ dstOffset * blockSize ] );
            pSrc = &( ( ( unsigned char* )( data->data ) )[ srcOffset * blockSize ] );
            memcpy( pDst, pSrc, blockSize );
        }
    }
    
    return imagesData;
}

/*!
 *
 *
 */
void sctImagesDestroyData( SCTImagesData* data )
{
    if( data != NULL )
    {
        if( data->data != NULL )
        {
            siCommonMemoryFree( NULL, data->data );
            data->data = NULL;
        }
        siCommonMemoryFree( NULL, data );
    }
}

/*!
 *
 *
 */
static int sctiGetMortonOrderOffset( int x, int y )
{
    unsigned int    v;

    SCT_ASSERT( x <= 0xFFFF );
    SCT_ASSERT( y <= 0xFFFF );

    /* Combine x and y into a single word and use the perfect shuffle from
     * Hacker's Delight. */
    
    v = ( ( unsigned int )( x ) & 0xFFFF ) | ( ( unsigned int )( y & 0xFFFF ) << 16 );

    v = ( ( v & 0x0000FF00 ) << 8 ) | ( ( v >> 8 ) & 0x0000FF00 ) | ( v & 0xFF0000FF );
    v = ( ( v & 0x00F000F0 ) << 4 ) | ( ( v >> 4 ) & 0x00F000F0 ) | ( v & 0xF00FF00F );
    v = ( ( v & 0x0C0C0C0C ) << 2 ) | ( ( v >> 2 ) & 0x0C0C0C0C ) | ( v & 0xC3C3C3C3 );
    v = ( ( v & 0x22222222 ) << 1 ) | ( ( v >> 1 ) & 0x22222222 ) | ( v & 0x99999999 );

    return ( int )( v );
}

/*!
 *
 *
 */
static SCTBoolean sctiIsValidImagesFormat( SCTImagesFormat format )
{
    if( format == IMAGES_A8          ||
        format == IMAGES_L8          ||
        format == IMAGES_RGBA5551    ||
        format == IMAGES_RGBA4444    ||
        format == IMAGES_RGB565      ||
        format == IMAGES_BGR565      ||        
        format == IMAGES_RGB8        ||
        format == IMAGES_BGR8        ||
        format == IMAGES_RGBX8       ||
        format == IMAGES_XBGR8       ||
        format == IMAGES_RGBA8       ||
        format == IMAGES_ABGR8       ||
        format == IMAGES_RGBA8_PRE   ||
        format == IMAGES_ABGR8_PRE   ||  
        format == IMAGES_XRGB8       ||
        format == IMAGES_BGRX8       ||
        format == IMAGES_ARGB8       ||
        format == IMAGES_BGRA8       ||
        format == IMAGES_ARGB8_PRE   ||
        format == IMAGES_BGRA8_PRE   ||
        format == IMAGES_ETC1_RGB8   ||
        format == IMAGES_PVRTC_RGB4  ||
        format == IMAGES_PVRTC_RGB2  ||
        format == IMAGES_PVRTC_RGBA4 ||
        format == IMAGES_PVRTC_RGBA2 ||
        format == IMAGES_YUV12 )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
static int sctiGetImagesPixelSize( SCTImagesFormat format )
{
    switch( format )
    {
    case IMAGES_A8:
    case IMAGES_L8:
        return 1;

    case IMAGES_RGBA5551:
    case IMAGES_RGBA4444:
    case IMAGES_RGB565:
    case IMAGES_BGR565:        
        return 2;

    case IMAGES_RGB8:
    case IMAGES_BGR8:
        return 3;

    case IMAGES_RGBX8:
    case IMAGES_XBGR8:
    case IMAGES_RGBA8:
    case IMAGES_ABGR8:
    case IMAGES_RGBA8_PRE:
    case IMAGES_ABGR8_PRE:
    case IMAGES_XRGB8:
    case IMAGES_BGRX8:
    case IMAGES_ARGB8:
    case IMAGES_BGRA8:
    case IMAGES_ARGB8_PRE:
    case IMAGES_BGRA8_PRE:
        return 4;

    default:
        SCT_ASSERT_ALWAYS( 0 );
        return -1;
    }
}

/*
 *
 *
 */
static void sctiFilterRGBA8Pixel( double u, double v, int width, int height, const void* src, unsigned char* color )
{
    int             pixel[ 4 ];
    unsigned char*  p1;
    unsigned char*  p2;
    double          x;
    double          y;
    double          ur;
    double          vr;
    double          uo;
    double          vo;
    
    SCT_ASSERT( src != NULL );
    SCT_ASSERT( color != NULL );

    x = floor( u );
    y = floor( v );
    ur = u - x;
    vr = v - y;
    uo = 1 - ur;
    vo = 1 - vr;
    
    p1 = &( ( unsigned char* )( src ) )[ ( ( ( int )( y ) * width ) + ( int )( x ) ) * 4 ]; // RGBA8 = 4
    p2 = p1;
    
    if( ( int )( x ) < width )
    {
        p2 += 4;
    }
    
    pixel[ 0 ] = ( int )( ( *( p1 )     * uo + *( p2 )     * ur ) * vo );
    pixel[ 1 ] = ( int )( ( *( p1 + 1 ) * uo + *( p2 + 1 ) * ur ) * vo );
    pixel[ 2 ] = ( int )( ( *( p1 + 2 ) * uo + *( p2 + 2 ) * ur ) * vo );
    pixel[ 3 ] = ( int )( ( *( p1 + 3 ) * uo + *( p2 + 3 ) * ur ) * vo );

    if( ( int )( y ) < height )
    {
        p1 += ( width * 4 );        
        p2 += ( width * 4 );
    }

    pixel[ 0 ] = ( int )( pixel[ 0 ] + ( *( p1 )     * uo + *( p2 )     * ur ) * vr );
    pixel[ 1 ] = ( int )( pixel[ 1 ] + ( *( p1 + 1 ) * uo + *( p2 + 1 ) * ur ) * vr );
    pixel[ 2 ] = ( int )( pixel[ 2 ] + ( *( p1 + 2 ) * uo + *( p2 + 2 ) * ur ) * vr );
    pixel[ 3 ] = ( int )( pixel[ 3 ] + ( *( p1 + 3 ) * uo + *( p2 + 3 ) * ur ) * vr );
   
    color[ 0 ] = ( unsigned char )( pixel[ 0 ] );
    color[ 1 ] = ( unsigned char )( pixel[ 1 ] );
    color[ 2 ] = ( unsigned char )( pixel[ 2 ] );
    color[ 3 ] = ( unsigned char )( pixel[ 3 ] );
}

/*!
 *
 *
 */
static void sctiAssignPixel( SCTImagesFormat format, void* dest, const unsigned char* color )
{
    unsigned char*  cp = ( unsigned char* )( dest );
    unsigned short* sp = ( unsigned short* )( dest );
    unsigned short  s;

    SCT_ASSERT( dest != NULL );
    SCT_ASSERT( color != NULL );

    switch( format )
    {
    case IMAGES_A8:
    case IMAGES_L8:
        *cp = ( unsigned char )( ( ( int )( color[ 0 ] ) + color[ 1 ] + color[ 2 ] + color[ 3 ] ) / 4 );
        break;

    case IMAGES_RGBA4444:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 0 ] >> 4 ) << 12 ) & 0xF000 ) | 
            ( ( ( unsigned int )( color[ 1 ] >> 4 ) << 8 )  & 0x0F00 ) |
            ( ( ( unsigned int )( color[ 2 ] >> 4 ) << 4 )  & 0x00F0 ) |
              ( ( unsigned int )( color[ 3 ] >> 4 )         & 0x000F ) );
        *sp = s;
        break;

    case IMAGES_RGBA5551:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 0 ] >> 5 ) << 11 ) & 0xF800 ) | 
            ( ( ( unsigned int )( color[ 1 ] >> 5 ) << 6 )  & 0x07C0 ) |
            ( ( ( unsigned int )( color[ 2 ] >> 5 ) << 1 )  & 0x003E ) |
              ( ( unsigned int )( color[ 3 ] + 0.5f )       & 0x0001 ) );
        *sp = s;
        break;
    
    case IMAGES_RGB565:
        s = ( unsigned short )(
            ( unsigned int )( color[ 0 ] << 8 & 0xF800 ) | 
            ( unsigned int )( color[ 1 ] << 3 & 0x07E0 ) |
            ( unsigned int )( color[ 2 ] >> 3 & 0x001F ) );
        *sp = s;
        break;

    case IMAGES_BGR565:
        s = ( unsigned short )(
            ( unsigned int )( color[ 2 ] << 8 & 0xF800 ) | 
            ( unsigned int )( color[ 1 ] << 3 & 0x07E0 ) |
            ( unsigned int )( color[ 0 ] >> 3 & 0x001F ) );
        *sp = s;
        break;
        
    case IMAGES_RGB8:   /* Intentional. */
    case IMAGES_RGBX8:        
        *cp++ = ( unsigned char )( color[ 0 ] );
        *cp++ = ( unsigned char )( color[ 1 ] );
        *cp   = ( unsigned char )( color[ 2 ] );
        break;

    case IMAGES_BGR8:
        *cp++ = ( unsigned char )( color[ 2 ] );
        *cp++ = ( unsigned char )( color[ 1 ] );
        *cp   = ( unsigned char )( color[ 0 ] );
        break;

    case IMAGES_XBGR8:
        *cp++ = 0;
        *cp++ = ( unsigned char )( color[ 2 ] );
        *cp++ = ( unsigned char )( color[ 1 ] );
        *cp   = ( unsigned char )( color[ 0 ] );
        break;
        
    case IMAGES_RGBA8:
        *cp++ = ( unsigned char )( color[ 0 ] );
        *cp++ = ( unsigned char )( color[ 1 ] );
        *cp++ = ( unsigned char )( color[ 2 ] );
        *cp   = ( unsigned char )( color[ 3 ] );
        break;

    case IMAGES_ABGR8:
        *cp++ = ( unsigned char )( color[ 3 ] );
        *cp++ = ( unsigned char )( color[ 2 ] );
        *cp++ = ( unsigned char )( color[ 2 ] );
        *cp   = ( unsigned char )( color[ 0 ] );
        break;
        
    case IMAGES_RGBA8_PRE:
        *cp++ = ( unsigned char )( sctMin( color[ 0 ] * color[ 3 ], 0xFF ) );
        *cp++ = ( unsigned char )( sctMin( color[ 1 ] * color[ 3 ], 0xFF ) );
        *cp++ = ( unsigned char )( sctMin( color[ 2 ] * color[ 3 ], 0xFF ) );
        *cp   = ( unsigned char )( color[ 3 ] );
        break;

    case IMAGES_ABGR8_PRE:
        *cp++ = ( unsigned char )( color[ 3 ] );
        *cp++ = ( unsigned char )( sctMin( color[ 2 ] * color[ 3 ], 0xFF ) );
        *cp++ = ( unsigned char )( sctMin( color[ 1 ] * color[ 3 ], 0xFF ) );
        *cp   = ( unsigned char )( sctMin( color[ 0 ] * color[ 3 ], 0xFF ) );
        break;

    case IMAGES_XRGB8:
        cp++;
        *cp++ = ( unsigned char )( color[ 0 ] );
        *cp++ = ( unsigned char )( color[ 1 ] );
        *cp   = ( unsigned char )( color[ 2 ] );
        break;

    case IMAGES_BGRX8:
        *cp++ = ( unsigned char )( color[ 2 ] );
        *cp++ = ( unsigned char )( color[ 1 ] );
        *cp   = ( unsigned char )( color[ 0 ] );
        break;
        
    case IMAGES_ARGB8:
        *cp++ = ( unsigned char )( color[ 3 ] );
        *cp++ = ( unsigned char )( color[ 0 ] );
        *cp++ = ( unsigned char )( color[ 1 ] );
        *cp   = ( unsigned char )( color[ 2 ] );
        break;

    case IMAGES_BGRA8:
        *cp++ = ( unsigned char )( color[ 2 ] );
        *cp++ = ( unsigned char )( color[ 1 ] );
        *cp++ = ( unsigned char )( color[ 0 ] );
        *cp   = ( unsigned char )( color[ 3 ] );
        break;       

    case IMAGES_ARGB8_PRE:
        *cp++ = ( unsigned char )( color[ 3 ] );
        *cp++ = ( unsigned char )( sctMin( color[ 0 ] * color[ 3 ], 0xFF ) );
        *cp++ = ( unsigned char )( sctMin( color[ 1 ] * color[ 3 ], 0xFF ) );
        *cp   = ( unsigned char )( sctMin( color[ 2 ] * color[ 3 ], 0xFF ) );
        break;

    case IMAGES_BGRA8_PRE:
        *cp++ = ( unsigned char )( sctMin( color[ 2 ] * color[ 3 ], 0xFF ) );
        *cp++ = ( unsigned char )( sctMin( color[ 1 ] * color[ 3 ], 0xFF ) );
        *cp   = ( unsigned char )( sctMin( color[ 0 ] * color[ 3 ], 0xFF ) );
        *cp++ = ( unsigned char )( color[ 3 ] );        
        break;

     default:
         SCT_ASSERT_ALWAYS( 0 );
         break;
    }
}

/*!
 * Macros for YUV conversion.
 *
 */
#define rgbtoy( r, g, b, y )            y = ( unsigned char )( ( ( int )( 30 * ( r ) ) + ( int )( 59 * ( g ) ) + ( int )( 11 * ( b ) ) ) / 100 )

#define rgbtoyuv( r, g, b, y, u, v )    rgbtoy( ( b ), ( g ), ( r ), ( y ) ); \
                                        u = ( unsigned char )( ( ( int )( -17 * ( r ) ) - ( int )( 33 * ( g ) ) + ( int )( 50 * ( b ) ) + 12800 ) / 100 ); \
                                        v = ( unsigned char )( ( ( int )( 50 * ( r ) ) - ( int )( 42 * ( g ) ) - ( int )( 8 * ( b ) ) + 12800 ) / 100 )

/*!
 *
 *
 */
static SCTImagesData* sctiCreateYUV12Data( SCTImagesFormat srcFormat, int srcWidth, int srcHeight, const void* srcData )
{
    int             dataLength;
    unsigned char*  buf;
    SCTImagesData*  imagesData;

    ( void )( srcData );
    
    if( srcFormat != IMAGES_RGB8 || ( srcWidth % 2 ) != 0 || ( srcHeight % 2 ) != 0 )
    {
        return NULL;
    }

    /* Y + UV */
    dataLength = srcWidth * srcHeight + ( srcWidth / 2 * srcHeight / 2 ) * 2;

    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, dataLength  ) );
    if( buf == NULL )
    {
        return NULL;
    }

    imagesData = sctImagesCreateData( IMAGES_YUV12, srcWidth, srcHeight, buf, dataLength, SCT_FALSE );
    if( imagesData == NULL )
    {
        siCommonMemoryFree( NULL, buf );
        return NULL;
    }

    buf = NULL; /* buf moved into imagesData */

    {
        /* Adapted from
         * http://samplecodebank.blogspot.com/2011/05/rgb-to-yv12-conversion-code-in-c.html. Seems
         * good enough for doing the conversion outside the benchmark loop. */
        
        unsigned int            planeSize;
        unsigned int            halfWidth;

        unsigned char*          yplane;
        unsigned char*          uplane;
        unsigned char*          vplane;
        const unsigned char*    rgbIndex;

        int                     x, y;
        unsigned char*          yline;
        unsigned char*          uline;
        unsigned char*          vline;
        const int               rgbIncrement = 3;
        
        planeSize = srcWidth * srcHeight;
        halfWidth = srcWidth >> 1;

        yplane   = ( unsigned char* )( imagesData->data );
        uplane   = ( unsigned char* )( imagesData->data ) + planeSize;
        vplane   = ( unsigned char* )( imagesData->data ) + planeSize + ( planeSize >> 2 );
        rgbIndex = ( const unsigned char* )( srcData );
        
        for( y = 0; y < srcHeight; ++y )
        {
            yline = yplane + ( y * srcWidth );
            uline = uplane + ( ( y >> 1 ) * halfWidth );
            vline = vplane + ( ( y >> 1 ) * halfWidth );

            for( x = 0; x < srcWidth; x += 2 )
            {
                rgbtoyuv( rgbIndex[ 0 ], rgbIndex[ 1 ], rgbIndex[ 2 ], *yline, *uline, *vline );
                rgbIndex += rgbIncrement;
                yline++;
                rgbtoyuv( rgbIndex[ 0 ], rgbIndex[ 1 ], rgbIndex[ 2 ], *yline, *uline, *vline );
                rgbIndex += rgbIncrement;
                yline++;
                uline++;
                vline++;
            }
        }
    }
    
    return imagesData;
}
