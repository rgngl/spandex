/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_imagescopytoopengles1action.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_OPENGLES1_MODULE )
# include "sct_opengles1module.h"
#endif  /* defined( INCLUDE_OPENGLES1_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateImagesCopyToOpenGLES1ActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTImagesCopyToOpenGLES1ActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTImagesCopyToOpenGLES1ActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTImagesCopyToOpenGLES1ActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CopyToOpenGLES1@Images action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTImagesCopyToOpenGLES1ActionContext ) );

    context->moduleContext = ( SCTImagesModuleContext* )( moduleContext );

    if( sctiParseImagesCopyToOpenGLES1ActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyImagesCopyToOpenGLES1ActionContext( context );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesCopyToOpenGLES1ActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in CopyToOpenGLES1@Images context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyImagesCopyToOpenGLES1ActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCopyToOpenGLES1ActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( INCLUDE_OPENGLES1_MODULE )
    SCT_LOG_ERROR( "CopyToOpenGLES1@Images action init OpenGLES1 module not available." );
    return SCT_FALSE;
#else   /* !defined( INCLUDE_OPENGLES1_MODULE ) */
    return SCT_TRUE;    
#endif  /* !defined( INCLUDE_OPENGLES1_MODULE ) */  
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCopyToOpenGLES1ActionExecute( SCTAction* action, int frameNumber )
{
#if defined( INCLUDE_OPENGLES1_MODULE )
    SCTImagesCopyToOpenGLES1ActionContext*  context;
    ImagesCopyToOpenGLES1ActionData*        data;
    SCTImagesData*                          srcData;
    SCTOpenGLES1Data*                       dstData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTImagesCopyToOpenGLES1ActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiImagesModuleGetData( context->moduleContext, data->dataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data index in CopyToOpenGLES1@Images action execute." );
        return SCT_FALSE;
    }

    dstData = sctOpenGLES1CreateData( srcData->data, srcData->length, SCT_TRUE );
    if( dstData == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CopyToOpenGLES1@Images action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES1ModuleSetDataExt( data->openGLES1DataIndex, dstData ) == SCT_FALSE )
    {
        sctOpenGLES1DestroyData( dstData );        
        SCT_LOG_ERROR( "Copying data to OpenGLES1 failed in CopyToOpenGLES1@Images action execute." );
        return SCT_FALSE;
    }   

    return SCT_TRUE;
#else   /* defined( INCLUDE_OPENGLES1_MODULE ) */
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );
    SCT_ASSERT_ALWAYS( 0 );
    
    return SCT_FALSE;
#endif  /* defined( INCLUDE_OPENGLES1_MODULE ) */  
}

/*!
 *
 *
 */
void sctiImagesCopyToOpenGLES1ActionTerminate( SCTAction* action )
{
    SCTImagesCopyToOpenGLES1ActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTImagesCopyToOpenGLES1ActionContext* )( action->context );

#if defined( INCLUDE_OPENGLES1_MODULE )    
    sctOpenGLES1ModuleSetDataExt( context->data.openGLES1DataIndex, NULL );
#else   /* defined( INCLUDE_OPENGLES1_MODULE ) */
    SCT_USE_VARIABLE( context );
#endif  /* defined( INCLUDE_OPENGLES1_MODULE ) */    
}

/*!
 *
 *
 */
void sctiImagesCopyToOpenGLES1ActionDestroy( SCTAction* action )
{
    SCTImagesCopyToOpenGLES1ActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTImagesCopyToOpenGLES1ActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyImagesCopyToOpenGLES1ActionContext( context );
}
