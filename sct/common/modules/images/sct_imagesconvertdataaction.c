/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_imagesconvertdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateImagesConvertDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTImagesConvertDataActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTImagesConvertDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTImagesConvertDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ConvertData@Images action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTImagesConvertDataActionContext ) );

    context->moduleContext = ( SCTImagesModuleContext* )( moduleContext );

    if( sctiParseImagesConvertDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyImagesConvertDataActionContext( context );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.srcDataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesConvertDataActionContext( context );
        SCT_LOG_ERROR( "Invalid source data index in ConvertData@Images context creation." );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.dstDataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesConvertDataActionContext( context );
        SCT_LOG_ERROR( "Invalid destination data index in ConvertData@Images context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyImagesConvertDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiImagesConvertDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiImagesConvertDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTImagesConvertDataActionContext*  context;
    ImagesConvertDataActionData*        data;
    SCTImagesData*                      srcData;
    SCTImagesData*                      dstData;    

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTImagesConvertDataActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiImagesModuleGetData( context->moduleContext, data->srcDataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data in ConvertData@Images action execute." );
        return SCT_FALSE;
    }

    dstData = sctiImagesModuleGetData( context->moduleContext, data->dstDataIndex );
    if( dstData != NULL )
    {
        SCT_LOG_ERROR( "Destination data index already used in ConvertData@Images action execute." );
        return SCT_FALSE;
    }
    
    dstData = sctImagesCreateConvertedData( srcData->format,
                                            srcData->width,
                                            srcData->height,
                                            srcData->data,
                                            data->format );
    
    if( dstData == NULL )
    {
        SCT_LOG_ERROR( "Convert failed in ConvertData@Images action execute." );
        return SCT_FALSE;
    }
        
    sctiImagesModuleSetData( context->moduleContext, data->dstDataIndex, dstData );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiImagesConvertDataActionTerminate( SCTAction* action )
{
    SCTImagesConvertDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTImagesConvertDataActionContext* )( action->context );

    sctiImagesModuleDeleteData( context->moduleContext, context->data.dstDataIndex );
}

/*!
 *
 *
 */
void sctiImagesConvertDataActionDestroy( SCTAction* action )
{
    SCTImagesConvertDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTImagesConvertDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyImagesConvertDataActionContext( context );
}
