#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# Images module provides support for build-in image data which can be used
# without separate image data files.

######################################################################
SetModuleName( 'Images' )

######################################################################
AddInclude( 'sct_imagesmodule.h' )

######################################################################
MAX_DATA_INDEX  = 256

######################################################################
# CreateData. Create image data to the specified data index. Type specifies the
# kind of image to create. Format specifies the image data format. Width and
# height specify the image data width and height, with following format specific
# restrictions. With RGB/A formats, data is currently derived from the 512*512
# RGBA8 reference image by resampling and potential format conversion. IMAGES_A8
# data is created from the reference image by averaging RGBA channels. A8 or
# RGB/A8 data has no size restrictions. With ETC1_RGB8 and PVRTC formats, data
# is provide only in 1x1 - 1024x1024 square POT sizes. Note that currently
# ETC1_RGB8 provides visually incorrect data for sizes 1 and 2. Similarly, PVRTC
# provides visually incorrect data for sizes 1, 2, 4 and 8. Visually invalid
# data is provided anyway to support complete textures for mipmapping. Image
# data is destroyed during action terminate, if still valid.
AddActionConfig( 'CreateData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(        'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   EnumAttribute(       'Type', 'SCTImagesType',                [ 'IMAGES_FLOWER' ] ),
                   EnumAttribute(       'Format', 'SCTImagesFormat',            [ 'IMAGES_A8',
                                                                                  'IMAGES_L8',
                                                                                  'IMAGES_RGBA5551',
                                                                                  'IMAGES_RGBA4444',
                                                                                  'IMAGES_RGB565',
                                                                                  'IMAGES_BGR565',
                                                                                  'IMAGES_RGB8',
                                                                                  'IMAGES_BGR8',
                                                                                  'IMAGES_RGBX8',
                                                                                  'IMAGES_XBGR8',
                                                                                  'IMAGES_RGBA8',
                                                                                  'IMAGES_ABGR8',
                                                                                  'IMAGES_RGBA8_PRE',
                                                                                  'IMAGES_ABGR8_PRE', 
                                                                                  'IMAGES_XRGB8',
                                                                                  'IMAGES_BGRX8',
                                                                                  'IMAGES_ARGB8',
                                                                                  'IMAGES_BGRA8', 
                                                                                  'IMAGES_ARGB8_PRE',
                                                                                  'IMAGES_BGRA8_PRE',
                                                                                  'IMAGES_ETC1_RGB8',
                                                                                  'IMAGES_PVRTC_RGB4',
                                                                                  'IMAGES_PVRTC_RGB2',
                                                                                  'IMAGES_PVRTC_RGBA4',
                                                                                  'IMAGES_PVRTC_RGBA2' ] ),
                   IntAttribute(        'Width',                                1 ),
                   IntAttribute(        'Height',                               1 )
                   ]
                 )

######################################################################
# CropData. Select an image data subreqion from the source data index and copy
# it to the destination data index. Crop area must reside inside the source
# data. With ETC1 and PVRTC texture data format, the crop x, y, width, and
# height must be a multiple of 4. With 2BPP PVRTC, width and height must be
# larger than 16. With 4BPP PVRTC, width and height must be larger than 8. A
# PVRTC crop area must be a square. Destination image data is destroyed during
# action terminate, if still valid.
AddActionConfig( 'CropData',
                 'Init'+'Execute'+'Terminate'+'Destroy',                 
                 [ IntAttribute(        'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(        'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(        'X',                                    0 ),
                   IntAttribute(        'Y',                                    0 ),
                   IntAttribute(        'Width',                                1 ),
                   IntAttribute(        'Height',                               1 )
                   ]
                 )

######################################################################
# ResampleData. Resample the image data in the source data index and copy it to
# the destination data index, doing a format conversion if needed. Only RGBA8
# image data format is currently supported as source. Destination image data is
# destroyed during action terminate, if still valid.
AddActionConfig( 'ResampleData',
                 'Init'+'Execute'+'Terminate'+'Destroy',                 
                 [ IntAttribute(        'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(        'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   EnumAttribute(       'Format', 'SCTImagesFormat',            [ 'IMAGES_A8',
                                                                                  'IMAGES_L8',
                                                                                  'IMAGES_RGBA5551',
                                                                                  'IMAGES_RGBA4444',
                                                                                  'IMAGES_RGB565',
                                                                                  'IMAGES_RGB8',
                                                                                  'IMAGES_BGR8',
                                                                                  'IMAGES_RGBX8',
                                                                                  'IMAGES_XBGR8',
                                                                                  'IMAGES_RGBA8',
                                                                                  'IMAGES_ABGR8',
                                                                                  'IMAGES_RGBA8_PRE',
                                                                                  'IMAGES_ABGR8_PRE', 
                                                                                  'IMAGES_XRGB8',
                                                                                  'IMAGES_BGRX8',
                                                                                  'IMAGES_ARGB8',
                                                                                  'IMAGES_BGRA8', 
                                                                                  'IMAGES_ARGB8_PRE',
                                                                                  'IMAGES_BGRA8_PRE' ] ),
                   IntAttribute(        'Width',                                1 ),
                   IntAttribute(        'Height',                               1 )
                   ]
                 )

######################################################################
# ConvertData. Convert the source data to format and copy it to the destination
# data index. Destination image data is destroyed during action terminate, if
# still valid. Different formats may have special requirements for the source
# data format and size; IMAGES_YUV12 requires RGB8 source data with width and
# height divisible by 2.
AddActionConfig( 'ConvertData',
                 'Init'+'Execute'+'Terminate'+'Destroy',                 
                 [ IntAttribute(        'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(        'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   EnumAttribute(       'Format', 'SCTImagesFormat',            [ 'IMAGES_YUV12' ] ),
                   ]
                 )

######################################################################
# DeleteData. Delete the data specified by the data index, and set the data
# index as free.
AddActionConfig( 'DeleteData',
                 'Execute'+'Destroy',
                 [ IntAttribute(        'DataIndex',                            0, MAX_DATA_INDEX - 1 )
                   ]
                 )

######################################################################
# CopyToEgl. Copy image data to Egl.
AddActionConfig( 'CopyToEgl',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(        'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(        'EglDataIndex',                         0 ),
                   ]
                 )

######################################################################
# CopyToOpenVG. Copy image data to OpenVG.
AddActionConfig( 'CopyToOpenVG',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(        'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(        'OpenVGDataIndex',                      0 ),
                   ]
                 )

######################################################################
# CopyToOpenGLES1. Copy image data to OpenGLES1.
AddActionConfig( 'CopyToOpenGLES1',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(        'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(        'OpenGLES1DataIndex',                   0 ),                   
                   ]
                 )

######################################################################
# CopyToOpenGLES2. Copy image data to OpenGLES2.
AddActionConfig( 'CopyToOpenGLES2',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(        'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(        'OpenGLES2DataIndex',                   0 ),                   
                   ]
                 )

######################################################################
# END OF FILE
######################################################################
