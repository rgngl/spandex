/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/* begin binary data: */
const char bin_data_etc1_8[] = /* 84 */
{0x34,0x00,0x00,0x00,0x08,0x00,0x00,0x00,0x08,0x00,0x00,0x00,0x00,0x00,0x00
,0x00,0x36,0x00,0x01,0x00,0x20,0x00,0x00,0x00,0x04,0x00,0x00,0x00,0xFF,0xFF
,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0x50
,0x56,0x52,0x21,0x01,0x00,0x00,0x00,0x49,0x49,0x39,0x69,0x11,0x15,0xFB,0xB5
,0x25,0x25,0x25,0x91,0xF7,0x11,0xC0,0xCA,0x77,0x76,0x63,0x88,0xAB,0xA8,0xCA
,0x8F,0x74,0x74,0x73,0x99,0xFD,0x88,0x30,0x35};
/* end binary data. size = 84 bytes */

