/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_imagescopytoeglaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_EGL_MODULE )
# include "sct_eglmodule.h"
#endif  /* defined( INCLUDE_EGL_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateImagesCopyToEglActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTImagesCopyToEglActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTImagesCopyToEglActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTImagesCopyToEglActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CopyToEgl@Images action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTImagesCopyToEglActionContext ) );

    context->moduleContext = ( SCTImagesModuleContext* )( moduleContext );

    if( sctiParseImagesCopyToEglActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyImagesCopyToEglActionContext( context );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesCopyToEglActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in CopyToEgl@Images context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyImagesCopyToEglActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCopyToEglActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( INCLUDE_EGL_MODULE )
    SCT_LOG_ERROR( "CopyToEgl@Images action init EGL module not available." );
    return SCT_FALSE;
#else   /* !defined( INCLUDE_EGL_MODULE ) */
    return SCT_TRUE;    
#endif  /* !defined( INCLUDE_EGL_MODULE ) */  
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCopyToEglActionExecute( SCTAction* action, int frameNumber )
{
#if defined( INCLUDE_EGL_MODULE )
    SCTImagesCopyToEglActionContext*    context;
    ImagesCopyToEglActionData*          data;
    SCTImagesData*                      srcData;
    SCTEglData*                         dstData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTImagesCopyToEglActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiImagesModuleGetData( context->moduleContext, data->dataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data index in CopyToEgl@Images action execute." );
        return SCT_FALSE;
    }

    dstData = sctEglCreateData( srcData->data, srcData->length, SCT_TRUE );
    if( dstData == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CopyToEgl@Images action execute." );
        return SCT_FALSE;
    }

    if( sctEglModuleSetDataExt( data->eglDataIndex, dstData ) == SCT_FALSE )
    {
        sctEglDestroyData( dstData );        
        SCT_LOG_ERROR( "Copying data to Egl failed in CopyToEgl@Images action execute." );
        return SCT_FALSE;
    }   

    return SCT_TRUE;
#else   /* defined( INCLUDE_EGL_MODULE ) */
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );
    SCT_ASSERT_ALWAYS( 0 );
    
    return SCT_FALSE;
#endif  /* defined( INCLUDE_EGL_MODULE ) */  
}

/*!
 *
 *
 */
void sctiImagesCopyToEglActionTerminate( SCTAction* action )
{
    SCTImagesCopyToEglActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTImagesCopyToEglActionContext* )( action->context );

#if defined( INCLUDE_EGL_MODULE )    
    sctEglModuleSetDataExt( context->data.eglDataIndex, NULL );
#else   /* defined( INCLUDE_EGL_MODULE ) */
    SCT_USE_VARIABLE( context );
#endif  /* defined( INCLUDE_EGL_MODULE ) */    
}

/*!
 *
 *
 */
void sctiImagesCopyToEglActionDestroy( SCTAction* action )
{
    SCTImagesCopyToEglActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTImagesCopyToEglActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyImagesCopyToEglActionContext( context );
}
