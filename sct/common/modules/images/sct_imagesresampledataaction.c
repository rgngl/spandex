/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_imagesresampledataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateImagesResampleDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTImagesResampleDataActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTImagesResampleDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTImagesResampleDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ResampleData@Images action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTImagesResampleDataActionContext ) );

    context->moduleContext = ( SCTImagesModuleContext* )( moduleContext );

    if( sctiParseImagesResampleDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyImagesResampleDataActionContext( context );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.srcDataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesResampleDataActionContext( context );
        SCT_LOG_ERROR( "Invalid source data index in ResampleData@Images context creation." );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.dstDataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesResampleDataActionContext( context );
        SCT_LOG_ERROR( "Invalid destination data index in ResampleData@Images context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyImagesResampleDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiImagesResampleDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiImagesResampleDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTImagesResampleDataActionContext* context;
    ImagesResampleDataActionData*       data;
    SCTImagesData*                      srcData;
    SCTImagesData*                      dstData;    

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTImagesResampleDataActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiImagesModuleGetData( context->moduleContext, data->srcDataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data in ResampleData@Images action execute." );
        return SCT_FALSE;
    }

    dstData = sctiImagesModuleGetData( context->moduleContext, data->dstDataIndex );
    if( dstData != NULL )
    {
        SCT_LOG_ERROR( "Destination data index already used in ResampleData@Images action execute." );
        return SCT_FALSE;
    }

    if( srcData->format != IMAGES_RGBA8 )
    {
        SCT_LOG_ERROR( "Unsupported source image data format in ResampleData@Images action execute." );
        return SCT_FALSE;
    }   

    if( data->format != IMAGES_A8        &&
        data->format != IMAGES_L8        &&
        data->format != IMAGES_RGBA5551  &&
        data->format != IMAGES_RGBA4444  &&        
        data->format != IMAGES_RGB565    &&
        data->format != IMAGES_BGR565    &&        
        data->format != IMAGES_RGB8      &&
        data->format != IMAGES_BGR8      &&  
        data->format != IMAGES_RGBX8     &&
        data->format != IMAGES_XBGR8     &&
        data->format != IMAGES_RGBA8     &&
        data->format != IMAGES_ABGR8     &&
        data->format != IMAGES_RGBA8_PRE &&
        data->format != IMAGES_ABGR8_PRE &&     
        data->format != IMAGES_XRGB8     &&
        data->format != IMAGES_BGRX8     &&  
        data->format != IMAGES_ARGB8     &&
        data->format != IMAGES_BGRA8     && 
        data->format != IMAGES_ARGB8_PRE &&
        data->format != IMAGES_BGRA8_PRE )
    {
        SCT_LOG_ERROR( "Unsupported destination image data format in ResampleData@Images action execute." );
        return SCT_FALSE;
    }   
    
    dstData = sctImagesCreateRescaledRGBData( srcData->format,
                                              srcData->width,
                                              srcData->height,
                                              srcData->data,
                                              data->format,
                                              data->width,
                                              data->height );
    
    if( dstData == NULL )
    {
        SCT_LOG_ERROR( "Resample failed in ResampleData@Images action execute." );
        return SCT_FALSE;
    }
        
    sctiImagesModuleSetData( context->moduleContext, data->dstDataIndex, dstData );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiImagesResampleDataActionTerminate( SCTAction* action )
{
    SCTImagesResampleDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTImagesResampleDataActionContext* )( action->context );

    sctiImagesModuleDeleteData( context->moduleContext, context->data.dstDataIndex );
}

/*!
 *
 *
 */
void sctiImagesResampleDataActionDestroy( SCTAction* action )
{
    SCTImagesResampleDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTImagesResampleDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyImagesResampleDataActionContext( context );
}
