/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_imagescropdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateImagesCropDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTImagesCropDataActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTImagesCropDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTImagesCropDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CropData@Images action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTImagesCropDataActionContext ) );

    context->moduleContext = ( SCTImagesModuleContext* )( moduleContext );

    if( sctiParseImagesCropDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyImagesCropDataActionContext( context );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.srcDataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesCropDataActionContext( context );
        SCT_LOG_ERROR( "Invalid source data index in CropData@Images context creation." );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.dstDataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesCropDataActionContext( context );
        SCT_LOG_ERROR( "Invalid destination data index in CropData@Images context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyImagesCropDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCropDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCropDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTImagesCropDataActionContext* context;
    ImagesCropDataActionData*       data;
    SCTImagesData*                  srcData;
    SCTImagesData*                  dstData;    

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTImagesCropDataActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiImagesModuleGetData( context->moduleContext, data->srcDataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data in CropData@Images action execute." );
        return SCT_FALSE;
    }

    dstData = sctiImagesModuleGetData( context->moduleContext, data->dstDataIndex );
    if( dstData != NULL )
    {
        SCT_LOG_ERROR( "Destination data index already used in CropData@Images action execute." );
        return SCT_FALSE;
    }

    if( ( data->x + data->width ) > ( srcData->width ) ||
        ( data->y + data->height ) > ( srcData->height ) )
    {
        SCT_LOG_ERROR( "Invalid crop area in CropData@Images action execute." );
        return SCT_FALSE;
    }       

    if( srcData->format == IMAGES_A8        ||
        srcData->format == IMAGES_L8        ||
        srcData->format == IMAGES_RGBA5551  ||
        srcData->format == IMAGES_RGBA4444  ||
        srcData->format == IMAGES_RGB565    ||
        srcData->format == IMAGES_BGR565    ||        
        srcData->format == IMAGES_RGB8      ||
        srcData->format == IMAGES_BGR8      ||  
        srcData->format == IMAGES_RGBX8     ||
        srcData->format == IMAGES_XBGR8     ||
        srcData->format == IMAGES_RGBA8     ||
        srcData->format == IMAGES_ABGR8     ||
        srcData->format == IMAGES_RGBA8_PRE ||
        srcData->format == IMAGES_ABGR8_PRE ||     
        srcData->format == IMAGES_XRGB8     ||
        srcData->format == IMAGES_BGRX8     ||  
        srcData->format == IMAGES_ARGB8     ||
        srcData->format == IMAGES_BGRA8     || 
        srcData->format == IMAGES_ARGB8_PRE ||
        srcData->format == IMAGES_BGRA8_PRE )
    {
        dstData = sctImagesCreateCroppedRGBData( srcData, data->x, data->y, data->width, data->height );
    }
    else if( srcData->format == IMAGES_ETC1_RGB8 )
    {
        if( ( data->x % 4 != 0 )     ||
            ( data->width % 4 != 0 ) ||
            ( data->y % 4 != 0 )     ||
            ( data->height % 4 != 0 ) )
        {
            SCT_LOG_ERROR( "Invalid crop area for ETC1 data in CropData@Images action execute." );
            return SCT_FALSE;
        }
        
        dstData = sctImagesCreateCroppedETC1Data( srcData, data->x, data->y, data->width, data->height );
    }
    else if( srcData->format == IMAGES_PVRTC_RGB4 ||
             srcData->format == IMAGES_PVRTC_RGBA4 )
    {
        if( ( data->width < 8 )  ||
            ( data->height < 8 ) ||
            ( data->width != data->height ) ||
            ( data->x % 4 != 0 )     ||
            ( data->width % 4 != 0 ) ||
            ( data->y % 4 != 0 )     ||
            ( data->height % 4 != 0 ) )
        {
            SCT_LOG_ERROR( "Invalid crop area for 4BPP PVRTC data in CropData@Images action execute." );
            return SCT_FALSE;
        }

        dstData = sctImagesCreateCroppedPVRTCData( srcData, data->x, data->y, data->width, data->height );
    }
    else if( srcData->format == IMAGES_PVRTC_RGB2 ||
             srcData->format == IMAGES_PVRTC_RGBA2 )
    {
        if( ( data->width < 16 )  ||
            ( data->height < 16 ) ||
            ( data->width != data->height ) ||
            ( data->x % 4 != 0 )     ||
            ( data->width % 4 != 0 ) ||
            ( data->y % 4 != 0 )     ||
            ( data->height % 4 != 0 ) )
        {
            SCT_LOG_ERROR( "Invalid crop area for 2BPP PVRTC data in CropData@Images action execute." );            
            return SCT_FALSE;
        }
        dstData = sctImagesCreateCroppedPVRTCData( srcData, data->x, data->y, data->width, data->height );
    }
    else
    {
        
        SCT_LOG_ERROR( "Unsupported data format in CropData@Images action execute." );
        return SCT_FALSE;
    }
    
    if( dstData == NULL )
    {
        SCT_LOG_ERROR( "Crop failed in CropData@Images action execute." );
        return SCT_FALSE;
    }
        
    sctiImagesModuleSetData( context->moduleContext, data->dstDataIndex, dstData );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiImagesCropDataActionTerminate( SCTAction* action )
{
    SCTImagesCropDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTImagesCropDataActionContext* )( action->context );

    sctiImagesModuleDeleteData( context->moduleContext, context->data.dstDataIndex );
}

/*!
 *
 *
 */
void sctiImagesCropDataActionDestroy( SCTAction* action )
{
    SCTImagesCropDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTImagesCropDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyImagesCropDataActionContext( context );
}
