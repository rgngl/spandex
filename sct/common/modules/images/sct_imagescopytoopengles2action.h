/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_IMAGESCOPYTOOPENGLES2ACTION_H__ )
#define __SCT_IMAGESCOPYTOOPENGLES2ACTION_H__

#include "sct_types.h"
#include "sct_imagesmodule.h"
#include "sct_imagesmodule_parser.h"

/*!
 *
 */
typedef struct
{
    SCTImagesModuleContext*             moduleContext;
    ImagesCopyToOpenGLES2ActionData     data;
} SCTImagesCopyToOpenGLES2ActionContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                               sctiCreateImagesCopyToOpenGLES2ActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroyImagesCopyToOpenGLES2ActionContext( void* context );

    SCTBoolean                          sctiImagesCopyToOpenGLES2ActionInit( SCTAction* action, SCTBenchmark* benchmark );
    SCTBoolean                          sctiImagesCopyToOpenGLES2ActionExecute( SCTAction* action, int frameNumber );
    void                                sctiImagesCopyToOpenGLES2ActionTerminate( SCTAction* action );
    void                                sctiImagesCopyToOpenGLES2ActionDestroy( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_IMAGESCOPYTOOPENGLES2ACTION_H__ ) */
