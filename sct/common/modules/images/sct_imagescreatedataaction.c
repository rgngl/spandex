/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_imagescreatedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#if !defined( SCT_IMAGES_MODULE_EXCLUDE_RGB_DATA )
# define SCT_IMAGES_RGB_DATA_HEADER_LENGTH  52
# include "sct_images_rgba8_512.h"
#endif  /* !defined( SCT_IMAGES_MODULE_EXCLUDE_RGB_DATA ) */

#if !defined( SCT_IMAGES_MODULE_EXCLUDE_ETC_DATA )
# define SCT_IMAGES_ETC1_DATA_HEADER_LENGTH  52
# include "sct_images_etc1_4.h"
# include "sct_images_etc1_8.h"
# include "sct_images_etc1_16.h"
# include "sct_images_etc1_32.h"
# include "sct_images_etc1_64.h"
# include "sct_images_etc1_128.h"
# include "sct_images_etc1_256.h"
# include "sct_images_etc1_512.h"
# include "sct_images_etc1_1024.h"
#endif  /* !defined( SCT_IMAGES_MODULE_EXCLUDE_ETC_DATA ) */

#if !defined( SCT_IMAGES_MODULE_EXCLUDE_PVR_DATA )
# define SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH  52
# include "sct_images_pvr2b_16.h"
# include "sct_images_pvr2b_32.h"
# include "sct_images_pvr2b_64.h"
# include "sct_images_pvr2b_128.h"
# include "sct_images_pvr2b_256.h"
# include "sct_images_pvr2b_512.h"
# include "sct_images_pvr2b_1024.h"
# include "sct_images_pvr4b_16.h"
# include "sct_images_pvr4b_32.h"
# include "sct_images_pvr4b_64.h"
# include "sct_images_pvr4b_128.h"
# include "sct_images_pvr4b_256.h"
# include "sct_images_pvr4b_512.h"
# include "sct_images_pvr4b_1024.h"
# include "sct_images_pvr2ba_16.h"
# include "sct_images_pvr2ba_32.h"
# include "sct_images_pvr2ba_64.h"
# include "sct_images_pvr2ba_128.h"
# include "sct_images_pvr2ba_256.h"
# include "sct_images_pvr2ba_512.h"
# include "sct_images_pvr2ba_1024.h"
# include "sct_images_pvr4ba_16.h"
# include "sct_images_pvr4ba_32.h"
# include "sct_images_pvr4ba_64.h"
# include "sct_images_pvr4ba_128.h"
# include "sct_images_pvr4ba_256.h"
# include "sct_images_pvr4ba_512.h"
# include "sct_images_pvr4ba_1024.h"
#endif  /* !defined( SCT_IMAGES_MODULE_EXCLUDE_PVR_DATA ) */

/*!
 *
 *
 */
void* sctiCreateImagesCreateDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTImagesCreateDataActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTImagesCreateDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTImagesCreateDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateData@Images action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTImagesCreateDataActionContext ) );
    
    context->moduleContext = ( SCTImagesModuleContext* )( moduleContext );

    if( sctiParseImagesCreateDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyImagesCreateDataActionContext( context );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesCreateDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in CreateData@Images context creation." );
        return NULL;
    }

    /* Set data pointer and data length. */
    switch( context->data.format )
    {
    case IMAGES_A8:
    case IMAGES_L8:
    case IMAGES_RGBA5551:
    case IMAGES_RGBA4444:
    case IMAGES_RGB565:
    case IMAGES_BGR565:        
    case IMAGES_RGB8:
    case IMAGES_BGR8:        
    case IMAGES_RGBX8:
    case IMAGES_XBGR8:        
    case IMAGES_RGBA8:
    case IMAGES_ABGR8:        
    case IMAGES_RGBA8_PRE:
    case IMAGES_ABGR8_PRE:        
    case IMAGES_XRGB8:
    case IMAGES_BGRX8:        
    case IMAGES_ARGB8:
    case IMAGES_BGRA8:        
    case IMAGES_ARGB8_PRE:
    case IMAGES_BGRA8_PRE:        
        
#if !defined( SCT_IMAGES_MODULE_EXCLUDE_RGB_DATA )
        context->ptr = bin_data_rgba8_512;
        context->len = sizeof( bin_data_rgba8_512 );
        
        /* Remove data header from the data and data length. */    
        context->ptr    += SCT_IMAGES_RGB_DATA_HEADER_LENGTH;
        context->len    -= SCT_IMAGES_RGB_DATA_HEADER_LENGTH;
        
#endif  /* !defined( SCT_IMAGES_MODULE_EXCLUDE_RGB565_DATA ) */
        
        break;
        
    case IMAGES_ETC1_RGB8:
        
#if !defined( SCT_IMAGES_MODULE_EXCLUDE_ETC_DATA )

       /* ETC textures are always square. */
        if( context->data.width != context->data.height )
        {
            sctiDestroyImagesCreateDataActionContext( context );
            SCT_LOG_ERROR( "Non-square ETC1 texture size size in CreateData@Images context creation." );
            return NULL;
        }
        
        switch( context->data.width )   
        {
        case 1:
        case 2:
        case 4:
            context->ptr = bin_data_etc1_4;
            context->len = sizeof( bin_data_etc1_4 );
            break;
        case 8:            
            context->ptr = bin_data_etc1_8;
            context->len = sizeof( bin_data_etc1_8 );
        case 16:            
            context->ptr = bin_data_etc1_16;
            context->len = sizeof( bin_data_etc1_16 );
        case 32:
            context->ptr = bin_data_etc1_32;
            context->len = sizeof( bin_data_etc1_32 );
            break;
        case 64:
            context->ptr = bin_data_etc1_64;
            context->len = sizeof( bin_data_etc1_64 );
            break;
        case 128:
            context->ptr = bin_data_etc1_128;
            context->len = sizeof( bin_data_etc1_128 );
            break;
        case 256:
            context->ptr = bin_data_etc1_256;
            context->len = sizeof( bin_data_etc1_256 );
            break;
        case 512:
            context->ptr = bin_data_etc1_512;
            context->len = sizeof( bin_data_etc1_512 );
            break;
        case 1024:
            context->ptr = bin_data_etc1_1024;
            context->len = sizeof( bin_data_etc1_1024 );
            break;
        default:
            sctiDestroyImagesCreateDataActionContext( context );
            SCT_LOG_ERROR( "Unexpected ETC1 size in CreateData@Images context creation." );
            return NULL;
        }
        
        /* Remove data header from the data and data length. */    
        context->ptr += SCT_IMAGES_ETC1_DATA_HEADER_LENGTH;
        context->len -= SCT_IMAGES_ETC1_DATA_HEADER_LENGTH;
        
#endif  /* !defined( SCT_IMAGES_MODULE_EXCLUDE_ETC_DATA ) */
        
        break;
        
    case IMAGES_PVRTC_RGB2:
        
#if !defined( SCT_IMAGES_MODULE_EXCLUDE_PVR_DATA )

       /* PVR textures are always square. */
        if( context->data.width != context->data.height )
        {
            sctiDestroyImagesCreateDataActionContext( context );
            SCT_LOG_ERROR( "Non-square PVR texture size size in CreateData@Images context creation." );
            return NULL;
        }
        
        switch( context->data.width )   
        {
        case 1:
        case 2:
        case 4:
        case 8:
            context->ptr = bin_data_pvr2b_16;
            context->len = ( 16 * 8 * 2 + 7 ) / 8 + SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
            break;
        case 16:
            context->ptr = bin_data_pvr2b_16;
            context->len = sizeof( bin_data_pvr2b_16 );
            break;
        case 32:
            context->ptr = bin_data_pvr2b_32;
            context->len = sizeof( bin_data_pvr2b_32 );
            break;
        case 64:
            context->ptr = bin_data_pvr2b_64;
            context->len = sizeof( bin_data_pvr2b_64 );
            break;
        case 128:
            context->ptr = bin_data_pvr2b_128;
            context->len = sizeof( bin_data_pvr2b_128 );
            break;
        case 256:
            context->ptr = bin_data_pvr2b_256;
            context->len = sizeof( bin_data_pvr2b_256 );
            break;
        case 512:
            context->ptr = bin_data_pvr2b_512;
            context->len = sizeof( bin_data_pvr2b_512 );
            break;
        case 1024:
            context->ptr = bin_data_pvr2b_1024;
            context->len = sizeof( bin_data_pvr2b_1024 );
            break;
        default:
            sctiDestroyImagesCreateDataActionContext( context );
            SCT_LOG_ERROR( "Unexpected PVRTC size in CreateData@Images context creation." );
            return NULL;
        }

        /* Remove data header from the data and data length. */    
        context->ptr += SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
        context->len -= SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
        
#endif  /* !defined( SCT_IMAGES_MODULE_EXCLUDE_PVR_DATA ) */
        
        break;
        
    case IMAGES_PVRTC_RGB4:
        
#if !defined( SCT_IMAGES_MODULE_EXCLUDE_PVR_DATA )

       /* PVR textures are always square. */
        if( context->data.width != context->data.height )
        {
            sctiDestroyImagesCreateDataActionContext( context );
            SCT_LOG_ERROR( "Non-square PVR texture size size in CreateData@Images context creation." );
            return NULL;
        }
        
        switch( context->data.width )
        {
        case 1:
        case 2:
        case 4:
        case 8:            
            context->ptr = bin_data_pvr4b_16;
            context->len = ( 8 * 8 * 4 + 7 ) / 8 + SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
            break;
        case 16:
            context->ptr = bin_data_pvr4b_16;
            context->len = sizeof( bin_data_pvr4b_16 );
            break;
        case 32:
            context->ptr = bin_data_pvr4b_32;
            context->len = sizeof( bin_data_pvr4b_32 );
            break;
        case 64:
            context->ptr = bin_data_pvr4b_64;
            context->len = sizeof( bin_data_pvr4b_64 );
            break;
        case 128:
            context->ptr = bin_data_pvr4b_128;
            context->len = sizeof( bin_data_pvr4b_128 );
            break;
        case 256:
            context->ptr = bin_data_pvr4b_256;
            context->len = sizeof( bin_data_pvr4b_256 );
            break;
        case 512:
            context->ptr = bin_data_pvr4b_512;
            context->len = sizeof( bin_data_pvr4b_512 );
            break;
        case 1024:
            context->ptr = bin_data_pvr4b_1024;
            context->len = sizeof( bin_data_pvr4b_1024 );
            break;
        default:
            sctiDestroyImagesCreateDataActionContext( context );
            SCT_LOG_ERROR( "Unexpected PVRTC size in CreateData@Images context creation." );
            return NULL;
        }

        /* Remove data header from the data and data length. */    
        context->ptr += SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
        context->len -= SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
        
#endif  /* !defined( SCT_IMAGES_MODULE_EXCLUDE_PVR_DATA ) */        
        
        break;
        
    case IMAGES_PVRTC_RGBA2:

#if !defined( SCT_IMAGES_MODULE_EXCLUDE_PVR_DATA )

       /* PVR textures are always square. */
        if( context->data.width != context->data.height )
        {
            sctiDestroyImagesCreateDataActionContext( context );
            SCT_LOG_ERROR( "Non-square PVR texture size size in CreateData@Images context creation." );
            return NULL;
        }
        
        switch( context->data.width )
        {
        case 1:
        case 2:
        case 4:
        case 8:
            context->ptr = bin_data_pvr2ba_16;
            context->len = ( 16 * 8 * 2 + 7 ) / 8 + SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
            break;
        case 16:
            context->ptr = bin_data_pvr2ba_16;
            context->len = sizeof( bin_data_pvr2ba_16 );
            break;
        case 32:
            context->ptr = bin_data_pvr2ba_32;
            context->len = sizeof( bin_data_pvr2ba_32 );
            break;
        case 64:
            context->ptr = bin_data_pvr2ba_64;
            context->len = sizeof( bin_data_pvr2ba_64 );
            break;
        case 128:
            context->ptr = bin_data_pvr2ba_128;
            context->len = sizeof( bin_data_pvr2ba_128 );
            break;
        case 256:
            context->ptr = bin_data_pvr2ba_256;
            context->len = sizeof( bin_data_pvr2ba_256 );
            break;
        case 512:
            context->ptr = bin_data_pvr2ba_512;
            context->len = sizeof( bin_data_pvr2ba_512 );
            break;
        case 1024:
            context->ptr = bin_data_pvr2ba_1024;
            context->len = sizeof( bin_data_pvr2ba_1024 );
            break;
        default:
            sctiDestroyImagesCreateDataActionContext( context );
            SCT_LOG_ERROR( "Unexpected PVRTC size in CreateData@Images context creation." );
            return NULL;
        }

        /* Remove data header from the data and data length. */    
        context->ptr += SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
        context->len -= SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
        
#endif  /* !defined( SCT_IMAGES_MODULE_EXCLUDE_PVR_DATA ) */
        
        break;
        
    case IMAGES_PVRTC_RGBA4:

#if !defined( SCT_IMAGES_MODULE_EXCLUDE_PVR_DATA )

       /* PVR textures are always square. */
        if( context->data.width != context->data.height )
        {
            sctiDestroyImagesCreateDataActionContext( context );
            SCT_LOG_ERROR( "Non-square PVR texture size size in CreateData@Images context creation." );
            return NULL;
        }
        
        switch( context->data.width )
        {
        case 1:
        case 2:
        case 4:
        case 8:            
            context->ptr = bin_data_pvr4ba_16;
            context->len = ( 8 * 8 * 4 + 7 ) / 8 + SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
            break;
        case 16:
            context->ptr = bin_data_pvr4ba_16;
            context->len = sizeof( bin_data_pvr4ba_16 );
            break;
        case 32:
            context->ptr = bin_data_pvr4ba_32;
            context->len = sizeof( bin_data_pvr4ba_32 );
            break;
        case 64:
            context->ptr = bin_data_pvr4ba_64;
            context->len = sizeof( bin_data_pvr4ba_64 );
            break;
        case 128:
            context->ptr = bin_data_pvr4ba_128;
            context->len = sizeof( bin_data_pvr4ba_128 );
            break;
        case 256:
            context->ptr = bin_data_pvr4ba_256;
            context->len = sizeof( bin_data_pvr4ba_256 );
            break;
        case 512:
            context->ptr = bin_data_pvr4ba_512;
            context->len = sizeof( bin_data_pvr4ba_512 );
            break;
        case 1024:
            context->ptr = bin_data_pvr4ba_1024;
            context->len = sizeof( bin_data_pvr4ba_1024 );
            break;
        default:
            sctiDestroyImagesCreateDataActionContext( context );
            SCT_LOG_ERROR( "Unexpected PVRTC size in CreateData@Images context creation." );
            return NULL;
        }

        /* Remove data header from the data and data length. */    
        context->ptr += SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
        context->len -= SCT_IMAGES_PVRTC_DATA_HEADER_LENGTH;
        
#endif  /* !defined( SCT_IMAGES_MODULE_EXCLUDE_PVR_DATA ) */
        
        break;       
        
    default:
        sctiDestroyImagesCreateDataActionContext( context );        
        SCT_LOG_ERROR( "Unexpected format in CreateData@Images context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyImagesCreateDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCreateDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTImagesCreateDataActionContext*   context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTImagesCreateDataActionContext* )( action->context );
    
    if( context->ptr == NULL )
    {
        SCT_LOG_ERROR( "Needed data excluded from build in CreateData@Images action init." );    
        return SCT_FALSE;
    }

    SCT_ASSERT_ALWAYS( context->ptr != NULL );
    SCT_ASSERT_ALWAYS( context->len > 0 );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCreateDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTImagesCreateDataActionContext*   context;
    ImagesCreateDataActionData*         data;
    SCTImagesData*                      rawData;
    SCTImagesFormat                     format;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTImagesCreateDataActionContext* )( action->context );
    data    = &( context->data );

    rawData = sctiImagesModuleGetData( context->moduleContext, data->dataIndex );
    if( rawData != NULL )
    {
        SCT_LOG_ERROR( "Data index already used in CreateData@Images action execute." );
        return SCT_FALSE;
    }

    SCT_ASSERT( context->ptr != NULL );
    SCT_ASSERT( context->len > 0 );    

    format = data->format;

    if( format == IMAGES_A8             ||
        format == IMAGES_L8             ||
        format == IMAGES_RGBA5551       ||
        format == IMAGES_RGBA4444       ||
        format == IMAGES_RGB565         ||
        format == IMAGES_BGR565         ||        
        format == IMAGES_RGB8           ||
        format == IMAGES_BGR8           ||  
        format == IMAGES_RGBX8          ||
        format == IMAGES_XBGR8          ||
        format == IMAGES_RGBA8          ||
        format == IMAGES_ABGR8          ||
        format == IMAGES_RGBA8_PRE      ||
        format == IMAGES_ABGR8_PRE      ||     
        format == IMAGES_XRGB8          ||
        format == IMAGES_BGRX8          ||  
        format == IMAGES_ARGB8          ||
        format == IMAGES_BGRA8          || 
        format == IMAGES_ARGB8_PRE      ||
        format == IMAGES_BGRA8_PRE )
    {
        rawData = sctImagesCreateRescaledRGBData( IMAGES_RGBA8,
                                                  512, 512,
                                                  context->ptr,
                                                  format,
                                                  data->width,
                                                  data->height );
    }
    else
    {
        /* Casting the const ok since we do a copy from the data. */
        rawData = sctImagesCreateData( format,
                                       data->width,
                                       data->height,
                                       ( void* )( context->ptr ),
                                       context->len,
                                       SCT_TRUE );
    }
    
    if( rawData == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateData@Images action execute." );
        return SCT_FALSE;
    }
        
    sctiImagesModuleSetData( context->moduleContext, data->dataIndex, rawData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiImagesCreateDataActionTerminate( SCTAction* action )
{
    SCTImagesCreateDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTImagesCreateDataActionContext* )( action->context );

    sctiImagesModuleDeleteData( context->moduleContext, context->data.dataIndex );
}

/*!
 *
 *
 */
void sctiImagesCreateDataActionDestroy( SCTAction* action )
{
    SCTImagesCreateDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTImagesCreateDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyImagesCreateDataActionContext( context );
}
