/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_imagescopytoopenvgaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_OPENVG_MODULE )
# include "sct_openvgmodule.h"
#endif  /* defined( INCLUDE_OPENVG_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateImagesCopyToOpenVGActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTImagesCopyToOpenVGActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTImagesCopyToOpenVGActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTImagesCopyToOpenVGActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CopyToOpenVG@Images action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTImagesCopyToOpenVGActionContext ) );

    context->moduleContext = ( SCTImagesModuleContext* )( moduleContext );

    if( sctiParseImagesCopyToOpenVGActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyImagesCopyToOpenVGActionContext( context );
        return NULL;
    }

    if( sctiImagesModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyImagesCopyToOpenVGActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in CopyToOpenVG@Images context creation." );
        return NULL;
    }
   
    return context;
}

/*!
 *
 *
 */
void sctiDestroyImagesCopyToOpenVGActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCopyToOpenVGActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( INCLUDE_OPENVG_MODULE )
    SCT_LOG_ERROR( "CopyToOpenVG@Images action init OpenVG module not available." );
    return SCT_FALSE;
#else   /* !defined( INCLUDE_OPENVG_MODULE ) */
    return SCT_TRUE;    
#endif  /* !defined( INCLUDE_OPENVG_MODULE ) */  
}

/*!
 *
 *
 */
SCTBoolean sctiImagesCopyToOpenVGActionExecute( SCTAction* action, int frameNumber )
{
#if defined( INCLUDE_OPENVG_MODULE )
    SCTImagesCopyToOpenVGActionContext* context;
    ImagesCopyToOpenVGActionData*       data;
    SCTImagesData*                      srcData;
    SCTOpenVGData*                      dstData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTImagesCopyToOpenVGActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiImagesModuleGetData( context->moduleContext, data->dataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data index in CopyToOpenVG@Images action execute." );
        return SCT_FALSE;
    }

    dstData = sctOpenVGCreateData( srcData->data, srcData->length, SCT_TRUE );
    if( dstData == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CopyToOpenVG@Images action execute." );
        return SCT_FALSE;
    }

    if( sctOpenVGModuleSetDataExt( data->openVGDataIndex, dstData ) == SCT_FALSE )
    {
        sctOpenVGDestroyData( dstData );        
        SCT_LOG_ERROR( "Copying data to OpenVG failed in CopyToOpenVG@Images action execute." );
        return SCT_FALSE;
    }   

    return SCT_TRUE;
#else   /* defined( INCLUDE_OPENVG_MODULE ) */
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );
    SCT_ASSERT_ALWAYS( 0 );
    
    return SCT_FALSE;
#endif  /* defined( INCLUDE_OPENVG_MODULE ) */  
}

/*!
 *
 *
 */
void sctiImagesCopyToOpenVGActionTerminate( SCTAction* action )
{
    SCTImagesCopyToOpenVGActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTImagesCopyToOpenVGActionContext* )( action->context );

#if defined( INCLUDE_OPENVG_MODULE )    
    sctOpenVGModuleSetDataExt( context->data.openVGDataIndex, NULL );
#else   /* defined( INCLUDE_OPENVG_MODULE ) */
    SCT_USE_VARIABLE( context );
#endif  /* defined( INCLUDE_OPENVG_MODULE ) */    
}

/*!
 *
 *
 */
void sctiImagesCopyToOpenVGActionDestroy( SCTAction* action )
{
    SCTImagesCopyToOpenVGActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTImagesCopyToOpenVGActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyImagesCopyToOpenVGActionContext( context );
}
