#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

##############################################################################
SetModuleName( 'Vgi' )

##############################################################################
AddInclude( 'sct_vgimodule.h' )
AddInclude( 'VG/vgcontext.h' )

##############################################################################
AddActionConfig( 'CreateContext',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'ColorSpace', 'VGIColorSpace',          [ 'VGI_COLORSPACE_LINEAR',
                                                                                          'VGI_COLORSPACE_SRGB' ] ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

##############################################################################
AddActionConfig( 'DestroyContext',
                 'Execute'+'Destroy',
                 []
                 )

##############################################################################
AddActionConfig( 'Resize',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

##############################################################################
AddActionConfig( 'CopyToTarget',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   EnumAttribute(               'TargetFormat', 'VGIColorBufferFormat', [ 'VGI_COLOR_BUFFER_FORMAT_RGB565',
                                                                                          'VGI_COLOR_BUFFER_FORMAT_RGB888',
                                                                                          'VGI_COLOR_BUFFER_FORMAT_XRGB8888',
                                                                                          'VGI_COLOR_BUFFER_FORMAT_ARGB8888' ] ),
                   OnOffAttribute(              'Mask' ),
                   EnumAttribute(               'Hint', 'VGICopyToTargetHint',          [ 'VGI_SKIP_TRANSPARENT_PIXELS',
                                                                                          'VGI_COPY_TRANSPARENT_PIXELS' ] ),
                   OnOffAttribute(              'BlitToScreen' )
                   ],
                 [ ValidatorConfig( 'TargetFormat == VGI_COLOR_BUFFER_FORMAT_ARGB8888 && Mask != SCT_TRUE',
                                    'ARGB8888 format uses embedded mask' ) ]
                 )

######################################################################
# END OF FILE
######################################################################
