/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgicopytotargetaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include <VG/vgcontext.h>
#include "sct_sivgi.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateVgiCopyToTargetActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVgiCopyToTargetActionContext*    context;
    SCTVgiModuleContext*                mc; 

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTVgiModuleContext* )( moduleContext );
    context = ( SCTVgiCopyToTargetActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVgiCopyToTargetActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CopyToTarget@Vgi context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVgiCopyToTargetActionContext ) );

    context->moduleContext = mc;

    if( sctiParseVgiCopyToTargetActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVgiCopyToTargetActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVgiCopyToTargetActionContext( void* context )
{
    SCTVgiCopyToTargetActionContext*    c;
    if( context == NULL )
    {
        return;
    }

    c = ( SCTVgiCopyToTargetActionContext* )( context );
    SCT_ASSERT_ALWAYS( c->target == NULL );

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiVgiCopyToTargetActionInit( SCTAction* action, SCTBenchmark *benchmark )
{
    SCTVgiCopyToTargetActionContext*    context;
    VgiCopyToTargetActionData*          data;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( benchmark );

    context = ( SCTVgiCopyToTargetActionContext* )( action->context );
    data    = &( context->data );

    if( context->target != NULL )
    {
        return SCT_TRUE;
    }

    SCT_ASSERT_ALWAYS( context->moduleContext->siVgiContext != NULL );

    context->target = siVgiGetTarget( context->moduleContext->siVgiContext,
                                      data->width,
                                      data->height,
                                      data->targetFormat, 
                                      data->mask );
    if( context->target == NULL )
    {
        SCT_LOG_ERROR( "Target creation failed in CopyToTarget@Vgi action init." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiVgiCopyToTargetActionExecute( SCTAction* action, int frameNumber )
{
    SCTVgiCopyToTargetActionContext*    context;
    VgiCopyToTargetActionData*          data;
    SCTVgiTarget*                       target;
    int                                 status;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTVgiCopyToTargetActionContext* )( action->context );
    data    = &( context->data );
    target  = context->target;

    SCT_ASSERT_ALWAYS( target != NULL );

    if( data->width  != context->moduleContext->width ||
        data->height != context->moduleContext->height )
    {
        SCT_LOG_ERROR( "Context and target size mismatch in CopyToTarget@Vgi action execute." );
        return SCT_FALSE;
    }

    if( siVgiLockTarget( context->moduleContext->siVgiContext, target ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Target lock failed in CopyToTarget@Vgi action execute." );
        return SCT_FALSE;
    }

    status = VGICopyToTarget( target->format,
                              target->bitmapStride, 
                              target->bitmapData,
                              target->maskStride,
                              target->maskData,
                              data->hint );

    siVgiUnlockTarget( context->moduleContext->siVgiContext, target );

    if( status != VGI_OK )
    {
        SCT_LOG_ERROR( "CopyToTarget failed in CopyToTarget@Vgi action execute." );
        return SCT_FALSE;
    }

    if( data->blitToScreen == SCT_TRUE && 
        siVgiBlitTarget( context->moduleContext->siVgiContext, target ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Blit failed in CopyToTarget@Vgi action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiVgiCopyToTargetActionTerminate( SCTAction* action )
{
    SCTVgiCopyToTargetActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTVgiCopyToTargetActionContext* )( action->context );

    if( context->target != NULL )
    {
        siVgiReleaseTarget( context->moduleContext->siVgiContext, context->target );
        context->target = NULL;
    }
}

/*!
 *
 *
 */
void sctiVgiCopyToTargetActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyVgiCopyToTargetActionContext( ( SCTVgiCopyToTargetActionContext* )( action->context ) );
    action->context = NULL;
}
