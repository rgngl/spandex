/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgiresizeaction.h"

#include "sct_sicommon.h"
#include "sct_sivgi.h"
#include "sct_utils.h"

#include <VG/vgcontext.h>

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateVgiResizeActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVgiResizeActionContext*          context;
    SCTVgiModuleContext*                mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTVgiModuleContext* )( moduleContext );
    context = ( SCTVgiResizeActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVgiResizeActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Resize@Vgi context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVgiResizeActionContext ) );

    context->moduleContext = mc;

    if( sctiParseVgiResizeActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVgiResizeActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVgiResizeActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiVgiResizeActionExecute( SCTAction *action, int framenumber )
{
    SCTVgiResizeActionContext*          context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTVgiResizeActionContext* )( action->context );

    if( VGIResize( context->data.width, context->data.height ) != VGI_OK )
    {
        SCT_LOG_ERROR( "VGIResize failed in Resize@Vgi action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiVgiResizeActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyVgiResizeActionContext( ( SCTVgiResizeActionContext* )( action->context ) );
    action->context = NULL;
}

