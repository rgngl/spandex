/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgicreatecontextaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <VG/vgcontext.h>

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateVgiCreateContextActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVgiCreateContextActionContext*   context;
    SCTVgiModuleContext*                mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTVgiModuleContext* )( moduleContext );
    context = ( SCTVgiCreateContextActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVgiCreateContextActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateContext@Vgi context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVgiCreateContextActionContext ) );

    context->moduleContext = mc;

    if( sctiParseVgiCreateContextActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVgiCreateContextActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVgiCreateContextActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiVgiCreateContextActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_USE_VARIABLE( benchmark );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiVgiCreateContextActionExecute( SCTAction* action, int framenumber )
{
    SCTVgiCreateContextActionContext*   context;
    VgiCreateContextActionData*         data;
    int                                 status;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTVgiCreateContextActionContext* )( action->context );
    data    = &( context->data );

    status = VGIInitializeEx( data->width, data->height, data->colorSpace, 1, 1 ); 

    if( status != VGI_OK )
    {
        SCT_LOG_ERROR( "VGI context creation failed in CreateContext@Vgi action execute." );
        return SCT_FALSE;
    }
    context->moduleContext->width  = data->width;
    context->moduleContext->height = data->height;

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiVgiCreateContextActionTerminate( SCTAction* action )
{
    SCTVgiCreateContextActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTVgiCreateContextActionContext* )( action->context );

    VGITerminate();

    context->moduleContext->width  = 0;
    context->moduleContext->height = 0;
}

/*!
 *
 *
 */
void sctiVgiCreateContextActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyVgiCreateContextActionContext( action->context );
    action->context = NULL;
}




