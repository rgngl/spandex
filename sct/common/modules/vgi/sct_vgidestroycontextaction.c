/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgidestroycontextaction.h"

#include "sct_sicommon.h"
#include "sct_sivgi.h"
#include "sct_utils.h"

#include <VG/vgcontext.h>

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateVgiDestroyContextActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVgiDestroyContextActionContext*  context;
    SCTVgiModuleContext*                mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTVgiModuleContext* )( moduleContext );
    context = ( SCTVgiDestroyContextActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVgiDestroyContextActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyContext@Vgi context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVgiDestroyContextActionContext ) );

    context->moduleContext = mc;

    if( sctiParseVgiDestroyContextActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVgiDestroyContextActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVgiDestroyContextActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiVgiDestroyContextActionExecute( SCTAction *action, int framenumber )
{
    SCTVgiDestroyContextActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTVgiDestroyContextActionContext* )( action->context );

    VGITerminate();

    context->moduleContext->width  = 0;
    context->moduleContext->height = 0;

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiVgiDestroyContextActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyVgiDestroyContextActionContext( ( SCTVgiDestroyContextActionContext* )( action->context ) );
    action->context = NULL;
}




