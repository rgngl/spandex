/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgimodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_vgimodule_actions.h"
#include "sct_sivgi.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 */
SCTAttributeList*       sctiVgiModuleInfo( SCTModule* module );
SCTAction*              sctiVgiModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    sctiVgiModuleDestroy( SCTModule* module );

/*!
 *
 */
SCTModule* sctCreateVgiModule( void )
{
    SCTModule*                  module;
    SCTVgiModuleContext*        context;

    context = ( SCTVgiModuleContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVgiModuleContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTVgiModuleContext ) );

    /* Create module data structure. */
    module = sctCreateModule( "Vgi",
                              context,
#if defined( _WIN32 )
                              _vgi_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiVgiModuleInfo,
                              sctiVgiModuleCreateAction,
                              sctiVgiModuleDestroy );

    if( module == NULL )
    {
        siCommonMemoryFree( NULL, context );
        return NULL;
    }

    context->siVgiContext = siVgiGetContext();
    if( context->siVgiContext == NULL )
    {
        sctiVgiModuleDestroy( module );
        siCommonMemoryFree( NULL, module );
        return NULL;
    }

    return module;
}

/*!
 *
 */
SCTAttributeList* sctiVgiModuleInfo( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( module->context != NULL );

    return sctCreateAttributeList();
}

/*!
 *
 */
SCTAction* sctiVgiModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCTVgiModuleContext*        moduleContext;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    moduleContext = ( SCTVgiModuleContext* )( module->context );

    SCT_ASSERT_ALWAYS( moduleContext != NULL );

    return sctCreateActionFromTemplate( name, type, "Vgi", moduleContext, attributes, VgiActionTemplates, SCT_ARRAY_LENGTH( VgiActionTemplates ) );
}

/*!
 *
 */
void sctiVgiModuleDestroy( SCTModule* module )
{
    SCTVgiModuleContext*        context;

    SCT_ASSERT_ALWAYS( module != NULL );

    context = ( SCTVgiModuleContext* )( module->context );
    if( context != NULL )
    {
        if( context->siVgiContext != NULL )
        {
            siVgiReleaseContext( context->siVgiContext );
            context->siVgiContext = NULL;
        }

        siCommonMemoryFree( NULL, context );
        module->context = NULL;
    }
}
