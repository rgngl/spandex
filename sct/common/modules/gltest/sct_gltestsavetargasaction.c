/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_gltestsavetargasaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_OPENGLES1_MODULE )
#include "../opengles1/sct_gl1.h"
#endif  /* defined( INCLUDE_OPENGLES1_MODULE ) */

#if defined( INCLUDE_OPENGLES2_MODULE )
#include "../opengles2/sct_gl2.h"
#endif  /* defined( INCLUDE_OPENGLES2_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateGltestSaveTargasActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTGltestSaveTargasActionContext*   context;

    SCT_USE_VARIABLE( moduleContext );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTGltestSaveTargasActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTGltestSaveTargasActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SaveTargas@Gltest action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTGltestSaveTargasActionContext ) );

    if( sctiParseGltestSaveTargasActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyGltestSaveTargasActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyGltestSaveTargasActionContext( void* context )
{
    SCTGltestSaveTargasActionContext*   c;

    if( context == NULL )
    {
        return;
    }
    
    c = ( SCTGltestSaveTargasActionContext* )( context );

    SCT_ASSERT_ALWAYS( c->pixels == NULL );
    SCT_ASSERT_ALWAYS( c->filename == NULL );
    SCT_ASSERT_ALWAYS( c->benchmarkName == NULL );
    
    if( c->data.frameNumbers != NULL )
    {
        sctDestroyIntVector( c->data.frameNumbers );
        c->data.frameNumbers = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiGltestSaveTargasActionInit( SCTAction *action, SCTBenchmark *benchmark )
{
    SCTGltestSaveTargasActionContext*   context;
    GltestSaveTargasActionData*         data;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTGltestSaveTargasActionContext* )( action->context );
    data    = &( context->data );

    if( context->pixels != NULL )
    {
        return SCT_TRUE;
    }

    context->pixelsLength = data->width * data->height * 4;
    context->pixels       = ( unsigned char* )( siCommonMemoryAlloc( NULL, context->pixelsLength ) );
    if( context->pixels == NULL )
    {
        SCT_LOG_ERROR( "Memory allocation failed in SaveTargas@Gltest action init." );
        return SCT_FALSE;
    }

    context->benchmarkName = sctDuplicateString( benchmark->name );
    if( context->benchmarkName == NULL )
    {
        SCT_LOG_ERROR( "String duplication failed in SaveTargas@Gltest action init." );        
        sctiGltestSaveTargasActionTerminate( action );
        return SCT_FALSE;
    }

    context->filenameLength = 512;
    context->filename    = ( char* )( siCommonMemoryAlloc( NULL, context->filenameLength ) );   
    if( context->filename == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SaveTargas@Gltest action init." );
        sctiGltestSaveTargasActionTerminate( action );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiGltestSaveTargasActionExecute( SCTAction* action, int frameNumber )
{
    SCTGltestSaveTargasActionContext*   context;
    GltestSaveTargasActionData*         data;
    GLenum                              err;
    unsigned char                       header[ 18 ];
    unsigned char*                      datas[ 2 ];
    int                                 lengths[ 2 ];
    int                                 i;
    SCTBoolean                          found;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    if( frameNumber == -1 )
    {
        return SCT_TRUE;
    }

    context = ( SCTGltestSaveTargasActionContext* )( action->context );
    data    = &( context->data );
   
    /* Check if the current framenumber is in the framenumber vector. Use brute
       force search with the assumption that the number of elements in
       framenumber vector is limited. TODO: more efficient search?  */
    found = SCT_FALSE;
    for( i = 0; i < data->frameNumbers->length; ++i )
    {
        if( data->frameNumbers->data[ i ] == frameNumber )
        {
            found = SCT_TRUE;
            break;
        }
    }
    
    if( found == SCT_FALSE )
    {
        /* The current framenumber was not in the framenumber vector. */
        return SCT_TRUE;
    }
    
    if( sctGltestFormatTargaFilename( context->filename,
                                      context->filenameLength,
                                      data->path,
                                      context->benchmarkName,
                                      frameNumber ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Targa filename formatting failed in SaveTargas@Gltest action execute." );
        return SCT_FALSE;
    }

#if defined( SCT_GLTEST_MODULE_CHECK_ERRORS )
    /* Clear gl error. Beware infinite loops due to undefined context, etc. */
    for( i = 0; i < 16; ++i )
    {
        if( glGetError() == GL_NO_ERROR )
        {
            break;
        }
    }
#endif  /* defined( SCT_GLTEST_MODULE_CHECK_ERRORS ) */
    
    glPixelStorei( GL_PACK_ALIGNMENT, 1 );
    glReadPixels( data->x,
                  data->y,
                  data->width, 
                  data->height,
                  GL_RGBA,
                  GL_UNSIGNED_BYTE,
                  context->pixels );

#if defined( SCT_GLTEST_MODULE_CHECK_ERRORS )    
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 128 ];
        sprintf( buf, "GL error 0x%x in SaveTargas@Gltest action init.", err );
        SCT_LOG_ERROR( buf );
        
        return SCT_FALSE;
    }
#else   /* defined( SCT_GLTEST_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_GLTEST_MODULE_CHECK_ERRORS ) */
    
    sctGltestSwapRgbaToBgra( context->pixels, context->pixelsLength );
    sctGltestCreateBGRATargaHeader( header, data->width, data->height );
    
    datas[ 0 ]      = header;
    lengths[ 0 ]    = 18;       
    datas[ 1 ]	    = context->pixels;
    lengths[ 1 ]    = context->pixelsLength;
    
    if( sctGltestSaveData( context->filename,
                           ( const unsigned char** )( datas ),
                           ( const int* )( lengths ),
                           2 ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Save data failed in SaveTargas@Gltest action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiGltestSaveTargasActionTerminate( SCTAction* action )
{
    SCTGltestSaveTargasActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTGltestSaveTargasActionContext* )( action->context );

    if( context->pixels != NULL )
    {
        siCommonMemoryFree( NULL, context->pixels );
        context->pixels = NULL;
    }

    if( context->filename != NULL )
    {
        siCommonMemoryFree( NULL, context->filename );
        context->filename = NULL;
    }

    if( context->benchmarkName != NULL )
    {
        siCommonMemoryFree( NULL, context->benchmarkName );
        context->benchmarkName = NULL;
    }
}

/*!
 *
 *
 */
void sctiGltestSaveTargasActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyGltestSaveTargasActionContext( action->context );
    action->context = NULL;
}

