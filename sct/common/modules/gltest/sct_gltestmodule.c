/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_gltestmodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"
#include "sct_sifile.h"

#include "sct_gltestmodule_actions.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
SCTAttributeList*   sctiGltestModuleInfo( SCTModule* module );
SCTAction*          sctiGltestModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                sctiGltestModuleDestroy( SCTModule* module );

/*!
 *
 *
 */
SCTModule* sctCreateGltestModule( void )
{
    SCTModule*  module;

    module = sctCreateModule( "Gltest",
                              NULL,
#if defined( _WIN32 )
                              _gltest_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiGltestModuleInfo,
                              sctiGltestModuleCreateAction,
                              sctiGltestModuleDestroy );

    return module;
}

/*!
 *
 *
 */
SCTAttributeList* sctiGltestModuleInfo( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );

    return sctCreateAttributeList();
}

/*!
 *
 *
 */
SCTAction* sctiGltestModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    return sctCreateActionFromTemplate( name, type, "Gltest", NULL, attributes, GltestActionTemplates, SCT_ARRAY_LENGTH( GltestActionTemplates ) );
}

/*!
 *
 *
 */
void sctiGltestModuleDestroy( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );
}

/*!
 *
 *
 */
SCTBoolean sctGltestFormatTargaFilename( char* buffer, int length, const char* path, const char* benchmark, int frameNumber )
{
    char    b[ 32 ];
    int     i, l;
  
    sprintf( b, "%d", frameNumber );
    l = strlen( path ) + strlen( benchmark ) + strlen( b ) + 7;

    if( l >= length )
    {
        return SCT_FALSE;
    }
    
    sprintf( buffer, "%s/%s_%d.tga", path, benchmark, frameNumber );
        
    /* Convert undesired characters in file name to underscores. */
    for( i = 0; buffer[ i ] != '\0'; ++i )
    {
        if( buffer[ i ] == ' '  )
        {
            buffer[ i ] = '_';
        }
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctGltestSwapRgbaToBgra( unsigned char* rgbaPixels, int length )
{
    int             i;
    unsigned char   r, g, b, a;
    unsigned char*  ptr; 
    
    for( i = 0; i < length; i += 4 )
    {
        ptr = ( unsigned char* )( rgbaPixels ) + i;
        r = *ptr++;
        g = *ptr++;
        b = *ptr++;
        a = *ptr++;
            
        ptr = ( unsigned char* )( rgbaPixels ) + i;
        *ptr++ = b;
        *ptr++ = g;
        *ptr++ = r;
        *ptr++ = a;
    }
}

/*!
 *
 *
 */
void sctGltestCreateBGRATargaHeader( unsigned char header[ 18 ], int width, int height  )
{
    header[ 0 ]     = 0;                                    /* No image ID field included */
    header[ 1 ]     = 0;                                    /* No color-map data included */
    header[ 2 ]     = 2;                                    /* Uncompressed, true-color image */
        
    header[ 3 ]     = 0;                                    /* No use for color map specification */
    header[ 4 ]     = 0;
    header[ 5 ]     = 0;
    header[ 6 ]     = 0;
    header[ 7 ]     = 0;
        
    header[ 8 ]     = 0;                                    /* x-origin of image = 0 */
    header[ 9 ]     = 0;
        
    header[ 10 ]    = 0;                                    /* y-origin of image = 0 */
    header[ 11 ]    = 0;
        
    header[ 12 ]    = ( unsigned char )( width & 0xFF );    /* Image width LSB */
    header[ 13 ]    = ( unsigned char )( width >> 8 );      /* Image width MSB */

    header[ 14 ]    = ( unsigned char )( height & 0xFF );   /* Image height LSB */
    header[ 15 ]    = ( unsigned char )( height >> 8 );     /* Image height MSB */
        
    header[ 16 ]    = 32;                                   /* Pixel depth */
        
    header[ 17 ]    = 8;                                    /* Alpha channel bits per pixel */
}

/*!
 *
 *
 */
SCTBoolean sctGltestSaveData( const char* filename, const unsigned char** datas, const int* lengths, int count )
{
    SCTFile f;
    int     i, len;
    void*   context;

    SCT_ASSERT_ALWAYS( filename != NULL );
    SCT_ASSERT_ALWAYS( datas != NULL );
    SCT_ASSERT_ALWAYS( lengths != NULL );
    SCT_ASSERT_ALWAYS( count > 0 );

    context = siFileCreateContext();
    if( context == NULL )
    {
        return SCT_FALSE;
    }
    
    if( siFileMkdir( context, filename ) == SCT_FALSE )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: creating directory for \"%s\" failed.", filename );
#endif  /* defined( SCT_DEBUG ) */

        siFileDestroyContext( context );
        return SCT_FALSE;
    }

    f = siFileOpen( context, filename, "wb" );
    if( f == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: creating file \"%s\" failed.", filename );
#endif  /* defined( SCT_DEBUG ) */

        siFileDestroyContext( context );
        return SCT_FALSE;
    }

    for( i = 0; i < count; ++i )
    {
        SCT_ASSERT_ALWAYS( datas[ i ] != NULL && lengths[ i ] > 0 );
        len = siFileWrite( context, f, ( const char* )( datas[ i ] ), lengths[ i ] );
        if( len != lengths[ i ] )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: writing to file \"%s\" failed.", filename );
#endif  /* defined( SCT_DEBUG ) */

            siFileDestroyContext( context );
            siFileClose( context, f );
            return SCT_FALSE;
        }
    }

    siFileClose( context, f );
    siFileDestroyContext( context );

    return SCT_TRUE;
}

