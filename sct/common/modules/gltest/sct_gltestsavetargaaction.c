/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_gltestsavetargaaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_OPENGLES1_MODULE )
#include "../opengles1/sct_gl1.h"
#endif  /* defined( INCLUDE_OPENGLES1_MODULE ) */

#if defined( INCLUDE_OPENGLES2_MODULE )
#include "../opengles2/sct_gl2.h"
#endif  /* defined( INCLUDE_OPENGLES2_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateGltestSaveTargaActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTGltestSaveTargaActionContext*    context;

    SCT_USE_VARIABLE( moduleContext );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTGltestSaveTargaActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTGltestSaveTargaActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SaveTarga@Gltest action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTGltestSaveTargaActionContext ) );

    if( sctiParseGltestSaveTargaActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyGltestSaveTargaActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyGltestSaveTargaActionContext( void* context )
{
    SCTGltestSaveTargaActionContext*    c;

    if( context == NULL )
    {
        return;
    }
    c = ( SCTGltestSaveTargaActionContext* )( context );

    SCT_ASSERT_ALWAYS( c->pixels == NULL );
    SCT_ASSERT_ALWAYS( c->filename == NULL );
    SCT_ASSERT_ALWAYS( c->benchmarkName == NULL );

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiGltestSaveTargaActionInit( SCTAction *action, SCTBenchmark *benchmark )
{
    SCTGltestSaveTargaActionContext*    context;
    GltestSaveTargaActionData*          data;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTGltestSaveTargaActionContext* )( action->context );
    data    = &( context->data );

    if( context->pixels != NULL )
    {
        return SCT_TRUE;
    }

    context->pixelsLength = data->width * data->height * 4;
    context->pixels       = ( unsigned char* )( siCommonMemoryAlloc( NULL, context->pixelsLength ) );
    if( context->pixels == NULL )
    {
        SCT_LOG_ERROR( "Memory allocation failed in SaveTarga@Gltest action init." );
        return SCT_FALSE;
    }

    context->benchmarkName = sctDuplicateString( benchmark->name );
    if( context->benchmarkName == NULL )
    {
        SCT_LOG_ERROR( "String duplication failed in SaveTarga@Gltest action init." );                
        sctiGltestSaveTargaActionTerminate( action );
        return SCT_FALSE;
    }
    
    context->filenameLength = 512;    
    context->filename    = ( char* )( siCommonMemoryAlloc( NULL, context->filenameLength ) );
    if( context->filename == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SaveTarga@Gltest action init." );
        sctiGltestSaveTargaActionTerminate( action );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiGltestSaveTargaActionExecute( SCTAction* action, int frameNumber )
{
    SCTGltestSaveTargaActionContext*    context;
    GltestSaveTargaActionData*          data;
    GLenum                              err;
    unsigned char                       header[ 18 ];
    unsigned char*                      datas[ 2 ];
    int                                 lengths[ 2 ];
    int                                 i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    SCT_ASSERT( SCT_ARRAY_LENGTH( datas ) == SCT_ARRAY_LENGTH( lengths ) );

    if( frameNumber == -1 )
    {
        return SCT_TRUE;
    }
    
    context = ( SCTGltestSaveTargaActionContext* )( action->context );
    data    = &( context->data );

    if( data->frameNumber < 0 || data->frameNumber == frameNumber )
    {
        if( sctGltestFormatTargaFilename( context->filename,
                                          context->filenameLength,
                                          data->path,
                                          context->benchmarkName,
                                          frameNumber ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Targa filename formatting failed in SaveTarga@Gltest action execute." );
            return SCT_FALSE;
        }

#if defined( SCT_GLTEST_MODULE_CHECK_ERRORS )
        /* Clear gl error. Beware infinite loops due to undefined context, etc. */
        for( i = 0; i < 16; ++i )
        {
            if( glGetError() == GL_NO_ERROR )
            {
                break;
            }
        }
#else   /* defined( SCT_GLTEST_MODULE_CHECK_ERRORS ) */
        SCT_USE_VARIABLE( i );
#endif  /* defined( SCT_GLTEST_MODULE_CHECK_ERRORS ) */
        
        glPixelStorei( GL_PACK_ALIGNMENT, 1 );
        glReadPixels( data->x,
                      data->y,
                      data->width, 
                      data->height,
                      GL_RGBA,
                      GL_UNSIGNED_BYTE,
                      context->pixels );

#if defined( SCT_GLTEST_MODULE_CHECK_ERRORS )        
        err = glGetError();
        if( err != GL_NO_ERROR )
        {
			char buf[ 128 ];
            sprintf( buf, "GL error 0x%x in SaveTarga@Gltest action execute.", err );
            SCT_LOG_ERROR( buf );

            return SCT_FALSE;
        }

#else   /* defined( SCT_GLTEST_MODULE_CHECK_ERRORS ) */
        SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_GLTEST_MODULE_CHECK_ERRORS ) */
        
        sctGltestSwapRgbaToBgra( context->pixels, context->pixelsLength );
        sctGltestCreateBGRATargaHeader( header, data->width, data->height );
    
        datas[ 0 ]      = header;
        lengths[ 0 ]    = 18;       
        datas[ 1 ]	    = context->pixels;
        lengths[ 1 ]    = context->pixelsLength;
    
        if( sctGltestSaveData( context->filename,
                               ( const unsigned char** )( datas ),
                               ( const int* )( lengths ),
                               2 ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Save data failed in SaveTarga@Gltest action execute." );
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiGltestSaveTargaActionTerminate( SCTAction* action )
{
    SCTGltestSaveTargaActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTGltestSaveTargaActionContext* )( action->context );

    if( context->pixels != NULL )
    {
        siCommonMemoryFree( NULL, context->pixels );
        context->pixels = NULL;
    }

    if( context->filename != NULL )
    {
        siCommonMemoryFree( NULL, context->filename );
        context->filename = NULL;
    }

    if( context->benchmarkName != NULL )
    {
        siCommonMemoryFree( NULL, context->benchmarkName );
        context->benchmarkName = NULL;
    }
}

/*!
 *
 *
 */
void sctiGltestSaveTargaActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyGltestSaveTargaActionContext( action->context );
    action->context = NULL;
}
