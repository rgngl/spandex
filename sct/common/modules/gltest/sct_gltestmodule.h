/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_GLTESTMODULE_H__ )
#define __SCT_GLTESTMODULE_H__

#include "sct_types.h"

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    SCTModule*                  sctCreateGltestModule( void );

    /*
     * Utility functions.
     */
    SCTBoolean                  sctGltestFormatTargaFilename( char* buffer, int length, const char* path, const char* benchmark, int frameNumber );
    void                        sctGltestSwapRgbaToBgra( unsigned char* pixels, int length );
    void                        sctGltestCreateBGRATargaHeader( unsigned char header[ 18 ], int width, int height  );   
    SCTBoolean                  sctGltestSaveData( const char* filename, const unsigned char** datas, const int* lengths, int count );
    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_GLTESTMODULE_H__ ) */
