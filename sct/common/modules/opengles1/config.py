#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
SetModuleName( 'OpenGLES1' )

######################################################################
AddInclude( 'sct_opengles1module.h' )
AddInclude( 'sct_gl1.h' )
AddInclude( 'sct_opengles1utils.h' )

######################################################################
MAX_TEXTURE_DATA_INDEX                  = 4096
MAX_COMPRESSED_TEXTURE_DATA_INDEX       = 4096
MAX_TEXTURE_INDEX                       = 4096
MAX_ARRAY_INDEX                         = 4096
MAX_BUFFER_INDEX                        = 4096
MAX_MESH_INDEX                          = 4096
MAX_MESH_BUFFER_INDEX                   = 4096
MAX_DATA_INDEX                          = 4096
MAX_MESH_INDEX                          = 4096
MAX_STRINGDATA_LENGTH                   = 8128
MAX_FILENAME_LENGTH                     = 256
MAX_EXTENSION_NAME_LENGTH               = 512
MAX_FUNCTION_NAME_LENGTH                = 512
MAX_ERROR_MESSAGE_LENGTH                = 128

######################################################################
# ARRAYS AND DATA ####################################################
######################################################################

######################################################################
# CreateArray. Create an empty array with a given type to the specified array
# index. The action fails if the array index is already in use. Arrays are used
# to hold data which is used for example to define vertex data in the
# VertexPointer action.
AddActionConfig( 'CreateArray',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   EnumAttribute(               'Type', 'GLint',                        [ 'GL_BYTE',
                                                                                          'GL_UNSIGNED_BYTE',
                                                                                          'GL_SHORT',
                                                                                          'GL_UNSIGNED_SHORT',
                                                                                          'GL_FIXED',
                                                                                          'GL_FLOAT' ] )
                   ]
                 )

######################################################################
# AppendToArray. Append data to the array specified by the array index. The
# array index must refer to a valid array. Data is given using a vector of
# double precision floating point values, but it is converted into the array
# type internally. The array grows as needed; NOTE that this means the array
# should be used f.ex. as vertex array only after all the data has been
# appended.
AddActionConfig( 'AppendToArray',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   DoubleVectorAttribute(       'Data' )
                   ]
                 )

######################################################################
# AppendDataToArray. Append raw data specified by the data index to the array
# specified by the array index. Raw data is for example loaded from a binary
# file using the LoadData action. Data is appended as is and interpreted as the
# array type when used. The array grows as needed; NOTE that this means the
# array should be used f.ex. as vertex array only after all the data has been
# appended.
AddActionConfig( 'AppendDataToArray',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 )
                   ]
                 )

######################################################################
# ClearArray. Clear the array specified by the array index. Clearing deallocates
# the memory reserved for the array data.
AddActionConfig( 'ClearArray',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 )
                   ]
                 )

######################################################################
# DeleteArray. Delete the array specified by the array index. The array index is
# released and can be subsequently used in the CreateArray action.
AddActionConfig( 'DeleteArray',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 )
                   ]
                 )

######################################################################
# CreateData. Create empty data with the given length to the specified data
# index. The action fails if the data index is already in use and the existing
# data does not have exactly the specified length. Data is used for example to
# load texture and vertex data from a file.
AddActionConfig( 'CreateData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'Length',                               1 )
                   ]
                 )

######################################################################
# LoadData. Load data from the file specified by the file name to the data
# specified by the data index. Creates a new data with the length matching that
# of the file. If the data already exists in the data index, it is reused if the
# length of the existing data and the length of the file match
# exactly. Otherwise, the action reports an error.
AddActionConfig( 'LoadData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   StringAttribute(             'Filename',                             MAX_FILENAME_LENGTH )
                   ]
                 )

######################################################################
# Decode. Decode data stored into the source data index. The source data must
# follow the file format used by the selected encoder. The destination data
# index must be free, or the data length must be exactly the same as the data
# payload in the data.
AddActionConfig( 'Decode',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   AutoEnumAttribute(           'Encoder',                             [ 'PVRTEXTOOL3' ] ),
                   ]
                 )

######################################################################
# DecodeETC. Decode ETC texture compressed (GL_ETC1_RGB8_OES) data stored into
# the source data index. The source data must follow the file format used by the
# selected decoder: ATI corresponds ATI compressonator utility, ERICSSON
# corresponds Ericsson etcpack. The destination data index must be free, or the
# data length must be exactly the same as the data payload in the compressed
# data. The resulting data can be used in the CreateCompressedTextureData action
# with the internal type GL_ETC1_RGB8_OES.
AddActionConfig( 'DecodeETC',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   AutoEnumAttribute(           'Encoder',                             [ 'ATI',
                                                                                         'ERICSSON' ] ),
                   ]
                 )

######################################################################
# DecodeTarga. Decode targa data (24bpp RGB, 32bpp RGBA). The destination data
# index must be free, or the data length must be exactly the same as the data
# payload in the targa data. The resulting data can be used in the
# CreateTextureData action.
AddActionConfig( 'DecodeTarga',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   ]
                 )

######################################################################
# DeleteData. Delete the data specified by the data index, and set the the data
# index as free.
AddActionConfig( 'DeleteData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 )
                   ]
                 )

######################################################################
# STATE SETUP, ETC. ##################################################
######################################################################

######################################################################
# Info. Print information about the OpenGLES 1.1 implementation to the system
# output through siCommonWriteChar function. System output is usually the report
# file.
AddActionConfig( 'Info',
                 'Execute'+'Destroy',
                 []
                 )

######################################################################
# CheckExtension. Check the availability of a given OpenGLES1 extension. The
# action reports an error if the extension is not present.
AddActionConfig( 'CheckExtension',
                 'Execute'+'Destroy',
                 [ StringAttribute(     'Extension',                            MAX_EXTENSION_NAME_LENGTH )
                   ]
                 )

######################################################################
# CheckFunction. Check the availability of the given function. The action
# reports an error if the function is not present.
AddActionConfig( 'CheckFunction',
                 'Execute'+'Destroy',
                 [ StringAttribute(             'Function',                            MAX_FUNCTION_NAME_LENGTH )
                   ]
                 )

######################################################################
# Check error. Message can be used to detect the specific action which failed
# the error check.
AddActionConfig( 'CheckError',
                 'Execute'+'Destroy',
                 [ StringAttribute(     'Message',                              MAX_ERROR_MESSAGE_LENGTH )
                   ],
                 )

######################################################################
# CheckValue. Query OpenGLES1 value(s) and compare to input value(s) according
# to condition; <queried value> <condition> <value>. Values length must match
# the queried value length, e.g. GL_MAX_VIEWPORT_DIMS expects 2 values, both of
# which are compared using the same condition. SCT_FOUND condition can be used
# only with GL_COMPRESSED_TEXTURE_FORMATS.
AddActionConfig( 'CheckValue',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Value', 'GLint',                       [ 'GL_MAX_LIGHTS',
                                                                                          'GL_MAX_CLIP_PLANES',
                                                                                          'GL_MAX_MODELVIEW_STACK_DEPTH',
                                                                                          'GL_MAX_PROJECTION_STACK_DEPTH',
                                                                                          'GL_MAX_TEXTURE_STACK_DEPTH',
                                                                                          'GL_SUBPIXEL_BITS',
                                                                                          'GL_MAX_TEXTURE_SIZE',
                                                                                          'GL_MAX_VIEWPORT_DIMS',
                                                                                          'GL_ALIASED_POINT_SIZE_RANGE',
                                                                                          'GL_SMOOTH_POINT_SIZE_RANGE',
                                                                                          'GL_ALIASED_LINE_WIDTH_RANGE',
                                                                                          'GL_SMOOTH_LINE_WIDTH_RANGE',
                                                                                          'GL_MAX_TEXTURE_UNITS',
                                                                                          'GL_SAMPLE_BUFFERS',
                                                                                          'GL_SAMPLES',
                                                                                          'GL_COMPRESSED_TEXTURE_FORMATS',
                                                                                          'GL_NUM_COMPRESSED_TEXTURE_FORMATS',
                                                                                          'GL_RED_BITS',
                                                                                          'GL_GREEN_BITS',
                                                                                          'GL_BLUE_BITS',
                                                                                          'GL_ALPHA_BITS',
                                                                                          'GL_DEPTH_BITS',
                                                                                          'GL_STENCIL_BITS' ] ),
                   EnumAttribute(               'Condition', 'SCTCondition',            [ 'SCT_LESS',
                                                                                          'SCT_LEQUAL',
                                                                                          'SCT_GREATER',
                                                                                          'SCT_GEQUAL',
                                                                                          'SCT_EQUAL',
                                                                                          'SCT_NOTEQUAL',
                                                                                          'SCT_FOUND' ] ),
                   FloatVectorAttribute(        'Values' ),
                   ]
                 )

######################################################################
# ActiveTexture. Set currently active texture unit.
AddActionConfig( 'ActiveTexture',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Unit',                                 0 )
                   ]
                 )

######################################################################
# AlphaFunc. Set alpha function.
AddActionConfig( 'AlphaFunc',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Func', 'GLenum',                       [ 'GL_NEVER',
                                                                                          'GL_ALWAYS',
                                                                                          'GL_LESS',
                                                                                          'GL_LEQUAL',
                                                                                          'GL_EQUAL',
                                                                                          'GL_GEQUAL',
                                                                                          'GL_GREATER',
                                                                                          'GL_NOTEQUAL' ] ),
                   FloatAttribute(              'Ref' )
                   ]
                 )

######################################################################
# BlendFunc. Set blend function parameters.
AddActionConfig( 'BlendFunc',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Sfactor', 'GLenum',                    [ 'GL_ZERO',
                                                                                          'GL_ONE',
                                                                                          'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA',
                                                                                          'GL_DST_ALPHA',
                                                                                          'GL_ONE_MINUS_DST_ALPHA',
                                                                                          'GL_DST_COLOR',
                                                                                          'GL_ONE_MINUS_DST_COLOR',
                                                                                          'GL_SRC_ALPHA_SATURATE' ] ),
                   EnumAttribute(               'Dfactor', 'GLenum',                    [ 'GL_ZERO',
                                                                                           'GL_ONE',
                                                                                           'GL_SRC_ALPHA',
                                                                                           'GL_ONE_MINUS_SRC_ALPHA',
                                                                                           'GL_ONE_MINUS_DST_ALPHA',
                                                                                           'GL_SRC_COLOR',
                                                                                           'GL_ONE_MINUS_SRC_COLOR',
                                                                                           'GL_DST_ALPHA' ] )
                   ]
                 )

######################################################################
# Clear. Clear selected buffer(s).
AddActionConfig( 'Clear',
                 'Execute'+'Destroy',
                 [ EnumMaskAttribute(           'Mask', 'GLbitfield',                   [ 'GL_COLOR_BUFFER_BIT',
                                                                                          'GL_DEPTH_BUFFER_BIT',
                                                                                          'GL_STENCIL_BUFFER_BIT' ] )
                   ]
                 )

######################################################################
# ClearColor. Set clear color.
AddActionConfig( 'ClearColor',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Color',                                4 )
                   ]
                 )

######################################################################
# ClearDepth. Set clear depth value.
AddActionConfig( 'ClearDepth',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'Depth' )
                   ]
                 )

######################################################################
# ClearStencil. Set clear stencil value.
AddActionConfig( 'ClearStencil',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'S' )
                   ]
                 )

######################################################################
# ClientActiveTexture. Set currently active client texture unit.
AddActionConfig( 'ClientActiveTexture',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Unit',                                 0 )
                   ]
                 )

######################################################################
# ClipPlane. Set the clip plane equation. Requires OpenGLES 1.1.
AddActionConfig( 'ClipPlane',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'Plane',                                0 ),
                   FloatArrayAttribute(         'Equation',                             4 )
                   ]
                 )

######################################################################
# Color. Set the current vertex color.
AddActionConfig( 'Color',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Color',                                4 )
                   ]
                 )

######################################################################
# ColorMask. Set color mask.
AddActionConfig( 'ColorMask',
                 'Execute'+'Destroy',
                 [ OnOffAttribute(              'Red' ),
                   OnOffAttribute(              'Green' ),
                   OnOffAttribute(              'Blue' ),
                   OnOffAttribute(              'Alpha' )
                   ]
                 )

######################################################################
# CullFace. Set polygon cull face rule.
AddActionConfig( 'CullFace',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_FRONT',
                                                                                          'GL_BACK',
                                                                                          'GL_FRONT_AND_BACK' ] )
                   ]
                 )

######################################################################
# DepthFunc. Set depth test function.
AddActionConfig( 'DepthFunc',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Func', 'GLenum',                       [ 'GL_NEVER',
                                                                                          'GL_LESS',
                                                                                          'GL_EQUAL',
                                                                                          'GL_LEQUAL',
                                                                                          'GL_GREATER',
                                                                                          'GL_NOTEQUAL',
                                                                                          'GL_GEQUAL',
                                                                                          'GL_ALWAYS' ] )
                   ]
                 )

######################################################################
# DepthMask. Set depth mask.
AddActionConfig( 'DepthMask',
                 'Execute'+'Destroy',
                 [ OnOffAttribute(              'Flag' )
                   ]
                 )

######################################################################
# DepthRange. Set depth range values..
AddActionConfig( 'DepthRange',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'NearPlane' ),
                   FloatAttribute(              'FarPlane' )
                   ]
                 )

######################################################################
# Disable. Disable state.
AddActionConfig( 'Disable',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Cap', 'GLenum',                        [ 'GL_ALPHA_TEST',
                                                                                          'GL_BLEND',
                                                                                          'GL_COLOR_LOGIC_OP',
                                                                                          'GL_COLOR_MATERIAL',
                                                                                          'GL_CULL_FACE',
                                                                                          'GL_DEPTH_TEST',
                                                                                          'GL_DITHER',
                                                                                          'GL_FOG',
                                                                                          'GL_LIGHTING',
                                                                                          'GL_LINE_SMOOTH',
                                                                                          'GL_MATRIX_PALETTE_OES',
                                                                                          'GL_MULTISAMPLE',
                                                                                          'GL_NORMALIZE',
                                                                                          'GL_POINT_SMOOTH',
                                                                                          'GL_POINT_SPRITE_OES',
                                                                                          'GL_POLYGON_OFFSET_FILL',
                                                                                          'GL_RESCALE_NORMAL',
                                                                                          'GL_SAMPLE_ALPHA_TO_COVERAGE',
                                                                                          'GL_SAMPLE_ALPHA_TO_ONE',
                                                                                          'GL_SAMPLE_COVERAGE',
                                                                                          'GL_SCISSOR_TEST',
                                                                                          'GL_STENCIL_TEST',
                                                                                          'GL_TEXTURE_2D',
                                                                                          'GL_LIGHT0',
                                                                                          'GL_LIGHT1',
                                                                                          'GL_LIGHT2',
                                                                                          'GL_LIGHT3',
                                                                                          'GL_LIGHT4',
                                                                                          'GL_LIGHT5',
                                                                                          'GL_LIGHT6',
                                                                                          'GL_LIGHT7',
                                                                                          'GL_CLIP_PLANE0',
                                                                                          'GL_CLIP_PLANE1',
                                                                                          'GL_CLIP_PLANE2',
                                                                                          'GL_CLIP_PLANE3',
                                                                                          'GL_CLIP_PLANE4',
                                                                                          'GL_CLIP_PLANE5' ] )
                   ]
                 )

######################################################################
# DisableClientState. Disable client state.
AddActionConfig( 'DisableClientState',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Array', 'GLenum',                      [ 'GL_COLOR_ARRAY',
                                                                                          'GL_MATRIX_INDEX_ARRAY_OES',
                                                                                          'GL_NORMAL_ARRAY',
                                                                                          'GL_POINT_SIZE_ARRAY_OES',
                                                                                          'GL_TEXTURE_COORD_ARRAY',
                                                                                          'GL_VERTEX_ARRAY',
                                                                                          'GL_WEIGHT_ARRAY_OES' ] )
                   ]
                 )

######################################################################
# Enable. Enable state.
AddActionConfig( 'Enable',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Cap', 'GLenum',                        [ 'GL_ALPHA_TEST',
                                                                                          'GL_BLEND',
                                                                                          'GL_COLOR_LOGIC_OP',
                                                                                          'GL_COLOR_MATERIAL',
                                                                                          'GL_CULL_FACE',
                                                                                          'GL_DEPTH_TEST',
                                                                                          'GL_DITHER',
                                                                                          'GL_FOG',
                                                                                          'GL_LIGHTING',
                                                                                          'GL_LINE_SMOOTH',
                                                                                          'GL_MATRIX_PALETTE_OES',
                                                                                          'GL_MULTISAMPLE',
                                                                                          'GL_NORMALIZE',
                                                                                          'GL_POINT_SMOOTH',
                                                                                          'GL_POINT_SPRITE_OES',
                                                                                          'GL_POLYGON_OFFSET_FILL',
                                                                                          'GL_RESCALE_NORMAL',
                                                                                          'GL_SAMPLE_ALPHA_TO_COVERAGE',
                                                                                          'GL_SAMPLE_ALPHA_TO_ONE',
                                                                                          'GL_SAMPLE_COVERAGE',
                                                                                          'GL_SCISSOR_TEST',
                                                                                          'GL_STENCIL_TEST',
                                                                                          'GL_TEXTURE_2D',
                                                                                          'GL_LIGHT0',
                                                                                          'GL_LIGHT1',
                                                                                          'GL_LIGHT2',
                                                                                          'GL_LIGHT3',
                                                                                          'GL_LIGHT4',
                                                                                          'GL_LIGHT5',
                                                                                          'GL_LIGHT6',
                                                                                          'GL_LIGHT7',
                                                                                          'GL_CLIP_PLANE0',
                                                                                          'GL_CLIP_PLANE1',
                                                                                          'GL_CLIP_PLANE2',
                                                                                          'GL_CLIP_PLANE3',
                                                                                          'GL_CLIP_PLANE4',
                                                                                          'GL_CLIP_PLANE5' ] )
                   ]
                 )

######################################################################
# EnableClientState. Enable client state.
AddActionConfig( 'EnableClientState',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Array', 'GLenum',                      [ 'GL_COLOR_ARRAY',
                                                                                          'GL_MATRIX_INDEX_ARRAY_OES',
                                                                                          'GL_NORMAL_ARRAY',
                                                                                          'GL_POINT_SIZE_ARRAY_OES',
                                                                                          'GL_TEXTURE_COORD_ARRAY',
                                                                                          'GL_VERTEX_ARRAY',
                                                                                          'GL_WEIGHT_ARRAY_OES' ] )
                   ]
                 )

######################################################################
# Finish. Wait for submitted drawing commands to finish. 
AddActionConfig( 'Finish',
                 'Execute'+'Destroy',
                 [ ]
                 )

######################################################################
# Flush. Flush submitted drawing commands.
AddActionConfig( 'Flush',
                 'Execute'+'Destroy',
                 [ ]
                 )

######################################################################
# Fog. Set all fog related parameters through a single action.
AddActionConfig( 'Fog',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_LINEAR',
                                                                                          'GL_EXP',
                                                                                          'GL_EXP2' ] ),
                   FloatAttribute(              'Density',                              0 ),
                   FloatAttribute(              'Start' ),
                   FloatAttribute(              'End' ),
                   FloatArrayAttribute(         'Color',                                4 )
                   ]
                 )

######################################################################
# FrontFace. Set polygon front face.
AddActionConfig( 'FrontFace',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_CW',
                                                                                          'GL_CCW' ] )
                   ]
                 )

######################################################################
# Hint. Set hint.
AddActionConfig( 'Hint',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_FOG_HINT',
                                                                                          'GL_GENERATE_MIPMAP_HINT',
                                                                                          'GL_LINE_SMOOTH_HINT',
                                                                                          'GL_PERSPECTIVE_CORRECTION_HINT',
                                                                                          'GL_POINT_SMOOTH_HINT' ] ),
                   EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_FASTEST',
                                                                                          'GL_NICEST',
                                                                                          'GL_DONT_CARE' ] )
                   ]
                 )

######################################################################
# Light. Configure all light related parameters through a single action.
AddActionConfig( 'Light',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Light',                                0 ),
                   FloatArrayAttribute(         'Ambient',                              4 ),
                   FloatArrayAttribute(         'Diffuse',                              4 ),
                   FloatArrayAttribute(         'Specular',                             4 ),
                   FloatArrayAttribute(         'Position',                             4 ),
                   FloatArrayAttribute(         'SpotDirection',                        3 ),
                   FloatAttribute(              'SpotExponent',                         0, 128 ),
                   FloatAttribute(              'SpotCutoff' ),
                   FloatAttribute(              'ConstantAttenuation' ),
                   FloatAttribute(              'LinearAttenuation' ),
                   FloatAttribute(              'QuadraticAttenuation' )
                   ]
                 )

######################################################################
# LightColor. Configure light color parameters.
AddActionConfig( 'LightColor',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Light',                                0 ),
                   FloatArrayAttribute(         'Ambient',                              4 ),
                   FloatArrayAttribute(         'Diffuse',                              4 ),
                   FloatArrayAttribute(         'Specular',                             4 )
                   ]
                 )

######################################################################
# LightPosition. Set light position.
AddActionConfig( 'LightPosition',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Light',                                0 ),
                   FloatArrayAttribute(         'Position',                             4 )
                   ]
                 )

######################################################################
# LightSpot. Set spot light parameters.
AddActionConfig( 'LightSpot',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Light',                                0 ),
                   FloatArrayAttribute(         'SpotDirection',                        3 ),
                   FloatAttribute(              'SpotExponent',                         0, 128 ),
                   FloatAttribute(              'SpotCutoff' )
                   ]
                 )

######################################################################
# LightSpotDirection. Set spot light direction.
AddActionConfig( 'LightSpotDirection',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Light',                                0 ),
                   FloatArrayAttribute(         'SpotDirection',                        3 )
                   ]
                 )

######################################################################
# LightAttenuation. Set light attenuation parameters.
AddActionConfig( 'LightAttenuation',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Light',                                0 ),
                   FloatAttribute(              'ConstantAttenuation' ),
                   FloatAttribute(              'LinearAttenuation' ),
                   FloatAttribute(              'QuadraticAttenuation' )
                   ]
                 )

######################################################################
# LightModelAmbient. Set ambient light parameters.
AddActionConfig( 'LightModelAmbient',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Ambient',                              4 )
                   ]
                 )

######################################################################
# LightModelTwoSided. Turn on/off two-sided lighting .
AddActionConfig( 'LightModelTwoSided',
                 'Execute'+'Destroy',
                 [ OnOffAttribute(              'TwoSide' )
                   ]
                 )

######################################################################
# LineWidth. Set line width.
AddActionConfig( 'LineWidth',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'Width' )
                   ]
                 )

######################################################################
# LogicOp.
AddActionConfig( 'LogicOp',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Opcode', 'GLenum',                     [ 'GL_CLEAR',
                                                                                          'GL_SET',
                                                                                          'GL_COPY',
                                                                                          'GL_COPY_INVERTED',
                                                                                          'GL_NOOP',
                                                                                          'GL_INVERT',
                                                                                          'GL_AND',
                                                                                          'GL_NAND',
                                                                                          'GL_OR',
                                                                                          'GL_NOR',
                                                                                          'GL_XOR',
                                                                                          'GL_EQUIV',
                                                                                          'GL_AND_REVERSE',
                                                                                          'GL_AND_INVERTED',
                                                                                          'GL_OR_REVERSE',
                                                                                          'GL_OR_INVERTED' ]  )
                   ]
                 )

######################################################################
# Material. Configure all material related parameters through a single action.
AddActionConfig( 'Material',
                 'Execute'+'Destroy',
                [ FloatArrayAttribute(          'Ambient',                              4 ),
                  FloatArrayAttribute(          'Diffuse',                              4 ),
                  FloatArrayAttribute(          'Specular',                             4 ),
                  FloatArrayAttribute(          'Emission',                             4 ),
                  FloatAttribute(               'Shininess',                            0, 128 ),
                  ]
                 )

######################################################################
# MultiTexCoord.
AddActionConfig( 'MultiTexCoord',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Unit',                                 0 ),
                   FloatArrayAttribute(         'Coordinates',                          4 )
                   ]
                 )

######################################################################
# Normal.
AddActionConfig( 'Normal',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Coordinates',                          3 )
                   ]
                 )

######################################################################
# PointParameter. Configure all point related parameters through a single
# action. Requires OpenGLES 1.1.
AddActionConfig( 'PointParameter',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                [ FloatAttribute(               'SizeMin' ),
                  FloatAttribute(               'SizeMax' ),
                  FloatAttribute(               'FadeThresholdSize' ),
                  FloatArrayAttribute(          'DistanceAttenuation',                  3 )
                  ]
                 )

######################################################################
# PointSize.
AddActionConfig( 'PointSize',
                 'Execute'+'Destroy',
                [ FloatAttribute(               'Size' )
                  ]
                 )

######################################################################
# PolygonOffset.
AddActionConfig( 'PolygonOffset',
                 'Execute'+'Destroy',
                [ FloatAttribute(               'Factor' ),
                  FloatAttribute(               'Units' )
                  ]
                 )

######################################################################
# ReadPixels. Read RGBA pixels from the currently used framebuffer to the data
# referred by the data index. New data is created, if needed. The action creates
# an error if the existing data does not have the length of
# width*height*sizeof(RGBA8888).
AddActionConfig( 'ReadPixels',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

######################################################################
# SampleCoverage.
AddActionConfig( 'SampleCoverage',
                 'Execute'+'Destroy',
                [ FloatAttribute(               'Value' ),
                  OnOffAttribute(               'Invert' )
                  ]
                 )

######################################################################
# Scissor.
AddActionConfig( 'Scissor',
                 'Execute'+'Destroy',
                [ IntAttribute(                 'X' ),
                  IntAttribute(                 'Y' ),
                  IntAttribute(                 'Width',                                0 ),
                  IntAttribute(                 'Height',                               0 )
                  ]
                 )

######################################################################
# ShadeModel.
AddActionConfig( 'ShadeModel',
                 'Execute'+'Destroy',
                [ EnumAttribute(                'Mode', 'GLenum',                       [ 'GL_FLAT',
                                                                                          'GL_SMOOTH' ] ) 
                  ]
                 )

######################################################################
# StencilFunc.
AddActionConfig( 'StencilFunc',
                 'Execute'+'Destroy',
                [ EnumAttribute(                'Func', 'GLenum',                       [ 'GL_NEVER',
                                                                                          'GL_LESS',
                                                                                          'GL_LEQUAL',
                                                                                          'GL_GREATER',
                                                                                          'GL_GEQUAL',
                                                                                          'GL_EQUAL',
                                                                                          'GL_NOTEQUAL',
                                                                                          'GL_ALWAYS' ] ),
                  IntAttribute(                 'Ref' ),
                  Hex32Attribute(               'Mask' )
                  ]
                 )

######################################################################
# StencilMask.
AddActionConfig( 'StencilMask',
                 'Execute'+'Destroy',
                 [ Hex32Attribute(              'Mask' )
                   ]
                 )

######################################################################
# StencilOp.
AddActionConfig( 'StencilOp',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Fail', 'GLenum',                       [ 'GL_KEEP',
                                                                                          'GL_ZERO',
                                                                                          'GL_REPLACE',
                                                                                          'GL_INCR',
                                                                                          'GL_DECR',
                                                                                          'GL_INVERT' ] ),
                   EnumAttribute(               'Zfail', 'GLenum',                      [ 'GL_KEEP',
                                                                                          'GL_ZERO',
                                                                                          'GL_REPLACE',
                                                                                          'GL_INCR',
                                                                                          'GL_DECR',
                                                                                          'GL_INVERT' ] ),
                   EnumAttribute(               'Zpass', 'GLenum',                      [ 'GL_KEEP',
                                                                                          'GL_ZERO',
                                                                                          'GL_REPLACE',
                                                                                          'GL_INCR',
                                                                                          'GL_DECR',
                                                                                          'GL_INVERT' ] )
                   ]
                 )

######################################################################
# ViewPort.
AddActionConfig( 'Viewport',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'X' ),
                   IntAttribute(                'Y' ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 )
                   ]
                 )

######################################################################
# MATRISES ###########################################################
######################################################################

######################################################################
# MatrixMode.
AddActionConfig( 'MatrixMode',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_MODELVIEW',
                                                                                          'GL_PROJECTION',
                                                                                          'GL_TEXTURE',
                                                                                          'GL_MATRIX_PALETTE_OES' ] )
                   ]
                 )

######################################################################
# LoadIdentity.
AddActionConfig( 'LoadIdentity',
                 'Execute'+'Destroy',
                 []
                 )

######################################################################
# Translate.
AddActionConfig( 'Translate',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Vector',                               3 )
                   ]
                 )

######################################################################
# Rotate.
AddActionConfig( 'Rotate',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'Angle' ),
                   FloatArrayAttribute(         'Vector',                               3 )
                   ]
                 )

######################################################################
# Scale.
AddActionConfig( 'Scale',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Vector',                               3 )
                   ]
                 )

######################################################################
# LoadMatrix.
AddActionConfig( 'LoadMatrix',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'M',                                    16 )
                   ]
                 )

######################################################################
# MultMatrix.
AddActionConfig( 'MultMatrix',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'M',                                    16 )
                   ]
                 )

######################################################################
# Frustum. Set viewing fustum parameters.
AddActionConfig( 'Frustum',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'Left' ),
                   FloatAttribute(              'Right' ),
                   FloatAttribute(              'Bottom' ),
                   FloatAttribute(              'Top' ),
                   FloatAttribute(              'NearPlane' ),
                   FloatAttribute(              'FarPlane' )
                   ]
                 )

######################################################################
# Ortho.
AddActionConfig( 'Ortho',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'Left' ),
                   FloatAttribute(              'Right' ),
                   FloatAttribute(              'Bottom' ),
                   FloatAttribute(              'Top' ),
                   FloatAttribute(              'NearPlane' ),
                   FloatAttribute(              'FarPlane' )
                   ]
                 )

######################################################################
# PopMatrix.
AddActionConfig( 'PopMatrix',
                 'Execute'+'Destroy',
                [ ]
                 )

######################################################################
# PushMatrix.
AddActionConfig( 'PushMatrix',
                 'Execute'+'Destroy',
                [ ]
                 )

######################################################################
# TEXTURING ##########################################################
######################################################################

######################################################################
# CreateTextureData. Create texture data to the specified texture data index
# from the raw pixel data specified by the data index. The texture data is
# specified for mipmap level 0 only. The texture data index must be unused, or
# alternatively, it must contain texture data with similar width, height, and
# type. The texture data can be used subsequently in the TexImage2D and
# TexSubImage2D actions.
AddActionConfig( 'CreateTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),                  
                   EnumAttribute(               'Type', 'SCTOpenGLES1TextureType',      [ 'OPENGLES1_LUMINANCE8',
                                                                                          'OPENGLES1_ALPHA8',
                                                                                          'OPENGLES1_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES1_RGB565',
                                                                                          'OPENGLES1_RGB888',
                                                                                          'OPENGLES1_RGBA4444',
                                                                                          'OPENGLES1_RGBA5551',
                                                                                          'OPENGLES1_RGBA8888' ] )
                   ]
                 )

######################################################################
# CreateNullTextureData. Create texture data to the specified texture data index
# with unspecified (NULL) pixel data. The texture data index must be unused. The
# texture data can be used subsequently in the TexImage2D and TexSubImage2D
# actions. 
AddActionConfig( 'CreateNullTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_INDEX - 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES1TextureType',      [ 'OPENGLES1_LUMINANCE8',
                                                                                          'OPENGLES1_ALPHA8',
                                                                                          'OPENGLES1_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES1_RGB565',
                                                                                          'OPENGLES1_RGB888',
                                                                                          'OPENGLES1_RGBA4444',
                                                                                          'OPENGLES1_RGBA5551',
                                                                                          'OPENGLES1_RGBA8888' ] )
                   ]
                 )

######################################################################
# CreateSolidTextureData. Create solid color texture data to the specified
# texture data index. The texture data index must be unused. The texture data
# can be used subsequently in the TexImage2D and TexSubImage2D actions. 
AddActionConfig( 'CreateSolidTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   FloatArrayAttribute(         'Color',                                4, 0, 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES1TextureType',      [ 'OPENGLES1_LUMINANCE8',
                                                                                          'OPENGLES1_ALPHA8',
                                                                                          'OPENGLES1_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES1_RGB565',
                                                                                          'OPENGLES1_RGB888',
                                                                                          'OPENGLES1_RGBA4444',
                                                                                          'OPENGLES1_RGBA5551',
                                                                                          'OPENGLES1_RGBA8888' ] )
                   ]
                 )

######################################################################
# CreateBarTextureData. Create texture data with a vertical bar pattern to the
# specified texture data index. The texture data index must be unused. The
# texture data can be used subsequently in the TexImage2D and TexSubImage2D
# actions. 
AddActionConfig( 'CreateBarTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   FloatArrayAttribute(         'Color1',                               4, 0, 1 ),
                   FloatArrayAttribute(         'Color2',                               4, 0, 1 ),
                   IntAttribute(                'Bars',                                 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES1TextureType',      [ 'OPENGLES1_LUMINANCE8',
                                                                                          'OPENGLES1_ALPHA8',
                                                                                          'OPENGLES1_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES1_RGB565',
                                                                                          'OPENGLES1_RGB888',
                                                                                          'OPENGLES1_RGBA4444',
                                                                                          'OPENGLES1_RGBA5551',
                                                                                          'OPENGLES1_RGBA8888' ] )
                   ]
                 )

######################################################################
# CreateCheckerTextureData. Create texture data with a checker pattern to the
# specified texture data index. The texture data index must be unused. The
# texture data can be used subsequently in the TexImage2D and TexSubImage2D
# actions. 
AddActionConfig( 'CreateCheckerTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   FloatArrayAttribute(         'Color1',                               4, 0, 1 ),
                   FloatArrayAttribute(         'Color2',                               4, 0, 1 ),
                   IntAttribute(                'CheckersX',                            1 ),
                   IntAttribute(                'CheckersY',                            1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES1TextureType',      [ 'OPENGLES1_LUMINANCE8',
                                                                                          'OPENGLES1_ALPHA8',
                                                                                          'OPENGLES1_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES1_RGB565',
                                                                                          'OPENGLES1_RGB888',
                                                                                          'OPENGLES1_RGBA4444',
                                                                                          'OPENGLES1_RGBA5551',
                                                                                          'OPENGLES1_RGBA8888' ] )
                   ]
                 )

######################################################################
# CreateGradientTextureData. Create texture data with a vertical (up-down),
# horizontal (left-right) or diagonal (up-left-low-right) gradient to the
# specified texture data index. Gradient directions are described as the data is
# laid out in memory. The texture data index must be unused. The texture data
# can be used subsequently in the TexImage2D and TexSubImage2D actions. 
AddActionConfig( 'CreateGradientTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ), 
                   EnumAttribute(               'Gradient', 'SCTOpenGLES1Gradient',     [ 'OPENGLES1_GRADIENT_VERTICAL',
                                                                                          'OPENGLES1_GRADIENT_HORIZONTAL',
                                                                                          'OPENGLES1_GRADIENT_DIAGONAL' ] ),
                   FloatArrayAttribute(         'Color1',                               4, 0, 1 ),
                   FloatArrayAttribute(         'Color2',                               4, 0, 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES1TextureType',      [ 'OPENGLES1_LUMINANCE8',
                                                                                          'OPENGLES1_ALPHA8',
                                                                                          'OPENGLES1_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES1_RGB565',
                                                                                          'OPENGLES1_RGB888',
                                                                                          'OPENGLES1_RGBA4444',
                                                                                          'OPENGLES1_RGBA5551',
                                                                                          'OPENGLES1_RGBA8888' ] )
                   ]
                 )

######################################################################
# DeleteTextureData. Delete the texture data specified by the texture data
# index, and set the texture data index as free.
AddActionConfig( 'DeleteTextureData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 )
                   ]
                 )

######################################################################
# CreateCompressedTextureData. Create compressed texture data to the compressed
# texture data index from the data specified by the data index. The compressed
# texture data index must be unused, or alternatively, it must contain
# compressed texture data with similar width, height, and internal format. The
# compressed texture data can be used subsequently in the CompressedTexImage2D
# and CompressedTexSubImage2D actions.
AddActionConfig( 'CreateCompressedTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   EnumAttribute(               'InternalFormat', 'GLenum',             [ 'GL_ETC1_RGB8_OES',
                                                                                          'GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG',
                                                                                          'GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG',
                                                                                          'GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG',
                                                                                          'GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG' ] )
                   ]
                 )

######################################################################
# CreatePaletteCompressedTextureData. Create palette compressed texture data
# from an existing texture data. Existing texture data must be in the same
# format as the elements in the palette compressed data.
AddActionConfig( 'CreatePaletteCompressedTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 ),
                   IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_INDEX - 1 ),
                   EnumAttribute(               'Type', 'GLenum',                       [ 'GL_PALETTE4_RGB8_OES',
                                                                                          'GL_PALETTE4_RGBA8_OES',
                                                                                          'GL_PALETTE4_R5_G6_B5_OES',
                                                                                          'GL_PALETTE4_RGBA4_OES',
                                                                                          'GL_PALETTE4_RGB5_A1_OES',
                                                                                          'GL_PALETTE8_RGB8_OES',
                                                                                          'GL_PALETTE8_RGBA8_OES',
                                                                                          'GL_PALETTE8_R5_G6_B5_OES',
                                                                                          'GL_PALETTE8_RGBA4_OES',
                                                                                          'GL_PALETTE8_RGB5_A1_OES' ] )
                   ]
                 )

######################################################################
# DeleteCompressedTextureData. Delete the compressed texture data specified by
# the compressed texture data index, and set the compressed texture data index
# as free.
AddActionConfig( 'DeleteCompressedTextureData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 )
                   ]
                 )

######################################################################
# GenTexture. Create a new texture to the texture index. The texture index must
# be free.
AddActionConfig( 'GenTexture',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureIndex',                         0, MAX_TEXTURE_INDEX - 1 )
                   ]
                 )

######################################################################
# BindTexture. Bind a texture to the specified texture target. The texture is
# specified by the texture index. A new texture is created if the texture index
# does not refer to an existing texture. A default OpenGLES texture (0) is bound
# if the texture index is -1.
AddActionConfig( 'BindTexture',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   IntAttribute(                'TextureIndex',                         -1, MAX_TEXTURE_INDEX - 1 )
                   ]
                 )

######################################################################
# DeleteTexture. Delete the texture referred by the texture index. The texture
# index must refer to a valid texture.
AddActionConfig( 'DeleteTexture',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureIndex',                         0, MAX_TEXTURE_INDEX - 1 )
                   ]
                 )

######################################################################
# TexImage2D. Set the texture data for the currently bound texture in the
# specified texture target. The texture data is referred by the texture data
# index. Mipmap levels are defined for the texture if the texture data contains
# mipmap data.
AddActionConfig( 'TexImage2D',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] )
                   ]
                 )

######################################################################
# TexImage2DMML. Set the texture data for the currently bound texture in the
# specified texture target at the specified mipmap level. The texture data is
# referred by the texture data index. Negative mipmap levels are allowed to
# support extensions.
AddActionConfig( 'TexImage2DMML',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   IntAttribute(                'MipmapLevel' ),
                   ]
                 )

######################################################################
# TexSubImage2D. Set the texture sub region data to the currently bound texture
# in the specified texture target. Xoffset and Yoffset specify the origin of the
# texture sub region. The width and height of the texture sub region are defined
# by the width and height of the texture data. The texture data is referred by
# the texture data index. The texture sub region data is defined for the texture
# mipmap levels if the texture data contains mipmap data.
AddActionConfig( 'TexSubImage2D',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   IntAttribute(                'Xoffset' ),
                   IntAttribute(                'Yoffset' )
                   ]
                 )

######################################################################
# TexSubImage2DMML. Set the texture sub region data to the currently bound
# texture in the specified texture target at the specified mipmap level. Xoffset
# and Yoffset specify the origin of the texture sub region. The width and height
# of the texture sub region are defined by the width and height of the texture
# data. The texture data is referred by the texture data index. Negative mipmap
# levels are allowed to support extensions.
AddActionConfig( 'TexSubImage2DMML',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   IntAttribute(                'Xoffset' ),
                   IntAttribute(                'Yoffset' ),
                   IntAttribute(                'MipmapLevel' ),
                   ]
                 )

######################################################################
# CopyTexImage2D. Copy a selected region from the currently used framebuffer as
# a specified mipmap level into the currently bound texture in the specified
# texture target. Internal format specifies the resulting texture format. X, y,
# width and height specify the framebuffer region.
AddActionConfig( 'CopyTexImage2D',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   IntAttribute(                'Level',                                0 ),
                   EnumAttribute(               'InternalFormat', 'GLenum',             [ 'GL_ALPHA',
                                                                                          'GL_LUMINANCE',
                                                                                          'GL_LUMINANCE_ALPHA',
                                                                                          'GL_RGB',
                                                                                          'GL_RGBA' ] ),
                   IntAttribute(                'X' ),
                   IntAttribute(                'Y' ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 )
                   ]
                 )

######################################################################
# CopyTexSubImage2D. Copy a selected region from the framebuffer as a sub region
# to the specified mipmap level of the currently bound texture in the specified
# texture target. Xoffset and Yoffset specify the sub region within the
# texture. X, y, width and height specify the framebuffer region.
AddActionConfig( 'CopyTexSubImage2D',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   IntAttribute(                'Level',                                0 ),
                   IntAttribute(                'Xoffset' ),
                   IntAttribute(                'Yoffset' ),
                   IntAttribute(                'X' ),
                   IntAttribute(                'Y' ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 )
                   ]
                 )

######################################################################
# CompressedTexImage2D. Set compressed texture data to the currently bound
# texture in the specified texture target. The compressed texture data is
# referred by the compressed texture data index. Mipmap levels are defined for
# the texture if the compressed texture data contains mipmap data.
AddActionConfig( 'CompressedTexImage2D',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   ]
                 )

######################################################################
# CompressedTexImage2DMML. Set compressed texture data to the currently bound
# texture in the specified texture target at the specified mipmap level. The
# compressed texture data is referred by the compressed texture data
# index. Mipmap levels are defined for the texture if the compressed texture
# data contains mipmap data. Negative mipmap levels are allowed to support
# extensions.
AddActionConfig( 'CompressedTexImage2DMML',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   IntAttribute(                'MipmapLevel' )
                   ]
                 )

######################################################################
# CompressedTexSubImage2D. Set compressed texture data to the sub region of the
# currently bound texture in the specified texture target. The compressed
# texture data is referred by the compressed texture data index. Xoffset and
# Yoffset specify the origin of the texture sub region. The width and height of
# the texture sub region are defined by the width and height of the compressed
# texture data. Mipmap levels are defined for the texture if the compressed
# texture data contains mipmap data.
AddActionConfig( 'CompressedTexSubImage2D',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureIndex',                         0, MAX_TEXTURE_INDEX - 1 ),
                   IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   IntAttribute(                'Xoffset' ),
                   IntAttribute(                'Yoffset' )                  
                   ]
                 )

######################################################################
# CompressedTexSubImage2DMML. Set compressed texture data to the sub region of
# the currently bound texture in the specified texture target at the specified
# mipmap level. The compressed texture data is referred by the compressed
# texture data index. Xoffset and Yoffset specify the origin of the texture sub
# region. The width and height of the texture sub region are defined by the
# width and height of the compressed texture data. Negative mipmap levels are
# allowed to support extensions.
AddActionConfig( 'CompressedTexSubImage2DMML',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   IntAttribute(                'Xoffset' ),
                   IntAttribute(                'Yoffset' ),
                   IntAttribute(                'MipmapLevel' )
                   ]
                 )

######################################################################
# TexEnv. Configure all texture environment related parameters through a single
# action. Setting CoordReplace ON requires OpenGLES 1.1.
AddActionConfig( 'TexEnv',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'EnvMode', 'GLint',                     [ 'GL_MODULATE',
                                                                                          'GL_DECAL',
                                                                                          'GL_BLEND',
                                                                                          'GL_REPLACE',
                                                                                          'GL_ADD' ] ),
                   FloatArrayAttribute(         'EnvColor',                             4 ),
                   OnOffAttribute(              'CoordReplace' )
                   ]
                 )

######################################################################
# TexEnvMode.
AddActionConfig( 'TexEnvMode',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLint',                        [ 'GL_MODULATE',
                                                                                          'GL_DECAL',
                                                                                          'GL_BLEND',
                                                                                          'GL_REPLACE',
                                                                                          'GL_ADD' ] )
                   ]
                 )

######################################################################
# TexEnvColor.
AddActionConfig( 'TexEnvColor',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'EnvColor',                             4 )
                   ]
                 )

######################################################################
# TexEnvCoordReplace. Requires OpenGLES 1.1.
AddActionConfig( 'TexEnvCoordReplace',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ OnOffAttribute(              'CoordReplace' )
                   ]
                 )

######################################################################
# TexEnvCombineAlpha. Requires OpenGLES 1.1.
AddActionConfig( 'TexEnvCombineAlpha',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Func', 'GLint',                        [ 'GL_REPLACE',
                                                                                          'GL_MODULATE',
                                                                                          'GL_ADD',
                                                                                          'GL_ADD_SIGNED',
                                                                                          'GL_INTERPOLATE',
                                                                                          'GL_SUBTRACT' ] ),
                   EnumAttribute(               'Src0', 'GLint',                        [ 'GL_TEXTURE',
                                                                                          'GL_CONSTANT',
                                                                                          'GL_PRIMARY_COLOR',
                                                                                          'GL_PREVIOUS' ] ),
                   EnumAttribute(               'Operand0', 'GLint',                    [ 'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA' ] ),
                   EnumAttribute(               'Src1', 'GLint',                        [ 'GL_TEXTURE',
                                                                                          'GL_CONSTANT',
                                                                                          'GL_PRIMARY_COLOR',
                                                                                          'GL_PREVIOUS' ] ),
                   EnumAttribute(               'Operand1', 'GLint',                    [ 'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA' ] ),
                   EnumAttribute(               'Src2', 'GLint',                        [ 'GL_TEXTURE',
                                                                                          'GL_CONSTANT',
                                                                                          'GL_PRIMARY_COLOR',
                                                                                          'GL_PREVIOUS' ] ),
                   EnumAttribute(               'Operand2', 'GLint',                    [ 'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA' ] )
                   ]
                 )

######################################################################
# TexEnvCombineRGB. Requires OpenGLES 1.1.
AddActionConfig( 'TexEnvCombineRGB',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Func', 'GLint',                        [ 'GL_REPLACE',
                                                                                          'GL_MODULATE',
                                                                                          'GL_ADD',
                                                                                          'GL_ADD_SIGNED',
                                                                                          'GL_INTERPOLATE',
                                                                                          'GL_SUBTRACT',
                                                                                          'GL_DOT3_RGB',
                                                                                          'GL_DOT3_RGBA' ] ),
                   EnumAttribute(               'Src0', 'GLint',                        [ 'GL_TEXTURE',
                                                                                          'GL_CONSTANT',
                                                                                          'GL_PRIMARY_COLOR',
                                                                                          'GL_PREVIOUS' ] ),
                   EnumAttribute(               'Operand0', 'GLint',                    [ 'GL_SRC_COLOR',
                                                                                          'GL_ONE_MINUS_SRC_COLOR',
                                                                                          'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA' ] ),
                   EnumAttribute(               'Src1', 'GLint',                        [ 'GL_TEXTURE',
                                                                                          'GL_CONSTANT',
                                                                                          'GL_PRIMARY_COLOR',
                                                                                          'GL_PREVIOUS' ] ),
                   EnumAttribute(               'Operand1', 'GLint',                    [ 'GL_SRC_COLOR',
                                                                                          'GL_ONE_MINUS_SRC_COLOR',
                                                                                          'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA' ] ),
                   EnumAttribute(               'Src2', 'GLint',                        [ 'GL_TEXTURE',
                                                                                          'GL_CONSTANT',
                                                                                          'GL_PRIMARY_COLOR',
                                                                                          'GL_PREVIOUS' ] ),
                   EnumAttribute(               'Operand2', 'GLint',                    [ 'GL_SRC_COLOR',
                                                                                          'GL_ONE_MINUS_SRC_COLOR',
                                                                                          'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA' ] )
                   ]
                 )

######################################################################
# TexParameter. Configure all texture parameters through a single
# action. Setting GenerateMipmaps ON requires OpenGLES 1.1.
AddActionConfig( 'TexParameter',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   EnumAttribute(               'MinFilter', 'GLint',                   [ 'GL_NEAREST',
                                                                                          'GL_LINEAR',
                                                                                          'GL_NEAREST_MIPMAP_NEAREST',
                                                                                          'GL_LINEAR_MIPMAP_NEAREST',
                                                                                          'GL_NEAREST_MIPMAP_LINEAR',
                                                                                          'GL_LINEAR_MIPMAP_LINEAR' ] ),
                   EnumAttribute(               'MagFilter', 'GLint',                   [ 'GL_NEAREST',
                                                                                          'GL_LINEAR' ] ),
                   EnumAttribute(               'WrapS', 'GLint',                       [ 'GL_CLAMP_TO_EDGE',
                                                                                          'GL_REPEAT' ] ),
                   EnumAttribute(               'WrapT', 'GLint',                       [ 'GL_CLAMP_TO_EDGE',
                                                                                          'GL_REPEAT' ] ),
                   OnOffAttribute(              'GenerateMipmaps' )
                   ]
                 )

######################################################################
# TexParameterMinFilter.
AddActionConfig( 'TexParameterMinFilter',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   EnumAttribute(               'MinFilter', 'GLint',                   [ 'GL_NEAREST',
                                                                                          'GL_LINEAR',
                                                                                          'GL_NEAREST_MIPMAP_NEAREST',
                                                                                          'GL_LINEAR_MIPMAP_NEAREST',
                                                                                          'GL_NEAREST_MIPMAP_LINEAR',
                                                                                          'GL_LINEAR_MIPMAP_LINEAR' ] )
                   ]
                 )

######################################################################
# TexParameterMagFilter.
AddActionConfig( 'TexParameterMagFilter',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   EnumAttribute(               'MagFilter', 'GLint',                   [ 'GL_NEAREST',
                                                                                          'GL_LINEAR' ] )
                   ]
                 )

######################################################################
# TexParameterWrapS.
AddActionConfig( 'TexParameterWrapS',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   EnumAttribute(               'WrapS', 'GLint',                       [ 'GL_CLAMP_TO_EDGE',
                                                                                          'GL_REPEAT' ] )
                   ]
                 )

######################################################################
# TexParameterWrapT.
AddActionConfig( 'TexParameterWrapT',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   EnumAttribute(               'WrapT', 'GLint',                       [ 'GL_CLAMP_TO_EDGE',
                                                                                          'GL_REPEAT' ] )
                   ]
                 )

######################################################################
# TexParameterGenerateMipmaps. Requires OpenGLES 1.1.
AddActionConfig( 'TexParameterGenerateMipmaps',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   OnOffAttribute(              'GenerateMipmaps' )
                   ]
                 )

######################################################################
# TextureCropRect. Requires OES_draw_texture extension.
AddActionConfig( 'TextureCropRect',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'X' ),
                   IntAttribute(                'Y' ),
                   IntAttribute(                'Width' ),
                   IntAttribute(                'Height' )
                   ]
                 )

######################################################################
# DrawTex. Requires OES_draw_texture extension.
AddActionConfig( 'DrawTex',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ FloatAttribute(              'X' ),
                   FloatAttribute(              'Y' ),
                   FloatAttribute(              'Z' ),
                   FloatAttribute(              'Width' ),
                   FloatAttribute(              'Height' )
                   ]
                 )

######################################################################
# VERTEX ARRAYS ######################################################
######################################################################

######################################################################
# ColorPointer.
AddActionConfig( 'ColorPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'Size',                                 1 ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# NormalPointer.
AddActionConfig( 'NormalPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# TexCoordPointer.
AddActionConfig( 'TexCoordPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'Size',                                 1 ),
                   IntAttribute(                'Offset',                               0 )                  
                   ]
                 )

######################################################################
# VertexPointer.
AddActionConfig( 'VertexPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'Size',                                 1 ),
                   IntAttribute(                'Offset',                               0 )                  
                   ]
                 )

######################################################################
# DrawArrays. Draw graphics primitives specified by mode. First specifies the
# first and count specifies the number of vertex attributes to source from the
# enabled arrays.
AddActionConfig( 'DrawArrays',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_POINTS',
                                                                                          'GL_LINE_STRIP',
                                                                                          'GL_LINE_LOOP',
                                                                                          'GL_LINES',
                                                                                          'GL_TRIANGLE_STRIP',
                                                                                          'GL_TRIANGLE_FAN',
                                                                                          'GL_TRIANGLES' ] ),
                   IntAttribute(                'First',                                0 ),
                   IntAttribute(                'Count',                                0 )
                   ]
                 )

######################################################################
# DrawElements. Draw graphics primitives specified by mode. Count specifies the
# number of vertex attributes to source from the enabled vertex attribute
# arrays. The array data stored in the array index specifies the indices for the
# attribute array elements to source. Array data starts from the element
# specified by offset.
AddActionConfig( 'DrawElements',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_POINTS',
                                                                                          'GL_LINE_STRIP',
                                                                                          'GL_LINE_LOOP',
                                                                                          'GL_LINES',
                                                                                          'GL_TRIANGLE_STRIP',
                                                                                          'GL_TRIANGLE_FAN',
                                                                                          'GL_TRIANGLES' ] ),
                   IntAttribute(                'Count',                                0 ),
                   IntAttribute(                'Offset',                               0 ),
                   IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 )
                   ]
                 )

######################################################################
# BUFFERS ############################################################
######################################################################

######################################################################
# GenBuffer. Create a new buffer to the buffer index. The buffer index must be
# free. Requires OpenGLES 1.1.
AddActionConfig( 'GenBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# BufferData. Binds buffer to given target (if bind is specified) and set buffer
# data. The data is taken from the array specified by the array index. Offset
# and size can be used to load a subset of the array data to the buffer. Both
# offset and size are given as array elements. The size is expanded to the array
# length when set to 0. Requires OpenGLES 1.1.
AddActionConfig( 'BufferData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_ARRAY_BUFFER',
                                                                                          'GL_ELEMENT_ARRAY_BUFFER' ] ),
                   EnumAttribute(               'Usage', 'GLenum',                      [ 'GL_STATIC_DRAW',
                                                                                          'GL_DYNAMIC_DRAW' ] ),
                   IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'Offset',                               0 ),
                   IntAttribute(                'Size',                                 0 )
                   ]
                 )

######################################################################
# BufferSubData. Binds buffer to given target (if bind is specified) and sets
# buffer sub data. The data is taken from the array specified by the array
# index. Array offset and size can be used to load a subset of the array data to
# the buffer. Both array offset and size are given as array elements. The size
# is expanded to the array length when set to 0. The type of the array must
# match the array type previously used to set the buffer data.  Requires
# OpenGLES 1.1.
AddActionConfig( 'BufferSubData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_ARRAY_BUFFER',
                                                                                          'GL_ELEMENT_ARRAY_BUFFER' ] ),
                   IntAttribute(                'Offset',                               0 ),
                   IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'ArrayOffset',                          0 ),
                   IntAttribute(                'Size',                                 0 )
                   ]
                 )

######################################################################
# BindBuffer. Bind a buffer. The buffer to bind is specified by the buffer
# index. A new buffer is created if the buffer index does not refer to an
# existing buffer. A default OpenGLES buffer (0) is bound if the buffer index is
# -1. Requires OpenGLES 1.1.
AddActionConfig( 'BindBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_ARRAY_BUFFER',
                                                                                          'GL_ELEMENT_ARRAY_BUFFER' ] ),
                   IntAttribute(                'BufferIndex',                          -1, MAX_BUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# DeleteBuffer. Delete a buffer. The buffer to delete is specified by the buffer
# index. The buffer must exist. Requires OpenGLES 1.1.
AddActionConfig( 'DeleteBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# ColorBuffer. Binds given buffer to GL_ARRAY_BUFFER target (if bind is
# specified) and sets color pointer. Requires OpenGLES 1.1.
AddActionConfig( 'ColorBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   IntAttribute(                'Size',                                 1 ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# NormalBuffer. Binds given buffer to GL_ARRAY_BUFFER target (if bind is
# specified) and sets normal pointer. Requires OpenGLES 1.1.
AddActionConfig( 'NormalBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# TexCoordBuffer. Binds given buffer to GL_ARRAY_BUFFER target (if bind is
# specified) and sets texcoord pointer. Requires OpenGLES 1.1.
AddActionConfig( 'TexCoordBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   IntAttribute(                'Size',                                 1 ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# VertexBuffer. Binds given buffer to GL_ARRAY_BUFFER target (if bind is
# specified) and sets vertex pointer. Requires OpenGLES 1.1.
AddActionConfig( 'VertexBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   IntAttribute(                'Size',                                 1 ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# BufferDrawElements. Binds given buffer to GL_ELEMENT_ARRAY_BUFFER target (if
# bind is specified) and draws graphics primitives specified by mode. Count
# number of vertex attributes are sourced from the enabled vertex attribute
# arrays. The sourced elements are specified by the buffer. Array data within
# the buffer starts from the element specified by offset. Requires OpenGLES 1.1.
AddActionConfig( 'BufferDrawElements',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_POINTS',
                                                                                          'GL_LINE_STRIP',
                                                                                          'GL_LINE_LOOP',
                                                                                          'GL_LINES',
                                                                                          'GL_TRIANGLE_STRIP',
                                                                                          'GL_TRIANGLE_FAN',
                                                                                          'GL_TRIANGLES' ] ),
                   IntAttribute(                'Count',                                0 ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# MESH ###############################################################
######################################################################

######################################################################
# CreateMesh. Create a mesh to the specified mesh index from the
# arrays specified by the array indices. The sizes vector defines the
# number of components to sample from each array per mesh
# vertex. Every array must have the same number of per-vertex data
# elements. The mesh index must be free.
AddActionConfig( 'CreateMesh',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'MeshIndex',                            0, MAX_MESH_INDEX - 1 ),
                   IntVectorAttribute(          'ArrayIndices' ),
                   IntVectorAttribute(          'Sizes' )
                   ],
                 [ ValidatorConfig( 'ArrayIndices->length != Sizes->length',
                                    'CreateMesh@OpenGLES1 ArrayIndices and Sizes differ in length' ) ]
                 )

######################################################################
# DeleteMesh.
AddActionConfig( 'DeleteMesh',
                 'Execute'+'Destroy',
                 [ IntAttribute(        'MeshIndex',                                    0, MAX_MESH_INDEX - 1 )
                   ]
                 )

######################################################################
# MeshColorPointer. Set a color array pointer to point to the mesh data. The
# array index defines the array within the mesh.
AddActionConfig( 'MeshColorPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'MeshIndex',                            0, MAX_MESH_INDEX - 1 ),
                   IntAttribute(                'ArrayIndex',                           0 )
                   ]
                 )

######################################################################
# MeshNormalPointer. Set a normal array pointer to point to the mesh data. The
# array index defines the array within the mesh.
AddActionConfig( 'MeshNormalPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'MeshIndex',                            0, MAX_MESH_INDEX - 1 ),
                   IntAttribute(                'ArrayIndex',                           0 )
                   ]
                 )

######################################################################
# MeshTexCoordPointer. Set a texture coordinate array pointer to point to the
# mesh data. The array index defines the array within the mesh.
AddActionConfig( 'MeshTexCoordPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'MeshIndex',                            0, MAX_MESH_INDEX - 1 ),
                   IntAttribute(                'ArrayIndex',                           0 )
                   ]
                 )

######################################################################
# MeshVertexPointer. Set a vertex array pointer to point to the mesh data. The
# array index defines the array within the mesh.
AddActionConfig( 'MeshVertexPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'MeshIndex',                            0, MAX_MESH_INDEX - 1 ),
                   IntAttribute(                'ArrayIndex',                           0 )
                   ]
                 )

######################################################################
# MeshBufferData. Binds given buffer to GL_ARRAY_BUFFER target (if bind is
# specified) and sets mesh data to the buffer. The mesh is referred by the mesh
# index. The mesh index must refer to a valid mesh. Requires OpenGLES 1.1.
AddActionConfig( 'MeshBufferData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   IntAttribute(                'MeshIndex',                            0, MAX_MESH_INDEX - 1 ),
                   EnumAttribute(               'Usage', 'GLenum',                      [ 'GL_STATIC_DRAW',
                                                                                          'GL_DYNAMIC_DRAW' ] ),
                   ]
                 )

######################################################################
# BufferMeshColorPointer. Binds given buffer to GL_ARRAY_BUFFER target (if bind
# is specified) and sets the color pointer to point to the mesh data. The array
# index defines the array within the mesh. Requires OpenGLES 1.1.
AddActionConfig( 'BufferMeshColorPointer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [  IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                    OnOffAttribute(              'Bind' ),
                    IntAttribute(                'ArrayIndex',                           0 )
                   ]
                 )

######################################################################
# BufferMeshNormalPointer. Binds given buffer to GL_ARRAY_BUFFER target (if bind
# is specified) and sets the normal pointer to point to the mesh data. The array
# index defines the array within the mesh. Requires OpenGLES 1.1.
AddActionConfig( 'BufferMeshNormalPointer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [  IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                    OnOffAttribute(              'Bind' ),
                    IntAttribute(                'ArrayIndex',                           0 )
                   ]
                 )

######################################################################
# BufferMeshTexCoordPointer. Binds given buffer to GL_ARRAY_BUFFER target (if
# bind is specified) and sets the tex coord pointer to point to the mesh
# data. The array index defines the array within the mesh. Requires OpenGLES
# 1.1.
AddActionConfig( 'BufferMeshTexCoordPointer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [  IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                    OnOffAttribute(              'Bind' ),
                    IntAttribute(                'ArrayIndex',                           0 )
                   ]
                 )

######################################################################
# BufferMeshVertexPointer. Binds given buffer to GL_ARRAY_BUFFER target (if bind
# is specified) and sets the vertex pointer to point to the mesh data. The array
# index defines the array within the mesh. Requires OpenGLES 1.1.
AddActionConfig( 'BufferMeshVertexPointer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [  IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                    OnOffAttribute(              'Bind' ),
                    IntAttribute(                'ArrayIndex',                           0 )
                   ]
                 )

######################################################################
# EXTENSIONS #########################################################
######################################################################

######################################################################
# PointSizePointer. Requires OES_point_size_array extension.
AddActionConfig( 'PointSizePointer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'Offset',                               0 )                                     
                   ]
                 )

######################################################################
# MatrixIndexPointer. Requires OES_matrix_palette extension.
AddActionConfig( 'MatrixIndexPointer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'Size',                                 1 ),
                   IntAttribute(                'Offset',                               0 )                  
                   ]
                 )

######################################################################
# WeightPointer. Requires OES_matrix_palette extension.
AddActionConfig( 'WeightPointer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'Size',                                 1 ),
                   IntAttribute(                'Offset',                               0 )                  
                   ]
                 )

######################################################################
# LoadPaletteFromModelViewMatrix. Requires OES_matrix_palette extension.
AddActionConfig( 'LoadPaletteFromModelViewMatrix',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 []
                 )

######################################################################
# CurrentPaletteMatrix. Requires OES_matrix_palette extension.
AddActionConfig( 'CurrentPaletteMatrix',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'Index',                                0 )
                   ]
                 )

######################################################################
# PointSizeBuffer. Binds given buffer to GL_ARRAY_BUFFER target (if bind is
# specified) and sets point size pointer. Requires OpenGLES 1.1 and
# OES_point_size_array extension.
AddActionConfig( 'PointSizeBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# MatrixIndexBuffer. Binds given buffer to GL_ARRAY_BUFFER target (if bind is
# specified) and sets matrix index pointer. Requires OpenGLES 1.1 and
# OES_matrix_palette extension.
AddActionConfig( 'MatrixIndexBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   IntAttribute(                'Size',                                 1 ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# WeightBuffer. Binds given buffer to GL_ARRAY_BUFFER target (if bind is
# specified) and sets weight pointer. Requires OpenGLES 1.1 and
# OES_matrix_palette extension.
AddActionConfig( 'WeightBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   IntAttribute(                'Size',                                 1 ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

################################################################################
# EGL IMAGE
################################################################################

################################################################################
# EglImageTargetTexture2D. Requires GL_OES_EGL_image extension. Set the texture
# data for the currently bound texture in the specified texture
# target. EglImageIndex defines the EGL image index at EGL module.
AddActionConfig( 'EglImageTargetTexture2D',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D' ] ),
                   IntAttribute(                'EglImageIndex',                        0 ),
                   ]
                 )

######################################################################
# END OF FILE
######################################################################
