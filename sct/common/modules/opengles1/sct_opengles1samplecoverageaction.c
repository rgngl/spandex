/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1samplecoverageaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1SampleCoverageActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1SampleCoverageActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1SampleCoverageActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1SampleCoverageActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SampleCoverage@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1SampleCoverageActionContext ) );

    if( sctiParseOpenGLES1SampleCoverageActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1SampleCoverageActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1SampleCoverageActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}


/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1SampleCoverageActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1SampleCoverageActionContext*    context;
    int                                         err;
    OpenGLES1SampleCoverageActionData*          data;
    GLboolean                                   invert  = GL_FALSE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1SampleCoverageActionContext* )( action->context );
    data    = &( context->data );

    if( data->invert == SCT_TRUE )
    {
        invert = GL_TRUE;
    }

    GL_SAMPLE_COVERAGEF( data->value, invert );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in SampleCoverage@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1SampleCoverageActionDestroy( SCTAction* action )
{
    SCTOpenGLES1SampleCoverageActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1SampleCoverageActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1SampleCoverageActionContext( context );
}
