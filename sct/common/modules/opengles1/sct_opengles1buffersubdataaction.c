/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1buffersubdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1BufferSubDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1BufferSubDataActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1BufferSubDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1BufferSubDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BufferSubData@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1BufferSubDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1BufferSubDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferSubDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferSubDataActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in BufferSubData@OpenGLES1 context creation." );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferSubDataActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in BufferSubData@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1BufferSubDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BufferSubDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_LOG_ERROR( "BufferSubData@OpenGLES1 not supported in OpenGLES 1.0." );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BufferSubDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1BufferSubDataActionContext* context;
    GLenum                                  err;
    OpenGLES1BufferSubDataActionData*       data;
    SCTOpenGLES1Array*                      array;
    SCTOpenGLES1Buffer*                     buffer;
    int                                     elementSize;
    int                                     size;
    int                                     offset;
    int                                     arrayOffset;
    GLenum                                  bufferType;
    unsigned char*                          ptr;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1BufferSubDataActionContext* )( action->context );
    data    = &( context->data );

#if defined( GL_VERSION_ES_CM_1_1 )
    array = sctiOpenGLES1ModuleGetArray( context->moduleContext, data->arrayIndex );
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array in BufferSubData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    buffer = sctiOpenGLES1ModuleGetBuffer( context->moduleContext, data->bufferIndex );
    if( buffer == NULL )
    {
        SCT_LOG_ERROR( "Invalid buffer in BufferSubData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    bufferType = sctOpenGLES1GetBufferArrayType( buffer, 0 );
    
    if( array->type != bufferType )
    {
        SCT_LOG_ERROR( "Array type does not match buffer type in BufferSubData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    elementSize = sctOpenGLES1GetGLTypeSize( bufferType );
    offset      = data->offset * elementSize;
    arrayOffset = data->arrayOffset * elementSize;

    if( data->size <= 0 )
    {
        if( array->lengthInBytes == 0 )
        {
            SCT_LOG_ERROR( "Undefined size with null array data in BufferSubData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        size = ( array->lengthInBytes - arrayOffset );
    }
    else
    {
        size = data->size * elementSize;
        if( ( arrayOffset + size ) > array->lengthInBytes )
        {
            SCT_LOG_ERROR( "Size exceeds array length in BufferSubData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
    }
    
    ptr = ( unsigned char* )( array->data ) + arrayOffset;
    if( data->bind != SCT_FALSE )
    {
        glBindBuffer( data->target, buffer->buffer );
    }
    glBufferSubData( data->target, offset, size, ptr );
#else   /* defined( GL_VERSION_ES_CM_1_1 ) */
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( array );
    SCT_USE_VARIABLE( buffer );
    SCT_USE_VARIABLE( elementSize );
    SCT_USE_VARIABLE( size );
    SCT_USE_VARIABLE( offset );
    SCT_USE_VARIABLE( arrayOffset );
    SCT_USE_VARIABLE( bufferType );
    SCT_USE_VARIABLE( ptr );
    SCT_ASSERT_ALWAYS( 0 );
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */
    
#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BufferSubData@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1BufferSubDataActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenGLES1BufferSubDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES1BufferSubDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1BufferSubDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1BufferSubDataActionContext( context );
}
