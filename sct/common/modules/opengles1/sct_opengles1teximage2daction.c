/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1teximage2daction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1TexImage2DActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1TexImage2DActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1TexImage2DActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1TexImage2DActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in TexImage2D@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1TexImage2DActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1TexImage2DActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1TexImage2DActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1TexImage2DActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in TexImage2D@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1TexImage2DActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TexImage2DActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1TexImage2DActionContext*    context;
    GLenum                                  err;
    int                                     i;
    OpenGLES1TexImage2DActionData*          data;
    SCTOpenGLES1TextureData*                textureData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1TexImage2DActionContext* )( action->context );
    data    = &( context->data );

    textureData = sctiOpenGLES1ModuleGetTextureData( context->moduleContext, data->textureDataIndex );
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Invalid texture data in TexImage2D@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

    for( i = 0; i < textureData->mipmapCount; ++i )
    {
        glTexImage2D( data->target, 
                      i, 
                      textureData->glFormat, 
                      textureData->mipmaps[ i ].width, 
                      textureData->mipmaps[ i ].height, 
                      0, 
                      textureData->glFormat, 
                      textureData->glType,
                      textureData->mipmaps[ i ].data );
    }

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in TexImage2D@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1TexImage2DActionDestroy( SCTAction* action )
{
    SCTOpenGLES1TexImage2DActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1TexImage2DActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1TexImage2DActionContext( context );
}
