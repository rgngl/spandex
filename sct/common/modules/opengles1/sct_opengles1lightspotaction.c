/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1lightspotaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1LightSpotActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1LightSpotActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1LightSpotActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1LightSpotActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LightSpot@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1LightSpotActionContext ) );

    if( sctiParseOpenGLES1LightSpotActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1LightSpotActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1LightSpotActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1LightSpotActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1LightSpotActionContext* context;
    int                                 err;
    OpenGLES1LightSpotActionData*       data;
    GLenum                              light;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1LightSpotActionContext* )( action->context );
    data    = &( context->data );

    light = ( GLenum )( GL_LIGHT0 + data->light );
    
    GL_LIGHTFV( light, GL_SPOT_DIRECTION, data->spotDirection );
    GL_LIGHTF( light, GL_SPOT_EXPONENT, data->spotExponent );
    GL_LIGHTF( light, GL_SPOT_CUTOFF, data->spotCutoff );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in LightSpot@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1LightSpotActionDestroy( SCTAction* action )
{
    SCTOpenGLES1LightSpotActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1LightSpotActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1LightSpotActionContext( context );
}
