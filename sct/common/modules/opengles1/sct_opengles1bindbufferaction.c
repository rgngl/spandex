/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1bindbufferaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1BindBufferActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1BindBufferActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1BindBufferActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1BindBufferActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BindBuffer@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1BindBufferActionContext ) );

    context->created = SCT_FALSE;

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1BindBufferActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BindBufferActionContext( context );
        return NULL;
    }

    if( context->data.bufferIndex >= 0 && 
        sctiOpenGLES1ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BindBufferActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in BindBuffer@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1BindBufferActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BindBufferActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_LOG_ERROR( "BindBuffer@OpenGLES1 not supported in OpenGLES 1.0." );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BindBufferActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1BindBufferActionContext*    context;
    GLenum                                  err;
    OpenGLES1BindBufferActionData*          data;
    SCTOpenGLES1Buffer*                     buffer;
    GLuint                                  bufferId    = 0;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1BindBufferActionContext* )( action->context );
    data    = &( context->data );

#if defined( GL_VERSION_ES_CM_1_1 )
    if( data->bufferIndex >= 0 )
    {
        buffer = sctiOpenGLES1ModuleGetBuffer( context->moduleContext, data->bufferIndex );
        if( buffer == NULL )
        {
            glGenBuffers( 1, &bufferId );
            buffer = sctOpenGLES1CreateBuffer( bufferId );
            if( buffer == NULL )
            {
                glDeleteBuffers( 1, &bufferId );
                SCT_LOG_ERROR( "Buffer creation failed in BindBuffer@OpenGLES1 action execute." );
                return SCT_FALSE;
            }
            sctiOpenGLES1ModuleSetBuffer( context->moduleContext, data->bufferIndex, buffer );
            context->created = SCT_TRUE;
        }
        
        bufferId = buffer->buffer;
    }

    glBindBuffer( data->target, bufferId );
#else   /* defined( GL_VERSION_ES_CM_1_1 ) */
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( buffer );
    SCT_USE_VARIABLE( bufferId );
    SCT_ASSERT_ALWAYS( 0 );
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */
    
#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BindBuffer@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1BindBufferActionTerminate( SCTAction* action )
{
    SCTOpenGLES1BindBufferActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1BindBufferActionContext* )( action->context );

    if( context->created == SCT_TRUE )
    {
        sctiOpenGLES1ModuleDeleteBuffer( context->moduleContext, context->data.bufferIndex );
        context->created = SCT_FALSE;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES1BindBufferActionDestroy( SCTAction* action )
{
    SCTOpenGLES1BindBufferActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1BindBufferActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1BindBufferActionContext( context );
}
