/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1texcoordpointeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1TexCoordPointerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1TexCoordPointerActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1TexCoordPointerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1TexCoordPointerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in TexCoordPointer@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1TexCoordPointerActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1TexCoordPointerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1TexCoordPointerActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )    
    {
        sctiDestroyOpenGLES1TexCoordPointerActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in TexCoordPointer@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1TexCoordPointerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TexCoordPointerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1TexCoordPointerActionContext*   context;
    OpenGLES1TexCoordPointerActionData*         data;
    SCTOpenGLES1Array*                          array;
    int                                         offset;
    int                                         err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1TexCoordPointerActionContext* )( action->context );
    data    = &( context->data );

    array = sctiOpenGLES1ModuleGetArray( context->moduleContext, data->arrayIndex );    
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array index in TexCoordPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( data->offset >= array->length )
    {
        SCT_LOG_ERROR( "Offset out of bounds in TexCoordPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    offset = data->offset * sctOpenGLES1GetGLTypeSize( array->type ) * data->size;

    glTexCoordPointer( data->size, 
                       array->type, 
                       0,  
                       ( unsigned char* )( array->data ) + offset );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in TexCoordPointer@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1TexCoordPointerActionDestroy( SCTAction* action )
{
    SCTOpenGLES1TexCoordPointerActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1TexCoordPointerActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1TexCoordPointerActionContext( context );
}
