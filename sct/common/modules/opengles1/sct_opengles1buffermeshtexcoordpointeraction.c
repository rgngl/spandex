/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1buffermeshtexcoordpointeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1BufferMeshTexCoordPointerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1BufferMeshTexCoordPointerActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1BufferMeshTexCoordPointerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1BufferMeshTexCoordPointerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BufferMeshTexCoordPointer@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1BufferMeshTexCoordPointerActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1BufferMeshTexCoordPointerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferMeshTexCoordPointerActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferMeshTexCoordPointerActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in BufferMeshTexCoordPointer@OpenGLES1 context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1BufferMeshTexCoordPointerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BufferMeshTexCoordPointerActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_LOG_ERROR( "BufferMeshTexCoordPointer@OpenGLES1 not supported in OpenGLES 1.0." );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BufferMeshTexCoordPointerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1BufferMeshTexCoordPointerActionContext* context;
    OpenGLES1BufferMeshTexCoordPointerActionData*       data;
    SCTOpenGLES1Buffer*                                 buffer;
    GLenum                                              err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1BufferMeshTexCoordPointerActionContext* )( action->context );
    data    = &( context->data );

    buffer = sctiOpenGLES1ModuleGetBuffer( context->moduleContext, data->bufferIndex );
    if( buffer == NULL )
    {
        SCT_LOG_ERROR( "Invalid buffer in BufferMeshTexCoordPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES1IsValidBufferArrayIndex( buffer, data->arrayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Array index out of bounds in BufferMeshTexCoordPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( data->bind != SCT_FALSE )
    {
        glBindBuffer( GL_ARRAY_BUFFER, buffer->buffer );
    }
    glTexCoordPointer( sctOpenGLES1GetBufferArrayComponents( buffer, data->arrayIndex ),
                       sctOpenGLES1GetBufferArrayType( buffer, data->arrayIndex ), 
                       sctOpenGLES1GetBufferArrayStride( buffer, data->arrayIndex ), 
                       ( const void* )( sctOpenGLES1GetBufferArrayOffset( buffer, data->arrayIndex ) ) );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BufferMeshTexCoordPointer@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1BufferMeshTexCoordPointerActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenGLES1BufferMeshTexCoordPointerActionDestroy( SCTAction* action )
{
    SCTOpenGLES1BufferMeshTexCoordPointerActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1BufferMeshTexCoordPointerActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1BufferMeshTexCoordPointerActionContext( context );
}
