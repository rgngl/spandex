/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1createcheckertexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1CreateCheckerTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1CreateCheckerTextureDataActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1CreateCheckerTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CreateCheckerTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateCheckerTextureData@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1CreateCheckerTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1CreateCheckerTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateCheckerTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateCheckerTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in CreateCheckerTextureData@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1CreateCheckerTextureDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateCheckerTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateCheckerTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1CreateCheckerTextureDataActionContext*  context;
    OpenGLES1CreateCheckerTextureDataActionData*        data;
    SCTOpenGLES1TextureData*                            textureData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1CreateCheckerTextureDataActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES1ModuleGetTextureData( context->moduleContext, data->textureDataIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Texture data index already used in CreateCheckerTextureData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    textureData = sctOpenGLES1CreateCheckerTextureData( data->type, 
                                                        data->width, 
                                                        data->height, 
                                                        data->color1, 
                                                        data->color2, 
                                                        data->checkersX, 
                                                        data->checkersY, 
                                                        data->mipmaps );
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Texture data creation failed in CreateCheckerTextureData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES1ModuleSetTextureData( context->moduleContext, data->textureDataIndex, textureData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateCheckerTextureDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES1CreateCheckerTextureDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1CreateCheckerTextureDataActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteTextureData( context->moduleContext, context->data.textureDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateCheckerTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES1CreateCheckerTextureDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1CreateCheckerTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1CreateCheckerTextureDataActionContext( context );
}
