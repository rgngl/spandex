/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1pointsizebufferaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1PointSizeBufferActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1PointSizeBufferActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1PointSizeBufferActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1PointSizeBufferActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PointSizeBuffer@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1PointSizeBufferActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1PointSizeBufferActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1PointSizeBufferActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1PointSizeBufferActionContext( context );        
        SCT_LOG_ERROR( "Invalid buffer index in PointSizeBuffer@OpenGLES1 context creation." );
        return NULL;
    }
    
    context->func               = NULL;    
    context->extensionValidated = SCT_FALSE;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1PointSizeBufferActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1PointSizeBufferActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenGLES1PointSizeBufferActionContext*   context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenGLES1PointSizeBufferActionContext* )( action->context );  

#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_USE_VARIABLE( context );
    SCT_LOG_ERROR( "PointSizeBuffer@OpenGLES1 not supported in OpenGLES 1.0." );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */   
    context->func = ( SCTOpenGLES1PointSizePointerFunc )( siCommonGetProcAddress( NULL, "glPointSizePointerOES" ) );
    if( context->func == NULL )
    {
        SCT_LOG_ERROR( "glPointSizePointerOES function not found in PointSizeBuffer@OpenGLES1 action init." );
        return SCT_FALSE;
    }
    context->extensionValidated = SCT_FALSE;
    
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */   
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1PointSizeBufferActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1PointSizeBufferActionContext*   context;
    OpenGLES1PointSizeBufferActionData*         data;
    SCTOpenGLES1Buffer*                         buffer;
    int                                         err;
    int                                         elementSize;
    GLenum                                      type;
    int                                         stride;
    int                                         offset;
    const char*                                 extString;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1PointSizeBufferActionContext* )( action->context );
    data    = &( context->data );

    if( context->extensionValidated == SCT_FALSE )
    {
        extString = ( const char* )( glGetString( GL_EXTENSIONS ) );        
        if( extString == NULL || strstr( extString, "OES_point_size_array" ) == NULL )
        {
            SCT_LOG_ERROR( "OES_point_size_array extension not supported in PointSizeBuffer@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
        context->extensionValidated = SCT_TRUE;
    }

    buffer = sctiOpenGLES1ModuleGetBuffer( context->moduleContext, data->bufferIndex );
    if( buffer == NULL )
    {
        SCT_LOG_ERROR( "Invalid buffer in PointSizeBufferBuffer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    type        = sctOpenGLES1GetBufferArrayType( buffer, 0 );
    elementSize = sctOpenGLES1GetGLTypeSize( type );

    if( data->offset >= sctOpenGLES1GetBufferArrayLength( buffer, 0 ) )
    {
        SCT_LOG_ERROR( "Too short buffer data in PointSizeBufferBuffer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    stride     = sctOpenGLES1GetBufferArrayStride( buffer, 0 );
    offset     = sctOpenGLES1GetBufferArrayOffset( buffer, 0 );

    if( stride == 0 )
    {
        offset = offset + data->offset * elementSize;
    }
    else
    {
        offset = offset + data->offset * stride;
    }

    SCT_ASSERT( context->func != NULL );

    if( data->bind != SCT_FALSE )
    {
        glBindBuffer( GL_ARRAY_BUFFER, buffer->buffer );
    }
    context->func( type, stride, ( void* )( offset ) );   

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in PointSizeBuffer@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();        

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1PointSizeBufferActionTerminate( SCTAction* action )
{
    SCTOpenGLES1PointSizeBufferActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1PointSizeBufferActionContext* )( action->context );
    
    context->func               = NULL;    
    context->extensionValidated = SCT_FALSE;
}

/*!
 *
 *
 */
void sctiOpenGLES1PointSizeBufferActionDestroy( SCTAction* action )
{
    SCTOpenGLES1PointSizeBufferActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1PointSizeBufferActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1PointSizeBufferActionContext( context );
}
