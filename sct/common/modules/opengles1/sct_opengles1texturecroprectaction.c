/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1texturecroprectaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1TextureCropRectActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1TextureCropRectActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1TextureCropRectActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1TextureCropRectActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in TextureCropRect@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1TextureCropRectActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1TextureCropRectActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1TextureCropRectActionContext( context );
        return NULL;
    }

    context->extensionValidated = SCT_FALSE;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1TextureCropRectActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TextureCropRectActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenGLES1TextureCropRectActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
    context = ( SCTOpenGLES1TextureCropRectActionContext* )( action->context );
    context->extensionValidated = SCT_FALSE;
    
#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_LOG_ERROR( "TextureCropRect@OpenGLES1 not supported in OpenGLES 1.0" );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TextureCropRectActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1TextureCropRectActionContext*   context;
    OpenGLES1TextureCropRectActionData*         data;
    int                                         err;
    int                                         rect[ 4 ];
    const char*                                 extString;    

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1TextureCropRectActionContext* )( action->context );
    data    = & ( context->data );

    if( context->extensionValidated == SCT_FALSE )
    {
        extString = ( const char* )( glGetString( GL_EXTENSIONS ) );                
        if( extString == NULL || strstr( extString, "OES_draw_texture" ) == NULL )
        {
            SCT_LOG_ERROR( "OES_draw_texture extension not supported in TextureCropRect@OpenGLES1@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
        context->extensionValidated = SCT_TRUE;
    }
    
    rect[ 0 ] = data->x;
    rect[ 1 ] = data->y;
    rect[ 2 ] = data->width;
    rect[ 3 ] = data->height;
    
#if defined( GL_VERSION_ES_CM_1_1 )
# if defined( SCT_WINDOWS_MOBILE )
	SCT_ASSERT_ALWAYS( 0 );     /* DOES GL_TEXTURE_CROP_RECT_OES ACTUALLY WORK?*/
# else  /* defined( SCT_WINDOWS_MOBILE ) */
	glTexParameteriv( GL_TEXTURE_2D, GL_TEXTURE_CROP_RECT_OES, rect );
# endif /* defined( SCT_WINDOWS_MOBILE ) */
    SCT_USE_VARIABLE( rect );
	SCT_ASSERT_ALWAYS( 0 );	   
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */  
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in TextureCropRect@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();
        
        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1TextureCropRectActionTerminate( SCTAction* action )
{
    SCTOpenGLES1TextureCropRectActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1TextureCropRectActionContext* )( action->context );
    
    context->extensionValidated = SCT_FALSE;
}

/*!
 *
 *
 */
void sctiOpenGLES1TextureCropRectActionDestroy( SCTAction* action )
{
    SCTOpenGLES1TextureCropRectActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1TextureCropRectActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1TextureCropRectActionContext( context );
}

