/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1colormaskaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1ColorMaskActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1ColorMaskActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1ColorMaskActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1ColorMaskActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ColorMask@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1ColorMaskActionContext ) );

    if( sctiParseOpenGLES1ColorMaskActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1ColorMaskActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1ColorMaskActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}


/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1ColorMaskActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1ColorMaskActionContext* context;
    int                                 err;
    OpenGLES1ColorMaskActionData*       data;
    GLboolean                           red             = GL_FALSE;
    GLboolean                           green           = GL_FALSE;
    GLboolean                           blue            = GL_FALSE;
    GLboolean                           alpha           = GL_FALSE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1ColorMaskActionContext* )( action->context );
    data    = &( context->data );

    if( data->red == SCT_TRUE )
    {
        red = GL_TRUE;
    }
    if( data->green == SCT_TRUE )
    {
        green = GL_TRUE;
    }
    if( data->blue == SCT_TRUE )
    {
        blue = GL_TRUE;
    }
    if( data->alpha == SCT_TRUE )
    {
        alpha = GL_TRUE;
    }

    glColorMask( red, green, blue, alpha );
    
#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in ColorMask@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1ColorMaskActionDestroy( SCTAction* action )
{
    SCTOpenGLES1ColorMaskActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1ColorMaskActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1ColorMaskActionContext( context );
}
