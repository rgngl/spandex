/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1infoaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1InfoActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1InfoActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1InfoActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1InfoActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Info@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1InfoActionContext ) );

    if( sctiParseOpenGLES1InfoActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1InfoActionContext( context );
        return NULL;
    }

    context->reported = SCT_FALSE;
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1InfoActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1InfoActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1InfoActionContext*  context;
    SCTAttributeList*               attributes;
    int                             err;
    SCTBoolean                      status              = SCT_TRUE;
    const char*                     str;
    GLint                           intValues[ 4 ];
    GLfloat                         floatValues[ 4 ];
    char                            buffer[ 64 ];

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1InfoActionContext* )( action->context );

    if( context->reported == SCT_TRUE )
    {
        return SCT_TRUE;
    }

    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Info@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    /* Store max lights. */
    glGetIntegerv( GL_MAX_LIGHTS, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX LIGHTS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

#if defined( GL_VERSION_ES_CM_1_1 )
    /* Store max clip planes. */
    glGetIntegerv( GL_MAX_CLIP_PLANES, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX CLIP PLANES", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */
    
    /* Store max modelview stack depth. */
    glGetIntegerv( GL_MAX_MODELVIEW_STACK_DEPTH, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX MODELVIEW STACK DEPTH", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max projection stack depth. */
    glGetIntegerv( GL_MAX_PROJECTION_STACK_DEPTH, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX PROJECTION STACK DEPTH", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max texture stack depth. */
    glGetIntegerv( GL_MAX_TEXTURE_STACK_DEPTH, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX TEXTURE STACK DEPTH", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store subpixel bits. */
    glGetIntegerv( GL_SUBPIXEL_BITS, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL SUBPIXEL BITS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
    
    /* Store max texture size. */
    glGetIntegerv( GL_MAX_TEXTURE_SIZE, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX TEXTURE SIZE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max viewport dims. */
    glGetIntegerv( GL_MAX_VIEWPORT_DIMS, intValues );
    sprintf( buffer, "%u,%u", intValues[ 0 ], intValues[ 1 ] );
    if( sctAddNameValueToList( attributes, "GL MAX VIEWPORT DIMS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

#if !defined( GERBERA_ES_VERSION )
    
    /* Store aliased point size range. */
    glGetFloatv( GL_ALIASED_POINT_SIZE_RANGE, floatValues );
    sprintf( buffer, "%f,%f", floatValues[ 0 ], floatValues[ 1 ] );
    if( sctAddNameValueToList( attributes, "GL ALIASED POINT SIZE RANGE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store smooth point size range. */
    glGetFloatv( GL_SMOOTH_POINT_SIZE_RANGE, floatValues );
    sprintf( buffer, "%f,%f", floatValues[ 0 ], floatValues[ 1 ] );
    if( sctAddNameValueToList( attributes, "GL SMOOTH POINT SIZE RANGE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store aliased line width range. */
    glGetFloatv( GL_ALIASED_LINE_WIDTH_RANGE, floatValues );
    sprintf( buffer, "%f,%f", floatValues[ 0 ], floatValues[ 1 ] );
    if( sctAddNameValueToList( attributes, "GL ALIASED LINE WIDTH RANGE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store smooth line width range. */
    glGetFloatv( GL_SMOOTH_LINE_WIDTH_RANGE, floatValues );
    sprintf( buffer, "%f,%f", floatValues[ 0 ], floatValues[ 1 ] );
    if( sctAddNameValueToList( attributes, "GL SMOOTH LINE WIDTH RANGE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
#else  /* !defined( GERBERA_ES_VERSION ) */
    SCT_USE_VARIABLE( floatValues );
#endif  /* !defined( GERBERA_ES_VERSION ) */
    
    /* Store max texture units. */
    glGetIntegerv( GL_MAX_TEXTURE_UNITS, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX TEXTURE UNITS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store sample buffers. */
    glGetIntegerv( GL_SAMPLE_BUFFERS, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL SAMPLE BUFFERS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store samples. */
    glGetIntegerv( GL_SAMPLES, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL SAMPLES", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store number of compressed texture formats. */
    glGetIntegerv( GL_NUM_COMPRESSED_TEXTURE_FORMATS, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL NUM COMPRESSED TEXTURE FORMATS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store GL extensions string. */
    str = ( const char* )( glGetString( GL_EXTENSIONS ) );
    if( str == NULL ||
        sctAddNameValueToList( attributes, "GL EXTENSIONS", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store GL renderer. */
    str = ( const char* )( glGetString( GL_RENDERER ) );
    if( str == NULL ||
        sctAddNameValueToList( attributes, "GL RENDERER", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store GL vendor. */
    str = ( const char* )( glGetString( GL_VENDOR ) );
    if( str == NULL || 
        sctAddNameValueToList( attributes, "GL VENDOR", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
    
    /* Store GL version. */
    str = ( const char* )( glGetString( GL_VERSION ) );
    if( str == NULL ||
        sctAddNameValueToList( attributes, "GL VERSION", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store viewport; usually the screen size. */
    glGetIntegerv( GL_VIEWPORT, intValues );
    sprintf( buffer, "%d,%d,%d,%d", intValues[ 0 ], intValues[ 1 ], intValues[ 2 ], intValues[ 3 ] );
    if( sctAddNameValueToList( attributes, "GL viewport", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }  

    if( status == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Failure in Info@OpenGLES1 action execute." );
    }

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    if( status == SCT_TRUE )
    {
        err = glGetError();
        if( err != GL_NO_ERROR )
        {
            char buf[ 256 ];
            sprintf( buf, "GL error %s in Info@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
            SCT_LOG_ERROR( buf );
            sctiOpenGLES1ClearGLError();
            status = SCT_FALSE;
        }
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    if( status == SCT_TRUE && sctReportSection( "OpenGLES1", "MODULE", attributes ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Report section failed in Info@OpenGLES1 action execute." );
        status = SCT_FALSE;
    }

    sctDestroyAttributeList( attributes );

    context->reported = SCT_TRUE;

    return status;
}

/*!
 *
 *
 */
void sctiOpenGLES1InfoActionDestroy( SCTAction* action )
{
    SCTOpenGLES1InfoActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1InfoActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1InfoActionContext( context );
}
