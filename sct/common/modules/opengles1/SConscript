#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

parserSource = 'sct_opengles1module_parser.c'
parserHeader = 'sct_opengles1module_parser.h'
actionHeader = 'sct_opengles1module_actions.h'

Import( 'env' )

generatedFiles = [parserSource, parserHeader, actionHeader]
env.ModuleGenerator( generatedFiles, 'config.py' )

script = File( '%s/python/mkactionparser.py' % env['SPANDEX_HOME'] )
pyLib  = File( '%s/python/sct/action.py' % env['SPANDEX_HOME'] )
Depends( generatedFiles, script )
Depends( generatedFiles, pyLib )

sourceFiles = [ 'sct_opengles1activetextureaction.c',
                'sct_opengles1alphafuncaction.c',
                'sct_opengles1appenddatatoarrayaction.c',
                'sct_opengles1appendtoarrayaction.c',
                'sct_opengles1bindbufferaction.c',
                'sct_opengles1bindtextureaction.c',
                'sct_opengles1blendfuncaction.c',
                'sct_opengles1bufferdataaction.c',
                'sct_opengles1bufferdrawelementsaction.c',
                'sct_opengles1buffermeshcolorpointeraction.c',
                'sct_opengles1buffermeshnormalpointeraction.c',
                'sct_opengles1buffermeshtexcoordpointeraction.c',
                'sct_opengles1buffermeshvertexpointeraction.c',
                'sct_opengles1buffersubdataaction.c',
                'sct_opengles1checkerroraction.c',
                'sct_opengles1checkextensionaction.c',
                'sct_opengles1checkfunctionaction.c',
                'sct_opengles1checkvalueaction.c',
                'sct_opengles1clearaction.c',
                'sct_opengles1cleararrayaction.c',
                'sct_opengles1clearcoloraction.c',
                'sct_opengles1cleardepthaction.c',
                'sct_opengles1clearstencilaction.c',
                'sct_opengles1clientactivetextureaction.c',
                'sct_opengles1clipplaneaction.c',
                'sct_opengles1coloraction.c',
                'sct_opengles1colorbufferaction.c',
                'sct_opengles1colormaskaction.c',
                'sct_opengles1colorpointeraction.c',
                'sct_opengles1compressedteximage2daction.c',
                'sct_opengles1compressedteximage2dmmlaction.c',
                'sct_opengles1compressedtexsubimage2daction.c',
                'sct_opengles1compressedtexsubimage2dmmlaction.c',
                'sct_opengles1copyteximage2daction.c',
                'sct_opengles1copytexsubimage2daction.c',
                'sct_opengles1createarrayaction.c',
                'sct_opengles1createbartexturedataaction.c',
                'sct_opengles1createcheckertexturedataaction.c',
                'sct_opengles1createcompressedtexturedataaction.c',
                'sct_opengles1createdataaction.c',
                'sct_opengles1creategradienttexturedataaction.c',                
                'sct_opengles1createmeshaction.c',
                'sct_opengles1createnulltexturedataaction.c',
                'sct_opengles1createpalettecompressedtexturedataaction.c',
                'sct_opengles1createsolidtexturedataaction.c',
                'sct_opengles1createtexturedataaction.c',
                'sct_opengles1cullfaceaction.c',
                'sct_opengles1currentpalettematrixaction.c',
                'sct_opengles1decodeaction.c',                
                'sct_opengles1decodeetcaction.c',
                'sct_opengles1decodetargaaction.c',
                'sct_opengles1deletearrayaction.c',
                'sct_opengles1deletebufferaction.c',
                'sct_opengles1deletecompressedtexturedataaction.c',
                'sct_opengles1deletedataaction.c',
                'sct_opengles1deletemeshaction.c',
                'sct_opengles1deletetextureaction.c',
                'sct_opengles1deletetexturedataaction.c',
                'sct_opengles1depthfuncaction.c',
                'sct_opengles1depthmaskaction.c',
                'sct_opengles1depthrangeaction.c',
                'sct_opengles1disableaction.c',
                'sct_opengles1disableclientstateaction.c',
                'sct_opengles1drawarraysaction.c',
                'sct_opengles1drawelementsaction.c',
                'sct_opengles1drawtexaction.c',
                'sct_opengles1enableaction.c',
                'sct_opengles1enableclientstateaction.c',
                'sct_opengles1finishaction.c',
                'sct_opengles1flushaction.c',
                'sct_opengles1fogaction.c',
                'sct_opengles1frontfaceaction.c',
                'sct_opengles1frustumaction.c',
                'sct_opengles1genbufferaction.c',
                'sct_opengles1gentextureaction.c',
                'sct_opengles1hintaction.c',
                'sct_opengles1infoaction.c',
                'sct_opengles1lightaction.c',
                'sct_opengles1lightcoloraction.c',
                'sct_opengles1lightpositionaction.c',
                'sct_opengles1lightspotaction.c',
                'sct_opengles1lightspotdirectionaction.c',
                'sct_opengles1lightattenuationaction.c',
                'sct_opengles1lightmodelambientaction.c',
                'sct_opengles1lightmodeltwosidedaction.c',
                'sct_opengles1linewidthaction.c',
                'sct_opengles1loaddataaction.c',
                'sct_opengles1loadidentityaction.c',
                'sct_opengles1loadmatrixaction.c',
                'sct_opengles1loadpalettefrommodelviewmatrixaction.c',
                'sct_opengles1logicopaction.c',
                'sct_opengles1materialaction.c',
                'sct_opengles1matrixindexbufferaction.c',
                'sct_opengles1matrixindexpointeraction.c',
                'sct_opengles1matrixmodeaction.c',
                'sct_opengles1meshbufferdataaction.c',
                'sct_opengles1meshcolorpointeraction.c',
                'sct_opengles1meshnormalpointeraction.c',
                'sct_opengles1meshtexcoordpointeraction.c',
                'sct_opengles1meshvertexpointeraction.c',
                'sct_opengles1module.c',
                'sct_opengles1multitexcoordaction.c',
                'sct_opengles1multmatrixaction.c',
                'sct_opengles1normalaction.c',
                'sct_opengles1normalbufferaction.c',
                'sct_opengles1normalpointeraction.c',
                'sct_opengles1orthoaction.c',
                'sct_opengles1pointparameteraction.c',
                'sct_opengles1pointsizeaction.c',
                'sct_opengles1pointsizebufferaction.c',
                'sct_opengles1pointsizepointeraction.c',
                'sct_opengles1polygonoffsetaction.c',
                'sct_opengles1popmatrixaction.c',
                'sct_opengles1pushmatrixaction.c',
                'sct_opengles1readpixelsaction.c',
                'sct_opengles1rotateaction.c',
                'sct_opengles1samplecoverageaction.c',
                'sct_opengles1scaleaction.c',
                'sct_opengles1scissoraction.c',
                'sct_opengles1shademodelaction.c',
                'sct_opengles1stencilfuncaction.c',
                'sct_opengles1stencilmaskaction.c',
                'sct_opengles1stencilopaction.c',
                'sct_opengles1texcoordbufferaction.c',
                'sct_opengles1texcoordpointeraction.c',
                'sct_opengles1texenvaction.c',
                'sct_opengles1texenvcoloraction.c',
                'sct_opengles1texenvcombinealphaaction.c',
                'sct_opengles1texenvcombinergbaction.c',
                'sct_opengles1texenvcoordreplaceaction.c',
                'sct_opengles1texenvmodeaction.c',
                'sct_opengles1teximage2daction.c',
                'sct_opengles1teximage2dmmlaction.c',                
                'sct_opengles1texparameteraction.c',
                'sct_opengles1texparametergeneratemipmapsaction.c',
                'sct_opengles1texparametermagfilteraction.c',
                'sct_opengles1texparameterminfilteraction.c',
                'sct_opengles1texparameterwrapsaction.c',
                'sct_opengles1texparameterwraptaction.c',
                'sct_opengles1texsubimage2daction.c',
                'sct_opengles1texsubimage2dmmlaction.c',                
                'sct_opengles1texturecroprectaction.c',
                'sct_opengles1translateaction.c',
                'sct_opengles1utils.c',
                'sct_opengles1vertexbufferaction.c',
                'sct_opengles1vertexpointeraction.c',
                'sct_opengles1viewportaction.c',
                'sct_opengles1weightbufferaction.c',
                'sct_opengles1weightpointeraction.c',
                'sct_opengles1eglimagetargettexture2daction.c',
                parserSource ]

lib = env.Library( 'opengles1_module.lib', sourceFiles )
Return( 'lib' )
