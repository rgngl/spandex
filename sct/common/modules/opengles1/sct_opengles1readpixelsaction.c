/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1readpixelsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1ReadPixelsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1ReadPixelsActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1ReadPixelsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1ReadPixelsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ReadPixels@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1ReadPixelsActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1ReadPixelsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1ReadPixelsActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1ReadPixelsActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in ReadPixels@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1ReadPixelsActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1ReadPixelsActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1ReadPixelsActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1ReadPixelsActionContext*    context;
    OpenGLES1ReadPixelsActionData*          data;
    GLenum                                  err;
    SCTOpenGLES1Data*                       rawData;
    int                                     dataLength;
    void*                                   buffer;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1ReadPixelsActionContext* )( action->context );
    data    = &( context->data );

    dataLength = data->width * data->height * sctOpenGLES1GetTexelSize( OPENGLES1_RGBA8888 );
    SCT_ASSERT( dataLength > 0 );

    rawData = sctiOpenGLES1ModuleGetData( context->moduleContext, data->dataIndex );
    if( rawData != NULL )
    {
        if( rawData->length != dataLength )
        {
            SCT_LOG_ERROR( "Invalid data in ReadPixels@OpenGLES1 action execute." );
            return SCT_FALSE;
        }        
    }
    else  
    {    
        buffer = siCommonMemoryAlloc( NULL, dataLength );
        if( buffer == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in ReadPixels@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
        
        rawData = sctOpenGLES1CreateData( buffer, dataLength, SCT_FALSE );
        if( rawData == NULL )
        {
            siCommonMemoryFree( NULL, buffer );
            SCT_LOG_ERROR( "Allocation failed in ReadPixels@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        sctiOpenGLES1ModuleSetData( context->moduleContext, data->dataIndex, rawData );
    }

    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
    glReadPixels( data->x, data->y, data->width, data->height, GL_RGBA, GL_UNSIGNED_BYTE, rawData->data );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in ReadPixels@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1ReadPixelsActionTerminate( SCTAction* action )
{
    SCTOpenGLES1ReadPixelsActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTOpenGLES1ReadPixelsActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteData( context->moduleContext, context->data.dataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1ReadPixelsActionDestroy( SCTAction* action )
{
    SCTOpenGLES1ReadPixelsActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1ReadPixelsActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1ReadPixelsActionContext( context );
}
