/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1checkextensionaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1CheckExtensionActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1CheckExtensionActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1CheckExtensionActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CheckExtensionActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CheckExtension@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1CheckExtensionActionContext ) );

    if( sctiParseOpenGLES1CheckExtensionActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CheckExtensionActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1CheckExtensionActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CheckExtensionActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1CheckExtensionActionContext*    context;
    const char*                                 str;
    const char*                                 s;
    int                                         len;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1CheckExtensionActionContext* )( action->context );

    /* Get OpenGLES1 extension string. */
    str = ( const char* )( glGetString( GL_EXTENSIONS ) );
    if( str == NULL )
    {
        SCT_LOG_ERROR( "Getting extension string failed in CheckExtension@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    len = strlen( context->data.extension );
    
    if( ( s = strstr( str, context->data.extension ) ) == NULL ||
        ( s[ len ] != ' ' &&
          s[ len ] != '\0' ) )
    {
        char buf[ 1024 ];
        sprintf( buf, "Extension %s not found in CheckExtension@OpenGLES1 action execute.", context->data.extension );
        SCT_LOG_ERROR( buf );
        
        return SCT_FALSE;
    }  

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CheckExtensionActionDestroy( SCTAction* action )
{
    SCTOpenGLES1CheckExtensionActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1CheckExtensionActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1CheckExtensionActionContext( context );
}

