/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1createdataaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1CreateDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1CreateDataActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1CreateDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CreateDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateData@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1CreateDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1CreateDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in CreateData@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1CreateDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1CreateDataActionContext*    context;
    OpenGLES1CreateDataActionData*          data;
    SCTOpenGLES1Data*                       rawData;
    void*                                   buffer;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1CreateDataActionContext* )( action->context );
    data    = &( context->data );

    rawData = sctiOpenGLES1ModuleGetData( context->moduleContext, data->dataIndex );
    if( rawData != NULL )
    {
        if( rawData->length != data->length )
        {
            SCT_LOG_ERROR( "Data index already used in CreateData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
    }
    else
    {
        buffer = siCommonMemoryAlloc( NULL, data->length );
        if( buffer == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in CreateData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        rawData = sctOpenGLES1CreateData( buffer, data->length, SCT_FALSE );
        if( rawData == NULL )
        {
            siCommonMemoryFree( NULL, buffer );
            SCT_LOG_ERROR( "Allocation failed in CreateData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
        
        sctiOpenGLES1ModuleSetData( context->moduleContext, data->dataIndex, rawData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES1CreateDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1CreateDataActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteData( context->moduleContext, context->data.dataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES1CreateDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1CreateDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1CreateDataActionContext( context );
}
