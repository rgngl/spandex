/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_OPENGLES1CLIPPLANEACTION_H__ )
#define __SCT_OPENGLES1CLIPPLANEACTION_H__

#include "sct_types.h"
#include "sct_opengles1module.h"
#include "sct_opengles1module_parser.h"

/*!
 *
 */
typedef struct
{
    OpenGLES1ClipPlaneActionData        data;
} SCTOpenGLES1ClipPlaneActionContext;
 
#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                               sctiCreateOpenGLES1ClipPlaneActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroyOpenGLES1ClipPlaneActionContext( void* context );

    SCTBoolean                          sctiOpenGLES1ClipPlaneActionInit( SCTAction* action, SCTBenchmark* benchmark );        
    SCTBoolean                          sctiOpenGLES1ClipPlaneActionExecute( SCTAction* action, int frameNumber );
    void                                sctiOpenGLES1ClipPlaneActionTerminate( SCTAction* action );       
    void                                sctiOpenGLES1ClipPlaneActionDestroy( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_OPENGLES1CLIPPLANEACTION_H__ ) */
