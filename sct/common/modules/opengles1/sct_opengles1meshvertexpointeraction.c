/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1meshvertexpointeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1MeshVertexPointerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1MeshVertexPointerActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1MeshVertexPointerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1MeshVertexPointerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in MeshVertexPointer@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1MeshVertexPointerActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1MeshVertexPointerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1MeshVertexPointerActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidMeshIndex( context->moduleContext, context->data.meshIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1MeshVertexPointerActionContext( context );        
        SCT_LOG_ERROR( "Invalid mesh index in MeshVertexPointer@OpenGLES1 context creation." );
        return NULL;
    }
   
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1MeshVertexPointerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1MeshVertexPointerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1MeshVertexPointerActionContext* context;
    OpenGLES1MeshVertexPointerActionData*       data;
    SCTOpenGLES1Mesh*                           mesh;
    int                                         err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1MeshVertexPointerActionContext* )( action->context );
    data    = &( context->data );

    mesh = sctiOpenGLES1ModuleGetMesh( context->moduleContext, data->meshIndex );
    if( mesh == NULL )
    {
        SCT_LOG_ERROR( "Invalid mesh index in MeshVertexPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES1IsValidMeshArrayIndex( mesh, data->arrayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Array index out of bounds in MeshVertexPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    glVertexPointer( sctOpenGLES1MeshArrayComponents( mesh, data->arrayIndex ),
                     sctOpenGLES1MeshArrayType( mesh, data->arrayIndex ), 
                     sctOpenGLES1MeshArrayStride( mesh, data->arrayIndex ),
                     sctOpenGLES1MeshArrayPointer( mesh, data->arrayIndex ) );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in MeshVertexPointer@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();
        
        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1MeshVertexPointerActionDestroy( SCTAction* action )
{
    SCTOpenGLES1MeshVertexPointerActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1MeshVertexPointerActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1MeshVertexPointerActionContext( context );
}
