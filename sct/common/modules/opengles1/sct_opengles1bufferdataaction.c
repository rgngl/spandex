/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1bufferdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1BufferDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1BufferDataActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1BufferDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1BufferDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BufferData@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1BufferDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1BufferDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferDataActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in BufferData@OpenGLES1 context creation." );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferDataActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in BufferData@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1BufferDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BufferDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_LOG_ERROR( "BufferData@OpenGLES1 not supported in OpenGLES 1.0." );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BufferDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1BufferDataActionContext*    context;
    GLenum                                  err;
    OpenGLES1BufferDataActionData*          data;
    SCTOpenGLES1Array*                      array;
    SCTOpenGLES1Buffer*                     buffer;
    int                                     elementSize;
    int                                     offset;
    int                                     size;
    unsigned char*                          ptr;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1BufferDataActionContext* )( action->context );
    data    = &( context->data );

#if defined( GL_VERSION_ES_CM_1_1 )
    array = sctiOpenGLES1ModuleGetArray( context->moduleContext, data->arrayIndex );
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array in BufferData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    buffer = sctiOpenGLES1ModuleGetBuffer( context->moduleContext, data->bufferIndex );
    if( buffer == NULL )
    {
        SCT_LOG_ERROR( "Invalid buffer in BufferData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    elementSize = sctOpenGLES1GetGLTypeSize( array->type );
    offset      = data->offset * elementSize;

    if( offset >= array->lengthInBytes )
    {
        SCT_LOG_ERROR( "Offset exceeds array length in BufferData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( data->size <= 0 )
    {
        if( array->lengthInBytes == 0 )
        {
            SCT_LOG_ERROR( "Undefined size with null array data in BufferData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        size = ( array->lengthInBytes - offset );
    }
    else
    {
        size = data->size * elementSize;

        if( ( array->data != NULL ) && ( ( offset + size ) > array->lengthInBytes ) )
        {
            SCT_LOG_ERROR( "Offset + size exceeds array length in BufferData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
    }

    ptr = ( unsigned char* )( array->data ) + offset;
    if( data->bind != SCT_FALSE )
    {
        glBindBuffer( data->target, buffer->buffer );
    }
    glBufferData( data->target, size, ptr, data->usage );
    sctOpenGLES1InitializeBufferFromArray( buffer, array, ( data->offset ), ( size / elementSize ) );
#else   /* defined( GL_VERSION_ES_CM_1_1 ) */
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( array );
    SCT_USE_VARIABLE( buffer );
    SCT_USE_VARIABLE( elementSize );
    SCT_USE_VARIABLE( offset );
    SCT_USE_VARIABLE( size );
    SCT_USE_VARIABLE( ptr );
    SCT_ASSERT_ALWAYS( 0 );
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */
    
#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BufferData@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1BufferDataActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenGLES1BufferDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES1BufferDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1BufferDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1BufferDataActionContext( context );
}
