/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1texsubimage2dmmlaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1TexSubImage2DMMLActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1TexSubImage2DMMLActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1TexSubImage2DMMLActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1TexSubImage2DMMLActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in TexSubImage2DMML@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1TexSubImage2DMMLActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1TexSubImage2DMMLActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1TexSubImage2DMMLActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1TexSubImage2DMMLActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in TexSubImage2DMML@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1TexSubImage2DMMLActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TexSubImage2DMMLActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1TexSubImage2DMMLActionContext*  context;
    int                                         err;
    OpenGLES1TexSubImage2DMMLActionData*        data;
    SCTOpenGLES1TextureData*                    textureData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1TexSubImage2DMMLActionContext* )( action->context );
    data    = &( context->data );

    textureData = sctiOpenGLES1ModuleGetTextureData( context->moduleContext, data->textureDataIndex );    
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Invalid texture data in TexSubImage2DMML@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

    glTexSubImage2D( data->target, 
                     data->mipmapLevel, 
                     data->xoffset,
                     data->yoffset,
                     textureData->mipmaps[ 0 ].width, 
                     textureData->mipmaps[ 0 ].height, 
                     textureData->glFormat, 
                     textureData->glType,
                     textureData->mipmaps[ 0 ].data );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in TexSubImage2DMML@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();
        
        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1TexSubImage2DMMLActionDestroy( SCTAction* action )
{
    SCTOpenGLES1TexSubImage2DMMLActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1TexSubImage2DMMLActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1TexSubImage2DMMLActionContext( context );
}
