/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1decodetargaaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

#define SCT_TARGA_HEADER_LENGTH         18
#define SCT_MINIMUM_TARGA_LENGTH        SCT_TARGA_HEADER_LENGTH + 4

/*!
 *
 *
 */
void* sctiCreateOpenGLES1DecodeTargaActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1DecodeTargaActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1DecodeTargaActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1DecodeTargaActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DecodeTarga@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1DecodeTargaActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1DecodeTargaActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DecodeTargaActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.srcDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DecodeTargaActionContext( context );
        SCT_LOG_ERROR( "Invalid source data index in DecodeTarga@OpenGLES1 context creation." );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.dstDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DecodeTargaActionContext( context );
        SCT_LOG_ERROR( "Invalid destination data index in DecodeTarga@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1DecodeTargaActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1DecodeTargaActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1DecodeTargaActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1DecodeTargaActionContext*   context;
    OpenGLES1DecodeTargaActionData*         data;
    SCTOpenGLES1Data*                       srcData;
    SCTOpenGLES1Data*                       dstData;
    unsigned char*                          ptr;
    int                                     width;
    int                                     height;
    int                                     bits;
    int                                     length;
    int                                     i;
    unsigned char                           r, g, b, a;    
    int                                     bytesPerPixel;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1DecodeTargaActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiOpenGLES1ModuleGetData( context->moduleContext, data->srcDataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data index in DecodeTarga@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( srcData->length < SCT_MINIMUM_TARGA_LENGTH )
    {
        SCT_LOG_ERROR( "Invalid targa data in DecodeTarga@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    ptr = ( unsigned char* )( srcData->data );
    if( ptr[ 0 ]  != 0 ||
        ptr[ 1 ]  != 0 ||
        ptr[ 2 ]  != 2 ||
        ptr[ 3 ]  != 0 ||
        ptr[ 4 ]  != 0 ||
        ptr[ 5 ]  != 0 ||
        ptr[ 6 ]  != 0 ||
        ptr[ 7 ]  != 0 ||
        ptr[ 8 ]  != 0 ||
        ptr[ 9 ]  != 0 ||
        ptr[ 10 ] != 0 ||
        ptr[ 11 ] != 0 )
    {
        SCT_LOG_ERROR( "Unsupported targa header in DecodeTarga@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    width     = ( ( ptr[ 12 ] & 0xFF ) | ( ptr[ 13 ] << 8 ) );
    height    = ( ( ptr[ 14 ] & 0xFF ) | ( ptr[ 15 ] << 8 ) );
    bits      = ptr[ 16 ];

    if( bits != 24 && bits != 32 )
    {
        SCT_LOG_ERROR( "Unsupported pixel depth in DecodeTarga@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    bytesPerPixel = bits / 8;
    length = width * height * bytesPerPixel;

    if( srcData->length < ( SCT_TARGA_HEADER_LENGTH + length ) )
    {
        SCT_LOG_ERROR( "Invalid targa length in DecodeTarga@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    dstData = sctiOpenGLES1ModuleGetData( context->moduleContext, data->dstDataIndex );
    if( dstData != NULL )
    {
        if( dstData->length != length )
        {
            SCT_LOG_ERROR( "Destination data index already in use in DecodeTarga@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        memcpy( dstData->data, srcData->data, length );
    }
    else
    {
        dstData = sctOpenGLES1CreateData( &( ptr[ SCT_TARGA_HEADER_LENGTH ] ), length, SCT_TRUE );
        if( dstData == NULL )
        {
            SCT_LOG_ERROR( "Destination data allocation failed in DecodeTarga@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        sctiOpenGLES1ModuleSetData( context->moduleContext, data->dstDataIndex, dstData );
    }

    if( bytesPerPixel == 3 )
    {
        /* Swap RGB order */
        for( i = 0; i < length; i += 3 )
        {
            ptr = ( unsigned char* )( dstData->data ) + i;
            r = *ptr++;
            g = *ptr++;
            b = *ptr++;
            
            ptr = ( unsigned char* )( dstData->data ) + i;
            *ptr++ = b;
            *ptr++ = g;
            *ptr++ = r;
        }
    }
    else if( bytesPerPixel == 4 )
    {
        /* Swap RGBA order */
        for( i = 0; i < length; i += 4 )
        {
            ptr = ( unsigned char* )( dstData->data ) + i;
            r = *ptr++;
            g = *ptr++;
            b = *ptr++;
            a = *ptr++;
            
            ptr = ( unsigned char* )( dstData->data ) + i;
            *ptr++ = b;
            *ptr++ = g;
            *ptr++ = r;
            *ptr++ = a;
        }
    }
    else
    {
        SCT_ASSERT_ALWAYS( 0 );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1DecodeTargaActionTerminate( SCTAction* action )
{
    SCTOpenGLES1DecodeTargaActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1DecodeTargaActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteData( context->moduleContext, context->data.dstDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1DecodeTargaActionDestroy( SCTAction* action )
{
    SCTOpenGLES1DecodeTargaActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1DecodeTargaActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1DecodeTargaActionContext( context );
}
