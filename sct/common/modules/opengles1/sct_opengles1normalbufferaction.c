/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1normalbufferaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1NormalBufferActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1NormalBufferActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1NormalBufferActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1NormalBufferActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in NormalBuffer@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1NormalBufferActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1NormalBufferActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1NormalBufferActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1NormalBufferActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in NormalBuffer@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1NormalBufferActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1NormalBufferActionInit( SCTAction* action, SCTBenchmark* benchmark )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_LOG_ERROR( "NormalBuffer@OpenGLES1 not supported in OpenGLES 1.0." );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1NormalBufferActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1NormalBufferActionContext*  context;
    OpenGLES1NormalBufferActionData*        data;
    SCTOpenGLES1Buffer*                     buffer;
    int                                     err;
    int                                     elementSize;
    GLenum                                  type;
    int                                     stride;
    int                                     offset;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1NormalBufferActionContext* )( action->context );
    data    = &( context->data );

    buffer = sctiOpenGLES1ModuleGetBuffer( context->moduleContext, data->bufferIndex );
    if( buffer == NULL )
    {
        SCT_LOG_ERROR( "Invalid buffer in NormalBuffer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    type        = sctOpenGLES1GetBufferArrayType( buffer, 0 );
    elementSize = sctOpenGLES1GetGLTypeSize( type );

    if( data->offset >= sctOpenGLES1GetBufferArrayLength( buffer, 0 ) )
    {
        SCT_LOG_ERROR( "Too short buffer data in NormalBuffer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    stride     = sctOpenGLES1GetBufferArrayStride( buffer, 0 );
    offset     = sctOpenGLES1GetBufferArrayOffset( buffer, 0 );

    if( stride == 0 )
    {
        offset = offset + data->offset * elementSize * 3;
    }
    else
    {
        offset = offset + data->offset * stride;
    }

    if( data->bind != SCT_FALSE )
    {
        glBindBuffer( GL_ARRAY_BUFFER, buffer->buffer );
    }
    glNormalPointer( type, stride, ( void* )( offset ) );    
    
#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in NormalBuffer@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1NormalBufferActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenGLES1NormalBufferActionDestroy( SCTAction* action )
{
    SCTOpenGLES1NormalBufferActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1NormalBufferActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1NormalBufferActionContext( context );
}
