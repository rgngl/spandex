/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1pointsizepointeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1PointSizePointerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1PointSizePointerActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1PointSizePointerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1PointSizePointerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PointSizePointer@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1PointSizePointerActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1PointSizePointerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1PointSizePointerActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )            
    {
        sctiDestroyOpenGLES1PointSizePointerActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in PointSizePointer@OpenGLES1 context creation." );
        return NULL;
    }

    context->func               = NULL;    
    context->extensionValidated = SCT_FALSE;   
                                     
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1PointSizePointerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1PointSizePointerActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenGLES1PointSizePointerActionContext*  context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenGLES1PointSizePointerActionContext* )( action->context );  

    context->func = ( SCTOpenGLES1PointSizePointerFunc )( siCommonGetProcAddress( NULL, "glPointSizePointerOES" ) );
    if( context->func == NULL )
    {
        SCT_LOG_ERROR( "glPointSizePointerOES function not found in PointSizePointer@OpenGLES1 context creation." );
        return SCT_FALSE;
    }
    context->extensionValidated = SCT_FALSE;
   
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1PointSizePointerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1PointSizePointerActionContext*  context;
    OpenGLES1PointSizePointerActionData*        data;
    SCTOpenGLES1Array*                          array;
    int                                         offset;        
    int                                         err;
    const char*                                 extString;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1PointSizePointerActionContext* )( action->context );
    data    = &( context->data );

    if( context->extensionValidated == SCT_FALSE )
    {
        extString = ( const char* )( glGetString( GL_EXTENSIONS ) );        
        if( extString == NULL || strstr( extString, "OES_point_size_array" ) == NULL )
        {
            SCT_LOG_ERROR( "OES_point_size_array extension not supported in PointSizePointer@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
        context->extensionValidated = SCT_TRUE;
    }

    array = sctiOpenGLES1ModuleGetArray( context->moduleContext, data->arrayIndex );
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array index in PointSizePointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( data->offset >= array->length )
    {
        SCT_LOG_ERROR( "Offset out of bounds in PointSizePointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    offset = data->offset * sctOpenGLES1GetGLTypeSize( array->type );

    SCT_ASSERT( context->func != NULL );
    
    context->func( array->type,
                   0,
                   ( unsigned char* )( array->data ) + offset );   

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in PointSizePointer@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();
        
        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1PointSizePointerActionTerminate( SCTAction* action )
{
    SCTOpenGLES1PointSizePointerActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1PointSizePointerActionContext* )( action->context );
    
    context->func               = NULL;    
    context->extensionValidated = SCT_FALSE;
}

/*!
 *
 *
 */
void sctiOpenGLES1PointSizePointerActionDestroy( SCTAction* action )
{
    SCTOpenGLES1PointSizePointerActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1PointSizePointerActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1PointSizePointerActionContext( context );
}
