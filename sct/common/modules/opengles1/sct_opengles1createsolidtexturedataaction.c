/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1createsolidtexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1CreateSolidTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1CreateSolidTextureDataActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1CreateSolidTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CreateSolidTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateSolidTextureData@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1CreateSolidTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1CreateSolidTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateSolidTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateSolidTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in CreateSolidTextureData@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1CreateSolidTextureDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateSolidTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateSolidTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1CreateSolidTextureDataActionContext*    context;
    OpenGLES1CreateSolidTextureDataActionData*          data;
    SCTOpenGLES1TextureData*                            textureData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1CreateSolidTextureDataActionContext* )( action->context );
    data    = &( context->data );

    textureData = sctiOpenGLES1ModuleGetTextureData( context->moduleContext, data->textureDataIndex );
    if( textureData != NULL )
    {
        SCT_LOG_ERROR( "Texture data index already used in CreateSolidTextureData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    textureData = sctOpenGLES1CreateSolidTextureData( data->type, 
                                                      data->width, 
                                                      data->height, 
                                                      data->color, 
                                                      data->mipmaps );
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Texture data creation failed in CreateSolidTextureData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }
    sctiOpenGLES1ModuleSetTextureData( context->moduleContext, data->textureDataIndex, textureData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateSolidTextureDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES1CreateSolidTextureDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1CreateSolidTextureDataActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteTextureData( context->moduleContext, context->data.textureDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateSolidTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES1CreateSolidTextureDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1CreateSolidTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1CreateSolidTextureDataActionContext( context );
}
