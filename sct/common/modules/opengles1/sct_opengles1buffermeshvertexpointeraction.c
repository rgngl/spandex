/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1buffermeshvertexpointeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1BufferMeshVertexPointerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1BufferMeshVertexPointerActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1BufferMeshVertexPointerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1BufferMeshVertexPointerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BufferMeshVertexPointer@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1BufferMeshVertexPointerActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1BufferMeshVertexPointerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferMeshVertexPointerActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferMeshVertexPointerActionContext( context );        
        SCT_LOG_ERROR( "Invalid buffer index in BufferMeshVertexPointer@OpenGLES1 context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1BufferMeshVertexPointerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BufferMeshVertexPointerActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_LOG_ERROR( "BufferMeshVertexPointer@OpenGLES1 not supported in OpenGLES 1.0." );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BufferMeshVertexPointerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1BufferMeshVertexPointerActionContext*   context;
    OpenGLES1BufferMeshVertexPointerActionData*         data;
    SCTOpenGLES1Buffer*                                 buffer;
    GLenum                                              err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1BufferMeshVertexPointerActionContext* )( action->context );
    data    = &( context->data );

    buffer = sctiOpenGLES1ModuleGetBuffer( context->moduleContext, data->bufferIndex );
    if( buffer == NULL )
    {
        SCT_LOG_ERROR( "Invalid buffer in BufferMeshVertexPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES1IsValidBufferArrayIndex( buffer, data->arrayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Array index out of bounds in BufferMeshVertexPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( data->bind != SCT_FALSE )
    {
        glBindBuffer( GL_ARRAY_BUFFER, buffer->buffer );
    }
    glVertexPointer( sctOpenGLES1GetBufferArrayComponents( buffer, data->arrayIndex ), 
                     sctOpenGLES1GetBufferArrayType( buffer, data->arrayIndex ), 
                     sctOpenGLES1GetBufferArrayStride( buffer, data->arrayIndex ), 
                     ( const void* )( sctOpenGLES1GetBufferArrayOffset( buffer, data->arrayIndex ) ) );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BufferMeshVertexPointer@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1BufferMeshVertexPointerActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenGLES1BufferMeshVertexPointerActionDestroy( SCTAction* action )
{
    SCTOpenGLES1BufferMeshVertexPointerActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1BufferMeshVertexPointerActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1BufferMeshVertexPointerActionContext( context );
}
