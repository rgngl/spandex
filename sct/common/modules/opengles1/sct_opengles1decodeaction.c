/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1decodeaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

#define SCT_PVRTEXTOOL3_HEADER_LENGTH   52

/*!
 *
 *
 */
void* sctiCreateOpenGLES1DecodeActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1DecodeActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1DecodeActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1DecodeActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Decode@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1DecodeActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1DecodeActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DecodeActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.srcDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DecodeActionContext( context );
        SCT_LOG_ERROR( "Invalid source data index in Decode@OpenGLES1 context creation." );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.dstDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DecodeActionContext( context );
        SCT_LOG_ERROR( "Invalid destination data index in Decode@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1DecodeActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1DecodeActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1DecodeActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1DecodeActionContext*    context;
    OpenGLES1DecodeActionData*          data;
    SCTOpenGLES1Data*                   srcData;
    SCTOpenGLES1Data*                   dstData;
    unsigned char*                      ptr          = NULL;
    int                                 length       = 0;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1DecodeActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiOpenGLES1ModuleGetData( context->moduleContext, data->srcDataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data index in Decode@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( data->encoder == DECODE_ENCODER_PVRTEXTOOL3 )
    {
        if( srcData->length <= SCT_PVRTEXTOOL3_HEADER_LENGTH )
        {
            SCT_LOG_ERROR( "Invalid PVRTEXTOOL3 data in Decode@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        ptr    = &( ( unsigned char* )( srcData->data ) )[ SCT_PVRTEXTOOL3_HEADER_LENGTH ];
        length = srcData->length - SCT_PVRTEXTOOL3_HEADER_LENGTH;
    }
    else
    {
        SCT_LOG_ERROR( "Unexpected encoder in Decode@OpenGLES1 action execute." );
        return SCT_FALSE;
    }
    
    dstData = sctiOpenGLES1ModuleGetData( context->moduleContext, data->dstDataIndex );
    if( dstData != NULL )
    {
        if( dstData->length != length )
        {
            SCT_LOG_ERROR( "Destination data index already in use in Decode@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        memcpy( dstData->data, ptr, length );
    }
    else
    {
        dstData = sctOpenGLES1CreateData( ptr, length, SCT_TRUE );
        if( dstData == NULL )
        {
            SCT_LOG_ERROR( "Destination data allocation failed in Decode@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        sctiOpenGLES1ModuleSetData( context->moduleContext, data->dstDataIndex, dstData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1DecodeActionTerminate( SCTAction* action )
{
    SCTOpenGLES1DecodeActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1DecodeActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteData( context->moduleContext, context->data.dstDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1DecodeActionDestroy( SCTAction* action )
{
    SCTOpenGLES1DecodeActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1DecodeActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1DecodeActionContext( context );
}
