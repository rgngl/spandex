/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1utils.h"
#include "sct_utils.h"
#include "sct_sicommon.h"

#include <math.h>
#include <string.h>

#define TRIANGLE_LIMIT          ( SCT_MAX_SHORT / 3 )
#define MAX_SHORT               SCT_MAX_SHORT

/*!
 *
 */
typedef enum
{
    SCT_OPENGLES1_TEXTURE_PATTERN_NULL,
    SCT_OPENGLES1_TEXTURE_PATTERN_SOLID,
    SCT_OPENGLES1_TEXTURE_PATTERN_BAR,
    SCT_OPENGLES1_TEXTURE_PATTERN_CHECKER,
    SCT_OPENGLES1_TEXTURE_PATTERN_VGRADIENT,
    SCT_OPENGLES1_TEXTURE_PATTERN_HGRADIENT,
    SCT_OPENGLES1_TEXTURE_PATTERN_DGRADIENT       
} SCTOpenGLES1TexturePattern;

/*!
 *
 */
typedef struct
{
    int                         components;
    int                         type;
    unsigned char*              data;
    int                         size;
} SCTOpenGLES1Attrib;

/*!
 *
 */
static SCTOpenGLES1TextureData* sctiOpenGLES1CreateTextureData( SCTOpenGLES1TextureType type, int width, int height, SCTOpenGLES1TexturePattern pattern, const float color1[ 4 ], const float color2[ 4 ], int x, int y, SCTBoolean mipmaps );
static unsigned char*           sctiCreateSolidTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color[ 4 ] );
static unsigned char*           sctiCreateBarTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int barCount );
static unsigned char*           sctiCreateCheckerTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int checkerCountX, int checkerCountY  );
static unsigned char*           sctiCreateGradientTextureData( SCTOpenGLES1TextureType type, SCTOpenGLES1TexturePattern gradient, int width, int height, const float color1[ 4 ], const float color2[ 4 ] );

static void                     sctiGetTextureTypeSizeAndElements( SCTOpenGLES1TextureType type, int* size, int* elements );
static void                     sctiAssignTextureColor( SCTOpenGLES1TextureType type, void* dest, const float* color );

static void*                    sctiGetMeshAttrib( SCTOpenGLES1Mesh* mesh, int arrayIndex, int attributeIndex );
static SCTOpenGLES1Mesh*        sctiOpenGLES1CreateEmptyMesh( SCTOpenGLES1Attrib* attributes, int attributesLength, int attributeCount );

/*!
 *
 */
static unsigned char* sctiCreateSolidTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color[ 4 ] ) 
{
    int                 size        = 0;
    int                 elements    = 0;
    unsigned char*      buf;
    unsigned char*      bluf;
    int                 x, y;

    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color != NULL );

    sctiGetTextureTypeSizeAndElements( type, &size, &elements );
    SCT_ASSERT( size != 0 && elements != 0 );

    /* Reserve memory for texture data */
    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, size * width * height ) );
    if( buf == NULL )
    {
        return NULL;
    }
    bluf = buf;

    /* Fill in the texture data */
    for( y = 0; y < height; ++y )
    {
        for( x = 0; x < width; ++x )
        {
            sctiAssignTextureColor( type, bluf, color );
            bluf += size;
        }
    }

    return buf;
}

/*!
 *
 *
 */
static unsigned char* sctiCreateBarTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int barCount ) 
{
    int                 size        = 0;
    int                 elements    = 0;
    unsigned char*      buf;
    unsigned char*      bluf;
    int                 x, y;
    int                 d;

    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color1 != NULL );
    SCT_ASSERT( color2 != NULL );
    SCT_ASSERT( barCount > 0 );

    /* Calculate the bar width in texels. Make sure bar width is at least 1 */
    d = width / barCount;
    d = d ? d : 1;

    sctiGetTextureTypeSizeAndElements( type, &size, &elements );
    SCT_ASSERT( size != 0 && elements != 0 );

    /* Reserve memory for texture data */
    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, size * width * height ) );
    if( buf == NULL )
    {
        return NULL;
    }
    bluf = buf;

    /* Fill in the texture data */
    for( y = 0; y < height; ++y )
    {
        for( x = 0; x < width; ++x )
        {
            sctiAssignTextureColor( type, bluf, ( ( x / d ) % 2 ) ? color1 : color2 );
            bluf += size;
        }
    }

    return buf;
}

/*!
 *
 *
 */
static unsigned char* sctiCreateCheckerTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int checkerCountX, int checkerCountY ) 
{
    int                 size        = 0;
    int                 elements    = 0;
    unsigned char*      buf;
    unsigned char*      bluf;
    int                 x, y;
    int                 dx, dy;
    const float*        color;

    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color1 != NULL );
    SCT_ASSERT( color2 != NULL );
    SCT_ASSERT( checkerCountX > 0 );
    SCT_ASSERT( checkerCountY > 0 );

    dx = width / checkerCountX;
    dx = dx ? dx : 1;
    dy = height / checkerCountY;
    dy = dy ? dy : 1;

    sctiGetTextureTypeSizeAndElements( type, &size, &elements );
    SCT_ASSERT( size != 0 && elements != 0 );

    /* Reserve memory for texture data */
    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, size * width * height ) );
    if( buf == NULL )
    {
        return NULL;
    }
    bluf = buf;

    /* Fill in the texture data */
    for( y = 0; y < height; ++y )
    {
        for( x = 0; x < width; ++x )
        {
            if( ( x / dx ) % 2 )
            {
                if( ( y / dy ) % 2 )
                {
                    color = color1;
                }
                else 
                {
                    color = color2;
                }
            }
            else
            {
                if( ( y / dy ) % 2 )
                {
                    color = color2;
                }
                else
                {
                    color = color1;
                }
            }
            sctiAssignTextureColor( type, bluf, color );
            bluf += size;
        }
    }

    return buf;
}

/*!
 *
 *
 */
static unsigned char* sctiCreateGradientTextureData( SCTOpenGLES1TextureType type, SCTOpenGLES1TexturePattern gradient, int width, int height, const float color1[ 4 ], const float color2[ 4 ] ) 
{
    int                 size        = 0;
    int                 elements    = 0;
    unsigned char*      buf;
    unsigned char*      bluf;
    int                 x, y;
    float               d           = 0.0;
    float               r;
    float               color[ 4 ];
   
    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color1 != NULL );
    SCT_ASSERT( color2 != NULL );
       
    sctiGetTextureTypeSizeAndElements( type, &size, &elements );
    SCT_ASSERT( size != 0 && elements != 0 );

    /* Reserve memory for texture data */
    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, size * width * height ) );
    if( buf == NULL )
    {
        return NULL;
    }
    bluf = buf;
   
    if( gradient == SCT_OPENGLES1_TEXTURE_PATTERN_HGRADIENT )
    {
        if( width > 1 )
        {
            d = 1.0f / ( float )( width - 1 );
        }
        for( y = 0; y < height; ++y )
        {
            r = 0;            
            for( x = 0; x < width; ++x )
            {
                color[ 0 ] = color1[ 0 ] * ( 1 - r ) + color2[ 0 ] * ( r );
                color[ 1 ] = color1[ 1 ] * ( 1 - r ) + color2[ 1 ] * ( r );
                color[ 2 ] = color1[ 2 ] * ( 1 - r ) + color2[ 2 ] * ( r );
                color[ 3 ] = color1[ 3 ] * ( 1 - r ) + color2[ 3 ] * ( r );                
                sctiAssignTextureColor( type, bluf, color );
                bluf += size;
                r += d;
            }
        }
    }
    else if( gradient == SCT_OPENGLES1_TEXTURE_PATTERN_VGRADIENT )
    {
        if( height > 1 )
        {
            d = 1.0f / ( float )( height - 1 );
        }
        r = 0;
        
        for( y = 0; y < height; ++y )
        {
            color[ 0 ] = color1[ 0 ] * ( 1 - r ) + color2[ 0 ] * ( r );
            color[ 1 ] = color1[ 1 ] * ( 1 - r ) + color2[ 1 ] * ( r );
            color[ 2 ] = color1[ 2 ] * ( 1 - r ) + color2[ 2 ] * ( r );
            color[ 3 ] = color1[ 3 ] * ( 1 - r ) + color2[ 3 ] * ( r );                
            
            for( x = 0; x < width; ++x )
            {
                sctiAssignTextureColor( type, bluf, color );
                bluf += size;
            }
            r += d;
        }
    }
    else if( gradient == SCT_OPENGLES1_TEXTURE_PATTERN_DGRADIENT )
    {
        d = 1.0f / ( float )( width * height - 1 );
        r = 0;
        
        for( y = 0; y < height; ++y )
        {
            for( x = 0; x < width; ++x )
            {
                r = y * x * d;
                
                color[ 0 ] = color1[ 0 ] * ( 1 - r ) + color2[ 0 ] * ( r );
                color[ 1 ] = color1[ 1 ] * ( 1 - r ) + color2[ 1 ] * ( r );
                color[ 2 ] = color1[ 2 ] * ( 1 - r ) + color2[ 2 ] * ( r );
                color[ 3 ] = color1[ 3 ] * ( 1 - r ) + color2[ 3 ] * ( r );                
                
                sctiAssignTextureColor( type, bluf, color );
                bluf += size;
            }
        }
    }
    else
    {
        siCommonMemoryFree( NULL, buf );
        return NULL;
    }
    
    return buf;
}

/*!
 *
 */
static void sctiGetTextureTypeSizeAndElements( SCTOpenGLES1TextureType type, int* size, int* elements )
{
    SCT_ASSERT( type == OPENGLES1_LUMINANCE8 || 
                type == OPENGLES1_ALPHA8     || 
                type == OPENGLES1_LUMINANCE_ALPHA88 ||
                type == OPENGLES1_RGB565     || 
                type == OPENGLES1_RGB888     || 
                type == OPENGLES1_RGBA4444   ||
                type == OPENGLES1_RGBA5551   || 
                type == OPENGLES1_RGBA8888   || 
                type == OPENGLES1_RGBA8888 );
    SCT_ASSERT( size != NULL );
    SCT_ASSERT( elements != NULL );

    switch( type )
    {
    case OPENGLES1_LUMINANCE8:
    case OPENGLES1_ALPHA8:
        *size     = sizeof( char );
        *elements = 1;
        break;

    case OPENGLES1_LUMINANCE_ALPHA88:
        *size     = sizeof( char ) * 2;
        *elements = 2;
        break;

    case OPENGLES1_RGB565:
        *size     = sizeof( char ) * 2;
        *elements = 3;
        break;

    case OPENGLES1_RGBA4444:
    case OPENGLES1_RGBA5551:
        *size     = sizeof( char ) * 2;
        *elements = 4;
        break;

    case OPENGLES1_RGB888:
        *size     = sizeof( char ) * 3;
        *elements = 3;
        break;

    case OPENGLES1_RGBA8888:
        *size     = sizeof( char ) * 4;
        *elements = 4;
        break;
    }
}

/*!
 *
 */
void sctiAssignTextureColor( SCTOpenGLES1TextureType type, void* dest, const float* color )
{
    unsigned char  *cp = ( unsigned char* )( dest );
    unsigned short *sp = ( unsigned short* )( dest );
    unsigned short   s;

    SCT_ASSERT( type == OPENGLES1_LUMINANCE8 || 
                type == OPENGLES1_ALPHA8     || 
                type == OPENGLES1_LUMINANCE_ALPHA88 ||
                type == OPENGLES1_RGB565     || 
                type == OPENGLES1_RGB888     ||
                type == OPENGLES1_RGBA4444   ||
                type == OPENGLES1_RGBA5551   || 
                type == OPENGLES1_RGBA8888 );
    SCT_ASSERT( dest != NULL );
    SCT_ASSERT( color != NULL );

    switch( type )
    {
    case OPENGLES1_LUMINANCE8:
        *cp   = ( unsigned char )( color[ 0 ] * 255 );
        break;
        
    case OPENGLES1_ALPHA8:
        *cp   = ( unsigned char )( color[ 3 ] * 255 );
        break;
        
    case OPENGLES1_LUMINANCE_ALPHA88:
        *cp++ = ( unsigned char )( color[ 0 ] * 255 );
        *cp   = ( unsigned char )( color[ 3 ] * 255 );
        break;

    case OPENGLES1_RGB565:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 0 ] * 31 ) << 11 ) & 0xF800 ) | 
            ( ( ( unsigned int )( color[ 1 ] * 63 ) << 5 )  & 0x07E0 ) |
            (   ( unsigned int )( color[ 2 ] * 31 )         & 0x001F ) );
        *sp = s;
        break;

    case OPENGLES1_RGBA4444:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 0 ] * 15 ) << 12 ) & 0xF000 ) | 
            ( ( ( unsigned int )( color[ 1 ] * 15 ) << 8 )  & 0x0F00 ) |
            ( ( ( unsigned int )( color[ 2 ] * 15 ) << 4 )  & 0x00F0 ) |
              ( ( unsigned int )( color[ 3 ] * 15 )         & 0x000F ) );
        *sp = s;
        break;

    case OPENGLES1_RGBA5551:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 0 ] * 31 ) << 11 ) & 0xF800 ) | 
            ( ( ( unsigned int )( color[ 1 ] * 31 ) << 6 )  & 0x07C0 ) |
            ( ( ( unsigned int )( color[ 2 ] * 31 ) << 1 )  & 0x003E ) |
              ( ( unsigned int )( color[ 3 ] + 0.5f )       & 0x0001 ) );
        *sp = s;
        break;

    case OPENGLES1_RGB888:
        *cp++ = ( unsigned char )( color[ 0 ] * 255 );
        *cp++ = ( unsigned char )( color[ 1 ] * 255 );
        *cp   = ( unsigned char )( color[ 2 ] * 255 );
        break;

    case OPENGLES1_RGBA8888:
        *cp++ = ( unsigned char )( color[ 0 ] * 255 );
        *cp++ = ( unsigned char )( color[ 1 ] * 255 );
        *cp++ = ( unsigned char )( color[ 2 ] * 255 );
        *cp   = ( unsigned char )( color[ 3 ] * 255 );
        break;
    }
}

/*!
 *
 *
 */
int sctOpenGLES1GetMipmapLevels( int width, int height )
{
    int         w       = width;
    int         h       = height;
    int         levels   = 0;

    SCT_ASSERT( width > 0 );
    SCT_ASSERT( height > 0 );

    for( ; ( w != 0 ) || ( h != 0 ) ; )
    {
        w = w / 2;
        h = h / 2; 
        ++levels;
    }

    return levels;
}

/*!
 *
 */
static SCTOpenGLES1TextureData* sctiOpenGLES1CreateTextureData( SCTOpenGLES1TextureType type, int width, int height, SCTOpenGLES1TexturePattern pattern, const float color1[ 4 ], const float color2[ 4 ], int x, int y, SCTBoolean mipmaps )
{
    int                         mipmapLevels    = 1;
    int                         w               = width;
    int                         h               = height;
    int                         i;
    SCTOpenGLES1TextureData*    textureData;

    SCT_ASSERT( type == OPENGLES1_LUMINANCE8 || 
                type == OPENGLES1_ALPHA8     || 
                type == OPENGLES1_LUMINANCE_ALPHA88 ||
                type == OPENGLES1_RGB565     || 
                type == OPENGLES1_RGB888     || 
                type == OPENGLES1_RGBA4444   ||
                type == OPENGLES1_RGBA5551   || 
                type == OPENGLES1_RGBA8888 );

    SCT_ASSERT( width > 0 );
    SCT_ASSERT( height > 0 );
    SCT_ASSERT( pattern != SCT_OPENGLES1_TEXTURE_PATTERN_NULL || ( color1 == NULL && color2 == NULL && x == 0 && y == 0 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES1_TEXTURE_PATTERN_SOLID || ( color2 == NULL && x == 0 && y == 0 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES1_TEXTURE_PATTERN_BAR || ( x > 0 && y == 0 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES1_TEXTURE_PATTERN_CHECKER || ( x > 0 && y > 0 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES1_TEXTURE_PATTERN_VGRADIENT || ( x == 0 && y == 0 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES1_TEXTURE_PATTERN_HGRADIENT || ( x == 0 && y == 0 ) );

    textureData = ( SCTOpenGLES1TextureData* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1TextureData ) ) );
    if( textureData == NULL )
    {
        return NULL;
    }
    memset( textureData, 0, sizeof( SCTOpenGLES1TextureData ) );

    textureData->type = type;
    
    if( sctOpenGLES1MapTextureTypeToGL( type, &( textureData->glFormat ), &( textureData->glType ) ) == SCT_FALSE )
    {
        sctOpenGLES1DestroyTextureData( textureData );
        return NULL;
    }

    /* Calculate number of mipmaps, if mipmap generation is enabled */
    if( mipmaps == SCT_TRUE )
    {
        mipmapLevels = sctOpenGLES1GetMipmapLevels( width, height );
    }

    /* Reserve memory for mipmap data structures */
    textureData->mipmaps = ( SCTOpenGLES1MipMap* )( siCommonMemoryAlloc( NULL, mipmapLevels * sizeof( SCTOpenGLES1MipMap ) ) );
    if( textureData->mipmaps == NULL )
    {       
        sctOpenGLES1DestroyTextureData( textureData );
        return NULL;
    }
    memset( textureData->mipmaps, 0, mipmapLevels * sizeof( SCTOpenGLES1MipMap ) );
    textureData->mipmapCount = mipmapLevels;

    /* Create texture data for each mipmap level */
    for( i = 0; i < mipmapLevels; ++i )
    {
        textureData->mipmaps[ i ].width     = w;
        textureData->mipmaps[ i ].height    = h;
        switch( pattern )
        {
        default:
        case SCT_OPENGLES1_TEXTURE_PATTERN_NULL:
            textureData->mipmaps[ i ].data = NULL;
            break;

        case SCT_OPENGLES1_TEXTURE_PATTERN_SOLID:
            textureData->mipmaps[ i ].data  = sctiCreateSolidTextureData( type, w, h, color1 );
            break;

        case SCT_OPENGLES1_TEXTURE_PATTERN_BAR:
            textureData->mipmaps[ i ].data  = sctiCreateBarTextureData( type, w, h, color1, color2, x );
            break;

        case SCT_OPENGLES1_TEXTURE_PATTERN_CHECKER:
            textureData->mipmaps[ i ].data  = sctiCreateCheckerTextureData( type, w, h, color1, color2, x, y );
            break;
            
        case SCT_OPENGLES1_TEXTURE_PATTERN_VGRADIENT:   /* Intentional */
        case SCT_OPENGLES1_TEXTURE_PATTERN_HGRADIENT:   /* Intentional */
        case SCT_OPENGLES1_TEXTURE_PATTERN_DGRADIENT:   /* Intentional */
            textureData->mipmaps[ i ].data  = sctiCreateGradientTextureData( type, pattern, w, h, color1, color2 );
            break;
        }

        if( pattern != SCT_OPENGLES1_TEXTURE_PATTERN_NULL && textureData->mipmaps[ i ].data == NULL )
        {
            sctOpenGLES1DestroyTextureData( textureData );
            return NULL;
        }

        /* Divide w and h by 2 if they larger than 1 */
        w = w > 1 ? w / 2 : w;
        h = h > 1 ? h / 2 : h;

        SCT_ASSERT( w >= 1 );
        SCT_ASSERT( h >= 1 );
    }

    return textureData;
}

/*!
 *
 */
SCTOpenGLES1TextureData* sctOpenGLES1CreateNullTextureData( SCTOpenGLES1TextureType type, int width, int height, SCTBoolean mipmaps )
{
    return sctiOpenGLES1CreateTextureData( type, width, height, SCT_OPENGLES1_TEXTURE_PATTERN_NULL, NULL, NULL, 0, 0, mipmaps );
}

/*!
 *
 */
SCTOpenGLES1TextureData* sctOpenGLES1CreateSolidTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color[ 4 ], SCTBoolean mipmaps )
{
    return sctiOpenGLES1CreateTextureData( type, width, height, SCT_OPENGLES1_TEXTURE_PATTERN_SOLID, color, NULL, 0, 0, mipmaps );
}

/*!
 *
 */
SCTOpenGLES1TextureData* sctOpenGLES1CreateBarTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int bars, SCTBoolean mipmaps )
{
    return sctiOpenGLES1CreateTextureData( type, width, height, SCT_OPENGLES1_TEXTURE_PATTERN_BAR, color1, color2, bars, 0, mipmaps );
}

/*!
 *
 */
SCTOpenGLES1TextureData* sctOpenGLES1CreateCheckerTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int checkerX, int checkerY, SCTBoolean mipmaps )
{
    return sctiOpenGLES1CreateTextureData( type, width, height, SCT_OPENGLES1_TEXTURE_PATTERN_CHECKER, color1, color2, checkerX, checkerY, mipmaps );
}

/*!
 *
 */
SCTOpenGLES1TextureData* sctOpenGLES1CreateGradientTextureData( SCTOpenGLES1TextureType type, SCTOpenGLES1Gradient gradient, int width, int height, const float color1[ 4 ], const float color2[ 4 ], SCTBoolean mipmaps )
{
    SCTOpenGLES1TexturePattern  g;
    
    switch( gradient )
    {
    case OPENGLES1_GRADIENT_VERTICAL:
        g = SCT_OPENGLES1_TEXTURE_PATTERN_VGRADIENT;
        break;

    case OPENGLES1_GRADIENT_HORIZONTAL:
        g = SCT_OPENGLES1_TEXTURE_PATTERN_HGRADIENT;
        break;

    case OPENGLES1_GRADIENT_DIAGONAL:
        g = SCT_OPENGLES1_TEXTURE_PATTERN_DGRADIENT;
        break;
        
    default:
        return NULL;

    }
    return sctiOpenGLES1CreateTextureData( type, width, height, g, color1, color2, 0, 0, mipmaps );
}

/*!
 *
 */
void sctOpenGLES1DestroyTextureData( SCTOpenGLES1TextureData* textureData )
{
    int i;

    if( textureData != NULL )
    {
        for( i = 0; i < textureData->mipmapCount; ++i )
        {
            SCT_ASSERT_ALWAYS( textureData->mipmaps != NULL );
            if( textureData->mipmaps[ i ].data != NULL )
            {
                siCommonMemoryFree( NULL, textureData->mipmaps[ i ].data );
            }
        }
        if( textureData->mipmaps != NULL )
        {
            siCommonMemoryFree( NULL, textureData->mipmaps );
        }
        siCommonMemoryFree( NULL, textureData );
    }
}

/*!
 *
 */
SCTOpenGLES1CompressedTextureData* sctOpenGLES1CreateCompressedTextureData( GLenum internalFormat, SCTOpenGLES1CompressedMipMap* mipmaps, int mipmapCount )
{
    SCTOpenGLES1CompressedTextureData*  compressedTextureData;

    SCT_ASSERT( mipmaps != NULL );
    SCT_ASSERT( mipmapCount > 0 );

    compressedTextureData = ( SCTOpenGLES1CompressedTextureData* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CompressedTextureData ) ) );
    if( compressedTextureData == NULL )
    {
        return NULL;
    }

    compressedTextureData->internalFormat       = internalFormat;
    compressedTextureData->mipmaps              = mipmaps;
    compressedTextureData->mipmapCount          = mipmapCount;

    return compressedTextureData;
}

/*!
 *
 */
void sctOpenGLES1DestroyCompressedMipMapData( SCTOpenGLES1CompressedMipMap* mipmaps, int mipmapCount )
{
    int i;

    if( mipmaps == NULL )
    {
        return;
    }

    SCT_ASSERT( mipmapCount > 0 );

    for( i = 0; i < mipmapCount; ++i )
    {
        if( mipmaps[ i ].data != NULL )
        {
            siCommonMemoryFree( NULL, mipmaps[ i ].data );
            mipmaps[ i ].data = NULL;
        }
    }

    siCommonMemoryFree( NULL, mipmaps );
}

/*!
 *
 */
void sctOpenGLES1DestroyCompressedTextureData( SCTOpenGLES1CompressedTextureData* textureData )
{
    if( textureData != NULL )
    {
        if( textureData->mipmaps != NULL )
        {
            sctOpenGLES1DestroyCompressedMipMapData( textureData->mipmaps, textureData->mipmapCount );
            textureData->mipmaps = NULL;
        }
        siCommonMemoryFree( NULL, textureData );
    }
}

/*!
 *
 */
int sctOpenGLES1GetTexelSize( SCTOpenGLES1TextureType type )
{
    switch( type )
    {
    case OPENGLES1_LUMINANCE8:
    case OPENGLES1_ALPHA8:
        return 1;

    case OPENGLES1_LUMINANCE_ALPHA88:
    case OPENGLES1_RGB565:
    case OPENGLES1_RGBA4444:
    case OPENGLES1_RGBA5551:
        return 2;

    case OPENGLES1_RGB888:
        return 3;

    case OPENGLES1_RGBA8888:
        return 4;
    default:
        return -1;
    };
}

/*!
 *
 */
SCTBoolean sctOpenGLES1MapTextureTypeToGL( SCTOpenGLES1TextureType type, GLenum* glFormat, GLenum* glType )
{
    switch( type )
    {
    case OPENGLES1_LUMINANCE8:
        *glFormat = GL_LUMINANCE;
        *glType   = GL_UNSIGNED_BYTE;
        break;

    case OPENGLES1_ALPHA8:
        *glFormat = GL_ALPHA;
        *glType   = GL_UNSIGNED_BYTE;
        break;

    case OPENGLES1_LUMINANCE_ALPHA88:
        *glFormat = GL_LUMINANCE_ALPHA;
        *glType   = GL_UNSIGNED_BYTE;
        break;

    case OPENGLES1_RGB565:
        *glFormat = GL_RGB;
        *glType   = GL_UNSIGNED_SHORT_5_6_5;
        break;

    case OPENGLES1_RGB888:
        *glFormat = GL_RGB;
        *glType   = GL_UNSIGNED_BYTE;
        break;

    case OPENGLES1_RGBA4444:
        *glFormat = GL_RGBA;
        *glType   = GL_UNSIGNED_SHORT_4_4_4_4;
        break;

    case OPENGLES1_RGBA5551:
        *glFormat = GL_RGBA;
        *glType   = GL_UNSIGNED_SHORT_5_5_5_1;
        break;

    case OPENGLES1_RGBA8888:
        *glFormat = GL_RGBA;
        *glType   = GL_UNSIGNED_BYTE;
        break;

    default:
        return SCT_FALSE;
    };

    return SCT_TRUE;
}

/*!
 *
 */
int sctOpenGLES1GetGLTypeSize( GLenum type )
{
    switch( type )
    {
    case GL_BYTE:
        return sizeof( GLbyte );

    case GL_UNSIGNED_BYTE:
        return sizeof( GLubyte );

    case GL_SHORT:
        return sizeof( GLshort );

    case GL_UNSIGNED_SHORT:
        return sizeof( GLushort );

    case GL_FLOAT:
        return sizeof( GLfloat );

    case GL_FIXED:
        return sizeof( GLfixed );

    default:
        return -1;
    }
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES1IsPot( int width, int height )
{
    if( ( ( ( width - 1 ) & width ) == 0 ) && ( ( ( height - 1 ) & height ) == 0 ) )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES1Texture* sctOpenGLES1CreateTexture( GLuint texture )
{
    SCTOpenGLES1Texture* t;

    t = ( SCTOpenGLES1Texture* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1Texture ) ) );
    if( t == NULL )
    {
        return NULL;
    }
    t->texture = texture;

    return t;
}

/*!
 *
 *
 */
void sctOpenGLES1DestroyTexture( SCTOpenGLES1Texture* texture )
{
    if( texture != NULL )
    {
        siCommonMemoryFree( NULL, texture );
    }
}

/*!
 *
 *
 */
SCTOpenGLES1Array* sctOpenGLES1CreateArray( GLenum type )
{
    SCTOpenGLES1Array*  array;

    array = ( SCTOpenGLES1Array* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1Array ) ) );
    if( array == NULL )
    {
        return NULL;
    }
    memset( array, 0, sizeof( SCTOpenGLES1Array ) );

    array->type         = type;

    return array;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES1ClearArray( SCTOpenGLES1Array* array )
{
    SCT_ASSERT( array != NULL );

    if( array->data != NULL )
    {
        siCommonMemoryFree( NULL, array->data );
        array->data = NULL;
    }

    array->length           = 0;
    array->lengthInBytes    = 0;
    array->maxLengthInBytes = 0;

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES1AddDataToArray( SCTOpenGLES1Array* array, const void* data, int length )
{
    int                 newlength;
    unsigned char*      ptr;

    SCT_ASSERT( array != NULL );
    SCT_ASSERT( data != NULL );
    SCT_ASSERT( length > 0 );

    newlength = array->lengthInBytes + length;

    if( newlength > array->maxLengthInBytes )
    {
        unsigned char* newdata; 
        int alloclength = sctMax( newlength, 2 * array->maxLengthInBytes );
        newdata = ( unsigned char* )( siCommonMemoryAlloc( NULL, alloclength ) );
        if( newdata == NULL )
        {
            return SCT_FALSE;
        }
        if( array->data != NULL )
        {
            memcpy( newdata, array->data, array->lengthInBytes );
            siCommonMemoryFree( NULL, array->data );
        }
        array->data             = newdata;
        array->maxLengthInBytes = alloclength;
    }

    ptr = ( unsigned char* )( array->data ) + array->lengthInBytes;
    memcpy( ptr, data, length );

    array->length        += length;
    array->lengthInBytes = newlength;

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES1AddDoubleDataToArray( SCTOpenGLES1Array* array, const double* data, int length )
{
    int                 newlength;
    unsigned char*      ptr;

    SCT_ASSERT( array != NULL );

    if( data == NULL || length <= 0 )
    {
        return SCT_TRUE;
    }

    newlength = array->lengthInBytes + sctOpenGLES1GetGLTypeSize( array->type ) * length;

    if( newlength > array->maxLengthInBytes )
    {
        unsigned char* newdata; 
        int alloclength = sctMax( newlength, 2 * array->maxLengthInBytes );
        newdata = ( unsigned char* )( siCommonMemoryAlloc( NULL, alloclength ) );
        if( newdata == NULL )
        {
            return SCT_FALSE;
        }
        if( array->data != NULL )
        {
            memcpy( newdata, array->data, array->lengthInBytes );
            siCommonMemoryFree( NULL, array->data );
        }
        array->data             = newdata;
        array->maxLengthInBytes = alloclength;
    }

    ptr = ( unsigned char* )( array->data ) + array->lengthInBytes;
    if( sctOpenGLES1ConvertDoubleArray( ptr, array->type, data, length ) == SCT_FALSE )
    {
        return SCT_FALSE;
    }

    array->length        += length;
    array->lengthInBytes = newlength;

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctOpenGLES1DestroyArray( SCTOpenGLES1Array* array )
{
    if( array != NULL )
    {
        if( array->data != NULL )
        {
            siCommonMemoryFree( NULL, array->data );
            array->data = NULL;
        }
        siCommonMemoryFree( NULL, array );
    }
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES1ConvertDoubleArray( void* dst, GLenum dstType, const double* src, int length )
{
    int                 i;
    SCTBoolean          ok        = SCT_TRUE;

    GLbyte*             bytedst   = ( GLbyte* )( dst );
    GLubyte*            ubytedst  = ( GLubyte* )( dst );
    GLshort*            shortdst  = ( GLshort* )( dst );
    GLushort*           ushortdst = ( GLushort* )( dst );
    GLfloat*            floatdst  = ( GLfloat* )( dst );
    GLfixed*            fixeddst  = ( GLfixed* )( dst );

    SCT_ASSERT( dst != NULL );
    SCT_ASSERT( dstType == GL_BYTE  || 
                dstType != GL_UNSIGNED_BYTE || 
                dstType != GL_SHORT || 
                dstType != GL_UNSIGNED_SHORT || 
                dstType != GL_FLOAT || 
                dstType != GL_FIXED );
    SCT_ASSERT( src != NULL );
    SCT_ASSERT( length > 0 );

    for( i = 0; i < length && ok == SCT_TRUE; ++i )
    {
        switch( dstType )
        {
        case GL_BYTE:
            if( *src < -0x7F || *src > 0x7F )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *bytedst++ = ( GLbyte )( *src++ );
            }
            break;
        case GL_UNSIGNED_BYTE:
            if( *src < 0 || *src > 0xFF )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *ubytedst++ = ( GLubyte )( *src++ );
            }
            break;
        case GL_SHORT:
            if( *src < -0x7FFF || *src > 0x7FFF )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *shortdst++ = ( GLshort )( *src++ );
            }
            break;
        case GL_UNSIGNED_SHORT:
            if( *src < 0 || *src > 0xFFFF )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *ushortdst++ = ( GLushort )( *src++ );
            }
            break;
        case GL_FIXED:
            if( *src < -0x7FFF || *src > 0x7FFF )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *fixeddst++ = ( GLfixed )( SCT_FLOAT_TO_FIXED( *src++ ) );
            }
            break;

        default:
        case GL_FLOAT:
            *floatdst++ = ( GLfloat )( *src++ );
            break;
        }
    }

    return ok;
}

/*!
 *
 *
 */
SCTOpenGLES1Buffer* sctOpenGLES1CreateBuffer( GLuint buffer )
{
    SCTOpenGLES1Buffer* b;

    b = ( SCTOpenGLES1Buffer* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1Buffer ) ) );
    if( b == NULL )
    {
        return NULL;
    }
    memset( b, 0, sizeof( SCTOpenGLES1Buffer ) );

    b->buffer = buffer;

    if( sctOpenGLES1InitializeBufferFromArray( b, NULL, -1, -1 ) == SCT_FALSE )
    {
        sctOpenGLES1DestroyBuffer( b );
        return NULL;
    }

    return b;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES1InitializeBufferFromArray( SCTOpenGLES1Buffer* buffer, SCTOpenGLES1Array* array, int offset, int length )
{
    int elementSize = 0;

    SCT_ASSERT( buffer != NULL );

    if( buffer->itemCount < 1 )
    {
        SCT_ASSERT( buffer->items == NULL );
        buffer->items = ( SCTOpenGLES1BufferItem* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1BufferItem ) ) );
        if( buffer->items == NULL )
        {
            return SCT_FALSE;
        }
        buffer->itemCount = 1;
    }

    if( array != NULL )
    {
        buffer->items[ 0 ].type          = array->type;
        buffer->items[ 0 ].length        = array->length;
        buffer->items[ 0 ].lengthInBytes = array->lengthInBytes;
        buffer->items[ 0 ].components    = -1;
        buffer->items[ 0 ].stride        = 0;
        buffer->items[ 0 ].offset        = 0;

        elementSize = sctOpenGLES1GetGLTypeSize( array->type );
    }
    else
    {
        buffer->items[ 0 ].type          = 0;
        buffer->items[ 0 ].length        = 0;
        buffer->items[ 0 ].lengthInBytes = 0;
        buffer->items[ 0 ].components    = -1;
        buffer->items[ 0 ].stride        = 0;
        buffer->items[ 0 ].offset        = 0;
    }

    if( offset >= 0 )
    {
        buffer->items[ 0 ].offset        = offset;
    }

    if( length >= 0 )
    {
        buffer->items[ 0 ].length        = length;
        buffer->items[ 0 ].lengthInBytes = length * elementSize;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES1InitializeBufferFromMesh( SCTOpenGLES1Buffer* buffer, SCTOpenGLES1Mesh* mesh )
{
    int         i;

    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( mesh != NULL );

    if( buffer->itemCount < mesh->attributeArrayCount )
    {
        if( buffer->items != NULL )
        {
            siCommonMemoryFree( NULL, buffer->items );
            buffer->items = NULL;
        }

        buffer->items = ( SCTOpenGLES1BufferItem* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1BufferItem ) * mesh->attributeArrayCount ) );
        if( buffer->items == NULL )
        {
            return SCT_FALSE;
        }
        memset( buffer->items, 0, sizeof( SCTOpenGLES1BufferItem ) * mesh->attributeArrayCount );

        buffer->itemCount = mesh->attributeArrayCount;
    }

    for( i = 0; i < mesh->attributeArrayCount; ++i )
    {
        buffer->items[ i ].type          = mesh->attributeArrays[ i ].type;
        buffer->items[ i ].length        = mesh->attributeCount * mesh->attributeArrays[ i ].components;
        buffer->items[ i ].lengthInBytes = mesh->attributeArrays[ i ].elementSizeInMem * mesh->attributeCount;
        buffer->items[ i ].components    = mesh->attributeArrays[ i ].components;
        buffer->items[ i ].stride        = mesh->attributeArrays[ i ].stride;
        buffer->items[ i ].offset        = ( unsigned int )( ( unsigned char* )( mesh->attributeArrays[ i ].pointer ) - ( unsigned char* )( mesh->data ) );
    }

    return SCT_TRUE;
}


/*!
 *
 *
 */
void sctOpenGLES1DestroyBuffer( SCTOpenGLES1Buffer* buffer )
{
    if( buffer != NULL )
    {
        if( buffer->items != NULL )
        {
            siCommonMemoryFree( NULL, buffer->items );
            buffer->items = NULL;
        }

        siCommonMemoryFree( NULL, buffer );
    }
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES1IsValidBufferArrayIndex( SCTOpenGLES1Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );

    if( index < 0 || index >= buffer->itemCount )
    {
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
int sctOpenGLES1GetBufferArrayCount( SCTOpenGLES1Buffer* buffer )
{
    SCT_ASSERT( buffer != NULL );

    return buffer->itemCount;
}


/*!
 *
 *
 */
GLenum sctOpenGLES1GetBufferArrayType( SCTOpenGLES1Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].type;
}

/*!
 *
 *
 */
int sctOpenGLES1GetBufferArrayLength( SCTOpenGLES1Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].length;
}

/*!
 *
 *
 */
int sctOpenGLES1GetBufferArrayLengthInBytes( SCTOpenGLES1Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].lengthInBytes;
}

/*!
 *
 *
 */
int sctOpenGLES1GetBufferArrayComponents( SCTOpenGLES1Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].components;
}

/*!
 *
 *
 */
int sctOpenGLES1GetBufferArrayStride( SCTOpenGLES1Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].stride;
}

/*!
 *
 *
 */
int sctOpenGLES1GetBufferArrayOffset( SCTOpenGLES1Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].offset;
}

/*!
 *
 *
 */
SCTOpenGLES1Data* sctOpenGLES1CreateData( void* data, int length, SCTBoolean copy )
{
    SCTOpenGLES1Data* d;

    SCT_ASSERT( data != NULL );
    SCT_ASSERT( length > 0 );

    d = ( SCTOpenGLES1Data* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1Data ) ) );
    if( d == NULL )
    {
        return NULL;
    }
    
    if( copy == SCT_TRUE )
    {
        d->data = siCommonMemoryAlloc( NULL, length );
        if( d->data == NULL )
        {
            siCommonMemoryFree( NULL, d );
            return NULL;
        }
        memcpy( d->data, data, length );
    }
    else
    {
        d->data = data;
    }
   
    d->length = length;

    return d;
}

/*!
 *
 *
 */
void sctOpenGLES1DestroyData( SCTOpenGLES1Data* data )
{
    if( data != NULL )
    {
        if( data->data != NULL )
        {
            siCommonMemoryFree( NULL, data->data );
            data->data = NULL;
        }
        siCommonMemoryFree( NULL, data );
    }
}

/*!
 *
 *
 */
SCTOpenGLES1Mesh* sctOpenGLES1CreateMesh( SCTOpenGLES1Array* arrays[], int components[], int arraysLength )
{
    SCTOpenGLES1Mesh*           mesh;
    int                         i, j;
    int                         attributeCount;
    SCTOpenGLES1Attrib*         attribs;

    SCT_ASSERT( arrays != NULL );
    SCT_ASSERT( components != NULL );
    SCT_ASSERT( arraysLength >= 1 );

    attribs = ( SCTOpenGLES1Attrib* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1Attrib ) * arraysLength ) );
    if( attribs == NULL )
    {
        return NULL;
    }

    attributeCount = ( arrays[ 0 ]->length / components[ 0 ] );
    if( attributeCount < 1 )
    {
        siCommonMemoryFree( NULL, attribs );
        return NULL;
    }

    for( i = 0; i < arraysLength; ++i )
    {
        // All arrays must have the same number of elements (component tuples)
        SCT_ASSERT( components[ i ] != 0 );
        if( ( arrays[ i ]->length / components[ i ] ) != attributeCount )
        {
            siCommonMemoryFree( NULL, attribs );
            return NULL;
        }
        attribs[ i ].components = components[ i ];
        attribs[ i ].type       = arrays[ i ]->type;
        attribs[ i ].data       = ( unsigned char* )( arrays[ i ]->data );
        attribs[ i ].size       = sctOpenGLES1GetGLTypeSize( arrays[ i ]->type ) * components[ i ];
    }

    mesh = sctiOpenGLES1CreateEmptyMesh( attribs, arraysLength, attributeCount );

    if( mesh == NULL )
    {
        siCommonMemoryFree( NULL, attribs );
        return NULL;
    }

    for( i = 0; i < arraysLength; ++i )
    {
        for( j = 0; j < attributeCount; j++ )
        {
            memcpy( sctiGetMeshAttrib( mesh, i, j ), attribs[ i ].data, attribs[ i ].size );
            attribs[ i ].data += attribs[ i ].size;
        }
    }

    siCommonMemoryFree( NULL, attribs );

    return mesh;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static void* sctiGetMeshAttrib( SCTOpenGLES1Mesh* mesh, int arrayIndex, int attributeIndex )
{
    SCT_ASSERT( mesh != NULL );
    SCT_ASSERT( arrayIndex >= 0 && arrayIndex < mesh->attributeArrayCount );
    SCT_ASSERT( attributeIndex >= 0 && attributeIndex < mesh->attributeCount );
    SCT_ASSERT( mesh->attributeArrays[ arrayIndex ].pointer != NULL );

    return ( unsigned char* )( mesh->attributeArrays[ arrayIndex ].pointer ) + ( attributeIndex * mesh->attributeArrays[ arrayIndex ].stride );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static SCTOpenGLES1Mesh* sctiOpenGLES1CreateEmptyMesh( SCTOpenGLES1Attrib* attributes, int attributesLength, int attributeCount )
{
    SCTOpenGLES1Mesh*   mesh;
    int                 accum;
    int                 meshElementSize, totalSize;
    int                 i;

    SCT_ASSERT( attributes != NULL );
    SCT_ASSERT( attributesLength >= 1 );
    SCT_ASSERT( attributeCount >= 1 );

    mesh = ( SCTOpenGLES1Mesh* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1Mesh ) ) );
    if( mesh == NULL )
    {
        return NULL;
    }
    memset( mesh, 0, sizeof( SCTOpenGLES1Mesh ) );

    mesh->attributeArrays = ( SCTOpenGLES1MeshItem* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1MeshItem ) * attributesLength ) );
    if( mesh->attributeArrays == NULL )
    {
        sctOpenGLES1DestroyMesh( mesh );
        return NULL;
    }

    mesh->attributeCount        = attributeCount;
    mesh->attributeArrayCount   = attributesLength;
    meshElementSize             = 0;
    totalSize                   = 0;

    for( i = 0; i < attributesLength; ++i )
    {
        mesh->attributeArrays[ i ].elementSizeInMem = attributes[ i ].size;
        mesh->attributeArrays[ i ].components       = attributes[ i ].components;
        mesh->attributeArrays[ i ].type             = attributes[ i ].type;

        mesh->attributeArrays[ i ].elementSizeInMem += mesh->attributeArrays[ i ].elementSizeInMem  < sizeof( long ) ? ( sizeof( long ) - mesh->attributeArrays[ i ].elementSizeInMem ) : mesh->attributeArrays[ i ].elementSizeInMem  % sizeof( long );

        meshElementSize += mesh->attributeArrays[ i ].elementSizeInMem;
    }

    totalSize = ( meshElementSize * attributeCount );

    SCT_ASSERT( totalSize > 0 );

    /* Store the total size of the data in vertex data buffer. */
    mesh->length = totalSize;

    /* Reserve memory for vertex data buffers. Zero memory. */
    mesh->data = siCommonMemoryAlloc( NULL, totalSize );
    if( mesh->data == NULL )
    {
        sctOpenGLES1DestroyMesh( mesh );
        return NULL;
    }
    memset( mesh->data, 0, totalSize );

    accum = 0;
    
    for( i = 0; i < attributesLength; ++i )
    {
        mesh->attributeArrays[ i ].pointer    = ( unsigned char* )( mesh->data ) + accum;
        mesh->attributeArrays[ i ].stride     = meshElementSize;
        accum += mesh->attributeArrays[ i ].elementSizeInMem;
    }
    
    return mesh;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void sctOpenGLES1DestroyMesh( SCTOpenGLES1Mesh* mesh )
{
    if( mesh != NULL )
    {
        if( mesh->data != NULL )
        {
            siCommonMemoryFree( NULL, mesh->data );
        }
        if( mesh->attributeArrays != NULL )
        {
            siCommonMemoryFree( NULL, mesh->attributeArrays );
        }

        siCommonMemoryFree( NULL, mesh );
    }
}

/*!
 *
 *
 */
int sctOpenGLES1MeshLength( SCTOpenGLES1Mesh* mesh )
{
    SCT_ASSERT( mesh != NULL );
    
    return mesh->length;
}

/*!
 *
 *
 */
void* sctOpenGLES1MeshData( SCTOpenGLES1Mesh* mesh )
{
    SCT_ASSERT( mesh != NULL );
    
    return mesh->data;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES1IsValidMeshArrayIndex( SCTOpenGLES1Mesh* mesh, int index )
{
    SCT_ASSERT( mesh != NULL );

    if( index < 0 || index >= mesh->attributeArrayCount )
    {
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
int sctOpenGLES1MeshArrayComponents( SCTOpenGLES1Mesh* mesh, int index )
{
    SCT_ASSERT( mesh != NULL );
    SCT_ASSERT( index >= 0 && index < mesh->attributeArrayCount );

    return mesh->attributeArrays[ index ].components;
}

/*!
 *
 *
 */
GLenum sctOpenGLES1MeshArrayType( SCTOpenGLES1Mesh* mesh, int index )
{
    SCT_ASSERT( mesh != NULL );
    SCT_ASSERT( index >= 0 && index < mesh->attributeArrayCount );

    return mesh->attributeArrays[ index ].type;
}

/*!
 *
 *
 */
int sctOpenGLES1MeshArrayStride( SCTOpenGLES1Mesh* mesh, int index )
{
    SCT_ASSERT( mesh != NULL );
    SCT_ASSERT( index >= 0 && index < mesh->attributeArrayCount );

    return mesh->attributeArrays[ index ].stride;
}

/*!
 *
 *
 */
void* sctOpenGLES1MeshArrayPointer( SCTOpenGLES1Mesh* mesh, int index )
{
    SCT_ASSERT( mesh != NULL );
    SCT_ASSERT( index >= 0 && index < mesh->attributeArrayCount );

    return mesh->attributeArrays[ index ].pointer;
}
