/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1currentpalettematrixaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1CurrentPaletteMatrixActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1CurrentPaletteMatrixActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1CurrentPaletteMatrixActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CurrentPaletteMatrixActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CurrentPaletteMatrix@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1CurrentPaletteMatrixActionContext ) );

    if( sctiParseOpenGLES1CurrentPaletteMatrixActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CurrentPaletteMatrixActionContext( context );
        return NULL;
    }

    context->func          = NULL;
    context->funcValidated = SCT_FALSE;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1CurrentPaletteMatrixActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CurrentPaletteMatrixActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenGLES1CurrentPaletteMatrixActionContext*  context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenGLES1CurrentPaletteMatrixActionContext* )( action->context );  

    context->func = ( SCTOpenGLES1CurrentPaletteMatrixFunc )( siCommonGetProcAddress( NULL, "glCurrentPaletteMatrixOES" ) );
    if( context->func == NULL )
    {
        SCT_LOG_ERROR( "glCurrentPaletteMatrixOES function not found in CurrentPaletteMatrix@OpenGLES1 action init." );
        return SCT_FALSE;
    }
    context->funcValidated = SCT_FALSE;
   
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CurrentPaletteMatrixActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1CurrentPaletteMatrixActionContext*  context;
    OpenGLES1CurrentPaletteMatrixActionData*        data;
    int                                             err;
    const char*                                     extString;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1CurrentPaletteMatrixActionContext* )( action->context );
    data    = &( context->data );
    
    if( context->funcValidated == SCT_FALSE )
    {
        extString = ( const char* )( glGetString( GL_EXTENSIONS ) );
        if( extString == NULL || strstr( extString, "OES_matrix_palette" ) == NULL )
        {
            SCT_LOG_ERROR( "OES_matrix_palette extension not supported in CurrentPaletteMatrix@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
        context->funcValidated = SCT_TRUE;
    }
    
    SCT_ASSERT( context->func != NULL );
    
    context->func( data->index );
    
#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
   err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in CurrentPaletteMatrix@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CurrentPaletteMatrixActionTerminate( SCTAction* action )
{
    SCTOpenGLES1CurrentPaletteMatrixActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1CurrentPaletteMatrixActionContext* )( action->context );

    context->func          = NULL;
    context->funcValidated = SCT_FALSE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CurrentPaletteMatrixActionDestroy( SCTAction* action )
{
    SCTOpenGLES1CurrentPaletteMatrixActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1CurrentPaletteMatrixActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1CurrentPaletteMatrixActionContext( context );
}
