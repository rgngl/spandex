/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1deletecompressedtexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1DeleteCompressedTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1DeleteCompressedTextureDataActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1DeleteCompressedTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1DeleteCompressedTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DeleteCompressedTextureData@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1DeleteCompressedTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1DeleteCompressedTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DeleteCompressedTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidCompressedTextureDataIndex( context->moduleContext, context->data.compressedTextureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DeleteCompressedTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid compressed texture data index in CompressedDeleteTextureData@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1DeleteCompressedTextureDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1DeleteCompressedTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1DeleteCompressedTextureDataActionContext*   context;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1DeleteCompressedTextureDataActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteCompressedTextureData( context->moduleContext, context->data.compressedTextureDataIndex );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1DeleteCompressedTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES1DeleteCompressedTextureDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1DeleteCompressedTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1DeleteCompressedTextureDataActionContext( context );
}
