/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1creategradienttexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1CreateGradientTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1CreateGradientTextureDataActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1CreateGradientTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CreateGradientTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateGradientTextureData@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1CreateGradientTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1CreateGradientTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateGradientTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateGradientTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in CreateGradientTextureData@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1CreateGradientTextureDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateGradientTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateGradientTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1CreateGradientTextureDataActionContext* context;
    OpenGLES1CreateGradientTextureDataActionData*       data;
    SCTOpenGLES1TextureData*                            textureData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1CreateGradientTextureDataActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES1ModuleGetTextureData( context->moduleContext, data->textureDataIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Texture data index already used in CreateGradientTextureData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    textureData = sctOpenGLES1CreateGradientTextureData( data->type,
                                                         data->gradient,
                                                         data->width, 
                                                         data->height, 
                                                         data->color1, 
                                                         data->color2, 
                                                         data->mipmaps );
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Texture data creation failed in CreateGradientTextureData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES1ModuleSetTextureData( context->moduleContext, data->textureDataIndex, textureData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateGradientTextureDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES1CreateGradientTextureDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1CreateGradientTextureDataActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteTextureData( context->moduleContext, context->data.textureDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateGradientTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES1CreateGradientTextureDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1CreateGradientTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1CreateGradientTextureDataActionContext( context );
}
