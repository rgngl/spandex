/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1meshcolorpointeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1MeshColorPointerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1MeshColorPointerActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1MeshColorPointerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1MeshColorPointerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in MeshColorPointer@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1MeshColorPointerActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1MeshColorPointerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1MeshColorPointerActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidMeshIndex( context->moduleContext, context->data.meshIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1MeshColorPointerActionContext( context );        
        SCT_LOG_ERROR( "Invalid mesh index in MeshColorPointer@OpenGLES1 context creation." );
        return NULL;
    }
   
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1MeshColorPointerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1MeshColorPointerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1MeshColorPointerActionContext*  context;
    OpenGLES1MeshColorPointerActionData*        data;
    SCTOpenGLES1Mesh*                           mesh;
    int                                         err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1MeshColorPointerActionContext* )( action->context );
    data    = &( context->data );

    mesh = sctiOpenGLES1ModuleGetMesh( context->moduleContext, data->meshIndex );
    if( mesh == NULL )
    {
        SCT_LOG_ERROR( "Invalid mesh index in MeshColorPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES1IsValidMeshArrayIndex( mesh, data->arrayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Array index out of bounds in MeshColorPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    glColorPointer( sctOpenGLES1MeshArrayComponents( mesh, data->arrayIndex ),
                    sctOpenGLES1MeshArrayType( mesh, data->arrayIndex ), 
                    sctOpenGLES1MeshArrayStride( mesh, data->arrayIndex ),
                    sctOpenGLES1MeshArrayPointer( mesh, data->arrayIndex ) );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in MeshColorPointer@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1MeshColorPointerActionDestroy( SCTAction* action )
{
    SCTOpenGLES1MeshColorPointerActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1MeshColorPointerActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1MeshColorPointerActionContext( context );
}
