/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1lightaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1LightActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1LightActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1LightActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1LightActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Light@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1LightActionContext ) );

    if( sctiParseOpenGLES1LightActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1LightActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1LightActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1LightActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1LightActionContext* context;
    int                             err;
    OpenGLES1LightActionData*       data;
    GLenum                          light;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1LightActionContext* )( action->context );
    data    = &( context->data );

    light = ( GLenum )( GL_LIGHT0 + data->light );
    
    GL_LIGHTFV( light, GL_AMBIENT, data->ambient );
    GL_LIGHTFV( light, GL_DIFFUSE, data->diffuse );
    GL_LIGHTFV( light, GL_SPECULAR, data->specular );
    GL_LIGHTFV( light, GL_POSITION, data->position );
    GL_LIGHTFV( light, GL_SPOT_DIRECTION, data->spotDirection );
    GL_LIGHTF( light, GL_SPOT_EXPONENT, data->spotExponent );
    GL_LIGHTF( light, GL_SPOT_CUTOFF, data->spotCutoff );
    GL_LIGHTF( light, GL_CONSTANT_ATTENUATION, data->constantAttenuation );
    GL_LIGHTF( light, GL_LINEAR_ATTENUATION, data->linearAttenuation );
    GL_LIGHTF( light, GL_QUADRATIC_ATTENUATION, data->quadraticAttenuation );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in Light@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1LightActionDestroy( SCTAction* action )
{
    SCTOpenGLES1LightActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1LightActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1LightActionContext( context );
}
