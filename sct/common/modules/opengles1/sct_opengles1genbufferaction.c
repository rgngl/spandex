/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1genbufferaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1GenBufferActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1GenBufferActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1GenBufferActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1GenBufferActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in GenBuffer@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1GenBufferActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1GenBufferActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1GenBufferActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1GenBufferActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in GenBuffer@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1GenBufferActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1GenBufferActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_LOG_ERROR( "GenBuffer@OpenGLES1 not supported in OpenGLES 1.0." );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1GenBufferActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1GenBufferActionContext* context;
    GLenum                              err;
    OpenGLES1GenBufferActionData*       data;
    SCTOpenGLES1Buffer*                 buffer;
    GLuint                              bufferId;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1GenBufferActionContext* )( action->context );
    data    = &( context->data );

#if defined( GL_VERSION_ES_CM_1_1 )
    if( sctiOpenGLES1ModuleGetBuffer( context->moduleContext, data->bufferIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Buffer index already used in GenBuffer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    glGenBuffers( 1, &bufferId );
    buffer = sctOpenGLES1CreateBuffer( bufferId );
    if( buffer == NULL )
    {
        glDeleteBuffers( 1, &bufferId );
        SCT_LOG_ERROR( "Buffer creation failed in GenBuffer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }
    
    sctiOpenGLES1ModuleSetBuffer( context->moduleContext, data->bufferIndex, buffer );
#else   /* defined( GL_VERSION_ES_CM_1_1 ) */
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( buffer );
    SCT_USE_VARIABLE( bufferId );
    SCT_ASSERT_ALWAYS( 0 );
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */
    
#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in GenBuffer@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1GenBufferActionTerminate( SCTAction* action )
{
    SCTOpenGLES1GenBufferActionContext* context;
    OpenGLES1GenBufferActionData*       data;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1GenBufferActionContext* )( action->context );
    data    = &( context->data );

    sctiOpenGLES1ModuleDeleteBuffer( context->moduleContext, data->bufferIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1GenBufferActionDestroy( SCTAction* action )
{
    SCTOpenGLES1GenBufferActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1GenBufferActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1GenBufferActionContext( context );
}
