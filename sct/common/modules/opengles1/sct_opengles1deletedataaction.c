/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1deletedataaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1DeleteDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1DeleteDataActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1DeleteDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1DeleteDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DeleteData@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1DeleteDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1DeleteDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DeleteDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DeleteDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in DeleteData@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1DeleteDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1DeleteDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1DeleteDataActionContext*    context;
    OpenGLES1DeleteDataActionData*          data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1DeleteDataActionContext* )( action->context );
    data    = &( context->data );

    sctiOpenGLES1ModuleDeleteData( context->moduleContext, data->dataIndex );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1DeleteDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES1DeleteDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1DeleteDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1DeleteDataActionContext( context );
}
