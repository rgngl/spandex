/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_OPENGLES1MODULE_H__ )
#define __SCT_OPENGLES1MODULE_H__

#include "sct_types.h"
#include "sct_opengles1utils.h"
#include "sct_gl1.h"

#define SCT_MAX_OPENGLES1_TEXTURE_DATAS                 4096
#define SCT_MAX_OPENGLES1_TEXTURES                      4096
#define SCT_MAX_OPENGLES1_COMPRESSED_TEXTURE_DATAS      4096
#define SCT_MAX_OPENGLES1_ARRAYS                        4096
#define SCT_MAX_OPENGLES1_BUFFERS                       4096
#define SCT_MAX_OPENGLES1_DATAS                         4096
#define SCT_MAX_OPENGLES1_MESHES                        4096

/*!
 *
 *
 */
typedef struct
{
    SCTOpenGLES1TextureData*            textureDatas[ SCT_MAX_OPENGLES1_TEXTURE_DATAS ];
    SCTOpenGLES1Texture*                textures[ SCT_MAX_OPENGLES1_TEXTURES ];
    SCTOpenGLES1CompressedTextureData*  compressedTextureDatas[ SCT_MAX_OPENGLES1_COMPRESSED_TEXTURE_DATAS ];
    SCTOpenGLES1Array*                  arrays[ SCT_MAX_OPENGLES1_ARRAYS ];
    SCTOpenGLES1Buffer*                 buffers[ SCT_MAX_OPENGLES1_BUFFERS ];
    SCTOpenGLES1Data*                   datas[ SCT_MAX_OPENGLES1_DATAS ];
    SCTOpenGLES1Mesh*                   meshes[ SCT_MAX_OPENGLES1_MESHES ];
} SCTOpenGLES1ModuleContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    SCTModule*                          sctCreateOpenGLES1Module( void );

    /* External OpenGLES1 module data access. */   
    SCTBoolean                          sctOpenGLES1ModuleSetDataExt( int index, SCTOpenGLES1Data* data );
    GLuint                              sctOpenGLES1ModuleGetTextureExt( int index );
    
    /* Texture datas */
    SCTBoolean                          sctiOpenGLES1ModuleIsValidTextureDataIndex( SCTOpenGLES1ModuleContext* moduleContext, int index );
    SCTOpenGLES1TextureData*            sctiOpenGLES1ModuleGetTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES1ModuleSetTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1TextureData* textureData );
    void                                sctiOpenGLES1ModuleDeleteTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index );

    /* Textures */
    SCTBoolean                          sctiOpenGLES1ModuleIsValidTextureIndex( SCTOpenGLES1ModuleContext* moduleContext, int index );
    SCTOpenGLES1Texture*                sctiOpenGLES1ModuleGetTexture( SCTOpenGLES1ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES1ModuleSetTexture( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1Texture* texture );
    void                                sctiOpenGLES1ModuleDeleteTexture( SCTOpenGLES1ModuleContext* moduleContext, int index );

    /* Compressed texture datas */
    SCTBoolean                          sctiOpenGLES1ModuleIsValidCompressedTextureDataIndex( SCTOpenGLES1ModuleContext* moduleContext, int index );
    SCTOpenGLES1CompressedTextureData*  sctiOpenGLES1ModuleGetCompressedTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES1ModuleSetCompressedTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1CompressedTextureData* compressedTextureData );
    void                                sctiOpenGLES1ModuleDeleteCompressedTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index );

    /* Arrays */
    SCTBoolean                          sctiOpenGLES1ModuleIsValidArrayIndex( SCTOpenGLES1ModuleContext* moduleContext, int index );
    SCTOpenGLES1Array*                  sctiOpenGLES1ModuleGetArray( SCTOpenGLES1ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES1ModuleSetArray( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1Array* array );
    void                                sctiOpenGLES1ModuleDeleteArray( SCTOpenGLES1ModuleContext* moduleContext, int index );

    /* Buffers */
    SCTBoolean                          sctiOpenGLES1ModuleIsValidBufferIndex( SCTOpenGLES1ModuleContext* moduleContext, int index );
    SCTOpenGLES1Buffer*                 sctiOpenGLES1ModuleGetBuffer( SCTOpenGLES1ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES1ModuleSetBuffer( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1Buffer* buffer );
    void                                sctiOpenGLES1ModuleDeleteBuffer( SCTOpenGLES1ModuleContext* moduleContext, int index );

    /* Datas */
    SCTBoolean                          sctiOpenGLES1ModuleIsValidDataIndex( SCTOpenGLES1ModuleContext* moduleContext, int index );
    SCTOpenGLES1Data*                   sctiOpenGLES1ModuleGetData( SCTOpenGLES1ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES1ModuleSetData( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1Data* data );
    void                                sctiOpenGLES1ModuleDeleteData( SCTOpenGLES1ModuleContext* moduleContext, int index );

    /* Meshes */
    SCTBoolean                          sctiOpenGLES1ModuleIsValidMeshIndex( SCTOpenGLES1ModuleContext* moduleContext, int index );
    SCTOpenGLES1Mesh*                   sctiOpenGLES1ModuleGetMesh( SCTOpenGLES1ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES1ModuleSetMesh( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1Mesh* mesh );
    void                                sctiOpenGLES1ModuleDeleteMesh( SCTOpenGLES1ModuleContext* moduleContext, int index );

    /* Clear error */
    const char*                         sctiOpenGLES1GetErrorString( int err );
    void                                sctiOpenGLES1ClearGLError( void );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_OPENGLES1MODULE_H__ ) */
