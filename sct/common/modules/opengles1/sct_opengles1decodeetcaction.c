/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1decodeetcaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

#define SCT_ATI_ETC_HEADER_LENGTH           20
#define SCT_MINIMUM_ATI_ETC_LENGTH          SCT_ATI_ETC_HEADER_LENGTH + 4

#define SCT_ERICSSON_ETC_HEADER_LENGTH      16
#define SCT_MINIMUM_ERICSSON_ETC_LENGTH     SCT_ERICSSON_ETC_HEADER_LENGTH + 4

/*!
 *
 *
 */
typedef struct
{
    unsigned int        signature;
    unsigned int        width;
    unsigned int        height;
    unsigned int        flags;
    unsigned int        dataOffset;  // From start of header/file
} SCT_ATI_ETC_HEADER;

/*!
 *
 *
 */
#define ATI_ETC_SIGNATURE   0xEC000001
#define ATI_ETC_RGB         0x00000001

/*!
 *
 *
 */
void* sctiCreateOpenGLES1DecodeETCActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1DecodeETCActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1DecodeETCActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1DecodeETCActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DecodeETC@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1DecodeETCActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1DecodeETCActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DecodeETCActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.srcDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DecodeETCActionContext( context );
        SCT_LOG_ERROR( "Invalid source data index in DecodeETC@OpenGLES1 context creation." );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.dstDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DecodeETCActionContext( context );
        SCT_LOG_ERROR( "Invalid destination data index in DecodeETC@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1DecodeETCActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1DecodeETCActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1DecodeETCActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1DecodeETCActionContext* context;
    OpenGLES1DecodeETCActionData*       data;
    SCTOpenGLES1Data*                   srcData;
    SCTOpenGLES1Data*                   dstData;
    unsigned char*                      ptr          = NULL;
    int                                 length       = 0;
    SCT_ATI_ETC_HEADER*                 header;
    unsigned int                        totalBlocks;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    SCT_ASSERT( sizeof( SCT_ATI_ETC_HEADER ) == SCT_ATI_ETC_HEADER_LENGTH );

    context = ( SCTOpenGLES1DecodeETCActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiOpenGLES1ModuleGetData( context->moduleContext, data->srcDataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data index in DecodeETC@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( data->encoder == DECODEETC_ENCODER_ATI )
    {
        if( srcData->length < SCT_MINIMUM_ATI_ETC_LENGTH )
        {
            SCT_LOG_ERROR( "Invalid ATI ETC data in DecodeETC@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        header = ( SCT_ATI_ETC_HEADER* )( srcData->data );

        if( header->signature != ATI_ETC_SIGNATURE )
        {
            SCT_LOG_ERROR( "Invalid ATI ETC signature in DecodeETC@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        if( header->flags != ATI_ETC_RGB )
        {
            SCT_LOG_ERROR( "Invalid ATI ETC flags in DecodeETC@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        totalBlocks = ( ( header->width / 4 ) * ( header->height / 4 ) );
        length      = totalBlocks * 8;

        if( srcData->length < ( SCT_ATI_ETC_HEADER_LENGTH + length ) )
        {
            SCT_LOG_ERROR( "Invalid ATI ETC length in DecodeETC@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        ptr = &( ( unsigned char* )( srcData->data ) )[ header->dataOffset ];
    }
    else if( data->encoder == DECODEETC_ENCODER_ERICSSON )
    {
        if( srcData->length < SCT_MINIMUM_ERICSSON_ETC_LENGTH )
        {
            SCT_LOG_ERROR( "Invalid ERICSSON ETC data in DecodeETC@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        length = srcData->length - SCT_ERICSSON_ETC_HEADER_LENGTH;
        
        ptr = &( ( unsigned char* )( srcData->data ) )[ SCT_ERICSSON_ETC_HEADER_LENGTH ];
    }
    else
    {
        SCT_LOG_ERROR( "Unexpected encoder in DecodeETC@OpenGLES1 action execute." );
        return SCT_FALSE;
    }
    
    dstData = sctiOpenGLES1ModuleGetData( context->moduleContext, data->dstDataIndex );
    if( dstData != NULL )
    {
        if( dstData->length != length )
        {
            SCT_LOG_ERROR( "Destination data index already in use in DecodeETC@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        memcpy( dstData->data, ptr, length );
    }
    else
    {
        dstData = sctOpenGLES1CreateData( ptr, length, SCT_TRUE );
        if( dstData == NULL )
        {
            SCT_LOG_ERROR( "Destination data allocation failed in DecodeETC@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        sctiOpenGLES1ModuleSetData( context->moduleContext, data->dstDataIndex, dstData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1DecodeETCActionTerminate( SCTAction* action )
{
    SCTOpenGLES1DecodeETCActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1DecodeETCActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteData( context->moduleContext, context->data.dstDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1DecodeETCActionDestroy( SCTAction* action )
{
    SCTOpenGLES1DecodeETCActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1DecodeETCActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1DecodeETCActionContext( context );
}
