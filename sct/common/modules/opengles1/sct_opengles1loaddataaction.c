/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1loaddataaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1LoadDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1LoadDataActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1LoadDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1LoadDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LoadData@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1LoadDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1LoadDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1LoadDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1LoadDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in LoadData@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1LoadDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1LoadDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1LoadDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1LoadDataActionContext*  context;
    OpenGLES1LoadDataActionData*        data;
    SCTOpenGLES1Data*                   rawData;
    void*                               fileContext;
    SCTFile                             file;
    int                                 fileSize;
    char*                               buffer;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1LoadDataActionContext* )( action->context );
    data    = &( context->data );

    rawData = sctiOpenGLES1ModuleGetData( context->moduleContext, data->dataIndex );

    fileContext = siFileCreateContext();
    if( fileContext == NULL )
    {
        SCT_LOG_ERROR( "Invalid file context in LoadData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    file = siFileOpen( fileContext, data->filename, "rb" );
    if( file == NULL )
    {
        siFileDestroyContext( fileContext );
        SCT_LOG_ERROR( "File open failed in LoadData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    fileSize = siFileSize( fileContext, file );
    if( fileSize < 1 )
    {
        siFileClose( fileContext, file );
        siFileDestroyContext( fileContext );        
        SCT_LOG_ERROR( "Invalid file size in LoadData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( rawData != NULL )
    {
        if( rawData->length != fileSize )
        {
            siFileClose( fileContext, file );
            siFileDestroyContext( fileContext );                    

            SCT_LOG_ERROR( "Data index already used in LoadData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
        buffer = ( char* )( rawData->data );
    }
    else
    {
        buffer = ( char* )( siCommonMemoryAlloc( NULL, fileSize ) );
        if( buffer == NULL )
        {
            siFileClose( fileContext, file );
            siFileDestroyContext( fileContext );
            SCT_LOG_ERROR( "File buffer alloc failed in LoadData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
    }

    if( siFileRead( fileContext, file, buffer, fileSize ) != ( unsigned int )( fileSize ) )
    {
        if( rawData == NULL )
        {
            siCommonMemoryFree( NULL, buffer );
        }
        siFileClose( fileContext, file );
        siFileDestroyContext( fileContext );
        SCT_LOG_ERROR( "File read failed in LoadData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    siFileClose( fileContext, file );
    siFileDestroyContext( fileContext );

    if( rawData == NULL )
    {
        rawData = sctOpenGLES1CreateData( buffer, fileSize, SCT_FALSE );
        if( rawData == NULL )
        {
            siCommonMemoryFree( NULL, buffer );
            SCT_LOG_ERROR( "Data creation failed in LoadData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        sctiOpenGLES1ModuleSetData( context->moduleContext, data->dataIndex, rawData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1LoadDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES1LoadDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1LoadDataActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteData( context->moduleContext, context->data.dataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1LoadDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES1LoadDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1LoadDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1LoadDataActionContext( context );
}
