/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1texenvcombinealphaaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1TexEnvCombineAlphaActionContext( void* moduleContext, SCTAttributeList* attributes )
{ 
    SCTOpenGLES1TexEnvCombineAlphaActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1TexEnvCombineAlphaActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1TexEnvCombineAlphaActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in TexEnvCombineAlpha@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1TexEnvCombineAlphaActionContext ) );

    if( sctiParseOpenGLES1TexEnvCombineAlphaActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1TexEnvCombineAlphaActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1TexEnvCombineAlphaActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TexEnvCombineAlphaActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_LOG_ERROR( "TexEnvCombineAlpha@OpenGLES1 not supported in OpenGLES 1.0." );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TexEnvCombineAlphaActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1TexEnvCombineAlphaActionContext*    context;
    int                                             err;
    OpenGLES1TexEnvCombineAlphaActionData*          data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1TexEnvCombineAlphaActionContext* )( action->context );
    data    = &( context->data );

    GL_TEX_ENVF( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE );
    GL_TEX_ENVF( GL_TEXTURE_ENV, GL_COMBINE_ALPHA, ( float )( data->func ) );
    GL_TEX_ENVF( GL_TEXTURE_ENV, GL_SRC0_ALPHA, ( float )( data->src0 ) );
    GL_TEX_ENVF( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, ( float )( data->operand0 ) );
    GL_TEX_ENVF( GL_TEXTURE_ENV, GL_SRC1_ALPHA, ( float )( data->src1 ) );
    GL_TEX_ENVF( GL_TEXTURE_ENV, GL_OPERAND1_ALPHA, ( float )( data->operand1 ) );
    GL_TEX_ENVF( GL_TEXTURE_ENV, GL_SRC2_ALPHA, ( float )( data->src2 ) );
    GL_TEX_ENVF( GL_TEXTURE_ENV, GL_OPERAND2_ALPHA, ( float )( data->operand2 ) );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in TexEnvCombineAlpha@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();
        
        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1TexEnvCombineAlphaActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenGLES1TexEnvCombineAlphaActionDestroy( SCTAction* action )
{
    SCTOpenGLES1TexEnvCombineAlphaActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1TexEnvCombineAlphaActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1TexEnvCombineAlphaActionContext( context );
}
