/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1checkvalueaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
typedef enum
{
    SCT_ELEMENT_TYPE_Z,
    SCT_ELEMENT_TYPE_R,
} SCTElementType;

/*!
 *
 *
 */
static const int elements[] = { GL_MAX_LIGHTS,                       1,  SCT_ELEMENT_TYPE_Z,
                                GL_MAX_CLIP_PLANES,                  1,  SCT_ELEMENT_TYPE_Z,
                                GL_MAX_MODELVIEW_STACK_DEPTH,        1,  SCT_ELEMENT_TYPE_Z,
                                GL_MAX_PROJECTION_STACK_DEPTH,       1,  SCT_ELEMENT_TYPE_Z,
                                GL_MAX_TEXTURE_STACK_DEPTH,          1,  SCT_ELEMENT_TYPE_Z,
                                GL_SUBPIXEL_BITS,                    1,  SCT_ELEMENT_TYPE_Z,
                                GL_MAX_TEXTURE_SIZE,                 1,  SCT_ELEMENT_TYPE_Z,
                                GL_MAX_VIEWPORT_DIMS,                2,  SCT_ELEMENT_TYPE_Z,
                                GL_ALIASED_POINT_SIZE_RANGE,         2,  SCT_ELEMENT_TYPE_R,
                                GL_SMOOTH_POINT_SIZE_RANGE,          2,  SCT_ELEMENT_TYPE_R,
                                GL_ALIASED_LINE_WIDTH_RANGE,         2,  SCT_ELEMENT_TYPE_R,
                                GL_SMOOTH_LINE_WIDTH_RANGE,          2,  SCT_ELEMENT_TYPE_R,
                                GL_MAX_TEXTURE_UNITS,                1,  SCT_ELEMENT_TYPE_Z,
                                GL_SAMPLE_BUFFERS,                   1,  SCT_ELEMENT_TYPE_Z,
                                GL_SAMPLES,                          1,  SCT_ELEMENT_TYPE_Z,
                                GL_COMPRESSED_TEXTURE_FORMATS,       0,  SCT_ELEMENT_TYPE_Z, /* Special case. */
                                GL_NUM_COMPRESSED_TEXTURE_FORMATS,   1,  SCT_ELEMENT_TYPE_Z,
                                GL_RED_BITS,                         1,  SCT_ELEMENT_TYPE_Z,
                                GL_GREEN_BITS,                       1,  SCT_ELEMENT_TYPE_Z,
                                GL_BLUE_BITS,                        1,  SCT_ELEMENT_TYPE_Z,
                                GL_ALPHA_BITS,                       1,  SCT_ELEMENT_TYPE_Z,
                                GL_DEPTH_BITS,                       1,  SCT_ELEMENT_TYPE_Z,
                                GL_STENCIL_BITS,                     1,  SCT_ELEMENT_TYPE_Z };

/*!
 *
 *
 */
static SCTBoolean sctiPassesCondition( float v1, float v2, SCTCondition c );

/*!
 *
 *
 */
void* sctiCreateOpenGLES1CheckValueActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1CheckValueActionContext*    context;
    int                                     i;
    int                                     len;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1CheckValueActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CheckValueActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CheckValue@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1CheckValueActionContext ) );

    if( sctiParseOpenGLES1CheckValueActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CheckValueActionContext( context );
        return NULL;
    }

    for( i = 0; i < SCT_ARRAY_LENGTH( elements ); i += 3 )
    {
        if( elements[ i ] == context->data.value )
        {
            len = elements[ i + 1 ];

            if( len == 0 )
            {
                /* Special cases that need to query number of elements. */
                if( context->data.condition != SCT_FOUND )
                {
                    SCT_LOG_ERROR( "Invalid condition in CheckValue@OpenGLES1 action context creation." );                
                    sctiDestroyOpenGLES1CheckValueActionContext( context );
                    return NULL;
                }
                
                break;
            }
            else if( len != context->data.values->length )
            {
                SCT_LOG_ERROR( "Values does not match value in CheckValue@OpenGLES1 action context creation." );                
                sctiDestroyOpenGLES1CheckValueActionContext( context );
                return NULL;
            }
            else
            {
                if( context->data.condition == SCT_FOUND )
                {
                    SCT_LOG_ERROR( "Unsupported condition in CheckValue@OpenGLES1 action context creation." );                
                    sctiDestroyOpenGLES1CheckValueActionContext( context );
                    return NULL;
                }
                
                break;
            }
        }
    }

    if( i >= SCT_ARRAY_LENGTH( elements ) )
    {
        SCT_LOG_ERROR( "Unexpected value in CheckValue@OpenGLES1 action context creation." );                
        sctiDestroyOpenGLES1CheckValueActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1CheckValueActionContext( void* context )
{
    SCTOpenGLES1CheckValueActionContext*    c;
   
    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenGLES1CheckValueActionContext* )( context );
    
    if( c->data.values != NULL )
    {
        sctDestroyFloatVector( c->data.values );
        c->data.values = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CheckValueActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1CheckValueActionContext*    context;
    OpenGLES1CheckValueActionData*          data;
    GLint                                   integerv[ 2 ];
    GLfloat                                 floatv[ 2 ];
    int                                     i;
    int                                     j;
    int                                     k;
    GLint                                   element;
    int                                     len;
    int                                     type;
    SCTBoolean                              s;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1CheckValueActionContext* )( action->context );
    data    = &( context->data );
   
    for( i = 0; i < SCT_ARRAY_LENGTH( elements ); i += 3 )
    {
        if( elements[ i ] == context->data.value )
        {
            element = elements[ i ];
            len     = elements[ i + 1 ];
            type    = elements[ i + 2 ];

            // Special cases
            if( element == GL_COMPRESSED_TEXTURE_FORMATS )
            {
                GLint   se          = GL_NUM_COMPRESSED_TEXTURE_FORMATS;
                int     sl;
                GLint*  rvalues; 

                SCT_ASSERT_ALWAYS( data->condition == SCT_FOUND );
                               
                glGetIntegerv( se, &sl );

                if( sl <= 0 )
                {
                    if( data->values->length != 0 )
                    {
                        SCT_LOG_ERROR( "Empty values in CheckValue@OpenGLES1 action execute." );
                        return SCT_FALSE;
                    }
                }
                else
                {
                    rvalues = ( GLint* )( siCommonMemoryAlloc( NULL, sl * sizeof( GLint ) ) );
                    if( rvalues == NULL )
                    {
                        SCT_LOG_ERROR( "Allocation failed in CheckValue@OpenGLES1 action execute." );
                        return SCT_FALSE;
                    }
                    memset( rvalues, 0, sl * sizeof( GLint ) );

                    glGetIntegerv( element, rvalues );

                    for( j = 0; j < data->values->length; ++j )
                    {
                        for( k = 0; k < sl; ++k )
                        {
                            if( ( GLint )( data->values->data[ j ] ) == rvalues[ k ] )
                            {
                                /* Found. */
                                break;
                            }
                        }
                        
                        if( k >= sl )
                        {
                            /* Not found. */
                            siCommonMemoryFree( NULL, rvalues );
                            SCT_LOG_ERROR( "Value not found in CheckValue@OpenGLES1 action execute." );
                            return SCT_FALSE;
                        }                   
                    }
                    siCommonMemoryFree( NULL, rvalues );
                }
            }
            else
            {
                switch( type )
                {
                case SCT_ELEMENT_TYPE_Z:
                    glGetIntegerv( element, integerv );
                    break;
                    
                case SCT_ELEMENT_TYPE_R:
                    glGetFloatv( element, floatv );
                    break;
                    
                default:
                    SCT_ASSERT_ALWAYS( 0 );
                    break;
                }

                s = SCT_TRUE;
                for( j = 0; j < len; ++j )
                {
                    switch( type )
                    {
                    case SCT_ELEMENT_TYPE_Z:
                        if( sctiPassesCondition( ( float )( integerv[ j ] ), data->values->data[ j ], data->condition ) == SCT_FALSE )
                        {
                            s = SCT_FALSE;
                        }
                        break;
                    
                    case SCT_ELEMENT_TYPE_R:
                        if( sctiPassesCondition( ( float )( floatv[ j ] ), data->values->data[ j ],  data->condition ) == SCT_FALSE )
                        {
                            s = SCT_FALSE;
                        }
                        break;
                    
                    default:
                        SCT_ASSERT_ALWAYS( 0 );
                        break;
                    }
                }

                if( s == SCT_FALSE )
                {
                    SCT_LOG_ERROR( "Condition failed in CheckValue@OpenGLES1 action execute." );
                    return SCT_FALSE;
                }               
            }
            
            break;            
        }
    }

    /* Should not come here without founding matching element. */
    SCT_ASSERT_ALWAYS( i < SCT_ARRAY_LENGTH( elements ) );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CheckValueActionDestroy( SCTAction* action )
{
    SCTOpenGLES1CheckValueActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1CheckValueActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1CheckValueActionContext( context );
}


/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static SCTBoolean sctiPassesCondition( float v1, float v2, SCTCondition c )
{
    switch( c )
    {
    case SCT_LESS:
        if( v1 < v2 )
        {
            return SCT_TRUE;
        }
        break;
        
    case SCT_LEQUAL:
        if( v1 <= v2 )
        {
            return SCT_TRUE;
        }
        break;
        
    case SCT_GREATER:
        if( v1 > v2 )
        {
            return SCT_TRUE;
        }
        break;

    case SCT_GEQUAL:
        if( v1 >= v2 )
        {
            return SCT_TRUE;
        }
        break;
        
    case SCT_EQUAL:
        if( v1 == v2 )
        {
            return SCT_TRUE;
        }
        break;

    case SCT_NOTEQUAL:
        if( v1 != v2 )
        {
            return SCT_TRUE;
        }
        break;

    default:
        break;
    }

    return SCT_FALSE;
}
