/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1createmeshaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"
#include "sct_opengles1utils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1CreateMeshActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1CreateMeshActionContext*    context;
    int                                     i;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1CreateMeshActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CreateMeshActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateMesh@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1CreateMeshActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1CreateMeshActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateMeshActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidMeshIndex( context->moduleContext, context->data.meshIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateMeshActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in CreateMesh@OpenGLES1 context creation." );
        return NULL;
    }

    for( i = 0; i < context->data.arrayIndices->length; ++i )
    {
        if( sctiOpenGLES1ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndices->data[ i ] ) == SCT_FALSE )
        {
            sctiDestroyOpenGLES1CreateMeshActionContext( context );
            SCT_LOG_ERROR( "Array index out of range in CreateMesh@OpenGLES1 context creation." );
            return NULL;
        }
    }

    context->arrays = ( SCTOpenGLES1Array** )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1Array* ) * context->data.arrayIndices->length ) );
    if( context->arrays == NULL )
    {
        sctiDestroyOpenGLES1CreateMeshActionContext( context );
        SCT_LOG_ERROR( "Allocation failed in CreateMesh@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1CreateMeshActionContext( void* context )
{
    SCTOpenGLES1CreateMeshActionContext*    c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenGLES1CreateMeshActionContext* )( context );

    if( c->arrays != NULL )
    {
        siCommonMemoryFree( NULL, c->arrays );
        c->arrays = NULL;
    }

    if( c->data.arrayIndices != NULL )
    {
        sctDestroyIntVector( c->data.arrayIndices );
        c->data.arrayIndices = NULL;
    }

    if( c->data.sizes != NULL )
    {
        sctDestroyIntVector( c->data.sizes );
        c->data.sizes = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateMeshActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateMeshActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1CreateMeshActionContext*    context;
    OpenGLES1CreateMeshActionData*          data;
    SCTOpenGLES1Mesh*                       mesh;
    int                                     i;
    int                                     length;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1CreateMeshActionContext* )( action->context );
    data    = &( context->data );

    length = data->arrayIndices->length;

    mesh = sctiOpenGLES1ModuleGetMesh( context->moduleContext, data->meshIndex );
    if( mesh != NULL )
    {
        SCT_LOG_ERROR( "Mesh index already used in CreateMesh@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    for( i = 0; i < length; ++i )
    {
        context->arrays[ i ] = sctiOpenGLES1ModuleGetArray( context->moduleContext, data->arrayIndices->data[ i ] );
        if( context->arrays[ i ] == NULL )
        {
            SCT_LOG_ERROR( "Invalid array index in CreateMesh@OpenGLES1 action execute." );
            return SCT_FALSE;
        }
    }

    mesh = sctOpenGLES1CreateMesh( context->arrays, data->sizes->data, length );
    if( mesh == NULL )
    {
        SCT_LOG_ERROR( "Mesh creation failed in CreateMesh@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES1ModuleSetMesh( context->moduleContext, data->meshIndex, mesh );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateMeshActionTerminate( SCTAction* action )
{
    SCTOpenGLES1CreateMeshActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1CreateMeshActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteMesh( context->moduleContext, context->data.meshIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateMeshActionDestroy( SCTAction* action )
{
    SCTOpenGLES1CreateMeshActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1CreateMeshActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1CreateMeshActionContext( context );
}
