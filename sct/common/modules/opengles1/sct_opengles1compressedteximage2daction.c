/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1compressedteximage2daction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1CompressedTexImage2DActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1CompressedTexImage2DActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1CompressedTexImage2DActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CompressedTexImage2DActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CompressedTexImage2D@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1CompressedTexImage2DActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1CompressedTexImage2DActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CompressedTexImage2DActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidCompressedTextureDataIndex( context->moduleContext, context->data.compressedTextureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CompressedTexImage2DActionContext( context );
        SCT_LOG_ERROR( "Invalid compressed texture data index in CompressedTexImage2D@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1CompressedTexImage2DActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CompressedTexImage2DActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1CompressedTexImage2DActionContext*  context;
    GLenum                                          err;
    int                                             i;
    OpenGLES1CompressedTexImage2DActionData*        data;
    SCTOpenGLES1CompressedTextureData*              compressedTextureData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1CompressedTexImage2DActionContext* )( action->context );
    data    = &( context->data );

    compressedTextureData = sctiOpenGLES1ModuleGetCompressedTextureData( context->moduleContext, data->compressedTextureDataIndex );
    if( compressedTextureData == NULL )
    {
        SCT_LOG_ERROR( "Invalid texture data in CompressedTexImage2D@OpenGLES1 action execute." );
        return SCT_FALSE;
    }
    for( i = 0; i < compressedTextureData->mipmapCount; ++i )
    {
        glCompressedTexImage2D( data->target, 
                                i, 
                                compressedTextureData->internalFormat, 
                                compressedTextureData->mipmaps[ i ].width, 
                                compressedTextureData->mipmaps[ i ].height, 
                                0, 
                                compressedTextureData->mipmaps[ i ].imageSize,
                                compressedTextureData->mipmaps[ i ].data );
    }

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */  
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in CompressedTexImage2D@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CompressedTexImage2DActionDestroy( SCTAction* action )
{
    SCTOpenGLES1CompressedTexImage2DActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1CompressedTexImage2DActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1CompressedTexImage2DActionContext( context );
}
