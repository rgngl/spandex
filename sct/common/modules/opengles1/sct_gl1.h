/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/*!
 * Wrapper that should be included instead of GL.H. This file includes the
 * platform-specific GL.H and defines macros that wrap floating-point and
 * fixed-point versions of some functions as needed.
 */

#if !defined( __SCT_GL1_H__ )
#define __SCT_GL1_H__

#include "sct_sicommon.h"
#include "sct_types.h"

/* 
   Use flags SCT_FIXED_POINT_OGLES, SCT_FLOATING_POINT_OGLES to select which
   OpenGLES to use.  Default is fixed point ogles.
*/
#if !defined( SCT_FIXED_POINT_OGLES )
# if !defined( SCT_FLOATING_POINT_OGLES )
#  define SCT_FIXED_POINT_OGLES
# endif /* !defined( SCT_FLOATING_POINT_OGLES ) */
#endif  /* !defined( SCT_FIXED_POINT_OGLES ) */

#include <GLES/gl.h>

#if !defined( GL_TEXTURE_CROP_RECT_OES )
# define GL_TEXTURE_CROP_RECT_OES                       0x8B9D
#endif  /* defined( GL_TEXTURE_CROP_RECT_OES ) */

#if !defined( GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG )
# define GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG             0x8C00
#endif  /* !defined( GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG ) */
#if !defined( GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG )
# define GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG             0x8C01
#endif  /* !defined( GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG ) */
#if !defined( GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG )
# define GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG            0x8C02
#endif  /* !defined( GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG ) */
#if !defined( GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG )
# define GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG            0x8C03
#endif  /* !defined( GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG ) */
#if !defined( GL_ETC1_RGB8_OES )
# define GL_ETC1_RGB8_OES                               0x8D64
#endif  /* !defined( GL_ETC1_RGB8_OES ) */

/* Some defines that have been found be missing in some gl headers. */

# if !defined( GL_VIEWPORT )
#  define GL_VIEWPORT                                   0x0BA2
# endif /* !defined( GL_VIEWPORT ) */
# if !defined( GL_COMBINE )
#   define GL_COMBINE                                   0x8570
# endif /* !defined( GL_COMBINE ) */
# if !defined( GL_COMBINE_RGB )
#   define GL_COMBINE_RGB                               0x8571
# endif /* !defined( GL_COMBINE_RGB ) */
# if !defined( GL_COMBINE_ALPHA )
#   define GL_COMBINE_ALPHA                             0x8572
# endif /* !defined( GL_COMBINE_ALPHA ) */
# if !defined( GL_SOURCE0_RGB )
#   define GL_SOURCE0_RGB                               0x8580
# endif /* !defined( GL_SOURCE0_RGB ) */
# if !defined( GL_SOURCE1_RGB )
#   define GL_SOURCE1_RGB                               0x8581
# endif /* !defined( GL_SOURCE1_RGB ) */
# if !defined( GL_SOURCE2_RGB )
#   define GL_SOURCE2_RGB                               0x8582
# endif /* !defined( GL_SOURCE2_RGB ) */
# if !defined( GL_SOURCE0_ALPHA )
#   define GL_SOURCE0_ALPHA                             0x8588
# endif /* !defined( GL_SOURCE0_ALPHA ) */
# if !defined( GL_SOURCE1_ALPHA )
#   define GL_SOURCE1_ALPHA                             0x8589
# endif /* !defined( GL_SOURCE1_ALPHA ) */
# if !defined( GL_SOURCE2_ALPHA )
#   define GL_SOURCE2_ALPHA                             0x858A
# endif /* !defined( GL_SOURCE2_ALPHA ) */
# if !defined( GL_SRC0_RGB )
#   define GL_SRC0_RGB                                  GL_SOURCE0_RGB
# endif /* !defined( GL_SRC0_RGB ) */
# if !defined( GL_SRC1_RGB )
#   define GL_SRC1_RGB                                  GL_SOURCE1_RGB
# endif /* !defined( GL_SRC1_RGB ) */
# if !defined( GL_SRC2_RGB )
#   define GL_SRC2_RGB                                  GL_SOURCE2_RGB
# endif /* !defined( GL_SRC2_RGB ) */
# if !defined( GL_SRC0_ALPHA )
#   define GL_SRC0_ALPHA                                GL_SOURCE0_ALPHA
# endif /* !defined( GL_SRC0_ALPHA ) */
#if !defined( GL_SRC1_ALPHA )
#   define GL_SRC1_ALPHA                                GL_SOURCE1_ALPHA
# endif /* !defined( GL_SRC1_ALPHA ) */
# if !defined( GL_SRC2_ALPHA )
#   define GL_SRC2_ALPHA                                GL_SOURCE2_ALPHA
# endif /* !defined( GL_SRC2_ALPHA ) */
# if !defined( GL_OPERAND0_RGB )
#   define GL_OPERAND0_RGB                              0x8590
# endif /* !defined( GL_OPERAND0_RGB ) */
# if !defined( GL_OPERAND1_RGB )
#   define GL_OPERAND1_RGB                              0x8591
# endif /* !defined( GL_OPERAND1_RGB ) */
# if !defined( GL_OPERAND2_RGB )
#   define GL_OPERAND2_RGB                              0x8592
# endif /* !defined( GL_OPERAND2_RGB ) */
# if !defined( GL_OPERAND0_ALPHA )
#   define GL_OPERAND0_ALPHA                            0x8598
# endif /* !defined( GL_OPERAND0_ALPHA ) */
# if !defined( GL_OPERAND1_ALPHA )
#   define GL_OPERAND1_ALPHA                            0x8599
# endif /* !defined( GL_OPERAND1_ALPHA ) */
# if !defined( GL_OPERAND2_ALPHA )
#   define GL_OPERAND2_ALPHA                            0x859A
# endif /* !defined( GL_OPERAND2_ALPHA ) */
# if !defined( GL_RGB_SCALE )
#   define GL_RGB_SCALE                                 0x8573
# endif /* !defined( GL_RGB_SCALE ) */
# if !defined( GL_ALPHA_SCALE )
#   define GL_ALPHA_SCALE                               0x0D1C
# endif /* !defined( GL_ALPHA_SCALE ) */
# if !defined( GL_ADD_SIGNED )
#   define GL_ADD_SIGNED                                0x8574
# endif /* !defined( GL_ADD_SIGNED ) */
# if !defined( GL_INTERPOLATE )
#   define GL_INTERPOLATE                               0x8575
# endif /* !defined( GL_INTERPOLATE ) */
# if !defined( GL_SUBTRACT )
#   define GL_SUBTRACT                                  0x84E7
# endif /* !defined( GL_SUBTRACT ) */
# if !defined( GL_DOT3_RGB )
#   define GL_DOT3_RGB                                  0x86AE
# endif /* !defined( GL_DOT3_RGB ) */
# if !defined( GL_DOT3_RGBA )
#   define GL_DOT3_RGBA                                 0x86AF
# endif /* !defined( GL_DOT3_RGBA ) */
# if !defined( GL_CONSTANT )
#   define GL_CONSTANT                                  0x8576
# endif /* !defined( GL_CONSTANT ) */
# if !defined( GL_PRIMARY_COLOR )
#   define GL_PRIMARY_COLOR                             0x8577
# endif /* !defined( GL_PRIMARY_COLOR ) */
# if !defined( GL_PREVIOUS )
#   define GL_PREVIOUS                                  0x8578
# endif /* !defined( GL_PREVIOUS ) */
# if !defined( GL_MATRIX_PALETTE_OES )
#   define GL_MATRIX_PALETTE_OES                        0x8840
# endif /* !defined( GL_MATRIX_PALETTE_OES ) */
# if !defined( GL_POINT_SPRITE_OES )
#	define GL_POINT_SPRITE_OES                          0x8861
# endif /* !defined( GL_POINT_SPRITE_OES ) */
# if !defined( GL_GENERATE_MIPMAP )
#   define GL_GENERATE_MIPMAP                           0x8191
# endif /* !defined( GL_GENERATE_MIPMAP ) */
# if !defined( GL_CLIP_PLANE0 )
#	define GL_CLIP_PLANE0                               0x3000
# endif /* !defined( GL_CLIP_PLANE0 ) */
# if !defined( GL_GENERATE_MIPMAP_HINT )
#   define GL_GENERATE_MIPMAP_HINT                      0x8192
# endif /* !defined( GL_GENERATE_MIPMAP_HINT ) */
# if !defined( GL_ARRAY_BUFFER )
#   define GL_ARRAY_BUFFER                              0x8892
# endif /* !defined( GL_ARRAY_BUFFER ) */
# if !defined( GL_ELEMENT_ARRAY_BUFFER )
#   define GL_ELEMENT_ARRAY_BUFFER                      0x8893
# endif /* !defined( GL_ELEMENT_ARRAY_BUFFER ) */
# if !defined( GL_STATIC_DRAW )
#   define GL_STATIC_DRAW                               0x88E4
# endif /* !defined( GL_STATIC_DRAW ) */
# if !defined( GL_DYNAMIC_DRAW )
#   define GL_DYNAMIC_DRAW                              0x88E8
# endif /* !defined( GL_DYNAMIC_DRAW ) */
# if !defined( GL_POINT_SPRITE_OES )
#   define GL_POINT_SPRITE_OES                          0x8861
# endif /* !defined( GL_POINT_SPRITE_OES ) */
# if !defined( GL_COORD_REPLACE_OES )
#   define GL_COORD_REPLACE_OES                         0x8862
# endif /* !defined( GL_COORD_REPLACE_OES )*/
# if !defined( GL_CLIP_PLANE0 )
#  define GL_CLIP_PLANE0                                0x3000
# endif /* !defined( GL_CLIP_PLANE0 ) */
# if !defined( GL_CLIP_PLANE1 )
#  define GL_CLIP_PLANE1                                0x3001
# endif /* !defined( GL_CLIP_PLANE1 ) */
# if !defined( GL_CLIP_PLANE2 )
#  define GL_CLIP_PLANE2                                0x3002
# endif /* !defined( GL_CLIP_PLANE2 ) */
# if !defined( GL_CLIP_PLANE3 )
#  define GL_CLIP_PLANE3                                0x3003
# endif /* !defined( GL_CLIP_PLANE3 ) */
# if !defined( GL_CLIP_PLANE4 )
#  define GL_CLIP_PLANE4                                0x3004
# endif /* !defined( GL_CLIP_PLANE4 ) */
# if !defined( GL_CLIP_PLANE5 )
#  define GL_CLIP_PLANE5                                0x3005
# endif /* !defined( GL_CLIP_PLANE5 ) */

#if !defined( GL_MATRIX_INDEX_ARRAY_OES )
# define GL_MATRIX_INDEX_ARRAY_OES                      0x8844
#endif  /* !defined( GL_MATRIX_INDEX_ARRAY_OES ) */

#if !defined( GL_POINT_SIZE_ARRAY_OES )
# define GL_POINT_SIZE_ARRAY_OES                        0x8B9C
#endif /* !defined( GL_POINT_SIZE_ARRAY_OES ) */

#if !defined( GL_WEIGHT_ARRAY_OES )
# define GL_WEIGHT_ARRAY_OES                            0x86AD
#endif  /* !defined( GL_WEIGHT_ARRAY_OES ) */

#if defined( SCT_FIXED_POINT_OGLES )

# define FLT2CLAMPX( flt )      ( ( GLclampx )( ( flt ) * 65536.0f ) )
# define FLT2FIXED( flt )       ( ( GLfixed )( ( flt ) * 65536.0f ) )

SCT_INLINE static void sctFogf( GLenum pname, const GLfloat param )
{
    switch( pname )
    {
    case GL_FOG_MODE:
        glFogx( pname, ( GLfixed )( param ) );
        break;
    case GL_FOG_DENSITY:
    case GL_FOG_START:
    case GL_FOG_END:
        glFogx( pname, FLT2FIXED( param ) );
        break;
    case GL_FOG_COLOR:
    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }
}    

SCT_INLINE static void sctFogfv( GLenum pname, const GLfloat* params )
{
    switch( pname )
    {
    case GL_FOG_MODE:
        glFogx( pname, ( GLfixed )( params ) );
        break;
    case GL_FOG_DENSITY:
    case GL_FOG_START:
    case GL_FOG_END:
        glFogx( pname, FLT2FIXED( params[ 0 ] ) );
        break;
    case GL_FOG_COLOR:
    {
        GLfixed fixedColor[ 4 ];
        int     i;

        for( i = 0; i < 4; i++ )
        {
            fixedColor[ i ] = FLT2FIXED( params[ i ] );
        }
        glFogxv( GL_FOG_COLOR, fixedColor );
        break;
    }
    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }
}    

SCT_INLINE static void sctLightf( const GLenum light, const GLenum pname, const GLfloat param )
{
    switch( pname )
    {
    case GL_SPOT_EXPONENT:
    case GL_SPOT_CUTOFF:
    case GL_CONSTANT_ATTENUATION:
    case GL_LINEAR_ATTENUATION:
    case GL_QUADRATIC_ATTENUATION:
        glLightx( light, pname, ( GLfixed )( param ) );
        break;
    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }
}

SCT_INLINE static void sctLightfv( const GLenum light, const GLenum pname, const GLfloat* params )
{
    GLfixed fixedParams[ 4 ];
    int     i;

    switch( pname )
    {
    case GL_AMBIENT:
    case GL_DIFFUSE:
    case GL_SPECULAR:
    case GL_POSITION:
    case GL_EMISSION:
        for( i = 0; i < 4; i++ )
        {
            fixedParams[ i ] = FLT2FIXED( params[ i ] );
        }
        break;
    case GL_SPOT_DIRECTION:
        for( i = 0; i < 3; i++ )
        {
            fixedParams[ i ] = FLT2FIXED( params[ i ] );
        }
        break;
    case GL_SPOT_EXPONENT:
    case GL_SPOT_CUTOFF:
    case GL_CONSTANT_ATTENUATION:
    case GL_LINEAR_ATTENUATION:
    case GL_QUADRATIC_ATTENUATION:
        fixedParams[ 0 ] = FLT2FIXED( params[ 0 ] );
        break;
    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }
    
    glLightxv( light, pname, fixedParams );
}    

SCT_INLINE static void sctLightModelfv( const GLenum pname, const GLfloat* params )
{
    GLfixed fixedParams[ 4 ];
    int     i;

    switch( pname )
    {
    case GL_LIGHT_MODEL_AMBIENT:
        for( i = 0; i < 4; i++ )
        {
            fixedParams[ i ] = FLT2FIXED( params[ i ] );
        }
        break;
    case GL_LIGHT_MODEL_TWO_SIDE:
        fixedParams[ 0 ] = FLT2FIXED( params[ 0 ] );
        break;
    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }
    glLightModelxv( pname, fixedParams );
}

SCT_INLINE static void sctMaterialfv( const GLenum face, const GLenum pname, const GLfloat* params )
{
    GLfixed fixedParams[ 4 ];
    int     i;

    switch( pname )
    {
    case GL_AMBIENT:
    case GL_DIFFUSE:
    case GL_SPECULAR:
    case GL_EMISSION:
    case GL_AMBIENT_AND_DIFFUSE:
        for( i = 0; i < 4; i++ )
        {
            fixedParams[ i ] = FLT2FIXED( params[ i ] );
        }
        break;
    case GL_SHININESS:
        fixedParams[ 0 ] = FLT2FIXED( params[ 0 ] );
        break;
    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }
    
    glMaterialxv( face, pname, fixedParams );
}

SCT_INLINE static void sctMultMatrixf( const GLfloat* m )
{
    GLfixed fixedMatrix[ 16 ];
    int     i;

    for( i = 0; i < 16; i++ )
    {
        fixedMatrix[ i ] = FLT2FIXED( m[ i ] );
    }
    glMultMatrixx( fixedMatrix );
}

SCT_INLINE static void sctTexEnvfv( const GLenum target, const GLenum pname, const GLfloat *params )
{
    GLfixed fixedParams[ 4 ];
    int     i;

    for( i = 0; i < 4; i++ )
    {
        fixedParams[ i ] = FLT2FIXED( params[ i ] );
    }
    glTexEnvxv( target, pname, fixedParams );
}

SCT_INLINE static void sctClipPlanef( const GLenum plane, const GLfloat *equation )
{
#if defined( GL_VERSION_ES_CM_1_1 )
    GLfixed fixedParams[ 4 ];
    int     i;

    for( i = 0; i < 4; i++ )
    {
        fixedParams[ i ] = FLT2FIXED( equation[ i ] );
    }
    glClipPlanex( plane, fixedParams );
#else   /* defined( GL_VERSION_ES_CM_1_1 ) */
    SCT_USE_VARIABLE( plane );
    SCT_USE_VARIABLE( equation );
    SCT_ASSERT_ALWAYS( 0 );
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */
}

SCT_INLINE static void sctLoadMatrixf( const GLfloat* m )
{
    GLfixed     fm[ 16 ];
    int         i;

    for( i = 0; i < 16; i++ )
    {
        fm[ i ] = FLT2FIXED( m[ i ] );
    }
    glLoadMatrixx( fm );
}

SCT_INLINE static void sctPointParameterfv( const GLenum pname, const GLfloat* params )
{
#if defined( GL_VERSION_ES_CM_1_1 )
    switch( pname )
    {
    case GL_POINT_SIZE_MIN:
    case GL_POINT_SIZE_MAX:
    case GL_POINT_FADE_THRESHOLD_SIZE:
        glPointParameterx( pname, FLT2FIXED( params[ 0 ] ) );
        break;
    case GL_POINT_DISTANCE_ATTENUATION:
    {
        GLfixed fixedParams[ 3 ];
        int i;

        for( i = 0; i < 3; i++ )
        {
            fixedParams[ i ] = FLT2FIXED( params[ i ] );
        }
        glPointParameterxv( pname, fixedParams );
        break;
    }
    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }
#else   /* defined( GL_VERSION_ES_CM_1_1 ) */
    SCT_USE_VARIABLE( pname );
    SCT_USE_VARIABLE( params );
    SCT_ASSERT_ALWAYS( 0 );
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */
}

# define GL_ALPHA_FUNC( func, ref )                     glAlphaFuncx( ( func ), FLT2CLAMPX( ref ) )
# define GL_CLEAR_COLOR( r, g, b, a )                   glClearColorx( FLT2CLAMPX( r ), FLT2CLAMPX( g ), FLT2CLAMPX( b ), FLT2CLAMPX( a ) )
# define GL_CLEAR_DEPTHF( d )                           glClearDepthx( FLT2CLAMPX( d ) )
# define GL_CLIP_PLANEF( plane, equation )              sctClipPlanef( ( plane ), ( equation ) )
# define GL_COLOR4F( r, g, b, a )                       glColor4x( FLT2FIXED( r ), FLT2FIXED( g ), FLT2FIXED( b ), FLT2FIXED( a ) )
# define GL_DEPTH_RANGEF( zNear, zFar )                 glDepthRangex( FLT2CLAMPX( zNear ), FLT2CLAMPX( zFar ) )
# define GL_FOGF( pname, param )                        sctFogf( ( pname ), ( param ) )
# define GL_FOGFV( pname, params )                      sctFogfv( ( pname ), ( params ) )
# define GL_FRUSTUMF( l, r, b, t, n, f )                glFrustumx( FLT2FIXED( l ), FLT2FIXED( r ), FLT2FIXED( b ), FLT2FIXED( t ), FLT2FIXED( n ), FLT2FIXED( f ) )
# define GL_LIGHTF( light, pname, param )               glLightx( ( light ), ( pname ), FLT2FIXED( param ) )
# define GL_LIGHTFV( light, pname, params )             sctLightfv( ( light ), ( pname ), ( params ) )
# define GL_LIGHT_MODELF( pname, param )                glLightModelx( ( pname ), FLT2FIXED( param ) )
# define GL_LIGHT_MODELFV( pname, params )              sctLightModelfv( ( pname ), ( params ) )
# define GL_LINE_WIDTHF( width )                        glLineWidthx( FLT2FIXED( width ) )
# define GL_LOAD_MATRIXF( m )                           sctLoadMatrixf( ( m ) )
# define GL_MATERIALF( face, pname, param )             glMaterialx( ( face ), ( pname ), FLT2FIXED( param ) )
# define GL_MATERIALFV( face, pname, params )           sctMaterialfv( ( face ), ( pname ), ( params ) )
# define GL_MULT_MATRIXF( m )                           sctMultMatrixf( ( m ) )
# define GL_MULTI_TEX_COORD4F( target, s, t, r, q )     glMultiTexCoord4x( ( target ), FLT2FIXED( s ), FLT2FIXED( t ), FLT2FIXED( r ), FLT2FIXED( q ) )
# define GL_NORMAL3F( nx, ny, nz )                      glNormal3x( FLT2FIXED( nz ), FLT2FIXED( ny ), FLT2FIXED( nz ) )
# define GL_ORTHOF( l, r, b, t, n, f )                  glOrthox( FLT2FIXED( l ), FLT2FIXED( r ), FLT2FIXED( b ), FLT2FIXED( t ), FLT2FIXED( n ), FLT2FIXED( f ) )
# define GL_POINT_PARAMETERF( pname, param )            glPointParameterx( ( pname ), FLT2FIXED( param ) )
# define GL_POINT_PARAMETERFV( pname, params )          sctPointParameterfv( ( pname ), ( params ) )
# define GL_POINT_SIZEF( size )                         glPointSizex( FLT2FIXED( size ) )
# define GL_POLYGON_OFFSET( f, u )                      glPolygonOffsetx( FLT2FIXED( f ), FLT2FIXED( u ) )
# define GL_ROTATEF( angle, x, y, z )                   glRotatex( FLT2FIXED( angle ), FLT2FIXED( x ), FLT2FIXED( y ), FLT2FIXED( z ) )
# define GL_SAMPLE_COVERAGEF( value, invert )           glSampleCoveragex( FLT2FIXED( value ), ( invert ) )
# define GL_SCALEF( x, y, z )                           glScalex( FLT2FIXED( x ), FLT2FIXED( y ), FLT2FIXED( z ) )
# define GL_TRANSLATEF( x, y, z )                       glTranslatex( FLT2FIXED( x ), FLT2FIXED( y ), FLT2FIXED( z ) )
# define GL_TEX_PARAMETERF( target, pname, param )      glTexParameterx( ( target ), ( pname ), ( GLfixed )( param ) )
# define GL_TEX_ENVF( target, pname, param )            glTexEnvx( ( target ), ( pname ), ( GLfixed )( param ) )
# define GL_TEX_ENVFV( target, pname, param )           sctTexEnvfv( ( target ), ( pname ), ( param ) )

#endif /* defined( SCT_FIXED_POINT_OGLES ) */

#if defined( SCT_FLOATING_POINT_OGLES )

# define GL_ALPHA_FUNC( func, ref )                     glAlphaFunc( ( func ), ( ref ) )
# define GL_CLEAR_COLOR( r, g, b, a )                   glClearColor( ( r ), ( g ), ( b ), ( a )  )
# define GL_CLEAR_DEPTHF( d )                           glClearDepthf( ( d ) )
# define GL_CLIP_PLANEF( plane, equation )              glClipPlanef( ( plane ), ( equation ) )
# define GL_COLOR4F( r, g, b, a )                       glColor4f( ( r ), ( g ), ( b ), ( a ) )
# define GL_DEPTH_RANGEF( zNear, zFar )                 glDepthRangef( ( zNear ), ( zFar ) )
# define GL_FOGF( pname, param )                        glFogf( ( pname ), ( param ) )
# define GL_FOGFV( pname, params )                      glFogfv( ( pname ), ( params ) )
# define GL_FRUSTUMF( l, r, b, t, n, f )                glFrustumf( ( l ), ( r ), ( b ), ( t ), ( n ), ( f ) )
# define GL_LIGHTF( light, pname, param )               glLightf( ( light ), ( pname ), ( param ) )
# define GL_LIGHTFV( light, pname, params )             glLightfv( ( light ), ( pname ), ( params ) )
# define GL_LIGHT_MODELF( pname, param )                glLightModelf( ( pname ), ( param ) )
# define GL_LIGHT_MODELFV( pname, params )              glLightModelfv( ( pname ), ( params ) )
# define GL_LINE_WIDTHF( width )                        glLineWidth( ( width ) )
# define GL_LOAD_MATRIXF( m )                           glLoadMatrixf( ( m ) )
# define GL_MATERIALF( face, pname, param )             glMaterialf( ( face ), ( pname ), ( param ) )
# define GL_MATERIALFV( face, pname, params )           glMaterialfv( ( face ), ( pname ), ( params ) )
# define GL_MULT_MATRIXF( m )                           glMultMatrixf( ( m ) )
# define GL_MULTI_TEX_COORD4F( target, s, t, r, q )     glMultiTexCoord4f( ( target ), ( s ), ( t ), ( r ), ( q ) )
# define GL_NORMAL3F( nx, ny, nz )                      glNormal3f( ( nz ), ( ny ), ( nz ) )
# define GL_ORTHOF( l, r, b, t, n, f )                  glOrthof( ( l ), ( r ), ( b ), ( t ), ( n ), ( f ) )
# define GL_POINT_PARAMETERF( pname, param )            glPointParameterf( ( pname ), ( param ) )
# define GL_POINT_PARAMETERFV( pname, params )          glPointParameterfv( ( pname ), ( params ) )
# define GL_POINT_SIZEF( size )                         glPointSizex( FLT2FIXED( size ) )
# define GL_POLYGON_OFFSET( f, u )                      glPolygonOffset( ( f ), ( u ) )
# define GL_ROTATEF( angle, x, y, z )                   glRotatef( ( angle ), ( x ), ( y ), ( z ) )
# define GL_SAMPLE_COVERAGEF( value, invert )           glSampleCoveragef( ( value ), ( invert ) )
# define GL_SCALEF( x, y, z )                           glScalef( ( x ), ( y ), ( z ) )
# define GL_TEX_PARAMETERF( target, pname, param )      glTexParameterf( ( target ), ( pname ), ( param ) )
# define GL_TEX_ENVF( target, pname, param )            glTexEnvf( ( target ), ( pname ), ( param ) )
# define GL_TEX_ENVFV( target, pname, param )           glTexEnvfv( ( target ), ( pname ), ( param ) )
# define GL_TRANSLATEF( x, y, z )                       glTranslatef( ( x ), ( y ), ( z ) )

#endif /* defined( SCT_FLOATING_POINT_OGLES ) */

/*!
 *
 */
typedef void ( *SCTOpenGLES1WeightPointerFunc )( GLint size, GLenum type, GLsizei stride, const GLvoid *pointer );
typedef void ( *SCTOpenGLES1CurrentPaletteMatrixFunc )( GLuint index );
typedef void ( *SCTOpenGLES1DrawTexfFunc )( GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height );
typedef void ( *SCTOpenGLES1LoadPaletteFromModelViewMatrixFunc )( void );
typedef void ( *SCTOpenGLES1MatrixIndexPointerFunc )( GLint size, GLenum type, GLsizei stride, const GLvoid *pointer );
typedef void ( *SCTOpenGLES1PointSizePointerFunc )( GLenum type, GLsizei stride, const GLvoid *pointer );

/* Assorted hacks for getting OpenGLES working in windows mobile */
#if defined( SCT_WINDOWS_MOBILE )

# define glGetFloatv( a, b )							

# if !defined( GL_VIEWPORT )
#  define GL_VIEWPORT                                   0x0BA2
# endif	/* !defined( GL_VIEWPORT ) */

# if !defined( GL_MAX_CLIP_PLANES )
#  define GL_MAX_CLIP_PLANES							0x0D32
# endif	/* !defined( GL_MAX_CLIP_PLANES ) */

#endif	/* defined( SCT_WINDOWS_MOBILE ) */

#endif /* !defined( __SCT_GL1_H__ ) */
