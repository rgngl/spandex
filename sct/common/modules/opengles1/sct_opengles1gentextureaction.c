/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1gentextureaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1GenTextureActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1GenTextureActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1GenTextureActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1GenTextureActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in GenTexture@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1GenTextureActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1GenTextureActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1GenTextureActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidTextureIndex( context->moduleContext, context->data.textureIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1GenTextureActionContext( context );
        SCT_LOG_ERROR( "Invalid texture index in GenTexture@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1GenTextureActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1GenTextureActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1GenTextureActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1GenTextureActionContext*    context;
    GLenum                                  err;
    OpenGLES1GenTextureActionData*          data;
    SCTOpenGLES1Texture*                    texture;
    GLuint                                  textureId   = 0;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1GenTextureActionContext* )( action->context );
    data    = &( context->data );

    texture = sctiOpenGLES1ModuleGetTexture( context->moduleContext, data->textureIndex );
    if( texture != NULL )
    {
       SCT_LOG_ERROR( "Texture index already in use in GenTexture@OpenGLES1 action execute." );
       return SCT_FALSE;
    }
    
    glGenTextures( 1, &textureId );
    texture = sctOpenGLES1CreateTexture( textureId );

    if( texture == NULL )
    {
        glDeleteTextures( 1, &textureId );
        SCT_LOG_ERROR( "Texture creation failed in GenTexture@OpenGLES1 action execute." );
        return SCT_FALSE;
    }
    sctiOpenGLES1ModuleSetTexture( context->moduleContext, data->textureIndex, texture );
    
#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in GenTexture@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1GenTextureActionTerminate( SCTAction* action )
{
    SCTOpenGLES1GenTextureActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1GenTextureActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteTexture( context->moduleContext, context->data.textureIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1GenTextureActionDestroy( SCTAction* action )
{
    SCTOpenGLES1GenTextureActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1GenTextureActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1GenTextureActionContext( context );
}
