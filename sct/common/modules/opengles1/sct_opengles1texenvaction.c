/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1texenvaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1TexEnvActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1TexEnvActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1TexEnvActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1TexEnvActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in TexEnv@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1TexEnvActionContext ) );

    if( sctiParseOpenGLES1TexEnvActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1TexEnvActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1TexEnvActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TexEnvActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenGLES1TexEnvActionContext*    context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenGLES1TexEnvActionContext* )( action->context );
    
#if !defined( GL_VERSION_ES_CM_1_1 )
    if( context->data.coordReplace != SCT_FALSE )
    {
        SCT_LOG_ERROR( "TexEnv@OpenGLES1 coord replace not supported in OpenGLES 1.0." );
        return SCT_FALSE;
    }
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    SCT_USE_VARIABLE( context );
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TexEnvActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1TexEnvActionContext*    context;
    int                                 err;
    OpenGLES1TexEnvActionData*          data;
    GLboolean                           coordReplace    = GL_FALSE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1TexEnvActionContext* )( action->context );
    data    = &( context->data );

    if( data->coordReplace == SCT_TRUE )
    {
        coordReplace = GL_TRUE;
    }

    GL_TEX_ENVF( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, ( float )( data->envMode ) );
    GL_TEX_ENVFV( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, data->envColor );

#if defined( GL_VERSION_ES_CM_1_1 )
    GL_TEX_ENVF( GL_POINT_SPRITE_OES, GL_COORD_REPLACE_OES, ( float )( coordReplace ) );
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in TexEnv@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();
        
        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1TexEnvActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenGLES1TexEnvActionDestroy( SCTAction* action )
{
    SCTOpenGLES1TexEnvActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1TexEnvActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1TexEnvActionContext( context );
}
