/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1matrixmodeaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1MatrixModeActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1MatrixModeActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1MatrixModeActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1MatrixModeActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in MatrixMode@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1MatrixModeActionContext ) );

    if( sctiParseOpenGLES1MatrixModeActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1MatrixModeActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1MatrixModeActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1MatrixModeActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenGLES1MatrixModeActionContext*    context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark );

    context = ( SCTOpenGLES1MatrixModeActionContext* )( action->context );

#if !defined( GL_VERSION_ES_CM_1_1 )
    if( context->data.mode == GL_MATRIX_PALETTE_OES )
    {
        SCT_LOG_ERROR( "MatrixMode@OpenGLES1 GL_MATRIX_PALETTE_OES not supported in OpenGLES 1.0." );
        return SCT_FALSE;
    }
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    SCT_USE_VARIABLE( context );
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1MatrixModeActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1MatrixModeActionContext*    context;
    int                                     err;
    OpenGLES1MatrixModeActionData*          data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1MatrixModeActionContext* )( action->context );
    data    = &( context->data );

    glMatrixMode( data->mode );
    
#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in MatrixMode@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1MatrixModeActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenGLES1MatrixModeActionDestroy( SCTAction* action )
{
    SCTOpenGLES1MatrixModeActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1MatrixModeActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1MatrixModeActionContext( context );
}
