/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1normalpointeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1NormalPointerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1NormalPointerActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1NormalPointerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1NormalPointerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in NormalPointer@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1NormalPointerActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1NormalPointerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1NormalPointerActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1NormalPointerActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in NormalPointer@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1NormalPointerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1NormalPointerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1NormalPointerActionContext* context;
    OpenGLES1NormalPointerActionData*       data;
    SCTOpenGLES1Array*                      array;
    int                                     offset;
    int                                     err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1NormalPointerActionContext* )( action->context );
    data    = &( context->data );

    array = sctiOpenGLES1ModuleGetArray( context->moduleContext, data->arrayIndex );   
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array index in NormalPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( data->offset >= array->length )
    {
        SCT_LOG_ERROR( "Offset out of bounds in NormalPointer@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    offset = data->offset * sctOpenGLES1GetGLTypeSize( array->type ) * 3;

    glNormalPointer( array->type,
                     0,
                     ( unsigned char* )( array->data ) + offset );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in NormalPointer@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();        

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1NormalPointerActionDestroy( SCTAction* action )
{
    SCTOpenGLES1NormalPointerActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1NormalPointerActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1NormalPointerActionContext( context );
}
