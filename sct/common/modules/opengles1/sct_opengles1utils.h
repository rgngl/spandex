/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_OPENGLES1UTILS_H__ )
#define __SCT_OPENGLES1UTILS_H__

#include "sct_types.h"
#include "sct_utils.h"
#include "sct_gl1.h"

/*!
 *
 */
typedef enum
{
    OPENGLES1_LUMINANCE8,
    OPENGLES1_ALPHA8,
    OPENGLES1_LUMINANCE_ALPHA88,
    OPENGLES1_RGB565,
    OPENGLES1_RGB888,
    OPENGLES1_RGBA4444,
    OPENGLES1_RGBA5551,
    OPENGLES1_RGBA8888
} SCTOpenGLES1TextureType;

/*!
 *
 */
typedef enum
{
    OPENGLES1_GRADIENT_VERTICAL,
    OPENGLES1_GRADIENT_HORIZONTAL,
    OPENGLES1_GRADIENT_DIAGONAL,    
} SCTOpenGLES1Gradient;

/*!
 *
 */
typedef struct
{
    int                                 width;
    int                                 height;
    unsigned char*                      data;
} SCTOpenGLES1MipMap;

/*!
 *
 */
typedef struct
{
    SCTOpenGLES1TextureType             type;
    GLenum                              glFormat;
    GLenum                              glType;
    int                                 mipmapCount;
    SCTOpenGLES1MipMap*                 mipmaps;
} SCTOpenGLES1TextureData;

/*!
 *
 */
typedef struct
{
    int                                 width;
    int                                 height;
    int                                 alignment;
    int                                 imageSize;
    unsigned char*                      data;
} SCTOpenGLES1CompressedMipMap;

/*!
 *
 */
typedef struct
{
    GLenum                              internalFormat;
    int                                 mipmapCount;
    SCTOpenGLES1CompressedMipMap*       mipmaps;
} SCTOpenGLES1CompressedTextureData;

/*
 *
 */
typedef struct
{
    GLuint                              texture;
} SCTOpenGLES1Texture;

/*!
 *
 */
typedef struct
{
    GLenum                              type;
    int                                 length;
    int                                 lengthInBytes;
    int                                 maxLengthInBytes;
    void*                               data;
} SCTOpenGLES1Array;

/*!
 *
 */
typedef struct
{
    GLenum                              type;
    int                                 length;
    int                                 lengthInBytes;
    int                                 components;
    int                                 stride;
    unsigned int                        offset;
} SCTOpenGLES1BufferItem;

/*!
 *
 */
typedef struct
{
    GLuint                              buffer;
    SCTOpenGLES1BufferItem*             items;
    int                                 itemCount;
} SCTOpenGLES1Buffer;

/*
 *
 */
typedef struct
{
    void*                               data;
    int                                 length;
} SCTOpenGLES1Data;

/*!
 *
 */
typedef struct
{
    int                                 elementSizeInMem;
    int                                 components;
    GLenum                              type;
    int                                 stride;
    void*                               pointer;
} SCTOpenGLES1MeshItem;

/*!
 *
 */
typedef struct
{
    int                                 attributeCount;
    int                                 attributeArrayCount;
    SCTOpenGLES1MeshItem*               attributeArrays;
    int                                 length;
    void*                               data;
} SCTOpenGLES1Mesh;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */
    
    SCTOpenGLES1TextureData*            sctOpenGLES1CreateNullTextureData( SCTOpenGLES1TextureType type, int width, int height, SCTBoolean mipmaps );
    SCTOpenGLES1TextureData*            sctOpenGLES1CreateSolidTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color[ 4 ], SCTBoolean mipmaps );
    SCTOpenGLES1TextureData*            sctOpenGLES1CreateBarTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int bars, SCTBoolean mipmaps );
    SCTOpenGLES1TextureData*            sctOpenGLES1CreateCheckerTextureData( SCTOpenGLES1TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int checkersX, int checkersY, SCTBoolean mipmaps );
    SCTOpenGLES1TextureData*            sctOpenGLES1CreateGradientTextureData( SCTOpenGLES1TextureType type, SCTOpenGLES1Gradient gradient, int width, int height, const float color1[ 4 ], const float color2[ 4 ], SCTBoolean mipmaps );
    
    void                                sctOpenGLES1DestroyTextureData( SCTOpenGLES1TextureData* textureData );

    SCTOpenGLES1CompressedTextureData*  sctOpenGLES1CreateCompressedTextureData( GLenum internalFormat, SCTOpenGLES1CompressedMipMap* mipmaps, int mipmapCount );
    void                                sctOpenGLES1DestroyCompressedMipMapData( SCTOpenGLES1CompressedMipMap* mipmaps, int mipmapCount );
    void                                sctOpenGLES1DestroyCompressedTextureData( SCTOpenGLES1CompressedTextureData* textureData );

    int                                 sctOpenGLES1GetTexelSize( SCTOpenGLES1TextureType type );
    SCTBoolean                          sctOpenGLES1MapTextureTypeToGL( SCTOpenGLES1TextureType type, GLenum* glFormat, GLenum* glType );
    int                                 sctOpenGLES1GetMipmapLevels( int width, int height );
    int                                 sctOpenGLES1GetGLTypeSize( GLenum type );
    SCTBoolean                          sctOpenGLES1IsPot( int width, int height );
    
    SCTOpenGLES1Texture*                sctOpenGLES1CreateTexture( GLuint texture );
    void                                sctOpenGLES1DestroyTexture( SCTOpenGLES1Texture* texture );

    SCTOpenGLES1Array*                  sctOpenGLES1CreateArray( GLenum type );
    SCTBoolean                          sctOpenGLES1AddDataToArray( SCTOpenGLES1Array* array, const void* data, int length );
    SCTBoolean                          sctOpenGLES1AddDoubleDataToArray( SCTOpenGLES1Array* array, const double* data, int length );
    SCTBoolean                          sctOpenGLES1ClearArray( SCTOpenGLES1Array* array );
    void                                sctOpenGLES1DestroyArray( SCTOpenGLES1Array* array );
    SCTBoolean                          sctOpenGLES1ConvertDoubleArray( void* dst, GLenum dstType, const double* src, int length );

    SCTOpenGLES1Buffer*                 sctOpenGLES1CreateBuffer( GLuint buffer );
    SCTBoolean                          sctOpenGLES1InitializeBufferFromArray( SCTOpenGLES1Buffer* buffer, SCTOpenGLES1Array* array, int offset, int length );
    SCTBoolean                          sctOpenGLES1InitializeBufferFromMesh( SCTOpenGLES1Buffer* buffer, SCTOpenGLES1Mesh* mesh );
    void                                sctOpenGLES1DestroyBuffer( SCTOpenGLES1Buffer* buffer );
    SCTBoolean                          sctOpenGLES1IsValidBufferArrayIndex( SCTOpenGLES1Buffer* buffer, int index );
    int                                 sctOpenGLES1GetBufferArrayCount( SCTOpenGLES1Buffer* buffer );
    GLenum                              sctOpenGLES1GetBufferArrayType( SCTOpenGLES1Buffer* buffer, int index );
    int                                 sctOpenGLES1GetBufferArrayLength( SCTOpenGLES1Buffer* buffer, int index );
    int                                 sctOpenGLES1GetBufferArrayLengthInBytes( SCTOpenGLES1Buffer* buffer, int index );
    int                                 sctOpenGLES1GetBufferArrayComponents( SCTOpenGLES1Buffer* buffer, int index );
    int                                 sctOpenGLES1GetBufferArrayStride( SCTOpenGLES1Buffer* buffer, int index );
    int                                 sctOpenGLES1GetBufferArrayOffset( SCTOpenGLES1Buffer* buffer, int index );

    SCTOpenGLES1Data*                   sctOpenGLES1CreateData( void* data, int length, SCTBoolean copy );
    void                                sctOpenGLES1DestroyData( SCTOpenGLES1Data* data );

    SCTOpenGLES1Mesh*                   sctOpenGLES1CreateMesh( SCTOpenGLES1Array* arrays[], int components[], int arraysLength );
    void                                sctOpenGLES1DestroyMesh( SCTOpenGLES1Mesh* mesh );
    int                                 sctOpenGLES1MeshLength( SCTOpenGLES1Mesh* mesh );
    void*                               sctOpenGLES1MeshData( SCTOpenGLES1Mesh* mesh );
    SCTBoolean                          sctOpenGLES1IsValidMeshArrayIndex( SCTOpenGLES1Mesh* mesh, int index );
    int                                 sctOpenGLES1MeshArrayComponents( SCTOpenGLES1Mesh* mesh, int index );
    GLenum                              sctOpenGLES1MeshArrayType( SCTOpenGLES1Mesh* mesh, int index );
    int                                 sctOpenGLES1MeshArrayStride( SCTOpenGLES1Mesh* mesh, int index );
    void*                               sctOpenGLES1MeshArrayPointer( SCTOpenGLES1Mesh* mesh, int index );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_OPENGLES1UTILS_H__ ) */
