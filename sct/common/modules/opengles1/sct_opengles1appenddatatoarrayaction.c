/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1appenddatatoarrayaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1AppendDataToArrayActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1AppendDataToArrayActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1AppendDataToArrayActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1AppendDataToArrayActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in AppendDataToArray@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1AppendDataToArrayActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1AppendDataToArrayActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1AppendDataToArrayActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1AppendDataToArrayActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in AppendDataToArray@OpenGLES1 context creation." );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1AppendDataToArrayActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in AppendDataToArray@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1AppendDataToArrayActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1AppendDataToArrayActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1AppendDataToArrayActionContext* context;
    OpenGLES1AppendDataToArrayActionData*       data;
    SCTOpenGLES1Data*                           rawData;
    SCTOpenGLES1Array*                          array;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1AppendDataToArrayActionContext* )( action->context );
    data    = &( context->data );

    array = sctiOpenGLES1ModuleGetArray( context->moduleContext, data->arrayIndex );
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array index in AppendDataToArray@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    rawData = sctiOpenGLES1ModuleGetData( context->moduleContext, data->dataIndex );
    if( rawData == NULL )
    {
        SCT_LOG_ERROR( "Invalid data index in AppendDataToArray@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES1AddDataToArray( array, rawData->data, rawData->length ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Data append failed in AppendDataToArray@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1AppendDataToArrayActionDestroy( SCTAction* action )
{
    SCTOpenGLES1AppendDataToArrayActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1AppendDataToArrayActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1AppendDataToArrayActionContext( context );
}
