/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1module.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"
#include "sct_opengles1module_parser.h"
#include "sct_opengles1module_actions.h"

#include <stdio.h>
#include <string.h>

/* Local function declarations. */
SCTAttributeList*       sctiOpenGLES1ModuleInfo( SCTModule* module );
SCTAction*              sctiOpenGLES1ModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    sctiOpenGLES1ModuleDestroy( SCTModule* module );

/*!
 *
 */
SCTBoolean sctOpenGLES1ModuleSetDataExt( int index, SCTOpenGLES1Data* data )
{
    SCTOpenGLES1ModuleContext*  context;
    SCTBoolean                  pointerRegistered   = SCT_FALSE;

    context = ( SCTOpenGLES1ModuleContext* )( sctGetRegisteredPointer( SCT_OPENGLES1_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || context == NULL )
    {
        return SCT_FALSE;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context, index ) == SCT_FALSE )    
    {
        return SCT_FALSE;
    }
  
    sctiOpenGLES1ModuleDeleteData( context, index );
    sctiOpenGLES1ModuleSetData( context, index, data );

    return SCT_TRUE;
}

/*!
 *
 */
GLuint sctOpenGLES1ModuleGetTextureExt( int index )
{
    SCTOpenGLES1ModuleContext*  context;
    SCTBoolean                  pointerRegistered   = SCT_FALSE;

    if( index < 0 || index >= SCT_MAX_OPENGLES1_TEXTURES )
    {
        return 0;
    }

    context = ( SCTOpenGLES1ModuleContext* )( sctGetRegisteredPointer( SCT_OPENGLES1_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || context == NULL )
    {
        return 0;
    }

    if( context->textures[ index ] == NULL )
    {
        return 0;
    }
    
    return context->textures[ index ]->texture;
}

/*!
 * Create opengles1 module data structure which defines the name of the module
 * and sets up the module interface functions.
 *
 * \return Module structure for OpenGLES1 module, or NULL if failure.
 */
SCTModule* sctCreateOpenGLES1Module( void )
{
    SCTModule*                  module;
    SCTOpenGLES1ModuleContext*  context;
    int                         i;

    /* Create OpenGLES1 module context and zero memory. */
    context = ( SCTOpenGLES1ModuleContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1ModuleContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1ModuleContext ) );

    for( i = 0; i < SCT_MAX_OPENGLES1_TEXTURES; ++i )
    {
        context->textures[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES1_TEXTURE_DATAS; ++i )
    {
        context->textureDatas[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES1_COMPRESSED_TEXTURE_DATAS; ++i )
    {
        context->compressedTextureDatas[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES1_ARRAYS; ++i )
    {
        context->arrays[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES1_BUFFERS; ++i )
    {
        context->buffers[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES1_DATAS; ++i )
    {
        context->datas[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES1_MESHES; ++i )
    {
        context->meshes[ i ] = NULL;
    }

    /* Create module data structure. */
    module = sctCreateModule( "OpenGLES1",
                              context,
#if defined( _WIN32 )
                              _opengles1_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiOpenGLES1ModuleInfo,
                              sctiOpenGLES1ModuleCreateAction,
                              sctiOpenGLES1ModuleDestroy );

    if( module == NULL )
    {
        siCommonMemoryFree( NULL, context );
        return NULL;
    }

    if( sctRegisterPointer( SCT_OPENGLES1_MODULE_POINTER, context ) == SCT_FALSE )
    {
        sctiOpenGLES1ModuleDestroy( module );
        sctDestroyModule( module );
        return NULL;
    }
   
    return module;
}

/*!
 * Create attribute list containing OpenGLES1 module information.
 *
 * \param module OpenGLES1 module. Must be defined.
 *
 * \return OpenGLES1 system information as attribute list, or NULL if
 * failure.
 */
SCTAttributeList* sctiOpenGLES1ModuleInfo( SCTModule* module )
{
    SCTAttributeList*   attributes;

    SCT_ASSERT_ALWAYS( module != NULL );

    /* Create attribute list for storing OpenGLES1 module information. */
    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        return NULL;
    }
    
    return attributes;
}


/*!
 * Create OpenGLES1 module action. 
 *
 * \param module OpenGLES1 module. Must be defined.
 * \param name Action name. Must be defined.
 * \param type Action type. Must be defined.
 * \param attributes List of action attributes. Must be defined.
 * \param log Log to use for warning and error reporting.
 *
 * \return Action data structure, or NULL if failure.
 *
 */
SCTAction* sctiOpenGLES1ModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCTOpenGLES1ModuleContext*  moduleContext;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    moduleContext = ( SCTOpenGLES1ModuleContext* )( module->context );

    SCT_ASSERT_ALWAYS( moduleContext != NULL );

    return sctCreateActionFromTemplate( name, type, "OpenGLES1", moduleContext, attributes, OpenGLES1ActionTemplates, SCT_ARRAY_LENGTH( OpenGLES1ActionTemplates ) );
}

/*!
 * Destroy OpenGLES1 module context. This function does not deallocate
 * module data structure but instead simply does the needed module
 * cleanup, if any.
 *
 * \param module Module to destroy. Must be defined.
 */
void sctiOpenGLES1ModuleDestroy( SCTModule* module )
{
    SCTOpenGLES1ModuleContext*  context;
    int                         i;

    SCT_ASSERT_ALWAYS( module != NULL );

    sctUnregisterPointer( SCT_OPENGLES1_MODULE_POINTER );
    
    context = ( SCTOpenGLES1ModuleContext* )( module->context );
   
    if( context != NULL )
    {
        for( i = 0; i < SCT_MAX_OPENGLES1_TEXTURES; ++i )
        {
            SCT_ASSERT_ALWAYS( context->textures[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES1_TEXTURE_DATAS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->textureDatas[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES1_COMPRESSED_TEXTURE_DATAS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->compressedTextureDatas[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES1_ARRAYS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->arrays[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES1_BUFFERS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->buffers[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES1_DATAS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->datas[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES1_MESHES; ++i )
        {
            SCT_ASSERT_ALWAYS( context->meshes[ i ] == NULL );
        }

        siCommonMemoryFree( NULL, context );
        module->context = NULL;
    }
}

//######################################################################
// TEXTURE DATAS
/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1ModuleIsValidTextureDataIndex( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURE_DATAS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES1TextureData* sctiOpenGLES1ModuleGetTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURE_DATAS );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURE_DATAS )
    {
        return moduleContext->textureDatas[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleSetTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1TextureData* textureData )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURE_DATAS );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURE_DATAS )
    {
        moduleContext->textureDatas[ index ] = textureData;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleDeleteTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCTOpenGLES1TextureData*    textureData;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURE_DATAS )
    {
        textureData = moduleContext->textureDatas[ index ];
        if( textureData != NULL )
        {
            sctOpenGLES1DestroyTextureData( textureData );
            moduleContext->textureDatas[ index ] = NULL;
        }
    }
}

//######################################################################
// TEXTURES
/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1ModuleIsValidTextureIndex( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURES )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES1Texture* sctiOpenGLES1ModuleGetTexture( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURES );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURES )
    {
        return moduleContext->textures[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleSetTexture( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1Texture* texture )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURES );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURES )
    {
        moduleContext->textures[ index ] = texture;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleDeleteTexture( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCTOpenGLES1Texture*    texture;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_TEXTURES )
    {
        texture = moduleContext->textures[ index ];
        if( texture != NULL )
        {
            glDeleteTextures( 1, &( texture->texture ) );

            sctOpenGLES1DestroyTexture( texture );
            moduleContext->textures[ index ] = NULL;
        }
    }
}

//######################################################################
// COMPRESSED TEXTURE DATAS
/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1ModuleIsValidCompressedTextureDataIndex( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES1_COMPRESSED_TEXTURE_DATAS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES1CompressedTextureData* sctiOpenGLES1ModuleGetCompressedTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_COMPRESSED_TEXTURE_DATAS );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_COMPRESSED_TEXTURE_DATAS )
    {
        return moduleContext->compressedTextureDatas[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleSetCompressedTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1CompressedTextureData* compressedTextureData )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_COMPRESSED_TEXTURE_DATAS );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_COMPRESSED_TEXTURE_DATAS )
    {
        moduleContext->compressedTextureDatas[ index ] = compressedTextureData;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleDeleteCompressedTextureData( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCTOpenGLES1CompressedTextureData*  compressedTextureData;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_COMPRESSED_TEXTURE_DATAS )
    {
        compressedTextureData = moduleContext->compressedTextureDatas[ index ];
        if( compressedTextureData != NULL )
        {
            sctOpenGLES1DestroyCompressedTextureData( compressedTextureData );
            moduleContext->compressedTextureDatas[ index ] = NULL;
        }
    }
}

//######################################################################
// ARRAYS

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1ModuleIsValidArrayIndex( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES1_ARRAYS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES1Array* sctiOpenGLES1ModuleGetArray( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_ARRAYS );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_ARRAYS )
    {
        return moduleContext->arrays[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleSetArray( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1Array* array )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_ARRAYS );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_ARRAYS )
    {
        moduleContext->arrays[ index ] = array;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleDeleteArray( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCTOpenGLES1Array*  array;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_ARRAYS )
    {
        array = moduleContext->arrays[ index ];
        if( array != NULL )
        {
            sctOpenGLES1DestroyArray( array );
            moduleContext->arrays[ index ] = NULL;
        }
    }
}

//######################################################################
// BUFFERS

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1ModuleIsValidBufferIndex( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES1_BUFFERS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES1Buffer* sctiOpenGLES1ModuleGetBuffer( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_BUFFERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_BUFFERS )
    {
        return moduleContext->buffers[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleSetBuffer( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1Buffer* buffer )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_BUFFERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_BUFFERS )
    {
        moduleContext->buffers[ index ] = buffer;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleDeleteBuffer( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCTOpenGLES1Buffer* buffer;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_BUFFERS )
    {
        buffer = moduleContext->buffers[ index ];
        if( buffer != NULL )
        {            
#if defined( GL_VERSION_ES_CM_1_1 )
            glDeleteBuffers( 1, &( buffer->buffer ) );
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */

            sctOpenGLES1DestroyBuffer( buffer );
            moduleContext->buffers[ index ] = NULL;
        }
    }
}

// ######################################################################
// DATAS

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1ModuleIsValidDataIndex( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES1_DATAS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES1Data* sctiOpenGLES1ModuleGetData( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_DATAS );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_DATAS )
    {
        return moduleContext->datas[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleSetData( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1Data* data )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_DATAS );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_DATAS )
    {
        moduleContext->datas[ index ] = data;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleDeleteData( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCTOpenGLES1Data*   data;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_DATAS )
    {
        data = moduleContext->datas[ index ];
        if( data != NULL )
        {
            sctOpenGLES1DestroyData( data );
            moduleContext->datas[ index ] = NULL;
        }
    }
}

// ######################################################################
// MESHES

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1ModuleIsValidMeshIndex( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES1_MESHES )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES1Mesh* sctiOpenGLES1ModuleGetMesh( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_MESHES );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_MESHES )
    {
        return moduleContext->meshes[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleSetMesh( SCTOpenGLES1ModuleContext* moduleContext, int index, SCTOpenGLES1Mesh* mesh )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES1_MESHES );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_MESHES )
    {
        moduleContext->meshes[ index ] = mesh;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES1ModuleDeleteMesh( SCTOpenGLES1ModuleContext* moduleContext, int index )
{
    SCTOpenGLES1Mesh*   mesh;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES1_MESHES )
    {
        mesh = moduleContext->meshes[ index ];
        if( mesh != NULL )
        {
            sctOpenGLES1DestroyMesh( mesh );
            moduleContext->meshes[ index ] = NULL;
        }
    }
}

/*!
 *
 *
 */
const char* sctiOpenGLES1GetErrorString( int err )
{
    switch( err )
    {
    case GL_NO_ERROR:
        return "GL_NO_ERROR";       
    case GL_INVALID_ENUM:
        return "GL_INVALID_ENUM";
    case GL_INVALID_VALUE:
        return "GL_INVALID_VALUE";
    case GL_INVALID_OPERATION:
        return "GL_INVALID_OPERATION";
    case GL_STACK_OVERFLOW:
        return "GL_STACK_OVERFLOW";
    case GL_STACK_UNDERFLOW:
        return "GL_STACK_UNDERFLOW";
    case GL_OUT_OF_MEMORY:
        return "GL_OUT_OF_MEMORY";
    default:
        return "UNKNOWN";
    }
}

/*!
 *
 *
 */
void sctiOpenGLES1ClearGLError( void )
{
    int i;
    
    /* Beware infinite loop. */
    for( i = 0; i < 256; ++i )
    {
        if( glGetError() == GL_NO_ERROR )
        {
            break;
        }
    }
}
