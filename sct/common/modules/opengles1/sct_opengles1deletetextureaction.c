/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1deletetextureaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1DeleteTextureActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1DeleteTextureActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1DeleteTextureActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1DeleteTextureActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DeleteTexture@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1DeleteTextureActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1DeleteTextureActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DeleteTextureActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidTextureIndex( context->moduleContext, context->data.textureIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DeleteTextureActionContext( context );
        SCT_LOG_ERROR( "Invalid texture index in DeleteTexture@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1DeleteTextureActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1DeleteTextureActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1DeleteTextureActionContext* context;
    GLenum                                  err;
    OpenGLES1DeleteTextureActionData*       data;
    SCTOpenGLES1Texture*                    texture;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1DeleteTextureActionContext* )( action->context );
    data    = &( context->data );

    texture = sctiOpenGLES1ModuleGetTexture( context->moduleContext, data->textureIndex );
    if( texture == NULL )
    {
        SCT_LOG_ERROR( "Invalid texture in DeleteTexture@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES1ModuleDeleteTexture( context->moduleContext, data->textureIndex );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in DeleteTexture@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1DeleteTextureActionDestroy( SCTAction* action )
{
    SCTOpenGLES1DeleteTextureActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1DeleteTextureActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1DeleteTextureActionContext( context );
}
