/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1deletearrayaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1DeleteArrayActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1DeleteArrayActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1DeleteArrayActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1DeleteArrayActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DeleteArray@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1DeleteArrayActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1DeleteArrayActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1DeleteArrayActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )    
    {
        sctiDestroyOpenGLES1DeleteArrayActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in DeleteArray@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1DeleteArrayActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1DeleteArrayActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1DeleteArrayActionContext*   context;
    OpenGLES1DeleteArrayActionData*         data;
    SCTOpenGLES1Array*                      array;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1DeleteArrayActionContext* )( action->context );
    data    = &( context->data );

    array = sctiOpenGLES1ModuleGetArray( context->moduleContext, data->arrayIndex );        
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array index in DeleteArray@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES1ModuleDeleteArray( context->moduleContext, data->arrayIndex );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1DeleteArrayActionDestroy( SCTAction* action )
{
    SCTOpenGLES1DeleteArrayActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1DeleteArrayActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1DeleteArrayActionContext( context );
}
