/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1createcompressedtexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1CreateCompressedTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1CreateCompressedTextureDataActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1CreateCompressedTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CreateCompressedTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateCompressedTextureData@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1CreateCompressedTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1CreateCompressedTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateCompressedTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidCompressedTextureDataIndex( context->moduleContext, context->data.compressedTextureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateCompressedTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid compressed texture data index in CreateCompressedTextureData@OpenGLES1 context creation." );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateCompressedTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in CreateCompressedTextureData@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1CreateCompressedTextureDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateCompressedTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateCompressedTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1CreateCompressedTextureDataActionContext*   context;
    OpenGLES1CreateCompressedTextureDataActionData*         data;
    SCTOpenGLES1Data*                                       rawData;
    SCTOpenGLES1CompressedTextureData*                      compressedTextureData;
    SCTOpenGLES1CompressedMipMap*                           compressedMipmap;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1CreateCompressedTextureDataActionContext* )( action->context );
    data    = &( context->data );

    rawData = sctiOpenGLES1ModuleGetData( context->moduleContext, data->dataIndex );
    if( rawData == NULL )
    {
        SCT_LOG_ERROR( "Invalid data index in CreateCompressedTextureData@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    compressedTextureData = sctiOpenGLES1ModuleGetCompressedTextureData( context->moduleContext, data->compressedTextureDataIndex );
    if( compressedTextureData != NULL )
    {
        if( compressedTextureData->internalFormat != data->internalFormat ||
            compressedTextureData->mipmapCount != 1 ||
            compressedTextureData->mipmaps[ 0 ].width != data->width ||
            compressedTextureData->mipmaps[ 0 ].height != data->height ||
            compressedTextureData->mipmaps[ 0 ].imageSize != rawData->length )
        {
            SCT_LOG_ERROR( "Compressed texture data index already used in CreateCompressedTextureData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        memcpy( compressedTextureData->mipmaps[ 0 ].data, rawData->data, rawData->length );
    }
    else
    {
        compressedMipmap = ( SCTOpenGLES1CompressedMipMap* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CompressedMipMap ) ) );
        if( compressedMipmap == NULL )
        {
            SCT_LOG_ERROR( "Memory allocation failed in CreateCompressedTextureData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        compressedMipmap->width     = data->width;
        compressedMipmap->height    = data->height;
        compressedMipmap->alignment = 1;
        compressedMipmap->imageSize = rawData->length;
        compressedMipmap->data = ( unsigned char* )( siCommonMemoryAlloc( NULL, rawData->length ) );
        if( compressedMipmap->data == NULL )
        {
            siCommonMemoryFree( NULL, compressedMipmap );
            SCT_LOG_ERROR( "Memory allocation failed in CreateCompressedTextureData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        memcpy( compressedMipmap->data, rawData->data, rawData->length );

        compressedTextureData = ( SCTOpenGLES1CompressedTextureData* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CompressedTextureData ) ) );
        if( compressedTextureData == NULL )
        {
            siCommonMemoryFree( NULL, compressedMipmap->data );
            siCommonMemoryFree( NULL, compressedMipmap );
            SCT_LOG_ERROR( "Memory allocation failed in CreateCompressedTextureData@OpenGLES1 action execute." );
            return SCT_FALSE;
        }

        compressedTextureData->internalFormat       = data->internalFormat;
        compressedTextureData->mipmapCount          = 1;
        compressedTextureData->mipmaps              = compressedMipmap;

        sctiOpenGLES1ModuleSetCompressedTextureData( context->moduleContext, data->compressedTextureDataIndex, compressedTextureData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateCompressedTextureDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES1CreateCompressedTextureDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1CreateCompressedTextureDataActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteCompressedTextureData( context->moduleContext, context->data.compressedTextureDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateCompressedTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES1CreateCompressedTextureDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1CreateCompressedTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1CreateCompressedTextureDataActionContext( context );
}
