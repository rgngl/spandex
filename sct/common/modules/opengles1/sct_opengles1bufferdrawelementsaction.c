/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1bufferdrawelementsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1BufferDrawElementsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1BufferDrawElementsActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1BufferDrawElementsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1BufferDrawElementsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BufferDrawElements@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1BufferDrawElementsActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1BufferDrawElementsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferDrawElementsActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BufferDrawElementsActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in BufferDrawElements@OpenGLES1 context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1BufferDrawElementsActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BufferDrawElementsActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_LOG_ERROR( "BufferDrawElements@OpenGLES1 not supported in OpenGLES 1.0." );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BufferDrawElementsActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1BufferDrawElementsActionContext*    context;
    OpenGLES1BufferDrawElementsActionData*          data;
    GLenum                                          err;
    SCTOpenGLES1Buffer*                             buffer;
    int                                             elementSize;
    GLenum                                          bufferType;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1BufferDrawElementsActionContext* )( action->context );
    data    = &( context->data );

    buffer = sctiOpenGLES1ModuleGetBuffer( context->moduleContext, data->bufferIndex );
    if( buffer == NULL )
    {
        SCT_LOG_ERROR( "Invalid buffer in BufferDrawElements@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES1GetBufferArrayCount( buffer ) != 1 )
    {
        SCT_LOG_ERROR( "Interleaved index array in BufferDrawElements@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    bufferType = sctOpenGLES1GetBufferArrayType( buffer, 0 ); 

    if( bufferType != GL_UNSIGNED_BYTE && bufferType != GL_UNSIGNED_SHORT )
    {
        SCT_LOG_ERROR( "Invalid buffer type in BufferDrawElements@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    elementSize = sctOpenGLES1GetGLTypeSize( bufferType );

    if( ( sctOpenGLES1GetBufferArrayLength( buffer, 0 ) - data->offset ) < data->count )
    {
        SCT_LOG_ERROR( "Too short buffer data in BufferDrawElements@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    if( data->bind != SCT_FALSE )
    {
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, buffer->buffer );
    }
    glDrawElements( data->mode, data->count, bufferType, ( const void* )( data->offset * elementSize ) );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BufferDrawElements@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1BufferDrawElementsActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenGLES1BufferDrawElementsActionDestroy( SCTAction* action )
{
    SCTOpenGLES1BufferDrawElementsActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1BufferDrawElementsActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1BufferDrawElementsActionContext( context );
}
