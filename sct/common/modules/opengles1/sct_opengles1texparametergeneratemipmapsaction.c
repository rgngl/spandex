/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1texparametergeneratemipmapsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1TexParameterGenerateMipmapsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1TexParameterGenerateMipmapsActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1TexParameterGenerateMipmapsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1TexParameterGenerateMipmapsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in TexParameterGenerateMipmaps@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1TexParameterGenerateMipmapsActionContext ) );

    if( sctiParseOpenGLES1TexParameterGenerateMipmapsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1TexParameterGenerateMipmapsActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1TexParameterGenerateMipmapsActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TexParameterGenerateMipmapsActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
#if !defined( GL_VERSION_ES_CM_1_1 )
    SCT_LOG_ERROR( "TexParameterGenerateMipmaps@OpenGLES1 mipmap generation not supported in OpenGLES 1.0." );
    return SCT_FALSE;
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    return SCT_TRUE;
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TexParameterGenerateMipmapsActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1TexParameterGenerateMipmapsActionContext*   context;
    int                                                     err;
    OpenGLES1TexParameterGenerateMipmapsActionData*         data;
    GLfloat                                                 generateMipmaps = 0.0f;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1TexParameterGenerateMipmapsActionContext* )( action->context );
    data    = &( context->data );

    if( data->generateMipmaps == SCT_TRUE )
    {
        generateMipmaps = 1.0f;
    }

#if defined( GL_VERSION_ES_CM_1_1 )
    GL_TEX_PARAMETERF( data->target, GL_GENERATE_MIPMAP, generateMipmaps );
#else   /* defined( GL_VERSION_ES_CM_1_1 ) */
    SCT_USE_VARIABLE( generateMipmaps );
    SCT_ASSERT_ALWAYS( 0 );
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in TexParameterGenerateMipmaps@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();
        
        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1TexParameterGenerateMipmapsActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenGLES1TexParameterGenerateMipmapsActionDestroy( SCTAction* action )
{
    SCTOpenGLES1TexParameterGenerateMipmapsActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1TexParameterGenerateMipmapsActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1TexParameterGenerateMipmapsActionContext( context );
}
