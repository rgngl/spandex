/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1bindtextureaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1BindTextureActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1BindTextureActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1BindTextureActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1BindTextureActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BindTexture@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1BindTextureActionContext ) );

    context->created = SCT_FALSE;

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1BindTextureActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BindTextureActionContext( context );
        return NULL;
    }

    if( context->data.textureIndex >= 0 &&
        sctiOpenGLES1ModuleIsValidTextureIndex( context->moduleContext, context->data.textureIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1BindTextureActionContext( context );
        SCT_LOG_ERROR( "Invalid texture index in BindTexture@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1BindTextureActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BindTextureActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1BindTextureActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1BindTextureActionContext*   context;
    GLenum                                  err;
    OpenGLES1BindTextureActionData*         data;
    SCTOpenGLES1Texture*                    texture;
    GLuint                                  textureId   = 0;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1BindTextureActionContext* )( action->context );
    data    = &( context->data );

    if( data->textureIndex >= 0 )
    {
        texture = sctiOpenGLES1ModuleGetTexture( context->moduleContext, data->textureIndex );
        if( texture == NULL )
        {
            glGenTextures( 1, &textureId );
            texture = sctOpenGLES1CreateTexture( textureId );
            if( texture == NULL )
            {
                glDeleteTextures( 1, &textureId );
                SCT_LOG_ERROR( "Texture creation failed in BindTexture@OpenGLES1 action execute." );
                return SCT_FALSE;
            }
            sctiOpenGLES1ModuleSetTexture( context->moduleContext, data->textureIndex, texture );
            context->created = SCT_TRUE;
        }
        textureId = texture->texture;
    }

    glBindTexture( data->target, textureId );

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BindTexture@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1BindTextureActionTerminate( SCTAction* action )
{
    SCTOpenGLES1BindTextureActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1BindTextureActionContext* )( action->context );

    if( context->created == SCT_TRUE )
    {
        sctiOpenGLES1ModuleDeleteTexture( context->moduleContext, context->data.textureIndex );
        context->created = SCT_FALSE;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES1BindTextureActionDestroy( SCTAction* action )
{
    SCTOpenGLES1BindTextureActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1BindTextureActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1BindTextureActionContext( context );
}
