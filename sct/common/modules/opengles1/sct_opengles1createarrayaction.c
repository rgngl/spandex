/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1createarrayaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1CreateArrayActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1CreateArrayActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1CreateArrayActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1CreateArrayActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateArray@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1CreateArrayActionContext ) );

    context->moduleContext = ( SCTOpenGLES1ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES1CreateArrayActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1CreateArrayActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES1ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )    
    {
        sctiDestroyOpenGLES1CreateArrayActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in CreateArray@OpenGLES1 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1CreateArrayActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateArrayActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1CreateArrayActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1CreateArrayActionContext*   context;
    OpenGLES1CreateArrayActionData*         data;
    SCTOpenGLES1Array*                      array;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1CreateArrayActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES1ModuleGetArray( context->moduleContext, data->arrayIndex ) != NULL )    
    {
        SCT_LOG_ERROR( "Array index already used in CreateArray@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    array = sctOpenGLES1CreateArray( data->type );
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Array creation failed in CreateArray@OpenGLES1 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES1ModuleSetArray( context->moduleContext, data->arrayIndex, array );    

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateArrayActionTerminate( SCTAction* action )
{
    SCTOpenGLES1CreateArrayActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES1CreateArrayActionContext* )( action->context );

    sctiOpenGLES1ModuleDeleteArray( context->moduleContext, context->data.arrayIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES1CreateArrayActionDestroy( SCTAction* action )
{
    SCTOpenGLES1CreateArrayActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1CreateArrayActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1CreateArrayActionContext( context );
}
