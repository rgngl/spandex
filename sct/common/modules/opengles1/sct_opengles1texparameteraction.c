/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles1texparameteraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl1.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES1TexParameterActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES1TexParameterActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES1TexParameterActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES1TexParameterActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in TexParameter@OpenGLES1 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES1TexParameterActionContext ) );

    if( sctiParseOpenGLES1TexParameterActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES1TexParameterActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES1TexParameterActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TexParameterActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenGLES1TexParameterActionContext*  context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenGLES1TexParameterActionContext* )( action->context );

#if !defined( GL_VERSION_ES_CM_1_1 )
    if( context->data.generateMipmaps == SCT_TRUE )
    {
        SCT_LOG_ERROR( "TexParameter@OpenGLES1 mipmap generation not supported in OpenGLES 1.0." );
        return SCT_FALSE;
    }
#else   /* !defined( GL_VERSION_ES_CM_1_1 ) */
    SCT_USE_VARIABLE( context );
#endif  /* !defined( GL_VERSION_ES_CM_1_1 ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES1TexParameterActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES1TexParameterActionContext*  context;
    int                                     err;
    OpenGLES1TexParameterActionData*        data;
    GLfloat                                 generateMipmaps = 0.0f;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES1TexParameterActionContext* )( action->context );
    data    = &( context->data );

    if( data->generateMipmaps == SCT_TRUE )
    {
        generateMipmaps = 1.0f;
    }

    GL_TEX_PARAMETERF( data->target, GL_TEXTURE_MIN_FILTER, ( float )( data->minFilter ) );
    GL_TEX_PARAMETERF( data->target, GL_TEXTURE_MAG_FILTER, ( float )( data->magFilter ) );

    GL_TEX_PARAMETERF( data->target, GL_TEXTURE_WRAP_S, ( float )( data->wrapS ) );
    GL_TEX_PARAMETERF( data->target, GL_TEXTURE_WRAP_T, ( float )( data->wrapT ) );

#if defined( GL_VERSION_ES_CM_1_1 )
    GL_TEX_PARAMETERF( data->target, GL_GENERATE_MIPMAP, generateMipmaps );
#else   /* defined( GL_VERSION_ES_CM_1_1 ) */
    SCT_USE_VARIABLE( generateMipmaps );
#endif  /* defined( GL_VERSION_ES_CM_1_1 ) */

#if defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS )
    /* Check errors */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in TexParameter@OpenGLES1 action execute.", sctiOpenGLES1GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES1ClearGLError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENGLES1_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES1TexParameterActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenGLES1TexParameterActionDestroy( SCTAction* action )
{
    SCTOpenGLES1TexParameterActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES1TexParameterActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES1TexParameterActionContext( context );
}
