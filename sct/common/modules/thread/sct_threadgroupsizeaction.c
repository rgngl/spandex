/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_threadgroupsizeaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_threadmodule.h"
#include "sct_threadmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateThreadGroupSizeActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTThreadGroupSizeActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTThreadGroupSizeActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTThreadGroupSizeActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in GroupSize@Thread context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTThreadGroupSizeActionContext ) );

    context->moduleContext = ( SCTThreadModuleContext* )( moduleContext );

    if( sctiParseThreadGroupSizeActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyThreadGroupSizeActionContext( context );
        return NULL;
    }

    if( sctiThreadModuleIsValidGroupIndex( context->moduleContext, context->data.groupIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid group index in GroupSize@Thread context creation." );
        sctiDestroyThreadGroupSizeActionContext( context );
        return NULL;
    }

    if( context->data.size >= SCT_MAX_GROUP_SIZE )
    {
        SCT_LOG_ERROR( "Invalid group size in GroupSize@Thread context creation." );
        sctiDestroyThreadGroupSizeActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyThreadGroupSizeActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiThreadGroupSizeActionExecute( SCTAction* action, int framenumber )
{
    SCTThreadGroupSizeActionContext*    context;
    ThreadGroupSizeActionData*          data;
    SCTThreadGroup*                     group;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTThreadGroupSizeActionContext* )( action->context );
    data    = &( context->data );

    group = sctiThreadModuleGetGroup( context->moduleContext, data->groupIndex );
    if( group == NULL )
    {
        SCT_LOG_ERROR( "Invalid group in GroupSize@Thread action execute." );
        return SCT_FALSE;
    }

    SCT_ASSERT( group->mutex != NULL );
    siCommonLockMutex( NULL, group->mutex );

    group->size = data->size;
    
    siCommonUnlockMutex( NULL, group->mutex );
        
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiThreadGroupSizeActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyThreadGroupSizeActionContext( ( SCTThreadGroupSizeActionContext* )( action->context ) );
    action->context = NULL;
}

