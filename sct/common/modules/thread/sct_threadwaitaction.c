/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_threadwaitaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_threadmodule.h"
#include "sct_threadmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateThreadWaitActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTThreadWaitActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTThreadWaitActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTThreadWaitActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Wait@Thread context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTThreadWaitActionContext ) );

    context->moduleContext = ( SCTThreadModuleContext* )( moduleContext );

    if( sctiParseThreadWaitActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyThreadWaitActionContext( context );
        return NULL;
    }

    if( sctiThreadModuleIsValidSignalIndex( context->moduleContext, context->data.signalIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid signal index in Wait@Thread context creation." );
        sctiDestroyThreadWaitActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyThreadWaitActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiThreadWaitActionExecute( SCTAction* action, int framenumber )
{
    SCTThreadWaitActionContext* context;
    ThreadWaitActionData*       data;
    int                         s;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTThreadWaitActionContext* )( action->context );
    data    = &( context->data );

    s = sctiThreadModuleWait( context->moduleContext, data->signalIndex, data->timeoutMillis );

    if( s == 0 )
    {
        SCT_LOG_ERROR( "Waiting timeout in Wait@Thread action execute." );        
        return SCT_FALSE;
    }
    else if( s < 0 )
    {
        /* Don't log cancelled waits as errors. However, the action must return
         * SCT_FALSE to abort the benchmark loop. */
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiThreadWaitActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyThreadWaitActionContext( ( SCTThreadWaitActionContext* )( action->context ) );
    action->context = NULL;
}

