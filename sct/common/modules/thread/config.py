#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
SetModuleName( 'Thread' )

######################################################################
AddInclude( 'sct_threadmodule.h' )
AddInclude( 'sct_sicommon.h' )

######################################################################
MAX_SIGNAL_COUNT    = 8
MAX_GROUP_COUNT     = 8
MAX_GROUP_SIZE      = 8

######################################################################
# Signal. Release a single thread waiting for a signal. After release, the
# signal is again set as unsignalled.
AddActionConfig( 'Signal',
                 'Execute'+'Destroy',                 
                 [ IntAttribute(        'SignalIndex',                                  0, MAX_SIGNAL_COUNT )
                   ]
                 )

######################################################################
# Wait for a signal to become signalled. Use 0 timeout for infinite wait; note
# that infinite wait might still be limited by the system.
AddActionConfig( 'Wait',
                 'Execute'+'Destroy',                                  
                 [ IntAttribute(        'SignalIndex',                                  0, MAX_SIGNAL_COUNT ),
                   IntAttribute(        'TimeoutMillis',                                0 )
                   ]
                 )

######################################################################
# Set current thread priority. Thread priority is reverted back to
# THREAD_PRIORITY_NORMAL in action terminate.
AddActionConfig( 'SetPriority',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(       'Priority', 'SCTThreadPriority',                [ 'SCT_THREAD_PRIORITY_LOW',
                                                                                          'SCT_THREAD_PRIORITY_NORMAL',
                                                                                          'SCT_THREAD_PRIORITY_HIGH' ] )
                   ]
                 )

######################################################################
# CompleteBenchmark. Complete a benchmark explicitly from the thread. Allowed
# only in benchmarks with repeats set as 0. Use only in benchmark/final actions.
AddActionConfig( 'CompleteBenchmark',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(        'Framenumber',                                  0 )
                   ]
                 )

######################################################################
# GroupSize. Set group size for group complete.
AddActionConfig( 'GroupSize',
                 'Execute'+'Destroy',
                 [ IntAttribute(        'GroupIndex',                                   0, MAX_GROUP_COUNT ),
                   IntAttribute(        'Size',                                         1, MAX_GROUP_SIZE ),
                   ]
                 )

######################################################################
# GroupCompleteBenchmark. Complete a benchmark co-operatively by a group of
# threads. Waits for the signal until the full group of threads have completed
# the benchmark. Allowed only in benchmarks with repeats set as 0. Use only in
# benchmark/final actions.
AddActionConfig( 'GroupCompleteBenchmark',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(        'Framenumber',                                  0 ),
                   IntAttribute(        'GroupIndex',                                   0, MAX_GROUP_COUNT ),
                   IntAttribute(        'SignalIndex',                                  0, MAX_SIGNAL_COUNT )
                   ]
                 )

######################################################################
# END OF FILE
######################################################################
