/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_threadcompletebenchmarkaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_benchmark.h"

#include "sct_threadmodule.h"
#include "sct_threadmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateThreadCompleteBenchmarkActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTThreadCompleteBenchmarkActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTThreadCompleteBenchmarkActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTThreadCompleteBenchmarkActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CompleteBenchmark@Thread context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTThreadCompleteBenchmarkActionContext ) );

    context->moduleContext = ( SCTThreadModuleContext* )( moduleContext );

    if( sctiParseThreadCompleteBenchmarkActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyThreadCompleteBenchmarkActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyThreadCompleteBenchmarkActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiThreadCompleteBenchmarkActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTThreadCompleteBenchmarkActionContext*   context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTThreadCompleteBenchmarkActionContext* )( action->context );

    if( sctBenchmarkCanComplete( benchmark ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Unsupported explicit complete in CompleteBenchmark@Thread action init." );
        return SCT_FALSE;
    }

    context->currentBenchmark = benchmark;
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiThreadCompleteBenchmarkActionExecute( SCTAction* action, int framenumber )
{
    SCTThreadCompleteBenchmarkActionContext*   context;
    ThreadCompleteBenchmarkActionData*         data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTThreadCompleteBenchmarkActionContext* )( action->context );
    data    = &( context->data );

    if( framenumber >= data->framenumber )
    {
        SCT_ASSERT( context->currentBenchmark != NULL );
        sctBenchmarkComplete( context->currentBenchmark );
    }           
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiThreadCompleteBenchmarkActionTerminate( SCTAction* action )
{
    SCTThreadCompleteBenchmarkActionContext*   context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT( action->context != NULL );    

    context = ( SCTThreadCompleteBenchmarkActionContext* )( action->context );    

    context->currentBenchmark = NULL;
}

/*!
 *
 *
 */
void sctiThreadCompleteBenchmarkActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyThreadCompleteBenchmarkActionContext( ( SCTThreadCompleteBenchmarkActionContext* )( action->context ) );
    action->context = NULL;
}

