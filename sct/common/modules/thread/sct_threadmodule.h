/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_THREADMODULE_H__ )
#define __SCT_THREADMODULE_H__

#include "sct_types.h"

/*!
 *
 *
 */
#define SCT_SIGNAL_COUNT        8
#define SCT_GROUP_COUNT         8
#define SCT_MAX_GROUP_SIZE      8

/*!
 *
 *
 */
typedef struct
{
    void*                       signal;
} SCTThreadSignal;

/*!
 *
 *
 */
typedef struct
{
    void*                       mutex;
    int                         size;
    int                         signals[ SCT_MAX_GROUP_SIZE ];
    int                         waiting;
} SCTThreadGroup;

/*!
 *
 *
 */
typedef struct
{
    SCTThreadSignal*            signals[ SCT_SIGNAL_COUNT ];
    SCTThreadGroup*             groups[ SCT_GROUP_COUNT ];    
} SCTThreadModuleContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    SCTModule*                  sctCreateThreadModule( void );

    SCTBoolean                  sctiThreadModuleIsValidSignalIndex( SCTThreadModuleContext* moduleContext, int index );
    void                        sctiThreadModuleSignal( SCTThreadModuleContext* moduleContext, int index );
    int                         sctiThreadModuleWait( SCTThreadModuleContext* moduleContext, int index, int timeoutMillis );

    SCTBoolean                  sctiThreadModuleIsValidGroupIndex( SCTThreadModuleContext* moduleContext, int index );
    SCTThreadGroup*             sctiThreadModuleGetGroup( SCTThreadModuleContext* moduleContext, int index );
    
    SCTBoolean                  sctThreadModuleResetExt( void );    
    void                        sctThreadModuleCancelWaitsExt( void );
    SCTBoolean                  sctThreadModuleSignalExt( int index );
    int                         sctThreadModuleWaitExt( int index, int timeoutMillis );    
    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_THREADMODULE_H__ ) */
