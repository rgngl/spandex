/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_threadsignalaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_threadmodule.h"
#include "sct_threadmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateThreadSignalActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTThreadSignalActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTThreadSignalActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTThreadSignalActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Signal@Thread context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTThreadSignalActionContext ) );

    context->moduleContext = ( SCTThreadModuleContext* )( moduleContext );

    if( sctiParseThreadSignalActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyThreadSignalActionContext( context );
        return NULL;
    }

    if( sctiThreadModuleIsValidSignalIndex( context->moduleContext, context->data.signalIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid signal index in Signal@Thread context creation." );
        sctiDestroyThreadSignalActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyThreadSignalActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiThreadSignalActionExecute( SCTAction* action, int framenumber )
{
    SCTThreadSignalActionContext*   context;
    ThreadSignalActionData*         data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTThreadSignalActionContext* )( action->context );
    data    = &( context->data );

    sctiThreadModuleSignal( context->moduleContext, data->signalIndex );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiThreadSignalActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyThreadSignalActionContext( ( SCTThreadSignalActionContext* )( action->context ) );
    action->context = NULL;
}

