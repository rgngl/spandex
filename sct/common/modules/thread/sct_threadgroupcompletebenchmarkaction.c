/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_threadgroupcompletebenchmarkaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_benchmark.h"

#include "sct_threadmodule.h"
#include "sct_threadmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateThreadGroupCompleteBenchmarkActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTThreadGroupCompleteBenchmarkActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTThreadGroupCompleteBenchmarkActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTThreadGroupCompleteBenchmarkActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in GroupCompleteBenchmark@Thread context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTThreadGroupCompleteBenchmarkActionContext ) );

    context->moduleContext = ( SCTThreadModuleContext* )( moduleContext );

    if( sctiParseThreadGroupCompleteBenchmarkActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyThreadGroupCompleteBenchmarkActionContext( context );
        return NULL;
    }

    if( sctiThreadModuleIsValidGroupIndex( context->moduleContext, context->data.groupIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid group index in GroupCompleteBenchmark@Thread context creation." );
        sctiDestroyThreadGroupCompleteBenchmarkActionContext( context );
        return NULL;
    }

    if( sctiThreadModuleIsValidSignalIndex( context->moduleContext, context->data.signalIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid signal index in GroupCompleteBenchmark@Thread context creation." );
        sctiDestroyThreadGroupCompleteBenchmarkActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyThreadGroupCompleteBenchmarkActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiThreadGroupCompleteBenchmarkActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTThreadGroupCompleteBenchmarkActionContext*   context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTThreadGroupCompleteBenchmarkActionContext* )( action->context );

    if( sctBenchmarkCanComplete( benchmark ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Unsupported explicit complete in GroupCompleteBenchmark@Thread action init." );
        return SCT_FALSE;
    }

    context->currentBenchmark = benchmark;
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiThreadGroupCompleteBenchmarkActionExecute( SCTAction* action, int framenumber )
{
    SCTThreadGroupCompleteBenchmarkActionContext*   context;
    ThreadGroupCompleteBenchmarkActionData*         data;
    SCTThreadGroup*                                 group;
    int                                             i;
    int                                             s;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTThreadGroupCompleteBenchmarkActionContext* )( action->context );
    data    = &( context->data );

    if( framenumber >= data->framenumber )
    {
        group = sctiThreadModuleGetGroup( context->moduleContext, data->groupIndex );
        if( group == NULL )
        {
            SCT_LOG_ERROR( "Invalid group in GroupCompleteBenchmark@Thread action execute." );
            return SCT_FALSE;
        }

        SCT_ASSERT( group->mutex != NULL );
        siCommonLockMutex( NULL, group->mutex );

        if( ( group->waiting + 1 ) < group->size )
        {
            /* Need to wait for more threads to complete. Add signal to group
             * waiting list, release group mutex, and wait for the signal. */
            group->signals[ group->waiting ] = data->signalIndex;
            group->waiting++;
            
            siCommonUnlockMutex( NULL, group->mutex );
            
            s = sctiThreadModuleWait( context->moduleContext, data->signalIndex, 0 );
            if( s == 0 )
            {
                SCT_LOG_ERROR( "Waiting timeout in GroupCompleteBenchmark@Thread action execute." );        
                return SCT_FALSE;
            }
            else if( s < 0 )
            {
                /* Don't log cancelled waits as errors. However, the action must return
                 * SCT_FALSE to abort the benchmark loop. */
                return SCT_FALSE;
            }
        }
        else
        {
            /* Enough threads have completed. Mark benchmark as completed and
             * signal all waiting threads. */
            SCT_ASSERT( context->currentBenchmark != NULL );
            sctBenchmarkComplete( context->currentBenchmark );
            
            for( i = 0; i < group->waiting; ++i )
            {
                SCT_ASSERT( group->signals[ i ] >= 0 );
                sctiThreadModuleSignal( context->moduleContext, group->signals[ i ] );
                group->signals[ i ] = -1;
            }
            
            group->waiting = 0;
            
            siCommonUnlockMutex( NULL, group->mutex );
        }
    }           
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiThreadGroupCompleteBenchmarkActionTerminate( SCTAction* action )
{
    SCTThreadGroupCompleteBenchmarkActionContext*   context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT( action->context != NULL );    

    context = ( SCTThreadGroupCompleteBenchmarkActionContext* )( action->context );    

    context->currentBenchmark = NULL;
}

/*!
 *
 *
 */
void sctiThreadGroupCompleteBenchmarkActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyThreadGroupCompleteBenchmarkActionContext( ( SCTThreadGroupCompleteBenchmarkActionContext* )( action->context ) );
    action->context = NULL;
}

