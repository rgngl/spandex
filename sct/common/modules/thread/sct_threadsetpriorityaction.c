/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_threadsetpriorityaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_threadmodule.h"
#include "sct_threadmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateThreadSetPriorityActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTThreadSetPriorityActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTThreadSetPriorityActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTThreadSetPriorityActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SetPriority@Thread context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTThreadSetPriorityActionContext ) );

    context->moduleContext = ( SCTThreadModuleContext* )( moduleContext );

    if( sctiParseThreadSetPriorityActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyThreadSetPriorityActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyThreadSetPriorityActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiThreadSetPriorityActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiThreadSetPriorityActionExecute( SCTAction* action, int framenumber )
{
    SCTThreadSetPriorityActionContext*  context;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTThreadSetPriorityActionContext* )( action->context );

    if( siCommonSetThreadPriority( NULL, context->data.priority ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Setting priority failed in SetPriority@Thread action execute." );        
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiThreadSetPriorityActionTerminate( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT( action->context != NULL );    

    siCommonSetThreadPriority( NULL, SCT_THREAD_PRIORITY_NORMAL );   
}

/*!
 *
 *
 */
void sctiThreadSetPriorityActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyThreadSetPriorityActionContext( ( SCTThreadSetPriorityActionContext* )( action->context ) );
    action->context = NULL;
}

