/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_threadmodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_threadmodule_parser.h"
#include "sct_threadmodule_actions.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
SCTAttributeList*       sctiThreadModuleInfo( SCTModule* module );
SCTAction*              sctiThreadModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    sctiThreadModuleDestroy( SCTModule* module );

static SCTThreadSignal* sctiCreateThreadSignal( void );
static void             sctiDestroyThreadSignal( SCTThreadSignal* signal );
static SCTBoolean       sctiThreadModuleRecreateSignals( SCTThreadModuleContext* context );
static void             sctiThreadModuleDestroySignals( SCTThreadModuleContext* context );

static SCTThreadGroup*  sctiCreateThreadGroup( void );
static void             sctiDestroyThreadGroup( SCTThreadGroup* signal );
static SCTBoolean       sctiThreadModuleRecreateGroups( SCTThreadModuleContext* context );
static void             sctiThreadModuleDestroyGroups( SCTThreadModuleContext* context );

/*!
 *
 *
 */
SCTModule* sctCreateThreadModule( void )
{
    SCTModule*              module;
    SCTThreadModuleContext* context;
         
    context = ( SCTThreadModuleContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTThreadModuleContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    
    memset( context, 0, sizeof( SCTThreadModuleContext ) );
    
    module = sctCreateModule( "Thread",
                              context,
#if defined( _WIN32 )
                              _thread_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiThreadModuleInfo,
                              sctiThreadModuleCreateAction,
                              sctiThreadModuleDestroy );

    if( module == NULL )
    {
        siCommonMemoryFree( NULL, context );
        return NULL;
    }

    if( sctiThreadModuleRecreateSignals( context ) == SCT_FALSE )
    {
        sctiThreadModuleDestroy( module );
        sctDestroyModule( module );            
            
        return NULL;
    }

    if( sctiThreadModuleRecreateGroups( context ) == SCT_FALSE )
    {
        sctiThreadModuleDestroy( module );
        sctDestroyModule( module );            
            
        return NULL;
    }
    
    if( sctRegisterPointer( SCT_THREAD_MODULE_POINTER, context ) == SCT_FALSE )
    {
        sctiThreadModuleDestroy( module );        
        sctDestroyModule( module );            

        return NULL;
    }
    
    return module;
}

/*!
 *
 *
 */
SCTAttributeList* sctiThreadModuleInfo( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );
    
    return sctCreateAttributeList();
}

/*!
 *
 *
 */
SCTAction* sctiThreadModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCTThreadModuleContext* moduleContext;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    moduleContext = ( SCTThreadModuleContext* )( module->context );

    SCT_ASSERT_ALWAYS( moduleContext != NULL );

    return sctCreateActionFromTemplate( name, type, "Thread", moduleContext, attributes, ThreadActionTemplates, SCT_ARRAY_LENGTH( ThreadActionTemplates ) );
}

/*!
 *
 *
 */
void sctiThreadModuleDestroy( SCTModule* module )
{
    SCTThreadModuleContext* context;

    SCT_ASSERT_ALWAYS( module != NULL );

    context = ( SCTThreadModuleContext* )( module->context );

    sctUnregisterPointer( SCT_THREAD_MODULE_POINTER );
    
    if( context == NULL )
    {
        return;
    }

    sctiThreadModuleDestroySignals( context );
    sctiThreadModuleDestroyGroups( context );    
    
    siCommonMemoryFree( NULL, context );
    module->context = NULL;
}

// ######################################################################

/*!
 *
 *
 */
static SCTThreadSignal* sctiCreateThreadSignal()
{
    SCTThreadSignal* signal;

    signal = ( SCTThreadSignal* )( siCommonMemoryAlloc( NULL, sizeof( SCTThreadSignal ) ) );

    if( signal == NULL )
    {
        return NULL;
    }

    memset( signal, 0, sizeof( SCTThreadSignal ) );

    signal->signal = siCommonCreateSignal( NULL );

    if( signal->signal == NULL )
    {
        sctiDestroyThreadSignal( signal );
        return NULL;
    }

    return signal;
}

/*!
 *
 *
 */
static void sctiDestroyThreadSignal( SCTThreadSignal* signal )
{
    if( signal == NULL )
    {
        return;
    }
        
    if( signal->signal != NULL )
    {
        siCommonDestroySignal( NULL, signal->signal );
        signal->signal = NULL;
    }

    siCommonMemoryFree( NULL, signal );
}

/*!
 *
 *
 */
static SCTBoolean sctiThreadModuleRecreateSignals( SCTThreadModuleContext* context )
{   
    int i;

    SCT_ASSERT( context != NULL );
    
    for( i = 0; i < SCT_SIGNAL_COUNT; ++i )
    {
        if( context->signals[ i ] != NULL )
        {
            sctiDestroyThreadSignal( context->signals[ i ] );
            context->signals[ i ] = NULL;
        }
    }

    for( i = 0; i < SCT_SIGNAL_COUNT; ++i )
    {   
        context->signals[ i ] = sctiCreateThreadSignal();
        if( context->signals[ i ] == NULL )
        {
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
static void sctiThreadModuleDestroySignals( SCTThreadModuleContext* context )
{
    int i;

    SCT_ASSERT( context != NULL );

    for( i = 0; i < SCT_SIGNAL_COUNT; ++i )
    {
        if( context->signals[ i ] != NULL )
        {
            sctiDestroyThreadSignal( context->signals[ i ] );
            context->signals[ i ] = NULL;
        }
    }   
}

/*!
 *
 *
 */
SCTBoolean sctiThreadModuleIsValidSignalIndex( SCTThreadModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index < 0 || index >= SCT_SIGNAL_COUNT )
    {
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 *
 */
void sctiThreadModuleSignal( SCTThreadModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( sctiThreadModuleIsValidSignalIndex( moduleContext, index ) == SCT_TRUE );
    SCT_ASSERT( moduleContext->signals[ index ] != NULL );    

    siCommonTriggerSignal( NULL, moduleContext->signals[ index ]->signal );
}

/*!
 *
 *
 */
int sctiThreadModuleWait( SCTThreadModuleContext* moduleContext, int index, int timeoutMillis )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( sctiThreadModuleIsValidSignalIndex( moduleContext, index ) == SCT_TRUE );
    SCT_ASSERT( moduleContext->signals[ index ] != NULL );

    return siCommonWaitForSignal( NULL, moduleContext->signals[ index ]->signal, timeoutMillis );
}

/*!
 *
 *
 */
static SCTThreadGroup* sctiCreateThreadGroup()
{
    SCTThreadGroup* group;
    int             i;

    group = ( SCTThreadGroup* )( siCommonMemoryAlloc( NULL, sizeof( SCTThreadGroup ) ) );

    if( group == NULL )
    {
        return NULL;
    }

    memset( group, 0, sizeof( SCTThreadGroup ) );

    group->mutex = siCommonCreateMutex( NULL );

    if( group->mutex == NULL )
    {
        sctiDestroyThreadGroup( group );
        return NULL;
    }

    group->size    = 0;
    group->waiting = 0;
    for( i = 0; i < SCT_MAX_GROUP_SIZE; ++i )
    {
        group->signals[ i ] = -1;
    }
    
    return group;
}

/*!
 *
 *
 */
static void sctiDestroyThreadGroup( SCTThreadGroup* group )
{
    if( group == NULL )
    {
        return;
    }
        
    if( group->mutex != NULL )
    {
        siCommonDestroyMutex( NULL, group->mutex );
        group->mutex = NULL;
    }

    siCommonMemoryFree( NULL, group );
}

/*!
 *
 *
 */
static SCTBoolean sctiThreadModuleRecreateGroups( SCTThreadModuleContext* context )
{   
    int i;

    SCT_ASSERT( context != NULL );
    
    for( i = 0; i < SCT_GROUP_COUNT; ++i )
    {
        if( context->groups[ i ] != NULL )
        {
            sctiDestroyThreadGroup( context->groups[ i ] );
            context->groups[ i ] = NULL;
        }
    }

    for( i = 0; i < SCT_GROUP_COUNT; ++i )
    {   
        context->groups[ i ] = sctiCreateThreadGroup();
        if( context->groups[ i ] == NULL )
        {
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
static void sctiThreadModuleDestroyGroups( SCTThreadModuleContext* context )
{
    int i;

    SCT_ASSERT( context != NULL );

    for( i = 0; i < SCT_GROUP_COUNT; ++i )
    {
        if( context->groups[ i ] != NULL )
        {
            sctiDestroyThreadGroup( context->groups[ i ] );
            context->groups[ i ] = NULL;
        }
    }   
}

/*!
 *
 *
 */
SCTBoolean sctiThreadModuleIsValidGroupIndex( SCTThreadModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index < 0 || index >= SCT_GROUP_COUNT )
    {
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 *
 */
SCTThreadGroup* sctiThreadModuleGetGroup( SCTThreadModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_GROUP_COUNT );

    if( index >= 0 && index < SCT_GROUP_COUNT )
    {
        return moduleContext->groups[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
SCTBoolean sctThreadModuleResetExt()
{
    SCTThreadModuleContext* moduleContext;
    SCTBoolean              pointerRegistered   = SCT_FALSE;

    moduleContext = ( SCTThreadModuleContext* )( sctGetRegisteredPointer( SCT_THREAD_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || moduleContext == NULL )
    {
        return SCT_FALSE;
    }

    return ( sctiThreadModuleRecreateSignals( moduleContext ) == SCT_TRUE &&
             sctiThreadModuleRecreateGroups( moduleContext ) == SCT_TRUE ) ? SCT_TRUE : SCT_FALSE;
}

/*!
 *
 *
 */
void sctThreadModuleCancelWaitsExt()
{
    SCTThreadModuleContext* moduleContext;
    SCTBoolean              pointerRegistered   = SCT_FALSE;
    int                     i;

    moduleContext = ( SCTThreadModuleContext* )( sctGetRegisteredPointer( SCT_THREAD_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || moduleContext == NULL )
    {
        return;
    }

    for( i = 0; i < SCT_SIGNAL_COUNT; ++i )
    {
        if( moduleContext->signals[ i ] != NULL )
        {
            siCommonCancelSignal( NULL, moduleContext->signals[ i ]->signal );
        }
    }
}

/*!
 *
 *
 */
SCTBoolean sctThreadModuleSignalExt( int index )
{
    SCTThreadModuleContext* moduleContext;
    SCTBoolean              pointerRegistered   = SCT_FALSE;

    moduleContext = ( SCTThreadModuleContext* )( sctGetRegisteredPointer( SCT_THREAD_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || moduleContext == NULL )
    {
        return SCT_FALSE;
    }

    if( sctiThreadModuleIsValidSignalIndex( moduleContext, index ) == SCT_FALSE )
    {
        return SCT_FALSE;
    }
    
    sctiThreadModuleSignal( moduleContext, index );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
int sctThreadModuleWaitExt( int index, int timeoutMillis )
{
    SCTThreadModuleContext* moduleContext;
    SCTBoolean              pointerRegistered   = SCT_FALSE;

    moduleContext = ( SCTThreadModuleContext* )( sctGetRegisteredPointer( SCT_THREAD_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || moduleContext == NULL )
    {
        return -1;
    }

    if( sctiThreadModuleIsValidSignalIndex( moduleContext, index ) == SCT_FALSE )
    {
        return -1;
    }
    
    return sctiThreadModuleWait( moduleContext, index, timeoutMillis );
}
