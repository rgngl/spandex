/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2readpixelsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2ReadPixelsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2ReadPixelsActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2ReadPixelsActionContext* )siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2ReadPixelsActionContext ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ReadPixels@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2ReadPixelsActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2ReadPixelsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2ReadPixelsActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2ReadPixelsActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in ReadPixels@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2ReadPixelsActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ReadPixelsActionInit( SCTAction *action, SCTBenchmark *benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ReadPixelsActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2ReadPixelsActionContext*    context;
    OpenGLES2ReadPixelsActionData*          data;
    GLenum                                  err;
    SCTOpenGLES2Data*                       rawData;
    int                                     dataLength;
    void*                                   buffer;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2ReadPixelsActionContext* )( action->context );
    data    = &( context->data );

    dataLength = data->width * data->height * sctOpenGLES2GetTexelSize( OPENGLES2_RGBA8888 );
    SCT_ASSERT( dataLength > 0 );

    rawData = sctiOpenGLES2ModuleGetData( context->moduleContext, data->dataIndex );
    if( rawData != NULL )
    {
        if( rawData->length != dataLength )
        {
            SCT_LOG_ERROR( "Invalid data in ReadPixels@OpenGLES2 action execute." );
            return SCT_FALSE;
        }        
    }
    else  
    {    
        buffer = siCommonMemoryAlloc( NULL, dataLength );
        if( buffer == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in ReadPixels@OpenGLES2 action execute." );
            return SCT_FALSE;
        }
        
        rawData = sctOpenGLES2CreateData( buffer, dataLength, SCT_FALSE );
        if( rawData == NULL )
        {
            siCommonMemoryFree( NULL, buffer );
            SCT_LOG_ERROR( "Allocation failed in ReadPixels@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        sctiOpenGLES2ModuleSetData( context->moduleContext, data->dataIndex, rawData );
    }

    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
    glReadPixels( data->x, data->y, data->width, data->height, GL_RGBA, GL_UNSIGNED_BYTE, rawData->data );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in ReadPixels@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2ReadPixelsActionTerminate( SCTAction *action )
{
    SCTOpenGLES2ReadPixelsActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTOpenGLES2ReadPixelsActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteData( context->moduleContext, context->data.dataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2ReadPixelsActionDestroy( SCTAction* action )
{
    SCTOpenGLES2ReadPixelsActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2ReadPixelsActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2ReadPixelsActionContext( context );
}
