/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2createprogramaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CreateProgramActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CreateProgramActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CreateProgramActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CreateProgramActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateProgram@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CreateProgramActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CreateProgramActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateProgramActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidProgramIndex( context->moduleContext, context->data.programIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateProgramActionContext( context );
        SCT_LOG_ERROR( "Invalid program index in CreateProgram@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CreateProgramActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateProgramActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateProgramActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CreateProgramActionContext* context;
    OpenGLES2CreateProgramActionData*       data;
    SCTOpenGLES2Program*                    program;
    GLuint                                  programId;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CreateProgramActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetProgram( context->moduleContext, data->programIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Program index already in use in CreateProgram@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    programId = glCreateProgram();
    if( programId == 0 )
    {      
        sctiOpenGLES2ClearGLError();
        SCT_LOG_ERROR( "Program creation failed in CreateProgram@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    program = sctOpenGLES2CreateProgram( programId );
    if( program == NULL )
    {      
        glDeleteProgram( programId );
        SCT_LOG_ERROR( "Memory allocation failed in CreateProgram@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    
    sctiOpenGLES2ModuleSetProgram( context->moduleContext, data->programIndex, program );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateProgramActionTerminate( SCTAction* action )
{
    SCTOpenGLES2CreateProgramActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2CreateProgramActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteProgram( context->moduleContext, context->data.programIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateProgramActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CreateProgramActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CreateProgramActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CreateProgramActionContext( context );
}
