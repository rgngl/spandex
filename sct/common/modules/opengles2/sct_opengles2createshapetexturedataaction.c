/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2createshapetexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CreateShapeTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CreateShapeTextureDataActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CreateShapeTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CreateShapeTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateShapeTextureData@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CreateShapeTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CreateShapeTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateShapeTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateShapeTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in CreateShapeTextureData@OpenGLES2 context creation." );
        return NULL;
    }

    switch( context->data.shape )
    {
    case OPENGLES2_SHAPE_ROUND:
        if( context->data.parameters->length != 9 )
        {
            sctiDestroyOpenGLES2CreateShapeTextureDataActionContext( context );
            SCT_LOG_ERROR( "Invalid parameters for OPENGLES2_SHAPE_ROUND in CreateShapeTextureData@OpenGLES2 context creation." );
            return NULL;
        }
        break;
        
    default:
        sctiDestroyOpenGLES2CreateShapeTextureDataActionContext( context );        
        SCT_LOG_ERROR( "Unexpected shape in CreateShapeTextureData@OpenGLES2 context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CreateShapeTextureDataActionContext( void* context )
{
    SCTOpenGLES2CreateShapeTextureDataActionContext*    c;
   
    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenGLES2CreateShapeTextureDataActionContext* )( context );
    
    if( c->data.parameters != NULL )
    {
        sctDestroyFloatVector( c->data.parameters );
        c->data.parameters = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateShapeTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateShapeTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CreateShapeTextureDataActionContext*    context;
    OpenGLES2CreateShapeTextureDataActionData*          data;
    SCTOpenGLES2TextureData*                            textureData  = NULL;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CreateShapeTextureDataActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetTextureData( context->moduleContext, data->textureDataIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Texture data index already used in CreateShapeTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( data->shape == OPENGLES2_SHAPE_ROUND )
    {
        float   color1[ 4 ];
        float   color2[ 4 ];
        int     size;

        color1[ 0 ] = data->parameters->data[ 0 ];
        color1[ 1 ] = data->parameters->data[ 1 ];
        color1[ 2 ] = data->parameters->data[ 2 ];
        color1[ 3 ] = data->parameters->data[ 3 ];

        color2[ 0 ] = data->parameters->data[ 4 ];
        color2[ 1 ] = data->parameters->data[ 5 ];
        color2[ 2 ] = data->parameters->data[ 6 ];
        color2[ 3 ] = data->parameters->data[ 7 ];

        size = ( int )( data->parameters->data[ 8 ] );
        
        textureData = sctOpenGLES2CreateRoundTextureData( data->type, 
                                                          size, 
                                                          color1, 
                                                          color2, 
                                                          data->mipmaps );
    }
    
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Texture data creation failed in CreateShapeTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES2ModuleSetTextureData( context->moduleContext, data->textureDataIndex, textureData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateShapeTextureDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES2CreateShapeTextureDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2CreateShapeTextureDataActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteTextureData( context->moduleContext, context->data.textureDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateShapeTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CreateShapeTextureDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CreateShapeTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CreateShapeTextureDataActionContext( context );
}
