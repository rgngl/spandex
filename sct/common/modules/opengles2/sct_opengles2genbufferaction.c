/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2genbufferaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2GenBufferActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2GenBufferActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2GenBufferActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2GenBufferActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in GenBuffer@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2GenBufferActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2GenBufferActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2GenBufferActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2GenBufferActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in GenBuffer@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2GenBufferActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2GenBufferActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2GenBufferActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2GenBufferActionContext* context;
    GLenum                              err;
    OpenGLES2GenBufferActionData*       data;
    SCTOpenGLES2Buffer*                 buffer;
    GLuint                              bufferId;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2GenBufferActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetBuffer( context->moduleContext, data->bufferIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Buffer index already used in GenBuffer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    glGenBuffers( 1, &bufferId );
    buffer = sctOpenGLES2CreateBuffer( bufferId );
    if( buffer == NULL )
    {
        glDeleteBuffers( 1, &bufferId );
        SCT_LOG_ERROR( "Buffer creation failed in GenBuffer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    
    sctiOpenGLES2ModuleSetBuffer( context->moduleContext, data->bufferIndex, buffer );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in GenBuffer@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2GenBufferActionTerminate( SCTAction* action )
{
    SCTOpenGLES2GenBufferActionContext* context;
    OpenGLES2GenBufferActionData*       data;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2GenBufferActionContext* )( action->context );
    data    = &( context->data );

    sctiOpenGLES2ModuleDeleteBuffer( context->moduleContext, data->bufferIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2GenBufferActionDestroy( SCTAction* action )
{
    SCTOpenGLES2GenBufferActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2GenBufferActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2GenBufferActionContext( context );
}
