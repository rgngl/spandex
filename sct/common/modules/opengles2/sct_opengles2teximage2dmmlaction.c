/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2teximage2dmmlaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2TexImage2DMMLActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2TexImage2DMMLActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2TexImage2DMMLActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2TexImage2DMMLActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in TexImage2DMML@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2TexImage2DMMLActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2TexImage2DMMLActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2TexImage2DMMLActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2TexImage2DMMLActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in TexImage2DMML@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2TexImage2DMMLActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2TexImage2DMMLActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2TexImage2DMMLActionContext* context;
    GLenum                                  err;
    OpenGLES2TexImage2DMMLActionData*       data;
    SCTOpenGLES2TextureData*                textureData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2TexImage2DMMLActionContext* )( action->context );
    data    = &( context->data );

    textureData = sctiOpenGLES2ModuleGetTextureData( context->moduleContext, data->textureDataIndex );
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Invalid texture data in TexImage2DMML@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

    glTexImage2D( data->target, 
                  data->mipmapLevel, 
                  textureData->glFormat, 
                  textureData->mipmaps[ 0 ].width, 
                  textureData->mipmaps[ 0 ].height, 
                  0, 
                  textureData->glFormat, 
                  textureData->glType,
                  textureData->mipmaps[ 0 ].data );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in TexImage2DMML@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2TexImage2DMMLActionDestroy( SCTAction* action )
{
    SCTOpenGLES2TexImage2DMMLActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2TexImage2DMMLActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2TexImage2DMMLActionContext( context );
}
