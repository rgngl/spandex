/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2bufferdrawelementsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2BufferDrawElementsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2BufferDrawElementsActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2BufferDrawElementsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2BufferDrawElementsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BufferDrawElements@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2BufferDrawElementsActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2BufferDrawElementsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BufferDrawElementsActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BufferDrawElementsActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in BufferDrawElements@OpenGLES2 context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2BufferDrawElementsActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2BufferDrawElementsActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2BufferDrawElementsActionContext*    context;
    OpenGLES2BufferDrawElementsActionData*          data;
    GLenum                                          err;
    SCTOpenGLES2Buffer*                             buffer;
    int                                             elementSize;
    GLenum                                          bufferType;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2BufferDrawElementsActionContext* )( action->context );
    data    = &( context->data );

    buffer = sctiOpenGLES2ModuleGetBuffer( context->moduleContext, data->bufferIndex );
    if( buffer == NULL )
    {
        SCT_LOG_ERROR( "Invalid buffer in BufferDrawElements@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES2GetBufferArrayCount( buffer ) != 1 )
    {
        SCT_LOG_ERROR( "Interleaved index array in BufferDrawElements@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    bufferType = sctOpenGLES2GetBufferArrayType( buffer, 0 ); 

    if( bufferType != GL_UNSIGNED_BYTE && bufferType != GL_UNSIGNED_SHORT )
    {
        SCT_LOG_ERROR( "Invalid buffer type in BufferDrawElements@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    elementSize = sctOpenGLES2GetGLTypeSize( bufferType );

    if( ( sctOpenGLES2GetBufferArrayLength( buffer, 0 ) - data->offset ) < data->count )
    {
        SCT_LOG_ERROR( "Too short buffer data in BufferDrawElements@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( data->bind != SCT_FALSE )
    {
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, buffer->buffer );
    }
    glDrawElements( data->mode, data->count, bufferType, ( const void* )( data->offset * elementSize ) );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BufferDrawElements@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2BufferDrawElementsActionDestroy( SCTAction* action )
{
    SCTOpenGLES2BufferDrawElementsActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2BufferDrawElementsActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2BufferDrawElementsActionContext( context );
}
