/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2createpalettecompressedtexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"
#include "sct_opengles2utils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 */
static SCT_INLINE int   sctiAddCompressedTexel( unsigned char* texelPtr, unsigned char* palettePtr, int paletteElements, int bytesPerPaletteElement, int nextFreePaletteElement, unsigned char** paletteTexelPtr, int texelCounter );
static SCT_INLINE int   sctiFindPaletteElement( unsigned char* texelPtr, unsigned char* palettePtr, int paletteElements, int bytesPerPaletteElement );
static SCT_INLINE void  sctiAssingNewPaletteElement( unsigned char* texelPtr, unsigned char* palettePtr, int bytesPerPaletteElement, int paletteElementIndex );
static SCT_INLINE void  sctiSetPaletteTexel( unsigned char** texelPtr, int paletteElements, int index, int texelCounter );

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CreatePaletteCompressedTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CreatePaletteCompressedTextureDataActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CreatePaletteCompressedTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CreatePaletteCompressedTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePaletteCompressedTextureData@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CreatePaletteCompressedTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CreatePaletteCompressedTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreatePaletteCompressedTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreatePaletteCompressedTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in CreatePaletteCompressedTextureData@OpenGLES2 context creation." );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidCompressedTextureDataIndex( context->moduleContext, context->data.compressedTextureDataIndex ) == SCT_FALSE )    
    {
        sctiDestroyOpenGLES2CreatePaletteCompressedTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid compressed texture data index in CreatePaletteCompressedTextureData@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CreatePaletteCompressedTextureDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreatePaletteCompressedTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreatePaletteCompressedTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CreatePaletteCompressedTextureDataActionContext* context;
    OpenGLES2CreatePaletteCompressedTextureDataActionData*       data;
    SCTOpenGLES2TextureData*                                     textureData;
    SCTOpenGLES2CompressedTextureData*                           compressedTextureData;
    SCTOpenGLES2CompressedMipMap*                                mipmaps                 = NULL;
    int                                                          mipmapLevels;
    int                                                          paletteElements;
    int                                                          bytesPerPaletteElement;
    int                                                          paletteDataLength;
    int                                                          mipmapLevelsLength;
    int                                                          i, j, k;
    unsigned char*                                               texelPtr;
    unsigned char*                                               palettePtr;
    unsigned char*                                               paletteTexelPtr; 
    int                                                          texelCount;
    int                                                          nextFreePaletteElement;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CreatePaletteCompressedTextureDataActionContext* )( action->context );
    data    = &( context->data );

    compressedTextureData = sctiOpenGLES2ModuleGetCompressedTextureData( context->moduleContext, data->compressedTextureDataIndex );
    if( compressedTextureData != NULL )
    {   
        SCT_LOG_ERROR( "Compressed texture data index already used in CreatePaletteCompressedTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    
    textureData = sctiOpenGLES2ModuleGetTextureData( context->moduleContext, data->textureDataIndex );    
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Invalid texture data index in CreatePaletteCompressedTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    
    if( ( ( data->type == GL_PALETTE4_RGB8_OES     || data->type == GL_PALETTE8_RGB8_OES )      && textureData->type != OPENGLES2_RGB888 )   ||
        ( ( data->type == GL_PALETTE4_RGBA8_OES    || data->type == GL_PALETTE8_RGBA8_OES )     && textureData->type != OPENGLES2_RGBA8888 ) ||
        ( ( data->type == GL_PALETTE4_R5_G6_B5_OES || data->type == GL_PALETTE8_R5_G6_B5_OES )  && textureData->type != OPENGLES2_RGB565 )   ||
        ( ( data->type == GL_PALETTE4_RGBA4_OES    || data->type == GL_PALETTE8_RGBA4_OES )     && textureData->type != OPENGLES2_RGBA4444 ) ||
        ( ( data->type == GL_PALETTE4_RGB5_A1_OES  || data->type == GL_PALETTE8_RGB5_A1_OES )   && textureData->type != OPENGLES2_RGBA5551 ) )
    {
        SCT_LOG_ERROR( "Texture type mismatch in CreatePaletteCompressedTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    mipmapLevels = textureData->mipmapCount;

    switch( data->type )
    {
    case GL_PALETTE4_RGB8_OES:
        paletteElements         = 16;
        bytesPerPaletteElement  = 3;
        break;

    case GL_PALETTE4_RGBA8_OES:
        paletteElements         = 16;
        bytesPerPaletteElement  = 4;
        break;

    case GL_PALETTE4_R5_G6_B5_OES:
        paletteElements         = 16;
        bytesPerPaletteElement  = 2;
        break;

    case GL_PALETTE4_RGBA4_OES:
        paletteElements         = 16;
        bytesPerPaletteElement  = 2;
        break;

    case GL_PALETTE4_RGB5_A1_OES:
        paletteElements         = 16;
        bytesPerPaletteElement  = 2;
        break;

    case GL_PALETTE8_RGB8_OES:
        paletteElements         = 256;
        bytesPerPaletteElement  = 3;
        break;

    case GL_PALETTE8_RGBA8_OES:
        paletteElements         = 256;
        bytesPerPaletteElement  = 4;
        break;

    case GL_PALETTE8_R5_G6_B5_OES:
        paletteElements         = 256;
        bytesPerPaletteElement  = 2;
        break;

    case GL_PALETTE8_RGBA4_OES:
        paletteElements         = 256;
        bytesPerPaletteElement  = 2;
        break;

    case GL_PALETTE8_RGB5_A1_OES:
        paletteElements         = 256;
        bytesPerPaletteElement  = 2;
        break;

    default:
        SCT_LOG_ERROR( "Invalid type CreatePaletteCompressedTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    };

    /* Calculate the memory needed for palette data */
    paletteDataLength  = paletteElements * bytesPerPaletteElement;

    /* Calculate the memory needed for texels */
    mipmapLevelsLength = 0;
    for( i = 0; i < mipmapLevels; ++i )
    {
        mipmapLevelsLength += textureData->mipmaps[ i ].width * textureData->mipmaps[ i ].height;
    }   
    if( paletteElements == 16 )
    {
        mipmapLevelsLength = ( mipmapLevelsLength / 2 ) + ( mipmapLevelsLength % 2 );
    }
      
    /* Allocate memory for mipmaps */
    mipmaps = ( SCTOpenGLES2CompressedMipMap* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CompressedMipMap ) * 1 ) );
    if( mipmaps == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePaletteCompressedTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    memset( mipmaps, 0, sizeof( SCTOpenGLES2CompressedMipMap ) );

    mipmaps[ 0 ].width          = textureData->mipmaps[ 0 ].width;
    mipmaps[ 0 ].height         = textureData->mipmaps[ 0 ].height;
    mipmaps[ 0 ].imageSize      = paletteDataLength + mipmapLevelsLength;

    mipmaps[ 0 ].data           = ( unsigned char* )( siCommonMemoryAlloc( NULL, paletteDataLength + mipmapLevelsLength ) );

    if( mipmaps[ 0 ].data == NULL )
    {
        sctOpenGLES2DestroyCompressedMipMapData( mipmaps, 1 );
        SCT_LOG_ERROR( "Allocation failed in CreatePaletteCompressedTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    /* Create palette compressed texture data */
    palettePtr          = mipmaps[ 0 ].data;
    paletteTexelPtr     = ( ( unsigned char* )( mipmaps[ 0 ].data ) + paletteDataLength ); 

    for( i = 0, k = 0, nextFreePaletteElement = 0; i < mipmapLevels; ++i )
    {
        texelCount = textureData->mipmaps[ i ].width * textureData->mipmaps[ i ].height;
        texelPtr   = textureData->mipmaps[ i ].data;

        for( j = 0; j < texelCount; ++j, k++ )
        {
            nextFreePaletteElement = sctiAddCompressedTexel( texelPtr,
                                                             palettePtr,
                                                             paletteElements,
                                                             bytesPerPaletteElement,
                                                             nextFreePaletteElement,
                                                             &paletteTexelPtr,
                                                             k );
            texelPtr += bytesPerPaletteElement;
        }
    }

    compressedTextureData = sctOpenGLES2CreateCompressedTextureData( data->type, mipmaps, 1 );
    if( compressedTextureData == NULL )
    {
        sctOpenGLES2DestroyCompressedMipMapData( mipmaps, 1 );
        SCT_LOG_ERROR( "Allocation failed in CreatePaletteCompressedTextureData@OpenGLES2 action execute." );
        
        return SCT_FALSE;
    }

    sctiOpenGLES2ModuleSetCompressedTextureData( context->moduleContext, data->compressedTextureDataIndex, compressedTextureData );    

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CreatePaletteCompressedTextureDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES2CreatePaletteCompressedTextureDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2CreatePaletteCompressedTextureDataActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteCompressedTextureData( context->moduleContext, context->data.compressedTextureDataIndex );    
}

/*!
 *
 *
 */
void sctiOpenGLES2CreatePaletteCompressedTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CreatePaletteCompressedTextureDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CreatePaletteCompressedTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CreatePaletteCompressedTextureDataActionContext( context );
}

/* ********************************************************************** */

/*!
 *
 */
static SCT_INLINE int sctiAddCompressedTexel( unsigned char* texelPtr, unsigned char* palettePtr, int paletteElements, int bytesPerPaletteElement, int nextFreePaletteElement, unsigned char** paletteTexelPtr, int texelCounter )
{
    int index;

    index = sctiFindPaletteElement( texelPtr, palettePtr, nextFreePaletteElement, bytesPerPaletteElement );
    if( index < 0 )
    {
        sctiAssingNewPaletteElement( texelPtr, palettePtr, bytesPerPaletteElement, nextFreePaletteElement );
        index = nextFreePaletteElement;
        nextFreePaletteElement += 1;
    }
    sctiSetPaletteTexel( paletteTexelPtr, paletteElements, index, texelCounter );

    return nextFreePaletteElement;
}

/*!
 *
 */
static SCT_INLINE int sctiFindPaletteElement( unsigned char* texelPtr, unsigned char* palettePtr, int paletteElements, int bytesPerPaletteElement )
{
    int i;

    for( i = 0; i < paletteElements; ++i )
    {
        if( memcmp( texelPtr, palettePtr, bytesPerPaletteElement ) == 0 )
        {
            return i;
        }
        palettePtr += bytesPerPaletteElement;
    }

    return -1;
}

/*!
 *
 */
static SCT_INLINE void sctiAssingNewPaletteElement( unsigned char* texelPtr, unsigned char* palettePtr, int bytesPerPaletteElement, int paletteElementIndex )
{
    memcpy( ( palettePtr + paletteElementIndex * bytesPerPaletteElement ), texelPtr, bytesPerPaletteElement );
}

/*!
 *
 */
static SCT_INLINE void sctiSetPaletteTexel( unsigned char** texelPtr, int paletteElements, int index, int texelCounter )
{
    if( paletteElements == 16 )
    {
        SCT_ASSERT( index >= 0 && index < 16 );

        if( ( texelCounter % 2 ) == 0 )
        {
            **texelPtr = ( unsigned char )( index << 4 );
        }
        else
        {
            **texelPtr = ( unsigned char )( **texelPtr | index ); 
            *texelPtr += 1;
        }
    }
    else
    {
        SCT_ASSERT( paletteElements == 256 );
        SCT_ASSERT( index >= 0 && index < 256 );

        **texelPtr = ( unsigned char )( index );
        *texelPtr += 1;
    }
}
