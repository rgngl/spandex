/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2bindtextureaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2BindTextureActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2BindTextureActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2BindTextureActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2BindTextureActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BindTexture@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2BindTextureActionContext ) );

    context->created = SCT_FALSE;

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2BindTextureActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BindTextureActionContext( context );
        return NULL;
    }

    if( context->data.textureIndex >= 0 &&
        sctiOpenGLES2ModuleIsValidTextureIndex( context->moduleContext, context->data.textureIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BindTextureActionContext( context );
        SCT_LOG_ERROR( "Invalid texture index in BindTexture@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2BindTextureActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2BindTextureActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2BindTextureActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2BindTextureActionContext*   context;
    GLenum                                  err;
    OpenGLES2BindTextureActionData*         data;
    SCTOpenGLES2Texture*                    texture;
    GLuint                                  textureId   = 0;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2BindTextureActionContext* )( action->context );
    data    = &( context->data );

    if( data->textureIndex >= 0 )
    {
        texture = sctiOpenGLES2ModuleGetTexture( context->moduleContext, data->textureIndex );
        if( texture == NULL )
        {
            glGenTextures( 1, &textureId );
            texture = sctOpenGLES2CreateTexture( textureId );          
            if( texture == NULL )
            {
                glDeleteTextures( 1, &textureId );
                SCT_LOG_ERROR( "Texture creation failed in BindTexture@OpenGLES2 action execute." );
                return SCT_FALSE;
            }
            sctiOpenGLES2ModuleSetTexture( context->moduleContext, data->textureIndex, texture );
            context->created = SCT_TRUE;
        }
        textureId = texture->texture;
    }

    glBindTexture( data->target, textureId );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BindTexture@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2BindTextureActionTerminate( SCTAction* action )
{
    SCTOpenGLES2BindTextureActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2BindTextureActionContext* )( action->context );

    if( context->created == SCT_TRUE )
    {
        sctiOpenGLES2ModuleDeleteTexture( context->moduleContext, context->data.textureIndex );
        context->created = SCT_FALSE;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2BindTextureActionDestroy( SCTAction* action )
{
    SCTOpenGLES2BindTextureActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2BindTextureActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2BindTextureActionContext( context );
}
