/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2meshvertexattribpointeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2MeshVertexAttribPointerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2MeshVertexAttribPointerActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2MeshVertexAttribPointerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2MeshVertexAttribPointerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in MeshVertexAttribPointer@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2MeshVertexAttribPointerActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2MeshVertexAttribPointerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2MeshVertexAttribPointerActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.registerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2MeshVertexAttribPointerActionContext( context );
        SCT_LOG_ERROR( "Invalid register index in MeshVertexAttribPointer@OpenGLES2 context creation." );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidMeshIndex( context->moduleContext, context->data.meshIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2MeshVertexAttribPointerActionContext( context );
        SCT_LOG_ERROR( "Invalid mesh index in MeshVertexAttribPointer@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2MeshVertexAttribPointerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2MeshVertexAttribPointerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2MeshVertexAttribPointerActionContext*   context;
    OpenGLES2MeshVertexAttribPointerActionData*         data;
    SCTOpenGLES2Mesh*                                   mesh;
    GLenum                                              err;
    int                                                 index;
    GLboolean                                           normalized      = GL_FALSE;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2MeshVertexAttribPointerActionContext* )( action->context );
    data    = &( context->data );

    index = ( int )( sctiOpenGLES2ModuleGetRegisterValue( context->moduleContext, data->registerIndex ) );
    if( index < 0 )
    {
        SCT_LOG_ERROR( "Invalid value in register in MeshVertexAttribPointer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    mesh = sctiOpenGLES2ModuleGetMesh( context->moduleContext, data->meshIndex );
    if( mesh == NULL )
    {
        SCT_LOG_ERROR( "Invalid mesh index in MeshVertexAttribPointer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES2IsValidMeshArrayIndex( mesh, data->arrayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Array index out of bounds in MeshVertexAttribPointer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( data->normalized == SCT_TRUE )
    {
        normalized = GL_TRUE;
    }

    glVertexAttribPointer( index, 
                           sctOpenGLES2MeshArrayComponents( mesh, data->arrayIndex ),
                           sctOpenGLES2MeshArrayType( mesh, data->arrayIndex ), 
                           normalized, 
                           sctOpenGLES2MeshArrayStride( mesh, data->arrayIndex ),
                           sctOpenGLES2MeshArrayPointer( mesh, data->arrayIndex ) );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in MeshVertexAttribPointer@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2MeshVertexAttribPointerActionDestroy( SCTAction* action )
{
    SCTOpenGLES2MeshVertexAttribPointerActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2MeshVertexAttribPointerActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2MeshVertexAttribPointerActionContext( context );
}
