/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2bindbufferaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2BindBufferActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2BindBufferActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2BindBufferActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2BindBufferActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BindBuffer@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2BindBufferActionContext ) );

    context->created = SCT_FALSE;

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2BindBufferActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BindBufferActionContext( context );
        return NULL;
    }

    if( context->data.bufferIndex >= 0 && 
        sctiOpenGLES2ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BindBufferActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in BindBuffer@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2BindBufferActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2BindBufferActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2BindBufferActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2BindBufferActionContext*    context;
    GLenum                                  err;
    OpenGLES2BindBufferActionData*          data;
    SCTOpenGLES2Buffer*                     buffer;
    GLuint                                  bufferId        = 0;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2BindBufferActionContext* )( action->context );
    data    = &( context->data );

    if( data->bufferIndex >= 0 )
    {
        buffer = sctiOpenGLES2ModuleGetBuffer( context->moduleContext, data->bufferIndex );
        if( buffer == NULL )
        {
            glGenBuffers( 1, &bufferId );
            buffer = sctOpenGLES2CreateBuffer( bufferId );
            if( buffer == NULL )
            {
                glDeleteBuffers( 1, &bufferId );
                SCT_LOG_ERROR( "Buffer creation failed in BindBuffer@OpenGLES2 action execute." );
                return SCT_FALSE;
            }
            sctiOpenGLES2ModuleSetBuffer( context->moduleContext, data->bufferIndex, buffer );
            context->created = SCT_TRUE;
        }
        
        bufferId = buffer->buffer;
    }

    glBindBuffer( data->target, bufferId );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BindBuffer@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2BindBufferActionTerminate( SCTAction* action )
{
    SCTOpenGLES2BindBufferActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2BindBufferActionContext* )( action->context );

    if( context->created == SCT_TRUE )
    {
        sctiOpenGLES2ModuleDeleteBuffer( context->moduleContext, context->data.bufferIndex );
        context->created = SCT_FALSE;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2BindBufferActionDestroy( SCTAction* action )
{
    SCTOpenGLES2BindBufferActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2BindBufferActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2BindBufferActionContext( context );
}
