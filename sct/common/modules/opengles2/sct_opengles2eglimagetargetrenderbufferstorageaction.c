/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2eglimagetargetrenderbufferstorageaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_EGL_MODULE )
# include "sct_eglmodule.h"
#endif  /* defined( INCLUDE_EGL_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateOpenGLES2EglImageTargetRenderbufferStorageActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2EglImageTargetRenderbufferStorageActionContext* context;
    SCTOpenGLES2ModuleContext*                                  mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenGLES2ModuleContext* )( moduleContext );
    context = ( SCTOpenGLES2EglImageTargetRenderbufferStorageActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2EglImageTargetRenderbufferStorageActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in EglImageTargetRenderbufferStorage@OpenGLES2 context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2EglImageTargetRenderbufferStorageActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenGLES2EglImageTargetRenderbufferStorageActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2EglImageTargetRenderbufferStorageActionContext( context );
        return NULL;
    }

    context->glEGLImageTargetRenderbufferStorageOES = NULL;
    context->validated                              = SCT_FALSE;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2EglImageTargetRenderbufferStorageActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2EglImageTargetRenderbufferStorageActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenGLES2EglImageTargetRenderbufferStorageActionContext*   context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenGLES2EglImageTargetRenderbufferStorageActionContext* )( action->context );  

#if !defined( INCLUDE_EGL_MODULE )
    SCT_USE_VARIABLE( context );
    SCT_LOG_ERROR( "EglImageTargetRenderbufferStorage@OpenGLES2 action requires EGL module." );
    return SCT_FALSE;
    
#else  /* !defined( INCLUDE_EGL_MODULE ) */    
    context->glEGLImageTargetRenderbufferStorageOES = ( SCTEGLImageTargetRenderbufferStorageOES )( siCommonGetProcAddress( NULL, "glEGLImageTargetRenderbufferStorageOES" ) );

    if( context->glEGLImageTargetRenderbufferStorageOES == NULL )
    {
        SCT_LOG_ERROR( "glEGLImageTargetRenderbufferStorageOES function not found in EglImageTargetRenderbufferStorage@OpenGLES2 action init." );
        return SCT_FALSE;
    }
    
    context->validated = SCT_FALSE;   
    
    return SCT_TRUE;
#endif  /* !defined( INCLUDE_EGL_MODULE ) */        
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2EglImageTargetRenderbufferStorageActionExecute( SCTAction* action, int framenumber )
{
#if defined( INCLUDE_EGL_MODULE )    
    SCTOpenGLES2EglImageTargetRenderbufferStorageActionContext* context;
    OpenGLES2EglImageTargetRenderbufferStorageActionData*       data;
    const char*                                                 extString;
    EGLImageKHR                                                 eglImage    = ( EGLImageKHR )( 0 );
    GLenum                                                      err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenGLES2EglImageTargetRenderbufferStorageActionContext* )( action->context );
    data    = &( context->data );

    if( context->validated == SCT_FALSE )
    {
        extString = ( const char* )( glGetString( GL_EXTENSIONS ) );
        if( extString == NULL || strstr( extString, "GL_OES_EGL_image" ) == NULL )
        {
            SCT_LOG_ERROR( "GL_OES_EGL_image extension not supported in EglImageTargetRenderbufferStorage@OpenGLES2 action execute." );
            return SCT_FALSE;
        }
        context->validated = SCT_TRUE;
    }

    eglImage = sctEglModuleGetImageExt( data->eglImageIndex );
    if( eglImage == EGL_NO_IMAGE_KHR )
    {
        SCT_LOG_ERROR( "Invalid EGL image in EglImageTargetRenderbufferStorage@OpenGLES2 action execute." );        
        return SCT_FALSE;
    }

    SCT_ASSERT( context->glEGLImageTargetRenderbufferStorageOES != NULL );
    context->glEGLImageTargetRenderbufferStorageOES( data->target, ( SCTGLeglImageOES )( eglImage ) );

# if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in EglImageTargetRenderbufferStorage@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
# else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
# endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    
    return SCT_TRUE;
#else   /* defined( INCLUDE_EGL_MODULE ) */
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );    
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;    
#endif  /* defined( INCLUDE_EGL_MODULE ) */    
}

/*!
 *
 *
 */
void sctiOpenGLES2EglImageTargetRenderbufferStorageActionTerminate( SCTAction* action )
{
    SCTOpenGLES2EglImageTargetRenderbufferStorageActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2EglImageTargetRenderbufferStorageActionContext* )( action->context );

    context->glEGLImageTargetRenderbufferStorageOES = NULL;
    context->validated                              = SCT_FALSE;
}

/*!
 *
 *
 */
void sctiOpenGLES2EglImageTargetRenderbufferStorageActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenGLES2EglImageTargetRenderbufferStorageActionContext( action->context );
    action->context = NULL;
}
