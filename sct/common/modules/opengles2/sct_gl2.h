/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Wrapper that should be included instead of GL2.H. This file
 * includes the platform-specific GL2.H and defines additional macros
 * as needed. */

#if !defined( __SCT_GL2_H__ )
#define __SCT_GL2_H__

#include "sct_sicommon.h"
#include "sct_types.h"

/* Fix apple madness. */
#if defined( SCT_IOS_OPENGLES2 ) 
# include <OpenGLES/ES2/gl.h> 
#else  /* defined( SCT_IPHONE_OPENGLES2 ) */
# include <GLES2/gl2.h> 
#endif /* defined( SCT_IPHONE_OPENGLES2 ) */

/*!
 *
 */
typedef void* SCTGLeglImageOES;
typedef void ( *SCTEGLImageTargetTexture2DOES )( GLenum target, SCTGLeglImageOES image );
typedef void ( *SCTEGLImageTargetRenderbufferStorageOES )( GLenum target, SCTGLeglImageOES image );

#if !defined( GL_DEPTH_STENCIL_OES )
# define GL_DEPTH_STENCIL_OES                           0x84F9
#endif  /* !defined( GL_DEPTH_STENCIL_OES ) */

#if !defined( GL_UNSIGNED_INT_24_8_OES )
# define GL_UNSIGNED_INT_24_8_OES                       0x84FA
#endif  /* !defined( GL_UNSIGNED_INT_24_8_OES ) */

#if !defined( GL_DEPTH24_STENCIL8_OES )
# define GL_DEPTH24_STENCIL8_OES                        0x88F0
#endif  /* !defined( GL_DEPTH24_STENCIL8_OES ) */

#if !defined( GL_TEXTURE_EXTERNAL_OES )
# define GL_TEXTURE_EXTERNAL_OES                        0x8D65
#endif  /* !defined( GL_TEXTURE_EXTERNAL_OES ) */

#if !defined( GL_BGRA_EXT )
# define GL_BGRA_EXT                                    0x80E1
#endif  /* !defined( GL_BGRA_EXT ) */

#if !defined( GL_FRAMEBUFFER_INCOMPLETE_FORMATS )
# define GL_FRAMEBUFFER_INCOMPLETE_FORMATS              0x8CDA
#endif  /* !defined( GL_FRAMEBUFFER_INCOMPLETE_FORMATS ) */

#if !defined( GL_PALETTE4_RGB8_OES )
# define GL_PALETTE4_RGB8_OES                           0x8B90
#endif  /* !defined( GL_PALETTE4_RGB8_OES ) */

#if !defined( GL_PALETTE4_RGBA8_OES )
# define GL_PALETTE4_RGBA8_OES                          0x8B91
#endif  /* !defined( GL_PALETTE4_RGBA8_OES ) */

#if !defined( GL_PALETTE4_R5_G6_B5_OES )
# define GL_PALETTE4_R5_G6_B5_OES                       0x8B92
#endif  /* !defined( GL_PALETTE4_R5_G6_B5_OES ) */

#if !defined( GL_PALETTE4_RGBA4_OES )
#define GL_PALETTE4_RGBA4_OES                           0x8B93
#endif  /* !defined( GL_PALETTE4_RGBA4_OES ) */

#if !defined( GL_PALETTE4_RGB5_A1_OES )
#define GL_PALETTE4_RGB5_A1_OES                         0x8B94
#endif  /* !defined( GL_PALETTE4_RGB5_A1_OES ) */

#if !defined( GL_PALETTE8_RGB8_OES )
#define GL_PALETTE8_RGB8_OES                            0x8B95
#endif  /* !defined( GL_PALETTE8_RGB8_OES ) */

#if !defined( GL_PALETTE8_RGBA8_OES )
#define GL_PALETTE8_RGBA8_OES                           0x8B96
#endif  /* !defined( GL_PALETTE8_RGBA8_OES ) */

#if !defined( GL_PALETTE8_R5_G6_B5_OES )
#define GL_PALETTE8_R5_G6_B5_OES                        0x8B97
#endif  /* !defined( GL_PALETTE8_R5_G6_B5_OES ) */

#if !defined( GL_PALETTE8_RGBA4_OES )
#define GL_PALETTE8_RGBA4_OES                           0x8B98
#endif  /* !defined( GL_PALETTE8_RGBA4_OES ) */

#if !defined( GL_PALETTE8_RGB5_A1_OES )
#define GL_PALETTE8_RGB5_A1_OES                         0x8B99
#endif  /* !defined( GL_PALETTE8_RGB5_A1_OES ) */

#if !defined( GL_HALF_FLOAT_OES )
# define GL_HALF_FLOAT_OES                              0x8D61
#endif  /* !defined( GL_HALF_FLOAT_OES ) */

#ifndef GL_OES_stencil4
#define GL_STENCIL_INDEX4_OES                           0x8D47
#endif

#if !defined( GL_RGB8_OES )
# define GL_RGB8_OES                                    0x8051
#endif  /* !defined( GL_RGB8_OES ) */

#if !defined( GL_RGBA8_OES )
# define GL_RGBA8_OES                                   0x8058
#endif  /* !defined( GL_RGBA8_OES ) */

#if !defined( GL_DEPTH_COMPONENT24_OES )
# define GL_DEPTH_COMPONENT24_OES                       0x81A6
#endif  /* !defined( GL_DEPTH_COMPONENT24_OES ) */

#if !defined( GL_DEPTH_COMPONENT32_OES )
# define GL_DEPTH_COMPONENT32_OES                      0x81A7
#endif  /* !defined( GL_DEPTH_COMPONENT32_OES ) */

#if !defined( GL_UNSIGNED_SHORT )
# define GL_UNSIGNED_SHORT                              0x1403
#endif  /* !defined( GL_UNSIGNED_SHORT ) */

#if !defined( GL_DEPTH_COMPONENT )
#define GL_DEPTH_COMPONENT                              0x1902
#endif  /* !defined( GL_DEPTH_COMPONENT ) */

#if !defined( GL_ATC_RGB_AMD )
# define GL_ATC_RGB_AMD                                 0x8C92
#endif  /* !defined( GL_ATC_RGB_AMD ) */

#if !defined( GL_ATC_RGBA_AMD )
# define GL_ATC_RGBA_AMD                                0x8C93
#endif  /* !defined( GL_ATC_RGBA_AMD ) */

#if !defined( GL_ETC1_RGB8_OES )
# define GL_ETC1_RGB8_OES                               0x8D64
#endif  /* !defined( GL_ETC1_RGB8_OES ) */

#if !defined( GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG )
# define GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG             0x8C00
#endif  /* !defined( GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG ) */

#if !defined( GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG )
# define GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG             0x8C01
#endif  /* !defined( GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG ) */

#if !defined( GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG )
# define GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG            0x8C02
#endif  /* !defined( GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG ) */

#if !defined( GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG )
# define GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG            0x8C03
#endif  /* !defined( GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG ) */

#if !defined( GL_PLATFORM_BINARY )
# define GL_PLATFORM_BINARY                             0x8D63
#endif /* GL_PLATFORM_BINARY */

#endif /* !defined( __SCT_GL2_H__ ) */
