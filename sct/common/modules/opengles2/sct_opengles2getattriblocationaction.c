/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2getattriblocationaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2GetAttribLocationActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2GetAttribLocationActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2GetAttribLocationActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2GetAttribLocationActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in GetAttribLocation@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2GetAttribLocationActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2GetAttribLocationActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2GetAttribLocationActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.registerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2GetAttribLocationActionContext( context );
        SCT_LOG_ERROR( "Invalid register index in GetAttribLocation@OpenGLES2 context creation." );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidProgramIndex( context->moduleContext, context->data.programIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2GetAttribLocationActionContext( context );
        SCT_LOG_ERROR( "Invalid program index in GetAttribLocation@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2GetAttribLocationActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2GetAttribLocationActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2GetAttribLocationActionContext* context;
    GLenum                                      err;
    OpenGLES2GetAttribLocationActionData*       data;
    SCTOpenGLES2Program*                        program;
    int                                         location;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2GetAttribLocationActionContext* )( action->context );
    data    = &( context->data );

    program = sctiOpenGLES2ModuleGetProgram( context->moduleContext, data->programIndex );
    if( program == NULL )
    {
        SCT_LOG_ERROR( "Invalid program index in GetAttribLocation@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    location = glGetAttribLocation( program->program, data->name );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( location < 0 || err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in GetAttribLocation@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    sctiOpenGLES2ModuleSetRegisterValue( context->moduleContext, data->registerIndex, location );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2GetAttribLocationActionDestroy( SCTAction* action )
{
    SCTOpenGLES2GetAttribLocationActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2GetAttribLocationActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2GetAttribLocationActionContext( context );
}
