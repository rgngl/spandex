/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2genrenderbufferaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2GenRenderbufferActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2GenRenderbufferActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2GenRenderbufferActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2GenRenderbufferActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in GenRenderbuffer@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2GenRenderbufferActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2GenRenderbufferActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2GenRenderbufferActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidRenderbufferIndex( context->moduleContext, context->data.renderbufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2GenRenderbufferActionContext( context );
        SCT_LOG_ERROR( "Invalid renderbuffer index in GenRenderbuffer@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2GenRenderbufferActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2GenRenderbufferActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );
    
    return SCT_TRUE;
}
/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2GenRenderbufferActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2GenRenderbufferActionContext*   context;
    GLenum                                      err;
    OpenGLES2GenRenderbufferActionData*         data;
    SCTOpenGLES2Renderbuffer*                   renderbuffer;
    GLuint                                      renderbufferId  = 0;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2GenRenderbufferActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetRenderbuffer( context->moduleContext, data->renderbufferIndex ) != NULL )
    {
       SCT_LOG_ERROR( "Renderbuffer index already in use in GenRenderbuffer@OpenGLES2 action execute." );
       return SCT_FALSE;
    }
    
    glGenRenderbuffers( 1, &renderbufferId );
    renderbuffer = sctOpenGLES2CreateRenderbuffer( renderbufferId );

    if( renderbuffer == NULL )
    {
        glDeleteRenderbuffers( 1, &renderbufferId );
        SCT_LOG_ERROR( "Renderbuffer creation failed in GenRenderbuffer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    sctiOpenGLES2ModuleSetRenderbuffer( context->moduleContext, data->renderbufferIndex, renderbuffer );
    
#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in GenRenderbuffer@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2GenRenderbufferActionTerminate( SCTAction* action )
{
    SCTOpenGLES2GenRenderbufferActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2GenRenderbufferActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteRenderbuffer( context->moduleContext, context->data.renderbufferIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2GenRenderbufferActionDestroy( SCTAction* action )
{
    SCTOpenGLES2GenRenderbufferActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2GenRenderbufferActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2GenRenderbufferActionContext( context );
}
