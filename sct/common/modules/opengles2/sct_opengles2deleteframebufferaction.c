/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2deleteframebufferaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2DeleteFramebufferActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2DeleteFramebufferActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2DeleteFramebufferActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2DeleteFramebufferActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DeleteFramebuffer@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2DeleteFramebufferActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2DeleteFramebufferActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DeleteFramebufferActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidFramebufferIndex( context->moduleContext, context->data.framebufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DeleteFramebufferActionContext( context );
        SCT_LOG_ERROR( "Invalid framebuffer index in DeleteFramebuffer@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2DeleteFramebufferActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2DeleteFramebufferActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2DeleteFramebufferActionContext* context;
    int                                         err;
    OpenGLES2DeleteFramebufferActionData*       data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2DeleteFramebufferActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetFramebuffer( context->moduleContext, data->framebufferIndex ) == NULL )
    {
        SCT_LOG_ERROR( "Invalid framebuffer in DeleteFramebuffer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES2ModuleDeleteFramebuffer( context->moduleContext, data->framebufferIndex );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in DeleteFramebuffer@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2DeleteFramebufferActionDestroy( SCTAction* action )
{
    SCTOpenGLES2DeleteFramebufferActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2DeleteFramebufferActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2DeleteFramebufferActionContext( context );
}
