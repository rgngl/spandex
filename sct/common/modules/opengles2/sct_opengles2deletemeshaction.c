/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2deletemeshaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2DeleteMeshActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2DeleteMeshActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2DeleteMeshActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2DeleteMeshActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DeleteMesh@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2DeleteMeshActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2DeleteMeshActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DeleteMeshActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidMeshIndex( context->moduleContext, context->data.meshIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DeleteMeshActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in DeleteMesh@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2DeleteMeshActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2DeleteMeshActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2DeleteMeshActionContext*    context;
    OpenGLES2DeleteMeshActionData*          data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2DeleteMeshActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetMesh( context->moduleContext, data->meshIndex ) == NULL )
    {
        SCT_LOG_ERROR( "Invalid array index in DeleteMesh@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES2ModuleDeleteMesh( context->moduleContext, data->meshIndex );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2DeleteMeshActionDestroy( SCTAction* action )
{
    SCTOpenGLES2DeleteMeshActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2DeleteMeshActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2DeleteMeshActionContext( context );
}
