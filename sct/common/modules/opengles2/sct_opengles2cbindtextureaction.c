/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2cbindtextureaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_THREAD_MODULE )
# include "sct_threadmodule.h"
#endif  /* defined( INCLUDE_THREAD_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CBindTextureActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CBindTextureActionContext*  context;
    int                                     i;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CBindTextureActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CBindTextureActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CBindTexture@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CBindTextureActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CBindTextureActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CBindTextureActionContext( context );
        return NULL;
    }

    for( i = 0; i < context->data.textures->length; ++i )
    {
        if( context->data.textures->data[ i ] != -1 &&
            sctiOpenGLES2ModuleIsValidTextureIndex( context->moduleContext, context->data.textures->data[ i ] ) == SCT_FALSE )
        {
            sctiDestroyOpenGLES2CBindTextureActionContext( context );
            SCT_LOG_ERROR( "Invalid texture index in CBindTexture@OpenGLES2 context creation." );
            return NULL;
        }
    }

    if( sctiOpenGLES2ModuleIsValidFlagIndex( context->moduleContext, context->data.flagIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CBindTextureActionContext( context );
        SCT_LOG_ERROR( "Invalid flag index in CBindTexture@OpenGLES2 context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CBindTextureActionContext( void* context )
{
    SCTOpenGLES2CBindTextureActionContext*  c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenGLES2CBindTextureActionContext* )( context );
    
    if( c->data.textures != NULL )
    {
        sctDestroyIntVector( c->data.textures );
        c->data.textures = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CBindTextureActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( INCLUDE_THREAD_MODULE )
    SCT_LOG_ERROR( "CBindTexture@OpenGLES2 action requires Thread module." );
    return SCT_FALSE;
#else    /* !defined( INCLUDE_THREAD_MODULE ) */
    return SCT_TRUE;    
#endif  /* !defined( INCLUDE_THREAD_MODULE ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CBindTextureActionExecute( SCTAction* action, int frameNumber )
{
#if defined( INCLUDE_THREAD_MODULE )
    SCTOpenGLES2CBindTextureActionContext*  context;
    GLenum                                  err;
    OpenGLES2CBindTextureActionData*        data;
    SCTOpenGLES2Texture*                    texture;
    int                                     textureIndex;
    GLuint                                  textureId        = 0;
    SCTOpenGLES2Flag*                       flag;
    float                                   flagValue        = 0;
    int                                     flagValueInt;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CBindTextureActionContext* )( action->context );
    data    = &( context->data );

    flag = sctiOpenGLES2ModuleGetFlag( context->moduleContext, data->flagIndex );
    if( flag == NULL )
    {
        SCT_LOG_ERROR( "Undefined flag in CBindTexture@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES2GetFlag( flag, &flagValue ) == SCT_TRUE )
    {
        if( data->finish == SCT_TRUE )
        {
            glFinish();
        }

        flagValueInt = ( int )( flagValue );

        if( flagValueInt < 0 || flagValueInt >= data->textures->length )
        {
            SCT_LOG_ERROR( "Invalid flag value in CBindTexture@OpenGLES2 action execute." );
            return SCT_FALSE;
        }
        
        textureIndex = data->textures->data[ flagValueInt ];
        
        if( textureIndex >= 0 )
        {
            texture = sctiOpenGLES2ModuleGetTexture( context->moduleContext, textureIndex );
            if( texture == NULL )
            {
                SCT_LOG_ERROR( "Invalid texture in CBindTexture@OpenGLES2 action execute." );
                return SCT_FALSE;
            }
            textureId = texture->texture;            
        }

#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: CBindTexture@OpenGLES2 texture switch to %u", textureId );
#endif  /* defined( SCT_DEBUG ) */
        
        glBindTexture( data->target, textureId );

# if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
        /* Check for errors. */
        err = glGetError();
        if( err != GL_NO_ERROR )
        {
            char buf[ 256 ];
            sprintf( buf, "GL error %s in CBindTexture@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
            SCT_LOG_ERROR( buf );
            sctiOpenGLES2ClearGLError();

            return SCT_FALSE;
        }
# else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
        SCT_USE_VARIABLE( err );
# endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    
        if( sctThreadModuleSignalExt( data->threadSignalIndex ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Signaling failed in CBindTexture@OpenGLES2 action execute." );
            return SCT_FALSE;
        }
    }
    
    return SCT_TRUE;

#else   /* defined( INCLUDE_THREAD_MODULE ) */
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;
#endif  /* defined( INCLUDE_THREAD_MODULE ) */   
}

/*!
 *
 *
 */
void sctiOpenGLES2CBindTextureActionTerminate( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
}

/*!
 *
 *
 */
void sctiOpenGLES2CBindTextureActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CBindTextureActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CBindTextureActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CBindTextureActionContext( context );
}
