/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_OPENGLES2CREATEPALETTECOMPRESSEDTEXTUREDATAACTION_H__ )
#define __SCT_OPENGLES2CREATEPALETTECOMPRESSEDTEXTUREDATAACTION_H__

#include "sct_types.h"
#include "sct_opengles2module.h"
#include "sct_opengles2module_parser.h"

/*!
 *
 */
typedef struct
{
    SCTOpenGLES2ModuleContext*                              moduleContext;
    OpenGLES2CreatePaletteCompressedTextureDataActionData   data;
} SCTOpenGLES2CreatePaletteCompressedTextureDataActionContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                               sctiCreateOpenGLES2CreatePaletteCompressedTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroyOpenGLES2CreatePaletteCompressedTextureDataActionContext( void* context );

    SCTBoolean                          sctiOpenGLES2CreatePaletteCompressedTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark );
    SCTBoolean                          sctiOpenGLES2CreatePaletteCompressedTextureDataActionExecute( SCTAction* action, int frameNumber );
    void                                sctiOpenGLES2CreatePaletteCompressedTextureDataActionTerminate( SCTAction* action );
    void                                sctiOpenGLES2CreatePaletteCompressedTextureDataActionDestroy( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_OPENGLES2CREATEPALETTECOMPRESSEDTEXTUREDATAACTION_H__ ) */
