/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_OPENGLES2MODULE_H__ )
#define __SCT_OPENGLES2MODULE_H__

#include "sct_types.h"
#include "sct_opengles2utils.h"
#include "sct_gl2.h"

#define SCT_MAX_OPENGLES2_TEXTURE_DATAS                 4096
#define SCT_MAX_OPENGLES2_TEXTURES                      4096
#define SCT_MAX_OPENGLES2_COMPRESSED_TEXTURE_DATAS      4096
#define SCT_MAX_OPENGLES2_ARRAYS                        4096
#define SCT_MAX_OPENGLES2_BUFFERS                       4096
#define SCT_MAX_OPENGLES2_FRAMEBUFFERS                  4096
#define SCT_MAX_OPENGLES2_RENDERBUFFERS                 4096
#define SCT_MAX_OPENGLES2_SHADERS                       4096
#define SCT_MAX_OPENGLES2_PROGRAMS                      4096
#define SCT_MAX_OPENGLES2_REGISTERS                     4096
#define SCT_MAX_OPENGLES2_DATAS                         4096
#define SCT_MAX_OPENGLES2_MESHES                        4096
#define SCT_MAX_OPENGLES2_FLAGS                         8

/*!
 *
 *
 */
typedef struct
{
    SCTOpenGLES2TextureData*            textureDatas[ SCT_MAX_OPENGLES2_TEXTURE_DATAS ];
    SCTOpenGLES2Texture*                textures[ SCT_MAX_OPENGLES2_TEXTURES ];
    SCTOpenGLES2CompressedTextureData*  compressedTextureDatas[ SCT_MAX_OPENGLES2_COMPRESSED_TEXTURE_DATAS ];
    SCTOpenGLES2Array*                  arrays[ SCT_MAX_OPENGLES2_ARRAYS ];
    SCTOpenGLES2Buffer*                 buffers[ SCT_MAX_OPENGLES2_BUFFERS ];
    SCTOpenGLES2Framebuffer*            framebuffers[ SCT_MAX_OPENGLES2_FRAMEBUFFERS ];
    SCTOpenGLES2Renderbuffer*           renderbuffers[ SCT_MAX_OPENGLES2_RENDERBUFFERS ];
    SCTOpenGLES2Shader*                 shaders[ SCT_MAX_OPENGLES2_SHADERS ];
    SCTOpenGLES2Program*                programs[ SCT_MAX_OPENGLES2_PROGRAMS ];
    double                              registers[ SCT_MAX_OPENGLES2_REGISTERS ];
    SCTOpenGLES2Data*                   datas[ SCT_MAX_OPENGLES2_DATAS ];
    SCTOpenGLES2Mesh*                   meshes[ SCT_MAX_OPENGLES2_MESHES ];
    SCTOpenGLES2Flag*                   flags[ SCT_MAX_OPENGLES2_FLAGS ];
} SCTOpenGLES2ModuleContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    SCTModule*                          sctCreateOpenGLES2Module( void );

    /* External OpenGLES2 module data access. */       
    SCTBoolean                          sctOpenGLES2ModuleSetDataExt( int index, SCTOpenGLES2Data* data );
    GLuint                              sctOpenGLES2ModuleGetTextureExt( int index );
    GLuint                              sctOpenGLES2ModuleGetRenderbufferExt( int index );    
    
    /* Texture datas */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidTextureDataIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2TextureData*            sctiOpenGLES2ModuleGetTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2TextureData* textureData );
    void                                sctiOpenGLES2ModuleDeleteTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index );

    /* Textures */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidTextureIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2Texture*                sctiOpenGLES2ModuleGetTexture( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetTexture( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Texture* texture );
    void                                sctiOpenGLES2ModuleDeleteTexture( SCTOpenGLES2ModuleContext* moduleContext, int index );

    /* Compressed texture datas */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidCompressedTextureDataIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2CompressedTextureData*  sctiOpenGLES2ModuleGetCompressedTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetCompressedTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2CompressedTextureData* compressedTextureData );
    void                                sctiOpenGLES2ModuleDeleteCompressedTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index );

    /* Arrays */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidArrayIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2Array*                  sctiOpenGLES2ModuleGetArray( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetArray( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Array* array );
    void                                sctiOpenGLES2ModuleDeleteArray( SCTOpenGLES2ModuleContext* moduleContext, int index );

    /* Buffers */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidBufferIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2Buffer*                 sctiOpenGLES2ModuleGetBuffer( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetBuffer( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Buffer* buffer );
    void                                sctiOpenGLES2ModuleDeleteBuffer( SCTOpenGLES2ModuleContext* moduleContext, int index );

    /* Framebuffers */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidFramebufferIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2Framebuffer*            sctiOpenGLES2ModuleGetFramebuffer( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetFramebuffer( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Framebuffer* framebuffer );
    void                                sctiOpenGLES2ModuleDeleteFramebuffer( SCTOpenGLES2ModuleContext* moduleContext, int index );

    /* Renderbuffers */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidRenderbufferIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2Renderbuffer*           sctiOpenGLES2ModuleGetRenderbuffer( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetRenderbuffer( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Renderbuffer* renderbuffer );
    void                                sctiOpenGLES2ModuleDeleteRenderbuffer( SCTOpenGLES2ModuleContext* moduleContext, int index );

    /* Shaders */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidShaderIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2Shader*                 sctiOpenGLES2ModuleGetShader( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetShader( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Shader* shader );
    void                                sctiOpenGLES2ModuleDeleteShader( SCTOpenGLES2ModuleContext* moduleContext, int index );

    /* Programs */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidProgramIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2Program*                sctiOpenGLES2ModuleGetProgram( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetProgram( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Program* program );
    void                                sctiOpenGLES2ModuleDeleteProgram( SCTOpenGLES2ModuleContext* moduleContext, int index );

    /* Register */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidRegisterIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    double                              sctiOpenGLES2ModuleGetRegisterValue( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetRegisterValue( SCTOpenGLES2ModuleContext* moduleContext, int index, double value );

    /* Datas */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidDataIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2Data*                   sctiOpenGLES2ModuleGetData( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetData( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Data* data );
    void                                sctiOpenGLES2ModuleDeleteData( SCTOpenGLES2ModuleContext* moduleContext, int index );

    /* Meshes */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidMeshIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2Mesh*                   sctiOpenGLES2ModuleGetMesh( SCTOpenGLES2ModuleContext* moduleContext, int index );
    void                                sctiOpenGLES2ModuleSetMesh( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Mesh* mesh );
    void                                sctiOpenGLES2ModuleDeleteMesh( SCTOpenGLES2ModuleContext* moduleContext, int index );

    /* Flags */
    SCTBoolean                          sctiOpenGLES2ModuleIsValidFlagIndex( SCTOpenGLES2ModuleContext* moduleContext, int index );
    SCTOpenGLES2Flag*                   sctiOpenGLES2ModuleGetFlag( SCTOpenGLES2ModuleContext* moduleContext, int index );
    
    /* Clear error */
    const char*                         sctiOpenGLES2GetErrorString( int err );
    void                                sctiOpenGLES2ClearGLError( void );
    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_OPENGLES2MODULE_H__ ) */
