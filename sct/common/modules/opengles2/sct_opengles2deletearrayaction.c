/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2deletearrayaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2DeleteArrayActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2DeleteArrayActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2DeleteArrayActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2DeleteArrayActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DeleteArray@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2DeleteArrayActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2DeleteArrayActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DeleteArrayActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DeleteArrayActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in DeleteArray@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2DeleteArrayActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2DeleteArrayActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2DeleteArrayActionContext*   context;
    OpenGLES2DeleteArrayActionData*         data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2DeleteArrayActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetArray( context->moduleContext, data->arrayIndex ) == NULL )
    {
        SCT_LOG_ERROR( "Array index already used in DeleteArray@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES2ModuleDeleteArray( context->moduleContext, data->arrayIndex );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2DeleteArrayActionDestroy( SCTAction* action )
{
    SCTOpenGLES2DeleteArrayActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2DeleteArrayActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2DeleteArrayActionContext( context );
}
