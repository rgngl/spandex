#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
# MODULE NAME
SetModuleName( 'OpenGLES2' )

######################################################################
# INCLUDES FOR THE PARSER
AddInclude( 'sct_opengles2module.h' )
AddInclude( 'sct_gl2.h' )
AddInclude( 'sct_opengles2utils.h' )

######################################################################
# LIMITS: see sct_opengles2module.h for corresponding defines in
# c-side
MAX_TEXTURE_DATA_INDEX                  = 4096
MAX_COMPRESSED_TEXTURE_DATA_INDEX       = 4096
MAX_TEXTURE_INDEX                       = 4096
MAX_ARRAY_INDEX                         = 4096
MAX_BUFFER_INDEX                        = 4096
MAX_FRAMEBUFFER_INDEX                   = 4096
MAX_RENDERBUFFER_INDEX                  = 4096
MAX_SHADER_INDEX                        = 4096
MAX_PROGRAM_INDEX                       = 4096
MAX_REGISTER_INDEX                      = 4096
MAX_DATA_INDEX                          = 4096
MAX_MESH_INDEX                          = 4096
MAX_VARIABLE_LENGTH                     = 256
MAX_FILENAME_LENGTH                     = 256
MAX_STRINGDATA_LENGTH                   = 8128
MAX_EXTENSION_NAME_LENGTH               = 512
MAX_FUNCTION_NAME_LENGTH                = 512
MAX_ERROR_MESSAGE_LENGTH                = 128
MAX_FLAG_INDEX                          = 8

######################################################################
########################### ARRAY ACTIONS ############################
######################################################################

######################################################################
# CheckExtension. Check the availability of a given OpenGLES2 extension. The
# action reports an error if the extension is not present.
AddActionConfig( 'CheckExtension',
                 'Execute'+'Destroy',
                 [ StringAttribute(             'Extension',                            MAX_EXTENSION_NAME_LENGTH )
                   ]
                 )

######################################################################
# CheckFunction. Check the availability of the given function. The action
# reports an error if the function is not present.
AddActionConfig( 'CheckFunction',
                 'Execute'+'Destroy',
                 [ StringAttribute(             'Function',                            MAX_FUNCTION_NAME_LENGTH )
                   ]
                 )

######################################################################
# CheckValue. Query OpenGLES2 value(s) and compare to input value(s) according
# to condition; <queried value> <condition> <value>. Values length must match
# the queried value length, e.g. GL_MAX_VIEWPORT_DIMS expects 2 values, both of
# which are compared using the same condition. SCT_FOUND condition can be used
# only with GL_COMPRESSED_TEXTURE_FORMATS and GL_SHADER_BINARY_FORMATS.
AddActionConfig( 'CheckValue',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Value', 'GLint',                       [ 'GL_SUBPIXEL_BITS',
                                                                                          'GL_MAX_TEXTURE_SIZE',
                                                                                          'GL_MAX_CUBE_MAP_TEXTURE_SIZE',
                                                                                          'GL_MAX_VIEWPORT_DIMS',
                                                                                          'GL_ALIASED_POINT_SIZE_RANGE',
                                                                                          'GL_ALIASED_LINE_WIDTH_RANGE',
                                                                                          'GL_SAMPLE_BUFFERS',
                                                                                          'GL_SAMPLES',
                                                                                          'GL_COMPRESSED_TEXTURE_FORMATS',
                                                                                          'GL_NUM_COMPRESSED_TEXTURE_FORMATS',
                                                                                          'GL_SHADER_BINARY_FORMATS',
                                                                                          'GL_NUM_SHADER_BINARY_FORMATS',
                                                                                          'GL_SHADER_COMPILER',
                                                                                          'GL_MAX_VERTEX_ATTRIBS',
                                                                                          'GL_MAX_VERTEX_UNIFORM_VECTORS',
                                                                                          'GL_MAX_VARYING_VECTORS',
                                                                                          'GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS',
                                                                                          'GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS',
                                                                                          'GL_MAX_TEXTURE_IMAGE_UNITS',
                                                                                          'GL_MAX_FRAGMENT_UNIFORM_VECTORS',
                                                                                          'GL_MAX_RENDERBUFFER_SIZE',
                                                                                          'GL_RED_BITS',
                                                                                          'GL_GREEN_BITS',
                                                                                          'GL_BLUE_BITS',
                                                                                          'GL_ALPHA_BITS',
                                                                                          'GL_DEPTH_BITS',
                                                                                          'GL_STENCIL_BITS',
                                                                                          'GL_IMPLEMENTATION_COLOR_READ_TYPE',
                                                                                          'GL_IMPLEMENTATION_COLOR_READ_FORMAT' ] ),
                   EnumAttribute(               'Condition', 'SCTCondition',            [ 'SCT_LESS',
                                                                                          'SCT_LEQUAL',
                                                                                          'SCT_GREATER',
                                                                                          'SCT_GEQUAL',
                                                                                          'SCT_EQUAL',
                                                                                          'SCT_NOTEQUAL',
                                                                                          'SCT_FOUND' ] ),
                   FloatVectorAttribute(        'Values' ),
                   ]
                 )

################################################################################
# Check error. Message can be used to detect the specific action which failed
# the error check.
AddActionConfig( 'CheckError',
                 'Execute'+'Destroy',
                 [ StringAttribute(             'Message',                              MAX_ERROR_MESSAGE_LENGTH )
                   ],
                 )

######################################################################
# CreateArray. Create an empty array with a given type to the specified array
# index. The action fails if the array index is already in use. Arrays are used
# to hold data which is used for example to define vertex attribute data in the
# VertexAttribPointer action.
AddActionConfig( 'CreateArray',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   EnumAttribute(               'Type', 'GLint',                        [ 'GL_BYTE',
                                                                                          'GL_UNSIGNED_BYTE',
                                                                                          'GL_SHORT',
                                                                                          'GL_UNSIGNED_SHORT',
                                                                                          'GL_FIXED',
                                                                                          'GL_FLOAT',
                                                                                          'GL_HALF_FLOAT_OES' ] )
                   ]
                 )

######################################################################
# AppendToArray. Append data to the array specified by the array index. The
# array index must refer to a valid array. Data is given using a vector of
# double precision floating point values but it is converted into the array type
# internally. The array grows as needed; NOTE that this means the array should
# be used f.ex. as vertex array only after all the data has been appended.
AddActionConfig( 'AppendToArray',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   DoubleVectorAttribute(       'Data' )
                   ]
                 )

######################################################################
# AppendDataToArray. Append raw data specified by the data index to the array
# specified by the array index. Raw data is for example loaded from a binary
# file using the LoadData action. Data is appended as is and interpreted as the
# array type when used. The array grows as needed; NOTE that this means the
# array should be used f.ex. as vertex array only after all the data has been
# appended.
AddActionConfig( 'AppendDataToArray',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 )
                   ]
                 )

######################################################################
# ClearArray. Clear the array specified by the array index. Clearing deallocates
# the memory reserved for the array data.
AddActionConfig( 'ClearArray',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 )
                   ]
                 )

######################################################################
# DeleteArray. Delete the array specified by the array index. The array index is
# released and can be subsequently used in the CreateArray action.
AddActionConfig( 'DeleteArray',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 )
                   ]
                 )

######################################################################
############################## REGISTER ##############################
######################################################################

######################################################################
# SetRegister. Set a value to the register specified by the register
# index. Registers are used to specify vertex attribute and uniform variable
# indices in the shader programs.
AddActionConfig( 'SetRegister',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   FloatAttribute(              'Value',                                0 )
                   ]
                 )

######################################################################
# AddRegister. Add a value to the register specified by the register
# index.
AddActionConfig( 'AddRegister',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   FloatAttribute(              'Value' )
                   ]
                 )

######################################################################
# AccumulateRegister. Add a value to the register specified by the register
# index. Resets value ResetValue when the ResetLimit is reached; register value
# > ResetLimit when value > 0, register value < ResetLimit when value <
# 0.
AddActionConfig( 'AccumulateRegister',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   FloatAttribute(              'Value' ),
                   FloatAttribute(              'ResetLimit' ),
                   FloatAttribute(              'ResetValue' ),
                   ]
                 )

######################################################################
############################### DATA #################################
######################################################################

######################################################################
# CreateData. Create empty data with the given length to the specified data
# index. The action fails if the data index is already in use and the existing
# data does not have exactly the specified length. Data is used for example to
# load texture and vertex data from a file.
AddActionConfig( 'CreateData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'Length',                               1 )
                   ]
                 )

######################################################################
# LoadData. Load data from the file specified by the file name to the data
# specified by the data index. Creates a new data with the length matching that
# of the file. If the data already exists in the data index, it is reused if the
# length of the existing data and the length of the file match. Otherwise, the
# action reports an error.
AddActionConfig( 'LoadData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   StringAttribute(             'Filename',                             MAX_FILENAME_LENGTH )
                   ]
                 )

######################################################################
# SetData. Set data from the string to the data specified by the data
# index. Creates a new data with the length matching that of the string. If the
# data already exists in the data index, it is reused if the length of the
# existing data and the length of the string match. Otherwise, the action
# reports an error.
AddActionConfig( 'SetData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   StringAttribute(             'Data',                                 MAX_STRINGDATA_LENGTH )
                   ]
                 )

######################################################################
# Decode. Decode data stored into the source data index. The source data must
# follow the file format used by the selected encoder. The destination data
# index must be free, or the data length must be exactly the same as the data
# payload in the data.
AddActionConfig( 'Decode',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   AutoEnumAttribute(           'Encoder',                             [ 'PVRTEXTOOL3' ] ),
                   ]
                 )

######################################################################
# DecodeATC. Decode ATI texture compressed (ATI_TC_RGB, ATI_TC_RGBA) data stored
# into the source data index. The compressed data must follow the structure used
# by the ATI compressonator utility. The destination data index must be free, or
# the data length must be exactly the same as the data payload in the compressed
# data. The resulting data can be used in the CreateCompressedTextureData action
# with the internal type GL_ATC_RGB_AMD or GL_ATC_RGBA_AMD.
AddActionConfig( 'DecodeATC',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   ]
                 )

######################################################################
# DecodeETC. Decode ETC texture compressed (GL_ETC1_RGB8_OES) data stored into
# the source data index. The source data must follow the file format used by the
# selected encoder: ATI corresponds ATI compressonator utility, ERICSSON
# corresponds Ericsson etcpack. The destination data index must be free, or the
# data length must be exactly the same as the data payload in the compressed
# data. The resulting data can be used in the CreateCompressedTextureData action
# with the internal type GL_ETC1_RGB8_OES.
AddActionConfig( 'DecodeETC',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   AutoEnumAttribute(           'Encoder',                             [ 'ATI',
                                                                                         'ERICSSON' ] ),
                   ]
                 )

######################################################################
# DecodeTarga. Decode targa data (24bpp RGB, 32bpp RGBA). The destination data
# index must be free, or the data length must be exactly the same as the data
# payload in the targa data. The resulting data can be used in the
# CreateTextureData action.
AddActionConfig( 'DecodeTarga',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   ]
                 )

######################################################################
# DeleteData. Delete the data specified by the data index, and set the
# the data index as free.
AddActionConfig( 'DeleteData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 )
                   ]
                 )

######################################################################
######################### TEXTURE DATA ###############################
######################################################################

######################################################################
# CreateTextureData. Create texture data to the specified texture data index
# from the raw pixel data specified by the data index. The texture data is
# specified for mipmap level 0 only. The texture data index must be unused, or
# alternatively, it must contain texture data with similar width, height, and
# type. The texture data can be used subsequently in the TexImage2D and
# TexSubImage2D actions.
AddActionConfig( 'CreateTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   EnumAttribute(               'Type', 'SCTOpenGLES2TextureType',      [ 'OPENGLES2_LUMINANCE8',
                                                                                          'OPENGLES2_ALPHA8',
                                                                                          'OPENGLES2_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES2_RGB565',
                                                                                          'OPENGLES2_RGB888',
                                                                                          'OPENGLES2_RGBA4444',
                                                                                          'OPENGLES2_RGBA5551',
                                                                                          'OPENGLES2_RGBA8888',
                                                                                          'OPENGLES2_BGRA8888',
                                                                                          'OPENGLES2_DEPTH',
                                                                                          'OPENGLES2_DEPTH32',
                                                                                          'OPENGLES2_DEPTH24_STENCIL8' ] )
                   ]
                 )

######################################################################
# CreateNullTextureData. Create texture data to the specified texture data index
# with unspecified (NULL) pixel data. The texture data index must be unused. The
# texture data can be used subsequently in the TexImage2D and TexSubImage2D
# actions.
AddActionConfig( 'CreateNullTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_INDEX - 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES2TextureType',      [ 'OPENGLES2_LUMINANCE8',
                                                                                          'OPENGLES2_ALPHA8',
                                                                                          'OPENGLES2_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES2_RGB565',
                                                                                          'OPENGLES2_RGB888',
                                                                                          'OPENGLES2_RGBA4444',
                                                                                          'OPENGLES2_RGBA5551',
                                                                                          'OPENGLES2_RGBA8888',
                                                                                          'OPENGLES2_DEPTH',
                                                                                          'OPENGLES2_DEPTH32',
                                                                                          'OPENGLES2_DEPTH24_STENCIL8' ] )
                   ]
                 )

######################################################################
# CreateSolidTextureData. Create solid color texture data to the specified
# texture data index. The texture data index must be unused. The texture data
# can be used subsequently in the TexImage2D and TexSubImage2D actions.
AddActionConfig( 'CreateSolidTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   FloatArrayAttribute(         'Color',                                4, 0, 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES2TextureType',      [ 'OPENGLES2_LUMINANCE8',
                                                                                          'OPENGLES2_ALPHA8',
                                                                                          'OPENGLES2_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES2_RGB565',
                                                                                          'OPENGLES2_RGB888',
                                                                                          'OPENGLES2_RGBA4444',
                                                                                          'OPENGLES2_RGBA5551',
                                                                                          'OPENGLES2_RGBA8888',
                                                                                          'OPENGLES2_BGRA8888',
                                                                                          'OPENGLES2_DEPTH',
                                                                                          'OPENGLES2_DEPTH32',
                                                                                          'OPENGLES2_DEPTH24_STENCIL8' ] )
                   ]
                 )

######################################################################
# CreateBarTextureData. Create texture data with a vertical bar pattern to the
# specified texture data index. The texture data index must be unused. The
# texture data can be used subsequently in the TexImage2D and TexSubImage2D
# actions.
AddActionConfig( 'CreateBarTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   FloatArrayAttribute(         'Color1',                               4, 0, 1 ),
                   FloatArrayAttribute(         'Color2',                               4, 0, 1 ),
                   IntAttribute(                'Bars',                                 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES2TextureType',      [ 'OPENGLES2_LUMINANCE8',
                                                                                          'OPENGLES2_ALPHA8',
                                                                                          'OPENGLES2_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES2_RGB565',
                                                                                          'OPENGLES2_RGB888',
                                                                                          'OPENGLES2_RGBA4444',
                                                                                          'OPENGLES2_RGBA5551',
                                                                                          'OPENGLES2_RGBA8888',
                                                                                          'OPENGLES2_BGRA8888',
                                                                                          'OPENGLES2_DEPTH',
                                                                                          'OPENGLES2_DEPTH32',
                                                                                          'OPENGLES2_DEPTH24_STENCIL8' ] )
                   ]
                 )

######################################################################
# CreateCheckerTextureData. Create texture data with a checker pattern to the
# specified texture data index. The texture data index must be unused. The
# texture data can be used subsequently in the TexImage2D and TexSubImage2D
# actions.
AddActionConfig( 'CreateCheckerTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   FloatArrayAttribute(         'Color1',                               4, 0, 1 ),
                   FloatArrayAttribute(         'Color2',                               4, 0, 1 ),
                   IntAttribute(                'CheckersX',                            1 ),
                   IntAttribute(                'CheckersY',                            1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES2TextureType',      [ 'OPENGLES2_LUMINANCE8',
                                                                                          'OPENGLES2_ALPHA8',
                                                                                          'OPENGLES2_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES2_RGB565',
                                                                                          'OPENGLES2_RGB888',
                                                                                          'OPENGLES2_RGBA4444',
                                                                                          'OPENGLES2_RGBA5551',
                                                                                          'OPENGLES2_RGBA8888',
                                                                                          'OPENGLES2_BGRA8888',
                                                                                          'OPENGLES2_DEPTH',
                                                                                          'OPENGLES2_DEPTH32',
                                                                                          'OPENGLES2_DEPTH24_STENCIL8' ] )
                   ]
                 )

######################################################################
# CreateGradientTextureData. Create texture data with a vertical (up-down),
# horizontal (left-right) or diagonal (up-left-low-right) gradient to the
# specified texture data index. Gradient directions are described as the data is
# laid out in memory. The texture data index must be unused. The texture data
# can be used subsequently in the TexImage2D and TexSubImage2D actions.
AddActionConfig( 'CreateGradientTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Gradient', 'SCTOpenGLES2Gradient',     [ 'OPENGLES2_GRADIENT_VERTICAL',
                                                                                          'OPENGLES2_GRADIENT_HORIZONTAL',
                                                                                          'OPENGLES2_GRADIENT_DIAGONAL' ] ),
                   FloatArrayAttribute(         'Color1',                               4, 0, 1 ),
                   FloatArrayAttribute(         'Color2',                               4, 0, 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES2TextureType',      [ 'OPENGLES2_LUMINANCE8',
                                                                                          'OPENGLES2_ALPHA8',
                                                                                          'OPENGLES2_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES2_RGB565',
                                                                                          'OPENGLES2_RGB888',
                                                                                          'OPENGLES2_RGBA4444',
                                                                                          'OPENGLES2_RGBA5551',
                                                                                          'OPENGLES2_RGBA8888',
                                                                                          'OPENGLES2_BGRA8888',
                                                                                          'OPENGLES2_DEPTH',
                                                                                          'OPENGLES2_DEPTH32',
                                                                                          'OPENGLES2_DEPTH24_STENCIL8' ] )
                   ]
                 )

######################################################################
# CreateShapeTextureData. Create texture data with a desired shape to the
# specified texture data index. The texture data index must be unused. The
# texture data can be used subsequently in the TexImage2D and TexSubImage2D
# actions. Shape-specific pParameters are as follows.
#
# OPENGLES2_SHAPE_ROUND: {r1,g1,b1,a1,r2,g2,b2,a2,size}. Color1 specifies the
# inner color. Color2 specifies the outer color. Size specifies the square
# texture size.
#
AddActionConfig( 'CreateShapeTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Shape', 'SCTOpenGLES2Shape',           [ 'OPENGLES2_SHAPE_ROUND' ] ),
                   FloatVectorAttribute(        'Parameters' ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES2TextureType',      [ 'OPENGLES2_LUMINANCE8',
                                                                                          'OPENGLES2_ALPHA8',
                                                                                          'OPENGLES2_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES2_RGB565',
                                                                                          'OPENGLES2_RGB888',
                                                                                          'OPENGLES2_RGBA4444',
                                                                                          'OPENGLES2_RGBA5551',
                                                                                          'OPENGLES2_RGBA8888',
                                                                                          'OPENGLES2_BGRA8888',
                                                                                          'OPENGLES2_DEPTH',
                                                                                          'OPENGLES2_DEPTH32',
                                                                                          'OPENGLES2_DEPTH24_STENCIL8' ] )
                   ]
                 )

######################################################################
# CreateNoiseTextureData. Create noise texture data. The texture data index must
# be unused. The texture data can be used subsequently in the TexImage2D and
# TexSubImage2D actions. Noise parameters are as follows.
#
# OPENGLES2_NOISE_RANDOM: {r,g,b,a,separated,seed}. r,g,b,a defines the color
# which is modulated with the [0,1] noise value. Use non-zero separated value to
# use a different random value per rgba channel. Use seed to define how many
# random values should be skipped before they are used for the noise texture.
#
# OPENGLES2_NOISE_ORIGINAL_PERLIN: {r,g,b,a,alpha,beta,n}. See
# http://mrl.nyu.edu/~perlin/doc/oscar.html#noise. Sensible parameters are
# 2,2,8.
#
# OPENGLES2_NOISE_LOW/HIGH_QUALITY_PERLIN:
# {r,g,b,a,frequency,persistence,octaves,amplitude,materialCoverage,materialDensity}. See
# http://www.dreamincode.net/forums/topic/66480-perlin-noise/. Sensible
# parameters 0.01,0.75,8,1,1,0.5.
#
# Note that noise functions always generate the same output from the same
# input. In addition, large perlin noise texture data might be very slow to
# generate.
#
AddActionConfig( 'CreateNoiseTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Noise', 'SCTOpenGLES2Noise',           [ 'OPENGLES2_NOISE_RANDOM',
                                                                                          'OPENGLES2_NOISE_ORIGINAL_PERLIN',
                                                                                          'OPENGLES2_NOISE_LOW_QUALITY_PERLIN',
                                                                                          'OPENGLES2_NOISE_HIGH_QUALITY_PERLIN' ] ),
                   FloatVectorAttribute(        'Parameters' ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES2TextureType',      [ 'OPENGLES2_LUMINANCE8',
                                                                                          'OPENGLES2_ALPHA8',
                                                                                          'OPENGLES2_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES2_RGB565',
                                                                                          'OPENGLES2_RGB888',
                                                                                          'OPENGLES2_RGBA4444',
                                                                                          'OPENGLES2_RGBA5551',
                                                                                          'OPENGLES2_RGBA8888',
                                                                                          'OPENGLES2_BGRA8888',
                                                                                          'OPENGLES2_DEPTH',
                                                                                          'OPENGLES2_DEPTH32',
                                                                                          'OPENGLES2_DEPTH24_STENCIL8' ] )
                   ]
                 )

######################################################################
# DeleteTextureData. Delete the texture data specified by the texture data
# index, and set the texture data index as free.
AddActionConfig( 'DeleteTextureData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 )
                   ]
                 )

######################################################################
# CreateCompressedTextureData. Create compressed texture data to the compressed
# texture data index from the data specified by the data index. The compressed
# texture data index must be unused, or alternatively, it must contain
# compressed texture data with similar width, height, and internal format. The
# compressed texture data can be used subsequently in the CompressedTexImage2D
# and CompressedTexSubImage2D actions.
AddActionConfig( 'CreateCompressedTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   EnumAttribute(               'InternalFormat', 'GLenum',             [ 'GL_ETC1_RGB8_OES',
                                                                                          'GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG',
                                                                                          'GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG',
                                                                                          'GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG',
                                                                                          'GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG',
                                                                                          'GL_ATC_RGB_AMD',
                                                                                          'GL_ATC_RGBA_AMD' ] )
                   ]
                 )

######################################################################
# CreatePaletteCompressedTextureData. Create palette compressed texture data
# from an existing texture data. Existing texture data must be in the same
# format as the elements in the palette compressed data.
AddActionConfig( 'CreatePaletteCompressedTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 ),
                   IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_INDEX - 1 ),
                   EnumAttribute(               'Type', 'GLenum',                       [ 'GL_PALETTE4_RGB8_OES',
                                                                                          'GL_PALETTE4_RGBA8_OES',
                                                                                          'GL_PALETTE4_R5_G6_B5_OES',
                                                                                          'GL_PALETTE4_RGBA4_OES',
                                                                                          'GL_PALETTE4_RGB5_A1_OES',
                                                                                          'GL_PALETTE8_RGB8_OES',
                                                                                          'GL_PALETTE8_RGBA8_OES',
                                                                                          'GL_PALETTE8_R5_G6_B5_OES',
                                                                                          'GL_PALETTE8_RGBA4_OES',
                                                                                          'GL_PALETTE8_RGB5_A1_OES' ] )
                   ]
                 )

######################################################################
# DeleteCompressedTextureData. Delete the compressed texture data specified by
# the compressed texture data index, and set the compressed texture data index
# as free.
AddActionConfig( 'DeleteCompressedTextureData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 )
                   ]
                 )

######################################################################
######################################################################
######################################################################
##################### OPENGL ES2 ACTIONS #############################
######################################################################
######################################################################
######################################################################

######################################################################
# VertexAttrib. Set the vertex attribute values for the vertex attribute
# index. The vertex attribute index is specified by the value stored in the
# register specified by the register index. The values vector can define from
# one to four floating point values.
AddActionConfig( 'VertexAttrib',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   FloatVectorAttribute(        'Values' )
                   ],
                 [ ValidatorConfig( 'Values->length != 1 && Values->length != 2 && Values->length != 3 && Values->length != 4',
                                    'VertexAttrib@OpenGLES2 invalid values length' ) ]

                 )

######################################################################
# VertexAttribPointer. Set the vertex attribute pointer for the vertex attribute
# index to point to the array indicated by the array index. The vertex attribute
# index is specified by the value stored in the register specified by the
# register index. Size defines the number of array elements per vertex, and
# normalized specifies whether or not the array data is normalized to the array
# type range. Offset can be used to select the starting element within the
# array.
AddActionConfig( 'VertexAttribPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'Size',                                 1 ),
                   OnOffAttribute(              'Normalized' ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# BufferVertexAttribPointer. Binds the buffer to GL_ARRAY_BUFFER target (if bind
# is specified) and sets the vertex attribute pointer for the vertex attribute
# index to point to buffer data. The vertex attribute index is specified by the
# value stored in the register specified by the register index. Size defines the
# number of array elements per vertex, and normalized specifies whether or not
# the array data is normalized to the array type range. Offset can be used to
# select the starting element within the array.
AddActionConfig( 'BufferVertexAttribPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   IntAttribute(                'Size',                                 1 ),
                   OnOffAttribute(              'Normalized' ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# DrawArrays. Draw graphics primitives specified by mode. First specifies the
# first and count specifies the number of vertex attributes to source from the
# enabled vertex attribute arrays.
AddActionConfig( 'DrawArrays',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_POINTS',
                                                                                          'GL_LINE_STRIP',
                                                                                          'GL_LINE_LOOP',
                                                                                          'GL_LINES',
                                                                                          'GL_TRIANGLE_STRIP',
                                                                                          'GL_TRIANGLE_FAN',
                                                                                          'GL_TRIANGLES' ] ),
                   IntAttribute(                'First',                                0 ),
                   IntAttribute(                'Count',                                0 )
                   ]
                 )

######################################################################
# DrawElements. Draw graphics primitives specified by mode. Count specifies the
# number of vertex attributes to source from the enabled vertex attribute
# arrays. The array data stored in the array index specifies the indices for the
# attribute array elements to source. Array data starts from the element
# specified by offset.
AddActionConfig( 'DrawElements',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_POINTS',
                                                                                          'GL_LINE_STRIP',
                                                                                          'GL_LINE_LOOP',
                                                                                          'GL_LINES',
                                                                                          'GL_TRIANGLE_STRIP',
                                                                                          'GL_TRIANGLE_FAN',
                                                                                          'GL_TRIANGLES' ] ),
                   IntAttribute(                'Count',                                0 ),
                   IntAttribute(                'Offset',                               0 ),
                   IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 )
                   ]
                 )

######################################################################
# BufferDrawElements. Binds the buffer to GL_ELEMENT_ARRAY_BUFFER target (if
# bind is specified) and draws graphics primitives specified by mode. Count
# number of vertex attributes are sourced from the enabled vertex attribute
# arrays. Array data within the buffer starts from the element specified by
# offset.
AddActionConfig( 'BufferDrawElements',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_POINTS',
                                                                                          'GL_LINE_STRIP',
                                                                                          'GL_LINE_LOOP',
                                                                                          'GL_LINES',
                                                                                          'GL_TRIANGLE_STRIP',
                                                                                          'GL_TRIANGLE_FAN',
                                                                                          'GL_TRIANGLES' ] ),
                   IntAttribute(                'Count',                                0 ),
                   IntAttribute(                'Offset',                               0 )
                   ]
                 )

######################################################################
# EnableVertexAttribArray. Enable a vertex attribute array. The vertex attribute
# index is specified by the value stored in the register specified by the
# register index.
AddActionConfig( 'EnableVertexAttribArray',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 )
                   ]
                 )

######################################################################
# DisableVertexAttribArray. Disable a vertex attribute array. The vertex
# attribute index is specified by the value stored in the register specified by
# the register index.
AddActionConfig( 'DisableVertexAttribArray',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 )
                   ]
                 )

######################################################################
# BindBuffer. Bind a buffer. The buffer to bind is specified by the buffer
# index. A new buffer is created if the buffer index does not refer to an
# existing buffer. A default OpenGLES2 buffer (0) is bound if the buffer index
# is -1.
AddActionConfig( 'BindBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_ARRAY_BUFFER',
                                                                                          'GL_ELEMENT_ARRAY_BUFFER' ] ),
                   IntAttribute(                'BufferIndex',                          -1, MAX_BUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# DeleteBuffer. Delete a buffer. The buffer to delete is specified by the buffer
# index. The buffer must exist.
AddActionConfig( 'DeleteBuffer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# GenBuffer. Create a new buffer to the buffer index. The buffer index must be
# free.
AddActionConfig( 'GenBuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# BufferData. Binds buffer to given target (if bind is specified) and sets
# buffer data. The data is taken from the array specified by the array
# index. Offset and size can be used to load a subset of the array data to the
# buffer. Both offset and size are given as array elements. The size is expanded
# to the array length when set to 0.
AddActionConfig( 'BufferData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_ARRAY_BUFFER',
                                                                                          'GL_ELEMENT_ARRAY_BUFFER' ] ),
                   EnumAttribute(               'Usage', 'GLenum',                      [ 'GL_STATIC_DRAW',
                                                                                          'GL_DYNAMIC_DRAW',
                                                                                          'GL_STREAM_DRAW' ] ),
                   IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'Offset',                               0 ),
                   IntAttribute(                'Size',                                 0 )
                   ]
                 )

######################################################################
# BufferSubData. Binds buffer to given target (if bind is specified) and sets
# buffer sub data. The data is taken from the array specified by the array
# index. Array offset and size can be used to load a subset of the array data to
# the buffer. Both array offset and size are given as array elements. The size
# is expanded to the array length when set to 0. The type of the array must
# match the array type previously used to set the buffer data.
AddActionConfig( 'BufferSubData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_ARRAY_BUFFER',
                                                                                          'GL_ELEMENT_ARRAY_BUFFER' ] ),
                   IntAttribute(                'Offset',                               0 ),
                   IntAttribute(                'ArrayIndex',                           0, MAX_ARRAY_INDEX - 1 ),
                   IntAttribute(                'ArrayOffset',                          0 ),
                   IntAttribute(                'Size',                                 0 )
                   ]
                 )

######################################################################
# DepthRange. Set depth range parameters.
AddActionConfig( 'DepthRange',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'NearPlane' ),
                   FloatAttribute(              'FarPlane' )
                   ]
                 )

######################################################################
# ViewPort. Set viewport parameters.
AddActionConfig( 'Viewport',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'X' ),
                   IntAttribute(                'Y' ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 )
                   ]
                 )

######################################################################
# ActiveTexture. Set currently active texture unit.
AddActionConfig( 'ActiveTexture',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Unit',                                 0 )
                   ]
                 )

######################################################################
# FrontFace. Set winding rule for defining the polygon front face.
AddActionConfig( 'FrontFace',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_CW',
                                                                                          'GL_CCW' ] )
                   ]
                 )

######################################################################
# LineWidth. Set line width.
AddActionConfig( 'LineWidth',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'Width' )
                   ]
                 )

######################################################################
# CullFace. Set polygon cull face rule.
AddActionConfig( 'CullFace',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_FRONT',
                                                                                          'GL_BACK',
                                                                                          'GL_FRONT_AND_BACK' ] )
                   ]
                 )

######################################################################
# PolygonOffset. Set polygon offset parameters.
AddActionConfig( 'PolygonOffset',
                 'Execute'+'Destroy',
                 [ FloatAttribute(               'Factor' ),
                   FloatAttribute(               'Units' )
                   ]
                 )

######################################################################
# Enable. Enable state.
AddActionConfig( 'Enable',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Cap', 'GLenum',                        [ 'GL_CULL_FACE',
                                                                                          'GL_POLYGON_OFFSET_FILL',
                                                                                          'GL_SCISSOR_TEST',
                                                                                          'GL_SAMPLE_COVERAGE',
                                                                                          'GL_STENCIL_TEST',
                                                                                          'GL_DEPTH_TEST',
                                                                                          'GL_BLEND',
                                                                                          'GL_DITHER' ] )
                   ]
                 )

######################################################################
# Disable. Disable state.
AddActionConfig( 'Disable',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Cap', 'GLenum',                        [ 'GL_CULL_FACE',
                                                                                          'GL_POLYGON_OFFSET_FILL',
                                                                                          'GL_SCISSOR_TEST',
                                                                                          'GL_SAMPLE_COVERAGE',
                                                                                          'GL_STENCIL_TEST',
                                                                                          'GL_DEPTH_TEST',
                                                                                          'GL_BLEND',
                                                                                          'GL_DITHER' ] )
                   ]
                 )

######################################################################
# Flush. Flush submitted drawing commands.
AddActionConfig( 'Flush',
                 'Execute'+'Destroy',
                 [ ]
                 )

######################################################################
# Finish. Wait for submitted drawing commands to finish.
AddActionConfig( 'Finish',
                 'Execute'+'Destroy',
                 [ ]
                 )

######################################################################
# Hint. Set hint.
AddActionConfig( 'Hint',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_GENERATE_MIPMAP_HINT' ] ),
                   EnumAttribute(               'Mode', 'GLenum',                       [ 'GL_FASTEST',
                                                                                          'GL_NICEST',
                                                                                          'GL_DONT_CARE' ] )
                   ]
                 )

######################################################################
# Info. Print information about the OpenGLES 2.0 implementation to the
# output. Output is usually the report file.
AddActionConfig( 'Info',
                 'Execute'+'Destroy',
                 []
                 )

######################################################################
# Scissor. Set scissor rectangle.
AddActionConfig( 'Scissor',
                 'Execute'+'Destroy',
                [ IntAttribute(                 'X' ),
                  IntAttribute(                 'Y' ),
                  IntAttribute(                 'Width',                                0 ),
                  IntAttribute(                 'Height',                               0 )
                  ]
                 )

######################################################################
# SampleCoverage. Set sample coverage parameters.
AddActionConfig( 'SampleCoverage',
                 'Execute'+'Destroy',
                [ FloatAttribute(               'Value' ),
                  OnOffAttribute(               'Invert' )
                  ]
                 )

######################################################################
# StencilFunc. Set stencil function parameters.
AddActionConfig( 'StencilFunc',
                 'Execute'+'Destroy',
                [ EnumAttribute(                'Func', 'GLenum',                       [ 'GL_NEVER',
                                                                                          'GL_LESS',
                                                                                          'GL_LEQUAL',
                                                                                          'GL_GREATER',
                                                                                          'GL_GEQUAL',
                                                                                          'GL_EQUAL',
                                                                                          'GL_NOTEQUAL',
                                                                                          'GL_ALWAYS' ] ),
                  IntAttribute(                 'Ref' ),
                  Hex32Attribute(               'Mask' )
                  ]
                 )

######################################################################
# StencilFuncSeparate. Set stencil function parameters for the specific polygon
# face.
AddActionConfig( 'StencilFuncSeparate',
                 'Execute'+'Destroy',
                [ EnumAttribute(                'Face', 'GLenum',                       [ 'GL_FRONT',
                                                                                          'GL_BACK',
                                                                                          'GL_FRONT_AND_BACK' ] ),
                  EnumAttribute(                'Func', 'GLenum',                       [ 'GL_NEVER',
                                                                                          'GL_LESS',
                                                                                          'GL_LEQUAL',
                                                                                          'GL_GREATER',
                                                                                          'GL_GEQUAL',
                                                                                          'GL_EQUAL',
                                                                                          'GL_NOTEQUAL',
                                                                                          'GL_ALWAYS' ] ),
                  IntAttribute(                 'Ref' ),
                  Hex32Attribute(               'Mask' )
                  ]
                 )

######################################################################
# StencilOp. Set stencil operation parameters.
AddActionConfig( 'StencilOp',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Fail', 'GLenum',                       [ 'GL_KEEP',
                                                                                          'GL_ZERO',
                                                                                          'GL_REPLACE',
                                                                                          'GL_INCR',
                                                                                          'GL_DECR',
                                                                                          'GL_INVERT',
                                                                                          'GL_INCR_WRAP',
                                                                                          'GL_DECR_WRAP' ] ),
                   EnumAttribute(               'Zfail', 'GLenum',                      [ 'GL_KEEP',
                                                                                          'GL_ZERO',
                                                                                          'GL_REPLACE',
                                                                                          'GL_INCR',
                                                                                          'GL_DECR',
                                                                                          'GL_INVERT',
                                                                                          'GL_INCR_WRAP',
                                                                                          'GL_DECR_WRAP' ] ),
                   EnumAttribute(               'Zpass', 'GLenum',                      [ 'GL_KEEP',
                                                                                          'GL_ZERO',
                                                                                          'GL_REPLACE',
                                                                                          'GL_INCR',
                                                                                          'GL_DECR',
                                                                                          'GL_INVERT',
                                                                                          'GL_INCR_WRAP',
                                                                                          'GL_DECR_WRAP' ] )
                   ]
                 )

######################################################################
# StencilOpSeparate. Set stencil operation parameters for the specified polygon
# face.
AddActionConfig( 'StencilOpSeparate',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Face', 'GLenum',                       [ 'GL_FRONT',
                                                                                          'GL_BACK',
                                                                                          'GL_FRONT_AND_BACK' ] ),
                   EnumAttribute(               'Fail', 'GLenum',                       [ 'GL_KEEP',
                                                                                          'GL_ZERO',
                                                                                          'GL_REPLACE',
                                                                                          'GL_INCR',
                                                                                          'GL_DECR',
                                                                                          'GL_INVERT',
                                                                                          'GL_INCR_WRAP',
                                                                                          'GL_DECR_WRAP' ] ),
                   EnumAttribute(               'Zfail', 'GLenum',                      [ 'GL_KEEP',
                                                                                          'GL_ZERO',
                                                                                          'GL_REPLACE',
                                                                                          'GL_INCR',
                                                                                          'GL_DECR',
                                                                                          'GL_INVERT',
                                                                                          'GL_INCR_WRAP',
                                                                                          'GL_DECR_WRAP' ] ),
                   EnumAttribute(               'Zpass', 'GLenum',                      [ 'GL_KEEP',
                                                                                          'GL_ZERO',
                                                                                          'GL_REPLACE',
                                                                                          'GL_INCR',
                                                                                          'GL_DECR',
                                                                                          'GL_INVERT',
                                                                                          'GL_INCR_WRAP',
                                                                                          'GL_DECR_WRAP' ] )
                   ]
                 )

######################################################################
# DepthFunc. Set depth test function.
AddActionConfig( 'DepthFunc',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Func', 'GLenum',                       [ 'GL_NEVER',
                                                                                          'GL_LESS',
                                                                                          'GL_LEQUAL',
                                                                                          'GL_GREATER',
                                                                                          'GL_GEQUAL',
                                                                                          'GL_EQUAL',
                                                                                          'GL_NOTEQUAL',
                                                                                          'GL_ALWAYS' ] )
                   ]
                 )

######################################################################
# BlendFunc. Set blend function parameters.
AddActionConfig( 'BlendFunc',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Sfactor', 'GLenum',                    [ 'GL_ZERO',
                                                                                          'GL_ONE',
                                                                                          'GL_SRC_COLOR',
                                                                                          'GL_ONE_MINUS_SRC_COLOR',
                                                                                          'GL_DST_COLOR',
                                                                                          'GL_ONE_MINUS_DST_COLOR',
                                                                                          'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA',
                                                                                          'GL_DST_ALPHA',
                                                                                          'GL_ONE_MINUS_DST_ALPHA',
                                                                                          'GL_CONSTANT_COLOR',
                                                                                          'GL_ONE_MINUS_CONSTANT_COLOR',
                                                                                          'GL_CONSTANT_ALPHA',
                                                                                          'GL_ONE_MINUS_CONSTANT_ALPHA',
                                                                                          'GL_SRC_ALPHA_SATURATE' ] ),
                   EnumAttribute(               'Dfactor', 'GLenum',                    [ 'GL_ZERO',
                                                                                          'GL_ONE',
                                                                                          'GL_SRC_COLOR',
                                                                                          'GL_ONE_MINUS_SRC_COLOR',
                                                                                          'GL_DST_COLOR',
                                                                                          'GL_ONE_MINUS_DST_COLOR',
                                                                                          'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA',
                                                                                          'GL_DST_ALPHA',
                                                                                          'GL_ONE_MINUS_DST_ALPHA',
                                                                                          'GL_CONSTANT_COLOR',
                                                                                          'GL_ONE_MINUS_CONSTANT_COLOR',
                                                                                          'GL_CONSTANT_ALPHA',
                                                                                          'GL_ONE_MINUS_CONSTANT_ALPHA' ] ) ]
                 )

######################################################################
# BlendFuncSeparate. Set blend function parameters for the RGB and alpha
# channels.
AddActionConfig( 'BlendFuncSeparate',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'SrcRGB', 'GLenum',                     [ 'GL_ZERO',
                                                                                          'GL_ONE',
                                                                                          'GL_SRC_COLOR',
                                                                                          'GL_ONE_MINUS_SRC_COLOR',
                                                                                          'GL_DST_COLOR',
                                                                                          'GL_ONE_MINUS_DST_COLOR',
                                                                                          'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA',
                                                                                          'GL_DST_ALPHA',
                                                                                          'GL_ONE_MINUS_DST_ALPHA',
                                                                                          'GL_CONSTANT_COLOR',
                                                                                          'GL_ONE_MINUS_CONSTANT_COLOR',
                                                                                          'GL_CONSTANT_ALPHA',
                                                                                          'GL_ONE_MINUS_CONSTANT_ALPHA',
                                                                                          'GL_SRC_ALPHA_SATURATE' ] ),
                   EnumAttribute(               'DstRGB', 'GLenum',                     [ 'GL_ZERO',
                                                                                          'GL_ONE',
                                                                                          'GL_SRC_COLOR',
                                                                                          'GL_ONE_MINUS_SRC_COLOR',
                                                                                          'GL_DST_COLOR',
                                                                                          'GL_ONE_MINUS_DST_COLOR',
                                                                                          'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA',
                                                                                          'GL_DST_ALPHA',
                                                                                          'GL_ONE_MINUS_DST_ALPHA',
                                                                                          'GL_CONSTANT_COLOR',
                                                                                          'GL_ONE_MINUS_CONSTANT_COLOR',
                                                                                          'GL_CONSTANT_ALPHA',
                                                                                          'GL_ONE_MINUS_CONSTANT_ALPHA' ] ),
                   EnumAttribute(               'SrcAlpha', 'GLenum',                   [ 'GL_ZERO',
                                                                                          'GL_ONE',
                                                                                          'GL_SRC_COLOR',
                                                                                          'GL_ONE_MINUS_SRC_COLOR',
                                                                                          'GL_DST_COLOR',
                                                                                          'GL_ONE_MINUS_DST_COLOR',
                                                                                          'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA',
                                                                                          'GL_DST_ALPHA',
                                                                                          'GL_ONE_MINUS_DST_ALPHA',
                                                                                          'GL_CONSTANT_COLOR',
                                                                                          'GL_ONE_MINUS_CONSTANT_COLOR',
                                                                                          'GL_CONSTANT_ALPHA',
                                                                                          'GL_ONE_MINUS_CONSTANT_ALPHA',
                                                                                          'GL_SRC_ALPHA_SATURATE' ] ),
                   EnumAttribute(               'DstAlpha', 'GLenum',                   [ 'GL_ZERO',
                                                                                          'GL_ONE',
                                                                                          'GL_SRC_COLOR',
                                                                                          'GL_ONE_MINUS_SRC_COLOR',
                                                                                          'GL_DST_COLOR',
                                                                                          'GL_ONE_MINUS_DST_COLOR',
                                                                                          'GL_SRC_ALPHA',
                                                                                          'GL_ONE_MINUS_SRC_ALPHA',
                                                                                          'GL_DST_ALPHA',
                                                                                          'GL_ONE_MINUS_DST_ALPHA',
                                                                                          'GL_CONSTANT_COLOR',
                                                                                          'GL_ONE_MINUS_CONSTANT_COLOR',
                                                                                          'GL_CONSTANT_ALPHA',
                                                                                          'GL_ONE_MINUS_CONSTANT_ALPHA' ] ) ]
                 )

######################################################################
# BlendEquation. Set blend equation.
AddActionConfig( 'BlendEquation',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'GLenum',                        [ 'GL_FUNC_ADD',
                                                                                           'GL_FUNC_SUBTRACT',
                                                                                           'GL_FUNC_REVERSE_SUBTRACT' ] ) ]
                 )

######################################################################
# BlendEquationSeparate. Set blend equation for the RGB and alpha channels.
AddActionConfig( 'BlendEquationSeparate',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'ModeRGB', 'GLenum',                    [ 'GL_FUNC_ADD',
                                                                                          'GL_FUNC_SUBTRACT',
                                                                                          'GL_FUNC_REVERSE_SUBTRACT' ] ),
                   EnumAttribute(               'ModeAlpha', 'GLenum',                  [ 'GL_FUNC_ADD',
                                                                                          'GL_FUNC_SUBTRACT',
                                                                                          'GL_FUNC_REVERSE_SUBTRACT' ] ) ]
                 )

######################################################################
# BlendColor. Set blend color.
AddActionConfig( 'BlendColor',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Color',                                4 )
                   ]
                 )

######################################################################
# ColorMask. Set color mask.
AddActionConfig( 'ColorMask',
                 'Execute'+'Destroy',
                 [ OnOffAttribute(              'Red' ),
                   OnOffAttribute(              'Green' ),
                   OnOffAttribute(              'Blue' ),
                   OnOffAttribute(              'Alpha' )
                   ]
                 )

######################################################################
# Clear. Clear selected buffer(s).
AddActionConfig( 'Clear',
                 'Execute'+'Destroy',
                 [ EnumMaskAttribute(           'Mask', 'GLbitfield',                   [ 'GL_COLOR_BUFFER_BIT',
                                                                                          'GL_DEPTH_BUFFER_BIT',
                                                                                          'GL_STENCIL_BUFFER_BIT' ] )
                   ]
                 )

######################################################################
# ClearColor. Set clear color.
AddActionConfig( 'ClearColor',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Color',                                4 )
                   ]
                 )

######################################################################
# DepthMask. Set depth mask.
AddActionConfig( 'DepthMask',
                 'Execute'+'Destroy',
                 [ OnOffAttribute(              'Flag' )
                   ]
                 )

######################################################################
# ClearDepth. Set clear depth value.
AddActionConfig( 'ClearDepth',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'Depth' )
                   ]
                 )

######################################################################
# StencilMask. Set stencil mask.
AddActionConfig( 'StencilMask',
                 'Execute'+'Destroy',
                 [ Hex32Attribute(              'Mask' )
                   ]
                 )

######################################################################
# StencilMaskSeparate. Set stencil mask for the specified polygon
# face.
AddActionConfig( 'StencilMaskSeparate',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Face', 'GLenum',                       [ 'GL_FRONT',
                                                                                          'GL_BACK',
                                                                                          'GL_FRONT_AND_BACK' ] ),
                   Hex32Attribute(              'Mask' )
                   ]
                 )

######################################################################
# ClearStencil. Set clear stencil value.
AddActionConfig( 'ClearStencil',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'S' )
                   ]
                 )

######################################################################
# ReadPixels. Read RGBA pixels from the currently used framebuffer to the data
# referred by the data index. New data is created, if needed. The action creates
# an error if the existing data does not have the length of
# width*height*sizeof(RGBA8888).
AddActionConfig( 'ReadPixels',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

######################################################################
# GenTexture. Create a new texture to the texture index. The texture index must
# be free.
AddActionConfig( 'GenTexture',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureIndex',                         0, MAX_TEXTURE_INDEX - 1 )
                   ]
                 )

######################################################################
# TexImage2D. Set the texture data for the currently bound texture in the
# specified texture target. The texture data is referred by the texture data
# index. Mipmap levels are defined for the texture if the texture data contains
# mipmap data.
AddActionConfig( 'TexImage2D',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ] )
                   ]
                 )

######################################################################
# TexImage2DMML. Set the texture data for the currently bound texture in the
# specified texture target at the specified mipmap level. The texture data is
# referred by the texture data index. Negative mipmap levels are allowed to
# support extensions.
AddActionConfig( 'TexImage2DMML',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ] ),
                   IntAttribute(                'MipmapLevel' )
                   ]
                 )

######################################################################
# TexSubImage2D. Set the texture sub region data to the currently bound texture
# in the specified texture target. Xoffset and Yoffset specify the origin of the
# texture sub region. The width and height of the texture sub region are defined
# by the width and height of the texture data. The texture data is referred by
# the texture data index. The texture sub region data is defined for the texture
# mipmap levels if the texture data contains mipmap data.
AddActionConfig( 'TexSubImage2D',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ] ),
                   IntAttribute(                'Xoffset' ),
                   IntAttribute(                'Yoffset' )
                   ]
                 )

######################################################################
# TexSubImage2DMML. Set the texture sub region data to the currently bound
# texture in the specified texture target at the specified mipmap level. Xoffset
# and Yoffset specify the origin of the texture sub region. The width and height
# of the texture sub region are defined by the width and height of the texture
# data. The texture data is referred by the texture data index. Negative mipmap
# levels are allowed to support extensions.
AddActionConfig( 'TexSubImage2DMML',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ] ),
                   IntAttribute(                'Xoffset' ),
                   IntAttribute(                'Yoffset' ),
                   IntAttribute(                'MipmapLevel' )
                   ]
                 )

######################################################################
# CopyTexImage2D. Copy a selected region from the currently used framebuffer as
# a specified mipmap level into the currently bound texture in the specified
# texture target. Internal format specifies the resulting texture format. X, y,
# width and height specify the framebuffer region.
AddActionConfig( 'CopyTexImage2D',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ] ),
                   IntAttribute(                'Level',                                0 ),
                   EnumAttribute(               'InternalFormat', 'GLenum',             [ 'GL_ALPHA',
                                                                                          'GL_LUMINANCE',
                                                                                          'GL_LUMINANCE_ALPHA',
                                                                                          'GL_RGB',
                                                                                          'GL_RGBA' ] ),
                   IntAttribute(                'X' ),
                   IntAttribute(                'Y' ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 )
                   ]
                 )

######################################################################
# CopyTexSubImage2D. Copy a selected region from the framebuffer as a sub region
# to the specified mipmap level of the currently bound texture in the specified
# texture target. Xoffset and Yoffset specify the sub region within the
# texture. X, y, width and height specify the framebuffer region.
AddActionConfig( 'CopyTexSubImage2D',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ] ),
                   IntAttribute(                'Level',                                0 ),
                   IntAttribute(                'Xoffset' ),
                   IntAttribute(                'Yoffset' ),
                   IntAttribute(                'X' ),
                   IntAttribute(                'Y' ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 )
                   ]
                 )

######################################################################
# CompressedTexImage2D. Set compressed texture data to the currently bound
# texture in the specified texture target. The compressed texture data is
# referred by the compressed texture data index. Mipmap levels are defined for
# the texture if the compressed texture data contains mipmap data.
AddActionConfig( 'CompressedTexImage2D',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ] )
                   ]
                 )

######################################################################
# CompressedTexImage2DMML. Set compressed texture data to the currently bound
# texture in the specified texture target at the specified mipmap level. The
# compressed texture data is referred by the compressed texture data
# index. Negative mipmap levels are allowed to support extensions.
AddActionConfig( 'CompressedTexImage2DMML',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ] ),
                   IntAttribute(                'MipmapLevel' )
                   ]
                 )

######################################################################
# CompressedTexSubImage2D. Set compressed texture data to the sub region of the
# currently bound texture in the specified texture target. The compressed
# texture data is referred by the compressed texture data index. Xoffset and
# Yoffset specify the origin of the texture sub region. The width and height of
# the texture sub region are defined by the width and height of the compressed
# texture data. Mipmap levels are defined for the texture if the compressed
# texture data contains mipmap data.
AddActionConfig( 'CompressedTexSubImage2D',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ] ),
                   IntAttribute(                'Xoffset' ),
                   IntAttribute(                'Yoffset' )
                   ]
                 )

######################################################################
# CompressedTexSubImage2DMML. Set compressed texture data to the sub region of
# the currently bound texture in the specified texture target at the specified
# mipmap level. The compressed texture data is referred by the compressed
# texture data index. Xoffset and Yoffset specify the origin of the texture sub
# region. The width and height of the texture sub region are defined by the
# width and height of the compressed texture data. Negative mipmap levels are
# allowed to support extensions.
AddActionConfig( 'CompressedTexSubImage2DMML',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CompressedTextureDataIndex',           0, MAX_COMPRESSED_TEXTURE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ] ),
                   IntAttribute(                'Xoffset' ),
                   IntAttribute(                'Yoffset' ),
                   IntAttribute(                'MipmapLevel' )
                   ]
                 )

######################################################################
# TexParameter. Set all texture parameters for the texture bound in the
# specified texture target with a single action.
AddActionConfig( 'TexParameter',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP',
                                                                                          'GL_TEXTURE_EXTERNAL_OES' ] ),
                   EnumAttribute(               'MinFilter', 'GLint',                   [ 'GL_NEAREST',
                                                                                          'GL_LINEAR',
                                                                                          'GL_NEAREST_MIPMAP_NEAREST',
                                                                                          'GL_LINEAR_MIPMAP_NEAREST',
                                                                                          'GL_NEAREST_MIPMAP_LINEAR',
                                                                                          'GL_LINEAR_MIPMAP_LINEAR' ] ),
                   EnumAttribute(               'MagFilter', 'GLint',                   [ 'GL_NEAREST',
                                                                                          'GL_LINEAR' ] ),
                   EnumAttribute(               'WrapS', 'GLint',                       [ 'GL_CLAMP_TO_EDGE',
                                                                                          'GL_REPEAT',
                                                                                          'GL_MIRRORED_REPEAT' ] ),
                   EnumAttribute(               'WrapT', 'GLint',                       [ 'GL_CLAMP_TO_EDGE',
                                                                                          'GL_REPEAT',
                                                                                          'GL_MIRRORED_REPEAT' ] )
                   ]
                 )

######################################################################
# TexParameterMinFilter. Set the texture minification filter for the texture
# bound in the specified texture target.
AddActionConfig( 'TexParameterMinFilter',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP',
                                                                                          'GL_TEXTURE_EXTERNAL_OES' ] ),
                   EnumAttribute(               'MinFilter', 'GLint',                   [ 'GL_NEAREST',
                                                                                          'GL_LINEAR',
                                                                                          'GL_NEAREST_MIPMAP_NEAREST',
                                                                                          'GL_LINEAR_MIPMAP_NEAREST',
                                                                                          'GL_NEAREST_MIPMAP_LINEAR',
                                                                                          'GL_LINEAR_MIPMAP_LINEAR' ] )
                   ]
                 )

######################################################################
# TexParameterMagFilter. Set the texture magnification filter for the texture
# bound in the specified texture target.
AddActionConfig( 'TexParameterMagFilter',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP',
                                                                                          'GL_TEXTURE_EXTERNAL_OES' ] ),
                   EnumAttribute(               'MagFilter', 'GLint',                   [ 'GL_NEAREST',
                                                                                          'GL_LINEAR' ] )
                   ]
                 )


######################################################################
# TexParameterSwapS. Set the texture WrapS mode for the texture bound in the
# specified texture target.
AddActionConfig( 'TexParameterWrapS',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP',
                                                                                          'GL_TEXTURE_EXTERNAL_OES' ] ),
                   EnumAttribute(               'WrapS', 'GLint',                       [ 'GL_CLAMP_TO_EDGE',
                                                                                          'GL_REPEAT',
                                                                                          'GL_MIRRORED_REPEAT' ] )
                   ]
                 )

######################################################################
# TexParameterSwapT. Set the texture WrapT mode for the texture bound in the
# specified texture target.
AddActionConfig( 'TexParameterWrapT',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP',
                                                                                          'GL_TEXTURE_EXTERNAL_OES' ] ),
                   EnumAttribute(               'WrapT', 'GLint',                       [ 'GL_CLAMP_TO_EDGE',
                                                                                          'GL_REPEAT',
                                                                                          'GL_MIRRORED_REPEAT' ] )
                   ]
                 )

######################################################################
# BindTexture. Bind a texture to the specified texture target. The texture is
# specified by the texture index. A new texture is created if the texture index
# does not refer to an existing texture. A default OpenGLES2 texture (0) is
# bound if the texture index is -1.
AddActionConfig( 'BindTexture',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP',
                                                                                          'GL_TEXTURE_EXTERNAL_OES' ] ),
                   IntAttribute(                'TextureIndex',                         -1, MAX_TEXTURE_INDEX - 1 )
                   ]
                 )

######################################################################
# DeleteTexture. Delete the texture referred by the texture index. The texture
# index must refer to a valid texture.
AddActionConfig( 'DeleteTexture',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TextureIndex',                         0, MAX_TEXTURE_INDEX - 1 )
                   ]
                 )

######################################################################
# GenerateMipmap. Generate mipmaps for the texture bound in the specified
# texture target.
AddActionConfig( 'GenerateMipmap',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP' ] )
                   ]
                 )

######################################################################
# BindRenderbuffer. Bind a render buffer to the specified target. The render
# buffer is specified by the render buffer index. A new render buffer is created
# if the render buffer index does not refer to an existing buffer. A default
# OpenGLES2 render buffer (0) is bound if the render buffer index is -1.
AddActionConfig( 'BindRenderbuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_RENDERBUFFER' ] ),
                   IntAttribute(                'RenderbufferIndex',                    -1, MAX_RENDERBUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# DeleteRenderbuffer. Delete a render buffer. The render buffer index must refer
# to an existing buffer.
AddActionConfig( 'DeleteRenderbuffer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RenderbufferIndex',                    0, MAX_RENDERBUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# GenRenderbuffer. Create a new render buffer to the specified render buffer
# index. The render buffer index must be free.
AddActionConfig( 'GenRenderbuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'RenderbufferIndex',                    0, MAX_RENDERBUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# RenderbufferStorage. Set the storage parameters for the currently bound render
# buffer.
AddActionConfig( 'RenderbufferStorage',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_RENDERBUFFER' ] ),
                   EnumAttribute(               'InternalFormat', 'GLenum',             [ 'GL_RGBA4',
                                                                                          'GL_RGB5_A1',
                                                                                          'GL_RGB565',
                                                                                          'GL_DEPTH_COMPONENT16',
                                                                                          'GL_STENCIL_INDEX8',
                                                                                          'GL_RGB8_OES',
                                                                                          'GL_RGBA8_OES',
                                                                                          'GL_DEPTH_COMPONENT24_OES',
                                                                                          'GL_DEPTH_COMPONENT32_OES',
                                                                                          'GL_STENCIL_INDEX4_OES',
                                                                                          'GL_DEPTH24_STENCIL8_OES' ] ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 )
                   ]
                 )

######################################################################
# BindFramebuffer. Bind a frame buffer to the specified frame buffer target. The
# frame buffer is specified by the frame buffer index. A new frame buffer is
# created if the frame buffer index does not refer to an existing frame
# buffer. A default OpenGLES2 frame buffer (0) is bound if the frame buffer
# index is -1.
AddActionConfig( 'BindFramebuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_FRAMEBUFFER' ] ),
                   IntAttribute(                'FramebufferIndex',                     -1, MAX_FRAMEBUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# DeleteFramebuffer. Delete a frame buffer. The frame buffer index must refer to
# an existing buffer.
AddActionConfig( 'DeleteFramebuffer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'FramebufferIndex',                     0, MAX_FRAMEBUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# GenFramebuffer. Create a new frame buffer to the specified frame buffer
# index. The frame buffer index must be free.
AddActionConfig( 'GenFramebuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FramebufferIndex',                     0, MAX_FRAMEBUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# CurrentFramebuffer. Query current frame buffer and store it to the specified
# frame buffer index. The frame buffer index must be free.
AddActionConfig( 'CurrentFramebuffer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FramebufferIndex',                     0, MAX_FRAMEBUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# CheckFramebufferStatus. Check that the frame buffer bound to the target is
# valid. The action returns an error if the frame buffer is in an invalid state.
AddActionConfig( 'CheckFramebufferStatus',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_FRAMEBUFFER' ] )
                   ]
                 )

######################################################################
# FramebufferTexture2D. Bind a texture to the specified attachment point of the
# currently bound frame buffer. The texture is specified by the texture target
# and the texture index. The texture index -1 detaches any image from the
# specified attachment point.
AddActionConfig( 'FramebufferTexture2D',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_FRAMEBUFFER' ] ),
                   EnumAttribute(               'Attachment', 'GLenum',                 [ 'GL_COLOR_ATTACHMENT0',
                                                                                          'GL_DEPTH_ATTACHMENT',
                                                                                          'GL_STENCIL_ATTACHMENT' ] ),
                   EnumAttribute(               'TexTarget', 'GLenum',                  [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ] ),
                   IntAttribute(                'TextureIndex',                         -1, MAX_TEXTURE_INDEX - 1 ),
                   IntAttribute(                'Level',                                0, 0 )
                   ]
                 )

######################################################################
# FramebufferRenderbuffer. Bind a render buffer to the specified attachment
# point of the currently bound frame buffer. The render buffer is specified by
# the render buffer index. The render buffer index -1 detaches any image from
# the specified attachment point.
AddActionConfig( 'FramebufferRenderbuffer',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_FRAMEBUFFER' ] ),
                   EnumAttribute(               'Attachment', 'GLenum',                 [ 'GL_COLOR_ATTACHMENT0',
                                                                                          'GL_DEPTH_ATTACHMENT',
                                                                                          'GL_STENCIL_ATTACHMENT' ] ),
                   EnumAttribute(               'RenderbufferTarget', 'GLenum',         [ 'GL_RENDERBUFFER' ] ),
                   IntAttribute(                'RenderbufferIndex',                    -1, MAX_RENDERBUFFER_INDEX - 1 )
                   ]
                 )

######################################################################
# AttachShader. Attach a shader to the program.
AddActionConfig( 'AttachShader',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ProgramIndex',                         0, MAX_PROGRAM_INDEX - 1 ),
                   IntAttribute(                'ShaderIndex',                          0, MAX_SHADER_INDEX - 1 )
                   ]
                 )

######################################################################
# BindAttribLocation. Bind a named variable into the specified location of the
# shader program.
AddActionConfig( 'BindAttribLocation',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ProgramIndex',                         0, MAX_PROGRAM_INDEX - 1 ),
                   IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   StringAttribute(             'Name',                                 MAX_VARIABLE_LENGTH )
                   ]
                 )

######################################################################
# CompileShader. Compile a shader referred by the shader index.
AddActionConfig( 'CompileShader',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ShaderIndex',                          0, MAX_SHADER_INDEX - 1 )
                   ]
                 )

######################################################################
# CreateProgram. Create a new shader program to the program index.
AddActionConfig( 'CreateProgram',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ProgramIndex',                         0, MAX_PROGRAM_INDEX - 1 )
                   ]
                 )

######################################################################
# CreateShader. Create a new shader to the shader index.
AddActionConfig( 'CreateShader',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [  EnumAttribute(               'Type', 'GLenum',                      [ 'GL_FRAGMENT_SHADER',
                                                                                          'GL_VERTEX_SHADER' ] ),
                    IntAttribute(                'ShaderIndex',                         0, MAX_SHADER_INDEX - 1 )
                   ]
                 )

######################################################################
# DeleteShader. Delete a shader referred by the shader index. The shader index
# must refer to a valid shader.
AddActionConfig( 'DeleteShader',
                 'Execute'+'Destroy',
                 [  IntAttribute(                'ShaderIndex',                         0, MAX_SHADER_INDEX - 1 )
                   ]
                 )

######################################################################
# DetachShader. Detach a shader from the shader program. The program index and
# the shader index must refer to a valid program and shader, respectively.
AddActionConfig( 'DetachShader',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ProgramIndex',                         0, MAX_PROGRAM_INDEX - 1 ),
                   IntAttribute(                'ShaderIndex',                          0, MAX_SHADER_INDEX - 1 )
                   ]
                 )

######################################################################
# DeleteProgram. Delete the shader program referred by the program index. The
# program index must refer to a valid program.
AddActionConfig( 'DeleteProgram',
                 'Execute'+'Destroy',
                 [  IntAttribute(                'ProgramIndex',                        0, MAX_PROGRAM_INDEX - 1 )
                   ]
                 )

######################################################################
# GetAttribLocation. Get the location of the named attribute in the shader
# program specified by the program index. The location value is stored to the
# register specified by the register index.
AddActionConfig( 'GetAttribLocation',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   IntAttribute(                'ProgramIndex',                         0, MAX_PROGRAM_INDEX - 1 ),
                   StringAttribute(             'Name',                                 MAX_VARIABLE_LENGTH )
                   ]
                 )

######################################################################
# GetUniformLocation. Get the location of the named uniform in the shader
# program specified by the program index. The location value is stored to the
# register specified by the register index.
AddActionConfig( 'GetUniformLocation',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   IntAttribute(                'ProgramIndex',                         0, MAX_PROGRAM_INDEX - 1 ),
                   StringAttribute(             'Name',                                 MAX_VARIABLE_LENGTH )
                   ]
                 )

######################################################################
# LinkProgram. Link the shader program referred by the program index. The
# program index must refer to a valid shader program.
AddActionConfig( 'LinkProgram',
                 'Execute'+'Destroy',
                 [  IntAttribute(                'ProgramIndex',                        0, MAX_PROGRAM_INDEX - 1 )
                   ]
                 )

######################################################################
# ReleaseShaderCompiler. Release the shader compiler.
AddActionConfig( 'ReleaseShaderCompiler',
                 'Execute'+'Destroy',
                 []
                 )

######################################################################
# ShaderBinary. Load shader binary to shaders specified by the shader
# indices. The shader indices must refer to valid shaders. The shader binary is
# loaded from the data referred by the data index. The data index must refer to
# a valid data.
AddActionConfig( 'ShaderBinary',
                 'Execute'+'Destroy',
                 [ IntVectorAttribute(          'ShaderIndices' ),
                   EnumAttribute(               'BinaryFormat', 'GLenum',               [ 'GL_PLATFORM_BINARY' ] ),
                   IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 )
                   ]
                 )

######################################################################
# ShaderSource. Load shader source to the shader referred by the shader
# index. The shader source is loaded from the data referred by the data
# index. The data index must refer to a valid data.
AddActionConfig( 'ShaderSource',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ShaderIndex',                          0, MAX_SHADER_INDEX - 1 ),
                   IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 )
                   ]
                 )

######################################################################
# Uniformf. Set the uniform value as floating point. The value in the register
# specified by the register index specifies the index of the uniform variable in
# the currently used shader program. Uniform values are given as sets of one to
# four floats. Count specifies the number of value sets. Values vector specifies
# the actual uniform values. The length of the values vector must be a multiple
# of 1, 2, 3, or 4.
AddActionConfig( 'Uniformf',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   IntAttribute(                'Count',                                1 ),
                   FloatVectorAttribute(        'Values' )
                   ],
                 [ ValidatorConfig( '( Values->length / Count ) != 1 && ( Values->length / Count ) != 2 && ( Values->length / Count ) != 3 && ( Values->length / Count ) != 4' ,
                                    'Uniformf@OpenGLES2 invalid values length' ) ]
                 )

######################################################################
# Uniformi. Set the uniform value as integer. The value in the register
# specified by the register index specifies the index of the uniform variable in
# the currently used shader program. Uniform values are given as sets of one to
# four integers. Count specifies the number of value sets. Values vector
# specifies the actual uniform values. The length of the values vector must be a
# multiple of 1, 2, 3, or 4.
AddActionConfig( 'Uniformi',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   IntAttribute(                'Count',                                1 ),
                   IntVectorAttribute(          'Values' )
                   ],
                 [ ValidatorConfig( '( Values->length / Count ) != 1 && ( Values->length / Count ) != 2 && ( Values->length / Count ) != 3 && ( Values->length / Count ) != 4' ,
                                    'Uniformi@OpenGLES2 invalid values length' ) ]
                 )

######################################################################
# UniformMatrix. Set the uniform value as 2x2, 3x3, or 4x4 matrices. The value
# in the register specified by the register index specifies the index of the
# uniform variable in the currently used shader program. Uniform values are
# given as sets of 4, 9, or 16 floats. Count specifies the number of value
# sets. Values vector specifies the actual uniform values. The length of the
# values vector must be a multiple of 4, 9, or 16.
AddActionConfig( 'UniformMatrix',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   IntAttribute(                'Count',                                1 ),
                   FloatVectorAttribute(        'Values' )
                   ],
                 [ ValidatorConfig( '( Values->length / Count ) != 4 && ( Values->length / Count ) != 9 && ( Values->length / Count ) != 16' ,
                                    'UniformMatrix@OpenGLES2 invalid values length' ) ]
                 )

######################################################################
# UniformfFromRegister. Set a single uniform value as floating point from the
# value stored in the source register. The value in the register specified by
# the register index specifies the index of the uniform variable in the
# currently used shader program. The source register value can be used as a
# varying counter for the shader when used in combination with the register
# actions.
AddActionConfig( 'UniformfFromRegister',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   IntAttribute(                'SourceRegisterIndex',                  0, MAX_REGISTER_INDEX - 1 )
                   ]
                 )

######################################################################
# UniformiFromRegister. Set a single uniform value as integer from the value
# stored in the source register. The value in the register specified by the
# register index specifies the index of the uniform variable in the currently
# used shader program. The source register value can be used to change active
# texture for the shader when used in combination with the register actions.
AddActionConfig( 'UniformiFromRegister',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   IntAttribute(                'SourceRegisterIndex',                  0, MAX_REGISTER_INDEX - 1 )
                   ]
                 )

######################################################################
# UseProgram. Use a shader program specified by the program index. The program
# index must refer to a valid shader program.
AddActionConfig( 'UseProgram',
                 'Execute'+'Destroy',
                 [  IntAttribute(                'ProgramIndex',                        -1, MAX_PROGRAM_INDEX - 1 )
                   ]
                 )

######################################################################
# ValidateProgram. Validate a shader program specified by the program index. The
# program index must refer to a valid shader program.
AddActionConfig( 'ValidateProgram',
                 'Execute'+'Destroy',
                 [  IntAttribute(                'ProgramIndex',                        0, MAX_PROGRAM_INDEX - 1 )
                   ]
                 )

######################################################################
# CreateMesh. Create a mesh to the specified mesh index from the arrays
# specified by the array indices. The sizes vector defines the number of
# components to sample from each array per mesh vertex. Every array must have
# the same number of per-vertex data elements. The mesh index must be free.
AddActionConfig( 'CreateMesh',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'MeshIndex',                            0, MAX_MESH_INDEX - 1 ),
                   IntVectorAttribute(          'ArrayIndices' ),
                   IntVectorAttribute(          'Sizes' )
                   ],
                 [ ValidatorConfig( 'ArrayIndices->length != Sizes->length',
                                    'CreateMesh@OpenGLES2 ArrayIndices and Sizes differ in length' ) ]
                 )

######################################################################
# DeleteMesh. Delete the mesh referred by the mesh index. The mesh index must
# refer to a valid mesh.
AddActionConfig( 'DeleteMesh',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'MeshIndex',                            0, MAX_MESH_INDEX - 1 )
                   ]
                 )

######################################################################
# MeshVertexAttribPointer. Set a vertex array pointer to point to the mesh
# data. The value at the register referred by the register index defines the
# vertex attribute index. The array index defines the array within the mesh.
AddActionConfig( 'MeshVertexAttribPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   IntAttribute(                'MeshIndex',                            0, MAX_MESH_INDEX - 1 ),
                   IntAttribute(                'ArrayIndex',                           0 ),
                   OnOffAttribute(              'Normalized' )
                   ]
                 )

######################################################################
# MeshBufferData. Binds buffer to GL_ARRAY_BUFFER target (if bind is specified)
# and sets mesh data. The mesh is referred by the mesh index. The mesh index
# must refer to a valid mesh.
AddActionConfig( 'MeshBufferData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   IntAttribute(                'MeshIndex',                            0, MAX_MESH_INDEX - 1 ),
                   EnumAttribute(               'Usage', 'GLenum',                      [ 'GL_STATIC_DRAW',
                                                                                          'GL_DYNAMIC_DRAW',
                                                                                          'GL_STREAM_DRAW' ] ),
                   ]
                 )

######################################################################
# BufferMeshVertexAttribPointer. Binds buffer to GL_ARRAY_BUFFER target (if bind
# is specified) and sets a vertex array pointer to point to the mesh data. The
# value at the register referred by the register index defines the vertex
# attribute index. The array index defines the array within the mesh.
AddActionConfig( 'BufferMeshVertexAttribPointer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'BufferIndex',                          0, MAX_BUFFER_INDEX - 1 ),
                   OnOffAttribute(              'Bind' ),
                   IntAttribute(                'RegisterIndex',                        0, MAX_REGISTER_INDEX - 1 ),
                   IntAttribute(                'ArrayIndex',                           0 ),
                   OnOffAttribute(              'Normalized' )
                   ]
                 )

################################################################################
# EGL IMAGE
################################################################################

################################################################################
# EglImageTargetTexture2D. Requires GL_OES_EGL_image extension. Set the texture
# data for the currently bound texture in the specified texture
# target. EglImageIndex defines the EGL image index at EGL
# module. GL_TEXTURE_EXTERNAL_OES target requires GL_OES_EGL_image_external
# extension.
AddActionConfig( 'EglImageTargetTexture2D',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_EXTERNAL_OES' ] ),
                   IntAttribute(                'EglImageIndex',                        0 ),
                   ]
                 )

################################################################################
# EglImageTargetRenderbufferStorage. Requires GL_OES_EGL_image extension. Set
# the renderbuffer data for the currently bound renderbuffer in the specified
# renderbuffer target. EglImageIndex defines the EGL image index at EGL module.
AddActionConfig( 'EglImageTargetRenderbufferStorage',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ EnumAttribute(               'Target', 'GLenum',                     [ 'GL_RENDERBUFFER' ] ),
                   IntAttribute(                'EglImageIndex',                        0 ),
                   ]
                 )


######################################################################
# Multithread benchmark support
######################################################################

######################################################################
# Flag. Set the flag and the flag value, and wait for another thread to set the
# signal. ThreadSignalIndex defines signal index at Thread module.
AddActionConfig( 'Flag',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FlagIndex',                            0, MAX_FLAG_INDEX - 1 ),
                   FloatAttribute(              'Value' ),
                   IntAttribute(                'ThreadSignalIndex',                    0 ),
                   ]
                 )

######################################################################
# CBindTexture. Conditional bind texture. Does nothing if the flag is not
# set. If the flag is set, calls glFinish if enabled and binds the texture from
# the textures vector with the index taken from the flag value. Finally, sets
# the signal.
AddActionConfig( 'CBindTexture',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FlagIndex',                            0, MAX_FLAG_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP',
                                                                                          'GL_TEXTURE_EXTERNAL_OES' ] ),
                   IntVectorAttribute(          'Textures' ),
                   OnOffAttribute(              'Finish' ),
                   IntAttribute(                'ThreadSignalIndex',                    0 ),
                   ]
                 )

######################################################################
# CreateGrowingTextureData. Create a growing texture data to the specified
# texture data index. Texture grows per benchmark frame, i.e. current width =
# Width + framenumber * WidthInc, until width reaches the with limit, after
# which it is reset back to initial value and the grow repeats; the same logic
# applies to height. The texture data index must be unused. This action is
# designed for creating potential memory fragmentation for texture/graphics
# memory studies.
AddActionConfig( 'CreateGrowingTextureData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_DATA_INDEX - 1 ),
                   FloatArrayAttribute(         'Color',                                4, 0, 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   IntAttribute(                'WidthInc',                             0 ),
                   IntAttribute(                'HeightInc',                            0 ),
                   IntAttribute(                'WidthLimit',                           1 ),
                   IntAttribute(                'HeightLimit',                          1 ),
                   OnOffAttribute(              'Mipmaps' ),
                   EnumAttribute(               'Type', 'SCTOpenGLES2TextureType',      [ 'OPENGLES2_LUMINANCE8',
                                                                                          'OPENGLES2_ALPHA8',
                                                                                          'OPENGLES2_LUMINANCE_ALPHA88',
                                                                                          'OPENGLES2_RGB565',
                                                                                          'OPENGLES2_RGB888',
                                                                                          'OPENGLES2_RGBA4444',
                                                                                          'OPENGLES2_RGBA5551',
                                                                                          'OPENGLES2_RGBA8888',
                                                                                          'OPENGLES2_BGRA8888',
                                                                                          'OPENGLES2_DEPTH',
                                                                                          'OPENGLES2_DEPTH32',
                                                                                          'OPENGLES2_DEPTH24_STENCIL8' ] )
                   ]
                 )

######################################################################
# StashTexture. Creates a stashed texture from the texture data to the selected
# texture target. The newly created texture is left bound in the target. The
# idea of stashed textures is to enable testing how much texture data can be
# allocated until the graphics memory is exhausted, among other things.
# MemoryLimit defines the maximun amount of texture memory to allocate after
# which the action signals the benchmark as completed if SignalComplete is
# defined. SignalCompleted can be defined only in benchmarks with Repeats
# 0. MemoryLimit 0 means no limit. The action does nothing if the memory limit
# has been reached and SignalComplete is not defined. MaxTextureCount defines
# the upper limit for the number of stashed textures, overflow signals a
# benchmark error. The action prints the amount of stashed texture data during
# action terminate as a [Target: STASH_TEXTURE] section, after which the stashed
# textures are released. The texture data index must refer to a valid texture
# data. Use PrintProgress to monitor the amount of texture data as the benchmark
# progresses.
AddActionConfig( 'StashTexture',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TextureDataIndex',                     0, MAX_TEXTURE_INDEX - 1 ),
                   EnumAttribute(               'Target', 'GLenum',                     [ 'GL_TEXTURE_2D',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                                                                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z',
                                                                                          'GL_TEXTURE_EXTERNAL_OES' ] ),
                   Hex32Attribute(              'MemoryLimit' ),
                   IntAttribute(                'MaxTextureCount',                      1 ),
                   OnOffAttribute(              'SignalComplete' ),
                   OnOffAttribute(              'PrintProgress' ),
                   ]
                 )

######################################################################
# END OF FILE
######################################################################
