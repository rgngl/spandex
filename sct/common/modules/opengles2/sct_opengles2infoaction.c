/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2infoaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2InfoActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2InfoActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2InfoActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2InfoActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Info@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2InfoActionContext ) );

    if( sctiParseOpenGLES2InfoActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2InfoActionContext( context );
        return NULL;
    }

    context->reported = SCT_FALSE;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2InfoActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2InfoActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2InfoActionContext*  context;
    SCTAttributeList*               attributes;
    GLenum                          err;
    SCTBoolean                      status          = SCT_TRUE;
    const char*                     str;
    GLint                           intValues[ 4 ];
    GLfloat                         floatValues[ 4 ];
    GLboolean                       booleanValues[ 4 ];
    char                            buffer[ 64 ];

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2InfoActionContext* )( action->context );

    if( context->reported == SCT_TRUE )
    {
        return SCT_TRUE;
    }

    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Info@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    /* Store subpixel bits. */
    glGetIntegerv( GL_SUBPIXEL_BITS, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL SUBPIXEL BITS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
    
    /* Store max texture size. */
    glGetIntegerv( GL_MAX_TEXTURE_SIZE, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX TEXTURE SIZE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max cube map texture size. */
    glGetIntegerv( GL_MAX_CUBE_MAP_TEXTURE_SIZE, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX CUBE MAP TEXTURE SIZE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max viewport dims. */
    glGetIntegerv( GL_MAX_VIEWPORT_DIMS, intValues );
    sprintf( buffer, "%u,%u", intValues[ 0 ], intValues[ 1 ] );
    if( sctAddNameValueToList( attributes, "GL MAX VIEWPORT DIMS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
    
    /* Store aliased point size range. */
    glGetFloatv( GL_ALIASED_POINT_SIZE_RANGE, floatValues );
    sprintf( buffer, "%f,%f", floatValues[ 0 ], floatValues[ 1 ] );
    if( sctAddNameValueToList( attributes, "GL ALIASED POINT SIZE RANGE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store aliased line width range. */
    glGetFloatv( GL_ALIASED_LINE_WIDTH_RANGE, floatValues );
    sprintf( buffer, "%f,%f", floatValues[ 0 ], floatValues[ 1 ] );
    if( sctAddNameValueToList( attributes, "GL ALIASED LINE WIDTH RANGE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store sample buffers. */
    glGetIntegerv( GL_SAMPLE_BUFFERS, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL SAMPLE BUFFERS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store samples. */
    glGetIntegerv( GL_SAMPLES, intValues );
    sprintf( buffer, "%u", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL SAMPLES", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store number of compressed texture formats. */
    glGetIntegerv( GL_NUM_COMPRESSED_TEXTURE_FORMATS, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL NUM COMPRESSED TEXTURE FORMATS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store number of shader binary formats. */
    glGetIntegerv( GL_NUM_SHADER_BINARY_FORMATS, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL NUM SHADER BINARY FORMATS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
    
    /* Store shader compiler flag. */
    glGetBooleanv( GL_SHADER_COMPILER, booleanValues );
    sprintf( buffer, "%d", booleanValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL SHADER COMPILER", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
    
//     /* Store fragment precision high flag. */
//     glGetBooleanv( GL_FRAGMENT_PRECISION_HIGH, booleanValues );
//     sprintf( buffer, "%d", booleanValues[ 0 ] );
//     if( sctAddNameValueToList( attributes, "GL FRAGMENT PRECISION HIGH", buffer ) == SCT_FALSE )
//     {
//         status = SCT_FALSE;
//     }

    /* Store GL extensions string. */
    str = ( const char* )( glGetString( GL_EXTENSIONS ) );
    if( str == NULL ||
        sctAddNameValueToList( attributes, "GL EXTENSIONS", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store GL renderer. */
    str = ( const char* )( glGetString( GL_RENDERER ) );
    if( str == NULL ||
        sctAddNameValueToList( attributes, "GL RENDERER", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store shading language version. */
    str = ( const char* )( glGetString( GL_SHADING_LANGUAGE_VERSION ) );
    if( str == NULL ||
        sctAddNameValueToList( attributes, "GL SHADING LANGUAGE VERSION", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
    
    /* Store GL vendor. */
    str = ( const char* )( glGetString( GL_VENDOR ) );
    if( str == NULL || 
        sctAddNameValueToList( attributes, "GL VENDOR", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
    
    /* Store GL version. */
    str = ( const char* )( glGetString( GL_VERSION ) );
    if( str == NULL ||
        sctAddNameValueToList( attributes, "GL VERSION", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max vertex attribs. */
    glGetIntegerv( GL_MAX_VERTEX_ATTRIBS, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX VERTEX ATTRIBS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max vertex uniform vectors. */
    glGetIntegerv( GL_MAX_VERTEX_UNIFORM_VECTORS, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX VERTEX UNIFORM VECTORS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max varying vectors. */
    glGetIntegerv( GL_MAX_VARYING_VECTORS, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX VARYING VECTORS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max combined texture image units. */
    glGetIntegerv( GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX COMBINED TEXTURE IMAGE UNITS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max vertex texture image units. */
    glGetIntegerv( GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX VERTEX TEXTURE IMAGE UNITS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max texture image units. */
    glGetIntegerv( GL_MAX_TEXTURE_IMAGE_UNITS, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX TEXTURE IMAGE UNITS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max fragment uniform vectors. */
    glGetIntegerv( GL_MAX_FRAGMENT_UNIFORM_VECTORS, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX FRAGMENT UNIFORM VECTORS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max renderbuffer size. */
    glGetIntegerv( GL_MAX_RENDERBUFFER_SIZE, intValues );
    sprintf( buffer, "%d", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL MAX RENDERBUFFER SIZE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store implementation color read type. */
    glGetIntegerv( GL_IMPLEMENTATION_COLOR_READ_TYPE, intValues );
    sprintf( buffer, "0x%x", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL IMPLEMENTATION COLOR READ TYPE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store implementation color read format. */
    glGetIntegerv( GL_IMPLEMENTATION_COLOR_READ_FORMAT, intValues );
    sprintf( buffer, "0x%x", intValues[ 0 ] );
    if( sctAddNameValueToList( attributes, "GL IMPLEMENTATION COLOR READ FORMAT", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store viewport; usually the screen size. */
    glGetIntegerv( GL_VIEWPORT, intValues );
    sprintf( buffer, "%d,%d,%d,%d", intValues[ 0 ], intValues[ 1 ], intValues[ 2 ], intValues[ 3 ] );
    if( sctAddNameValueToList( attributes, "GL viewport", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }  

    if( status == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Failure in Info@OpenGLES2 action execute." );
    }

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    if( status == SCT_TRUE )
    {
        err = glGetError();
        if( err != GL_NO_ERROR )
        {
            char buf[ 256 ];
            sprintf( buf, "GL error %s in Info@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
            SCT_LOG_ERROR( buf );
            sctiOpenGLES2ClearGLError();

            status = SCT_FALSE;
        }
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    if( status == SCT_TRUE && sctReportSection( "OpenGLES2", "MODULE", attributes ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Report section failed in Info@OpenGLES2 action execute." );
        status = SCT_FALSE;
    }

    sctDestroyAttributeList( attributes );

    context->reported = SCT_TRUE;

    return status;
}

/*!
 *
 *
 */
void sctiOpenGLES2InfoActionDestroy( SCTAction* action )
{
    SCTOpenGLES2InfoActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2InfoActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2InfoActionContext( context );
}
