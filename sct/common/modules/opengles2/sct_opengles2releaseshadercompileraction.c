/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2releaseshadercompileraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2ReleaseShaderCompilerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2ReleaseShaderCompilerActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2ReleaseShaderCompilerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2ReleaseShaderCompilerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ReleaseShaderCompiler@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2ReleaseShaderCompilerActionContext ) );

    if( sctiParseOpenGLES2ReleaseShaderCompilerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2ReleaseShaderCompilerActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2ReleaseShaderCompilerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ReleaseShaderCompilerActionExecute( SCTAction* action, int frameNumber )
{
    GLenum  err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );

    glReleaseShaderCompiler();

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in ReleaseShaderCompiler@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* SCT_OPENGLES2_MODULE_CHECK_ERRORS */
    SCT_USE_VARIABLE( err );
#endif  /* SCT_OPENGLES2_MODULE_CHECK_ERRORS */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2ReleaseShaderCompilerActionDestroy( SCTAction* action )
{
    SCTOpenGLES2ReleaseShaderCompilerActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2ReleaseShaderCompilerActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2ReleaseShaderCompilerActionContext( context );
}
