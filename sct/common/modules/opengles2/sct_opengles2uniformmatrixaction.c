/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2uniformmatrixaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2UniformMatrixActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2UniformMatrixActionContext* context;
    SCTOpenGLES2ModuleContext*              mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenGLES2ModuleContext* )( moduleContext );
    context = ( SCTOpenGLES2UniformMatrixActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2UniformMatrixActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in UniformMatrix@OpenGLES2 context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2UniformMatrixActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenGLES2UniformMatrixActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2UniformMatrixActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.registerIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid register index in UniformMatrix@OpenGLES2 context creation." );
        return NULL;      
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2UniformMatrixActionContext( void* context )
{
    SCTOpenGLES2UniformMatrixActionContext* c;

    if( context == NULL )
    {
        return;
    }
    
    c = ( SCTOpenGLES2UniformMatrixActionContext* )( context );
    if( c->data.values != NULL )
    {
        sctDestroyFloatVector( c->data.values );
        c->data.values = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2UniformMatrixActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2UniformMatrixActionContext* context;
    OpenGLES2UniformMatrixActionData*       data;
    GLint                                   err;
    int                                     location;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2UniformMatrixActionContext* )( action->context );
    data    = &( context->data );

    location = ( int )( sctiOpenGLES2ModuleGetRegisterValue( context->moduleContext, data->registerIndex ) );
    if( location < 0 )
    {
        SCT_LOG_ERROR( "Invalid location index in UniformMatrix@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    switch( data->values->length / data->count )
    {
    case 4:
        glUniformMatrix2fv( location, data->count, GL_FALSE, data->values->data );
        break;
    case 9:
        glUniformMatrix3fv( location, data->count, GL_FALSE, data->values->data );
        break;
    case 16:
        glUniformMatrix4fv( location, data->count, GL_FALSE, data->values->data );
        break;
    default:
        SCT_LOG_ERROR( "Unsupported number of values in UniformMatrix@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in UniformMatrix@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2UniformMatrixActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenGLES2UniformMatrixActionContext( action->context );
    action->context = NULL;
}

