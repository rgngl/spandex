/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2bindrenderbufferaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2BindRenderbufferActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2BindRenderbufferActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2BindRenderbufferActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2BindRenderbufferActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BindRenderbuffer@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2BindRenderbufferActionContext ) );

    context->created = SCT_FALSE;

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2BindRenderbufferActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BindRenderbufferActionContext( context );
        return NULL;
    }

    if( context->data.renderbufferIndex >= 0 &&
        sctiOpenGLES2ModuleIsValidRenderbufferIndex( context->moduleContext, context->data.renderbufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BindRenderbufferActionContext( context );
        SCT_LOG_ERROR( "Invalid renderbuffer index in BindRenderbuffer@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2BindRenderbufferActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2BindRenderbufferActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2BindRenderbufferActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2BindRenderbufferActionContext*  context;
    GLenum                                      err;
    OpenGLES2BindRenderbufferActionData*        data;
    SCTOpenGLES2Renderbuffer*                   renderbuffer;
    GLuint                                      renderbufferId  = 0;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2BindRenderbufferActionContext* )( action->context );
    data    = &( context->data );

    if( data->renderbufferIndex >= 0 )
    {
        renderbuffer = sctiOpenGLES2ModuleGetRenderbuffer( context->moduleContext, data->renderbufferIndex );
        if( renderbuffer == NULL )
        {
            glGenRenderbuffers( 1, &renderbufferId );
            renderbuffer = sctOpenGLES2CreateRenderbuffer( renderbufferId );
            if( renderbuffer == NULL )
            {
                glDeleteRenderbuffers( 1, &renderbufferId );
                SCT_LOG_ERROR( "Renderbuffer creation failed in BindRenderbuffer@OpenGLES2 action execute." );
                return SCT_FALSE;
            }
            sctiOpenGLES2ModuleSetRenderbuffer( context->moduleContext, data->renderbufferIndex, renderbuffer );
            context->created = SCT_TRUE;
        }

        renderbufferId = renderbuffer->renderbuffer;
    }
    
    glBindRenderbuffer( data->target, renderbufferId );
    
#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BindRenderbuffer@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2BindRenderbufferActionTerminate( SCTAction* action )
{
    SCTOpenGLES2BindRenderbufferActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2BindRenderbufferActionContext* )( action->context );

    if( context->created == SCT_TRUE )
    {
        sctiOpenGLES2ModuleDeleteRenderbuffer( context->moduleContext, context->data.renderbufferIndex );
        context->created = SCT_FALSE;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2BindRenderbufferActionDestroy( SCTAction* action )
{
    SCTOpenGLES2BindRenderbufferActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2BindRenderbufferActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2BindRenderbufferActionContext( context );
}
