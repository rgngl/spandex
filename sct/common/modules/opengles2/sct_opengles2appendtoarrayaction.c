/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2appendtoarrayaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2AppendToArrayActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2AppendToArrayActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2AppendToArrayActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2AppendToArrayActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in AppendToArray@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2AppendToArrayActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2AppendToArrayActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2AppendToArrayActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2AppendToArrayActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in AppendToArray@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2AppendToArrayActionContext( void* context )
{
    SCTOpenGLES2AppendToArrayActionContext* c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenGLES2AppendToArrayActionContext* )( context );
   
    if( c->data.data != NULL )
    {
        sctDestroyDoubleVector( c->data.data );
        c->data.data = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2AppendToArrayActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2AppendToArrayActionContext* context;
    OpenGLES2AppendToArrayActionData*       data;
    SCTOpenGLES2Array*                      array;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2AppendToArrayActionContext* )( action->context );
    data    = &( context->data );

    array = sctiOpenGLES2ModuleGetArray( context->moduleContext, data->arrayIndex );
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array index in AppendToArray@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES2AddDoubleDataToArray( array, data->data->data, data->data->length ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Data append failed in AppendToArray@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2AppendToArrayActionDestroy( SCTAction* action )
{
    SCTOpenGLES2AppendToArrayActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2AppendToArrayActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2AppendToArrayActionContext( context );
}
