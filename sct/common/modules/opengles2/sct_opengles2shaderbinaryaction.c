/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2shaderbinaryaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2ShaderBinaryActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2ShaderBinaryActionContext*  context;
    int                                     i;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2ShaderBinaryActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2ShaderBinaryActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ShaderBinary@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2ShaderBinaryActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2ShaderBinaryActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2ShaderBinaryActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2ShaderBinaryActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in ShaderBinary@OpenGLES2 context creation." );
        return NULL;
    }

    for( i = 0; i < context->data.shaderIndices->length; ++i )
    {
        if( sctiOpenGLES2ModuleIsValidShaderIndex( context->moduleContext, context->data.shaderIndices->data[ i ] ) == SCT_FALSE )
        {
            sctiDestroyOpenGLES2ShaderBinaryActionContext( context );
            SCT_LOG_ERROR( "Shader index out of range in ShaderBinary@OpenGLES2 context creation." );
            return NULL;
        }
    }

    context->shaders = ( GLuint* )( siCommonMemoryAlloc( NULL, context->data.shaderIndices->length * sizeof( GLuint* ) ) );
    if( context->shaders == NULL )
    {
        sctiDestroyOpenGLES2ShaderBinaryActionContext( context );
        SCT_LOG_ERROR( "Shader array allocation failed in ShaderBinary@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2ShaderBinaryActionContext( void* context )
{
    SCTOpenGLES2ShaderBinaryActionContext*  c;
  
    c = ( SCTOpenGLES2ShaderBinaryActionContext* )( context );

    if( c == NULL )
    {
        return;
    }
   
    if( c->shaders != NULL )
    {
        siCommonMemoryFree( NULL, c->shaders );
        c->shaders = NULL;
    }

    if( c->data.shaderIndices != NULL )
    {
        sctDestroyIntVector( c->data.shaderIndices );
        c->data.shaderIndices = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ShaderBinaryActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2ShaderBinaryActionContext*  context;
    GLenum                                  err;
    OpenGLES2ShaderBinaryActionData*        data;
    int                                     i;
    SCTOpenGLES2Data*                       shaderBinary;
    SCTOpenGLES2Shader*                     shader;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2ShaderBinaryActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT( context->shaders != NULL );

    for( i = 0; i < data->shaderIndices->length; ++i )
    {
        shader = sctiOpenGLES2ModuleGetShader( context->moduleContext, data->shaderIndices->data[ i ] );
        if( shader == NULL )
        {
            SCT_LOG_ERROR( "Invalid shader index in ShaderBinary@OpenGLES2 action execute." );
            return SCT_FALSE;
        }
        context->shaders[ i ] = shader->shader;
    }

    shaderBinary = sctiOpenGLES2ModuleGetData( context->moduleContext, data->dataIndex );
    if( shaderBinary == NULL )
    {
        SCT_LOG_ERROR( "Invalid data index in ShaderBinary@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    
    glShaderBinary( data->shaderIndices->length, context->shaders, data->binaryFormat, shaderBinary->data, shaderBinary->length ) ;

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in ShaderBinary@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2ShaderBinaryActionDestroy( SCTAction* action )
{
    SCTOpenGLES2ShaderBinaryActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2ShaderBinaryActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2ShaderBinaryActionContext( context );
}
