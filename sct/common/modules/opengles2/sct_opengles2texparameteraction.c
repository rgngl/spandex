/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2texparameteraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2TexParameterActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2TexParameterActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2TexParameterActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2TexParameterActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in TexParameter@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2TexParameterActionContext ) );

    if( sctiParseOpenGLES2TexParameterActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2TexParameterActionContext( context );
        return NULL;
    }


    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2TexParameterActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2TexParameterActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2TexParameterActionContext*  context;
    GLenum                                  err;
    OpenGLES2TexParameterActionData*        data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2TexParameterActionContext* )( action->context );
    data    = &( context->data );

    glTexParameterf( data->target, GL_TEXTURE_MIN_FILTER, ( float )( data->minFilter ) );
    glTexParameterf( data->target, GL_TEXTURE_MAG_FILTER, ( float )( data->magFilter ) );

    glTexParameterf( data->target, GL_TEXTURE_WRAP_S, ( float )( data->wrapS ) );
    glTexParameterf( data->target, GL_TEXTURE_WRAP_T, ( float )( data->wrapT ) );
        
#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in TexParameter@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2TexParameterActionDestroy( SCTAction* action )
{
    SCTOpenGLES2TexParameterActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2TexParameterActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2TexParameterActionContext( context );
}
