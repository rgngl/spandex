/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2createtexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CreateTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CreateTextureDataActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CreateTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CreateTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateTextureData@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CreateTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CreateTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in CreateTextureData@OpenGLES2 context creation." );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in CreateTextureData@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CreateTextureDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CreateTextureDataActionContext* context;
    OpenGLES2CreateTextureDataActionData*       data;
    SCTOpenGLES2Data*                           rawData;
    SCTOpenGLES2TextureData*                    textureData;
    SCTOpenGLES2MipMap*                         mipmap;
    GLenum                                      glFormat;
    GLenum                                      glType;
    int                                         texelSize;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CreateTextureDataActionContext* )( action->context );
    data    = &( context->data );

    if( sctOpenGLES2MapTextureTypeToGL( data->type, &glFormat, &glType ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid type in CreateTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    texelSize = sctOpenGLES2GetTexelSize( data->type );
    if( texelSize < 0 )
    {
        SCT_LOG_ERROR( "Invalid type in CreateTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    rawData = sctiOpenGLES2ModuleGetData( context->moduleContext, data->dataIndex );
    if( rawData == NULL )
    {
        SCT_LOG_ERROR( "Invalid data index in CreateTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( rawData->length != ( data->width * data->height * texelSize ) )
    {
        SCT_LOG_ERROR( "Invalid data in CreateTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    textureData = sctiOpenGLES2ModuleGetTextureData( context->moduleContext, data->textureDataIndex );
    if( textureData != NULL )
    {
        if( textureData->type != data->type ||
            textureData->mipmapCount != 1 ||
            textureData->mipmaps[ 0 ].width != data->width ||
            textureData->mipmaps[ 0 ].height != data->height ||
            textureData->mipmaps[ 0 ].data == NULL )
        {
            SCT_LOG_ERROR( "Texture data index already used in CreateTextureData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        memcpy( textureData->mipmaps[ 0 ].data, rawData->data, rawData->length );
    }
    else
    {
        mipmap = ( SCTOpenGLES2MipMap* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2MipMap ) ) );
        if( mipmap == NULL )
        {
            SCT_LOG_ERROR( "Memory allocation failed in CreateTextureData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        mipmap->width     = data->width;
        mipmap->height    = data->height;
        mipmap->data = ( unsigned char* )( siCommonMemoryAlloc( NULL, rawData->length ) );
        if( mipmap->data == NULL )
        {
            siCommonMemoryFree( NULL, mipmap );
            SCT_LOG_ERROR( "Memory allocation failed in CreateTextureData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        memcpy( mipmap->data, rawData->data, rawData->length );
        
        textureData = ( SCTOpenGLES2TextureData* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2TextureData ) ) );
        if( textureData == NULL )
        {
            siCommonMemoryFree( NULL, mipmap->data );
            siCommonMemoryFree( NULL, mipmap );
            SCT_LOG_ERROR( "Memory allocation failed in CreateTextureData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        textureData->type        = data->type;
        textureData->glFormat    = glFormat;
        textureData->glType      = glType;
        textureData->mipmapCount = 1;
        textureData->mipmaps     = mipmap;

        sctiOpenGLES2ModuleSetTextureData( context->moduleContext, data->textureDataIndex, textureData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateTextureDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES2CreateTextureDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2CreateTextureDataActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteTextureData( context->moduleContext, context->data.textureDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CreateTextureDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CreateTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CreateTextureDataActionContext( context );
}
