/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2loaddataaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2LoadDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2LoadDataActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2LoadDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2LoadDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LoadData@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2LoadDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2LoadDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2LoadDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2LoadDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in LoadData@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2LoadDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2LoadDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2LoadDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2LoadDataActionContext*  context;
    OpenGLES2LoadDataActionData*        data;
    SCTOpenGLES2Data*                   rawData;
    void*                               fileContext;
    SCTFile                             file;
    int                                 fileSize;
    char*                               buffer;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2LoadDataActionContext* )( action->context );
    data    = &( context->data );

    rawData = sctiOpenGLES2ModuleGetData( context->moduleContext, data->dataIndex );

    fileContext = siFileCreateContext();
    if( fileContext == NULL )
    {
        SCT_LOG_ERROR( "Invalid file context in LoadData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    file = siFileOpen( fileContext, data->filename, "rb" );
    if( file == NULL )
    {
        siFileDestroyContext( fileContext );
        SCT_LOG_ERROR( "File open failed in LoadData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    fileSize = siFileSize( fileContext, file );
    if( fileSize < 1 )
    {
        siFileClose( fileContext, file );
        siFileDestroyContext( fileContext );
        SCT_LOG_ERROR( "Invalid file size in LoadData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( rawData != NULL )
    {
        if( rawData->length != fileSize )
        {
            siFileClose( fileContext, file );
            siFileDestroyContext( fileContext );

            SCT_LOG_ERROR( "Data index already used in LoadData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }
        buffer = ( char* )( rawData->data );
    }
    else
    {
        buffer = ( char* )( siCommonMemoryAlloc( NULL, fileSize ) );
        if( buffer == NULL )
        {
            siFileClose( fileContext, file );
            siFileDestroyContext( fileContext );
            SCT_LOG_ERROR( "File buffer alloc failed in LoadData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }
    }

    if( siFileRead( fileContext, file, buffer, fileSize ) != ( unsigned int )( fileSize ) )
    {
        if( rawData == NULL )
        {
            siCommonMemoryFree( NULL, buffer );
        }
        siFileClose( fileContext, file );
        siFileDestroyContext( fileContext );
        SCT_LOG_ERROR( "File read failed in LoadData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    siFileClose( fileContext, file );
    siFileDestroyContext( fileContext );

    if( rawData == NULL )
    {
        rawData = sctOpenGLES2CreateData( buffer, fileSize, SCT_FALSE );
        if( rawData == NULL )
        {
            siCommonMemoryFree( NULL, buffer );
            SCT_LOG_ERROR( "Data creation failed in LoadData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        sctiOpenGLES2ModuleSetData( context->moduleContext, data->dataIndex, rawData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2LoadDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES2LoadDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2LoadDataActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteData( context->moduleContext, context->data.dataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2LoadDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES2LoadDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2LoadDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2LoadDataActionContext( context );
}
