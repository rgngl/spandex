/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2uniformfaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2UniformfActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2UniformfActionContext*  context;
    SCTOpenGLES2ModuleContext*          mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenGLES2ModuleContext* )( moduleContext );
    context = ( SCTOpenGLES2UniformfActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2UniformfActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Uniformf@OpenGLES2 context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2UniformfActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenGLES2UniformfActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2UniformfActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.registerIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid register index in Uniformf@OpenGLES2 context creation." );
        return NULL;      
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2UniformfActionContext( void* context )
{
    SCTOpenGLES2UniformfActionContext* c;

    if( context == NULL )
    {
        return;
    }
    
    c = ( SCTOpenGLES2UniformfActionContext* )( context );
    if( c->data.values != NULL )
    {
        sctDestroyFloatVector( c->data.values );
        c->data.values = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2UniformfActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2UniformfActionContext*  context;
    OpenGLES2UniformfActionData*        data;
    GLenum                              err;
    int                                 location;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2UniformfActionContext* )( action->context );
    data    = &( context->data );

    location = ( int )( sctiOpenGLES2ModuleGetRegisterValue( context->moduleContext, data->registerIndex ) );
    if( location < 0 )
    {
        SCT_LOG_ERROR( "Invalid value in register in Uniformf@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    switch( data->values->length / data->count )
    {
    case 1:
        glUniform1fv( location, data->count, data->values->data );
        break;
    case 2:
        glUniform2fv( location, data->count, data->values->data );
        break;
    case 3:
        glUniform3fv( location, data->count, data->values->data );
        break;
    case 4:
        glUniform4fv( location, data->count, data->values->data );
        break;
    default:
        SCT_LOG_ERROR( "Unsupported number of values in Uniformf@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in Uniformf@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2UniformfActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenGLES2UniformfActionContext( action->context );
    action->context = NULL;
}

