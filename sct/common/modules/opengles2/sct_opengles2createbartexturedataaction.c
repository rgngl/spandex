/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2createbartexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CreateBarTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CreateBarTextureDataActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CreateBarTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CreateBarTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateBarTextureData@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CreateBarTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CreateBarTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateBarTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateBarTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in CreateBarTextureData@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CreateBarTextureDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateBarTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateBarTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CreateBarTextureDataActionContext*  context;
    OpenGLES2CreateBarTextureDataActionData*        data;
    SCTOpenGLES2TextureData*                        textureData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CreateBarTextureDataActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetTextureData( context->moduleContext, data->textureDataIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Texture data index already used in CreateBarTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    textureData = sctOpenGLES2CreateBarTextureData( data->type, 
                                                    data->width, 
                                                    data->height, 
                                                    data->color1, 
                                                    data->color2, 
                                                    data->bars, 
                                                    data->mipmaps );
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Texture data creation failed in CreateBarTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES2ModuleSetTextureData( context->moduleContext, data->textureDataIndex, textureData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateBarTextureDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES2CreateBarTextureDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2CreateBarTextureDataActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteTextureData( context->moduleContext, context->data.textureDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateBarTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CreateBarTextureDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CreateBarTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CreateBarTextureDataActionContext( context );
}
