/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2detachshaderaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2DetachShaderActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2DetachShaderActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2DetachShaderActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2DetachShaderActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DetachShader@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2DetachShaderActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2DetachShaderActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DetachShaderActionContext( context );
        return NULL;
    }

    if(sctiOpenGLES2ModuleIsValidProgramIndex( context->moduleContext, context->data.programIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DetachShaderActionContext( context );
        SCT_LOG_ERROR( "Invalid program index in DetachShader@OpenGLES2 context creation." );
        return NULL;
    }

    if(sctiOpenGLES2ModuleIsValidShaderIndex( context->moduleContext, context->data.shaderIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DetachShaderActionContext( context );
        SCT_LOG_ERROR( "Invalid shader index in DetachShader@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2DetachShaderActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2DetachShaderActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2DetachShaderActionContext*  context;
    GLenum                                  err;
    OpenGLES2DetachShaderActionData*        data;
    SCTOpenGLES2Shader*                     shader;
    SCTOpenGLES2Program*                    program;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2DetachShaderActionContext* )( action->context );
    data    = &( context->data );

    shader = sctiOpenGLES2ModuleGetShader( context->moduleContext, data->shaderIndex );
    if( shader == NULL )
    {
        SCT_LOG_ERROR( "Invalid shader index in AttachShader@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    program = sctiOpenGLES2ModuleGetProgram( context->moduleContext, data->programIndex );
    if( program == NULL )
    {
        SCT_LOG_ERROR( "Invalid program index in AttachShader@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    glDetachShader( program->program, shader->shader );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in DetachShader@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2DetachShaderActionDestroy( SCTAction* action )
{
    SCTOpenGLES2DetachShaderActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2DetachShaderActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2DetachShaderActionContext( context );
}
