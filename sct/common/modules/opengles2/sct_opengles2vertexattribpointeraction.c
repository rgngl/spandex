/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2vertexattribpointeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2VertexAttribPointerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2VertexAttribPointerActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2VertexAttribPointerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2VertexAttribPointerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in VertexAttribPointer@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2VertexAttribPointerActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2VertexAttribPointerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2VertexAttribPointerActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.registerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2VertexAttribPointerActionContext( context );
        SCT_LOG_ERROR( "Invalid register index in VertexAttribPointer@OpenGLES2 context creation." );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2VertexAttribPointerActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in VertexAttribPointer@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2VertexAttribPointerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2VertexAttribPointerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2VertexAttribPointerActionContext*   context;
    OpenGLES2VertexAttribPointerActionData*         data;
    SCTOpenGLES2Array*                              array;
    GLenum                                          err;
    int                                             index;
    GLboolean                                       normalized      = GL_FALSE;
    int                                             offset;    

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2VertexAttribPointerActionContext* )( action->context );
    data    = &( context->data );

    index = ( int )( sctiOpenGLES2ModuleGetRegisterValue( context->moduleContext, data->registerIndex ) );
    if( index < 0 )
    {
        SCT_LOG_ERROR( "Invalid value in register in VertexAttribPointer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    array = sctiOpenGLES2ModuleGetArray( context->moduleContext, data->arrayIndex );
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array index in VertexAttribPointer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( data->offset >= array->length )
    {
        SCT_LOG_ERROR( "Offset out of bounds in VertexAttribPointer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( data->normalized == SCT_TRUE )
    {
        normalized = GL_TRUE;
    }

    offset = data->offset * sctOpenGLES2GetGLTypeSize( array->type ) * data->size;

    glVertexAttribPointer( index, 
                           data->size, 
                           array->type, 
                           normalized, 
                           0,  
                           ( unsigned char* )( array->data ) + offset );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in VertexAttribPointer@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2VertexAttribPointerActionDestroy( SCTAction* action )
{
    SCTOpenGLES2VertexAttribPointerActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2VertexAttribPointerActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2VertexAttribPointerActionContext( context );
}
