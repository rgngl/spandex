/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2deleteshaderaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2DeleteShaderActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2DeleteShaderActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2DeleteShaderActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2DeleteShaderActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DeleteShader@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2DeleteShaderActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2DeleteShaderActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DeleteShaderActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidShaderIndex( context->moduleContext, context->data.shaderIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DeleteShaderActionContext( context );
        SCT_LOG_ERROR( "Invalid shader index in DeleteShader@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2DeleteShaderActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2DeleteShaderActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2DeleteShaderActionContext*  context;
    GLenum                                  err;
    OpenGLES2DeleteShaderActionData*        data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2DeleteShaderActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetShader( context->moduleContext, data->shaderIndex ) == NULL )
    {
        SCT_LOG_ERROR( "Invalid shader index in DeleteShader@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    
    sctiOpenGLES2ModuleDeleteShader( context->moduleContext, data->shaderIndex );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in DeleteShader@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2DeleteShaderActionDestroy( SCTAction* action )
{
    SCTOpenGLES2DeleteShaderActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2DeleteShaderActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2DeleteShaderActionContext( context );
}
