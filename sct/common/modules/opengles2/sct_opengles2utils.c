/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 * Perlin noise code adapted from
 * http://mrl.nyu.edu/~perlin/doc/oscar.html#noise, copyright Ken Perlin.
 *
 * Quality perlin noise code adapted from online discussion at
 * http://www.dreamincode.net/forums/topic/66480-perlin-noise/.
 *
 * This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2utils.h"
#include "sct_utils.h"
#include "sct_sicommon.h"

#include <math.h>
#include <string.h>
#include <stdlib.h>

#define TRIANGLE_LIMIT          ( SCT_MAX_SHORT / 3 )
#define MAX_SHORT               SCT_MAX_SHORT
#define BAR_COUNT               4

/*!
 *
 */
typedef enum
{
    SCT_OPENGLES2_TEXTURE_PATTERN_NULL,
    SCT_OPENGLES2_TEXTURE_PATTERN_SOLID,
    SCT_OPENGLES2_TEXTURE_PATTERN_BAR,
    SCT_OPENGLES2_TEXTURE_PATTERN_CHECKER,
    SCT_OPENGLES2_TEXTURE_PATTERN_VGRADIENT,
    SCT_OPENGLES2_TEXTURE_PATTERN_HGRADIENT,
    SCT_OPENGLES2_TEXTURE_PATTERN_DGRADIENT,
    SCT_OPENGLES2_TEXTURE_PATTERN_ROUND,    
    SCT_OPENGLES2_TEXTURE_PATTERN_RANDOM_NOISE,
    SCT_OPENGLES2_TEXTURE_PATTERN_ORIGINAL_PERLIN_NOISE,
    SCT_OPENGLES2_TEXTURE_PATTERN_LOW_QUALITY_PERLIN_NOISE,
    SCT_OPENGLES2_TEXTURE_PATTERN_HIGH_QUALITY_PERLIN_NOISE,
} SCTOpenGLES2TexturePattern;

/*!
 *
 */
typedef struct
{
    int                         components;
    int                         type;
    unsigned char*              data;
    int                         size;
} SCTOpenGLES2Attrib;

/*!
 *
 *
 */
static SCTOpenGLES2TextureData* sctiOpenGLES2CreateTextureData( SCTOpenGLES2TextureType type, int width, int height, SCTOpenGLES2TexturePattern pattern, const float* parameters, int parameterCount, SCTBoolean mipmaps );
static unsigned char*           sctiCreateSolidTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color[ 4 ] );
static unsigned char*           sctiCreateBarTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int barCount );
static unsigned char*           sctiCreateCheckerTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int checkerCountX, int checkerCountY  );
static unsigned char*           sctiCreateGradientTextureData( SCTOpenGLES2TextureType type, int width, int height, SCTOpenGLES2TexturePattern gradient, const float color1[ 4 ], const float color2[ 4 ] );
static unsigned char*           sctiCreateRoundTextureData( SCTOpenGLES2TextureType type, int size, const float color1[ 4 ], const float color2[ 4 ] );
static unsigned char*           sctiCreateNoiseTextureData( SCTOpenGLES2TextureType type, int width, int height, SCTOpenGLES2TexturePattern noise, const float color[ 4 ], const float* parameters, int parameterCount );
static SCTBoolean               sctiIsValidTextureType( SCTOpenGLES2TextureType type );
static void                     sctiGetTextureTypeSizeAndElements( SCTOpenGLES2TextureType type, int* size, int* elements );
static void                     sctiAssignTextureColor( SCTOpenGLES2TextureType type, void* dest, const float* color );

static void*                    sctiGetMeshAttrib( SCTOpenGLES2Mesh* mesh, int arrayIndex, int attributeIndex );
static SCTOpenGLES2Mesh*        sctiOpenGLES2CreateEmptyMesh( SCTOpenGLES2Attrib* attributes, int attributesLength, int attributeCount );

static double                   sctiOriginalNoise2( double vec[ 2 ] );
static double                   sctiSmooth( double x, double y );
static double                   sctiNearest( double x, double y );
static double                   sctiNoise( int x, int y );
static double                   sctiInterpolate( double x, double y, double a );
static double                   sctiQualityPerlinNoise2D( int x, int y, double frequency, double persistence, int octaves, double amplitude, double materialCoverage, double materialDensity, SCTBoolean hq );
static double                   sctiOriginalPerlinNoise2D( double x, double y, double alpha, double beta, int n );
static double                   sctiLowQualityPerlinNoise2D( int x, int y, double frequency, double persistence, int octaves, double amplitude, double materialCoverage, double materialDensity );
static double                   sctiHighQualityPerlinNoise2D( int x, int y, double frequency, double persistence, int octaves, double amplitude, double materialCoverage, double materialDensity );
static double                   sctiOriginalPerlinNoise2D( double x, double y, double alpha, double beta, int n );
/*!
 *
 */
static unsigned char* sctiCreateSolidTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color[ 4 ] ) 
{
    int                 size        = 0;
    int                 elements    = 0;
    unsigned char*      buf;
    unsigned char*      bluf;
    int                 x, y;

    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color != NULL );

    sctiGetTextureTypeSizeAndElements( type, &size, &elements );
    SCT_ASSERT( size != 0 && elements != 0 );

    /* Reserve memory for texture data */
    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, size * width * height ) );
    if( buf == NULL )
    {
        return NULL;
    }
    bluf = buf;

    /* Fill in the texture data */
    for( y = 0; y < height; ++y )
    {
        for( x = 0; x < width; ++x )
        {
            sctiAssignTextureColor( type, bluf, color );
            bluf += size;
        }
    }

    return buf;
}

/*!
 *
 *
 */
static unsigned char* sctiCreateBarTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int barCount ) 
{
    int                 size        = 0;
    int                 elements    = 0;
    unsigned char*      buf;
    unsigned char*      bluf;
    int                 x, y;
    int                 d;

    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color1 != NULL );
    SCT_ASSERT( color2 != NULL );
    SCT_ASSERT( barCount > 0 );

    /* Calculate the bar width in texels. Make sure bar width is at least 1 */
    d = width / barCount;
    d = d ? d : 1;

    sctiGetTextureTypeSizeAndElements( type, &size, &elements );
    SCT_ASSERT( size != 0 && elements != 0 );

    /* Reserve memory for texture data */
    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, size * width * height ) );
    if( buf == NULL )
    {
        return NULL;
    }
    bluf = buf;

    /* Fill in the texture data */
    for( y = 0; y < height; ++y )
    {
        for( x = 0; x < width; ++x )
        {
            sctiAssignTextureColor( type, bluf, ( ( x / d ) % 2 ) ? color1 : color2 );
            bluf += size;
        }
    }

    return buf;
}

/*!
 *
 *
 */
static unsigned char* sctiCreateCheckerTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int checkerCountX, int checkerCountY ) 
{
    int                 size        = 0;
    int                 elements    = 0;
    unsigned char*      buf;
    unsigned char*      bluf;
    int                 x, y;
    int                 dx, dy;
    const float*        color;

    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color1 != NULL );
    SCT_ASSERT( color2 != NULL );
    SCT_ASSERT( checkerCountX > 0 );
    SCT_ASSERT( checkerCountY > 0 );

    dx = width / checkerCountX;
    dx = dx ? dx : 1;
    dy = height / checkerCountY;
    dy = dy ? dy : 1;

    sctiGetTextureTypeSizeAndElements( type, &size, &elements );
    SCT_ASSERT( size != 0 && elements != 0 );

    /* Reserve memory for texture data */
    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, size * width * height ) );
    if( buf == NULL )
    {
        return NULL;
    }
    bluf = buf;

    /* Fill in the texture data */
    for( y = 0; y < height; ++y )
    {
        for( x = 0; x < width; ++x )
        {
            if( ( x / dx ) % 2 )
            {
                if( ( y / dy ) % 2 )
                {
                    color = color1;
                }
                else 
                {
                    color = color2;
                }
            }
            else
            {
                if( ( y / dy ) % 2 )
                {
                    color = color2;
                }
                else
                {
                    color = color1;
                }
            }
            sctiAssignTextureColor( type, bluf, color );
            bluf += size;
        }
    }

    return buf;
}

/*!
 *
 *
 */
static unsigned char* sctiCreateRoundTextureData( SCTOpenGLES2TextureType type, int size, const float color1[ 4 ], const float color2[ 4 ] ) 
{
    int                 sz          = 0;
    int                 elements    = 0;
    unsigned char*      buf;
    unsigned char*      bluf;
    int                 x, y;
    float               color[ 4 ];
    float               dx;
    float               dy;    
    float               r;
    float               d;
    float               rr;
    
    SCT_ASSERT( size > 0 );
    SCT_ASSERT( color1 != NULL );
    SCT_ASSERT( color2 != NULL );

    sctiGetTextureTypeSizeAndElements( type, &sz, &elements );
    SCT_ASSERT( sz != 0 && elements != 0 );

    /* Reserve memory for texture data */
    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, sz * size * size ) );
    if( buf == NULL )
    {
        return NULL;
    }
    bluf = buf;

    r = size / 2.0f;
    for( y = 0; y < size; ++y )
    {
        for( x = 0; x < size; ++x )
        {
            dx = ( float )( x - size / 2.0f );
            dy = ( float )( y - size / 2.0f );
            d = ( float )( sqrt( dx * dx + dy * dy ) );
            
            rr = r - d;
            rr = sctMinf( 1.0f, rr );
            rr = sctMaxf( 0.0f, rr );            
            
            color[ 0 ] = rr * color1[ 0 ] + ( 1 - rr ) * color2[ 0 ];
            color[ 1 ] = rr * color1[ 1 ] + ( 1 - rr ) * color2[ 1 ];
            color[ 2 ] = rr * color1[ 2 ] + ( 1 - rr ) * color2[ 2 ];
            color[ 3 ] = rr * color1[ 3 ] + ( 1 - rr ) * color2[ 3 ];             

            sctiAssignTextureColor( type, bluf, color );
            bluf += sz;
        }
    }

    return buf;
}

/*!
 *
 *
 */
static unsigned char* sctiCreateGradientTextureData( SCTOpenGLES2TextureType type, int width, int height, SCTOpenGLES2TexturePattern gradient, const float color1[ 4 ], const float color2[ 4 ] ) 
{
    int                 size        = 0;
    int                 elements    = 0;
    unsigned char*      buf;
    unsigned char*      bluf;
    int                 x, y;
    float               d           = 0.0;
    float               r;
    float               color[ 4 ];

    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color1 != NULL );
    SCT_ASSERT( color2 != NULL );
       
    sctiGetTextureTypeSizeAndElements( type, &size, &elements );
    SCT_ASSERT( size != 0 && elements != 0 );

    /* Reserve memory for texture data */
    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, size * width * height ) );
    if( buf == NULL )
    {
        return NULL;
    }
    bluf = buf;

    if( gradient == SCT_OPENGLES2_TEXTURE_PATTERN_HGRADIENT )
    {
        if( width > 1 )
        {
            d = 1.0f / ( float )( width - 1 );
        }
        for( y = 0; y < height; ++y )
        {
            r = 0;            
            for( x = 0; x < width; ++x )
            {
                color[ 0 ] = color1[ 0 ] * ( 1 - r ) + color2[ 0 ] * ( r );
                color[ 1 ] = color1[ 1 ] * ( 1 - r ) + color2[ 1 ] * ( r );
                color[ 2 ] = color1[ 2 ] * ( 1 - r ) + color2[ 2 ] * ( r );
                color[ 3 ] = color1[ 3 ] * ( 1 - r ) + color2[ 3 ] * ( r );                
                sctiAssignTextureColor( type, bluf, color );
                bluf += size;
                r += d;
            }
        }
    }
    else if( gradient == SCT_OPENGLES2_TEXTURE_PATTERN_VGRADIENT )    
    {
        if( height > 1 )
        {
            d = 1.0f / ( float )( height - 1 );
        }
        r = 0;
        
        for( y = 0; y < height; ++y )
        {
            color[ 0 ] = color1[ 0 ] * ( 1 - r ) + color2[ 0 ] * ( r );
            color[ 1 ] = color1[ 1 ] * ( 1 - r ) + color2[ 1 ] * ( r );
            color[ 2 ] = color1[ 2 ] * ( 1 - r ) + color2[ 2 ] * ( r );
            color[ 3 ] = color1[ 3 ] * ( 1 - r ) + color2[ 3 ] * ( r );                
            
            for( x = 0; x < width; ++x )
            {
                sctiAssignTextureColor( type, bluf, color );
                bluf += size;
            }
            r += d;
        }
    }
    else if( gradient == SCT_OPENGLES2_TEXTURE_PATTERN_DGRADIENT )
    {
        d = 1.0f / ( float )( width * height - 1 );
        r = 0;
        
        for( y = 0; y < height; ++y )
        {
            for( x = 0; x < width; ++x )
            {
                r = y * x * d;
                
                color[ 0 ] = color1[ 0 ] * ( 1 - r ) + color2[ 0 ] * ( r );
                color[ 1 ] = color1[ 1 ] * ( 1 - r ) + color2[ 1 ] * ( r );
                color[ 2 ] = color1[ 2 ] * ( 1 - r ) + color2[ 2 ] * ( r );
                color[ 3 ] = color1[ 3 ] * ( 1 - r ) + color2[ 3 ] * ( r );                
                
                sctiAssignTextureColor( type, bluf, color );
                bluf += size;
            }
        }
    }
    else
    {
        siCommonMemoryFree( NULL, buf );
        return NULL;
    }
    
    return buf;
}

/*!
 *
 */
static unsigned char* sctiCreateNoiseTextureData( SCTOpenGLES2TextureType type, int width, int height, SCTOpenGLES2TexturePattern noise, const float color[ 4 ], const float* parameters, int parameterCount ) 
{
    int                 size        = 0;
    int                 elements    = 0;
    unsigned char*      buf;
    unsigned char*      bluf;
    int                 x, y;
    double              c;
    double              cc[ 4 ];    
    float               clr[ 4 ];
    int                 mt_index    = 0;
    void*               mtContext   = NULL;
        
    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color != NULL );

    sctiGetTextureTypeSizeAndElements( type, &size, &elements );
    SCT_ASSERT( size != 0 && elements != 0 );

    SCT_ASSERT( noise != SCT_OPENGLES2_TEXTURE_PATTERN_RANDOM_NOISE || parameterCount == 2  );    
    SCT_ASSERT( noise != SCT_OPENGLES2_TEXTURE_PATTERN_ORIGINAL_PERLIN_NOISE || parameterCount == 3  );
    SCT_ASSERT( noise != SCT_OPENGLES2_TEXTURE_PATTERN_LOW_QUALITY_PERLIN_NOISE || parameterCount == 6 );
    SCT_ASSERT( noise != SCT_OPENGLES2_TEXTURE_PATTERN_HIGH_QUALITY_PERLIN_NOISE || parameterCount == 6 );
    
    /* Reserve memory for texture data */
    buf = ( unsigned char* )( siCommonMemoryAlloc( NULL, size * width * height ) );
    if( buf == NULL )
    {
        return NULL;
    }
    bluf = buf;

    if( noise == SCT_OPENGLES2_TEXTURE_PATTERN_RANDOM_NOISE )
    {
        mtContext = sctCreateMerseneTwisterContext();
        if( mtContext == NULL )
        {
            siCommonMemoryFree( NULL, buf );
            return NULL;
        }

        /* Use parameter to skip some random values. */
        for( x = 0; x < ( int )( parameters[ 1 ] ); ++x )
        {
            sctMerseneTwister( &mt_index, mtContext );
        }
    }
        
    for( y = 0; y < height; ++y )
    {
        for( x = 0; x < width; ++x )
        {
            /* Generating other than random per-pixel noise seems already so
             * slow that adding switch into pixel loop should not add too
             * much. */
            switch( noise )
            {
            case SCT_OPENGLES2_TEXTURE_PATTERN_RANDOM_NOISE:
                if( ( int )( parameters[ 0 ] ) == 0 )
                {
                    c = sctMerseneTwister( &mt_index, mtContext );
                    cc[ 0 ] = c;  cc[ 1 ] = c;  cc[ 2 ] = c;  cc[ 3 ] = c; 
                }
                else
                {
                    cc[ 0 ] = sctMerseneTwister( &mt_index, mtContext );
                    cc[ 1 ] = sctMerseneTwister( &mt_index, mtContext );
                    cc[ 2 ] = sctMerseneTwister( &mt_index, mtContext );
                    cc[ 3 ] = sctMerseneTwister( &mt_index, mtContext );
                }
                    
                break;
                
            case SCT_OPENGLES2_TEXTURE_PATTERN_ORIGINAL_PERLIN_NOISE:
                c = sctiOriginalPerlinNoise2D( x / ( double )( width ), y / ( double )( height ), parameters[ 0 ], parameters[ 1 ], ( int )( parameters[ 2 ] ) );
                cc[ 0 ] = c;  cc[ 1 ] = c;  cc[ 2 ] = c;  cc[ 3 ] = c;                 
                break;
                
            case SCT_OPENGLES2_TEXTURE_PATTERN_LOW_QUALITY_PERLIN_NOISE:
                c = sctiLowQualityPerlinNoise2D( x, y, parameters[ 0 ], parameters[ 1 ], ( int )( parameters[ 2 ] ), parameters[ 3 ], parameters[ 4 ], parameters[ 5 ] );
                cc[ 0 ] = c;  cc[ 1 ] = c;  cc[ 2 ] = c;  cc[ 3 ] = c; 
                break;

            case SCT_OPENGLES2_TEXTURE_PATTERN_HIGH_QUALITY_PERLIN_NOISE:
                c = sctiHighQualityPerlinNoise2D( x, y, parameters[ 0 ], parameters[ 1 ], ( int )( parameters[ 2 ] ), parameters[ 3 ], parameters[ 4 ], parameters[ 5 ] );
                    cc[ 0 ] = c;  cc[ 1 ] = c;  cc[ 2 ] = c;  cc[ 3 ] = c;                 
                break;
            default:
                SCT_ASSERT( 0 );
                c = 0;
                cc[ 0 ] = c;  cc[ 1 ] = c;  cc[ 2 ] = c;  cc[ 3 ] = c; 
                break;
            }
            
            clr[ 0 ] = ( float )( color[ 0 ] * cc[ 0 ] );
            clr[ 1 ] = ( float )( color[ 1 ] * cc[ 1 ] );
            clr[ 2 ] = ( float )( color[ 2 ] * cc[ 2 ] );
            clr[ 3 ] = ( float )( color[ 3 ] * cc[ 3 ] );
            sctiAssignTextureColor( type, bluf, clr );
            bluf += size;
        }
    }        

    sctDestroyMerseneTwisterContext( mtContext );
    
    return buf;
}

/*!
 *
 */
static SCTBoolean sctiIsValidTextureType( SCTOpenGLES2TextureType type )
{
    if( type == OPENGLES2_LUMINANCE8        || 
        type == OPENGLES2_ALPHA8            || 
        type == OPENGLES2_LUMINANCE_ALPHA88 ||
        type == OPENGLES2_RGB565            || 
        type == OPENGLES2_RGB888            || 
        type == OPENGLES2_RGBA4444          ||
        type == OPENGLES2_RGBA5551          || 
        type == OPENGLES2_RGBA8888          || 
        type == OPENGLES2_RGBA8888          ||
        type == OPENGLES2_BGRA8888          ||
        type == OPENGLES2_DEPTH             ||
        type == OPENGLES2_DEPTH32           ||
        type == OPENGLES2_DEPTH24_STENCIL8 )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 */
static void sctiGetTextureTypeSizeAndElements( SCTOpenGLES2TextureType type, int* size, int* elements )
{
    SCT_ASSERT( sctiIsValidTextureType( type ) == SCT_TRUE );
    SCT_ASSERT( size != NULL );
    SCT_ASSERT( elements != NULL );
    SCT_USE_VARIABLE( sctiIsValidTextureType );
    
    switch( type )
    {
    case OPENGLES2_LUMINANCE8:
    case OPENGLES2_ALPHA8:
        *size     = sizeof( char );
        *elements = 1;
        break;

    case OPENGLES2_LUMINANCE_ALPHA88:
        *size     = sizeof( char ) * 2;
        *elements = 2;
        break;

    case OPENGLES2_RGB565:
        *size     = sizeof( char ) * 2;
        *elements = 3;
        break;

    case OPENGLES2_RGBA4444:
    case OPENGLES2_RGBA5551:
        *size     = sizeof( char ) * 2;
        *elements = 4;
        break;

    case OPENGLES2_RGB888:
        *size     = sizeof( char ) * 3;
        *elements = 3;
        break;

    case OPENGLES2_RGBA8888:
    case OPENGLES2_BGRA8888:        
        *size     = sizeof( char ) * 4;
        *elements = 4;
        break;

    case OPENGLES2_DEPTH:
        *size     = sizeof( char ) * 2;
        *elements = 1;
        break;

    case OPENGLES2_DEPTH32:
        *size     = sizeof( char ) * 4;
        *elements = 1;
        break;
        
    case OPENGLES2_DEPTH24_STENCIL8:
        *size     = sizeof( char ) * 4;
        *elements = 2;
        break;
    }
}

/*!
 *
 */
void sctiAssignTextureColor( SCTOpenGLES2TextureType type, void* dest, const float* color )
{
    unsigned char  *cp = ( unsigned char* )( dest );
    unsigned short *sp = ( unsigned short* )( dest );
    unsigned int   *ip = ( unsigned int* )( dest );    
    unsigned short  s;
    unsigned int    i;

    SCT_ASSERT( sctiIsValidTextureType( type ) == SCT_TRUE );    
    SCT_ASSERT( dest != NULL );
    SCT_ASSERT( color != NULL );
    SCT_USE_VARIABLE( sctiIsValidTextureType );
    
    switch( type )
    {
    case OPENGLES2_LUMINANCE8:
        *cp   = ( unsigned char )( color[ 0 ] * 255 );
        break;
        
    case OPENGLES2_ALPHA8:
        *cp   = ( unsigned char )( color[ 3 ] * 255 );
        break;
        
    case OPENGLES2_LUMINANCE_ALPHA88:
        *cp++ = ( unsigned char )( color[ 0 ] * 255 );
        *cp   = ( unsigned char )( color[ 3 ] * 255 );
        break;

    case OPENGLES2_RGB565:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 0 ] * 31 ) << 11 ) & 0xF800 ) | 
            ( ( ( unsigned int )( color[ 1 ] * 63 ) << 5 )  & 0x07E0 ) |
            (   ( unsigned int )( color[ 2 ] * 31 )         & 0x001F ) );
        *sp = s;
        break;

    case OPENGLES2_RGBA4444:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 0 ] * 15 ) << 12 ) & 0xF000 ) | 
            ( ( ( unsigned int )( color[ 1 ] * 15 ) << 8 )  & 0x0F00 ) |
            ( ( ( unsigned int )( color[ 2 ] * 15 ) << 4 )  & 0x00F0 ) |
              ( ( unsigned int )( color[ 3 ] * 15 )         & 0x000F ) );
        *sp = s;
        break;

    case OPENGLES2_RGBA5551:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 0 ] * 31 ) << 11 ) & 0xF800 ) | 
            ( ( ( unsigned int )( color[ 1 ] * 31 ) << 6 )  & 0x07C0 ) |
            ( ( ( unsigned int )( color[ 2 ] * 31 ) << 1 )  & 0x003E ) |
              ( ( unsigned int )( color[ 3 ] + 0.5f )       & 0x0001 ) );
        *sp = s;
        break;

    case OPENGLES2_RGB888:
        *cp++ = ( unsigned char )( color[ 0 ] * 255 );
        *cp++ = ( unsigned char )( color[ 1 ] * 255 );
        *cp   = ( unsigned char )( color[ 2 ] * 255 );
        break;

    case OPENGLES2_RGBA8888:
        *cp++ = ( unsigned char )( color[ 0 ] * 255 );
        *cp++ = ( unsigned char )( color[ 1 ] * 255 );
        *cp++ = ( unsigned char )( color[ 2 ] * 255 );
        *cp   = ( unsigned char )( color[ 3 ] * 255 );
        break;

    case OPENGLES2_BGRA8888:
        *cp++ = ( unsigned char )( color[ 2 ] * 255 );
        *cp++ = ( unsigned char )( color[ 1 ] * 255 );
        *cp++ = ( unsigned char )( color[ 0 ] * 255 );
        *cp   = ( unsigned char )( color[ 3 ] * 255 );
        break;
        
    case OPENGLES2_DEPTH:
        s = ( unsigned short )( ( color[ 0 ] * 0xFFFF ) );
        *sp = s;
        break;

    case OPENGLES2_DEPTH32:
        i = ( unsigned int )( ( color[ 0 ] * 0xFFFFFFFF ) );
        *ip = i;
        break;
        
    case OPENGLES2_DEPTH24_STENCIL8:
        i = ( unsigned int )(
            ( ( ( unsigned int )( color[ 0 ] * 0x00FFFFFF ) << 8 ) & 0xFFFFFF00 ) |
              ( ( unsigned int )( color[ 1 ] * 0x000000FF )        & 0x000000FF ) );
        *ip = i;
        break;        
    }
}

/*!
 *
 *
 */
int sctOpenGLES2GetMipmapLevels( int width, int height )
{
    int         w       = width;
    int         h       = height;
    int         levels   = 0;

    SCT_ASSERT( width > 0 );
    SCT_ASSERT( height > 0 );

    for( ; ( w != 0 ) || ( h != 0 ) ; )
    {
        w = w / 2;
        h = h / 2; 
        ++levels;
    }

    return levels;
}

/*!
 *
 */
static SCTOpenGLES2TextureData* sctiOpenGLES2CreateTextureData( SCTOpenGLES2TextureType type, int width, int height, SCTOpenGLES2TexturePattern pattern, const float* parameters, int parameterCount, SCTBoolean mipmaps )
{
    int                         mipmapLevels    = 1;
    int                         w               = width;
    int                         h               = height;
    int                         i;
    SCTOpenGLES2TextureData*    textureData;

    SCT_ASSERT( sctiIsValidTextureType( type ) == SCT_TRUE );
    SCT_ASSERT( width > 0 );
    SCT_ASSERT( height > 0 );
    SCT_ASSERT( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_NULL || ( parameterCount == 0 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_SOLID || ( parameterCount == 4 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_BAR || ( parameterCount == 9 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_CHECKER || ( parameterCount == 10 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_VGRADIENT || ( parameterCount == 8 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_HGRADIENT || ( parameterCount == 8 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_ROUND || ( parameterCount == 8 ) );    
    SCT_ASSERT( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_RANDOM_NOISE || ( parameterCount == 6 ) );    
    SCT_ASSERT( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_ORIGINAL_PERLIN_NOISE || ( parameterCount == 7 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_LOW_QUALITY_PERLIN_NOISE || ( parameterCount == 10 ) );
    SCT_ASSERT( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_HIGH_QUALITY_PERLIN_NOISE || ( parameterCount == 10 ) );
    SCT_USE_VARIABLE( sctiIsValidTextureType );
    
    textureData = ( SCTOpenGLES2TextureData* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2TextureData ) ) );
    if( textureData == NULL )
    {
        return NULL;
    }
    memset( textureData, 0, sizeof( SCTOpenGLES2TextureData ) );

    textureData->type = type;
    
    if( sctOpenGLES2MapTextureTypeToGL( type, &( textureData->glFormat ), &( textureData->glType ) ) == SCT_FALSE )
    {
        sctOpenGLES2DestroyTextureData( textureData );
        return NULL;
    }

    /* Calculate number of mipmaps, if mipmap generation is enabled */
    if( mipmaps == SCT_TRUE )
    {
        /* Depth texture does not support mipmaps */
        if( type == OPENGLES2_DEPTH || type == OPENGLES2_DEPTH32 || type == OPENGLES2_DEPTH24_STENCIL8 )
        {
            sctOpenGLES2DestroyTextureData( textureData );
            return NULL;
        }

        mipmapLevels = sctOpenGLES2GetMipmapLevels( width, height );
    }

    /* Reserve memory for mipmap data structures */
    textureData->mipmaps = ( SCTOpenGLES2MipMap* )( siCommonMemoryAlloc( NULL, mipmapLevels * sizeof( SCTOpenGLES2MipMap ) ) );
    if( textureData->mipmaps == NULL )
    {       
        sctOpenGLES2DestroyTextureData( textureData );
        return NULL;
    }
    memset( textureData->mipmaps, 0, mipmapLevels * sizeof( SCTOpenGLES2MipMap ) );
    textureData->mipmapCount = mipmapLevels;

    /* Create texture data for each mipmap level */
    for( i = 0; i < mipmapLevels; ++i )
    {
        textureData->mipmaps[ i ].width     = w;
        textureData->mipmaps[ i ].height    = h;
        switch( pattern )
        {
        default:
        case SCT_OPENGLES2_TEXTURE_PATTERN_NULL:
            textureData->mipmaps[ i ].data = NULL;
            break;

        case SCT_OPENGLES2_TEXTURE_PATTERN_SOLID:
            textureData->mipmaps[ i ].data  = sctiCreateSolidTextureData( type, w, h, &parameters[ 0 ] );
            break;

        case SCT_OPENGLES2_TEXTURE_PATTERN_BAR:
            textureData->mipmaps[ i ].data  = sctiCreateBarTextureData( type, w, h, &parameters[ 0 ], &parameters[ 4 ], ( int )( parameters[ 8 ] ) );
            break;

        case SCT_OPENGLES2_TEXTURE_PATTERN_CHECKER:
            textureData->mipmaps[ i ].data  = sctiCreateCheckerTextureData( type, w, h, &parameters[ 0 ], &parameters[ 4 ], ( int )( parameters[ 8 ] ), ( int )( parameters[ 9 ] ) );
            break;

        case SCT_OPENGLES2_TEXTURE_PATTERN_ROUND:
            textureData->mipmaps[ i ].data  = sctiCreateRoundTextureData( type, w, &parameters[ 0 ], &parameters[ 4 ] );
            break;
            
        case SCT_OPENGLES2_TEXTURE_PATTERN_RANDOM_NOISE:            
        case SCT_OPENGLES2_TEXTURE_PATTERN_ORIGINAL_PERLIN_NOISE:        /* Intentional */
        case SCT_OPENGLES2_TEXTURE_PATTERN_LOW_QUALITY_PERLIN_NOISE:     /* Intentional */
        case SCT_OPENGLES2_TEXTURE_PATTERN_HIGH_QUALITY_PERLIN_NOISE:    /* Intentional */
            textureData->mipmaps[ i ].data  = sctiCreateNoiseTextureData( type, w, h, pattern, &parameters[ 0 ], &parameters[ 4 ], parameterCount - 4 );
            break;
            
        case SCT_OPENGLES2_TEXTURE_PATTERN_VGRADIENT:   /* Intentional */
        case SCT_OPENGLES2_TEXTURE_PATTERN_HGRADIENT:   /* Intentional */
        case SCT_OPENGLES2_TEXTURE_PATTERN_DGRADIENT:   /* Intentional */
            textureData->mipmaps[ i ].data  = sctiCreateGradientTextureData( type, w, h, pattern, &parameters[ 0 ], &parameters[ 4 ] );
            break;
        }

        if( pattern != SCT_OPENGLES2_TEXTURE_PATTERN_NULL && textureData->mipmaps[ i ].data == NULL )
        {
            sctOpenGLES2DestroyTextureData( textureData );
            return NULL;
        }

        /* Divide w and h by 2 if they larger than 1 */
        w = w > 1 ? w / 2 : w;
        h = h > 1 ? h / 2 : h;

        SCT_ASSERT( w >= 1 );
        SCT_ASSERT( h >= 1 );
    }

    return textureData;
}

/*!
 *
 */
SCTOpenGLES2TextureData* sctOpenGLES2CreateNullTextureData( SCTOpenGLES2TextureType type, int width, int height, SCTBoolean mipmaps )
{
    return sctiOpenGLES2CreateTextureData( type, width, height, SCT_OPENGLES2_TEXTURE_PATTERN_NULL, NULL, 0, mipmaps );
}

/*!
 *
 */
SCTOpenGLES2TextureData* sctOpenGLES2CreateSolidTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color[ 4 ], SCTBoolean mipmaps )
{
    return sctiOpenGLES2CreateTextureData( type, width, height, SCT_OPENGLES2_TEXTURE_PATTERN_SOLID, color, 4, mipmaps );
}

/*!
 *
 */
SCTOpenGLES2TextureData* sctOpenGLES2CreateBarTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int bars, SCTBoolean mipmaps )
{
    float params[ 9 ];
    
    params[ 0 ] = color1[ 0 ];
    params[ 1 ] = color1[ 1 ];
    params[ 2 ] = color1[ 2 ];
    params[ 3 ] = color1[ 3 ];
    params[ 4 ] = color2[ 0 ];
    params[ 5 ] = color2[ 1 ];
    params[ 6 ] = color2[ 2 ];
    params[ 7 ] = color2[ 3 ];
    params[ 8 ] = ( float )( bars );
    
    return sctiOpenGLES2CreateTextureData( type, width, height, SCT_OPENGLES2_TEXTURE_PATTERN_BAR, params, 9, mipmaps );
}

/*!
 *
 */
SCTOpenGLES2TextureData* sctOpenGLES2CreateCheckerTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int checkerX, int checkerY, SCTBoolean mipmaps )
{
    float params[ 10 ];
    
    params[ 0 ] = color1[ 0 ];
    params[ 1 ] = color1[ 1 ];
    params[ 2 ] = color1[ 2 ];
    params[ 3 ] = color1[ 3 ];
    params[ 4 ] = color2[ 0 ];
    params[ 5 ] = color2[ 1 ];
    params[ 6 ] = color2[ 2 ];
    params[ 7 ] = color2[ 3 ];
    params[ 8 ] = ( float )( checkerX );
    params[ 9 ] = ( float )( checkerY );
    
    return sctiOpenGLES2CreateTextureData( type, width, height, SCT_OPENGLES2_TEXTURE_PATTERN_CHECKER, params, 10, mipmaps );
}

/*!
 *
 */
SCTOpenGLES2TextureData* sctOpenGLES2CreateRoundTextureData( SCTOpenGLES2TextureType type, int size, const float color1[ 4 ], const float color2[ 4 ], SCTBoolean mipmaps )
{
    float params[ 8 ];
    
    params[ 0 ] = color1[ 0 ];
    params[ 1 ] = color1[ 1 ];
    params[ 2 ] = color1[ 2 ];
    params[ 3 ] = color1[ 3 ];
    params[ 4 ] = color2[ 0 ];
    params[ 5 ] = color2[ 1 ];
    params[ 6 ] = color2[ 2 ];
    params[ 7 ] = color2[ 3 ];
    
    return sctiOpenGLES2CreateTextureData( type, size, size, SCT_OPENGLES2_TEXTURE_PATTERN_ROUND, params, 8, mipmaps );
}

/*!
 *
 */
SCTOpenGLES2TextureData* sctOpenGLES2CreateGradientTextureData( SCTOpenGLES2TextureType type, SCTOpenGLES2Gradient gradient, int width, int height, const float color1[ 4 ], const float color2[ 4 ], SCTBoolean mipmaps )
{
    SCTOpenGLES2TexturePattern  g;
    float                       params[ 8 ];
    
    params[ 0 ] = color1[ 0 ];
    params[ 1 ] = color1[ 1 ];
    params[ 2 ] = color1[ 2 ];
    params[ 3 ] = color1[ 3 ];
    params[ 4 ] = color2[ 0 ];
    params[ 5 ] = color2[ 1 ];
    params[ 6 ] = color2[ 2 ];
    params[ 7 ] = color2[ 3 ];
    
    switch( gradient )
    {
    case OPENGLES2_GRADIENT_VERTICAL:
        g = SCT_OPENGLES2_TEXTURE_PATTERN_VGRADIENT;
        break;

    case OPENGLES2_GRADIENT_HORIZONTAL:
        g = SCT_OPENGLES2_TEXTURE_PATTERN_HGRADIENT;
        break;

    case OPENGLES2_GRADIENT_DIAGONAL:
        g = SCT_OPENGLES2_TEXTURE_PATTERN_DGRADIENT;
        break;
        
    default:
        return NULL;

    }
    return sctiOpenGLES2CreateTextureData( type, width, height, g, params, 8, mipmaps );
}

/*!
 *
 */
SCTOpenGLES2TextureData* sctOpenGLES2CreateNoiseTextureData( SCTOpenGLES2TextureType type, SCTOpenGLES2Noise noise, int width, int height, const float* parameters, int parameterCount, SCTBoolean mipmaps )
{
    SCTOpenGLES2TexturePattern  n;
        
    switch( noise )
    {
    case OPENGLES2_NOISE_RANDOM:
        n = SCT_OPENGLES2_TEXTURE_PATTERN_RANDOM_NOISE;
        break;
        
    case OPENGLES2_NOISE_ORIGINAL_PERLIN:
        n = SCT_OPENGLES2_TEXTURE_PATTERN_ORIGINAL_PERLIN_NOISE;
        break;

    case OPENGLES2_NOISE_LOW_QUALITY_PERLIN:
        n = SCT_OPENGLES2_TEXTURE_PATTERN_LOW_QUALITY_PERLIN_NOISE;
        break;

    case OPENGLES2_NOISE_HIGH_QUALITY_PERLIN:
        n = SCT_OPENGLES2_TEXTURE_PATTERN_HIGH_QUALITY_PERLIN_NOISE;
        break;
        
    default:
        return NULL;

    }
    
    return sctiOpenGLES2CreateTextureData( type, width, height, n, parameters, parameterCount, mipmaps );    
}

/*!
 *
 */
void sctOpenGLES2DestroyTextureData( SCTOpenGLES2TextureData* textureData )
{
    int i;

    if( textureData != NULL )
    {
        for( i = 0; i < textureData->mipmapCount; ++i )
        {
            SCT_ASSERT_ALWAYS( textureData->mipmaps != NULL );
            if( textureData->mipmaps[ i ].data != NULL )
            {
                siCommonMemoryFree( NULL, textureData->mipmaps[ i ].data );
            }
        }
        if( textureData->mipmaps != NULL )
        {
            siCommonMemoryFree( NULL, textureData->mipmaps );
        }
        siCommonMemoryFree( NULL, textureData );
    }
}

/*!
 *
 */
SCTOpenGLES2CompressedTextureData* sctOpenGLES2CreateCompressedTextureData( GLenum internalFormat, SCTOpenGLES2CompressedMipMap* mipmaps, int mipmapCount )
{
    SCTOpenGLES2CompressedTextureData*  compressedTextureData;

    SCT_ASSERT( mipmaps != NULL );
    SCT_ASSERT( mipmapCount > 0 );

    compressedTextureData = ( SCTOpenGLES2CompressedTextureData* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CompressedTextureData ) ) );
    if( compressedTextureData == NULL )
    {
        return NULL;
    }

    compressedTextureData->internalFormat       = internalFormat;
    compressedTextureData->mipmaps              = mipmaps;
    compressedTextureData->mipmapCount          = mipmapCount;

    return compressedTextureData;
}

/*!
 *
 */
void sctOpenGLES2DestroyCompressedMipMapData( SCTOpenGLES2CompressedMipMap* mipmaps, int mipmapCount )
{
    int i;

    if( mipmaps == NULL )
    {
        return;
    }

    SCT_ASSERT( mipmapCount > 0 );

    for( i = 0; i < mipmapCount; ++i )
    {
        if( mipmaps[ i ].data != NULL )
        {
            siCommonMemoryFree( NULL, mipmaps[ i ].data );
            mipmaps[ i ].data = NULL;
        }
    }

    siCommonMemoryFree( NULL, mipmaps );
}

/*!
 *
 */
void sctOpenGLES2DestroyCompressedTextureData( SCTOpenGLES2CompressedTextureData* textureData )
{
    if( textureData != NULL )
    {
        if( textureData->mipmaps != NULL )
        {
            sctOpenGLES2DestroyCompressedMipMapData( textureData->mipmaps, textureData->mipmapCount );
            textureData->mipmaps = NULL;
        }
        siCommonMemoryFree( NULL, textureData );
    }
}

/*!
 *
 */
int sctOpenGLES2GetTexelSize( SCTOpenGLES2TextureType type )
{
    switch( type )
    {
    case OPENGLES2_LUMINANCE8:
    case OPENGLES2_ALPHA8:
        return 1;

    case OPENGLES2_LUMINANCE_ALPHA88:
    case OPENGLES2_RGB565:
    case OPENGLES2_RGBA4444:
    case OPENGLES2_RGBA5551:
        return 2;

    case OPENGLES2_RGB888:
        return 3;

    case OPENGLES2_RGBA8888:
    case OPENGLES2_BGRA8888:        
        return 4;
        
    case OPENGLES2_DEPTH:
        return 2;

    case OPENGLES2_DEPTH32:
    case OPENGLES2_DEPTH24_STENCIL8:
        return 4;
        
    default:
        return -1;
    };
}

/*!
 *
 */
SCTBoolean sctOpenGLES2MapTextureTypeToGL( SCTOpenGLES2TextureType type, GLenum* glFormat, GLenum* glType )
{
    switch( type )
    {
    case OPENGLES2_LUMINANCE8:
        *glFormat = GL_LUMINANCE;
        *glType   = GL_UNSIGNED_BYTE;
        break;

    case OPENGLES2_ALPHA8:
        *glFormat = GL_ALPHA;
        *glType   = GL_UNSIGNED_BYTE;
        break;

    case OPENGLES2_LUMINANCE_ALPHA88:
        *glFormat = GL_LUMINANCE_ALPHA;
        *glType   = GL_UNSIGNED_BYTE;
        break;

    case OPENGLES2_RGB565:
        *glFormat = GL_RGB;
        *glType   = GL_UNSIGNED_SHORT_5_6_5;
        break;

    case OPENGLES2_RGB888:
        *glFormat = GL_RGB;
        *glType   = GL_UNSIGNED_BYTE;
        break;

    case OPENGLES2_RGBA4444:
        *glFormat = GL_RGBA;
        *glType   = GL_UNSIGNED_SHORT_4_4_4_4;
        break;

    case OPENGLES2_RGBA5551:
        *glFormat = GL_RGBA;
        *glType   = GL_UNSIGNED_SHORT_5_5_5_1;
        break;

    case OPENGLES2_RGBA8888:
        *glFormat = GL_RGBA;
        *glType   = GL_UNSIGNED_BYTE;
        break;
        
    case OPENGLES2_BGRA8888:        
        *glFormat = GL_BGRA_EXT;
        *glType   = GL_UNSIGNED_BYTE;
        break;

    case OPENGLES2_DEPTH:
        *glFormat = GL_DEPTH_COMPONENT;
        *glType   = GL_UNSIGNED_SHORT;
        break;

    case OPENGLES2_DEPTH32:
        *glFormat = GL_DEPTH_COMPONENT;
        *glType   = GL_UNSIGNED_INT;
        break;

    case OPENGLES2_DEPTH24_STENCIL8:
        *glFormat = GL_DEPTH_STENCIL_OES;
        *glType   = GL_UNSIGNED_INT_24_8_OES;
        break;
        
    default:
        return SCT_FALSE;
    };

    return SCT_TRUE;
}

/*!
 *
 */
int sctOpenGLES2GetGLTypeSize( GLenum type )
{
    switch( type )
    {
    case GL_BYTE:
        return sizeof( GLbyte );

    case GL_UNSIGNED_BYTE:
        return sizeof( GLubyte );

    case GL_SHORT:
        return sizeof( GLshort );

    case GL_UNSIGNED_SHORT:
        return sizeof( GLushort );

    case GL_UNSIGNED_INT:
        return sizeof( GLuint );
        
    case GL_FLOAT:
        return sizeof( GLfloat );

    case GL_FIXED:
        return sizeof( GLfixed );

    case GL_HALF_FLOAT_OES:
        return sizeof( GLshort );
        
    default:
        return -1;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Texture* sctOpenGLES2CreateTexture( GLuint texture )
{
    SCTOpenGLES2Texture* t;

    t = ( SCTOpenGLES2Texture* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Texture ) ) );
    if( t == NULL )
    {
        return NULL;
    }
    t->texture = texture;

    return t;
}

/*!
 *
 *
 */
void sctOpenGLES2DestroyTexture( SCTOpenGLES2Texture* texture )
{
    if( texture != NULL )
    {
        siCommonMemoryFree( NULL, texture );
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Framebuffer* sctOpenGLES2CreateFramebuffer( GLuint framebuffer )
{
    SCTOpenGLES2Framebuffer* fb;

    fb = ( SCTOpenGLES2Framebuffer* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Framebuffer ) ) );
    if( fb == NULL )
    {
        return NULL;
    }
    fb->framebuffer = framebuffer;

    return fb;
}

/*!
 *
 *
 */
void sctOpenGLES2DestroyFramebuffer( SCTOpenGLES2Framebuffer* framebuffer )
{
    if( framebuffer != NULL )
    {
        siCommonMemoryFree( NULL, framebuffer );
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Renderbuffer* sctOpenGLES2CreateRenderbuffer( GLuint renderbuffer )
{
    SCTOpenGLES2Renderbuffer* rb;

    rb = ( SCTOpenGLES2Renderbuffer* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Renderbuffer ) ) );
    if( rb == NULL )
    {
        return NULL;
    }
    rb->renderbuffer = renderbuffer;

    return rb;
}

/*!
 *
 *
 */
void sctOpenGLES2DestroyRenderbuffer( SCTOpenGLES2Renderbuffer* renderbuffer )
{
    if( renderbuffer != NULL )
    {
        siCommonMemoryFree( NULL, renderbuffer );
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Array* sctOpenGLES2CreateArray( GLenum type )
{
    SCTOpenGLES2Array*  array;

    array = ( SCTOpenGLES2Array* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Array ) ) );
    if( array == NULL )
    {
        return NULL;
    }
    memset( array, 0, sizeof( SCTOpenGLES2Array ) );

    array->type         = type;

    return array;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES2ClearArray( SCTOpenGLES2Array* array )
{
    SCT_ASSERT( array != NULL );

    if( array->data != NULL )
    {
        siCommonMemoryFree( NULL, array->data );
        array->data = NULL;
    }

    array->length           = 0;
    array->lengthInBytes    = 0;
    array->maxLengthInBytes = 0;

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES2AddDataToArray( SCTOpenGLES2Array* array, const void* data, int length )
{
    int                 newlength;
    unsigned char*      ptr;

    SCT_ASSERT( array != NULL );
    SCT_ASSERT( data != NULL );
    SCT_ASSERT( length > 0 );

    newlength = array->lengthInBytes + length;

    if( newlength > array->maxLengthInBytes )
    {
        unsigned char* newdata; 
        int alloclength = sctMax( newlength, 2 * array->maxLengthInBytes );
        newdata = ( unsigned char* )( siCommonMemoryAlloc( NULL, alloclength ) );
        if( newdata == NULL )
        {
            return SCT_FALSE;
        }
        if( array->data != NULL )
        {
            memcpy( newdata, array->data, array->lengthInBytes );
            siCommonMemoryFree( NULL, array->data );
        }
        array->data             = newdata;
        array->maxLengthInBytes = alloclength;
    }

    ptr = ( unsigned char* )( array->data ) + array->lengthInBytes;
    memcpy( ptr, data, length );

    array->length        += length;
    array->lengthInBytes = newlength;

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES2AddDoubleDataToArray( SCTOpenGLES2Array* array, const double* data, int length )
{
    int                 newlength;
    unsigned char*      ptr;

    SCT_ASSERT( array != NULL );

    if( data == NULL || length <= 0 )
    {
        return SCT_TRUE;
    }

    newlength = array->lengthInBytes + sctOpenGLES2GetGLTypeSize( array->type ) * length;

    if( newlength > array->maxLengthInBytes )
    {
        unsigned char* newdata; 
        int alloclength = sctMax( newlength, 2 * array->maxLengthInBytes );
        newdata = ( unsigned char* )( siCommonMemoryAlloc( NULL, alloclength ) );
        if( newdata == NULL )
        {
            return SCT_FALSE;
        }
        if( array->data != NULL )
        {
            memcpy( newdata, array->data, array->lengthInBytes );
            siCommonMemoryFree( NULL, array->data );
        }
        array->data             = newdata;
        array->maxLengthInBytes = alloclength;
    }

    ptr = ( unsigned char* )( array->data ) + array->lengthInBytes;
    if( sctOpenGLES2ConvertDoubleArray( ptr, array->type, data, length ) == SCT_FALSE )
    {
        return SCT_FALSE;
    }

    array->length        += length;
    array->lengthInBytes = newlength;

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctOpenGLES2DestroyArray( SCTOpenGLES2Array* array )
{
    if( array != NULL )
    {
        if( array->data != NULL )
        {
            siCommonMemoryFree( NULL, array->data );
            array->data = NULL;
        }
        siCommonMemoryFree( NULL, array );
    }
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES2ConvertDoubleArray( void* dst, GLenum dstType, const double* src, int length )
{
    int                 i;
    SCTBoolean          ok        = SCT_TRUE;
    GLbyte*             bytedst   = ( GLbyte* )( dst );
    GLubyte*            ubytedst  = ( GLubyte* )( dst );
    GLshort*            shortdst  = ( GLshort* )( dst );
    GLushort*           ushortdst = ( GLushort* )( dst );
    GLfloat*            floatdst  = ( GLfloat* )( dst );
    GLfixed*            fixeddst  = ( GLfixed* )( dst );

    SCT_ASSERT( dst != NULL );
    SCT_ASSERT( dstType == GL_BYTE  || 
                dstType != GL_UNSIGNED_BYTE || 
                dstType != GL_SHORT || 
                dstType != GL_UNSIGNED_SHORT || 
                dstType != GL_FLOAT || 
                dstType != GL_FIXED ||
                dstType != GL_HALF_FLOAT_OES );
    SCT_ASSERT( src != NULL );
    SCT_ASSERT( length > 0 );

    for( i = 0; i < length && ok == SCT_TRUE; ++i )
    {
        switch( dstType )
        {
        case GL_BYTE:
            if( *src < -0x7F || *src > 0x7F )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *bytedst++ = ( GLbyte )( *src++ );
            }
            break;
        case GL_UNSIGNED_BYTE:
            if( *src < 0 || *src > 0xFF )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *ubytedst++ = ( GLubyte )( *src++ );
            }
            break;
        case GL_SHORT:
            if( *src < -0x7FFF || *src > 0x7FFF )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *shortdst++ = ( GLshort )( *src++ );
            }
            break;
        case GL_UNSIGNED_SHORT:
            if( *src < 0 || *src > 0xFFFF )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *ushortdst++ = ( GLushort )( *src++ );
            }
            break;
        case GL_FIXED:
            if( *src < -0x7FFF || *src > 0x7FFF )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *fixeddst++ = ( GLfixed )( SCT_FLOAT_TO_FIXED( *src++ ) );
            }
            break;

        case GL_HALF_FLOAT_OES:
            *ushortdst++ = ( GLushort )( sctiOpenGLES2FloatToHalfFloat( ( float )( *src++ ) ) );
            break;
            
        default:
        case GL_FLOAT:
            *floatdst++ = ( GLfloat )( *src++ );
            break;
        }
    }

    return ok;
}

/*!
 *
 *
 */
unsigned short sctiOpenGLES2FloatToHalfFloat( float f )
{
    unsigned int            b;
    float*                  fp;
    const unsigned short    basetable[ 512 ] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80, 0x100, 0x200, 0x400, 0x800, 0xc00, 0x1000, 0x1400, 0x1800, 0x1c00, 0x2000, 0x2400, 0x2800, 0x2c00, 0x3000, 0x3400, 0x3800, 0x3c00, 0x4000, 0x4400, 0x4800, 0x4c00, 0x5000, 0x5400, 0x5800, 0x5c00, 0x6000, 0x6400, 0x6800, 0x6c00, 0x7000, 0x7400, 0x7800, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x7c00, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8000, 0x8001, 0x8002, 0x8004, 0x8008, 0x8010, 0x8020, 0x8040, 0x8080, 0x8100, 0x8200, 0x8400, 0x8800, 0x8c00, 0x9000, 0x9400, 0x9800, 0x9c00, 0xa000, 0xa400, 0xa800, 0xac00, 0xb000, 0xb400, 0xb800, 0xbc00, 0xc000, 0xc400, 0xc800, 0xcc00, 0xd000, 0xd400, 0xd800, 0xdc00, 0xe000, 0xe400, 0xe800, 0xec00, 0xf000, 0xf400, 0xf800, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00, 0xfc00 };
    const unsigned char     shifttable[ 512 ] = { 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10, 0xf, 0xe, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0xd, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11, 0x10, 0xf, 0xe, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0xd, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0xd };

    SCT_ASSERT( sizeof( float* ) == sizeof( unsigned int* ) );

    /* Get the bit presentation of the floating point value. Note: do this
     * through "float* fp" as direct conversion seems to give a warning
     * "dereferencing type-punned pointer will break strict-aliasing rules" with
     * some compilers.  */
    fp = &f;
    b = ( unsigned int )( *( ( unsigned int * )( fp ) ) );
    return ( unsigned short )( basetable[ ( b >> 23 ) & 0x1ff ] + ( ( b & 0x007fffff ) >> shifttable[ ( b >> 23 ) & 0x1ff ] ) );
}

/*!
 *
 *
 */
SCTOpenGLES2Buffer* sctOpenGLES2CreateBuffer( GLuint buffer )
{
    SCTOpenGLES2Buffer* b;

    b = ( SCTOpenGLES2Buffer* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Buffer ) ) );
    if( b == NULL )
    {
        return NULL;
    }
    memset( b, 0, sizeof( SCTOpenGLES2Buffer ) );

    b->buffer = buffer;

    if( sctOpenGLES2InitializeBufferFromArray( b, NULL, -1, -1 ) == SCT_FALSE )
    {
        sctOpenGLES2DestroyBuffer( b );
        return NULL;
    }

    return b;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES2InitializeBufferFromArray( SCTOpenGLES2Buffer* buffer, SCTOpenGLES2Array* array, int offset, int length )
{
    int elementSize = 0;

    SCT_ASSERT( buffer != NULL );

    if( buffer->itemCount < 1 )
    {
        SCT_ASSERT( buffer->items == NULL );
        buffer->items = ( SCTOpenGLES2BufferItem* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2BufferItem ) ) );
        if( buffer->items == NULL )
        {
            return SCT_FALSE;
        }
        buffer->itemCount = 1;
    }

    if( array != NULL )
    {
        buffer->items[ 0 ].type          = array->type;
        buffer->items[ 0 ].length        = array->length;
        buffer->items[ 0 ].lengthInBytes = array->lengthInBytes;
        buffer->items[ 0 ].components    = -1;
        buffer->items[ 0 ].stride        = 0;
        buffer->items[ 0 ].offset        = 0;

        elementSize = sctOpenGLES2GetGLTypeSize( array->type );
    }
    else
    {
        buffer->items[ 0 ].type          = 0;
        buffer->items[ 0 ].length        = 0;
        buffer->items[ 0 ].lengthInBytes = 0;
        buffer->items[ 0 ].components    = -1;
        buffer->items[ 0 ].stride        = 0;
        buffer->items[ 0 ].offset        = 0;
    }

    if( offset >= 0 )
    {
        buffer->items[ 0 ].offset        = offset;
    }

    if( length >= 0 )
    {
        buffer->items[ 0 ].length        = length;
        buffer->items[ 0 ].lengthInBytes = length * elementSize;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES2InitializeBufferFromMesh( SCTOpenGLES2Buffer* buffer, SCTOpenGLES2Mesh* mesh )
{
    int         i;

    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( mesh != NULL );

    if( buffer->itemCount < mesh->attributeArrayCount )
    {
        if( buffer->items != NULL )
        {
            siCommonMemoryFree( NULL, buffer->items );
            buffer->items = NULL;
        }

        buffer->items = ( SCTOpenGLES2BufferItem* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2BufferItem ) * mesh->attributeArrayCount ) );
        if( buffer->items == NULL )
        {
            return SCT_FALSE;
        }
        memset( buffer->items, 0, sizeof( SCTOpenGLES2BufferItem ) * mesh->attributeArrayCount );

        buffer->itemCount = mesh->attributeArrayCount;
    }

    for( i = 0; i < mesh->attributeArrayCount; ++i )
    {
        buffer->items[ i ].type          = mesh->attributeArrays[ i ].type;
        buffer->items[ i ].length        = mesh->attributeCount * mesh->attributeArrays[ i ].components;
        buffer->items[ i ].lengthInBytes = mesh->attributeArrays[ i ].elementSizeInMem * mesh->attributeCount;
        buffer->items[ i ].components    = mesh->attributeArrays[ i ].components;
        buffer->items[ i ].stride        = mesh->attributeArrays[ i ].stride;
        buffer->items[ i ].offset        = ( unsigned int )( ( unsigned char* )( mesh->attributeArrays[ i ].pointer ) - ( unsigned char* )( mesh->data ) );
    }

    return SCT_TRUE;
}


/*!
 *
 *
 */
void sctOpenGLES2DestroyBuffer( SCTOpenGLES2Buffer* buffer )
{
    if( buffer != NULL )
    {
        if( buffer->items != NULL )
        {
            siCommonMemoryFree( NULL, buffer->items );
            buffer->items = NULL;
        }

        siCommonMemoryFree( NULL, buffer );
    }
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES2IsValidBufferArrayIndex( SCTOpenGLES2Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );

    if( index < 0 || index >= buffer->itemCount )
    {
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
int sctOpenGLES2GetBufferArrayCount( SCTOpenGLES2Buffer* buffer )
{
    SCT_ASSERT( buffer != NULL );

    return buffer->itemCount;
}


/*!
 *
 *
 */
GLenum sctOpenGLES2GetBufferArrayType( SCTOpenGLES2Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].type;
}

/*!
 *
 *
 */
int sctOpenGLES2GetBufferArrayLength( SCTOpenGLES2Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].length;
}

/*!
 *
 *
 */
int sctOpenGLES2GetBufferArrayLengthInBytes( SCTOpenGLES2Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].lengthInBytes;
}

/*!
 *
 *
 */
int sctOpenGLES2GetBufferArrayComponents( SCTOpenGLES2Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].components;
}

/*!
 *
 *
 */
int sctOpenGLES2GetBufferArrayStride( SCTOpenGLES2Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].stride;
}

/*!
 *
 *
 */
int sctOpenGLES2GetBufferArrayOffset( SCTOpenGLES2Buffer* buffer, int index )
{
    SCT_ASSERT( buffer != NULL );
    SCT_ASSERT( index >= 0 && index < buffer->itemCount );

    return buffer->items[ index ].offset;
}

/*!
 *
 *
 */
SCTOpenGLES2Shader* sctOpenGLES2CreateShader( GLuint shader )
{
    SCTOpenGLES2Shader* s;

    s = ( SCTOpenGLES2Shader* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Shader ) ) );
    if( s == NULL )
    {
        return NULL;
    }
    s->shader = shader;

    return s;
}

/*!
 *
 *
 */
void sctOpenGLES2DestroyShader( SCTOpenGLES2Shader* shader )
{
    if( shader != NULL )
    {
        siCommonMemoryFree( NULL, shader );
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Program* sctOpenGLES2CreateProgram( GLuint program )
{
    SCTOpenGLES2Program* p;

    p = ( SCTOpenGLES2Program* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Program ) ) );
    if( p == NULL )
    {
        return NULL;
    }
    p->program = program;

    return p;
}

/*!
 *
 *
 */
void sctOpenGLES2DestroyProgram( SCTOpenGLES2Program* program )
{
    if( program != NULL )
    {
        siCommonMemoryFree( NULL, program );
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Data* sctOpenGLES2CreateData( void* data, int length, SCTBoolean copy )
{
    SCTOpenGLES2Data* d;

    SCT_ASSERT( data != NULL );
    SCT_ASSERT( length > 0 );

    d = ( SCTOpenGLES2Data* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Data ) ) );
    if( d == NULL )
    {
        return NULL;
    }
    
    if( copy == SCT_TRUE )
    {
        d->data = siCommonMemoryAlloc( NULL, length );
        if( d->data == NULL )
        {
            siCommonMemoryFree( NULL, d );
            return NULL;
        }
        memcpy( d->data, data, length );
    }
    else
    {
        d->data = data;
    }
   
    d->length = length;

    return d;
}

/*!
 *
 *
 */
void sctOpenGLES2DestroyData( SCTOpenGLES2Data* data )
{
    if( data != NULL )
    {
        if( data->data != NULL )
        {
            siCommonMemoryFree( NULL, data->data );
            data->data = NULL;
        }
        siCommonMemoryFree( NULL, data );
    }
}


/* ********************************************************************** */

SCTOpenGLES2Mesh* sctOpenGLES2CreateMesh( SCTOpenGLES2Array* arrays[], int components[], int arraysLength )
{
    SCTOpenGLES2Mesh*           mesh;
    int                         i, j;
    int                         attributeCount;
    SCTOpenGLES2Attrib*         attribs;

    SCT_ASSERT( arrays != NULL );
    SCT_ASSERT( components != NULL );
    SCT_ASSERT( arraysLength >= 1 );

    attribs = ( SCTOpenGLES2Attrib* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Attrib ) * arraysLength ) );
    if( attribs == NULL )
    {
        return NULL;
    }

    attributeCount = ( arrays[ 0 ]->length /components[ 0 ] );
    if( attributeCount < 1 )
    {
        siCommonMemoryFree( NULL, attribs );
        return NULL;
    }

    for( i = 0; i < arraysLength; ++i )
    {
        // All arrays must have the same number of elements (component tuples)
        SCT_ASSERT( components[ i ] != 0 );
        if( ( arrays[ i ]->length /components[ i ] ) != attributeCount )
        {
            siCommonMemoryFree( NULL, attribs );
            return NULL;
        }
        attribs[ i ].components = components[ i ];
        attribs[ i ].type       = arrays[ i ]->type;
        attribs[ i ].data       = ( unsigned char* )( arrays[ i ]->data );
        attribs[ i ].size       = sctOpenGLES2GetGLTypeSize( arrays[ i ]->type ) * components[ i ];
    }

    mesh = sctiOpenGLES2CreateEmptyMesh( attribs, arraysLength, attributeCount );

    if( mesh == NULL )
    {
        siCommonMemoryFree( NULL, attribs );
        return NULL;
    }

    for( i = 0; i < arraysLength; ++i )
    {
        for( j = 0; j < attributeCount; j++ )
        {
            memcpy( sctiGetMeshAttrib( mesh, i, j ), attribs[ i ].data, attribs[ i ].size );
            attribs[ i ].data += attribs[ i ].size;
        }
    }

    siCommonMemoryFree( NULL, attribs );

    return mesh;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static void* sctiGetMeshAttrib( SCTOpenGLES2Mesh* mesh, int arrayIndex, int attributeIndex )
{
    SCT_ASSERT( mesh != NULL );
    SCT_ASSERT( arrayIndex >= 0 && arrayIndex < mesh->attributeArrayCount );
    SCT_ASSERT( attributeIndex >= 0 && attributeIndex < mesh->attributeCount );
    SCT_ASSERT( mesh->attributeArrays[ arrayIndex ].pointer != NULL );

    return ( unsigned char* )( mesh->attributeArrays[ arrayIndex ].pointer ) + ( attributeIndex * mesh->attributeArrays[ arrayIndex ].stride );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static SCTOpenGLES2Mesh* sctiOpenGLES2CreateEmptyMesh( SCTOpenGLES2Attrib* attributes, int attributesLength, int attributeCount )
{
    SCTOpenGLES2Mesh*   mesh;
    int                 accum;
    int                 meshElementSize, totalSize;
    int                 i;

    SCT_ASSERT( attributes != NULL );
    SCT_ASSERT( attributesLength >= 1 );
    SCT_ASSERT( attributeCount >= 1 );

    mesh = ( SCTOpenGLES2Mesh* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Mesh ) ) );
    if( mesh == NULL )
    {
        return NULL;
    }
    memset( mesh, 0, sizeof( SCTOpenGLES2Mesh ) );

    mesh->attributeArrays = ( SCTOpenGLES2MeshItem* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2MeshItem ) * attributesLength ) );
    if( mesh->attributeArrays == NULL )
    {
        sctOpenGLES2DestroyMesh( mesh );
        return NULL;
    }

    mesh->attributeCount        = attributeCount;
    mesh->attributeArrayCount   = attributesLength;
    meshElementSize             = 0;
    totalSize                   = 0;

    for( i = 0; i < attributesLength; ++i )
    {
        mesh->attributeArrays[ i ].elementSizeInMem = attributes[ i ].size;
        mesh->attributeArrays[ i ].components       = attributes[ i ].components;
        mesh->attributeArrays[ i ].type             = attributes[ i ].type;

        mesh->attributeArrays[ i ].elementSizeInMem += mesh->attributeArrays[ i ].elementSizeInMem  < sizeof( long ) ? ( sizeof( long ) - mesh->attributeArrays[ i ].elementSizeInMem ) : mesh->attributeArrays[ i ].elementSizeInMem  % sizeof( long );

        meshElementSize += mesh->attributeArrays[ i ].elementSizeInMem;
    }

    totalSize = ( meshElementSize * attributeCount );

    SCT_ASSERT( totalSize > 0 );

    /* Store the total size of the data in vertex data buffer. */
    mesh->length = totalSize;

    /* Reserve memory for vertex data buffers. Zero memory. */
    mesh->data = siCommonMemoryAlloc( NULL, totalSize );
    if( mesh->data == NULL )
    {
        sctOpenGLES2DestroyMesh( mesh );
        return NULL;
    }
    memset( mesh->data, 0, totalSize );

    accum = 0;
    
    for( i = 0; i < attributesLength; ++i )
    {
        mesh->attributeArrays[ i ].pointer    = ( unsigned char* )( mesh->data ) + accum;
        mesh->attributeArrays[ i ].stride     = meshElementSize;
        accum += mesh->attributeArrays[ i ].elementSizeInMem;
    }
    
    return mesh;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void sctOpenGLES2DestroyMesh( SCTOpenGLES2Mesh* mesh )
{
    if( mesh != NULL )
    {
        if( mesh->data != NULL )
        {
            siCommonMemoryFree( NULL, mesh->data );
        }
        if( mesh->attributeArrays != NULL )
        {
            siCommonMemoryFree( NULL, mesh->attributeArrays );
        }

        siCommonMemoryFree( NULL, mesh );
    }
}

/*!
 *
 *
 */
int sctOpenGLES2MeshLength( SCTOpenGLES2Mesh* mesh )
{
    SCT_ASSERT( mesh != NULL );
    
    return mesh->length;
}

/*!
 *
 *
 */
void* sctOpenGLES2MeshData( SCTOpenGLES2Mesh* mesh )
{
    SCT_ASSERT( mesh != NULL );
    
    return mesh->data;
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES2IsValidMeshArrayIndex( SCTOpenGLES2Mesh* mesh, int index )
{
    SCT_ASSERT( mesh != NULL );

    if( index < 0 || index >= mesh->attributeArrayCount )
    {
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
int sctOpenGLES2MeshArrayComponents( SCTOpenGLES2Mesh* mesh, int index )
{
    SCT_ASSERT( mesh != NULL );
    SCT_ASSERT( index >= 0 && index < mesh->attributeArrayCount );

    return mesh->attributeArrays[ index ].components;
}

/*!
 *
 *
 */
GLenum sctOpenGLES2MeshArrayType( SCTOpenGLES2Mesh* mesh, int index )
{
    SCT_ASSERT( mesh != NULL );
    SCT_ASSERT( index >= 0 && index < mesh->attributeArrayCount );

    return mesh->attributeArrays[ index ].type;
}

/*!
 *
 *
 */
int sctOpenGLES2MeshArrayStride( SCTOpenGLES2Mesh* mesh, int index )
{
    SCT_ASSERT( mesh != NULL );
    SCT_ASSERT( index >= 0 && index < mesh->attributeArrayCount );

    return mesh->attributeArrays[ index ].stride;
}

/*!
 *
 *
 */
void* sctOpenGLES2MeshArrayPointer( SCTOpenGLES2Mesh* mesh, int index )
{
    SCT_ASSERT( mesh != NULL );
    SCT_ASSERT( index >= 0 && index < mesh->attributeArrayCount );

    return mesh->attributeArrays[ index ].pointer;
}

/*!
 *
 *
 */
SCTOpenGLES2Flag* sctOpenGLES2CreateFlag( void )
{
    SCTOpenGLES2Flag* flag;

    flag = ( SCTOpenGLES2Flag* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Flag ) ) );
    if( flag == NULL )
    {
        return NULL;
    }
    memset( flag, 0, sizeof( SCTOpenGLES2Flag ) );

    flag->mutex = siCommonCreateMutex( NULL );
    if( flag->mutex == NULL )
    {
        sctOpenGLES2DestroyFlag( flag );
        return NULL;
    }
    
    flag->flagged = SCT_FALSE;
    flag->value   = 0.0f;    

    return flag;
}

/*!
 *
 *
 */
void sctOpenGLES2SetFlag( SCTOpenGLES2Flag* flag, float value )
{
    SCT_ASSERT_ALWAYS( flag != NULL );
    SCT_ASSERT_ALWAYS( flag->mutex != NULL );

    siCommonLockMutex( NULL, flag->mutex );

    flag->flagged = SCT_TRUE;
    flag->value   = value;

    siCommonUnlockMutex( NULL, flag->mutex );    
}

/*!
 *
 *
 */
SCTBoolean sctOpenGLES2GetFlag( SCTOpenGLES2Flag* flag, float* value )
{
    SCTBoolean b;
    
    SCT_ASSERT_ALWAYS( flag != NULL );
    SCT_ASSERT_ALWAYS( flag->mutex != NULL );
    SCT_ASSERT_ALWAYS( value != NULL );

    siCommonLockMutex( NULL, flag->mutex );

    b = flag->flagged;
    if( b == SCT_TRUE )
    {
        *value = flag->value;
    }

    flag->flagged = SCT_FALSE;
    flag->value   = 0.0f;    
    
    siCommonUnlockMutex( NULL, flag->mutex );    

    return b;
}

/*!
 *
 *
 */
void sctOpenGLES2ClearFlag( SCTOpenGLES2Flag* flag )
{
    SCT_ASSERT_ALWAYS( flag != NULL );
    SCT_ASSERT_ALWAYS( flag->mutex != NULL );

    siCommonLockMutex( NULL, flag->mutex );

    flag->flagged = SCT_FALSE;
    flag->value   = 0.0f;

    siCommonUnlockMutex( NULL, flag->mutex );    
}

/*!
 *
 *
 */
void sctOpenGLES2DestroyFlag( SCTOpenGLES2Flag* flag )
{
    if( flag != NULL )
    {
        if( flag->mutex != NULL )
        {
            siCommonDestroyMutex( NULL, flag->mutex );
            flag->mutex = NULL;
        }

        siCommonMemoryFree( NULL, flag );
    }
}

/* ********************************************************************** */
/* NOISE */
/* ********************************************************************** */

/*!
 * Precalculated arrays used for creating the original perlin noise.
 *
 */
const int perlin_p[ 514 ] = { 0, 17, 22, 67, 225, 52, 123, 24, 44, 205, 12, 245, 50, 166, 199, 202, 185, 110, 14, 126, 178, 21, 129, 255, 104, 253, 153, 4, 85, 214, 84, 243, 79, 224, 191, 209, 20, 41, 109, 201, 103, 196, 37, 2, 18, 77, 233, 46, 101, 45, 222, 68, 53, 235, 232, 99, 76, 29, 138, 64, 97, 163, 187, 240, 57, 229, 140, 30, 182, 230, 28, 111, 1, 160, 215, 98, 159, 184, 188, 43, 194, 183, 142, 208, 130, 212, 60, 169, 9, 54, 8, 238, 154, 148, 173, 152, 144, 200, 174, 66, 49, 90, 254, 55, 33, 13, 198, 75, 181, 78, 206, 105, 176, 234, 203, 69, 83, 72, 19, 51, 210, 56, 15, 211, 32, 58, 149, 226, 116, 219, 42, 156, 186, 121, 143, 59, 100, 141, 70, 155, 5, 146, 61, 133, 71, 221, 171, 218, 36, 228, 16, 86, 26, 63, 93, 134, 74, 164, 39, 27, 118, 38, 132, 236, 119, 147, 179, 89, 162, 249, 192, 88, 117, 239, 246, 3, 40, 127, 213, 241, 170, 125, 244, 91, 108, 137, 7, 94, 220, 145, 180, 250, 23, 252, 207, 168, 92, 35, 31, 73, 112, 113, 96, 106, 122, 251, 248, 120, 193, 165, 161, 135, 34, 231, 167, 65, 124, 195, 25, 139, 47, 197, 175, 48, 10, 247, 223, 217, 190, 95, 6, 242, 80, 131, 128, 237, 150, 107, 151, 216, 115, 158, 87, 11, 204, 82, 172, 81, 62, 157, 177, 114, 189, 227, 102, 136, 0, 17, 22, 67, 225, 52, 123, 24, 44, 205, 12, 245, 50, 166, 199, 202, 185, 110, 14, 126, 178, 21, 129, 255, 104, 253, 153, 4, 85, 214, 84, 243, 79, 224, 191, 209, 20, 41, 109, 201, 103, 196, 37, 2, 18, 77, 233, 46, 101, 45, 222, 68, 53, 235, 232, 99, 76, 29, 138, 64, 97, 163, 187, 240, 57, 229, 140, 30, 182, 230, 28, 111, 1, 160, 215, 98, 159, 184, 188, 43, 194, 183, 142, 208, 130, 212, 60, 169, 9, 54, 8, 238, 154, 148, 173, 152, 144, 200, 174, 66, 49, 90, 254, 55, 33, 13, 198, 75, 181, 78, 206, 105, 176, 234, 203, 69, 83, 72, 19, 51, 210, 56, 15, 211, 32, 58, 149, 226, 116, 219, 42, 156, 186, 121, 143, 59, 100, 141, 70, 155, 5, 146, 61, 133, 71, 221, 171, 218, 36, 228, 16, 86, 26, 63, 93, 134, 74, 164, 39, 27, 118, 38, 132, 236, 119, 147, 179, 89, 162, 249, 192, 88, 117, 239, 246, 3, 40, 127, 213, 241, 170, 125, 244, 91, 108, 137, 7, 94, 220, 145, 180, 250, 23, 252, 207, 168, 92, 35, 31, 73, 112, 113, 96, 106, 122, 251, 248, 120, 193, 165, 161, 135, 34, 231, 167, 65, 124, 195, 25, 139, 47, 197, 175, 48, 10, 247, 223, 217, 190, 95, 6, 242, 80, 131, 128, 237, 150, 107, 151, 216, 115, 158, 87, 11, 204, 82, 172, 81, 62, 157, 177, 114, 189, 227, 102, 136, 0, 17 };

const double perlin_g2[ 514 ][ 2 ] = { { 7.024702e-01, -7.117132e-01 }, { 3.770901e-02, 9.992888e-01 }, { 7.344469e-01, -6.786661e-01 }, { -5.249502e-01, 8.511329e-01 }, { -6.811494e-01, 7.321445e-01 }, { -8.365272e-01, 5.479253e-01 }, { -4.866587e-01, -8.735922e-01 }, { 9.978744e-01, -6.516730e-02 }, { 6.259736e-01, 7.798442e-01 }, { 9.897704e-01, 1.426696e-01 }, { -3.949225e-01, 9.187144e-01 }, { 6.755849e-01, -7.372822e-01 }, { 9.959904e-01, -8.946022e-02 }, { -8.665316e-01, -4.991222e-01 }, { -3.236877e-01, 9.461640e-01 }, { -7.131587e-01, 7.010026e-01 }, { 9.578263e-01, 2.873479e-01 }, { -1.480818e-01, -9.889751e-01 }, { 6.776129e-01, 7.354188e-01 }, { 9.898605e-01, 1.420428e-01 }, { -7.156423e-01, -6.984669e-01 }, { 4.112889e-01, -9.115051e-01 }, { -2.129497e-01, 9.770632e-01 }, { 3.595890e-01, 9.331108e-01 }, { 2.143114e-01, -9.767654e-01 }, { 3.219027e-01, 9.467727e-01 }, { 9.417419e-01, -3.363364e-01 }, { 2.772993e-01, 9.607836e-01 }, { 7.538606e-01, -6.570344e-01 }, { 8.087361e-01, -5.881717e-01 }, { -2.564347e-01, 9.665616e-01 }, { 8.331681e-01, -5.530199e-01 }, { -6.336611e-01, -7.736108e-01 }, { -7.536040e-01, -6.573286e-01 }, { -8.427855e-01, -5.382496e-01 }, { -9.996539e-01, -2.630668e-02 }, { -4.584632e-01, -8.887134e-01 }, { -8.162993e-03, -9.999667e-01 }, { -9.552444e-01, 2.958176e-01 }, { 7.873482e-01, -6.165085e-01 }, { 5.306182e-01, -8.476109e-01 }, { 7.572263e-01, -6.531526e-01 }, { 9.431155e-01, -3.324652e-01 }, { -9.925744e-01, -1.216390e-01 }, { 7.906309e-01, 6.122931e-01 }, { -1.286296e-01, -9.916927e-01 }, { 1.543769e-01, -9.880120e-01 }, { 7.704305e-02, -9.970278e-01 }, { -4.472136e-01, -8.944272e-01 }, { -9.610485e-01, -2.763799e-01 }, { -6.326436e-01, 7.744431e-01 }, { -9.978429e-01, 6.564756e-02 }, { -4.393277e-01, 8.983269e-01 }, { 1.584659e-01, 9.873645e-01 }, { -1.175231e-01, -9.930701e-01 }, { -7.875050e-01, 6.163083e-01 }, { -5.210305e-02, -9.986417e-01 }, { -3.542271e-01, -9.351594e-01 }, { 4.453761e-01, 8.953436e-01 }, { -1.452948e-01, 9.893884e-01 }, { 7.014951e-01, -7.126743e-01 }, { -4.212228e-01, 9.069572e-01 }, { 7.744695e-01, 6.326112e-01 }, { -5.194692e-01, 8.544892e-01 }, { -8.866530e-01, -4.624354e-01 }, { 8.181046e-01, 5.750694e-01 }, { 7.814959e-01, 6.239103e-01 }, { -9.291831e-01, 3.696198e-01 }, { 8.476909e-01, 5.304904e-01 }, { 6.719124e-01, 7.406307e-01 }, { -3.791601e-01, 9.253311e-01 }, { -5.503134e-01, -8.349582e-01 }, { -9.546625e-01, 2.976904e-01 }, { -6.427352e-01, 7.660884e-01 }, { -3.823518e-01, -9.240168e-01 }, { 9.475355e-01, 3.196505e-01 }, { -3.388986e-01, 9.408229e-01 }, { 5.680346e-01, -8.230047e-01 }, { 5.273629e-01, -8.496402e-01 }, { -4.999256e-01, -8.660683e-01 }, { -9.004150e-01, 4.350320e-01 }, { -1.869124e-01, 9.823766e-01 }, { 2.702564e-01, -9.627884e-01 }, { 7.321330e-01, 6.811617e-01 }, { -6.791896e-01, -7.339629e-01 }, { 9.878370e-01, 1.554929e-01 }, { 6.209637e-01, 7.838394e-01 }, { 6.652060e-01, -7.466598e-01 }, { -9.892637e-01, -1.461412e-01 }, { 6.060432e-01, -7.954317e-01 }, { 6.494048e-01, -7.604429e-01 }, { 9.290426e-01, -3.699727e-01 }, { 9.007241e-01, -4.343917e-01 }, { 8.398190e-01, 5.428665e-01 }, { 8.327536e-01, -5.536438e-01 }, { 2.369562e-01, -9.715203e-01 }, { 8.434976e-01, -5.371329e-01 }, { 7.520284e-01, -6.591307e-01 }, { -9.107288e-01, 4.130049e-01 }, { 1.464053e-01, 9.892247e-01 }, { 8.885109e-01, -4.588554e-01 }, { 7.769328e-01, 6.295835e-01 }, { -5.645558e-01, 8.253949e-01 }, { 4.865311e-01, 8.736633e-01 }, { -9.473406e-01, -3.202278e-01 }, { 1.324851e-01, 9.911850e-01 }, { -9.556067e-02, -9.954236e-01 }, { -9.999268e-01, -1.209589e-02 }, { 9.549073e-01, 2.969040e-01 }, { -9.962109e-01, 8.697079e-02 }, { 8.433209e-01, 5.374104e-01 }, { 6.000000e-01, -8.000000e-01 }, { 9.998994e-01, 1.418297e-02 }, { -5.734623e-01, 8.192319e-01 }, { 2.511748e-01, -9.679418e-01 }, { -5.571236e-01, -8.304296e-01 }, { 7.225664e-01, 6.913015e-01 }, { 2.144964e-01, -9.767248e-01 }, { -4.011032e-01, -9.160329e-01 }, { 5.501033e-01, 8.350966e-01 }, { -2.881252e-01, 9.575927e-01 }, { -7.838884e-01, -6.209017e-01 }, { 9.721426e-01, 2.343901e-01 }, { -9.979517e-01, 6.397127e-02 }, { -2.177253e-01, 9.760101e-01 }, { 2.130396e-01, -9.770436e-01 }, { 9.987523e-01, -4.993762e-02 }, { 3.399229e-01, -9.404533e-01 }, { 9.872618e-01, -1.591042e-01 }, { 8.989324e-01, -4.380873e-01 }, { -9.590427e-01, 2.832616e-01 }, { 9.945865e-01, 1.039120e-01 }, { 6.168279e-01, 7.870981e-01 }, { 9.526099e-01, 3.041947e-01 }, { -2.030005e-01, -9.791786e-01 }, { -3.401361e-01, 9.403762e-01 }, { 5.518669e-01, 8.339322e-01 }, { -9.779177e-01, -2.089905e-01 }, { -8.535557e-01, 5.210015e-01 }, { -9.976837e-01, -6.802389e-02 }, { -4.416265e-01, -8.971990e-01 }, { 9.550641e-01, -2.963992e-01 }, { -1.559626e-01, 9.877630e-01 }, { -9.611761e-01, 2.759357e-01 }, { -7.290033e-01, 6.845102e-01 }, { -5.602127e-01, -8.283488e-01 }, { 9.482009e-01, 3.176714e-01 }, { 4.299336e-01, 9.028605e-01 }, { 9.983797e-01, -5.690247e-02 }, { -8.083230e-01, 5.887392e-01 }, { -4.047471e-01, -9.144287e-01 }, { -7.603737e-01, -6.494859e-01 }, { 3.431930e-01, -9.392649e-01 }, { 3.015214e-01, -9.534594e-01 }, { -8.069779e-01, 5.905816e-01 }, { 5.893523e-01, -8.078762e-01 }, { 7.450841e-01, -6.669705e-01 }, { 8.540114e-02, -9.963466e-01 }, { 9.544800e-01, 2.982750e-01 }, { 7.482444e-01, -6.634232e-01 }, { -8.615060e-01, -5.077475e-01 }, { 2.408939e-02, 9.997098e-01 }, { 7.617786e-01, 6.478374e-01 }, { -2.033132e-01, 9.791137e-01 }, { -8.232663e-01, -5.676554e-01 }, { 9.838699e-01, -1.788854e-01 }, { 5.854906e-01, 8.106792e-01 }, { 7.338198e-01, 6.793441e-01 }, { -4.486260e-01, -8.937196e-01 }, { 5.226810e-01, 8.525283e-01 }, { -3.110391e-01, -9.503971e-01 }, { 8.386073e-01, 5.447364e-01 }, { 6.875303e-01, 7.261556e-01 }, { -7.071068e-01, 7.071068e-01 }, { 8.219515e-02, 9.966163e-01 }, { 6.525231e-01, 7.577688e-01 }, { 9.940392e-01, -1.090236e-01 }, { -8.723623e-01, -4.888600e-01 }, { -1.715095e-01, -9.851825e-01 }, { 8.766901e-01, -4.810556e-01 }, { 5.536438e-01, -8.327536e-01 }, { 5.795238e-01, -8.149553e-01 }, { -1.210544e-01, -9.926459e-01 }, { -8.657684e-03, 9.999625e-01 }, { 7.724968e-01, 6.350186e-01 }, { -1.445337e-01, 9.894999e-01 }, { 3.205962e-01, 9.472160e-01 }, { -5.365138e-01, 8.438915e-01 }, { -6.742694e-01, 7.384855e-01 }, { -8.794912e-01, 4.759152e-01 }, { -9.692855e-01, -2.459381e-01 }, { -7.485464e-01, -6.630824e-01 }, { 9.024092e-01, 4.308801e-01 }, { 8.043661e-01, -5.941340e-01 }, { -8.271473e-01, 5.619852e-01 }, { 8.589018e-01, 5.121402e-01 }, { 2.664740e-01, 9.638421e-01 }, { -1.218913e-01, 9.925435e-01 }, { 9.557790e-01, 2.940858e-01 }, { 8.749575e-01, 4.841998e-01 }, { 5.102019e-01, 8.600547e-01 }, { -7.676101e-01, 6.409171e-01 }, { -3.875886e-01, -9.218324e-01 }, { 8.858315e-01, -4.640070e-01 }, { -9.548169e-01, 2.971947e-01 }, { 4.611087e-01, 8.873437e-01 }, { -9.358759e-01, 3.523298e-01 }, { 2.350824e-01, -9.719754e-01 }, { -6.593550e-02, -9.978239e-01 }, { -3.399395e-02, -9.994220e-01 }, { -4.127885e-01, 9.108269e-01 }, { -9.674378e-01, -2.531087e-01 }, { 3.807498e-01, -9.246781e-01 }, { 5.258221e-01, 8.505946e-01 }, { -9.943092e-01, 1.065331e-01 }, { 7.498379e-01, 6.616216e-01 }, { -6.401844e-01, 7.682213e-01 }, { -2.126175e-01, -9.771355e-01 }, { -5.761570e-01, 8.173390e-01 }, { -9.959972e-01, 8.938436e-02 }, { -8.176282e-01, -5.757465e-01 }, { -9.134688e-01, 4.069088e-01 }, { 9.928962e-01, -1.189834e-01 }, { 7.623766e-01, -6.471336e-01 }, { 4.606872e-01, -8.875625e-01 }, { 9.011667e-01, -4.334726e-01 }, { -8.273498e-01, -5.616870e-01 }, { -4.585396e-01, -8.886740e-01 }, { 3.407040e-01, 9.401706e-01 }, { 6.546338e-01, -7.559462e-01 }, { -9.999044e-01, -1.382356e-02 }, { 7.754130e-01, 6.314544e-01 }, { -9.562498e-01, 2.925513e-01 }, { 8.449545e-01, 5.348382e-01 }, { 8.486169e-01, -5.290079e-01 }, { 5.497476e-01, -8.353308e-01 }, { -5.093209e-01, 8.605767e-01 }, { 9.821023e-01, 1.883484e-01 }, { 9.701425e-01, 2.425356e-01 }, { -7.941618e-01, 6.077064e-01 }, { 2.530835e-02, 9.996797e-01 }, { -9.778789e-01, 2.091720e-01 }, { 7.540816e-01, -6.567807e-01 }, { 3.632082e-01, -9.317080e-01 }, { 6.196443e-01, 7.848828e-01 }, { 1.950740e-01, -9.807885e-01 }, { -1.375684e-01, 9.904923e-01 }, { 9.562000e-01, -2.927143e-01 }, { 4.016615e-01, 9.157882e-01 }, { -6.686334e-01, -7.435923e-01 }, { 5.368591e-02, -9.985579e-01 }, { 8.192319e-01, 5.734623e-01 }, { 6.438228e-02, 9.979253e-01 }, { -7.901468e-01, 6.129176e-01 }, { 9.053575e-02, -9.958932e-01 }, { -5.804152e-02, 9.983142e-01 }, { 7.024702e-01, -7.117132e-01 }, { 3.770901e-02, 9.992888e-01 }, { 7.344469e-01, -6.786661e-01 }, { -5.249502e-01, 8.511329e-01 }, { -6.811494e-01, 7.321445e-01 }, { -8.365272e-01, 5.479253e-01 }, { -4.866587e-01, -8.735922e-01 }, { 9.978744e-01, -6.516730e-02 }, { 6.259736e-01, 7.798442e-01 }, { 9.897704e-01, 1.426696e-01 }, { -3.949225e-01, 9.187144e-01 }, { 6.755849e-01, -7.372822e-01 }, { 9.959904e-01, -8.946022e-02 }, { -8.665316e-01, -4.991222e-01 }, { -3.236877e-01, 9.461640e-01 }, { -7.131587e-01, 7.010026e-01 }, { 9.578263e-01, 2.873479e-01 }, { -1.480818e-01, -9.889751e-01 }, { 6.776129e-01, 7.354188e-01 }, { 9.898605e-01, 1.420428e-01 }, { -7.156423e-01, -6.984669e-01 }, { 4.112889e-01, -9.115051e-01 }, { -2.129497e-01, 9.770632e-01 }, { 3.595890e-01, 9.331108e-01 }, { 2.143114e-01, -9.767654e-01 }, { 3.219027e-01, 9.467727e-01 }, { 9.417419e-01, -3.363364e-01 }, { 2.772993e-01, 9.607836e-01 }, { 7.538606e-01, -6.570344e-01 }, { 8.087361e-01, -5.881717e-01 }, { -2.564347e-01, 9.665616e-01 }, { 8.331681e-01, -5.530199e-01 }, { -6.336611e-01, -7.736108e-01 }, { -7.536040e-01, -6.573286e-01 }, { -8.427855e-01, -5.382496e-01 }, { -9.996539e-01, -2.630668e-02 }, { -4.584632e-01, -8.887134e-01 }, { -8.162993e-03, -9.999667e-01 }, { -9.552444e-01, 2.958176e-01 }, { 7.873482e-01, -6.165085e-01 }, { 5.306182e-01, -8.476109e-01 }, { 7.572263e-01, -6.531526e-01 }, { 9.431155e-01, -3.324652e-01 }, { -9.925744e-01, -1.216390e-01 }, { 7.906309e-01, 6.122931e-01 }, { -1.286296e-01, -9.916927e-01 }, { 1.543769e-01, -9.880120e-01 }, { 7.704305e-02, -9.970278e-01 }, { -4.472136e-01, -8.944272e-01 }, { -9.610485e-01, -2.763799e-01 }, { -6.326436e-01, 7.744431e-01 }, { -9.978429e-01, 6.564756e-02 }, { -4.393277e-01, 8.983269e-01 }, { 1.584659e-01, 9.873645e-01 }, { -1.175231e-01, -9.930701e-01 }, { -7.875050e-01, 6.163083e-01 }, { -5.210305e-02, -9.986417e-01 }, { -3.542271e-01, -9.351594e-01 }, { 4.453761e-01, 8.953436e-01 }, { -1.452948e-01, 9.893884e-01 }, { 7.014951e-01, -7.126743e-01 }, { -4.212228e-01, 9.069572e-01 }, { 7.744695e-01, 6.326112e-01 }, { -5.194692e-01, 8.544892e-01 }, { -8.866530e-01, -4.624354e-01 }, { 8.181046e-01, 5.750694e-01 }, { 7.814959e-01, 6.239103e-01 }, { -9.291831e-01, 3.696198e-01 }, { 8.476909e-01, 5.304904e-01 }, { 6.719124e-01, 7.406307e-01 }, { -3.791601e-01, 9.253311e-01 }, { -5.503134e-01, -8.349582e-01 }, { -9.546625e-01, 2.976904e-01 }, { -6.427352e-01, 7.660884e-01 }, { -3.823518e-01, -9.240168e-01 }, { 9.475355e-01, 3.196505e-01 }, { -3.388986e-01, 9.408229e-01 }, { 5.680346e-01, -8.230047e-01 }, { 5.273629e-01, -8.496402e-01 }, { -4.999256e-01, -8.660683e-01 }, { -9.004150e-01, 4.350320e-01 }, { -1.869124e-01, 9.823766e-01 }, { 2.702564e-01, -9.627884e-01 }, { 7.321330e-01, 6.811617e-01 }, { -6.791896e-01, -7.339629e-01 }, { 9.878370e-01, 1.554929e-01 }, { 6.209637e-01, 7.838394e-01 }, { 6.652060e-01, -7.466598e-01 }, { -9.892637e-01, -1.461412e-01 }, { 6.060432e-01, -7.954317e-01 }, { 6.494048e-01, -7.604429e-01 }, { 9.290426e-01, -3.699727e-01 }, { 9.007241e-01, -4.343917e-01 }, { 8.398190e-01, 5.428665e-01 }, { 8.327536e-01, -5.536438e-01 }, { 2.369562e-01, -9.715203e-01 }, { 8.434976e-01, -5.371329e-01 }, { 7.520284e-01, -6.591307e-01 }, { -9.107288e-01, 4.130049e-01 }, { 1.464053e-01, 9.892247e-01 }, { 8.885109e-01, -4.588554e-01 }, { 7.769328e-01, 6.295835e-01 }, { -5.645558e-01, 8.253949e-01 }, { 4.865311e-01, 8.736633e-01 }, { -9.473406e-01, -3.202278e-01 }, { 1.324851e-01, 9.911850e-01 }, { -9.556067e-02, -9.954236e-01 }, { -9.999268e-01, -1.209589e-02 }, { 9.549073e-01, 2.969040e-01 }, { -9.962109e-01, 8.697079e-02 }, { 8.433209e-01, 5.374104e-01 }, { 6.000000e-01, -8.000000e-01 }, { 9.998994e-01, 1.418297e-02 }, { -5.734623e-01, 8.192319e-01 }, { 2.511748e-01, -9.679418e-01 }, { -5.571236e-01, -8.304296e-01 }, { 7.225664e-01, 6.913015e-01 }, { 2.144964e-01, -9.767248e-01 }, { -4.011032e-01, -9.160329e-01 }, { 5.501033e-01, 8.350966e-01 }, { -2.881252e-01, 9.575927e-01 }, { -7.838884e-01, -6.209017e-01 }, { 9.721426e-01, 2.343901e-01 }, { -9.979517e-01, 6.397127e-02 }, { -2.177253e-01, 9.760101e-01 }, { 2.130396e-01, -9.770436e-01 }, { 9.987523e-01, -4.993762e-02 }, { 3.399229e-01, -9.404533e-01 }, { 9.872618e-01, -1.591042e-01 }, { 8.989324e-01, -4.380873e-01 }, { -9.590427e-01, 2.832616e-01 }, { 9.945865e-01, 1.039120e-01 }, { 6.168279e-01, 7.870981e-01 }, { 9.526099e-01, 3.041947e-01 }, { -2.030005e-01, -9.791786e-01 }, { -3.401361e-01, 9.403762e-01 }, { 5.518669e-01, 8.339322e-01 }, { -9.779177e-01, -2.089905e-01 }, { -8.535557e-01, 5.210015e-01 }, { -9.976837e-01, -6.802389e-02 }, { -4.416265e-01, -8.971990e-01 }, { 9.550641e-01, -2.963992e-01 }, { -1.559626e-01, 9.877630e-01 }, { -9.611761e-01, 2.759357e-01 }, { -7.290033e-01, 6.845102e-01 }, { -5.602127e-01, -8.283488e-01 }, { 9.482009e-01, 3.176714e-01 }, { 4.299336e-01, 9.028605e-01 }, { 9.983797e-01, -5.690247e-02 }, { -8.083230e-01, 5.887392e-01 }, { -4.047471e-01, -9.144287e-01 }, { -7.603737e-01, -6.494859e-01 }, { 3.431930e-01, -9.392649e-01 }, { 3.015214e-01, -9.534594e-01 }, { -8.069779e-01, 5.905816e-01 }, { 5.893523e-01, -8.078762e-01 }, { 7.450841e-01, -6.669705e-01 }, { 8.540114e-02, -9.963466e-01 }, { 9.544800e-01, 2.982750e-01 }, { 7.482444e-01, -6.634232e-01 }, { -8.615060e-01, -5.077475e-01 }, { 2.408939e-02, 9.997098e-01 }, { 7.617786e-01, 6.478374e-01 }, { -2.033132e-01, 9.791137e-01 }, { -8.232663e-01, -5.676554e-01 }, { 9.838699e-01, -1.788854e-01 }, { 5.854906e-01, 8.106792e-01 }, { 7.338198e-01, 6.793441e-01 }, { -4.486260e-01, -8.937196e-01 }, { 5.226810e-01, 8.525283e-01 }, { -3.110391e-01, -9.503971e-01 }, { 8.386073e-01, 5.447364e-01 }, { 6.875303e-01, 7.261556e-01 }, { -7.071068e-01, 7.071068e-01 }, { 8.219515e-02, 9.966163e-01 }, { 6.525231e-01, 7.577688e-01 }, { 9.940392e-01, -1.090236e-01 }, { -8.723623e-01, -4.888600e-01 }, { -1.715095e-01, -9.851825e-01 }, { 8.766901e-01, -4.810556e-01 }, { 5.536438e-01, -8.327536e-01 }, { 5.795238e-01, -8.149553e-01 }, { -1.210544e-01, -9.926459e-01 }, { -8.657684e-03, 9.999625e-01 }, { 7.724968e-01, 6.350186e-01 }, { -1.445337e-01, 9.894999e-01 }, { 3.205962e-01, 9.472160e-01 }, { -5.365138e-01, 8.438915e-01 }, { -6.742694e-01, 7.384855e-01 }, { -8.794912e-01, 4.759152e-01 }, { -9.692855e-01, -2.459381e-01 }, { -7.485464e-01, -6.630824e-01 }, { 9.024092e-01, 4.308801e-01 }, { 8.043661e-01, -5.941340e-01 }, { -8.271473e-01, 5.619852e-01 }, { 8.589018e-01, 5.121402e-01 }, { 2.664740e-01, 9.638421e-01 }, { -1.218913e-01, 9.925435e-01 }, { 9.557790e-01, 2.940858e-01 }, { 8.749575e-01, 4.841998e-01 }, { 5.102019e-01, 8.600547e-01 }, { -7.676101e-01, 6.409171e-01 }, { -3.875886e-01, -9.218324e-01 }, { 8.858315e-01, -4.640070e-01 }, { -9.548169e-01, 2.971947e-01 }, { 4.611087e-01, 8.873437e-01 }, { -9.358759e-01, 3.523298e-01 }, { 2.350824e-01, -9.719754e-01 }, { -6.593550e-02, -9.978239e-01 }, { -3.399395e-02, -9.994220e-01 }, { -4.127885e-01, 9.108269e-01 }, { -9.674378e-01, -2.531087e-01 }, { 3.807498e-01, -9.246781e-01 }, { 5.258221e-01, 8.505946e-01 }, { -9.943092e-01, 1.065331e-01 }, { 7.498379e-01, 6.616216e-01 }, { -6.401844e-01, 7.682213e-01 }, { -2.126175e-01, -9.771355e-01 }, { -5.761570e-01, 8.173390e-01 }, { -9.959972e-01, 8.938436e-02 }, { -8.176282e-01, -5.757465e-01 }, { -9.134688e-01, 4.069088e-01 }, { 9.928962e-01, -1.189834e-01 }, { 7.623766e-01, -6.471336e-01 }, { 4.606872e-01, -8.875625e-01 }, { 9.011667e-01, -4.334726e-01 }, { -8.273498e-01, -5.616870e-01 }, { -4.585396e-01, -8.886740e-01 }, { 3.407040e-01, 9.401706e-01 }, { 6.546338e-01, -7.559462e-01 }, { -9.999044e-01, -1.382356e-02 }, { 7.754130e-01, 6.314544e-01 }, { -9.562498e-01, 2.925513e-01 }, { 8.449545e-01, 5.348382e-01 }, { 8.486169e-01, -5.290079e-01 }, { 5.497476e-01, -8.353308e-01 }, { -5.093209e-01, 8.605767e-01 }, { 9.821023e-01, 1.883484e-01 }, { 9.701425e-01, 2.425356e-01 }, { -7.941618e-01, 6.077064e-01 }, { 2.530835e-02, 9.996797e-01 }, { -9.778789e-01, 2.091720e-01 }, { 7.540816e-01, -6.567807e-01 }, { 3.632082e-01, -9.317080e-01 }, { 6.196443e-01, 7.848828e-01 }, { 1.950740e-01, -9.807885e-01 }, { -1.375684e-01, 9.904923e-01 }, { 9.562000e-01, -2.927143e-01 }, { 4.016615e-01, 9.157882e-01 }, { -6.686334e-01, -7.435923e-01 }, { 5.368591e-02, -9.985579e-01 }, { 8.192319e-01, 5.734623e-01 }, { 6.438228e-02, 9.979253e-01 }, { -7.901468e-01, 6.129176e-01 }, { 9.053575e-02, -9.958932e-01 }, { -5.804152e-02, 9.983142e-01 }, { 7.024702e-01, -7.117132e-01 }, { 3.770901e-02, 9.992888e-01 } };

#define B               0x100
#define BM              0xff
#define N               0x1000
#define s_curve( t )    ( t * t * ( 3. - 2. * t ) )
#define lerp( t, a, b ) ( a + t * ( b - a ) )

/*!
 *
 *
 */
static double sctiOriginalNoise2( double vec[ 2 ] )
{
    int             bx0, bx1, by0, by1, b00, b10, b01, b11;
    double          rx0, rx1, ry0, ry1, sx, sy, a, b, t, u, v;
    const double*   q;
    int             i, j;

    t   = vec[ 0 ] + N;
    bx0 = ( ( int )( t ) ) & BM;
    bx1 = ( bx0 + 1 ) & BM;
    rx0 = t - ( int )( t );
    rx1 = rx0 - 1.;

    t   = vec[ 1 ] + N;
    by0 = ( ( int )( t ) ) & BM;
    by1 = ( by0 + 1 ) & BM;
    ry0 = t - ( int )( t );
    ry1 = ry0 - 1.;
        
    i = perlin_p[ bx0 ];
    j = perlin_p[ bx1 ];

    b00 = perlin_p[ i + by0 ];
    b10 = perlin_p[ j + by0 ];
    b01 = perlin_p[ i + by1 ];
    b11 = perlin_p[ j + by1 ];

    sx = s_curve( rx0 );
    sy = s_curve( ry0 );

    q = perlin_g2[ b00 ];
    u = rx0 * q[ 0 ] + ry0 * q[ 1 ];
    q = perlin_g2[ b10 ];
    v = rx1 * q[ 0 ] + ry0 * q[ 1 ];    
    a = lerp( sx, u, v );

    q = perlin_g2[ b01 ];
    u = rx0 * q[ 0 ] + ry1 * q[ 1 ];        
    q = perlin_g2[ b11 ];
    v = rx1 * q[ 0 ] + ry1 * q[ 1 ];            
    b = lerp( sx, u, v );

    return lerp( sy, a, b );
}

/*!
 *
 *
 */
static double sctiOriginalPerlinNoise2D( double x, double y, double alpha, double beta, int n )
{
    int     i;
    double  val;
    double  sum     = 0;
    double  p[ 2 ];
    double  scale   = 1;

    p[ 0 ] = x;
    p[ 1 ] = y;
    for( i = 0; i < n; i++ )
    {
        val = sctiOriginalNoise2( p );
        sum += val / scale;
        scale *= alpha;
        p[ 0 ] *= beta;
        p[ 1 ] *= beta;
    }
    
    return ( sum + 1.0 ) / 2.0;
}

/*!
 *
 *
 */
static double sctiSmooth( double x, double y )
{
    double n1 = sctiNoise( ( int )( x ), ( int )( y ) );
    double n2 = sctiNoise( ( int )( x + 1 ), ( int )( y ) );
    double n3 = sctiNoise( ( int )( x ), ( int )( y + 1 ));
    double n4 = sctiNoise( ( int )( x + 1 ), ( int )( y + 1 ) );

    double i1 = sctiInterpolate( n1, n2, x - ( int )( x ) );
    double i2 = sctiInterpolate( n3, n4, x - ( int )( x ) );

    return sctiInterpolate(i1, i2, y - ( int )( y ) );
}

/*!
 *
 *
 */
static double sctiNearest( double x, double y )
{
    return sctiNoise( ( int )( x ), ( int )( y ) );
}

/*!
 *
 *
 */
static double sctiNoise( int x, int y )
{
    int n;
    
    n = x + y * 57;
    n = ( n << 13 ) ^ n;
    
    return ( 1.0 - ( ( n * ( n * n * 15731 + 789221 ) + 1376312589 ) & 0x7fffffff ) / 1073741824.0 );
}

/*!
 *
 *
 */
static double sctiInterpolate( double x, double y, double a )
{
    double val = ( 1 - cos( a * 3.14573 ) ) * 0.5;
    return x * ( 1 - val ) + y * val;
}

/*!
 *
 *
 */
static double sctiLowQualityPerlinNoise2D( int x, int y, double frequency, double persistence, int octaves, double amplitude, double materialCoverage, double materialDensity )
{
    return sctiQualityPerlinNoise2D( x, y, frequency, persistence, octaves, amplitude, materialCoverage, materialDensity, SCT_FALSE );    
}

/*!
 *
 *
 */
static double sctiHighQualityPerlinNoise2D( int x, int y, double frequency, double persistence, int octaves, double amplitude, double materialCoverage, double materialDensity )
{
    return sctiQualityPerlinNoise2D( x, y, frequency, persistence, octaves, amplitude, materialCoverage, materialDensity, SCT_TRUE );
}

/*!
 *
 *
 */
static double sctiQualityPerlinNoise2D( int x, int y, double frequency, double persistence, int octaves, double amplitude, double materialCoverage, double materialDensity, SCTBoolean hq )
{
    double total    = 0;
    int    octave;

    if( hq )
    {
        for( octave = 0; octave < octaves; ++octave )
        {
            total = total + sctiSmooth( x * frequency, y * frequency ) * amplitude;
            frequency = frequency * 2;
            amplitude = amplitude * persistence;
        }
    }
    else
    {
        for( octave = 0; octave < octaves; ++octave )
        {
            total = total + sctiNearest( x * frequency, y * frequency ) * amplitude;
            frequency = frequency * 2;
            amplitude = amplitude * persistence;
        }
    }
        
    total = ( total + materialCoverage ) * materialDensity;
    if( total < 0 )
    {
        total = 0;
    }
    else if( total > 1 )
    {
        total = 1;
    }

    return total;
}

