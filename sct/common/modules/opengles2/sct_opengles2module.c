/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2module.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"
#include "sct_opengles2module_parser.h"
#include "sct_opengles2module_actions.h"

#include <stdio.h>
#include <string.h>

/* Local function declarations. */
SCTAttributeList*       sctiOpenGLES2ModuleInfo( SCTModule* module );
SCTAction*              sctiOpenGLES2ModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    sctiOpenGLES2ModuleDestroy( SCTModule* module );

/*!
 *
 */
SCTBoolean sctOpenGLES2ModuleSetDataExt( int index, SCTOpenGLES2Data* data )
{
    SCTOpenGLES2ModuleContext*  context;
    SCTBoolean                  pointerRegistered   = SCT_FALSE;

    context = ( SCTOpenGLES2ModuleContext* )( sctGetRegisteredPointer( SCT_OPENGLES2_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || context == NULL )
    {
        return SCT_FALSE;
    }

    if( sctiOpenGLES2ModuleIsValidDataIndex( context, index ) == SCT_FALSE )
    {
        return SCT_FALSE;
    }
    
    sctiOpenGLES2ModuleDeleteData( context, index );
    sctiOpenGLES2ModuleSetData( context, index, data );

    return SCT_TRUE;
}

/*!
 *
 */
GLuint sctOpenGLES2ModuleGetTextureExt( int index )
{
    SCTOpenGLES2ModuleContext*  context;
    SCTBoolean                  pointerRegistered   = SCT_FALSE;

    if( index < 0 || index >= SCT_MAX_OPENGLES2_TEXTURES )
    {
        return 0;
    }

    context = ( SCTOpenGLES2ModuleContext* )( sctGetRegisteredPointer( SCT_OPENGLES2_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || context == NULL )
    {
        return 0;
    }

    if( context->textures[ index ] == NULL )
    {
        return 0;
    }
    
    return context->textures[ index ]->texture;
}

/*!
 *
 */
GLuint sctOpenGLES2ModuleGetRenderbufferExt( int index )
{
    SCTOpenGLES2ModuleContext*  context;
    SCTBoolean                  pointerRegistered   = SCT_FALSE;

    if( index < 0 || index >= SCT_MAX_OPENGLES2_RENDERBUFFERS )
    {
        return 0;
    }

    context = ( SCTOpenGLES2ModuleContext* )( sctGetRegisteredPointer( SCT_OPENGLES2_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || context == NULL )
    {
        return 0;
    }

    if( context->renderbuffers[ index ] == NULL )
    {
        return 0;
    }
    
    return context->renderbuffers[ index ]->renderbuffer;
}

/*!
 * Create opengles2 module data structure which defines the name of
 * the module and sets up the module interface functions.
 *
 * \return Module structure for OpenGLES2 module, or NULL if failure.
 */
SCTModule* sctCreateOpenGLES2Module( void )
{
    SCTModule*                  module;
    SCTOpenGLES2ModuleContext*  context;
    int                         i;

    /* Create OpenGLES2 module context and zero memory. */
    context = ( SCTOpenGLES2ModuleContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2ModuleContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2ModuleContext ) );

    for( i = 0; i < SCT_MAX_OPENGLES2_TEXTURES; ++i )
    {
        context->textures[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_TEXTURE_DATAS; ++i )
    {
        context->textureDatas[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_COMPRESSED_TEXTURE_DATAS; ++i )
    {
        context->compressedTextureDatas[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_ARRAYS; ++i )
    {
        context->arrays[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_BUFFERS; ++i )
    {
        context->buffers[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_FRAMEBUFFERS; ++i )
    {
        context->framebuffers[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_RENDERBUFFERS; ++i )
    {
        context->renderbuffers[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_SHADERS; ++i )
    {
        context->shaders[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_PROGRAMS; ++i )
    {
        context->programs[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_REGISTERS; ++i )
    {
        context->registers[ i ] = -1;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_DATAS; ++i )
    {
        context->datas[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_MESHES; ++i )
    {
        context->meshes[ i ] = NULL;
    }
    for( i = 0; i < SCT_MAX_OPENGLES2_FLAGS; ++i )
    {
        /* Note: it is ok to have NULL flags, e.g. in systems that do not
         * support multithreading. */
        context->flags[ i ] = sctOpenGLES2CreateFlag();
    }
    
    /* Create module data structure. */
    module = sctCreateModule( "OpenGLES2",
                              context,
#if defined( _WIN32 )
                              _opengles2_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiOpenGLES2ModuleInfo,
                              sctiOpenGLES2ModuleCreateAction,
                              sctiOpenGLES2ModuleDestroy );

    if( module == NULL )
    {
        siCommonMemoryFree( NULL, context );
        return NULL;
    }

    if( sctRegisterPointer( SCT_OPENGLES2_MODULE_POINTER, context ) == SCT_FALSE )
    {
        sctiOpenGLES2ModuleDestroy( module );
        sctDestroyModule( module );
        return NULL;
    }
    
    return module;
}


/*!
 * Create attribute list containing OpenGLES2 module information.
 *
 * \param module OpenGLES2 module. Must be defined.
 *
 * \return OpenGLES2 system information as attribute list, or NULL if
 * failure.
 */
SCTAttributeList* sctiOpenGLES2ModuleInfo( SCTModule* module )
{
    SCTAttributeList*           attributes;

    SCT_ASSERT_ALWAYS( module != NULL );

    /* Create attribute list for storing OpenGLES2 module information. */
    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        return NULL;
    }
    
    return attributes;
}


/*!
 * Create OpenGLES2 module action. 
 *
 * \param module OpenGLES2 module. Must be defined.
 * \param name Action name. Must be defined.
 * \param type Action type. Must be defined.
 * \param attributes List of action attributes. Must be defined.
 * \param log Log to use for warning and error reporting.
 *
 * \return Action data structure, or NULL if failure.
 *
 */
SCTAction* sctiOpenGLES2ModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCTOpenGLES2ModuleContext* moduleContext;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    moduleContext = ( SCTOpenGLES2ModuleContext* )( module->context );

    SCT_ASSERT_ALWAYS( moduleContext != NULL );

    return sctCreateActionFromTemplate( name,
                                        type,
                                        "OpenGLES2",
                                        moduleContext,
                                        attributes,
                                        OpenGLES2ActionTemplates,
                                        SCT_ARRAY_LENGTH( OpenGLES2ActionTemplates ) );
}

/*!
 * Destroy OpenGLES2 module context. This function does not deallocate
 * module data structure but instead simply does the needed module
 * cleanup, if any.
 *
 * \param module Module to destroy. Must be defined.
 */
void sctiOpenGLES2ModuleDestroy( SCTModule* module )
{
    SCTOpenGLES2ModuleContext*  context;
    int                         i;

    SCT_ASSERT_ALWAYS( module != NULL );

    sctUnregisterPointer( SCT_OPENGLES2_MODULE_POINTER );
    
    context = ( SCTOpenGLES2ModuleContext* )( module->context );
    
    if( context != NULL )
    {
        for( i = 0; i < SCT_MAX_OPENGLES2_TEXTURES; ++i )
        {
            SCT_ASSERT_ALWAYS( context->textures[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_TEXTURE_DATAS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->textureDatas[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_COMPRESSED_TEXTURE_DATAS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->compressedTextureDatas[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_ARRAYS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->arrays[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_BUFFERS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->buffers[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_FRAMEBUFFERS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->framebuffers[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_RENDERBUFFERS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->renderbuffers[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_RENDERBUFFERS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->renderbuffers[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_SHADERS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->shaders[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_PROGRAMS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->programs[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_DATAS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->datas[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_MESHES; ++i )
        {
            SCT_ASSERT_ALWAYS( context->meshes[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENGLES2_FLAGS; ++i )
        {
            sctOpenGLES2DestroyFlag( context->flags[ i ] );
            context->flags[ i ] = NULL;
        }
        
        siCommonMemoryFree( NULL, context );
        module->context = NULL;
    }
}

//######################################################################
// TEXTURE DATAS
/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidTextureDataIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURE_DATAS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2TextureData* sctiOpenGLES2ModuleGetTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURE_DATAS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURE_DATAS )
    {
        return moduleContext->textureDatas[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2TextureData* textureData )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURE_DATAS );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURE_DATAS )
    {
        moduleContext->textureDatas[ index ] = textureData;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleDeleteTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCTOpenGLES2TextureData*    textureData;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURE_DATAS )
    {
        textureData = moduleContext->textureDatas[ index ];
        if( textureData != NULL )
        {
            sctOpenGLES2DestroyTextureData( textureData );
            moduleContext->textureDatas[ index ] = NULL;
        }
    }
}

//######################################################################
// TEXTURES
/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidTextureIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURES )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Texture* sctiOpenGLES2ModuleGetTexture( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURES );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURES )
    {
        return moduleContext->textures[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetTexture( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Texture* texture )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURES );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURES )
    {
        moduleContext->textures[ index ] = texture;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleDeleteTexture( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCTOpenGLES2Texture*    texture;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_TEXTURES )
    {
        texture = moduleContext->textures[ index ];
        if( texture != NULL )
        {
            glDeleteTextures( 1, &( texture->texture ) );

            sctOpenGLES2DestroyTexture( texture );
            moduleContext->textures[ index ] = NULL;
        }
    }
}

//######################################################################
// COMPRESSED TEXTURE DATAS
/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidCompressedTextureDataIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );    

    if( index >= 0 && index < SCT_MAX_OPENGLES2_COMPRESSED_TEXTURE_DATAS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2CompressedTextureData* sctiOpenGLES2ModuleGetCompressedTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_COMPRESSED_TEXTURE_DATAS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_COMPRESSED_TEXTURE_DATAS )
    {
        return moduleContext->compressedTextureDatas[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetCompressedTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2CompressedTextureData* compressedTextureData )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_COMPRESSED_TEXTURE_DATAS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_COMPRESSED_TEXTURE_DATAS )
    {
        moduleContext->compressedTextureDatas[ index ] = compressedTextureData;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleDeleteCompressedTextureData( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCTOpenGLES2CompressedTextureData*  compressedTextureData;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_COMPRESSED_TEXTURE_DATAS )
    {
        compressedTextureData = moduleContext->compressedTextureDatas[ index ];
        if( compressedTextureData != NULL )
        {
            sctOpenGLES2DestroyCompressedTextureData( compressedTextureData );
            moduleContext->compressedTextureDatas[ index ] = NULL;
        }
    }
}

//######################################################################
// ARRAYS

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidArrayIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES2_ARRAYS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Array* sctiOpenGLES2ModuleGetArray( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_ARRAYS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_ARRAYS )
    {
        return moduleContext->arrays[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetArray( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Array* array )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_ARRAYS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_ARRAYS )
    {
        moduleContext->arrays[ index ] = array;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleDeleteArray( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCTOpenGLES2Array*  array;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_ARRAYS )
    {
        array = moduleContext->arrays[ index ];
        if( array != NULL )
        {
            sctOpenGLES2DestroyArray( array );
            moduleContext->arrays[ index ] = NULL;
        }
    }
}

//######################################################################
// BUFFERS

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidBufferIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES2_BUFFERS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Buffer* sctiOpenGLES2ModuleGetBuffer( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_BUFFERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_BUFFERS )
    {
        return moduleContext->buffers[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetBuffer( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Buffer* buffer )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_BUFFERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_BUFFERS )
    {
        moduleContext->buffers[ index ] = buffer;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleDeleteBuffer( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCTOpenGLES2Buffer* buffer;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_BUFFERS )
    {
        buffer = moduleContext->buffers[ index ];
        if( buffer != NULL )
        {
            glDeleteBuffers( 1, &( buffer->buffer ) );
            
            sctOpenGLES2DestroyBuffer( buffer );
            moduleContext->buffers[ index ] = NULL;
        }
    }
}

//######################################################################
// FRAMEBUFFERS

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidFramebufferIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_FRAMEBUFFERS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Framebuffer* sctiOpenGLES2ModuleGetFramebuffer( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_FRAMEBUFFERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_FRAMEBUFFERS )
    {
        return moduleContext->framebuffers[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetFramebuffer( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Framebuffer* framebuffer )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_FRAMEBUFFERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_FRAMEBUFFERS )
    {
        moduleContext->framebuffers[ index ] = framebuffer;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleDeleteFramebuffer( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCTOpenGLES2Framebuffer*    framebuffer;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_FRAMEBUFFERS )
    {
        framebuffer = moduleContext->framebuffers[ index ];
        if( framebuffer != NULL )
        {
            if( framebuffer->framebuffer != 0 )
            {
                glDeleteFramebuffers( 1, &( framebuffer->framebuffer ) );
            }

            sctOpenGLES2DestroyFramebuffer( framebuffer );
            moduleContext->framebuffers[ index ] = NULL;
        }
    }
}

//######################################################################
// RENDERBUFFERS

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidRenderbufferIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_RENDERBUFFERS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Renderbuffer* sctiOpenGLES2ModuleGetRenderbuffer( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_RENDERBUFFERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_RENDERBUFFERS )
    {
        return moduleContext->renderbuffers[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetRenderbuffer( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Renderbuffer* renderbuffer )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_RENDERBUFFERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_RENDERBUFFERS )
    {
        moduleContext->renderbuffers[ index ] = renderbuffer;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleDeleteRenderbuffer( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCTOpenGLES2Renderbuffer*   renderbuffer;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_RENDERBUFFERS )
    {
        renderbuffer = moduleContext->renderbuffers[ index ];
        if( renderbuffer != NULL )
        {
            glDeleteRenderbuffers( 1, &( renderbuffer->renderbuffer ) );

            sctOpenGLES2DestroyRenderbuffer( renderbuffer );
            moduleContext->renderbuffers[ index ] = NULL;
        }
    }
}

// ######################################################################
// SHADERS

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidShaderIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES2_SHADERS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Shader* sctiOpenGLES2ModuleGetShader( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_SHADERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_SHADERS )
    {
        return moduleContext->shaders[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetShader( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Shader* shader )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_SHADERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_SHADERS )
    {
        moduleContext->shaders[ index ] = shader;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleDeleteShader( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCTOpenGLES2Shader* shader;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_SHADERS )
    {
        shader = moduleContext->shaders[ index ];
        if( shader != NULL )
        {
            glDeleteShader( shader->shader );

            sctOpenGLES2DestroyShader( shader );
            moduleContext->shaders[ index ] = NULL;
        }
    }
}

// ######################################################################
// PROGRAMS

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidProgramIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES2_PROGRAMS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Program* sctiOpenGLES2ModuleGetProgram( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_PROGRAMS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_PROGRAMS )
    {
        return moduleContext->programs[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetProgram( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Program* program )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_PROGRAMS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_PROGRAMS )
    {
        moduleContext->programs[ index ] = program;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleDeleteProgram( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCTOpenGLES2Program*        program;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_PROGRAMS )
    {
        program = moduleContext->programs[ index ];
        if( program != NULL )
        {
            glDeleteProgram( program->program );

            sctOpenGLES2DestroyProgram( program );
            moduleContext->programs[ index ] = NULL;
        }
    }
}

// ######################################################################
// REGISTERS

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidRegisterIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENGLES2_REGISTERS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
double sctiOpenGLES2ModuleGetRegisterValue( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_REGISTERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_REGISTERS )
    {
        return moduleContext->registers[ index ];
    }

    return -1;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetRegisterValue( SCTOpenGLES2ModuleContext* moduleContext, int index, double value )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_REGISTERS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_REGISTERS )
    {
        moduleContext->registers[ index ] = value;
    }
}

// ######################################################################
// DATAS

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidDataIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_DATAS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Data* sctiOpenGLES2ModuleGetData( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_DATAS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_DATAS )
    {
        return moduleContext->datas[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetData( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Data* data )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_DATAS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_DATAS )
    {
        moduleContext->datas[ index ] = data;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleDeleteData( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCTOpenGLES2Data*   data;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_DATAS )
    {
        data = moduleContext->datas[ index ];
        if( data != NULL )
        {
            sctOpenGLES2DestroyData( data );
            moduleContext->datas[ index ] = NULL;
        }
    }
}

// ######################################################################
// MESHES

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidMeshIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_MESHES )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Mesh* sctiOpenGLES2ModuleGetMesh( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_MESHES );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_MESHES )
    {
        return moduleContext->meshes[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleSetMesh( SCTOpenGLES2ModuleContext* moduleContext, int index, SCTOpenGLES2Mesh* mesh )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_MESHES );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_MESHES )
    {
        moduleContext->meshes[ index ] = mesh;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ModuleDeleteMesh( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCTOpenGLES2Mesh*   mesh;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_MESHES )
    {
        mesh = moduleContext->meshes[ index ];
        if( mesh != NULL )
        {
            sctOpenGLES2DestroyMesh( mesh );
            moduleContext->meshes[ index ] = NULL;
        }
    }
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2ModuleIsValidFlagIndex( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_FLAGS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenGLES2Flag* sctiOpenGLES2ModuleGetFlag( SCTOpenGLES2ModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENGLES2_FLAGS );

    if( index >= 0 && index < SCT_MAX_OPENGLES2_FLAGS )
    {
        return moduleContext->flags[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
const char* sctiOpenGLES2GetErrorString( int err )
{
    switch( err )
    {
    case GL_NO_ERROR:
        return "GL_NO_ERROR";
    case GL_INVALID_ENUM:
        return "GL_INVALID_ENUM";
    case GL_INVALID_VALUE:
        return "GL_INVALID_VALUE";
    case GL_INVALID_OPERATION:
        return "GL_INVALID_OPERATION";
    case GL_OUT_OF_MEMORY:
        return "GL_OUT_OF_MEMORY";
    case GL_INVALID_FRAMEBUFFER_OPERATION:
        return "GL_INVALID_FRAMEBUFFER_OPERATION";        
    default:
        return "UNKNOWN";
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2ClearGLError( void )
{
    int i;
    
    /* Beware infinite loop. */
    for( i = 0; i < 256; ++i )
    {
        if( glGetError() == GL_NO_ERROR )
        {
            break;
        }
    }
}
