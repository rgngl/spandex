/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_OPENGLES2UTILS_H__ )
#define __SCT_OPENGLES2UTILS_H__

#include "sct_types.h"
#include "sct_utils.h"
#include "sct_gl2.h"

/*!
 *
 */
typedef enum
{
    OPENGLES2_LUMINANCE8,
    OPENGLES2_ALPHA8,
    OPENGLES2_LUMINANCE_ALPHA88,
    OPENGLES2_RGB565,
    OPENGLES2_RGB888,
    OPENGLES2_RGBA4444,
    OPENGLES2_RGBA5551,
    OPENGLES2_RGBA8888,
    OPENGLES2_BGRA8888,    
    OPENGLES2_DEPTH,
    OPENGLES2_DEPTH32,
    OPENGLES2_DEPTH24_STENCIL8,
} SCTOpenGLES2TextureType;

/*!
 *
 */
typedef enum
{
    OPENGLES2_GRADIENT_VERTICAL,
    OPENGLES2_GRADIENT_HORIZONTAL,
    OPENGLES2_GRADIENT_DIAGONAL,    
} SCTOpenGLES2Gradient;

/*!
 *
 */
typedef enum
{
    OPENGLES2_NOISE_RANDOM,
    OPENGLES2_NOISE_ORIGINAL_PERLIN,
    OPENGLES2_NOISE_LOW_QUALITY_PERLIN,
    OPENGLES2_NOISE_HIGH_QUALITY_PERLIN,    
} SCTOpenGLES2Noise;

/*!
 *
 */
typedef enum
{
    OPENGLES2_SHAPE_ROUND,
} SCTOpenGLES2Shape;

/*!
 *
 */
typedef struct
{
    int                                 width;
    int                                 height;
    unsigned char*                      data;
} SCTOpenGLES2MipMap;

/*!
 *
 */
typedef struct
{
    SCTOpenGLES2TextureType             type;
    GLenum                              glFormat;
    GLenum                              glType;
    int                                 mipmapCount;
    SCTOpenGLES2MipMap*                 mipmaps;
} SCTOpenGLES2TextureData;

/*!
 *
 */
typedef struct
{
    int                                 width;
    int                                 height;
    int                                 alignment;
    int                                 imageSize;
    unsigned char*                      data;
} SCTOpenGLES2CompressedMipMap;

/*!
 *
 */
typedef struct
{
    GLenum                              internalFormat;
    int                                 mipmapCount;
    SCTOpenGLES2CompressedMipMap*       mipmaps;
} SCTOpenGLES2CompressedTextureData;

/*
 *
 */
typedef struct
{
    GLuint                              texture;
} SCTOpenGLES2Texture;

/*
 *
 */
typedef struct
{
    GLuint                              framebuffer;
} SCTOpenGLES2Framebuffer;

/*
 *
 */
typedef struct
{
    GLuint                              renderbuffer;
} SCTOpenGLES2Renderbuffer;

/*!
 *
 */
typedef struct
{
    GLenum                              type;
    int                                 length;
    int                                 lengthInBytes;
    int                                 maxLengthInBytes;
    void*                               data;
} SCTOpenGLES2Array;

/*!
 *
 */
typedef struct
{
    GLenum                              type;
    int                                 length;
    int                                 lengthInBytes;
    int                                 components;
    int                                 stride;
    unsigned int                        offset;
} SCTOpenGLES2BufferItem;

/*!
 *
 */
typedef struct
{
    GLuint                              buffer;
    SCTOpenGLES2BufferItem*             items;
    int                                 itemCount;
} SCTOpenGLES2Buffer;

/*
 *
 */
typedef struct
{
    GLuint                              shader;
} SCTOpenGLES2Shader;

/*
 *
 */
typedef struct
{
    GLuint                              program;
} SCTOpenGLES2Program;

/*
 *
 */
typedef struct
{
    void*                               data;
    int                                 length;
} SCTOpenGLES2Data;

/*!
 *
 */
typedef struct
{
    int                                 elementSizeInMem;
    int                                 components;
    GLenum                              type;
    int                                 stride;
    void*                               pointer;
} SCTOpenGLES2MeshItem;

/*!
 *
 */
typedef struct
{
    int                                 attributeCount;
    int                                 attributeArrayCount;
    SCTOpenGLES2MeshItem*               attributeArrays;
    int                                 length;
    void*                               data;
} SCTOpenGLES2Mesh;

/*
 *
 */
typedef struct
{
    void*                               mutex;
    SCTBoolean                          flagged;
    float                               value;
} SCTOpenGLES2Flag;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */
    
    SCTOpenGLES2TextureData*    sctOpenGLES2CreateNullTextureData( SCTOpenGLES2TextureType type, int width, int height, SCTBoolean mipmaps );
    SCTOpenGLES2TextureData*    sctOpenGLES2CreateSolidTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color[ 4 ], SCTBoolean mipmaps );
    SCTOpenGLES2TextureData*    sctOpenGLES2CreateBarTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int bars, SCTBoolean mipmaps );
    SCTOpenGLES2TextureData*    sctOpenGLES2CreateCheckerTextureData( SCTOpenGLES2TextureType type, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int checkersX, int checkersY, SCTBoolean mipmaps );
    SCTOpenGLES2TextureData*    sctOpenGLES2CreateGradientTextureData( SCTOpenGLES2TextureType type, SCTOpenGLES2Gradient gradient, int width, int height, const float color1[ 4 ], const float color2[ 4 ], SCTBoolean mipmaps );
    SCTOpenGLES2TextureData*    sctOpenGLES2CreateNoiseTextureData( SCTOpenGLES2TextureType type, SCTOpenGLES2Noise noise, int width, int height, const float* parameters, int parameterCount, SCTBoolean mipmaps );
    SCTOpenGLES2TextureData*    sctOpenGLES2CreateRoundTextureData( SCTOpenGLES2TextureType type, int size, const float color1[ 4 ], const float color2[ 4 ], SCTBoolean mipmaps );
    
    void                        sctOpenGLES2DestroyTextureData( SCTOpenGLES2TextureData* textureData );

    SCTOpenGLES2CompressedTextureData*  sctOpenGLES2CreateCompressedTextureData( GLenum internalFormat, SCTOpenGLES2CompressedMipMap* mipmaps, int mipmapCount );
    void                                sctOpenGLES2DestroyCompressedMipMapData( SCTOpenGLES2CompressedMipMap* mipmaps, int mipmapCount );
    void                                sctOpenGLES2DestroyCompressedTextureData( SCTOpenGLES2CompressedTextureData* textureData );

    int                         sctOpenGLES2GetTexelSize( SCTOpenGLES2TextureType type );
    SCTBoolean                  sctOpenGLES2MapTextureTypeToGL( SCTOpenGLES2TextureType type, GLenum* glFormat, GLenum* glType );
    int                         sctOpenGLES2GetMipmapLevels( int width, int height );
    int                         sctOpenGLES2GetGLTypeSize( GLenum type );

    SCTOpenGLES2Texture*        sctOpenGLES2CreateTexture( GLuint texture );
    void                        sctOpenGLES2DestroyTexture( SCTOpenGLES2Texture* texture );

    SCTOpenGLES2Framebuffer*    sctOpenGLES2CreateFramebuffer( GLuint framebuffer );
    void                        sctOpenGLES2DestroyFramebuffer( SCTOpenGLES2Framebuffer* framebuffer );

    SCTOpenGLES2Renderbuffer*   sctOpenGLES2CreateRenderbuffer( GLuint renderbuffer );
    void                        sctOpenGLES2DestroyRenderbuffer( SCTOpenGLES2Renderbuffer* renderbuffer );

    SCTOpenGLES2Array*          sctOpenGLES2CreateArray( GLenum type );
    SCTBoolean                  sctOpenGLES2AddDataToArray( SCTOpenGLES2Array* array, const void* data, int length );
    SCTBoolean                  sctOpenGLES2AddDoubleDataToArray( SCTOpenGLES2Array* array, const double* data, int length );
    SCTBoolean                  sctOpenGLES2ClearArray( SCTOpenGLES2Array* array );
    void                        sctOpenGLES2DestroyArray( SCTOpenGLES2Array* array );
    SCTBoolean                  sctOpenGLES2ConvertDoubleArray( void* dst, GLenum dstType, const double* src, int length );
    unsigned short              sctiOpenGLES2FloatToHalfFloat( float f );
    
    SCTOpenGLES2Buffer*         sctOpenGLES2CreateBuffer( GLuint buffer );
    SCTBoolean                  sctOpenGLES2InitializeBufferFromArray( SCTOpenGLES2Buffer* buffer, SCTOpenGLES2Array* array, int offset, int length );
    SCTBoolean                  sctOpenGLES2InitializeBufferFromMesh( SCTOpenGLES2Buffer* buffer, SCTOpenGLES2Mesh* mesh );
    void                        sctOpenGLES2DestroyBuffer( SCTOpenGLES2Buffer* buffer );
    SCTBoolean                  sctOpenGLES2IsValidBufferArrayIndex( SCTOpenGLES2Buffer* buffer, int index );
    int                         sctOpenGLES2GetBufferArrayCount( SCTOpenGLES2Buffer* buffer );
    GLenum                      sctOpenGLES2GetBufferArrayType( SCTOpenGLES2Buffer* buffer, int index );
    int                         sctOpenGLES2GetBufferArrayLength( SCTOpenGLES2Buffer* buffer, int index );
    int                         sctOpenGLES2GetBufferArrayLengthInBytes( SCTOpenGLES2Buffer* buffer, int index );
    int                         sctOpenGLES2GetBufferArrayComponents( SCTOpenGLES2Buffer* buffer, int index );
    int                         sctOpenGLES2GetBufferArrayStride( SCTOpenGLES2Buffer* buffer, int index );
    int                         sctOpenGLES2GetBufferArrayOffset( SCTOpenGLES2Buffer* buffer, int index );

    SCTOpenGLES2Shader*         sctOpenGLES2CreateShader( GLuint shader );
    void                        sctOpenGLES2DestroyShader( SCTOpenGLES2Shader* program );

    SCTOpenGLES2Program*        sctOpenGLES2CreateProgram( GLuint program );
    void                        sctOpenGLES2DestroyProgram( SCTOpenGLES2Program* program );

    SCTOpenGLES2Data*           sctOpenGLES2CreateData( void* data, int length, SCTBoolean copy );
    void                        sctOpenGLES2DestroyData( SCTOpenGLES2Data* data );

    SCTOpenGLES2Mesh*           sctOpenGLES2CreateMesh( SCTOpenGLES2Array* arrays[], int components[], int arraysLength );
    void                        sctOpenGLES2DestroyMesh( SCTOpenGLES2Mesh* mesh );
    int                         sctOpenGLES2MeshLength( SCTOpenGLES2Mesh* mesh );
    void*                       sctOpenGLES2MeshData( SCTOpenGLES2Mesh* mesh );
    SCTBoolean                  sctOpenGLES2IsValidMeshArrayIndex( SCTOpenGLES2Mesh* mesh, int index );
    int                         sctOpenGLES2MeshArrayComponents( SCTOpenGLES2Mesh* mesh, int index );
    GLenum                      sctOpenGLES2MeshArrayType( SCTOpenGLES2Mesh* mesh, int index );
    int                         sctOpenGLES2MeshArrayStride( SCTOpenGLES2Mesh* mesh, int index );
    void*                       sctOpenGLES2MeshArrayPointer( SCTOpenGLES2Mesh* mesh, int index );

    SCTOpenGLES2Flag*           sctOpenGLES2CreateFlag( void );
    void                        sctOpenGLES2SetFlag( SCTOpenGLES2Flag* flag, float value );
    SCTBoolean                  sctOpenGLES2GetFlag( SCTOpenGLES2Flag* flag, float* value );    
    void                        sctOpenGLES2ClearFlag( SCTOpenGLES2Flag* flag );    
    void                        sctOpenGLES2DestroyFlag( SCTOpenGLES2Flag* flag );
    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_OPENGLES2UTILS_H__ ) */
