/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2createmeshaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"
#include "sct_opengles2utils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CreateMeshActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CreateMeshActionContext*    context;
    int                                     i;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CreateMeshActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CreateMeshActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateMesh@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CreateMeshActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CreateMeshActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateMeshActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidMeshIndex( context->moduleContext, context->data.meshIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateMeshActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in CreateMesh@OpenGLES2 context creation." );
        return NULL;
    }

    for( i = 0; i < context->data.arrayIndices->length; ++i )
    {
        if( sctiOpenGLES2ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndices->data[ i ] ) == SCT_FALSE )
        {
            sctiDestroyOpenGLES2CreateMeshActionContext( context );
            SCT_LOG_ERROR( "Array index out of range in CreateMesh@OpenGLES2 context creation." );
            return NULL;
        }
    }

    context->arrays = ( SCTOpenGLES2Array** )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2Array* ) * context->data.arrayIndices->length ) );
    if( context->arrays == NULL )
    {
        sctiDestroyOpenGLES2CreateMeshActionContext( context );
        SCT_LOG_ERROR( "Allocation failed in CreateMesh@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CreateMeshActionContext( void* context )
{
    SCTOpenGLES2CreateMeshActionContext*    c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenGLES2CreateMeshActionContext* )( context );

    if( c->arrays != NULL )
    {
        siCommonMemoryFree( NULL, c->arrays );
        c->arrays = NULL;
    }

    if( c->data.arrayIndices != NULL )
    {
        sctDestroyIntVector( c->data.arrayIndices );
        c->data.arrayIndices = NULL;
    }

    if( c->data.sizes != NULL )
    {
        sctDestroyIntVector( c->data.sizes );
        c->data.sizes = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateMeshActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateMeshActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CreateMeshActionContext*    context;
    OpenGLES2CreateMeshActionData*          data;
    SCTOpenGLES2Mesh*                       mesh;
    int                                     i;
    int                                     length;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CreateMeshActionContext* )( action->context );
    data    = &( context->data );

    length = data->arrayIndices->length;

    mesh = sctiOpenGLES2ModuleGetMesh( context->moduleContext, data->meshIndex );
    if( mesh != NULL )
    {
        SCT_LOG_ERROR( "Mesh index already used in CreateMesh@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    for( i = 0; i < length; ++i )
    {
        context->arrays[ i ] = sctiOpenGLES2ModuleGetArray( context->moduleContext, data->arrayIndices->data[ i ] );
        if( context->arrays[ i ] == NULL )
        {
            SCT_LOG_ERROR( "Invalid array index in CreateMesh@OpenGLES2 action execute." );
            return SCT_FALSE;
        }
    }

    mesh = sctOpenGLES2CreateMesh( context->arrays, data->sizes->data, length );
    if( mesh == NULL )
    {
        SCT_LOG_ERROR( "Mesh creation failed in CreateMesh@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES2ModuleSetMesh( context->moduleContext, data->meshIndex, mesh );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateMeshActionTerminate( SCTAction* action )
{
    SCTOpenGLES2CreateMeshActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2CreateMeshActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteMesh( context->moduleContext, context->data.meshIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateMeshActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CreateMeshActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CreateMeshActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CreateMeshActionContext( context );
}
