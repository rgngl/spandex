/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2decodeatcaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

#define SCT_ATC_HEADER_LENGTH           20
#define SCT_MINIMUM_ATC_LENGTH          SCT_ATC_HEADER_LENGTH + 4

/*!
 *
 *
 */
typedef struct
{
    unsigned int        signature;
    unsigned int        width;
    unsigned int        height;
    unsigned int        flags;
    unsigned int        dataOffset;  // From start of header/file
} SCT_ATC_HEADER;

/*!
 *
 *
 */
#define ATC_SIGNATURE   0xCCC40002
#define ATC_RGB         0x00000001
#define ATC_RGBA        0x00000002
#define ATC_TILED       0X00000004

/*!
 *
 *
 */
void* sctiCreateOpenGLES2DecodeATCActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2DecodeATCActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2DecodeATCActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2DecodeATCActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DecodeATC@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2DecodeATCActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2DecodeATCActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DecodeATCActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidDataIndex( context->moduleContext, context->data.srcDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DecodeATCActionContext( context );
        SCT_LOG_ERROR( "Invalid source data index in DecodeATC@OpenGLES2 context creation." );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidDataIndex( context->moduleContext, context->data.dstDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DecodeATCActionContext( context );
        SCT_LOG_ERROR( "Invalid destination data index in DecodeATC@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2DecodeATCActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2DecodeATCActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2DecodeATCActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2DecodeATCActionContext* context;
    OpenGLES2DecodeATCActionData*       data;
    SCTOpenGLES2Data*                   srcData;
    SCTOpenGLES2Data*                   dstData;
    unsigned char*                      ptr;
    int                                 length;
    SCT_ATC_HEADER*                     header;
    unsigned int                        totalBlocks;
    unsigned int                        hasAlpha;
    unsigned int                        bytesPerBlock;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    SCT_ASSERT( sizeof( SCT_ATC_HEADER ) == SCT_ATC_HEADER_LENGTH );

    context = ( SCTOpenGLES2DecodeATCActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiOpenGLES2ModuleGetData( context->moduleContext, data->srcDataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data index in DecodeATC@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( srcData->length < SCT_MINIMUM_ATC_LENGTH )
    {
        SCT_LOG_ERROR( "Invalid ATC data in DecodeATC@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    header = ( SCT_ATC_HEADER* )( srcData->data );

    if( header->signature != ATC_SIGNATURE )
    {
        SCT_LOG_ERROR( "Invalid ATC signature in DecodeATC@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    totalBlocks   = ( ( header->width + 3 ) >> 2 ) * ( ( header->height + 3 ) >> 2 );
    hasAlpha      = header->flags & ATC_RGBA;
    bytesPerBlock = ( hasAlpha ) ? 16 : 8;
    length        = totalBlocks * bytesPerBlock;

    if( srcData->length < ( SCT_ATC_HEADER_LENGTH + length ) )
    {
        SCT_LOG_ERROR( "Invalid ATC length in DecodeATC@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    ptr = &( ( unsigned char* )( srcData->data ) )[ header->dataOffset ];

    dstData = sctiOpenGLES2ModuleGetData( context->moduleContext, data->dstDataIndex );
    if( dstData != NULL )
    {
        if( dstData->length != length )
        {
            SCT_LOG_ERROR( "Destination data index already in use in DecodeATC@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        memcpy( dstData->data, ptr, length );
    }
    else
    {
        dstData = sctOpenGLES2CreateData( ptr, length, SCT_TRUE );
        if( dstData == NULL )
        {
            SCT_LOG_ERROR( "Destination data allocation failed in DecodeATC@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        sctiOpenGLES2ModuleSetData( context->moduleContext, data->dstDataIndex, dstData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2DecodeATCActionTerminate( SCTAction* action )
{
    SCTOpenGLES2DecodeATCActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2DecodeATCActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteData( context->moduleContext, context->data.dstDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2DecodeATCActionDestroy( SCTAction* action )
{
    SCTOpenGLES2DecodeATCActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2DecodeATCActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2DecodeATCActionContext( context );
}
