/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2createshaderaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CreateShaderActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CreateShaderActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CreateShaderActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CreateShaderActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateShader@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CreateShaderActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CreateShaderActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateShaderActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidShaderIndex( context->moduleContext, context->data.shaderIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateShaderActionContext( context );
        SCT_LOG_ERROR( "Invalid shader index in CreateShader@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CreateShaderActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateShaderActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateShaderActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CreateShaderActionContext*  context;
    OpenGLES2CreateShaderActionData*        data;
    SCTOpenGLES2Shader*                     shader;
    GLuint                                  shaderId;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CreateShaderActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetShader( context->moduleContext, data->shaderIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Shader index already in use in CreateShader@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    shaderId = glCreateShader( data->type );
    if( shaderId == 0 )
    {
        sctiOpenGLES2ClearGLError();
        SCT_LOG_ERROR( "Shader creation failed in CreateShader@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    shader = sctOpenGLES2CreateShader( shaderId );
    if( shader == NULL )
    {      
        glDeleteShader( shaderId );
        SCT_LOG_ERROR( "Memory allocation failed in CreateShader@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    
    sctiOpenGLES2ModuleSetShader( context->moduleContext, data->shaderIndex, shader );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateShaderActionTerminate( SCTAction* action )
{
    SCTOpenGLES2CreateShaderActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2CreateShaderActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteShader( context->moduleContext, context->data.shaderIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateShaderActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CreateShaderActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CreateShaderActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CreateShaderActionContext( context );
}
