/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2buffermeshvertexattribpointeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2BufferMeshVertexAttribPointerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2BufferMeshVertexAttribPointerActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2BufferMeshVertexAttribPointerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2BufferMeshVertexAttribPointerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BufferMeshVertexAttribPointer@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2BufferMeshVertexAttribPointerActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2BufferMeshVertexAttribPointerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BufferMeshVertexAttribPointerActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BufferMeshVertexAttribPointerActionContext( context );        
        SCT_LOG_ERROR( "Invalid buffer index in BufferMeshVertexAttribPointer@OpenGLES2 context creation." );
        return NULL;
    }
    
    if( sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.registerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BufferMeshVertexAttribPointerActionContext( context );
        SCT_LOG_ERROR( "Invalid register index in BufferMeshVertexAttribPointer@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2BufferMeshVertexAttribPointerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2BufferMeshVertexAttribPointerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2BufferMeshVertexAttribPointerActionContext* context;
    OpenGLES2BufferMeshVertexAttribPointerActionData*       data;
    SCTOpenGLES2Buffer*                                     buffer;
    GLenum                                                  err;
    int                                                     index;
    GLboolean                                               normalized      = GL_FALSE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2BufferMeshVertexAttribPointerActionContext* )( action->context );
    data    = &( context->data );

    index = ( int )( sctiOpenGLES2ModuleGetRegisterValue( context->moduleContext, data->registerIndex ) );
    if( index < 0 )
    {
        SCT_LOG_ERROR( "Invalid location index in BufferMeshVertexAttribPointer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    buffer = sctiOpenGLES2ModuleGetBuffer( context->moduleContext, data->bufferIndex );
    if( buffer == NULL )
    {
        SCT_LOG_ERROR( "Invalid buffer in BufferMeshVertexAttribPointer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES2IsValidBufferArrayIndex( buffer, data->arrayIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Array index out of bounds in BufferMeshVertexAttribPointer@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( data->normalized == SCT_TRUE )
    {
        normalized = GL_TRUE;
    }

    if( data->bind != SCT_FALSE )
    {
        glBindBuffer( GL_ARRAY_BUFFER, buffer->buffer );
    }
    glVertexAttribPointer( index, 
                           sctOpenGLES2GetBufferArrayComponents( buffer, data->arrayIndex ), 
                           sctOpenGLES2GetBufferArrayType( buffer, data->arrayIndex ), 
                           normalized, 
                           sctOpenGLES2GetBufferArrayStride( buffer, data->arrayIndex ), 
                           ( const void* )( sctOpenGLES2GetBufferArrayOffset( buffer, data->arrayIndex ) ) );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BufferMeshVertexAttribPointer@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2BufferMeshVertexAttribPointerActionDestroy( SCTAction* action )
{
    SCTOpenGLES2BufferMeshVertexAttribPointerActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2BufferMeshVertexAttribPointerActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2BufferMeshVertexAttribPointerActionContext( context );
}
