/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2setregisteraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2SetRegisterActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2SetRegisterActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2SetRegisterActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2SetRegisterActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SetRegister@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2SetRegisterActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2SetRegisterActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2SetRegisterActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.registerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2SetRegisterActionContext( context );
        SCT_LOG_ERROR( "Invalid register index in SetRegister@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2SetRegisterActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2SetRegisterActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2SetRegisterActionContext*   context;
    OpenGLES2SetRegisterActionData*         data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2SetRegisterActionContext* )( action->context );
    data    = &( context->data );

    sctiOpenGLES2ModuleSetRegisterValue( context->moduleContext, data->registerIndex, data->value );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2SetRegisterActionDestroy( SCTAction* action )
{
    SCTOpenGLES2SetRegisterActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2SetRegisterActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2SetRegisterActionContext( context );
}
