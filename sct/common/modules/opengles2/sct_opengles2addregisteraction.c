/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2addregisteraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2AddRegisterActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2AddRegisterActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2AddRegisterActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2AddRegisterActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in AddRegister@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2AddRegisterActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2AddRegisterActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2AddRegisterActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.registerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2AddRegisterActionContext( context );
        SCT_LOG_ERROR( "Invalid register index in AddRegister@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2AddRegisterActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2AddRegisterActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2AddRegisterActionContext*   context;
    OpenGLES2AddRegisterActionData*         data;
    double                                  v;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2AddRegisterActionContext* )( action->context );
    data    = &( context->data );

    v = sctiOpenGLES2ModuleGetRegisterValue( context->moduleContext, data->registerIndex );
    sctiOpenGLES2ModuleSetRegisterValue( context->moduleContext,
                                         data->registerIndex,
                                         data->value + v );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2AddRegisterActionDestroy( SCTAction* action )
{
    SCTOpenGLES2AddRegisterActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2AddRegisterActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2AddRegisterActionContext( context );
}
