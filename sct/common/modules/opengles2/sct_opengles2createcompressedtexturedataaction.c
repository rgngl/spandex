/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2createcompressedtexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CreateCompressedTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CreateCompressedTextureDataActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CreateCompressedTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CreateCompressedTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateCompressedTextureData@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CreateCompressedTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CreateCompressedTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateCompressedTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidCompressedTextureDataIndex( context->moduleContext, context->data.compressedTextureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateCompressedTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid compressed texture data index in CreateCompressedTextureData@OpenGLES2 context creation." );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateCompressedTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in CreateCompressedTextureData@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CreateCompressedTextureDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateCompressedTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateCompressedTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CreateCompressedTextureDataActionContext*   context;
    OpenGLES2CreateCompressedTextureDataActionData*         data;
    SCTOpenGLES2Data*                                       rawData;
    SCTOpenGLES2CompressedTextureData*                      compressedTextureData;
    SCTOpenGLES2CompressedMipMap*                           compressedMipmap;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CreateCompressedTextureDataActionContext* )( action->context );
    data    = &( context->data );

    rawData = sctiOpenGLES2ModuleGetData( context->moduleContext, data->dataIndex );
    if( rawData == NULL )
    {
        SCT_LOG_ERROR( "Invalid data index in CreateCompressedTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    compressedTextureData = sctiOpenGLES2ModuleGetCompressedTextureData( context->moduleContext, data->compressedTextureDataIndex );
    if( compressedTextureData != NULL )
    {
        if( compressedTextureData->internalFormat != data->internalFormat ||
            compressedTextureData->mipmapCount != 1 ||
            compressedTextureData->mipmaps[ 0 ].width != data->width ||
            compressedTextureData->mipmaps[ 0 ].height != data->height ||
            compressedTextureData->mipmaps[ 0 ].imageSize != rawData->length )
        {
            SCT_LOG_ERROR( "Compressed texture data index already used in CreateCompressedTextureData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        memcpy( compressedTextureData->mipmaps[ 0 ].data, rawData->data, rawData->length );
    }
    else
    {
        compressedMipmap = ( SCTOpenGLES2CompressedMipMap* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CompressedMipMap ) ) );
        if( compressedMipmap == NULL )
        {
            SCT_LOG_ERROR( "Memory allocation failed in CreateCompressedTextureData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        compressedMipmap->width     = data->width;
        compressedMipmap->height    = data->height;
        compressedMipmap->alignment = 1;
        compressedMipmap->imageSize = rawData->length;
        compressedMipmap->data = ( unsigned char* )( siCommonMemoryAlloc( NULL, rawData->length ) );
        if( compressedMipmap->data == NULL )
        {
            siCommonMemoryFree( NULL, compressedMipmap );
            SCT_LOG_ERROR( "Memory allocation failed in CreateCompressedTextureData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        memcpy( compressedMipmap->data, rawData->data, rawData->length );

        compressedTextureData = ( SCTOpenGLES2CompressedTextureData* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CompressedTextureData ) ) );
        if( compressedTextureData == NULL )
        {
            siCommonMemoryFree( NULL, compressedMipmap->data );
            siCommonMemoryFree( NULL, compressedMipmap );
            SCT_LOG_ERROR( "Memory allocation failed in CreateCompressedTextureData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        compressedTextureData->internalFormat       = data->internalFormat;
        compressedTextureData->mipmapCount          = 1;
        compressedTextureData->mipmaps              = compressedMipmap;

        sctiOpenGLES2ModuleSetCompressedTextureData( context->moduleContext, data->compressedTextureDataIndex, compressedTextureData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateCompressedTextureDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES2CreateCompressedTextureDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2CreateCompressedTextureDataActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteCompressedTextureData( context->moduleContext, context->data.compressedTextureDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateCompressedTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CreateCompressedTextureDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CreateCompressedTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CreateCompressedTextureDataActionContext( context );
}
