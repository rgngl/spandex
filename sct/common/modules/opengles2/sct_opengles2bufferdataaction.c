/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2bufferdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2BufferDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2BufferDataActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2BufferDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2BufferDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BufferData@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2BufferDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2BufferDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BufferDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BufferDataActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in BufferData@OpenGLES2 context creation." );
        return NULL;
    }
    
    if( sctiOpenGLES2ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BufferDataActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in BufferData@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2BufferDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2BufferDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2BufferDataActionContext*    context;
    GLenum                                  err;
    OpenGLES2BufferDataActionData*          data;
    SCTOpenGLES2Array*                      array;
    SCTOpenGLES2Buffer*                     buffer;
    int                                     elementSize;
    int                                     offset;
    int                                     size;
    unsigned char*                          ptr;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2BufferDataActionContext* )( action->context );
    data    = &( context->data );

    array = sctiOpenGLES2ModuleGetArray( context->moduleContext, data->arrayIndex );
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array in BufferData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    buffer = sctiOpenGLES2ModuleGetBuffer( context->moduleContext, data->bufferIndex );
    if( buffer == NULL )
    {
        SCT_LOG_ERROR( "Invalid buffer in BufferData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    elementSize = sctOpenGLES2GetGLTypeSize( array->type );
    offset      = data->offset * elementSize;

    if( offset >= array->lengthInBytes )
    {
        SCT_LOG_ERROR( "Offset exceeds array length in BufferData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( data->size <= 0 )
    {
        if( array->lengthInBytes == 0 )
        {
            SCT_LOG_ERROR( "Undefined size with null array data in BufferData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        size = ( array->lengthInBytes - offset );
    }
    else
    {
        size = data->size * elementSize;

        if( ( array->data != NULL ) && ( ( offset + size ) > array->lengthInBytes ) )
        {
            SCT_LOG_ERROR( "Offset + size exceeds array length in BufferData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }
    }

    ptr = ( unsigned char* )( array->data ) + offset;
    if( data->bind != SCT_FALSE )
    {
        glBindBuffer( data->target, buffer->buffer );
    }
    glBufferData( data->target, size, ptr, data->usage );
    sctOpenGLES2InitializeBufferFromArray( buffer, array, ( data->offset ), ( size / elementSize ) );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BufferData@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2BufferDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES2BufferDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2BufferDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2BufferDataActionContext( context );
}
