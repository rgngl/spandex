/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2meshbufferdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2MeshBufferDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2MeshBufferDataActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2MeshBufferDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2MeshBufferDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in MeshBufferData@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2MeshBufferDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2MeshBufferDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2MeshBufferDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidBufferIndex( context->moduleContext, context->data.bufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2MeshBufferDataActionContext( context );
        SCT_LOG_ERROR( "Invalid buffer index in MeshBufferData@OpenGLES2 context creation." );
        return NULL;
    }
    
    if( sctiOpenGLES2ModuleIsValidMeshIndex( context->moduleContext, context->data.meshIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2MeshBufferDataActionContext( context );
        SCT_LOG_ERROR( "Invalid mesh index in MeshBufferData@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2MeshBufferDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2MeshBufferDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2MeshBufferDataActionContext*    context;
    GLenum                                      err;
    OpenGLES2MeshBufferDataActionData*          data;
    SCTOpenGLES2Mesh*                           mesh;
    SCTOpenGLES2Buffer*                         buffer;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2MeshBufferDataActionContext* )( action->context );
    data    = &( context->data );

    buffer = sctiOpenGLES2ModuleGetBuffer( context->moduleContext, data->bufferIndex );
    if( buffer == NULL )
    {
        SCT_LOG_ERROR( "Invalid buffer in MeshBufferData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
   
    mesh = sctiOpenGLES2ModuleGetMesh( context->moduleContext, data->meshIndex );
    if( mesh == NULL )
    {
        SCT_LOG_ERROR( "Invalid mesh in MeshBufferData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( data->bind != SCT_FALSE )
    {
        glBindBuffer( GL_ARRAY_BUFFER, buffer->buffer );
    }
    glBufferData( GL_ARRAY_BUFFER, 
                  sctOpenGLES2MeshLength( mesh ), 
                  sctOpenGLES2MeshData( mesh ), 
                  data->usage );

    sctOpenGLES2InitializeBufferFromMesh( buffer, mesh );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in MeshBufferData@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2MeshBufferDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES2MeshBufferDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2MeshBufferDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2MeshBufferDataActionContext( context );
}
