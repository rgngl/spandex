/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2checkerroraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CheckErrorActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CheckErrorActionContext*    context;
    SCTOpenGLES2ModuleContext*              mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenGLES2ModuleContext* )( moduleContext );
    context = ( SCTOpenGLES2CheckErrorActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CheckErrorActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CheckError@OpenGLES2 context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CheckErrorActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenGLES2CheckErrorActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CheckErrorActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CheckErrorActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CheckErrorActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CheckErrorActionContext*    context;
    OpenGLES2CheckErrorActionData*          data;
    GLenum                                  err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CheckErrorActionContext* )( action->context );
    data    = &( context->data );
    
    err = glGetError();
    
    if( err != GL_NO_ERROR )
    {
        char buf[ 512 ];
        if( data->message != NULL && strlen( data->message ) < 1 )
        {
            sprintf( buf, "GL error %s in CheckError@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        }
        else
        {
            sprintf( buf, "GL error %s, message \"%s\" in CheckError@OpenGLES2 action execute.",
                     sctiOpenGLES2GetErrorString( err ), data->message );
        }
        SCT_LOG_ERROR( buf );

#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: %s", buf );
#endif  /* defined( SCT_DEBUG ) */
        
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CheckErrorActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenGLES2CheckErrorActionContext( action->context );
    action->context = NULL;
}
