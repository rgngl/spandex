/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2stashtextureaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"
#include "sct_benchmark.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
static const char*  sctiTextureTargetString( GLenum target );
static void         sctiBytesAsString( unsigned long bytes, char buf[ 64 ] );

/*!
 *
 *
 */
void* sctiCreateOpenGLES2StashTextureActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2StashTextureActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2StashTextureActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2StashTextureActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in StashTexture@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2StashTextureActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2StashTextureActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2StashTextureActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2StashTextureActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in StashTexture@OpenGLES2 context creation." );
        return NULL;
    }

    context->stash = ( GLuint* )( siCommonMemoryAlloc( NULL, context->data.maxTextureCount * sizeof( GLuint ) ) );
    if( context->stash == NULL )
    {
        sctiDestroyOpenGLES2StashTextureActionContext( context );
        SCT_LOG_ERROR( "Allocation failed in StashTexture@OpenGLES2 context creation." );
        return NULL;
    }
    memset( context->stash, 0, context->data.maxTextureCount * sizeof( GLuint ) );
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2StashTextureActionContext( void* context )
{
    SCTOpenGLES2StashTextureActionContext*  c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenGLES2StashTextureActionContext* )( context );
    
    if( c->stash != NULL )
    {
        siCommonMemoryFree( NULL, c->stash );
        c->stash = NULL;
    }
    
    siCommonMemoryFree( NULL, c );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2StashTextureActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenGLES2StashTextureActionContext*  context;
    int                                     i;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenGLES2StashTextureActionContext* )( action->context );

    context->currentBenchmark   = benchmark;
    context->stashIndex         = 0;
    context->bytes              = 0;
    context->reported           = SCT_FALSE;

    for( i = 0; i < context->data.maxTextureCount; ++i )
    {
        context->stash[ i ] = 0;
    }

    if( context->data.signalComplete != SCT_FALSE )
    {
        if( sctBenchmarkCanComplete( context->currentBenchmark ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Unsupported explicit complete in StashTexture@OpenGLES2 action init." );
            return SCT_FALSE;
        }
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2StashTextureActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2StashTextureActionContext*  context;
    GLenum                                  err;
    int                                     i;
    OpenGLES2StashTextureActionData*        data;
    SCTOpenGLES2TextureData*                textureData;
    int                                     pixelSize;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2StashTextureActionContext* )( action->context );
    data    = &( context->data );

    if( frameNumber < 0 )
    {
        return SCT_TRUE;
    }

    if( data->memoryLimit > 0 && context->bytes > data->memoryLimit )
    {
        return SCT_TRUE;
    }
    
    textureData = sctiOpenGLES2ModuleGetTextureData( context->moduleContext, data->textureDataIndex );
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Invalid texture data in StashTexture@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( frameNumber >= data->maxTextureCount )
    {
        SCT_LOG_ERROR( "Too many textures in StashTexture@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( context->stash[ frameNumber ] != 0 )
    {
        glDeleteTextures( 1, &( context->stash[ frameNumber ] ) );
        context->stash[ frameNumber ] = 0;
    }
    
    glGenTextures( 1, &( context->stash[ frameNumber ] ) );

    if( context->stash[ frameNumber ] == 0 )
    {
        SCT_LOG_ERROR( "OOM reached in StashTexture@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    glBindTexture( data->target, context->stash[ frameNumber ] );    
    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

    pixelSize = sctOpenGLES2GetTexelSize( textureData->type );
    
    for( i = 0; i < textureData->mipmapCount; ++i )
    {
        glTexImage2D( data->target, 
                      i, 
                      textureData->glFormat, 
                      textureData->mipmaps[ i ].width, 
                      textureData->mipmaps[ i ].height, 
                      0, 
                      textureData->glFormat, 
                      textureData->glType,
                      textureData->mipmaps[ i ].data );

        err = glGetError();
        if( err != GL_NO_ERROR )
        {
            if( err == GL_OUT_OF_MEMORY )
            {
                SCT_LOG_ERROR( "OOM reached in StashTexture@OpenGLES2 action execute." );
            }
            else
            {
                char buf[ 256 ];
                sprintf( buf, "GL error %s in StashTexture@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
                SCT_LOG_ERROR( buf );
            }
            
            sctiOpenGLES2ClearGLError();
            
            return SCT_FALSE;
        }

        context->bytes += pixelSize * textureData->mipmaps[ i ].width * textureData->mipmaps[ i ].height;
    }

    if( data->printProgress != SCT_FALSE )
    {
        char    buf[ 256 ];
        char    buf2[ 64 ];

        sctiBytesAsString( context->bytes, buf2 );
        
        sprintf( buf, "SPANDEX: stash texture %s %s", sctiTextureTargetString( data->target ), buf2 );
        siCommonDebugPrintf( NULL, buf );
    }

    if( data->signalComplete != SCT_FALSE )
    {
        if( data->memoryLimit > 0  && context->bytes > data->memoryLimit )
        {
            SCT_ASSERT( context->currentBenchmark != NULL );
            sctBenchmarkComplete( context->currentBenchmark );
            return SCT_TRUE;
        }
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2StashTextureActionTerminate( SCTAction* action )
{
    SCTOpenGLES2StashTextureActionContext*  context;
    SCTAttributeList*                       attributes;
    char                                    buf[ 256 ];
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2StashTextureActionContext* )( action->context );

    if( context->reported == SCT_FALSE )
    {    
        attributes = sctCreateAttributeList();
        if( attributes != NULL )
        {
            sctiBytesAsString( context->bytes, buf );
            sctAddNameValueToList( attributes, "Textures", buf );
        
            sctReportSection( sctiTextureTargetString( context->data.target ), "STASH_TEXTURE", attributes );                
            sctDestroyAttributeList( attributes );

            context->reported = SCT_TRUE;
        }
        
    }

    if( context->stashIndex > 0 )
    {
        glDeleteTextures( context->stashIndex, context->stash );
        context->stashIndex = 0;
    }    
}

/*!
 *
 *
 */
void sctiOpenGLES2StashTextureActionDestroy( SCTAction* action )
{
    SCTOpenGLES2StashTextureActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2StashTextureActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2StashTextureActionContext( context );
}

/*!
 *
 *
 */
static const char* sctiTextureTargetString( GLenum target )
{
    switch( target )
    {
    case GL_TEXTURE_2D:
        return "GL_TEXTURE_2D";

    case GL_TEXTURE_CUBE_MAP_POSITIVE_X:
    case GL_TEXTURE_CUBE_MAP_POSITIVE_Y:
    case GL_TEXTURE_CUBE_MAP_POSITIVE_Z:
    case GL_TEXTURE_CUBE_MAP_NEGATIVE_X:
    case GL_TEXTURE_CUBE_MAP_NEGATIVE_Y:
    case GL_TEXTURE_CUBE_MAP_NEGATIVE_Z:
        return "GL_TEXTURE_CUBE";

    default:
        SCT_ASSERT( 0 );
        return "UNKNOWN";
    }
}

/*!
 *
 *
 */
static void sctiBytesAsString( unsigned long bytes, char buf[ 64 ] )
{
    SCT_ASSERT( buf != NULL );
    
    buf[ 0 ] = '\0';
    
    if( bytes > ( 1024 * 1024 * 10 ) )
    {
        sprintf( buf, "%.2f MB", bytes / ( 1024.0f * 1024.0f ) );
    }
    else if( bytes > ( 1024 * 10 ) )
    {
        sprintf( buf, "%.2f KB", bytes / ( 1024.0f ) );
    }
    else
    {
        sprintf( buf, "%lu B", bytes );
    }            
}
