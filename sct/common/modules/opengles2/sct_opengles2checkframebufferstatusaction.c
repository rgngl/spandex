/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2checkframebufferstatusaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
const char* sctiOpenGLES2GetFramebufferStatusString( GLenum status );

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CheckFramebufferStatusActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CheckFramebufferStatusActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CheckFramebufferStatusActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CheckFramebufferStatusActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CheckFramebufferStatus@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CheckFramebufferStatusActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CheckFramebufferStatusActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CheckFramebufferStatusActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CheckFramebufferStatusActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CheckFramebufferStatusActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CheckFramebufferStatusActionContext*    context;
    GLenum                                              err;
    OpenGLES2CheckFramebufferStatusActionData*          data;
    GLenum                                              status;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CheckFramebufferStatusActionContext* )( action->context );
    data    = &( context->data );

    status = glCheckFramebufferStatus( data->target );

    if( status == 0 || status != GL_FRAMEBUFFER_COMPLETE )
    {
        char buf[ 256 ];
        sprintf( buf, "Status %s in CheckFramebufferStatus@OpenGLES2 action execute.", sctiOpenGLES2GetFramebufferStatusString( status ) );
        SCT_LOG_ERROR( buf );
        
        return SCT_FALSE;
    }

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in CheckFramebufferStatus@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CheckFramebufferStatusActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CheckFramebufferStatusActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CheckFramebufferStatusActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CheckFramebufferStatusActionContext( context );
}

/*!
 *
 *
 */
const char* sctiOpenGLES2GetFramebufferStatusString( GLenum status )
{
    switch( status )
    {
    case GL_FRAMEBUFFER_COMPLETE:
        return "GL_FRAMEBUFFER_COMPLETE";
    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        return "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        return "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";
    case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
        return "GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS";
    case GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
        return "GL_FRAMEBUFFER_INCOMPLETE_FORMATS";
    case GL_FRAMEBUFFER_UNSUPPORTED:
        return "GL_FRAMEBUFFER_UNSUPPORTED";
    default:
        return "UNKNOWN";
    }    
}

