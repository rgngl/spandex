/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2creategrowingtexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CreateGrowingTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CreateGrowingTextureDataActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CreateGrowingTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CreateGrowingTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateGrowingTextureData@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CreateGrowingTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CreateGrowingTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateGrowingTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateGrowingTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in CreateGrowingTextureData@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CreateGrowingTextureDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateGrowingTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateGrowingTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CreateGrowingTextureDataActionContext*  context;
    OpenGLES2CreateGrowingTextureDataActionData*        data;
    SCTOpenGLES2TextureData*                            textureData;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CreateGrowingTextureDataActionContext* )( action->context );
    data    = &( context->data );

    textureData = sctiOpenGLES2ModuleGetTextureData( context->moduleContext, data->textureDataIndex );
    if( textureData != NULL )
    {
        SCT_LOG_ERROR( "Texture data index already used in CreateGrowingTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    
    if( frameNumber < 1 )
    {
        context->currentWidth  = data->width;
        context->currentHeight = data->height;
    }
    else
    {
        context->currentWidth  += data->widthInc;
        context->currentHeight += data->heightInc;

        if( context->currentWidth > data->widthLimit )
        {
            context->currentWidth = data->width;
        }

        if( context->currentHeight > data->heightLimit )
        {
            context->currentHeight = data->height;
        }
    }

    textureData = sctOpenGLES2CreateSolidTextureData( data->type, 
                                                      context->currentWidth, 
                                                      context->currentHeight, 
                                                      data->color, 
                                                      data->mipmaps );
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Texture data creation failed in CreateGrowingTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    sctiOpenGLES2ModuleSetTextureData( context->moduleContext, data->textureDataIndex, textureData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateGrowingTextureDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES2CreateGrowingTextureDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2CreateGrowingTextureDataActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteTextureData( context->moduleContext, context->data.textureDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateGrowingTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CreateGrowingTextureDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CreateGrowingTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CreateGrowingTextureDataActionContext( context );
}
