/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2createnoisetexturedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CreateNoiseTextureDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CreateNoiseTextureDataActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CreateNoiseTextureDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CreateNoiseTextureDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateNoiseTextureData@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CreateNoiseTextureDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CreateNoiseTextureDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateNoiseTextureDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidTextureDataIndex( context->moduleContext, context->data.textureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateNoiseTextureDataActionContext( context );
        SCT_LOG_ERROR( "Invalid texture data index in CreateNoiseTextureData@OpenGLES2 context creation." );
        return NULL;
    }

    switch( context->data.noise )
    {
    case OPENGLES2_NOISE_RANDOM:
        if( context->data.parameters->length != 6 )
        {
            sctiDestroyOpenGLES2CreateNoiseTextureDataActionContext( context );
            SCT_LOG_ERROR( "Invalid parameters for OPENGLES2_NOISE_RANDOM in CreateNoiseTextureData@OpenGLES2 context creation." );
            return NULL;
        }
        break;
        
    case OPENGLES2_NOISE_ORIGINAL_PERLIN:
        if( context->data.parameters->length != 7 )
        {
            sctiDestroyOpenGLES2CreateNoiseTextureDataActionContext( context );
            SCT_LOG_ERROR( "Invalid parameters for OPENGLES2_NOISE_ORIGINAL_PERLIN in CreateNoiseTextureData@OpenGLES2 context creation." );
            return NULL;
        }
        break;

    case OPENGLES2_NOISE_LOW_QUALITY_PERLIN:    /* Intentional */
    case OPENGLES2_NOISE_HIGH_QUALITY_PERLIN:        
        if( context->data.parameters->length != 10 )
        {
            sctiDestroyOpenGLES2CreateNoiseTextureDataActionContext( context );
            SCT_LOG_ERROR( "Invalid parameters for OPENGLES2_NOISE_LOW/HIGH_QUALITY_PERLIN in CreateNoiseTextureData@OpenGLES2 context creation." );
            return NULL;
        }
        break;
    default:
        sctiDestroyOpenGLES2CreateNoiseTextureDataActionContext( context );
        SCT_LOG_ERROR( "Unexpected noise type in CreateNoiseTextureData@OpenGLES2 context creation." );
        return NULL;
    };
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CreateNoiseTextureDataActionContext( void* context )
{
    SCTOpenGLES2CreateNoiseTextureDataActionContext*    c;    

    if( context == NULL )
    {
        return;
    }
    
    c = ( SCTOpenGLES2CreateNoiseTextureDataActionContext* )( context );

    if( c->data.parameters != NULL )
    {
        sctDestroyFloatVector( c->data.parameters );
        c->data.parameters = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateNoiseTextureDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateNoiseTextureDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CreateNoiseTextureDataActionContext*    context;
    OpenGLES2CreateNoiseTextureDataActionData*          data;
    SCTOpenGLES2TextureData*                            textureData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CreateNoiseTextureDataActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetTextureData( context->moduleContext, data->textureDataIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Texture data index already used in CreateNoiseTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    textureData = sctOpenGLES2CreateNoiseTextureData( data->type,
                                                      data->noise,
                                                      data->width, 
                                                      data->height,
                                                      data->parameters->data,
                                                      data->parameters->length,
                                                      data->mipmaps );
    if( textureData == NULL )
    {
        SCT_LOG_ERROR( "Texture data creation failed in CreateNoiseTextureData@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES2ModuleSetTextureData( context->moduleContext, data->textureDataIndex, textureData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateNoiseTextureDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES2CreateNoiseTextureDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2CreateNoiseTextureDataActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteTextureData( context->moduleContext, context->data.textureDataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateNoiseTextureDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CreateNoiseTextureDataActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CreateNoiseTextureDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CreateNoiseTextureDataActionContext( context );
}
