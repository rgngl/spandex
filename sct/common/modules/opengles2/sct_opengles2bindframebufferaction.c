/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2bindframebufferaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2BindFramebufferActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2BindFramebufferActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2BindFramebufferActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2BindFramebufferActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BindFramebuffer@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2BindFramebufferActionContext ) );

    context->created = SCT_FALSE;

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2BindFramebufferActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BindFramebufferActionContext( context );
        return NULL;
    }

    if( context->data.framebufferIndex >= 0 &&
        sctiOpenGLES2ModuleIsValidFramebufferIndex( context->moduleContext, context->data.framebufferIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2BindFramebufferActionContext( context );
        SCT_LOG_ERROR( "Invalid framebuffer index in BindFramebuffer@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2BindFramebufferActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2BindFramebufferActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2BindFramebufferActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2BindFramebufferActionContext*   context;
    GLenum                                      err;
    OpenGLES2BindFramebufferActionData*         data;
    SCTOpenGLES2Framebuffer*                    framebuffer;
    GLuint                                      framebufferId   = 0;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2BindFramebufferActionContext* )( action->context );
    data    = &( context->data );

    if( data->framebufferIndex >= 0 )
    {
        framebuffer = sctiOpenGLES2ModuleGetFramebuffer( context->moduleContext, data->framebufferIndex );
        if( framebuffer == NULL )
        {
            glGenFramebuffers( 1, &framebufferId );
            framebuffer = sctOpenGLES2CreateFramebuffer( framebufferId );
            if( framebuffer == NULL )
            {
                glDeleteFramebuffers( 1, &framebufferId );
                SCT_LOG_ERROR( "Framebuffer creation failed in BindFramebuffer@OpenGLES2 action execute." );
                return SCT_FALSE;
            }
            sctiOpenGLES2ModuleSetFramebuffer( context->moduleContext, data->framebufferIndex, framebuffer );
            context->created = SCT_TRUE;
        }

        framebufferId = framebuffer->framebuffer;
    }
    
    glBindFramebuffer( data->target, framebufferId );
    
#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in BindFramebuffer@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2BindFramebufferActionTerminate( SCTAction* action )
{
    SCTOpenGLES2BindFramebufferActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2BindFramebufferActionContext* )( action->context );

    if( context->created == SCT_TRUE )
    {
        sctiOpenGLES2ModuleDeleteFramebuffer( context->moduleContext, context->data.framebufferIndex );
        context->created = SCT_FALSE;
    }
}

/*!
 *
 *
 */
void sctiOpenGLES2BindFramebufferActionDestroy( SCTAction* action )
{
    SCTOpenGLES2BindFramebufferActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2BindFramebufferActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2BindFramebufferActionContext( context );
}
