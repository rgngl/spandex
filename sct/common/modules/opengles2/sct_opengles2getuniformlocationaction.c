/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2getuniformlocationaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2GetUniformLocationActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2GetUniformLocationActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2GetUniformLocationActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2GetUniformLocationActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in GetUniformLocation@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2GetUniformLocationActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2GetUniformLocationActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2GetUniformLocationActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.registerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2GetUniformLocationActionContext( context );
        SCT_LOG_ERROR( "Invalid register index in GetUniformLocation@OpenGLES2 context creation." );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidProgramIndex( context->moduleContext, context->data.programIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2GetUniformLocationActionContext( context );
        SCT_LOG_ERROR( "Invalid program index in GetUniformLocation@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2GetUniformLocationActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2GetUniformLocationActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2GetUniformLocationActionContext*    context;
    GLenum                                          err;
    OpenGLES2GetUniformLocationActionData*          data;
    SCTOpenGLES2Program*                            program;
    int                                             location;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2GetUniformLocationActionContext* )( action->context );
    data    = &( context->data );

    program = sctiOpenGLES2ModuleGetProgram( context->moduleContext, data->programIndex );
    if( program == NULL )
    {
        SCT_LOG_ERROR( "Invalid program index in GetUniformLocation@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    location = glGetUniformLocation( program->program, data->name );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in GetUniformLocation@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    if( location < 0 )
    {
        SCT_LOG_ERROR( "Invalid location value in GetUniformLocation@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES2ModuleSetRegisterValue( context->moduleContext, data->registerIndex, location );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2GetUniformLocationActionDestroy( SCTAction* action )
{
    SCTOpenGLES2GetUniformLocationActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2GetUniformLocationActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2GetUniformLocationActionContext( context );
}
