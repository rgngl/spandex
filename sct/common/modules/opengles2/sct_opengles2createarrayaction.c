/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2createarrayaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CreateArrayActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CreateArrayActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CreateArrayActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CreateArrayActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateArray@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CreateArrayActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CreateArrayActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateArrayActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CreateArrayActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in CreateArray@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CreateArrayActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateArrayActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CreateArrayActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CreateArrayActionContext*   context;
    OpenGLES2CreateArrayActionData*         data;
    SCTOpenGLES2Array*                      array;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CreateArrayActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenGLES2ModuleGetArray( context->moduleContext, data->arrayIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Array index already used in CreateArray@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    array = sctOpenGLES2CreateArray( data->type );
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Array creation failed in CreateArray@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    sctiOpenGLES2ModuleSetArray( context->moduleContext, data->arrayIndex, array );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateArrayActionTerminate( SCTAction* action )
{
    SCTOpenGLES2CreateArrayActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2CreateArrayActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteArray( context->moduleContext, context->data.arrayIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2CreateArrayActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CreateArrayActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CreateArrayActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CreateArrayActionContext( context );
}
