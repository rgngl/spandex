/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2enablevertexattribarrayaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2EnableVertexAttribArrayActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2EnableVertexAttribArrayActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2EnableVertexAttribArrayActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2EnableVertexAttribArrayActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in EnableVertexAttribArray@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2EnableVertexAttribArrayActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2EnableVertexAttribArrayActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2EnableVertexAttribArrayActionContext( context );
        return NULL;
    }

    if(sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.registerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2EnableVertexAttribArrayActionContext( context );
        SCT_LOG_ERROR( "Invalid register index in EnableVertexAttribArray@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2EnableVertexAttribArrayActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2EnableVertexAttribArrayActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2EnableVertexAttribArrayActionContext*   context;
    GLenum                                              err;
    OpenGLES2EnableVertexAttribArrayActionData*         data;
    int                                                 index;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2EnableVertexAttribArrayActionContext* )( action->context );
    data    = &( context->data );

    index = ( int )( sctiOpenGLES2ModuleGetRegisterValue( context->moduleContext, data->registerIndex ) );
    if( index < 0 )
    {
        SCT_LOG_ERROR( "Invalid value in register in EnableVertexAttribArray@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    glEnableVertexAttribArray( index );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in EnableVertexAttribArray@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2EnableVertexAttribArrayActionDestroy( SCTAction* action )
{
    SCTOpenGLES2EnableVertexAttribArrayActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2EnableVertexAttribArrayActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2EnableVertexAttribArrayActionContext( context );
}
