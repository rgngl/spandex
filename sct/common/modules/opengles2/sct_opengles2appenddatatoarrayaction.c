/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2appenddatatoarrayaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2AppendDataToArrayActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2AppendDataToArrayActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2AppendDataToArrayActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2AppendDataToArrayActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in AppendDataToArray@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2AppendDataToArrayActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2AppendDataToArrayActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2AppendDataToArrayActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2AppendDataToArrayActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in AppendDataToArray@OpenGLES2 context creation." );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2AppendDataToArrayActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in AppendDataToArray@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2AppendDataToArrayActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2AppendDataToArrayActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2AppendDataToArrayActionContext* context;
    OpenGLES2AppendDataToArrayActionData*       data;
    SCTOpenGLES2Data*                           rawData;
    SCTOpenGLES2Array*                          array;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2AppendDataToArrayActionContext* )( action->context );
    data    = &( context->data );

    array = sctiOpenGLES2ModuleGetArray( context->moduleContext, data->arrayIndex );
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array index in AppendDataToArray@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    rawData = sctiOpenGLES2ModuleGetData( context->moduleContext, data->dataIndex );
    if( rawData == NULL )
    {
        SCT_LOG_ERROR( "Invalid data index in AppendDataToArray@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    if( sctOpenGLES2AddDataToArray( array, rawData->data, rawData->length ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Data append failed in AppendDataToArray@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2AppendDataToArrayActionDestroy( SCTAction* action )
{
    SCTOpenGLES2AppendDataToArrayActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2AppendDataToArrayActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2AppendDataToArrayActionContext( context );
}
