/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2linkprogramaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2LinkProgramActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2LinkProgramActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2LinkProgramActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2LinkProgramActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LinkProgram@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2LinkProgramActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2LinkProgramActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2LinkProgramActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidProgramIndex( context->moduleContext, context->data.programIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2LinkProgramActionContext( context );
        SCT_LOG_ERROR( "Invalid program index in LinkProgram@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2LinkProgramActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2LinkProgramActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2LinkProgramActionContext*   context;
    OpenGLES2LinkProgramActionData*         data;
    SCTOpenGLES2Program*                    program;
    GLint                                   linkResult      = GL_FALSE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2LinkProgramActionContext* )( action->context );
    data    = &( context->data );

    program = sctiOpenGLES2ModuleGetProgram( context->moduleContext, data->programIndex );

    if( program == NULL )
    {
        SCT_LOG_ERROR( "Invalid program index in LinkProgram@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    
    glLinkProgram( program->program );

    glGetProgramiv( program->program, GL_LINK_STATUS, &linkResult);
    if( linkResult != GL_TRUE )
    {
        char msgbuf[ 384 ];
        char infobuf[ 256 ];

        glGetProgramInfoLog( program->program, 256, NULL, infobuf );
        
        sprintf( msgbuf, "LinkProgram@OpenGLES2 action execute failed, %s.", infobuf );
        SCT_LOG_ERROR( msgbuf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2LinkProgramActionDestroy( SCTAction* action )
{
    SCTOpenGLES2LinkProgramActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2LinkProgramActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2LinkProgramActionContext( context );
}
