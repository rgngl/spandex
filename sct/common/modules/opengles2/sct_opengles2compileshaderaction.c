/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2compileshaderaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CompileShaderActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CompileShaderActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CompileShaderActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CompileShaderActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CompileShader@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CompileShaderActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CompileShaderActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CompileShaderActionContext( context );
        return NULL;
    }

    if(sctiOpenGLES2ModuleIsValidShaderIndex( context->moduleContext, context->data.shaderIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CompileShaderActionContext( context );
        SCT_LOG_ERROR( "Invalid shader index in CompileShader@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CompileShaderActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CompileShaderActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CompileShaderActionContext* context;
    OpenGLES2CompileShaderActionData*       data;
    SCTOpenGLES2Shader*                     shader;
    GLint                                   compileResult   = GL_FALSE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CompileShaderActionContext* )( action->context );
    data    = &( context->data );

    shader = sctiOpenGLES2ModuleGetShader( context->moduleContext, data->shaderIndex );
    if( shader == NULL )
    {
        SCT_LOG_ERROR( "Invalid shader index in CompileShader@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    glCompileShader( shader->shader );

    glGetShaderiv( shader->shader, GL_COMPILE_STATUS, &compileResult);
    if( compileResult != GL_TRUE )
    {
        char msgbuf[ 384 ];
        char infobuf[ 256 ];

        glGetShaderInfoLog( shader->shader, 256, NULL, infobuf );

        sprintf( msgbuf, "CompileShader@OpenGLES2 action execute failed, %s.", infobuf );
        SCT_LOG_ERROR( msgbuf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CompileShaderActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CompileShaderActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CompileShaderActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CompileShaderActionContext( context );
}
