/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2drawelementsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2DrawElementsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2DrawElementsActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2DrawElementsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2DrawElementsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DrawElements@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2DrawElementsActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2DrawElementsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DrawElementsActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidArrayIndex( context->moduleContext, context->data.arrayIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2DrawElementsActionContext( context );
        SCT_LOG_ERROR( "Invalid array index in DrawElements@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2DrawElementsActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2DrawElementsActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2DrawElementsActionContext*  context;
    OpenGLES2DrawElementsActionData*        data;
    GLenum                                  err;
    SCTOpenGLES2Array*                      array;
    int                                     elementSize;
    void*                                   ptr;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2DrawElementsActionContext* )( action->context );
    data    = &( context->data );

    array = sctiOpenGLES2ModuleGetArray( context->moduleContext, data->arrayIndex );
    if( array == NULL )
    {
        SCT_LOG_ERROR( "Invalid array index in DrawElements@OpenGLES2 action execute." );
        return SCT_FALSE;
    }
    if( array->type != GL_UNSIGNED_BYTE && array->type != GL_UNSIGNED_SHORT )
    {
        SCT_LOG_ERROR( "Invalid array type in DrawElements@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    elementSize = sctOpenGLES2GetGLTypeSize( array->type );

    if( ( array->length - data->offset ) < data->count )
    {
        SCT_LOG_ERROR( "Array shorter than count in DrawElements@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    ptr = ( ( unsigned char* )( array->data ) + ( data->offset * elementSize ) );
    glDrawElements( data->mode, data->count, array->type, ptr );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in DrawElements@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2DrawElementsActionDestroy( SCTAction* action )
{
    SCTOpenGLES2DrawElementsActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2DrawElementsActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2DrawElementsActionContext( context );
}
