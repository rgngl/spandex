/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2uniformffromregisteraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2UniformfFromRegisterActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2UniformfFromRegisterActionContext*  context;
    SCTOpenGLES2ModuleContext*                      mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenGLES2ModuleContext* )( moduleContext );
    context = ( SCTOpenGLES2UniformfFromRegisterActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2UniformfFromRegisterActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in UniformfFromRegister@OpenGLES2 context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2UniformfFromRegisterActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenGLES2UniformfFromRegisterActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2UniformfFromRegisterActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.registerIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid register index in UniformfFromRegister@OpenGLES2 context creation." );
        return NULL;      
    }

    if( sctiOpenGLES2ModuleIsValidRegisterIndex( context->moduleContext, context->data.sourceRegisterIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid source register index in UniformfFromRegister@OpenGLES2 context creation." );
        return NULL;      
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2UniformfFromRegisterActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2UniformfFromRegisterActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2UniformfFromRegisterActionContext*  context;
    OpenGLES2UniformfFromRegisterActionData*        data;
    GLenum                                          err;
    int                                             location;
    float                                           value;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2UniformfFromRegisterActionContext* )( action->context );
    data    = &( context->data );

    location = ( int )( sctiOpenGLES2ModuleGetRegisterValue( context->moduleContext, data->registerIndex ) );
    if( location < 0 )
    {
        SCT_LOG_ERROR( "Invalid value in register in UniformfFromRegister@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    value = ( float )( sctiOpenGLES2ModuleGetRegisterValue( context->moduleContext, data->sourceRegisterIndex ) );
    
    glUniform1fv( location, 1, &value );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in UniformfFromRegister@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2UniformfFromRegisterActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenGLES2UniformfFromRegisterActionContext( action->context );
    action->context = NULL;
}

