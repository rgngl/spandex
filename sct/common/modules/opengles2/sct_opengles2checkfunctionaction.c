/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2checkfunctionaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CheckFunctionActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CheckFunctionActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CheckFunctionActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CheckFunctionActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CheckFunction@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CheckFunctionActionContext ) );

    if( sctiParseOpenGLES2CheckFunctionActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CheckFunctionActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CheckFunctionActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CheckFunctionActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CheckFunctionActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CheckFunctionActionContext* )( action->context );

    if( siCommonGetProcAddress( NULL, context->data.function ) == NULL )
    {
        char buf[ 1024 ];
        sprintf( buf, "Function %s not found in CheckFunction@OpenGLES2 action execute.", context->data.function );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }  

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CheckFunctionActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CheckFunctionActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CheckFunctionActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CheckFunctionActionContext( context );
}

