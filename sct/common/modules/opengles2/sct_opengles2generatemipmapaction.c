/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2generatemipmapaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2GenerateMipmapActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2GenerateMipmapActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2GenerateMipmapActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2GenerateMipmapActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in GenerateMipmap@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2GenerateMipmapActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2GenerateMipmapActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2GenerateMipmapActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2GenerateMipmapActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2GenerateMipmapActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2GenerateMipmapActionContext*    context;
    GLenum                                      err;
    OpenGLES2GenerateMipmapActionData*          data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2GenerateMipmapActionContext* )( action->context );
    data    = &( context->data );

    glGenerateMipmap( data->target );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in GenerateMipmap@OpenGLES action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2GenerateMipmapActionDestroy( SCTAction* action )
{
    SCTOpenGLES2GenerateMipmapActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2GenerateMipmapActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2GenerateMipmapActionContext( context );
}
