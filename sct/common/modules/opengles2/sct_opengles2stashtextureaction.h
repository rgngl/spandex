/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_OPENGLES2STASHTEXTUREACTION_H__ )
#define __SCT_OPENGLES2STASHTEXTUREACTION_H__

#include "sct_types.h"
#include "sct_opengles2module.h"
#include "sct_opengles2module_parser.h"

/*!
 *
 */
typedef struct
{
    SCTOpenGLES2ModuleContext*          moduleContext;              
    OpenGLES2StashTextureActionData     data;
    GLuint*                             stash;
    int                                 stashIndex;
    unsigned long                       bytes;
    SCTBoolean                          reported;
    SCTBenchmark*                       currentBenchmark;
} SCTOpenGLES2StashTextureActionContext;
 
#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                               sctiCreateOpenGLES2StashTextureActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroyOpenGLES2StashTextureActionContext( void* context );

    SCTBoolean                          sctiOpenGLES2StashTextureActionInit( SCTAction* action, SCTBenchmark* benchmark );
    SCTBoolean                          sctiOpenGLES2StashTextureActionExecute( SCTAction* action, int frameNumber );
    void                                sctiOpenGLES2StashTextureActionTerminate( SCTAction* action );
    void                                sctiOpenGLES2StashTextureActionDestroy( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_OPENGLES2STASHTEXTUREACTION_H__ ) */
