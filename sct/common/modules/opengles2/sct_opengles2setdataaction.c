/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2setdataaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2SetDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2SetDataActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2SetDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2SetDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SetData@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2SetDataActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2SetDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2SetDataActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2SetDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in SetData@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2SetDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2SetDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2SetDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2SetDataActionContext*   context;
    OpenGLES2SetDataActionData*         data;
    SCTOpenGLES2Data*                   rawData;
    int                                 stringLength;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2SetDataActionContext* )( action->context );
    data    = &( context->data );

    rawData = sctiOpenGLES2ModuleGetData( context->moduleContext, data->dataIndex );

    stringLength = strlen( data->data );
    
    if( rawData != NULL )
    {
        if( rawData->length != stringLength )
        {
            SCT_LOG_ERROR( "Data index already used in SetData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }
        strcpy( ( char* )( rawData->data ), ( const char* )( data->data ) );
    }
    else
    {
        rawData = sctOpenGLES2CreateData( data->data, stringLength, SCT_TRUE );
        if( rawData == NULL )
        {
            SCT_LOG_ERROR( "Data creation failed in SetData@OpenGLES2 action execute." );
            return SCT_FALSE;
        }

        sctiOpenGLES2ModuleSetData( context->moduleContext, data->dataIndex, rawData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2SetDataActionTerminate( SCTAction* action )
{
    SCTOpenGLES2SetDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenGLES2SetDataActionContext* )( action->context );

    sctiOpenGLES2ModuleDeleteData( context->moduleContext, context->data.dataIndex );
}

/*!
 *
 *
 */
void sctiOpenGLES2SetDataActionDestroy( SCTAction* action )
{
    SCTOpenGLES2SetDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2SetDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2SetDataActionContext( context );
}
