/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2eglimagetargettexture2daction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_EGL_MODULE )
# include "sct_eglmodule.h"
#endif  /* defined( INCLUDE_EGL_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateOpenGLES2EglImageTargetTexture2DActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2EglImageTargetTexture2DActionContext*   context;
    SCTOpenGLES2ModuleContext*                          mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenGLES2ModuleContext* )( moduleContext );
    context = ( SCTOpenGLES2EglImageTargetTexture2DActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2EglImageTargetTexture2DActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in EglImageTargetTexture2D@OpenGLES2 context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2EglImageTargetTexture2DActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenGLES2EglImageTargetTexture2DActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2EglImageTargetTexture2DActionContext( context );
        return NULL;
    }

    context->glEGLImageTargetTexture2DOES = NULL;
    context->validated                    = SCT_FALSE;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2EglImageTargetTexture2DActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2EglImageTargetTexture2DActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenGLES2EglImageTargetTexture2DActionContext*   context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenGLES2EglImageTargetTexture2DActionContext* )( action->context );  

#if !defined( INCLUDE_EGL_MODULE )
    SCT_USE_VARIABLE( context );
    SCT_LOG_ERROR( "EglImageTargetTexture2D@OpenGLES2 action requires EGL module." );
    return SCT_FALSE;
    
#else   /* !defined( INCLUDE_EGL_MODULE ) */    
    context->glEGLImageTargetTexture2DOES = ( SCTEGLImageTargetTexture2DOES )( siCommonGetProcAddress( NULL, "glEGLImageTargetTexture2DOES" ) );

    if( context->glEGLImageTargetTexture2DOES == NULL )
    {
        SCT_LOG_ERROR( "glEGLImageTargetTexture2DOES function not found in EglImageTargetTexture2D@OpenGLES2 action init." );
        return SCT_FALSE;
    }
    
    context->validated = SCT_FALSE;   
    
    return SCT_TRUE;
#endif  /* !defined( INCLUDE_EGL_MODULE ) */    
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2EglImageTargetTexture2DActionExecute( SCTAction* action, int framenumber )
{
#if defined( INCLUDE_EGL_MODULE )    
    SCTOpenGLES2EglImageTargetTexture2DActionContext*   context;
    OpenGLES2EglImageTargetTexture2DActionData*         data;
    const char*                                         extString;
    EGLImageKHR                                         eglImage    = ( EGLImageKHR )( 0 );
    GLenum                                              err;    

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenGLES2EglImageTargetTexture2DActionContext* )( action->context );
    data    = &( context->data );

    if( context->validated == SCT_FALSE )
    {
        extString = ( const char* )( glGetString( GL_EXTENSIONS ) );

# if !defined( SCT_PRIVATE_GL_OES_EGL_IMAGE_EXTENSION )
        if( data->target == GL_TEXTURE_2D )
        {
            if( extString == NULL || strstr( extString, "GL_OES_EGL_image" ) == NULL )
            {
                SCT_LOG_ERROR( "GL_OES_EGL_image extension not supported in EglImageTargetTexture2D@OpenGLES2 action execute." );
                return SCT_FALSE;
            }
        }
        else if( data->target == GL_TEXTURE_EXTERNAL_OES )
        {
            if( extString == NULL || strstr( extString, "GL_OES_EGL_image_external" ) == NULL )
            {
                SCT_LOG_ERROR( "GL_OES_EGL_image_external extension not supported in EglImageTargetTexture2D@OpenGLES2 action execute." );
                return SCT_FALSE;
            }
        }
# endif /* !defined( SCT_PRIVATE_GL_OES_EGL_IMAGE_EXTENSION ) */

        context->validated = SCT_TRUE;
    }

    eglImage = sctEglModuleGetImageExt( data->eglImageIndex );
    if( eglImage == EGL_NO_IMAGE_KHR )
    {
        SCT_LOG_ERROR( "Invalid EGL image in EglImageTargetTexture2D@OpenGLES2 action execute." );        
        return SCT_FALSE;
    }

    SCT_ASSERT( context->glEGLImageTargetTexture2DOES != NULL );
    context->glEGLImageTargetTexture2DOES( data->target, ( SCTGLeglImageOES )( eglImage ) );
    
# if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in EglImageTargetTexture2D@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
# else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
# endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    
    return SCT_TRUE;
#else   /* defined( INCLUDE_EGL_MODULE ) */
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );    
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;    
#endif  /* defined( INCLUDE_EGL_MODULE ) */    
}

/*!
 *
 *
 */
void sctiOpenGLES2EglImageTargetTexture2DActionTerminate( SCTAction* action )
{
    SCTOpenGLES2EglImageTargetTexture2DActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2EglImageTargetTexture2DActionContext* )( action->context );

    context->glEGLImageTargetTexture2DOES = NULL;
    context->validated                    = SCT_FALSE;
}

/*!
 *
 *
 */
void sctiOpenGLES2EglImageTargetTexture2DActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenGLES2EglImageTargetTexture2DActionContext( action->context );
    action->context = NULL;
}
