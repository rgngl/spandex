/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2attachshaderaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2AttachShaderActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2AttachShaderActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2AttachShaderActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2AttachShaderActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in AttachShader@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2AttachShaderActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2AttachShaderActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2AttachShaderActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidProgramIndex( context->moduleContext, context->data.programIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2AttachShaderActionContext( context );
        SCT_LOG_ERROR( "Invalid program index in AttachShader@OpenGLES2 context creation." );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidShaderIndex( context->moduleContext, context->data.shaderIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2AttachShaderActionContext( context );
        SCT_LOG_ERROR( "Invalid shader index in AttachShader@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2AttachShaderActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2AttachShaderActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2AttachShaderActionContext*  context;
    GLenum                                  err;
    OpenGLES2AttachShaderActionData*        data;
    SCTOpenGLES2Shader*                     shader;
    SCTOpenGLES2Program*                    program;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2AttachShaderActionContext* )( action->context );
    data    = &( context->data );

    shader = sctiOpenGLES2ModuleGetShader( context->moduleContext, data->shaderIndex );
    if( shader == NULL )
    {
        SCT_LOG_ERROR( "Invalid shader index in AttachShader@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    program = sctiOpenGLES2ModuleGetProgram( context->moduleContext, data->programIndex );
    if( program == NULL )
    {
        SCT_LOG_ERROR( "Invalid program index in AttachShader@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    glAttachShader( program->program, shader->shader );

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */   
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in AttachShader@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2AttachShaderActionDestroy( SCTAction* action )
{
    SCTOpenGLES2AttachShaderActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2AttachShaderActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2AttachShaderActionContext( context );
}
