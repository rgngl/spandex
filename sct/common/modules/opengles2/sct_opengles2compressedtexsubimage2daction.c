/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2compressedtexsubimage2daction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenGLES2CompressedTexSubImage2DActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2CompressedTexSubImage2DActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2CompressedTexSubImage2DActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2CompressedTexSubImage2DActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CompressedTexSubImage2D@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2CompressedTexSubImage2DActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2CompressedTexSubImage2DActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CompressedTexSubImage2DActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidCompressedTextureDataIndex( context->moduleContext, context->data.compressedTextureDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2CompressedTexSubImage2DActionContext( context );
        SCT_LOG_ERROR( "Invalid compressed texture data index in CompressedTexSubImage2D@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2CompressedTexSubImage2DActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2CompressedTexSubImage2DActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenGLES2CompressedTexSubImage2DActionContext*   context;
    GLenum                                              err;
    int                                                 i;
    OpenGLES2CompressedTexSubImage2DActionData*         data;
    SCTOpenGLES2CompressedTextureData*                  compressedTextureData;
    int                                                 divider;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2CompressedTexSubImage2DActionContext* )( action->context );
    data    = &( context->data );

    compressedTextureData = sctiOpenGLES2ModuleGetCompressedTextureData( context->moduleContext, data->compressedTextureDataIndex );
    if( compressedTextureData == NULL )
    {
        SCT_LOG_ERROR( "Invalid texture data in CompressedTexSubImage2D@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    divider = 1;
    for( i = 0; i < compressedTextureData->mipmapCount; ++i )
    {
        glCompressedTexSubImage2D( data->target, 
                                   i, 
                                   data->xoffset / divider,
                                   data->yoffset / divider,
                                   compressedTextureData->mipmaps[ i ].width, 
                                   compressedTextureData->mipmaps[ i ].height, 
                                   compressedTextureData->internalFormat,  
                                   compressedTextureData->mipmaps[ i ].imageSize,
                                   compressedTextureData->mipmaps[ i ].data );
        divider *= 2;
    }

#if defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS )
    /* Check for errors. */  
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "GL error %s in CompressedTexSubImage2D@OpenGLES2 action execute.", sctiOpenGLES2GetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenGLES2ClearGLError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENGLES2_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenGLES2CompressedTexSubImage2DActionDestroy( SCTAction* action )
{
    SCTOpenGLES2CompressedTexSubImage2DActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2CompressedTexSubImage2DActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2CompressedTexSubImage2DActionContext( context );
}
