/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_opengles2flagaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_gl2.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_THREAD_MODULE )
# include "sct_threadmodule.h"
#endif  /* defined( INCLUDE_THREAD_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateOpenGLES2FlagActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenGLES2FlagActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenGLES2FlagActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenGLES2FlagActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Flag@OpenGLES2 action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenGLES2FlagActionContext ) );

    context->moduleContext = ( SCTOpenGLES2ModuleContext* )( moduleContext );

    if( sctiParseOpenGLES2FlagActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2FlagActionContext( context );
        return NULL;
    }

    if( sctiOpenGLES2ModuleIsValidFlagIndex( context->moduleContext, context->data.flagIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenGLES2FlagActionContext( context );
        SCT_LOG_ERROR( "Invalid flag index in Flag@OpenGLES2 context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenGLES2FlagActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2FlagActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( INCLUDE_THREAD_MODULE )
    SCT_LOG_ERROR( "Flag@OpenGLES2 action requires Thread module." );
    return SCT_FALSE;
#else    /* !defined( INCLUDE_THREAD_MODULE ) */
    return SCT_TRUE;    
#endif  /* !defined( INCLUDE_THREAD_MODULE ) */
}

/*!
 *
 *
 */
SCTBoolean sctiOpenGLES2FlagActionExecute( SCTAction* action, int frameNumber )
{
#if defined( INCLUDE_THREAD_MODULE )
    SCTOpenGLES2FlagActionContext*  context;
    OpenGLES2FlagActionData*        data;
    SCTOpenGLES2Flag*               flag;
    int                             s;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenGLES2FlagActionContext* )( action->context );
    data    = &( context->data );

    flag = sctiOpenGLES2ModuleGetFlag( context->moduleContext, data->flagIndex );
    if( flag == NULL )
    {
        SCT_LOG_ERROR( "Undefined flag in Flag@OpenGLES2 action execute." );
        return SCT_FALSE;
    }

    sctOpenGLES2SetFlag( flag, data->value );

    s = sctThreadModuleWaitExt( data->threadSignalIndex, 0 );    
        
    if( s == 0 )
    {
        SCT_LOG_ERROR( "Waiting timeout in Flag@Thread action execute." );        
        return SCT_FALSE;
    }
    else if( s < 0 )
    {
        /* Don't log cancelled waits as errors. However, the action must return
         * SCT_FALSE to abort the benchmark loop. */
        return SCT_FALSE;
    }
        
    return SCT_TRUE;
    
#else   /* defined( INCLUDE_THREAD_MODULE ) */
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;
#endif  /* defined( INCLUDE_THREAD_MODULE ) */      
}

/*!
 *
 *
 */
void sctiOpenGLES2FlagActionTerminate( SCTAction* action )
{
#if defined( INCLUDE_THREAD_MODULE )
    SCTOpenGLES2FlagActionContext*  context;
    OpenGLES2FlagActionData*        data;
    SCTOpenGLES2Flag*               flag;

    context = ( SCTOpenGLES2FlagActionContext* )( action->context );    
    data    = &( context->data );
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    flag = sctiOpenGLES2ModuleGetFlag( context->moduleContext, data->flagIndex );
    if( flag == NULL )
    {
        return;
    }

    sctOpenGLES2ClearFlag( flag );   

#else   /* defined( INCLUDE_THREAD_MODULE ) */
    SCT_USE_VARIABLE( action );
#endif  /* defined( INCLUDE_THREAD_MODULE ) */    
}

/*!
 *
 *
 */
void sctiOpenGLES2FlagActionDestroy( SCTAction* action )
{
    SCTOpenGLES2FlagActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenGLES2FlagActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenGLES2FlagActionContext( context );
}
