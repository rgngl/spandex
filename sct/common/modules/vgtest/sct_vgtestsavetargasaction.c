/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgtestsavetargasaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_sifile.h"

#include "../openvg/sct_vg.h"
#include "../openvg/sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateVgtestSaveTargasActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVgtestSaveTargasActionContext*   context;

    SCT_USE_VARIABLE( moduleContext );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTVgtestSaveTargasActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVgtestSaveTargasActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SaveTargas@Vgtest action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVgtestSaveTargasActionContext ) );

    if( sctiParseVgtestSaveTargasActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVgtestSaveTargasActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVgtestSaveTargasActionContext( void* context )
{
    SCTVgtestSaveTargasActionContext*   c;

    if( context == NULL )
    {
        return;
    }
    c = ( SCTVgtestSaveTargasActionContext* )( context );

    SCT_ASSERT_ALWAYS( c->framebuffer == NULL );
    SCT_ASSERT_ALWAYS( c->filename == NULL );
    SCT_ASSERT_ALWAYS( c->benchmarkName == NULL );

    if( c->data.frameNumbers != NULL )
    {
        sctDestroyIntVector( c->data.frameNumbers );
        c->data.frameNumbers = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiVgtestSaveTargasActionInit( SCTAction *action, SCTBenchmark *benchmark )
{
    SCTVgtestSaveTargasActionContext*   context;
    VgtestSaveTargasActionData*         data;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTVgtestSaveTargasActionContext* )( action->context );
    data    = &( context->data );

    if( context->framebuffer != NULL )
    {
        return SCT_TRUE;
    }

    context->framebuffer = sctOpenVGCreateImageData( VG_sRGBA_8888, 
                                                     data->width, 
                                                     data->height );

    if( context->framebuffer == NULL )
    {
        SCT_LOG_ERROR( "Framebuffer allocation failed in SaveTargas@Vgtest action init." );
        return SCT_FALSE;
    }

    SCT_ASSERT_ALWAYS( context->framebuffer->stride == data->width * 4 );

    context->benchmarkName = sctDuplicateString( benchmark->name );
    if( context->benchmarkName == NULL )
    {
        SCT_LOG_ERROR( "Out of memory" );
        sctiVgtestSaveTargasActionTerminate( action );
        return SCT_FALSE;
    }

    context->filenameLength = 512;
    context->filename       = ( char* )( siCommonMemoryAlloc( NULL, context->filenameLength ) );
    if( context->filename == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SaveTargas@Vgtest action init." );
        sctiVgtestSaveTargasActionTerminate( action );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiVgtestSaveTargasActionExecute( SCTAction* action, int frameNumber )
{
    SCTVgtestSaveTargasActionContext*   context;
    VgtestSaveTargasActionData*         data;
    SCTOpenVGImageData*                 framebuffer;
    VGErrorCode                         err;
    unsigned char                       header[ 18 ];
    unsigned char*                      datas[ 2 ];
    int                                 lengths[ 2 ];
    int                                 i;
    SCTBoolean                          found;    

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    if( frameNumber == -1 )
    {
        return SCT_TRUE;
    }
    
    context = ( SCTVgtestSaveTargasActionContext* )( action->context );
    data    = &( context->data );

    /* Check if the current framenumber is in the framenumber vector. Use brute
       force search with the assumption that the number of elements in
       framenumber vector is limited. TODO: more efficient search?  */
    found = SCT_FALSE;
    for( i = 0; i < data->frameNumbers->length; ++i )
    {
        if( data->frameNumbers->data[ i ] == frameNumber )
        {
            found = SCT_TRUE;
            break;
        }
    }
    
    if( found == SCT_FALSE )
    {
        /* The current framenumber was not in the framenumber vector. */
        return SCT_TRUE;
    }
    
    if( sctVgtestFormatTargaFilename( context->filename,
                                      context->filenameLength,
                                      data->path,
                                      context->benchmarkName,
                                      frameNumber ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Targa filename formatting failed in SaveTargas@Vgtest action execute." );
        return SCT_FALSE;
    }

#if defined( SCT_VGTEST_MODULE_CHECK_ERRORS )        
    /* Clear vg error. Beware infinite loops due to undefined context, etc. */
    for( i = 0; i < 16; ++i )
    {
        if( vgGetError() == VG_NO_ERROR )
        {
            break;
        }
    }
#endif  /* defined( SCT_VGTEST_MODULE_CHECK_ERRORS ) */
    
    framebuffer = context->framebuffer;
    vgReadPixels( framebuffer->data,
                  framebuffer->stride,
                  framebuffer->format,
                  data->x, data->y,
                  data->width, data->height );

#if defined( SCT_VGTEST_MODULE_CHECK_ERRORS )    
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 128 ];
        sprintf( buf, "VG error 0x%x in SaveTargas@Vgtest action execute.", err );
        SCT_LOG_ERROR( buf );
            
        return SCT_FALSE;
    }
#else   /* defined( SCT_VGTEST_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_VGTEST_MODULE_CHECK_ERRORS ) */
    
    sctVgtestSwapAbgrToBgra( ( unsigned char* )( framebuffer->data ), framebuffer->dataLength );
    sctVgtestCreateBgraTargaHeader( header, data->width, data->height );
    
    datas[ 0 ]      = header;
    lengths[ 0 ]    = 18;       
    datas[ 1 ]	    = ( unsigned char* )( framebuffer->data );
    lengths[ 1 ]    = framebuffer->dataLength;
    
    if( sctVgtestSaveData( context->filename,
                           ( const unsigned char** )( datas ),
                           ( const int* )( lengths ),
                           2 ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Save data failed in SaveTargas@Vgtest action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiVgtestSaveTargasActionTerminate( SCTAction* action )
{
    SCTVgtestSaveTargasActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTVgtestSaveTargasActionContext* )( action->context );

    if( context->benchmarkName != NULL )
    {
        siCommonMemoryFree( NULL, context->benchmarkName );
        context->benchmarkName = NULL;
    }
    
    if( context->framebuffer != NULL )
    {
        sctOpenVGDestroyImageData( context->framebuffer );
        context->framebuffer = NULL;
    }

    if( context->filename != NULL )
    {
        siCommonMemoryFree( NULL, context->filename );
        context->filename = NULL;
    }
}

/*!
 *
 *
 */
void sctiVgtestSaveTargasActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyVgtestSaveTargasActionContext( action->context );
    action->context = NULL;
}
