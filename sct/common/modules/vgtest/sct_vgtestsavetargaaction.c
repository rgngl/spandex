/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgtestsavetargaaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_sifile.h"

#include "../openvg/sct_vg.h"
#include "../openvg/sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateVgtestSaveTargaActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVgtestSaveTargaActionContext*    context;

    SCT_USE_VARIABLE( moduleContext );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTVgtestSaveTargaActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVgtestSaveTargaActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SaveTarga@Vgtest action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVgtestSaveTargaActionContext ) );

    if( sctiParseVgtestSaveTargaActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVgtestSaveTargaActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVgtestSaveTargaActionContext( void* context )
{
    SCTVgtestSaveTargaActionContext*    c;

    if( context == NULL )
    {
        return;
    }
    c = ( SCTVgtestSaveTargaActionContext* )( context );

    SCT_ASSERT_ALWAYS( c->framebuffer == NULL );
    SCT_ASSERT_ALWAYS( c->filename == NULL );
    SCT_ASSERT_ALWAYS( c->benchmarkName == NULL );

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiVgtestSaveTargaActionInit( SCTAction *action, SCTBenchmark *benchmark )
{
    SCTVgtestSaveTargaActionContext*    context;
    VgtestSaveTargaActionData*          data;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTVgtestSaveTargaActionContext* )( action->context );
    data    = &( context->data );

    if( context->framebuffer != NULL )
    {
        return SCT_TRUE;
    }

    context->framebuffer = sctOpenVGCreateImageData( VG_sRGBA_8888, 
                                                     data->width, 
                                                     data->height );

    if( context->framebuffer == NULL )
    {
        SCT_LOG_ERROR( "Framebuffer allocation failed in SaveTarga@Vgtest action init." );
        return SCT_FALSE;
    }

    SCT_ASSERT_ALWAYS( context->framebuffer->stride == data->width * 4 );

    context->benchmarkName = sctDuplicateString( benchmark->name );
    if( context->benchmarkName == NULL )
    {
        SCT_LOG_ERROR( "Out of memory in SaveTarga@Vgtest action init." );
        sctiVgtestSaveTargaActionTerminate( action );
        return SCT_FALSE;
    }

    context->filenameLength = 512;
    context->filename       = ( char* )( siCommonMemoryAlloc( NULL, context->filenameLength ) );
    if( context->filename == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SaveTarga@Vgtest action init." );
        sctiVgtestSaveTargaActionTerminate( action );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiVgtestSaveTargaActionExecute( SCTAction* action, int frameNumber )
{
    SCTVgtestSaveTargaActionContext*    context;
    VgtestSaveTargaActionData*          data;
    SCTOpenVGImageData*                 framebuffer;
    VGErrorCode                         err;
    unsigned char                       header[ 18 ];
    unsigned char*                      datas[ 2 ];
    int                                 lengths[ 2 ];
    int                                 i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    SCT_ASSERT( SCT_ARRAY_LENGTH( datas ) == SCT_ARRAY_LENGTH( lengths ) );

    if( frameNumber == -1 )
    {
        return SCT_TRUE;
    }
    
    context = ( SCTVgtestSaveTargaActionContext* )( action->context );
    data    = &( context->data );

    if( data->frameNumber < 0 || data->frameNumber == frameNumber )
    {
        if( sctVgtestFormatTargaFilename( context->filename,
                                          context->filenameLength,
                                          data->path,
                                          context->benchmarkName,
                                          frameNumber ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Targa filename formatting failed in SaveTarga@Vgtest action execute." );
            return SCT_FALSE;
        }

#if defined( SCT_VGTEST_MODULE_CHECK_ERRORS )        
        /* Clear vg error. Beware infinite loops due to undefined context, etc. */
        for( i = 0; i < 16; ++i )
        {
            if( vgGetError() == VG_NO_ERROR )
            {
                break;
            }
        }
#else   /* defined( SCT_VGTEST_MODULE_CHECK_ERRORS ) */
        SCT_USE_VARIABLE( i );
#endif  /* defined( SCT_VGTEST_MODULE_CHECK_ERRORS ) */

        framebuffer = context->framebuffer;
        vgReadPixels( framebuffer->data,
                      framebuffer->stride,
                      framebuffer->format,
                      data->x, data->y,
                      data->width, data->height );
                  
#if defined( SCT_VGTEST_MODULE_CHECK_ERRORS )    
        err = vgGetError();
        if( err != VG_NO_ERROR )
        {
            char buf[ 128 ];
            sprintf( buf, "VG error 0x%x in SaveTarga@Vgtest action execute.", err );
            SCT_LOG_ERROR( buf );
            
            return SCT_FALSE;
        }
#else   /* defined( SCT_VGTEST_MODULE_CHECK_ERRORS ) */
        SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_VGTEST_MODULE_CHECK_ERRORS ) */

        sctVgtestSwapAbgrToBgra( ( unsigned char* )( framebuffer->data ), framebuffer->dataLength );
        sctVgtestCreateBgraTargaHeader( header, data->width, data->height );
    
        datas[ 0 ]      = header;
        lengths[ 0 ]    = 18;       
        datas[ 1 ]	    = ( unsigned char* )( framebuffer->data );
        lengths[ 1 ]    = framebuffer->dataLength;
    
        if( sctVgtestSaveData( context->filename,
                               ( const unsigned char** )( datas ),
                               ( const int* )( lengths ),
                               2 ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Save data failed in SaveTarga@Vgtest action execute." );
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiVgtestSaveTargaActionTerminate( SCTAction* action )
{
    SCTVgtestSaveTargaActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTVgtestSaveTargaActionContext* )( action->context );

    if( context->benchmarkName != NULL )
    {
        siCommonMemoryFree( NULL, context->benchmarkName );
        context->benchmarkName = NULL;
    }
    
    if( context->framebuffer != NULL )
    {
        sctOpenVGDestroyImageData( context->framebuffer );
        context->framebuffer = NULL;
    }

    if( context->filename != NULL )
    {
        siCommonMemoryFree( NULL, context->filename );
        context->filename = NULL;
    }
}

/*!
 *
 *
 */
void sctiVgtestSaveTargaActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyVgtestSaveTargaActionContext( action->context );
    action->context = NULL;
}
