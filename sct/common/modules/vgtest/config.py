#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
SetModuleName( 'Vgtest' )

######################################################################
AddInclude( 'sct_vgtestmodule.h' )

######################################################################
MAX_FILENAME_LENGTH = 256

######################################################################
AddActionConfig( 'SaveTarga',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FrameNumber',                          -1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   StringAttribute(             'Path',                                 MAX_FILENAME_LENGTH )
                   ]
                 )

######################################################################
AddActionConfig( 'SaveTargas',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntVectorAttribute(          'FrameNumbers' ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   StringAttribute(             'Path',                                 MAX_FILENAME_LENGTH )
                   ]
                 )

######################################################################
# END OF FILE
######################################################################
