/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vguloadmatrixaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vgu.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateVguLoadMatrixActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVguLoadMatrixActionContext*      context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTVguLoadMatrixActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVguLoadMatrixActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LoadMatrix@Vgu action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVguLoadMatrixActionContext ) );

    context->moduleContext = ( SCTVguModuleContext* )( moduleContext );

    if( sctiParseVguLoadMatrixActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVguLoadMatrixActionContext( context );
        return NULL;
    }

    if( sctiVguModuleIsValidMatrixIndex( context->moduleContext, context->data.matrixIndex ) == SCT_FALSE )
    {
        sctiDestroyVguLoadMatrixActionContext( context );
        SCT_LOG_ERROR( "Invalid matrix index in LoadMatrix@Vgu context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVguLoadMatrixActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiVguLoadMatrixActionExecute( SCTAction* action, int frameNumber )
{
    SCTVguLoadMatrixActionContext*      context;
    VguLoadMatrixActionData*            data;
    VGfloat*                            matrix;
    VGErrorCode                         err;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTVguLoadMatrixActionContext* )( action->context );
    data    = &( context->data );

    matrix = sctiVguModuleGetMatrix( context->moduleContext, data->matrixIndex );
    SCT_ASSERT( matrix != NULL );

    vgLoadMatrix( matrix );

#if defined( SCT_VGU_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 128 ];
        sprintf( buf, "VG error 0x%x in LoadMatrix@Vgu action execute.", err );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
#else   /* defined( SCT_VGU_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_VGU_MODULE_CHECK_ERRORS ) */  
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiVguLoadMatrixActionDestroy( SCTAction* action )
{
    SCTVguLoadMatrixActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTVguLoadMatrixActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyVguLoadMatrixActionContext( context );
}
