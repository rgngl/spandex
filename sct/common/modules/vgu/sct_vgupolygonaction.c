/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgupolygonaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vgu.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateVguPolygonActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVguPolygonActionContext* context;
    SCTVguModuleContext*        mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTVguModuleContext* )( moduleContext );
    context = ( SCTVguPolygonActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVguPolygonActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Polygon@Vgu context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVguPolygonActionContext ) );

    context->moduleContext = mc;

    if( sctiParseVguPolygonActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVguPolygonActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVguPolygonActionContext( void* context )
{
    SCTVguPolygonActionContext* c;

    if( context == NULL )
    {
        return;
    }
    
    c = ( SCTVguPolygonActionContext* )( context );
    if( c->data.points != NULL )
    {
        sctDestroyFloatVector( c->data.points );
        c->data.points = NULL;
    }

    siCommonMemoryFree( NULL, context );  
}

/*!
 *
 *
 */
SCTBoolean sctiVguPolygonActionExecute( SCTAction* action, int framenumber )
{
    SCTVguPolygonActionContext* context;
    VguPolygonActionData*       data;
    VGUErrorCode                err;
    VGPath                      path;
    VGboolean                   closed  = VG_FALSE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTVguPolygonActionContext* )( action->context );
    data    = &( context->data );

    path = sctOpenVGModuleGetPathExt( data->pathIndex );
    if( path == VG_INVALID_HANDLE )
    {
        SCT_LOG_ERROR( "Invalid path in Polygon@Vgu action execute." );
        return SCT_FALSE;
    }

    if( data->closed == SCT_TRUE )
    {
        closed = VG_TRUE;
    }
    
    err = vguPolygon( path, data->points->data, data->points->length / 2, closed );

    if( err != VGU_NO_ERROR )
    {
        char buf[ 128 ];
        sprintf( buf, "Polygon@Vgu action execute failed, error %s", sctiVguGetErrorString( err ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiVguPolygonActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyVguPolygonActionContext( action->context );
    action->context = NULL;
}
