#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
SetModuleName( 'Vgu' )

######################################################################
AddInclude( 'sct_vgumodule.h' )
AddInclude( 'sct_vgu.h' )

######################################################################
MAX_PATH_COUNT      = 1024
MAX_MATRIX_COUNT    = 8

################################################################################
def MakeCapabilityValidationExpr( attributeName, capability ):
    return '( %s == "OFF" and vgu.contextParameters[VGU.%s] == False ) or ( %s == "ON" and vgu.contextParameters[VGU.%s] == True )' % \
           (attributeName, capability, attributeName, capability)

################################################################################
#
AddActionConfig( 'Line',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_COUNT - 1 ),
                   FloatAttribute(              'X0' ),
                   FloatAttribute(              'Y0' ),
                   FloatAttribute(              'X1' ),
                   FloatAttribute(              'Y1' )
                   ]
                 )

################################################################################
#
AddActionConfig( 'Polygon',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_COUNT - 1 ),
                   FloatVectorAttribute(        'Points' ),
                   OnOffAttribute(              'Closed' )
                   ],
                 [ ValidatorConfig( 'Points != NULL && Points->length % 2 != 0',
                                    'Invalid point data' ) ]
                 )

################################################################################
#
AddActionConfig( 'Rect',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_COUNT - 1 ),
                   FloatAttribute(              'X' ),
                   FloatAttribute(              'Y' ),
                   FloatAttribute(              'Width',                                0 ),
                   FloatAttribute(              'Height',                               0 )
                   ]
                 )

################################################################################
#
AddActionConfig( 'RoundRect',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_COUNT - 1 ),
                   FloatAttribute(              'X' ),
                   FloatAttribute(              'Y' ),
                   FloatAttribute(              'Width',                                0 ),
                   FloatAttribute(              'Height',                               0 ),
                   FloatAttribute(              'ArcWidth' ),
                   FloatAttribute(              'ArcHeight' )
                   ]
                 )

################################################################################
#
AddActionConfig( 'Ellipse',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_COUNT - 1 ),
                   FloatAttribute(              'Cx' ),
                   FloatAttribute(              'Cy' ),
                   FloatAttribute(              'Width',                                0 ),
                   FloatAttribute(              'Height',                               0 )
                   ]
                 )

################################################################################
#
AddActionConfig( 'Arc',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_COUNT - 1 ),
                   FloatAttribute(              'X' ),
                   FloatAttribute(              'Y' ),
                   FloatAttribute(              'Width',                                0 ),
                   FloatAttribute(              'Height',                               0 ),
                   FloatAttribute(              'StartAngle' ),
                   FloatAttribute(              'AngleExtent' ),
                   EnumAttribute(               'ArcType', 'VGUArcType',                [ 'VGU_ARC_OPEN',
                                                                                          'VGU_ARC_CHORD',
                                                                                          'VGU_ARC_PIE' ] )
                   ]
                 )

################################################################################
#
AddActionConfig( 'LoadMatrix',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'MatrixIndex',                          0, MAX_MATRIX_COUNT - 1 )
                   ]
                 )

################################################################################
#
AddActionConfig( 'ComputeWarpQuadToSquare',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'MatrixIndex',                          0, MAX_MATRIX_COUNT - 1 ),
                   FloatAttribute(              'Sx0' ),
                   FloatAttribute(              'Sy0' ),
                   FloatAttribute(              'Sx1' ),
                   FloatAttribute(              'Sy1' ),
                   FloatAttribute(              'Sx2' ),
                   FloatAttribute(              'Sy2' ),
                   FloatAttribute(              'Sx3' ),
                   FloatAttribute(              'Sy3' )
                   ]
                 )

################################################################################
#
AddActionConfig( 'ComputeWarpSquareToQuad',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'MatrixIndex',                          0, MAX_MATRIX_COUNT - 1 ),
                   FloatAttribute(              'Dx0' ),
                   FloatAttribute(              'Dy0' ),
                   FloatAttribute(              'Dx1' ),
                   FloatAttribute(              'Dy1' ),
                   FloatAttribute(              'Dx2' ),
                   FloatAttribute(              'Dy2' ),
                   FloatAttribute(              'Dx3' ),
                   FloatAttribute(              'Dy3' )
                   ]
                 )

################################################################################
#
AddActionConfig( 'ComputeWarpQuadToQuad',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'MatrixIndex',                          0, MAX_MATRIX_COUNT - 1 ),
                   FloatAttribute(              'Dx0' ),
                   FloatAttribute(              'Dy0' ),
                   FloatAttribute(              'Dx1' ),
                   FloatAttribute(              'Dy1' ),
                   FloatAttribute(              'Dx2' ),
                   FloatAttribute(              'Dy2' ),
                   FloatAttribute(              'Dx3' ),
                   FloatAttribute(              'Dy3' ),
                   FloatAttribute(              'Sx0' ),
                   FloatAttribute(              'Sy0' ),
                   FloatAttribute(              'Sx1' ),
                   FloatAttribute(              'Sy1' ),
                   FloatAttribute(              'Sx2' ),
                   FloatAttribute(              'Sy2' ),
                   FloatAttribute(              'Sx3' ),
                   FloatAttribute(              'Sy3' )
                   ]
                 )

######################################################################
# END OF FILE
######################################################################
