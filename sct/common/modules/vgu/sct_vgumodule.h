/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_VGUMODULE_H__ )
#define __SCT_VGUMODULE_H__

#include "sct_types.h"
#include "sct_vgu.h"

#if !defined( INCLUDE_OPENVG_MODULE )
# error "Vgu module requires also OpenVG module"
#endif  /* !defined( INCLUDE_OPENVG_MODULE ) */

#include "sct_openvgmodule.h"

#define SCT_MAX_VGU_MATRICES                    8

/*!
 * Vgu module context.
 *
 */
typedef struct
{
    VGfloat                     matrices[ SCT_MAX_VGU_MATRICES ][ 9 ];
} SCTVguModuleContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    SCTModule*                  sctCreateVguModule( void );

    /* Matrices */
    SCTBoolean                  sctiVguModuleIsValidMatrixIndex( SCTVguModuleContext* moduleContext, int index );
    VGfloat*                    sctiVguModuleGetMatrix( SCTVguModuleContext* moduleContext, int index );

    /* Vgu error string */
    const char*                 sctiVguGetErrorString( VGUErrorCode err );
    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_VGUMODULE_H__ ) */
