/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgucomputewarpsquaretoquadaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vgu.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateVguComputeWarpSquareToQuadActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVguComputeWarpSquareToQuadActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTVguComputeWarpSquareToQuadActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVguComputeWarpSquareToQuadActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ComputeWarpSquareToQuad@Vgu action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVguComputeWarpSquareToQuadActionContext ) );

    context->moduleContext = ( SCTVguModuleContext* )( moduleContext );

    if( sctiParseVguComputeWarpSquareToQuadActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVguComputeWarpSquareToQuadActionContext( context );
        return NULL;
    }

    if( sctiVguModuleIsValidMatrixIndex( context->moduleContext, context->data.matrixIndex ) == SCT_FALSE )
    {
        sctiDestroyVguComputeWarpSquareToQuadActionContext( context );
        SCT_LOG_ERROR( "Invalid matrix index in ComputeWarpSquareToQuad@Vgu context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVguComputeWarpSquareToQuadActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiVguComputeWarpSquareToQuadActionExecute( SCTAction* action, int frameNumber )
{
    SCTVguComputeWarpSquareToQuadActionContext* context;
    VguComputeWarpSquareToQuadActionData*       data;
    VGfloat*                                    matrix;
    VGUErrorCode                                err;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTVguComputeWarpSquareToQuadActionContext* )( action->context );
    data    = &( context->data );

    matrix = sctiVguModuleGetMatrix( context->moduleContext, data->matrixIndex );
    SCT_ASSERT( matrix != NULL );

    err = vguComputeWarpSquareToQuad( data->dx0, data->dy0,
                                      data->dx1, data->dy1,
                                      data->dx2, data->dy2,
                                      data->dx3, data->dy3,
                                      matrix );

    if( err != VGU_NO_ERROR )
    {
        char buf[ 128 ];
        sprintf( buf, "ComputeWarpSquareToQuad@Vgu action execute failed, error %s", sctiVguGetErrorString( err ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
     
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiVguComputeWarpSquareToQuadActionDestroy( SCTAction* action )
{
    SCTVguComputeWarpSquareToQuadActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTVguComputeWarpSquareToQuadActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyVguComputeWarpSquareToQuadActionContext( context );
}
