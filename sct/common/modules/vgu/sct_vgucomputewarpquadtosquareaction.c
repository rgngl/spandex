/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgucomputewarpquadtosquareaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vgu.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateVguComputeWarpQuadToSquareActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVguComputeWarpQuadToSquareActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTVguComputeWarpQuadToSquareActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVguComputeWarpQuadToSquareActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ComputeWarpQuadToSquare@Vgu action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVguComputeWarpQuadToSquareActionContext ) );

    context->moduleContext = ( SCTVguModuleContext* )( moduleContext );

    if( sctiParseVguComputeWarpQuadToSquareActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVguComputeWarpQuadToSquareActionContext( context );
        return NULL;
    }

    if( sctiVguModuleIsValidMatrixIndex( context->moduleContext, context->data.matrixIndex ) == SCT_FALSE )
    {
        sctiDestroyVguComputeWarpQuadToSquareActionContext( context );
        SCT_LOG_ERROR( "Invalid matrix index in ComputeWarpQuadToSquare@Vgu context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVguComputeWarpQuadToSquareActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiVguComputeWarpQuadToSquareActionExecute( SCTAction* action, int frameNumber )
{
    SCTVguComputeWarpQuadToSquareActionContext* context;
    VguComputeWarpQuadToSquareActionData*       data;
    VGfloat*                                    matrix;
    VGUErrorCode                                err;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTVguComputeWarpQuadToSquareActionContext* )( action->context );
    data    = &( context->data );

    matrix = sctiVguModuleGetMatrix( context->moduleContext, data->matrixIndex );
    SCT_ASSERT( matrix != NULL );

    err = vguComputeWarpQuadToSquare( data->sx0, data->sy0,
                                      data->sx1, data->sy1,
                                      data->sx2, data->sy2,
                                      data->sx3, data->sy3,
                                      matrix );

    if( err != VGU_NO_ERROR )
    {
        char buf[ 128 ];
        sprintf( buf, "ComputeWarpQuadToSquare@Vgu action execute failed, error %s", sctiVguGetErrorString( err ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
     
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiVguComputeWarpQuadToSquareActionDestroy( SCTAction* action )
{
    SCTVguComputeWarpQuadToSquareActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTVguComputeWarpQuadToSquareActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyVguComputeWarpQuadToSquareActionContext( context );
}
