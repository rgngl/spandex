/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgurectaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vgu.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateVguRectActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTVguRectActionContext*    context;
    SCTVguModuleContext*        mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTVguModuleContext* )( moduleContext );
    context = ( SCTVguRectActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVguRectActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Rect@Vgu context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTVguRectActionContext ) );

    context->moduleContext = mc;

    if( sctiParseVguRectActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyVguRectActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyVguRectActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiVguRectActionExecute( SCTAction* action, int framenumber )
{
    SCTVguRectActionContext*    context;
    VguRectActionData*          data;
    VGUErrorCode                err;
    VGPath                      path;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTVguRectActionContext* )( action->context );
    data    = &( context->data );

    path = sctOpenVGModuleGetPathExt( data->pathIndex );
    if( path == VG_INVALID_HANDLE )
    {
        SCT_LOG_ERROR( "Invalid path in Rect@Vgu action execute." );
        return SCT_FALSE;
    }

    err = vguRect( path, data->x, data->y, data->width, data->height );

    if( err != VGU_NO_ERROR )
    {
        char buf[ 128 ];
        sprintf( buf, "Rect@Vgu action execute failed, error %s", sctiVguGetErrorString( err ) );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiVguRectActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyVguRectActionContext( action->context );
    action->context = NULL;
}
