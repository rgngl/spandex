/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_vgumodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_vgumodule_parser.h"
#include "sct_vgumodule_actions.h"
#include "sct_vgu.h"

#include <stdio.h>
#include <string.h>

/* Local function declarations. */
SCTAttributeList*       sctiVguModuleInfo( SCTModule* module );
SCTAction*              sctiVguModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    sctiVguModuleDestroy( SCTModule* module );

/*!
 * Create vgu module data structure which defines the name of the module and
 * sets up the module interface functions.
 *
 * \return Module structure for Vgu module, or NULL if failure.
 */
SCTModule* sctCreateVguModule( void )
{
    SCTModule*                  module;
    SCTVguModuleContext*        context;

    /* Create Vgu module context and zero memory. */
    context = ( SCTVguModuleContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTVguModuleContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTVguModuleContext ) );

    /* Create module data structure. */
    module = sctCreateModule( "Vgu",
                              context,
#if defined( _WIN32 )
                              _vgu_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiVguModuleInfo,
                              sctiVguModuleCreateAction,
                              sctiVguModuleDestroy );

    if( module == NULL )
    {
        siCommonMemoryFree( NULL, context );
        return NULL;
    }

    return module;
}

/*!
 * Create attribute list containing Vgu module information. 
 *
 * \param module Vgu module. Must be defined.
 *
 * \return Vgu system information as attribute list, or NULL if
 * failure.
 */
SCTAttributeList* sctiVguModuleInfo( SCTModule* module )
{
    SCTAttributeList*           attributes;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( module->context != NULL );

    /* Create attribute list for storing Vgu module information. */
    attributes = sctCreateAttributeList();

    return attributes;
}

/*!
 * Create Vgu module action. 
 *
 * \param module Vgu module. Must be defined.
 * \param name Action name. Must be defined.
 * \param type Action type. Must be defined.
 * \param attributes List of action attributes. Must be defined.
 * \param log Log to use for warning and error reporting.
 *
 * \return Action data structure, or NULL if failure.
 *
 */
SCTAction* sctiVguModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCTVguModuleContext* moduleContext;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    moduleContext = ( SCTVguModuleContext* )( module->context );

    SCT_ASSERT_ALWAYS( moduleContext != NULL );

    return sctCreateActionFromTemplate( name,
                                        type,
                                        "Vgu",
                                        moduleContext,
                                        attributes,
                                        VguActionTemplates,
                                        SCT_ARRAY_LENGTH( VguActionTemplates ) );
}

/*!
 * Destroy Vgu module context. This function does not deallocate module data
 * structure but instead simply does the needed module cleanup, if any.
 *
 * \param module Module to destroy. Must be defined.
 */
void sctiVguModuleDestroy( SCTModule* module )
{
    SCTVguModuleContext*    context;

    SCT_ASSERT_ALWAYS( module != NULL );

    context = ( SCTVguModuleContext* )( module->context );
    if( context != NULL )
    {
        siCommonMemoryFree( NULL, context );
        module->context = NULL;
    }
}

/*!
 *
 *
 */
SCTBoolean sctiVguModuleIsValidMatrixIndex( SCTVguModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index >= 0 && index < SCT_MAX_VGU_MATRICES )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
VGfloat* sctiVguModuleGetMatrix( SCTVguModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_VGU_MATRICES );

    if( index >= 0 && index < SCT_MAX_VGU_MATRICES )
    {
        return moduleContext->matrices[ index ];
    }
    
    return NULL;
}

/*!
 *
 *
 */
const char* sctiVguGetErrorString( VGUErrorCode err )
{
    switch( err )
    {
    case VGU_NO_ERROR:
        return "VGU_NO_ERROR";
    case VGU_BAD_HANDLE_ERROR:
        return "VGU_BAD_HANDLE_ERROR";
    case VGU_ILLEGAL_ARGUMENT_ERROR:
        return "VGU_ILLEGAL_ARGUMENT_ERROR";
    case VGU_OUT_OF_MEMORY_ERROR:
        return "VGU_OUT_OF_MEMORY_ERROR";
    case VGU_PATH_CAPABILITY_ERROR:
        return "VGU_PATH_CAPABILITY_ERROR";
    case VGU_BAD_WARP_ERROR:
        return "VGU_BAD_WARP_ERROR";      
    default:
        return "UNEXPECTED";
    }
}

