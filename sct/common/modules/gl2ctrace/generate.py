#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys
import os
import optparse
import re

def getFrames( str ):
    r = re.compile( '\(([0-9]*) frames\)', re.DOTALL | re.DOTALL )
    return int( re.search( r, str ).group( 1 ) )

def getFrameRange( str ):
    r = re.compile( 'Use frames ([0-9]*) - ([0-9]*)', re.DOTALL | re.DOTALL )
    s = re.search( r, str )
    if not s:
        return [ 0, 0x7FFFFFFF ]
    return [ int( s.group( 1 ) ), int( s.group( 2 ) ) ]

class Action:
    def __init__( self, name, type, module, attributes ):
        self.name       = name
        self.type       = type
        self.module     = module
        self.attributes = attributes
        self.created    = False

class Benchmark:
    def __init__( self, name, actions, repeats = -1 ):
        self.name       = name
        self.actions    = actions
        self.repeats    = repeats


def prepare( templateFolder, inputFolder, outputFolder, benchmarkFile = None, targaFolder = None ):
    templateFolder = templateFolder.replace( '\\', '/' )
    inputFolder    = inputFolder.replace( '\\', '/' )
    outputFolder   = outputFolder.replace( '\\', '/' )

    if templateFolder[ -1 :  ] != '/':
        templateFolder += '/'

    if inputFolder[ -1 : 0 ] != '/':
        inputFolder += '/'

    if outputFolder[ -1 : 0 ] != '/':
        outputFolder += '/'
    
    animatedFiles = filter( lambda x: x[ 0 : len( 'anim' ) ] == 'anim', os.listdir( inputFolder ) )
    animatedFiles.sort()

    template_animated_h_filename = templateFolder + 'template_animated.h'
    template_animated_c_filename = templateFolder + 'template_animated.c'
    
    config_filename              = outputFolder + 'config.py'

    action_h_filename_template   = outputFolder + 'sct_gl2ctrace%saction.h'
    action_c_filename_template   = outputFolder + 'sct_gl2ctrace%saction.c'

    ctrace_filename_template     = outputFolder + 'sct_gl2ctrace_%s.inl'

    configFile = open( config_filename, 'w' )
    configFile.write( "# GENERATED FILE\n" )
    configFile.write( "SetModuleName( 'Gl2Ctrace' )\n" )
    configFile.write( "AddInclude( 'sct_gl2ctracemodule.h' )\n\n" )

    actionSourceFiles = []
    benchmarks = []

    index = 1
    for f in animatedFiles:
        name = 'Animated%03d' % index

        hfile = open( template_animated_h_filename ).read()
        cfile = open( template_animated_c_filename ).read()

        hfile = hfile.replace( '$NAME', name )
        cfile = cfile.replace( '$NAME', name )

        hfileName = action_h_filename_template % name
        cfileName = action_c_filename_template % name

        open( hfileName, 'w' ).write( hfile )
        open( cfileName, 'w' ).write( cfile )

        ctracefileName = ctrace_filename_template % name

        ctracefile = open( inputFolder + '/' + f ).read()

        totalFrames = getFrames( ctracefile )
        frameRange = getFrameRange( ctracefile )

        if frameRange[ 1 ] >= totalFrames:
            frameRange[ 1 ] = totalFrames - 1
        
        if frameRange[ 0 ] < 0 or frameRange[ 0 ] > frameRange[ 1 ] or frameRange[ 1 ] >= totalFrames:
            raise 'Trace file contains invalid benchmark frame range'
       
        header = frameRange[ 0 ]
        trailer = totalFrames - frameRange[ 1 ] - 1
        frames  = frameRange[ 1 ] - frameRange[ 0 ] + 1
                       
        open( ctracefileName, 'w' ).write( ctracefile )

        configFile.write( "AddActionConfig( '%s',\n" % name )
        configFile.write( "                 'Init'+'Execute'+'Terminate'+'Destroy',\n" )
        configFile.write( "                 [ IntAttribute( 'Header',   0  ),\n" )
        configFile.write( "                   IntAttribute( 'Trailer',  0  ) ]\n" )
        configFile.write( "                 )\n\n" )

        actions = []
        actions += [ Action( name, name, 'Gl2Ctrace', { 'Header'  : str( header ),
                                                        'Trailer' : str( trailer ) } ) ]

        actionSourceFiles += [ cfileName ]
        benchmarks += [ Benchmark( name, actions, frames ) ]
        index += 1

    configFile.close()

    if benchmarkFile:
        bf = open( benchmarkFile, 'w' )
    else:
        bf = sys.stdout
        
    for b in benchmarks:
        actions = []
        # Write needed actions        
        for a in b.actions:
            if a.created == False:
                bf.write( '[%s:%s@%s]\n' % ( a.name, a.type, a.module, ) )
                for att in a.attributes.keys():
                    bf.write( '    %s : %s\n' % ( att, a.attributes[ att ], ) )
                bf.write( '\n' )
                a.created = True
            actions += [ a.name ]
            
        # Write benchmark
        bf.write( '[%s:BENCHMARK]\n' % b.name )
        bf.write( '    Repeats          : %d\n' % b.repeats )
        bf.write( '    InitActions      : None\n' )
        bf.write( '    BenchmarkActions : %s\n\n' % '+'.join( actions ) )

    return actionSourceFiles


if __name__ == '__main__':

    usage = "usage: %prog [options] <template folder> <input folder> <output folder>"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--targaFolder',
                       type    = 'string',
                       dest    = 'targaFolder',
                       default = '',
                       help    = 'Folder to save screenshots' )

    ( options, args ) = parser.parse_args()

    if len( args ) != 3:
        parser.error( 'incorrect number of arguments' )
        sys.exit( -1 )

    templateFolder      = args[ 0 ]
    srcFolder           = args[ 1 ]
    dstFolder           = args[ 2 ]

    prepare( templateFolder, srcFolder, dstFolder, None, options.targaFolder )

