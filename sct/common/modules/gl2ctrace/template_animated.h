/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_GL2CTRACE$NAMEACTION_H__ )
#define __SCT_GL2CTRACE$NAMEACTION_H__

#include "sct_types.h"
#include "sct_gl2ctracemodule.h"
#include "sct_gl2ctracemodule_parser.h"

/*!
 *
 */
typedef struct
{
    void*                               wrapperContext;
    Gl2Ctrace$NAMEActionData            data;
    int                                 lastFrame;
} SCTGl2Ctrace$NAMEActionContext;
 
#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                               sctiCreateGl2Ctrace$NAMEActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroyGl2Ctrace$NAMEActionContext( void* context );

    SCTBoolean                          sctiGl2Ctrace$NAMEActionInit( SCTAction* action, SCTBenchmark* benchmark );
    SCTBoolean                          sctiGl2Ctrace$NAMEActionExecute( SCTAction* action, int frameNumber );
    void                                sctiGl2Ctrace$NAMEActionTerminate( SCTAction* action );
    void                                sctiGl2Ctrace$NAMEActionDestroy( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_GL2CTRACE$NAMEACTION_H__ ) */
