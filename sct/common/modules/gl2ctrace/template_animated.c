/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma warning (disable: 4305)
#pragma warning (disable: 4100)
#pragma warning (disable: 4505)
#pragma warning (disable: 4245)
#pragma warning (disable: 4090)
#pragma warning (disable: 4132)

#include "sct_gl2ctrace$NAMEaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#include <gles2/gl2.h>
#include <egl/egl.h>

/* TODO FIXME: temporary typedefs to fix the problem in OpenGLES2 traces */
typedef GLuint GLshader;
typedef GLuint GLprogram;
typedef GLuint GLattribute;
typedef GLuint GLuniform;
typedef GLuint GLframebuffer;
typedef GLuint GLrenderbuffer;
typedef void* GLshadertype;
typedef void* GLprecisiontype;

#include "sct_eglwrapper.h"
#include "sct_gl2ctrace_$NAME.inl"

/*!
 *
 */
void* sctiCreateGl2Ctrace$NAMEActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTGl2Ctrace$NAMEActionContext* context;

    SCT_USE_VARIABLE( moduleContext );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTGl2Ctrace$NAMEActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTGl2Ctrace$NAMEActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in $NAME@Gl2Ctrace context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTGl2Ctrace$NAMEActionContext ) );

    if( sctiParseGl2Ctrace$NAMEActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyGl2Ctrace$NAMEActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyGl2Ctrace$NAMEActionContext( void* context )
{
    SCTGl2Ctrace$NAMEActionContext* c = ( SCTGl2Ctrace$NAMEActionContext* )( context );

    if( c == NULL )
    {
        return;
    }

    SCT_ASSERT_ALWAYS( c->wrapperContext == NULL );
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 */
SCTBoolean sctiGl2Ctrace$NAMEActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTGl2Ctrace$NAMEActionContext* context;
    Gl2Ctrace$NAMEActionData*       data;    
    int                             i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTGl2Ctrace$NAMEActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT_ALWAYS( context->wrapperContext == NULL );
    
    context->wrapperContext = sctCreateEglWrapperContext();
    if( context->wrapperContext == NULL )
    {
        SCT_LOG_ERROR( "Creating wrapper context failed in $NAME@Gl2Ctrace action init." );
        return SCT_FALSE;
    }
    
    for( i = 0; i < data->header; ++i )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: gl2ctrace init, playing frame %d", i );
#endif  /* defined( SCT_DEBUG ) */
        
        playFrame( context->wrapperContext, i );
        
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: gl2ctrace init, frame %d done", i );
#endif  /* defined( SCT_DEBUG ) */
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiGl2Ctrace$NAMEActionExecute( SCTAction* action, int frameNumber )
{
    SCTGl2Ctrace$NAMEActionContext* context;
    Gl2Ctrace$NAMEActionData*       data;
    GLenum                          err;
    int                             f;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTGl2Ctrace$NAMEActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT_ALWAYS( context->wrapperContext != NULL );
    
    f = data->header + frameNumber;
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: gl2ctrace execute, playing frame %d", f );
#endif  /* defined( SCT_DEBUG ) */
    
    playFrame( context->wrapperContext, f );
    context->lastFrame = f;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: gl2ctrace execute, frame %d done", f );
#endif  /* defined( SCT_DEBUG ) */
    
#if defined( SCT_GL2CTRACE_CHECK_ERRORS )
    err = glGetError();
    if( err != GL_NO_ERROR )
    {
        char buf[ 128 ];
        sprintf( buf, "GL error 0x%x in $NAME@Gl2Ctrace action execute.", err );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }
#else   /* defined( SCT_GL2CTRACE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_GL2CTRACE_CHECK_ERRORS ) */
    
    return SCT_TRUE;
}

/*!
 *
 */
void sctiGl2Ctrace$NAMEActionTerminate( SCTAction* action )
{
    SCTGl2Ctrace$NAMEActionContext* context;
    Gl2Ctrace$NAMEActionData*       data;    
    int                             i;
    int                             f;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTGl2Ctrace$NAMEActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT_ALWAYS( context->wrapperContext != NULL );
    
    for( i = 1; i <= data->trailer; ++i )
    {
        f = context->lastFrame + i;
        
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: gl2ctrace terminate, playing frame %d", f );
#endif  /* defined( SCT_DEBUG ) */
        
        playFrame( context->wrapperContext, f );

#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: gl2ctrace terminate, frame %d done", f );
#endif  /* defined( SCT_DEBUG ) */        
    }

    sctDestroyEglSwapperContext( context->wrapperContext );
    context->wrapperContext = NULL;   
}

/*!
 *
 *
 */
void sctiGl2Ctrace$NAMEActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyGl2Ctrace$NAMEActionContext( ( SCTGl2Ctrace$NAMEActionContext* )( action->context ) );
    action->context = NULL;
}

