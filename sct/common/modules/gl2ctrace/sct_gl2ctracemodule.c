/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_gl2ctracemodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_gl2ctracemodule_parser.h"
#include "sct_gl2ctracemodule_actions.h"

#include <stdio.h>
#include <string.h>

/* Local function declarations. */
SCTAttributeList*       sctiGl2CtraceModuleInfo( SCTModule* module );
SCTAction*              sctiGl2CtraceModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    sctiGl2CtraceModuleDestroy( SCTModule* module );

/*!
 *
 */
SCTModule* sctCreateGl2CtraceModule( void )
{
    return sctCreateModule( "Gl2Ctrace",
                            NULL,
#if defined( _WIN32 )
                            _gl2ctrace_parser_config,
#else   /* defined( _WIN32 )*/
                            "",
#endif  /* defined( _WIN32 ) */
                            sctiGl2CtraceModuleInfo,
                            sctiGl2CtraceModuleCreateAction,
                            sctiGl2CtraceModuleDestroy );
}

/*!
 *
 */
SCTAttributeList* sctiGl2CtraceModuleInfo( SCTModule* module )
{
    SCTAttributeList*           attributes;

    SCT_ASSERT_ALWAYS( module != NULL );

    attributes = sctCreateAttributeList();

    return attributes;
}

/*!
 *
 */
SCTAction* sctiGl2CtraceModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    return sctCreateActionFromTemplate( name,
                                        type,
                                        "Gl2Ctrace",
                                        NULL,
                                        attributes,
                                        Gl2CtraceActionTemplates,
                                        SCT_ARRAY_LENGTH( Gl2CtraceActionTemplates ) );
}

/*!
 *
 */
void sctiGl2CtraceModuleDestroy( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );
}
