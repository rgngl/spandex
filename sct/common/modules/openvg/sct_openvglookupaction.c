/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvglookupaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGLookupActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGLookupActionContext*   context;
    SCTOpenVGModuleContext*         mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGLookupActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGLookupActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Lookup@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGLookupActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGLookupActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLookupActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.dstImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLookupActionContext( context );
        SCT_LOG_ERROR( "Destination image index out of range in Lookup@OpenVG action context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.srcImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLookupActionContext( context );
        SCT_LOG_ERROR( "Source image index out of range in Lookup@OpenVG action context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidLookupTableIndex( context->moduleContext, context->data.redLutIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLookupActionContext( context );
        SCT_LOG_ERROR( "Red lut index out of range in Lookup@OpenVG action context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidLookupTableIndex( context->moduleContext, context->data.greenLutIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLookupActionContext( context );
        SCT_LOG_ERROR( "Green lut index out of range in Lookup@OpenVG action context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidLookupTableIndex( context->moduleContext, context->data.blueLutIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLookupActionContext( context );
        SCT_LOG_ERROR( "Blue lut index out of range in Lookup@OpenVG action context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidLookupTableIndex( context->moduleContext, context->data.alphaLutIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLookupActionContext( context );
        SCT_LOG_ERROR( "Alpha lut index out of range in Lookup@OpenVG action context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGLookupActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGLookupActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGLookupActionContext*   context;
    OpenVGLookupActionData*         data;
    VGErrorCode                     err;
    SCTOpenVGImage*                 dstImage;
    SCTOpenVGImage*                 srcImage;
    SCTOpenVGLookupTable*           redLUT;
    SCTOpenVGLookupTable*           greenLUT;
    SCTOpenVGLookupTable*           blueLUT;
    SCTOpenVGLookupTable*           alphaLUT;
    VGboolean                       outputLinear            = VG_FALSE;
    VGboolean                       outputPremultiplied     = VG_FALSE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGLookupActionContext* )( action->context );
    data    = &( context->data );

    dstImage = sctiOpenVGModuleGetImage( context->moduleContext, data->dstImageIndex );
    if( dstImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid destination image index in Lookup@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( dstImage->image != VG_INVALID_HANDLE );

    srcImage = sctiOpenVGModuleGetImage( context->moduleContext, data->srcImageIndex );
    if( srcImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid source image index in Lookup@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( srcImage->image != VG_INVALID_HANDLE );

    redLUT = sctiOpenVGModuleGetLookupTable( context->moduleContext, data->redLutIndex );
    if( redLUT == NULL || redLUT->type != SCT_OPENVG_LUT_UBYTE8 )
    {
        SCT_LOG_ERROR( "Invalid red lut in Lookup@OpenVG action execute." );
        return SCT_FALSE;
    }

    greenLUT = sctiOpenVGModuleGetLookupTable( context->moduleContext, data->greenLutIndex );
    if( greenLUT == NULL || greenLUT->type != SCT_OPENVG_LUT_UBYTE8 )
    {
        SCT_LOG_ERROR( "Invalid green lut in Lookup@OpenVG action execute." );
        return SCT_FALSE;
    }

    blueLUT = sctiOpenVGModuleGetLookupTable( context->moduleContext, data->blueLutIndex );
    if( blueLUT == NULL || blueLUT->type != SCT_OPENVG_LUT_UBYTE8 )
    {
        SCT_LOG_ERROR( "Invalid blue lut in Lookup@OpenVG action execute." );
        return SCT_FALSE;
    }

    alphaLUT = sctiOpenVGModuleGetLookupTable( context->moduleContext, data->alphaLutIndex );
    if( alphaLUT == NULL || alphaLUT->type != SCT_OPENVG_LUT_UBYTE8 )
    {
        SCT_LOG_ERROR( "Invalid alpha lut in Lookup@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( data->outputLinear == SCT_TRUE )
    {
        outputLinear = VG_TRUE;
    }
    if( data->outputPremultiplied == SCT_TRUE )
    {
        outputPremultiplied = VG_TRUE;
    }
    
    vgLookup( dstImage->image, 
              srcImage->image, 
              ( VGubyte* )( redLUT->lookupTable ),
              ( VGubyte* )( greenLUT->lookupTable ), 
              ( VGubyte* )( blueLUT->lookupTable ),
              ( VGubyte* )( alphaLUT->lookupTable ), 
              outputLinear,
              outputPremultiplied );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in Lookup@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGLookupActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGLookupActionContext( action->context );
    action->context = NULL;
}
