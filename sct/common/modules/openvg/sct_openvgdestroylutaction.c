/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdestroylutaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGDestroyLutActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDestroyLutActionContext*   context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGDestroyLutActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDestroyLutActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyLut@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDestroyLutActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGDestroyLutActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyLutActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidLookupTableIndex( context->moduleContext, context->data.lutIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyLutActionContext( context );
        SCT_LOG_ERROR( "Invalid lut index in DestroyLut@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDestroyLutActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDestroyLutActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGDestroyLutActionContext*   context;
    OpenVGDestroyLutActionData*         data;
    int                                 index;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGDestroyLutActionContext* )( action->context );
    data    = &( context->data );

    index = data->lutIndex;
    if( sctiOpenVGModuleGetLookupTable( context->moduleContext, index ) == NULL )
    {
        SCT_LOG_ERROR( "Invalid lut index in DestroyLut@OpenVG action execute." );
        return SCT_FALSE;
    }
    sctiOpenVGModuleDeleteLookupTables( context->moduleContext, index );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGDestroyLutActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGDestroyLutActionContext( action->context );
    action->context = NULL;
}
