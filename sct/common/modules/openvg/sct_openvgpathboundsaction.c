/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgpathboundsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGPathBoundsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGPathBoundsActionContext*   context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGPathBoundsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGPathBoundsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PathBounds@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGPathBoundsActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGPathBoundsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPathBoundsActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPathBoundsActionContext( context );
        SCT_LOG_ERROR( "Invalid path index in PathBounds@OpenVG context creation." );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidFloatIndex( context->moduleContext, context->data.minXIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid MinXIndex in PathBounds@OpenVG context creation." );
        sctiDestroyOpenVGPathBoundsActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidFloatIndex( context->moduleContext, context->data.minYIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid MinYIndex in PathBounds@OpenVG context creation." );
        sctiDestroyOpenVGPathBoundsActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidFloatIndex( context->moduleContext, context->data.widthIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid WidthIndex in PathBounds@OpenVG context creation." );
        sctiDestroyOpenVGPathBoundsActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidFloatIndex( context->moduleContext, context->data.heightIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid HeightIndex in PathBounds@OpenVG context creation." );
        sctiDestroyOpenVGPathBoundsActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGPathBoundsActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGPathBoundsActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGPathBoundsActionContext*   context;
    OpenVGPathBoundsActionData*         data;
    VGErrorCode                         err;
    SCTOpenVGPath*                      path;
    VGfloat                             x, y, width, height;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGPathBoundsActionContext* )( action->context );
    data    = &( context->data );

    path = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( path == NULL )
    {
        SCT_LOG_ERROR( "Invalid path in PathBounds@OpenVG action execute." );
        return SCT_FALSE;
    }

    SCT_ASSERT( path->path != VG_INVALID_HANDLE );

    vgPathBounds( path->path, 
                  &x,
                  &y,
                  &width,
                  &height );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in PathBounds@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    sctiOpenVGModuleSetFloat( context->moduleContext, data->minXIndex, x );
    sctiOpenVGModuleSetFloat( context->moduleContext, data->minYIndex, y );
    sctiOpenVGModuleSetFloat( context->moduleContext, data->widthIndex, width );
    sctiOpenVGModuleSetFloat( context->moduleContext, data->heightIndex, height );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGPathBoundsActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGPathBoundsActionContext( action->context );
    action->context = NULL;
}
