/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdashpatternaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGDashPatternActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDashPatternActionContext*  context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGDashPatternActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDashPatternActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DashPattern@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDashPatternActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGDashPatternActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDashPatternActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDashPatternActionContext( void* context )
{
    SCTOpenVGDashPatternActionContext* c;

    if( context == NULL )
    {
        return;
    }
    
    c = ( SCTOpenVGDashPatternActionContext* )( context );
    if( c->data.patterns != NULL )
    {
        sctDestroyFloatVector( c->data.patterns );
        c->data.patterns = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDashPatternActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGDashPatternActionContext*  context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGDashPatternActionContext* )( action->context );
    
#if defined( SPANDEX_DEPRECATED_OPENVG_1_0 )
    if( context->data.dashPhaseReset != SCT_FALSE )
    {
        SCT_LOG_ERROR( "DashPattern@OpenVG action init dash phase reset not supported." );
        return SCT_FALSE;
    }
#else   /* defined( SPANDEX_DEPRECATED_OPENVG_1_0 ) */
    SCT_USE_VARIABLE( context );
#endif  /* defined( SPANDEX_DEPRECATED_OPENVG_1_0 ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDashPatternActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGDashPatternActionContext*  context;
    OpenVGDashPatternActionData*        data;
    VGErrorCode                         err;
    VGboolean                           dashPhaseReset  = VG_FALSE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGDashPatternActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT( data->patterns != NULL );

    vgSetfv( VG_STROKE_DASH_PATTERN, data->patterns->length, data->patterns->data );
    vgSetf( VG_STROKE_DASH_PHASE, data->dashPhase );

#if !defined( SPANDEX_DEPRECATED_OPENVG_1_0 )
    if( data->dashPhaseReset == SCT_TRUE )
    {
        dashPhaseReset = VG_TRUE;
    }
    vgSeti( VG_STROKE_DASH_PHASE_RESET, dashPhaseReset );
#else   /* !defined( SPANDEX_DEPRECATED_OPENVG_1_0 ) */
    SCT_USE_VARIABLE( dashPhaseReset );
#endif  /* !defined( SPANDEX_DEPRECATED_OPENVG_1_0 ) */

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in DashPattern@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGDashPatternActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenVGDashPatternActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGDashPatternActionContext( action->context );
    action->context = NULL;
}

