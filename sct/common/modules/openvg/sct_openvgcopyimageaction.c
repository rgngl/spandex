/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcopyimageaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCopyImageActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCopyImageActionContext*    context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCopyImageActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCopyImageActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CopyImage@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCopyImageActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCopyImageActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCopyImageActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.srcImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCopyImageActionContext( context );
        SCT_LOG_ERROR( "Source image index out of range in CopyImage@OpenVG context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.dstImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCopyImageActionContext( context );
        SCT_LOG_ERROR( "Destination image index out of range in CopyImage@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCopyImageActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCopyImageActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGCopyImageActionContext*    context;
    OpenVGCopyImageActionData*          data;
    SCTOpenVGImage*                     srcImage;
    SCTOpenVGImage*                     dstImage;
    VGboolean                           dither;
    VGErrorCode                         err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGCopyImageActionContext* )( action->context );
    data    = &( context->data );

    srcImage = sctiOpenVGModuleGetImage( context->moduleContext, data->srcImageIndex );
    if( srcImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid source image in CopyImage@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( srcImage->image != VG_INVALID_HANDLE );

    dstImage = sctiOpenVGModuleGetImage( context->moduleContext, data->dstImageIndex );
    if( dstImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid destination image in CopyImage@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( dstImage->image != VG_INVALID_HANDLE );

    if( data->dither == SCT_TRUE )
    {
        dither = VG_TRUE;
    }
    else
    {
        dither = VG_FALSE;
    }

    vgCopyImage( dstImage->image,
                 data->dx,
                 data->dy,
                 srcImage->image,
                 data->sx,
                 data->sy,
                 data->width,
                 data->height,
                 dither );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in CopyImage@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCopyImageActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCopyImageActionContext( action->context );
    action->context = NULL;
}
