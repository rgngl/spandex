/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgsetpixelsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGSetPixelsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGSetPixelsActionContext*    context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGSetPixelsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGSetPixelsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SetPixels@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGSetPixelsActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGSetPixelsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSetPixelsActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.imageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSetPixelsActionContext( context );
        SCT_LOG_ERROR( "Image index out of range in SetPixels@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGSetPixelsActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSetPixelsActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGSetPixelsActionContext*    context;
    OpenVGSetPixelsActionData*          data;
    SCTOpenVGImage*                     sctImage;
    VGErrorCode                         err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGSetPixelsActionContext* )( action->context );
    data    = &( context->data );

    sctImage = sctiOpenVGModuleGetImage( context->moduleContext, data->imageIndex );
    if( sctImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid image index in SetPixels@OpenVG action execute." );
        return SCT_FALSE;
    }

    SCT_ASSERT( sctImage->image != VG_INVALID_HANDLE );

    vgSetPixels( data->dx,
                 data->dy,
                 sctImage->image,
                 data->sx,
                 data->sy,
                 data->width,
                 data->height );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in SetPixels@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGSetPixelsActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGSetPixelsActionContext( action->context );
    action->context = NULL;
}
