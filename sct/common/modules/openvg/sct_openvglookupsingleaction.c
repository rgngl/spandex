/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvglookupsingleaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGLookupSingleActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGLookupSingleActionContext* context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGLookupSingleActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGLookupSingleActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LookupSingle@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGLookupSingleActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGLookupSingleActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLookupSingleActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.dstImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLookupSingleActionContext( context );
        SCT_LOG_ERROR( "Destination image index out of range in LookupSingle@OpenVG action context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.srcImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLookupSingleActionContext( context );
        SCT_LOG_ERROR( "Source image index out of range in LookupSingle@OpenVG action context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidLookupTableIndex( context->moduleContext, context->data.lutIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLookupSingleActionContext( context );
        SCT_LOG_ERROR( "Lut index out of range in LookupSingle@OpenVG action context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGLookupSingleActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGLookupSingleActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGLookupSingleActionContext* context;
    OpenVGLookupSingleActionData*       data;
    VGErrorCode                         err;
    SCTOpenVGImage*                     dstImage;
    SCTOpenVGImage*                     srcImage;
    SCTOpenVGLookupTable*               lookupTable;
    VGboolean                           outputLinear            = VG_FALSE;
    VGboolean                           outputPremultiplied     = VG_FALSE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGLookupSingleActionContext* )( action->context );
    data    = &( context->data );

    dstImage = sctiOpenVGModuleGetImage( context->moduleContext, data->dstImageIndex );
    if( dstImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid destination image index in LookupSingle@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( dstImage->image != VG_INVALID_HANDLE );

    srcImage = sctiOpenVGModuleGetImage( context->moduleContext, data->srcImageIndex );
    if( srcImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid source image index in LookupSingle@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( srcImage->image != VG_INVALID_HANDLE );

    lookupTable = sctiOpenVGModuleGetLookupTable( context->moduleContext, data->lutIndex );
    if( lookupTable == NULL || lookupTable->type != SCT_OPENVG_LUT_UINT32 )
    {
        SCT_LOG_ERROR( "Invalid LUT in LookupSingle@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( data->outputLinear == SCT_TRUE )
    {
        outputLinear = VG_TRUE;
    }
    if( data->outputPremultiplied == SCT_TRUE )
    {
        outputPremultiplied = VG_TRUE;
    }

    vgLookupSingle( dstImage->image, 
                    srcImage->image, 
                    ( VGuint* )( lookupTable->lookupTable ), 
                    data->sourceChannel,
                    outputLinear,
                    outputPremultiplied );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in LookupSingle@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGLookupSingleActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGLookupSingleActionContext( action->context );
    action->context = NULL;
}
