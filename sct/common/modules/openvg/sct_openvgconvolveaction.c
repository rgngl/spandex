/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgconvolveaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGConvolveActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGConvolveActionContext* context;
    SCTOpenVGModuleContext*         mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGConvolveActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGConvolveActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Convolve@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGConvolveActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGConvolveActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGConvolveActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.dstImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGConvolveActionContext( context );
        SCT_LOG_ERROR( "Destination image index out of range in Convolve@OpenVG context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.srcImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGConvolveActionContext( context );
        SCT_LOG_ERROR( "Source image index out of range in Convolve@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGConvolveActionContext( void* context )
{
    SCTOpenVGConvolveActionContext* c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenVGConvolveActionContext* )( context );
    if( c->data.kernel != NULL )
    {
        sctDestroyIntVector( c->data.kernel );
        c->data.kernel = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGConvolveActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGConvolveActionContext* context;
    OpenVGConvolveActionData*       data;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGConvolveActionContext* )( action->context );
    data    = &( context->data );

    if( context->kernel == NULL )
    {
        context->kernel = ( VGshort* )( siCommonMemoryAlloc( NULL, data->kernel->length * sizeof( VGshort ) ) );
        if( context->kernel == NULL )
        {
            sctiOpenVGConvolveActionTerminate( action );
            SCT_LOG_ERROR( "Allocation failed in Convolve@OpenVG action init." );
            return SCT_FALSE;
        }
        if( sctOpenVGConvertTypeArray( context->kernel, 
                                       VG_PATH_DATATYPE_S_16, 
                                       data->kernel->data, 
                                       VG_PATH_DATATYPE_S_32, 
                                       data->kernel->length ) == SCT_FALSE )
        {
            sctiOpenVGConvolveActionTerminate( action );
            SCT_LOG_ERROR( "Invalid kernel data in Convolve@OpenVG action init." );
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGConvolveActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGConvolveActionContext* context;
    OpenVGConvolveActionData*       data;
    VGErrorCode                     err;
    SCTOpenVGImage*                 dstImage;
    SCTOpenVGImage*                 srcImage;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );
    
    context = ( SCTOpenVGConvolveActionContext* )( action->context );
    data    = &( context->data );

    dstImage = sctiOpenVGModuleGetImage( context->moduleContext, data->dstImageIndex );
    if( dstImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid destination image in Convolve@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( dstImage->image != VG_INVALID_HANDLE );

    srcImage = sctiOpenVGModuleGetImage( context->moduleContext, data->srcImageIndex );
    if( srcImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid source image index in Convolve@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( srcImage->image != VG_INVALID_HANDLE );

    vgConvolve( dstImage->image, 
                srcImage->image,
                data->kernelWidth, 
                data->kernelHeight,
                data->shiftX, 
                data->shiftY,
                context->kernel,
                data->scale,
                data->bias,
                data->tilingMode );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in Convolve@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGConvolveActionTerminate( SCTAction* action )
{
    SCTOpenVGConvolveActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    
    context = ( SCTOpenVGConvolveActionContext* )( action->context );

    if( context->kernel != NULL )
    {
        siCommonMemoryFree( NULL, context->kernel );
        context->kernel = NULL;
    }
}

/*!
 *
 *
 */
void sctiOpenVGConvolveActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGConvolveActionContext( action->context );
    action->context = NULL;
}
