/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgfilterformatpremultipliedaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGFilterFormatPremultipliedActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGFilterFormatPremultipliedActionContext*    context;
    SCTOpenVGModuleContext*                             mc;

    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGFilterFormatPremultipliedActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGFilterFormatPremultipliedActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in FilterFormatPremultiplied@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGFilterFormatPremultipliedActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGFilterFormatPremultipliedActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGFilterFormatPremultipliedActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGFilterFormatPremultipliedActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGFilterFormatPremultipliedActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGFilterFormatPremultipliedActionContext*    context;
    OpenVGFilterFormatPremultipliedActionData*          data;
    VGErrorCode                                         err;
    VGint                                               premultiplied   = VG_FALSE;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGFilterFormatPremultipliedActionContext* )( action->context );
    data    = &( context->data );

    if( data->premultiplied == SCT_TRUE )
    {
        premultiplied = VG_TRUE;
    }

    vgSeti( VG_FILTER_FORMAT_PREMULTIPLIED, premultiplied );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in FilterFormatPremultiplied@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGFilterFormatPremultipliedActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGFilterFormatPremultipliedActionContext( action->context );
    action->context = NULL;
}
