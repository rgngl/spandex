/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgfilterformatlinearaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGFilterFormatLinearActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGFilterFormatLinearActionContext*   context;
    SCTOpenVGModuleContext*                     mc;

    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGFilterFormatLinearActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGFilterFormatLinearActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in FilterFormatLinear@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGFilterFormatLinearActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGFilterFormatLinearActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGFilterFormatLinearActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGFilterFormatLinearActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGFilterFormatLinearActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGFilterFormatLinearActionContext*   context;
    OpenVGFilterFormatLinearActionData*         data;
    VGErrorCode                                 err;
    VGint                                       linear  = VG_FALSE;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGFilterFormatLinearActionContext* )( action->context );
    data    = &( context->data );

    if( data->linear == SCT_TRUE )
    {
        linear = VG_TRUE;
    }

    vgSeti( VG_FILTER_FORMAT_LINEAR, linear );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in FilterFormatLinear@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGFilterFormatLinearActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGFilterFormatLinearActionContext( action->context );
    action->context = NULL;
}
