/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgclosepathdirectaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGClosePathDirectActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGClosePathDirectActionContext*  context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGClosePathDirectActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGClosePathDirectActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ClosePathDirect@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGClosePathDirectActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGClosePathDirectActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGClosePathDirectActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGClosePathDirectActionContext( context );
        SCT_LOG_ERROR( "Path data index out of range in ClosePathDirect@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGClosePathDirectActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGClosePathDirectActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGClosePathDirectActionContext*  context;
    OpenVGClosePathDirectActionData*        data;
    VGubyte                                 pathSegments[ 1 ];
    SCTOpenVGPath*                          sctPath;
    VGErrorCode                             err;
    VGfloat                                 dummy;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGClosePathDirectActionContext* )( action->context );
    data    = &( context->data );

    sctPath = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( sctPath == NULL )
    {
        SCT_LOG_ERROR( "Invalid path in ClosePathDirect@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctPath->path != VG_INVALID_HANDLE );

    pathSegments[ 0 ] = VG_CLOSE_PATH;

    vgAppendPathData( sctPath->path, 1, pathSegments, &dummy );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in ClosePathDirect@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGClosePathDirectActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGClosePathDirectActionContext( action->context );
    action->context = NULL;
}
