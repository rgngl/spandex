/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatemasklayeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateMaskLayerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateMaskLayerActionContext*  context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCreateMaskLayerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateMaskLayerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateMaskLayer@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateMaskLayerActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCreateMaskLayerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateMaskLayerActionContext( context );
        return NULL;
    }

#if defined( OPENVG_VERSION_1_1 )
    if( sctiOpenVGModuleIsValidMaskLayerIndex( context->moduleContext, context->data.maskLayerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateMaskLayerActionContext( context );
        SCT_LOG_ERROR( "Invalid path index in CreateMaskLayer@OpenVG context creation." );
        return NULL;
    }
#endif  /* defined( OPENVG_VERSION_1_1 ) */

    return context;   
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateMaskLayerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateMaskLayerActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( OPENVG_VERSION_1_1 )
    SCT_LOG_ERROR( "CreateMaskLayer@OpenVG not supported in OpenVG 1.0." );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    return SCT_TRUE;   
#endif  /* !defined( OPENVG_VERSION_1_1 ) */   
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateMaskLayerActionExecute( SCTAction* action, int framenumber )
{
#if !defined( OPENVG_VERSION_1_1 )
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCTOpenVGCreateMaskLayerActionContext*  context;
    OpenVGCreateMaskLayerActionData*        data;
    int                                     index;
    SCTOpenVGMaskLayer*                     sctMaskLayer;
    VGMaskLayer                             vgMaskLayer;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGCreateMaskLayerActionContext* )( action->context );
    data    = &( context->data );

    index        = data->maskLayerIndex;
    sctMaskLayer = sctiOpenVGModuleGetMaskLayer( context->moduleContext, index );
    if( sctMaskLayer != NULL )
    {
        SCT_LOG_ERROR( "Mask layer index already in use in CreateMaskLayer@OpenVG action execute." );
        return SCT_FALSE;
    }

    vgMaskLayer = vgCreateMaskLayer( data->width, data->height );

    if( vgMaskLayer == VG_INVALID_HANDLE )
    {
        SCT_LOG_ERROR( "VGMaskLayer creation failed in CreateMaskLayer@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctMaskLayer = sctOpenVGCreateMaskLayer( vgMaskLayer );
    if( sctMaskLayer == NULL )
    {
        vgDestroyMaskLayer( vgMaskLayer );
        SCT_LOG_ERROR( "Allocation failed in CreateMaskLayer@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleSetMaskLayer( context->moduleContext, index, sctMaskLayer );

    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
}

/*!
 *
 *
 */
void sctiOpenVGCreateMaskLayerActionTerminate( SCTAction* action )
{
#if !defined( OPENVG_VERSION_1_1 )
    SCT_USE_VARIABLE( action );
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCTOpenVGCreateMaskLayerActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTOpenVGCreateMaskLayerActionContext* )( action->context );

    sctiOpenVGModuleDeleteMaskLayer( context->moduleContext, context->data.maskLayerIndex );
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
}

/*!
 *
 *
 */
void sctiOpenVGCreateMaskLayerActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCreateMaskLayerActionContext( action->context );
    action->context = NULL;
}
