/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgscubicbeziersaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGSCubicBeziersActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGSCubicBeziersActionContext*    context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGSCubicBeziersActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGSCubicBeziersActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SCubicBeziers@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGSCubicBeziersActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGSCubicBeziersActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSCubicBeziersActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathDataIndex( context->moduleContext, context->data.pathDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSCubicBeziersActionContext( context );
        SCT_LOG_ERROR( "Path data index out of range in SCubicBeziers@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGSCubicBeziersActionContext( void* context )
{
    SCTOpenVGSCubicBeziersActionContext*    c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenVGSCubicBeziersActionContext* )( context );
    if( c->data.coordinates != NULL )
    {
        sctDestroyDoubleVector( c->data.coordinates );
        c->data.coordinates = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSCubicBeziersActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGSCubicBeziersActionContext*    context;
    OpenVGSCubicBeziersActionData*          data;
    VGubyte                                 segment;
    int                                     i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGSCubicBeziersActionContext* )( action->context );
    data    = &( context->data );

    context->pathType         = ( VGPathDatatype )( -1 );
    context->numOfSegments    = data->coordinates->length / 4;
    context->numOfCoordinates = data->coordinates->length;

    if( context->pathSegments == NULL )
    {
        context->pathSegments = ( VGubyte* )( siCommonMemoryAlloc( NULL, context->numOfSegments ) ); 
    }
    if( context->pathData == NULL )
    {
        context->pathData = siCommonMemoryAlloc( NULL, context->numOfCoordinates * sizeof( VGfloat ) );
    }
    
    if( context->pathSegments == NULL || context->pathData == NULL )
    {
        sctiOpenVGSCubicBeziersActionTerminate( action );
        SCT_LOG_ERROR( "Allocation failed in SCubicBeziers@OpenVG action init." );
        return SCT_FALSE;
    }

    if( data->relative == SCT_FALSE )
    {
        segment = VG_SCUBIC_TO_ABS;
    }
    else
    {
        segment = VG_SCUBIC_TO_REL;
    }

    for( i = 0; i < context->numOfSegments; ++i )
    {
        context->pathSegments[ i ] = segment;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSCubicBeziersActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGSCubicBeziersActionContext*    context;
    OpenVGSCubicBeziersActionData*          data;
    SCTOpenVGPathData*                      pathData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGSCubicBeziersActionContext* )( action->context );
    data    = &( context->data );

    pathData = sctiOpenVGModuleGetPathData( context->moduleContext, data->pathDataIndex );
    if( pathData == NULL )
    {
        SCT_LOG_ERROR( "Invalid path data index in SCubicBeziers@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( context->pathType != pathData->type )
    {
        context->pathType = pathData->type;
        if( sctOpenVGConvertDoubleArray( context->pathData, 
                                         context->pathType,
                                         context->data.coordinates->data, 
                                         context->numOfCoordinates ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Coordinate data out of path datatype range in SCubicBeziers@OpenVG action execute." );
            return SCT_FALSE;
        }
    }

    if( sctOpenVGAddPathData( pathData, 
                              context->pathSegments, context->numOfSegments, 
                              context->pathData, context->numOfCoordinates, 
                              context->pathType ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Path data append failed in SCubicBeziers@OpenVG action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGSCubicBeziersActionTerminate( SCTAction* action )
{
    SCTOpenVGSCubicBeziersActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGSCubicBeziersActionContext* )( action->context );

    if( context->pathSegments != NULL )
    {
        siCommonMemoryFree( NULL, context->pathSegments );
        context->pathSegments = NULL;
    }
    if( context->pathData != NULL )
    {
        siCommonMemoryFree( NULL, context->pathData );
        context->pathData = NULL;
    }
}

/*!
 *
 *
 */
void sctiOpenVGSCubicBeziersActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGSCubicBeziersActionContext( action->context );
    action->context = NULL;
}
