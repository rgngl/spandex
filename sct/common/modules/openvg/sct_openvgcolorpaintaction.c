/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcolorpaintaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGColorPaintActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGColorPaintActionContext*   context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGColorPaintActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGColorPaintActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ColorPaint@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGColorPaintActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGColorPaintActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGColorPaintActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPaintIndex( context->moduleContext, context->data.paintIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGColorPaintActionContext( context );
        SCT_LOG_ERROR( "Paint index out of range in ColorPaint@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGColorPaintActionContext( void* context )
{
    SCTOpenVGColorPaintActionContext* c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenVGColorPaintActionContext* )( context );

    SCT_ASSERT_ALWAYS(  c->data.paintIndex >= SCT_MAX_OPENVG_PAINTS ||
                        c->moduleContext->paints[ c->data.paintIndex ] == VG_INVALID_HANDLE );

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGColorPaintActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGColorPaintActionContext*   context;
    OpenVGColorPaintActionData*         data;
    int                                 index;
    SCTOpenVGPaint*                     sctPaint;
    VGErrorCode                         err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGColorPaintActionContext* )( action->context );
    data    = &( context->data );

    index    = data->paintIndex;
    sctPaint = sctiOpenVGModuleGetPaint( context->moduleContext, index );
    if( sctPaint  == NULL )
    {
        SCT_LOG_ERROR( "Invalid paint in ColorPaint@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctPaint->paint != VG_INVALID_HANDLE );

    vgSetParameteri( sctPaint->paint, VG_PAINT_TYPE, VG_PAINT_TYPE_COLOR );
    vgSetParameterfv( sctPaint->paint, VG_PAINT_COLOR, 4, data->color );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in ColorPaint@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGColorPaintActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGColorPaintActionContext( action->context );
    action->context = NULL;
}




