/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatepathaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreatePathActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreatePathActionContext*   context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCreatePathActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreatePathActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePath@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreatePathActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCreatePathActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreatePathActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreatePathActionContext( context );
        SCT_LOG_ERROR( "Invalid path index in CreatePath@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreatePathActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreatePathActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreatePathActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGCreatePathActionContext*   context;
    OpenVGCreatePathActionData*         data;
    int                                 index;
    SCTOpenVGPath*                      sctPath;
    VGPath                              vgPath;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGCreatePathActionContext* )( action->context );
    data    = &( context->data );

    index = data->pathIndex;
    if( sctiOpenVGModuleGetPath( context->moduleContext, index ) != NULL )
    {
        SCT_LOG_ERROR( "Path index already in use in CreatePath@OpenVG action execute." );
        return SCT_FALSE;
    }

    vgPath = vgCreatePath( VG_PATH_FORMAT_STANDARD,
                           data->datatype,
                           data->scale, 
                           data->bias,
                           data->segmentCapacityHint,
                           data->coordCapacityHint,
                           data->capabilities );

    if( vgPath == VG_INVALID_HANDLE )
    {
        SCT_LOG_ERROR( "VGPath creation failed in CreatePath@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctPath = sctOpenVGCreatePath( vgPath, data->datatype );
    if( sctPath == NULL )
    {
        vgDestroyPath( vgPath );
        SCT_LOG_ERROR( "Allocation failed in CreatePath@OpenVG action execute." );
        return SCT_FALSE;
    }
    sctiOpenVGModuleSetPath( context->moduleContext, index, sctPath );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreatePathActionTerminate( SCTAction* action )
{
    SCTOpenVGCreatePathActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTOpenVGCreatePathActionContext* )( action->context );
    
    sctiOpenVGModuleDeletePath( context->moduleContext, context->data.pathIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreatePathActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCreatePathActionContext( action->context );
    action->context = NULL;
}
