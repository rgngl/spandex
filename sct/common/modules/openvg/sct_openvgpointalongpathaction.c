/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgpointalongpathaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGPointAlongPathActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGPointAlongPathActionContext*   context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGPointAlongPathActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGPointAlongPathActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PointAlongPath@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGPointAlongPathActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGPointAlongPathActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPointAlongPathActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPointAlongPathActionContext( context );
        SCT_LOG_ERROR( "Invalid path index in PointAlongPath@OpenVG context creation." );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidFloatIndex( context->moduleContext, context->data.xIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid XIndex in PointAlongPath@OpenVG context creation." );
        sctiDestroyOpenVGPointAlongPathActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidFloatIndex( context->moduleContext, context->data.yIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid YIndex in PointAlongPath@OpenVG context creation." );
        sctiDestroyOpenVGPointAlongPathActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidFloatIndex( context->moduleContext, context->data.tangentXIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid TangentXIndex in PointAlongPath@OpenVG context creation." );
        sctiDestroyOpenVGPointAlongPathActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidFloatIndex( context->moduleContext, context->data.tangentYIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid TangentYIndex in PointAlongPath@OpenVG context creation." );
        sctiDestroyOpenVGPointAlongPathActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGPointAlongPathActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGPointAlongPathActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGPointAlongPathActionContext*   context;
    OpenVGPointAlongPathActionData*         data;
    VGErrorCode                             err;
    SCTOpenVGPath*                          path;
    VGint                                   numSegments;
    VGfloat                                 x, y, tx, ty;
    VGfloat                                 *xp, *yp, *txp, *typ;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGPointAlongPathActionContext* )( action->context );
    data    = &( context->data );

    path = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( path == NULL )
    {
        SCT_LOG_ERROR( "Invalid path in PointAlongPath@OpenVG action execute." );
        return SCT_FALSE;
    }

    SCT_ASSERT( path->path != VG_INVALID_HANDLE );

    if( data->numSegments > 0 )
    {
        numSegments = data->numSegments;
    }
    else
    {
        numSegments = ( vgGetParameteri( path->path, VG_PATH_NUM_SEGMENTS ) - data->startSegment );
    }

    x = y = tx = ty = 0;
    xp = yp = txp = typ = NULL;

    if( data->point == SCT_TRUE )
    {
        xp = &x;
        yp = &y;
    }
    if( data->tangent == SCT_TRUE )
    {
        txp = &tx;
        typ = &ty;
    }

    vgPointAlongPath( path->path, 
                      data->startSegment,
                      numSegments,
                      data->distance,
                      xp,
                      yp,
                      txp,
                      typ );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in PointAlongPath@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    if( data->point == SCT_TRUE )
    {
        sctiOpenVGModuleSetFloat( context->moduleContext, data->xIndex, x );
        sctiOpenVGModuleSetFloat( context->moduleContext, data->yIndex, y );
    }
    if( data->tangent == SCT_TRUE )
    {
        sctiOpenVGModuleSetFloat( context->moduleContext, data->tangentXIndex, tx );
        sctiOpenVGModuleSetFloat( context->moduleContext, data->tangentYIndex, ty );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGPointAlongPathActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGPointAlongPathActionContext( action->context );
    action->context = NULL;
}
