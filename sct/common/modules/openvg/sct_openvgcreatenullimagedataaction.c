/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatenullimagedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateNullImageDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateNullImageDataActionContext*  context;
    SCTOpenVGModuleContext*                     mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCreateNullImageDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateNullImageDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateNullImageData@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateNullImageDataActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCreateNullImageDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateNullImageDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageDataIndex( context->moduleContext, context->data.imageDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateNullImageDataActionContext( context );
        SCT_LOG_ERROR( "Image data index out of range in CreateNullImageData@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateNullImageDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateNullImageDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateNullImageDataActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGCreateNullImageDataActionContext*  context;
    OpenVGCreateNullImageDataActionData*        data;
    int                                         index;
    SCTOpenVGImageData*                         imageData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGCreateNullImageDataActionContext* )( action->context );
    data    = &( context->data );

    index = data->imageDataIndex;
    if( sctiOpenVGModuleGetImageData( context->moduleContext, index ) != NULL )
    {
        SCT_LOG_ERROR( "Image data index already in use in CreateNullImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    imageData = sctOpenVGCreateImageData( data->format, data->width, data->height );

    if( imageData == NULL )
    {
        SCT_LOG_ERROR( "Image data creation failed in CreateNullImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleSetImageData( context->moduleContext, index, imageData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreateNullImageDataActionTerminate( SCTAction* action )
{
    SCTOpenVGCreateNullImageDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTOpenVGCreateNullImageDataActionContext* )( action->context );

    sctiOpenVGModuleDeleteImageData( context->moduleContext, context->data.imageDataIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreateNullImageDataActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCreateNullImageDataActionContext( action->context );
    action->context = NULL;
}
