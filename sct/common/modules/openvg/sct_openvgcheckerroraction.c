/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcheckerroraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCheckErrorActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCheckErrorActionContext*   context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCheckErrorActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCheckErrorActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CheckError@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCheckErrorActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCheckErrorActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCheckErrorActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCheckErrorActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCheckErrorActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGCheckErrorActionContext*   context;
    OpenVGCheckErrorActionData*         data;
    VGErrorCode                         err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGCheckErrorActionContext* )( action->context );
    data    = &( context->data );
    
    err = vgGetError();
    
    if( err != VG_NO_ERROR )
    {
        char buf[ 512 ];
        if( data->message != NULL && strlen( data->message ) < 1 )
        {
            sprintf( buf, "VG error %s in CheckError@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        }
        else
        {
            sprintf( buf, "VG error %s, message \"%s\" in CheckError@OpenVG action execute.",
                     sctiOpenVGGetErrorString( err ), data->message );
        }
        SCT_LOG_ERROR( buf );
        
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: %s", buf );
#endif  /* defined( SCT_DEBUG ) */
        
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCheckErrorActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCheckErrorActionContext( action->context );
    action->context = NULL;
}
