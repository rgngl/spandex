/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgscissorrectsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGScissorRectsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGScissorRectsActionContext* context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGScissorRectsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGScissorRectsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ScissorRects@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGScissorRectsActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGScissorRectsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGScissorRectsActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGScissorRectsActionContext( void* context )
{
    SCTOpenVGScissorRectsActionContext* c;
    if( context == NULL )
    {
        return;
    }
    c = ( SCTOpenVGScissorRectsActionContext* )( context );
    if( c->data.rectangles != NULL )
    {
        sctDestroyIntVector( c->data.rectangles );
        c->data.rectangles = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGScissorRectsActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGScissorRectsActionContext* context;
    OpenVGScissorRectsActionData*       data;
    VGErrorCode                         err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGScissorRectsActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT( data->rectangles != NULL );
    SCT_ASSERT( data->rectangles->length % 4 == 0 );

    vgSetiv( VG_SCISSOR_RECTS, data->rectangles->length, (  const VGint* )( data->rectangles->data ) );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in ScissorRects@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGScissorRectsActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGScissorRectsActionContext( action->context );
    action->context = NULL;
}
