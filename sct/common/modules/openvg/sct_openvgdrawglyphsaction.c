/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdrawglyphsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGDrawGlyphsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDrawGlyphsActionContext*   context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGDrawGlyphsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDrawGlyphsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DrawGlyphs@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDrawGlyphsActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGDrawGlyphsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDrawGlyphsActionContext( context );
        return NULL;
    }

    if( context->data.glyphCount > context->data.glyphIndices->length )
    {
        sctiDestroyOpenVGDrawGlyphsActionContext( context );
        SCT_LOG_ERROR( "Too few glyph indices in DrawGlyphs@OpenVG context creation." );
        return NULL;
    }

    if( ( context->data.adjustmentsX->length > 0 ) && 
        ( context->data.glyphCount > context->data.adjustmentsX->length ) )
    {
        sctiDestroyOpenVGDrawGlyphsActionContext( context );
        SCT_LOG_ERROR( "Invalid adjustment X in DrawGlyphs@OpenVG context creation." );
        return NULL;
    }

    if( ( context->data.adjustmentsY->length > 0 ) && 
        ( context->data.glyphCount > context->data.adjustmentsY->length ) )
    {
        sctiDestroyOpenVGDrawGlyphsActionContext( context );
        SCT_LOG_ERROR( "Invalid adjustment Y in DrawGlyphs@OpenVG context creation." );
        return NULL;
    }

#if defined( OPENVG_VERSION_1_1 )
    if( sctiOpenVGModuleIsValidFontIndex( context->moduleContext, context->data.fontIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDrawGlyphsActionContext( context );
        SCT_LOG_ERROR( "Font index out of range in DrawGlyphs@OpenVG context creation." );
        return NULL;
    } 
#endif  /* defined( OPENVG_VERSION_1_1 ) */

    return context;    
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDrawGlyphsActionContext( void* context )
{
    SCTOpenVGDrawGlyphsActionContext*   c;
    if( context == NULL )
    {
        return;
    }
    c = ( SCTOpenVGDrawGlyphsActionContext* )( context );
    if( c->data.glyphIndices != NULL )
    {
        sctDestroyIntVector( c->data.glyphIndices );
        c->data.glyphIndices = NULL;
    }
    if( c->data.adjustmentsX != NULL )
    {
        sctDestroyFloatVector( c->data.adjustmentsX );
        c->data.adjustmentsX = NULL;
    }
    if( c->data.adjustmentsY != NULL )
    {
        sctDestroyFloatVector( c->data.adjustmentsY );
        c->data.adjustmentsY = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDrawGlyphsActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( OPENVG_VERSION_1_1 )
    SCT_LOG_ERROR( "DrawGlyphs@OpenVG not supported in OpenVG 1.0." );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */  
    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */    
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDrawGlyphsActionExecute( SCTAction* action, int framenumber )
{
#if !defined( OPENVG_VERSION_1_1 )
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCTOpenVGDrawGlyphsActionContext*   context;
    OpenVGDrawGlyphsActionData*         data;
    SCTOpenVGFont*                      font;
    VGErrorCode                         err;
    VGuint*                             glyphIndices     = NULL;
    VGfloat*                            adjustmentsX     = NULL;
    VGfloat*                            adjustmentsY     = NULL;
    VGboolean                           allowAutoHinting = VG_FALSE;
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGDrawGlyphsActionContext* )( action->context );
    data    = &( context->data );

    font = sctiOpenVGModuleGetFont( context->moduleContext, data->fontIndex );
    if( font == NULL )
    {
        SCT_LOG_ERROR( "Invalid font in DrawGlyphs@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( font->font != VG_INVALID_HANDLE );

    glyphIndices = ( VGuint* )( data->glyphIndices->data );

    if( data->adjustmentsX->length > 0 )
    {
        adjustmentsX = ( VGfloat* )( data->adjustmentsX->data );
    }
    if( data->adjustmentsY->length > 0 )
    {
        adjustmentsY = ( VGfloat* )( data->adjustmentsY->data );
    }
    if( data->allowAutoHinting == SCT_TRUE )
    {
        allowAutoHinting = VG_TRUE;
    }

    vgDrawGlyphs( font->font, 
                  data->glyphCount,
                  glyphIndices,
                  adjustmentsX,
                  adjustmentsY,
                  data->paintModes, 
                  allowAutoHinting );

# if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in DrawGlyphs@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();
        
        return SCT_FALSE;
    }
# else  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
# endif /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
}

/*!
 *
 *
 */
void sctiOpenVGDrawGlyphsActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenVGDrawGlyphsActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGDrawGlyphsActionContext( action->context );
    action->context = NULL;
}
