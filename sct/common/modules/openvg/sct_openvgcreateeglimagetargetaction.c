/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreateeglimagetargetaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

#if defined( INCLUDE_EGL_MODULE )
# include "sct_eglmodule.h"
#endif  /* defined( INCLUDE_EGL_MODULE ) */

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateEglImageTargetActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateEglImageTargetActionContext* context;
    SCTOpenVGModuleContext*                     mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCreateEglImageTargetActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateEglImageTargetActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateEglImageTarget@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateEglImageTargetActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCreateEglImageTargetActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateEglImageTargetActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.imageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateEglImageTargetActionContext( context );
        SCT_LOG_ERROR( "Image index out of range in CreateEglImageTarget@OpenVG context creation." );
        return NULL;
    } 

    context->vgCreateEGLImageTargetKHR = NULL;
    context->validated                 = SCT_FALSE;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateEglImageTargetActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateEglImageTargetActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGCreateEglImageTargetActionContext* context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGCreateEglImageTargetActionContext* )( action->context );  

#if !defined( INCLUDE_EGL_MODULE )
    SCT_USE_VARIABLE( context );
    SCT_LOG_ERROR( "CreateEglImageTarget@OpenVG action requires EGL module." );
    return SCT_FALSE;
    
#else   /* !defined( INCLUDE_EGL_MODULE ) */
    context->vgCreateEGLImageTargetKHR = ( SCTPfnCreateEGLImageTargetKHR )( siCommonGetProcAddress( NULL, "vgCreateEGLImageTargetKHR" ) );
    if( context->vgCreateEGLImageTargetKHR == NULL )
    {
        SCT_LOG_ERROR( "vgCreateEGLImageTargetKHR function not found in CreateEglImageTarget@OpenVG action init." );
        return SCT_FALSE;
    }
    
    context->validated = SCT_FALSE;   
    
    return SCT_TRUE;
#endif  /* !defined( INCLUDE_EGL_MODULE ) */       
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateEglImageTargetActionExecute( SCTAction* action, int framenumber )
{
#if defined( INCLUDE_EGL_MODULE )    
    SCTOpenVGCreateEglImageTargetActionContext* context;
    OpenVGCreateEglImageTargetActionData*       data;
    int                                         index;
    SCTOpenVGImage*                             sctImage;
    VGImage                                     vgImage;
    EGLImageKHR                                 eglImage    = ( EGLImageKHR )( 0 );
    const char*                                 extString;
    int                                         width;
    int                                         height;    
    VGErrorCode                                 err;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGCreateEglImageTargetActionContext* )( action->context );
    data    = &( context->data );

    index = data->imageIndex;
    sctImage = sctiOpenVGModuleGetImage( context->moduleContext, index );
    if( sctImage != NULL )
    {
        SCT_LOG_ERROR( "Image index already in use in CreateEglImageTarget@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( context->validated == SCT_FALSE )
    {
        extString = ( const char* )( vgGetString( VG_EXTENSIONS ) );
        if( extString == NULL || strstr( extString, "VG_KHR_EGL_image" ) == NULL )
        {
            SCT_LOG_ERROR( "VG_KHR_EGL_image extension not supported in CreateEglImageTarget@OpenVG action execute." );
            return SCT_FALSE;
        }
        context->validated = SCT_TRUE;
    }

    eglImage = sctEglModuleGetImageExt( data->eglImageIndex );
    if( eglImage == ( EGLImageKHR )( 0 ) )
    {
        SCT_LOG_ERROR( "Invalid EGL image in CreateEglImageTarget@OpenVG action execute." );        
        return SCT_FALSE;
    }

    SCT_ASSERT( context->vgCreateEGLImageTargetKHR != NULL );
    
    vgImage = context->vgCreateEGLImageTargetKHR( ( SCTVGeglImageKHR )( eglImage ) );    
    if( vgImage == VG_INVALID_HANDLE )
    {
        char buf[ 256 ];
        
        err = vgGetError();
        sprintf( buf, "VGImage creation failed with error %s in CreateEglImageTarget@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();
        
        return SCT_FALSE;
    }

    width  = ( int )( vgGetParameteri( vgImage, VG_IMAGE_WIDTH ) );
    height = ( int )( vgGetParameteri( vgImage, VG_IMAGE_HEIGHT ) );    
    
    sctImage = sctOpenVGCreateImage( vgImage, width, height );
    if( sctImage == NULL )
    {
        vgDestroyImage( vgImage );
        SCT_LOG_ERROR( "Allocation failed in CreateEglImageTarget@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleSetImage( context->moduleContext, index, sctImage );

    return SCT_TRUE;
#else   /* defined( INCLUDE_EGL_MODULE ) */
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;    
#endif  /* defined( INCLUDE_EGL_MODULE ) */    
}

/*!
 *
 *
 */
void sctiOpenVGCreateEglImageTargetActionTerminate( SCTAction* action )
{
    SCTOpenVGCreateEglImageTargetActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGCreateEglImageTargetActionContext* )( action->context );

    sctiOpenVGModuleDeleteImage( context->moduleContext, context->data.imageIndex );

    context->vgCreateEGLImageTargetKHR = NULL;
    context->validated                 = SCT_FALSE;
}

/*!
 *
 *
 */
void sctiOpenVGCreateEglImageTargetActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCreateEglImageTargetActionContext( action->context );
    action->context = NULL;
}
