#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
SetModuleName( 'OpenVG' )

######################################################################
AddInclude( 'sct_openvgmodule.h' )
AddInclude( 'sct_vg.h' )

######################################################################
MAX_PAINT_INDEX             = 4096
MAX_INDEX_INDEX             = 4096
MAX_PATH_INDEX              = 4096
MAX_DATA_INDEX              = 4096
MAX_IMAGE_DATA_INDEX        = 4096
MAX_PATH_DATA_INDEX         = 4096
MAX_TIGER_INDEX             = 16
MAX_FLOAT_INDEX             = 16
MAX_LOOKUP_TABLE_INDEX      = 16
MAX_FILENAME_LENGTH         = 256
MAX_EXTENSION_NAME_LENGTH   = 512
MAX_FUNCTION_NAME_LENGTH    = 512

# OpenVG 1.1
MAX_FONT_INDEX              = 4096
MAX_MASK_LAYER_INDEX        = 4096

MAX_HANDLE_INDEX            = min( MAX_INDEX_INDEX, MAX_MASK_LAYER_INDEX )
MAX_ERROR_MESSAGE_LENGTH    = 128

################################################################################
# Info. Print information about OpenVG implementation to output; usually to
# report file.
AddActionConfig( 'Info',
                 'Execute'+'Destroy',
                 [ ],
                 )

######################################################################
# CheckExtension. Check the availability of the given OpenVG extension. The action
# reports an error if the extension is not present.
AddActionConfig( 'CheckExtension',
                 'Execute'+'Destroy',
                 [ StringAttribute(             'Extension',                            MAX_EXTENSION_NAME_LENGTH )
                   ]
                 )

######################################################################
# CheckFunction. Check the availability of the given function. The action
# reports an error if the function is not present.
AddActionConfig( 'CheckFunction',
                 'Execute'+'Destroy',
                 [ StringAttribute(             'Function',                            MAX_FUNCTION_NAME_LENGTH )
                   ]
                 )

######################################################################
# CheckValue. Query OpenVG value(s) and compare to input value(s) according to
# condition; <queried value> <condition> <value>. Currently, SCT_FOUND condition
# cannot be used with any value.
AddActionConfig( 'CheckValue',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Value', 'VGint',                       [ 'VG_MAX_SCISSOR_RECTS',
                                                                                          'VG_MAX_DASH_COUNT',
                                                                                          'VG_MAX_KERNEL_SIZE',
                                                                                          'VG_MAX_SEPARABLE_KERNEL_SIZE',
                                                                                          'VG_MAX_COLOR_RAMP_STOPS',
                                                                                          'VG_MAX_IMAGE_WIDTH',
                                                                                          'VG_MAX_IMAGE_HEIGHT',
                                                                                          'VG_MAX_IMAGE_PIXELS',
                                                                                          'VG_MAX_IMAGE_BYTES',
                                                                                          'VG_MAX_GAUSSIAN_STD_DEVIATION' ] ),
                   EnumAttribute(               'Condition', 'SCTCondition',            [ 'SCT_LESS',
                                                                                          'SCT_LEQUAL',
                                                                                          'SCT_GREATER',
                                                                                          'SCT_GEQUAL',
                                                                                          'SCT_EQUAL',
                                                                                          'SCT_NOTEQUAL',
                                                                                          'SCT_FOUND' ] ),
                   FloatVectorAttribute(        'Values' ),
                   ]
                 )


################################################################################
# CompareFloat. Compare value in a float register Index to Value. Allow Delta
# difference.  Action returns an error if difference is larger than abs(Delta).
AddActionConfig( 'CompareFloat',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Index',                                0, MAX_FLOAT_INDEX - 1 ),
                   FloatAttribute(              'Value' ),
                   FloatAttribute(              'Delta' )
                   ]
                 )

################################################################################
# Flush. OpenVG flush.
AddActionConfig( 'Flush',
                 'Execute'+'Destroy',
                 [ ],
                 )

################################################################################
# Finish. OpenVG finish. 
AddActionConfig( 'Finish',
                 'Execute'+'Destroy',
                 [ ],
                 )

################################################################################
# CheckError. Message can be used to detect the specific action which failed the
# error check.
AddActionConfig( 'CheckError',
                 'Execute'+'Destroy',
                 [ StringAttribute(             'Message',                              MAX_ERROR_MESSAGE_LENGTH )
                   ],
                 )

################################################################################
# MatrixMode. Set OpenVG matrix mode. VG_MATRIX_GLYPH_USER_TO_SURFACE mode is
# supported only in OpenVG 1.1.
AddActionConfig( 'MatrixMode',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'VGMatrixMode',                 [ 'VG_MATRIX_PATH_USER_TO_SURFACE',
                                                                                          'VG_MATRIX_IMAGE_USER_TO_SURFACE',
                                                                                          'VG_MATRIX_FILL_PAINT_TO_USER',
                                                                                          'VG_MATRIX_STROKE_PAINT_TO_USER',
                                                                                          'VG_MATRIX_GLYPH_USER_TO_SURFACE' ] )
                   ]
                 )

################################################################################
# FillRule. Set OpenVG fill rule.
AddActionConfig( 'FillRule',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Rule', 'VGFillRule',                   [ 'VG_EVEN_ODD',
                                                                                          'VG_NON_ZERO' ] )
                   ]
                 )

################################################################################
# ImageQuality. Set OpenVG image quality.
AddActionConfig( 'ImageQuality',
                 'Execute'+'Destroy',
                  [ EnumAttribute(              'Quality', 'VGImageQuality',            [ 'VG_IMAGE_QUALITY_NONANTIALIASED',
                                                                                          'VG_IMAGE_QUALITY_FASTER',
                                                                                          'VG_IMAGE_QUALITY_BETTER' ] )
                    ]
                  )

################################################################################
# RenderingQuality. Set OpenVG rendering quality.
AddActionConfig( 'RenderingQuality',
                 'Execute'+'Destroy',
                  [ EnumAttribute(              'Quality', 'VGRenderingQuality',        [ 'VG_RENDERING_QUALITY_NONANTIALIASED',
                                                                                          'VG_RENDERING_QUALITY_FASTER',
                                                                                          'VG_RENDERING_QUALITY_BETTER' ] )
                    ]
                  )

################################################################################
# BlendMode. Set OpenVG blend mode.
AddActionConfig( 'BlendMode',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'VGBlendMode',                  [ 'VG_BLEND_SRC',
                                                                                          'VG_BLEND_SRC_OVER',
                                                                                          'VG_BLEND_DST_OVER',
                                                                                          'VG_BLEND_SRC_IN',
                                                                                          'VG_BLEND_DST_IN',
                                                                                          'VG_BLEND_MULTIPLY',
                                                                                          'VG_BLEND_SCREEN',
                                                                                          'VG_BLEND_DARKEN',
                                                                                          'VG_BLEND_LIGHTEN',
                                                                                          'VG_BLEND_ADDITIVE' ] )
                   ]
                 )

################################################################################
# ScissorRects. Set OpenVG scissor rects. When defined, the length of the
# rectangle vector must be a multiple of four (minX, minY, width, height).
AddActionConfig( 'ScissorRects',
                 'Execute'+'Destroy',
                 [ IntVectorAttribute(          'Rectangles' )
                   ],
                 [ ValidatorConfig( 'Rectangles != NULL && Rectangles->length % 4 != 0',
                                    'Invalid scissor rectangles data' )
                   ]
                 )

################################################################################
# Stroke. Set all OpenVG stroke parameters with a single action.
AddActionConfig( 'Stroke',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'LineWidth' ),
                   EnumAttribute(               'CapStyle', 'VGCapStyle',               [ 'VG_CAP_BUTT',
                                                                                          'VG_CAP_ROUND',
                                                                                          'VG_CAP_SQUARE' ] ),
                   EnumAttribute(               'JoinStyle', 'VGJoinStyle',             [ 'VG_JOIN_MITER',
                                                                                          'VG_JOIN_ROUND',
                                                                                          'VG_JOIN_BEVEL' ] ),
                   FloatAttribute(              'MiterLimit' )
                   ]
                 )

################################################################################
# StrokeLineWidth. Set OpenVG stroke line width.
AddActionConfig( 'StrokeLineWidth',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'LineWidth' )
                   ]
                 )

################################################################################
# StrokeCapStyle. Set OpenVG stroke cap style.
AddActionConfig( 'StrokeCapStyle',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'CapStyle', 'VGCapStyle',               [ 'VG_CAP_BUTT',
                                                                                          'VG_CAP_ROUND',
                                                                                          'VG_CAP_SQUARE' ] )
                   ]
                 )

################################################################################
# StrokeJoinStyle. Set OpenVG stroke join style.
AddActionConfig( 'StrokeJoinStyle',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'JoinStyle', 'VGJoinStyle',             [ 'VG_JOIN_MITER',
                                                                                          'VG_JOIN_ROUND',
                                                                                          'VG_JOIN_BEVEL' ] )
                   ]
                 )

################################################################################
# StrokeMiterLimit. Set OpenVG stroke miter limit.
AddActionConfig( 'StrokeMiterLimit',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'MiterLimit' )
                   ]
                 )

################################################################################
# StrokeDashPattern. Set OpenVG stroke dash patterns. When defined, the length
# of the pattern vector must be a multiple of two (on, off).
AddActionConfig( 'StrokeDashPattern',
                 'Execute'+'Destroy',
                 [ FloatVectorAttribute(        'Patterns' )
                   ],
                 [ ValidatorConfig( 'Patterns != NULL && Patterns->length % 2 != 0',
                                    'Invalid dash pattern data' ) ]
                 )

################################################################################
# StrokeDashPhase. Set OpenVG stroke dash phase.
AddActionConfig( 'StrokeDashPhase',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'DashPhase' ),
                   ],
                 )

################################################################################
# DashPattern. Set all OpenVG stroke dash pattern parameters with a single
# action.
AddActionConfig( 'DashPattern',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ FloatVectorAttribute(        'Patterns' ),
                   FloatAttribute(              'DashPhase' ),
                   OnOffAttribute(              'DashPhaseReset' )
                   ],
                 [ ValidatorConfig( 'Patterns != NULL && Patterns->length % 2 != 0',
                                    'Invalid dash pattern data' ) ]
                 )

################################################################################
# TileFillColor. Set OpenVG tile fill color.
AddActionConfig( 'TileFillColor',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Color',                                4, 0, 1 )
                   ],
                 )

################################################################################
# ClearColor. Set OpenVG clear color.
AddActionConfig( 'ClearColor',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Color',                                4, 0, 1 )
                   ],
                 )

################################################################################
# GlyphOrigin. Set OpenVG glyph origin. GlyphOrigin action is supported only in
# OpenVG 1.1.
AddActionConfig( 'GlyphOrigin',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ FloatArrayAttribute(         'Origin',                               2 )
                   ],
                 )

################################################################################
# Enable. Enable OpenVG state. VG_COLOR_TRANSFORM is supported only in OpenVG
# 1.1.
AddActionConfig( 'Enable',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Param',  'VGParamType',                [ 'VG_COLOR_TRANSFORM',
                                                                                          'VG_MASKING',
                                                                                          'VG_SCISSORING',
                                                                                          'VG_STROKE_DASH_PHASE_RESET',
                                                                                          'VG_FILTER_FORMAT_LINEAR',
                                                                                          'VG_FILTER_FORMAT_PREMULTIPLIED' ] )
                   ]
                 )

################################################################################
# Disable. Disable OpenVG state. VG_COLOR_TRANSFORM is supported only in OpenVG
# 1.1.
AddActionConfig( 'Disable',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Param',  'VGParamType',                [ 'VG_COLOR_TRANSFORM',
                                                                                          'VG_MASKING',
                                                                                          'VG_SCISSORING',
                                                                                          'VG_STROKE_DASH_PHASE_RESET',
                                                                                          'VG_FILTER_FORMAT_LINEAR',
                                                                                          'VG_FILTER_FORMAT_PREMULTIPLIED' ] )
                   ]
                 )

################################################################################
# Masking. Enable/disable OpenVG masking. Alternatively, use Enable/Disable
# action.
AddActionConfig( 'Masking',
                 'Execute'+'Destroy',
                 [ OnOffAttribute(              'Masking' )
                   ]
                 )

################################################################################
# Scissoring. Enable/disable OpenVG scissoring. Alternatively, use
# Enable/Disable action.
AddActionConfig( 'Scissoring',
                 'Execute'+'Destroy',
                 [ OnOffAttribute(              'Scissoring' )
                   ]
                 )

################################################################################
# PixelLayout. Set OpenVG pixel layout.
AddActionConfig( 'PixelLayout',
                 'Execute'+'Destroy',
                  [ EnumAttribute(              'Layout', 'VGPixelLayout',              [ 'VG_PIXEL_LAYOUT_UNKNOWN',
                                                                                          'VG_PIXEL_LAYOUT_RGB_VERTICAL',
                                                                                          'VG_PIXEL_LAYOUT_BGR_VERTICAL',
                                                                                          'VG_PIXEL_LAYOUT_RGB_HORIZONTAL',
                                                                                          'VG_PIXEL_LAYOUT_BGR_HORIZONTAL' ] )
                    ]
                  )

################################################################################
# FilterFormatLinear. Enable/disable OpenVG linear filter format. Alternatively,
# use Enable/Disable action.
AddActionConfig( 'FilterFormatLinear',
                 'Execute'+'Destroy',
                 [ OnOffAttribute(              'Linear' )
                   ],
                 )

################################################################################
# FilterFormatPremultiplied. Enable/disable OpenVG premultiplied filter
# format. Alternatively, use Enable/Disable action.
AddActionConfig( 'FilterFormatPremultiplied',
                 'Execute'+'Destroy',
                 [ OnOffAttribute(              'Premultiplied' )
                   ],
                 )

################################################################################
# FilterChannelMask. Set OpenVG filter channel mask.
AddActionConfig( 'FilterChannelMask',
                 'Execute'+'Destroy',
                 [ EnumMaskAttribute(           'Mask', 'VGint',                        [ 'VG_RED',
                                                                                          'VG_GREEN',
                                                                                          'VG_BLUE',
                                                                                          'VG_ALPHA' ] ),
                   ]
                 )

################################################################################
# LoadIdentity. Load identity to current matrix.
AddActionConfig( 'LoadIdentity',
                 'Execute'+'Destroy',
                 [ ]
                 )

################################################################################
# LoadMatrix. Load matrix to current matrix.
AddActionConfig( 'LoadMatrix',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Matrix',                               9 )
                   ]
                 )

################################################################################
# MultMatrix. Multiply current matrix.
AddActionConfig( 'MultMatrix',
                 'Execute'+'Destroy',
                 [ FloatArrayAttribute(         'Matrix',                               9 )
                   ]
                 )

################################################################################
# Translate. Add translation to current matrix.
AddActionConfig( 'Translate',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'Tx' ),
                   FloatAttribute(              'Ty' )
                   ]
                 )

################################################################################
# Scale. Add scale to current matrix.
AddActionConfig( 'Scale',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'Sx' ),
                   FloatAttribute(              'Sy' )
                   ]
                 )

################################################################################
# Shear. Add shear to current matrix.
AddActionConfig( 'Shear',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'Shx' ),
                   FloatAttribute(              'Shy' )
                   ]
                 )

################################################################################
# Rotate. Add rotate to current matrix.
AddActionConfig( 'Rotate',
                 'Execute'+'Destroy',
                 [ FloatAttribute(              'Angle' )
                   ]
                 )

################################################################################
# Mask. OpenVG mask operation. Handle index refers to an image index in OpenVG
# 1.0, and to an image index or a mask layer index in OpenVG 1.1. Use handle
# index -1 with VG_CLEAR_MASK and VG_FILL_MASK operations.
AddActionConfig( 'Mask',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'HandleIndex',                          -1, MAX_HANDLE_INDEX - 1 ),
                   AutoEnumAttribute(           'HandleType',                           [ 'IMAGE',
                                                                                          'MASK_LAYER' ] ),
                   EnumAttribute(               'Operation', 'VGMaskOperation',         [ 'VG_CLEAR_MASK',
                                                                                          'VG_FILL_MASK',
                                                                                          'VG_SET_MASK',
                                                                                          'VG_UNION_MASK',
                                                                                          'VG_INTERSECT_MASK',
                                                                                          'VG_SUBTRACT_MASK' ] ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# RenderToMask. OpenVG render to mask. RenderToMask action is supported only in
# OpenVG 1.1.
AddActionConfig( 'RenderToMask',
                 'Init'+'Execute'+'Terminate'+'Destroy',                 
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   EnumMaskAttribute(           'PaintModes', 'VGbitfield',             [ 'VG_STROKE_PATH',
                                                                                          'VG_FILL_PATH' ] ),
                   EnumAttribute(               'Operation', 'VGMaskOperation',         [ 'VG_CLEAR_MASK',
                                                                                          'VG_FILL_MASK',
                                                                                          'VG_SET_MASK',
                                                                                          'VG_UNION_MASK',
                                                                                          'VG_INTERSECT_MASK',
                                                                                          'VG_SUBTRACT_MASK' ] )
                   ]
                 )

################################################################################
# CreateMaskLayer. Create OpenVG mask layer. CreateMaskLayer action is supported
# only in OpenVG 1.1.
AddActionConfig( 'CreateMaskLayer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'MaskLayerIndex',                       0, MAX_MASK_LAYER_INDEX - 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# DestroytMaskLayer. Destroy OpenVG mask layer. DestroyMaskLayer action is
# supported only in OpenVG 1.1.
AddActionConfig( 'DestroyMaskLayer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'MaskLayerIndex',                       0, MAX_MASK_LAYER_INDEX - 1 )
                   ]
                 )

################################################################################
# FillMaskLayer. Fill OpenVG mask layer. FillMaskLayer action is supported only
# in OpenVG 1.1.
AddActionConfig( 'FillMaskLayer',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'MaskLayerIndex',                       0, MAX_MASK_LAYER_INDEX - 1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   FloatAttribute(              'Value',                                0, 1 )
                   ]
                 )

################################################################################
# CopyMask. Copy OpenVG mask. CopyMask action is supported only in OpenVG 1.1.
AddActionConfig( 'CopyMask',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'MaskLayerIndex',                       0, MAX_MASK_LAYER_INDEX - 1 ),
                   IntAttribute(                'Dx',                                   0 ),
                   IntAttribute(                'Dy',                                   0 ),
                   IntAttribute(                'Sx',                                   0 ),
                   IntAttribute(                'Sy',                                   0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# Clear. OpenVG clear. Use VG_MAXINT for width and height if 0.
AddActionConfig( 'Clear',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                0 ),
                   IntAttribute(                'Height',                               0 )
                   ]
                 )

################################################################################
# CreatePathData. Create path data. Path elements are first added to the path
# data, and the path data is then added to the OpenVG path.
AddActionConfig( 'CreatePathData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathDataIndex',                        0, MAX_PATH_DATA_INDEX - 1 ),
                   EnumAttribute(               'Datatype', 'VGPathDatatype',           [ 'VG_PATH_DATATYPE_S_8',
                                                                                          'VG_PATH_DATATYPE_S_16',
                                                                                          'VG_PATH_DATATYPE_S_32',
                                                                                          'VG_PATH_DATATYPE_F' ] ),
                   IntAttribute(                'InitialSegments',                      1 ),
                   IntAttribute(                'InitialData',                          1 )
                   ]
                 )

################################################################################
# ClearPathData. Clear path data. Clearing removes all path elements from the
# path data.
AddActionConfig( 'ClearPathData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathDataIndex',                        0, MAX_PATH_DATA_INDEX - 1 )
                   ]
                 )

################################################################################
# DestroyPathData. Destroy path data.
AddActionConfig( 'DestroyPathData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathDataIndex',                        0, MAX_PATH_DATA_INDEX - 1 )
                   ]
                 )

################################################################################
# MoveTo. Add MoveTo element to path data.
AddActionConfig( 'MoveTo',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathDataIndex',                        0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleArrayAttribute(        'Point',                                2 ),
                   OnOffAttribute(              'Relative' )
                   ]
                 )

################################################################################
# Lines. Add one or more Line element(s) to path data. The length of Coordinates
# must be a multiple of two.
AddActionConfig( 'Lines',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathDataIndex',                        0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   OnOffAttribute(              'Relative' )
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 2 != 0',
                                    'Invalid line coordinate data' ) ]
                 )

################################################################################
# Arcs. Add one or more Arc element(s) to path data. The length of Coordinates
# must be a multiple of five.
AddActionConfig( 'Arcs',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathDataIndex',                        0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   AutoEnumAttribute(           'ArcType',                              [ 'SCCWARC',
                                                                                          'SCWARC',
                                                                                          'LCCWARC',
                                                                                          'LCWARC' ] ),
                   OnOffAttribute(              'Relative' )
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 5 != 0',
                                    'Invalid arc coordinate data' ) ]
                 )

################################################################################
# QuadraticBeziers. Add one or more QuadraticBezier element(s) to path data. The
# length of Coordinates must be a multiple of four.
AddActionConfig( 'QuadraticBeziers',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathDataIndex',                        0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   OnOffAttribute(              'Relative' )
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 4 != 0',
                                    'Invalid quadratic bezier coordinate data' ) ]

                 )

################################################################################
# CubicBeziers. Add one or more CubicBezier element(s) to path data. The length
# of Coordinates must be a multiple of six.
AddActionConfig( 'CubicBeziers',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathDataIndex',                        0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   OnOffAttribute(              'Relative' ),
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 6 != 0',
                                    'Invalid cubic bezier coordinate data' ) ]

                 )

################################################################################
# SQuadBeziers. Add one or more SQuadBezier element(s) to path data. The length
# of Coordinates must be a multiple of two.
AddActionConfig( 'SQuadBeziers',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathDataIndex',                        0, MAX_PATH_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   OnOffAttribute(              'Relative' )
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 2 != 0',
                                    'Invalid squad bezier coordinate data' ) ]
                 )

################################################################################
# SCubicBeziers. Add one or more SCubicBezier element(s) to path data. The
# length of Coordinates must be a multiple of four.
AddActionConfig( 'SCubicBeziers',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathDataIndex',                        0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   OnOffAttribute(              'Relative' )
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 4 != 0',
                                    'Invalid scubic bezier coordinate data' ) ]
                 )

################################################################################
# ClosePath. Add ClosePath element to path data.
AddActionConfig( 'ClosePath',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathDataIndex',                        0, MAX_PATH_INDEX - 1 ),
                   ]
                 )

################################################################################
# CreatePath. Create OpenVG path.
AddActionConfig( 'CreatePath',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   EnumAttribute(               'Datatype', 'VGPathDatatype',           [ 'VG_PATH_DATATYPE_S_8',
                                                                                          'VG_PATH_DATATYPE_S_16',
                                                                                          'VG_PATH_DATATYPE_S_32',
                                                                                          'VG_PATH_DATATYPE_F' ] ),
                   FloatAttribute(              'Scale' ),
                   FloatAttribute(              'Bias' ),
                   IntAttribute(                'SegmentCapacityHint',                  0 ),
                   IntAttribute(                'CoordCapacityHint',                    0 ),
                   EnumMaskAttribute(           'Capabilities', 'VGbitfield',           [ 'VG_PATH_CAPABILITY_APPEND_FROM',
                                                                                          'VG_PATH_CAPABILITY_APPEND_TO',
                                                                                          'VG_PATH_CAPABILITY_MODIFY',
                                                                                          'VG_PATH_CAPABILITY_TRANSFORM_FROM',
                                                                                          'VG_PATH_CAPABILITY_TRANSFORM_TO',
                                                                                          'VG_PATH_CAPABILITY_INTERPOLATE_FROM',
                                                                                          'VG_PATH_CAPABILITY_INTERPOLATE_TO',
                                                                                          'VG_PATH_CAPABILITY_PATH_LENGTH',
                                                                                          'VG_PATH_CAPABILITY_POINT_ALONG_PATH',
                                                                                          'VG_PATH_CAPABILITY_TANGENT_ALONG_PATH',
                                                                                          'VG_PATH_CAPABILITY_PATH_BOUNDS',
                                                                                          'VG_PATH_CAPABILITY_PATH_TRANSFORMED_BOUNDS',
                                                                                          'VG_PATH_CAPABILITY_ALL' ] )
                   ]
                 )

################################################################################
# ClearPath. Clear OpenVG path. 
AddActionConfig( 'ClearPath',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   EnumMaskAttribute(           'Capabilities', 'VGbitfield',           [ 'VG_PATH_CAPABILITY_APPEND_FROM',
                                                                                          'VG_PATH_CAPABILITY_APPEND_TO',
                                                                                          'VG_PATH_CAPABILITY_MODIFY',
                                                                                          'VG_PATH_CAPABILITY_TRANSFORM_FROM',
                                                                                          'VG_PATH_CAPABILITY_TRANSFORM_TO',
                                                                                          'VG_PATH_CAPABILITY_INTERPOLATE_FROM',
                                                                                          'VG_PATH_CAPABILITY_INTERPOLATE_TO',
                                                                                          'VG_PATH_CAPABILITY_PATH_LENGTH',
                                                                                          'VG_PATH_CAPABILITY_POINT_ALONG_PATH',
                                                                                          'VG_PATH_CAPABILITY_TANGENT_ALONG_PATH',
                                                                                          'VG_PATH_CAPABILITY_PATH_BOUNDS',
                                                                                          'VG_PATH_CAPABILITY_PATH_TRANSFORMED_BOUNDS',
                                                                                          'VG_PATH_CAPABILITY_ALL'] )
                   ]
                 )

################################################################################
# DestroyPath. Destroy OpenVG path.
AddActionConfig( 'DestroyPath',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 )
                   ]
                 )

################################################################################
# RemovePathCapabilities. Remove capabilities from OpenVG path.
AddActionConfig( 'RemovePathCapabilities',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   EnumMaskAttribute(           'Capabilities', 'VGbitfield',           [ 'VG_PATH_CAPABILITY_APPEND_FROM',
                                                                                          'VG_PATH_CAPABILITY_APPEND_TO',
                                                                                          'VG_PATH_CAPABILITY_MODIFY',
                                                                                          'VG_PATH_CAPABILITY_TRANSFORM_FROM',
                                                                                          'VG_PATH_CAPABILITY_TRANSFORM_TO',
                                                                                          'VG_PATH_CAPABILITY_INTERPOLATE_FROM',
                                                                                          'VG_PATH_CAPABILITY_INTERPOLATE_TO',
                                                                                          'VG_PATH_CAPABILITY_PATH_LENGTH',
                                                                                          'VG_PATH_CAPABILITY_POINT_ALONG_PATH',
                                                                                          'VG_PATH_CAPABILITY_TANGENT_ALONG_PATH',
                                                                                          'VG_PATH_CAPABILITY_PATH_BOUNDS',
                                                                                          'VG_PATH_CAPABILITY_PATH_TRANSFORMED_BOUNDS',
                                                                                          'VG_PATH_CAPABILITY_ALL' ] )
                   ]
                 )

################################################################################
# AppendPath. Append source OpenVG path to destination OpenVG path.
AddActionConfig( 'AppendPath',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DstPathIndex',                         0, MAX_PATH_INDEX - 1 ),
                   IntAttribute(                'SrcPathIndex',                         0, MAX_PATH_INDEX - 1 )
                   ]
                 )

################################################################################
# AppendPathData. Append path data to OpenVG path.
AddActionConfig( 'AppendPathData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   IntAttribute(                'PathDataIndex',                        0, MAX_PATH_DATA_INDEX - 1 ),
                   ]
                 )

################################################################################
# ModifyPathCoords. Modify coordinates in OpenVG path. Modify all segments
# starting from StartIndex if NumSegments equals zero.
AddActionConfig( 'ModifyPathCoords',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   IntAttribute(                'PathDataIndex',                        0, MAX_PATH_DATA_INDEX - 1 ),
                   IntAttribute(                'StartIndex',                           0 ),
                   IntAttribute(                'NumSegments',                          0 )
                   ]
                 )

################################################################################
# TransformPath. Transform OpenVG path using the current user-to-surface
# transformation.
AddActionConfig( 'TransformPath',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DstPathIndex',                         0, MAX_PATH_INDEX - 1 ),
                   IntAttribute(                'SrcPathIndex',                         0, MAX_PATH_INDEX - 1 )
                   ]
                 )

################################################################################
# InterpolatePath. Interpolate OpenVG paths.
AddActionConfig( 'InterpolatePath',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DstPathIndex',                         0, MAX_PATH_INDEX - 1 ),
                   IntAttribute(                'StartPathIndex',                       0, MAX_PATH_INDEX - 1 ),
                   IntAttribute(                'EndPathIndex',                         0, MAX_PATH_INDEX - 1 ),
                   FloatAttribute(              'Amount' ),
                   ]
                 )

################################################################################
# PathLength. Calculate OpenVG path length. Calculate the length of all segments
# if NumSegments equals zero; starting from StartSegment. Result is stored in
# the float register referred by LengthIndex.
AddActionConfig( 'PathLength',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   IntAttribute(                'StartSegment',                         0 ),
                   IntAttribute(                'NumSegments',                          0 ),
                   IntAttribute(                'LengthIndex',                          0, MAX_FLOAT_INDEX - 1 )
                   ]
                 )

################################################################################
# PointAlongPath. Calculate point along OpenVG path. Include all segments if
# NumSegments equals zero; starting from StartSegment. Results are stored in the
# float registers referred by XIndex, YIndex, TangentXIndex, and TangentYIndex.
AddActionConfig( 'PointAlongPath',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   IntAttribute(                'StartSegment',                         0 ),
                   IntAttribute(                'NumSegments',                          0 ),
                   FloatAttribute(              'Distance' ),
                   OnOffAttribute(              'Point' ),
                   OnOffAttribute(              'Tangent' ),
                   IntAttribute(                'XIndex',                               0, MAX_FLOAT_INDEX - 1 ),
                   IntAttribute(                'YIndex',                               0, MAX_FLOAT_INDEX - 1 ),
                   IntAttribute(                'TangentXIndex',                        0, MAX_FLOAT_INDEX - 1 ),
                   IntAttribute(                'TangentYIndex',                        0, MAX_FLOAT_INDEX - 1 )
                   ]
                 )

################################################################################
# PathBounds. Calculate OpenVG path bounds. Results are stored in the float
# registers referred by MinXIndex, MinYIndex, WidthIndex, and HeightIndex.
AddActionConfig( 'PathBounds',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   IntAttribute(                'MinXIndex',                            0, MAX_FLOAT_INDEX - 1 ),
                   IntAttribute(                'MinYIndex',                            0, MAX_FLOAT_INDEX - 1 ),
                   IntAttribute(                'WidthIndex',                           0, MAX_FLOAT_INDEX - 1 ),
                   IntAttribute(                'HeightIndex',                          0, MAX_FLOAT_INDEX - 1 )
                   ]
                 )

################################################################################
# PathTransformedBounds. Calculate OpenVG transformed path bounds. Results are
# stored in the float registers referred by MinXIndex, MinYIndex, WidthIndex,
# and HeightIndex.
AddActionConfig( 'PathTransformedBounds',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   IntAttribute(                'MinXIndex',                            0, MAX_FLOAT_INDEX - 1 ),
                   IntAttribute(                'MinYIndex',                            0, MAX_FLOAT_INDEX - 1 ),
                   IntAttribute(                'WidthIndex',                           0, MAX_FLOAT_INDEX - 1 ),
                   IntAttribute(                'HeightIndex',                          0, MAX_FLOAT_INDEX - 1 )
                   ]
                 )

################################################################################
# DrawPath. Draw OpenVG path.
AddActionConfig( 'DrawPath',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   EnumMaskAttribute(           'PaintModes', 'VGbitfield',             [ 'VG_STROKE_PATH',
                                                                                          'VG_FILL_PATH' ] )
                   ]
                 )

################################################################################
# MoveToDirect. Add MoveTo element directly to OpenVG path.
AddActionConfig( 'MoveToDirect',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleArrayAttribute(        'Point',                                2 ),
                   OnOffAttribute(              'Relative' )
                   ]
                 )

################################################################################
# LinesDirect. Add one or more Line element(s) directly to OpenVG path. The
# length of Coordinates must be a multiple of two.
AddActionConfig( 'LinesDirect',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   OnOffAttribute(              'Relative' )
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 2 != 0',
                                    'Invalid line coordinate data' ) ]
                 )

################################################################################
# ArcsDirect. Add one or more Arc element(s) directly to OpenVG path. The length
# of Coordinates must be a multiple of five.
AddActionConfig( 'ArcsDirect',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   AutoEnumAttribute(           'ArcType',                              [ 'SCCWARC',
                                                                                          'SCWARC',
                                                                                          'LCCWARC',
                                                                                          'LCWARC' ] ),
                   OnOffAttribute(              'Relative' )
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 5 != 0',
                                    'Invalid arc coordinate data' ) ]
                 )

################################################################################
# QuadraticBeziersDirect. Add one or more QuadraticBezier element(s) directly to
# OpenVG path. The length of Coordinates must be a multiple of four.
AddActionConfig( 'QuadraticBeziersDirect', 
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   OnOffAttribute(              'Relative' )
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 4 != 0',
                                    'Invalid quadratic bezier coordinate data' ) ]

                 )

################################################################################
# CubicBeziersDirect. Add one or more CubicBezier element(s) directly to OpenVG
# path. The length of Coordinates must be a multiple of six.
AddActionConfig( 'CubicBeziersDirect',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   OnOffAttribute(              'Relative' ),
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 6 != 0',
                                    'Invalid cubic bezier coordinate data' ) ]

                 )

################################################################################
# SQuadBeziersDirect. Add one or more SQuadBezier element(s) directly to OpenVG
# path. The length of Coordinates must be a multiple of four.
AddActionConfig( 'SQuadBeziersDirect',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   OnOffAttribute(              'Relative' )
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 2 != 0',
                                    'Invalid squad bezier coordinate data' ) ]
                 )

################################################################################
# SCubicBeziersDirect. Add one or more SCubicBezier element(s) directly to
# OpenVG path. The length of Coordinates must be a multiple of four.
AddActionConfig( 'SCubicBeziersDirect',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_DATA_INDEX - 1 ),
                   DoubleVectorAttribute(       'Coordinates' ),
                   OnOffAttribute(              'Relative' )
                   ],
                 [ ValidatorConfig( 'Coordinates == NULL || Coordinates->length % 4 != 0',
                                    'Invalid scubic bezier coordinate data' ) ]
                 )

################################################################################
# ClosePathDirect. Add ClosePath element directly to OpenVG path.
AddActionConfig( 'ClosePathDirect',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   ]
                 )

################################################################################
# CreatePaint. Create OpenVG paint.
AddActionConfig( 'CreatePaint',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 )
                   ]
                 )

################################################################################
# DestroyPaint. Destroy OpenVG paint.
AddActionConfig( 'DestroyPaint',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 )
                   ]
                 )

################################################################################
# SetPaint. Take OpenVG paint into use.
AddActionConfig( 'SetPaint',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   EnumMaskAttribute(           'PaintModes', 'VGbitfield',             [ 'VG_STROKE_PATH',
                                                                                          'VG_FILL_PATH' ] )
                   ]
                 )

################################################################################
# ColorPaint. Configure OpenVG paint as color paint using a single action.
AddActionConfig( 'ColorPaint',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   FloatArrayAttribute(         'Color',                                4, 0, 1 )
                   ]
                 )

################################################################################
# LinearGradientPaint. Configure OpenVG paint as linear gradient paint using a
# single action.
AddActionConfig( 'LinearGradientPaint',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   FloatArrayAttribute(         'Point0',                               2 ),
                   FloatArrayAttribute(         'Point1',                               2 )
                   ]
                 )

################################################################################
# RadialGradientPaint. Configure OpenVG paint as radial gradient paint using a
# single action.
AddActionConfig( 'RadialGradientPaint',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   FloatArrayAttribute(         'CenterPoint',                          2 ),
                   FloatArrayAttribute(         'FocalPoint',                           2 ),
                   FloatAttribute(              'Radius' )
                   ]
                 )

################################################################################
# PatternPaint. Configure OpenVG paint as pattern paint using a single action.
AddActionConfig( 'PatternPaint',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   IntAttribute(                'ImageIndex',                           0, MAX_INDEX_INDEX - 1 ),
                   EnumAttribute(               'TilingMode', 'VGTilingMode',           [ 'VG_TILE_FILL',
                                                                                          'VG_TILE_PAD',
                                                                                          'VG_TILE_REPEAT',
                                                                                          'VG_TILE_REFLECT' ] )
                   ]
                 )

################################################################################
# ColorRamps. Configure OpenVG paint color ramps using a single action.
AddActionConfig( 'ColorRamps',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   EnumAttribute(               'SpreadMode', 'VGColorRampSpreadMode',  [ 'VG_COLOR_RAMP_SPREAD_PAD',
                                                                                          'VG_COLOR_RAMP_SPREAD_REPEAT',
                                                                                          'VG_COLOR_RAMP_SPREAD_REFLECT' ] ),
                   OnOffAttribute(              'Premultiplied' ),
                   FloatVectorAttribute(        'Stops' )
                   ],
                 [ ValidatorConfig( 'Stops == NULL || Stops->length % 5 != 0',
                                    'Invalid color ramps stops data' ) ]
                 )

################################################################################
# PaintType. Set OpenVG paint type.
AddActionConfig( 'PaintType',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   EnumAttribute(               'PaintType', 'VGPaintType',             [ 'VG_PAINT_TYPE_COLOR',
                                                                                          'VG_PAINT_TYPE_LINEAR_GRADIENT',
                                                                                          'VG_PAINT_TYPE_RADIAL_GRADIENT',
                                                                                          'VG_PAINT_TYPE_PATTERN' ] )
                   ]
                 )

################################################################################
# PaintColor. Set OpenVG paint color parameter.
AddActionConfig( 'PaintColor',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   FloatArrayAttribute(         'Color',                                4, 0, 1 )
                   ]
                 )

################################################################################
# PaintLinearGradient. Set OpenVG paint linear gradient parameter.
AddActionConfig( 'PaintLinearGradient',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   FloatArrayAttribute(         'Point0',                               2 ),
                   FloatArrayAttribute(         'Point1',                               2 )
                   ]
                 )

################################################################################
# PaintRadialGradient. Set OpenVG paint radial gradient parameter.
AddActionConfig( 'PaintRadialGradient',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   FloatArrayAttribute(         'CenterPoint',                          2 ),
                   FloatArrayAttribute(         'FocalPoint',                           2 ),
                   FloatAttribute(              'Radius' )
                   ]
                 )

################################################################################
# PaintColorRampSpreadMode. Set OpenVG paint color ramp spread mode parameter.
AddActionConfig( 'PaintColorRampSpreadMode',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   EnumAttribute(               'SpreadMode', 'VGColorRampSpreadMode',  [ 'VG_COLOR_RAMP_SPREAD_PAD',
                                                                                          'VG_COLOR_RAMP_SPREAD_REPEAT',
                                                                                          'VG_COLOR_RAMP_SPREAD_REFLECT' ] )
                   ]
                 )

################################################################################
# PaintColorRampStops. Set OpenVG paint color ramp stop parameters.
AddActionConfig( 'PaintColorRampStops',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   FloatVectorAttribute(        'Stops' )
                   ],
                 [ ValidatorConfig( 'Stops == NULL || Stops->length % 5 != 0',
                                    'Invalid color ramps stops data' ) ]
                 )

################################################################################
# PaintColorRampPremultiplied. Set OpenVG paint color ramp premultiplied
# parameter
AddActionConfig( 'PaintColorRampPremultiplied',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   OnOffAttribute(              'Premultiplied' )
                   ]
                 )

################################################################################
# PaintPattern. Set OpenVG paint pattern.
AddActionConfig( 'PaintPattern',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   IntAttribute(                'ImageIndex',                           0, MAX_INDEX_INDEX - 1 ),
                   ]
                 )

################################################################################
# PaintPatternTilingMode. Set OpenVG paint patter tiling mode.
AddActionConfig( 'PaintPatternTilingMode',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'PaintIndex',                           0, MAX_PAINT_INDEX - 1 ),
                   EnumAttribute(               'TilingMode', 'VGTilingMode',           [ 'VG_TILE_FILL',
                                                                                          'VG_TILE_PAD',
                                                                                          'VG_TILE_REPEAT',
                                                                                          'VG_TILE_REFLECT' ] )
                   ]
                 )

######################################################################
# CreateData. Create empty data with the given length to the specified data
# index. The action fails if the data index is already in use and the existing
# data does not have exactly the specified length. 
AddActionConfig( 'CreateData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'Length',                               1 )
                   ]
                 )

######################################################################
# LoadData. Load data from the file specified by the file name to the data
# specified by the data index. Creates a new data with the length matching that
# of the file. If the data already exists in the data index, it is reused if the
# length of the existing data and the length of the file match
# exactly. Otherwise, the action reports an error.
AddActionConfig( 'LoadData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   StringAttribute(             'Filename',                             MAX_FILENAME_LENGTH )
                   ]
                 )

######################################################################
# Decode. Decode data stored into the source data index. The source data must
# follow the file format used by the selected encoder. The destination data
# index must be free, or the data length must be exactly the same as the data
# payload in the data.
AddActionConfig( 'Decode',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   AutoEnumAttribute(           'Encoder',                              [ 'PVRTEXTOOL3' ] ),
                   ]
                 )

######################################################################
# DecodeTarga. Decode targa data (24bpp RGB, 32bpp RGBA). The destination data
# index must be free, or the data length must be exactly the same as the data
# payload in the targa data.
AddActionConfig( 'DecodeTarga',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'SrcDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   IntAttribute(                'DstDataIndex',                         0, MAX_DATA_INDEX - 1 ),
                   ]
                 )

######################################################################
# DestroyData. Destroy the data specified by the data index, and set the the
# data index as free.
AddActionConfig( 'DestroyData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 )
                   ]
                 )

################################################################################
# CreateImageData. Create image data from data. Image data index must be
# unused. Data index must point to a valid data with size matching that of the
# resulting image data.
AddActionConfig( 'CreateImageData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ImageDataIndex',                       0, MAX_IMAGE_DATA_INDEX - 1 ),
                   IntAttribute(                'DataIndex',                            0, MAX_DATA_INDEX - 1 ),
                   EnumAttribute(               'Format', 'VGImageFormat',              [ 'VG_sRGBX_8888',
                                                                                          'VG_sRGBA_8888',
                                                                                          'VG_sRGBA_8888_PRE',
                                                                                          'VG_sRGB_565',
                                                                                          'VG_sRGBA_5551',
                                                                                          'VG_sRGBA_4444',
                                                                                          'VG_sL_8',
                                                                                          'VG_lRGBX_8888',
                                                                                          'VG_lRGBA_8888',
                                                                                          'VG_lRGBA_8888_PRE',
                                                                                          'VG_lL_8',
                                                                                          'VG_A_8',
                                                                                          'VG_BW_1',
                                                                                          'VG_A_1',
                                                                                          'VG_A_4',
                                                                                          'VG_sXRGB_8888',
                                                                                          'VG_sARGB_8888',
                                                                                          'VG_sARGB_8888_PRE',
                                                                                          'VG_sARGB_1555',
                                                                                          'VG_sARGB_4444',
                                                                                          'VG_lXRGB_8888',
                                                                                          'VG_lARGB_8888',
                                                                                          'VG_lARGB_8888_PRE',
                                                                                          'VG_sBGRX_8888',
                                                                                          'VG_sBGRA_8888',
                                                                                          'VG_sBGRA_8888_PRE',
                                                                                          'VG_sBGR_565',
                                                                                          'VG_sBGRA_5551',
                                                                                          'VG_sBGRA_4444',
                                                                                          'VG_lBGRX_8888',
                                                                                          'VG_lBGRA_8888',
                                                                                          'VG_lBGRA_8888_PRE',
                                                                                          'VG_sXBGR_8888',
                                                                                          'VG_sABGR_8888',
                                                                                          'VG_sABGR_8888_PRE',
                                                                                          'VG_sABGR_1555',
                                                                                          'VG_sABGR_4444',
                                                                                          'VG_lXBGR_8888',
                                                                                          'VG_lABGR_8888',
                                                                                          'VG_lABGR_8888_PRE' ] ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# CreateNullImageData. Create an image data filled with zeroes. Image data index
# must be unused.
AddActionConfig( 'CreateNullImageData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ImageDataIndex',                       0, MAX_IMAGE_DATA_INDEX - 1 ),
                   EnumAttribute(               'Format', 'VGImageFormat',              [ 'VG_sRGBX_8888',
                                                                                          'VG_sRGBA_8888',
                                                                                          'VG_sRGBA_8888_PRE',
                                                                                          'VG_sRGB_565',
                                                                                          'VG_sRGBA_5551',
                                                                                          'VG_sRGBA_4444',
                                                                                          'VG_sL_8',
                                                                                          'VG_lRGBX_8888',
                                                                                          'VG_lRGBA_8888',
                                                                                          'VG_lRGBA_8888_PRE',
                                                                                          'VG_lL_8',
                                                                                          'VG_A_8',
                                                                                          'VG_BW_1',
                                                                                          'VG_A_1',
                                                                                          'VG_A_4',
                                                                                          'VG_sXRGB_8888',
                                                                                          'VG_sARGB_8888',
                                                                                          'VG_sARGB_8888_PRE',
                                                                                          'VG_sARGB_1555',
                                                                                          'VG_sARGB_4444',
                                                                                          'VG_lXRGB_8888',
                                                                                          'VG_lARGB_8888',
                                                                                          'VG_lARGB_8888_PRE',
                                                                                          'VG_sBGRX_8888',
                                                                                          'VG_sBGRA_8888',
                                                                                          'VG_sBGRA_8888_PRE',
                                                                                          'VG_sBGR_565',
                                                                                          'VG_sBGRA_5551',
                                                                                          'VG_sBGRA_4444',
                                                                                          'VG_lBGRX_8888',
                                                                                          'VG_lBGRA_8888',
                                                                                          'VG_lBGRA_8888_PRE',
                                                                                          'VG_sXBGR_8888',
                                                                                          'VG_sABGR_8888',
                                                                                          'VG_sABGR_8888_PRE',
                                                                                          'VG_sABGR_1555',
                                                                                          'VG_sABGR_4444',
                                                                                          'VG_lXBGR_8888',
                                                                                          'VG_lABGR_8888',
                                                                                          'VG_lABGR_8888_PRE' ] ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

######################################################################
# CreateSolidImageData. Create solid color image data to the specified image
# data index. Image data index must be unused.
AddActionConfig( 'CreateSolidImageData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ImageDataIndex',                       0, MAX_IMAGE_DATA_INDEX - 1 ),
                   FloatArrayAttribute(         'Color',                                4, 0, 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   EnumAttribute(               'Format', 'VGImageFormat',              [ 'VG_sRGBX_8888',
                                                                                          'VG_sRGBA_8888',
                                                                                          'VG_sRGBA_8888_PRE',
                                                                                          'VG_sRGB_565',
                                                                                          'VG_sRGBA_5551',
                                                                                          'VG_sRGBA_4444',
                                                                                          'VG_sL_8',
                                                                                          'VG_lRGBX_8888',
                                                                                          'VG_lRGBA_8888',
                                                                                          'VG_lRGBA_8888_PRE',
                                                                                          'VG_lL_8',
                                                                                          'VG_A_8',
                                                                                          'VG_BW_1',
                                                                                          'VG_A_1',
                                                                                          'VG_A_4',
                                                                                          'VG_sXRGB_8888',
                                                                                          'VG_sARGB_8888',
                                                                                          'VG_sARGB_8888_PRE',
                                                                                          'VG_sARGB_1555',
                                                                                          'VG_sARGB_4444',
                                                                                          'VG_lXRGB_8888',
                                                                                          'VG_lARGB_8888',
                                                                                          'VG_lARGB_8888_PRE',
                                                                                          'VG_sBGRX_8888',
                                                                                          'VG_sBGRA_8888',
                                                                                          'VG_sBGRA_8888_PRE',
                                                                                          'VG_sBGR_565',
                                                                                          'VG_sBGRA_5551',
                                                                                          'VG_sBGRA_4444',
                                                                                          'VG_lBGRX_8888',
                                                                                          'VG_lBGRA_8888',
                                                                                          'VG_lBGRA_8888_PRE',
                                                                                          'VG_sXBGR_8888',
                                                                                          'VG_sABGR_8888',
                                                                                          'VG_sABGR_8888_PRE',
                                                                                          'VG_sABGR_1555',
                                                                                          'VG_sABGR_4444',
                                                                                          'VG_lXBGR_8888',
                                                                                          'VG_lABGR_8888',
                                                                                          'VG_lABGR_8888_PRE' ] ),
                   ]
                 )

######################################################################
# CreateBarImageData. Create image data with a vertical bar pattern to the
# specified image data index. Image data index must be unused.
AddActionConfig( 'CreateBarImageData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ImageDataIndex',                       0, MAX_IMAGE_DATA_INDEX - 1 ),
                   FloatArrayAttribute(         'Color1',                               4, 0, 1 ),
                   FloatArrayAttribute(         'Color2',                               4, 0, 1 ),
                   IntAttribute(                'Bars',                                 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   EnumAttribute(               'Format', 'VGImageFormat',              [ 'VG_sRGBX_8888',
                                                                                          'VG_sRGBA_8888',
                                                                                          'VG_sRGBA_8888_PRE',
                                                                                          'VG_sRGB_565',
                                                                                          'VG_sRGBA_5551',
                                                                                          'VG_sRGBA_4444',
                                                                                          'VG_sL_8',
                                                                                          'VG_lRGBX_8888',
                                                                                          'VG_lRGBA_8888',
                                                                                          'VG_lRGBA_8888_PRE',
                                                                                          'VG_lL_8',
                                                                                          'VG_A_8',
                                                                                          'VG_BW_1',
                                                                                          'VG_A_1',
                                                                                          'VG_A_4',
                                                                                          'VG_sXRGB_8888',
                                                                                          'VG_sARGB_8888',
                                                                                          'VG_sARGB_8888_PRE',
                                                                                          'VG_sARGB_1555',
                                                                                          'VG_sARGB_4444',
                                                                                          'VG_lXRGB_8888',
                                                                                          'VG_lARGB_8888',
                                                                                          'VG_lARGB_8888_PRE',
                                                                                          'VG_sBGRX_8888',
                                                                                          'VG_sBGRA_8888',
                                                                                          'VG_sBGRA_8888_PRE',
                                                                                          'VG_sBGR_565',
                                                                                          'VG_sBGRA_5551',
                                                                                          'VG_sBGRA_4444',
                                                                                          'VG_lBGRX_8888',
                                                                                          'VG_lBGRA_8888',
                                                                                          'VG_lBGRA_8888_PRE',
                                                                                          'VG_sXBGR_8888',
                                                                                          'VG_sABGR_8888',
                                                                                          'VG_sABGR_8888_PRE',
                                                                                          'VG_sABGR_1555',
                                                                                          'VG_sABGR_4444',
                                                                                          'VG_lXBGR_8888',
                                                                                          'VG_lABGR_8888',
                                                                                          'VG_lABGR_8888_PRE' ] ),
                   ]
                 )

######################################################################
# CreateCheckerImageData. Create image data with a checker pattern to the
# specified image data index. Image data index must be unused.
AddActionConfig( 'CreateCheckerImageData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ImageDataIndex',                       0, MAX_IMAGE_DATA_INDEX - 1 ),
                   FloatArrayAttribute(         'Color1',                               4, 0, 1 ),
                   FloatArrayAttribute(         'Color2',                               4, 0, 1 ),
                   IntAttribute(                'CheckersX',                            1 ),
                   IntAttribute(                'CheckersY',                            1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   EnumAttribute(               'Format', 'VGImageFormat',              [ 'VG_sRGBX_8888',
                                                                                          'VG_sRGBA_8888',
                                                                                          'VG_sRGBA_8888_PRE',
                                                                                          'VG_sRGB_565',
                                                                                          'VG_sRGBA_5551',
                                                                                          'VG_sRGBA_4444',
                                                                                          'VG_sL_8',
                                                                                          'VG_lRGBX_8888',
                                                                                          'VG_lRGBA_8888',
                                                                                          'VG_lRGBA_8888_PRE',
                                                                                          'VG_lL_8',
                                                                                          'VG_A_8',
                                                                                          'VG_BW_1',
                                                                                          'VG_A_1',
                                                                                          'VG_A_4',
                                                                                          'VG_sXRGB_8888',
                                                                                          'VG_sARGB_8888',
                                                                                          'VG_sARGB_8888_PRE',
                                                                                          'VG_sARGB_1555',
                                                                                          'VG_sARGB_4444',
                                                                                          'VG_lXRGB_8888',
                                                                                          'VG_lARGB_8888',
                                                                                          'VG_lARGB_8888_PRE',
                                                                                          'VG_sBGRX_8888',
                                                                                          'VG_sBGRA_8888',
                                                                                          'VG_sBGRA_8888_PRE',
                                                                                          'VG_sBGR_565',
                                                                                          'VG_sBGRA_5551',
                                                                                          'VG_sBGRA_4444',
                                                                                          'VG_lBGRX_8888',
                                                                                          'VG_lBGRA_8888',
                                                                                          'VG_lBGRA_8888_PRE',
                                                                                          'VG_sXBGR_8888',
                                                                                          'VG_sABGR_8888',
                                                                                          'VG_sABGR_8888_PRE',
                                                                                          'VG_sABGR_1555',
                                                                                          'VG_sABGR_4444',
                                                                                          'VG_lXBGR_8888',
                                                                                          'VG_lABGR_8888',
                                                                                          'VG_lABGR_8888_PRE' ] ),
                   ]
                 )

######################################################################
# CreateGradientImageData. Create image data with a vertical (up-down),
# horizontal (left-right) or diagonal (up-left-low-right) gradient to the
# specified image data index. Gradient directions are described as the data is
# laid out in memory. Image data index must be unused.
AddActionConfig( 'CreateGradientImageData',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ImageDataIndex',                       0, MAX_IMAGE_DATA_INDEX - 1 ), 
                   EnumAttribute(               'Gradient', 'SCTOpenVGGradient',        [ 'OPENVG_GRADIENT_VERTICAL',
                                                                                          'OPENVG_GRADIENT_HORIZONTAL',
                                                                                          'OPENVG_GRADIENT_DIAGONAL' ] ),
                   FloatArrayAttribute(         'Color1',                               4, 0, 1 ),
                   FloatArrayAttribute(         'Color2',                               4, 0, 1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   EnumAttribute(               'Format', 'VGImageFormat',              [ 'VG_sRGBX_8888',
                                                                                          'VG_sRGBA_8888',
                                                                                          'VG_sRGBA_8888_PRE',
                                                                                          'VG_sRGB_565',
                                                                                          'VG_sRGBA_5551',
                                                                                          'VG_sRGBA_4444',
                                                                                          'VG_sL_8',
                                                                                          'VG_lRGBX_8888',
                                                                                          'VG_lRGBA_8888',
                                                                                          'VG_lRGBA_8888_PRE',
                                                                                          'VG_lL_8',
                                                                                          'VG_A_8',
                                                                                          'VG_BW_1',
                                                                                          'VG_A_1',
                                                                                          'VG_A_4',
                                                                                          'VG_sXRGB_8888',
                                                                                          'VG_sARGB_8888',
                                                                                          'VG_sARGB_8888_PRE',
                                                                                          'VG_sARGB_1555',
                                                                                          'VG_sARGB_4444',
                                                                                          'VG_lXRGB_8888',
                                                                                          'VG_lARGB_8888',
                                                                                          'VG_lARGB_8888_PRE',
                                                                                          'VG_sBGRX_8888',
                                                                                          'VG_sBGRA_8888',
                                                                                          'VG_sBGRA_8888_PRE',
                                                                                          'VG_sBGR_565',
                                                                                          'VG_sBGRA_5551',
                                                                                          'VG_sBGRA_4444',
                                                                                          'VG_lBGRX_8888',
                                                                                          'VG_lBGRA_8888',
                                                                                          'VG_lBGRA_8888_PRE',
                                                                                          'VG_sXBGR_8888',
                                                                                          'VG_sABGR_8888',
                                                                                          'VG_sABGR_8888_PRE',
                                                                                          'VG_sABGR_1555',
                                                                                          'VG_sABGR_4444',
                                                                                          'VG_lXBGR_8888',
                                                                                          'VG_lABGR_8888',
                                                                                          'VG_lABGR_8888_PRE' ] ),
                   ]
                 )

################################################################################
# DestroyImageData. Destroy image data.
AddActionConfig( 'DestroyImageData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ImageDataIndex',                       0, MAX_IMAGE_DATA_INDEX - 1 )
                   ]
                 )

################################################################################
# CreateImage. Create OpenVG image.
AddActionConfig( 'CreateImage',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ImageIndex',                           0, MAX_INDEX_INDEX - 1 ),
                   EnumAttribute(               'Format', 'VGImageFormat',              [ 'VG_sRGBX_8888',
                                                                                          'VG_sRGBA_8888',
                                                                                          'VG_sRGBA_8888_PRE',
                                                                                          'VG_sRGB_565',
                                                                                          'VG_sRGBA_5551',
                                                                                          'VG_sRGBA_4444',
                                                                                          'VG_sL_8',
                                                                                          'VG_lRGBX_8888',
                                                                                          'VG_lRGBA_8888',
                                                                                          'VG_lRGBA_8888_PRE',
                                                                                          'VG_lL_8',
                                                                                          'VG_A_8',
                                                                                          'VG_BW_1',
                                                                                          'VG_A_1',
                                                                                          'VG_A_4',
                                                                                          'VG_sXRGB_8888',
                                                                                          'VG_sARGB_8888',
                                                                                          'VG_sARGB_8888_PRE',
                                                                                          'VG_sARGB_1555',
                                                                                          'VG_sARGB_4444',
                                                                                          'VG_lXRGB_8888',
                                                                                          'VG_lARGB_8888',
                                                                                          'VG_lARGB_8888_PRE',
                                                                                          'VG_sBGRX_8888',
                                                                                          'VG_sBGRA_8888',
                                                                                          'VG_sBGRA_8888_PRE',
                                                                                          'VG_sBGR_565',
                                                                                          'VG_sBGRA_5551',
                                                                                          'VG_sBGRA_4444',
                                                                                          'VG_lBGRX_8888',
                                                                                          'VG_lBGRA_8888',
                                                                                          'VG_lBGRA_8888_PRE',
                                                                                          'VG_sXBGR_8888',
                                                                                          'VG_sABGR_8888',
                                                                                          'VG_sABGR_8888_PRE',
                                                                                          'VG_sABGR_1555',
                                                                                          'VG_sABGR_4444',
                                                                                          'VG_lXBGR_8888',
                                                                                          'VG_lABGR_8888',
                                                                                          'VG_lABGR_8888_PRE' ] ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   EnumMaskAttribute(           'AllowedQuality', 'VGbitfield',         [ 'VG_IMAGE_QUALITY_NONANTIALIASED',
                                                                                          'VG_IMAGE_QUALITY_FASTER',
                                                                                          'VG_IMAGE_QUALITY_BETTER' ] )
                   ]
                 )

################################################################################
# DestroyImage. Destroy OpenVG image.
AddActionConfig( 'DestroyImage',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ImageIndex',                           0, MAX_INDEX_INDEX - 1 )
                   ]
                 )

################################################################################
# ClearImage. Clear OpenVG image. 
AddActionConfig( 'ClearImage',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ImageIndex',                           0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# ImageSubData. Set OpenVG image subdata from image data.
AddActionConfig( 'ImageSubData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'SrcImageDataIndex',                    0, MAX_IMAGE_DATA_INDEX - 1 ),
                   IntAttribute(                'DstImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# GetImageSubData. Get OpenVG image subdata to image data.
AddActionConfig( 'GetImageSubData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'SrcImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'DstImageDataIndex',                    0, MAX_IMAGE_DATA_INDEX - 1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# ChildImage. Create OpenVG child image.
AddActionConfig( 'ChildImage',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ImageIndex',                           0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'ParentImageIndex',                     0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'X',                                    0 ),
                   IntAttribute(                'Y',                                    0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   ]
                 )

################################################################################
# CopyImage. Copy pixels from one OpenVG image to another.
AddActionConfig( 'CopyImage',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DstImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'Dx',                                   0 ),
                   IntAttribute(                'Dy',                                   0 ),
                   IntAttribute(                'SrcImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'Sx',                                   0 ),
                   IntAttribute(                'Sy',                                   0 ),                   
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 ),
                   OnOffAttribute(              'Dither' )
                   ]
                 )

################################################################################
# ImageMode. Set OpenVG image mode.
AddActionConfig( 'ImageMode',
                 'Execute'+'Destroy',
                 [ EnumAttribute(               'Mode', 'VGImageMode',                  [ 'VG_DRAW_IMAGE_NORMAL',
                                                                                          'VG_DRAW_IMAGE_MULTIPLY',
                                                                                          'VG_DRAW_IMAGE_STENCIL' ] )
                   ]
                 )

################################################################################
# DrawImage. Draw OpenVG image onto the drawing surface. 
AddActionConfig( 'DrawImage',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ImageIndex',                           0, MAX_INDEX_INDEX - 1 )
                   ]
                 )

################################################################################
# SetPixels. Copy pixel data from OpenVG image onto the drawing surface.
AddActionConfig( 'SetPixels',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Dx',                                   0 ),
                   IntAttribute(                'Dy',                                   0 ),
                   IntAttribute(                'ImageIndex',                           0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'Sx',                                   0 ),
                   IntAttribute(                'Sy',                                   0 ),                   
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# WritePixels. Copy pixel data from image data onto the drawing surface.
AddActionConfig( 'WritePixels',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ImageDataIndex',                       0, MAX_IMAGE_DATA_INDEX - 1 ),
                   IntAttribute(                'Dx',                                   0 ),
                   IntAttribute(                'Dy',                                   0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# GetPixels. Copy pixels from the drawing surface to OpenVG image.
AddActionConfig( 'GetPixels',
                 'Execute'+'Destroy',            
                 [ IntAttribute(                'ImageIndex',                           0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'Dx',                                   0 ),
                   IntAttribute(                'Dy',                                   0 ),
                   IntAttribute(                'Sx',                                   0 ),
                   IntAttribute(                'Sy',                                   0 ),                   
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# ReadPixels. Copy pixels from the drawing surface to image data.
AddActionConfig( 'ReadPixels',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'ImageDataIndex',                       0, MAX_IMAGE_DATA_INDEX - 1 ),
                   IntAttribute(                'Sx',                                   0 ),
                   IntAttribute(                'Sy',                                   0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# CopyPixels. Copy pixels from one drawing surface location onto another.
AddActionConfig( 'CopyPixels',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Dx',                                   0 ),
                   IntAttribute(                'Dy',                                   0 ),
                   IntAttribute(                'Sx',                                   0 ),
                   IntAttribute(                'Sy',                                   0 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# CreateFont. Create OpenVG font. Supported only in OpenVG 1.1.
AddActionConfig( 'CreateFont',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FontIndex',                            0, MAX_FONT_INDEX - 1 ),
                   IntAttribute(                'GlyphCapacityHint',                    0 )
                   ]
                 )

################################################################################
# DestroyFont. Destroy OpenVG font. Supported only in OpenVG 1.1.
AddActionConfig( 'DestroyFont',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FontIndex',                            0, MAX_FONT_INDEX - 1 )
                   ]
                 )

################################################################################
# SetGlyphToPath. Set glyph (OpenVG path) to OpenVG font. Supported only in
# OpenVG 1.1.
AddActionConfig( 'SetGlyphToPath',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FontIndex',                            0, MAX_FONT_INDEX - 1 ),
                   IntAttribute(                'GlyphIndex',                           0 ),
                   IntAttribute(                'PathIndex',                            0, MAX_PATH_INDEX - 1 ),
                   OnOffAttribute(              'IsHinted' ),
                   FloatArrayAttribute(         'GlyphOrigin',                          2 ),
                   FloatArrayAttribute(         'Escapement',                           2 )
                   ]
                 )

################################################################################
# SetGlyphToImage. Set glyph (OpenVG image) to OpenVG font. Supported only in
# OpenVG 1.1.
AddActionConfig( 'SetGlyphToImage',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FontIndex',                            0, MAX_FONT_INDEX - 1 ),
                   IntAttribute(                'GlyphIndex',                           0 ),
                   IntAttribute(                'ImageIndex',                           0, MAX_INDEX_INDEX - 1 ),
                   FloatArrayAttribute(         'GlyphOrigin',                          2 ),
                   FloatArrayAttribute(         'Escapement',                           2 )
                   ]
                )

################################################################################
# ClearGlypgh. Clear glyph, i.e. remove glyph from OpenVG font. Supported only
# in OpenVG 1.1.
AddActionConfig( 'ClearGlyph',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FontIndex',                            0, MAX_FONT_INDEX - 1 ),
                   IntAttribute(                'GlyphIndex',                           0 )
                   ]
                 )

################################################################################
# DrawGlyph. Draw glyph from OpenVG font. Supported only in OpenVG 1.1.
AddActionConfig( 'DrawGlyph',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FontIndex',                            0, MAX_FONT_INDEX - 1 ),
                   IntAttribute(                'GlyphIndex',                           0 ),
                   EnumMaskAttribute(           'PaintModes', 'VGbitfield',             [ 'VG_STROKE_PATH',
                                                                                          'VG_FILL_PATH' ] ),
                   OnOffAttribute(              'AllowAutoHinting' )
                   ]
                 )

################################################################################
# DrawGlyphs. Draw glyphs from OpenVG font. Supported only in OpenVG 1.1.
AddActionConfig( 'DrawGlyphs',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'FontIndex',                            0, MAX_FONT_INDEX - 1 ),
                   IntAttribute(                'GlyphCount',                           1 ),
                   IntVectorAttribute(          'GlyphIndices' ),
                   FloatVectorAttribute(        'AdjustmentsX' ),
                   FloatVectorAttribute(        'AdjustmentsY' ),
                   EnumMaskAttribute(           'PaintModes', 'VGbitfield',             [ 'VG_STROKE_PATH',
                                                                                          'VG_FILL_PATH' ] ),
                   OnOffAttribute(              'AllowAutoHinting' )
                   ]
                 )

################################################################################
# ColorMatrix. Do color matrix transformation to the pixel data in the OpenVG
# image.
AddActionConfig( 'ColorMatrix',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DstImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'SrcImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   FloatArrayAttribute(         'Matrix',                               20 )
                   ]
                 )

################################################################################
# Convolve. Do convolution transformation to the pixel data in the OpenVG image.
AddActionConfig( 'Convolve',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DstImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'SrcImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'ShiftX' ),
                   IntAttribute(                'ShiftY' ),
                   IntVectorAttribute(          'Kernel' ),
                   IntAttribute(                'KernelWidth',                          1 ),
                   IntAttribute(                'KernelHeight',                         1 ),
                   FloatAttribute(              'Scale' ),
                   FloatAttribute(              'Bias' ),
                   EnumAttribute(               'TilingMode', 'VGTilingMode',           [ 'VG_TILE_FILL',
                                                                                          'VG_TILE_PAD',
                                                                                          'VG_TILE_REPEAT',
                                                                                          'VG_TILE_REFLECT' ] )
                   ],
                 [ ValidatorConfig( 'Kernel != NULL && Kernel->length % ( KernelWidth * KernelHeight ) != 0',
                                    'Invalid kernel data' ) ]
                 )

################################################################################
# SeparableConvolve. Do separable convolution transformation to the pixel data
# in the OpenVG image.
AddActionConfig( 'SeparableConvolve',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'DstImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'SrcImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'ShiftX' ),
                   IntAttribute(                'ShiftY' ),
                   IntVectorAttribute(          'KernelX' ),
                   IntVectorAttribute(          'KernelY' ),
                   IntAttribute(                'KernelWidth',                          1 ),
                   IntAttribute(                'KernelHeight',                         1 ),
                   FloatAttribute(              'Scale' ),
                   FloatAttribute(              'Bias' ),
                   EnumAttribute(               'TilingMode', 'VGTilingMode',           [ 'VG_TILE_FILL',
                                                                                          'VG_TILE_PAD',
                                                                                          'VG_TILE_REPEAT',
                                                                                          'VG_TILE_REFLECT' ] )
                   ],
                 [ ValidatorConfig( 'KernelX != NULL && KernelX->length % ( KernelWidth * KernelHeight ) != 0',
                                    'Invalid kernel x data' ),
                   ValidatorConfig( 'KernelY != NULL && KernelY->length % ( KernelWidth * KernelHeight ) != 0',
                                    'Invalid kernel y data' ) ]
                 )

################################################################################
# GaussianBlur. Do gaussian blur transformation to the pixel data in the OpenVG
# image.
AddActionConfig( 'GaussianBlur',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DstImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'SrcImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   FloatAttribute(              'StdDeviationX' ),
                   FloatAttribute(              'StdDeviationY' ),
                   EnumAttribute(               'TilingMode', 'VGTilingMode',           [ 'VG_TILE_FILL',
                                                                                          'VG_TILE_PAD',
                                                                                          'VG_TILE_REPEAT',
                                                                                          'VG_TILE_REFLECT' ] )
                   ]
                 )

################################################################################
# CreateLut. Create a lookup table.
AddActionConfig( 'CreateLut',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'LutIndex',                             0, MAX_LOOKUP_TABLE_INDEX - 1 ),
                   EnumAttribute(               'Type', 'SCTOpenVGLUTType',             [ 'SCT_OPENVG_LUT_UBYTE8',
                                                                                          'SCT_OPENVG_LUT_UINT32' ] )
                   ]
                 )

################################################################################
# LutData. Set a lookup table data.
AddActionConfig( 'LutData',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'LutIndex',                             0, MAX_LOOKUP_TABLE_INDEX - 1 ),
                   Hex32ArrayAttribute(         'Data',                                 256 )
                   ]
                 )

################################################################################
# DestroyLut. Destroy a lookup table.
AddActionConfig( 'DestroyLut',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'LutIndex',                             0, MAX_LOOKUP_TABLE_INDEX - 1 )
                   ]
                 )

################################################################################
# Lookup. Do a color lookup transformation for every OpenVG image pixel channel.
AddActionConfig( 'Lookup',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DstImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'SrcImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'RedLutIndex',                          0, MAX_LOOKUP_TABLE_INDEX - 1 ),
                   IntAttribute(                'GreenLutIndex',                        0, MAX_LOOKUP_TABLE_INDEX - 1 ),
                   IntAttribute(                'BlueLutIndex',                         0, MAX_LOOKUP_TABLE_INDEX - 1 ),
                   IntAttribute(                'AlphaLutIndex',                        0, MAX_LOOKUP_TABLE_INDEX - 1 ), 
                   OnOffAttribute(              'OutputLinear' ),
                   OnOffAttribute(              'OutputPremultiplied' )
                   ]
                 )

################################################################################
# LookupSingle. Do a color lookup for OpenVG image.
AddActionConfig( 'LookupSingle',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'DstImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'SrcImageIndex',                        0, MAX_INDEX_INDEX - 1 ),
                   EnumAttribute(               'SourceChannel', 'VGImageChannel',      [ 'VG_RED',
                                                                                          'VG_GREEN',
                                                                                          'VG_BLUE' ] ),
                   IntAttribute(                'LutIndex',                             0, MAX_LOOKUP_TABLE_INDEX - 1 ),
                   OnOffAttribute(              'OutputLinear' ),
                   OnOffAttribute(              'OutputPremultiplied' )
                   ]
                 )

################################################################################
# ColorTransform. Set a color transform. Supported only in OpenVG 1.1.
AddActionConfig( 'ColorTransform',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ FloatArrayAttribute(         'Matrix',                               8 )
                   ]
                 )

################################################################################
# CreateTiger. Create tiger path. Tiger path is taken from the OpenVG reference
# engine sample code.
AddActionConfig( 'CreateTiger',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TigerIndex',                           0, MAX_TIGER_INDEX - 1 ),
                   ] 
                 )

################################################################################
# DrawTiger. Draw tiger path.
#
# NOTE: drawing the tiger modifues the following OpenVG state: fill rule, paint
# mode, cap style, join style, miter limit, stroke width, fill paint, stroke
# paint, and path user to surface transformation.
AddActionConfig( 'DrawTiger',
                 'Execute'+'Destroy'+'GetWorkload',
                 [ IntAttribute(                'TigerIndex',                           0, MAX_TIGER_INDEX - 1 ),
                   IntAttribute(                'Iterations',                           1 ),
                   IntAttribute(                'Width',                                1 ),
                   IntAttribute(                'Height',                               1 )
                   ]
                 )

################################################################################
# DestroyTiger. Destroy a tiger path.
AddActionConfig( 'DestroyTiger',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TigerIndex',                           0, MAX_TIGER_INDEX - 1 ),
                   ] 
                 )

################################################################################
# EGL IMAGE
################################################################################

################################################################################
# CreateEglImageTarget. Create OpenVG image from EGL image. Requires
# VG_KHR_EGL_image extension. EglImageIndex defines the EGL image index at EGL
# module.
AddActionConfig( 'CreateEglImageTarget',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'ImageIndex',                           0, MAX_INDEX_INDEX - 1 ),
                   IntAttribute(                'EglImageIndex',                        0 ),
                   ]
                 )

######################################################################
# END OF FILE
######################################################################
