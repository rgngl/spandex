/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreategradientimagedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateGradientImageDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateGradientImageDataActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenVGCreateGradientImageDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateGradientImageDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateGradientImageData@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateGradientImageDataActionContext ) );

    context->moduleContext = ( SCTOpenVGModuleContext* )( moduleContext );

    if( sctiParseOpenVGCreateGradientImageDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateGradientImageDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageDataIndex( context->moduleContext, context->data.imageDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateGradientImageDataActionContext( context );
        SCT_LOG_ERROR( "Invalid image data index in CreateGradientImageData@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateGradientImageDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateGradientImageDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateGradientImageDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGCreateGradientImageDataActionContext*  context;
    OpenVGCreateGradientImageDataActionData*        data;
    SCTOpenVGImageData*                             imageData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGCreateGradientImageDataActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenVGModuleGetImageData( context->moduleContext, data->imageDataIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Image data index already used in CreateGradientImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    imageData = sctOpenVGCreateGradientImageData( data->format,
                                                  data->width, 
                                                  data->height, 
                                                  data->color1, 
                                                  data->color2,
                                                  data->gradient );
    if( imageData == NULL )
    {
        SCT_LOG_ERROR( "Image data creation failed in CreateGradientImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleSetImageData( context->moduleContext, data->imageDataIndex, imageData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreateGradientImageDataActionTerminate( SCTAction* action )
{
    SCTOpenVGCreateGradientImageDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGCreateGradientImageDataActionContext* )( action->context );

    sctiOpenVGModuleDeleteImageData( context->moduleContext, context->data.imageDataIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreateGradientImageDataActionDestroy( SCTAction* action )
{
    SCTOpenVGCreateGradientImageDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGCreateGradientImageDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenVGCreateGradientImageDataActionContext( context );
}
