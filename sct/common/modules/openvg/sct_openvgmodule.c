/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgmodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_openvgmodule_parser.h"
#include "sct_openvgmodule_actions.h"
#include "sct_vg.h"

#include "sct_openvgtiger.h"

#include <stdio.h>
#include <string.h>

/* Local function declarations. */
SCTAttributeList*       sctiOpenVGModuleInfo( SCTModule* module );
SCTAction*              sctiOpenVGModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    sctiOpenVGModuleDestroy( SCTModule* module );

/*!
 *
 */
SCTBoolean sctOpenVGModuleSetDataExt( int index, SCTOpenVGData* data )
{
    SCTOpenVGModuleContext* context;
    SCTBoolean              pointerRegistered   = SCT_FALSE;

    context = ( SCTOpenVGModuleContext* )( sctGetRegisteredPointer( SCT_OPENVG_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || context == NULL )
    {
        return SCT_FALSE;
    }

    if( sctiOpenVGModuleIsValidDataIndex( context, index ) == SCT_FALSE )    
    {
        return SCT_FALSE;
    }
    
    sctiOpenVGModuleDeleteData( context, index );
    sctiOpenVGModuleSetData( context, index, data );

    return SCT_TRUE;
}

/*!
 *
 */
VGPath sctOpenVGModuleGetPathExt( int index )
{
    SCTOpenVGModuleContext*     context;
    SCTBoolean                  pointerRegistered       = SCT_FALSE;

    if( index < 0 || index >= SCT_MAX_OPENVG_PATHS )
    {
        return VG_INVALID_HANDLE;
    }

    context = ( SCTOpenVGModuleContext* )( sctGetRegisteredPointer( SCT_OPENVG_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || context == NULL )
    {
        return VG_INVALID_HANDLE;
    }

    if( context->paths[ index ] == NULL )
    {
        return VG_INVALID_HANDLE;
    }
    
    return context->paths[ index ]->path;
}

/*!
 *
 */
VGImage sctOpenVGModuleGetImageExt( int index )
{
    SCTOpenVGModuleContext*     context;
    SCTBoolean                  pointerRegistered       = SCT_FALSE;
   
    if( index < 0 || index >= SCT_MAX_OPENVG_IMAGES )
    {
        return VG_INVALID_HANDLE;
    }

    context = ( SCTOpenVGModuleContext* )( sctGetRegisteredPointer( SCT_OPENVG_MODULE_POINTER, &pointerRegistered ) );

    if( pointerRegistered == SCT_FALSE || context == NULL )
    {
        return VG_INVALID_HANDLE;
    }

    if( context->images[ index ] == NULL )
    {
        return VG_INVALID_HANDLE;
    }
    
    return context->images[ index ]->image;
}

/*!
 * Create openvg module data structure which defines the name of the
 * module and sets up the module interface functions.
 *
 * \return Module structure for OpenVG module, or NULL if failure.
 */
SCTModule* sctCreateOpenVGModule( void )
{
    SCTModule*                  module;
    SCTOpenVGModuleContext*     context;
    int                         i;

    /* Create OpenVG module context and zero memory. */
    context = ( SCTOpenVGModuleContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGModuleContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGModuleContext ) );

    for( i = 0; i < SCT_MAX_OPENVG_IMAGES; ++i )
    {
        context->images[ i ] = NULL;
    }

    for( i = 0; i < SCT_MAX_OPENVG_PAINTS; ++i )
    {
        context->paints[ i ] = NULL;
    }

    for( i = 0; i < SCT_MAX_OPENVG_PATHS; ++i )
    {
        context->paths[ i ] = NULL;
    }

    for( i = 0; i < SCT_MAX_OPENVG_DATA_ELEMENTS; ++i )
    {
        context->dataElements[ i ] = NULL;
    }
    
    for( i = 0; i < SCT_MAX_OPENVG_IMAGE_DATA_ELEMENTS; ++i )
    {
        context->imageDataElements[ i ] = NULL;
    }

    for( i = 0; i < SCT_MAX_OPENVG_PATH_DATA_ELEMENTS; ++i )
    {
        context->pathDataElements[ i ] = NULL;
    }

#if defined( OPENVG_VERSION_1_1 )

    for( i = 0; i < SCT_MAX_OPENVG_FONTS; ++i )
    {
        context->fonts[ i ] = NULL;
    }

    for( i = 0; i < SCT_MAX_OPENVG_MASK_LAYERS; ++i )
    {
        context->maskLayers[ i ] = NULL;
    }

#endif  /* defined( OPENVG_VERSION_1_1 ) */

    for( i = 0; i < SCT_MAX_OPENVG_TIGERS; ++i )
    {
        context->tigers[ i ] = NULL;
    }

    for( i = 0; i < SCT_MAX_OPENVG_FLOATS; ++i )
    {
        context->floats[ i ] = 0;
    }

    for( i = 0; i < SCT_MAX_OPENVG_LOOKUP_TABLES; ++i )
    {
        context->lookupTables[ i ] = NULL;
    }

    /* Create module data structure. */
    module = sctCreateModule( "OpenVG",
                              context,
#if defined( _WIN32 )
                              _openvg_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiOpenVGModuleInfo,
                              sctiOpenVGModuleCreateAction,
                              sctiOpenVGModuleDestroy );

    if( module == NULL )
    {
        siCommonMemoryFree( NULL, context );
        return NULL;
    }

    if( sctRegisterPointer( SCT_OPENVG_MODULE_POINTER, context ) == SCT_FALSE )
    {
        sctiOpenVGModuleDestroy( module );
        sctDestroyModule( module );
        return NULL;
    }

    return module;
}

/*!
 * Create attribute list containing OpenVG module information. 
 *
 * \param module OpenVG module. Must be defined.
 *
 * \return OpenVG system information as attribute list, or NULL if
 * failure.
 */
SCTAttributeList* sctiOpenVGModuleInfo( SCTModule* module )
{
    SCTAttributeList*           attributes;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( module->context != NULL );

    /* Create attribute list for storing OpenVG module information. */
    attributes = sctCreateAttributeList();

    return attributes;
}

/*!
 * Create OpenVG module action. 
 *
 * \param module OpenVG module. Must be defined.
 * \param name Action name. Must be defined.
 * \param type Action type. Must be defined.
 * \param attributes List of action attributes. Must be defined.
 * \param log Log to use for warning and error reporting.
 *
 * \return Action data structure, or NULL if failure.
 *
 */
SCTAction* sctiOpenVGModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCTOpenVGModuleContext* moduleContext;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    moduleContext = ( SCTOpenVGModuleContext* )( module->context );

    SCT_ASSERT_ALWAYS( moduleContext != NULL );

    return sctCreateActionFromTemplate( name, type, "OpenVG", moduleContext, attributes, OpenVGActionTemplates, SCT_ARRAY_LENGTH( OpenVGActionTemplates ) );
}

/*!
 * Destroy OpenVG module context. This function does not deallocate
 * module data structure but instead simply does the needed module
 * cleanup, if any.
 *
 * \param module Module to destroy. Must be defined.
 */
void sctiOpenVGModuleDestroy( SCTModule* module )
{
    SCTOpenVGModuleContext*     context;
    int                         i;

    SCT_ASSERT_ALWAYS( module != NULL );

    sctUnregisterPointer( SCT_OPENVG_MODULE_POINTER );

    context = ( SCTOpenVGModuleContext* )( module->context );
    if( context != NULL )
    {
        /* Make sure OpenVG surface has been destroyed when the module
         * is destroyed. */
        SCT_ASSERT_ALWAYS( context->vgContext == NULL );

        for( i = 0; i < SCT_MAX_OPENVG_IMAGES; ++i )
        {
            SCT_ASSERT_ALWAYS( context->images[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENVG_PAINTS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->paints[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENVG_PATHS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->paths[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENVG_DATA_ELEMENTS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->dataElements[ i ] == NULL );
        }
        
        for( i = 0; i < SCT_MAX_OPENVG_IMAGE_DATA_ELEMENTS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->imageDataElements[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENVG_PATH_DATA_ELEMENTS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->pathDataElements[ i ] == NULL );
        }

#if defined( OPENVG_VERSION_1_1 )

        for( i = 0; i < SCT_MAX_OPENVG_FONTS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->fonts[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENVG_MASK_LAYERS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->maskLayers[ i ] == NULL );
        }

#endif  /* defined( OPENVG_VERSION_1_1 ) */

        for( i = 0; i < SCT_MAX_OPENVG_TIGERS; ++i )
        {
            SCT_ASSERT_ALWAYS( context->tigers[ i ] == NULL );
        }

        for( i = 0; i < SCT_MAX_OPENVG_LOOKUP_TABLES; ++i )
        {
            SCT_ASSERT_ALWAYS( context->lookupTables[ i ] == NULL );
        }

        siCommonMemoryFree( NULL, context );
        module->context = NULL;
    }
}

//######################################################################
// OpenVG module images
/*!
 *
 *
 */
SCTBoolean sctiOpenVGModuleIsValidImageIndex( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index >= 0 && index < SCT_MAX_OPENVG_IMAGES )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenVGImage* sctiOpenVGModuleGetImage( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_IMAGES );

    if( index >= 0 && index < SCT_MAX_OPENVG_IMAGES )
    {
        return moduleContext->images[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenVGModuleSetImage( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGImage* image )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_IMAGES );
    SCT_ASSERT( image != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_IMAGES )
    {
        SCT_ASSERT( moduleContext->images[ index ] == NULL );
        moduleContext->images[ index ] = image;
    }
}

/*!
 *
 *
 */
void sctiOpenVGModuleDeleteImage( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCTOpenVGImage* image;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_IMAGES )
    {
        image = moduleContext->images[ index ];
        if( image != NULL )
        {
            vgDestroyImage( image->image );
            sctOpenVGDestroyImage( image );
            moduleContext->images[ index ] = NULL;
        }
    }
}

//######################################################################
// OpenVG paints
/*!
 *
 *
 */
SCTBoolean sctiOpenVGModuleIsValidPaintIndex( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENVG_PAINTS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenVGPaint* sctiOpenVGModuleGetPaint( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_PAINTS );

    if( index >= 0 && index < SCT_MAX_OPENVG_PAINTS )
    {
        return moduleContext->paints[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenVGModuleSetPaint( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGPaint* paint )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_PAINTS );
    SCT_ASSERT( paint != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_PAINTS )
    {
        SCT_ASSERT( moduleContext->paints[ index ] == NULL );
        moduleContext->paints[ index ] = paint;
    }
}

/*!
 *
 *
 */
void sctiOpenVGModuleDeletePaint( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCTOpenVGPaint* paint;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_PAINTS )
    {
        paint = moduleContext->paints[ index ];
        if( paint != NULL )
        {
            vgDestroyPaint( paint->paint );
            sctOpenVGDestroyPaint( paint );
            moduleContext->paints[ index ] = NULL;
        }
    }
}

//######################################################################
// OpenVG paths
/*!
 *
 *
 */
SCTBoolean sctiOpenVGModuleIsValidPathIndex( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENVG_PATHS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenVGPath* sctiOpenVGModuleGetPath( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_PATHS );

    if( index >= 0 && index < SCT_MAX_OPENVG_PATHS )
    {
        return moduleContext->paths[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenVGModuleSetPath( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGPath* path )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_PATHS );
    SCT_ASSERT( path != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_PATHS )
    {
        SCT_ASSERT( moduleContext->paths[ index ] == NULL );
        moduleContext->paths[ index ] = path;
    }
}

/*!
 *
 *
 */
void sctiOpenVGModuleDeletePath( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCTOpenVGPath*  path;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_PATHS )
    {
        path = moduleContext->paths[ index ];
        if( path != NULL )
        {
            vgDestroyPath( path->path );
            sctOpenVGDestroyPath( path );
            moduleContext->paths[ index ] = NULL;
        }
    }
}

//######################################################################
// OpenVG data elements
/*!
 *
 *
 */
SCTBoolean sctiOpenVGModuleIsValidDataIndex( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENVG_DATA_ELEMENTS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenVGData* sctiOpenVGModuleGetData( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_DATA_ELEMENTS );

    if( index >= 0 && index < SCT_MAX_OPENVG_DATA_ELEMENTS )
    {
        return moduleContext->dataElements[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenVGModuleSetData( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGData* data )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_DATA_ELEMENTS );

    if( index >= 0 && index < SCT_MAX_OPENVG_DATA_ELEMENTS )
    {
        moduleContext->dataElements[ index ] = data;
    }
}

/*!
 *
 *
 */
void sctiOpenVGModuleDeleteData( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCTOpenVGData*  data;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_DATA_ELEMENTS )
    {
        data = moduleContext->dataElements[ index ];
        if( data != NULL )
        {
            sctOpenVGDestroyData( data );
            moduleContext->dataElements[ index ] = NULL;
        }
    }
}

//######################################################################
// OpenVG image data elements
/*!
 *
 *
 */
SCTBoolean sctiOpenVGModuleIsValidImageDataIndex( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENVG_IMAGE_DATA_ELEMENTS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenVGImageData* sctiOpenVGModuleGetImageData( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_IMAGE_DATA_ELEMENTS );

    if( index >= 0 && index < SCT_MAX_OPENVG_IMAGE_DATA_ELEMENTS )
    {
        return moduleContext->imageDataElements[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenVGModuleSetImageData( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGImageData* imageData )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_IMAGE_DATA_ELEMENTS );
    SCT_ASSERT( imageData != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_IMAGE_DATA_ELEMENTS )
    {
        SCT_ASSERT( moduleContext->imageDataElements[ index ] == NULL );
        moduleContext->imageDataElements[ index ] = imageData;
    }
}

/*!
 *
 *
 */
void sctiOpenVGModuleDeleteImageData( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCTOpenVGImageData* imageData;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_IMAGE_DATA_ELEMENTS )
    {
        imageData = moduleContext->imageDataElements[ index ];
        if( imageData != NULL )
        {
            sctOpenVGDestroyImageData( imageData );
            moduleContext->imageDataElements[ index ] = NULL;
        }
    }
}

//######################################################################
// OpenVG path data elements
/*!
 *
 *
 */
SCTBoolean sctiOpenVGModuleIsValidPathDataIndex( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENVG_PATH_DATA_ELEMENTS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenVGPathData* sctiOpenVGModuleGetPathData( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_PATH_DATA_ELEMENTS );

    if( index >= 0 && index < SCT_MAX_OPENVG_PATH_DATA_ELEMENTS )
    {
        return moduleContext->pathDataElements[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenVGModuleSetPathData( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGPathData* pathData )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_PATH_DATA_ELEMENTS );
    SCT_ASSERT( pathData != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_PATH_DATA_ELEMENTS )
    {
        SCT_ASSERT( moduleContext->pathDataElements[ index ] == NULL );
        moduleContext->pathDataElements[ index ] = pathData;
    }
}

/*!
 *
 *
 */
void sctiOpenVGModuleDeletePathData( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCTOpenVGPathData*  pathData;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_PATH_DATA_ELEMENTS )
    {
        pathData = moduleContext->pathDataElements[ index ];
        if( pathData != NULL )
        {
            sctOpenVGDestroyPathData( pathData );
            moduleContext->pathDataElements[ index ] = NULL;
        }
    }
}

#if defined( OPENVG_VERSION_1_1 )
//######################################################################
// OpenVG 1.1 fonts
/*!
 *
 *
 */
SCTBoolean sctiOpenVGModuleIsValidFontIndex( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENVG_PATHS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenVGFont* sctiOpenVGModuleGetFont( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_FONTS );

    if( index >= 0 && index < SCT_MAX_OPENVG_FONTS )
    {
        return moduleContext->fonts[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenVGModuleSetFont( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGFont* font )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_FONTS );
    SCT_ASSERT( font != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_FONTS )
    {
        SCT_ASSERT( moduleContext->fonts[ index ] == NULL );
        moduleContext->fonts[ index ] = font;
    }
}

/*!
 *
 *
 */
void sctiOpenVGModuleDeleteFont( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCTOpenVGFont*  font;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_FONTS )
    {
        font = moduleContext->fonts[ index ];
        if( font != NULL )
        {
            vgDestroyFont( font->font );
            sctOpenVGDestroyFont( font );
            moduleContext->fonts[ index ] = NULL;
        }
    }
}

//######################################################################
// OpenVG 1.1 mask layers
/*!
 *
 *
 */
SCTBoolean sctiOpenVGModuleIsValidMaskLayerIndex( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENVG_MASK_LAYERS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenVGMaskLayer* sctiOpenVGModuleGetMaskLayer( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_MASK_LAYERS );

    if( index >= 0 && index < SCT_MAX_OPENVG_MASK_LAYERS )
    {
        return moduleContext->maskLayers[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenVGModuleSetMaskLayer( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGMaskLayer* maskLayer )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_MASK_LAYERS );
    SCT_ASSERT( maskLayer != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_MASK_LAYERS )
    {
        SCT_ASSERT( moduleContext->maskLayers[ index ] == NULL );
        moduleContext->maskLayers[ index ] = maskLayer;
    }
}

/*!
 *
 *
 */
void sctiOpenVGModuleDeleteMaskLayer( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCTOpenVGMaskLayer* maskLayer;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_MASK_LAYERS )
    {
        maskLayer = moduleContext->maskLayers[ index ];
        if( maskLayer != NULL )
        {
            vgDestroyMaskLayer( maskLayer->maskLayer );
            sctOpenVGDestroyMaskLayer( maskLayer );
            moduleContext->maskLayers[ index ] = NULL;
        }
    }
}

#endif /* defined( OPENVG_VERSION_1_1 ) */

//######################################################################
// Tigers
/*!
 *
 *
 */
SCTBoolean sctiOpenVGModuleIsValidTigerIndex( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENVG_TIGERS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
void* sctiOpenVGModuleGetTiger( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_TIGERS );

    if( index >= 0 && index < SCT_MAX_OPENVG_TIGERS )
    {
        return moduleContext->tigers[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenVGModuleSetTiger( SCTOpenVGModuleContext* moduleContext, int index, void* tiger )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_TIGERS );
    SCT_ASSERT( tiger != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_TIGERS )
    {
        SCT_ASSERT( moduleContext->tigers[ index ] == NULL );
        moduleContext->tigers[ index ] = tiger;
    }
}

/*!
 *
 *
 */
void sctiOpenVGModuleDeleteTiger( SCTOpenVGModuleContext* moduleContext, int index )
{
    void*   tiger;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_TIGERS )
    {
        tiger = moduleContext->tigers[ index ];
        if( tiger != NULL )
        {
            sctOpenVGDestroyTiger( tiger );
            moduleContext->tigers[ index ] = NULL;
        }
    }
}

//######################################################################
// Floats
/*!
 *
 *
 */
SCTBoolean sctiOpenVGModuleIsValidFloatIndex( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENVG_FLOATS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
VGfloat sctiOpenVGModuleGetFloat( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_FLOATS );

    if( index >= 0 && index < SCT_MAX_OPENVG_FLOATS )
    {
        return moduleContext->floats[ index ];
    }

    return 0.0f;
}

/*!
 *
 *
 */
void sctiOpenVGModuleSetFloat( SCTOpenVGModuleContext* moduleContext, int index, VGfloat f )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_FLOATS );

    if( index >= 0 && index < SCT_MAX_OPENVG_FLOATS )
    {
        moduleContext->floats[ index ] = f;
    }
}

//######################################################################
// Lookup tables
/*!
 *
 *
 */
SCTBoolean sctiOpenVGModuleIsValidLookupTableIndex( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );
    
    if( index >= 0 && index < SCT_MAX_OPENVG_LOOKUP_TABLES )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
SCTOpenVGLookupTable* sctiOpenVGModuleGetLookupTable( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_LOOKUP_TABLES );

    if( index >= 0 && index < SCT_MAX_OPENVG_LOOKUP_TABLES )
    {
        return moduleContext->lookupTables[ index ];
    }

    return NULL;
}

/*!
 *
 *
 */
void sctiOpenVGModuleSetLookupTable( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGLookupTable* lookupTable )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_OPENVG_LOOKUP_TABLES );
    SCT_ASSERT( lookupTable != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_LOOKUP_TABLES )
    {
        SCT_ASSERT( moduleContext->lookupTables[ index ] == NULL );
        moduleContext->lookupTables[ index ] = lookupTable;
    }
}

/*!
 *
 *
 */
void sctiOpenVGModuleDeleteLookupTables( SCTOpenVGModuleContext* moduleContext, int index )
{
    SCTOpenVGLookupTable*   lookupTable;

    SCT_ASSERT( moduleContext != NULL );

    if( index >= 0 && index < SCT_MAX_OPENVG_LOOKUP_TABLES )
    {
        lookupTable = moduleContext->lookupTables[ index ];
        if( lookupTable != NULL )
        {
            sctOpenVGDestroyLookupTable( lookupTable );
            moduleContext->lookupTables[ index ] = NULL;
        }
    }
}

/*!
 *
 *
 */
const char* sctiOpenVGGetErrorString( VGErrorCode err )
{
    switch( err )
    {
    case VG_NO_ERROR:
        return "VG_NO_ERROR";
    case VG_BAD_HANDLE_ERROR:
        return "VG_BAD_HANDLE_ERROR";
    case VG_ILLEGAL_ARGUMENT_ERROR:
        return "VG_ILLEGAL_ARGUMENT_ERROR";
    case VG_OUT_OF_MEMORY_ERROR:
        return "VG_OUT_OF_MEMORY_ERROR";
    case VG_PATH_CAPABILITY_ERROR:
        return "VG_PATH_CAPABILITY_ERROR";
    case VG_UNSUPPORTED_IMAGE_FORMAT_ERROR:
        return "VG_UNSUPPORTED_IMAGE_FORMAT_ERROR";
    case VG_UNSUPPORTED_PATH_FORMAT_ERROR:
        return "VG_UNSUPPORTED_PATH_FORMAT_ERROR";
    case VG_IMAGE_IN_USE_ERROR:
        return "VG_IMAGE_IN_USE_ERROR";
    default:
        return "UNKNOWN";
    }
}

/*!
 *
 *
 */
void sctiOpenVGClearVGError( void )
{
    int i;
    
    /* Beware infinite loop. */
    for( i = 0; i < 256; ++i )
    {
        if( vgGetError() == VG_NO_ERROR )
        {
            break;
        }
    }
}
