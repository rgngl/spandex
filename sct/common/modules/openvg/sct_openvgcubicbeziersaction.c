/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcubicbeziersaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCubicBeziersActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCubicBeziersActionContext* context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCubicBeziersActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCubicBeziersActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CubicBeziers@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCubicBeziersActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCubicBeziersActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCubicBeziersActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathDataIndex( context->moduleContext, context->data.pathDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCubicBeziersActionContext( context );
        SCT_LOG_ERROR( "Path data index out of range in CubicBeziers@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCubicBeziersActionContext( void* context )
{
    SCTOpenVGCubicBeziersActionContext* c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenVGCubicBeziersActionContext* )( context );
    if( c->data.coordinates != NULL )
    {
        sctDestroyDoubleVector( c->data.coordinates );
        c->data.coordinates = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCubicBeziersActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGCubicBeziersActionContext* context;
    OpenVGCubicBeziersActionData*       data;
    VGubyte                             segment;
    int                                 i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGCubicBeziersActionContext* )( action->context );
    data    = &( context->data );

    context->pathType         = ( VGPathDatatype )( -1 );
    context->numOfSegments    = data->coordinates->length / 6;
    context->numOfCoordinates = data->coordinates->length;

    if( context->pathSegments == NULL )
    {
        context->pathSegments = ( VGubyte* )( siCommonMemoryAlloc( NULL, context->numOfSegments ) ); 
    }
    if( context->pathData == NULL )
    {
        context->pathData = siCommonMemoryAlloc( NULL, context->numOfCoordinates * sizeof( VGfloat ) );
    }
    
    if( context->pathSegments == NULL || context->pathData == NULL )
    {
        sctiOpenVGCubicBeziersActionTerminate( action );
        SCT_LOG_ERROR( "Allocation failed in CubicBeziers@OpenVG action init." );
        return SCT_FALSE;
    }

    if( data->relative == SCT_FALSE )
    {
        segment = VG_CUBIC_TO_ABS;
    }
    else
    {
        segment = VG_CUBIC_TO_REL;
    }

    for( i = 0; i < context->numOfSegments; ++i )
    {
        context->pathSegments[ i ] = segment;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCubicBeziersActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGCubicBeziersActionContext* context;
    OpenVGCubicBeziersActionData*       data;
    SCTOpenVGPathData*                  pathData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGCubicBeziersActionContext* )( action->context );
    data    = &( context->data );

    pathData = sctiOpenVGModuleGetPathData( context->moduleContext, data->pathDataIndex );
    if( pathData == NULL )
    {
        SCT_LOG_ERROR( "Invalid path data index in CubicBeziers@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( context->pathType != pathData->type )
    {
        context->pathType = pathData->type;
        if( sctOpenVGConvertDoubleArray( context->pathData, 
                                         context->pathType,
                                         context->data.coordinates->data, 
                                         context->numOfCoordinates ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Coordinate data out of path datatype range in CubicBeziers@OpenVG action execute." );
            return SCT_FALSE;
        }
    }

    if( sctOpenVGAddPathData( pathData, 
                              context->pathSegments, context->numOfSegments, 
                              context->pathData, context->numOfCoordinates, 
                              context->pathType ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Path data append failed in CubicBeziers@OpenVG action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCubicBeziersActionTerminate( SCTAction* action )
{
    SCTOpenVGCubicBeziersActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGCubicBeziersActionContext* )( action->context );

    if( context->pathSegments != NULL )
    {
        siCommonMemoryFree( NULL, context->pathSegments );
        context->pathSegments = NULL;
    }
    if( context->pathData != NULL )
    {
        siCommonMemoryFree( NULL, context->pathData );
        context->pathData = NULL;
    }
}

/*!
 *
 *
 */
void sctiOpenVGCubicBeziersActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCubicBeziersActionContext( action->context );
    action->context = NULL;
}
