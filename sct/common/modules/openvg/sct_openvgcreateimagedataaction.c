/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreateimagedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateImageDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateImageDataActionContext*  context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCreateImageDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateImageDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateImageData@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateImageDataActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCreateImageDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateImageDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageDataIndex( context->moduleContext, context->data.imageDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateImageDataActionContext( context );
        SCT_LOG_ERROR( "Image data index out of range in CreateImageData@OpenVG context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateImageDataActionContext( context );
        SCT_LOG_ERROR( "Data index out of range in CreateImageData@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateImageDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateImageDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateImageDataActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGCreateImageDataActionContext*  context;
    OpenVGCreateImageDataActionData*        data;
    int                                     imageDataIndex;    
    SCTOpenVGData*                          rawData;    
    SCTOpenVGImageData*                     imageData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGCreateImageDataActionContext* )( action->context );
    data    = &( context->data );
   
    imageDataIndex = data->imageDataIndex;
    if( sctiOpenVGModuleGetImageData( context->moduleContext, imageDataIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Image data index already in use in CreateImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    rawData = sctiOpenVGModuleGetData( context->moduleContext, data->dataIndex );
    if( rawData == NULL )
    {
        SCT_LOG_ERROR( "Invalid data in CreateImageData@OpenVG action execute." );
        return SCT_FALSE;
    }
    
    imageData = sctOpenVGCreateImageData( data->format, data->width, data->height );
    if( imageData == NULL )
    {
        SCT_LOG_ERROR( "Image data creation failed in CreateImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( imageData->dataLength != rawData->length )
    {
        sctOpenVGDestroyImageData( imageData );
        SCT_LOG_ERROR( "Data length does not match with image data length in CreateImageData@OpenVG action execute." );
        return SCT_FALSE;
    }
    
    memcpy( imageData->data, rawData->data, rawData->length );
    
    sctiOpenVGModuleSetImageData( context->moduleContext, imageDataIndex, imageData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreateImageDataActionTerminate( SCTAction* action )
{
    SCTOpenVGCreateImageDataActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTOpenVGCreateImageDataActionContext* )( action->context );

    sctiOpenVGModuleDeleteImageData( context->moduleContext, context->data.imageDataIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreateImageDataActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCreateImageDataActionContext( action->context );
    action->context = NULL;
}
