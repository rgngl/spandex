/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdecodeaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

#define SCT_PVRTEXTOOL3_HEADER_LENGTH   52

/*!
 *
 *
 */
void* sctiCreateOpenVGDecodeActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDecodeActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenVGDecodeActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDecodeActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Decode@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDecodeActionContext ) );

    context->moduleContext = ( SCTOpenVGModuleContext* )( moduleContext );

    if( sctiParseOpenVGDecodeActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDecodeActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidDataIndex( context->moduleContext, context->data.srcDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDecodeActionContext( context );
        SCT_LOG_ERROR( "Invalid source data index in Decode@OpenVG context creation." );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidDataIndex( context->moduleContext, context->data.dstDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDecodeActionContext( context );
        SCT_LOG_ERROR( "Invalid destination data index in Decode@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDecodeActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDecodeActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDecodeActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGDecodeActionContext*   context;
    OpenVGDecodeActionData*         data;
    SCTOpenVGData*                  srcData;
    SCTOpenVGData*                  dstData;
    unsigned char*                  ptr          = NULL;
    int                             length       = 0;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGDecodeActionContext* )( action->context );
    data    = &( context->data );

    srcData = sctiOpenVGModuleGetData( context->moduleContext, data->srcDataIndex );
    if( srcData == NULL )
    {
        SCT_LOG_ERROR( "Invalid source data index in Decode@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( data->encoder == DECODE_ENCODER_PVRTEXTOOL3 )
    {
        if( srcData->length <= SCT_PVRTEXTOOL3_HEADER_LENGTH )
        {
            SCT_LOG_ERROR( "Invalid PVRTEXTOOL3 data in Decode@OpenVG action execute." );
            return SCT_FALSE;
        }

        ptr    = &( ( unsigned char* )( srcData->data ) )[ SCT_PVRTEXTOOL3_HEADER_LENGTH ];
        length = srcData->length - SCT_PVRTEXTOOL3_HEADER_LENGTH;
    }
    else
    {
        SCT_LOG_ERROR( "Unexpected encoder in Decode@OpenVG action execute." );
        return SCT_FALSE;
    }
    
    dstData = sctiOpenVGModuleGetData( context->moduleContext, data->dstDataIndex );
    if( dstData != NULL )
    {
        if( dstData->length != length )
        {
            SCT_LOG_ERROR( "Destination data index already in use in Decode@OpenVG action execute." );
            return SCT_FALSE;
        }

        memcpy( dstData->data, ptr, length );
    }
    else
    {
        dstData = sctOpenVGCreateData( ptr, length, SCT_TRUE );
        if( dstData == NULL )
        {
            SCT_LOG_ERROR( "Destination data allocation failed in Decode@OpenVG action execute." );
            return SCT_FALSE;
        }

        sctiOpenVGModuleSetData( context->moduleContext, data->dstDataIndex, dstData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGDecodeActionTerminate( SCTAction* action )
{
    SCTOpenVGDecodeActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGDecodeActionContext* )( action->context );

    sctiOpenVGModuleDeleteData( context->moduleContext, context->data.dstDataIndex );
}

/*!
 *
 *
 */
void sctiOpenVGDecodeActionDestroy( SCTAction* action )
{
    SCTOpenVGDecodeActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGDecodeActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenVGDecodeActionContext( context );
}
