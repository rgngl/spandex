/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgimagesubdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGImageSubDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGImageSubDataActionContext* context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGImageSubDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGImageSubDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ImageSubData@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGImageSubDataActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGImageSubDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGImageSubDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageDataIndex( context->moduleContext, context->data.srcImageDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGImageSubDataActionContext( context );
        SCT_LOG_ERROR( "Source image data index out of range in ImageSubData@OpenVG context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.dstImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGImageSubDataActionContext( context );
        SCT_LOG_ERROR( "Destination data index out of range in CreateImageSubData@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGImageSubDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGImageSubDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGImageSubDataActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGImageSubDataActionContext* context;
    OpenVGImageSubDataActionData*       data;
    SCTOpenVGImage*                     sctImage;
    SCTOpenVGImageData*                 imageDataElement;
    VGErrorCode                         err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGImageSubDataActionContext* )( action->context );
    data    = &( context->data );

    imageDataElement = sctiOpenVGModuleGetImageData( context->moduleContext, data->srcImageDataIndex );
    if( imageDataElement == NULL )
    {
        SCT_LOG_ERROR( "Invalid source image data in ImageSubData@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctImage = sctiOpenVGModuleGetImage( context->moduleContext, data->dstImageIndex );
    if( sctImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid destination data image in ImageSubData@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctImage->image != VG_INVALID_HANDLE );

    if( ( imageDataElement->width < data->width || imageDataElement->height < data->height ) &&
        ( imageDataElement->width < ( sctImage->width - data->x ) ||
          imageDataElement->height < ( sctImage->height - data->y ) ) )
    {
        SCT_LOG_ERROR( "Incompatible source image data in ImageSubData@OpenVG action execute." );
        return SCT_FALSE;
    }
   
    vgImageSubData( sctImage->image,
                    imageDataElement->data,
                    imageDataElement->stride,
                    imageDataElement->format,
                    data->x,
                    data->y,
                    data->width,
                    data->height );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in ImageSubData@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGImageSubDataActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGImageSubDataActionContext( action->context );
    action->context = NULL;
}
