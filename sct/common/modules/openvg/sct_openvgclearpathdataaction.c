/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgclearpathdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGClearPathDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGClearPathDataActionContext*    context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGClearPathDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGClearPathDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ClearPathData@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGClearPathDataActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGClearPathDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGClearPathDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathDataIndex( context->moduleContext, context->data.pathDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGClearPathDataActionContext( context );
        SCT_LOG_ERROR( "Invalid path data index in ClearPathData@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGClearPathDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGClearPathDataActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGClearPathDataActionContext*    context;
    OpenVGClearPathDataActionData*          data;
    SCTOpenVGPathData*                      pathData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGClearPathDataActionContext* )( action->context );
    data    = &( context->data );

    pathData = sctiOpenVGModuleGetPathData( context->moduleContext, data->pathDataIndex );
    if( pathData == NULL )
    {
        SCT_LOG_ERROR( "Invalid path data in ClearPathData@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctOpenVGClearPathData( pathData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGClearPathDataActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGClearPathDataActionContext( action->context );
    action->context = NULL;
}
