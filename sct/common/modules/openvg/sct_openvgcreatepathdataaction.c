/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatepathdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreatePathDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreatePathDataActionContext*   context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCreatePathDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreatePathDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePathData@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreatePathDataActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCreatePathDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreatePathDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathDataIndex( context->moduleContext, context->data.pathDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreatePathDataActionContext( context );
        SCT_LOG_ERROR( "Invalid path data index in CreatePathData@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreatePathDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreatePathDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreatePathDataActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGCreatePathDataActionContext*   context;
    OpenVGCreatePathDataActionData*         data;
    int                                     index;
    SCTOpenVGPathData*                      pathData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGCreatePathDataActionContext* )( action->context );
    data    = &( context->data );

    index = data->pathDataIndex;
    if( sctiOpenVGModuleGetPathData( context->moduleContext, index ) != NULL )
    {
        SCT_LOG_ERROR( "Path data index already in use in CreatePathData@OpenVG action execute." );
        return SCT_FALSE;
    }

    pathData = sctOpenVGCreatePathData( data->datatype, data->initialSegments, data->initialData );
    if( pathData == NULL )
    {
        SCT_LOG_ERROR( "Path data creation failed in CreatePathData@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleSetPathData( context->moduleContext, index, pathData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreatePathDataActionTerminate( SCTAction* action )
{
    SCTOpenVGCreatePathDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTOpenVGCreatePathDataActionContext* )( action->context );
    
    sctiOpenVGModuleDeletePathData( context->moduleContext, context->data.pathDataIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreatePathDataActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCreatePathDataActionContext( action->context );
    action->context = NULL;
}
