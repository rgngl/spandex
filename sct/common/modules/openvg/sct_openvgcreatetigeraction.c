/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatetigeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgtiger.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateTigerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateTigerActionContext*  context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCreateTigerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateTigerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateTiger@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateTigerActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCreateTigerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateTigerActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidTigerIndex( context->moduleContext, context->data.tigerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateTigerActionContext( context );
        SCT_LOG_ERROR( "Invalid tiger index in CreateTiger@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateTigerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateTigerActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateTigerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGCreateTigerActionContext*  context;
    OpenVGCreateTigerActionData*        data;
    void*                               tiger;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGCreateTigerActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenVGModuleGetTiger( context->moduleContext, data->tigerIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Tiger index already in use in CreateTiger@OpenVG action execute." );
        return SCT_FALSE;
    }

    tiger = sctOpenVGCreateTiger();
    if( tiger == NULL )
    {
        SCT_LOG_ERROR( "Tiger creation failed in CreateTiger@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleSetTiger( context->moduleContext, data->tigerIndex, tiger );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreateTigerActionTerminate( SCTAction* action )
{
    SCTOpenVGCreateTigerActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTOpenVGCreateTigerActionContext* )( action->context );

    sctiOpenVGModuleDeleteTiger( context->moduleContext, context->data.tigerIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreateTigerActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCreateTigerActionContext( ( SCTOpenVGCreateTigerActionContext* )( action->context ) );
    action->context = NULL;
}

