/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvginterpolatepathaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGInterpolatePathActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGInterpolatePathActionContext*  context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGInterpolatePathActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGInterpolatePathActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in InterpolatePath@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGInterpolatePathActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGInterpolatePathActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGInterpolatePathActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.dstPathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGInterpolatePathActionContext( context );
        SCT_LOG_ERROR( "Invalid destination path index in InterpolatePath@OpenVG context creation." );
        return NULL;
    }
    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.startPathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGInterpolatePathActionContext( context );
        SCT_LOG_ERROR( "Invalid start path index in InterpolatePath@OpenVG context creation." );
        return NULL;
    }
    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.endPathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGInterpolatePathActionContext( context );
        SCT_LOG_ERROR( "Invalid end path index in InterpolatePath@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGInterpolatePathActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGInterpolatePathActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGInterpolatePathActionContext*  context;
    OpenVGInterpolatePathActionData*        data;
    VGErrorCode                             err;
    SCTOpenVGPath*                          dst;
    SCTOpenVGPath*                          startPath;
    SCTOpenVGPath*                          endPath;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );
    
    context = ( SCTOpenVGInterpolatePathActionContext* )( action->context );
    data    = &( context->data );

    dst = sctiOpenVGModuleGetPath( context->moduleContext, data->dstPathIndex );
    if( dst == NULL )
    {
        SCT_LOG_ERROR( "Invalid dst path in InterpolatePath@OpenVG action execute." );
        return SCT_FALSE;
    }
    startPath = sctiOpenVGModuleGetPath( context->moduleContext, data->startPathIndex );
    if( startPath == NULL )
    {
        SCT_LOG_ERROR( "Invalid start path in InterpolatePath@OpenVG action execute." );
        return SCT_FALSE;
    }
    endPath = sctiOpenVGModuleGetPath( context->moduleContext, data->endPathIndex );
    if( endPath == NULL )
    {
        SCT_LOG_ERROR( "Invalid end path in InterpolatePath@OpenVG action execute." );
        return SCT_FALSE;
    }

    SCT_ASSERT( dst->path != VG_INVALID_HANDLE );
    SCT_ASSERT( startPath->path != VG_INVALID_HANDLE );
    SCT_ASSERT( endPath->path != VG_INVALID_HANDLE );

    vgInterpolatePath( dst->path, startPath->path, endPath->path, data->amount );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in InterpolatePath@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGInterpolatePathActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGInterpolatePathActionContext( action->context );
    action->context = NULL;
}
