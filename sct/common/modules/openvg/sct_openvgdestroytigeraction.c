/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdestroytigeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgtiger.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGDestroyTigerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDestroyTigerActionContext* context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGDestroyTigerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDestroyTigerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyTiger@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDestroyTigerActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGDestroyTigerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyTigerActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidTigerIndex( context->moduleContext, context->data.tigerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyTigerActionContext( context );
        SCT_LOG_ERROR( "Invalid tiger index in DestroyTiger@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDestroyTigerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDestroyTigerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGDestroyTigerActionContext* context;
    OpenVGDestroyTigerActionData*       data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGDestroyTigerActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenVGModuleGetTiger( context->moduleContext, data->tigerIndex ) == NULL )
    {
        SCT_LOG_ERROR( "Invalid tiger index in DestroyTiger@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleDeleteTiger( context->moduleContext, data->tigerIndex );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGDestroyTigerActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGDestroyTigerActionContext( action->context );
    action->context = NULL;
}
