/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_utils.h"
#include "sct_openvgutils.h"
#include "sct_sicommon.h"

#include <string.h>

#pragma warning(disable: 4310)

/*!
 *
 *
 */
static SCTBoolean           sctiGetImageDataSizeAndStride( VGImageFormat format, int width, int height, int* datasize, int* stride );
static int                  sctiGetPathDatatypeSize( VGPathDatatype type );
static int                  sctiGetBitsPerPixel( VGImageFormat format );
static void                 sctiAssignImageColor( VGImageFormat format, void* dest, const float* color, unsigned int shift );

/*!
 *
 *
 */
SCTOpenVGImageData* sctOpenVGCreateImageData( VGImageFormat format, int width, int height )
{
    SCTOpenVGImageData* imageData;
    int                 datasize    = 0;
    int                 stride      = 0;

    if( sctiGetImageDataSizeAndStride( format, width, height, &datasize, &stride ) == SCT_FALSE )
    {
        return NULL;
    }
    
    imageData = ( SCTOpenVGImageData* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGImageData ) ) );
    if( imageData == NULL )
    {
        return NULL;
    }
    memset( imageData, 0, sizeof( SCTOpenVGImageData ) );

    imageData->data = siCommonMemoryAlloc( NULL, datasize );
    if( imageData->data == NULL )
    {
        sctOpenVGDestroyImageData( imageData );
        return NULL;
    }
    memset( imageData->data, 0, datasize );
    
    imageData->stride     = stride;
    imageData->format     = format;
    imageData->width      = ( VGint )( width );
    imageData->height     = ( VGint )( height );
    imageData->dataLength = datasize;

    return imageData;
}

/*!
 *
 */
SCTOpenVGImageData* sctOpenVGCreateSolidImageData( VGImageFormat format, int width, int height, const float color[ 4 ] )
{
    SCTOpenVGImageData* imageData;
    unsigned char*      buf;
    unsigned int        bitShifter;
    int                 x, y;
    int                 sizeInBits;
    
    imageData = sctOpenVGCreateImageData( format, width, height );
    if( imageData == NULL )
    {
        return NULL;
    }

    sizeInBits = sctiGetBitsPerPixel( format );
    SCT_ASSERT( sizeInBits == 1  ||
                sizeInBits == 4  ||
                sizeInBits == 8  ||
                sizeInBits == 16 ||
                sizeInBits == 32 );

    bitShifter = 0;   
    /* Fill in the texture data */
    for( y = 0; y < height; ++y )
    {
        buf = ( unsigned char* )( imageData->data ) + imageData->stride * y; 
        for( x = 0; x < width; ++x )
        {
            sctiAssignImageColor( format,
                                  buf,
                                  color,
                                  bitShifter );
            bitShifter += sizeInBits;
            buf += ( bitShifter / 8 );
            bitShifter %= 8;
        }
    }
    
    return imageData;
}

/*!
 *
 */
SCTOpenVGImageData* sctOpenVGCreateBarImageData( VGImageFormat format, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int bars )
{
    SCTOpenVGImageData* imageData;
    unsigned char*      buf;
    unsigned int        bitShifter;
    int                 x, y;
    int                 sizeInBits;
    int                 d;
    
    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color1 != NULL );
    SCT_ASSERT( color2 != NULL );
    SCT_ASSERT( bars > 0 );

    /* Calculate the bar width in pixels. Make sure bar width is at least 1 */
    d = width / bars;
    d = d ? d : 1;

    imageData = sctOpenVGCreateImageData( format, width, height );
    if( imageData == NULL )
    {
        return NULL;
    }

    sizeInBits = sctiGetBitsPerPixel( format );
    SCT_ASSERT( sizeInBits == 1  ||
                sizeInBits == 4  ||
                sizeInBits == 8  ||
                sizeInBits == 16 ||
                sizeInBits == 32 );

    bitShifter = 0;   
    /* Fill in the texture data */
    for( y = 0; y < height; ++y )
    {
        buf = ( unsigned char* )( imageData->data ) + imageData->stride * y; 
        for( x = 0; x < width; ++x )
        {
            sctiAssignImageColor( format,
                                  buf,
                                  ( ( x / d ) % 2 ) ? color1 : color2,
                                  bitShifter );
            bitShifter += sizeInBits;
            buf += ( bitShifter / 8 );
            bitShifter %= 8;
        }
    }
    
    return imageData;
}

/*!
 *
 */
SCTOpenVGImageData* sctOpenVGCreateCheckerImageData( VGImageFormat format, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int checkersX, int checkersY )
{
    SCTOpenVGImageData* imageData;
    unsigned char*      buf;
    unsigned int        bitShifter;
    int                 x, y;
    int                 sizeInBits;
    int                 dx, dy;
    const float*        color;
    
    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color1 != NULL );
    SCT_ASSERT( color2 != NULL );
    SCT_ASSERT( checkersX > 0 );
    SCT_ASSERT( checkersY > 0 );

    dx = width / checkersX;
    dx = dx ? dx : 1;
    dy = height / checkersY;
    dy = dy ? dy : 1;

    imageData = sctOpenVGCreateImageData( format, width, height );
    if( imageData == NULL )
    {
        return NULL;
    }

    sizeInBits = sctiGetBitsPerPixel( format );
    SCT_ASSERT( sizeInBits == 1  ||
                sizeInBits == 4  ||
                sizeInBits == 8  ||
                sizeInBits == 16 ||
                sizeInBits == 32 );

    bitShifter = 0;   
    /* Fill in the texture data */
    for( y = 0; y < height; ++y )
    {
        buf = ( unsigned char* )( imageData->data ) + imageData->stride * y; 
        for( x = 0; x < width; ++x )
        {
            if( ( x / dx ) % 2 )
            {
                if( ( y / dy ) % 2 )
                {
                    color = color1;
                }
                else
                {
                    color = color2;
                }
            }
            else
            {
                if( ( y / dy ) % 2 )
                {
                    color = color2;
                }
                else
                {
                    color = color1;
                }
            }
            
            sctiAssignImageColor( format,
                                  buf,
                                  color,
                                  bitShifter );
            bitShifter += sizeInBits;
            buf += ( bitShifter / 8 );
            bitShifter %= 8;
        }
    }
    
    return imageData;
}
/*!
 *
 */
SCTOpenVGImageData* sctOpenVGCreateGradientImageData( VGImageFormat format, int width, int height, const float color1[ 4 ], const float color2[ 4 ], SCTOpenVGGradient gradient )
{
    SCTOpenVGImageData* imageData;
    unsigned char*      buf;
    unsigned int        bitShifter;
    int                 x, y;
    int                 sizeInBits;
    float               d           = 0.0;
    float               r;
    float               color[ 4 ];

    SCT_ASSERT( width > 0 && height > 0 );
    SCT_ASSERT( color1 != NULL );
    SCT_ASSERT( color2 != NULL );

    imageData = sctOpenVGCreateImageData( format, width, height );
    if( imageData == NULL )
    {
        return NULL;
    }

    sizeInBits = sctiGetBitsPerPixel( format );
    SCT_ASSERT( sizeInBits == 1  ||
                sizeInBits == 4  ||
                sizeInBits == 8  ||
                sizeInBits == 16 ||
                sizeInBits == 32 );

    bitShifter = 0;   
    
    if( gradient == OPENVG_GRADIENT_HORIZONTAL )
    {
        if( width > 1 )
        {
            d = 1.0f / ( float )( width - 1 );
        }
        for( y = 0; y < height; ++y )
        {
            buf = ( unsigned char* )( imageData->data ) + imageData->stride * y;             
            r = 0;
            for( x = 0; x < width; ++x )
            {
                color[ 0 ] = color1[ 0 ] * ( 1 - r ) + color2[ 0 ] * ( r );
                color[ 1 ] = color1[ 1 ] * ( 1 - r ) + color2[ 1 ] * ( r );
                color[ 2 ] = color1[ 2 ] * ( 1 - r ) + color2[ 2 ] * ( r );
                color[ 3 ] = color1[ 3 ] * ( 1 - r ) + color2[ 3 ] * ( r );

                sctiAssignImageColor( format,
                                      buf,
                                      color,
                                      bitShifter );
                bitShifter += sizeInBits;
                buf += ( bitShifter / 8 );
                bitShifter %= 8;
                r += d;
            }
        }
    }
    else if( gradient == OPENVG_GRADIENT_VERTICAL )
    {
        if( height > 1 )
        {
            d = 1.0f / ( float )( height - 1 );
        }

        r = 0;
        for( y = 0; y < height; ++y )
        {
            buf = ( unsigned char* )( imageData->data ) + imageData->stride * y;         
            
            color[ 0 ] = color1[ 0 ] * ( 1 - r ) + color2[ 0 ] * ( r );
            color[ 1 ] = color1[ 1 ] * ( 1 - r ) + color2[ 1 ] * ( r );
            color[ 2 ] = color1[ 2 ] * ( 1 - r ) + color2[ 2 ] * ( r );
            color[ 3 ] = color1[ 3 ] * ( 1 - r ) + color2[ 3 ] * ( r );
            
            for( x = 0; x < width; ++x )
            {
                sctiAssignImageColor( format,
                                      buf,
                                      color,
                                      bitShifter );
                bitShifter += sizeInBits;
                buf += ( bitShifter / 8 );
                bitShifter %= 8;                
            }
            r += d;
        }
    }
    else
    {
        d = 1.0f / ( float )( width * height - 1 );
        r = 0;
        
        for( y = 0; y < height; ++y )
        {
            buf = ( unsigned char* )( imageData->data ) + imageData->stride * y;
            
            for( x = 0; x < width; ++x )
            {
                r = y * x * d;
                
                color[ 0 ] = color1[ 0 ] * ( 1 - r ) + color2[ 0 ] * ( r );
                color[ 1 ] = color1[ 1 ] * ( 1 - r ) + color2[ 1 ] * ( r );
                color[ 2 ] = color1[ 2 ] * ( 1 - r ) + color2[ 2 ] * ( r );
                color[ 3 ] = color1[ 3 ] * ( 1 - r ) + color2[ 3 ] * ( r );

                sctiAssignImageColor( format,
                                      buf,
                                      color,
                                      bitShifter );
                bitShifter += sizeInBits;
                buf += ( bitShifter / 8 );
                bitShifter %= 8;                
            }
        }
    }
    
    return imageData;
}

/*!
 *
 *
 */
void sctOpenVGDestroyImageData( SCTOpenVGImageData* data )
{
    if( data == NULL )
    {
        return;
    }

    if( data->data != NULL )
    {
        siCommonMemoryFree( NULL, data->data );
    }
    siCommonMemoryFree( NULL, data );
}

/*!
 *
 *
 */
static SCTBoolean sctiGetImageDataSizeAndStride( VGImageFormat format, int width, int height, int* datasize, int* stride )
{
    int bitsPerPixel = 0;
    int bitsPerLine  = 0;
    int s;

    bitsPerPixel = sctiGetBitsPerPixel( format );

    if( bitsPerPixel == 0 )
    {
        SCT_ASSERT( 0 );
        return SCT_FALSE;
    }
    
    /* Calculate the number of bytes per image line. */
    bitsPerLine = ( width * bitsPerPixel );
    s = bitsPerLine / 8;

    if( format == VG_BW_1 && ( bitsPerLine % 8 ) != 0 )
    {
        /* We add padding bits to line when the number of pixels in
         line is not multiple of 8. */
        s += 1;
    }
    
    *datasize = s * height;
    *stride   = s;

    return SCT_TRUE;
}

/*!
 *
 */
static int sctiGetBitsPerPixel( VGImageFormat format )
{   
    switch( format )
    {
    default:
        return 0;
    case VG_BW_1:
    case VG_A_1:
        return 1;
    case VG_A_4:
        return 4;
    case VG_sL_8:
    case VG_lL_8:
    case VG_A_8:
        return 8;
    case VG_sRGB_565:
    case VG_sBGR_565:
    case VG_sRGBA_5551:
    case VG_sRGBA_4444:
    case VG_sARGB_1555:
    case VG_sARGB_4444:
    case VG_sBGRA_5551:
    case VG_sBGRA_4444:
    case VG_sABGR_1555:
    case VG_sABGR_4444:
        return 16;
    case VG_sRGBX_8888:
    case VG_sRGBA_8888:
    case VG_sRGBA_8888_PRE:
    case VG_lRGBX_8888:
    case VG_lRGBA_8888:
    case VG_lRGBA_8888_PRE:
    case VG_sXRGB_8888:
    case VG_sARGB_8888:
    case VG_sARGB_8888_PRE:
    case VG_lXRGB_8888:
    case VG_lARGB_8888:
    case VG_lARGB_8888_PRE:
    case VG_sBGRX_8888:
    case VG_sBGRA_8888:
    case VG_sBGRA_8888_PRE:
    case VG_lBGRX_8888:
    case VG_lBGRA_8888:
    case VG_lBGRA_8888_PRE:
    case VG_sXBGR_8888:
    case VG_sABGR_8888:
    case VG_sABGR_8888_PRE:
    case VG_lXBGR_8888:
    case VG_lABGR_8888:
    case VG_lABGR_8888_PRE:
        return 32;
    }
}

/*!
 *
 */
void sctiAssignImageColor( VGImageFormat format, void* dest, const float* color, unsigned int shift )
{
    unsigned char*  cp = ( unsigned char* )( dest );
    unsigned short* sp = ( unsigned short* )( dest );
    unsigned int*   ip = ( unsigned int* )( dest );
    unsigned char   c;
    unsigned short  s;
    unsigned int    i;

    SCT_ASSERT( dest != NULL );
    SCT_ASSERT( color != NULL );

    switch( format )
    {
    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    case VG_BW_1:
        c = ( unsigned char )( ( ( ( unsigned int )( color[ 0 ] + 0.5f ) << shift ) & ( 0x0001 << shift ) ) );
        *cp |= c;
        break;
    case VG_A_1:
        c = ( unsigned char )( ( ( ( unsigned int )( color[ 3 ] + 0.5f ) << shift ) & ( 0x0001 << shift ) ) );
        *cp |= c;
        break;
    case VG_A_4:
        c = ( unsigned char )( ( ( ( unsigned int )( color[ 3 ] * 0xF ) << shift ) & ( 0x000F << shift ) ) );
        *cp |= c;
        break;
    case VG_sL_8:
    case VG_lL_8:
        *cp = ( unsigned char )( color[ 0 ] * 255 );        
        break;
    case VG_A_8:
        *cp = ( unsigned char )( color[ 3 ] * 255 );        
        break;        
    case VG_sRGB_565:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 0 ] * 0x1F ) << 11 ) & 0xF800 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0x3F ) << 5 )  & 0x07E0 ) |
            (   ( unsigned int )( color[ 2 ] * 0x1F )         & 0x001F ) );
        *sp = s;
        break;        
    case VG_sBGR_565:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 2 ] * 0x1F ) << 11 ) & 0xF800 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0x3F ) << 5 )  & 0x07E0 ) |
            (   ( unsigned int )( color[ 0 ] * 0x1F )         & 0x001F ) );
        *sp = s;
        break;
    case VG_sRGBA_4444:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 0 ] * 0xF ) << 12 ) & 0xF000 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xF ) << 8 )  & 0x0F00 ) |
            ( ( ( unsigned int )( color[ 2 ] * 0xF ) << 4 )  & 0x00F0 ) |
              ( ( unsigned int )( color[ 3 ] * 0xF )         & 0x000F ) );
        *sp = s;
        break;       
    case VG_sBGRA_4444:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 2 ] * 0xF ) << 12 ) & 0xF000 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xF ) << 8 )  & 0x0F00 ) |
            ( ( ( unsigned int )( color[ 0 ] * 0xF ) << 4 )  & 0x00F0 ) |
              ( ( unsigned int )( color[ 3 ] * 0xF )         & 0x000F ) );
        *sp = s;
        break;       
    case VG_sABGR_4444:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 3 ] * 0xF ) << 12 ) & 0xF000 ) |
            ( ( ( unsigned int )( color[ 2 ] * 0xF ) << 8 )  & 0x0F00 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xF ) << 4 )  & 0x00F0 ) |
              ( ( unsigned int )( color[ 0 ] * 0xF )         & 0x000F ) );
        *sp = s;
        break;       
    case VG_sARGB_4444:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 3 ] * 0xF ) << 12 ) & 0xF000 ) |
            ( ( ( unsigned int )( color[ 0 ] * 0xF ) << 8 )  & 0x0F00 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xF ) << 4 )  & 0x00F0 ) |
              ( ( unsigned int )( color[ 2 ] * 0xF )         & 0x000F ) );
        *sp = s;
        break;       
    case VG_sRGBA_5551:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 0 ] * 0x1F ) << 11 ) & 0xF800 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0x1F ) << 6 )  & 0x07C0 ) |
            ( ( ( unsigned int )( color[ 2 ] * 0x1F ) << 1 )  & 0x003E ) |
              ( ( unsigned int )( color[ 3 ] + 0.5f )         & 0x0001 ) );
        *sp = s;
        break;       
    case VG_sBGRA_5551:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 2 ] * 0x1F ) << 11 ) & 0xF800 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0x1F ) << 6 )  & 0x07C0 ) |
            ( ( ( unsigned int )( color[ 0 ] * 0x1F ) << 1 )  & 0x003E ) |
              ( ( unsigned int )( color[ 3 ] + 0.5f )         & 0x0001 ) );
        *sp = s;
        break;       
    case VG_sARGB_1555:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 3 ] + 0.5f ) << 15 ) & 0x8000 ) |
            ( ( ( unsigned int )( color[ 0 ] * 0x1F ) << 10 ) & 0x7C00 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0x1F ) << 5 )  & 0x03E0 ) |
              ( ( unsigned int )( color[ 2 ] * 0x1F )         & 0x001F ) );
        *sp = s;
        break;       
    case VG_sABGR_1555:
        s = ( unsigned short )(
            ( ( ( unsigned int )( color[ 3 ] + 0.5f ) << 15 ) & 0x8000 ) |
            ( ( ( unsigned int )( color[ 2 ] * 0x1F ) << 10 ) & 0x7C00 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0x1F ) << 5 )  & 0x03E0 ) |
              ( ( unsigned int )( color[ 0 ] * 0x1F )         & 0x001F ) );
        *sp = s;
        break;       
    case VG_sRGBX_8888:        
    case VG_lRGBX_8888:
        i = ( unsigned int )(
            ( ( ( unsigned int )( color[ 0 ] * 0xFF ) << 24 ) & 0xFF000000 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xFF ) << 16 ) & 0x00FF0000 ) |
            ( ( ( unsigned int )( color[ 2 ] * 0xFF ) << 8 )  & 0x0000FF00 ) |
              ( ( unsigned int )( 0 )                         & 0x000000FF ) );
        *ip = i;
        break;
    case VG_sXRGB_8888:
    case VG_lXRGB_8888:
        i = ( unsigned int )(
            ( ( ( unsigned int )( 0 )                 << 24 ) & 0xFF000000 ) |
            ( ( ( unsigned int )( color[ 0 ] * 0xFF ) << 16 ) & 0x00FF0000 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xFF ) << 8 )  & 0x0000FF00 ) |
              ( ( unsigned int )( color[ 2 ] * 0xFF )         & 0x000000FF ) );
        *ip = i;
        break;
    case VG_sBGRX_8888:
    case VG_lBGRX_8888:
        i = ( unsigned int )(
            ( ( ( unsigned int )( color[ 2 ] * 0xFF ) << 24 ) & 0xFF000000 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xFF ) << 16 ) & 0x00FF0000 ) |
            ( ( ( unsigned int )( color[ 0 ] * 0xFF ) << 8 )  & 0x0000FF00 ) |
              ( ( unsigned int )( 0 )                         & 0x000000FF ) );
        *ip = i;
        break;
    case VG_sXBGR_8888:
    case VG_lXBGR_8888:
        i = ( unsigned int )(
            ( ( ( unsigned int )( 0 )                 << 24 ) & 0xFF000000 ) |
            ( ( ( unsigned int )( color[ 2 ] * 0xFF ) << 16 ) & 0x00FF0000 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xFF ) << 8 )  & 0x0000FF00 ) |
              ( ( unsigned int )( color[ 0 ] * 0xFF )         & 0x000000FF ) );
        *ip = i;
        break;
    case VG_sRGBA_8888:
    case VG_sRGBA_8888_PRE:
    case VG_lRGBA_8888:
    case VG_lRGBA_8888_PRE:
        i = ( unsigned int )(
            ( ( ( unsigned int )( color[ 0 ] * 0xFF ) << 24 ) & 0xFF000000 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xFF ) << 16 ) & 0x00FF0000 ) |
            ( ( ( unsigned int )( color[ 2 ] * 0xFF ) << 8 )  & 0x0000FF00 ) |
              ( ( unsigned int )( color[ 3 ] * 0xFF )         & 0x000000FF ) );
        *ip = i;
        break;
    case VG_sARGB_8888:
    case VG_sARGB_8888_PRE:
    case VG_lARGB_8888:
    case VG_lARGB_8888_PRE:
        i = ( unsigned int )(
            ( ( ( unsigned int )( color[ 3 ] * 0xFF ) << 24 ) & 0xFF000000 ) |
            ( ( ( unsigned int )( color[ 0 ] * 0xFF ) << 16 ) & 0x00FF0000 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xFF ) << 8 )  & 0x0000FF00 ) |
              ( ( unsigned int )( color[ 2 ] * 0xFF )         & 0x000000FF ) );
        *ip = i;
        break;
    case VG_sBGRA_8888:
    case VG_sBGRA_8888_PRE:
    case VG_lBGRA_8888:
    case VG_lBGRA_8888_PRE:
        i = ( unsigned int )(
            ( ( ( unsigned int )( color[ 2 ] * 0xFF ) << 24 ) & 0xFF000000 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xFF ) << 16 ) & 0x00FF0000 ) |
            ( ( ( unsigned int )( color[ 0 ] * 0xFF ) << 8 )  & 0x0000FF00 ) |
              ( ( unsigned int )( color[ 3 ] * 0xFF )         & 0x000000FF ) );
        *ip = i;
        break;        
    case VG_sABGR_8888:
    case VG_sABGR_8888_PRE:
    case VG_lABGR_8888:
    case VG_lABGR_8888_PRE:
        i = ( unsigned int )(
            ( ( ( unsigned int )( color[ 3 ] * 0xFF ) << 24 ) & 0xFF000000 ) |
            ( ( ( unsigned int )( color[ 2 ] * 0xFF ) << 16 ) & 0x00FF0000 ) |
            ( ( ( unsigned int )( color[ 1 ] * 0xFF ) << 8 )  & 0x0000FF00 ) |
              ( ( unsigned int )( color[ 0 ] * 0xFF )         & 0x000000FF ) );
        *ip = i;
        break;
    }   
}

/*!
 *
 */
static int sctiGetPathDatatypeSize( VGPathDatatype type )
{
    switch( type )
    {
    case VG_PATH_DATATYPE_S_8:
        return sizeof( VGbyte );

    case VG_PATH_DATATYPE_S_16:
        return sizeof( VGshort );

    case VG_PATH_DATATYPE_S_32:
        return sizeof( VGint );

    case VG_PATH_DATATYPE_F:
        return sizeof( VGfloat );

    default:
        return -1;
    }
}

/*!
 *
 */
SCTOpenVGPath* sctOpenVGCreatePath( VGPath path, VGPathDatatype type )
{
    SCTOpenVGPath* p;
    
    p = ( SCTOpenVGPath* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGPath ) ) );
    if( p != NULL )
    {
        p->path = path;
        p->type = type;
    }
    return p;
}

/*!
 *
 */
void sctOpenVGDestroyPath( SCTOpenVGPath* path )
{
    if( path != NULL )
    {
        siCommonMemoryFree( NULL, path );
    }
}

/*!
 *
 */
SCTOpenVGImage* sctOpenVGCreateImage( VGImage image, int width, int height  )
{
    SCTOpenVGImage* i;

    i = ( SCTOpenVGImage* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGImage ) ) );
    if( i != NULL )
    {
        i->image  = image;
        i->width  = ( VGint )( width );
        i->height = ( VGint )( height );
    }
    return i;
}

/*!
 *
 */
void sctOpenVGDestroyImage( SCTOpenVGImage* image )
{
    if( image != NULL )
    {
        siCommonMemoryFree( NULL, image );
    }
}

/*!
 *
 */
SCTOpenVGPaint* sctOpenVGCreatePaint( VGPaint paint )
{
    SCTOpenVGPaint* p;
    
    p = ( SCTOpenVGPaint* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGPaint ) ) );
    if( p != NULL )
    {
        p->paint = paint;
    }
    return p;
}

/*!
 *
 */
void sctOpenVGDestroyPaint( SCTOpenVGPaint* paint )
{
    if( paint != NULL )
    {
        siCommonMemoryFree( NULL, paint );
    }
}

#if defined( OPENVG_VERSION_1_1 )

/*!
 *
 */
SCTOpenVGFont* sctOpenVGCreateFont( VGFont font )
{
    SCTOpenVGFont* f;
    
    f = ( SCTOpenVGFont* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGFont ) ) );
    if( f != NULL )
    {
        f->font = font;
    }
    return f;
}

/*!
 *
 */
void sctOpenVGDestroyFont( SCTOpenVGFont* font )
{
    if( font != NULL )
    {
        siCommonMemoryFree( NULL, font );
    }
}

/*!
 *
 */
SCTOpenVGMaskLayer* sctOpenVGCreateMaskLayer( VGMaskLayer maskLayer )
{
    SCTOpenVGMaskLayer* ml;
    
    ml = ( SCTOpenVGMaskLayer* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGMaskLayer ) ) );
    if( ml != NULL )
    {
        ml->maskLayer = maskLayer;
    }
    return ml;
}

/*!
 *
 */
void sctOpenVGDestroyMaskLayer( SCTOpenVGMaskLayer* maskLayer )
{
    if( maskLayer != NULL )
    {
        siCommonMemoryFree( NULL, maskLayer );
    }
}

#endif /* defined( OPENVG_VERSION_1_1 ) */

/*!
 *
 *
 */
SCTOpenVGPathData* sctOpenVGCreatePathData( VGPathDatatype type, int initialSegments, int initialData )
{
    SCTOpenVGPathData*  data;
    int                 segmentsSize;
    int                 dataSize;

    dataSize = sctiGetPathDatatypeSize( type ) * initialData;
    segmentsSize = initialSegments * sizeof( VGubyte );
    if( segmentsSize <= 0 || dataSize <= 0 )
    {
        return NULL;
    }

    data = ( SCTOpenVGPathData* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGPathData ) ) );
    if( data == NULL )
    {
        return NULL;
    }
    memset( data, 0, sizeof( SCTOpenVGPathData ) );

    data->type           = type;
    data->numSegments    = 0;
    data->maxNumSegments = initialSegments;
    data->dataLength     = 0;
    data->maxDataLength  = initialData;

    data->segments = ( VGubyte* )( siCommonMemoryAlloc( NULL, segmentsSize ) );
    data->data     = siCommonMemoryAlloc( NULL, dataSize );

    if( data->segments == NULL || data->data == NULL )
    {
        sctOpenVGDestroyPathData( data );
        return NULL;
    }

    memset( data->segments, 0, segmentsSize );
    memset( data->data, 0, dataSize );
    
    return data;
}

/*!
 *
 *
 */
void sctOpenVGClearPathData( SCTOpenVGPathData* pathData )
{
    SCT_ASSERT( pathData != NULL );

    pathData->numSegments = 0;
    pathData->dataLength  = 0;
}

/*!
 *
 *
 */
SCTBoolean sctOpenVGAddPathData( SCTOpenVGPathData* pathData, const VGubyte* segments, int numSegments, void* data, int dataLength, VGPathDatatype dataType )
{
    int dataTypeSize = 0;

    SCT_ASSERT( pathData != NULL );
    SCT_ASSERT( segments != NULL );
    SCT_ASSERT( numSegments > 0 );

    if( ( int )( dataType ) < 0 && dataLength > 0 )
    {
        return SCT_FALSE;
    }

    if( ( int )( dataType ) >= 0 && pathData->type != dataType )
    {
        return SCT_FALSE;
    }

    if( dataLength > 0 )
    {
        dataTypeSize = sctiGetPathDatatypeSize( dataType );
        SCT_ASSERT( dataTypeSize > 0 );
    }

    if( pathData->numSegments + numSegments > pathData->maxNumSegments )
    {
        VGubyte*    newSegments;
        int         newMaxNumSegments;

        newMaxNumSegments = ( pathData->numSegments + numSegments ) * 2;
        newSegments = ( VGubyte* )( siCommonMemoryAlloc( NULL, sizeof( VGubyte ) * newMaxNumSegments ) );
        if( newSegments == NULL )
        {
            return SCT_FALSE;
        }

        memcpy( newSegments, pathData->segments, sizeof( VGubyte ) * pathData->numSegments );
        siCommonMemoryFree( NULL, pathData->segments );

        pathData->segments = newSegments;
        pathData->maxNumSegments = newMaxNumSegments;
    }

    if( pathData->dataLength + dataLength > pathData->maxDataLength )
    {
        void*   newData;
        int     newMaxDataLength;

        newMaxDataLength = ( pathData->dataLength + dataLength ) * 2;
        newData = siCommonMemoryAlloc( NULL, dataTypeSize * newMaxDataLength );
        if( newData == NULL )
        {
            return SCT_FALSE;
        }

        memcpy( newData, pathData->data, dataTypeSize * pathData->dataLength );
        siCommonMemoryFree( NULL, pathData->data );

        pathData->data = newData;
        pathData->maxDataLength = newMaxDataLength;
    }

    SCT_ASSERT( pathData->numSegments + numSegments <= pathData->maxNumSegments );
    memcpy( pathData->segments + pathData->numSegments, segments, sizeof( VGubyte ) * numSegments );
    pathData->numSegments += numSegments;

    if( dataLength > 0 )
    {
        SCT_ASSERT( pathData->dataLength + dataLength <= pathData->maxDataLength );
        memcpy( ( ( char* )( pathData->data ) ) + ( pathData->dataLength * dataTypeSize ), data, dataLength * dataTypeSize );
        pathData->dataLength += dataLength;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctOpenVGDestroyPathData( SCTOpenVGPathData* pathData )
{
    if( pathData != NULL )
    {
        if( pathData->segments != NULL )
        {
            siCommonMemoryFree( NULL, pathData->segments );
            pathData->segments = NULL;
        }
        if( pathData->data != NULL )
        {
            siCommonMemoryFree( NULL, pathData->data );
            pathData->data = NULL;
        }

        siCommonMemoryFree( NULL, pathData );
    }
}


/*!
 *
 *
 */
void sctOpenVGAssignTypeArray( void* destination, VGPathDatatype type, const VGfloat* source, VGint length )
{
    int         i;     
    VGbyte*     cp  = ( VGbyte* )( destination );
    VGshort*    sp  = ( VGshort* )( destination );
    VGint*      ip  = ( VGint* )( destination );
    VGfloat*    fp  = ( VGfloat* )( destination );

    SCT_ASSERT_ALWAYS( destination != NULL );
    SCT_ASSERT_ALWAYS( type == VG_PATH_DATATYPE_S_8  || 
                       type != VG_PATH_DATATYPE_S_16 || 
                       type != VG_PATH_DATATYPE_S_32 || 
                       type != VG_PATH_DATATYPE_F );
    SCT_ASSERT_ALWAYS( source != NULL );
    SCT_ASSERT_ALWAYS( length > 0 );

    for( i = 0; i < length; ++i )
    {
        switch( type )
        {
        case VG_PATH_DATATYPE_S_8:
            *cp++ = ( VGbyte )( *source++ );
            break;

        case VG_PATH_DATATYPE_S_16:
            SCT_ASSERT_ALWAYS( ( int )sp % sizeof( VGshort ) == 0 ); /* Check alignment */
            *sp++ = ( VGshort )( *source++ );
            break;

        case VG_PATH_DATATYPE_S_32:
            SCT_ASSERT_ALWAYS( ( int )ip % sizeof( VGint ) == 0 ); /* Check alignment */
            *ip++ = ( VGint )( *source++ );
            break;

        default:
        case VG_PATH_DATATYPE_F:
            SCT_ASSERT_ALWAYS( (int )fp % sizeof( VGfloat ) == 0 );
            *fp++ = *source++;
            break;
        }
    }
}

/*!
 *
 *
 */
void sctOpenVGRescaleToDatatypeRange( VGfloat* src, VGfloat* dst, int length, VGPathDatatype datatype, VGfloat* scale, VGfloat* bias )
{
    int     i;
    VGfloat maxValue;
    VGfloat min, max;
    VGfloat div;
    VGfloat average;

    SCT_ASSERT_ALWAYS( src != NULL );
    SCT_ASSERT_ALWAYS( dst != NULL );
    SCT_ASSERT_ALWAYS( length > 0 );
    SCT_ASSERT_ALWAYS( scale != NULL );
    SCT_ASSERT_ALWAYS( bias != NULL );

    /* We do not need to do anything with float. */
    if( datatype == VG_PATH_DATATYPE_F )
    {
        for( i = 0; i < length; ++i )
        {
            dst[ i ] = src[ i ];
        }
        *scale = 1;
        *bias  = 0;
        return;
    }

    switch( datatype )
    {
    case VG_PATH_DATATYPE_S_8:
        maxValue = ( VGfloat )( 0xFF / 2.0f );
        break;

    case VG_PATH_DATATYPE_S_16:
        /* There seems to be an error in OpenVG 1.0 headers for VG_MAXSHORT and
         * VG_MAXINT definitions. They result to value -1. Patch it here. */
        maxValue = ( VGfloat )( ( 1 << 15 ) - 1 );
        break;

    case VG_PATH_DATATYPE_S_32:
        /* There seems to be an error in OpenVG 1.0 headers for VG_MAXSHORT and
         * VG_MAXINT definitions. They result to value -1. Patch it here. */
        maxValue = ( VGfloat )( ( unsigned )( 1ul << 31 )  - 1 );
        break;

    default:
        SCT_ASSERT_ALWAYS( 0 );
        return;
    }

    max = src[ 0 ];
    min = src[ 0 ];
    
    /* Find minimum and maximum values in the array. */
    for( i = 0; i < length; ++i )
    {
        if( src[ i ] > max )
        {
            max = src[ i ];
        }
        if( src[ i ] < min )
        {
            min = src[ i ];
        }
    }

    /*
        |
        |____________________________ max
        |
     div|
        |____________________________ average
        |
     div|
        |____________________________ min
        |
        |
        |______________________________

    */

    if( min == max )
    {
        maxValue = sctMaxf( maxValue, sctMaxf( -min, max ) );

        for( i = 0; i < length; ++i )
        {
            dst[ i ] = src[ i ] / maxValue;
        }
        *scale = maxValue;
        *bias = 0;
        return;
    }

    average = ( max + min ) / 2;
    div = average - min;
    div = sctMaxf( -div, div );

    for( i = 0; i < length; ++i )
    {
        dst[ i ] = ( src[ i ] - average ) / div;
        dst[ i ] *= maxValue;
    }

    *bias  = average;
    *scale = div / maxValue;
}

/*!
 *
 *
 */
SCTBoolean sctOpenVGConvertTypeArray( void* dst, VGPathDatatype dstType, void* src, VGPathDatatype srcType, int length )
{
    int         i;
    VGfloat*    tmpBuf;
    VGfloat*    p;
    SCTBoolean  ok      = SCT_TRUE;

    VGbyte*     csrc    = ( VGbyte* )( src );
    VGshort*    ssrc    = ( VGshort* )( src );
    VGint*      isrc    = ( VGint* )( src );
    VGfloat*    fsrc    = ( VGfloat* )( src );

    SCT_ASSERT_ALWAYS( src != NULL );
    SCT_ASSERT_ALWAYS( srcType == VG_PATH_DATATYPE_S_8  || 
                       srcType != VG_PATH_DATATYPE_S_16 || 
                       srcType != VG_PATH_DATATYPE_S_32 || 
                       srcType != VG_PATH_DATATYPE_F );
    SCT_ASSERT_ALWAYS( dst != NULL );
    SCT_ASSERT_ALWAYS( dstType == VG_PATH_DATATYPE_S_8  || 
                       dstType != VG_PATH_DATATYPE_S_16 || 
                       dstType != VG_PATH_DATATYPE_S_32 || 
                       dstType != VG_PATH_DATATYPE_F );
    SCT_ASSERT_ALWAYS( length > 0 );

    tmpBuf = ( VGfloat* )( siCommonMemoryAlloc( NULL, length * sizeof( VGfloat ) ) );
    if( tmpBuf == NULL )
    {
        return SCT_FALSE;
    }

    p = tmpBuf;

    /* First, convert src to float, perform range check. */
    for( i = 0; i < length && ok == SCT_TRUE; ++i )
    {
        /* Conversion to float. */
        switch( srcType )
        {
        case VG_PATH_DATATYPE_S_8:
            *p = *csrc++;
            break;

        case VG_PATH_DATATYPE_S_16:
            *p = *ssrc++;
            break;

        case VG_PATH_DATATYPE_S_32:
            *p = ( float )( *isrc++ );
            break;

        default:
        case VG_PATH_DATATYPE_F:
            *p = *fsrc++;
            break;
        }

        /* range check. */
        switch( dstType )
        {
        case VG_PATH_DATATYPE_S_8:
            if( ( *p > ( 0xFF / 2 ) ) || ( *p < ( -0xFF / 2 ) ) )
            {
                ok = SCT_FALSE;
            }
            break;

        case VG_PATH_DATATYPE_S_16:
            if( ( *p > ( SCT_MAX_SHORT ) ) || ( *p < ( SCT_MIN_SHORT ) ) )
            {
                ok = SCT_FALSE;
            }
            break;

        default:
        case VG_PATH_DATATYPE_S_32:
        case VG_PATH_DATATYPE_F:
            break;
        }

        p++;
    }

    if( ok == SCT_TRUE )
    {
        sctOpenVGAssignTypeArray( dst, dstType, tmpBuf, length );
    }

    siCommonMemoryFree( NULL, tmpBuf );

    return ok;
}

/*!
 *
 *
 */
SCTBoolean sctOpenVGConvertDoubleArray( void* dst, VGPathDatatype dstType, double* src, int length )
{
    int         i;
    SCTBoolean  ok      = SCT_TRUE;

    VGbyte*     cdst    = ( VGbyte* )( dst );
    VGshort*    sdst    = ( VGshort* )( dst );
    VGint*      idst    = ( VGint* )( dst );
    VGfloat*    fdst    = ( VGfloat* )( dst );

    SCT_ASSERT_ALWAYS( dst != NULL );
    SCT_ASSERT_ALWAYS( dstType == VG_PATH_DATATYPE_S_8  || 
                       dstType != VG_PATH_DATATYPE_S_16 || 
                       dstType != VG_PATH_DATATYPE_S_32 || 
                       dstType != VG_PATH_DATATYPE_F );
    SCT_ASSERT_ALWAYS( src != NULL );
    SCT_ASSERT_ALWAYS( length > 0 );

    for( i = 0; i < length && ok == SCT_TRUE; ++i )
    {
        switch( dstType )
        {
        case VG_PATH_DATATYPE_S_8:
            if( *src < -0x7F || *src > 0x7F )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *cdst++ = ( VGbyte )( *src++ );
            }
            break;

        case VG_PATH_DATATYPE_S_16:
            if( *src < -0x7FFF || *src > 0x7FFF )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *sdst++ = ( VGshort )( *src++ );
            }
            break;

        case VG_PATH_DATATYPE_S_32:
            if( *src < -0x7FFFFFFF || *src > 0x7FFFFFFF )
            {
                ok = SCT_FALSE;
            }
            else
            {
                *idst++ = ( VGint )( *src++ );
            }
            break;

        default:
        case VG_PATH_DATATYPE_F:
            *fdst++ = ( VGfloat )( *src++ );
            break;
        }
    }

    return ok;
}

/*!
 *
 *
 */
SCTOpenVGData* sctOpenVGCreateData( void* data, int length, SCTBoolean copy )
{
    SCTOpenVGData*  d;

    SCT_ASSERT( data != NULL );
    SCT_ASSERT( length > 0 );

    d = ( SCTOpenVGData* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGData ) ) );
    if( d == NULL )
    {
        return NULL;
    }
    
    if( copy == SCT_TRUE )
    {
        d->data = siCommonMemoryAlloc( NULL, length );
        if( d->data == NULL )
        {
            siCommonMemoryFree( NULL, d );
            return NULL;
        }
        memcpy( d->data, data, length );
    }
    else
    {
        d->data = data;
    }
   
    d->length = length;

    return d;
}

/*!
 *
 *
 */
void sctOpenVGDestroyData( SCTOpenVGData* data )
{
    if( data != NULL )
    {
        if( data->data != NULL )
        {
            siCommonMemoryFree( NULL, data->data );
            data->data = NULL;
        }
        siCommonMemoryFree( NULL, data );
    }
}

/*!
 *
 *
 */
void sctOpenVGInitializeLUT8( VGubyte* lut, int length, float start, float end )
{
    int     i, len;
    float   r;
    
    SCT_ASSERT_ALWAYS( lut != NULL );
    SCT_ASSERT_ALWAYS( length > 0 );

    if( length == 1 )
    {
        lut[ 0 ] = ( VGubyte )( ( start + end ) / 2 * 0xFF );
        return;
    }

    len = length - 1;
    /* Linear interpolation between start and end. */
    for( i = 0; i <= len; ++i )
    {
        r = ( len - i ) / ( VGfloat )( len );
        
        lut[ i ] = ( VGubyte )( ( start * r + end * ( 1.0f - r ) ) * 0xFF );
    }
}

/*!
 *
 *
 */
void sctOpenVGInitializeLUT32( VGuint* lut, int length, float start[ 4 ], float end[ 4 ] )
{
    int     i, len;
    float   r;
    
    SCT_ASSERT_ALWAYS( lut != NULL );
    SCT_ASSERT_ALWAYS( length > 0 );

    if( length == 1 )
    {
        lut[ 0 ] = ( VGubyte )( ( start[ 0 ] + end[ 0 ] ) / 2 * 0xFF ) << 24 |
            ( VGubyte )( ( start[ 1 ] + end[ 1 ] ) / 2 * 0xFF ) << 16 |
            ( VGubyte )( ( start[ 2 ] + end[ 2 ] ) / 2 * 0xFF ) << 8 |
            ( VGubyte )( ( start[ 3 ] + end[ 3 ] ) / 2 * 0xFF );
        return;
    }

    len = length - 1;
    /* Linear interpolation between start and end. */
    for( i = 0; i <= len; ++i )
    {
        r = ( len - i ) / ( VGfloat )( len );
        
        lut[ i ] = ( VGubyte )( ( start[ 0 ] * r + end[ 0 ] * ( 1.0f - r ) ) * 0xFF ) << 24 |
            ( VGubyte )( ( start[ 1 ] * r + end[ 1 ] * ( 1.0f - r ) ) * 0xFF ) << 16 |
            ( VGubyte )( ( start[ 2 ] * r + end[ 2 ] * ( 1.0f - r ) ) * 0xFF ) << 8 |
            ( VGubyte )( ( start[ 3 ] * r + end[ 3 ] * ( 1.0f - r ) ) * 0xFF );
    }
}

/*!
 *
 *
 */
SCTBoolean sctOpenVGFloatsEqual( VGfloat a, VGfloat b, VGfloat delta )
{
    VGfloat dif = a - b;
    dif = sctMaxf( dif, -dif );
    if( dif > delta  )
    {
        return SCT_FALSE;
    }
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTOpenVGLookupTable* sctOpenVGCreateLookupTable( SCTOpenVGLUTType type )
{
    SCTOpenVGLookupTable*   lut;
    size_t                  typeSize;

    SCT_ASSERT( type == SCT_OPENVG_LUT_UBYTE8 ||
                type == SCT_OPENVG_LUT_UINT32 );

    lut = ( SCTOpenVGLookupTable* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGLookupTable ) ) );
    if( lut == NULL )
    {
        return NULL;
    }
    lut->lookupTable = NULL;
    lut->type        = type;

    typeSize = type == SCT_OPENVG_LUT_UBYTE8 ? sizeof( VGubyte ) : sizeof( VGuint );
    
    lut->lookupTable = siCommonMemoryAlloc( NULL, typeSize * 256 );
    if( lut->lookupTable == NULL )
    {
        sctOpenVGDestroyLookupTable( lut );
        return NULL;
    }
    memset( lut->lookupTable, 0, typeSize * 256 );

    return lut;
}

/*!
 *
 *
 */
SCTBoolean sctOpenVGSetLookupTableData( SCTOpenVGLookupTable* lut, VGuint values[ 256 ] )
{
    int i;

    SCT_ASSERT_ALWAYS( lut != NULL );

    for( i = 0; i < 256; ++i )
    {
        if( lut->type == SCT_OPENVG_LUT_UBYTE8 )
        {
            if( values[ i ] > 0xFF )
            {
                return SCT_FALSE;
            }
            ( ( VGubyte* )( lut->lookupTable ) )[ i ] = ( VGubyte )( values[ i ] );
        }
        else
        {
            ( ( VGuint* )( lut->lookupTable ) )[ i ] = ( VGuint )( values[ i ] );
        }
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctOpenVGDestroyLookupTable( SCTOpenVGLookupTable* lookupTable )
{
    if( lookupTable != NULL )
    {
        if( lookupTable->lookupTable != NULL )
        {
            siCommonMemoryFree( NULL, lookupTable->lookupTable );
            lookupTable->lookupTable = NULL;
        }
        siCommonMemoryFree( NULL, lookupTable );
    }
}
