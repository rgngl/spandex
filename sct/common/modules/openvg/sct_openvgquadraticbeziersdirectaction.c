/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgquadraticbeziersdirectaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGQuadraticBeziersDirectActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGQuadraticBeziersDirectActionContext*   context;
    SCTOpenVGModuleContext*                         mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGQuadraticBeziersDirectActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGQuadraticBeziersDirectActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in QuadraticBeziersDirect@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGQuadraticBeziersDirectActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGQuadraticBeziersDirectActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGQuadraticBeziersDirectActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGQuadraticBeziersDirectActionContext( context );
        SCT_LOG_ERROR( "Path index out of range in QuadraticBeziersDirect@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGQuadraticBeziersDirectActionContext( void* context )
{
    SCTOpenVGQuadraticBeziersDirectActionContext*   c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenVGQuadraticBeziersDirectActionContext* )( context );
    if( c->data.coordinates != NULL )
    {
        sctDestroyDoubleVector( c->data.coordinates );
        c->data.coordinates = NULL;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGQuadraticBeziersDirectActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGQuadraticBeziersDirectActionContext*   context;
    OpenVGQuadraticBeziersDirectActionData*         data;
    VGubyte                                         segment;
    int                                             i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGQuadraticBeziersDirectActionContext* )( action->context );
    data    = &( context->data );

    context->pathType         = ( VGPathDatatype )( -1 );
    context->numOfSegments    = data->coordinates->length / 4;
    context->numOfCoordinates = data->coordinates->length;

    if( context->pathSegments == NULL )
    {
        context->pathSegments = ( VGubyte* )( siCommonMemoryAlloc( NULL, context->numOfSegments ) ); 
    }
    if( context->pathData == NULL )
    {
        context->pathData = siCommonMemoryAlloc( NULL, context->numOfCoordinates * sizeof( VGfloat ) );
    }
    
    if( context->pathSegments == NULL || context->pathData == NULL )
    {
        sctiOpenVGQuadraticBeziersDirectActionTerminate( action );
        SCT_LOG_ERROR( "Allocation failed in QuadraticBeziersDirect@OpenVG action init." );
        return SCT_FALSE;
    }

    if( data->relative == SCT_FALSE )
    {
        segment = VG_QUAD_TO_ABS;
    }
    else
    {
        segment = VG_QUAD_TO_REL;
    }

    for( i = 0; i < context->numOfSegments; ++i )
    {
        context->pathSegments[ i ] = segment;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGQuadraticBeziersDirectActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGQuadraticBeziersDirectActionContext*   context;
    OpenVGQuadraticBeziersDirectActionData*         data;
    SCTOpenVGPath*                                  sctPath;
    VGErrorCode                                     err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGQuadraticBeziersDirectActionContext* )( action->context );
    data    = &( context->data );

    sctPath = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( sctPath == NULL )
    {
        SCT_LOG_ERROR( "Invalid path in QuadraticBeziersDirect@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctPath->path != VG_INVALID_HANDLE );

    if( context->pathType != sctPath->type )
    {
        context->pathType = sctPath->type;
        if( sctOpenVGConvertDoubleArray( context->pathData, 
                                         context->pathType,
                                         context->data.coordinates->data, 
                                         context->numOfCoordinates ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Coordinate data out of path datatype range in QuadraticBeziersDirect@OpenVG action execute." );
            return SCT_FALSE;
        }
    }

    vgAppendPathData( sctPath->path, context->numOfSegments, context->pathSegments, context->pathData );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in QuadraticBeziersDirect@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGQuadraticBeziersDirectActionTerminate( SCTAction* action )
{
    SCTOpenVGQuadraticBeziersDirectActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGQuadraticBeziersDirectActionContext* )( action->context );

    if( context->pathSegments != NULL )
    {
        siCommonMemoryFree( NULL, context->pathSegments );
        context->pathSegments = NULL;
    }
    if( context->pathData != NULL )
    {
        siCommonMemoryFree( NULL, context->pathData );
        context->pathData = NULL;
    }
}

/*!
 *
 *
 */
void sctiOpenVGQuadraticBeziersDirectActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGQuadraticBeziersDirectActionContext( action->context );
    action->context = NULL;
}
