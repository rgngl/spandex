/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcomparefloataction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCompareFloatActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCompareFloatActionContext* context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCompareFloatActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCompareFloatActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CompareFloat@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCompareFloatActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCompareFloatActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCompareFloatActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidFloatIndex( context->moduleContext, context->data.index ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid index in CompareFloat@OpenVG context creation." );
        sctiDestroyOpenVGCompareFloatActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCompareFloatActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCompareFloatActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGCompareFloatActionContext* context;
    OpenVGCompareFloatActionData*       data;
    VGfloat                             value;
    VGfloat                             dif;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGCompareFloatActionContext* )( action->context );
    data    = &( context->data );
    
    value = sctiOpenVGModuleGetFloat( context->moduleContext, data->index );
    dif   = sctAbsf( ( value - data->value ) );

    if( dif > data->delta )
    {
        char buf[ 256 ];
        sprintf( buf, "CompareFloat@OpenVG execute: value %f, expected %f", value, data->value );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCompareFloatActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCompareFloatActionContext( action->context );
    action->context = NULL;
}


