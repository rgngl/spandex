/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgfillmasklayeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGFillMaskLayerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGFillMaskLayerActionContext*    context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGFillMaskLayerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGFillMaskLayerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in FillMaskLayer@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGFillMaskLayerActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGFillMaskLayerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGFillMaskLayerActionContext( context );
        return NULL;
    }

#if defined( OPENVG_VERSION_1_1 )
    if( sctiOpenVGModuleIsValidMaskLayerIndex( context->moduleContext, context->data.maskLayerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGFillMaskLayerActionContext( context );
        SCT_LOG_ERROR( "Invalid mask layer index in FillMaskLayer@OpenVG context creation." );
        return NULL;
    }
#endif  /* defined( OPENVG_VERSION_1_1 ) */

    return context;    
}

/*!
 *
 *
 */
void sctiDestroyOpenVGFillMaskLayerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGFillMaskLayerActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( OPENVG_VERSION_1_1 )
    SCT_LOG_ERROR( "FillMaskLayer@OpenVG not supported in OpenVG 1.0." );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */  
    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */    
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGFillMaskLayerActionExecute( SCTAction* action, int framenumber )
{
#if !defined( OPENVG_VERSION_1_1 )
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCTOpenVGFillMaskLayerActionContext*    context;
    OpenVGFillMaskLayerActionData*          data;
    SCTOpenVGMaskLayer*                     sctMaskLayer;  
    VGErrorCode                             err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGFillMaskLayerActionContext* )( action->context );
    data    = &( context->data );

    sctMaskLayer = sctiOpenVGModuleGetMaskLayer( context->moduleContext, data->maskLayerIndex );
    if( sctMaskLayer == NULL )
    {
        SCT_LOG_ERROR( "Invalid mask layer in FillMaskLayer@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctMaskLayer->maskLayer != VG_INVALID_HANDLE );

    vgFillMaskLayer( sctMaskLayer->maskLayer, 
                     data->x, 
                     data->y, 
                     data->width, 
                     data->height,
                     data->value );

# if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in FillMaskLayer@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
# else  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
# endif /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
}

/*!
 *
 *
 */
void sctiOpenVGFillMaskLayerActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenVGFillMaskLayerActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGFillMaskLayerActionContext( action->context );
    action->context = NULL;
}

