/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgsetpaintaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGSetPaintActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGSetPaintActionContext* context;
    SCTOpenVGModuleContext*         mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGSetPaintActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGSetPaintActionContext ) ) ); 
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SetPaint@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGSetPaintActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGSetPaintActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSetPaintActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPaintIndex( context->moduleContext, context->data.paintIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSetPaintActionContext( context );
        SCT_LOG_ERROR( "Paint index out of range in SetPaint@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGSetPaintActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSetPaintActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGSetPaintActionContext* context;
    OpenVGSetPaintActionData*       data;
    VGErrorCode                     err;
    SCTOpenVGPaint*                 sctPaint;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGSetPaintActionContext* )( action->context );
    data    = &( context->data );

    sctPaint = sctiOpenVGModuleGetPaint( context->moduleContext, data->paintIndex );
    if( sctPaint == NULL )
    {
        SCT_LOG_ERROR( "Invalid paint in SetPaint@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctPaint->paint != VG_INVALID_HANDLE );

    vgSetPaint( sctPaint->paint, data->paintModes );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in SetPaint@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGSetPaintActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGSetPaintActionContext( action->context );
    action->context = NULL;
}




