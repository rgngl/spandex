/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgchildimageaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGChildImageActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGChildImageActionContext*   context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGChildImageActionContext* )siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGChildImageActionContext ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ChildImage@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGChildImageActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGChildImageActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGChildImageActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.imageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGChildImageActionContext( context );
        SCT_LOG_ERROR( "Image index out of range in ChildImage@OpenVG creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.parentImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGChildImageActionContext( context );
        SCT_LOG_ERROR( "Parent image index out of range in ChildImage@OpenVG creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGChildImageActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGChildImageActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGChildImageActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGChildImageActionContext*   context;
    OpenVGChildImageActionData*         data;
    int                                 index;
    int                                 parentIndex;
    SCTOpenVGImage*                     sctChildImage;
    SCTOpenVGImage*                     sctParentImage;
    VGImage                             vgImage;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGChildImageActionContext* )( action->context );
    data    = &( context->data );

    index       = data->imageIndex;
    parentIndex = data->parentImageIndex;

    sctChildImage = sctiOpenVGModuleGetImage( context->moduleContext, index );
    if( sctChildImage != NULL )
    {
        SCT_LOG_ERROR( "Image index already in use in ChildImage@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctParentImage = sctiOpenVGModuleGetImage( context->moduleContext, parentIndex );
    if( sctParentImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid parent image in ChildImage@OpenVG action execute." );
        return SCT_FALSE;
    }

    SCT_ASSERT( sctParentImage->image != VG_INVALID_HANDLE );
    
    vgImage = vgChildImage( sctParentImage->image,
                            data->x,
                            data->y,
                            data->width,
                            data->height );
    
    if( vgImage == VG_INVALID_HANDLE )
    {
        SCT_LOG_ERROR( "Child image creation failed in ChildImage@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctChildImage = sctOpenVGCreateImage( vgImage, data->width, data->height );
    if( sctChildImage == NULL )
    {
        vgDestroyImage( vgImage );
        SCT_LOG_ERROR( "Allocation failed in ChildImage@OpenVG action execute." );
        return SCT_FALSE;
    }
    
    sctiOpenVGModuleSetImage( context->moduleContext, index, sctChildImage );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGChildImageActionTerminate( SCTAction* action )
{
    SCTOpenVGChildImageActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTOpenVGChildImageActionContext* )( action->context );

    sctiOpenVGModuleDeleteImage( context->moduleContext, context->data.imageIndex );
}

/*!
 *
 *
 */
void sctiOpenVGChildImageActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGChildImageActionContext( action->context );
    action->context = NULL;
}
