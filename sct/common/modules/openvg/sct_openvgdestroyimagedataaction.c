/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdestroyimagedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGDestroyImageDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDestroyImageDataActionContext* context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGDestroyImageDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDestroyImageDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyImageData@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDestroyImageDataActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGDestroyImageDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyImageDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageDataIndex( context->moduleContext, context->data.imageDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyImageDataActionContext( context );
        SCT_LOG_ERROR( "Image data index out of range in DestroyImageData@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDestroyImageDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDestroyImageDataActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGDestroyImageDataActionContext* context;
    OpenVGDestroyImageDataActionData*       data;
    int                                     index;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGDestroyImageDataActionContext* )( action->context );
    data    = &( context->data );

    index = data->imageDataIndex;
    if( sctiOpenVGModuleGetImageData( context->moduleContext, index ) == NULL )
    {
        SCT_LOG_ERROR( "Invalid image data in DestroyImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleDeleteImageData( context->moduleContext, index );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGDestroyImageDataActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGDestroyImageDataActionContext( action->context );
    action->context = NULL;
}
