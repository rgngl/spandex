/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdestroymasklayeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGDestroyMaskLayerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDestroyMaskLayerActionContext* context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGDestroyMaskLayerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDestroyMaskLayerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyMaskLayer@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDestroyMaskLayerActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGDestroyMaskLayerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyMaskLayerActionContext( context );
        return NULL;
    }

#if defined( OPENVG_VERSION_1_1 )
    if( sctiOpenVGModuleIsValidMaskLayerIndex( context->moduleContext, context->data.maskLayerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyMaskLayerActionContext( context );
        SCT_LOG_ERROR( "Invalid mask layer index in DestroyMaskLayer@OpenVG context creation." );
        return NULL;
    }
#endif  /* defined( OPENVG_VERSION_1_1 ) */

    return context;   
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDestroyMaskLayerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDestroyMaskLayerActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( OPENVG_VERSION_1_1 )
    SCT_LOG_ERROR( "DestroyMaskLayer@OpenVG not supported in OpenVG 1.0." );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */  
    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */    
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDestroyMaskLayerActionExecute( SCTAction* action, int framenumber )
{
#if !defined( OPENVG_VERSION_1_1 )
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCTOpenVGDestroyMaskLayerActionContext* context;
    OpenVGDestroyMaskLayerActionData*       data;
    int                                     index;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGDestroyMaskLayerActionContext* )( action->context );
    data    = &( context->data );

    index = data->maskLayerIndex;
    if( sctiOpenVGModuleGetMaskLayer( context->moduleContext, index ) == NULL )
    {
        SCT_LOG_ERROR( "Invalid mask layer in DestroyMaskLayer@OpenVG action execute." );
        return SCT_FALSE;
    }
    
    sctiOpenVGModuleDeleteMaskLayer( context->moduleContext, index );

    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
}

/*!
 *
 *
 */
void sctiOpenVGDestroyMaskLayerActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenVGDestroyMaskLayerActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGDestroyMaskLayerActionContext( action->context );
    action->context = NULL;
}

