/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgpathlengthaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGPathLengthActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGPathLengthActionContext*   context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGPathLengthActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGPathLengthActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PathLength@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGPathLengthActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGPathLengthActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPathLengthActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPathLengthActionContext( context );
        SCT_LOG_ERROR( "Invalid path index in PathLength@OpenVG context creation." );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidFloatIndex( context->moduleContext, context->data.lengthIndex ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid length in PathLength@OpenVG context creation." );
        sctiDestroyOpenVGPathLengthActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGPathLengthActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGPathLengthActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGPathLengthActionContext*   context;
    OpenVGPathLengthActionData*         data;
    VGErrorCode                         err;
    SCTOpenVGPath*                      path;
    VGint                               numSegments;
    VGfloat                             length;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGPathLengthActionContext* )( action->context );
    data    = &( context->data );

    path = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( path == NULL )
    {
        SCT_LOG_ERROR( "Invalid path in PathLength@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( path->path != VG_INVALID_HANDLE );

    if( data->numSegments > 0 )
    {
        numSegments = data->numSegments;
    }
    else
    {
        numSegments = ( vgGetParameteri( path->path, VG_PATH_NUM_SEGMENTS ) - data->startSegment );
    }

    length = vgPathLength( path->path, data->startSegment, numSegments );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in PathLength@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();
        
        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    sctiOpenVGModuleSetFloat( context->moduleContext, data->lengthIndex, length );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGPathLengthActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGPathLengthActionContext( action->context );
    action->context = NULL;
}
