/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgremovepathcapabilitiesaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGRemovePathCapabilitiesActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGRemovePathCapabilitiesActionContext*   context;
    SCTOpenVGModuleContext*                         mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGRemovePathCapabilitiesActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGRemovePathCapabilitiesActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in RemovePathCapabilities@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGRemovePathCapabilitiesActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGRemovePathCapabilitiesActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGRemovePathCapabilitiesActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGRemovePathCapabilitiesActionContext( context );
        SCT_LOG_ERROR( "Invalid path index in RemovePathCapabilities@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGRemovePathCapabilitiesActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGRemovePathCapabilitiesActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGRemovePathCapabilitiesActionContext*   context;
    OpenVGRemovePathCapabilitiesActionData*         data;
    SCTOpenVGPath*                                  sctPath;
    VGErrorCode                                     err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGRemovePathCapabilitiesActionContext* )( action->context );
    data    = &( context->data );

    sctPath = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( sctPath == NULL )
    {
        SCT_LOG_ERROR( "Invalid path index in RemovePathCapabilities@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctPath->path != VG_INVALID_HANDLE );

    vgRemovePathCapabilities( sctPath->path, data->capabilities );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in RemovePathCapabilities@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGRemovePathCapabilitiesActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGRemovePathCapabilitiesActionContext( action->context );
    action->context = NULL;
}
