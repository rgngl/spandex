/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgappendpathdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGAppendPathDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGAppendPathDataActionContext*   context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGAppendPathDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGAppendPathDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in AppendPathData@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGAppendPathDataActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGAppendPathDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGAppendPathDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGAppendPathDataActionContext( context );
        SCT_LOG_ERROR( "Invalid path index in AppendPathData@OpenVG context creation." );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathDataIndex( context->moduleContext, context->data.pathDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGAppendPathDataActionContext( context );
        SCT_LOG_ERROR( "Invalid path data index in AppendPathData@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGAppendPathDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGAppendPathDataActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGAppendPathDataActionContext*   context;
    OpenVGAppendPathDataActionData*         data;
    SCTOpenVGPath*                          sctPath;
    SCTOpenVGPathData*                      pathData;
    VGErrorCode                             err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGAppendPathDataActionContext* )( action->context );
    data    = &( context->data );

    sctPath = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( sctPath == NULL )
    {
        SCT_LOG_ERROR( "Invalid path in AppendPathData@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctPath->path != VG_INVALID_HANDLE );

    pathData = sctiOpenVGModuleGetPathData( context->moduleContext, data->pathDataIndex );
    if( pathData == NULL )
    {
        SCT_LOG_ERROR( "Invalid path data in AppendPathData@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( pathData->type != sctPath->type )
    {
        SCT_LOG_ERROR( "Path data type mismatch in AppendPathData@OpenVG action execute." );
        return SCT_FALSE;
    }

    vgAppendPathData( sctPath->path, pathData->numSegments, pathData->segments, pathData->data );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in AppendPathData@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGAppendPathDataActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGAppendPathDataActionContext( action->context );
    action->context = NULL;
}
