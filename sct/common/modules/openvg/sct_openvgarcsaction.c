/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgarcsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGArcsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGArcsActionContext* context;
    SCTOpenVGModuleContext*     mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGArcsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGArcsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Arcs@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGArcsActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGArcsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGArcsActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathDataIndex( context->moduleContext, context->data.pathDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGArcsActionContext( context );
        SCT_LOG_ERROR( "Path data index out of range in Arcs@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGArcsActionContext( void* context )
{
    SCTOpenVGArcsActionContext* c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenVGArcsActionContext* )( context );
    if( c->data.coordinates != NULL )
    {
        sctDestroyDoubleVector( c->data.coordinates );
        c->data.coordinates = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGArcsActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGArcsActionContext* context;
    OpenVGArcsActionData*       data;
    VGubyte                     segment;
    int                         i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGArcsActionContext* )( action->context );
    data    = &( context->data );

    context->pathType         = ( VGPathDatatype )( -1 );
    context->numOfSegments    = data->coordinates->length / 5;
    context->numOfCoordinates = data->coordinates->length;

    if( context->pathSegments == NULL )
    {
        context->pathSegments = ( VGubyte* )( siCommonMemoryAlloc( NULL, context->numOfSegments ) ); 
    }
    if( context->pathData == NULL )
    {
        context->pathData = siCommonMemoryAlloc( NULL, context->numOfCoordinates * sizeof( VGfloat ) );
    }
    
    if( context->pathSegments == NULL || context->pathData == NULL )
    {
        sctiOpenVGArcsActionTerminate( action );
        SCT_LOG_ERROR( "Allocation failed in Arcs@OpenVG action init." );
        return SCT_FALSE;
    }

    switch( data->arcType )
    {
    default:
    case ARCS_ARCTYPE_SCCWARC:
        segment = VG_SCCWARC_TO;
        break;

    case ARCS_ARCTYPE_SCWARC:
        segment = VG_SCWARC_TO;
        break;

    case ARCS_ARCTYPE_LCCWARC:
        segment = VG_LCCWARC_TO;
        break;

    case ARCS_ARCTYPE_LCWARC:
        segment = VG_LCWARC_TO;
        break;
    }

    if( data->relative == SCT_FALSE )
    {
        segment |= VG_ABSOLUTE;
    }
    else
    {
        segment |= VG_RELATIVE;
    }

    for( i = 0; i < context->numOfSegments; ++i )
    {
        context->pathSegments[ i ] = segment;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGArcsActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGArcsActionContext* context;
    OpenVGArcsActionData*       data;
    SCTOpenVGPathData*          pathData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGArcsActionContext* )( action->context );
    data    = &( context->data );

    pathData = sctiOpenVGModuleGetPathData( context->moduleContext, data->pathDataIndex );
    if( pathData == NULL )
    {
        SCT_LOG_ERROR( "Invalid path data index in Arcs@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( context->pathType != pathData->type )
    {
        context->pathType = pathData->type;
        if( sctOpenVGConvertDoubleArray( context->pathData, 
                                         context->pathType,
                                         context->data.coordinates->data, 
                                         context->numOfCoordinates ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Coordinate data out of path datatype range in Arcs@OpenVG action execute." );
            return SCT_FALSE;
        }
    }

    if( sctOpenVGAddPathData( pathData, 
                              context->pathSegments, context->numOfSegments, 
                              context->pathData, context->numOfCoordinates, 
                              context->pathType ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Path data append failed in Arcs@OpenVG action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGArcsActionTerminate( SCTAction* action )
{
    SCTOpenVGArcsActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGArcsActionContext* )( action->context );

    if( context->pathSegments != NULL )
    {
        siCommonMemoryFree( NULL, context->pathSegments );
        context->pathSegments = NULL;
    }
    if( context->pathData != NULL )
    {
        siCommonMemoryFree( NULL, context->pathData );
        context->pathData = NULL;
    }
}

/*!
 *
 *
 */
void sctiOpenVGArcsActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGArcsActionContext( action->context );
    action->context = NULL;
}
