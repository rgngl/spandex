/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgloaddataaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGLoadDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGLoadDataActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenVGLoadDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGLoadDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LoadData@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGLoadDataActionContext ) );

    context->moduleContext = ( SCTOpenVGModuleContext* )( moduleContext );

    if( sctiParseOpenVGLoadDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLoadDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLoadDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in LoadData@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGLoadDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGLoadDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGLoadDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGLoadDataActionContext* context;
    OpenVGLoadDataActionData*       data;
    SCTOpenVGData*                  rawData;
    void*                           fileContext;
    SCTFile                         file;
    int                             fileSize;
    char*                           buffer;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGLoadDataActionContext* )( action->context );
    data    = &( context->data );

    rawData = sctiOpenVGModuleGetData( context->moduleContext, data->dataIndex );

    fileContext = siFileCreateContext();
    if( fileContext == NULL )
    {
        SCT_LOG_ERROR( "Invalid file context in LoadData@OpenVG action execute." );
        return SCT_FALSE;
    }

    file = siFileOpen( fileContext, data->filename, "rb" );
    if( file == NULL )
    {
        siFileDestroyContext( fileContext );
        SCT_LOG_ERROR( "File open failed in LoadData@OpenVG action execute." );
        return SCT_FALSE;
    }

    fileSize = siFileSize( fileContext, file );
    if( fileSize < 1 )
    {
        siFileClose( fileContext, file );
        siFileDestroyContext( fileContext );
        SCT_LOG_ERROR( "Invalid file size in LoadData@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( rawData != NULL )
    {
        if( rawData->length != fileSize )
        {
            siFileClose( fileContext, file );
            siFileDestroyContext( fileContext );            

            SCT_LOG_ERROR( "Data index already used in LoadData@OpenVG action execute." );
            return SCT_FALSE;
        }
        buffer = ( char* )( rawData->data );
    }
    else
    {
        buffer = ( char* )( siCommonMemoryAlloc( NULL, fileSize ) );
        if( buffer == NULL )
        {
            siFileClose( fileContext, file );
            siFileDestroyContext( fileContext );
            
            SCT_LOG_ERROR( "File buffer alloc failed in LoadData@OpenVG action execute." );
            return SCT_FALSE;
        }
    }

    if( siFileRead( fileContext, file, buffer, fileSize ) != ( unsigned int )( fileSize ) )
    {
        if( rawData == NULL )
        {
            siCommonMemoryFree( NULL, buffer );
        }
        siFileClose( fileContext, file );
        siFileDestroyContext( fileContext );

        SCT_LOG_ERROR( "File read failed in LoadData@OpenVG action execute." );
        return SCT_FALSE;
    }

    siFileClose( fileContext, file );
    siFileDestroyContext( fileContext );

    if( rawData == NULL )
    {
        rawData = sctOpenVGCreateData( buffer, fileSize, SCT_FALSE );
        if( rawData == NULL )
        {
            siCommonMemoryFree( NULL, buffer );
            SCT_LOG_ERROR( "Data creation failed in LoadData@OpenVG action execute." );
            return SCT_FALSE;
        }

        sctiOpenVGModuleSetData( context->moduleContext, data->dataIndex, rawData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGLoadDataActionTerminate( SCTAction* action )
{
    SCTOpenVGLoadDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGLoadDataActionContext* )( action->context );

    sctiOpenVGModuleDeleteData( context->moduleContext, context->data.dataIndex );
}

/*!
 *
 *
 */
void sctiOpenVGLoadDataActionDestroy( SCTAction* action )
{
    SCTOpenVGLoadDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGLoadDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenVGLoadDataActionContext( context );
}
