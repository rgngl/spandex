/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgpaintcolorrampstopsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGPaintColorRampStopsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGPaintColorRampStopsActionContext*  context;
    SCTOpenVGModuleContext*                     mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGPaintColorRampStopsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGPaintColorRampStopsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PaintColorRampStops@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGPaintColorRampStopsActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGPaintColorRampStopsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPaintColorRampStopsActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPaintIndex( context->moduleContext, context->data.paintIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPaintColorRampStopsActionContext( context );
        SCT_LOG_ERROR( "Paint index out of range in PaintColorRampStops@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGPaintColorRampStopsActionContext( void* context )
{
    SCTOpenVGPaintColorRampStopsActionContext*  c;

    if( context == NULL )
    {
        return;
    }
    c = ( SCTOpenVGPaintColorRampStopsActionContext* )( context );
    if( c->data.stops != NULL )
    {
        sctDestroyFloatVector( c->data.stops );
        c->data.stops = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGPaintColorRampStopsActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGPaintColorRampStopsActionContext*  context;
    OpenVGPaintColorRampStopsActionData*        data;
    VGErrorCode                                 err;
    SCTOpenVGPaint*                             sctPaint;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGPaintColorRampStopsActionContext* )( action->context );
    data    = ( OpenVGPaintColorRampStopsActionData* )( &( context->data ) );

    sctPaint = sctiOpenVGModuleGetPaint( context->moduleContext, data->paintIndex );
    if( sctPaint == VG_INVALID_HANDLE )
    {
        SCT_LOG_ERROR( "Invalid paint in PaintColorRampStops@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT_ALWAYS( sctPaint->paint != VG_INVALID_HANDLE );
       
    vgSetParameterfv( sctPaint->paint, VG_PAINT_COLOR_RAMP_STOPS, data->stops->length, data->stops->data );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in PaintColorRampStops@OpenVG action execution.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGPaintColorRampStopsActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGPaintColorRampStopsActionContext( action->context );
    action->context = NULL;
}




