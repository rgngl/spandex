/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgmovetodirectaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGMoveToDirectActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGMoveToDirectActionContext* context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGMoveToDirectActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGMoveToDirectActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in MoveToDirect@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGMoveToDirectActionContext ) );

    context->moduleContext = mc;
    context->pathType      = ( VGPathDatatype )( -1 );

    if( sctiParseOpenVGMoveToDirectActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGMoveToDirectActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGMoveToDirectActionContext( context );
        SCT_LOG_ERROR( "Invalid path index in MoveToDirect@OpenVG context creation." );
        return NULL;
    }

    if( context->data.relative == SCT_FALSE )
    {
        context->pathSegments[ 0 ] = VG_MOVE_TO_ABS;
    }
    else
    {
        context->pathSegments[ 0 ] = VG_MOVE_TO_REL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGMoveToDirectActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}


/*!
 *
 *
 */
SCTBoolean sctiOpenVGMoveToDirectActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGMoveToDirectActionContext* context;
    OpenVGMoveToDirectActionData*       data;
    SCTOpenVGPath*                      sctPath;
    VGErrorCode                         err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGMoveToDirectActionContext* )( action->context );
    data    = &( context->data );

    sctPath = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( sctPath == NULL )
    {
        SCT_LOG_ERROR( "Invalid path in MoveToDirect@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctPath->path != VG_INVALID_HANDLE );

    if( context->pathType != sctPath->type )
    {
        context->pathType = sctPath->type;
        if( sctOpenVGConvertDoubleArray( context->pathData, 
                                         context->pathType,
                                         context->data.point, 
                                         2 ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Coordinate data out of path datatype range in MoveToDirect@OpenVG action execute." );
            return SCT_FALSE;
        }
    }

    vgAppendPathData( sctPath->path, 1, context->pathSegments, context->pathData );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in MoveToDirect@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGMoveToDirectActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGMoveToDirectActionContext( action->context );
    action->context = NULL;
}
