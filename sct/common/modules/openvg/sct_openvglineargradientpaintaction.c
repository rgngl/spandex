/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvglineargradientpaintaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGLinearGradientPaintActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGLinearGradientPaintActionContext*  context;
    SCTOpenVGModuleContext*                     mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGLinearGradientPaintActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGLinearGradientPaintActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LinearGradientPaint@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGLinearGradientPaintActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGLinearGradientPaintActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLinearGradientPaintActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPaintIndex( context->moduleContext, context->data.paintIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLinearGradientPaintActionContext( context );
        SCT_LOG_ERROR( "Paint index out of range in LinearGradientPaint@OpenVG context creation." );
        return NULL;
    } 

    context->linearGradient[ 0 ] = context->data.point0[ 0 ];
    context->linearGradient[ 1 ] = context->data.point0[ 1 ];
    context->linearGradient[ 2 ] = context->data.point1[ 0 ];
    context->linearGradient[ 3 ] = context->data.point1[ 1 ];

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGLinearGradientPaintActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGLinearGradientPaintActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGLinearGradientPaintActionContext*  context;
    OpenVGLinearGradientPaintActionData*        data;
    SCTOpenVGPaint*                             sctPaint;
    VGErrorCode                                 err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGLinearGradientPaintActionContext* )( action->context );
    data    = &( context->data );

    sctPaint = sctiOpenVGModuleGetPaint( context->moduleContext, data->paintIndex );
    if( sctPaint == NULL )
    {
        SCT_LOG_ERROR( "Invalid paint in LinearGradientPaint@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctPaint->paint != VG_INVALID_HANDLE );

    vgSetParameteri( sctPaint->paint, VG_PAINT_TYPE, VG_PAINT_TYPE_LINEAR_GRADIENT );
    vgSetParameterfv( sctPaint->paint, VG_PAINT_LINEAR_GRADIENT, SCT_ARRAY_LENGTH( context->linearGradient ), context->linearGradient );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in LinearGradientPaint@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGLinearGradientPaintActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGLinearGradientPaintActionContext( action->context );
    action->context = NULL;
}




