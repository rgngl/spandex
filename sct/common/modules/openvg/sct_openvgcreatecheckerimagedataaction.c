/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatecheckerimagedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateCheckerImageDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateCheckerImageDataActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenVGCreateCheckerImageDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateCheckerImageDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateCheckerImageData@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateCheckerImageDataActionContext ) );

    context->moduleContext = ( SCTOpenVGModuleContext* )( moduleContext );

    if( sctiParseOpenVGCreateCheckerImageDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateCheckerImageDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageDataIndex( context->moduleContext, context->data.imageDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateCheckerImageDataActionContext( context );
        SCT_LOG_ERROR( "Invalid image data index in CreateCheckerImageData@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateCheckerImageDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateCheckerImageDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateCheckerImageDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGCreateCheckerImageDataActionContext*   context;
    OpenVGCreateCheckerImageDataActionData*         data;
    SCTOpenVGImageData*                             imageData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGCreateCheckerImageDataActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenVGModuleGetImageData( context->moduleContext, data->imageDataIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Image data index already used in CreateCheckerImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    imageData = sctOpenVGCreateCheckerImageData( data->format, 
                                                 data->width, 
                                                 data->height, 
                                                 data->color1, 
                                                 data->color2, 
                                                 data->checkersX, 
                                                 data->checkersY );
    if( imageData == NULL )
    {
        SCT_LOG_ERROR( "Image data creation failed in CreateCheckerImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleSetImageData( context->moduleContext, data->imageDataIndex, imageData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreateCheckerImageDataActionTerminate( SCTAction* action )
{
    SCTOpenVGCreateCheckerImageDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGCreateCheckerImageDataActionContext* )( action->context );

    sctiOpenVGModuleDeleteImageData( context->moduleContext, context->data.imageDataIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreateCheckerImageDataActionDestroy( SCTAction* action )
{
    SCTOpenVGCreateCheckerImageDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGCreateCheckerImageDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenVGCreateCheckerImageDataActionContext( context );
}
