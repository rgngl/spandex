/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_VG_H__ )
#define __SCT_VG_H__

#if defined( SCT_KHR_COMPLIANT_INCLUDES )
# include <VG/openvg.h>
#else
# include <openvg.h>
#endif

#if defined( SCT_DEPRECATED_OPENVG_1_0 )
# define VG_sXRGB_8888                                  0 | (1 << 6)
# define VG_sARGB_8888                                  1 | (1 << 6)
# define VG_sARGB_8888_PRE                              2 | (1 << 6)
# define VG_sARGB_1555                                  4 | (1 << 6)
# define VG_sARGB_4444                                  5 | (1 << 6)
# define VG_lXRGB_8888                                  7 | (1 << 6)
# define VG_lARGB_8888                                  8 | (1 << 6)
# define VG_lARGB_8888_PRE                              9 | (1 << 6)
# define VG_sBGRX_8888                                  0 | (1 << 7)
# define VG_sBGRA_8888                                  1 | (1 << 7)
# define VG_sBGRA_8888_PRE                              2 | (1 << 7)
# define VG_sBGR_565                                    3 | (1 << 7)
# define VG_sBGRA_5551                                  4 | (1 << 7)
# define VG_sBGRA_4444                                  5 | (1 << 7)
# define VG_lBGRX_8888                                  7 | (1 << 7)
# define VG_lBGRA_8888                                  8 | (1 << 7)
# define VG_lBGRA_8888_PRE                              9 | (1 << 7)
# define VG_sXBGR_8888                                  0 | (1 << 6) | (1 << 7)
# define VG_sABGR_8888                                  1 | (1 << 6) | (1 << 7)
# define VG_sABGR_8888_PRE                              2 | (1 << 6) | (1 << 7)
# define VG_sABGR_1555                                  4 | (1 << 6) | (1 << 7)
# define VG_sABGR_4444                                  5 | (1 << 6) | (1 << 7)
# define VG_lXBGR_8888                                  7 | (1 << 6) | (1 << 7)
# define VG_lABGR_8888                                  8 | (1 << 6) | (1 << 7)
# define VG_lABGR_8888_PRE                              9 | (1 << 6) | (1 << 7)
# define VG_STROKE_DASH_PHASE_RESET                     0x1116
# define VG_MAX_GAUSSIAN_STD_DEVIATION                  0x116A
#endif  /* defined( SCT_DEPRECATED_OPENVG_1_0 ) */

#if !defined( OPENVG_VERSION_1_1 )
# define VG_A_1                                         13
# define VG_A_4                                         14
# define VG_GLYPH_ORIGIN                                0x1122
# define VG_COLOR_TRANSFORM                             0x1170
# define VG_COLOR_TRANSFORM_VALUES                      0x1171
# define VG_MATRIX_GLYPH_USER_TO_SURFACE                0x1404
#endif  /* !defined( OPENVG_VERSION_1_1 ) */

#endif /* !defined( __SCT_VG_H__ ) */
