/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatefontaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateFontActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateFontActionContext*   context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCreateFontActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateFontActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateFont@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateFontActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCreateFontActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateFontActionContext( context );
        return NULL;
    }

#if defined( OPENVG_VERSION_1_1 )
    if( sctiOpenVGModuleIsValidFontIndex( context->moduleContext, context->data.fontIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateFontActionContext( context );
        SCT_LOG_ERROR( "Font index out of range in CreateFont@OpenVG context creation." );
        return NULL;
    } 
#endif  /* !defined( OPENVG_VERSION_1_1 ) */

    return context;   
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateFontActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateFontActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark );

#if !defined( OPENVG_VERSION_1_1 )
    SCT_LOG_ERROR( "CreateFont@OpenVG not supported in OpenVG 1.0." );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */   
    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */    
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateFontActionExecute( SCTAction* action, int framenumber )
{
#if !defined( OPENVG_VERSION_1_1 )
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCTOpenVGCreateFontActionContext*   context;
    OpenVGCreateFontActionData*         data;
    int                                 index;
    SCTOpenVGFont*                      sctFont;
    VGFont                              vgFont;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGCreateFontActionContext* )( action->context );
    data    = &( context->data );

    index = data->fontIndex;
    if( sctiOpenVGModuleGetFont( context->moduleContext, index ) != NULL )
    {
        SCT_LOG_ERROR( "Font index already in use in CreateFont@OpenVG action execute." );
        return SCT_FALSE;
    }

    vgFont = vgCreateFont( data->glyphCapacityHint );
    if( vgFont == VG_INVALID_HANDLE )
    {
        SCT_LOG_ERROR( "VGFont creation failed in CreateFont@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctFont = sctOpenVGCreateFont( vgFont );
    if( sctFont == NULL )
    {
        vgDestroyFont( vgFont );
        SCT_LOG_ERROR( "Allocation in CreateFont@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleSetFont( context->moduleContext, index, sctFont );

    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
}

/*!
 *
 *
 */
void sctiOpenVGCreateFontActionTerminate( SCTAction* action )
{
#if !defined( OPENVG_VERSION_1_1 )
    SCT_USE_VARIABLE( action );
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCTOpenVGCreateFontActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGCreateFontActionContext* )( action->context );

    sctiOpenVGModuleDeleteFont( context->moduleContext, context->data.fontIndex );
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
}

/*!
 *
 *
 */
void sctiOpenVGCreateFontActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCreateFontActionContext( action->context );
    action->context = NULL;
}
