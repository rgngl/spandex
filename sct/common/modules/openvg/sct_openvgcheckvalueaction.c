/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcheckvalueaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
typedef enum
{
    SCT_ELEMENT_TYPE_Z,
    SCT_ELEMENT_TYPE_R,
} SCTElementType;

/*!
 *
 *
 */
static const int elements[] = { VG_MAX_SCISSOR_RECTS,              1, SCT_ELEMENT_TYPE_Z,
                                VG_MAX_DASH_COUNT,                 1, SCT_ELEMENT_TYPE_Z,
                                VG_MAX_KERNEL_SIZE,                1, SCT_ELEMENT_TYPE_Z,
                                VG_MAX_SEPARABLE_KERNEL_SIZE,      1, SCT_ELEMENT_TYPE_Z,
                                VG_MAX_COLOR_RAMP_STOPS,           1, SCT_ELEMENT_TYPE_Z,
                                VG_MAX_IMAGE_WIDTH,                1, SCT_ELEMENT_TYPE_Z,
                                VG_MAX_IMAGE_HEIGHT,               1, SCT_ELEMENT_TYPE_Z,
                                VG_MAX_IMAGE_PIXELS,               1, SCT_ELEMENT_TYPE_Z,
                                VG_MAX_IMAGE_BYTES,                1, SCT_ELEMENT_TYPE_Z,
                                VG_MAX_GAUSSIAN_STD_DEVIATION,     1, SCT_ELEMENT_TYPE_Z };

/*!
 *
 *
 */
static SCTBoolean sctiPassesCondition( float v1, float v2, SCTCondition c );

/*!
 *
 *
 */
void* sctiCreateOpenVGCheckValueActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCheckValueActionContext*   context;
    int                                 i;
    int                                 len;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenVGCheckValueActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCheckValueActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CheckValue@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCheckValueActionContext ) );

    if( sctiParseOpenVGCheckValueActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCheckValueActionContext( context );
        return NULL;
    }

    for( i = 0; i < SCT_ARRAY_LENGTH( elements ); i += 3 )
    {
        if( elements[ i ] == context->data.value )
        {
            len = elements[ i + 1 ];

            if( len == 0 )
            {
                /* Special cases that need to query number of elements. */
                if( context->data.condition != SCT_FOUND )
                {
                    SCT_LOG_ERROR( "Invalid condition in CheckValue@OpenVG action context creation." );                
                    sctiDestroyOpenVGCheckValueActionContext( context );
                    return NULL;
                }
                
                break;
            }
            else if( len != context->data.values->length )
            {
                SCT_LOG_ERROR( "Values does not match value in CheckValue@OpenVG action context creation." );                
                sctiDestroyOpenVGCheckValueActionContext( context );
                return NULL;
            }
            else
            {
                if( context->data.condition == SCT_FOUND )
                {
                    SCT_LOG_ERROR( "Unsupported condition in CheckValue@OpenVG action context creation." );                
                    sctiDestroyOpenVGCheckValueActionContext( context );
                    return NULL;
                }
                
                break;
            }
        }
    }

    if( i >= SCT_ARRAY_LENGTH( elements ) )
    {
        SCT_LOG_ERROR( "Unexpected value in CheckValue@OpenVG action context creation." );                
        sctiDestroyOpenVGCheckValueActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCheckValueActionContext( void* context )
{
    SCTOpenVGCheckValueActionContext*    c;
   
    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenVGCheckValueActionContext* )( context );
    
    if( c->data.values != NULL )
    {
        sctDestroyFloatVector( c->data.values );
        c->data.values = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCheckValueActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGCheckValueActionContext*   context;
    OpenVGCheckValueActionData*         data;
    VGint                               integerv[ 2 ];
    int                                 i;
    int                                 j;
    VGParamType                         element;
    int                                 len;
    int                                 type;
    SCTBoolean                          s;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGCheckValueActionContext* )( action->context );
    data    = &( context->data );
   
    for( i = 0; i < SCT_ARRAY_LENGTH( elements ); i += 3 )
    {
        if( elements[ i ] == context->data.value )
        {
            element = ( VGParamType )( elements[ i ] );
            len     = elements[ i + 1 ];
            type    = elements[ i + 2 ];

            switch( type )
            {
            case SCT_ELEMENT_TYPE_Z:
                vgGetiv( element, len, integerv );
                break;
                    
            default:
                SCT_ASSERT_ALWAYS( 0 );
                break;
            }
            
            s = SCT_TRUE;
            for( j = 0; j < len; ++j )
            {
                switch( type )
                {
                case SCT_ELEMENT_TYPE_Z:
                    if( sctiPassesCondition( ( float )( integerv[ j ] ), data->values->data[ j ], data->condition ) == SCT_FALSE )
                    {
                        s = SCT_FALSE;
                    }
                    break;
                    
                default:
                    SCT_ASSERT_ALWAYS( 0 );
                    break;
                }
            }

            if( s == SCT_FALSE )
            {
                SCT_LOG_ERROR( "Condition failed in CheckValue@OpenVG action execute." );
                return SCT_FALSE;
            }

            break;
        }
    }

    /* Should not come here without founding matching element. */
    SCT_ASSERT_ALWAYS( i < SCT_ARRAY_LENGTH( elements ) );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCheckValueActionDestroy( SCTAction* action )
{
    SCTOpenVGCheckValueActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGCheckValueActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenVGCheckValueActionContext( context );
}


/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static SCTBoolean sctiPassesCondition( float v1, float v2, SCTCondition c )
{
    switch( c )
    {
    case SCT_LESS:
        if( v1 < v2 )
        {
            return SCT_TRUE;
        }
        break;
        
    case SCT_LEQUAL:
        if( v1 <= v2 )
        {
            return SCT_TRUE;
        }
        break;
        
    case SCT_GREATER:
        if( v1 > v2 )
        {
            return SCT_TRUE;
        }
        break;

    case SCT_GEQUAL:
        if( v1 >= v2 )
        {
            return SCT_TRUE;
        }
        break;
        
    case SCT_EQUAL:
        if( v1 == v2 )
        {
            return SCT_TRUE;
        }
        break;

    case SCT_NOTEQUAL:
        if( v1 != v2 )
        {
            return SCT_TRUE;
        }
        break;

    default:
        break;
    }

    return SCT_FALSE;
}
