/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgrendertomaskaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGRenderToMaskActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGRenderToMaskActionContext* context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGRenderToMaskActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGRenderToMaskActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in RenderToMask@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGRenderToMaskActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGRenderToMaskActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGRenderToMaskActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGRenderToMaskActionContext( context );
        SCT_LOG_ERROR( "Invalid path index in RenderToMask@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGRenderToMaskActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGRenderToMaskActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( OPENVG_VERSION_1_1 )
    SCT_LOG_ERROR( "RenderToMask@OpenVG not supported in OpenVG 1.0." );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */  
    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */    
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGRenderToMaskActionExecute( SCTAction* action, int frameNumber )
{
#if !defined( OPENVG_VERSION_1_1 )
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCTOpenVGRenderToMaskActionContext* context;
    OpenVGRenderToMaskActionData*       data;
    VGErrorCode                         err;
    SCTOpenVGPath*                      path;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGRenderToMaskActionContext* )( action->context );
    data    = &( context->data );

    path = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( path == NULL )
    {
        SCT_LOG_ERROR( "Invalid path in RenderToMask@OpenVG action execute." );
        return SCT_FALSE;
    }

    SCT_ASSERT( path->path != VG_INVALID_HANDLE );

    vgRenderToMask( path->path, data->paintModes, data->operation );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in RenderToMask@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
}

/*!
 *
 *
 */
void sctiOpenVGRenderToMaskActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenVGRenderToMaskActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGRenderToMaskActionContext( action->context );
    action->context = NULL;
}

