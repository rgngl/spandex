/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdestroypaintaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGDestroyPaintActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDestroyPaintActionContext* context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGDestroyPaintActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDestroyPaintActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyPaint@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDestroyPaintActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGDestroyPaintActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyPaintActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPaintIndex( context->moduleContext, context->data.paintIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyPaintActionContext( context );
        SCT_LOG_ERROR( "Paint index out of range in DestroyPaint@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDestroyPaintActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDestroyPaintActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGDestroyPaintActionContext* context;
    OpenVGDestroyPaintActionData*       data;
    int                                 index;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGDestroyPaintActionContext* )( action->context );
    data    = &( context->data );

    index = data->paintIndex;
    if( sctiOpenVGModuleGetPaint( context->moduleContext, index ) == NULL )
    {
        SCT_LOG_ERROR( "Invalid paint in DestroyPaint@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleDeletePaint( context->moduleContext, index );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGDestroyPaintActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGDestroyPaintActionContext( action->context );
    action->context = NULL;
}
