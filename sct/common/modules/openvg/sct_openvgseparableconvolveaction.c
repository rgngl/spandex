/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgseparableconvolveaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGSeparableConvolveActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGSeparableConvolveActionContext*    context;
    SCTOpenVGModuleContext*                     mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGSeparableConvolveActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGSeparableConvolveActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SeparableConvolve@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGSeparableConvolveActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGSeparableConvolveActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSeparableConvolveActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.dstImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSeparableConvolveActionContext( context );
        SCT_LOG_ERROR( "Destination image index out of range in SeparableConvolve@OpenVG action context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.srcImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSeparableConvolveActionContext( context );
        SCT_LOG_ERROR( "Source image index out of range in SeparableConvolve@OpenVG action context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGSeparableConvolveActionContext( void* context )
{
    SCTOpenVGSeparableConvolveActionContext*    c;

    c = ( SCTOpenVGSeparableConvolveActionContext* )( context );

    if( c == NULL )
    {
        return;
    }
    
    if( c->data.kernelX != NULL )
    {
        sctDestroyIntVector( c->data.kernelX );
        c->data.kernelX = NULL;
    }
    if( c->data.kernelY != NULL )
    {
        sctDestroyIntVector( c->data.kernelY );
        c->data.kernelY = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSeparableConvolveActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGSeparableConvolveActionContext*    context;
    OpenVGSeparableConvolveActionData*          data;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGSeparableConvolveActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT_ALWAYS( ( context->kernelX == NULL && context->kernelY == NULL ) ||
                       ( context->kernelX != NULL && context->kernelY != NULL ) );

    if( context->kernelX == NULL )
    {
        context->kernelX = ( VGshort* )( siCommonMemoryAlloc( NULL, data->kernelX->length * sizeof( VGshort ) ) );
        context->kernelY = ( VGshort* )( siCommonMemoryAlloc( NULL, data->kernelY->length * sizeof( VGshort ) ) );
        if( context->kernelX == NULL || context->kernelY == NULL )
        {
            sctiOpenVGSeparableConvolveActionTerminate( action );
            SCT_LOG_ERROR( "Allocation failed in SeparableConvolve@OpenVG action init." );
            return SCT_FALSE;
        }
        if( sctOpenVGConvertTypeArray( context->kernelX, 
                                       VG_PATH_DATATYPE_S_16, 
                                       data->kernelX->data, 
                                       VG_PATH_DATATYPE_S_32, 
                                       data->kernelX->length ) == SCT_FALSE )
        {
            sctiOpenVGSeparableConvolveActionTerminate( action );
            SCT_LOG_ERROR( "Invalid kernelX data in SeparableConvolve@OpenVG action init." );
            return SCT_FALSE;
        }
        if( sctOpenVGConvertTypeArray( context->kernelY, 
                                       VG_PATH_DATATYPE_S_16, 
                                       data->kernelY->data, 
                                       VG_PATH_DATATYPE_S_32, 
                                       data->kernelY->length ) == SCT_FALSE )
        {
            sctiOpenVGSeparableConvolveActionTerminate( action );
            SCT_LOG_ERROR( "Invalid kernelY data in SeparableConvolve@OpenVG action init." );
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSeparableConvolveActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGSeparableConvolveActionContext*    context;
    OpenVGSeparableConvolveActionData*          data;
    VGErrorCode                                 err;
    SCTOpenVGImage*                             dstImage;
    SCTOpenVGImage*                             srcImage;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGSeparableConvolveActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT( context->kernelX != NULL && context->kernelY != NULL );

    dstImage = sctiOpenVGModuleGetImage( context->moduleContext, data->dstImageIndex );
    if( dstImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid destination image in SeparableConvolve@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( dstImage->image != VG_INVALID_HANDLE );

    srcImage = sctiOpenVGModuleGetImage( context->moduleContext, data->srcImageIndex );
    if( srcImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid source image in SeparableConvolve@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( srcImage->image != VG_INVALID_HANDLE );

    vgSeparableConvolve( dstImage->image, srcImage->image,
                         data->kernelWidth, data->kernelHeight,
                         data->shiftX, data->shiftY,
                         context->kernelX,
                         context->kernelY,
                         data->scale,
                         data->bias,
                         data->tilingMode );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in SeparableConvolve@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGSeparableConvolveActionTerminate( SCTAction* action )
{
    SCTOpenVGSeparableConvolveActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTOpenVGSeparableConvolveActionContext* )( action->context );

    if( context->kernelX != NULL )
    {
        siCommonMemoryFree( NULL, context->kernelX );
        context->kernelX = NULL;
    }

    if( context->kernelY != NULL )
    {
        siCommonMemoryFree( NULL, context->kernelY );
        context->kernelY = NULL;
    }
}

/*!
 *
 *
 */
void sctiOpenVGSeparableConvolveActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGSeparableConvolveActionContext( action->context );
    action->context = NULL;
}
