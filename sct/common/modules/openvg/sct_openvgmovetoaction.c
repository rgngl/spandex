/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgmovetoaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGMoveToActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGMoveToActionContext*   context;
    SCTOpenVGModuleContext*         mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGMoveToActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGMoveToActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in MoveTo@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGMoveToActionContext ) );

    context->moduleContext = mc;
    context->pathType      = ( VGPathDatatype )( -1 );

    if( sctiParseOpenVGMoveToActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGMoveToActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathDataIndex( context->moduleContext, context->data.pathDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGMoveToActionContext( context );
        SCT_LOG_ERROR( "Invalid path index in MoveTo@OpenVG context creation." );
        return NULL;
    }

    if( context->data.relative == SCT_FALSE )
    {
        context->pathSegments[ 0 ] = VG_MOVE_TO_ABS;
    }
    else
    {
        context->pathSegments[ 0 ] = VG_MOVE_TO_REL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGMoveToActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}


/*!
 *
 *
 */
SCTBoolean sctiOpenVGMoveToActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGMoveToActionContext*   context;
    OpenVGMoveToActionData*         data;
    SCTOpenVGPathData*              pathData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGMoveToActionContext* )( action->context );
    data    = &( context->data );

    pathData = sctiOpenVGModuleGetPathData( context->moduleContext, data->pathDataIndex );
    if( pathData == NULL )
    {
        SCT_LOG_ERROR( "Invalid path data in MoveTo@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( context->pathType != pathData->type )
    {
        context->pathType = pathData->type;
        if( sctOpenVGConvertDoubleArray( context->pathData, 
                                         context->pathType,
                                         context->data.point, 
                                         2 ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Coordinate data out of path datatype range in MoveTo@OpenVG action execute." );
            return SCT_FALSE;
        }
    }

    if( sctOpenVGAddPathData( pathData, 
                              context->pathSegments, 1, 
                              context->pathData, 2, 
                              context->pathType ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Path data append failed in MoveTo@OpenVG action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGMoveToActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGMoveToActionContext( action->context );
    action->context = NULL;
}
