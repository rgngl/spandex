/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcheckextensionaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCheckExtensionActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCheckExtensionActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenVGCheckExtensionActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCheckExtensionActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CheckExtension@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCheckExtensionActionContext ) );

    if( sctiParseOpenVGCheckExtensionActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCheckExtensionActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCheckExtensionActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCheckExtensionActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGCheckExtensionActionContext*   context;
    const char*                             str;
    const char*                             s;
    int                                     len;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGCheckExtensionActionContext* )( action->context );

    /* Get VG extension string. */
    str = ( const char* )( vgGetString( VG_EXTENSIONS ) );
    if( str == NULL )
    {
        SCT_LOG_ERROR( "Getting extension string failed in CheckExtension@OpenVG action execute." );
        return SCT_FALSE;
    }

    len = strlen( context->data.extension );
    
    if( ( s = strstr( str, context->data.extension ) ) == NULL ||
        ( s[ len ] != ' ' &&
          s[ len ] != '\0' ) )
    {
        char buf[ 1024 ];
        sprintf( buf, "Extension %s not found in CheckExtension@OpenVG action execute.", context->data.extension );
        SCT_LOG_ERROR( buf );

        return SCT_FALSE;
    }  

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCheckExtensionActionDestroy( SCTAction* action )
{
    SCTOpenVGCheckExtensionActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGCheckExtensionActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenVGCheckExtensionActionContext( context );
}

