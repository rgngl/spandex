/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgappendpathaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGAppendPathActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGAppendPathActionContext*   context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGAppendPathActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGAppendPathActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in AppendPath@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGAppendPathActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGAppendPathActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGAppendPathActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.dstPathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGAppendPathActionContext( context );
        SCT_LOG_ERROR( "Invalid destination path index in AppendPath@OpenVG context creation." );
        return NULL;
    }
    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.srcPathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGAppendPathActionContext( context );
        SCT_LOG_ERROR( "Invalid source path index in AppendPath@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGAppendPathActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGAppendPathActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGAppendPathActionContext*   context;
    OpenVGAppendPathActionData*         data;
    VGErrorCode                         err;
    SCTOpenVGPath*                      dstPath;
    SCTOpenVGPath*                      srcPath;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGAppendPathActionContext* )( action->context );
    data    = &( context->data );

    dstPath = sctiOpenVGModuleGetPath( context->moduleContext, data->dstPathIndex );
    if( dstPath == NULL )
    {
        SCT_LOG_ERROR( "Invalid destination path in AppendPath@OpenVG action execute." );
        return SCT_FALSE;
    }
    srcPath = sctiOpenVGModuleGetPath( context->moduleContext, data->srcPathIndex );
    if( srcPath == NULL )
    {
        SCT_LOG_ERROR( "Invalid source path in AppendPath@OpenVG action execute." );
        return SCT_FALSE;
    }

    SCT_ASSERT( dstPath->path != VG_INVALID_HANDLE );
    SCT_ASSERT( srcPath->path != VG_INVALID_HANDLE );

    vgAppendPath( dstPath->path, srcPath->path );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in AppendPath@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGAppendPathActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGAppendPathActionContext( action->context );
    action->context = NULL;
}
