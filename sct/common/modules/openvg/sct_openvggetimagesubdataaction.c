/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvggetimagesubdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGGetImageSubDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGGetImageSubDataActionContext*  context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGGetImageSubDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGGetImageSubDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in GetImageSubData@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGGetImageSubDataActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGGetImageSubDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGGetImageSubDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.srcImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGGetImageSubDataActionContext( context );
        SCT_LOG_ERROR( "Source image index out of range in GetImageSubData@OpenVG context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidImageDataIndex( context->moduleContext, context->data.dstImageDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGGetImageSubDataActionContext( context );
        SCT_LOG_ERROR( "Destination image data index out of range in GetImageSubData@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGGetImageSubDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGGetImageSubDataActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGGetImageSubDataActionContext*  context;
    OpenVGGetImageSubDataActionData*        data;
    SCTOpenVGImage*                         sctImage;
    SCTOpenVGImageData*                     imageDataElement;
    VGErrorCode                             err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGGetImageSubDataActionContext* )( action->context );
    data    = &( context->data );

    sctImage = sctiOpenVGModuleGetImage( context->moduleContext, data->srcImageIndex );
    if( sctImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid source image index in GetImageSubData@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctImage->image != VG_INVALID_HANDLE );

    imageDataElement = sctiOpenVGModuleGetImageData( context->moduleContext, data->dstImageDataIndex );
    if( imageDataElement == NULL )
    {
        SCT_LOG_ERROR( "Invalid destination image data index in GetImageSubData@OpenVG action execute." );
        return SCT_FALSE;
    }

    if( imageDataElement->width < data->width || imageDataElement->height < data->height )
    {
        SCT_LOG_ERROR( "Incompatible source image data in GetImageSubData@OpenVG action execute." );
        return SCT_FALSE;
    }

    vgGetImageSubData( sctImage->image,
                       imageDataElement->data,
                       imageDataElement->stride,
                       imageDataElement->format,
                       data->x,
                       data->y,
                       data->width,
                       data->height );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in GetImageSubData@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGGetImageSubDataActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGGetImageSubDataActionContext( action->context );
    action->context = NULL;
}
