/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgsetglyphtoimageaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGSetGlyphToImageActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGSetGlyphToImageActionContext*  context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGSetGlyphToImageActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGSetGlyphToImageActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SetGlyphToImage@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGSetGlyphToImageActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGSetGlyphToImageActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSetGlyphToImageActionContext( context );
        return NULL;
    }

#if defined( OPENVG_VERSION_1_1 )
    if( sctiOpenVGModuleIsValidFontIndex( context->moduleContext, context->data.fontIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSetGlyphToImageActionContext( context );
        SCT_LOG_ERROR( "Font index out of range in SetGlyphToImage@OpenVG context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.imageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSetGlyphToImageActionContext( context );
        SCT_LOG_ERROR( "Image index out of range in SetGlyphToImage@OpenVG context creation." );
        return NULL;
    } 
#endif  /* defined( OPENVG_VERSION_1_1 ) */

    return context;    
}

/*!
 *
 *
 */
void sctiDestroyOpenVGSetGlyphToImageActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSetGlyphToImageActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( OPENVG_VERSION_1_1 )
    SCT_LOG_ERROR( "SetGlyphToImage@OpenVG not supported in OpenVG 1.0." );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */  
    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */    
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSetGlyphToImageActionExecute( SCTAction* action, int framenumber )
{
#if !defined( OPENVG_VERSION_1_1 )
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCTOpenVGSetGlyphToImageActionContext*  context;
    OpenVGSetGlyphToImageActionData*        data;
    SCTOpenVGFont*                          font;
    SCTOpenVGImage*                         image;
    VGErrorCode                             err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGSetGlyphToImageActionContext* )( action->context );
    data    = &( context->data );

    font = sctiOpenVGModuleGetFont( context->moduleContext, data->fontIndex );
    if( font == NULL )
    {
        SCT_LOG_ERROR( "Invalid font in SetGlyphToPath@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( font->font != VG_INVALID_HANDLE );

    image = sctiOpenVGModuleGetImage( context->moduleContext, data->imageIndex );
    if( image == NULL )
    {
        SCT_LOG_ERROR( "Invalid image in SetGlyphToImage@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( image->image != VG_INVALID_HANDLE );

    vgSetGlyphToImage( font->font,
                       data->glyphIndex,
                       image->image,
                       data->glyphOrigin,
                       data->escapement );

# if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in SetGlyphToImage@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
# else  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
# endif /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
}

/*!
 *
 *
 */
void sctiOpenVGSetGlyphToImageActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenVGSetGlyphToImageActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGSetGlyphToImageActionContext( action->context );
    action->context = NULL;
}
