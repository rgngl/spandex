/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgscubicbeziersdirectaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGSCubicBeziersDirectActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGSCubicBeziersDirectActionContext*  context;
    SCTOpenVGModuleContext*                     mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGSCubicBeziersDirectActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGSCubicBeziersDirectActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SCubicBeziersDirect@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGSCubicBeziersDirectActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGSCubicBeziersDirectActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSCubicBeziersDirectActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSCubicBeziersDirectActionContext( context );
        SCT_LOG_ERROR( "Path index out of range in SCubicBeziersDirect@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGSCubicBeziersDirectActionContext( void* context )
{
    SCTOpenVGSCubicBeziersDirectActionContext* c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenVGSCubicBeziersDirectActionContext* )( context );
    if( c->data.coordinates != NULL )
    {
        sctDestroyDoubleVector( c->data.coordinates );
        c->data.coordinates = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSCubicBeziersDirectActionInit( SCTAction *action, SCTBenchmark *benchmark )
{
    SCTOpenVGSCubicBeziersDirectActionContext*  context;
    OpenVGSCubicBeziersDirectActionData*        data;
    VGubyte                                     segment;
    int                                         i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGSCubicBeziersDirectActionContext* )( action->context );
    data    = &( context->data );

    context->pathType         = ( VGPathDatatype )( -1 );
    context->numOfSegments    = data->coordinates->length / 4;
    context->numOfCoordinates = data->coordinates->length;

    if( context->pathSegments == NULL )
    {
        context->pathSegments = ( VGubyte* )( siCommonMemoryAlloc( NULL, context->numOfSegments ) ); 
    }
    if( context->pathData == NULL )
    {
        context->pathData = siCommonMemoryAlloc( NULL, context->numOfCoordinates * sizeof( VGfloat ) );
    }
    
    if( context->pathSegments == NULL || context->pathData == NULL )
    {
        sctiOpenVGSCubicBeziersDirectActionTerminate( action );
        SCT_LOG_ERROR( "Allocation failed in SCubicBeziersDirect@OpenVG action init." );
        return SCT_FALSE;
    }

    if( data->relative == SCT_FALSE )
    {
        segment = VG_SCUBIC_TO_ABS;
    }
    else
    {
        segment = VG_SCUBIC_TO_REL;
    }

    for( i = 0; i < context->numOfSegments; ++i )
    {
        context->pathSegments[ i ] = segment;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSCubicBeziersDirectActionExecute( SCTAction *action, int frameNumber )
{
    SCTOpenVGSCubicBeziersDirectActionContext*  context;
    OpenVGSCubicBeziersDirectActionData*        data;
    SCTOpenVGPath*                              sctPath;
    VGErrorCode                                 err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGSCubicBeziersDirectActionContext* )( action->context );
    data    = &( context->data );

    sctPath = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( sctPath == NULL )
    {
        SCT_LOG_ERROR( "Invalid path in SCubicBeziersDirect@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctPath->path != VG_INVALID_HANDLE );

    if( context->pathType != sctPath->type )
    {
        context->pathType = sctPath->type;
        if( sctOpenVGConvertDoubleArray( context->pathData, 
                                         context->pathType,
                                         context->data.coordinates->data, 
                                         context->numOfCoordinates ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Coordinate data out of path datatype range in SCubicBeziersDirect@OpenVG action execute." );
            return SCT_FALSE;
        }
    }

    vgAppendPathData( sctPath->path, context->numOfSegments, context->pathSegments, context->pathData );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in SCubicBeziersDirect@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGSCubicBeziersDirectActionTerminate( SCTAction* action )
{
    SCTOpenVGSCubicBeziersDirectActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGSCubicBeziersDirectActionContext* )( action->context );

    if( context->pathSegments != NULL )
    {
        siCommonMemoryFree( NULL, context->pathSegments );
        context->pathSegments = NULL;
    }
    if( context->pathData != NULL )
    {
        siCommonMemoryFree( NULL, context->pathData );
        context->pathData = NULL;
    }
}

/*!
 *
 *
 */
void sctiOpenVGSCubicBeziersDirectActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGSCubicBeziersDirectActionContext( action->context );
    action->context = NULL;
}
