/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdrawtigeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgtiger.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGDrawTigerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDrawTigerActionContext*    context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGDrawTigerActionContext* )siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDrawTigerActionContext ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DrawTiger@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDrawTigerActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGDrawTigerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDrawTigerActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidTigerIndex( context->moduleContext, context->data.tigerIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDrawTigerActionContext( context );
        SCT_LOG_ERROR( "Invalid tiger index in DrawTiger@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDrawTigerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDrawTigerActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGDrawTigerActionContext*    context;
    OpenVGDrawTigerActionData*          data;
    void*                               tiger;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGDrawTigerActionContext* )( action->context );
    data    = &( context->data );

    tiger = sctiOpenVGModuleGetTiger( context->moduleContext, data->tigerIndex );
    if( tiger == NULL )
    {
        SCT_LOG_ERROR( "Invalid tiger index in DrawTiger@OpenVG action execute." );
        return SCT_FALSE;
    }

    return sctOpenVGDrawTiger( tiger, data->width, data->height, data->iterations );
}

/*!
 *
 *
 */
void sctiOpenVGDrawTigerActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGDrawTigerActionContext( action->context );
    action->context = NULL;
}

/*!
 *
 *
 */
SCTStringList* sctiOpenVGDrawTigerActionGetWorkload( SCTAction* action )
{
    SCTOpenVGDrawTigerActionContext*    context;
    SCTStringList*                      workload;
    char                                buf[ 32 ];

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGDrawTigerActionContext* )( action->context );
    
    workload = sctCreateStringList();
    if( workload == NULL )
    {
        return NULL;
    }

    sprintf( buf, "%d tigers", context->data.iterations );
    if( sctAddStringCopyToList( workload, buf ) == SCT_FALSE )
    {
        sctDestroyStringList( workload );
        return NULL;
    }
   
    return workload;
}
