/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatesolidimagedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateSolidImageDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateSolidImageDataActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenVGCreateSolidImageDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateSolidImageDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateSolidImageData@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateSolidImageDataActionContext ) );

    context->moduleContext = ( SCTOpenVGModuleContext* )( moduleContext );

    if( sctiParseOpenVGCreateSolidImageDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateSolidImageDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageDataIndex( context->moduleContext, context->data.imageDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateSolidImageDataActionContext( context );
        SCT_LOG_ERROR( "Invalid image data index in CreateSolidImageData@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateSolidImageDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateSolidImageDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateSolidImageDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGCreateSolidImageDataActionContext* context;
    OpenVGCreateSolidImageDataActionData*       data;
    SCTOpenVGImageData*                         imageData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGCreateSolidImageDataActionContext* )( action->context );
    data    = &( context->data );

    imageData = sctiOpenVGModuleGetImageData( context->moduleContext, data->imageDataIndex );
    if( imageData != NULL )
    {
        SCT_LOG_ERROR( "Image data index already used in CreateSolidImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    imageData = sctOpenVGCreateSolidImageData( data->format, 
                                               data->width, 
                                               data->height, 
                                               data->color );
    if( imageData == NULL )
    {
        SCT_LOG_ERROR( "Image data creation failed in CreateSolidImageData@OpenVG action execute." );
        return SCT_FALSE;
    }
    sctiOpenVGModuleSetImageData( context->moduleContext, data->imageDataIndex, imageData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreateSolidImageDataActionTerminate( SCTAction* action )
{
    SCTOpenVGCreateSolidImageDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGCreateSolidImageDataActionContext* )( action->context );

    sctiOpenVGModuleDeleteImageData( context->moduleContext, context->data.imageDataIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreateSolidImageDataActionDestroy( SCTAction* action )
{
    SCTOpenVGCreateSolidImageDataActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGCreateSolidImageDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenVGCreateSolidImageDataActionContext( context );
}
