/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatedataaction.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include "sct_utils.h"
#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateDataActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenVGCreateDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateData@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateDataActionContext ) );

    context->moduleContext = ( SCTOpenVGModuleContext* )( moduleContext );

    if( sctiParseOpenVGCreateDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidDataIndex( context->moduleContext, context->data.dataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateDataActionContext( context );
        SCT_LOG_ERROR( "Invalid data index in CreateData@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGCreateDataActionContext*   context;
    OpenVGCreateDataActionData*         data;
    SCTOpenVGData*                      rawData;
    void*                               buffer;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGCreateDataActionContext* )( action->context );
    data    = &( context->data );

    rawData = sctiOpenVGModuleGetData( context->moduleContext, data->dataIndex );
    if( rawData != NULL )
    {
        if( rawData->length != data->length )
        {
            SCT_LOG_ERROR( "Data index already used in CreateData@OpenVG action execute." );
            return SCT_FALSE;
        }
    }
    else
    {
        buffer = siCommonMemoryAlloc( NULL, data->length );
        if( buffer == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in CreateData@OpenVG action execute." );
            return SCT_FALSE;
        }

        rawData = sctOpenVGCreateData( buffer, data->length, SCT_FALSE );
        if( rawData == NULL )
        {
            siCommonMemoryFree( NULL, buffer );
            SCT_LOG_ERROR( "Allocation failed in CreateData@OpenVG action execute." );
            return SCT_FALSE;
        }
        
        sctiOpenVGModuleSetData( context->moduleContext, data->dataIndex, rawData );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreateDataActionTerminate( SCTAction* action )
{
    SCTOpenVGCreateDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGCreateDataActionContext* )( action->context );

    sctiOpenVGModuleDeleteData( context->moduleContext, context->data.dataIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreateDataActionDestroy( SCTAction* action )
{
    SCTOpenVGCreateDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGCreateDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenVGCreateDataActionContext( context );
}
