/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgsquadbeziersdirectaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGSQuadBeziersDirectActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGSQuadBeziersDirectActionContext*   context;
    SCTOpenVGModuleContext*                     mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGSQuadBeziersDirectActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGSQuadBeziersDirectActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SQuadBeziersDirect@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGSQuadBeziersDirectActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGSQuadBeziersDirectActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSQuadBeziersDirectActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSQuadBeziersDirectActionContext( context );
        SCT_LOG_ERROR( "Path index out of range in SQuadBeziersDirect@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGSQuadBeziersDirectActionContext( void* context )
{
    SCTOpenVGSQuadBeziersDirectActionContext*   c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTOpenVGSQuadBeziersDirectActionContext* )( context );
    if( c->data.coordinates != NULL )
    {
        sctDestroyDoubleVector( c->data.coordinates );
        c->data.coordinates = NULL;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSQuadBeziersDirectActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGSQuadBeziersDirectActionContext*   context;
    OpenVGSQuadBeziersDirectActionData*         data;
    VGubyte                                     segment;
    int                                         i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGSQuadBeziersDirectActionContext* )( action->context );
    data    = &( context->data );

    context->pathType         = ( VGPathDatatype )( -1 );
    context->numOfSegments    = data->coordinates->length / 2;
    context->numOfCoordinates = data->coordinates->length;

    SCT_ASSERT_ALWAYS( ( context->pathSegments == NULL && context->pathData == NULL ) ||
                       ( context->pathSegments != NULL && context->pathData != NULL ) );

    if( context->pathSegments == NULL )
    {
        context->pathSegments = ( VGubyte* )( siCommonMemoryAlloc( NULL, context->numOfSegments ) ); 
    }
    if( context->pathData == NULL )
    {
        context->pathData = siCommonMemoryAlloc( NULL, context->numOfCoordinates * sizeof( VGfloat ) );
    }
    
    if( context->pathSegments == NULL || context->pathData == NULL )
    {
        sctiOpenVGSQuadBeziersDirectActionTerminate( action );
        SCT_LOG_ERROR( "Allocation failed in SQuadBeziersDirect@OpenVG action init." );
        return SCT_FALSE;
    }

    if( data->relative == SCT_FALSE )
    {
        segment = VG_SQUAD_TO_ABS;
    }
    else
    {
        segment = VG_SQUAD_TO_REL;
    }

    for( i = 0; i < context->numOfSegments; ++i )
    {
        context->pathSegments[ i ] = segment;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSQuadBeziersDirectActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGSQuadBeziersDirectActionContext*   context;
    OpenVGSQuadBeziersDirectActionData*         data;
    SCTOpenVGPath*                              sctPath;
    VGErrorCode                                 err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGSQuadBeziersDirectActionContext* )( action->context );
    data    = &( context->data );

    sctPath = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( sctPath == NULL )
    {
        SCT_LOG_ERROR( "Invalid path in SQuadBeziersDirect@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctPath->path != VG_INVALID_HANDLE );

    if( context->pathType != sctPath->type )
    {
        context->pathType = sctPath->type;
        if( sctOpenVGConvertDoubleArray( context->pathData, 
                                         context->pathType,
                                         context->data.coordinates->data, 
                                         context->numOfCoordinates ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "Coordinate data out of path datatype range in SQuadBeziersDirect@OpenVG action execute." );
            return SCT_FALSE;
        }
    }
    
    vgAppendPathData( sctPath->path, context->numOfSegments, context->pathSegments, context->pathData );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in SQuadBeziersDirect@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGSQuadBeziersDirectActionTerminate( SCTAction* action )
{
    SCTOpenVGSQuadBeziersDirectActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGSQuadBeziersDirectActionContext* )( action->context );

    if( context->pathSegments != NULL )
    {
        siCommonMemoryFree( NULL, context->pathSegments );
        context->pathSegments = NULL;
    }
    if( context->pathData != NULL )
    {
        siCommonMemoryFree( NULL, context->pathData );
        context->pathData = NULL;
    }
}

/*!
 *
 *
 */
void sctiOpenVGSQuadBeziersDirectActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGSQuadBeziersDirectActionContext( action->context );
    action->context = NULL;
}

