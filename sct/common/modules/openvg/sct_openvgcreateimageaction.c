/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreateimageaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateImageActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateImageActionContext*  context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCreateImageActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateImageActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateImage@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateImageActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCreateImageActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateImageActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.imageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateImageActionContext( context );
        SCT_LOG_ERROR( "Image index out of range in CreateImage@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateImageActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateImageActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGCreateImageActionContext*  context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGCreateImageActionContext* )( action->context );
    
#if defined( SCT_DEPRECATED_OPENVG_1_0 )
    if( context->data.format > VG_BW_1 )
    {
        SCT_LOG_ERROR( "CreateImage@OpenVG unsupported image format." );
        return SCT_FALSE;
    }
#else   /* defined( SCT_DEPRECATED_OPENVG_1_0 ) */
    SCT_USE_VARIABLE( context );
#endif  /* defined( SCT_DEPRECATED_OPENVG_1_0 ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateImageActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGCreateImageActionContext*  context;
    OpenVGCreateImageActionData*        data;
    int                                 index;
    SCTOpenVGImage*                     sctImage;
    VGImage                             vgImage;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGCreateImageActionContext* )( action->context );
    data    = &( context->data );

    index = data->imageIndex;
    sctImage = sctiOpenVGModuleGetImage( context->moduleContext, index );
    if( sctImage != NULL )
    {
        SCT_LOG_ERROR( "Image index already in use in CreateImage@OpenVG action execute." );
        return SCT_FALSE;
    }

    vgImage = vgCreateImage( data->format, data->width, data->height, data->allowedQuality );
    if( vgImage == VG_INVALID_HANDLE )
    {
        SCT_LOG_ERROR( "VGImage creation failed in CreateImage@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctImage = sctOpenVGCreateImage( vgImage, data->width, data->height );
    if( sctImage == NULL )
    {
        vgDestroyImage( vgImage );
        SCT_LOG_ERROR( "Allocation failed in CreateImage@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleSetImage( context->moduleContext, index, sctImage );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreateImageActionTerminate( SCTAction* action )
{
    SCTOpenVGCreateImageActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGCreateImageActionContext* )( action->context );

    sctiOpenVGModuleDeleteImage( context->moduleContext, context->data.imageIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreateImageActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCreateImageActionContext( action->context );
    action->context = NULL;
}
