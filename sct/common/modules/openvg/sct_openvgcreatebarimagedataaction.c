/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatebarimagedataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateBarImageDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateBarImageDataActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenVGCreateBarImageDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateBarImageDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateBarImageData@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateBarImageDataActionContext ) );

    context->moduleContext = ( SCTOpenVGModuleContext* )( moduleContext );

    if( sctiParseOpenVGCreateBarImageDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateBarImageDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageDataIndex( context->moduleContext, context->data.imageDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateBarImageDataActionContext( context );
        SCT_LOG_ERROR( "Invalid image data index in CreateBarImageData@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateBarImageDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
   
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateBarImageDataActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateBarImageDataActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGCreateBarImageDataActionContext*   context;
    OpenVGCreateBarImageDataActionData*         data;
    SCTOpenVGImageData*                         imageData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGCreateBarImageDataActionContext* )( action->context );
    data    = &( context->data );

    if( sctiOpenVGModuleGetImageData( context->moduleContext, data->imageDataIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Image data index already used in CreateBarImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    imageData = sctOpenVGCreateBarImageData( data->format, 
                                             data->width, 
                                             data->height, 
                                             data->color1, 
                                             data->color2, 
                                             data->bars );
    if( imageData == NULL )
    {
        SCT_LOG_ERROR( "Image data creation failed in CreateBarImageData@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleSetImageData( context->moduleContext, data->imageDataIndex, imageData );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreateBarImageDataActionTerminate( SCTAction* action )
{
    SCTOpenVGCreateBarImageDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGCreateBarImageDataActionContext* )( action->context );

    sctiOpenVGModuleDeleteImageData( context->moduleContext, context->data.imageDataIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreateBarImageDataActionDestroy( SCTAction* action )
{
    SCTOpenVGCreateBarImageDataActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGCreateBarImageDataActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenVGCreateBarImageDataActionContext( context );
}
