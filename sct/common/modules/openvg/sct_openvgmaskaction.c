/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgmaskaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGMaskActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGMaskActionContext* context;
    SCTOpenVGModuleContext*     mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGMaskActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGMaskActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Mask@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGMaskActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGMaskActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGMaskActionContext( context );
        return NULL;
    }

    if( context->data.handleIndex >= 0 )
    {
        if( context->data.handleType == MASK_HANDLETYPE_IMAGE )
        {
            if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.handleIndex ) == SCT_FALSE )
            {
                sctiDestroyOpenVGMaskActionContext( context );
                SCT_LOG_ERROR( "Invalid handle index in Mask@OpenVG action context creation." );
                return NULL;
            }
        }
        else if( context->data.handleType == MASK_HANDLETYPE_MASK_LAYER )
        {
#if defined( OPENVG_VERSION_1_1 )
            if( sctiOpenVGModuleIsValidMaskLayerIndex( context->moduleContext, context->data.handleIndex ) == SCT_FALSE )
            {
                sctiDestroyOpenVGMaskActionContext( context );
                SCT_LOG_ERROR( "Invalid handle index in Mask@OpenVG action context creation." );
                return NULL;
            }
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
        }
        else
        {
            sctiDestroyOpenVGMaskActionContext( context );
            SCT_LOG_ERROR( "Invalid handle type in Mask@OpenVG action context creation." );
            return NULL;
        }
    } 
    else 
    {
        if( context->data.operation != VG_CLEAR_MASK && context->data.operation != VG_FILL_MASK )
        {
            sctiDestroyOpenVGMaskActionContext( context );
            SCT_LOG_ERROR( "Unspecified handle index in Mask@OpenVG action context creation." );
            return NULL;
        }
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGMaskActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGMaskActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGMaskActionContext* context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGMaskActionContext* )( action->context );
   
#if !defined( OPENVG_VERSION_1_1 )   
    if( context->data.handleType == MASK_HANDLETYPE_MASK_LAYER )
    {
        SCT_LOG_ERROR( "Mask@OpenVG action init MaskLayer handle type not supported in OpenVG 1.0." );
        return SCT_FALSE;
    }
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCT_USE_VARIABLE( context );
#endif  /* !defined( OPENVG_VERSION_1_1 ) */

    return SCT_TRUE;
}


/*!
 *
 *
 */
SCTBoolean sctiOpenVGMaskActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGMaskActionContext* context;
    OpenVGMaskActionData*       data;
    VGErrorCode                 err;
    VGHandle                    handle;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGMaskActionContext* )( action->context );
    data    = &( context->data );

    if( data->handleIndex < 0 )
    {
        handle = VG_INVALID_HANDLE;
    }
    else
    {
        if( data->handleType == MASK_HANDLETYPE_IMAGE )
        {
            SCTOpenVGImage* image = sctiOpenVGModuleGetImage( context->moduleContext, data->handleIndex );
            if( image == NULL )
            {
                SCT_LOG_ERROR( "Invalid image in Mask@OpenVG action execute." );
                return SCT_FALSE;
            }
            SCT_ASSERT( image->image != VG_INVALID_HANDLE );

            handle = ( VGHandle )( image->image );
        }
        else
        {
#if !defined( OPENVG_VERSION_1_1 )
            SCT_ASSERT_ALWAYS( 0 );
            return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */
            SCTOpenVGMaskLayer* maskLayer = sctiOpenVGModuleGetMaskLayer( context->moduleContext, data->handleIndex );
            if( maskLayer == NULL )
            {
                SCT_LOG_ERROR( "Invalid mask layer in Mask@OpenVG action execute." );
                return SCT_FALSE;
            }
            SCT_ASSERT( maskLayer->maskLayer != VG_INVALID_HANDLE );

            handle = ( VGHandle )( maskLayer->maskLayer );
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
        }
    }

#if !defined( OPENVG_VERSION_1_1 )
    vgMask( ( VGImage )( handle ),
            data->operation, 
            data->x,
            data->y,
            data->width,
            data->height );
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    vgMask( handle, 
            data->operation, 
            data->x,
            data->y,
            data->width,
            data->height );
#endif  /* !defined( OPENVG_VERSION_1_1 ) */

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in Mask@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGMaskActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenVGMaskActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGMaskActionContext( action->context );
    action->context = NULL;
}
