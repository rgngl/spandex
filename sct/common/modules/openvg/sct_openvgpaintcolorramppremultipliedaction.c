/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgpaintcolorramppremultipliedaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGPaintColorRampPremultipliedActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGPaintColorRampPremultipliedActionContext*  context;
    SCTOpenVGModuleContext*                             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGPaintColorRampPremultipliedActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGPaintColorRampPremultipliedActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PaintColorRampPremultiplied@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGPaintColorRampPremultipliedActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGPaintColorRampPremultipliedActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPaintColorRampPremultipliedActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPaintIndex( context->moduleContext, context->data.paintIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPaintColorRampPremultipliedActionContext( context );
        SCT_LOG_ERROR( "Paint index out of range in PaintColorRampPremultiplied@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGPaintColorRampPremultipliedActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGPaintColorRampPremultipliedActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTOpenVGPaintColorRampPremultipliedActionContext*  context;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTOpenVGPaintColorRampPremultipliedActionContext* )( action->context );
    
#if defined( SCT_DEPRECATED_OPENVG_1_0 )
    if( context->data.premultiplied == SCT_FALSE )
    {
        SCT_LOG_ERROR( "PaintColorRampPremultiplied@OpenVG action init only premultiplied color ramp supported." );
        return SCT_FALSE;
    }
#else   /* defined( SCT_DEPRECATED_OPENVG_1_0 ) */
    SCT_USE_VARIABLE( context );
#endif /* defined( SCT_DEPRECATED_OPENVG_1_0 ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGPaintColorRampPremultipliedActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGPaintColorRampPremultipliedActionContext*  context;
    OpenVGPaintColorRampPremultipliedActionData*        data;
    VGErrorCode                                         err;
    SCTOpenVGPaint*                                     sctPaint;
    VGboolean                                           premultiplied   = VG_TRUE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGPaintColorRampPremultipliedActionContext* )( action->context );
    data    = &( context->data );

    sctPaint = sctiOpenVGModuleGetPaint( context->moduleContext, data->paintIndex );
    if( sctPaint == VG_INVALID_HANDLE )
    {
        SCT_LOG_ERROR( "Invalid paint in PaintColorRampPremultiplied@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT_ALWAYS( sctPaint->paint != VG_INVALID_HANDLE );
       
#if !defined( SCT_DEPRECATED_OPENVG_1_0 )
    if( data->premultiplied == SCT_FALSE )
    {
        premultiplied = VG_FALSE;
    }
    vgSetParameteri( sctPaint->paint, VG_PAINT_COLOR_RAMP_PREMULTIPLIED, premultiplied );
#else   /* !defined( SCT_DEPRECATED_OPENVG_1_0 ) */
    SCT_USE_VARIABLE( premultiplied );
#endif  /* !defined( SCT_DEPRECATED_OPENVG_1_0 ) */

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in PaintColorRampPremultiplied@OpenVG action execution.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGPaintColorRampPremultipliedActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenVGPaintColorRampPremultipliedActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGPaintColorRampPremultipliedActionContext( action->context );
    action->context = NULL;
}




