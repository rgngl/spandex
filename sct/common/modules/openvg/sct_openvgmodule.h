/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_OPENVGMODULE_H__ )
#define __SCT_OPENVGMODULE_H__

#include "sct_types.h"
#include "sct_vg.h"
#include "sct_openvgutils.h"

#define SCT_MAX_OPENVG_IMAGES                   4096
#define SCT_MAX_OPENVG_PAINTS                   4096
#define SCT_MAX_OPENVG_PATHS                    4096
#define SCT_MAX_OPENVG_DATA_ELEMENTS            4096
#define SCT_MAX_OPENVG_IMAGE_DATA_ELEMENTS      4096
#define SCT_MAX_OPENVG_PATH_DATA_ELEMENTS       4096
#define SCT_MAX_OPENVG_TIGERS                   16
#define SCT_MAX_OPENVG_FLOATS                   16
#define SCT_MAX_OPENVG_LOOKUP_TABLES            16

#if defined( OPENVG_VERSION_1_1 )
# define SCT_MAX_OPENVG_FONTS                   4096
# define SCT_MAX_OPENVG_MASK_LAYERS             4096
#endif  /* defined( OPENVG_VERSION_1_1 ) */

/*!
 * OpenVG module context.
 *
 */
typedef struct
{
    void*                       vgContext;
    SCTOpenVGImage*             images[ SCT_MAX_OPENVG_IMAGES ];
    SCTOpenVGPaint*             paints[ SCT_MAX_OPENVG_PAINTS ];
    SCTOpenVGPath*              paths[ SCT_MAX_OPENVG_PATHS ];
    SCTOpenVGData*              dataElements[ SCT_MAX_OPENVG_DATA_ELEMENTS ];     
    SCTOpenVGImageData*         imageDataElements[ SCT_MAX_OPENVG_IMAGE_DATA_ELEMENTS ]; 
    SCTOpenVGPathData*          pathDataElements[ SCT_MAX_OPENVG_PATH_DATA_ELEMENTS ];

#if defined( OPENVG_VERSION_1_1 )
    SCTOpenVGFont*              fonts[ SCT_MAX_OPENVG_FONTS ];
    SCTOpenVGMaskLayer*         maskLayers[ SCT_MAX_OPENVG_MASK_LAYERS ];
#endif  /* defined( OPENVG_VERSION_1_1 ) */

    void*                       tigers[ SCT_MAX_OPENVG_TIGERS ];
    VGfloat                     floats[ SCT_MAX_OPENVG_FLOATS ];
    SCTOpenVGLookupTable*       lookupTables[ SCT_MAX_OPENVG_LOOKUP_TABLES ];
} SCTOpenVGModuleContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    SCTModule*                  sctCreateOpenVGModule( void );

    /* ###################################################################### */    
    /* External OpenVG module data access. */
    SCTBoolean                  sctOpenVGModuleSetDataExt( int index, SCTOpenVGData* data );
    VGPath                      sctOpenVGModuleGetPathExt( int index );
    VGImage                     sctOpenVGModuleGetImageExt( int index );
   
    /* ###################################################################### */
    /* OpenVG images */
    SCTBoolean                  sctiOpenVGModuleIsValidImageIndex( SCTOpenVGModuleContext* moduleContext, int index );
    SCTOpenVGImage*             sctiOpenVGModuleGetImage( SCTOpenVGModuleContext* moduleContext, int index );
    void                        sctiOpenVGModuleSetImage( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGImage* image );
    void                        sctiOpenVGModuleDeleteImage( SCTOpenVGModuleContext* moduleContext, int index );
    
    /* ###################################################################### */
    /* OpenVG paints */
    SCTBoolean                  sctiOpenVGModuleIsValidPaintIndex( SCTOpenVGModuleContext* moduleContext, int index );
    SCTOpenVGPaint*             sctiOpenVGModuleGetPaint( SCTOpenVGModuleContext* moduleContext, int index );
    void                        sctiOpenVGModuleSetPaint( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGPaint* paint );
    void                        sctiOpenVGModuleDeletePaint( SCTOpenVGModuleContext* moduleContext, int index );

    /* ###################################################################### */
    /* OpenVG path */
    SCTBoolean                  sctiOpenVGModuleIsValidPathIndex( SCTOpenVGModuleContext* moduleContext, int index );
    SCTOpenVGPath*              sctiOpenVGModuleGetPath( SCTOpenVGModuleContext* moduleContext, int index );
    void                        sctiOpenVGModuleSetPath( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGPath* path );
    void                        sctiOpenVGModuleDeletePath( SCTOpenVGModuleContext* moduleContext, int index );

    /* ###################################################################### */
    /* Data */
    SCTBoolean                  sctiOpenVGModuleIsValidDataIndex( SCTOpenVGModuleContext* moduleContext, int index );
    SCTOpenVGData*              sctiOpenVGModuleGetData( SCTOpenVGModuleContext* moduleContext, int index );
    void                        sctiOpenVGModuleSetData( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGData* data );
    void                        sctiOpenVGModuleDeleteData( SCTOpenVGModuleContext* moduleContext, int index );
    
    /* ###################################################################### */
    /* Image data */
    SCTBoolean                  sctiOpenVGModuleIsValidImageDataIndex( SCTOpenVGModuleContext* moduleContext, int index );
    SCTOpenVGImageData*         sctiOpenVGModuleGetImageData( SCTOpenVGModuleContext* moduleContext, int index );
    void                        sctiOpenVGModuleSetImageData( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGImageData* imageData );
    void                        sctiOpenVGModuleDeleteImageData( SCTOpenVGModuleContext* moduleContext, int index );

    /* ###################################################################### */
    /* Path data */
    SCTBoolean                  sctiOpenVGModuleIsValidPathDataIndex( SCTOpenVGModuleContext* moduleContext, int index );
    SCTOpenVGPathData*          sctiOpenVGModuleGetPathData( SCTOpenVGModuleContext* moduleContext, int index );
    void                        sctiOpenVGModuleSetPathData( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGPathData* pathData );
    void                        sctiOpenVGModuleDeletePathData( SCTOpenVGModuleContext* moduleContext, int index );

#if defined( OPENVG_VERSION_1_1 )
    
    /* ###################################################################### */
    /* OpenVG 1.1 font */
    SCTBoolean                  sctiOpenVGModuleIsValidFontIndex( SCTOpenVGModuleContext* moduleContext, int index );
    SCTOpenVGFont*              sctiOpenVGModuleGetFont( SCTOpenVGModuleContext* moduleContext, int index );
    void                        sctiOpenVGModuleSetFont( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGFont* font );
    void                        sctiOpenVGModuleDeleteFont( SCTOpenVGModuleContext* moduleContext, int index );

    /* ###################################################################### */
    /* OpenVG 1.1 mask layer */
    SCTBoolean                  sctiOpenVGModuleIsValidMaskLayerIndex( SCTOpenVGModuleContext* moduleContext, int index );
    SCTOpenVGMaskLayer*         sctiOpenVGModuleGetMaskLayer( SCTOpenVGModuleContext* moduleContext, int index );
    void                        sctiOpenVGModuleSetMaskLayer( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGMaskLayer* maskLayer );
    void                        sctiOpenVGModuleDeleteMaskLayer( SCTOpenVGModuleContext* moduleContext, int index );

#endif /* defined( OPENVG_VERSION_1_1 ) */

    /* ###################################################################### */
    /* Tiger */
    SCTBoolean                  sctiOpenVGModuleIsValidTigerIndex( SCTOpenVGModuleContext* moduleContext, int index );
    void*                       sctiOpenVGModuleGetTiger( SCTOpenVGModuleContext* moduleContext, int index );
    void                        sctiOpenVGModuleSetTiger( SCTOpenVGModuleContext* moduleContext, int index, void* tiger );
    void                        sctiOpenVGModuleDeleteTiger( SCTOpenVGModuleContext* moduleContext, int index );

    /* ###################################################################### */
    /* Floats */
    SCTBoolean                  sctiOpenVGModuleIsValidFloatIndex( SCTOpenVGModuleContext* moduleContext, int index );
    VGfloat                     sctiOpenVGModuleGetFloat( SCTOpenVGModuleContext* moduleContext, int index );
    void                        sctiOpenVGModuleSetFloat( SCTOpenVGModuleContext* moduleContext, int index, VGfloat f );

    /* ###################################################################### */
    /* Lookup table */
    SCTBoolean                  sctiOpenVGModuleIsValidLookupTableIndex( SCTOpenVGModuleContext* moduleContext, int index );
    SCTOpenVGLookupTable*       sctiOpenVGModuleGetLookupTable( SCTOpenVGModuleContext* moduleContext, int index );
    void                        sctiOpenVGModuleSetLookupTable( SCTOpenVGModuleContext* moduleContext, int index, SCTOpenVGLookupTable* lookupTable );
    void                        sctiOpenVGModuleDeleteLookupTables( SCTOpenVGModuleContext* moduleContext, int index );

    /* Clear error */
    const char*                 sctiOpenVGGetErrorString( VGErrorCode err );
    void                        sctiOpenVGClearVGError( void );
    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_OPENVGMODULE_H__ ) */
