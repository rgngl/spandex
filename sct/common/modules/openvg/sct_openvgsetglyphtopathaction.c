/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgsetglyphtopathaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGSetGlyphToPathActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGSetGlyphToPathActionContext*   context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGSetGlyphToPathActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGSetGlyphToPathActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SetGlyphToPath@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGSetGlyphToPathActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGSetGlyphToPathActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSetGlyphToPathActionContext( context );
        return NULL;
    }

#if defined( OPENVG_VERSION_1_1 )   
    if( sctiOpenVGModuleIsValidFontIndex( context->moduleContext, context->data.fontIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSetGlyphToPathActionContext( context );
        SCT_LOG_ERROR( "Font index out of range in SetGlyphToPath@OpenVG context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidPathIndex( context->moduleContext, context->data.pathIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGSetGlyphToPathActionContext( context );
        SCT_LOG_ERROR( "Path index out of range in SetGlyphToPath@OpenVG context creation." );
        return NULL;
    } 
#endif  /* defined( OPENVG_VERSION_1_1 ) */  
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGSetGlyphToPathActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSetGlyphToPathActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( OPENVG_VERSION_1_1 )
    SCT_LOG_ERROR( "SetGlyphToPath@OpenVG not supported in OpenVG 1.0." );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */  
    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */    
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGSetGlyphToPathActionExecute( SCTAction* action, int framenumber )
{
#if !defined( OPENVG_VERSION_1_1 )
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCTOpenVGSetGlyphToPathActionContext*   context;
    OpenVGSetGlyphToPathActionData*         data;
    SCTOpenVGFont*                          font;
    SCTOpenVGPath*                          path;
    VGErrorCode                             err;
    VGboolean                               isHinted = VG_FALSE;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGSetGlyphToPathActionContext* )( action->context );
    data    = &( context->data );

    font = sctiOpenVGModuleGetFont( context->moduleContext, data->fontIndex );
    if( font == NULL )
    {
        SCT_LOG_ERROR( "Invalid font in SetGlyphToPath@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( font->font != VG_INVALID_HANDLE );

    path = sctiOpenVGModuleGetPath( context->moduleContext, data->pathIndex );
    if( path == NULL )
    {
        SCT_LOG_ERROR( "Invalid path in SetGlyphToPath@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( path->path != VG_INVALID_HANDLE );

    if( data->isHinted == SCT_TRUE )
    {
        isHinted = VG_TRUE;
    }

    vgSetGlyphToPath( font->font,
                      data->glyphIndex,
                      path->path,
                      isHinted,
                      data->glyphOrigin,
                      data->escapement );

# if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in SetGlyphToPath@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
# else  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
# endif /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
}

/*!
 *
 *
 */
void sctiOpenVGSetGlyphToPathActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenVGSetGlyphToPathActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGSetGlyphToPathActionContext( action->context );
    action->context = NULL;
}
