/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgreadpixelsaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGReadPixelsActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGReadPixelsActionContext*   context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGReadPixelsActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGReadPixelsActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ReadPixels@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGReadPixelsActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGReadPixelsActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGReadPixelsActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageDataIndex( context->moduleContext, context->data.imageDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGReadPixelsActionContext( context );
        SCT_LOG_ERROR( "Image data index out of range in ReadPixels@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGReadPixelsActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGReadPixelsActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGReadPixelsActionContext*   context;
    OpenVGReadPixelsActionData*         data;
    SCTOpenVGImageData*                 imageData;
    VGErrorCode                         err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGReadPixelsActionContext* )( action->context );
    data    = &( context->data );

    imageData = sctiOpenVGModuleGetImageData( context->moduleContext, data->imageDataIndex );
    if( imageData == NULL )
    {
        SCT_LOG_ERROR( "Invalid image data in ReadPixels@OpenVG action execute." );
        return SCT_FALSE;
    }

    vgReadPixels( imageData->data,
                  imageData->stride,
                  imageData->format,
                  data->sx,
                  data->sy,
                  data->width,
                  data->height );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in ReadPixels@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGReadPixelsActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGReadPixelsActionContext( action->context );
    action->context = NULL;
}
