/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdestroyfontaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGDestroyFontActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDestroyFontActionContext*  context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGDestroyFontActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDestroyFontActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyFont@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDestroyFontActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGDestroyFontActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyFontActionContext( context );
        return NULL;
    }

#if defined( OPENVG_VERSION_1_1 )
    if( sctiOpenVGModuleIsValidFontIndex( context->moduleContext, context->data.fontIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyFontActionContext( context );
        SCT_LOG_ERROR( "Font index out of range in DestroyFont@OpenVG context creation." );
        return NULL;
    } 
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
   
    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDestroyFontActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDestroyFontActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

#if !defined( OPENVG_VERSION_1_1 )
    SCT_LOG_ERROR( "DestroyFont@OpenVG not supported in OpenVG 1.0." );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */  
    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */    
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDestroyFontActionExecute( SCTAction* action, int framenumber )
{
#if !defined( OPENVG_VERSION_1_1 )
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );
    SCT_ASSERT_ALWAYS( 0 );
    return SCT_FALSE;
#else   /* !defined( OPENVG_VERSION_1_1 ) */
    SCTOpenVGDestroyFontActionContext*  context;
    OpenVGDestroyFontActionData*        data;
    int                                 index;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGDestroyFontActionContext* )( action->context );
    data    = &( context->data );

    index = data->fontIndex;
    if( sctiOpenVGModuleGetFont( context->moduleContext, index ) == NULL )
    {
        SCT_LOG_ERROR( "Invalid font in DestroyFont@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleDeleteFont( context->moduleContext, index );

    return SCT_TRUE;
#endif  /* !defined( OPENVG_VERSION_1_1 ) */
}

/*!
 *
 *
 */
void sctiOpenVGDestroyFontActionTerminate( SCTAction* action )
{
    SCT_USE_VARIABLE( action );
}

/*!
 *
 *
 */
void sctiOpenVGDestroyFontActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGDestroyFontActionContext( action->context );
    action->context = NULL;
}
