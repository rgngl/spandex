/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgpaintcolorrampspreadmodeaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGPaintColorRampSpreadModeActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGPaintColorRampSpreadModeActionContext* context;
    SCTOpenVGModuleContext*                         mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGPaintColorRampSpreadModeActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGPaintColorRampSpreadModeActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PaintColorRampSpreadMode@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGPaintColorRampSpreadModeActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGPaintColorRampSpreadModeActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPaintColorRampSpreadModeActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPaintIndex( context->moduleContext, context->data.paintIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPaintColorRampSpreadModeActionContext( context );
        SCT_LOG_ERROR( "Paint index out of range in PaintColorRampSpreadMode@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGPaintColorRampSpreadModeActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGPaintColorRampSpreadModeActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGPaintColorRampSpreadModeActionContext* context;
    OpenVGPaintColorRampSpreadModeActionData*       data;
    VGErrorCode                                     err;
    SCTOpenVGPaint*                                 sctPaint;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGPaintColorRampSpreadModeActionContext* )( action->context );
    data    = &( context->data );

    sctPaint = sctiOpenVGModuleGetPaint( context->moduleContext, data->paintIndex );
    if( sctPaint == VG_INVALID_HANDLE )
    {
        SCT_LOG_ERROR( "Invalid paint in PaintColorRampSpreadMode@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT_ALWAYS( sctPaint->paint != VG_INVALID_HANDLE );
       
    vgSetParameteri( sctPaint->paint, VG_PAINT_COLOR_RAMP_SPREAD_MODE, data->spreadMode );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in PaintColorRampSpreadMode@OpenVG action execution.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGPaintColorRampSpreadModeActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGPaintColorRampSpreadModeActionContext( action->context );
    action->context = NULL;
}




