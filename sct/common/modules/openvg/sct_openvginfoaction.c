/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvginfoaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 */
static SCTBoolean sctiReportHardwareCapabilities( SCTAttributeList* attributes );

/*!
 *
 *
 */
void* sctiCreateOpenVGInfoActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGInfoActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTOpenVGInfoActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGInfoActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Info@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGInfoActionContext ) );

    context->reported = SCT_FALSE;

    if( sctiParseOpenVGInfoActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGInfoActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGInfoActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGInfoActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGInfoActionContext* context;
    SCTAttributeList*           attributes;
    VGErrorCode                 err;    
    SCTBoolean                  status          = SCT_TRUE;
    const char*                 str;
    char                        buffer[ 64 ];
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGInfoActionContext* )( action->context );

    if( context->reported == SCT_TRUE )
    {
        return SCT_TRUE;
    }

    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Info@OpenVG action execute." );
        return SCT_FALSE;
    }
    
    /* Store VG vendor. */
    str = ( const char* )( vgGetString( VG_VENDOR ) );
    if( str == NULL || 
        sctAddNameValueToList( attributes, "VG vendor", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
    
    /* Store VG renderer. */
    str = ( const char* )( vgGetString( VG_RENDERER ) );
    if( str == NULL ||
        sctAddNameValueToList( attributes, "VG renderer", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store VG version. */
    str = ( const char* )( vgGetString( VG_VERSION ) );
    if( str == NULL ||
        sctAddNameValueToList( attributes, "VG version", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store VG extensions string. */
    str = ( const char* )( vgGetString( VG_EXTENSIONS ) );
    if( str == NULL ||
        sctAddNameValueToList( attributes, "VG extensions", str ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    /* Store max values. */
    sprintf( buffer, "%d", vgGeti( VG_MAX_SCISSOR_RECTS ) );
    if( sctAddNameValueToList( attributes, "VG_MAX_SCISSOR_RECTS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    sprintf( buffer, "%d", vgGeti( VG_MAX_DASH_COUNT ) );
    if( sctAddNameValueToList( attributes, "VG_MAX_DASH_COUNT", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
    
    sprintf( buffer, "%d", vgGeti( VG_MAX_KERNEL_SIZE ) );
    if( sctAddNameValueToList( attributes, "VG_MAX_KERNEL_SIZE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    sprintf( buffer, "%d", vgGeti( VG_MAX_SEPARABLE_KERNEL_SIZE ) );
    if( sctAddNameValueToList( attributes, "VG_MAX_SEPARABLE_KERNEL_SIZE", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    sprintf( buffer, "%d", vgGeti( VG_MAX_COLOR_RAMP_STOPS ) );
    if( sctAddNameValueToList( attributes, "VG_MAX_COLOR_RAMP_STOPS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    sprintf( buffer, "%d", vgGeti( VG_MAX_IMAGE_WIDTH ) );
    if( sctAddNameValueToList( attributes, "VG_MAX_IMAGE_WIDTH", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    sprintf( buffer, "%d", vgGeti( VG_MAX_IMAGE_HEIGHT ) );
    if( sctAddNameValueToList( attributes, "VG_MAX_IMAGE_HEIGHT", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    sprintf( buffer, "%d", vgGeti( VG_MAX_IMAGE_PIXELS ) );
    if( sctAddNameValueToList( attributes, "VG_MAX_IMAGE_PIXELS", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }
    
    sprintf( buffer, "%d", vgGeti( VG_MAX_IMAGE_BYTES ) );
    if( sctAddNameValueToList( attributes, "VG_MAX_IMAGE_BYTES", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }

    sprintf( buffer, "%d", vgGeti( VG_MAX_GAUSSIAN_STD_DEVIATION ) );
    if( sctAddNameValueToList( attributes, "VG_MAX_GAUSSIAN_STD_DEVIATION", buffer ) == SCT_FALSE )
    {
        status = SCT_FALSE;
    }   

    if( status == SCT_FALSE )
    {
        SCT_LOG_ERROR( "List add failed in Info@OpenVG action execute." );
    }   
    
    /* Report hardware capabilities. */
    if( status == SCT_TRUE && sctiReportHardwareCapabilities( attributes ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Reporting hardware capabilities failed in Info@OpenVG action execute." );
        status = SCT_FALSE;
    }

    if( status == SCT_TRUE )
    {
        err = vgGetError();
        if( err != VG_NO_ERROR )
        {
            char buf[ 256 ];
            sprintf( buf, "VG error %s in Info@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
            SCT_LOG_ERROR( buf );
            sctiOpenVGClearVGError();
            status = SCT_FALSE;
        }
    }

    /* Write a section to output file. *SLIGHT HACK* */
    if( status == SCT_TRUE && sctReportSection( "OpenVG", "MODULE", attributes ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Report section failed in Info@OpenVG action execute." );
        status = SCT_FALSE;
    }

    sctDestroyAttributeList( attributes );

    context->reported = SCT_TRUE;

    return status;
}

/*!
 *
 *
 */
void sctiOpenVGInfoActionDestroy( SCTAction* action )
{
    SCTOpenVGInfoActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTOpenVGInfoActionContext* )( action->context );

    action->context = NULL;
    sctiDestroyOpenVGInfoActionContext( context );
}

/*!
 *
 *
 */
static const char* _OpenVGImageFormatStrings[] = { "VG_sRGBX_8888",
                                                   "VG_sRGBA_8888",
                                                   "VG_sRGBA_8888_PRE",
                                                   "VG_sRGB_565",
                                                   "VG_sRGBA_5551",
                                                   "VG_sRGBA_4444",
                                                   "VG_sL_8",
                                                   "VG_lRGBX_8888",
                                                   "VG_lRGBA_8888",
                                                   "VG_lRGBA_8888_PRE",
                                                   "VG_lL_8",
                                                   "VG_A_8",
                                                   "VG_BW_1",
#if defined( OPENVG_VERSION_1_1 )
                                                   "VG_A_1",
                                                   "VG_A_4",
#endif /* defined( OPENVG_VERSION_1_1 ) */
                                                   "VG_sXRGB_8888",
                                                   "VG_sARGB_8888",
                                                   "VG_sARGB_8888_PRE",
                                                   "VG_sARGB_1555",
                                                   "VG_sARGB_4444",
                                                   "VG_lXRGB_8888",
                                                   "VG_lARGB_8888",
                                                   "VG_lARGB_8888_PRE",
                                                   "VG_sBGRX_8888",
                                                   "VG_sBGRA_8888",
                                                   "VG_sBGRA_8888_PRE",
                                                   "VG_sBGR_565",
                                                   "VG_sBGRA_5551",
                                                   "VG_sBGRA_4444",
                                                   "VG_lBGRX_8888",
                                                   "VG_lBGRA_8888",
                                                   "VG_lBGRA_8888_PRE",
                                                   "VG_sXBGR_8888",
                                                   "VG_sABGR_8888",
                                                   "VG_sABGR_8888_PRE",
                                                   "VG_sABGR_1555",
                                                   "VG_sABGR_4444",
                                                   "VG_lXBGR_8888",
                                                   "VG_lABGR_8888",
                                                   "VG_lABGR_8888_PRE" };

/*!
 *
 *
 */
static const int _OpenVGImageFormatValues[] = { VG_sRGBX_8888,
                                                VG_sRGBA_8888,
                                                VG_sRGBA_8888_PRE,
                                                VG_sRGB_565,
                                                VG_sRGBA_5551,
                                                VG_sRGBA_4444,
                                                VG_sL_8,
                                                VG_lRGBX_8888,
                                                VG_lRGBA_8888,
                                                VG_lRGBA_8888_PRE,
                                                VG_lL_8,
                                                VG_A_8,
                                                VG_BW_1,
#if defined( OPENVG_VERSION_1_1 )
                                                VG_A_1,
                                                VG_A_4,
#endif /* defined( OPENVG_VERSION_1_1 ) */
                                                VG_sXRGB_8888,
                                                VG_sARGB_8888,
                                                VG_sARGB_8888_PRE,
                                                VG_sARGB_1555,
                                                VG_sARGB_4444,
                                                VG_lXRGB_8888,
                                                VG_lARGB_8888,
                                                VG_lARGB_8888_PRE,
                                                VG_sBGRX_8888,
                                                VG_sBGRA_8888,
                                                VG_sBGRA_8888_PRE,
                                                VG_sBGR_565,
                                                VG_sBGRA_5551,
                                                VG_sBGRA_4444,
                                                VG_lBGRX_8888,
                                                VG_lBGRA_8888,
                                                VG_lBGRA_8888_PRE,
                                                VG_sXBGR_8888,
                                                VG_sABGR_8888,
                                                VG_sABGR_8888_PRE,
                                                VG_sABGR_1555,
                                                VG_sABGR_4444,
                                                VG_lXBGR_8888,
                                                VG_lABGR_8888,
                                                VG_lABGR_8888_PRE };

/*!
 *
 *
 */
static const char* _OpenVGPathDatatypeStrings[] = { "VG_PATH_DATATYPE_S_8",
                                                    "VG_PATH_DATATYPE_S_16",
                                                    "VG_PATH_DATATYPE_S_32",
                                                    "VG_PATH_DATATYPE_F" };

/*!
 *
 *
 */
static const int _OpenVGPathDatatypeValues[] = { VG_PATH_DATATYPE_S_8,
                                                 VG_PATH_DATATYPE_S_16,
                                                 VG_PATH_DATATYPE_S_32,
                                                 VG_PATH_DATATYPE_F };

/*!
 *
 */
#define OPENVG_INFO_BUF1_LENGTH    128
#define OPENVG_INFO_BUF2_LENGTH    8
/*
 *
 *
 */
static SCTBoolean sctiReportHardwareCapabilities( SCTAttributeList* attributes )
{
    char                                buf1[ OPENVG_INFO_BUF1_LENGTH ];
    char                                buf2[ OPENVG_INFO_BUF2_LENGTH ];
    VGHardwareQueryResult               hwQueryResult;
    int                                 i;
    const char*                         imageFormatString       = "HWA image format %s";
    const char*                         pathDatatypeString      = "HWA path datatype %s";

    SCT_ASSERT( attributes != NULL );

    for( i = 0; i < SCT_ARRAY_LENGTH( _OpenVGImageFormatValues ); ++i )
    {
        hwQueryResult = vgHardwareQuery( VG_IMAGE_FORMAT_QUERY, _OpenVGImageFormatValues[ i ] );

        // Beware buffer overrun
        SCT_ASSERT( ( strlen( _OpenVGImageFormatStrings[ i ] ) + strlen( imageFormatString ) ) < OPENVG_INFO_BUF1_LENGTH );

        sprintf( buf1, imageFormatString, _OpenVGImageFormatStrings[ i ] );

        if( hwQueryResult == VG_HARDWARE_ACCELERATED )
        {
            strcpy( buf2, "YES" );
        }
        else
        {
            strcpy( buf2, "NO" );
        }

        if( sctAddNameValueToList( attributes, buf1, buf2 ) == SCT_FALSE )
        {
            return SCT_FALSE;
        }
    }

    for( i = 0; i < SCT_ARRAY_LENGTH( _OpenVGPathDatatypeValues ); ++i )
    {
        hwQueryResult = vgHardwareQuery( VG_PATH_DATATYPE_QUERY, _OpenVGPathDatatypeValues[ i ] );

        // Beware buffer overrun
        SCT_ASSERT( ( strlen( _OpenVGPathDatatypeStrings[ i ] ) + strlen( pathDatatypeString ) ) < OPENVG_INFO_BUF1_LENGTH );

        sprintf( buf1, pathDatatypeString, _OpenVGPathDatatypeStrings[ i ] );

        if( hwQueryResult == VG_HARDWARE_ACCELERATED )
        {
            strcpy( buf2, "YES" );
        }
        else
        {
            strcpy( buf2, "NO" );
        }

        if( sctAddNameValueToList( attributes, buf1, buf2 ) == SCT_FALSE )
        {
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}
