/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgpaintradialgradientaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGPaintRadialGradientActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGPaintRadialGradientActionContext*  context;
    SCTOpenVGModuleContext*                     mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGPaintRadialGradientActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGPaintRadialGradientActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PaintRadialGradient@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGPaintRadialGradientActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGPaintRadialGradientActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPaintRadialGradientActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPaintIndex( context->moduleContext, context->data.paintIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGPaintRadialGradientActionContext( context );
        SCT_LOG_ERROR( "Paint index out of range in PaintRadialGradient@OpenVG context creation." );
        return NULL;
    } 

    context->radialGradient[ 0 ] = context->data.centerPoint[ 0 ];
    context->radialGradient[ 1 ] = context->data.centerPoint[ 1 ];
    context->radialGradient[ 2 ] = context->data.focalPoint[ 0 ];
    context->radialGradient[ 3 ] = context->data.focalPoint[ 1 ];
    context->radialGradient[ 4 ] = context->data.radius;

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGPaintRadialGradientActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGPaintRadialGradientActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGPaintRadialGradientActionContext*  context;
    OpenVGPaintRadialGradientActionData*        data;
    SCTOpenVGPaint*                             sctPaint;
    VGErrorCode                                 err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGPaintRadialGradientActionContext* )( action->context );
    data    = &( context->data );

    sctPaint = sctiOpenVGModuleGetPaint( context->moduleContext, data->paintIndex );
    if( sctPaint == NULL )
    {
        SCT_LOG_ERROR( "Invalid paint in PaintRadialGradient@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctPaint->paint != VG_INVALID_HANDLE );
   
    vgSetParameterfv( sctPaint->paint, VG_PAINT_RADIAL_GRADIENT, SCT_ARRAY_LENGTH( context->radialGradient ), context->radialGradient );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in RadialGradient@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();
        
        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGPaintRadialGradientActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGPaintRadialGradientActionContext( action->context );
    action->context = NULL;
}




