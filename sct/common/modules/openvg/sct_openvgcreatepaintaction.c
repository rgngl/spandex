/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatepaintaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreatePaintActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreatePaintActionContext*  context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCreatePaintActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreatePaintActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreatePaint@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreatePaintActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCreatePaintActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreatePaintActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPaintIndex( context->moduleContext, context->data.paintIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreatePaintActionContext( context );
        SCT_LOG_ERROR( "Paint index out of range in CreatePaint@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreatePaintActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreatePaintActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreatePaintActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGCreatePaintActionContext*  context;
    OpenVGCreatePaintActionData*        data;
    int                                 index;
    SCTOpenVGPaint*                     sctPaint;
    VGPaint                             vgPaint;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGCreatePaintActionContext* )( action->context );
    data    = &( context->data );

    index = data->paintIndex;
    if( sctiOpenVGModuleGetPaint( context->moduleContext, data->paintIndex ) != NULL )
    {
        SCT_LOG_ERROR( "Paint index already in use in CreatePaint@OpenVG action execute." );
        return SCT_FALSE;
    }

    vgPaint = vgCreatePaint();
    if( vgPaint == VG_INVALID_HANDLE )
    {
        SCT_LOG_ERROR( "VGPaint creation failed in CreatePaint@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctPaint = sctOpenVGCreatePaint( vgPaint );
    if( sctPaint == NULL )
    {
        vgDestroyPaint( vgPaint );
        SCT_LOG_ERROR( "Allocation in CreatePaint@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleSetPaint( context->moduleContext, index, sctPaint );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreatePaintActionTerminate( SCTAction* action )
{
    SCTOpenVGCreatePaintActionContext*  context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTOpenVGCreatePaintActionContext* )( action->context );

    sctiOpenVGModuleDeletePaint( context->moduleContext, context->data.paintIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreatePaintActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCreatePaintActionContext( action->context );
    action->context = NULL;
}




