/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgshearaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGShearActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGShearActionContext*    context;
    SCTOpenVGModuleContext*         mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGShearActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGShearActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Shear@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGShearActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGShearActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGShearActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGShearActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGShearActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGShearActionContext*    context;
    OpenVGShearActionData*          data;
    VGErrorCode                     err;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGShearActionContext* )( action->context );
    data    = &( context->data );

    vgShear( data->shx, data->shy );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in Shear@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else   /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGShearActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGShearActionContext( action->context );
    action->context = NULL;
}




