/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgclosepathaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"
#include "sct_openvgutils.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGClosePathActionContext( void* moduleContext, SCTAttributeList* attributes )
{
   SCTOpenVGClosePathActionContext* context;
    SCTOpenVGModuleContext*         mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGClosePathActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGClosePathActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ClosePath@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGClosePathActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGClosePathActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGClosePathActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathDataIndex( context->moduleContext, context->data.pathDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGClosePathActionContext( context );
        SCT_LOG_ERROR( "Path data index out of range in ClosePath@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGClosePathActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGClosePathActionExecute( SCTAction* action, int frameNumber )
{
    SCTOpenVGClosePathActionContext*    context;
    OpenVGClosePathActionData*          data;
    VGubyte                             pathSegments[ 1 ];
    SCTOpenVGPathData*                  pathData;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTOpenVGClosePathActionContext* )( action->context );
    data    = &( context->data );

    pathData = sctiOpenVGModuleGetPathData( context->moduleContext, data->pathDataIndex );
    if( pathData == NULL )
    {
        SCT_LOG_ERROR( "Invalid path data in ClosePath@OpenVG action execute." );
        return SCT_FALSE;
    }

    pathSegments[ 0 ] = VG_CLOSE_PATH;

    if( sctOpenVGAddPathData( pathData, 
                              pathSegments, 1,
                              NULL, 0, 
                              ( VGPathDatatype )( -1 ) ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Path data append failed in ClosePath@OpenVG action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGClosePathActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGClosePathActionContext( action->context );
    action->context = NULL;
}
