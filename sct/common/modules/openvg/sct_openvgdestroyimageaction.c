/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdestroyimageaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGDestroyImageActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDestroyImageActionContext* context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGDestroyImageActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDestroyImageActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyImage@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDestroyImageActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGDestroyImageActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyImageActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.imageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyImageActionContext( context );
        SCT_LOG_ERROR( "Image index out of range in DestroyImage@OpenVG creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDestroyImageActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDestroyImageActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGDestroyImageActionContext* context;
    OpenVGDestroyImageActionData*       data;
    int                                 index;
    SCTOpenVGImage*                     sctImage;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGDestroyImageActionContext* )( action->context );
    data    = &( context->data );

    index = data->imageIndex;
    sctImage = sctiOpenVGModuleGetImage( context->moduleContext, index );
    if( sctImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid image in DestroyImage@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( sctImage->image != VG_INVALID_HANDLE );

    sctiOpenVGModuleDeleteImage( context->moduleContext, index );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGDestroyImageActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGDestroyImageActionContext( action->context );
    action->context = NULL;
}
