/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvglutdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGLutDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGLutDataActionContext*  context;
    SCTOpenVGModuleContext*         mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGLutDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGLutDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LutData@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGLutDataActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGLutDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLutDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidLookupTableIndex( context->moduleContext, context->data.lutIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGLutDataActionContext( context );
        SCT_LOG_ERROR( "Invalid lut index in LutData@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGLutDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGLutDataActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGLutDataActionContext*  context;
    OpenVGLutDataActionData*        data;
    int                             index;
    SCTOpenVGLookupTable*           lookupTable;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGLutDataActionContext* )( action->context );
    data    = &( context->data );

    index = data->lutIndex;
    lookupTable = sctiOpenVGModuleGetLookupTable( context->moduleContext, index );
    if( lookupTable == NULL )
    {
        SCT_LOG_ERROR( "Invalid lut index in LutData@OpenVG action execute." );
        return SCT_FALSE;
    }
   
    if( sctOpenVGSetLookupTableData( lookupTable, ( VGuint* )( data->data ) ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Invalid lut data in LutData@OpenVG action execute." );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGLutDataActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGLutDataActionContext( action->context );
    action->context = NULL;
}
