/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcreatelutaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGCreateLutActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGCreateLutActionContext*    context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGCreateLutActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGCreateLutActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CreateLut@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGCreateLutActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGCreateLutActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateLutActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidLookupTableIndex( context->moduleContext, context->data.lutIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGCreateLutActionContext( context );
        SCT_LOG_ERROR( "Invalid lut index in CreateLut@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGCreateLutActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateLutActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGCreateLutActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGCreateLutActionContext*    context;
    OpenVGCreateLutActionData*          data;
    int                                 index;
    SCTOpenVGLookupTable*               lut;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGCreateLutActionContext* )( action->context );
    data    = &( context->data );

    index = data->lutIndex;
    if( sctiOpenVGModuleGetLookupTable( context->moduleContext, index ) != NULL )
    {
        SCT_LOG_ERROR( "Lut index already in use in CreateLut@OpenVG action execute." );
        return SCT_FALSE;
    }

    lut = sctOpenVGCreateLookupTable( data->type );
    if( lut == NULL )
    {
        SCT_LOG_ERROR( "Lut creation failed in CreateLut@OpenVG action execute." );
        return SCT_FALSE;
    }
    sctiOpenVGModuleSetLookupTable( context->moduleContext, index, lut );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGCreateLutActionTerminate( SCTAction* action )
{
    SCTOpenVGCreateLutActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );    

    context = ( SCTOpenVGCreateLutActionContext* )( action->context );
    
    sctiOpenVGModuleDeleteLookupTables( context->moduleContext, context->data.lutIndex );
}

/*!
 *
 *
 */
void sctiOpenVGCreateLutActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGCreateLutActionContext( action->context );
    action->context = NULL;
}
