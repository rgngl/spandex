/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_OPENVGUTILS_H__ )
#define __SCT_OPENVGUTILS_H__

#include "sct_types.h"
#include "sct_vg.h"

/*!
 *
 */
typedef enum
{
    OPENVG_GRADIENT_VERTICAL,
    OPENVG_GRADIENT_HORIZONTAL,
    OPENVG_GRADIENT_DIAGONAL,    
} SCTOpenVGGradient;

/*
 *
 */
typedef struct
{
    void*                       data;
    int                         length;
} SCTOpenVGData;

/*!
 *
 *
 */
typedef struct
{
    void*                       data;
    VGint                       stride;
    VGImageFormat               format;
    VGint                       width;
    VGint                       height;
    VGint                       dataLength;
} SCTOpenVGImageData;

/*!
 *
 *
 */
typedef struct
{
    VGubyte*                    segments;
    int                         numSegments;
    int                         maxNumSegments;
    VGPathDatatype              type;
    void*                       data;
    int                         dataLength;
    int                         maxDataLength;
}  SCTOpenVGPathData;

/*!
 *
 *
 */
typedef struct
{
    VGPath                      path;
    VGPathDatatype              type;
} SCTOpenVGPath;

/*!
 *
 *
 */
typedef struct
{
    VGImage                     image;
    VGint                       width;
    VGint                       height;   
} SCTOpenVGImage;

/*!
 *
 *
 */
typedef struct
{
    VGPaint                     paint;
} SCTOpenVGPaint;

#if defined( OPENVG_VERSION_1_1 )

/*!
 *
 *
 */
typedef struct
{
    VGFont                      font;
} SCTOpenVGFont;

/*!
 *
 *
 */
typedef struct
{
    VGMaskLayer                 maskLayer;
} SCTOpenVGMaskLayer;

#endif /* defined( OPENVG_VERSION_1_1 ) */

/*!
 *
 *
 */
typedef enum
{
    SCT_OPENVG_LUT_UBYTE8,
    SCT_OPENVG_LUT_UINT32
} SCTOpenVGLUTType;

/*!
 *
 *
 */
typedef struct
{
    SCTOpenVGLUTType            type;
    void*                       lookupTable;
} SCTOpenVGLookupTable;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    SCTOpenVGPath*              sctOpenVGCreatePath( VGPath path, VGPathDatatype type );
    void                        sctOpenVGDestroyPath( SCTOpenVGPath* path );

    SCTOpenVGImage*             sctOpenVGCreateImage( VGImage image, int width, int height );
    void                        sctOpenVGDestroyImage( SCTOpenVGImage* image );

    SCTOpenVGPaint*             sctOpenVGCreatePaint( VGPaint paint );
    void                        sctOpenVGDestroyPaint( SCTOpenVGPaint* paint );

#if defined( OPENVG_VERSION_1_1 )
    SCTOpenVGFont*              sctOpenVGCreateFont( VGFont font );
    void                        sctOpenVGDestroyFont( SCTOpenVGFont* font );

    SCTOpenVGMaskLayer*         sctOpenVGCreateMaskLayer( VGMaskLayer maskLayer );
    void                        sctOpenVGDestroyMaskLayer( SCTOpenVGMaskLayer* maskLayer );
#endif /* defined( OPENVG_VERSION_1_1 ) */

    void                        sctOpenVGAssignTypeArray( void* destination, VGPathDatatype type, const VGfloat* source, VGint length );
    void                        sctOpenVGRescaleToDatatypeRange( VGfloat* array, VGfloat* dst, int length, VGPathDatatype datatype, VGfloat* scale, VGfloat* bias );
    SCTBoolean                  sctOpenVGConvertTypeArray( void* dst, VGPathDatatype dstType, void* scc, VGPathDatatype srcType, int length );
    SCTBoolean                  sctOpenVGConvertDoubleArray( void* dst, VGPathDatatype dstType, double* src, int length );

    SCTOpenVGData*              sctOpenVGCreateData( void* data, int length, SCTBoolean copy );
    void                        sctOpenVGDestroyData( SCTOpenVGData* data );
    
    SCTOpenVGImageData*         sctOpenVGCreateImageData( VGImageFormat format, int width, int height );
    SCTOpenVGImageData*         sctOpenVGCreateSolidImageData( VGImageFormat format, int width, int height, const float color[ 4 ] );
    SCTOpenVGImageData*         sctOpenVGCreateBarImageData( VGImageFormat format, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int bars );
    SCTOpenVGImageData*         sctOpenVGCreateCheckerImageData( VGImageFormat format, int width, int height, const float color1[ 4 ], const float color2[ 4 ], int checkersX, int checkersY );
    SCTOpenVGImageData*         sctOpenVGCreateGradientImageData( VGImageFormat format, int width, int height, const float color1[ 4 ], const float color2[ 4 ], SCTOpenVGGradient gradient );
    
    void                        sctOpenVGDestroyImageData( SCTOpenVGImageData* data );

    void                        sctOpenVGInitializeLUT8( VGubyte* lut, int length, float start, float end );
    void                        sctOpenVGInitializeLUT32( VGuint* lut, int length, float start[ 4 ], float end[ 4 ] );
    SCTBoolean                  sctOpenVGFloatsEqual( VGfloat a, VGfloat b, VGfloat delta );

    SCTOpenVGPathData*          sctOpenVGCreatePathData( VGPathDatatype type, int initialSegments, int initialData );
    SCTBoolean                  sctOpenVGAddPathData( SCTOpenVGPathData* pathData, const VGubyte* segments, int numSegments, void* data, int dataLength, VGPathDatatype dataType );
    void                        sctOpenVGClearPathData( SCTOpenVGPathData* pathData );
    void                        sctOpenVGDestroyPathData( SCTOpenVGPathData* pathData );

    SCTOpenVGLookupTable*       sctOpenVGCreateLookupTable( SCTOpenVGLUTType type );
    SCTBoolean                  sctOpenVGSetLookupTableData( SCTOpenVGLookupTable* lut, VGuint values[ 256 ] );
    void                        sctOpenVGDestroyLookupTable( SCTOpenVGLookupTable* lut );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_OPENVGUTILS_H__ ) */
