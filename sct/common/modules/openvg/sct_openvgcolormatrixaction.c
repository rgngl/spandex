/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgcolormatrixaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGColorMatrixActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGColorMatrixActionContext*  context;
    SCTOpenVGModuleContext*             mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGColorMatrixActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGColorMatrixActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ColorMatrix@OpenVG action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGColorMatrixActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGColorMatrixActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGColorMatrixActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.dstImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGColorMatrixActionContext( context );
        SCT_LOG_ERROR( "Destination image index out of range in ColorMatrix@OpenVG context creation." );
        return NULL;
    } 

    if( sctiOpenVGModuleIsValidImageIndex( context->moduleContext, context->data.srcImageIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGColorMatrixActionContext( context );
        SCT_LOG_ERROR( "Source image index out of range in ColorMatrix@OpenVG context creation." );
        return NULL;
    } 

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGColorMatrixActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGColorMatrixActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGColorMatrixActionContext*  context;
    OpenVGColorMatrixActionData*        data;
    VGErrorCode                         err;
    SCTOpenVGImage*                     dstImage;
    SCTOpenVGImage*                     srcImage;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGColorMatrixActionContext* )( action->context );
    data    = &( context->data );

    dstImage = sctiOpenVGModuleGetImage( context->moduleContext, data->dstImageIndex );
    if( dstImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid destination image in ColorMatrix@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( dstImage->image != VG_INVALID_HANDLE );

    srcImage = sctiOpenVGModuleGetImage( context->moduleContext, data->srcImageIndex );
    if( srcImage == NULL )
    {
        SCT_LOG_ERROR( "Invalid source image index in ColorMatrix@OpenVG action execute." );
        return SCT_FALSE;
    }
    SCT_ASSERT( srcImage->image != VG_INVALID_HANDLE );

    vgColorMatrix( dstImage->image, srcImage->image, data->matrix );

#if defined( SCT_OPENVG_MODULE_CHECK_ERRORS )
    /* Check for errors. */
    err = vgGetError();
    if( err != VG_NO_ERROR )
    {
        char buf[ 256 ];
        sprintf( buf, "VG error %s in ColorMatrix@OpenVG action execute.", sctiOpenVGGetErrorString( err ) );
        SCT_LOG_ERROR( buf );
        sctiOpenVGClearVGError();

        return SCT_FALSE;
    }
#else  /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */
    SCT_USE_VARIABLE( err );
#endif /* defined( SCT_OPENVG_MODULE_CHECK_ERRORS ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGColorMatrixActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGColorMatrixActionContext( action->context );
    action->context = NULL;
}
