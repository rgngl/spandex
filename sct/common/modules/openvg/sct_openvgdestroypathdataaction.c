/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_openvgdestroypathdataaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_vg.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateOpenVGDestroyPathDataActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTOpenVGDestroyPathDataActionContext*  context;
    SCTOpenVGModuleContext*                 mc;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    mc      = ( SCTOpenVGModuleContext* )( moduleContext );
    context = ( SCTOpenVGDestroyPathDataActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTOpenVGDestroyPathDataActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in DestroyPathData@OpenVG context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTOpenVGDestroyPathDataActionContext ) );

    context->moduleContext = mc;

    if( sctiParseOpenVGDestroyPathDataActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyPathDataActionContext( context );
        return NULL;
    }

    if( sctiOpenVGModuleIsValidPathDataIndex( context->moduleContext, context->data.pathDataIndex ) == SCT_FALSE )
    {
        sctiDestroyOpenVGDestroyPathDataActionContext( context );
        SCT_LOG_ERROR( "Invalid path data index in DestroyPathData@OpenVG context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyOpenVGDestroyPathDataActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiOpenVGDestroyPathDataActionExecute( SCTAction* action, int framenumber )
{
    SCTOpenVGDestroyPathDataActionContext*  context;
    OpenVGDestroyPathDataActionData*        data;
    int                                     index;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTOpenVGDestroyPathDataActionContext* )( action->context );
    data    = &( context->data );

    index = data->pathDataIndex;
    if( sctiOpenVGModuleGetPathData( context->moduleContext, data->pathDataIndex ) == NULL )
    {
        SCT_LOG_ERROR( "Invalid path data index in DestroyPathData@OpenVG action execute." );
        return SCT_FALSE;
    }

    sctiOpenVGModuleDeletePathData( context->moduleContext, index );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiOpenVGDestroyPathDataActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyOpenVGDestroyPathDataActionContext( action->context );
    action->context = NULL;
}
