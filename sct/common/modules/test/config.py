#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# NOTE: Test module is mainly for test and benchmark development.

######################################################################
SetModuleName( 'Test' )

######################################################################
AddInclude( 'sct_testmodule.h' )

MAX_MESSAGE_LENGTH          = 256
MAX_TIMER_INDEX             = 256
MAX_MEMORY_COUNTER_INDEX    = 256

######################################################################
# Error. Trigger an error in the benchmark. Can be used for internal testing
# purposes to ensure spandex core recovers from benchmark errors. Use the frame
# number variable to select a specific benchmark frame which triggers the
# error. Frame number -1 means the error is always triggered, also as a part of
# init and terminate.
AddActionConfig( 'Error',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'FrameNumber',                          -1 ),
                   StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH )
                   ]
                 )

######################################################################
# RandomError. Trigger a random error in the benchmark. Can be used for internal
# testing purposes to ensure spandex core recovers from random benchmark errors,
# especially when used with infinite benchmark loops.
AddActionConfig( 'RandomError',
                 'Init'+'Execute'+'Terminate'+'Destroy',                 
                 [ FloatAttribute(              'Frequency',                            0, 1 ),
                   StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH )
                   ]
                 )

######################################################################
# Assert. Assert the spandex process. Can be used for internal testing/benchmark
# development purposes for example to ensure that some actions are not executed
# unexpectly. Use the frame number variable to select a specific benchmark frame
# which triggers the assert. Frame number -1 means the assert is always
# triggered, also as a part of init and terminate.
AddActionConfig( 'Assert',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'FrameNumber',                          -1 )
                   ]
                 )

######################################################################
# AssertExecuted. Assert the spandex process if the action is terminated without
# being executed. Can be used for internal testing/benchmark development
# purposes to ensure some actions are executed.
AddActionConfig( 'AssertExecuted',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 []
                 )

######################################################################
# AssertBenchmark. Assert the spandex process if the action was included in a
# failed benchmark. Benchmark failure is checked when the action is
# terminated. Useful for detecting random failures in infinite benchmark loops.
AddActionConfig( 'AssertBenchmark',
                 'Init'+'Execute'+'Terminate'+'Destroy',                 
                 [ ]
                 )

######################################################################
# Print. Print a message using siCommonDebugPrintf.
AddActionConfig( 'Print',
                 'Execute'+'Destroy',
                 [ StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH )
                   ]
                 )

######################################################################
# PrintFrame. Print the benchmark frame number with a message using
# siCommonDebugPrintf.
AddActionConfig( 'PrintFrame',
                 'Execute'+'Destroy',
                 [ StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH )
                   ]
                 )

######################################################################
# SetTimer. Set timer to match current system tick count.
AddActionConfig( 'SetTimer',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TimerIndex',                           0, MAX_TIMER_INDEX - 1 )
                   ]
                 )

######################################################################
# PrintSince. Print time in milliseconds since the last SetTimer using
# siCommonDebugPrintf.
AddActionConfig( 'PrintSince',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TimerIndex',                           0, MAX_TIMER_INDEX - 1 ),
                   StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH ),
                   ]
                 )

######################################################################
# LogSince. Per execution, log time in milliseconds since the last SetTimer, and
# at the action terminate write values into output file as a [Message:
# LOG_SINCE] section. MaxSampleCount defines the buffer size for logged values;
# overflowing values are discarded.
AddActionConfig( 'LogSince',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'TimerIndex',                           0, MAX_TIMER_INDEX - 1 ),
                   StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH ),
                   IntAttribute(                'MaxSampleCount',                       1 ),
                   ]
                 )

######################################################################
# QueryMemory. Query the value of given memory type into a memory
# counter. Please note that in some systems (e.g. in systems with EGL/EGL
# resource profiling extension), this action can be expected to work properly
# only after the benchmark has initialized EGL.
AddActionConfig( 'QueryMemory',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CounterIndex',                         0, MAX_MEMORY_COUNTER_INDEX - 1 ),
                   EnumAttribute(               'Type', 'SCTMemoryType',                [ 'SCT_TOTAL_SYSTEM_MEMORY',
                                                                                          'SCT_FREE_SYSTEM_MEMORY',
                                                                                          'SCT_USED_SYSTEM_MEMORY',
                                                                                          'SCT_TOTAL_GRAPHICS_MEMORY',
                                                                                          'SCT_FREE_GRAPHICS_MEMORY',
                                                                                          'SCT_USED_GRAPHICS_MEMORY',
                                                                                          'SCT_USED_PRIVATE_GRAPHICS_MEMORY',
                                                                                          'SCT_USED_SHARED_GRAPHICS_MEMORY' ] ),
                   ]
                 )

######################################################################
# PrintMemoryCounter. Print memory counter value using siCommonDebugPrintf.
AddActionConfig( 'PrintMemoryCounter',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CounterIndex',                         0, MAX_MEMORY_COUNTER_INDEX - 1 ),
                   StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH ),
                   ]
                 )

######################################################################
# LogMemoryCounter. Per frame, samples memory counter value, and at the action
# terminate prints samples into output file as a [Message: LOG_MEMORY_COUNTER]
# section. MaxSampleCount defines the buffer size for logged samples;
# overflowing samples are discarded.
AddActionConfig( 'LogMemoryCounter',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ IntAttribute(                'CounterIndex',                         0, MAX_MEMORY_COUNTER_INDEX - 1 ),
                   StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH ),
                   IntAttribute(                'MaxSampleCount',                       1 ),
                   ]
                 )

######################################################################
# CompareMemoryCounter. Compare memory counter value to reference value using
# defined condition; value CONDITION reference. The action reports an error if
# the condition does pass. Note: at the moment supports only 32-bit reference
# values.
AddActionConfig( 'CompareMemoryCounter',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'CounterIndex',                         0, MAX_MEMORY_COUNTER_INDEX - 1 ),
                   StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH ),
                   Hex32Attribute(              'Reference' ),
                   EnumAttribute(               'Condition', 'SCTCondition',            [ 'SCT_LESS',
                                                                                          'SCT_LEQUAL',
                                                                                          'SCT_GREATER',
                                                                                          'SCT_GEQUAL',
                                                                                          'SCT_EQUAL',
                                                                                          'SCT_NOTEQUAL', ] ),
                   ]
                 )

######################################################################
# Sleep. Sleep for a specified time in milliseconds.
AddActionConfig( 'Sleep',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Millis',                               0 ),
                   ]
                 )

######################################################################
# Delay. Sleep so that there will be at least millis delay between the last
# matching SetTimer and the action following the delay.
AddActionConfig( 'Delay',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TimerIndex',                           0, MAX_TIMER_INDEX - 1 ),
                   IntAttribute(                'Millis',                               0 ),
                   ]
                 )

######################################################################
# Busy. Generate CPU load for a specified time in milliseconds. Note that actual
# CPU load needed may vary between action execution depending on the system
# load.
AddActionConfig( 'Busy',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Millis',                               0 ),
                   ]
                 )

######################################################################
# BusyDelay. Generate CPU load so that there will be at least millis busy delay
# between the last matching SetTimer and the action following the delay. Note
# that actual CPU load needed may vary between action execution depending on the
# system load.
AddActionConfig( 'BusyDelay',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'TimerIndex',                           0, MAX_TIMER_INDEX - 1 ),
                   IntAttribute(                'Millis',                               0 ),
                   ]
                 )

######################################################################
# CpuLoad. Generate a fixed CPU load that should take a specified time in
# milliseconds to complete in an idle system. The fixed load is calculated at
# the action creation time, so it might be possible that its execution does not
# take exactly the defined time during action execute.
AddActionConfig( 'CpuLoad',
                 'Execute'+'Destroy',
                 [ IntAttribute(                'Millis',                               0 ),
                   ]
                 )

######################################################################
# LogMessage. Per frame, receive a pending message of matching type, and at the
# action terminate print the message values into output file as a [Message:
# LOG_MESSAGE] section. StartFrame can be used to define which frame is used to
# start receiving messages; this can be used to prevent blocking when receiving
# messages. MaxSampleCount defines the buffer size for logged values;
# overflowing samples are discarded. Use 0 timeout for infinite wait; note that
# infinite wait might still be limited by the system. The action reports an
# error if the action message type does not match the type of the received
# message. Note that this action requires that the underlying platform supports
# messages, i.e. implements the siCommonGetMessage function.
AddActionConfig( 'LogMessage',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH ),
                   IntAttribute(                'StartFrame',                           0 ),
                   EnumAttribute(               'Type', 'SCTMessageType',               [ 'SCT_MESSAGE_POST_BUFFER',
                                                                                          'SCT_MESSAGE_GET_BUFFER', ] ),
                   IntAttribute(                'TimeoutMillis',                        0 ),
                   IntAttribute(                'MaxSampleCount',                       1 ),
                   ]
                 )

######################################################################
# LogCpuLoad. Log CPU usage over an interval of frames, and at the action
# terminate print the message values into output file as a [Message:
# LOG_CPU_LOAD] section. Type defines the cpu load to log; system load or the
# ratio of process load to system load. MaxSampleCount defines the buffer size
# for logged values; overflowing samples are discarded. Note that this action
# requires that the underlying platform supports cpu load profiling,
# i.e. implements the siCommonSampleCpuLoad and related functions.
AddActionConfig( 'LogCpuLoad',
                 'Init'+'Execute'+'Terminate'+'Destroy',
                 [ StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH ),
                   EnumAttribute(               'Type', 'SCTCpuLoad',                   [ 'SCT_CPULOAD_SYSTEM',
                                                                                          'SCT_CPULOAD_PROCESS2BUSY',
                                                                                          'SCT_CPULOAD_PROCESS2TOTAL', ] ),
                   IntAttribute(                'Interval',                             1 ),
                   IntAttribute(                'MaxSampleCount',                       1 ),
                   ]
                 )

######################################################################
# Monitor. Monitor benchmark execution. Operation specifies monitor operation
# type, parameters defines the operation type specific parameters. Message can
# be used to identify the specific monitor instance in the output. Note that the
# operations printing into output file do not currently handle excape characters
# such as newline properly; escape characters in the message are best avoided.
#
# SCT_MONITOR_AVERAGE_FPS operation uses parameters {Samples} which defines the
# number of sequential timings captured from the benchmark loop iterations to
# use for calculating the average FPS. Samples must be >= 1. Calculated average
# FPS values are reported using siCommonDebugPrintf.
#
# SCT_TEST_AVERAGE_FPS operation uses parameters {Samples, Min, Max}. Samples
# operate as specified in MONITOR_AVERAGE_FPS operation above. Min defines the
# minimum allowed for the average FPS, Max defines the maximum. Both values are
# inclusive. Using negative values for Min and Max means the particular check is
# ignored. Out of bounds FPS values are reported using siCommonDebugPrintf.
#
# SCT_FORCE_AVERAGE_FPS operation is similar to TEST_AVERAGE_FPS test above,
# with the exception that out of bounds FPS values cause a benchmark error.
#
# SCT_LOG_AVERAGE_FPS operation uses parameters {Samples,
# MaxAverageCount}. Samples is used as defined above. MaxAverageCount defines
# the buffer size for logged averages. MaxAverageCount must be >= 1, averages
# overflow in last-out style. The operation stores averages and prints them into
# output file as [Message: LOG_AVERAGE_FPS_MONITOR] section at the action
# terminate.
#
# SCT_LOG_MILLIS operation uses parameters {MaxSampleCount}. MaxSampleCount
# defines the buffer size for logged samples; milliseconds between
# frames. MaxSampleCount must be >= 1, averages overflow in last-out style. The
# operation stores samples and prints them into output file as [Message:
# LOG_MILLIS_MONITOR] section at the action terminate.
#
# SCT_AVERAGE_FPS operation does not use any parameters. It prints the benchmark
# loop average FPS into output file as [Message: AVERAGE_FPS_MONITOR] section.
#
# SCT_MONITOR_LOAD operation uses parameters {Interval} which defines the
# frequency in benchmark frames in which to sample the system load. Interval must 
# be >= 1. Calculated system load values are reported using siCommonDebugPrintf. 
# Note that this operation requires that the underlying platform supports CPU 
# load profiling, i.e. implements the siCommonSampleCpuLoad and related functions.
#
# Note that Monitor can be used only in benchmark action list.
AddActionConfig( 'Monitor',
                 'Init'+'Execute'+'Terminate'+'Destroy',                 
                 [ EnumAttribute(               'Operation', 'SCTMonitorOperation',     [ 'SCT_MONITOR_AVERAGE_FPS',
                                                                                          'SCT_TEST_AVERAGE_FPS',
                                                                                          'SCT_FORCE_AVERAGE_FPS',
                                                                                          'SCT_LOG_AVERAGE_FPS',
                                                                                          'SCT_LOG_MILLIS',
                                                                                          'SCT_AVERAGE_FPS',
                                                                                          'SCT_MONITOR_LOAD' ] ),
                   
                   DoubleVectorAttribute(       'Parameters' ),
                   StringAttribute(             'Message',                              MAX_MESSAGE_LENGTH ),
                   ]
                 )

######################################################################
# ProfileEvent. Send profile event e.g. to system debug output.
AddActionConfig( 'ProfileEvent',
                 'Execute'+'Destroy',
                 [ StringAttribute(             'Event',                                MAX_MESSAGE_LENGTH )
                   ]
                 )

######################################################################
# END OF FILE
######################################################################
