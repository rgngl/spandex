/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testcomparememorycounteraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestCompareMemoryCounterActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestCompareMemoryCounterActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestCompareMemoryCounterActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestCompareMemoryCounterActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CompareMemoryCounter@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestCompareMemoryCounterActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestCompareMemoryCounterActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestCompareMemoryCounterActionContext( context );
        return NULL;
    }

    if( sctiTestModuleIsValidMemoryCounterIndex( context->moduleContext, context->data.counterIndex ) == SCT_FALSE )
    {
        sctiDestroyTestCompareMemoryCounterActionContext( context );
        SCT_LOG_ERROR( "Invalid counter index in CompareMemoryCounter@Test context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestCompareMemoryCounterActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestCompareMemoryCounterActionExecute( SCTAction* action, int framenumber )
{
    SCTTestCompareMemoryCounterActionContext*   context;
    TestCompareMemoryCounterActionData*         data;
    size_t                                      reference;
    size_t                                      value;
    char                                        referenceBuf[ 32 ];
    char                                        valueBuf[ 32 ];
    char                                        buffer[ 256 ];            

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestCompareMemoryCounterActionContext* )( action->context );
    data    = &( context->data );

    reference = data->reference;
    value     = sctiTestModuleGetMemoryCounter( context->moduleContext, data->counterIndex );

    SCT_ASSERT_ALWAYS( sctFormatSize_t( referenceBuf, sizeof( referenceBuf ), reference ) == SCT_TRUE );
    SCT_ASSERT_ALWAYS( sctFormatSize_t( valueBuf, sizeof( valueBuf ), value ) == SCT_TRUE );
    
    switch( data->condition )
    {
    case SCT_LESS:
        if( value >= reference )
        {
            sprintf( buffer, "Condition %s SCT_LESS %s failed in \"%s\" CompareMemoryCounter@Test action execute.", valueBuf, referenceBuf, data->message ); 
            SCT_LOG_ERROR( buffer );
            return SCT_FALSE;
        }           
        break;

    case SCT_LEQUAL:
        if( value > reference )
        {
            sprintf( buffer, "Condition %s SCT_LEQUAL %s failed in \"%s\" CompareMemoryCounter@Test action execute.", valueBuf, referenceBuf, data->message ); 
            SCT_LOG_ERROR( buffer );
            return SCT_FALSE;
        }
        break;

    case SCT_GREATER:
        if( value <= reference )
        {
            sprintf( buffer, "Condition %s SCT_GREATER %s failed in \"%s\" CompareMemoryCounter@Test action execute.", valueBuf, referenceBuf, data->message ); 
            SCT_LOG_ERROR( buffer );
            return SCT_FALSE;
        }
        break;

    case SCT_GEQUAL:
        if( value < reference )
        {
            sprintf( buffer, "Condition %s SCT_GEQUAL %s failed in \"%s\" CompareMemoryCounter@Test action execute.", valueBuf, referenceBuf, data->message ); 
            SCT_LOG_ERROR( buffer );
            return SCT_FALSE;
        }
        break;

    case SCT_EQUAL:
        if( value != reference )
        {
            sprintf( buffer, "Condition %s SCT_EQUAL %s failed in \"%s\" CompareMemoryCounter@Test action execute.", valueBuf, referenceBuf, data->message ); 
            SCT_LOG_ERROR( buffer );
            return SCT_FALSE;
        }
        break;

    case SCT_NOTEQUAL:
        if( value == reference )
        {
            sprintf( buffer, "Condition %s SCT_NOTEQUAL %s failed in \"%s\" CompareMemoryCounter@Test action execute.", valueBuf, referenceBuf, data->message ); 
            SCT_LOG_ERROR( buffer );
            return SCT_FALSE;
        }

        break;

    default:
        sprintf( buffer, "Unexpected condition in \"%s\" CompareMemoryCounter@Test action execute.", data->message ); 
        SCT_LOG_ERROR( buffer );
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestCompareMemoryCounterActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestCompareMemoryCounterActionContext( ( SCTTestCompareMemoryCounterActionContext* )( action->context ) );
    action->context = NULL;
}

