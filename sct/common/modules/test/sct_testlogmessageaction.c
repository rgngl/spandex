/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testlogmessageaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

#define SCT_DRAIN_TIMEOUT_MILLIS    50

/*!
 *
 *
 */
void* sctiCreateTestLogMessageActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestLogMessageActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestLogMessageActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestLogMessageActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LogMessage@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestLogMessageActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestLogMessageActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestLogMessageActionContext( context );
        return NULL;
    }

    context->bufferLength = context->data.maxSampleCount;
    context->buffer = ( double* )( siCommonMemoryAlloc( NULL, context->bufferLength * sizeof( double ) ) );
    
    if( context->buffer == NULL )
    {
        sctiDestroyTestLogMessageActionContext( context );        
        SCT_LOG_ERROR( "Allocation failed in LogMessage@Test context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestLogMessageActionContext( void* context )
{
    SCTTestLogMessageActionContext* c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTTestLogMessageActionContext* )( context );
   
    if( c->buffer != NULL )
    {
        siCommonMemoryFree( NULL, c->buffer );
        c->buffer = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestLogMessageActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTTestLogMessageActionContext* c;
    SCTMessage                      m;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    c = ( SCTTestLogMessageActionContext* )( action->context );
    c->bufferIndex = 0;
    c->reported    = SCT_FALSE;

    /* Drain pending messages. */
    while( siCommonGetMessage( NULL, &m, SCT_DRAIN_TIMEOUT_MILLIS ) > 0 );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiTestLogMessageActionExecute( SCTAction* action, int framenumber )
{
    SCTTestLogMessageActionContext* context;
    TestLogMessageActionData*       data;
    SCTMessage                      m;
    int                             s;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestLogMessageActionContext* )( action->context );
    data    = &( context->data );

    if( framenumber >= data->startFrame )
    {
        if( context->bufferIndex < context->bufferLength )
        {
            m.type   = SCT_MESSAGE_POST_BUFFER;
            m.millis = 0;
            m.value  = 0;
            
            s = siCommonGetMessage( NULL, &m, data->timeoutMillis );

            if( s != 1 )
            {
                SCT_LOG_ERROR( "siCommonGetMessage failed in LogMessage@Test action execute." );        
                return SCT_FALSE;
            }
            
            if( m.type != data->type )
            {
                SCT_LOG_ERROR( "Unexpected message type in LogMessage@Test action execute." );        
                return SCT_FALSE;
            }
            
            /* TODO: can there be explicitly cancelled siCommonGetMessages in
             * multithread cases? */
            context->buffer[ context->bufferIndex ] = m.value;
        }        
    }

    context->bufferIndex++;
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestLogMessageActionTerminate( SCTAction* action )
{
    SCTTestLogMessageActionContext* context;
    TestLogMessageActionData*       data;
    char                            buf1[ 32 ];
    char*                           buf2;
    int                             min;
    int                             i;
    SCTAttributeList*               attributes;
    SCTMessage                      m;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTTestLogMessageActionContext* )( action->context );
    data    = &( context->data );

    /* Drain pending messages. */
    while( siCommonGetMessage( NULL, &m, SCT_DRAIN_TIMEOUT_MILLIS ) > 0 );
    
    if( context->reported != SCT_FALSE )
    {
        return;
    }
    
    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        return;
    }
    
    buf2 = ( char* )( siCommonMemoryAlloc( NULL, context->bufferIndex * sizeof( buf1 ) ) );
    if( buf2 != NULL )
    {
        buf2[ 0 ] = '\0';
        
        min = sctMin( context->bufferIndex, data->maxSampleCount );
        for( i = 0; i < min; ++i )
        {
            sprintf( buf1, "%.1f, ", context->buffer[ i ] );
            strcat( buf2, buf1 );                   
        }
        
        sctAddNameValueToList( attributes, "Milliseconds", buf2 );
        siCommonMemoryFree( NULL, buf2 );
    }
    
    if( context->bufferIndex > data->maxSampleCount )
    {
        sctAddNameValueToList( attributes, "Warning", "Sample buffer overrrun" );
    }                
        
    sctReportSection( data->message, "LOG_MESSAGE", attributes );                
    sctDestroyAttributeList( attributes );

    context->reported = SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestLogMessageActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestLogMessageActionContext( ( SCTTestLogMessageActionContext* )( action->context ) );
    action->context = NULL;
}
