/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testbusydelayaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestBusyDelayActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestBusyDelayActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestBusyDelayActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestBusyDelayActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in BusyDelay@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestBusyDelayActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestBusyDelayActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestBusyDelayActionContext( context );
        return NULL;
    }

    if( sctiTestModuleIsValidTimerIndex( context->moduleContext, context->data.timerIndex ) == SCT_FALSE )
    {
        sctiDestroyTestBusyDelayActionContext( context );        
        SCT_LOG_ERROR( "Invalid timer index in BusyDelay@Test context creation." );
        return NULL;
    }

    context->frequency = siCommonGetTimerFrequency( NULL );
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestBusyDelayActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestBusyDelayActionExecute( SCTAction* action, int framenumber )
{
    SCTTestBusyDelayActionContext*  context;
    TestBusyDelayActionData*        data;
    unsigned long                   latest;
    unsigned long                   now;
    int                             millis;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestBusyDelayActionContext* )( action->context );
    data    = &( context->data );

    latest = sctiTestModuleGetTimer( context->moduleContext, data->timerIndex );
    now    = siCommonGetTimerTick( NULL );
    millis = ( int )( data->millis - ( ( now - latest ) / ( double )( context->frequency ) ) * 1000.0 );

    if( millis > 0 )
    {
        sctiTestModuleBusy( context->moduleContext, millis );
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestBusyDelayActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestBusyDelayActionContext( ( SCTTestBusyDelayActionContext* )( action->context ) );
    action->context = NULL;
}

