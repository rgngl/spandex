/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testquerymemoryaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_types.h"
#include "sct_result.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestQueryMemoryActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestQueryMemoryActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestQueryMemoryActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestQueryMemoryActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in QueryMemory@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestQueryMemoryActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestQueryMemoryActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestQueryMemoryActionContext( context );
        return NULL;
    }

    if( sctiTestModuleIsValidMemoryCounterIndex( context->moduleContext, context->data.counterIndex ) == SCT_FALSE )
    {
        sctiDestroyTestQueryMemoryActionContext( context );
        SCT_LOG_ERROR( "Invalid counter index in QueryMemory@Test context creation." );
        return NULL;
    }
        
    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestQueryMemoryActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestQueryMemoryActionExecute( SCTAction* action, int framenumber )
{
    SCTTestQueryMemoryActionContext*    context;
    TestQueryMemoryActionData*          data;
    size_t                              v;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestQueryMemoryActionContext* )( action->context );
    data    = &( context->data );

    switch( data->type )
    {
    case SCT_TOTAL_SYSTEM_MEMORY:
        v = siCommonQueryMemoryAttribute( NULL, SCT_MEMATTRIB_TOTAL_SYSTEM_MEMORY );
        break;
    case SCT_FREE_SYSTEM_MEMORY:
        v = siCommonQueryMemoryAttribute( NULL, SCT_MEMATTRIB_TOTAL_SYSTEM_MEMORY ) - siCommonQueryMemoryAttribute( NULL, SCT_MEMATTRIB_USED_SYSTEM_MEMORY );
        break;
        
    case SCT_USED_SYSTEM_MEMORY:
        v = siCommonQueryMemoryAttribute( NULL, SCT_MEMATTRIB_USED_SYSTEM_MEMORY );
        break;
        
    case SCT_TOTAL_GRAPHICS_MEMORY:
        v = siCommonQueryMemoryAttribute( NULL, SCT_MEMATTRIB_TOTAL_GRAPHICS_MEMORY );
        break;

    case SCT_FREE_GRAPHICS_MEMORY:
        v = siCommonQueryMemoryAttribute( NULL, SCT_MEMATTRIB_TOTAL_GRAPHICS_MEMORY ) - siCommonQueryMemoryAttribute( NULL, SCT_MEMATTRIB_USED_GRAPHICS_MEMORY );
        break;
        
    case SCT_USED_GRAPHICS_MEMORY:
        v = siCommonQueryMemoryAttribute( NULL, SCT_MEMATTRIB_USED_GRAPHICS_MEMORY );        
        break;
        
    case SCT_USED_PRIVATE_GRAPHICS_MEMORY:
        v = siCommonQueryMemoryAttribute( NULL, SCT_MEMATTRIB_USED_PRIVATE_GRAPHICS_MEMORY );
        break;
        
    case SCT_USED_SHARED_GRAPHICS_MEMORY:
        v = siCommonQueryMemoryAttribute( NULL, SCT_MEMATTRIB_USED_SHARED_GRAPHICS_MEMORY );
        break;

    default:
        SCT_LOG_ERROR( "Unexpected memory counter type in QueryMemory@Test action execute." );
        return SCT_FALSE;
    }

    sctiTestModuleSetMemoryCounter( context->moduleContext, data->counterIndex, v );
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestQueryMemoryActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestQueryMemoryActionContext( ( SCTTestQueryMemoryActionContext* )( action->context ) );
    action->context = NULL;
}

