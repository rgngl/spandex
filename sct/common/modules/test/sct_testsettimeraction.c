/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testsettimeraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestSetTimerActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestSetTimerActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestSetTimerActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestSetTimerActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in SetTimer@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestSetTimerActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestSetTimerActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestSetTimerActionContext( context );
        return NULL;
    }

    if( sctiTestModuleIsValidTimerIndex( context->moduleContext, context->data.timerIndex ) == SCT_FALSE )
    {
        sctiDestroyTestSetTimerActionContext( context );        
        SCT_LOG_ERROR( "Invalid timer index in SetTimer@Test context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestSetTimerActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestSetTimerActionExecute( SCTAction* action, int framenumber )
{
    SCTTestSetTimerActionContext*   context;
    TestSetTimerActionData*         data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestSetTimerActionContext* )( action->context );
    data    = &( context->data );

    sctiTestModuleSetTimer( context->moduleContext, data->timerIndex, siCommonGetTimerTick( NULL ) );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestSetTimerActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestSetTimerActionContext( ( SCTTestSetTimerActionContext* )( action->context ) );
    action->context = NULL;
}

