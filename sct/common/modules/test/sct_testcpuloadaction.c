/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testcpuloadaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestCpuLoadActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestCpuLoadActionContext*    context;
    int                             i;
    unsigned long                   iterations;
    unsigned long                   sum;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestCpuLoadActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestCpuLoadActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in CpuLoad@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestCpuLoadActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestCpuLoadActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestCpuLoadActionContext( context );
        return NULL;
    }

#define _ITERATIONS 4
    
    sum        = 0;
    iterations = 0;
    for( i = 0; i < _ITERATIONS; ++i )
    {
        iterations = sctiTestModuleGetBusyIterations( context->moduleContext, context->data.millis );

        if( sum + iterations < sum )
        {
            /* Overflow */
            break;
        }
        
        sum += iterations;
    }

    if( i == _ITERATIONS )
    {
        context->iterations = iterations;
    }
    else
    {
        context->iterations = sum / _ITERATIONS;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestCpuLoadActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestCpuLoadActionExecute( SCTAction* action, int framenumber )
{
    SCTTestCpuLoadActionContext*    context;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestCpuLoadActionContext* )( action->context );

    sctiTestModuleDoBusyIterations( context->moduleContext, context->iterations );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestCpuLoadActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestCpuLoadActionContext( ( SCTTestCpuLoadActionContext* )( action->context ) );
    action->context = NULL;
}

