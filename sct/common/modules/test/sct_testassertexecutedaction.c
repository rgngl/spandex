/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testassertexecutedaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestAssertExecutedActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestAssertExecutedActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestAssertExecutedActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestAssertExecutedActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in AssertExecuted@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestAssertExecutedActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestAssertExecutedActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestAssertExecutedActionContext( context );
        return NULL;
    }

    context->executed = SCT_FALSE;
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestAssertExecutedActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestAssertExecutedActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTTestAssertExecutedActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTTestAssertExecutedActionContext* )( action->context );

    context->executed = SCT_FALSE;
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiTestAssertExecutedActionExecute( SCTAction* action, int framenumber )
{
    SCTTestAssertExecutedActionContext* context;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestAssertExecutedActionContext* )( action->context );

    context->executed = SCT_TRUE;

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestAssertExecutedActionTerminate( SCTAction* action )
{
    SCTTestAssertExecutedActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTTestAssertExecutedActionContext* )( action->context );

    SCT_ASSERT_ALWAYS( context->executed != SCT_FALSE );
    context->executed = SCT_FALSE;
}

/*!
 *
 *
 */
void sctiTestAssertExecutedActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestAssertExecutedActionContext( ( SCTTestAssertExecutedActionContext* )( action->context ) );
    action->context = NULL;
}

