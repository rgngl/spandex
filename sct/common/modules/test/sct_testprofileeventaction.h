/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Kari J. Kangas.
 *
 * Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_TESTPROFILEEVENTACTION_H__ )
#define __SCT_TESTPROFILEEVENTACTION_H__

#include "sct_types.h"
#include "sct_testmodule.h"
#include "sct_testmodule_parser.h"

/*!
 *
 */
typedef struct
{
    SCTTestModuleContext*               moduleContext;
    TestProfileEventActionData          data;
} SCTTestProfileEventActionContext;
 
#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*								sctiCreateTestProfileEventActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroyTestProfileEventActionContext( void* context );

    SCTBoolean                          sctiTestProfileEventActionExecute( SCTAction *action, int framenumber );
    void                                sctiTestProfileEventActionDestroy( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_TESTPROFILEEVENTACTION_H__ ) */
