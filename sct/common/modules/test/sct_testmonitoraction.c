/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testmonitoraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
typedef struct
{
    int             sampleCount;
    unsigned long*  samples;
    unsigned long   frequency;
    int             startSample;
} SCTMonitorAverageFPSOperationData;

/*!
 *
 *
 */
typedef struct
{
    int             sampleCount;
    unsigned long*  samples;
    double          min;
    double          max;
    unsigned long   frequency;
    int             startSample;
} SCTTestAverageFPSOperationData;

/*!
 *
 *
 */
typedef struct
{
    int             sampleCount;
    unsigned long*  samples;
    unsigned long   frequency;
    int             startSample;
    int             maxAverageCount;
    double*         averages;
    int             averageIndex;
} SCTLogAverageFPSOperationData;

/*!
 *
 *
 */
typedef struct
{
    unsigned long   last;
    unsigned long   frequency;
    int             maxMillisCount;
    double*         millis;
    int             millisIndex;
} SCTLogMillisOperationData;

/*!
 *
 *
 */
typedef struct
{
    unsigned long   samples[ 2 ];
    int             frames;
    unsigned long   frequency;
} SCTAverageFPSOperationData;

/*!
 *
 *
 */
typedef struct
{
    int             interval;
    void*           sample;
} SCTMonitorLoadOperationData;

/*!
 *
 *
 */
static int sctiCollectSample( unsigned long now, int fn, unsigned long* samples, int startSample, int sampleCount );
static double sctiSampleAverage( int fn, unsigned long* samples, int startSample, int sampleCount, unsigned long freq );

/*!
 *
 *
 */
void* sctiCreateTestMonitorActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestMonitorActionContext*    context;
    TestMonitorActionData*          data;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestMonitorActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestMonitorActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestMonitorActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestMonitorActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestMonitorActionContext( context );
        return NULL;
    }

    data = &( context->data );

    if( data->operation == SCT_MONITOR_AVERAGE_FPS )
    {
        SCTMonitorAverageFPSOperationData*   d;

        if( data->parameters->length != 1 )
        {
            SCT_LOG_ERROR( "Invalid parameters for SCT_MONITOR_AVERAGE_FPS operation in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
       
        context->operationData = ( void* )( siCommonMemoryAlloc( NULL, sizeof( SCTMonitorAverageFPSOperationData ) ) );
        if( context->operationData == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
        memset( context->operationData, 0, sizeof( SCTMonitorAverageFPSOperationData ) );

        d = ( SCTMonitorAverageFPSOperationData* )( context->operationData );

        d->frequency   = siCommonGetTimerFrequency( NULL );
        d->sampleCount = ( int )( data->parameters->data[ 0 ] );
        
        if( d->sampleCount < 1 )
        {
            SCT_LOG_ERROR( "Invalid SCT_MONITOR_AVERAGE_FPS operation Samples parameter in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }

        d->samples = ( unsigned long* )( siCommonMemoryAlloc( NULL, sizeof( unsigned long ) * ( d->sampleCount + 1 ) ) );
        if( d->samples == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }        
    }
    else if( data->operation == SCT_TEST_AVERAGE_FPS || data->operation == SCT_FORCE_AVERAGE_FPS )
    {
        SCTTestAverageFPSOperationData*  d;

        if( data->parameters->length != 3 )
        {
            SCT_LOG_ERROR( "Invalid parameters for SCT_TEST_AVERAGE_FPS/SCT_FORCE_AVERAGE_FPS operation in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
       
        context->operationData = ( void* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestAverageFPSOperationData ) ) );
        if( context->operationData == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
        memset( context->operationData, 0, sizeof( SCTTestAverageFPSOperationData ) );

        d = ( SCTTestAverageFPSOperationData* )( context->operationData );

        d->frequency   = siCommonGetTimerFrequency( NULL );
        d->sampleCount = ( int )( data->parameters->data[ 0 ] );
        d->min         = ( int )( data->parameters->data[ 1 ] );
        d->max         = ( int )( data->parameters->data[ 2 ] );
        
        if( d->sampleCount < 1 )
        {
            SCT_LOG_ERROR( "Invalid SCT_TEST_AVERAGE_FPS/SCT_FORCE_AVERAGE_FPS operation Samples parameter in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }

        if( d->min >= 0.0 && d->max >= 0.0 && d->min > d->max )
        {
            SCT_LOG_ERROR( "Invalid SCT_TEST_AVERAGE_FPS/SCT_FORCE_AVERAGE_FPS operation Min,Max parameter in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;
        }
        
        d->samples = ( unsigned long* )( siCommonMemoryAlloc( NULL, sizeof( unsigned long ) * ( d->sampleCount + 1 ) ) );
        if( d->samples == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }        
    }
    else if( data->operation == SCT_LOG_AVERAGE_FPS )
    {
        SCTLogAverageFPSOperationData*   d;

        if( data->parameters->length != 2 )
        {
            SCT_LOG_ERROR( "Invalid parameters for SCT_LOG_AVERAGE_FPS operation in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
       
        context->operationData = ( void* )( siCommonMemoryAlloc( NULL, sizeof( SCTLogAverageFPSOperationData ) ) );
        if( context->operationData == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
        memset( context->operationData, 0, sizeof( SCTLogAverageFPSOperationData ) );

        d = ( SCTLogAverageFPSOperationData* )( context->operationData );

        d->frequency       = siCommonGetTimerFrequency( NULL );
        d->sampleCount     = ( int )( data->parameters->data[ 0 ] );
        d->maxAverageCount = ( int )( data->parameters->data[ 1 ] );
        
        if( d->sampleCount < 1 )
        {
            SCT_LOG_ERROR( "Invalid SCT_LOG_AVERAGE_FPS operation Samples parameter in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }

        if( d->maxAverageCount < 1 )
        {
            SCT_LOG_ERROR( "Invalid SCT_LOG_AVERAGE_FPS operation MaxAverageCount parameter in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
        
        d->samples = ( unsigned long* )( siCommonMemoryAlloc( NULL, sizeof( unsigned long ) * ( d->sampleCount + 1 ) ) );
        if( d->samples == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }

        d->averages = ( double* )( siCommonMemoryAlloc( NULL, sizeof( double ) * d->maxAverageCount ) );
        if( d->averages == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }       
    }
    else if( data->operation == SCT_LOG_MILLIS )
    {
        SCTLogMillisOperationData*   d;

        if( data->parameters->length != 1 )
        {
            SCT_LOG_ERROR( "Invalid parameters for SCT_LOG_MILLIS operation in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
       
        context->operationData = ( void* )( siCommonMemoryAlloc( NULL, sizeof( SCTLogMillisOperationData ) ) );
        if( context->operationData == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
        memset( context->operationData, 0, sizeof( SCTLogMillisOperationData ) );

        d = ( SCTLogMillisOperationData* )( context->operationData );

        d->frequency       = siCommonGetTimerFrequency( NULL );
        d->maxMillisCount  = ( int )( data->parameters->data[ 0 ] );
        
        if( d->maxMillisCount < 1 )
        {
            SCT_LOG_ERROR( "Invalid SCT_LOG_MILLIS operation MaxSampleCount parameter in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
        
        d->millis = ( double* )( siCommonMemoryAlloc( NULL, sizeof( double ) * d->maxMillisCount ) );
        if( d->millis == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }       
    }   
    else if( data->operation == SCT_AVERAGE_FPS )
    {
        SCTAverageFPSOperationData*  d;

        if( data->parameters->length != 0 )
        {
            SCT_LOG_ERROR( "Invalid parameters for SCT_AVERAGE_FPS operation in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
       
        context->operationData = ( void* )( siCommonMemoryAlloc( NULL, sizeof( SCTAverageFPSOperationData ) ) );
        if( context->operationData == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
        memset( context->operationData, 0, sizeof( SCTAverageFPSOperationData ) );

        d = ( SCTAverageFPSOperationData* )( context->operationData );

        d->frequency = siCommonGetTimerFrequency( NULL );
    }
    else if( data->operation == SCT_MONITOR_LOAD )
    {
        SCTMonitorLoadOperationData*    d;

        if( data->parameters->length != 1 )
        {
            SCT_LOG_ERROR( "Invalid parameters for SCT_MONITOR_LOAD operation in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
       
        context->operationData = ( void* )( siCommonMemoryAlloc( NULL, sizeof( SCTMonitorLoadOperationData ) ) );
        if( context->operationData == NULL )
        {
            SCT_LOG_ERROR( "Allocation failed in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
        memset( context->operationData, 0, sizeof( SCTMonitorLoadOperationData ) );

        d = ( SCTMonitorLoadOperationData* )( context->operationData );

        d->interval = ( int )( data->parameters->data[ 0 ] );

        if( d->interval < 1 )
        {
            SCT_LOG_ERROR( "Invalid SCT_MONITOR_LOAD operation Interval parameter in Monitor@Test context creation." );
            sctiDestroyTestMonitorActionContext( context );
            return NULL;        
        }
     }
     
    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestMonitorActionContext( void* context )
{
    SCTTestMonitorActionContext*    c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTTestMonitorActionContext* )( context );
   
    if( c->data.parameters != NULL )
    {
        sctDestroyDoubleVector( c->data.parameters );
        c->data.parameters = NULL;
    }

    if( c->operationData != NULL )
    {
        if( c->data.operation == SCT_MONITOR_AVERAGE_FPS )
        {
            SCTMonitorAverageFPSOperationData*   d;

            d = ( SCTMonitorAverageFPSOperationData* )( c->operationData );

            if( d->samples != NULL )
            {
                siCommonMemoryFree( NULL, d->samples );
                d->samples = NULL;
            }
        }
        else if( c->data.operation == SCT_TEST_AVERAGE_FPS || c->data.operation == SCT_FORCE_AVERAGE_FPS )
        {
            SCTTestAverageFPSOperationData*  d;

            d = ( SCTTestAverageFPSOperationData* )( c->operationData );
            
            if( d->samples != NULL )
            {
                siCommonMemoryFree( NULL, d->samples );
                d->samples = NULL;
            }
        }
        else if( c->data.operation == SCT_LOG_AVERAGE_FPS )
        {
            SCTLogAverageFPSOperationData*   d;

            d = ( SCTLogAverageFPSOperationData* )( c->operationData );

            if( d->samples != NULL )
            {
                siCommonMemoryFree( NULL, d->samples );
                d->samples = NULL;
            }

            if( d->averages != NULL )
            {
                siCommonMemoryFree( NULL, d->averages );
                d->averages = NULL;
            }
        }
        else if( c->data.operation == SCT_LOG_MILLIS )
        {
            SCTLogMillisOperationData*   d;

            d = ( SCTLogMillisOperationData* )( c->operationData );

            if( d->millis != NULL )
            {
                siCommonMemoryFree( NULL, d->millis );
                d->millis = NULL;
            }
        }
        else if( c->data.operation == SCT_MONITOR_LOAD )
        {
            SCTMonitorLoadOperationData*    d;

            d = ( SCTMonitorLoadOperationData* )( c->operationData );

            if( d->sample != NULL )
            {
                siCommonDestroyCpuLoadSample( NULL, d->sample );
                d->sample = NULL;
            }
        }
 
        siCommonMemoryFree( NULL, c->operationData );
        c->operationData = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestMonitorActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTTestMonitorActionContext*    c;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    c   = ( SCTTestMonitorActionContext* )( action->context );

    if( c->operationData != NULL )
    {
        if( c->data.operation == SCT_MONITOR_AVERAGE_FPS )
        {
            SCTMonitorAverageFPSOperationData*   d;
            d = ( SCTMonitorAverageFPSOperationData* )( c->operationData );
            d->startSample = 0;            
        }
        else if( c->data.operation == SCT_TEST_AVERAGE_FPS || c->data.operation == SCT_FORCE_AVERAGE_FPS )
        {
            SCTTestAverageFPSOperationData*  d;
            d = ( SCTTestAverageFPSOperationData* )( c->operationData );
            d->startSample = 0;
        }
        else if( c->data.operation == SCT_LOG_AVERAGE_FPS )
        {
            SCTLogAverageFPSOperationData*   d;
            d = ( SCTLogAverageFPSOperationData* )( c->operationData );
            d->startSample  = 0;
            d->averageIndex = 0;            
        }
        else if( c->data.operation == SCT_LOG_MILLIS )
        {
            SCTLogMillisOperationData*   d;
            d = ( SCTLogMillisOperationData* )( c->operationData );
            d->millisIndex = 0;
        }
        else if( c->data.operation == SCT_MONITOR_LOAD )
        {
            SCTMonitorLoadOperationData*    d;
            d = ( SCTMonitorLoadOperationData* )( c->operationData );

            if( d->sample != NULL )
            {
                siCommonDestroyCpuLoadSample( NULL, d->sample );
                d->sample = NULL;
            }
        }
     }

    c->reported = SCT_FALSE;
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiTestMonitorActionExecute( SCTAction* action, int framenumber )
{
    SCTTestMonitorActionContext*    context;
    TestMonitorActionData*          data;
    unsigned long                   now;    
    char                            buf[ 256 ];
    int                             s;
    double                          a;
    float                           load;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestMonitorActionContext* )( action->context );
    data    = &( context->data );

    if( framenumber < 0 )
    {
        /* Monitor cannot be used in init or terminate lists. */
        SCT_LOG_ERROR( "Monitor@Test action used outside benchmark action list" );
        return SCT_FALSE;
    }

    if( data->operation != SCT_MONITOR_LOAD )
    {
        now = siCommonGetTimerTick( NULL );
    }

    if( data->operation == SCT_AVERAGE_FPS )
    {
        SCTAverageFPSOperationData*  d;
        d = ( SCTAverageFPSOperationData* )( context->operationData );

        if( framenumber == 0 )
        {
            d->samples[ 0 ] = now;
        }
        else
        {
            d->samples[ 1 ] = now;
        }
        d->frames = framenumber;
    }
    else if( data->operation == SCT_MONITOR_AVERAGE_FPS )
    {
        SCTMonitorAverageFPSOperationData*   d;
        d = ( SCTMonitorAverageFPSOperationData* )( context->operationData );

        s = sctiCollectSample( now, framenumber, d->samples, d->startSample, d->sampleCount );
        if( s != d->startSample )
        {
            a = sctiSampleAverage( framenumber, d->samples, d->startSample, d->sampleCount, d->frequency );
            d->startSample = s;

            if( a <= 0.0 )
            {
                sprintf( buf, "SPANDEX: monitor \"%s\" average FPS inf.", data->message );
            }
            else
            {
                sprintf( buf, "SPANDEX: monitor \"%s\" average FPS %.1f", data->message, a );
            }
            
            siCommonDebugPrintf( NULL, buf ); 
        }
    }
    else if( data->operation == SCT_TEST_AVERAGE_FPS || data->operation == SCT_FORCE_AVERAGE_FPS )
    {
        SCTTestAverageFPSOperationData*  d;
        d = ( SCTTestAverageFPSOperationData* )( context->operationData );

        s = sctiCollectSample( now, framenumber, d->samples, d->startSample, d->sampleCount );
        if( s != d->startSample )
        {
            a = sctiSampleAverage( framenumber, d->samples, d->startSample, d->sampleCount, d->frequency );
            d->startSample = s;
            
            if( ( a > 0.0 ) &&
                ( ( d->min > 0.0 && a <= d->min ) ||
                  ( d->max > 0.0 && a >= d->max ) ) )
            {
                sprintf( buf, "SPANDEX: monitor \"%s\" average FPS %.1f out of bounds", data->message, a );
                siCommonDebugPrintf( NULL, buf ); 

                if( data->operation == SCT_FORCE_AVERAGE_FPS )
                {
                    sprintf( buf, "Monitor \"%s\" average FPS %.1f out of bounds in Monitor@Test action execute", data->message, a );
                    SCT_LOG_ERROR( buf );
                    return SCT_FALSE;
                }
            }
        }
    }
    else if( data->operation == SCT_LOG_AVERAGE_FPS )
    {
        SCTLogAverageFPSOperationData*   d;
        d = ( SCTLogAverageFPSOperationData* )( context->operationData );

        s = sctiCollectSample( now, framenumber, d->samples, d->startSample, d->sampleCount );
        if( s != d->startSample )
        {
            a = sctiSampleAverage( framenumber, d->samples, d->startSample, d->sampleCount, d->frequency );
            d->startSample = s;
            
            if( d->averageIndex < d->maxAverageCount )
            {
                d->averages[ d->averageIndex ] = a;
            }
            d->averageIndex++;
        }
    }
    else if( data->operation == SCT_LOG_MILLIS )
    {
        SCTLogMillisOperationData*   d;
        d = ( SCTLogMillisOperationData* )( context->operationData );

        a = ( now - d->last ) / ( double )( d->frequency ) * 1000.0;
        if( d->millisIndex < d->maxMillisCount )
        {
            if( d->last == 0 )
            {
                d->millis[ d->millisIndex ] = -1.0;
            }
            else
            {               
                d->millis[ d->millisIndex ] = a;
            }
        }
        d->last = now;
        d->millisIndex++;
    }
    else if( data->operation == SCT_MONITOR_LOAD )
    {
        SCTMonitorLoadOperationData* d;
        d = ( SCTMonitorLoadOperationData* )( context->operationData );

        if( d->sample == NULL )
        {
            d->sample = siCommonSampleCpuLoad( NULL );

            if( d->sample == NULL )
            {
                SCT_LOG_ERROR( "siCommonSampleCpuLoad failed in Monitor@Test action execute." );
                return SCT_FALSE;
            }
        }

        if( framenumber > 0 && ( ( framenumber + 1 ) % d->interval ) == 0 )
        {
            load = siCommonCalculateCpuLoad( NULL, d->sample, SCT_CPULOAD_SYSTEM );

            if( load < 0.0f )
            {
                SCT_LOG_ERROR( "siCommonCalculateCpuLoad failed in Monitor@Test action execute." );
                return SCT_FALSE;
            }

            sprintf( buf, "SPANDEX: monitor \"%s\" load %.1f", data->message, load * 100.0f );
            siCommonDebugPrintf( NULL, buf ); 

            siCommonDestroyCpuLoadSample( NULL, d->sample );
            d->sample = siCommonSampleCpuLoad( NULL );
        }

    }
    else
    {
        SCT_ASSERT_ALWAYS( 0 );
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestMonitorActionTerminate( SCTAction* action )
{
    SCTTestMonitorActionContext*    context;
    TestMonitorActionData*          data;
   
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTTestMonitorActionContext* )( action->context );
    data    = &( context->data );

    if( context->reported != SCT_FALSE )
    {
        return;
    }  
    
    /* NOTE: Just ignore allocation/output write errors below. */
    
    if( context->operationData != NULL )
    {
        if( data->operation == SCT_AVERAGE_FPS )
        {
            SCTAverageFPSOperationData* d;
            char                        buf[ 128 ];
            double                      average             = 0;
            double                      seconds;
            SCTAttributeList*           attributes;
            
            d = ( SCTAverageFPSOperationData* )( context->operationData );
            
            SCT_ASSERT_ALWAYS( d->frequency != 0 );
            seconds = ( d->samples[ 1 ] - d->samples[ 0 ] ) / ( double )( d->frequency );
            if( seconds != 0 )
            {
                average = d->frames / seconds;
            }
            
            attributes = sctCreateAttributeList();
            if( attributes == NULL )
            {
                return;
            }

            sprintf( buf, "%.1f", average );
            sctAddNameValueToList( attributes, "FPS", buf );
            
            sctReportSection( data->message, "AVERAGE_FPS_MONITOR", attributes );
            sctDestroyAttributeList( attributes );
        }
        else if( data->operation == SCT_LOG_AVERAGE_FPS )
        {
            SCTLogAverageFPSOperationData*  d;
            char                            buf1[ 16 ];
            char*                           buf2;
            int                             min;
            int                             i;
            SCTAttributeList*               attributes;
            
            d = ( SCTLogAverageFPSOperationData* )( context->operationData );

            attributes = sctCreateAttributeList();
            if( attributes == NULL )
            {
                return;
            }
            
            min = sctMin( d->averageIndex, d->maxAverageCount );

            if( min >= 1 )
            {
                buf2 = ( char* )( siCommonMemoryAlloc( NULL, min * sizeof( buf1 ) ) );
                if( buf2 != NULL )
                {
                    buf2[ 0 ] = '\0';
                    
                    for( i = 0; i < min; ++i )
                    {
                        if( d->averages[ i ] <= 0.0 || d->averages[ i ] > 100000.0 )
                        {
                            sprintf( buf1, "n/a, " );
                        }
                        else
                        {
                            sprintf( buf1, "%.1f, ", d->averages[ i ] );
                        }
                        
                        strcat( buf2, buf1 );
                    }

                    sctAddNameValueToList( attributes, "FPS averages", buf2 );
                    siCommonMemoryFree( NULL, buf2 );
                }
            }
           
            /* Print warning in case of potential average buffer overflow */
            if( d->averageIndex >= d->maxAverageCount )
            {
                sctAddNameValueToList( attributes, "Warning", "Sample buffer overrrun" );       
            }                
            
            sctReportSection( data->message, "LOG_AVERAGE_FPS_MONITOR", attributes );                
            sctDestroyAttributeList( attributes );
        }
        else if( data->operation == SCT_LOG_MILLIS )
        {
            SCTLogMillisOperationData*  d;
            char                        buf1[ 16 ];
            char*                       buf2;
            int                         i;
            int                         min;
            SCTAttributeList*           attributes;
            
            d = ( SCTLogMillisOperationData* )( context->operationData );

            attributes = sctCreateAttributeList();
            if( attributes == NULL )
            {
                return;
            }

            min = sctMin( d->millisIndex, d->maxMillisCount );
            if( min >= 1 )
            {
                buf2 = ( char* )( siCommonMemoryAlloc( NULL, min * sizeof( buf1 ) ) );
                if( buf2 != NULL )
                {
                    buf2[ 0 ] = '\0';
                    
                    for( i = 0; i < min; ++i )
                    {
                        if( d->millis[ i ] <= 0.0 || d->millis[ i ] > 100000.0 )
                        {
                            sprintf( buf1, "n/a, " );
                        }
                        else
                        {
                            sprintf( buf1, "%.1f ms, ", d->millis[ i ] );
                        }
                        strcat( buf2, buf1 );
                    }

                    sctAddNameValueToList( attributes, "Millis", buf2 );
                    siCommonMemoryFree( NULL, buf2 );
                }
                
                /* Print warning in case of potential millis buffer overflow */
                if( d->millisIndex >= d->maxMillisCount )
                {
                    sctAddNameValueToList( attributes, "Warning", "Sample buffer overrrun" );
                }                
            }
            
            sctReportSection( data->message, "LOG_MILLIS_MONITOR", attributes );                
            sctDestroyAttributeList( attributes );
        }
    }

    context->reported = SCT_TRUE;    
}

/*!
 *
 *
 */
void sctiTestMonitorActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestMonitorActionContext( ( SCTTestMonitorActionContext* )( action->context ) );
    action->context = NULL;
}

/*!
 *
 *
 */
static int sctiCollectSample( unsigned long now, int fn, unsigned long* samples, int startSample, int sampleCount )
{
    SCT_ASSERT( now > 0 );
    SCT_ASSERT( fn >= 0 );
    SCT_ASSERT( samples != NULL );
    SCT_ASSERT( startSample >= 0 );
    SCT_ASSERT( sampleCount > 0 );
    
    /* Using a circular buffer */
    samples[ fn % ( sampleCount + 1 ) ] = now;
        
    if( ( fn - startSample ) < sampleCount )
    {
        /* Need to collect more samples. */
        return startSample;
    }
    else
    {
        return ++startSample;
    }
}

/*!
 *
 *
 */
static double sctiSampleAverage( int fn, unsigned long* samples, int startSample, int sampleCount, unsigned long freq )
{
    int    i;
    double a;

    SCT_ASSERT( fn >= 0 );
    SCT_ASSERT( samples != NULL );
    SCT_ASSERT( startSample >= 0 );
    SCT_ASSERT( sampleCount > 0 );
    SCT_ASSERT( freq > 0 );
    
    a = 0;

    for( i = startSample; i < fn; ++i )
    {
        a += ( ( samples[ ( i + 1 ) % ( sampleCount + 1 ) ] -
                 samples[ i % ( sampleCount + 1 ) ] ) /
               ( double )( freq ) );
    }
    
    a /= ( sampleCount );
    if( a != 0.0 )
    {
        return 1.0 / a;
    }
    else
    {
        return -1.0;
    }
}
