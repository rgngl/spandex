/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testprintmemorycounteraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestPrintMemoryCounterActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestPrintMemoryCounterActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestPrintMemoryCounterActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestPrintMemoryCounterActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in PrintMemoryCounter@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestPrintMemoryCounterActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestPrintMemoryCounterActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestPrintMemoryCounterActionContext( context );
        return NULL;
    }

    if( sctiTestModuleIsValidMemoryCounterIndex( context->moduleContext, context->data.counterIndex ) == SCT_FALSE )
    {
        sctiDestroyTestPrintMemoryCounterActionContext( context );        
        SCT_LOG_ERROR( "Invalid counter index in PrintMemoryCounter@Test context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestPrintMemoryCounterActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestPrintMemoryCounterActionExecute( SCTAction* action, int framenumber )
{
    SCTTestPrintMemoryCounterActionContext* context;
    TestPrintMemoryCounterActionData*       data;
    char                                    buf1[ 32 ];
    char                                    buf2[ 256 ];

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestPrintMemoryCounterActionContext* )( action->context );
    data    = &( context->data );

    sctFormatSize_t( buf1, sizeof( buf1 ), sctiTestModuleGetMemoryCounter( context->moduleContext, data->counterIndex ) );
    sprintf( buf2, "SPANDEX: memory counter \"%s\" %s", data->message, buf1 );
    siCommonDebugPrintf( NULL, buf2 );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestPrintMemoryCounterActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestPrintMemoryCounterActionContext( ( SCTTestPrintMemoryCounterActionContext* )( action->context ) );
    action->context = NULL;
}

