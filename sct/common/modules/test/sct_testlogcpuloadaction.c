/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testlogcpuloadaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestLogCpuLoadActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestLogCpuLoadActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestLogCpuLoadActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestLogCpuLoadActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LogCpuLoad@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestLogCpuLoadActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestLogCpuLoadActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestLogCpuLoadActionContext( context );
        return NULL;
    }

    context->bufferLength = context->data.maxSampleCount;
    context->buffer = ( float* )( siCommonMemoryAlloc( NULL, context->bufferLength * sizeof( float ) ) );
    
    if( context->buffer == NULL )
    {
        sctiDestroyTestLogCpuLoadActionContext( context );        
        SCT_LOG_ERROR( "Allocation failed in LogCpuLoad@Test context creation." );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestLogCpuLoadActionContext( void* context )
{
    SCTTestLogCpuLoadActionContext* c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTTestLogCpuLoadActionContext* )( context );
   
    if( c->buffer != NULL )
    {
        siCommonMemoryFree( NULL, c->buffer );
        c->buffer = NULL;
    }

    if( c->startSample != NULL )
    {
        siCommonDestroyCpuLoadSample( NULL, c->startSample );
        c->startSample = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestLogCpuLoadActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTTestLogCpuLoadActionContext* c;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    c = ( SCTTestLogCpuLoadActionContext* )( action->context );
    c->bufferIndex = 0;
    c->reported    = SCT_FALSE;

    if( c->startSample != NULL )
    {
        siCommonDestroyCpuLoadSample( NULL, c->startSample );
        c->startSample = NULL;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiTestLogCpuLoadActionExecute( SCTAction* action, int framenumber )
{
    SCTTestLogCpuLoadActionContext* context;
    TestLogCpuLoadActionData*       data;
    float                           load;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestLogCpuLoadActionContext* )( action->context );
    data    = &( context->data );

    if( context->startSample == NULL )
    {
        context->startSample = siCommonSampleCpuLoad( NULL );

        if( context->startSample == NULL )
        {
            SCT_LOG_ERROR( "siCommonSampleCpuLoad failed in LogCpuLoad@Test action execute." );        
            return SCT_FALSE;
        }
    }

    if( framenumber > 0 && ( ( framenumber + 1 ) % data->interval ) == 0 )
    {
        if( context->bufferIndex < context->bufferLength )
        {
            load = siCommonCalculateCpuLoad( NULL, context->startSample, data->type );
            
            if( load < 0.0f )
            {
                SCT_LOG_ERROR( "siCommonCalculateCpuLoad failed in LogCpuLoad@Test action execute." );        
                return SCT_FALSE;
            }

            context->buffer[ context->bufferIndex ] = load;
        }

        context->bufferIndex++;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestLogCpuLoadActionTerminate( SCTAction* action )
{
    SCTTestLogCpuLoadActionContext* context;
    TestLogCpuLoadActionData*       data;
    char                            buf1[ 32 ];
    char*                           buf2;
    int                             min;
    int                             i;
    SCTAttributeList*               attributes;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTTestLogCpuLoadActionContext* )( action->context );
    data    = &( context->data );

    if( context->reported != SCT_FALSE )
    {
        return;
    }
    
    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        return;
    }
    
    buf2 = ( char* )( siCommonMemoryAlloc( NULL, context->bufferIndex * sizeof( buf1 ) ) );
    if( buf2 != NULL )
    {
        buf2[ 0 ] = '\0';
        
        min = sctMin( context->bufferIndex, data->maxSampleCount );
        for( i = 0; i < min; ++i )
        {
            sprintf( buf1, "%.1f, ", context->buffer[ i ] * 100.0f );
            strcat( buf2, buf1 );                   
        }
        
        sctAddNameValueToList( attributes, "Percentages", buf2 );
        siCommonMemoryFree( NULL, buf2 );
    }
    
    if( context->bufferIndex > data->maxSampleCount )
    {
        sctAddNameValueToList( attributes, "Warning", "Sample buffer overrrun" );
    }                
        
    sctReportSection( data->message, "LOG_CPU_LOAD", attributes );                
    sctDestroyAttributeList( attributes );

    context->reported = SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestLogCpuLoadActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestLogCpuLoadActionContext( ( SCTTestLogCpuLoadActionContext* )( action->context ) );
    action->context = NULL;
}
