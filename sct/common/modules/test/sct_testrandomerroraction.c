/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testrandomerroraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
static SCTBoolean sctiRandomError( double frequency, int* mtIndex, void* mtContext );

/*!
 *
 *
 */
void* sctiCreateTestRandomErrorActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestRandomErrorActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestRandomErrorActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestRandomErrorActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in RandomError@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestRandomErrorActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestRandomErrorActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestRandomErrorActionContext( context );
        return NULL;
    }

    context->mtContext = sctCreateMerseneTwisterContext();
    if( context->mtContext == NULL )
    {
        sctiDestroyTestRandomErrorActionContext( context );        
        SCT_LOG_ERROR( "Allocation failed in RandomError@Test context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestRandomErrorActionContext( void* context )
{
    SCTTestRandomErrorActionContext*    c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTTestRandomErrorActionContext* )( context );

    if( c->mtContext != NULL )
    {
        sctDestroyMerseneTwisterContext( c->mtContext );
        c->mtContext = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestRandomErrorActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTTestRandomErrorActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTTestRandomErrorActionContext* )( action->context );

    if( sctiRandomError( context->data.frequency, &( context->mtIndex ), context->mtContext ) == SCT_TRUE )
    {
        char buffer[ 256 ];
        
        sprintf( buffer, "Error %s in RandomError@Test action init.", context->data.message ); 
        SCT_LOG_ERROR( buffer );
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 *
 */
SCTBoolean sctiTestRandomErrorActionExecute( SCTAction* action, int framenumber )
{
    SCTTestRandomErrorActionContext*  context;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestRandomErrorActionContext* )( action->context );

    if( sctiRandomError( context->data.frequency, &( context->mtIndex ), context->mtContext ) == SCT_TRUE )
    {
        char buffer[ 256 ];
        
        sprintf( buffer, "Error %s in RandomError@Test action execute.", context->data.message ); 
        SCT_LOG_ERROR( buffer );
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 *
 *
 */
void sctiTestRandomErrorActionTerminate( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
}

/*!
 *
 *
 */
void sctiTestRandomErrorActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestRandomErrorActionContext( ( SCTTestRandomErrorActionContext* )( action->context ) );
    action->context = NULL;
}

/* ********************************************************************** */
/*!
 *
 *
 */
static SCTBoolean sctiRandomError( double frequency, int* mtIndex, void* mtContext )
{    
    if( frequency > sctMerseneTwister( mtIndex, mtContext ) )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}
