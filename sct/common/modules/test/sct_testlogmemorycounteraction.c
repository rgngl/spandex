/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testlogmemorycounteraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestLogMemoryCounterActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestLogMemoryCounterActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestLogMemoryCounterActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestLogMemoryCounterActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LogMemoryCounter@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestLogMemoryCounterActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestLogMemoryCounterActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestLogMemoryCounterActionContext( context );
        return NULL;
    }

    if( sctiTestModuleIsValidMemoryCounterIndex( context->moduleContext, context->data.counterIndex ) == SCT_FALSE )
    {
        sctiDestroyTestLogMemoryCounterActionContext( context );        
        SCT_LOG_ERROR( "Invalid counter index in LogMemoryCounter@Test context creation." );
        return NULL;
    }

    context->bufferLength = context->data.maxSampleCount;
    context->buffer = ( size_t* )( siCommonMemoryAlloc( NULL, context->bufferLength * sizeof( size_t ) ) );
    
    if( context->buffer == NULL )
    {
        sctiDestroyTestLogMemoryCounterActionContext( context );        
        SCT_LOG_ERROR( "Allocation failed in LogMemoryCounter@Test context creation." );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestLogMemoryCounterActionContext( void* context )
{
    SCTTestLogMemoryCounterActionContext*   c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTTestLogMemoryCounterActionContext* )( context );
   
    if( c->buffer != NULL )
    {
        siCommonMemoryFree( NULL, c->buffer );
        c->buffer = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestLogMemoryCounterActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTTestLogMemoryCounterActionContext*    c;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    c = ( SCTTestLogMemoryCounterActionContext* )( action->context );
    c->bufferIndex = 0;
    c->reported    = SCT_FALSE;

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiTestLogMemoryCounterActionExecute( SCTAction* action, int framenumber )
{
    SCTTestLogMemoryCounterActionContext*    context;
    TestLogMemoryCounterActionData*          data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestLogMemoryCounterActionContext* )( action->context );
    data    = &( context->data );

    if( context->bufferIndex < context->bufferLength )
    {
        context->buffer[ context->bufferIndex ] = sctiTestModuleGetMemoryCounter( context->moduleContext, data->counterIndex );
    }

    context->bufferIndex++;
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestLogMemoryCounterActionTerminate( SCTAction* action )
{
    SCTTestLogMemoryCounterActionContext*   context;
    TestLogMemoryCounterActionData*         data;
    char                                    buf1[ 30 ];
    char                                    buf2[ 32 ];
    char*                                   buf3;
    int                                     min;
    int                                     i;
    SCTAttributeList*                       attributes;
    size_t                                  minSample;
    size_t                                  maxSample;    
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTTestLogMemoryCounterActionContext* )( action->context );
    data    = &( context->data );

    if( context->reported != SCT_FALSE )
    {
        return;
    }  
    
    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        return;
    }
    
    buf3 = ( char* )( siCommonMemoryAlloc( NULL, context->bufferIndex * sizeof( buf1 ) ) );
    if( buf3 != NULL )
    {
        buf3[ 0 ] = '\0';
        min = sctMin( context->bufferIndex, data->maxSampleCount );

        minSample = context->buffer[ 0 ];
        maxSample = context->buffer[ 0 ];
        
        for( i = 0; i < min; ++i )
        {
            strcpy( buf1, "-" );
            if( context->buffer[ i ] < minSample )
            {
                minSample = context->buffer[ i ];                
            }
            if( context->buffer[ i ] > maxSample )
            {
                maxSample = context->buffer[ i ];                
            }
            sctFormatSize_t( buf1, sizeof( buf1 ), context->buffer[ i ] );
            sprintf( buf2, "%s, ", buf1 );
            strcat( buf3, buf2 );                   
        }
        
        sctAddNameValueToList( attributes, "Values", buf3 );
        siCommonMemoryFree( NULL, buf3 );

        sctFormatSize_t( buf1, sizeof( buf1 ), minSample );
        sctAddNameValueToList( attributes, "Min", buf1 );

        sctFormatSize_t( buf1, sizeof( buf1 ), maxSample );
        sctAddNameValueToList( attributes, "Max", buf1 );
    }
    
    if( context->bufferIndex > data->maxSampleCount )
    {
        sctAddNameValueToList( attributes, "Warning", "Sample buffer overrrun" );
    }                
        
    sctReportSection( data->message, "LOG_MEMORY_COUNTER", attributes );                
    sctDestroyAttributeList( attributes );

    context->reported = SCT_TRUE;    
}

/*!
 *
 *
 */
void sctiTestLogMemoryCounterActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestLogMemoryCounterActionContext( ( SCTTestLogMemoryCounterActionContext* )( action->context ) );
    action->context = NULL;
}
