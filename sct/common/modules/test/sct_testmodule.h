/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_TESTMODULE_H__ )
#define __SCT_TESTMODULE_H__

#include "sct_types.h"

#include <stddef.h>

#define SCT_MAX_TEST_TIMERS             256
#define SCT_MAX_TEST_MEMORY_COUNTERS    256

/*!
 *
 */
typedef enum
{
    SCT_MONITOR_AVERAGE_FPS,
    SCT_TEST_AVERAGE_FPS,
    SCT_FORCE_AVERAGE_FPS,
    SCT_LOG_AVERAGE_FPS,
    SCT_LOG_MILLIS,    
    SCT_AVERAGE_FPS,
    SCT_MONITOR_LOAD,
 } SCTMonitorOperation;

/*!
 *
 */
typedef enum
{
    SCT_TOTAL_SYSTEM_MEMORY,
    SCT_FREE_SYSTEM_MEMORY,
    SCT_USED_SYSTEM_MEMORY,
    SCT_TOTAL_GRAPHICS_MEMORY,
    SCT_FREE_GRAPHICS_MEMORY,    
    SCT_USED_GRAPHICS_MEMORY,
    SCT_USED_PRIVATE_GRAPHICS_MEMORY,
    SCT_USED_SHARED_GRAPHICS_MEMORY,
} SCTMemoryType;

/*!
 *
 *
 */
typedef struct
{
    unsigned long               timers[ SCT_MAX_TEST_TIMERS ];
    size_t                      memoryCounters[ SCT_MAX_TEST_MEMORY_COUNTERS ];
} SCTTestModuleContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    SCTModule*                  sctCreateTestModule( void );

    /* Timers. */
    SCTBoolean                  sctiTestModuleIsValidTimerIndex( SCTTestModuleContext* moduleContext, int index );
    unsigned long               sctiTestModuleGetTimer( SCTTestModuleContext* moduleContext, int index );
    void                        sctiTestModuleSetTimer( SCTTestModuleContext* moduleContext, int index, unsigned long v );

    /* Memory counters. */
    SCTBoolean                  sctiTestModuleIsValidMemoryCounterIndex( SCTTestModuleContext* moduleContext, int index );
    size_t                      sctiTestModuleGetMemoryCounter( SCTTestModuleContext* moduleContext, int index );
    void                        sctiTestModuleSetMemoryCounter( SCTTestModuleContext* moduleContext, int index, size_t v );

    /* Busy action. */
    void                        sctiTestModuleBusy( SCTTestModuleContext* moduleContext, int millis );
    unsigned long               sctiTestModuleGetBusyIterations( SCTTestModuleContext* moduleContext, int millis );
    void                        sctiTestModuleDoBusyIterations( SCTTestModuleContext* moduleContext, unsigned long iterations );
    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_TESTMODULE_H__ ) */
