/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_TESTLOGMESSAGEACTION_H__ )
#define __SCT_TESTLOGMESSAGEACTION_H__

#include "sct_types.h"
#include "sct_testmodule.h"
#include "sct_testmodule_parser.h"

/*!
 *
 */
typedef struct
{
    SCTTestModuleContext*               moduleContext;
    TestLogMessageActionData            data;
    double*                             buffer;
    int                                 bufferLength;
    int                                 bufferIndex;
    SCTBoolean                          reported;
} SCTTestLogMessageActionContext;
 
#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*								sctiCreateTestLogMessageActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroyTestLogMessageActionContext( void* context );

    SCTBoolean                          sctiTestLogMessageActionInit( SCTAction* action, SCTBenchmark* benchmark );    
    SCTBoolean                          sctiTestLogMessageActionExecute( SCTAction* action, int framenumber );
    void                                sctiTestLogMessageActionTerminate( SCTAction* action );    
    void                                sctiTestLogMessageActionDestroy( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_TESTLOGMESSAGEACTION_H__ ) */
