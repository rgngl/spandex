/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testerroraction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestErrorActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestErrorActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestErrorActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestErrorActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Error@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestErrorActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestErrorActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestErrorActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestErrorActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestErrorActionExecute( SCTAction* action, int framenumber )
{
    SCTTestErrorActionContext*  context;
    TestErrorActionData*        data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestErrorActionContext* )( action->context );
    data    = &( context->data );

    if( ( data->frameNumber < 0 ) || ( data->frameNumber == framenumber ) )
    {
        char buffer[ 256 ];
        
        sprintf( buffer, "Error %s in Error@Test action execute.", data->message ); 
        SCT_LOG_ERROR( buffer );
        
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestErrorActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestErrorActionContext( ( SCTTestErrorActionContext* )( action->context ) );
    action->context = NULL;
}

