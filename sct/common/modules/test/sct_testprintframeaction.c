/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testprintframeaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestPrintFrameActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestPrintFrameActionContext* context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestPrintFrameActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestPrintFrameActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Print@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestPrintFrameActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestPrintFrameActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestPrintFrameActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestPrintFrameActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestPrintFrameActionExecute( SCTAction* action, int framenumber )
{
    SCTTestPrintFrameActionContext* context;
    TestPrintFrameActionData*       data;
    char                            buf[ 256 ];

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );

    context = ( SCTTestPrintFrameActionContext* )( action->context );
    data    = &( context->data );

    sprintf( buf, "SPANDEX: frame %d \"%s\"", framenumber, data->message );
    siCommonDebugPrintf( NULL, buf );    

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestPrintFrameActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestPrintFrameActionContext( ( SCTTestPrintFrameActionContext* )( action->context ) );
    action->context = NULL;
}

