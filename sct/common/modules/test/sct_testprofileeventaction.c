/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Kari J. Kangas.
 *
 * Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testprofileeventaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestProfileEventActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestProfileEventActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestProfileEventActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestProfileEventActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in ProfileEvent@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestProfileEventActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestProfileEventActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestProfileEventActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestProfileEventActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestProfileEventActionExecute( SCTAction* action, int framenumber )
{
    SCTTestProfileEventActionContext*   context;
    TestProfileEventActionData*         data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestProfileEventActionContext* )( action->context );
    data    = &( context->data );

    siCommonProfileEvent( NULL, data->event );    

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestProfileEventActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestProfileEventActionContext( ( SCTTestProfileEventActionContext* )( action->context ) );
    action->context = NULL;
}

