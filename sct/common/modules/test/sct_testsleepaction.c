/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testsleepaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestSleepActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestSleepActionContext*  context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestSleepActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestSleepActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Sleep@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestSleepActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestSleepActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestSleepActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestSleepActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestSleepActionExecute( SCTAction* action, int framenumber )
{
    SCTTestSleepActionContext*  context;
    TestSleepActionData*        data;

    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestSleepActionContext* )( action->context );
    data    = &( context->data );

    siCommonSleep( NULL, ( unsigned int )( data->millis * 1000 ) );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestSleepActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestSleepActionContext( ( SCTTestSleepActionContext* )( action->context ) );
    action->context = NULL;
}

