/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testmodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"
#include "sct_testmodule_actions.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
SCTAttributeList*   sctiTestModuleInfo( SCTModule* module );
SCTAction*          sctiTestModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                sctiTestModuleDestroy( SCTModule* module );

/*!
 *
 *
 */
static void         _invert4x4Matrix( const float m[ 16 ], float out[ 16 ] );
static float        _busyIteration( float d );
static float        _busyIterations( unsigned long iterations );
    
/*!
 *
 *
 */
SCTModule* sctCreateTestModule( void )
{
    SCTModule*              module;
    SCTTestModuleContext*   context;

    context = ( SCTTestModuleContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestModuleContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestModuleContext ) );

    /* Context timers are all set to 0 above. */
    
    module = sctCreateModule( "Test",
                              context,
#if defined( _WIN32 )
                              _test_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiTestModuleInfo,
                              sctiTestModuleCreateAction,
                              sctiTestModuleDestroy );

    if( module == NULL )
    {
        siCommonMemoryFree( NULL, context );
        return NULL;
    }

    return module;
}

/*!
 *
 *
 */
SCTAttributeList* sctiTestModuleInfo( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );

    return sctCreateAttributeList();
}

/*!
 *
 *
 */
SCTAction* sctiTestModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCTTestModuleContext*   moduleContext;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    moduleContext = ( SCTTestModuleContext* )( module->context );

    SCT_ASSERT_ALWAYS( moduleContext != NULL );

    return sctCreateActionFromTemplate( name, type, "Test", moduleContext, attributes, TestActionTemplates, SCT_ARRAY_LENGTH( TestActionTemplates ) );
}

/*!
 *
 *
 */
void sctiTestModuleDestroy( SCTModule* module )
{
    SCTTestModuleContext*   context;

    SCT_ASSERT_ALWAYS( module != NULL );

    context = ( SCTTestModuleContext* )( module->context );

    if( context == NULL )
    {
        return;
    }
    
    siCommonMemoryFree( NULL, context );
    module->context = NULL;
}

/*!
 *
 *
 */
SCTBoolean sctiTestModuleIsValidTimerIndex( SCTTestModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index >= 0 && index < SCT_MAX_TEST_TIMERS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
unsigned long sctiTestModuleGetTimer( SCTTestModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_TEST_TIMERS );

    if( index >= 0 && index < SCT_MAX_TEST_TIMERS )
    {
        return moduleContext->timers[ index ];
    }

    return 0;
}

/*!
 *
 *
 */
void sctiTestModuleSetTimer( SCTTestModuleContext* moduleContext, int index, unsigned long v )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_TEST_TIMERS );
    
    if( index >= 0 && index < SCT_MAX_TEST_TIMERS )
    {
        moduleContext->timers[ index ] = v;
    }
}

/*!
 *
 *
 */
SCTBoolean sctiTestModuleIsValidMemoryCounterIndex( SCTTestModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_USE_VARIABLE( moduleContext );

    if( index >= 0 && index < SCT_MAX_TEST_MEMORY_COUNTERS )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 *
 *
 */
size_t sctiTestModuleGetMemoryCounter( SCTTestModuleContext* moduleContext, int index )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_TEST_MEMORY_COUNTERS );

    if( index >= 0 && index < SCT_MAX_TEST_MEMORY_COUNTERS )
    {
        return moduleContext->memoryCounters[ index ];
    }

    return 0;
}

/*!
 *
 *
 */
void sctiTestModuleSetMemoryCounter( SCTTestModuleContext* moduleContext, int index, size_t v )
{
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( index >= 0 && index < SCT_MAX_TEST_MEMORY_COUNTERS );
    
    if( index >= 0 && index < SCT_MAX_TEST_MEMORY_COUNTERS )
    {
        moduleContext->memoryCounters[ index ] = v;
    }
}

/*!
 *
 *
 */
void sctiTestModuleBusy( SCTTestModuleContext* moduleContext, int millis )
{
    unsigned long   then;
    
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( millis > 0 );

    then = siCommonGetTimerTick( NULL ) + ( unsigned long )( millis * ( double )( siCommonGetTimerFrequency( NULL ) ) / 1000.0 );
    
    for( ; ; )
    {        
        sctiTestModuleDoBusyIterations( moduleContext, 1 );

        if( siCommonGetTimerTick( NULL ) >= then )
        {
            return;
        }
    }
}

/*!
 *
 *
 */
unsigned long sctiTestModuleGetBusyIterations( SCTTestModuleContext* moduleContext, int millis )
{
    unsigned long   then;
    unsigned long   i;
    
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( millis > 0 );

    then = siCommonGetTimerTick( NULL ) + ( unsigned long )( millis * siCommonGetTimerFrequency( NULL ) / 1000.0 );
    
    for( i = 0 ; ; ++i )
    {        
        _busyIterations( i );

        if( siCommonGetTimerTick( NULL ) >= then )
        {
            return i;
        }
    }    
}

/*!
 *
 *
 */
void sctiTestModuleDoBusyIterations( SCTTestModuleContext* moduleContext, unsigned long iterations )
{
    unsigned long   i;
    unsigned long   never;
    
    SCT_ASSERT( moduleContext != NULL );
    SCT_ASSERT( iterations >= 0 );

    never = siCommonGetTimerTick( NULL ) + 0x0FFFFFFFUL;
    
    /* NOTE: the following loop should match the loop in
     * sctiTestModuleGetBusyIterations as closely as possible
     * performance-wise.*/
    
    for( i = 0 ; i < iterations; ++i )
    {        
        _busyIterations( i );
        if( siCommonGetTimerTick( NULL ) > never )
        {
            SCT_ASSERT_ALWAYS( 0 );    /* Should not be reached. */
            break;
        }
    }    
}

/*!
 *
 *
 */
static float _busyIterations( unsigned long iterations )
{
    unsigned long   i;
    float           f1 = 1.0f;
    float           f2 = 0.0f;
    
    for( i = 0 ; i < iterations; ++i )
    {
        f2 = _busyIteration( f1 );
        f1 = f2;
    }

    return f2;
}

/*!
 *
 *
 */
static float _busyIteration( float d )
{
    float   f;
    float   inmatrix[] = { 2.0f, 0.0f, 0.0f, 0.0f,
                           0.0f, 2.0f, 0.0f, 0.0f,
                           0.0f, 0.0f, 2.0f, 0.0f,
                           0.0f, 0.0f, 0.0f, 2.0f };
    float   outmatrix[ 16 ];
    int     i;

    _invert4x4Matrix( inmatrix, outmatrix );

    f = 0;
    for( i = 0; i < 16; ++i )
    {
        f += outmatrix[ i ];
    }

    return f / d;
}

/*!
 *
 *
 */
static void _invert4x4Matrix( const float m[ 16 ], float out[ 16 ] )
{
    float   inv[ 16 ];
    float   det;
    int     i;

    inv[0]  = m[5]*m[10]*m[15] - m[5]*m[11]*m[14] - m[9]*m[6]*m[15] + m[9]*m[7]*m[14] + m[13]*m[6]*m[11] - m[13]*m[7]*m[10];
    inv[4]  = -m[4]*m[10]*m[15] + m[4]*m[11]*m[14] + m[8]*m[6]*m[15] - m[8]*m[7]*m[14] - m[12]*m[6]*m[11] + m[12]*m[7]*m[10];
    inv[8]  = m[4]*m[9]*m[15] - m[4]*m[11]*m[13] - m[8]*m[5]*m[15] + m[8]*m[7]*m[13] + m[12]*m[5]*m[11] - m[12]*m[7]*m[9];
    inv[12] = -m[4]*m[9]*m[14] + m[4]*m[10]*m[13] + m[8]*m[5]*m[14] - m[8]*m[6]*m[13] - m[12]*m[5]*m[10] + m[12]*m[6]*m[9];
    inv[1]  = -m[1]*m[10]*m[15] + m[1]*m[11]*m[14] + m[9]*m[2]*m[15] - m[9]*m[3]*m[14] - m[13]*m[2]*m[11] + m[13]*m[3]*m[10];
    inv[5]  = m[0]*m[10]*m[15] - m[0]*m[11]*m[14] - m[8]*m[2]*m[15] + m[8]*m[3]*m[14] + m[12]*m[2]*m[11] - m[12]*m[3]*m[10];
    inv[9]  = -m[0]*m[9]*m[15] + m[0]*m[11]*m[13] + m[8]*m[1]*m[15] - m[8]*m[3]*m[13] - m[12]*m[1]*m[11] + m[12]*m[3]*m[9];
    inv[13] = m[0]*m[9]*m[14] - m[0]*m[10]*m[13] - m[8]*m[1]*m[14] + m[8]*m[2]*m[13] + m[12]*m[1]*m[10] - m[12]*m[2]*m[9];
    inv[2]  = m[1]*m[6]*m[15] - m[1]*m[7]*m[14] - m[5]*m[2]*m[15] + m[5]*m[3]*m[14] + m[13]*m[2]*m[7] - m[13]*m[3]*m[6];
    inv[6]  = -m[0]*m[6]*m[15] + m[0]*m[7]*m[14] + m[4]*m[2]*m[15] - m[4]*m[3]*m[14] - m[12]*m[2]*m[7] + m[12]*m[3]*m[6];
    inv[10] = m[0]*m[5]*m[15] - m[0]*m[7]*m[13] - m[4]*m[1]*m[15] + m[4]*m[3]*m[13] + m[12]*m[1]*m[7] - m[12]*m[3]*m[5];
    inv[14] = -m[0]*m[5]*m[14] + m[0]*m[6]*m[13] + m[4]*m[1]*m[14] - m[4]*m[2]*m[13] - m[12]*m[1]*m[6] + m[12]*m[2]*m[5];
    inv[3]  = -m[1]*m[6]*m[11] + m[1]*m[7]*m[10] + m[5]*m[2]*m[11] - m[5]*m[3]*m[10] - m[9]*m[2]*m[7] + m[9]*m[3]*m[6];
    inv[7]  = m[0]*m[6]*m[11] - m[0]*m[7]*m[10] - m[4]*m[2]*m[11] + m[4]*m[3]*m[10] + m[8]*m[2]*m[7] - m[8]*m[3]*m[6];
    inv[11] = -m[0]*m[5]*m[11] + m[0]*m[7]*m[9] + m[4]*m[1]*m[11] - m[4]*m[3]*m[9] - m[8]*m[1]*m[7] + m[8]*m[3]*m[5];
    inv[15] = m[0]*m[5]*m[10] - m[0]*m[6]*m[9] - m[4]*m[1]*m[10] + m[4]*m[2]*m[9] + m[8]*m[1]*m[6] - m[8]*m[2]*m[5];
    
    det = 1.0f / ( m[0]*inv[0] + m[1]*inv[4] + m[2]*inv[8] + m[3]*inv[12] );

    for( i = 0; i < 16; i++ )
    {
        out[ i ] = inv[ i ] * det;
    }
}

