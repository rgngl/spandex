/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testassertbenchmarkaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_types.h"
#include "sct_result.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestAssertBenchmarkActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestAssertBenchmarkActionContext*    context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestAssertBenchmarkActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestAssertBenchmarkActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in AssertBenchmark@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestAssertBenchmarkActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestAssertBenchmarkActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestAssertBenchmarkActionContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestAssertBenchmarkActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestAssertBenchmarkActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTTestAssertBenchmarkActionContext* context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCTTestAssertBenchmarkActionContext* )( action->context );

    context->benchmark = benchmark;
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiTestAssertBenchmarkActionExecute( SCTAction* action, int framenumber )
{
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( framenumber );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestAssertBenchmarkActionTerminate( SCTAction* action )
{
    SCTTestAssertBenchmarkActionContext*    context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTTestAssertBenchmarkActionContext* )( action->context );

    if( context->benchmark != NULL )
    {
        SCTBenchmarkResultListIterator  iterator;
        int                             i;
    
        SCT_ASSERT_ALWAYS( context->benchmark->benchmarkResultList != NULL );
        
        sctGetBenchmarkResultListIterator( context->benchmark->benchmarkResultList, &iterator );
        for( i = 0; sctGetNextBenchmarkResultListElement( &iterator ) != NULL; ++i )
        {
        }
        
        SCT_ASSERT_ALWAYS( i > 0 );

        context->benchmark = NULL;
    }
}

/*!
 *
 *
 */
void sctiTestAssertBenchmarkActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestAssertBenchmarkActionContext( ( SCTTestAssertBenchmarkActionContext* )( action->context ) );
    action->context = NULL;
}

