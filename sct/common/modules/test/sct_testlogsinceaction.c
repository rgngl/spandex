/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_testlogsinceaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_testmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateTestLogSinceActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTTestLogSinceActionContext*   context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTTestLogSinceActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTTestLogSinceActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in LogSince@Test context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTTestLogSinceActionContext ) );

    context->moduleContext = ( SCTTestModuleContext* )( moduleContext );

    if( sctiParseTestLogSinceActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyTestLogSinceActionContext( context );
        return NULL;
    }

    if( sctiTestModuleIsValidTimerIndex( context->moduleContext, context->data.timerIndex ) == SCT_FALSE )
    {
        sctiDestroyTestLogSinceActionContext( context );        
        SCT_LOG_ERROR( "Invalid timer index in LogSince@Test context creation." );
        return NULL;
    }

    context->bufferLength = context->data.maxSampleCount;
    context->buffer = ( double* )( siCommonMemoryAlloc( NULL, context->bufferLength * sizeof( double ) ) );
    
    if( context->buffer == NULL )
    {
        sctiDestroyTestLogSinceActionContext( context );        
        SCT_LOG_ERROR( "Allocation failed in LogSince@Test context creation." );
        return NULL;
    }

    context->frequency = siCommonGetTimerFrequency( NULL );
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroyTestLogSinceActionContext( void* context )
{
    SCTTestLogSinceActionContext*   c;
    
    if( context == NULL )
    {
        return;
    }

    c = ( SCTTestLogSinceActionContext* )( context );
   
    if( c->buffer != NULL )
    {
        siCommonMemoryFree( NULL, c->buffer );
        c->buffer = NULL;
    }
    
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiTestLogSinceActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCTTestLogSinceActionContext*   c;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    c = ( SCTTestLogSinceActionContext* )( action->context );
    c->bufferIndex = 0;
    c->reported    = SCT_FALSE;

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiTestLogSinceActionExecute( SCTAction* action, int framenumber )
{
    SCTTestLogSinceActionContext*   context;
    TestLogSinceActionData*         data;
    unsigned long                   latest;
    unsigned long                   now;
    double                          ms;
    
    SCT_ASSERT( action != NULL );
    SCT_ASSERT( action->context != NULL );    
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTTestLogSinceActionContext* )( action->context );
    data    = &( context->data );

    if( context->bufferIndex < context->bufferLength )
    {
        latest = sctiTestModuleGetTimer( context->moduleContext, data->timerIndex );
        now    = siCommonGetTimerTick( NULL );
        ms     = ( ( now - latest ) / ( double )( context->frequency ) ) * 1000.0 ;

        context->buffer[ context->bufferIndex ] = ms;
    }

    context->bufferIndex++;
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestLogSinceActionTerminate( SCTAction* action )
{
    SCTTestLogSinceActionContext*   context;
    TestLogSinceActionData*         data;
    char                            buf1[ 32 ];
    char*                           buf2;
    int                             min;
    int                             i;
    SCTAttributeList*               attributes;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTTestLogSinceActionContext* )( action->context );
    data    = &( context->data );

    if( context->reported != SCT_FALSE )
    {
        return;
    }
    
    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        return;
    }
    
    buf2 = ( char* )( siCommonMemoryAlloc( NULL, context->bufferIndex * sizeof( buf1 ) ) );
    if( buf2 != NULL )
    {
        buf2[ 0 ] = '\0';
        
        min = sctMin( context->bufferIndex, data->maxSampleCount );
        for( i = 0; i < min; ++i )
        {
            sprintf( buf1, "%.1f, ", context->buffer[ i ] );
            strcat( buf2, buf1 );                   
        }
        
        sctAddNameValueToList( attributes, "Milliseconds", buf2 );
        siCommonMemoryFree( NULL, buf2 );
    }
    
    if( context->bufferIndex > data->maxSampleCount )
    {
        sctAddNameValueToList( attributes, "Warning", "Sample buffer overrrun" );
    }                
        
    sctReportSection( data->message, "LOG_SINCE", attributes );                
    sctDestroyAttributeList( attributes );

    context->reported = SCT_TRUE;
}

/*!
 *
 *
 */
void sctiTestLogSinceActionDestroy( SCTAction* action )
{   
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyTestLogSinceActionContext( ( SCTTestLogSinceActionContext* )( action->context ) );
    action->context = NULL;
}
