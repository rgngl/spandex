/*
 * GENERATED FILE.
 *
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if defined( _WIN32 )
# pragma warning (disable: 4305)
# pragma warning (disable: 4100)
# pragma warning (disable: 4505)
# pragma warning (disable: 4245)
# pragma warning (disable: 4090)
#endif  /* defined( _WIN32 ) */

#include "sct_$LAPItrace$LNAMEaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

$INCLUDES

#include "sct_eglwrapper.h"
#include "$LNAME.inl"

/*!
 *
 */
void* sctiCreate$APITrace$NAMEActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCT$APITrace$NAMEActionContext* context;

    SCT_USE_VARIABLE( moduleContext );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCT$APITrace$NAMEActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCT$APITrace$NAMEActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in $NAME@$APITrace context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCT$APITrace$NAMEActionContext ) );

    if( sctiParse$APITrace$NAMEActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroy$APITrace$NAMEActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroy$APITrace$NAMEActionContext( void* context )
{
    SCT$APITrace$NAMEActionContext* c = ( SCT$APITrace$NAMEActionContext* )( context );

    if( c == NULL )
    {
        return;
    }

    SCT_ASSERT_ALWAYS( c->wrapperContext == NULL );
    siCommonMemoryFree( NULL, context );
}

/*!
 *
 */
SCTBoolean scti$APITrace$NAMEActionInit( SCTAction* action, SCTBenchmark* benchmark )
{
    SCT$APITrace$NAMEActionContext* context;
    $APITrace$NAMEActionData*       data;
    int                             i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    context = ( SCT$APITrace$NAMEActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT_ALWAYS( context->wrapperContext == NULL );
    
    context->wrapperContext = sctCreateEglWrapperContext();
    if( context->wrapperContext == NULL )
    {
        SCT_LOG_ERROR( "Creating wrapper context failed in $NAME@$APITrace action init." );
        return SCT_FALSE;
    }
    
    for( i = 0; i < data->header; ++i )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: $NAME@$APITrace init, playing frame %d", i );
#endif  /* defined( SCT_DEBUG ) */
        
        playFrame( context->wrapperContext, i );

#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: $NAME@$APITrace init, frame %d done", i );
#endif  /* defined( SCT_DEBUG ) */       
    }

    /* Error checking is not needed here as it can be done outside by defining a
     * suitable error checking action and adding it to the benchmark file.*/

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean scti$APITrace$NAMEActionExecute( SCTAction* action, int frameNumber )
{
    SCT$APITrace$NAMEActionContext* context;
    $APITrace$NAMEActionData*       data;
    int                             f;
   
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCT$APITrace$NAMEActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT_ALWAYS( context->wrapperContext != NULL );
    
    f = data->header + frameNumber;
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: $NAME@$APITrace execute, playing frame %d", f );
#endif  /* defined( SCT_DEBUG ) */
    
    playFrame( context->wrapperContext, f );
    context->lastFrame = f;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: $NAME@$APITrace execute, frame %d done", f );
#endif  /* defined( SCT_DEBUG ) */
    
    return SCT_TRUE;
}

/*!
 *
 */
void scti$APITrace$NAMEActionTerminate( SCTAction* action )
{
    SCT$APITrace$NAMEActionContext* context;
    $APITrace$NAMEActionData*       data;    
    int                             i;
    int                             f;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCT$APITrace$NAMEActionContext* )( action->context );
    data    = &( context->data );

    SCT_ASSERT_ALWAYS( context->wrapperContext != NULL );
    
    for( i = 1; i <= data->trailer; ++i )
    {
        f = context->lastFrame + i;
        
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: $NAME@$APITrace terminate, playing frame %d", f );
#endif  /* defined( SCT_DEBUG ) */
        
        playFrame( context->wrapperContext, f );
        
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: $NAME@$APITrace terminate, frame %d done", f );
#endif  /* defined( SCT_DEBUG ) */
        
    }

    sctDestroyEglSwapperContext( context->wrapperContext );
    context->wrapperContext = NULL;
}

/*!
 *
 *
 */
void scti$APITrace$NAMEActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroy$APITrace$NAMEActionContext( ( SCT$APITrace$NAMEActionContext* )( action->context ) );
    action->context = NULL;
}
