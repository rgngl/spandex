/*
 * GENERATED FILE.
 *
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_$UAPITRACE$UNAMEACTION_H__ )
#define __SCT_$UAPITRACE$UNAMEACTION_H__

#include "sct_types.h"
#include "sct_$LAPItracemodule.h"
#include "sct_$LAPItracemodule_parser.h"

/*!
 *
 */
typedef struct
{
    void*                               wrapperContext;
    $APITrace$NAMEActionData            data;
    int                                 lastFrame;
} SCT$APITrace$NAMEActionContext;
 
#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                               sctiCreate$APITrace$NAMEActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroy$APITrace$NAMEActionContext( void* context );

    SCTBoolean                          scti$APITrace$NAMEActionInit( SCTAction* action, SCTBenchmark* benchmark );
    SCTBoolean                          scti$APITrace$NAMEActionExecute( SCTAction* action, int frameNumber );
    void                                scti$APITrace$NAMEActionTerminate( SCTAction* action );
    void                                scti$APITrace$NAMEActionDestroy( SCTAction* action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_$UAPITRACE$UNAMEACTION_H__ ) */
