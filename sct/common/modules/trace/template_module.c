/*
 * GENERATED FILE. 
 *
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_$LNAMEtracemodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_$LNAMEtracemodule_parser.h"
#include "sct_$LNAMEtracemodule_actions.h"

#include <stdio.h>
#include <string.h>

/* Local function declarations. */
SCTAttributeList*       scti$NAMETraceModuleInfo( SCTModule* module );
SCTAction*              scti$NAMETraceModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    scti$NAMETraceModuleDestroy( SCTModule* module );

/*!
 *
 *
 */
SCTModule* sctCreate$NAMETraceModule( void )
{
    return sctCreateModule( "$NAMETrace",
                            NULL,
#if defined( _WIN32 )
                            _$LNAMEtrace_parser_config,
#else   /* defined( _WIN32 ) */
                            "",
#endif  /* defined( _WIN32 ) */
                            scti$NAMETraceModuleInfo,
                            scti$NAMETraceModuleCreateAction,
                            scti$NAMETraceModuleDestroy );
}

/*!
 *
 *
 */
SCTAttributeList* scti$NAMETraceModuleInfo( SCTModule* module )
{
    SCTAttributeList*   attributes;

    SCT_ASSERT_ALWAYS( module != NULL );

    attributes = sctCreateAttributeList();

    return attributes;
}

/*!
 *
 *
 */
SCTAction* scti$NAMETraceModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    return sctCreateActionFromTemplate( name,
                                        type,
                                        "$NAMETrace",
                                        NULL,
                                        attributes,
                                        $NAMETraceActionTemplates,
                                        SCT_ARRAY_LENGTH( $NAMETraceActionTemplates ) );
}

/*!
 *
 *
 */
void scti$NAMETraceModuleDestroy( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );
}
