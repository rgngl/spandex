/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_sicommon.h"
#include "sct_types.h"
#include "sct_utils.h"
#include "sct_eglwrapper.h"
#include "sct_siwindow.h"

#include <string.h>

/*!
 *
 *
 */
struct _SCTEGLWrapperContext
{
    void*       siWindow;
    SCTList*    windowList;
    SCTList*    pixmapList;
    EGLDisplay  display;     
};

typedef struct _SCTEGLWrapperContext SCTEGLWrapperContext;


/*!
 *
 *
 */
static void*            _createNativeWindowType( void* context, int x, int y, int width, int height, int mode );
static void             _destroyNativeWindowType( void* context, void* window );
//static SCTColorFormat   _getPixmapFormat( int mode );


/*!
 *
 *
 */
void* sctCreateEglWrapperContext()
{
    SCTEGLWrapperContext* context;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglWrapperContext start" );
#endif  /* defined( SCT_DEBUG ) */
    
    context = ( SCTEGLWrapperContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEGLWrapperContext ) ) );
    SCT_ASSERT_ALWAYS( context != NULL );
    
    memset( context, 0, sizeof( SCTEGLWrapperContext ) );

    /* Use do or die allocation/error checking throughout this file. */
    
    context->windowList = sctCreateList();
    SCT_ASSERT_ALWAYS( context->windowList != NULL );
    context->pixmapList = sctCreateList();
    SCT_ASSERT_ALWAYS( context->pixmapList != NULL );
    
    context->siWindow = siWindowCreateContext();
    SCT_ASSERT_ALWAYS( context->siWindow != NULL );

    context->display = eglGetDisplay( EGL_DEFAULT_DISPLAY );
    SCT_ASSERT_ALWAYS( context->display != EGL_NO_DISPLAY );

    SCT_ASSERT_ALWAYS( eglInitialize( context->display, NULL, NULL ) != SCT_FALSE );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglWrapperContext end" );
#endif  /* defined( SCT_DEBUG ) */
    
    return context;
}

/*!
 *
 *
 */
void sctDestroyEglSwapperContext( void* context )
{
    SCTEGLWrapperContext*   c;
    SCTListIterator         iterator;
    void*                   p;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctDestroyEglWrapperContext start" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( context != NULL );
    
    c = ( SCTEGLWrapperContext* )( context );

    SCT_ASSERT_ALWAYS( c->siWindow != NULL );
    SCT_ASSERT_ALWAYS( c->windowList != NULL );
    SCT_ASSERT_ALWAYS( c->pixmapList != NULL );

    sctGetListIterator( c->windowList, &iterator );
    while( ( p = sctGetNextListElement( &iterator ) ) != NULL )
    {
        siWindowDestroyWindow( c->siWindow, p );
    }
    sctDestroyList( c->windowList );
    c->windowList = NULL;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctDestroyEglWrapperContext windows destroyed" );
#endif  /* defined( SCT_DEBUG ) */
    
    sctGetListIterator( c->pixmapList, &iterator );
    while( ( p = sctGetNextListElement( &iterator ) ) != NULL )
    {
        siWindowDestroyPixmap( c->siWindow, p );
    }
    sctDestroyList( c->pixmapList );
    c->pixmapList = NULL;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctDestroyEglWrapperContext pixmaps destroyed" );
#endif  /* defined( SCT_DEBUG ) */
    
    siWindowDestroyContext( c->siWindow );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctDestroyEglWrapperContext window context destroyed" );
#endif  /* defined( SCT_DEBUG ) */
    
    if( c->display != EGL_NO_DISPLAY )
    {
        eglTerminate( c->display );
        c->display = EGL_NO_DISPLAY;

#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctDestroyEglWrapperContext EGL terminated" );
#endif  /* defined( SCT_DEBUG ) */      
    }
    
    eglReleaseThread();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctDestroyEglWrapperContext thread EGL released" );
#endif  /* defined( SCT_DEBUG ) */
    
    siCommonMemoryFree( NULL, c );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctDestroyEglWrapperContext end" );
#endif  /* defined( SCT_DEBUG ) */
   
}

/*
 *
 */
EGLConfig createEGLConfig28( void*  context,
                             EGLint config_id,
                             EGLint buffer_size,
                             EGLint red_size,
                             EGLint green_size,
                             EGLint blue_size,
                             EGLint alpha_size,
                             EGLint bind_to_texture_rgb,
                             EGLint bind_to_texture_rgba,
                             EGLint config_caveat,
                             EGLint depth_size,
                             EGLint level,
                             EGLint max_swap_interval,
                             EGLint min_swap_interval,
                             EGLint native_renderable,
                             EGLint sample_buffers,
                             EGLint samples,
                             EGLint stencil_size,
                             EGLint surface_type,
                             EGLint transparent_type,
                             EGLint transparent_red,
                             EGLint transparent_green,
                             EGLint transparent_blue,
                             EGLint luminance_size,
                             EGLint alpha_mask_size,
                             EGLint color_buffer_type,
                             EGLint conformant,
                             EGLint renderable_type )
{
    EGLint                  attrs[ 64 ];
    EGLint                  n               = 0;
    EGLint                  numConfigs      = 0;
    EGLConfig               config;
    SCTEGLWrapperContext*   c;

    SCT_ASSERT_ALWAYS( context != NULL );   
    c = ( SCTEGLWrapperContext* )( context );

    SCT_USE_VARIABLE( config_id );
    SCT_USE_VARIABLE( buffer_size );
    SCT_USE_VARIABLE( red_size );
    SCT_USE_VARIABLE( green_size );
    SCT_USE_VARIABLE( blue_size );
    SCT_USE_VARIABLE( alpha_size );
    SCT_USE_VARIABLE( bind_to_texture_rgb );
    SCT_USE_VARIABLE( bind_to_texture_rgba );
    SCT_USE_VARIABLE( config_caveat );
    SCT_USE_VARIABLE( depth_size );
    SCT_USE_VARIABLE( level );
    SCT_USE_VARIABLE( max_swap_interval );
    SCT_USE_VARIABLE( min_swap_interval );
    SCT_USE_VARIABLE( native_renderable );
    SCT_USE_VARIABLE( sample_buffers );
    SCT_USE_VARIABLE( samples );
    SCT_USE_VARIABLE( stencil_size );
    SCT_USE_VARIABLE( surface_type );
    SCT_USE_VARIABLE( transparent_type );
    SCT_USE_VARIABLE( transparent_red );
    SCT_USE_VARIABLE( transparent_green );
    SCT_USE_VARIABLE( transparent_blue );
    SCT_USE_VARIABLE( luminance_size );
    SCT_USE_VARIABLE( alpha_mask_size );
    SCT_USE_VARIABLE( color_buffer_type );
    SCT_USE_VARIABLE( conformant );
    SCT_USE_VARIABLE( renderable_type );

#if !defined( SCT_FORCE_OPENVG_TRACE_CONFIG )
    attrs[ n++ ] = EGL_SURFACE_TYPE;
    attrs[ n++ ] = EGL_WINDOW_BIT;
    attrs[ n++ ] = EGL_RED_SIZE;
    attrs[ n++ ] = red_size;
    attrs[ n++ ] = EGL_GREEN_SIZE;
    attrs[ n++ ] = green_size;
    attrs[ n++ ] = EGL_BLUE_SIZE;
    attrs[ n++ ] = blue_size;
    attrs[ n++ ] = EGL_ALPHA_SIZE;
    attrs[ n++ ] = alpha_size;
    attrs[ n++ ] = EGL_DEPTH_SIZE;
    attrs[ n++ ] = depth_size;
    attrs[ n++ ] = EGL_STENCIL_SIZE;
    attrs[ n++ ] = stencil_size;
    attrs[ n++ ] = EGL_RENDERABLE_TYPE;
    attrs[ n++ ] = renderable_type;
    attrs[ n++ ] = EGL_ALPHA_MASK_SIZE;
    attrs[ n++ ] = alpha_mask_size;
    attrs[ n++ ] = EGL_NONE;
#else   /* !defined( SCT_FORCE_OPENVG_TRACE_CONFIG ) */
    attrs[ n++ ] = EGL_SURFACE_TYPE;
    attrs[ n++ ] = EGL_WINDOW_BIT;
    attrs[ n++ ] = EGL_RED_SIZE;
    attrs[ n++ ] = 8;
    attrs[ n++ ] = EGL_GREEN_SIZE;
    attrs[ n++ ] = 8;
    attrs[ n++ ] = EGL_BLUE_SIZE;
    attrs[ n++ ] = 8;
    attrs[ n++ ] = EGL_ALPHA_SIZE;
    attrs[ n++ ] = 8;
    attrs[ n++ ] = EGL_ALPHA_MASK_SIZE;
    attrs[ n++ ] = 8;
    attrs[ n++ ] = EGL_RENDERABLE_TYPE;
    attrs[ n++ ] = EGL_OPENVG_BIT;
    attrs[ n++ ] = EGL_NONE;
#endif  /* !defined( SCT_FORCE_OPENVG_TRACE_CONFIG ) */
    
    eglChooseConfig( c->display, attrs, &config, 1, &numConfigs );
    
    SCT_ASSERT_ALWAYS( numConfigs > 0 );
    
    return config;
}

/*
 *
 */
void destroyEGLConfig2( void* context, EGLConfig config )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( config );  
}

/*!
 *
 */
EGLNativeDisplayType createEGLNativeDisplayType1( void* context )
{
    SCT_USE_VARIABLE( context );
    return EGL_DEFAULT_DISPLAY;
}

/*!
 *
 */
void destroyEGLNativeDisplayType2( void* context, EGLNativeDisplayType displayType )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( displayType );
}

/*
 *
 */
void* createEGLNativeWindowType1( void* context )
{
    SCTEGLWrapperContext*   c;
    int                     width;
    int                     height;

    SCT_ASSERT_ALWAYS( context != NULL );
    
    c = ( SCTEGLWrapperContext* )( context );
    SCT_ASSERT_ALWAYS( c->siWindow != NULL );
    
    siWindowGetScreenSize( c->siWindow, SCT_SCREEN_DEFAULT, &width, &height );
    
    return _createNativeWindowType( context, 0, 0, width, height, 0 );
}

/*
 *
 */
void* createEGLNativeWindowType6( void* context, int x, int y, int width, int height, int mode )
{
    return _createNativeWindowType( context, x, y, width, height, mode );
}

/*!
 *
 */
EGLNativeWindowType modifyEGLNativeWindowType7( void* context,
                                                EGLNativeWindowType nativeWindow,
                                                int x,
                                                int y,
                                                int width,
                                                int height,
                                                int mode )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( x );
    SCT_USE_VARIABLE( y );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( mode );

    /* TODO: currently do nothing. */ 
    
    return nativeWindow;
}

/*!
 *
 */
void* createEGLNativePixmapType4( void* context, int width, int height, int mode )
{
    SCTEGLWrapperContext*   c;
    void*                   pixmap;

    SCT_ASSERT_ALWAYS( context != NULL );
    
    c = ( SCTEGLWrapperContext* )( context );
    SCT_ASSERT_ALWAYS( c->siWindow != NULL );
    SCT_ASSERT_ALWAYS( c->pixmapList != NULL );
   
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( mode );

    pixmap = siWindowCreatePixmap( c->siWindow, width, height, SCT_COLOR_FORMAT_RGBA8888, NULL, 0 );
    SCT_ASSERT_ALWAYS( pixmap != NULL );

    SCT_ASSERT_ALWAYS( sctAddToList( c->pixmapList, pixmap ) != SCT_FALSE );     
    
    return siWindowGetEglPixmap( c->siWindow, pixmap );
}   

/*!
 *
 */
void* modifyEGLNativePixmapType5( void* context, void* pixmap, int width, int height, int mode )
{
    destroyEGLNativePixmapType2( context, pixmap );
    return createEGLNativePixmapType4( context, width, height, mode );
}

/*!
 *
 */
void destroyEGLNativePixmapType2( void* context, void* pixmap )
{
    SCTEGLWrapperContext*   c;    
    SCTListIterator         iterator;
    void*                   p;
    
    SCT_USE_VARIABLE( context );
    SCT_ASSERT_ALWAYS( pixmap != NULL );

    c = ( SCTEGLWrapperContext* )( context );
    SCT_ASSERT_ALWAYS( c->siWindow != NULL );
    SCT_ASSERT_ALWAYS( c->pixmapList != NULL );

    sctGetListIterator( c->pixmapList, &iterator );
    while( ( p = sctGetNextListElement( &iterator ) ) != NULL )
    {
        if( siWindowGetEglPixmap( c->siWindow, p ) == pixmap )
        {
            SCT_ASSERT_ALWAYS( sctRemoveFromList( c->pixmapList, p ) != SCT_FALSE );
            siWindowDestroyPixmap( c->siWindow, p );           
            return;
        }
    }
}

/*!
 *
 */
void destroyEGLNativeWindowType2( void* context, void* window )
{
    _destroyNativeWindowType( context, window );
}

/*
 *
 *
 */
static void* _createNativeWindowType( void* context, int x, int y, int width, int height, int mode )
{
    SCTEGLWrapperContext*   c;
    void*                   window;

    SCT_ASSERT_ALWAYS( context != NULL );
    
    c = ( SCTEGLWrapperContext* )( context );
    SCT_ASSERT_ALWAYS( c->siWindow != NULL );
   
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( mode );

    window = siWindowCreateWindow( c->siWindow, SCT_SCREEN_DEFAULT, x, y, width, height, SCT_COLOR_FORMAT_DEFAULT, NULL );
    SCT_ASSERT_ALWAYS( window != NULL );

    SCT_ASSERT_ALWAYS( sctAddToList( c->windowList, window ) != SCT_FALSE );     
    
    return siWindowGetEglWindow( c->siWindow, window );
}

/*!
 *
 *
 */
static void _destroyNativeWindowType( void* context, void* window )
{
    SCTEGLWrapperContext*   c;    
    SCTListIterator         iterator;
    void*                   p;
    
    SCT_USE_VARIABLE( context );
    SCT_ASSERT_ALWAYS( window != NULL );

    c = ( SCTEGLWrapperContext* )( context );
    SCT_ASSERT_ALWAYS( c->siWindow != NULL );
    SCT_ASSERT_ALWAYS( c->windowList != NULL );

    sctGetListIterator( c->windowList, &iterator );
    while( ( p = sctGetNextListElement( &iterator ) ) != NULL )
    {
        if( siWindowGetEglWindow( c->siWindow, p ) == window )
        {
            SCT_ASSERT_ALWAYS( sctRemoveFromList( c->windowList, p ) != SCT_FALSE );
            siWindowDestroyWindow( c->siWindow, p );
            return;
        }
    }
}

/* ********************************************************************** */
/* Hacks for broken traces*/
int createVGImage1( void* context )
{
    SCT_USE_VARIABLE( context );
    
    return 0;
}

void destroyVGImage2( void* context, int image )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( image );   
}

int createVGPath1( void* context )
{
    SCT_USE_VARIABLE( context );
    
    return 0;
}

void destroyVGPath2( void* context, int path )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( path );
    
}

int createVGPaint1( void* context )
{
    SCT_USE_VARIABLE( context );
    
    return 0;
}

void destroyVGPaint2( void* context, int paint )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( paint );
}

int createGLframebuffer1( void* context )
{
    SCT_USE_VARIABLE( context );
    
    return 0;
}

void destroyGLframebuffer2( void* context, int fb )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( fb );
}

int createGLrenderbuffer1( void* context )
{
    SCT_USE_VARIABLE( context );
    
    return 0;
}

void destroyGLrenderbuffer2( void* context, int fb )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( fb );
}

EGLConfig modifyEGLConfig13( void* context, EGLConfig config, EGLint config_id, EGLint buffer_size, EGLint red_size, EGLint green_size, EGLint blue_size, EGLint alpha_size, EGLint bind_to_texture_rgb, EGLint bind_to_texture_rgba, EGLint config_caveat, EGLint depth_size, EGLint level )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( config );
    SCT_USE_VARIABLE( config_id );
    SCT_USE_VARIABLE( buffer_size );
    SCT_USE_VARIABLE( red_size );
    SCT_USE_VARIABLE( green_size );
    SCT_USE_VARIABLE( blue_size );
    SCT_USE_VARIABLE( alpha_size );
    SCT_USE_VARIABLE( bind_to_texture_rgb );
    SCT_USE_VARIABLE( bind_to_texture_rgba );
    SCT_USE_VARIABLE( config_caveat );
    SCT_USE_VARIABLE( depth_size );
    SCT_USE_VARIABLE( level );

    return config;        
}

EGLConfig modifyEGLConfig29( void* context, EGLConfig config, EGLint config_id, EGLint buffer_size, EGLint red_size, EGLint green_size, EGLint blue_size, EGLint alpha_size, EGLint bind_to_texture_rgb, EGLint bind_to_texture_rgba, EGLint config_caveat, EGLint depth_size, EGLint level, EGLint max_swap_interval, EGLint min_swap_interval, EGLint native_renderable, EGLint sample_buffers, EGLint samples, EGLint stencil_size, EGLint surface_type, EGLint transparent_type, EGLint transparent_red, EGLint transparent_green, EGLint transparent_blue, EGLint luminance_size, EGLint alpha_mask_size, EGLint color_buffer_type, EGLint conformant, EGLint renderable_type )
{ 
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( config );
    SCT_USE_VARIABLE( config_id );
    SCT_USE_VARIABLE( buffer_size );
    SCT_USE_VARIABLE( red_size );
    SCT_USE_VARIABLE( green_size );
    SCT_USE_VARIABLE( blue_size );
    SCT_USE_VARIABLE( alpha_size );
    SCT_USE_VARIABLE( bind_to_texture_rgb );
    SCT_USE_VARIABLE( bind_to_texture_rgba );
    SCT_USE_VARIABLE( config_caveat );
    SCT_USE_VARIABLE( depth_size );
    SCT_USE_VARIABLE( level );
    SCT_USE_VARIABLE( max_swap_interval );
    SCT_USE_VARIABLE( min_swap_interval );
    SCT_USE_VARIABLE( native_renderable );
    SCT_USE_VARIABLE( sample_buffers );
    SCT_USE_VARIABLE( samples );
    SCT_USE_VARIABLE( stencil_size );
    SCT_USE_VARIABLE( surface_type );
    SCT_USE_VARIABLE( transparent_type );
    SCT_USE_VARIABLE( transparent_red );
    SCT_USE_VARIABLE( transparent_green );
    SCT_USE_VARIABLE( transparent_blue );
    SCT_USE_VARIABLE( luminance_size );
    SCT_USE_VARIABLE( alpha_mask_size );
    SCT_USE_VARIABLE( color_buffer_type );
    SCT_USE_VARIABLE( conformant );
    SCT_USE_VARIABLE( renderable_type );

    return config; 
}
