/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_sicommon.h"
#include "sct_types.h"
#include "sct_utils.h"
#include "sct_eglmemory.h"

#include <string.h>

#if defined( SCT_KHR_COMPLIANT_INCLUDES )
# include <EGL/egl.h>
#else
# include <egl.h>
#endif

#if !defined( EGL_PROF_QUERY_GLOBAL_BIT_NOK )
typedef unsigned int EGLNativeProcessIdTypeNOK;
#endif  /* !defined( EGL_PROF_QUERY_GLOBAL_BIT_NOK ) */

#if !defined( EGL_PROF_QUERY_GLOBAL_BIT_NOK )
# define EGL_PROF_QUERY_GLOBAL_BIT_NOK              0x0001
#endif  /* !defined( EGL_PROF_QUERY_GLOBAL_BIT_NOK ) */

#if !defined( EGL_PROF_QUERY_MEMORY_USAGE_BIT_NOK )
# define EGL_PROF_QUERY_MEMORY_USAGE_BIT_NOK        0x0002
#endif  /* !defined( EGL_PROF_QUERY_MEMORY_USAGE_BIT_NOK ) */

#if !defined( EGL_PROF_TOTAL_MEMORY_NOK )
# define EGL_PROF_TOTAL_MEMORY_NOK                  0x3070
#endif  /* !defined( EGL_PROF_TOTAL_MEMORY_NOK ) */

#if !defined( EGL_PROF_USED_MEMORY_NOK )
# define EGL_PROF_USED_MEMORY_NOK                   0x3071
#endif  /* !defined( EGL_PROF_USED_MEMORY_NOK ) */

#if !defined( EGL_PROF_PROCESS_ID_NOK )
# define EGL_PROF_PROCESS_ID_NOK                    0x3072
#endif  /* !defined( EGL_PROF_PROCESS_ID_NOK ) */

#if !defined( EGL_PROF_PROCESS_USED_PRIVATE_MEMORY_NOK )
# define EGL_PROF_PROCESS_USED_PRIVATE_MEMORY_NOK   0x3073
#endif  /* !defined( EGL_PROF_PROCESS_USED_PRIVATE_MEMORY_NOK ) */

#if !defined( EGL_PROF_PROCESS_USED_SHARED_MEMORY_NOK )
# define EGL_PROF_PROCESS_USED_SHARED_MEMORY_NOK    0x3074
#endif  /* !defined( EGL_PROF_PROCESS_USED_SHARED_MEMORY_NOK ) */

/*!
 *
 *
 */
typedef EGLBoolean ( *SCTEglQueryProfilingDataFunc )( EGLDisplay dpy, EGLint query_bits, EGLint* data, EGLint data_size, EGLint* data_count );

/*!
 *
 *
 */
struct _SCTEGLMemoryContext
{
    EGLDisplay                      display;
    SCTEglQueryProfilingDataFunc    func;
    EGLint*                         profileData;        /* Profile data element buffer. */
    EGLint                          profileDataCount;   /* Latest queried profile data element count. */
    EGLint                          profileDataLength;  /* Maximum length of the profile data buffer. */
};

typedef struct _SCTEGLMemoryContext SCTEGLMemoryContext;

/*!
 *
 *
 */
static SCTBoolean   sctiInitEglMemoryContext( void* context );

/*!
 * Create EGL memory context. Note that this function initializes and terminates
 * EGL, hence it should be called only during spandex initialization as it might
 * interfere with EGL module actions.
 *
 */
void* sctCreateEglMemoryContext()
{
    SCTEGLMemoryContext*    context;
    const char*             extString;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext start" );
#endif  /* defined( SCT_DEBUG ) */
    
    context = ( SCTEGLMemoryContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTEGLMemoryContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTEGLMemoryContext ) );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext context allocated" );
#endif  /* defined( SCT_DEBUG ) */
    
    /* Get EGL display, query EGL resource profile extension function, and
     * ensure EGL resource profile extension is listed in the extension
     * string. */
    
    context->display = eglGetDisplay( EGL_DEFAULT_DISPLAY );
    if( context->display == EGL_NO_DISPLAY )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext no EGL display" );
#endif  /* defined( SCT_DEBUG ) */
        
        sctDestroyEglMemoryContext( context );
        return NULL;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext got EGL display" );
#endif  /* defined( SCT_DEBUG ) */

    if( eglInitialize( context->display, NULL, NULL ) == EGL_FALSE )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext failed to initialize EGL" );
#endif  /* defined( SCT_DEBUG ) */
        
        sctDestroyEglMemoryContext( context );        
        return NULL;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext initialized EGL" );
#endif  /* defined( SCT_DEBUG ) */
    
    context->func = ( SCTEglQueryProfilingDataFunc )( siCommonGetProcAddress( NULL, "eglQueryProfilingDataNOK" ) );
    if( context->func == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext no resource profile func" );
#endif  /* defined( SCT_DEBUG ) */

        eglTerminate( context->display );
        sctDestroyEglMemoryContext( context );        
        return NULL;
    }
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext got resource profile function" );
#endif  /* defined( SCT_DEBUG ) */

    extString = ( const char* )( eglQueryString( context->display, EGL_EXTENSIONS ) );
    if( extString == NULL || strstr( extString, "EGL_NOK_resource_profiling" ) == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext no resource profile extension string" );
#endif  /* defined( SCT_DEBUG ) */

        eglTerminate( context->display );        
        sctDestroyEglMemoryContext( context );        
        return NULL;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext extension string present" );
#endif  /* defined( SCT_DEBUG ) */

    eglTerminate( context->display );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext terminated EGL" );
#endif  /* defined( SCT_DEBUG ) */
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctCreateEglMemoryContext end" );
#endif  /* defined( SCT_DEBUG ) */
    
    return context;
}

/*!
 *
 *
 */
void sctDestroyEglMemoryContext( void* context )
{
    SCTEGLMemoryContext*    c;

    if( context == NULL )
    {
        return;
    }
    
    c = ( SCTEGLMemoryContext* )( context );

    if( c->profileData != NULL )
    {
        siCommonMemoryFree( NULL, c->profileData );
        c->profileData = NULL;
    }

    siCommonMemoryFree( NULL, c );
}

/*!
 * Get EGL memory status. Note that this function relies EGL has been
 * initialized in the calling thread.
 *
 */
SCTBoolean sctGetEglMemoryStatus( void* context, SCTEGLMemory* memory )
{
    SCTEGLMemoryContext*    c;
    EGLint                  queryBits   = EGL_PROF_QUERY_GLOBAL_BIT_NOK | EGL_PROF_QUERY_MEMORY_USAGE_BIT_NOK;
    EGLint                  count;
    int                     i;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctGetEglMemoryStatus start" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( memory != NULL );
    
    c = ( SCTEGLMemoryContext* )( context );

    if( sctiInitEglMemoryContext( c ) == SCT_FALSE )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctGetEglMemoryStatus init failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        return SCT_FALSE;
    }
    
    if( c->func( c->display, queryBits, c->profileData, c->profileDataCount, &count ) == EGL_FALSE )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctGetEglMemoryStatus calling resource profile func failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        return SCT_FALSE;
    }

    memory->totalGraphicsMemory       = 0;
    memory->usedGraphicsMemory        = 0;
    memory->usedPrivateGraphicsMemory = 0;
    memory->usedSharedGraphicsMemory  = 0;
    
    for( i = 0; i < count; )
    {
        switch( c->profileData[ i ] )
        {
        case EGL_PROF_TOTAL_MEMORY_NOK:
            memory->totalGraphicsMemory = ( size_t )( c->profileData[ i + 1 ] );
            i += 2;
            break;
            
        case EGL_PROF_USED_MEMORY_NOK:
            memory->usedGraphicsMemory = ( size_t )( c->profileData[ i + 1 ] );
            i += 2;
            break;
                        
        case EGL_PROF_PROCESS_USED_PRIVATE_MEMORY_NOK:
            memory->usedPrivateGraphicsMemory += ( size_t )( c->profileData[ i + 1 ] );
            i += 2;
            break;
            
        case EGL_PROF_PROCESS_USED_SHARED_MEMORY_NOK:
            memory->usedSharedGraphicsMemory += ( size_t )( c->profileData[ i + 1 ] );
            i += 2;
            break;
            
        default:
            /* Just skip unpexpected attributes. */
            i += 2;
            break;
        }
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctGetEglMemoryStatus end" );
#endif  /* defined( SCT_DEBUG ) */
    
    return SCT_TRUE;

}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static SCTBoolean sctiInitEglMemoryContext( void* context )
{
    SCTEGLMemoryContext*    c;
    EGLint                  count;
    EGLint                  queryBits   = EGL_PROF_QUERY_GLOBAL_BIT_NOK | EGL_PROF_QUERY_MEMORY_USAGE_BIT_NOK;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctiInitEglMemoryContext start" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT( context != NULL );
    
    c = ( SCTEGLMemoryContext* )( context );

    SCT_ASSERT( c->display != EGL_NO_DISPLAY );
    SCT_ASSERT( c->func != NULL );
    
    if( c->func( c->display, queryBits, NULL, 0, &( count ) ) == EGL_FALSE ||
        count <= 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctiInitEglMemoryContext getting number of profile data elements failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        return SCT_FALSE;
    }

    if( count > c->profileDataLength )
    {
        if( c->profileData != NULL )
        {
            siCommonMemoryFree( NULL, c->profileData );
            c->profileData       = NULL;
            c->profileDataCount  = 0;
            c->profileDataLength = 0;
        }

        c->profileData = ( EGLint* )( siCommonMemoryAlloc( NULL, sizeof( EGLint ) * count ) );
        if( c->profileData == NULL )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctiInitEglMemoryContext allocation failed" );
#endif  /* defined( SCT_DEBUG ) */
            
            return SCT_FALSE;
        }
        c->profileDataLength = count;        
    }

    c->profileDataCount = count;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctiInitEglMemoryContext end" );
#endif  /* defined( SCT_DEBUG ) */
    
    return SCT_TRUE;
}
