/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "TestHarness.h"

#include "sicommon_mock.h"
#include "sct_action.h"

#include <string.h>

#ifdef _MSC_VER
#pragma warning (disable: 4514 )
#endif

int *context = NULL;
int destroyCalled = 0;

SCTBoolean actionInitMethod( SCTAction *action, SCTLog *log )
{
    ( void )action;
    ( void )log;
    return SCT_TRUE;
}

SCTBoolean actionExecuteMethod( SCTAction *action, int frameNumber, SCTLog *log )
{
    ( void )action;
    ( void )frameNumber;
    ( void )log;
    return SCT_TRUE;
}

SCTBoolean actionTerminateMethod( SCTAction *action, SCTLog *log )
{
    ( void )action;
    ( void )log;
    return SCT_TRUE;
}

void actionDestroyMethod( SCTAction *action )
{
    ( void )action;
    destroyCalled++;
}

SCTActionResult* actionGetResultMethod( SCTAction *action, double frameTime )
{
    ( void )action;
    ( void )frameTime;
    return NULL;
}

TEST( test, action )
{
    SCTAction                   *action;
    const char                  *name           = "name";
    const char                  *type           = "type";
    const char                  *module         = "module";

    /* Test normal create and destroy. */
    action = sctCreateAction( name, type, module,
                              context,
                              actionInitMethod,
                              actionExecuteMethod,
                              actionTerminateMethod,
                              actionDestroyMethod,
                              actionGetResultMethod );

    CHECK( action != NULL );
    CHECK( strcmp( name, action->name ) == 0 );
    CHECK( strcmp( type, action->type ) == 0 );
    CHECK( strcmp( module, action->module ) == 0 );
    CHECK( context == action->context );
    CHECK( actionInitMethod == action->init );
    CHECK( actionExecuteMethod == action->execute );
    CHECK( actionTerminateMethod == action->terminate );
    CHECK( actionDestroyMethod == action->destroy );
    CHECK( actionGetResultMethod == action->getResult );

    destroyCalled = 0;
    sctDestroyAction( action );
    CHECK( destroyCalled != 0 );

    /* Test null destroy. */
    sctDestroyAction( NULL );

    /* Test NULL function pointers. */
    action = sctCreateAction( name, type, module,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL );
    CHECK( action != NULL );
    sctDestroyAction( action );


    /* Check behavior when memory allocation fails. */
    tsiCommonSetAllocFailThreshold( 0, -1 );
    action = sctCreateAction( name, type, module,
                              context,
                              actionInitMethod,
                              actionExecuteMethod,
                              actionTerminateMethod,
                              actionDestroyMethod,
                              actionGetResultMethod );
    CHECK( action == NULL );
    tsiCommonSetAllocFailThreshold( -1, -1 );
}

TEST( test, actionList )
{
    SCTAction                   *action1;
    SCTAction                   *action2;
    SCTActionList               *list;
    SCTActionListIterator       iterator;

    action1 = sctCreateAction( "name", "type", "module",
                               context,
                               actionInitMethod,
                               actionExecuteMethod,
                               actionTerminateMethod,
                               actionDestroyMethod,
                               actionGetResultMethod );
    action2 = sctCreateAction( "name", "type", "module",
                               context,
                               actionInitMethod,
                               actionExecuteMethod,
                               actionTerminateMethod,
                               actionDestroyMethod,
                               actionGetResultMethod );

    /* Test normal create and destroy. */
    list = sctCreateActionList();
    CHECK( list != NULL );
    CHECK( action1 != NULL );
    CHECK( action2 != NULL );

    sctDestroyActionList( list );

    /* Test null destroy. */
    sctDestroyActionList( NULL );

    /* Test normal operation. */
    list = sctCreateActionList();
    CHECK( list != NULL );

    CHECK( sctAddActionToList( list, action1 ) == SCT_TRUE );
    CHECK( sctAddActionToList( list, action2 ) == SCT_TRUE );

    sctGetActionListIterator( list, &iterator );

    CHECK( sctGetNextActionListElement( &iterator ) == action1 );
    CHECK( sctGetNextActionListElement( &iterator ) == action2 );
    CHECK( sctGetNextActionListElement( &iterator ) == NULL );
    CHECK( sctGetNextActionListElement( &iterator ) == NULL );

    /* Make sure all action destroy methods are properly called. */
    destroyCalled = 0;
    sctDestroyActionList( list );
    CHECK( destroyCalled == 2 );

    /* Check behavior when memory allocation fails. */
    tsiCommonSetAllocFailThreshold( 0, -1 );
    list = sctCreateActionList();
    CHECK( list == NULL );

    tsiCommonSetAllocFailThreshold( 1, -1 );
    list = sctCreateActionList();
    CHECK( list == NULL );


    tsiCommonSetAllocFailThreshold( -1, -1 );
    list = sctCreateActionList();

    action1 = sctCreateAction( "name", "type", "module",
                               context,
                               actionInitMethod,
                               actionExecuteMethod,
                               actionTerminateMethod,
                               actionDestroyMethod,
                               actionGetResultMethod );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctAddActionToList( list, action1 ) == SCT_FALSE );

    tsiCommonSetAllocFailThreshold( -1, -1 );
    CHECK( sctAddActionToList( list, action1 ) == SCT_TRUE );

    tsiCommonSetAllocFailThreshold( -1, -1 );

    sctDestroyActionList( list );
}

int main( void )
{
 	TestResult tr;

    tsiCommonInitSystem();

	int s = !TestRegistry::runAllTests(tr);

    tsiCommonDestroySystem();

    return s;
}
