/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "TestHarness.h"

#include "sicommon_mock.h"
#include "sct_module.h"

#include <string.h>

#ifdef _MSC_VER
#pragma warning (disable: 4514 )
#endif

int *context      = NULL;
int destroyCalled = 0;


SCTAttributeList* moduleInfoMethod( SCTModule *module )
{
    ( void )module;
    return NULL;
}

SCTAction* moduleCreateActionMethod( SCTModule *module, const char *name, const char *type, SCTAttributeList *attributes )
{
    ( void )module;
    ( void )name;
    ( void )type;
    ( void )attributes;
//    ( void )log;
    return NULL;
}


void moduleDestroyMethod( SCTModule *module )
{
    ( void )module;
    destroyCalled++;
}


TEST( test, module )
{
    SCTModule                   *module;
    const char                  *name           = "name";

    /* Test normal create and destroy. */
    module = sctCreateModule( name,
                              context,
                              "",
                              moduleInfoMethod,
                              moduleCreateActionMethod,
                              moduleDestroyMethod );
    CHECK( module != NULL );
    CHECK( strcmp( name, module->name ) == 0 );
    CHECK( context == module->context );
    CHECK( moduleInfoMethod == module->info );
    CHECK( moduleCreateActionMethod == module->createAction );
    CHECK( moduleDestroyMethod == module->destroy );

    destroyCalled = 0;
    sctDestroyModule( module );
    CHECK( destroyCalled == 1 );
    sctDestroyModule( NULL );

    /* Test null function pointers. */
    module = sctCreateModule( name,
                              context,
                              "",
                              NULL,
                              NULL,
                              NULL );
    CHECK( module != NULL );
    sctDestroyModule( module );

    /* Check behavior when memory allocation fails. */
    tsiCommonSetAllocFailThreshold( 0, -1 );
    module = sctCreateModule( name,
                              context,
                              "",
                              moduleInfoMethod,
                              moduleCreateActionMethod,
                              moduleDestroyMethod );

    CHECK( module == NULL );
    tsiCommonSetAllocFailThreshold( -1, -1 );

    /* Test very long module name. */
    module = sctCreateModule( "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789",
                              context,
                              "",
                              NULL,
                              NULL,
                              NULL );
    CHECK( module != NULL );
    sctDestroyModule( module );

}


TEST( test, moduleList )
{
    SCTModule                   *module1;
    SCTModule                   *module2;
    SCTModuleList               *list;
    SCTModuleListIterator       iterator;

    module1 = sctCreateModule( "name",
                               context,
                               "",
                               moduleInfoMethod,
                               moduleCreateActionMethod,
                               moduleDestroyMethod );
    module2 = sctCreateModule( "name",
                               context,
                               "",
                               moduleInfoMethod,
                               moduleCreateActionMethod,
                               moduleDestroyMethod );

    CHECK( module1 != NULL );
    CHECK( module2 != NULL );

    /* Test create and empty list destroy. */
    list = sctCreateModuleList();
    CHECK( list != NULL );
    sctDestroyModuleList( list );

    /* Test null destroy. */
    sctDestroyModuleList( NULL );

    /* Test normal operation. */
    list = sctCreateModuleList();
    CHECK( list != NULL );

    CHECK( sctAddModuleToList( list, module1 ) == SCT_TRUE );
    CHECK( sctAddModuleToList( list, module2 ) == SCT_TRUE );
    
    sctGetModuleListIterator( list, &iterator );

    CHECK( sctGetNextModuleListElement( &iterator ) == module1 );
    CHECK( sctGetNextModuleListElement( &iterator ) == module2 );
    CHECK( sctGetNextModuleListElement( &iterator ) == NULL );
    CHECK( sctGetNextModuleListElement( &iterator ) == NULL ); 

    destroyCalled = 0;
    sctDestroyModuleList( list );
    CHECK( destroyCalled == 2 );

    /* Check behavior when memory allocation fails. */
    tsiCommonSetAllocFailThreshold( 0, -1 );
    list = sctCreateModuleList();
    CHECK( list == NULL );

    tsiCommonSetAllocFailThreshold( 1, -1 );
    list = sctCreateModuleList();
    CHECK( list == NULL );

    tsiCommonSetAllocFailThreshold( -1, -1 );

    module1 = sctCreateModule( "name",
                               context,
                               "",
                               moduleInfoMethod,
                               moduleCreateActionMethod,
                               moduleDestroyMethod );
    list = sctCreateModuleList();
    CHECK( module1 != NULL );
    CHECK( list != NULL );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctAddModuleToList( list, module1 ) == SCT_FALSE );

    tsiCommonSetAllocFailThreshold( -1, -1 );

    CHECK( sctAddModuleToList( list, module1 ) == SCT_TRUE );
    sctDestroyModuleList( list );
}


int main( void )
{
 	TestResult tr;

    tsiCommonInitSystem();

	int s = !TestRegistry::runAllTests(tr);

    tsiCommonDestroySystem();

    return s;
}
