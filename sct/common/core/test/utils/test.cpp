/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "TestHarness.h"

#include "sct_utils.h"
#include "sicommon_mock.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifdef _MSC_VER
/* Disable warnings about unreferenced inline functions (4514). */
#pragma warning(disable : 4514)
#pragma warning(disable : 4127)
#endif

////////////////////////////////////////////////////////////////////////////////
// Utility functions.
SCTAttributeList *_attribArrayToList( SCTAttribute *array, int size )
{
    SCTAttributeList *list = sctCreateAttributeList();

    assert( list != NULL );
    for( int i = 0; i < size; i++ )
    {
        // Add new attributes, not references. This way the list destroy won't
        // blow up later.
        sctAddNameValueToList( list, array[i].name, array[i].value );
    }
    return list;
}

#define ATTRIB_ARRAY_TO_LIST( array ) _attribArrayToList( array, SCT_ARRAY_LENGTH( array ) )

////////////////////////////////////////////////////////////////////////////////
// Test cases.
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
TEST( Success, FixedPoint )
{
    CHECK_EQUAL( ( int )0x00000000, SCT_FLOAT_TO_FIXED( 0.0f ) );
    CHECK_EQUAL( ( int )0x00010000, SCT_FLOAT_TO_FIXED( 1.0f ) );
    CHECK_EQUAL( ( int )0xFFFF0000, SCT_FLOAT_TO_FIXED( -1.0f ) );
}

////////////////////////////////////////////////////////////////////////////////
TEST( test, Attribute )
{
    SCTAttribute *a;
    char *name = "name";
    char *value = "value";

    tsiCommonResetSystem();

    a = sctCreateAttribute( name, value );
    CHECK( a != NULL );

    CHECK( strcmp( name, a->name ) == 0 );
    CHECK( strcmp( value, a->value ) == 0 );

    sctDestroyAttribute( a );
    sctDestroyAttribute( NULL );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    a = sctCreateAttribute( name, value );
    CHECK( a == NULL );

    tsiCommonSetAllocFailThreshold( 1, -1 );
    a = sctCreateAttribute( name, value );
    CHECK( a == NULL );

    tsiCommonSetAllocFailThreshold( 2, -1 );
    a = sctCreateAttribute( name, value );
    CHECK( a == NULL );

}

////////////////////////////////////////////////////////////////////////////////
TEST( test, List )
{
    SCTList                     *list;
    int                         element1;
    int                         element2;
    int                         element3;    
    SCTListIterator             iterator1;
    SCTListIterator             iterator2;
    SCTReverseListIterator      reverseIterator;
    
    tsiCommonResetSystem();

    list = sctCreateList();
    CHECK( list != NULL );
    CHECK( list->head == NULL );
    CHECK( list->tail == NULL );

    sctDestroyList( list );
    sctDestroyList( NULL );

    list = sctCreateList();
    CHECK( list != NULL );

    sctGetListIterator( list, &iterator1 );
    CHECK( iterator1.next == NULL );

    sctGetReverseListIterator( list, &reverseIterator );
    CHECK( reverseIterator.prev == NULL );

    CHECK( sctAddToList( list, &element1 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element2 ) == SCT_TRUE );

    CHECK( sctRemoveFromList( list, &element1 ) == SCT_TRUE );
    CHECK( sctRemoveFromList( list, &element1 ) == SCT_FALSE );
    
    sctGetListIterator( list, &iterator1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element2 );

    CHECK( sctRemoveFromList( list, &element2 ) == SCT_TRUE );

    sctGetListIterator( list, &iterator1 );
    CHECK( sctGetNextListElement( &iterator1 ) == NULL );
    
    CHECK( sctAddToList( list, &element1 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element2 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element3 ) == SCT_TRUE );

    CHECK( sctRemoveFromList( list, &element1 ) == SCT_TRUE );

    sctGetListIterator( list, &iterator1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element2 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element3 );
    CHECK( sctGetNextListElement( &iterator1 ) == NULL );        

    CHECK( sctRemoveFromList( list, &element2 ) == SCT_TRUE );
    CHECK( sctRemoveFromList( list, &element3 ) == SCT_TRUE );    
    
    CHECK( sctAddToList( list, &element1 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element2 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element3 ) == SCT_TRUE );

    CHECK( sctRemoveFromList( list, &element2 ) == SCT_TRUE );

    sctGetListIterator( list, &iterator1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element3 );
    CHECK( sctGetNextListElement( &iterator1 ) == NULL );        

    CHECK( sctRemoveFromList( list, &element1 ) == SCT_TRUE );
    CHECK( sctRemoveFromList( list, &element3 ) == SCT_TRUE );    

    CHECK( sctAddToList( list, &element1 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element2 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element3 ) == SCT_TRUE );

    CHECK( sctRemoveFromList( list, &element3 ) == SCT_TRUE );
    
    sctGetListIterator( list, &iterator1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element2 );
    CHECK( sctGetNextListElement( &iterator1 ) == NULL );        

    CHECK( sctRemoveFromList( list, &element1 ) == SCT_TRUE );
    CHECK( sctRemoveFromList( list, &element2 ) == SCT_TRUE );    
    
    CHECK( sctAddToList( list, &element1 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element2 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element2 ) == SCT_TRUE );    

    CHECK( sctRemoveFromList( list, &element2 ) == SCT_TRUE );
    
    CHECK( sctAddToList( list, &element2 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element3 ) == SCT_TRUE );    
    sctGetListIterator( list, &iterator1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element2 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element2 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element3 );
    CHECK( sctGetNextListElement( &iterator1 ) == NULL );            
    
    CHECK( sctRemoveFromList( list, &element2 ) == SCT_TRUE );
    CHECK( sctRemoveFromList( list, &element2 ) == SCT_TRUE );
    CHECK( sctRemoveFromList( list, &element2 ) == SCT_FALSE );
    CHECK( sctRemoveFromList( list, &element1 ) == SCT_TRUE );            

    sctGetListIterator( list, &iterator1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element3 );
    CHECK( sctRemoveFromList( list, &element3 ) == SCT_TRUE );
    CHECK( sctRemoveFromList( list, &element3 ) == SCT_FALSE );        

    CHECK( sctAddToList( list, &element1 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element2 ) == SCT_TRUE );

    CHECK( sctRemoveFromList( list, &element2 ) == SCT_TRUE );
    CHECK( sctRemoveFromList( list, &element2 ) == SCT_FALSE );
    CHECK( sctAddToList( list, &element3 ) == SCT_TRUE );    
    sctGetListIterator( list, &iterator1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element3 );    
    CHECK( sctGetNextListElement( &iterator1 ) == NULL );    

    CHECK( sctRemoveFromList( list, &element1 ) == SCT_TRUE );
    CHECK( sctRemoveFromList( list, &element3 ) == SCT_TRUE );    
    
    CHECK( sctAddToList( list, &element1 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &element2 ) == SCT_TRUE );
    
    sctGetListIterator( list, &iterator1 );
    sctGetListIterator( list, &iterator2 );
    sctGetReverseListIterator( list, &reverseIterator );

    CHECK( sctGetNextListElement( &iterator1 ) == &element1 );
    CHECK( sctGetNextListElement( &iterator1 ) == &element2 );
    CHECK( sctGetNextListElement( &iterator1 ) == NULL );
    CHECK( sctGetNextListElement( &iterator1 ) == NULL );

    CHECK( sctGetNextListElement( &iterator2 ) == &element1 );
    CHECK( sctGetNextListElement( &iterator2 ) == &element2 );
    CHECK( sctGetNextListElement( &iterator2 ) == NULL );
    CHECK( sctGetNextListElement( &iterator2 ) == NULL );

    CHECK( sctGetPrevListElement( &reverseIterator ) == &element2 );
    CHECK( sctGetPrevListElement( &reverseIterator ) == &element1 );
    CHECK( sctGetPrevListElement( &reverseIterator ) == NULL );
    CHECK( sctGetPrevListElement( &reverseIterator ) == NULL );

    sctDestroyList( list );

    /* Test behavior when memory allocation fails. */
    tsiCommonSetAllocFailThreshold( 0, -1 );
    list = sctCreateList();
    CHECK( list == NULL );

    tsiCommonSetAllocFailThreshold( -1, -1 );

    list = sctCreateList();
    CHECK( list != NULL );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctAddToList( list, &element1 ) == SCT_FALSE );

    tsiCommonSetAllocFailThreshold( -1, -1 );
    CHECK( sctAddToList( list, &element1 ) == SCT_TRUE );

    sctDestroyList( list );
}

////////////////////////////////////////////////////////////////////////////////
TEST( normal_operation, sctCountListElements )
{
    SCTList    *list;
    int         elem1, elem2, elem3;

    tsiCommonResetSystem();
    
    list = sctCreateList();
    CHECK( list != NULL );

    CHECK( sctCountListElements( list ) == 0 );
    CHECK( sctAddToList( list, &elem1 ) == SCT_TRUE );
    CHECK( sctCountListElements( list ) == 1 );
    CHECK( sctAddToList( list, &elem2 ) == SCT_TRUE );
    CHECK( sctAddToList( list, &elem3 ) == SCT_TRUE );
    CHECK( sctCountListElements( list ) == 3 );
    // Add some checks if element removal is implemented.

    sctDestroyList( list );

}

////////////////////////////////////////////////////////////////////////////////
TEST( test, AttributeList )
{
    SCTAttributeList                    *list;
    SCTAttribute                        *attribute1;
    SCTAttribute                        *attribute2;
    SCTAttribute                        *attribute3;
    const char                          *name           = "name";
    const char                          *value          = "value";
    SCTAttributeListIterator            iterator1;
    SCTAttributeListIterator            iterator2;
    
    tsiCommonResetSystem();

    list = sctCreateAttributeList();
    CHECK( list != NULL );

    sctDestroyAttributeList( list );
    sctDestroyAttributeList( NULL );

    list = sctCreateAttributeList();
    CHECK( list != NULL );
    
    attribute1 = sctCreateAttribute( name, value );
    attribute2 = sctCreateAttribute( name, value );
    CHECK( attribute1 != NULL );
    CHECK( attribute2 != NULL );

    CHECK( sctAddAttributeToList( list, attribute1 ) == SCT_TRUE );
    CHECK( sctAddAttributeToList( list, attribute2 ) == SCT_TRUE );
    CHECK( sctAddNameValueToList( list, name, value ) == SCT_TRUE );

    sctGetAttributeListIterator( list, &iterator1 );
    sctGetAttributeListIterator( list, &iterator2 );

    CHECK( sctGetNextAttributeListElement( &iterator1 ) == attribute1 );
    CHECK( sctGetNextAttributeListElement( &iterator1 ) == attribute2 );
    attribute3 = sctGetNextAttributeListElement( &iterator1 );
    CHECK( attribute3 != NULL );
    CHECK( sctGetNextAttributeListElement( &iterator1 ) == NULL );

    CHECK( sctGetNextAttributeListElement( &iterator2 ) == attribute1 );
    CHECK( sctGetNextAttributeListElement( &iterator2 ) == attribute2 );
    CHECK( sctGetNextAttributeListElement( &iterator2 ) == attribute3 );
    CHECK( sctGetNextAttributeListElement( &iterator2 ) == NULL );

    CHECK( strcmp( attribute3->name, name ) == 0 );
    CHECK( strcmp( attribute3->value, value ) == 0 );

    sctDestroyAttributeList( list );


    /* Test behavior when memory allocation fails. */
    tsiCommonSetAllocFailThreshold( 0, -1 );
    list = sctCreateAttributeList();
    CHECK( list == NULL );

    /* Test behavior when memory allocation fails. */
    tsiCommonSetAllocFailThreshold( 1, -1 );
    list = sctCreateAttributeList();
    CHECK( list == NULL );

    tsiCommonSetAllocFailThreshold( -1, -1 );
    list = sctCreateAttributeList();
    CHECK( list != NULL );

    attribute1 = sctCreateAttribute( name, value );
    CHECK( attribute1 != NULL );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctAddAttributeToList( list, attribute1 ) == SCT_FALSE );

    CHECK( sctAddNameValueToList( list, name, value ) == SCT_FALSE );
    tsiCommonSetAllocFailThreshold( 1, -1 );
    CHECK( sctAddNameValueToList( list, name, value ) == SCT_FALSE );
    tsiCommonSetAllocFailThreshold( 2, -1 );
    CHECK( sctAddNameValueToList( list, name, value ) == SCT_FALSE );
    tsiCommonSetAllocFailThreshold( 3, -1 );
    CHECK( sctAddNameValueToList( list, name, value ) == SCT_FALSE );

    tsiCommonSetAllocFailThreshold( -1, -1 );
    CHECK( sctAddAttributeToList( list, attribute1 ) == SCT_TRUE );

    sctDestroyAttributeList( list );
}

////////////////////////////////////////////////////////////////////////////////
TEST( CreatingAndDestroying, StringList )
{
    SCTStringList           *list;
    
    tsiCommonResetSystem();

    list = sctCreateStringList();
    CHECK( list != NULL );

    sctDestroyStringList( NULL );
    sctDestroyStringList( list );

    sctDestroyStringListElements( NULL );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    list = sctCreateStringList();
    CHECK( list == NULL );

    tsiCommonSetAllocFailThreshold( 1, -1 );
    list = sctCreateStringList();
    CHECK( list == NULL );

    tsiCommonSetAllocFailThreshold( -1, -1 );
}

////////////////////////////////////////////////////////////////////////////////

// Own version of _strdup. This is needed because the memory for strings have to
// be allocated with siCommonMemoryAlloc so that sctDestroyStringList can
// deallocate them with siCommomMemoryFree (_strdup uses malloc).
char *_stringDup( const char *string )
{
    char *dup;

    dup = (char *) siCommonMemoryAlloc( NULL, strlen( string ) + 1 );
    assert( dup );
    strcpy( dup, string );
    return dup;
}

TEST( AddStringToList, StringList )
{
    SCTStringList        *list;
    char                 *string1;
    char                 *string2;
    SCTStringListIterator iterator1;

    tsiCommonResetSystem();

    string1 = _stringDup( "string1" );
    string2 = _stringDup( "string2" );

    list = sctCreateStringList();
    CHECK( list != NULL );
    
    CHECK( sctAddStringToList( list, string1 ) == SCT_TRUE );
    CHECK( sctAddStringToList( list, string2 ) == SCT_TRUE );

    sctGetStringListIterator( list, &iterator1 );

    CHECK( string1 == sctGetNextStringListElement( &iterator1 ) );
    CHECK( string2 == sctGetNextStringListElement( &iterator1 ) );
    CHECK( NULL    == sctGetNextStringListElement( &iterator1 ) );

    sctDestroyStringList( list );
}

////////////////////////////////////////////////////////////////////////////////
TEST( AddStringCopyToList, StringList )
{
    SCTStringList        *list;
    char                  string1[] = "string1";
    char                  string2[] = "string2";
    char                 *string;
    SCTStringListIterator iterator1;

    tsiCommonResetSystem();

    list = sctCreateStringList();
    CHECK( list != NULL );
    
    CHECK( sctAddStringCopyToList( list, string1 ) == SCT_TRUE );
    CHECK( sctAddStringCopyToList( list, string2 ) == SCT_TRUE );

    sctGetStringListIterator( list, &iterator1 );

    string = sctGetNextStringListElement( &iterator1 );
    CHECK( string1 != string );
    CHECK_STRINGS_EQUAL( string1, string );
    string = sctGetNextStringListElement( &iterator1 );
    CHECK( string2 != string );
    CHECK_STRINGS_EQUAL( string2, string );
    CHECK( sctGetNextStringListElement( &iterator1 ) == NULL );

    sctDestroyStringList( list );
}

////////////////////////////////////////////////////////////////////////////////
TEST( GetStringAtIndex, StringList )
{
    SCTStringList        *list;
    char                  string1[] = "string1";
    char                  string2[] = "string2";

    tsiCommonResetSystem();

    list = sctCreateStringList();
    CHECK( list != NULL );
    
    CHECK( sctAddStringCopyToList( list, string1 ) == SCT_TRUE );
    CHECK( sctAddStringCopyToList( list, string2 ) == SCT_TRUE );

    CHECK_STRINGS_EQUAL( string1, sctGetStringAtIndex( list, 0 ) );
    CHECK_STRINGS_EQUAL( string2, sctGetStringAtIndex( list, 1 ) );
    CHECK_EQUAL( NULL, sctGetStringAtIndex( list, 2 ) );
    CHECK_EQUAL( NULL, sctGetStringAtIndex( list, 3 ) );

    sctDestroyStringList( list );
}

////////////////////////////////////////////////////////////////////////////////
TEST( EmptyString, SplitString )
{
    SCTStringListIterator iterator;
    SCTStringList *list = sctSplitString( "", 0, ' ' );
    
    CHECK_EQUAL( 1, sctCountListElements( list->list ) );
    sctGetStringListIterator( list, &iterator );
    CHECK_STRINGS_EQUAL( "", sctGetNextStringListElement( &iterator ) );
    CHECK_EQUAL( NULL, sctGetNextStringListElement( &iterator ) );

    sctDestroyStringList( list );
}

////////////////////////////////////////////////////////////////////////////////
TEST( StringWithNoSeparator, SplitString )
{
    SCTStringListIterator iterator;
    SCTStringList        *list = sctSplitString( "abc", 3, ' ' );

    CHECK_EQUAL( 1, sctCountListElements( list->list ) );
    sctGetStringListIterator( list, &iterator );
    CHECK_STRINGS_EQUAL( "abc", sctGetNextStringListElement( &iterator ) );
    CHECK_EQUAL( NULL, sctGetNextStringListElement( &iterator ) );

    sctDestroyStringList( list );
}

////////////////////////////////////////////////////////////////////////////////
TEST( JustSeparator, SplitString )
{
    SCTStringListIterator iterator;
    SCTStringList        *list = sctSplitString( " ", 1, ' ' );

    CHECK_EQUAL( 2, sctCountListElements( list->list ) );
    sctGetStringListIterator( list, &iterator );
    CHECK_STRINGS_EQUAL( "", sctGetNextStringListElement( &iterator ) );
    CHECK_STRINGS_EQUAL( "", sctGetNextStringListElement( &iterator ) );
    CHECK_EQUAL( NULL, sctGetNextStringListElement( &iterator ) );

    sctDestroyStringList( list );
}

////////////////////////////////////////////////////////////////////////////////
TEST( StringWithSingleSeparator, SplitString )
{
    SCTStringListIterator iterator;
    SCTStringList        *list = sctSplitString( "a b", 3, ' ' );

    CHECK_EQUAL( 2, sctCountListElements( list->list ) );
    sctGetStringListIterator( list, &iterator );
    CHECK_STRINGS_EQUAL( "a", sctGetNextStringListElement( &iterator ) );
    CHECK_STRINGS_EQUAL( "b", sctGetNextStringListElement( &iterator ) );
    CHECK_EQUAL( NULL, sctGetNextStringListElement( &iterator ) );

    sctDestroyStringList( list );
}

////////////////////////////////////////////////////////////////////////////////
TEST( StringWithLimitedLength, SplitString )
{
    SCTStringListIterator iterator;
    SCTStringList        *list = sctSplitString( "a b c d", 3, ' ' );

    CHECK_EQUAL( 2, sctCountListElements( list->list ) );
    sctGetStringListIterator( list, &iterator );
    CHECK_STRINGS_EQUAL( "a", sctGetNextStringListElement( &iterator ) );
    CHECK_STRINGS_EQUAL( "b", sctGetNextStringListElement( &iterator ) );
    CHECK_EQUAL( NULL, sctGetNextStringListElement( &iterator ) );

    sctDestroyStringList( list );
}


////////////////////////////////////////////////////////////////////////////////
TEST( OOM, SplitString )
{
    SCTStringList       *list;
    int                 allocCount, i;

    tsiCommonResetTestMemorySystem();
    list = sctSplitString( "a b c", 3, ' ' );
    allocCount = tsiCommonGetAllocCounter();
    sctDestroyStringList( list );

    for( i = 0; i < allocCount; ++i )
    {
        tsiCommonSetAllocFailThreshold( i, -1 );
        CHECK( sctSplitString( "a b c", 3, ' ' ) == NULL );
    }

    tsiCommonSetAllocFailThreshold( -1, -1 );
}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctHasAttribute )
{
    SCTAttribute int_attributes[] = { 
        { "attribute0", "-1" },
        { "attribute1", "0" },
        { "attribute2", "-0" },
        { "attribute3", "1" },
        { "attribute4", "2" },
        { "attribute5", "3" },
        { "attribute6", "4" } };

    SCTAttributeList *attributes = _attribArrayToList( int_attributes, SCT_ARRAY_LENGTH( int_attributes ) );

    CHECK( sctHasAttribute( attributes, "attribute5" ) == SCT_TRUE );
    CHECK( sctHasAttribute( attributes, "attribute7" ) == SCT_FALSE );

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsInt )
{
    SCTAttribute int_attributes[] = { 
        { "attribute0", "-1" },
        { "attribute1", "0" },
        { "attribute2", "-0" },
        { "attribute3", "1" },
        { "attribute4", "2" },
        { "attribute5", "3" },
        { "attribute6", "4" } };

    int value = 0;
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = _attribArrayToList( int_attributes, SCT_ARRAY_LENGTH( int_attributes ) );

    for( i = 0; i < SCT_ARRAY_LENGTH( int_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );

        CHECK( sctGetAttributeAsInt( attributes, tbuffer, &value ) );
        CHECK_EQUAL( value, atoi( int_attributes[ i ].value ) );
    }
    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsHex )
{
    SCTAttribute hex_attributes[] = { 
        { "attribute0", "0x0" },
        { "attribute1", "0x1" },
        { "attribute2", "0xabcD" },
        { "attribute3", "0x12fF" },
        { "attribute4", "0xFfFfF" },
        { "attribute5", "0x777" },
        { "attribute6", "0x1234" } };

    unsigned int value = 0;
    char tbuffer[ 32 ];
    int i;
    unsigned int l;
    char *p;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( hex_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( hex_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );

        CHECK( sctGetAttributeAsHex( attributes, tbuffer, &value ) );
        l = strtoul( hex_attributes[ i ].value, &p, 16 );
        CHECK( value == l );
    }
    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsFloat )
{
    SCTAttribute float_attributes[] = { 
        { "attribute0", "-1.0" },
        { "attribute1", "0.0" },
        { "attribute2", "1.0" },
        { "attribute3", "2.0" },
        { "attribute4", "3.0" },
        { "attribute5", "4.0" } };
        
    float value = 0.0f;
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( float_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( float_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );

        CHECK( sctGetAttributeAsFloat( attributes, tbuffer, &value ) );
        CHECK_EQUAL( value, atof( float_attributes[ i ].value ) );
    }
    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsString )
{
    SCTAttribute string_attributes[] = { 
        { "attribute0", "string1" },
        { "attribute1", "string2" },
        { "attribute2", "\0" } };

    char value[ 32 ];
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( string_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( string_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsString( attributes, tbuffer, value, 32 ) );
        CHECK( strcmp( value, string_attributes[ i ].value ) == 0 );
    }

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( truncate, sctGetAttributeAsString )
{
    SCTAttribute string_attributes[] = { 
        { "attribute0", "12345truncate" } };

    char value[ 6 ];

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( string_attributes );

    CHECK( sctGetAttributeAsString( attributes, "attribute0", value, 5 ) );
    CHECK( strcmp( value, "12345" ) == 0 );

    sctDestroyAttributeList( attributes );
}


////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsStringList )
{
    SCTAttribute string_list_attributes[] = { 
        { "attribute0", "{}" },
        { "attribute1", "{string1}" },
        { "attribute2", "{string2,string3}" },
        { "attribute3", "{string4,string5,string6}" },
        { "attribute4", "\0" } };

    SCTStringList* stringList;
    SCTStringListIterator iterator;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( string_list_attributes );

    CHECK( sctGetAttributeAsStringList( attributes, "attribute0", &stringList ) );
    sctGetStringListIterator( stringList, &iterator );
    CHECK( strcmp( sctGetNextStringListElement( &iterator ), "" ) == 0 );
    sctDestroyStringList( stringList );

    CHECK( sctGetAttributeAsStringList( attributes, "attribute1", &stringList ) );
    sctGetStringListIterator( stringList, &iterator );
    CHECK( strcmp( sctGetNextStringListElement( &iterator ), "string1" ) == 0 );
    sctDestroyStringList( stringList );

    CHECK( sctGetAttributeAsStringList( attributes, "attribute2", &stringList ) );
    sctGetStringListIterator( stringList, &iterator );
    CHECK( strcmp( sctGetNextStringListElement( &iterator ), "string2" ) == 0 );
    CHECK( strcmp( sctGetNextStringListElement( &iterator ), "string3" ) == 0 );
    sctDestroyStringList( stringList );

    CHECK( sctGetAttributeAsStringList( attributes, "attribute3", &stringList ) );
    sctGetStringListIterator( stringList, &iterator );
    CHECK( strcmp( sctGetNextStringListElement( &iterator ), "string4" ) == 0 );
    CHECK( strcmp( sctGetNextStringListElement( &iterator ), "string5" ) == 0 );
    CHECK( strcmp( sctGetNextStringListElement( &iterator ), "string6" ) == 0 );
    sctDestroyStringList( stringList );

    sctDestroyAttributeList( attributes );
}


////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsXY )
{
    SCTAttribute xy_attributes[] = { 
        { "attribute0", "1*2" },
        { "attribute1", "-1*2" },
        { "attribute2", "1*-2" },
        { "attribute3", "0*0" },
        { "attribute4", "1666*8822" },
        { "attribute5", "-1111*-11111" } };
        int values[][2] = { 
            {  1,  2 },
            { -1,  2 },
            {  1, -2 },
            {  0,  0 },
            {  1666, 8822 },
            { -1111, -11111 } };

    int xvalue = 0;
    int yvalue = 0;
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( xy_attributes );
    
    for( i = 0; i < SCT_ARRAY_LENGTH( xy_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );

        CHECK( sctGetAttributeAsXY( attributes, tbuffer, &xvalue, &yvalue  ) );
        CHECK_EQUAL( xvalue, values[ i ][ 0 ] );
        CHECK_EQUAL( yvalue, values[ i ][ 1 ] );
    }
    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsFloatArray )
{
    SCTAttribute float_array_attributes[] = { 
         { "attribute0", "{-3.0,2.0}" },
         { "attribute1", "{0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7}" } };
        
    float value[] = { 0.f, 0.f };
    float value2[ 260 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( float_array_attributes );

    CHECK( sctGetAttributeAsFloatArray( attributes, "attribute0", value, 2 ) );
    CHECK_EQUAL( -3.0, value[ 0 ] );
    CHECK_EQUAL( 2.0, value[ 1 ] );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctGetAttributeAsFloatArray( attributes, "attribute1", value2, 258 ) == SCT_FALSE );
    tsiCommonSetAllocFailThreshold( -1, -1 );

    CHECK( sctGetAttributeAsFloatArray( attributes, "attribute1", value2, 258 ) );

    for( i = 0; i < 258; i++ )
    {
        CHECK_EQUAL( i % 10, value2[ i ] );
    }

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsDoubleArray )
{
    SCTAttribute double_array_attributes[] = { 
         { "attribute0", "{-3.0,2.0}" },
         { "attribute1", "{0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7}" } };
        
    double value[] = { 0.f, 0.f };
    double value2[ 260 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( double_array_attributes );

    CHECK( sctGetAttributeAsDoubleArray( attributes, "attribute0", value, 2 ) );
    CHECK_EQUAL( -3.0, value[ 0 ] );
    CHECK_EQUAL( 2.0, value[ 1 ] );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctGetAttributeAsDoubleArray( attributes, "attribute1", value2, 258 ) == SCT_FALSE );
    tsiCommonSetAllocFailThreshold( -1, -1 );

    CHECK( sctGetAttributeAsDoubleArray( attributes, "attribute1", value2, 258 ) );

    for( i = 0; i < 258; i++ )
    {
        CHECK_EQUAL( i % 10, value2[ i ] );
    }

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsIntArray )
{
    SCTAttribute int_array_attributes[] = { 
         { "attribute0", "{-3,2}" },
         { "attribute1", "{0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7}" } };
        
    int value[] = { 0, 0 };
    int value2[ 260 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( int_array_attributes );

    CHECK( sctGetAttributeAsIntArray( attributes, "attribute0", value, 2 ) );
    CHECK_EQUAL( -3, value[0] );
    CHECK_EQUAL( 2, value[1] );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctGetAttributeAsIntArray( attributes, "attribute1", value2, 258 ) == SCT_FALSE );
    tsiCommonSetAllocFailThreshold( -1, -1 );

    CHECK( sctGetAttributeAsIntArray( attributes, "attribute1", value2, 258 ) );

    for( i = 0; i < 258; i++ )
    {
        CHECK_EQUAL( i % 10, value2[ i ] );
    }

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsHexArray )
{
    SCTAttribute hex_array_attributes[] = { 
         { "attribute0", "{0x3,0x2}" },
         { "attribute1", "{0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7}" } };
        
    unsigned int value[] = { 0, 0 };
    unsigned int value2[ 260 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( hex_array_attributes );

    CHECK( sctGetAttributeAsHexArray( attributes, "attribute0", value, 2 ) );
    CHECK_EQUAL( 3, ( int )( value[0] ) );
    CHECK_EQUAL( 2, ( int )( value[1] ) );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctGetAttributeAsHexArray( attributes, "attribute1", value2, 258 ) == SCT_FALSE );
    tsiCommonSetAllocFailThreshold( -1, -1 );

    CHECK( sctGetAttributeAsHexArray( attributes, "attribute1", value2, 258 ) );

    for( i = 0; i < 258; i++ )
    {
        CHECK_EQUAL( i % 10, ( int )( value2[ i ] ) );
    }

    sctDestroyAttributeList( attributes );
}


////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsFloatVector )
{
    SCTAttribute float_array_attributes[] = { 
         { "attribute0", "{-3.0,2.0}" },
         { "attribute1", "{0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7}" } };
    SCTFloatVector *vector = NULL;
    int i;
    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( float_array_attributes );
    int ac;

    CHECK( sctGetAttributeAsFloatVector( attributes, "attribute0", &vector ) );
    CHECK( vector != NULL );
    CHECK_EQUAL( 2, vector->length );
    CHECK_EQUAL( -3.0, vector->data[ 0 ] );
    CHECK_EQUAL( 2.0, vector->data[ 1 ] );

    sctDestroyFloatVector( vector );

    tsiCommonResetAllocCounter();
    CHECK( sctGetAttributeAsFloatVector( attributes, "attribute1", &vector ) );
    ac = tsiCommonGetAllocCounter();
    sctDestroyFloatVector( vector );

    for( i = 0; i < ac; i++ )
    {
        tsiCommonSetAllocFailThreshold( i, -1 );
        CHECK( sctGetAttributeAsFloatVector( attributes, "attribute1", &vector ) == SCT_FALSE );
    }

    tsiCommonSetAllocFailThreshold( -1, -1 );

    CHECK( sctGetAttributeAsFloatVector( attributes, "attribute1", &vector ) );

    CHECK( vector != NULL );
    CHECK_EQUAL( 258, vector->length );
    for( i = 0; i < 258; i++ )
    {
        CHECK_EQUAL( i % 10, vector->data[ i ] );
    }
    sctDestroyFloatVector( vector );

    sctDestroyAttributeList( attributes );

}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsDoubleVector )
{
    SCTAttribute double_array_attributes[] = { 
         { "attribute0", "{-3.0,2.0}" },
         { "attribute1", "{0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7}" } };
    SCTDoubleVector *vector = NULL;
    int i;
    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( double_array_attributes );
    int ac;

    CHECK( sctGetAttributeAsDoubleVector( attributes, "attribute0", &vector ) );
    CHECK( vector != NULL );
    CHECK_EQUAL( 2, vector->length );
    CHECK_EQUAL( -3.0, vector->data[ 0 ] );
    CHECK_EQUAL( 2.0, vector->data[ 1 ] );

    sctDestroyDoubleVector( vector );

    tsiCommonResetAllocCounter();
    CHECK( sctGetAttributeAsDoubleVector( attributes, "attribute1", &vector ) );
    ac = tsiCommonGetAllocCounter();
    sctDestroyDoubleVector( vector );

    for( i = 0; i < ac; i++ )
    {
        tsiCommonSetAllocFailThreshold( i, -1 );
        CHECK( sctGetAttributeAsDoubleVector( attributes, "attribute1", &vector ) == SCT_FALSE );
    }

    tsiCommonSetAllocFailThreshold( -1, -1 );

    CHECK( sctGetAttributeAsDoubleVector( attributes, "attribute1", &vector ) );

    CHECK( vector != NULL );
    CHECK_EQUAL( 258, vector->length );
    for( i = 0; i < 258; i++ )
    {
        CHECK_EQUAL( i % 10, vector->data[ i ] );
    }
    sctDestroyDoubleVector( vector );

    sctDestroyAttributeList( attributes );

}


////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsIntVector )
{
    SCTAttribute int_array_attributes[] = { 
         { "attribute0", "{-3,2}" },
         { "attribute1", "{0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7}" } };
    SCTIntVector *vector = NULL;
    int i;
    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( int_array_attributes );
    int ac;

    CHECK( sctGetAttributeAsIntVector( attributes, "attribute0", &vector ) );
    CHECK( vector != NULL );
    CHECK_EQUAL( 2, vector->length );
    CHECK_EQUAL( -3, vector->data[ 0 ] );
    CHECK_EQUAL( 2, vector->data[ 1 ] );

    sctDestroyIntVector( vector );

    tsiCommonResetAllocCounter();
    CHECK( sctGetAttributeAsIntVector( attributes, "attribute1", &vector ) );
    ac = tsiCommonGetAllocCounter();
    sctDestroyIntVector( vector );

    for( i = 0; i < ac; i++ )
    {
        tsiCommonSetAllocFailThreshold( i, -1 );
        CHECK( sctGetAttributeAsIntVector( attributes, "attribute1", &vector ) == SCT_FALSE );
    }

    tsiCommonSetAllocFailThreshold( -1, -1 );

    CHECK( sctGetAttributeAsIntVector( attributes, "attribute1", &vector ) );

    CHECK( vector != NULL );
    CHECK_EQUAL( 258, vector->length );
    for( i = 0; i < 258; i++ )
    {
        CHECK_EQUAL( i % 10, vector->data[ i ] );
    }
    sctDestroyIntVector( vector );

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsHexVector )
{
    SCTAttribute hex_array_attributes[] = { 
         { "attribute0", "{0x3,0x2}" },
         { "attribute1", "{0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7}" } };
    SCTHexVector *vector = NULL;
    int i;
    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( hex_array_attributes );
    int ac;

    CHECK( sctGetAttributeAsHexVector( attributes, "attribute0", &vector ) );
    CHECK( vector != NULL );
    CHECK_EQUAL( 2, vector->length );
    CHECK_EQUAL( 3, ( int )( vector->data[ 0 ] ) );
    CHECK_EQUAL( 2, ( int )( vector->data[ 1 ] ) );

    sctDestroyHexVector( vector );

    tsiCommonResetAllocCounter();
    CHECK( sctGetAttributeAsHexVector( attributes, "attribute1", &vector ) );
    ac = tsiCommonGetAllocCounter();
    sctDestroyHexVector( vector );

    for( i = 0; i < ac; i++ )
    {
        tsiCommonSetAllocFailThreshold( i, -1 );
        CHECK( sctGetAttributeAsHexVector( attributes, "attribute1", &vector ) == SCT_FALSE );
    }

    tsiCommonSetAllocFailThreshold( -1, -1 );

    CHECK( sctGetAttributeAsHexVector( attributes, "attribute1", &vector ) );

    CHECK( vector != NULL );
    CHECK_EQUAL( 258, vector->length );
    for( i = 0; i < 258; i++ )
    {
        CHECK_EQUAL( i % 10, ( int )( vector->data[ i ] ) );
    }
    sctDestroyHexVector( vector );

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( okValues, sctGetAttributeAsCInt )
{
    SCTAttribute cint_attributes[] = { 
        { "attribute0",  "=1" },
        { "attribute1",  "<1" },
        { "attribute2",  ">1" },
        { "attribute3",  "<=1" },
        { "attribute4",  ">=1" },
        { "attribute5",  "=-1111" },
        { "attribute6",  "=0" },
        { "attribute7",  "=9" },
        { "attribute8",  "=16" },
        { "attribute9",  ">=16" },
        { "attribute10", "=24" },
        { "attribute11", ">=24" },
        { "attribute12", "-" },        
    };
    
    int values[][ 2 ] = { 
            { SCT_EQUAL,    1 },
            { SCT_LESS,     1 },
            { SCT_GREATER,  1 },
            { SCT_LEQUAL,   1 },
            { SCT_GEQUAL,   1 },
            { SCT_EQUAL,    -1111 },
            { SCT_EQUAL,    0 },
            { SCT_EQUAL,    9 },
            { SCT_EQUAL,    16 },
            { SCT_GEQUAL,   16 },
            { SCT_EQUAL,    24 },
            { SCT_GEQUAL,   24 },
            { SCT_DONTCARE, 0 },                        
    };

    SCTConditionalInt   cint;
    char                tbuffer[ 32 ];
    int                 i;

    SCTAttributeList* attributes = ATTRIB_ARRAY_TO_LIST( cint_attributes );
    
    for( i = 0; i < SCT_ARRAY_LENGTH( cint_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );

        CHECK( sctGetAttributeAsCInt( attributes, tbuffer, &cint  ) );
        CHECK_EQUAL( cint.condition, values[ i ][ 0 ] );
        CHECK_EQUAL( cint.value, values[ i ][ 1 ] );
    }
    sctDestroyAttributeList( attributes );
}


////////////////////////////////////////////////////////////////////////////////
TEST( noValues, sctGetAttributeAsFloatVector )
{
    SCTAttribute float_array_attributes[] = { 
         { "attribute0", "{}" } };
    SCTFloatVector *vector = NULL;
        
    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( float_array_attributes );

    CHECK( sctGetAttributeAsFloatVector( attributes, "attribute0", &vector ) );
    CHECK( vector != NULL );
    CHECK_EQUAL( 0, vector->length );
    CHECK( vector->data == NULL );

    sctDestroyAttributeList( attributes );
    sctDestroyFloatVector( vector );
}
////////////////////////////////////////////////////////////////////////////////
TEST( noValues, sctGetAttributeAsDoubleVector )
{
    SCTAttribute double_array_attributes[] = { 
         { "attribute0", "{}" } };
    SCTDoubleVector *vector = NULL;
        
    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( double_array_attributes );

    CHECK( sctGetAttributeAsDoubleVector( attributes, "attribute0", &vector ) );
    CHECK( vector != NULL );
    CHECK_EQUAL( 0, vector->length );
    CHECK( vector->data == NULL );

    sctDestroyAttributeList( attributes );
    sctDestroyDoubleVector( vector );
}

////////////////////////////////////////////////////////////////////////////////
TEST( noValues, sctGetAttributeAsIntVector )
{
    SCTAttribute int_array_attributes[] = { 
         { "attribute0", "{}" } };
    SCTIntVector *vector = NULL;
        
    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( int_array_attributes );

    CHECK( sctGetAttributeAsIntVector( attributes, "attribute0", &vector ) );
    CHECK( vector != NULL );
    CHECK_EQUAL( 0, vector->length );
    CHECK( vector->data == NULL );

    sctDestroyAttributeList( attributes );
    sctDestroyIntVector( vector );
}

////////////////////////////////////////////////////////////////////////////////
TEST( noValues, sctGetAttributeAsHexVector )
{
    SCTAttribute hex_array_attributes[] = { 
         { "attribute0", "{}" } };
    SCTHexVector *vector = NULL;
        
    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( hex_array_attributes );

    CHECK( sctGetAttributeAsHexVector( attributes, "attribute0", &vector ) );
    CHECK( vector != NULL );
    CHECK_EQUAL( 0, vector->length );
    CHECK( vector->data == NULL );

    sctDestroyAttributeList( attributes );
    sctDestroyHexVector( vector );
}

////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsInt )
{
   SCTAttribute int_attributes[] = { 
        { "attribute0", "foo" },
        { "attribute1", "0.9" },
        { "attribute2", "1." },
        { "attribute3", "\0" },
        { "attribute4", "--1" },
        { "attribute5", "-1." },
        { "attribute6", "1 2" },
        { "attribute7", " 1" },
        { "attribute8", "1 " },
        { "attribute9", "foo1bar" },
        { "attribute10", "" },
        { "attribute11", "+0" }, 
        { "attribute12", "-+1" }, 
        { "attribute13", "+" }, 
        { "attribute14", "-" }
   };

    int value = 0;
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = _attribArrayToList( int_attributes, SCT_ARRAY_LENGTH( int_attributes ) );

    for( i = 0; i < SCT_ARRAY_LENGTH( int_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsInt( attributes, tbuffer, &value ) == SCT_FALSE );
    }
    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsInt )
{
    SCTAttribute int_attributes[] = { 
        { "attribute0", "foo" } };

    int value = 0;

    tsiCommonResetSystem();
    
    SCTAttributeList *attributes = _attribArrayToList( int_attributes, SCT_ARRAY_LENGTH( int_attributes ) );

    CHECK( sctGetAttributeAsInt( attributes, "", &value ) == SCT_FALSE );
    CHECK( sctGetAttributeAsInt( attributes, "attribute", &value ) == SCT_FALSE );

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsHex )
{
   SCTAttribute hex_attributes[] = { 
        { "attribute0", "foo" },
        { "attribute1", "x0" },
        { "attribute2", "0x01g" },
        { "attribute3", "-0x1" },
        { "attribute4", "0x" },
        { "attribute5", " 0x1" },
        { "attribute6", "1 2" },
        { "attribute7", "0 x 1" },
        { "attribute8", "0 x1" },
        { "attribute9", "0x 1" },
        { "attribute10", "" },
        { "attribute11", "xx0" }, 
        { "attribute12", "0xx1" }, 
        { "attribute13", "0x1-" }, 
        { "attribute14", "0x." } };

    unsigned int value = 0;
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( hex_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( hex_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsHex( attributes, tbuffer, &value ) == SCT_FALSE );
    }
    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsStringList )
{
    SCTAttribute string_list_attributes[] = { 
        { "attribute0", "{" },
        { "attribute1", "}" },
        { "attribute2", "{string2," },
        { "attribute3", "string4,string5,string6}" },
        { "attribute4", "s{tring4,string5,string6}" },
        { "attribute5", "\0" } };

    SCTStringList* stringList;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( string_list_attributes );

    CHECK( sctGetAttributeAsStringList( attributes, "attribute0", &stringList ) == SCT_FALSE );
    CHECK( sctGetAttributeAsStringList( attributes, "attribute1", &stringList ) == SCT_FALSE );
    CHECK( sctGetAttributeAsStringList( attributes, "attribute2", &stringList ) == SCT_FALSE );
    CHECK( sctGetAttributeAsStringList( attributes, "attribute3", &stringList ) == SCT_FALSE );
    CHECK( sctGetAttributeAsStringList( attributes, "attribute4", &stringList ) == SCT_FALSE );
    CHECK( sctGetAttributeAsStringList( attributes, "attribute5", &stringList ) == SCT_FALSE );

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsStringList )
{
    SCTAttribute string_list_attributes[] = { 
        { "attribute0", "{}" } };

    SCTStringList* stringList;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( string_list_attributes );

    CHECK( sctGetAttributeAsStringList( attributes, "", &stringList ) == SCT_FALSE );
    CHECK( sctGetAttributeAsStringList( attributes, "attribute", &stringList ) == SCT_FALSE );

    sctDestroyAttributeList( attributes );
}


////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsHex )
{
   SCTAttribute hex_attributes[] = { 
        { "attribute0", "foo" } };

    unsigned int value = 0;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( hex_attributes );

    CHECK( sctGetAttributeAsHex( attributes, "", &value ) == SCT_FALSE );
    CHECK( sctGetAttributeAsHex( attributes, "attribute", &value ) == SCT_FALSE );

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsFloat )
{
    SCTAttribute float_attributes[] = { 
        { "attribute0", "foo" },
        { "attribute1", "\0" },
        { "attribute2", "f0.1oo" },
        { "attribute3", "++0.1" },
        { "attribute4", "1.0.0" },
        { "attribute5", "1.0s" },
        { "attribute6", "1.0 1.0" },
        { "attribute7", " 1.0" },
        { "attribute8", "1.0 " },
        { "attribute9", "hello" },
        { "attribute10", "" },
        { "attribute11", "+" },
        { "attribute12", "-" },
        { "attribute13", "a.0" },
 };

    float value = 0.0f;
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( float_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( float_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsFloat( attributes, tbuffer, &value ) == SCT_FALSE );
    }

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsFloatArray )
{
    SCTAttribute float_array_attributes[] = { 
        { "attribute0",  "-3.0,2.0}" },
        { "attribute1",  "{-3.0,2.0" },
        { "attribute2",  "{-3.0 2.0}" },
        { "attribute3",  "{-3.0,2.0,}" },
        { "attribute4",  "{a,2.0,}" },
        { "attribute5",  "{2,b,}" },
        { "attribute6",  " {-3.0,2.0}" },
        { "attribute7",  "{-3.0,2.0} " },
        { "attribute8",  "{ -3.0,2.0}" },
        { "attribute9",  "{-3.0 ,2.0}" },
        { "attribute10", "{-3.0, 2.0}" },
        { "attribute11", "{-3.0,2.0 }" },
        { "attribute12", "{-3.0,2.0,1.0}" },
        { "attribute13", "{-3.0}" },
        { "attribute14", "{-3.0,}" },
        { "attribute15", "{}" },
        { "attribute16", "{,}" },
        { "attribute17", "{a,1}" },
        { "attribute18", "{1,b}" } };
        
    float value[] = { 0.f, 0.f };
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( float_array_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( float_array_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsFloatArray( attributes, tbuffer, value, 2 ) == SCT_FALSE );
    }
    sctDestroyAttributeList( attributes );
}
////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsDoubleArray )
{
    SCTAttribute double_array_attributes[] = { 
        { "attribute0",  "-3.0,2.0}" },
        { "attribute1",  "{-3.0,2.0" },
        { "attribute2",  "{-3.0 2.0}" },
        { "attribute3",  "{-3.0,2.0,}" },
        { "attribute4",  "{a,2.0,}" },
        { "attribute5",  "{2,b,}" },
        { "attribute6",  " {-3.0,2.0}" },
        { "attribute7",  "{-3.0,2.0} " },
        { "attribute8",  "{ -3.0,2.0}" },
        { "attribute9",  "{-3.0 ,2.0}" },
        { "attribute10", "{-3.0, 2.0}" },
        { "attribute11", "{-3.0,2.0 }" },
        { "attribute12", "{-3.0,2.0,1.0}" },
        { "attribute13", "{-3.0}" },
        { "attribute14", "{-3.0,}" },
        { "attribute15", "{}" },
        { "attribute16", "{,}" },
        { "attribute17", "{a,1}" },
        { "attribute18", "{1,b}" } };
        
    double value[] = { 0.f, 0.f };
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( double_array_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( double_array_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsDoubleArray( attributes, tbuffer, value, 2 ) == SCT_FALSE );
    }
    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsIntArray )
{
    SCTAttribute int_array_attributes[] = { 
        { "attribute0",  "-3,2" },
        { "attribute1",  "{-3,2" },
        { "attribute2",  "{-3 2}" },
        { "attribute3",  "{-3,2,}" },
        { "attribute4",  "{a,2,}" },
        { "attribute5",  "{2,b,}" },
        { "attribute6",  " {-3,2}" },
        { "attribute7",  "{-3,2} " },
        { "attribute8",  "{ -3,2}" },
        { "attribute9",  "{-3 ,2}" },
        { "attribute10", "{-3, 2}" },
        { "attribute11", "{-3,2 }" },
        { "attribute12", "{-3,2,1}" },
        { "attribute13", "{-3}" },
        { "attribute14", "{-3,}" },
        { "attribute15", "{}" },
        { "attribute16", "{,}" },
        { "attribute17", "{a,1}" },
        { "attribute18", "{1,b}" } };
        
    int value[] = { 0, 0 };
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( int_array_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( int_array_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsIntArray( attributes, tbuffer, value, 2 ) == SCT_FALSE );
    }
    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsHexArray )
{
    SCTAttribute hex_array_attributes[] = { 
        { "attribute0",  "{3,2}" },
        { "attribute1",  "{0x3,0x2" },
        { "attribute2",  "{0x3 0x2}" },
        { "attribute3",  "{0x3,0x2,}" },
        { "attribute4",  "{0xg,0x2,}" },
        { "attribute5",  "{0x2,0x/,}" },
        { "attribute6",  " {0x3,0x2}" },
        { "attribute7",  "{0x3,0x2} " },
        { "attribute8",  "{ 0x3,0x2}" },
        { "attribute9",  "{0x3 ,0x2}" },
        { "attribute10", "{0x3, 0x2}" },
        { "attribute11", "{0x3,0x2 }" },
        { "attribute12", "{0x3,0x2,0x1}" },
        { "attribute13", "{0x3}" },
        { "attribute14", "{0x3,}" },
        { "attribute15", "{}" },
        { "attribute16", "{,}" },
        { "attribute17", "{0xG,0x1}" },
        { "attribute18", "{0x1,0xx}" } };
        
    unsigned int value[] = { 0, 0 };
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( hex_array_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( hex_array_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsHexArray( attributes, tbuffer, value, 2 ) == SCT_FALSE );
    }
    sctDestroyAttributeList( attributes );
}


////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsFloatVector )
{
    SCTAttribute float_array_attributes[] = { 
        { "attribute0",  "-3.0,2.0}" },
        { "attribute1",  "{-3.0,2.0" },
        { "attribute2",  "{-3.0 2.0}" },
        { "attribute3",  "{-3.0,2.0,}" },
        { "attribute4",  "{a,2.0,}" },
        { "attribute5",  "{2,b,}" },
        { "attribute6",  " {-3.0,2.0}" },
        { "attribute7",  "{-3.0,2.0} " },
        { "attribute8",  "{ -3.0,2.0}" },
        { "attribute9",  "{-3.0 ,2.0}" },
        { "attribute10", "{-3.0, 2.0}" },
        { "attribute11", "{-3.0,2.0 }" },
        { "attribute12", "{-3.0,}" },
        { "attribute13", "{,}" },
        { "attribute14", "" } };
        
    SCTFloatVector *vector = NULL;
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( float_array_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( float_array_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsFloatVector( attributes, tbuffer, &vector ) == SCT_FALSE );
        CHECK( vector == NULL );
    }
    sctDestroyAttributeList( attributes );
}
////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsDoubleVector )
{
    SCTAttribute double_array_attributes[] = { 
        { "attribute0",  "-3.0,2.0}" },
        { "attribute1",  "{-3.0,2.0" },
        { "attribute2",  "{-3.0 2.0}" },
        { "attribute3",  "{-3.0,2.0,}" },
        { "attribute4",  "{a,2.0,}" },
        { "attribute5",  "{2,b,}" },
        { "attribute6",  " {-3.0,2.0}" },
        { "attribute7",  "{-3.0,2.0} " },
        { "attribute8",  "{ -3.0,2.0}" },
        { "attribute9",  "{-3.0 ,2.0}" },
        { "attribute10", "{-3.0, 2.0}" },
        { "attribute11", "{-3.0,2.0 }" },
        { "attribute12", "{-3.0,}" },
        { "attribute13", "{,}" },
        { "attribute14", "" } };
        
    SCTDoubleVector *vector = NULL;
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( double_array_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( double_array_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsDoubleVector( attributes, tbuffer, &vector ) == SCT_FALSE );
        CHECK( vector == NULL );
    }
    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsIntVector )
{
    SCTAttribute int_array_attributes[] = { 
        { "attribute0",  "-3,2}" },
        { "attribute1",  "{-3,2" },
        { "attribute2",  "{-3 2}" },
        { "attribute3",  "{-3,2,}" },
        { "attribute4",  "{a,2,}" },
        { "attribute5",  "{2,b,}" },
        { "attribute6",  " {-3,2}" },
        { "attribute7",  "{-3,2} " },
        { "attribute8",  "{ -3,2}" },
        { "attribute9",  "{-3 ,2}" },
        { "attribute10", "{-3, 2}" },
        { "attribute11", "{-3,2 }" },
        { "attribute12", "{-3,}" },
        { "attribute13", "{,}" },
        { "attribute14", "" } };
        
    SCTIntVector *vector = NULL;
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( int_array_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( int_array_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsIntVector( attributes, tbuffer, &vector ) == SCT_FALSE );
        CHECK( vector == NULL );
    }
    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsHexVector )
{
    SCTAttribute hex_array_attributes[] = { 
        { "attribute0",  "0x3,0x2}" },
        { "attribute1",  "{0x3,0x2" },
        { "attribute2",  "{0x3 0x2}" },
        { "attribute3",  "{0x3,0x2,}" },
        { "attribute4",  "{0xg,0x2,}" },
        { "attribute5",  "{0x2,0xg,}" },
        { "attribute6",  " {0x3,0x2}" },
        { "attribute7",  "{0x3,0x2} " },
        { "attribute8",  "{ 0x3,0x2}" },
        { "attribute9",  "{0x3 ,0x2}" },
        { "attribute10", "{0x3, 0x2}" },
        { "attribute11", "{0x3,0x2 }" },
        { "attribute12", "{0x3,}" },
        { "attribute13", "{,}" },
        { "attribute14", "" } };
        
    SCTHexVector *vector = NULL;
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( hex_array_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( hex_array_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsHexVector( attributes, tbuffer, &vector ) == SCT_FALSE );
        CHECK( vector == NULL );
    }
    sctDestroyAttributeList( attributes );
}


////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsXY )
{
    SCTAttribute xy_attributes[] = { 
        { "attribute0", "foo" },
        { "attribute1", "12345678901234567890*1234567890123456789" } };

    int xvalue = 0;
    int yvalue = 0;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( xy_attributes );

    CHECK( sctGetAttributeAsXY( attributes, "", &xvalue, &yvalue ) == SCT_FALSE );
    CHECK( sctGetAttributeAsXY( attributes, "attribute", &xvalue, &yvalue ) == SCT_FALSE );
    CHECK( sctGetAttributeAsXY( attributes, "attribute0", &xvalue, &yvalue ) == SCT_FALSE );
    CHECK( sctGetAttributeAsXY( attributes, "attribute1", &xvalue, &yvalue ) == SCT_FALSE );

    sctDestroyAttributeList( attributes );
}


////////////////////////////////////////////////////////////////////////////////
TEST( invalidValues, sctGetAttributeAsXY )
{
    SCTAttribute xy_attributes[] = { 
        { "attribute0", "*2" },
        { "attribute1", "1*" },
        { "attribute2", "+1*2" },
        { "attribute3", "1**2" },
        { "attribute4", "1666" },
        { "attribute5", "1*--2" },
        { "attribute6", "1* 2" },    
        { "attribute7", " 1*2" },
        { "attribute8", " 1*" }
    };

    int xvalue = 0;
    int yvalue = 0;
    char tbuffer[ 32 ];
    int i;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( xy_attributes );

    for( i = 0; i < SCT_ARRAY_LENGTH( xy_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsXY( attributes, tbuffer, &xvalue, &yvalue ) == SCT_FALSE );
    }
    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( InvalidValues, sctGetAttributeAsCInt )
{
    SCTAttribute cint_attributes[] = { 
        { "attribute0",  "foo" },
        { "attribute1",  "12345678901234567890*1234567890123456789" },
        { "attribute2",  "=12345678901234567890*1234567890123456789" },        
        { "attribute3",  "" },        
        { "attribute4",  "=" },
        { "attribute5",  "=-" },
        { "attribute6",  "=a" },
        { "attribute7",  "=-a" },
        { "attribute8",  "<<<1" },
        { "attribute9",  "==1" },
        { "attribute10", ">= 1" },
        { "attribute11", ">= 1" },
        { "attribute12", "> 1" },
        { "attribute12", "= 1" },
        { "attribute13", "<" },
        { "attribute14", ">" },
        { "attribute15", ">=" },
        { "attribute16", "<=" },
        { "attribute17", "<<1" },
        { "attribute18", ">>1" },
        { "attribute19", "-a" },
        { "attribute20", "- " },
        { "attribute21", "-<6" },                        
    };

    SCTConditionalInt   cint;
    int                 i;
    char                tbuffer[ 32 ];
    
    SCTAttributeList* attributes = ATTRIB_ARRAY_TO_LIST( cint_attributes );

    CHECK( sctGetAttributeAsCInt( attributes, "", &cint ) == SCT_FALSE );
    CHECK( sctGetAttributeAsCInt( attributes, "attribute", &cint ) == SCT_FALSE );
    
    for( i = 2; i < SCT_ARRAY_LENGTH( cint_attributes ); i++ )
    {
        sprintf( tbuffer, "attribute%d", i );
        CHECK( sctGetAttributeAsCInt( attributes, tbuffer, &cint ) == SCT_FALSE );
    }
    sctDestroyAttributeList( attributes );    
}

////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsFloat )
{
    SCTAttribute float_attributes[] = { 
        { "attribute0", "foo" } };

    float value = 0.0f;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( float_attributes );

    CHECK( sctGetAttributeAsFloat( attributes, "", &value ) == SCT_FALSE );
    CHECK( sctGetAttributeAsFloat( attributes, "attribute", &value ) == SCT_FALSE );

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsFloatArray )
{
    SCTAttribute float_array_attributes[] = { 
        { "attribute0", "{1.0,2.0}" } };

    float value[] = {0.0f, 0.0f};

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( float_array_attributes );

    CHECK( sctGetAttributeAsFloatArray( attributes, "", value, 2 ) == SCT_FALSE );
    CHECK( sctGetAttributeAsFloatArray( attributes, "attribute", value, 2 ) == SCT_FALSE );

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsIntArray )
{
    SCTAttribute int_array_attributes[] = { 
        { "attribute0", "{1,2}" } };

    int value[] = {0, 0};

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( int_array_attributes );

    CHECK( sctGetAttributeAsIntArray( attributes, "", value, 2 ) == SCT_FALSE );
    CHECK( sctGetAttributeAsIntArray( attributes, "attribute", value, 2 ) == SCT_FALSE );

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsHexArray )
{
    SCTAttribute hex_array_attributes[] = { 
        { "attribute0", "{0x1,0x2}" } };

    unsigned int value[] = {0, 0};

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( hex_array_attributes );

    CHECK( sctGetAttributeAsHexArray( attributes, "", value, 2 ) == SCT_FALSE );
    CHECK( sctGetAttributeAsHexArray( attributes, "attribute", value, 2 ) == SCT_FALSE );

    sctDestroyAttributeList( attributes );
}


////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsFloatVector )
{
    SCTAttribute float_array_attributes[] = { 
        { "attribute0", "{1.0,2.0}" } };
    SCTFloatVector *vector = NULL;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( float_array_attributes );

    CHECK( sctGetAttributeAsFloatVector( attributes, "", &vector ) == SCT_FALSE );
    CHECK( vector == NULL );
    CHECK( sctGetAttributeAsFloatVector( attributes, "attribute", &vector ) == SCT_FALSE );
    CHECK( vector == NULL );

    sctDestroyAttributeList( attributes );
}
////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsDoubleVector )
{
    SCTAttribute double_array_attributes[] = { 
        { "attribute0", "{1.0,2.0}" } };
    SCTDoubleVector *vector = NULL;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( double_array_attributes );

    CHECK( sctGetAttributeAsDoubleVector( attributes, "", &vector ) == SCT_FALSE );
    CHECK( vector == NULL );
    CHECK( sctGetAttributeAsDoubleVector( attributes, "attribute", &vector ) == SCT_FALSE );
    CHECK( vector == NULL );

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsHexVector )
{
    SCTAttribute hex_array_attributes[] = { 
        { "attribute0", "{0x1,0x2}" } };
    SCTHexVector *vector = NULL;

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( hex_array_attributes );

    CHECK( sctGetAttributeAsHexVector( attributes, "", &vector ) == SCT_FALSE );
    CHECK( vector == NULL );
    CHECK( sctGetAttributeAsHexVector( attributes, "attribute", &vector ) == SCT_FALSE );
    CHECK( vector == NULL );

    sctDestroyAttributeList( attributes );
}


////////////////////////////////////////////////////////////////////////////////
TEST( NonExistentValue, sctGetAttributeAsString )
{
    SCTAttribute string_attributes[] = { 
        { "attribute0", "foo" } };

    char buf[ 32 ];

    SCTAttributeList *attributes = ATTRIB_ARRAY_TO_LIST( string_attributes );

    CHECK( sctGetAttributeAsString( attributes, "", buf, 32 ) == SCT_FALSE );
    CHECK( sctGetAttributeAsString( attributes, "attribute", buf, 32 ) == SCT_FALSE );

    sctDestroyAttributeList( attributes );
}

////////////////////////////////////////////////////////////////////////////////
TEST( Success, SCTLog )
{
    SCTLog *log;

    tsiCommonResetSystem();
    
    log = sctCreateLog();
    CHECK( log != NULL );

    CHECK( strlen( log->error ) == 0  );
    CHECK_EQUAL( 0, sctCountWarnings( log ) );

    sctSetErrorLog( log );
    
    sctLogError( "Error" );
    sctLogWarning( "Warning1" );
    sctLogWarning( "Warning2" );

    CHECK( strcmp( log->error, "Error" ) == 0  );
    CHECK_EQUAL( 2, sctCountWarnings( log ) );

    CHECK_STRINGS_EQUAL( "Warning1", sctGetStringAtIndex( log->warnings, 0 ) );
    CHECK_STRINGS_EQUAL( "Warning2", sctGetStringAtIndex( log->warnings, 1 ) );

    sctDestroyLog( log );
    sctCleanupRegistry();
}

////////////////////////////////////////////////////////////////////////////////
TEST( sctResetLog, SCTLog )
{
    SCTLog *log;

    tsiCommonResetSystem();

    log = sctCreateLog();
    CHECK( log != NULL );

    sctSetErrorLog( log );
    sctLogError( "Error" );
    sctLogWarning( "Warning" );

    CHECK( strcmp( log->error, "Error" ) == 0  );
    CHECK_EQUAL( 1, sctCountWarnings( log ) );

    sctResetLog( log );

    CHECK( strlen( log->error ) == 0  );
    CHECK_EQUAL( 0, sctCountWarnings( log ) );
    
    sctLogError( "Errorx" );
    sctLogWarning( "Warning" );

    CHECK( strcmp( log->error, "Errorx" ) == 0  );
    CHECK_EQUAL( 1, sctCountWarnings( log ) );

    sctDestroyLog( log );
    sctCleanupRegistry();
}

////////////////////////////////////////////////////////////////////////////////
TEST( sctLogErrorFileLine, SCTLog )
{
    const char* longMessage = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890discard";
    const char* longFile = "discard123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
    const char* longResult = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890(42): 123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
    SCTLog *log;

    tsiCommonResetSystem();

    log = sctCreateLog();
    CHECK( log != NULL );

    sctSetErrorLog( log );
    
    sctLogErrorFileLine( "message", "file", 42 );
    CHECK( strcmp( log->error, "file(42): message" ) == 0 );

    sctResetLog( log );

    sctLogErrorFileLine( longMessage, longFile, 42 );
    CHECK( strcmp( log->error, longResult ) == 0 );

    sctDestroyLog( log );
    sctCleanupRegistry();
}

////////////////////////////////////////////////////////////////////////////////
TEST( Failure, SCTLog )
{
    tsiCommonResetSystem();
    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctCreateLog() == NULL );
    tsiCommonSetAllocFailThreshold( 1, -1 );
    CHECK( sctCreateLog() == NULL );

}

////////////////////////////////////////////////////////////////////////////////
int timerfunc()
{
    return 1000;
}

TEST( Ok, sctGetMinTicks )
{
    tsiCommonResetSystem();

    tsiCommonSetTimer( timerfunc, 1000000 );
    CHECK( sctGetMinTicks() > 0 );
}

////////////////////////////////////////////////////////////////////////////////
TEST( Strn, Common )
{
    char buffer[ 8 ];

    CHECK( sctStrncopy( buffer, "abc", 3 ) == SCT_TRUE );
    CHECK( strcmp( buffer, "abc" ) == 0 );

    CHECK( sctStrncopy( buffer, "abc", 4 ) == SCT_TRUE );
    CHECK( strcmp( buffer, "abc" ) == 0 );

    CHECK( sctStrncopy( buffer, "abc", 2 ) == SCT_FALSE );
    CHECK( strcmp( buffer, "ab" ) == 0 );

    CHECK( sctStrncopy( buffer, "abc", 1 ) == SCT_FALSE );
    CHECK( strcmp( buffer, "a" ) == 0 );

    CHECK( sctStrncopy( buffer, "", 1 ) == SCT_TRUE );
    CHECK( strcmp( buffer, "" ) == 0 );

    strcpy( buffer, "abc" );
    CHECK( sctStrncat( buffer, "def", 3 ) == SCT_TRUE );
    CHECK( strcmp( buffer, "abcdef" ) == 0 );

    strcpy( buffer, "abc" );
    CHECK( sctStrncat( buffer, "def", 2 ) == SCT_FALSE );
    CHECK( strcmp( buffer, "abcde" ) == 0 );

}

////////////////////////////////////////////////////////////////////////////////
int compareFloat( float f1, float f2, float delta = 0.0001 )
{
    return fabs( f1 - f2 ) < delta;
}

TEST( Atof, Common )
{

    CHECK( compareFloat( 2, 1 ) == 0 );

    CHECK( compareFloat( sctAtof( "1" ),    1.0f ) );
    CHECK( compareFloat( sctAtof( "-1" ),  -1.0f ) );
    CHECK( compareFloat( sctAtof( "1." ),   1.0f ) );
    CHECK( compareFloat( sctAtof( "-1." ), -1.0f ) );
    CHECK( compareFloat( sctAtof( ".1" ),   0.1f ) );
    CHECK( compareFloat( sctAtof( "-.1" ), -0.1f ) );


    CHECK( compareFloat( sctAtof( "12345" ),        12345.0f ) );
    CHECK( compareFloat( sctAtof( "-12345" ),       -12345.0f ) );
    CHECK( compareFloat( sctAtof( "0.12345" ),      0.12345f ) );
    CHECK( compareFloat( sctAtof( "-0.12345" ),     -0.12345f ) );
    CHECK( compareFloat( sctAtof( "1000.1000" ),    1000.1f ) );
    CHECK( compareFloat( sctAtof( "12345.5" ),      12345.5f ) );
    CHECK( compareFloat( sctAtof( "12345.25" ),     12345.25f ) );
    CHECK( compareFloat( sctAtof( "-12345.12345" ), -12345.12345f ) );


    CHECK( sctAtof( "" ) == 0.0f );
    CHECK( sctAtof( "-" ) == 0.0f );
    CHECK( sctAtof( "." ) == 0.0f );
    CHECK( sctAtof( "-." ) == 0.0f );
    CHECK( sctAtof( "0" ) == 0.0f );
    CHECK( sctAtof( "-0" ) == 0.0f );
    CHECK( sctAtof( "0." ) == 0.0f );
    CHECK( sctAtof( "-0." ) == 0.0f );
    CHECK( sctAtof( "0.0" ) == 0.0f );
    CHECK( sctAtof( "-0.0" ) == 0.0f );
    CHECK( sctAtof( "1.." ) == 0.0f );
    CHECK( sctAtof( "a.0" ) == 0.0f );
}

////////////////////////////////////////////////////////////////////////////////
TEST( test, sctMapStringToBoolean )
{
    CHECK( sctMapStringToBoolean( "ON" ) == 1 );
    CHECK( sctMapStringToBoolean( "OFF" ) == 0 );
    CHECK( sctMapStringToBoolean( "ONN" ) == -1 );
}

////////////////////////////////////////////////////////////////////////////////
TEST( test, sctFormatSize_t )
{
    char    buf[ 64 ];
    size_t  t =         0x7fffffff;

    CHECK( sizeof( size_t ) == 4 );
    
    strcpy( buf, "-" );
    CHECK( sctFormatSize_t( buf, 0, t ) == SCT_FALSE );
    CHECK( strcmp( buf, "-" ) == 0 );    
    CHECK( sctFormatSize_t( buf, 8, t ) == SCT_FALSE );
    CHECK( strcmp( buf, "-" ) == 0 );    
    CHECK( sctFormatSize_t( buf, 9, t ) == SCT_TRUE );
    CHECK( strcmp( buf, "7fffffff" ) == 0 );
}

////////////////////////////////////////////////////////////////////////////////
TEST( PointerNotRegistered, sctGetRegisteredPointer )
{
    SCTBoolean pointerRegistered = SCT_TRUE;

    tsiCommonResetSystem();

    void *pointer = sctGetRegisteredPointer( SCT_ERROR_LOG_POINTER, &pointerRegistered );
    CHECK( pointer == NULL );
    CHECK( pointerRegistered == SCT_FALSE );
    
}

TEST( SinglePointerRegistering, sctRegisterPointe )
{
    void *pointer = (void *) 0x1234;
    SCTBoolean pointerRegistered = SCT_FALSE;

    tsiCommonResetSystem();

    sctCleanupRegistry();

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctRegisterPointer( SCT_ERROR_LOG_POINTER, pointer ) == SCT_FALSE );
    tsiCommonSetAllocFailThreshold( -1, -1 );

    CHECK( sctRegisterPointer( SCT_ERROR_LOG_POINTER, pointer ) == SCT_TRUE );
    CHECK( sctGetRegisteredPointer( SCT_ERROR_LOG_POINTER, &pointerRegistered ) == pointer );
    CHECK( pointerRegistered == SCT_TRUE );
    CHECK( sctRegisterPointer( SCT_ERROR_LOG_POINTER, pointer ) == SCT_TRUE );
    CHECK( sctGetRegisteredPointer( SCT_ERROR_LOG_POINTER, &pointerRegistered ) == pointer );
    CHECK( pointerRegistered == SCT_TRUE );

    sctCleanupRegistry();
}

TEST( MultiplePointerRegistering, sctRegisterPointer )
{
    void *pointer1 = (void *) 0x1234;
    void *pointer2 = (void *) 0x5678;
    SCTBoolean pointerRegistered = SCT_FALSE;

    tsiCommonResetSystem();

    CHECK( sctRegisterPointer( SCT_ERROR_LOG_POINTER, pointer1 ) == SCT_TRUE );
    CHECK( sctRegisterPointer( SCT_THREAD_MODULE_POINTER, pointer2 ) == SCT_TRUE );
    CHECK( sctGetRegisteredPointer( SCT_ERROR_LOG_POINTER, &pointerRegistered ) == pointer1 );
    CHECK( pointerRegistered == SCT_TRUE );
    pointerRegistered = SCT_FALSE;
    CHECK( sctGetRegisteredPointer( SCT_THREAD_MODULE_POINTER, &pointerRegistered ) == pointer2 );
    CHECK( pointerRegistered == SCT_TRUE );

    sctUnregisterPointer( SCT_ERROR_LOG_POINTER );
    CHECK( sctGetRegisteredPointer( SCT_ERROR_LOG_POINTER, &pointerRegistered ) == NULL );
    sctUnregisterPointer( SCT_ERROR_LOG_POINTER );
    CHECK( sctGetRegisteredPointer( SCT_ERROR_LOG_POINTER, &pointerRegistered ) == NULL );
    sctUnregisterPointer( SCT_THREAD_MODULE_POINTER );
    CHECK( sctGetRegisteredPointer( SCT_THREAD_MODULE_POINTER, &pointerRegistered ) == NULL );

    CHECK( sctRegisterPointer( SCT_ERROR_LOG_POINTER, pointer1 ) == SCT_TRUE );
    CHECK( sctRegisterPointer( SCT_THREAD_MODULE_POINTER, pointer2 ) == SCT_TRUE );
    CHECK( sctGetRegisteredPointer( SCT_ERROR_LOG_POINTER, &pointerRegistered ) == pointer1 );
    CHECK( pointerRegistered == SCT_TRUE );
    pointerRegistered = SCT_FALSE;
    CHECK( sctGetRegisteredPointer( SCT_THREAD_MODULE_POINTER, &pointerRegistered ) == pointer2 );
    CHECK( pointerRegistered == SCT_TRUE );

    sctCleanupRegistry();
}

////////////////////////////////////////////////////////////////////////////////
int main( void )
{
    tsiCommonInitSystem();

 	TestResult tr;
	int s = !TestRegistry::runAllTests(tr);

    tsiCommonDestroySystem();
    
    return s;
}



