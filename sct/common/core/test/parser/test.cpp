/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <TestHarness.h>

#include "sct_parser.h"
#include "sct_utils.h"
#include "sct_tokenizer.h"
#include "sct_module.h"
#include "sct_action.h"
#include "sicommon_mock.h"

#include <assert.h>
#include <string.h>
#include <stdio.h>

#ifdef _MSC_VER
#pragma warning (disable: 4514 )
#endif

typedef struct
{
	SCTTokenType	type;
	char		   *value;
} TToken;

////////////////////////////////////////////////////////////////////////////////
// Variables that control the testing environment.
SCTBoolean           _tokenizerFailure;
SCTBoolean           _createActionFailure;
int                  _tokenIndex;
SCTTokenizerContext *_tokenizerContextPtr = (SCTTokenizerContext*) 0x1;
const TToken        *_tokenSource;

void _reset()
{
    _tokenizerFailure    = SCT_FALSE;
    _createActionFailure = SCT_FALSE;
    _tokenSource         = NULL;
    _tokenIndex          = 0;
}

//////////////////////////////////////////////////////////////////////////////// 
// Mock tokenizer functions.
SCTTokenizerContext *sctTokenizerInit()
{
	if( _tokenizerFailure )
    {
        // Put an error to the log as the real tokenizer would do it also.
        sctLogError( "Tokenizer: init failed" );
		return NULL;
    }

	_tokenIndex = 0;
	return _tokenizerContextPtr;
}

void sctTokenizerTerminate( SCTTokenizerContext *context )
{
	SCT_USE_VARIABLE( context );
}

// Implementation for tokenizer public functions.
SCTTokenType sctGetToken( SCTTokenizerContext *context, char **token )
{
	SCTTokenType	type;

    assert( context == _tokenizerContextPtr );
	assert( _tokenSource != NULL );

	*token = _tokenSource[_tokenIndex].value;
	type   = _tokenSource[_tokenIndex].type;

    // If this assert fires TOK_END is probably missing from the end of token list.
	assert( type <= TOK_ERROR ); 

    if( type != TOK_END )
        _tokenIndex++;

	return type;
}

void sctPutBackToken( SCTTokenizerContext *context )
{
    assert( context == _tokenizerContextPtr );
    _tokenIndex--;
}

void sctLogInputError( SCTTokenizerContext *context, char *error )
{
    assert( context == _tokenizerContextPtr );
    sctLogError( error );
}

void sctTokernizerCheckRelease( SCTTokenizerContext* context )
{
    assert( context == _tokenizerContextPtr );
}

////////////////////////////////////////////////////////////////////////////////
// Test setup.

static const char testModuleName[] = "TEST";

SCTBoolean testActionExecute( SCTAction *, int )
{
    return SCT_TRUE;
}

SCTAction *testModuleCreateActionMethod( SCTModule *module, const char *name, const char *type, SCTAttributeList *attributes )
{
    assert( strcmp( "TEST", type ) == 0 );

    if( _createActionFailure == SCT_TRUE )
    {
        SCT_LOG_ERROR( "Test action creation failed" );
        return NULL;
    }
    
    return sctCreateAction( name, type, module->name, attributes,
                            NULL, NULL, testActionExecute, NULL, NULL, NULL );
}
    
#define SETUP     SCTModuleList    *moduleList = sctCreateModuleList();    \
                  SCTBenchmarkList *bmarkList  = sctCreateBenchmarkList(); \
                  SCTActionList    *actionList = sctCreateActionList();    \
                  SCTLog           *log        = sctCreateLog();           \
                  assert( moduleList && bmarkList && actionList && log );  \
                  sctSetErrorLog( log ); \
                  sctAddModuleToList( moduleList, sctCreateModule( testModuleName, NULL, "", NULL, testModuleCreateActionMethod, NULL ) ); \
                  _reset();

#define TEARDOWN  sctDestroyModuleList( moduleList );   \
                  sctDestroyBenchmarkList( bmarkList ); \
                  sctDestroyActionListAndElements( actionList );   \
                  sctDestroyLog( log ); \
                  sctCleanupRegistry();

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Test cases.
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Test that failure to initialize the tokenizer causes parser to fail
// gracefully.
TEST( TokenizerInitFailTest, sctParseInput )
{
    SETUP;
	_tokenizerFailure = SCT_TRUE;

    CHECK( sctParseInput( moduleList, bmarkList, actionList ) == SCT_FALSE );
    CHECK_STRINGS_EQUAL( "Tokenizer: init failed", log->error );

    TEARDOWN;
}

////////////////////////////////////////////////////////////////////////////////
// Test that empty input does not cause parsing error and no benchmarks and no
// actions are created.
static const TToken emptyInputTokens[] =
{
    {TOK_END,   ""}
};

TEST( EmptyInput, sctParseInput )
{
    SETUP;
    _tokenSource = emptyInputTokens;

    CHECK( sctParseInput( moduleList, bmarkList, actionList ) == SCT_TRUE );
    CHECK_EQUAL( 0, sctCountListElements( bmarkList->list ) );
    CHECK_EQUAL( 0, sctCountListElements( actionList->list ) );

    TEARDOWN;
}

////////////////////////////////////////////////////////////////////////////////
// Action parsing test cases.
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Test that a simple action is parsed correctly.
#define TEST_ACTION_1_TOKENS {TOK_DELIMITER, "["}, \
                             {TOK_STRING, "Test action 1"}, \
                             {TOK_DELIMITER, ":"}, \
                             {TOK_STRING, "TEST"}, \
                             {TOK_DELIMITER, "@"}, \
                             {TOK_STRING, "TEST"}, \
                             {TOK_DELIMITER, "]"}, \
                             {TOK_STRING, "TestAttribute"},\
                             {TOK_DELIMITER, ":"}, \
                             {TOK_STRING, "123"}

#define TEST_ACTION_2_TOKENS {TOK_DELIMITER, "["}, \
                             {TOK_STRING, "Test action 2"}, \
                             {TOK_DELIMITER, ":"}, \
                             {TOK_STRING, "TEST"}, \
                             {TOK_DELIMITER, "@"}, \
                             {TOK_STRING, "TEST"}, \
                             {TOK_DELIMITER, "]"}, \
                             {TOK_STRING, "TestAttribute"},\
                             {TOK_DELIMITER, ":"}, \
                             {TOK_STRING, "456"}

static const TToken simpleActionTokens[] =
{
    TEST_ACTION_1_TOKENS,
    {TOK_END, ""}
};

TEST( SimpleAction, sctParseInput )
{
    SETUP;
    _tokenSource = simpleActionTokens;

    CHECK( sctParseInput( moduleList, bmarkList, actionList ) == SCT_TRUE );
    CHECK_EQUAL( 0, sctCountListElements( bmarkList->list ) );
    CHECK_EQUAL( 1, sctCountListElements( actionList->list ) );
    
    // Check that there were no warnings or errors.
    CHECK( strlen( log->error ) == 0 );
    CHECK_EQUAL( 0, sctCountWarnings( log ) );

    // Check the action.
    SCTActionListIterator iter1;
    sctGetActionListIterator( actionList, &iter1 );
    SCTAction *action = sctGetNextActionListElement( &iter1 );
    CHECK_STRINGS_EQUAL( "Test action 1", action->name );
    CHECK_STRINGS_EQUAL( "TEST", action->moduleName );
    CHECK_STRINGS_EQUAL( "TEST", action->type );

    // Check the action attribute.
    SCTAttributeListIterator iter2;
    sctGetAttributeListIterator( action->attributes, &iter2 );
    SCTAttribute *attribute = sctGetNextAttributeListElement( &iter2 );
    CHECK_STRINGS_EQUAL( "TestAttribute", attribute->name );
    CHECK_STRINGS_EQUAL( "123", attribute->value );
    
    TEARDOWN;
}

////////////////////////////////////////////////////////////////////////////////
// Test that an action section with no attributes is parsed correctly.
static const TToken actionSectionWithNoAttributesTokens[] =
{
    {TOK_DELIMITER, "["},
    {TOK_STRING,    "Test action"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING,    "TEST"},
    {TOK_DELIMITER, "@"},
    {TOK_STRING,    "TEST"},
    {TOK_DELIMITER, "]"},
    {TOK_END,       ""}
};

TEST( ActionSectionWithNoAttributes, sctParseInput )
{
    SETUP;
    _tokenSource = actionSectionWithNoAttributesTokens;

    CHECK( sctParseInput( moduleList, bmarkList, actionList ) == SCT_TRUE );
    CHECK_EQUAL( 0, sctCountListElements( bmarkList->list ) );
    CHECK_EQUAL( 1, sctCountListElements( actionList->list ) );
    
    // Check that there were no warnings or errors.
    CHECK( strlen( log->error ) == 0 );
    CHECK_EQUAL( 0, sctCountWarnings( log ) );

    // Check the action.
    SCTActionListIterator iter1;
    sctGetActionListIterator( actionList, &iter1 );
    SCTAction *action = sctGetNextActionListElement( &iter1 );
    CHECK_STRINGS_EQUAL( "Test action", action->name );
    CHECK_STRINGS_EQUAL( "TEST", action->moduleName );
    CHECK_STRINGS_EQUAL( "TEST", action->type );
    CHECK_EQUAL( 0, sctCountListElements( action->attributes->list ) );

    // Check the action attribute.
    
    TEARDOWN;
}

////////////////////////////////////////////////////////////////////////////////
// Test that an action without action type causes parsing error.
static const TToken missingActionTypeTokens[] =
{
    {TOK_DELIMITER, "["},
    {TOK_STRING, "Test action"},
    {TOK_DELIMITER, ":"},
    {TOK_DELIMITER, "@"},
    {TOK_STRING, "TEST"}, 
    {TOK_DELIMITER, "]"},
    {TOK_STRING, "TestAttribute"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "123"},
    {TOK_END, ""}
};

TEST( MissinActionType, sctParseInput )
{
    SETUP;

    _tokenSource = missingActionTypeTokens;

    CHECK( sctParseInput( moduleList, bmarkList, actionList ) == SCT_FALSE );
    CHECK_EQUAL( 0, sctCountListElements( bmarkList->list ) );
    CHECK_EQUAL( 0, sctCountListElements( actionList->list ) );
    CHECK_EQUAL( 0, sctCountWarnings( log ) );
    CHECK( strlen( log->error ) > 0 );

    TEARDOWN;
}

////////////////////////////////////////////////////////////////////////////////
// Test that an action without action module causes parsing error.
static const TToken missingActionModuleTokens[] =
{
    {TOK_DELIMITER, "["},
    {TOK_STRING, "Test action"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "TEST"},
    {TOK_DELIMITER, "@"},
    {TOK_DELIMITER, "]"},
    {TOK_STRING, "TestAttribute"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "123"},
    {TOK_END, ""}
};

TEST( MissingActionModule, sctParseInput )
{
    SETUP;

    _tokenSource = missingActionModuleTokens;

    CHECK( sctParseInput( moduleList, bmarkList, actionList ) == SCT_FALSE );
    CHECK_EQUAL( 0, sctCountListElements( bmarkList->list ) );
    CHECK_EQUAL( 0, sctCountListElements( actionList->list ) );
    CHECK_EQUAL( 0, sctCountWarnings( log ) );
    CHECK( strlen( log->error ) > 0 );

    TEARDOWN;
}

////////////////////////////////////////////////////////////////////////////////
// Test that two actions with the same name couses parsing error.
static const TToken duplicateActionNames[] =
{
    {TOK_DELIMITER, "["},
    {TOK_STRING, "Test action"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "TEST"},
    {TOK_DELIMITER, "@"},
    {TOK_STRING, "TEST"},
    {TOK_DELIMITER, "]"},
    {TOK_STRING, "TestAttribute1"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "123"},
    {TOK_DELIMITER, "["},
    {TOK_STRING, "Test action"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "TEST"},
    {TOK_DELIMITER, "@"},
    {TOK_STRING, "TEST"},
    {TOK_DELIMITER, "]"},
    {TOK_STRING, "TestAttribute2"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "123"},
    {TOK_END, ""}
};

TEST( DuplicateActionNames, sctParseInput )
{
    SETUP;

    _tokenSource = duplicateActionNames;

    CHECK( sctParseInput( moduleList, bmarkList, actionList ) == SCT_FALSE );
    CHECK_EQUAL( 0, sctCountListElements( bmarkList->list ) );
    CHECK_EQUAL( 1, sctCountListElements( actionList->list ) );
    CHECK_EQUAL( 0, sctCountWarnings( log ) );
    CHECK( strlen( log->error ) > 0 );

    TEARDOWN;
}

////////////////////////////////////////////////////////////////////////////////
// Test that create action failure is handled correctly.
TEST( CreateActionFailure, sctParseInput )
{
    SETUP;
    _tokenSource         = simpleActionTokens;
    _createActionFailure = SCT_TRUE;

    CHECK( sctParseInput( moduleList, bmarkList, actionList ) == SCT_FALSE );
    CHECK_EQUAL( 0, sctCountListElements( bmarkList->list ) );
    CHECK_EQUAL( 0, sctCountListElements( actionList->list ) );
    CHECK_EQUAL( 0, sctCountWarnings( log ) );
    CHECK( strlen( log->error ) > 0 );

    TEARDOWN;
}

////////////////////////////////////////////////////////////////////////////////
// Benchmark parsing test cases.
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Test that a simple benchmark with no init actions and a single benchmark
// action is parsed correctly.
static const TToken simplestBenchmarkTokens[] =
{
    TEST_ACTION_1_TOKENS,
    {TOK_DELIMITER, "["},
    {TOK_STRING, "Test benchmark"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "BENCHMARK"},
    {TOK_DELIMITER, "]"},
    {TOK_STRING, "InitActions"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "None"},
    {TOK_STRING, "BenchmarkActions"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "Test action 1"},
    {TOK_END, ""}
};

TEST( SimplestBenchmark, sctParseInput )
{
    SETUP;
    _tokenSource = simplestBenchmarkTokens;

    
    CHECK( sctParseInput( moduleList, bmarkList, actionList ) == SCT_TRUE );
    CHECK_EQUAL( 1, sctCountListElements( bmarkList->list ) );
    CHECK_EQUAL( 1, sctCountListElements( actionList->list ) );

    // Check the benchmark.
    SCTBenchmarkListIterator iter1;
    sctGetBenchmarkListIterator( bmarkList, &iter1 );
    SCTBenchmark *bmark = sctGetNextBenchmarkListElement( &iter1 );
    CHECK_STRINGS_EQUAL( "Test benchmark", bmark->name );
    CHECK_EQUAL( 0, sctCountListElements( bmark->mainThread.initActions->list ) );
    CHECK_EQUAL( 1, sctCountListElements( bmark->mainThread.benchmarkActions->list ) );
    CHECK_EQUAL( 0, sctCountListElements( bmark->attributes->list ) );
    // Check the benchmark action list.
    SCTActionListIterator iter2;
    sctGetActionListIterator( bmark->mainThread.benchmarkActions, &iter2 );
    SCTAction *action = sctGetNextActionListElement( &iter2 );
    CHECK_STRINGS_EQUAL( "Test action 1", action->name );
    
    TEARDOWN;
}

////////////////////////////////////////////////////////////////////////////////
// Test that a bencmark with two init actions and two benchmark actions is
// parsed correctly.
static const TToken twoBenchmarkActionsTokens[] =
{
    TEST_ACTION_1_TOKENS,
    TEST_ACTION_2_TOKENS,
    {TOK_DELIMITER, "["},
    {TOK_STRING, "Test benchmark"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "BENCHMARK"},
    {TOK_DELIMITER, "]"},
    {TOK_STRING, "InitActions"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "Test action 2"},
    {TOK_DELIMITER, "+"},
    {TOK_STRING, "Test action 1"},
    {TOK_STRING, "BenchmarkActions"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING, "Test action 1"},
    {TOK_DELIMITER, "+"},
    {TOK_STRING, "Test action 2"},
    {TOK_END, ""}
};
    
TEST( TwoBenchmarkActions, sctParseInput )
{
    SETUP;
    _tokenSource = twoBenchmarkActionsTokens;

    CHECK( sctParseInput( moduleList, bmarkList, actionList ) == SCT_TRUE );
    CHECK_EQUAL( 1, sctCountListElements( bmarkList->list ) );
    CHECK_EQUAL( 2, sctCountListElements( actionList->list ) );

    // Check the benchmark.
    SCTBenchmarkListIterator iter1;
    sctGetBenchmarkListIterator( bmarkList, &iter1 );
    SCTBenchmark *bmark = sctGetNextBenchmarkListElement( &iter1 );
    CHECK_STRINGS_EQUAL( "Test benchmark", bmark->name );
    CHECK_EQUAL( 2, sctCountListElements( bmark->mainThread.initActions->list ) );
    CHECK_EQUAL( 2, sctCountListElements( bmark->mainThread.benchmarkActions->list ) );
    CHECK_EQUAL( 0, sctCountListElements( bmark->attributes->list ) );
    // Check the init action list.
    SCTActionListIterator iter2;
    sctGetActionListIterator( bmark->mainThread.initActions, &iter2 );
    SCTAction *action = sctGetNextActionListElement( &iter2 );
    CHECK_STRINGS_EQUAL( "Test action 2", action->name );
    action = sctGetNextActionListElement( &iter2 );
    CHECK_STRINGS_EQUAL( "Test action 1", action->name );
    // Check the benchmark action list.
    sctGetActionListIterator( bmark->mainThread.benchmarkActions, &iter2 );
    action = sctGetNextActionListElement( &iter2 );
    CHECK_STRINGS_EQUAL( "Test action 1", action->name );
    action = sctGetNextActionListElement( &iter2 );
    CHECK_STRINGS_EQUAL( "Test action 2", action->name );
    
    TEARDOWN;
}

////////////////////////////////////////////////////////////////////////////////
// Test that input with missing section start is properly handled by the parser.
static const TToken missingSectionHeader[] =
{
    {TOK_STRING,    "ColorSpace"},
    {TOK_DELIMITER, ":"},
    {TOK_STRING,    "VGI_COLORSPACE_SRGB"},
    {TOK_END,       ""}
};

TEST( MissingSectionHeader, sctParseInput )
{
    SETUP;
    _tokenSource = missingSectionHeader;

    CHECK( sctParseInput( moduleList, bmarkList, actionList ) == SCT_FALSE );
    CHECK_EQUAL( 0, sctCountWarnings( log ) );
    CHECK( strlen( log->error ) > 0 );
    
    TEARDOWN;
}

int main( void )
{
 	TestResult tr;
	
	tsiCommonInitSystem();
	int exitCode = !TestRegistry::runAllTests( tr );
	tsiCommonDestroySystem();

	return exitCode;
}

