/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "TestHarness.h"

#include "sicommon_mock.h"
#include "sct_benchmark.h"
#include "sct_utils.h"
#include "sct_action.h"

#include <stdio.h>
#include <string.h>

#ifdef _MSC_VER
/* Disable warnings about unreferenced inline functions (4514). */
#pragma warning(disable : 4514)
#pragma warning(disable : 4127)
#endif


SCTBoolean execute( SCTAction *action, int frameNumber )
{
    SCT_USE_VARIABLE( action );
    SCT_USE_VARIABLE( frameNumber );
    return SCT_TRUE;
}

TEST( test, benchmark )
{
    SCTBenchmark                *benchmark;
    const char                  *name = "name";
    SCTAttributeList            *attributes;
    SCTAttributeList            *actionAttributes1;
    SCTAttributeList            *actionAttributes2;
    SCTActionList*              initActions[ 1 ];
    SCTActionList*              benchmarkActions[ 1 ];
    SCTActionList*              finalActions[ 1 ];
    SCTActionList*              preTerminateActions[ 1 ];
    SCTActionList*              terminateActions[ 1 ];
    int                         ac, i;
    SCTLog*                     log;                      
    SCTAction*                  action1;
    SCTAction*                  action2;

    attributes               = sctCreateAttributeList();
    actionAttributes1        = sctCreateAttributeList();
    actionAttributes2        = sctCreateAttributeList();
    initActions[ 0 ]         = sctCreateActionList();
    benchmarkActions[ 0 ]    = sctCreateActionList();
    finalActions[ 0 ]        = NULL ;
    preTerminateActions[ 0 ] = NULL;
    terminateActions[ 0 ]    = NULL;    

    CHECK( attributes != NULL );
    CHECK( actionAttributes1 != NULL );
    CHECK( actionAttributes2 != NULL );
    CHECK( initActions[ 0 ] != NULL );
    CHECK( benchmarkActions[ 0 ] != NULL );

    log = sctCreateLog();
    CHECK( log );
    sctSetErrorLog( log );

    action1 = sctCreateAction( "name", "type", "module",
                               actionAttributes1,
                               NULL,
                               NULL,
                               execute,
                               NULL,
                               NULL,
                               NULL );

    action2 = sctCreateAction( "name", "type", "module",
                               actionAttributes2,
                               NULL,
                               NULL,
                               execute,
                               NULL,
                               NULL,
                               NULL );

    CHECK( sctAddActionToList( benchmarkActions[ 0 ], action1 ) == SCT_TRUE );
    CHECK( sctAddActionToList( benchmarkActions[ 0 ], action2 ) == SCT_TRUE );

    tsiCommonResetAllocCounter();
    benchmark = sctCreateBenchmark( name,
                                    attributes,
                                    1,
                                    initActions,
                                    benchmarkActions,
                                    finalActions,
                                    preTerminateActions,
                                    terminateActions );
    ac = tsiCommonGetAllocCounter();

    CHECK( benchmark != NULL );
    CHECK( strcmp( name, benchmark->name ) == 0 );
    CHECK( benchmark->mainThread.initActions == initActions[ 0 ] );
    CHECK( benchmark->mainThread.benchmarkActions == benchmarkActions[ 0 ] );
    CHECK( benchmark->benchmarkResultList != NULL );

    sctDestroyBenchmark( benchmark );

    sctDestroyAction( action1 );
    sctDestroyAction( action2 );

    attributes            = sctCreateAttributeList();
    actionAttributes1     = sctCreateAttributeList();
    actionAttributes2     = sctCreateAttributeList();
    initActions[ 0 ]      = sctCreateActionList();
    benchmarkActions[ 0 ] = sctCreateActionList();

    CHECK( attributes != NULL );
    CHECK( actionAttributes1 != NULL );
    CHECK( actionAttributes2 != NULL );
    CHECK( initActions[ 0 ] != NULL );
    CHECK( benchmarkActions[ 0 ] != NULL );

    action1 = sctCreateAction( "name", "type", "module",
                               actionAttributes1,
                               NULL,
                               NULL,
                               execute,
                               NULL,
                               NULL,
                               NULL );

    action2 = sctCreateAction( "name", "type", "module",
                               actionAttributes2,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );

    CHECK( sctAddActionToList( benchmarkActions[ 0 ], action1 ) == SCT_TRUE );
    CHECK( sctAddActionToList( benchmarkActions[ 0 ], action2 ) == SCT_TRUE );

    CHECK( sctCreateBenchmark( name, attributes, 1, initActions, benchmarkActions, finalActions, preTerminateActions, terminateActions ) == NULL );

    sctDestroyAttributeList( attributes );
    sctDestroyActionList( initActions[ 0 ] );
    sctDestroyActionList( benchmarkActions[ 0 ] );

    sctDestroyAction( action1 );
    sctDestroyAction( action2 );
    sctResetLog( log );

    attributes               = sctCreateAttributeList();
    actionAttributes1        = sctCreateAttributeList();
    actionAttributes2        = sctCreateAttributeList();
    initActions[ 0 ]         = sctCreateActionList();
    benchmarkActions[ 0 ]    = sctCreateActionList();

    CHECK( attributes != NULL );
    CHECK( actionAttributes1 != NULL );
    CHECK( actionAttributes2 != NULL );
    CHECK( initActions[ 0 ] != NULL );
    CHECK( benchmarkActions[ 0 ] != NULL );

    action1 = sctCreateAction( "name", "type", "module",
                               actionAttributes1,
                               NULL,
                               NULL,
                               execute,
                               NULL,
                               NULL,
                               NULL );

    action2 = sctCreateAction( "name", "type", "module",
                               actionAttributes2,
                               NULL,
                               NULL,
                               execute,
                               NULL,
                               NULL,
                               NULL );

    CHECK( sctAddActionToList( benchmarkActions[ 0 ], action1 ) == SCT_TRUE );
    CHECK( sctAddActionToList( benchmarkActions[ 0 ], action2 ) == SCT_TRUE );

    CHECK( sctAddNameValueToList( attributes, SCT_BENCHMARK_REPEATS_ATTRIBUTE, "foo" ) == SCT_TRUE );

    CHECK( sctCreateBenchmark( name, attributes, 1, initActions, benchmarkActions, finalActions, preTerminateActions, terminateActions ) == NULL );

    sctDestroyAttributeList( attributes );
    sctDestroyActionList( initActions[ 0 ] );
    sctDestroyActionList( benchmarkActions[ 0 ] );

    sctDestroyAction( action1 );
    sctDestroyAction( action2 );
    sctResetLog( log );

    attributes              = sctCreateAttributeList();
    actionAttributes1       = sctCreateAttributeList();
    actionAttributes2       = sctCreateAttributeList();
    initActions[ 0 ]        = sctCreateActionList();
    benchmarkActions[ 0 ]   = sctCreateActionList();

    CHECK( attributes != NULL );
    CHECK( actionAttributes1 != NULL );
    CHECK( actionAttributes2 != NULL );
    CHECK( initActions != NULL );
    CHECK( benchmarkActions != NULL );

    action1 = sctCreateAction( "name", "type", "module",
                               actionAttributes1,
                               NULL,
                               NULL,
                               execute,
                               NULL,
                               NULL,
                               NULL );

    action2 = sctCreateAction( "name", "type", "module",
                               actionAttributes2,
                               NULL,
                               NULL,
                               execute,
                               NULL,
                               NULL,
                               NULL );

    CHECK( sctAddActionToList( benchmarkActions[ 0 ], action1 ) == SCT_TRUE );
    CHECK( sctAddActionToList( benchmarkActions[ 0 ], action2 ) == SCT_TRUE );

    for( i = 0; i < ac; i++ )
    {
        sctResetLog( log );
        tsiCommonSetAllocFailThreshold( i, -1 );
        CHECK( sctCreateBenchmark( name, attributes, 1, initActions, benchmarkActions, finalActions, preTerminateActions, terminateActions ) == NULL );
    }

    tsiCommonSetAllocFailThreshold( -1, -1 );

    sctDestroyAttributeList( attributes );
    sctDestroyActionList( initActions[ 0 ] );
    sctDestroyActionList( benchmarkActions[ 0 ] );

    sctDestroyAction( action1 );
    sctDestroyAction( action2 );
    sctResetLog( log );

    sctCleanupRegistry();
    sctDestroyLog( log );
}


TEST( test, benchmarkList )
{
    SCTBenchmarkList            *list;
    SCTBenchmarkListIterator    iterator;
    SCTBenchmark                *benchmark;
    const char                  *name = "name";
    SCTAttributeList            *attributes;
    SCTActionList*              initActions[ 1 ];
    SCTActionList*              benchmarkActions[ 1 ];
    SCTActionList*              finalActions[ 1 ];
    SCTActionList*              preTerminateActions[ 1 ];
    SCTActionList*              terminateActions[ 1 ];

    attributes               = sctCreateAttributeList();
    initActions[ 0 ]         = sctCreateActionList();
    benchmarkActions[ 0 ]    = sctCreateActionList();
    finalActions[ 0 ]        = NULL ;
    preTerminateActions[ 0 ] = NULL;
    terminateActions[ 0 ]    = NULL;    

    CHECK( attributes != NULL );
    CHECK( initActions != NULL );
    CHECK( benchmarkActions != NULL );

    benchmark = sctCreateBenchmark( name, attributes, 1, initActions, benchmarkActions, finalActions, preTerminateActions, terminateActions );
    CHECK( benchmark != NULL );

    /* Test normal create and destroy. */
    list = sctCreateBenchmarkList();
    CHECK( list != NULL );

    sctDestroyBenchmarkList( list );

    /* Test null destroy. */
    sctDestroyBenchmarkList( NULL );

    /* Test normal operation. */
    list = sctCreateBenchmarkList();
    CHECK( list != NULL );

    CHECK( sctAddBenchmarkToList( list, benchmark ) == SCT_TRUE );

    sctGetBenchmarkListIterator( list, &iterator );

    CHECK( sctGetNextBenchmarkListElement( &iterator ) == benchmark );
    CHECK( sctGetNextBenchmarkListElement( &iterator ) == NULL );
    CHECK( sctGetNextBenchmarkListElement( &iterator ) == NULL );

    sctDestroyBenchmarkList( list );

    /* Check behavior when memory allocation fails. */
    tsiCommonSetAllocFailThreshold( 0, -1 );
    list = sctCreateBenchmarkList();
    CHECK( list == NULL );

    tsiCommonSetAllocFailThreshold( 1, -1 );
    list = sctCreateBenchmarkList();
    CHECK( list == NULL );

    tsiCommonSetAllocFailThreshold( -1, -1 );
    list = sctCreateBenchmarkList();

    attributes = sctCreateAttributeList();
    initActions[ 0 ] = sctCreateActionList();
    benchmarkActions[ 0 ] = sctCreateActionList();

    CHECK( attributes != NULL );
    CHECK( initActions[ 0 ] != NULL );
    CHECK( benchmarkActions[ 0 ] != NULL );

    benchmark = sctCreateBenchmark( name, attributes, 1, initActions, benchmarkActions, finalActions, preTerminateActions, terminateActions );
    CHECK( benchmark != NULL );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctAddBenchmarkToList( list, benchmark ) == SCT_FALSE );

    tsiCommonSetAllocFailThreshold( -1, -1 );
    CHECK( sctAddBenchmarkToList( list, benchmark ) == SCT_TRUE );

    tsiCommonSetAllocFailThreshold( -1, -1 );

    sctDestroyBenchmarkList( list );
}



int main( void )
{
 	TestResult tr;
    int status;

    tsiCommonInitSystem();

    status = !TestRegistry::runAllTests( tr );
    
    tsiCommonDestroySystem();

    return status;
}
