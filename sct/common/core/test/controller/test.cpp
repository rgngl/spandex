/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "TestHarness.h"
#include "sicommon_mock.h"

#include "sct.h"
#include "sct_parser.h"
#include "sct_reporter.h"
#include "sct_benchmark.h"
#include "sct_utils.h"
#include "sct_action.h"

#ifdef _MSC_VER
#pragma warning (disable: 4514 )
#endif

#ifdef __cplusplus
extern "C" {
#endif

    SCTBoolean          parseFails      = SCT_FALSE;
    SCTModuleList       *parseModules   = NULL;
    SCTBenchmark        *benchmark1     = NULL;
    SCTBenchmark        *benchmark2     = NULL;
    SCTAction           *action1        = NULL;
    SCTAction           *action2        = NULL;

    SCTBoolean sctParseInput( SCTModuleList *modules, SCTBenchmarkList *benchmarks, SCTActionList *actions )
    {
        parseModules = modules;
        SCTBoolean status = SCT_TRUE;

        if( parseFails == SCT_TRUE )
        {
            sctLogError( "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" );
            return SCT_FALSE;
        }

        if( sctAddBenchmarkToList( benchmarks, benchmark1 ) == SCT_TRUE )
        {
            benchmark1 = NULL;
        }
        else
        {
            status = SCT_FALSE;
        }
        if( sctAddBenchmarkToList( benchmarks, benchmark2 ) == SCT_TRUE )
        {
            benchmark2 = NULL;
        }
        else
        {
            status = SCT_FALSE;
        }
        if( sctAddActionToList( actions, action1 ) == SCT_TRUE )
        {
            action1 = NULL;
        }
        else
        {
            status = SCT_FALSE;
        }
        if( sctAddActionToList( actions, action2 ) == SCT_TRUE )
        {
            action2 = NULL;
        }
        else
        {
            status = SCT_FALSE;
        }

        if( status == SCT_FALSE )
        {
            sctLogError( "terror" );
            return SCT_FALSE;
        }
        
        return SCT_TRUE;
    }

    SCTModuleList       *reportedModules = NULL;
    SCTActionList       *reportedActions = NULL;
    SCTBenchmarkList    *reportedBenchmarks = NULL;
    SCTBoolean          reportModuleInformationFails = SCT_FALSE;
    SCTBoolean          reportActionsFails = SCT_FALSE;
    SCTBoolean          reportBenchmarkFails = SCT_FALSE;

    SCTBoolean sctReportModuleInformation( SCTModuleList *modules )
    {
        if( reportModuleInformationFails == SCT_TRUE )
        {
            return SCT_FALSE;
        }
        reportedModules = modules;
        return SCT_TRUE;
    }

    SCTBoolean sctReportActions( SCTActionList *actions )
    {
        if( reportActionsFails == SCT_TRUE )
        {
            return SCT_FALSE;
        }

        reportedActions = actions;
        return SCT_TRUE;
    }

    SCTBoolean sctReportBenchmark( SCTBenchmark *benchmark )
    {
        if( reportBenchmarkFails == SCT_TRUE )
        {
            return SCT_FALSE;
        }

        if( reportedBenchmarks != NULL )
        {
            sctAddBenchmarkToList( reportedBenchmarks, benchmark );
        }
        return SCT_TRUE;
    }

    SCTBenchmarkList    *cycledBenchmarks = NULL;
    SCTBoolean          benchmarkRunCreationFails = SCT_FALSE;
    SCTBoolean          benchmarkRunCycleFails = SCT_FALSE;

    void* sctCreateBenchmarkRun( SCTBenchmark *benchmark )
    {
        if( benchmarkRunCreationFails == SCT_TRUE )
        {
            return NULL;
        }

        return benchmark;
    }

    SCTCycleStatus sctRunBenchmarkCycle( void *benchmarkRun )
    {
        static int flip = 0;
        if( benchmarkRunCycleFails == SCT_TRUE )
        {
            return SCT_CYCLE_ERROR;
        }

        if( cycledBenchmarks != NULL )
        {
            sctAddBenchmarkToList( cycledBenchmarks, ( SCTBenchmark* )benchmarkRun );
        }
        if( flip % 2 == 0 )
        {
            flip++;
            return SCT_CYCLE_FINISHED;
        }
        else
        {
            flip++;
            return SCT_CYCLE_CONTINUE;
        }
    }

    void sctDestroyBenchmarkRun( void *benchmarkRun )
    {
        ( void )benchmarkRun;
    }


    SCTModuleList* moduleListFunc()
    {
        return sctCreateModuleList();
    }
    
#ifdef __cplusplus
}
#endif

TEST( test, controller )
{
    void                        *context;
    SCTAttributeList            *attributes1;
    SCTAttributeList            *attributes2;
    SCTActionList*              initActions1[ 1 ];
    SCTActionList*              benchmarkActions1[ 1 ];
    SCTActionList*              finalActions[ 1 ];
    SCTActionList*              preTerminateActions[ 1 ];
    SCTActionList*              terminateActions[ 1 ];
    SCTActionList*              initActions2[ 1 ];
    SCTActionList*              benchmarkActions2[ 1 ];    
    SCTAttributeList            *action1Attributes;
    SCTAttributeList            *action2Attributes;
    int                         initAllocCount;
    int                         cycleAllocCount;
    int                         i;
    
    attributes1              = sctCreateAttributeList();
    attributes2              = sctCreateAttributeList();
    initActions1[ 0 ]        = sctCreateActionList();
    initActions2[ 0 ]        = sctCreateActionList();
    benchmarkActions1[ 0 ]   = sctCreateActionList();
    benchmarkActions2[ 0 ]   = sctCreateActionList();
    action1Attributes        = sctCreateAttributeList();
    action2Attributes        = sctCreateAttributeList();
    finalActions[ 0 ]        = NULL ;
    preTerminateActions[ 0 ] = NULL;
    terminateActions[ 0 ]    = NULL;    
    
    CHECK( attributes1 != NULL );
    CHECK( attributes2 != NULL );
    CHECK( initActions1[ 0 ] != NULL );
    CHECK( initActions2[ 0 ] != NULL );
    CHECK( benchmarkActions1[ 0 ] != NULL );
    CHECK( benchmarkActions2[ 0 ] != NULL );
    CHECK( action1Attributes != NULL );
    CHECK( action2Attributes != NULL );

    reportedBenchmarks = sctCreateBenchmarkList();
    CHECK( reportedBenchmarks != NULL );

    tsiCommonSetModuleListFunc( moduleListFunc );

    benchmark1 = sctCreateBenchmark( "benchmark1",
                                     attributes1,
                                     1,
                                     initActions1,
                                     benchmarkActions1,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    benchmark2 = sctCreateBenchmark( "benchmark1",
                                     attributes2,
                                     1,
                                     initActions2,
                                     benchmarkActions2,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );

    action1 = sctCreateAction( "action1", "type", "module",
                               action1Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );
    action2 = sctCreateAction( "action2", "type", "module",
                               action2Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );

    SCTBenchmark        *bm1    = benchmark1;
    SCTBenchmark        *bm2    = benchmark2;
    SCTAction           *a1     = action1;
    SCTAction           *a2     = action2;

    // Check that system error log is not set.
    CHECK( sctGetErrorLog() == NULL );

    tsiCommonResetAllocCounter();

    context = sctInitialize();
    CHECK( context != NULL );

    initAllocCount = tsiCommonGetAllocCounter();

    // Check that controller set the system error log.
    CHECK( sctGetErrorLog() != NULL );

    tsiCommonResetAllocCounter();

    SCTCycleStatus status;
    while( ( status = sctCycle( context ) ) == SCT_CYCLE_CONTINUE )
    {
        CHECK( status != SCT_CYCLE_ERROR );
    }

    cycleAllocCount = tsiCommonGetAllocCounter();

    SCTBenchmarkListIterator benchmarkListIterator;
    sctGetBenchmarkListIterator( reportedBenchmarks, &benchmarkListIterator );

    CHECK( sctGetNextBenchmarkListElement( &benchmarkListIterator ) == bm1 );
    CHECK( sctGetNextBenchmarkListElement( &benchmarkListIterator ) == bm2 );
    CHECK( sctGetNextBenchmarkListElement( &benchmarkListIterator ) == NULL );

    sctDestroyList( reportedBenchmarks->list );
    reportedBenchmarks->list = NULL;
    sctDestroyBenchmarkList( reportedBenchmarks );
    reportedBenchmarks = NULL;

    SCTActionListIterator actionListIterator;
    sctGetActionListIterator( reportedActions, &actionListIterator );
     
    CHECK( sctGetNextActionListElement( &actionListIterator ) == a1 );
    CHECK( sctGetNextActionListElement( &actionListIterator ) == a2 );
    CHECK( sctGetNextActionListElement( &actionListIterator ) == NULL );

    sctTerminate( context );


    /* Check reporting module failures. */
    reportModuleInformationFails = SCT_TRUE;
    attributes1                 = sctCreateAttributeList();
    attributes2                 = sctCreateAttributeList();
    initActions1[ 0 ]           = sctCreateActionList();
    initActions2[ 0 ]           = sctCreateActionList();
    benchmarkActions1[ 0 ]      = sctCreateActionList();
    benchmarkActions2[ 0 ]      = sctCreateActionList();
    action1Attributes           = sctCreateAttributeList();
    action2Attributes           = sctCreateAttributeList();

    CHECK( attributes1 != NULL );
    CHECK( attributes2 != NULL );
    CHECK( initActions1[ 0 ] != NULL );
    CHECK( initActions2[ 0 ] != NULL );
    CHECK( benchmarkActions1[ 0 ] != NULL );
    CHECK( benchmarkActions2[ 0 ] != NULL );
    CHECK( action1Attributes != NULL );
    CHECK( action2Attributes != NULL );

    reportedBenchmarks = sctCreateBenchmarkList();
    CHECK( reportedBenchmarks != NULL );
        
    benchmark1 = sctCreateBenchmark( "benchmark1",
                                     attributes1,
                                     1,
                                     initActions1,
                                     benchmarkActions1,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    benchmark2 = sctCreateBenchmark( "benchmark1",
                                     attributes2,
                                     1,
                                     initActions2,
                                     benchmarkActions2,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    
    action1 = sctCreateAction( "action1", "type", "module",
                               action1Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );
    action2 = sctCreateAction( "action2", "type", "module",
                               action2Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );

    CHECK( sctInitialize() == NULL );

    if( benchmark1 != NULL )
    {
        sctDestroyBenchmark( benchmark1 );
        benchmark1 = NULL;
    }
    if( benchmark2 != NULL )
    {
        sctDestroyBenchmark( benchmark2 );
        benchmark2 = NULL;
    }
    if( action1 != NULL )
    {
        sctDestroyAction( action1 );
        action1 = NULL;
    }
    if( action2 != NULL )
    {
        sctDestroyAction( action2 );
        action2 = NULL;
    }
    
    sctDestroyList( reportedBenchmarks->list );
    reportedBenchmarks->list = NULL;
    sctDestroyBenchmarkList( reportedBenchmarks );
    reportedBenchmarks = NULL;

    reportModuleInformationFails = SCT_FALSE;;


    /* Check reporting action failures. */
    reportActionsFails = SCT_TRUE;
    attributes1                 = sctCreateAttributeList();
    attributes2                 = sctCreateAttributeList();
    initActions1[ 0 ]           = sctCreateActionList();
    initActions2[ 0 ]           = sctCreateActionList();
    benchmarkActions1[ 0 ]      = sctCreateActionList();
    benchmarkActions2[ 0 ]      = sctCreateActionList();
    action1Attributes           = sctCreateAttributeList();
    action2Attributes           = sctCreateAttributeList();

    CHECK( attributes1 != NULL );
    CHECK( attributes2 != NULL );
    CHECK( initActions1[ 0 ] != NULL );
    CHECK( initActions2[ 0 ] != NULL );
    CHECK( benchmarkActions1[ 0 ] != NULL );
    CHECK( benchmarkActions2[ 0 ] != NULL );
    CHECK( action1Attributes != NULL );
    CHECK( action2Attributes != NULL );

    reportedBenchmarks = sctCreateBenchmarkList();
    CHECK( reportedBenchmarks != NULL );
        
    benchmark1 = sctCreateBenchmark( "benchmark1",
                                     attributes1,
                                     1,
                                     initActions1,
                                     benchmarkActions1,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    benchmark2 = sctCreateBenchmark( "benchmark1",
                                     attributes2,
                                     1,
                                     initActions2,
                                     benchmarkActions2,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    
    action1 = sctCreateAction( "action1", "type", "module",
                               action1Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );
    action2 = sctCreateAction( "action2", "type", "module",
                               action2Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );

    CHECK( sctInitialize() == NULL );

    if( benchmark1 != NULL )
    {
        sctDestroyBenchmark( benchmark1 );
        benchmark1 = NULL;
    }
    if( benchmark2 != NULL )
    {
        sctDestroyBenchmark( benchmark2 );
        benchmark2 = NULL;
    }
    if( action1 != NULL )
    {
        sctDestroyAction( action1 );
        action1 = NULL;
    }
    if( action2 != NULL )
    {
        sctDestroyAction( action2 );
        action2 = NULL;
    }
    
    sctDestroyList( reportedBenchmarks->list );
    reportedBenchmarks->list = NULL;
    sctDestroyBenchmarkList( reportedBenchmarks );
    reportedBenchmarks = NULL;

    reportActionsFails = SCT_FALSE;;


    /* Check all sort of failures. */
    attributes1                 = sctCreateAttributeList();
    attributes2                 = sctCreateAttributeList();
    initActions1[ 0 ]           = sctCreateActionList();
    initActions2[ 0 ]           = sctCreateActionList();
    benchmarkActions1[ 0 ]      = sctCreateActionList();
    benchmarkActions2[ 0 ]      = sctCreateActionList();
    action1Attributes           = sctCreateAttributeList();
    action2Attributes           = sctCreateAttributeList();

    CHECK( attributes1 != NULL );
    CHECK( attributes2 != NULL );
    CHECK( initActions1[ 0 ] != NULL );
    CHECK( initActions2[ 0 ] != NULL );
    CHECK( benchmarkActions1[ 0 ] != NULL );
    CHECK( benchmarkActions2[ 0 ] != NULL );
    CHECK( action1Attributes != NULL );
    CHECK( action2Attributes != NULL );

    reportedBenchmarks = sctCreateBenchmarkList();
    CHECK( reportedBenchmarks != NULL );
        
    benchmark1 = sctCreateBenchmark( "benchmark1",
                                     attributes1,
                                     1,
                                     initActions1,
                                     benchmarkActions1,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    benchmark2 = sctCreateBenchmark( "benchmark1",
                                     attributes2,
                                     1,
                                     initActions2,
                                     benchmarkActions2,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    
    action1 = sctCreateAction( "action1", "type", "module",
                               action1Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );
    action2 = sctCreateAction( "action2", "type", "module",
                               action2Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );

    context = sctInitialize();
    CHECK( context != NULL );

    reportBenchmarkFails = SCT_TRUE;
    sctCycle( context );
    reportBenchmarkFails = SCT_FALSE;

    sctTerminate( context );

    sctDestroyList( reportedBenchmarks->list );
    reportedBenchmarks->list = NULL;
    sctDestroyBenchmarkList( reportedBenchmarks );
    reportedBenchmarks = NULL;


    attributes1                 = sctCreateAttributeList();
    attributes2                 = sctCreateAttributeList();
    initActions1[ 0 ]           = sctCreateActionList();
    initActions2[ 0 ]           = sctCreateActionList();
    benchmarkActions1[ 0 ]      = sctCreateActionList();
    benchmarkActions2[ 0 ]      = sctCreateActionList();
    action1Attributes           = sctCreateAttributeList();
    action2Attributes           = sctCreateAttributeList();

    CHECK( attributes1 != NULL );
    CHECK( attributes2 != NULL );
    CHECK( initActions1[ 0 ] != NULL );
    CHECK( initActions2[ 0 ] != NULL );
    CHECK( benchmarkActions1[ 0 ] != NULL );
    CHECK( benchmarkActions2[ 0 ] != NULL );
    CHECK( action1Attributes != NULL );
    CHECK( action2Attributes != NULL );

    reportedBenchmarks = sctCreateBenchmarkList();
    CHECK( reportedBenchmarks != NULL );
        
    benchmark1 = sctCreateBenchmark( "benchmark1",
                                     attributes1,
                                     1,
                                     initActions1,
                                     benchmarkActions1,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    benchmark2 = sctCreateBenchmark( "benchmark1",
                                     attributes2,
                                     1,
                                     initActions2,
                                     benchmarkActions2,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    
    action1 = sctCreateAction( "action1", "type", "module",
                               action1Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );
    action2 = sctCreateAction( "action2", "type", "module",
                               action2Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );

    context = sctInitialize();
    CHECK( context != NULL );

    reportBenchmarkFails = SCT_TRUE;
    sctCycle( context );
    reportBenchmarkFails = SCT_FALSE;

    sctTerminate( context );

    sctDestroyList( reportedBenchmarks->list );
    reportedBenchmarks->list = NULL;
    sctDestroyBenchmarkList( reportedBenchmarks );
    reportedBenchmarks = NULL;



    attributes1                 = sctCreateAttributeList();
    attributes2                 = sctCreateAttributeList();
    initActions1[ 0 ]           = sctCreateActionList();
    initActions2[ 0 ]           = sctCreateActionList();
    benchmarkActions1[ 0 ]      = sctCreateActionList();
    benchmarkActions2[ 0 ]      = sctCreateActionList();
    action1Attributes           = sctCreateAttributeList();
    action2Attributes           = sctCreateAttributeList();

    CHECK( attributes1 != NULL );
    CHECK( attributes2 != NULL );
    CHECK( initActions1[ 0 ] != NULL );
    CHECK( initActions2[ 0 ] != NULL );
    CHECK( benchmarkActions1[ 0 ] != NULL );
    CHECK( benchmarkActions2[ 0 ] != NULL );
    CHECK( action1Attributes != NULL );
    CHECK( action2Attributes != NULL );

    reportedBenchmarks = sctCreateBenchmarkList();
    CHECK( reportedBenchmarks != NULL );
        
    benchmark1 = sctCreateBenchmark( "benchmark1",
                                     attributes1,
                                     1,
                                     initActions1,
                                     benchmarkActions1,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    benchmark2 = sctCreateBenchmark( "benchmark1",
                                     attributes2,
                                     1,
                                     initActions2,
                                     benchmarkActions2,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    
    action1 = sctCreateAction( "action1", "type", "module",
                               action1Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );
    action2 = sctCreateAction( "action2", "type", "module",
                               action2Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );

    context = sctInitialize();
    CHECK( context != NULL );

    reportActionsFails = SCT_TRUE;
    sctCycle( context );
    reportActionsFails = SCT_FALSE;

    sctTerminate( context );

    sctDestroyList( reportedBenchmarks->list );
    reportedBenchmarks->list = NULL;
    sctDestroyBenchmarkList( reportedBenchmarks );
    reportedBenchmarks = NULL;


    // *****
    attributes1                 = sctCreateAttributeList();
    attributes2                 = sctCreateAttributeList();
    initActions1[ 0 ]           = sctCreateActionList();
    initActions2[ 0 ]           = sctCreateActionList();
    benchmarkActions1[ 0 ]      = sctCreateActionList();
    benchmarkActions2[ 0 ]      = sctCreateActionList();
    action1Attributes           = sctCreateAttributeList();
    action2Attributes           = sctCreateAttributeList();

    CHECK( attributes1 != NULL );
    CHECK( attributes2 != NULL );
    CHECK( initActions1[ 0 ] != NULL );
    CHECK( initActions2[ 0 ] != NULL );
    CHECK( benchmarkActions1[ 0 ] != NULL );
    CHECK( benchmarkActions2[ 0 ] != NULL );
    CHECK( action1Attributes != NULL );
    CHECK( action2Attributes != NULL );

    reportedBenchmarks = sctCreateBenchmarkList();
    CHECK( reportedBenchmarks != NULL );
        
    benchmark1 = sctCreateBenchmark( "benchmark1",
                                     attributes1,
                                     1,
                                     initActions1,
                                     benchmarkActions1,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    benchmark2 = sctCreateBenchmark( "benchmark1",
                                     attributes2,
                                     1,
                                     initActions2,
                                     benchmarkActions2,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    
    action1 = sctCreateAction( "action1", "type", "module",
                               action1Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );
    action2 = sctCreateAction( "action2", "type", "module",
                               action2Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );

    context = sctInitialize();
    CHECK( context != NULL );

     benchmarkRunCreationFails = SCT_TRUE;
     sctCycle( context );
     benchmarkRunCreationFails = SCT_FALSE;

    sctTerminate( context );

    sctDestroyList( reportedBenchmarks->list );
    reportedBenchmarks->list = NULL;
    sctDestroyBenchmarkList( reportedBenchmarks );
    reportedBenchmarks = NULL;


    
    // *****
    attributes1                 = sctCreateAttributeList();
    attributes2                 = sctCreateAttributeList();
    initActions1[ 0 ]           = sctCreateActionList();
    initActions2[ 0 ]           = sctCreateActionList();
    benchmarkActions1[ 0 ]      = sctCreateActionList();
    benchmarkActions2[ 0 ]      = sctCreateActionList();
    action1Attributes           = sctCreateAttributeList();
    action2Attributes           = sctCreateAttributeList();

    CHECK( attributes1 != NULL );
    CHECK( attributes2 != NULL );
    CHECK( initActions1 != NULL );
    CHECK( initActions2 != NULL );
    CHECK( benchmarkActions1 != NULL );
    CHECK( benchmarkActions2 != NULL );
    CHECK( action1Attributes != NULL );
    CHECK( action2Attributes != NULL );

    reportedBenchmarks = sctCreateBenchmarkList();
    CHECK( reportedBenchmarks != NULL );
        
    benchmark1 = sctCreateBenchmark( "benchmark1",
                                     attributes1,
                                     1,
                                     initActions1,
                                     benchmarkActions1,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    benchmark2 = sctCreateBenchmark( "benchmark1",
                                     attributes2,
                                     1,
                                     initActions2,
                                     benchmarkActions2,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    
    action1 = sctCreateAction( "action1", "type", "module",
                               action1Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );
    action2 = sctCreateAction( "action2", "type", "module",
                               action2Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );

    context = sctInitialize();
    CHECK( context != NULL );

     benchmarkRunCycleFails = SCT_TRUE;
     sctCycle( context );
     benchmarkRunCycleFails = SCT_FALSE;

    sctTerminate( context );

    sctDestroyList( reportedBenchmarks->list );
    reportedBenchmarks->list = NULL;
    sctDestroyBenchmarkList( reportedBenchmarks );
    reportedBenchmarks = NULL;

     if( benchmark1 != NULL )
     {
         sctDestroyBenchmark( benchmark1 );
         benchmark1 = NULL;
     }
     if( benchmark2 != NULL )
     {
         sctDestroyBenchmark( benchmark2 );
        benchmark2 = NULL;
     }
     if( action1 != NULL )
     {
         sctDestroyAction( action1 );
         action1 = NULL;
     }
     if( action2 != NULL )
     {
         sctDestroyAction( action2 );
         action2 = NULL;
     }
       
     reportActionsFails = SCT_FALSE;


    // *****
    attributes1                 = sctCreateAttributeList();
    attributes2                 = sctCreateAttributeList();
    initActions1[ 0 ]           = sctCreateActionList();
    initActions2[ 0 ]           = sctCreateActionList();
    benchmarkActions1[ 0 ]      = sctCreateActionList();
    benchmarkActions2[ 0 ]      = sctCreateActionList();
    action1Attributes           = sctCreateAttributeList();
    action2Attributes           = sctCreateAttributeList();

    CHECK( attributes1 != NULL );
    CHECK( attributes2 != NULL );
    CHECK( initActions1[ 0 ] != NULL );
    CHECK( initActions2[ 0 ] != NULL );
    CHECK( benchmarkActions1[ 0 ] != NULL );
    CHECK( benchmarkActions2[ 0 ] != NULL );
    CHECK( action1Attributes != NULL );
    CHECK( action2Attributes != NULL );

    reportedBenchmarks = sctCreateBenchmarkList();
    CHECK( reportedBenchmarks != NULL );
        
    benchmark1 = sctCreateBenchmark( "benchmark1",
                                     attributes1,
                                     1,
                                     initActions1,
                                     benchmarkActions1,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    benchmark2 = sctCreateBenchmark( "benchmark1",
                                     attributes2,
                                     1,
                                     initActions2,
                                     benchmarkActions2,
                                     finalActions,
                                     preTerminateActions,
                                     terminateActions );
    
    action1 = sctCreateAction( "action1", "type", "module",
                               action1Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );
    action2 = sctCreateAction( "action2", "type", "module",
                               action2Attributes,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );

    context = sctInitialize();
    CHECK( context != NULL );

     benchmarkRunCreationFails = SCT_TRUE;
     reportBenchmarkFails = SCT_TRUE;
     sctCycle( context );
     benchmarkRunCreationFails = SCT_FALSE;
     reportBenchmarkFails = SCT_FALSE;

    sctTerminate( context );

    sctDestroyList( reportedBenchmarks->list );
    reportedBenchmarks->list = NULL;
    sctDestroyBenchmarkList( reportedBenchmarks );
    reportedBenchmarks = NULL;

    

//     /* Now, check out of memory. */

    // Check that system error log is not set.
    CHECK( sctGetErrorLog() == NULL );

    for( i = 0; i < initAllocCount; ++i )
    {
        tsiCommonSetAllocFailThreshold( -1, -1 );

        attributes1                 = sctCreateAttributeList();
        attributes2                 = sctCreateAttributeList();
        initActions1[ 0 ]           = sctCreateActionList();
        initActions2[ 0 ]           = sctCreateActionList();
        benchmarkActions1[ 0 ]      = sctCreateActionList();
        benchmarkActions2[ 0 ]      = sctCreateActionList();
        action1Attributes           = sctCreateAttributeList();
        action2Attributes           = sctCreateAttributeList();
        CHECK( attributes1 != NULL );
        CHECK( attributes2 != NULL );
        CHECK( initActions1[ 0 ] != NULL );
        CHECK( initActions2[ 0 ] != NULL );
        CHECK( benchmarkActions1[ 0 ] != NULL );
        CHECK( benchmarkActions2[ 0 ] != NULL );
        CHECK( action1Attributes != NULL );
        CHECK( action2Attributes != NULL );

        benchmark1 = sctCreateBenchmark( "benchmark1",
                                         attributes1,
                                         1,
                                         initActions1,
                                         benchmarkActions1,
                                         finalActions,
                                         preTerminateActions,
                                         terminateActions );

        benchmark2 = sctCreateBenchmark( "benchmark1",                                      
                                         attributes2,
                                         1,
                                         initActions2,
                                         benchmarkActions2,
                                         finalActions,
                                         preTerminateActions,
                                         terminateActions );

        action1 = sctCreateAction( "action1", "type", "module",
                                   action1Attributes,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL );
        
        action2 = sctCreateAction( "action2", "type", "module",
                                   action2Attributes,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL );

        tsiCommonSetAllocFailThreshold( i, -1 );
        CHECK( sctInitialize() == NULL );

        if( benchmark1 != NULL )
        {
            sctDestroyBenchmark( benchmark1 );
            benchmark1 = NULL;
        }
        if( benchmark2 != NULL )
        {
            sctDestroyBenchmark( benchmark2 );
            benchmark2 = NULL;
        }
        if( action1 != NULL )
        {
            sctDestroyAction( action1 );
            action1 = NULL;
        }
        if( action2 != NULL )
        {
            sctDestroyAction( action2 );
            action2 = NULL;
        }
    }

    tsiCommonSetAllocFailThreshold( -1, -1 );
    CHECK( sctGetErrorLog() == NULL );

     for( i = initAllocCount; i < initAllocCount + cycleAllocCount; ++i )
     {
        tsiCommonSetAllocFailThreshold( -1, -1 );

        attributes1                 = sctCreateAttributeList();
        attributes2                 = sctCreateAttributeList();
        initActions1[ 0 ]           = sctCreateActionList();
        initActions2[ 0 ]           = sctCreateActionList();
        benchmarkActions1[ 0 ]      = sctCreateActionList();
        benchmarkActions2[ 0 ]      = sctCreateActionList();
        action1Attributes           = sctCreateAttributeList();
        action2Attributes           = sctCreateAttributeList();
        CHECK( attributes1 != NULL );
        CHECK( attributes2 != NULL );
        CHECK( initActions1[ 0 ] != NULL );
        CHECK( initActions2[ 0 ] != NULL );
        CHECK( benchmarkActions1[ 0 ] != NULL );
        CHECK( benchmarkActions2[ 0 ] != NULL );
        CHECK( action1Attributes != NULL );
        CHECK( action2Attributes != NULL );

        benchmark1 = sctCreateBenchmark( "benchmark1",
                                         attributes1,
                                         1,
                                         initActions1,
                                         benchmarkActions1,
                                         finalActions,
                                         preTerminateActions,
                                         terminateActions );
        
        benchmark2 = sctCreateBenchmark( "benchmark1",
                                         attributes2,
                                         1,
                                         initActions2,
                                         benchmarkActions2,
                                         finalActions,
                                         preTerminateActions,
                                         terminateActions );

        action1 = sctCreateAction( "action1", "type", "module",
                                   action1Attributes,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL );
        
        action2 = sctCreateAction( "action2", "type", "module",
                                   action2Attributes,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL );

        tsiCommonSetAllocFailThreshold( i, -1 );

        reportedBenchmarks = sctCreateBenchmarkList();
        CHECK( reportedBenchmarks != NULL );

        tsiCommonSetAllocFailThreshold( i, -1 );
        context = sctInitialize();
        CHECK( context != NULL );
        while( ( status = sctCycle( context ) ) == SCT_CYCLE_CONTINUE );
        sctTerminate( context );

        if( benchmark1 != NULL )
        {
            sctDestroyBenchmark( benchmark1 );
        }
        if( benchmark2 != NULL )
        {
            sctDestroyBenchmark( benchmark2 );
        }
        if( action1 != NULL )
        {
            sctDestroyAction( action1 );
        }
        if( action2 != NULL )
        {
            sctDestroyAction( action2 );
        }
        sctDestroyList( reportedBenchmarks->list );
        reportedBenchmarks->list = NULL;
        sctDestroyBenchmarkList( reportedBenchmarks );
        reportedBenchmarks = NULL;
    }
   
    sctTerminate( NULL );
}

int main( void )
{
 	TestResult tr;

    tsiCommonInitSystem();

	int s = !TestRegistry::runAllTests(tr);

    tsiCommonDestroySystem();

    return s;
}
