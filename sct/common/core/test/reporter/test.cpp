/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct.h"
#include "sct_reporter.h"
#include "sct_utils.h"
#include "sct_benchmark.h"
#include "sct_module.h"
#include "sct_action.h"
#include "sct_result.h"

#include "TestHarness.h"
#include "sicommon_mock.h"

#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#pragma warning (disable: 4221)
#pragma warning (disable: 4514)

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


    SCTStringList* actionGetWorkloadMethod( SCTAction *action )
    {
        SCTStringList   *workloads;

        ( void )action;

        workloads = sctCreateStringList();
        if( workloads == NULL )
        {
            return NULL;
        }
        
        if( sctAddStringCopyToList( workloads, "workload1" ) == SCT_FALSE ||
            sctAddStringCopyToList( workloads, "workload2" ) == SCT_FALSE )
        {
            sctDestroyStringList( workloads );
            return NULL;
        }

        return workloads;
    }

    SCTStringList* actionGetNULLWorkloadMethod( SCTAction *action )
    {
        SCT_USE_VARIABLE( action );
        return NULL;
    }


#ifdef __cplusplus
}
#endif /* __cplusplus */


char expectedModuleInformation[] = "\
[module1: MODULE]\n\
\tname1                         : \"value1, with comma\"\n\
\tname2                         : value2\n\n\
[module2: MODULE]\n\
\tname1                         : \"value1, with comma\"\n\
\tname2                         : value2\n\n";

char expectedActionInformation[] = "\
[action1: type1@module1]\n\
\tname1                         : \"value1, with comma\"\n\
\tname2                         : value2\n\
\tWorkloads                     : workload1,workload2\n\n\
[action2: type2@module2]\n\
\tname1                         : \"value1, with comma\"\n\
\tname2                         : value2\n\n";


char expectedBenchmarkResult[] = "\
[benchmark: BENCHMARK]\n\
\tInitActions                   : Action1+Action2\n\
\tBenchmarkActions              : None\n\
\tRepeats                       : 42\n\
\tTimes                         : 1.000000,2.000000\n\
\tWarnings                      : warning1,\"warning2, with comma\"\n\
\tError                         : \"error1, with comma\"\n\n";

char expectedBenchmarkResult2[] = "\
[benchmark: BENCHMARK]\n\
\tInitActions                   : None\n\
\tBenchmarkActions              : None\n\
\tFoo                           : Bar\n\
\tRepeats                       : -1\n\
\tTimes                         : 1.000000,2.000000\n\
\tWarnings                      : warning1,\"warning2, with comma\"\n\
\tError                         : \"error1, with comma\"\n\n";



/* */

SCTAttributeList* moduleInfoMethod( SCTModule *module )
{
    SCTAttributeList *attributes;

    ( void )module;

    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        return NULL;
    }
    if( sctAddNameValueToList( attributes, "name1", "value1, with comma" ) != SCT_TRUE ||
        sctAddNameValueToList( attributes, "name2", "value2" ) != SCT_TRUE )
    {
        sctDestroyAttributeList( attributes );
        return NULL;
    }

    return attributes;
}

void moduleDestroyMethod( SCTModule *module )
{
    ( void )module;
}

int *context = NULL;


TEST( test, reportModuleInformation )
{
    SCTModuleList       *list;
    SCTModule           *module1;
    SCTModule           *module2;
    int                 writeCount;

    CHECK( sctReportModuleInformation( NULL ) == SCT_TRUE );

    list = sctCreateModuleList();
    CHECK( list != NULL );

    module1 = sctCreateModule( "module1",
                               context,
                               "",
                               moduleInfoMethod,
                               NULL,
                               moduleDestroyMethod );
    module2 = sctCreateModule( "module2",
                               context,
                               "",
                               moduleInfoMethod,
                               NULL,
                               moduleDestroyMethod );

    CHECK( module1 != NULL );
    CHECK( module2 != NULL );

    CHECK( sctAddModuleToList( list, module1 ) == SCT_TRUE );
    CHECK( sctAddModuleToList( list, module2 ) == SCT_TRUE );

    tsiCommonResetWriteCounter();
    tsiCommonResetOutput();

    CHECK( sctReportModuleInformation( list ) == SCT_TRUE );

    writeCount = tsiCommonGetWriteCounter();

    const char *s = tsiCommonGetOutputAndReset();
    CHECK_STRINGS_EQUAL( expectedModuleInformation, s );

    for( int i = 0; i <  writeCount; ++i )
    {
        tsiCommonSetWriteFailThreshold( i, -1 );
        CHECK( sctReportModuleInformation( list ) == SCT_FALSE );
        tsiCommonResetOutput();
    }

    tsiCommonSetWriteFailThreshold( -1, -1 );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctReportModuleInformation( list ) == SCT_FALSE );
    tsiCommonSetAllocFailThreshold( -1, -1 );

    sctDestroyModuleList( list );
}


TEST( test, reportActions )
{
    SCTActionList       *actionList;
    SCTAction           *action1;
    SCTAction           *action2;
    SCTAttributeList    *attributes1;
    SCTAttributeList    *attributes2;
    int                 writeCount;

    CHECK( sctReportActions( NULL ) == SCT_TRUE );

    attributes1 = sctCreateAttributeList();
    attributes2 = sctCreateAttributeList();
    CHECK( attributes1 != NULL );
    CHECK( attributes2 != NULL );

    CHECK( sctAddNameValueToList( attributes1, "name1", "value1, with comma" ) == SCT_TRUE );
    CHECK( sctAddNameValueToList( attributes1, "name2", "value2" ) == SCT_TRUE );

    CHECK( sctAddNameValueToList( attributes2, "name1", "value1, with comma" ) == SCT_TRUE );
    CHECK( sctAddNameValueToList( attributes2, "name2", "value2" ) == SCT_TRUE );

    actionList = sctCreateActionList();
    CHECK( actionList != NULL );

    action1 = sctCreateAction( "action1", "type1", "module1",
                               attributes1,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               actionGetWorkloadMethod );
    action2 = sctCreateAction( "action2", "type2", "module2",
                               attributes2,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL );
    CHECK( action1 != NULL );
    CHECK( action2 != NULL );

    CHECK( sctAddActionToList( actionList, action1 ) == SCT_TRUE );
    CHECK( sctAddActionToList( actionList, action2 ) == SCT_TRUE );

    tsiCommonResetWriteCounter();
    tsiCommonResetOutput();

    CHECK( sctReportActions( actionList ) == SCT_TRUE );

    writeCount = tsiCommonGetWriteCounter();

    const char *s = tsiCommonGetOutputAndReset();
    CHECK_STRINGS_EQUAL( expectedActionInformation, s );

    for( int i = 0; i <  writeCount; ++i )
    {
        tsiCommonSetWriteFailThreshold( i, -1 );
        CHECK( sctReportActions( actionList ) == SCT_FALSE );
        tsiCommonResetOutput();
    }

    tsiCommonSetWriteFailThreshold( -1, -1 );

    sctDestroyActionListAndElements( actionList );
}

TEST( test, reportActions2 )
{
    SCTActionList       *actionList;
    SCTAction           *action1;
    SCTAttributeList    *attributes1;

    CHECK( sctReportActions( NULL ) == SCT_TRUE );

    attributes1 = sctCreateAttributeList();
    CHECK( attributes1 != NULL );

    CHECK( sctAddNameValueToList( attributes1, "name1", "value1, with comma" ) == SCT_TRUE );
    CHECK( sctAddNameValueToList( attributes1, "name2", "value2" ) == SCT_TRUE );

    actionList = sctCreateActionList();
    CHECK( actionList != NULL );

    action1 = sctCreateAction( "action1", "type1", "module1",
                               attributes1,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               actionGetNULLWorkloadMethod );
    CHECK( action1 != NULL );

    CHECK( sctAddActionToList( actionList, action1 ) == SCT_TRUE );

    tsiCommonResetWriteCounter();
    tsiCommonResetOutput();

    CHECK( sctReportActions( actionList ) == SCT_FALSE );

    sctDestroyActionListAndElements( actionList );
}



TEST( test, reportBenchmark )
{
    SCTAction                   *action1;
    SCTAction                   *action2;
    SCTAttributeList            *attributes1;
    SCTAttributeList            *attributes2;
    SCTBenchmark                *benchmark;
    SCTAttributeList            *attributes;
    SCTActionList*              initActions[ 1 ];
    SCTActionList*              benchmarkActions[ 1 ];
    SCTActionList*              finalActions[ 1 ];
    SCTActionList*              preTerminateActions[ 1 ];
    SCTActionList*              terminateActions[ 1 ];
    int                         writeCount;
    SCTBenchmarkResult*         benchmarkResult;
    SCTLog                     *log;

    initActions[ 0 ]         = sctCreateActionList();
    benchmarkActions[ 0 ]    = sctCreateActionList();
    finalActions[ 0 ]        = NULL ;
    preTerminateActions[ 0 ] = NULL;
    terminateActions[ 0 ]    = NULL;       
    log                      = sctCreateLog();
    
    CHECK( initActions[ 0 ] != NULL );
    CHECK( benchmarkActions[ 0 ] != NULL );

    attributes = sctCreateAttributeList();
    CHECK( attributes != NULL );

    CHECK( sctAddNameValueToList( attributes, "Repeats", "42" ) == SCT_TRUE );

    CHECK( sctReportActions( NULL ) == SCT_TRUE );

    attributes1 = sctCreateAttributeList();
    attributes2 = sctCreateAttributeList();
    CHECK( attributes1 != NULL );
    CHECK( attributes2 != NULL );
    CHECK( sctAddNameValueToList( attributes1, "name1", "value1, with comma" ) == SCT_TRUE );
    CHECK( sctAddNameValueToList( attributes2, "name1", "value1, with comma" ) == SCT_TRUE );

    action1 = sctCreateAction( "Action1", "type1", "module1",
                               attributes1,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               actionGetWorkloadMethod );
    action2 = sctCreateAction( "Action2", "type1", "module1",
                               attributes2,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               NULL,
                               actionGetWorkloadMethod );
    CHECK( action1 != NULL );
    CHECK( action2 != NULL );

    CHECK( sctAddActionToList( initActions[ 0 ], action1 ) == SCT_TRUE );
    CHECK( sctAddActionToList( initActions[ 0 ], action2 ) == SCT_TRUE );

    benchmark = sctCreateBenchmark( "benchmark",
                                    attributes,
                                    1,
                                    initActions,
                                    benchmarkActions,
                                    finalActions,
                                    preTerminateActions,
                                    terminateActions );
    
    CHECK( benchmark != NULL );

    benchmarkResult = sctCreateBenchmarkResult();
    CHECK( benchmarkResult != NULL );
    benchmarkResult->time = 1;
    CHECK( sctAddBenchmarkResultToList( benchmark->benchmarkResultList, benchmarkResult ) != SCT_FALSE );

    benchmarkResult = sctCreateBenchmarkResult();
    CHECK( benchmarkResult != NULL );
    benchmarkResult->time = 2;
    CHECK( sctAddBenchmarkResultToList( benchmark->benchmarkResultList, benchmarkResult ) != SCT_FALSE );

    // Redirect error logging to the benchmark's log
    sctSetErrorLog( benchmark->log );
    
    sctLogWarning( "warning1" );
    sctLogWarning( "warning2, with comma" );

    sctLogError( "error1, with comma" );

    tsiCommonResetWriteCounter();
    tsiCommonResetOutput();

    CHECK( sctReportBenchmark( benchmark ) == SCT_TRUE );

    writeCount = tsiCommonGetWriteCounter();

    const char *s = tsiCommonGetOutputAndReset();
    CHECK_STRINGS_EQUAL( expectedBenchmarkResult, s );

    for( int i = 0; i <  writeCount; ++i )
    {
        tsiCommonSetWriteFailThreshold( i, -1 );
        CHECK( sctReportBenchmark( benchmark ) == SCT_FALSE );
        tsiCommonResetOutput();
    }

    tsiCommonSetWriteFailThreshold( -1, -1 );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctReportBenchmark( benchmark ) == SCT_TRUE );
    tsiCommonSetAllocFailThreshold( -1, -1 );
    
    sctDestroyBenchmark( benchmark );
    sctDestroyLog( log );
    sctCleanupRegistry();
    sctDestroyAction( action1 );
    sctDestroyAction( action2 );
}


TEST( test, reportBenchmark2 )
{
    SCTBenchmark                *benchmark;
    SCTAttributeList            *attributes;
    SCTActionList*              initActions[ 1 ];
    SCTActionList*              benchmarkActions[ 1 ];
    SCTActionList*              finalActions[ 1 ];
    SCTActionList*              preTerminateActions[ 1 ];
    SCTActionList*              terminateActions[ 1 ];
    int                         writeCount;
    SCTBenchmarkResult*         benchmarkResult;
    SCTLog                     *log;

    initActions[ 0 ]         = sctCreateActionList();
    benchmarkActions[ 0 ]    = sctCreateActionList();
    finalActions[ 0 ]        = NULL ;
    preTerminateActions[ 0 ] = NULL;
    terminateActions[ 0 ]    = NULL;    
    log                      = sctCreateLog();

    CHECK( initActions[ 0 ] != NULL );
    CHECK( benchmarkActions[ 0 ] != NULL );

    attributes = sctCreateAttributeList();
    CHECK( attributes != NULL );

    CHECK( sctAddNameValueToList( attributes, "Foo", "Bar" ) == SCT_TRUE );

    benchmark = sctCreateBenchmark( "benchmark",
                                    attributes,
                                    1,
                                    initActions,
                                    benchmarkActions,
                                    finalActions,
                                    preTerminateActions,
                                    terminateActions );
    
    CHECK( benchmark != NULL );

    benchmarkResult = sctCreateBenchmarkResult();
    CHECK( benchmarkResult != NULL );
    benchmarkResult->time = 1;
    CHECK( sctAddBenchmarkResultToList( benchmark->benchmarkResultList, benchmarkResult ) != SCT_FALSE );

    benchmarkResult = sctCreateBenchmarkResult();
    CHECK( benchmarkResult != NULL );
    benchmarkResult->time = 2;
    CHECK( sctAddBenchmarkResultToList( benchmark->benchmarkResultList, benchmarkResult ) != SCT_FALSE );

    // Redirect error logging to the benchmark's log
    sctSetErrorLog( benchmark->log );
    
    sctLogWarning( "warning1" );
    sctLogWarning( "warning2, with comma" );

    sctLogError( "error1, with comma" );

    tsiCommonResetWriteCounter();
    tsiCommonResetOutput();

    CHECK( sctReportBenchmark( benchmark ) == SCT_TRUE );

    writeCount = tsiCommonGetWriteCounter();

    const char *s = tsiCommonGetOutputAndReset();
    CHECK_STRINGS_EQUAL( expectedBenchmarkResult2, s );

    for( int i = 0; i <  writeCount; ++i )
    {
        tsiCommonSetWriteFailThreshold( i, -1 );
        CHECK( sctReportBenchmark( benchmark ) == SCT_FALSE );
        tsiCommonResetOutput();
    }

    tsiCommonSetWriteFailThreshold( -1, -1 );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctReportBenchmark( benchmark ) == SCT_TRUE );
    tsiCommonSetAllocFailThreshold( -1, -1 );
    
    sctDestroyBenchmark( benchmark );
    sctDestroyLog( log );
    sctCleanupRegistry();
}


int main( void )
{
    tsiCommonInitSystem();
    TestResult tr;
	int s = !TestRegistry::runAllTests( tr );

    tsiCommonDestroySystem();

    return s;
}

