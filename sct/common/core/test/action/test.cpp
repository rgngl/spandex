/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "TestHarness.h"

#include "sicommon_mock.h"
#include "sct_action.h"
#include "sct_utils.h"

#include <string.h>

#ifdef _MSC_VER
#pragma warning (disable: 4514 )
#endif

int *context = NULL;
int destroyCalled = 0;

SCTBoolean actionInitMethod( SCTAction *action, SCTBenchmark *benchmark )
{
    ( void )action;
    ( void )benchmark;
    return SCT_TRUE;
}

SCTBoolean actionExecuteMethod( SCTAction *action, int frameNumber )
{
    ( void )action;
    ( void )frameNumber;
    return SCT_TRUE;
}

void actionTerminateMethod( SCTAction *action )
{
    ( void )action;
}

void actionDestroyMethod( SCTAction *action )
{
    ( void )action;
    destroyCalled++;
}

SCTStringList* actionGetWorkloadMethod( SCTAction *action )
{
    ( void )action;
    return NULL;
}

TEST( test, action )
{
    SCTAction                   *action;
    const char                  *name           = "name";
    const char                  *type           = "type";
    const char                  *moduleName     = "module";
    SCTAttributeList            *attributes;

    attributes = sctCreateAttributeList();
    CHECK( attributes != NULL );

    /* Test normal create and destroy. */
    action = sctCreateAction( name, type, moduleName,
                              attributes,
                              context,
                              actionInitMethod,
                              actionExecuteMethod,
                              actionTerminateMethod,
                              actionDestroyMethod,
                              actionGetWorkloadMethod );

    CHECK( action != NULL );
    CHECK( strcmp( name, action->name ) == 0 );
    CHECK( strcmp( type, action->type ) == 0 );
    CHECK( strcmp( moduleName, action->moduleName ) == 0 );
    CHECK( attributes == action->attributes );
    CHECK( context == action->context );
    CHECK( actionInitMethod == action->init );
    CHECK( actionExecuteMethod == action->execute );
    CHECK( actionTerminateMethod == action->terminate );
    CHECK( actionDestroyMethod == action->destroy );
    CHECK( actionGetWorkloadMethod == action->getWorkload );

    destroyCalled = 0;
    sctDestroyAction( action );
    CHECK( destroyCalled != 0 );

    /* Test null destroy. */
    sctDestroyAction( NULL );

    attributes = sctCreateAttributeList();
    CHECK( attributes != NULL );

    /* Test NULL function pointers. */
    action = sctCreateAction( name, type, moduleName,
                              attributes,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL );
    CHECK( action != NULL );
    sctDestroyAction( action );

    attributes = sctCreateAttributeList();
    CHECK( attributes != NULL );

    /* Check behavior when memory allocation fails. */
    tsiCommonSetAllocFailThreshold( 0, -1 );
    action = sctCreateAction( name, type, moduleName,
                              attributes,
                              context,
                              actionInitMethod,
                              actionExecuteMethod,
                              actionTerminateMethod,
                              actionDestroyMethod,
                              actionGetWorkloadMethod );
    CHECK( action == NULL );
    tsiCommonSetAllocFailThreshold( -1, -1 );

    sctDestroyAttributeList( attributes );
}

TEST( test, actionList )
{
    SCTAction                           *action1;
    SCTAction                           *action2;
    SCTAction                           *action3;
    SCTActionList                       *list;
    SCTActionListIterator               iterator;
    SCTAttributeList                    *attributes;
    SCTActionListReverseIterator        reverseIterator;


    action1 = sctCreateAction( "name", "type", "module",
                               sctCreateAttributeList(),
                               context,
                               actionInitMethod,
                               actionExecuteMethod,
                               actionTerminateMethod,
                               actionDestroyMethod,
                               actionGetWorkloadMethod );
    action2 = sctCreateAction( "name", "type", "module",
                               sctCreateAttributeList(),
                               context,
                               actionInitMethod,
                               actionExecuteMethod,
                               actionTerminateMethod,
                               actionDestroyMethod,
                               actionGetWorkloadMethod );
    action3 = sctCreateAction( "name", "type", "module",
                               sctCreateAttributeList(),
                               context,
                               actionInitMethod,
                               actionExecuteMethod,
                               actionTerminateMethod,
                               actionDestroyMethod,
                               actionGetWorkloadMethod );

    /* Test normal create and destroy. */
    list = sctCreateActionList();
    CHECK( list != NULL );
    CHECK( action1 != NULL );
    CHECK( action2 != NULL );

    sctDestroyActionList( list );

    /* Test null destroy. */
    sctDestroyActionList( NULL );
    sctDestroyActionListAndElements( NULL );

    /* Test normal operation. */
    list = sctCreateActionList();
    CHECK( list != NULL );

    CHECK( sctAddActionToList( list, action1 ) == SCT_TRUE );
    CHECK( sctAddActionToList( list, action2 ) == SCT_TRUE );
    CHECK( sctAddActionToList( list, action3 ) == SCT_TRUE );

    sctGetActionListIterator( list, &iterator );

    CHECK( sctGetNextActionListElement( &iterator ) == action1 );
    CHECK( sctGetNextActionListElement( &iterator ) == action2 );
    CHECK( sctGetNextActionListElement( &iterator ) == action3 );
    CHECK( sctGetNextActionListElement( &iterator ) == NULL );
    CHECK( sctGetNextActionListElement( &iterator ) == NULL );

    sctGetActionListReverseIterator( list, &reverseIterator );

    CHECK( sctGetPrevActionListElement( &reverseIterator ) == action3 );
    CHECK( sctGetPrevActionListElement( &reverseIterator ) == action2 );
    CHECK( sctGetPrevActionListElement( &reverseIterator ) == action1 );
    CHECK( sctGetPrevActionListElement( &reverseIterator ) == NULL );
    CHECK( sctGetPrevActionListElement( &reverseIterator ) == NULL );


    /* Make sure all action destroy methods are properly called. */
    destroyCalled = 0;
    sctDestroyActionListAndElements( list );
    CHECK( destroyCalled == 3 );

    /* Test destroying the list without destroying the actions. */
    list = sctCreateActionList();
    CHECK( list != NULL );

    action1 = sctCreateAction( "name", "type", "module",
                               sctCreateAttributeList(),
                               context,
                               actionInitMethod,
                               actionExecuteMethod,
                               actionTerminateMethod,
                               actionDestroyMethod,
                               actionGetWorkloadMethod );


    CHECK( sctAddActionToList( list, action1 ) == SCT_TRUE );

    destroyCalled = 0;
    sctDestroyActionList( list );
    CHECK( destroyCalled == 0 );

    sctDestroyAction( action1 );

    /* Check behavior when memory allocation fails. */
    tsiCommonSetAllocFailThreshold( 0, -1 );
    list = sctCreateActionList();
    CHECK( list == NULL );

    tsiCommonSetAllocFailThreshold( 1, -1 );
    list = sctCreateActionList();
    CHECK( list == NULL );

    tsiCommonSetAllocFailThreshold( -1, -1 );
    list = sctCreateActionList();

    attributes = sctCreateAttributeList();

    action1 = sctCreateAction( "name", "type", "module",
                               attributes,
                               context,
                               actionInitMethod,
                               actionExecuteMethod,
                               actionTerminateMethod,
                               actionDestroyMethod,
                               actionGetWorkloadMethod );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctAddActionToList( list, action1 ) == SCT_FALSE );

    tsiCommonSetAllocFailThreshold( -1, -1 );
    CHECK( sctAddActionToList( list, action1 ) == SCT_TRUE );

    tsiCommonSetAllocFailThreshold( -1, -1 );

    sctDestroyActionListAndElements( list );
}

SCTBoolean      gActionContextCreation  = SCT_TRUE;
int*            gModuleContext          = ( int* )( 0x1234 );
int*            gActionContextPointer   = ( int* )( 0x1235 );

void* createActionContextMethod( void* moduleContext, SCTAttributeList* attributes )
{
    ( void )moduleContext;
    ( void )attributes;
    if( gActionContextCreation  == SCT_TRUE )
    {
        return gActionContextPointer;
    }
    else
    {
        return NULL;
    }
}

void destroyActionContextMethod( void* actionContext )
{
    ( void )actionContext;
}


TEST( test, actionTemplate )
{
    SCTActionTemplate           actionTemplates[ 1 ];
    SCTAction                   *action;
    const char                  *name           = "name";
    const char                  *type           = "type";
    const char                  *moduleName     = "module";
    SCTAttributeList            *attributes;
    int                         ac, i;
    SCTLog*                     log;

    actionTemplates[ 0 ].name                              = type;
    actionTemplates[ 0 ].createActionContextMethod         = createActionContextMethod;
    actionTemplates[ 0 ].destroyActionContextMethod        = destroyActionContextMethod;
    actionTemplates[ 0 ].initMethod                        = actionInitMethod;
    actionTemplates[ 0 ].executeMethod                     = actionExecuteMethod;
    actionTemplates[ 0 ].terminateMethod                   = actionTerminateMethod;
    actionTemplates[ 0 ].destroyMethod                     = actionDestroyMethod;
    actionTemplates[ 0 ].getWorkloadMethod                 = actionGetWorkloadMethod;

    attributes = sctCreateAttributeList();
    CHECK( attributes != NULL );

    tsiCommonResetAllocCounter();
    action = sctCreateActionFromTemplate( name, type, moduleName, gModuleContext,
                                          attributes,
                                          actionTemplates,
                                          SCT_ARRAY_LENGTH( actionTemplates ) );

    ac = tsiCommonGetAllocCounter();

    CHECK( action != NULL );
    CHECK( strcmp( name, action->name ) == 0 );
    CHECK( strcmp( type, action->type ) == 0 );
    CHECK( strcmp( moduleName, action->moduleName ) == 0 );
    CHECK( attributes == action->attributes );
    CHECK( gActionContextPointer == action->context );
    CHECK( actionInitMethod == action->init );
    CHECK( actionExecuteMethod == action->execute );
    CHECK( actionTerminateMethod == action->terminate );
    CHECK( actionDestroyMethod == action->destroy );
    CHECK( actionGetWorkloadMethod == action->getWorkload );

    sctDestroyAction( action );

    attributes = sctCreateAttributeList();
    CHECK( attributes != NULL );

    log = sctCreateLog();
    sctSetErrorLog( log );

     gActionContextCreation = SCT_FALSE;
     action = sctCreateActionFromTemplate( name, type, moduleName, gModuleContext,
                                           attributes,
                                           actionTemplates,
                                           SCT_ARRAY_LENGTH( actionTemplates ) );
     CHECK( action == NULL );
     gActionContextCreation = SCT_TRUE;

     action = sctCreateActionFromTemplate( name, "NOHALOOO!", moduleName, gModuleContext,
                                           attributes,
                                           actionTemplates,
                                           SCT_ARRAY_LENGTH( actionTemplates ) );
     CHECK( action == NULL );


    /* Check behavior when memory allocation fails. */
    for( i = 0; i < ac; ++i )
    {
        sctResetLog( log );

        tsiCommonSetAllocFailThreshold( i, -1 );
        action = sctCreateActionFromTemplate( name, type, moduleName, gModuleContext,
                                              attributes,
                                              actionTemplates,
                                              SCT_ARRAY_LENGTH( actionTemplates ) );

        CHECK( action == NULL );
    }

    tsiCommonSetAllocFailThreshold( -1, -1 );

    sctDestroyAttributeList( attributes );
    sctCleanupRegistry();
    sctDestroyLog( log );
}

int main( void )
{
 	TestResult tr;

    tsiCommonInitSystem();

	int s = !TestRegistry::runAllTests(tr);

    tsiCommonDestroySystem();

    return s;
}
