/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "TestHarness.h"

#include "sicommon_mock.h"
#include "sct_result.h"

#include <string.h>

#ifdef _MSC_VER
#pragma warning (disable: 4514 )
#endif


TEST( test, benchmarkResult )
{
    SCTBenchmarkResult*         result;

    result = sctCreateBenchmarkResult();
    CHECK( result != NULL );

    sctDestroyBenchmarkResult( NULL );
    sctDestroyBenchmarkResult( result );

    /* Check behavior when memory allocation fails. */
    tsiCommonSetAllocFailThreshold( 0, -1 );
    result = sctCreateBenchmarkResult();
    CHECK( result == NULL );

    tsiCommonSetAllocFailThreshold( -1, -1 );
}

TEST( test, benchmarkResultList )
{
    SCTBenchmarkResultList*             resultList;
    SCTBenchmarkResult*                 benchmarkResult;
    SCTBenchmarkResultListIterator      iterator;

    resultList = sctCreateBenchmarkResultList();
    CHECK( resultList != NULL );

    benchmarkResult = sctCreateBenchmarkResult();
    CHECK( benchmarkResult != NULL );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    CHECK( sctAddBenchmarkResultToList( resultList, benchmarkResult ) == SCT_FALSE );
    tsiCommonSetAllocFailThreshold( -1, -1 );

    CHECK( sctAddBenchmarkResultToList( resultList, benchmarkResult ) == SCT_TRUE );

    sctGetBenchmarkResultListIterator( resultList, &iterator );
    
    CHECK( sctGetNextBenchmarkResultListElement( &iterator ) == benchmarkResult );
    CHECK( sctGetNextBenchmarkResultListElement( &iterator ) == NULL );
    CHECK( sctGetNextBenchmarkResultListElement( &iterator ) == NULL );

    sctDestroyBenchmarkResultList( NULL );
    sctDestroyBenchmarkResultList( resultList );

    tsiCommonSetAllocFailThreshold( 0, -1 );
    resultList = sctCreateBenchmarkResultList();
    CHECK( resultList == NULL );

    tsiCommonSetAllocFailThreshold( 1, -1 );
    resultList = sctCreateBenchmarkResultList();
    CHECK( resultList == NULL );

    tsiCommonSetAllocFailThreshold( -1, -1 );
}


int main( void )
{
 	TestResult tr;

    tsiCommonInitSystem();

	int s = !TestRegistry::runAllTests(tr);

    tsiCommonDestroySystem();

    return s;
}
