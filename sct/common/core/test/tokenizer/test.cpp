/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

// Disable warning "unreferenced inline/local function has been removed"
#pragma warning( disable: 4514 )

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "TestHarness.h"
#include "sicommon_mock.h"

#include "sct_tokenizer.h"
#include "sct_tokenizer_internal.h"
#include "sct_utils.h"

#define TEST_SETUP( input ) SCTTokenizerContext *ctx = sctTokenizerInit(); \
                            SCTLog *log = sctCreateLog(); \
                            char *token; \
                            CHECK( ctx != NULL ); \
                            CHECK( log != NULL ); \
                            tsiCommonSetInput( input, strlen( input ) ); \
                            sctSetErrorLog( log );
#define TEST_TEARDOWN       sctTokenizerTerminate( ctx ); \
                            sctDestroyLog( log ); \
                            sctCleanupRegistry();
#define CHECK_TOKEN( type, value ) CHECK( type == sctGetToken( ctx, &token ) ); \
                                   CHECK_STRINGS_EQUAL( value, token );

TEST( EmptyInput, sctGetToken )
{
    TEST_SETUP( "" );
    CHECK_TOKEN( TOK_END, "" );
    TEST_TEARDOWN;
}

TEST( EmptyLine, sctGetToken )
{
    TEST_SETUP( "\n" );
    CHECK_TOKEN( TOK_END, "" );
    CHECK_STRINGS_EQUAL( "", token );
    TEST_TEARDOWN;
}

TEST( Comment, sctGetToken )
{
    TEST_SETUP( "# abc" );
    CHECK_TOKEN( TOK_END, "" );
    CHECK_STRINGS_EQUAL( "", token );
    TEST_TEARDOWN;
}

TEST( Delimiters, sctGetToken )
{
    TEST_SETUP( delimiters );
    for( int i = 0; i < SCT_ARRAY_LENGTH( delimiters ); i++ )
    {
        CHECK( TOK_DELIMITER == sctGetToken( ctx, &token ) );
        CHECK_EQUAL( delimiters[i], token[0] );
    }
    CHECK_TOKEN( TOK_END, "" );
    TEST_TEARDOWN;
}

TEST( SingleString, sctGetToken )
{
    TEST_SETUP( "abc" );
    CHECK_TOKEN( TOK_STRING, "abc" );
    CHECK_TOKEN( TOK_END, "" );
    TEST_TEARDOWN;
}

TEST( Mixed, sctGetToken )
{
    TEST_SETUP( "[test:test]\ntest:test" );
    CHECK_TOKEN( TOK_DELIMITER, "[" );
    CHECK_TOKEN( TOK_STRING, "test" );
    CHECK_TOKEN( TOK_DELIMITER, ":" );
    CHECK_TOKEN( TOK_STRING, "test" );
    CHECK_TOKEN( TOK_DELIMITER, "]" );
    CHECK_TOKEN( TOK_STRING, "test" );
    CHECK_TOKEN( TOK_DELIMITER, ":" );
    CHECK_TOKEN( TOK_STRING, "test" );
    CHECK_TOKEN( TOK_END, "" );
    TEST_TEARDOWN;
}

TEST( Complex, sctGetToken )
{
    TEST_SETUP( "#comment\n[test :test@test]#comment  \n\ntest :\"test test\"#comment\ntest:test\n#comment" );
    CHECK_TOKEN( TOK_DELIMITER, "[" );
    CHECK_TOKEN( TOK_STRING, "test" );
    CHECK_TOKEN( TOK_DELIMITER, ":" );
    CHECK_TOKEN( TOK_STRING, "test" );
    CHECK_TOKEN( TOK_DELIMITER, "@" );
    CHECK_TOKEN( TOK_STRING, "test" );
    CHECK_TOKEN( TOK_DELIMITER, "]" );
    CHECK_TOKEN( TOK_STRING, "test" );
    CHECK_TOKEN( TOK_DELIMITER, ":" );
    CHECK_TOKEN( TOK_STRING, "test test" );
    CHECK_TOKEN( TOK_STRING, "test" );
    CHECK_TOKEN( TOK_DELIMITER, ":" );
    CHECK_TOKEN( TOK_STRING, "test" );
    CHECK_TOKEN( TOK_END, "" );
    TEST_TEARDOWN;
}

TEST( QuotedString, sctGetToken )
{
    TEST_SETUP( "\"test with spaces\"" );
    CHECK_TOKEN( TOK_STRING, "test with spaces" );
    CHECK_TOKEN( TOK_END, "" );
    TEST_TEARDOWN;
}

TEST( String, sctPutBackToken )
{
    TEST_SETUP( "test" );
    CHECK_TOKEN( TOK_STRING, "test" );
    sctPutBackToken( ctx );
    CHECK_TOKEN( TOK_STRING, "test" );
    CHECK_TOKEN( TOK_END, "" );
    TEST_TEARDOWN;
}
    
TEST( Delimiter, sctPutBackToken )
{
    TEST_SETUP( ":" );
    CHECK_TOKEN( TOK_DELIMITER, ":" );
    sctPutBackToken( ctx );
    CHECK_TOKEN( TOK_DELIMITER, ":" );
    CHECK_TOKEN( TOK_END, "" );
    TEST_TEARDOWN;
}

// TEST( TooLongToken, sctGetToken )
// {
//     int tokenLength = SCT_MAX_TOKEN_LEN + 1;
//     char *token = (char *) malloc( tokenLength + 1 );
//     CHECK( token != NULL );
//     for( int i = 0; i < tokenLength; i++ )
//         token[i] = 'a';
//     token[tokenLength] = '\0';
//     TEST_SETUP( token );
//     CHECK_TOKEN( TOK_ERROR, "" );
//     TEST_TEARDOWN;
//     free( token );
// }

// TEST( TooLongQuotedString, sctGetToken )
// {
//     char token[SCT_MAX_TOKEN_LEN+4];
//     token[0] = '"';
//     for( int i = 0; i < SCT_MAX_TOKEN_LEN+1; i++ )
//         token[i+1] = 'a';
//     token[SCT_MAX_TOKEN_LEN+2] = '"';
//     token[SCT_MAX_TOKEN_LEN+3] = '\0';
//     TEST_SETUP( token );
//     CHECK_TOKEN( TOK_ERROR, "" );
//     TEST_TEARDOWN;
// }   

// TEST( MaxLengthQuotedString, sctGetToken )
// {
//     char token[SCT_MAX_TOKEN_LEN+3];
//     char expected[SCT_MAX_TOKEN_LEN+1];
//     token[0] = '"';
//     for( int i = 0; i < SCT_MAX_TOKEN_LEN; i++ )
//         token[i+1] = expected[i] = 'a';
//     token[SCT_MAX_TOKEN_LEN+1] = '"';
//     token[SCT_MAX_TOKEN_LEN+2] = '\0';
//     expected[SCT_MAX_TOKEN_LEN] = '\0';
//     TEST_SETUP( token );
//     CHECK_TOKEN( TOK_STRING, expected );
//     TEST_TEARDOWN;
// }   

TEST( NonterminatedQuotedString, sctGetToken )
{
    // Test for the case when there is a legal line with some tokens and a
    // non-terminated quoted string at the beginning of the next line. Tokenizer
    // should report TOK_ERROR when it encounters the non-terminated string and
    // know the correct line and column. There was a bug that the column was not
    // correct in this situation: the column actually referred to the last
    // column of the previous line.
    TEST_SETUP( "[Test:BENCHMARK]\n\"" );
    CHECK_TOKEN( TOK_DELIMITER, "[" );
    CHECK_TOKEN( TOK_STRING, "Test" );
    CHECK_TOKEN( TOK_DELIMITER, ":" );
    CHECK_TOKEN( TOK_STRING, "BENCHMARK" );
    CHECK_TOKEN( TOK_DELIMITER, "]" );
    CHECK_TOKEN( TOK_ERROR, "" );
    CHECK_EQUAL( 2, ctx->currentLine );
    CHECK_EQUAL( 1, ctx->currentTokenStartColumn );
    TEST_TEARDOWN;
}

int main( void )
{
 	TestResult tr;

    tsiCommonInitSystem();

	TestRegistry::runAllTests(tr);

    tsiCommonDestroySystem();
    
	return 0;
}
