/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "TestHarness.h"

#include "sct_runner.h"
#include "sct_threadrunner.h"

#include "sicommon_mock.h"
#include "sct_utils.h"
#include "sct_action.h"
#include "sct_benchmark.h"
#include "sct_result.h"

#ifdef _MSC_VER
#pragma warning (disable: 4514 )
#endif

int *context1 = NULL;
int *context2 = NULL;
int *context3 = NULL;
int *context4 = NULL;
int *context5 = NULL;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


    int         initCounter             = 0;
    int         initFailThreshold       = -1;
    int         executeCounter          = 0;
    int         executeFailThreshold    = -1;
    SCTBoolean  executeFails            = SCT_FALSE;
    

    SCTBoolean actionInitMethod( SCTAction *action, SCTBenchmark *benchmark )
    {
        ( void )action;
        ( void )benchmark;

        initCounter++;

        if( initFailThreshold >= 0 && initCounter >= initFailThreshold )
        {
            sctLogError( "Init failed" );
            return SCT_FALSE;
        }
        else
        {
            return SCT_TRUE;
        }
    }
    
    SCTBoolean actionExecuteMethod( SCTAction *action, int frameNumber )
    {
        ( void )action;
        ( void )frameNumber;

        executeCounter++;

        if( executeFailThreshold >= 0 && executeCounter >= executeFailThreshold )
        {
            sctLogError( "Execute failed" );
            return SCT_FALSE;
        }
        else
        {
            return SCT_TRUE;
        }
    }
    
    void actionTerminateMethod( SCTAction *action )
    {
        ( void )action;
    }
    
    void actionDestroyMethod( SCTAction *action )
    {
        ( void )action;
    }
    
    SCTStringList* actionGetWorkloadMethod( SCTAction *action )
    {
        SCTStringList   *workloads;

        ( void )action;

        workloads = sctCreateStringList();
        if( workloads == NULL )
        {
            return NULL;
        }
        
        if( sctAddStringCopyToList( workloads, "workload1" ) == SCT_FALSE ||
            sctAddStringCopyToList( workloads, "workload2" ) == SCT_FALSE )
        {
            sctDestroyStringList( workloads );
            return NULL;
        }

        return workloads;
    }

    void  sctTriggerBenchmarkWorkerThreads( SCTBenchmarkWorkerThreads* threads, SCTWorkerThreadCommand command )
    {
        ( void )threads;
        ( void )command;
    }
    
    SCTBoolean sctSyncBenchmarkWorkerThreads( SCTBenchmarkWorkerThreads* threads, SCTWorkerThreadState expectedState )
    {
        ( void )threads;
        ( void )expectedState;
        return SCT_TRUE;
    }
    
    void sctDisableWorkerThreadWaits( void )
    {
    }

    void sctEnableWorkerThreadWaits( void )
    {

    }
    
    SCTBoolean sctHasWorkerThreads( SCTBenchmark* benchmark )
    {
        ( void )benchmark;
        return SCT_FALSE;
    }
    
    SCTBoolean sctHasWorkerThread( SCTBenchmark* benchmark, int threadIndex )
    {
        ( void )benchmark;
        ( void )threadIndex;
        return SCT_FALSE;
    }
    
    SCTBenchmarkWorkerThreads* sctCreateBenchmarkWorkerThreads( SCTBenchmark* benchmark )
    {
        ( void )benchmark;
        return NULL;
    }
    
    void sctJoinBenchmarkWorkerThreads( SCTBenchmarkWorkerThreads* threads )
    {
        ( void )threads;
    }

    SCTBoolean sctResetSignals( void )
    {
        return SCT_TRUE;
    }
    
    void sctCancelSignalWaits( void )
    {
    }
    
#ifdef __cplusplus
}
#endif /* __cplusplus */

SCTBenchmark *createBenchmark( int execCount )
{
    SCTAttributeList    *attributes1;
    SCTAttributeList    *attributes2;
    SCTAttributeList    *attributes3;
    SCTAttributeList    *attributes4;

    SCTAttributeList    *benchmarkAttributes;

    SCTAction*           initAction1;
    SCTAction*           initAction2;

    SCTAction*           benchmarkAction1;
    SCTAction*           benchmarkAction2;
   
    SCTActionList*       initActions[ 1 ];
    SCTActionList*       benchmarkActions[ 1 ];
    SCTActionList*       finalActions[ 1 ];
    SCTActionList*       preTerminateActions[ 1 ];
    SCTActionList*       terminateActions[ 1 ];
    
    char                 buffer[10];

    attributes1         = sctCreateAttributeList();
    attributes2         = sctCreateAttributeList();
    attributes3         = sctCreateAttributeList();
    attributes4         = sctCreateAttributeList();
    benchmarkAttributes = sctCreateAttributeList();
    sprintf( buffer, "%d", execCount );
    sctAddNameValueToList( benchmarkAttributes, "Repeats", buffer );

    initAction1 = sctCreateAction( "initAction1", "type", "module",
                                   attributes1,
                                   context1,
                                   actionInitMethod,
                                   actionExecuteMethod,
                                   actionTerminateMethod,
                                   actionDestroyMethod,
                                   actionGetWorkloadMethod );
    initAction2 = sctCreateAction( "initAction2", "type", "module",
                                   attributes2,
                                   context2,
                                   actionInitMethod,
                                   actionExecuteMethod,
                                   actionTerminateMethod,
                                   actionDestroyMethod,
                                   actionGetWorkloadMethod );
    benchmarkAction1 = sctCreateAction( "benchmarkAction1", "type", "module",
                                        attributes3,
                                        context3,
                                        actionInitMethod,
                                        actionExecuteMethod,
                                        actionTerminateMethod,
                                        actionDestroyMethod,
                                        actionGetWorkloadMethod );
    benchmarkAction2 = sctCreateAction( "benchmarkAction2", "type", "module",
                                        attributes4,
                                        context4,
                                        actionInitMethod,
                                        actionExecuteMethod,
                                        actionTerminateMethod,
                                        actionDestroyMethod,
                                        actionGetWorkloadMethod );

    initActions[ 0 ]      = sctCreateActionList();
    benchmarkActions[ 0 ] = sctCreateActionList();
    finalActions[ 0 ]        = NULL ;
    preTerminateActions[ 0 ] = NULL;
    terminateActions[ 0 ]    = NULL;       
    
    sctAddActionToList( initActions[ 0 ], initAction1 );
    sctAddActionToList( initActions[ 0 ], initAction2 );

    sctAddActionToList( benchmarkActions[ 0 ], benchmarkAction1 );
    sctAddActionToList( benchmarkActions[ 0 ], benchmarkAction2 );

    SCTBenchmark *benchmark = sctCreateBenchmark( "name",
                                                  benchmarkAttributes,
                                                  1,
                                                  initActions,
                                                  benchmarkActions,
                                                  finalActions,
                                                  preTerminateActions,
                                                  terminateActions );
    return benchmark;
}

int tickMultiplier = 2;

int timer()
{
    static int ticker = 1;
    ticker *= tickMultiplier;
    return ticker;
}

TEST( test1, runner )
{
    SCTBenchmark                       *benchmark;
    SCTLog                             *systemErrorLog = sctCreateLog();
    void                               *benchmarkRun;
    int                                 createAllocCount;
    int                                 runAllocCount;
    int                                 initCount;
    int                                 execCount;
    int                                 i;

    CHECK( systemErrorLog != NULL );
    sctSetErrorLog( systemErrorLog );
    
    benchmark = createBenchmark( -1 );
    CHECK( benchmark != NULL );

    tsiCommonSetTimer( timer, 8 );
    
    /* First, test normal runner operation. */
    tsiCommonResetAllocCounter();

    initCounter = 0;
    benchmarkRun = sctCreateBenchmarkRun( benchmark );
    CHECK( benchmarkRun != NULL );

    initCount = initCounter;
    CHECK(initCount > 0);

    createAllocCount = tsiCommonGetAllocCounter();

    tsiCommonResetAllocCounter();

    executeCounter = 0;
    CHECK( sctRunBenchmarkCycle( benchmarkRun ) == SCT_CYCLE_FINISHED );
    execCount = executeCounter;
    CHECK(execCount > 0);

    runAllocCount = tsiCommonGetAllocCounter();

    CHECK( sctCountListElements( benchmark->benchmarkResultList->list ) > 0 );

    sctDestroyBenchmarkRun( benchmarkRun );

    sctDestroyActionListAndElements( benchmark->mainThread.initActions );
    sctDestroyActionListAndElements( benchmark->mainThread.benchmarkActions );

    benchmark->mainThread.initActions = NULL;
    benchmark->mainThread.benchmarkActions = NULL;
    sctDestroyBenchmark( benchmark );

    /* Secondly, test creation when memory allocation fails. */
    benchmark = createBenchmark( -1 );
    CHECK( benchmark != NULL );
    // Check that the system error log has been restored.
    CHECK( systemErrorLog == sctGetErrorLog() );

    for( i = 0; i < createAllocCount; ++i )
    {
        tsiCommonSetAllocFailThreshold( i, -1 );

        benchmarkRun = sctCreateBenchmarkRun( benchmark );
        CHECK( benchmarkRun == NULL );
        
        sctResetLog( benchmark->log );
    }

    tsiCommonSetAllocFailThreshold( -1, -1 );

    /* Test what happens when action init fails. */
    for( i = 0; i < initCount; i++ )
    {
        initCounter = 0;
        initFailThreshold = i;
        benchmarkRun = sctCreateBenchmarkRun( benchmark );
        CHECK( benchmarkRun == NULL );
        // Verify that the error generated by init was recorded to the
        // benchmark's error log.
        CHECK_STRINGS_EQUAL( "Init failed", benchmark->log->error );
        // Verify that the system error log was restored.
        CHECK( systemErrorLog == sctGetErrorLog() );
        sctResetLog( benchmark->log );
    }
    initFailThreshold = -1;

    /* Test what happens when action execute fails during benchmark run
     * creation. */
    /* NOTE! Hardcoded loop counter to 2 because sctCreateBenchmarkRun calls
     * each actions execute method once and there are two bechmark actions. */
    for( i = 2; i < 4; i++ )
    {
        executeCounter = 0;
        executeFailThreshold = i;
        benchmarkRun = sctCreateBenchmarkRun( benchmark );
        CHECK( benchmarkRun == NULL );
        // Verify that the error generated by init was recorded to the
        // benchmark's error log.
        CHECK_STRINGS_EQUAL( "Execute failed", benchmark->log->error );
        // Verify that the system error log was restored.
        CHECK( systemErrorLog == sctGetErrorLog() );
        sctResetLog( benchmark->log );
    }
    executeFailThreshold = -1;

    sctDestroyBenchmarkRun( benchmarkRun );

    sctDestroyActionListAndElements( benchmark->mainThread.initActions );
    sctDestroyActionListAndElements( benchmark->mainThread.benchmarkActions );

    benchmark->mainThread.initActions = NULL;
    benchmark->mainThread.benchmarkActions = NULL;
    sctDestroyBenchmark( benchmark );
    sctDestroyLog( systemErrorLog );
    sctCleanupRegistry();
}

TEST( executeCount, runBenchmarkCycle )
{
    SCTBenchmark                       *benchmark;
    SCTLog                             *systemErrorLog = sctCreateLog();
    void                               *benchmarkRun;

    CHECK( systemErrorLog != NULL );
    sctSetErrorLog( systemErrorLog );
    
    benchmark = createBenchmark( 10 );
    CHECK( benchmark != NULL );

    tsiCommonSetTimer( timer, 8 );
    
    /* First, test normal runner operation. */
    benchmarkRun = sctCreateBenchmarkRun( benchmark );
    CHECK( benchmarkRun != NULL );

    executeCounter = 0;
    CHECK( sctRunBenchmarkCycle( benchmarkRun ) == SCT_CYCLE_FINISHED );
    // Note that there are two instances of the same action in the benchmark so
    // the action will be executed twice for each repeat, hence 20 not 10.
    CHECK_EQUAL( 20, executeCounter );
    
    sctDestroyBenchmarkRun( benchmarkRun );
    sctDestroyActionListAndElements( benchmark->mainThread.initActions );
    sctDestroyActionListAndElements( benchmark->mainThread.benchmarkActions );
    benchmark->mainThread.initActions      = NULL;
    benchmark->mainThread.benchmarkActions = NULL;
    sctDestroyBenchmark( benchmark );
    sctDestroyLog( systemErrorLog );
    sctCleanupRegistry();
}

int main( void )
{
 	TestResult tr;

    // Let the mock common system create an error log for us so we it can be
    // used during testing.
    tsiCommonInitSystem();

	int s = !TestRegistry::runAllTests(tr);

    tsiCommonDestroySystem();

    return s;
}
