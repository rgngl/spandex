/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "TestHarness.h"

#include "sct_systemmodule.h"
#include "sicommon_mock.h"
#include "sct_module.h"
#include "sct_utils.h"

#include <string.h>

#ifdef _MSC_VER
#pragma warning (disable: 4514 )
#endif

TEST( createSystemModule, SystemModule )
{
    SCTModule*          module;
    SCTAttributeList*   attributes;
    int                 i;

    tsiCommonSetAllocFailThreshold( 0, 1 );
    module = sctCreateSystemModule();
    CHECK( module == NULL );

    tsiCommonSetAllocFailThreshold( -1, -1 );
    module = sctCreateSystemModule();

    CHECK( module != NULL );

    CHECK_STRINGS_EQUAL( module->name, "SYSTEM" );
    CHECK( module->context == NULL );
    CHECK( module->info != NULL );
    CHECK( module->createAction == NULL );
    CHECK( module->destroy == NULL );

    for( i = 0; ; ++i )
    {
        tsiCommonSetAllocFailThreshold( i, 1 );
        attributes = module->info( module );
        if( attributes != NULL )
        {
            break;
        }
    }

    sctDestroyModule( module );
    sctDestroyAttributeList( attributes );

}


int main( void )
{
 	TestResult tr;

    tsiCommonInitSystem();

	int s = !TestRegistry::runAllTests(tr);

    tsiCommonDestroySystem();

    return s;
}
