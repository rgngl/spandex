/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_action.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <string.h>

/*!
 * Create an action.
 *
 * \param name Name of the action. Must be defined.
 * \param type Type of the action. Must be defined.
 * \param moduleName Name of the action module. Must be defined.
 *\ param attributes Action attributes. Must be defined. Action takes responsibility of the attribute list.
 * \param context Module context. Ignored if NULL.
 * \param init Pointer to action init function.
 * \param execute Pointer to action execute function.
 * \param terminate Pointer to action terminate function.
 * \param destroy Pointer to action destroy function.
 * \param getResult Pointer to action get result function.
 * 
 * \return Newly created acdtion, or NULL if failure.
 *
 */
SCTAction* sctCreateAction( const char* name,
                            const char* type,
                            const char* moduleName,
                            SCTAttributeList* attributes,
                            void* context,
                            SCTActionInitMethod init,
                            SCTActionExecuteMethod execute,
                            SCTActionTerminateMethod terminate,
                            SCTActionDestroyMethod destroy,
                            SCTActionGetWorkloadMethod getWorkload )
{
    SCTAction* action;

    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( moduleName != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    action = ( SCTAction* )( siCommonMemoryAlloc( NULL, sizeof( SCTAction ) ) );
    if( action == NULL )
    {
        return NULL;
    }
    memset( action, 0, sizeof( SCTAction ) );

    if( ( action->name = sctDuplicateString( name ) ) == NULL )
    {
        siCommonMemoryFree( NULL, action );
        return NULL;
    }
    if( ( action->type = sctDuplicateString( type ) ) == NULL )
    {
        sctDestroyAction( action );
        return NULL;
    }
    if( ( action->moduleName = sctDuplicateString( moduleName ) ) == NULL )
    {
        sctDestroyAction( action );
        return NULL;
    }

    action->attributes  = attributes;
    action->context     = context;
    action->init        = init;
    action->execute     = execute;
    action->terminate   = terminate;
    action->destroy     = destroy;
    action->getWorkload = getWorkload;

    return action;
}

/*!
 * Destroy action. Ignored if action is NULL.
 *
 * \param action Action to destroy.
 *
 */
void sctDestroyAction( SCTAction* action )
{
    if( action != NULL )
    {
        if( action->destroy != NULL )
        {
            action->destroy( action );
        }

        if( action->attributes != NULL )
            sctDestroyAttributeList( action->attributes );
        if( action->name != NULL )
            siCommonMemoryFree( NULL, action->name );
        if( action->type != NULL )
            siCommonMemoryFree( NULL, action->type );
        if( action->moduleName != NULL )
            siCommonMemoryFree( NULL, action->moduleName );

        siCommonMemoryFree( NULL, action );
    }
}

/*!
 * Create an empty action list. 
 *
 * \return Newly created list, or NULL if failure.
 *
 */
SCTActionList* sctCreateActionList()
{
    SCTList*        list;
    SCTActionList*  actionList;

    list = sctCreateList();
    if( list == NULL )
    {
        return NULL;
    }
    
    actionList = ( SCTActionList* )( siCommonMemoryAlloc( NULL, sizeof( SCTActionList ) ) );
    if( actionList == NULL )
    {
        sctDestroyList( list );
        return NULL;
    }

    actionList->list = list;
    return actionList;
}

/*!
 * Add action to action list. 
 *
 * \param list Target list. Must be defined.
 * \param attribute Action to add. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctAddActionToList( SCTActionList* list, SCTAction* action )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( action != NULL );
    
    return sctAddToList( list->list, action );
}

/*!
 * Create action list iterator.
 *
 * \param list list. Must be defined.
 * \param iterator Target iterator. Must be defined.
 *
 * \return Action list iterator, or NULL if failure.
 *
 */
void sctGetActionListIterator( SCTActionList* list, SCTActionListIterator* iterator )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( iterator != NULL );

    sctGetListIterator( list->list, &( iterator->listIterator ) );
}

/*!
 * Get action list reverse iterator.
 *
 * \param list Target list. Must be defined.
 * \param iterator Target iterator. Must be defined.
 *
 */
void sctGetActionListReverseIterator( const SCTActionList* list, SCTActionListReverseIterator* iterator )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( iterator != NULL );

    sctGetReverseListIterator( list->list, &( iterator->listIterator ) );
}

/*!
 * Get next action.
 *
 * \param iterator List iterator. Must be defined.
 *
 * \return Action, or NULL if the end of the list has been reached.
 *
 */
SCTAction* sctGetNextActionListElement( SCTActionListIterator* listIterator )
{
    SCT_ASSERT_ALWAYS( listIterator != NULL );

    return ( SCTAction* )sctGetNextListElement( &( listIterator->listIterator ) );
}

/*!
 * Get previous action.
 *
 * \param list Target list. Must be defined.
 *
 * \return Action, or NULL if the start of the list has been reached.
 *
 */
SCTAction* sctGetPrevActionListElement( SCTActionListReverseIterator* listIterator )
{
    SCT_ASSERT_ALWAYS( listIterator != NULL );

    return ( SCTAction* )sctGetPrevListElement( &( listIterator->listIterator ) );
}

/*!
 * Destroy action list. This function DOES NOT destroy the list
 * elements.
 *
 * \param list Target list.
 *
 */
void sctDestroyActionList( SCTActionList* list )
{
    if( list == NULL )
    {
        return;
    }

    sctDestroyList( list->list );
    siCommonMemoryFree( NULL, list );
}

/*!
 * Destroy action list. This function also destroys the list elements.
 *
 * \param list Target list.
 *
 */
void sctDestroyActionListAndElements( SCTActionList* list )
{
    SCTActionListIterator   iterator;
    SCTAction*              action;

    if( list == NULL )
    {
        return;
    }

    sctGetActionListIterator( list, &iterator );
    while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
    {
        sctDestroyAction( action );
    }

    sctDestroyActionList( list );
}

/*!
 * Create action from template. This function is used by the
 * automatically created code which creates module actions.
 *
 * \param actionName Name of the action. Must be defined.
 * \param actionType Type of the action. Must be defined.
 * \param moduleName Name of the action module. Must be defined.
 * \param moduleContext Module context, or NULL if not used.
 * \param attributes List of action attributes. Must be defined.
 * \param templates Action template array to use for action creation. Must be defined.
 * \param templateCount Nunber of actions in the action template array. Must be > 0.
 *
 * \return Pointer to newly created action, or NULL if error.
 *
 */
SCTAction* sctCreateActionFromTemplate( const char* actionName, const char* actionType, const char* moduleName, void* moduleContext, SCTAttributeList* attributes, const SCTActionTemplate* templates, int templateCount )
{
    int         i;
    SCTAction*  action;
    void*       actionContext;    
    char        buf[ 64 ];

    SCT_ASSERT_ALWAYS( actionName != NULL );
    SCT_ASSERT_ALWAYS( actionType != NULL );
    SCT_ASSERT_ALWAYS( moduleName != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );
    SCT_ASSERT_ALWAYS( templates != NULL );
    SCT_ASSERT_ALWAYS( templateCount > 0 );

    for( i = 0; i < templateCount; ++i )
    {
        if( strcmp( actionType, templates[ i ].name ) == 0 )
        {
            SCT_ASSERT( templates[ i ].createActionContextMethod != NULL );
            SCT_ASSERT( templates[ i ].destroyActionContextMethod != NULL );

            actionContext = ( templates[ i ].createActionContextMethod )( moduleContext, attributes );
            if( actionContext == NULL )
            {
                return NULL;
            }
            action = sctCreateAction( actionName, 
                                      actionType,
                                      moduleName,
                                      attributes,
                                      actionContext,
                                      templates[ i ].initMethod,
                                      templates[ i ].executeMethod,
                                      templates[ i ].terminateMethod,
                                      templates[ i ].destroyMethod,
                                      templates[ i ].getWorkloadMethod );
            if( action == NULL )
            {
                sctStrncopy( buf, templates[ i ].name, 16 );
                sctStrncat( buf, "@", 1 );
                sctStrncat( buf, moduleName, 16 );

                sctStrncat( buf, " action creation failed.", 30 );
                SCT_LOG_ERROR( buf );

                ( templates[ i ].destroyActionContextMethod )( actionContext );
                return NULL;
            }
            else
            {
                return action;
            }
        }
    }

    strcpy( buf, "Unknown action " );
    sctStrncat( buf, actionType, 16 );
    strcat( buf, "@" );
    sctStrncat( buf, moduleName, 16 );
    strcat( buf, "." );
    SCT_LOG_ERROR( buf );

    return NULL;
}
