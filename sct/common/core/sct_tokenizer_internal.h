/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_TOKENIZER_INTERNAL_H__ )
#define __SCT_TOKENIZER_INTERNAL_H__

#include "sct_sicommon.h"

/*!
 *
 *
 */
struct _SCTTokenizerContext
{
    int             currentLine;
    int             currentColumn;
    int             currentTokenStartColumn;
    char            lastChar;
    SCTBoolean      characterWasPutBack;
    char*           lastToken;
    SCTTokenType    lastTokenType;
    char*           tokenBuffer;
    int             tokenBufferSize;
    SCTBoolean      tokenWasPutBack;
    char            release[ SCT_MAX_RELEASE_LENGTH + 1 ];
};

#if defined( __cplusplus )
extern "C" {
#endif /* defined( __cplusplus ) */

    /* Prototypes for module's internal functions. */
    SCTTokenType sctiGetTokenFromInput( SCTTokenizerContext* context );
    SCTBoolean   sctiIncreaseTokenBufferSize( SCTTokenizerContext* context );
    SCTBoolean   sctiIsDelimiter( char c );
    SCTBoolean   sctiIsWhitespace( char c );
    void         sctiPutBackChar( SCTTokenizerContext* context );
    char         sctiGetNextChar( SCTTokenizerContext* context );
    SCTTokenType sctiReadQuotedString( SCTTokenizerContext* context );
    void         sctiSkipWhitespaceAndComments( SCTTokenizerContext* context );

#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif  /* __SCT_TOKENIZER_INTERNAL_H__ */
    
