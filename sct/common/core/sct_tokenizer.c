/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_tokenizer.h"
#include "sct_tokenizer_internal.h"
#include "sct_utils.h"
#include "sct_version.h"

#include <stdio.h>
#include <string.h>

/*!
 * Initializes Tokenizer.
 *
 * \return Tokenizer context.
 *
 */
SCTTokenizerContext* sctTokenizerInit( void )
{
	SCTTokenizerContext*    context;

	context = ( SCTTokenizerContext* )( siCommonMemoryAlloc( 0, sizeof( SCTTokenizerContext ) ) );
	if( context == NULL )
	{
        SCT_LOG_ERROR( "Tokenizer init failed: out of memory" );
		return NULL;
	}

    /* Initial token buffer size of 4 Kb should be enough for most cases. */
    context->tokenBufferSize = 4096;
    context->tokenBuffer     = ( char* )( siCommonMemoryAlloc( NULL, context->tokenBufferSize ) );
    
    if( context->tokenBuffer == NULL )
    {
        SCT_LOG_ERROR( "Tokenizer init failed: could not allocate token buffer" );
        siCommonMemoryFree( NULL, context );
        return NULL;
    }
    
    context->currentLine             = 1;
    context->currentColumn           = 1;
    context->currentTokenStartColumn = 1;
    context->lastChar                = '\0';
    context->characterWasPutBack     = SCT_FALSE;
    context->lastTokenType           = TOK_ERROR;
    context->tokenWasPutBack         = SCT_FALSE;
    context->release[ 0 ]            = '\0';
    
	return context;
}

/*!
 * Release tokenizer context after it is no longer used.
 *
 * \param	context		Tokenizer context
 *
 */
void sctTokenizerTerminate( SCTTokenizerContext* context )
{
	SCT_ASSERT( context != NULL );
    siCommonMemoryFree( NULL, context->tokenBuffer );
	siCommonMemoryFree( NULL, context );
}

/*!
 * Get next token. The contents of the output buffer will be changed even if
 * there is an error. The output buffer will have NULL at the first character
 * location to make it a zero-lenght string.
 *
 * \param	ctx         Tokenizer context
 * \param	token       Output variable for returning a pointer to the token 
 *
 * \return	Token type. In error case TOK_ERROR is returned 
 */
SCTTokenType sctGetToken( SCTTokenizerContext* ctx, const char** token )
{
	SCTTokenizerContext*    context;

    context = ( SCTTokenizerContext* )( ctx );
    
	SCT_ASSERT( context != NULL );
	SCT_ASSERT( token != NULL );

    /* Check if last token was put back */
    if( context->tokenWasPutBack == SCT_TRUE )
    {
        context->tokenWasPutBack = SCT_FALSE;
    }
    else
    {
        /* Record the current column as the start of the current token. Its nice
         * to know for error reporting. */
        context->currentTokenStartColumn = context->currentColumn;

        /* Save token type and value to context so they can be retrieved if the
         * token is put back. */
        context->lastTokenType = sctiGetTokenFromInput( context );
        if( context->lastTokenType == TOK_END || context->lastTokenType == TOK_ERROR )
        {
            /* End-of-input does not have a token value and its safer to leave the
             * output buffer to indicate a zero-length string in case of an
             * error. */
            context->tokenBuffer[ 0 ] = '\0';
        }
    }
    
    *token = context->tokenBuffer;
    
    return context->lastTokenType;
}

/*!
 * Puts back the last read token so that next call to sctGetToken reports it
 * again.
 *
 * \param context   Tokenizer context pointer.
 */
void sctPutBackToken( SCTTokenizerContext* context )
{
    SCT_ASSERT( context != NULL );
    SCT_ASSERT( context->tokenWasPutBack == SCT_FALSE );
    context->tokenWasPutBack = SCT_TRUE;
}

/*!
 * Adds an error to the system error log information of the current input line
 * and column.
 *
 * \param context   Tokenizer context pointer.
 * \param error     Additional error message.
 */
void sctLogInputError( SCTTokenizerContext* context, const char* error )
{
    sprintf( context->tokenBuffer, "Error at input line %d, column %d: %s", context->currentLine, context->currentTokenStartColumn, error );
    sctLogError( context->tokenBuffer );
}

/*!
 * Check that the core release matches the input file release. Generate a
 * warning to the system log if the releases do not match. Call this function
 * after the whole input file has been parsed.
 *
 * \param context   Tokenizer context pointer.
 */
void sctTokernizerCheckRelease( SCTTokenizerContext* context )
{
    char buf[ 256 ];
            
#if defined( SCT_DEBUG )            
    siCommonDebugPrintf( NULL, "SPANDEX: core release %s, input file release %s", SCT_VERSION, context->release );
#endif  /* defined( SCT_DEBUG ) */

    if( strlen( context->release ) == 0 )
    {
        sprintf( buf, "unspecified input file release, might be incompatible with the core release %s", SCT_VERSION );

#if defined( SCT_DEBUG )            
        siCommonDebugPrintf( NULL, "SPANDEX: warning %s", buf );
#endif  /* defined( SCT_DEBUG ) */
        
        sctLogWarning( buf );
    }
    else if( strcmp( SCT_VERSION, context->release ) != 0 )
    {
        sprintf( buf, "core release %s does not match input file release %s", SCT_VERSION, context->release );
        
#if defined( SCT_DEBUG )            
        siCommonDebugPrintf( NULL, "SPANDEX: warning %s", buf );
#endif  /* defined( SCT_DEBUG ) */
                            
        sctLogWarning( buf );
    }
}

/*******************************************************************************
 * Internal functions.
 *******************************************************************************/

/*!
 * Reads of a token from the input and stores it to context->tokenBuffer.
 *
 * \param context   Tokenizer context pointer
 *
 * \return Type of the token read from the input or TOK_ERROR.
 */
SCTTokenType sctiGetTokenFromInput( SCTTokenizerContext* context )
{
    char    nextChar;
    int     bufferIndex;

    sctiSkipWhitespaceAndComments( context );

    nextChar = sctiGetNextChar( context );
    
    if( sctiIsDelimiter( nextChar ) )
    {
        context->tokenBuffer[ 0 ] = nextChar;
        context->tokenBuffer[ 1 ] = '\0';
        
        return TOK_DELIMITER;
    }
    else if( nextChar == SCT_END_OF_INPUT )
    {
        context->tokenBuffer[ 0 ] = '\0';
        
        return TOK_END;
    }
    else if( nextChar == quoteChar )
    {
        return sctiReadQuotedString( context );
    }
    
    /* This must be a string token, start reading more characters. */
    context->tokenBuffer[ 0 ] = nextChar;
    bufferIndex = 1;
    
    do
    {
        /* Is there still room in the token buffer? */
        if( bufferIndex >= context->tokenBufferSize - 2 &&
            sctiIncreaseTokenBufferSize( context ) == SCT_FALSE )
        {
            sctLogInputError( context, "could not increase token buffer length" );
            
            return TOK_ERROR;
        }
        
        nextChar = sctiGetNextChar( context );
        
        /* At this point any delimiter, white space or the comment character
         * simply means that the token we are reading ends. */
        if( sctiIsDelimiter( nextChar )  ||
            sctiIsWhitespace( nextChar ) ||
            nextChar == commentChar )
        {
            /* Character has be put back to ensure that the next token is read
             * correctly. */
            sctiPutBackChar( context );
            
            break;
        }
        
        context->tokenBuffer[bufferIndex++] = nextChar;
        
    } while( nextChar != SCT_END_OF_INPUT );
    
    /* Terminate the buffer with NULL. */
    context->tokenBuffer[ bufferIndex ] = '\0';
    
    return TOK_STRING;
}

/*!
 * Checks if the given character is one of the delimiters.
 *
 * \param c Character to check.
 *
 * \return  SCT_TRUE if c is a delimiter, SCT_FALSE otherwise.
 */
SCTBoolean sctiIsDelimiter( char c )
{
    int i;

    for( i = 0; i < SCT_ARRAY_LENGTH( delimiters ); i++ )
    {
        if( c == delimiters[ i ] )
        {
            return SCT_TRUE;
        }
    }
    
    return SCT_FALSE;
}

/*!
 * Checks if the given character is one of the white space characteres.
 *
 * \param c Character to check.
 *
 * \return  SCT_TRUE if c is a white space character, SCT_FALSE otherwise.
 */
SCTBoolean sctiIsWhitespace( char c )
{
    switch( c )
    {
    case ' ':
    case '\t':
    case '\n':
    case '\r': /* Carriage return (appears in DOS files when read in binary mode) */
        return SCT_TRUE;
        
    default:
        return SCT_FALSE;
    }
}

/*!
 * Reads all consecutive white space characters and comment strings from the
 * input.
 *
 * \param context   Tokenizer context
 * */
void sctiSkipWhitespaceAndComments( SCTTokenizerContext* context )
{
    char        nextChar;
    int         i;
    int         j;
    int         t;
    
    for(;;)
    {
        nextChar = sctiGetNextChar( context );
           
        if( nextChar == commentChar )
        {
            t = 1;
            
            /* Skip to the end-of-line or end-of-input. */
            for( i = 0, j = 0; ; ++i )
            {
                nextChar = sctiGetNextChar( context );
                if( nextChar == '\n' || nextChar == SCT_END_OF_INPUT )
                {
                    break;
                }

                if( t == 2 )    /* Collect release info into a buffer. */
                {
                    /* Skip potential spaces, tabs, carriage returns, etc. */
                    if( sctiIsWhitespace( nextChar ) == SCT_FALSE && j < SCT_MAX_RELEASE_LENGTH )
                    {
                        context->release[ j++ ] = nextChar;
                        context->release[ j ]   = '\0';
                    }
                }
                
                /* Look for the comment for release info. */
                if( t == 1 && nextChar != SCT_RELEASE_PREAMPLE[ i ] )
                {
                    t = 0;  /* Comment line does not contain release info. */
                }
                
                if( t == 1 && SCT_RELEASE_PREAMPLE[ i + 1 ] == '\0' )
                {
                    t = 2;  /* Comment line contains release info, collect it. */
                }
            }
        }
        
        if( sctiIsWhitespace( nextChar ) == SCT_FALSE )
        {
            sctiPutBackChar( context );
            return;
        }
    }
}

/*!
 * If sctiPutBackChar was called, returns the character read from the input last
 * time, otherwise returns the next character from the input stream.
 *
 * \param context   Tokenizer context pointer.
 *
 * \return The put back character or the next character from the input.
 * */
char sctiGetNextChar( SCTTokenizerContext* context )
{
    char    c;
    
    SCT_ASSERT( context != NULL );

    if( context->characterWasPutBack == SCT_TRUE )
    {
        c = context->lastChar;
        context->characterWasPutBack = SCT_FALSE;
    }
    else
    {
        c = siCommonReadChar( NULL );
    }
    if( c == '\n' )
    {
        context->currentLine            += 1;
        context->currentColumn           = 1;
        context->currentTokenStartColumn = 1;
    }
    else
    {
        context->currentColumn += 1;
    }
    
    context->lastChar = c;
    
    return c;
}

/*!
 * Makes sctiGetNextChar return the last character again, i.e., makes it look
 * like the the last character was "put back" to the input stream.
 *
 * \param context   Tokenizer context pointer.
 * */
void sctiPutBackChar( SCTTokenizerContext* context )
{
    SCT_ASSERT( context != NULL );
    SCT_ASSERT( context->characterWasPutBack == SCT_FALSE );

    if( context->lastChar == '\n' )
    {
        SCT_ASSERT( context->currentLine > 1 );
        context->currentLine  -= 1;
        context->currentColumn = 1;
    }
    else
    {
        SCT_ASSERT( context->currentColumn > 1 );
        context->currentColumn -= 1;
    }
    
    context->characterWasPutBack = SCT_TRUE;
}

/*!
 * Reads a quoted string from the input stream and stores it (without the
 * quotes) to the context->tokenBuffer. Escaped quotes are part of the quoted
 * string.
 *
 * \param context   Tokenizer context pointer.
 *
 * \return  TOK_STRING if succesfull, TOK_ERROR otherwise.
 * */
SCTTokenType sctiReadQuotedString( SCTTokenizerContext* context )
{
    int     bufferIndex     = 0;
    char    prevChar        = 0;
    char    nextChar;

    /* The opening quote has already been read from the stream. Read the input
    until there is another quote. Escaped quotes are part of the quoted
    string. If we hit a newline or the input ends, indicate an error. */
    for( ;; )
    {
        if( bufferIndex >= context->tokenBufferSize - 1 &&
            sctiIncreaseTokenBufferSize( context ) == SCT_FALSE )
        {
            sctLogInputError( context, "Could not increase token buffer size" );
            return TOK_ERROR;
        }
        
        nextChar = sctiGetNextChar( context );
        
        if( nextChar == '\n' || nextChar == SCT_END_OF_INPUT )
        {
            sctLogInputError( context, "Unterminated quoted string." );
            return TOK_ERROR;
        }

        context->tokenBuffer[ bufferIndex++ ] = nextChar;

        if( nextChar == quoteChar && prevChar != escapeChar )
        {
            break;
        }

        prevChar = nextChar;
    }
    
    /* Quote character was written to position bufferIndex-1 but it has to
     * removed so lets just write NULL over it (which also terminates the
     * string). */
    context->tokenBuffer[ bufferIndex - 1 ] = '\0';
    
    return TOK_STRING;
}

/*!
 * Tries to increase (currently, double) the token buffer size. The contents of
 * the buffer are copied to the new buffer and the old buffers memory is
 * released, if the call is succesfull.
 *
 * \param context   Tokenizer context pointer.
 *
 * \return SCT_TRUE if increasing succeeded, SCT_FALSE otherwise.
 */
SCTBoolean sctiIncreaseTokenBufferSize( SCTTokenizerContext* context )
{
    int     newSize;
    int     i;
    char*   newBuffer;
    
    SCT_ASSERT( context != NULL );
    SCT_ASSERT( context->tokenBufferSize > 0 );
    /* Kind of crazy to assert this but in this case doubling the
     * tokenBufferSize would cause an integer overflow. On the other hand at
     * this point the memory requirement is 1GB, which should be unlikely to
     * occur in practice... */
    SCT_ASSERT( context->tokenBufferSize <= 1024 * 1024 * 1024 );

    /* Try to double the amount of memory */
    newSize   = context->tokenBufferSize * 2;
    newBuffer = ( char* )( siCommonMemoryAlloc( NULL, newSize ) );
    
    if( newBuffer == NULL )
    {
        return SCT_FALSE;
    }

    /* Copy the contents of the old buffer */
    for( i = 0; i < context->tokenBufferSize; i++ )
    {
        newBuffer[ i ] = context->tokenBuffer[ i ];
    }
    
    /* Free the old buffer */
    siCommonMemoryFree( NULL, context->tokenBuffer );
    
    context->tokenBuffer     = newBuffer;
    context->tokenBufferSize = newSize;

    return SCT_TRUE;
}
