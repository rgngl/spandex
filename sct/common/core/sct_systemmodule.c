/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_systemmodule.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct.h"
#include "sct_module.h"
#include "sct_systemmodule.h"

#include <stdio.h>
#include <string.h>

/* Internal function prototypes */
static SCTAttributeList* sctiSystemModuleInfo( SCTModule* module );

/*!
 * Create system module data structure.
 *
 * \return System module data structure, or NULL if failure.
 *
 */
SCTModule* sctCreateSystemModule()
{
    return sctCreateModule( "SYSTEM",
                            NULL,
                            "",
                            sctiSystemModuleInfo,
                            NULL,
                            NULL );
}

/*!
 * Get system module information.
 *
 * \param module System module. Not used.
 *
 * \return List of system module attributes.
 *
 */
static SCTAttributeList* sctiSystemModuleInfo( SCTModule* module )
{
    SCTAttributeList*   attributeList;
    char                buf[ 128 ];
    void*               context        = siCommonGetContext();
    size_t              v;

    SCT_USE_VARIABLE( module );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctiSystemModuleInfo start" );
#endif  /* defined( SCT_DEBUG ) */
    
    /* Create empty attribute list. */
    attributeList = sctCreateAttributeList();
    if( attributeList == NULL )
    {
        return NULL;
    }

    if( sctAddNameValueToList( attributeList, "Device type", siCommonQuerySystemString( context, SCT_SYSSTRING_DEVICE_TYPE ) ) == SCT_FALSE ||
        sctAddNameValueToList( attributeList, "CPU", siCommonQuerySystemString( context, SCT_SYSSTRING_CPU ) ) == SCT_FALSE ||
        sctAddNameValueToList( attributeList, "OS", siCommonQuerySystemString( context, SCT_SYSSTRING_OS ) ) == SCT_FALSE )
    {
        sctDestroyAttributeList( attributeList );
        return NULL;
    }

    v = siCommonQueryMemoryAttribute( context, SCT_MEMATTRIB_TOTAL_SYSTEM_MEMORY );
    strcpy( buf, "-" );
    sctFormatSize_t( buf, sizeof( buf ), v );
    
    if( sctAddNameValueToList( attributeList, "Total system memory", buf ) == SCT_FALSE )
    {
        sctDestroyAttributeList( attributeList );
        return NULL;
    }

    v = siCommonQueryMemoryAttribute( context, SCT_MEMATTRIB_USED_SYSTEM_MEMORY ); 
    strcpy( buf, "-" );   
    sctFormatSize_t( buf, sizeof( buf ), v );    
    
    if( sctAddNameValueToList( attributeList, "Used system memory", buf ) == SCT_FALSE )
    {
        sctDestroyAttributeList( attributeList );
        return NULL;
    }

    v = siCommonQueryMemoryAttribute( context, SCT_MEMATTRIB_TOTAL_GRAPHICS_MEMORY );
    strcpy( buf, "-" );
    sctFormatSize_t( buf, sizeof( buf ), v );
    
    if( sctAddNameValueToList( attributeList, "Total graphics memory", buf ) == SCT_FALSE )
    {
        sctDestroyAttributeList( attributeList );
        return NULL;
    }

    v = siCommonQueryMemoryAttribute( context, SCT_MEMATTRIB_USED_GRAPHICS_MEMORY );
    strcpy( buf, "-" );
    sctFormatSize_t( buf, sizeof( buf ), v );
    
    if( sctAddNameValueToList( attributeList, "Used graphics memory", buf ) == SCT_FALSE )
    {
        sctDestroyAttributeList( attributeList );
        return NULL;
    }

    v = siCommonQueryMemoryAttribute( context, SCT_MEMATTRIB_USED_PRIVATE_GRAPHICS_MEMORY );
    strcpy( buf, "-" );
    sctFormatSize_t( buf, sizeof( buf ), v );
    
    if( sctAddNameValueToList( attributeList, "Used private graphics memory", buf ) == SCT_FALSE )
    {
        sctDestroyAttributeList( attributeList );
        return NULL;
    }

    v = siCommonQueryMemoryAttribute( context, SCT_MEMATTRIB_USED_SHARED_GRAPHICS_MEMORY );
    strcpy( buf, "-" );
    sctFormatSize_t( buf, sizeof( buf ), v );
    
    if( sctAddNameValueToList( attributeList, "Used shared graphics memory", buf ) == SCT_FALSE )
    {
        sctDestroyAttributeList( attributeList );
        return NULL;
    }

    siCommonGetTime( NULL, buf, 127 );

    /* Add date to attribute list. */
    if( sctAddNameValueToList( attributeList, "Date", buf ) == SCT_FALSE )
    {
        sctDestroyAttributeList( attributeList );
        return NULL;
    }

    /* Add SCT version to attribute list. */
    if( sctAddNameValueToList( attributeList, "Spandex release", SCT_VERSION_STRING ) == SCT_FALSE )
    {
        sctDestroyAttributeList( attributeList );
        return NULL;
    }

    /* Add timer frequency to attribute list. */
    sprintf( buf, "%lu", siCommonGetTimerFrequency( NULL ) );
    if( sctAddNameValueToList( attributeList, "Timer frequency", buf ) == SCT_FALSE )
    {
        sctDestroyAttributeList( attributeList );
        return NULL;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctiSystemModuleInfo end" );
#endif  /* defined( SCT_DEBUG ) */
    
    return attributeList;
}
