/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_reporter.h"
#include "sct_types.h"
#include "sct_utils.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_module.h"
#include "sct_result.h"

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <math.h>

/* internal helper functions */
static int sctiWriteModuleInformation( SCTModule* module );
static int sctiWriteActionInformation( SCTAction* action );

static int sctiWriteAttributeName( void* context, const char* name );
static int sctiWriteStringList( SCTStringListIterator* iterator );
static int sctiWriteActionListAttribute( void* context, const char* name, SCTActionList* actions );

static int sctiWriteSection( const char* name, const char* type, SCTAttributeList* attributes );
static int sctiWriteSectionHeader( void* context, const char* name, const char* type );
static int sctiWriteActionHeader( void* context, const char* actionName, const char* actionType, const char* actionModule );
static int sctiWriteAttributeNameAndValue( void* context, const char* name, const char* value );

static int sctiWriteString( void* context, const char* string );
static int sctiWriteQuotedString( void* context, const char* string );
static int sctiWriteRawString( void* context, const char* string );

/*!
 * Report a section to system output.
 *
 * \param name Section name. Must be defined.
 * \param type Section type. Must be defined.
 * \param attributes Section attributes. Ignored if NULL.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 */
SCTBoolean sctReportSection( const char* name, const char* type, SCTAttributeList* attributes )
{
    SCT_ASSERT( name != NULL );
    SCT_ASSERT( type != NULL );

    if( sctiWriteSection( name, type, attributes ) == 0 )
    {
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 * Report a comment string to system output
 *
 * \param commentString Comment string to write. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 */
SCTBoolean sctReportComment( const char* commentString )
{
    void*   context;
   
    SCT_ASSERT( commentString != NULL );

    context = siCommonGetContext();
    
    if( siCommonWriteChar( context, SCT_COMMENT_CHAR ) == 0 )
    {
        return SCT_FALSE;
    }

    if( sctiWriteRawString( context, commentString ) == 0 )
    {
        return SCT_FALSE;
    }

    if( siCommonWriteChar( context, '\n' ) == 0 )
    {
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 * Report information of modules in the module list to system output.
 *
 * \param modules module list. Ignored if NULL.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 */
SCTBoolean sctReportModuleInformation( SCTModuleList* modules )
{
    SCTModuleListIterator   iterator;
    SCTModule*              module; 

    if( modules == NULL )
    {
        return SCT_TRUE;
    }

    /* First, get module list iterator. */
    sctGetModuleListIterator( modules, &iterator );

    /* Then iterate over the module list, reporting each module. */
    while( ( module = sctGetNextModuleListElement( &iterator ) ) != NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: reporting \"%s\" module information", module->name );
#endif  /* defined( SCT_DEBUG ) */
        
        /* Check for potential errors. */
        if( sctiWriteModuleInformation( module ) == 0 )
        {
            return SCT_FALSE;
        }
    }
    
    /* Report success if everything went ok. */
    return SCT_TRUE;
}

/*!
 * Report actions to system output.
 *
 * \param actions Action list to report. Ignored if NULL.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 */
SCTBoolean sctReportActions( SCTActionList *actions )
{
    SCTActionListIterator   iterator;
    SCTAction*              action; 

    if( actions == NULL )
    {
        return SCT_TRUE;
    }

    /* First, get action list iterator. */
    sctGetActionListIterator( actions, &iterator );

    /* Then iterate over the module list, reporting each module. */
    while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
    {
        /* Check for potential errors. */
        if( sctiWriteActionInformation( action ) == 0 )
        {
            return SCT_FALSE;
        }
        siCommonYield( NULL );
    }

    /* Report success if everything went ok. */
    return SCT_TRUE;
}

/*!
 * Report benchmark to system output. 
 *
 * \param benchmark Benchmark to report. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 */
SCTBoolean sctReportBenchmark( SCTBenchmark* benchmark )
{
    SCTAttribute*                   attribute;
    SCTAttributeListIterator        attributeListIterator;
    SCTBenchmarkResult*             benchmarkResult;
    SCTBenchmarkResultListIterator  benchmarkResultListIterator;
    char                            buf[ 64 ];      /* Buffer for holding int/float as string. */
    int                             valueIndex;
    void*                           context;
    SCTStringListIterator           logIterator;
    int                             i;
    char                            initActionsBuf[ 64 ];
    char                            benchmarkActionsBuf[ 64 ];
    char                            finalActionsBuf[ 64 ];
    char                            preTerminateActionsBuf[ 64 ];
    char                            terminateActionsBuf[ 64 ];
    
    SCT_ASSERT( benchmark != NULL );

    context = siCommonGetContext();

    /* We write the benchmark section header and the benchmark attributes. */
    if( sctiWriteSectionHeader( context,  benchmark->name, SCT_BENCHMARK_TYPE_STRING ) == 0 )
    {
        return SCT_FALSE;
    }

    /* Write main thread init, benchmark, final, and terminate actions. */
    if( ( benchmark->mainThread.initActions != NULL &&
          sctiWriteActionListAttribute( context, SCT_BENCHMARK_INIT_ACTIONS, benchmark->mainThread.initActions ) == 0 ) ||
        sctiWriteActionListAttribute( context, SCT_BENCHMARK_BENCHMARK_ACTIONS, benchmark->mainThread.benchmarkActions ) == 0 ||
        ( benchmark->mainThread.finalActions != NULL &&
          sctiWriteActionListAttribute( context, SCT_BENCHMARK_FINAL_ACTIONS, benchmark->mainThread.finalActions ) == 0 ) ||
        ( benchmark->mainThread.preTerminateActions != NULL &&
          sctiWriteActionListAttribute( context, SCT_BENCHMARK_PRETERMINATE_ACTIONS, benchmark->mainThread.preTerminateActions ) == 0 ) ||
        ( benchmark->mainThread.terminateActions != NULL &&
          sctiWriteActionListAttribute( context, SCT_BENCHMARK_TERMINATE_ACTIONS, benchmark->mainThread.terminateActions ) == 0 ) )
        {
            return SCT_FALSE;
        }

    /* Write worker thread init, benchmark, final, and terminate actions. */    
    for( i = 0; i < SCT_MAX_WORKER_THREADS; ++i )
    {
        sprintf( initActionsBuf, "%s%d", SCT_BENCHMARK_INIT_ACTIONS, i + 1 );
        sprintf( benchmarkActionsBuf, "%s%d", SCT_BENCHMARK_BENCHMARK_ACTIONS, i + 1 );
        sprintf( finalActionsBuf, "%s%d", SCT_BENCHMARK_FINAL_ACTIONS, i + 1 );
        sprintf( preTerminateActionsBuf, "%s%d", SCT_BENCHMARK_PRETERMINATE_ACTIONS, i + 1 );
        sprintf( terminateActionsBuf, "%s%d", SCT_BENCHMARK_TERMINATE_ACTIONS, i + 1 );
        
        
        if( ( benchmark->workerThreads[ i ].initActions != NULL &&
              sctiWriteActionListAttribute( context, initActionsBuf, benchmark->workerThreads[ i ].initActions ) == 0 ) ||
            ( benchmark->workerThreads[ i ].benchmarkActions != NULL &&
              sctiWriteActionListAttribute( context, benchmarkActionsBuf, benchmark->workerThreads[ i ].benchmarkActions ) == 0 ) ||
            ( benchmark->workerThreads[ i ].finalActions != NULL &&
              sctiWriteActionListAttribute( context, finalActionsBuf, benchmark->workerThreads[ i ].finalActions ) == 0 ) ||
            ( benchmark->workerThreads[ i ].preTerminateActions != NULL &&
              sctiWriteActionListAttribute( context, preTerminateActionsBuf, benchmark->workerThreads[ i ].preTerminateActions ) == 0 ) ||
            ( benchmark->workerThreads[ i ].terminateActions != NULL &&
              sctiWriteActionListAttribute( context, terminateActionsBuf, benchmark->workerThreads[ i ].terminateActions ) == 0 ) )
        {
            return SCT_FALSE;
        }
    }
    
    if( benchmark->attributes != NULL )
    {
        sctGetAttributeListIterator( benchmark->attributes, &attributeListIterator );
        while( ( attribute = sctGetNextAttributeListElement( &attributeListIterator ) ) != NULL )
        {
            /* Skip Repeats attribute, as it is handled separately below. The
             * reason for this is that if the user specified -1 for repeats,
             * which means that a suitable repeat count is auto-detected, we
             * have to report the detected value, not the -1 that is still in
             * the attribute list. */
            if( strcmp( attribute->name, SCT_BENCHMARK_REPEATS_ATTRIBUTE ) == 0 )
            {
                continue;
            }
            
            if( sctiWriteAttributeNameAndValue( context, attribute->name, attribute->value ) == 0 )
            {
                return SCT_FALSE;
            }
        }
    }

    /* Report the actual Repeat count that was used. */
    sprintf( buf, "%d", benchmark->repeats );
    
    if( sctiWriteAttributeNameAndValue( context, SCT_BENCHMARK_REPEATS_ATTRIBUTE, buf ) == 0 )
    {
        return SCT_FALSE;
    }

    /* We write the benchmark result times. */
    sctGetBenchmarkResultListIterator( benchmark->benchmarkResultList, &benchmarkResultListIterator );

    if( sctiWriteAttributeName( context, SCT_BENCHMARK_TIMES_RESULT ) == 0 )
    {
        return SCT_FALSE;
    }
    
    valueIndex = 0;
    
    while( ( benchmarkResult = sctGetNextBenchmarkResultListElement( &benchmarkResultListIterator ) ) != NULL )
    {
        sprintf( buf, "%f", benchmarkResult->time );

        if( valueIndex > 0 )
        {
            if( sctiWriteRawString( context, "," ) == 0 )
            {
                return SCT_FALSE;
            }
        }
        
        if( sctiWriteString( context, buf ) == 0 )
        {
            return SCT_FALSE;
        }
        
        ++valueIndex;
    }
    
    if( siCommonWriteChar( context, '\n' ) == 0 )
    {
        return SCT_FALSE;
    }

    /* We write possible benchmark warnings and errors. */
    if( sctCountWarnings( benchmark->log ) > 0 )
    {
        if( sctiWriteAttributeName( context, SCT_BENCHMARK_WARNINGS_RESULT ) == 0 )
        {
            return SCT_FALSE;
        }
        
        sctGetStringListIterator( benchmark->log->warnings, &logIterator );
        
        if( sctiWriteStringList( &logIterator ) == 0 )
        {
            return SCT_FALSE;
        }
    }

    if( strlen( benchmark->log->error ) > 0 )
    {
        if( sctiWriteAttributeNameAndValue( context, SCT_BENCHMARK_ERROR_RESULT, benchmark->log->error ) == 0 )
        {
            return SCT_FALSE;
        }
    }

    if( siCommonWriteChar( context, '\n' ) == 0 )
    {
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 * Write module information to system output.
 *
 * \param module Module to write. Must be defined.
 *
 * \return Non-zero if success, zero if failure.
 */
static int sctiWriteModuleInformation( SCTModule* module )
{
    SCTAttributeList*   attributes  = NULL;
    int                 status      = 0;

    if( module->info != NULL )
    {
        attributes = module->info( module );
    }
    
    if( attributes == NULL )
    {
        return 0;
    }

    status = sctiWriteSection( module->name, SCT_MODULE_TYPE_STRING, attributes );
    
    sctDestroyAttributeList( attributes );

    return status;
}

/*!
 * Write action information to system output.
 *
 * \param action Action to write. Must be defined.
 *
 * \return Non-zero if success, zero if failure.
 */
static int sctiWriteActionInformation( SCTAction* action )
{
    SCTStringList*              workloads;
    SCTStringListIterator       stringListIterator;
    SCTAttribute*               attribute;
    SCTAttributeListIterator    attributeListIterator;
    void*                       context;

    SCT_ASSERT( action != NULL );

    context = siCommonGetContext();

    /* Write action header. */
    if( sctiWriteActionHeader( context, action->name, action->type, action->moduleName ) == 0 )
    {
        return 0;
    }

    if( action->attributes != NULL )
    {
        sctGetAttributeListIterator( action->attributes, &attributeListIterator );
        
        while( ( attribute = sctGetNextAttributeListElement( &attributeListIterator ) ) != NULL )
        {
            if( sctiWriteAttributeNameAndValue( context, attribute->name, attribute->value ) == 0 )
            {
                return 0;
            }
        }
    }

    /* Write action workload. */
    if( action->getWorkload != NULL )
    {
        if( sctiWriteAttributeName( NULL, SCT_BENCHMARK_WORKLOADS_RESULT ) == 0 )
        {
            return 0;
        }
        
        workloads = action->getWorkload( action );
        
        if( workloads == NULL )
        {
            return 0;
        }
        
        sctGetStringListIterator( workloads, &stringListIterator );
        
        if( sctiWriteStringList( &stringListIterator ) == 0 )
        {
            sctDestroyStringList( workloads );
            return 0;
        }
        
        sctDestroyStringList( workloads );
    }

    if( siCommonWriteChar( context, '\n' ) == 0 )
    {
        return 0;
    }

    return 1;
}

/* ********************************************************************** */
/* ********************************************************************** */

/*!
 * Write section to system output. Section consists of section name,
 * type, and a list of attributes.
 *
 * \param name Section name. Must be defined.
 * \param type Section type. Must be defined.
 * \param attributes Section attributes. Ignored if NULL.
 *
 * \return Non-zero if success, zero if failure.
 */
static int sctiWriteSection( const char* name, const char* type, SCTAttributeList* attributes )
{
    void*                       context     = siCommonGetContext();
    SCTAttributeListIterator    iterator;
    SCTAttribute*               attribute;

    SCT_ASSERT( name != NULL );
    SCT_ASSERT( type != NULL );

    if( sctiWriteSectionHeader( context, name, type ) == 0 )
    {
        return 0;
    }

    if( attributes != NULL )
    {
        sctGetAttributeListIterator( attributes, &iterator );
        
        while( ( attribute = sctGetNextAttributeListElement( &iterator ) ) != NULL )
        {
            if( sctiWriteAttributeNameAndValue( context, attribute->name, attribute->value ) == 0 )
            {
                return 0;
            }
        }
    }

    if( siCommonWriteChar( context, '\n' ) == 0 )
    {
        return 0;
    }

    return 1;
}

/*!
 * Write section header to output. 
 *
 * \param context Common system interface context.
 * \param name Name of the section. Must be defined
 * \param type Section type. Must be defined.
 *
 * \return Non-zero if success, zero if failure.
 *
 */
static int sctiWriteSectionHeader( void* context, const char* name, const char* type )
{
    SCT_ASSERT( name != NULL );
    SCT_ASSERT( type != NULL );

    /* Write data to system output, check for write status. */
    if( siCommonWriteChar( context, SCT_SECTION_HEADER_START ) == 0 )
    {
        return 0;
    }
    
    if( sctiWriteRawString( context, name ) == 0 )
    {
        return 0;
    }

    if( siCommonWriteChar( context, SCT_NAME_VALUE_SEPARATOR ) == 0 )
    {
        return 0;
    }

    if( siCommonWriteChar( context, ' ' ) == 0 )
    {
        return 0;
    }

    if( sctiWriteRawString( context, type ) == 0 )
    {
        return 0;
    }

    if( siCommonWriteChar( context, SCT_SECTION_HEADER_END ) == 0 )
    {
        return 0;
    }
    if( siCommonWriteChar( context, '\n' ) == 0 )
    {
        return 0;
    }

    return 1;
}

/*!
 * Write action header to output. 
 *
 * \param context Common system interface context.
 * \param name Action name. Must be defined
 * \param type Action type. Must be defined.
 * \param type Action module. Must be defined.
 *
 * \return Non-zero if success, zero if failure.
 *
 */
static int sctiWriteActionHeader( void* context, const char* actionName, const char* actionType, const char* actionModule )
{
    SCT_ASSERT_ALWAYS( actionName != NULL );
    SCT_ASSERT_ALWAYS( actionType != NULL );
    SCT_ASSERT_ALWAYS( actionModule != NULL );

    /* Write data to system output, check for write status. */
    if( siCommonWriteChar( context, SCT_SECTION_HEADER_START ) == 0 )
    {
        return 0;
    }
    
    if( sctiWriteRawString( context, actionName ) == 0 )
    {
        return 0;
    }

    if( siCommonWriteChar( context, SCT_NAME_VALUE_SEPARATOR ) == 0 )
    {
        return 0;
    }

    if( siCommonWriteChar( context, ' ' ) == 0 )
    {
        return 0;
    }

    if( sctiWriteRawString( context, actionType ) == 0 )
    {
        return 0;
    }

    if( siCommonWriteChar( context, SCT_TYPE_MODULE_SEPARATOR ) == 0 )
    {
        return 0;
    }

    if( sctiWriteRawString( context, actionModule ) == 0 )
    {
        return 0;
    }

    if( siCommonWriteChar( context, SCT_SECTION_HEADER_END ) == 0 )
    {
        return 0;
    }
    if( siCommonWriteChar( context, '\n' ) == 0 )
    {
        return 0;
    }

    return 1;
}

/*!
 * Write attribute name to system output. Attribute name is preceded
 * by tab and followed by ATTRIBUTE_SEPARATOR and a white space.
 *
 * \param context Common system interface context.
 * \param name Attribute name. Must be defined.
 *
 * \return Non-zero if success, zero if failure.
 */
int sctiWriteAttributeName( void* context, const char* name )
{
    int i;
    int nameLength;

    SCT_ASSERT( name != NULL );
    nameLength = strlen( name );
    
    if( siCommonWriteChar( context, '\t' ) == 0 )
    {
        return 0;      
    }
    
    if( sctiWriteString( context, name ) == 0 )
    {
        return 0;
    }
    
    for( i = nameLength; i < SCT_REPORT_NAME_FIELD_WIDTH; i++ )
    {
        if( siCommonWriteChar( context, ' ' ) == 0 )
        {
            return 0;
        }
    }
    
    if( siCommonWriteChar( context, SCT_NAME_VALUE_SEPARATOR ) == 0 )
    {
        return 0;
    }
    
    if( siCommonWriteChar( context, ' ' ) == 0 )
    {
        return 0;
    }
    
    return 1;
}

/*!
 * Write name-value pair to output.
 * 
 * \param context Common system interface context.
 * \param name Name string. Must be defined.
 * \param value Value string. Must be defined.
 *
 * \return Non-zero if success, zero if failure.
 */
static int sctiWriteAttributeNameAndValue( void* context, const char* name, const char* value )
{
    SCT_ASSERT( name != NULL );
    SCT_ASSERT( value != NULL );

    if( sctiWriteAttributeName( context, name ) == 0 )
    {
        return 0;
    }

    if( sctiWriteString( context, value ) == 0 )
    {
        return 0;
    }

    if( sctiWriteRawString( context, "\n" ) == 0 )
    {
        return 0;
    }

    return 1;
}

/*!
 * Write a string to output, puts quote characters (defined by QUOTATION_MARK)
 * around the string.
 * 
 * \param context       System information context
 * \param string        String
 *
 * \return 1 is success, 0 if failure. 
 */
static int sctiWriteQuotedString( void* context, const char* string )
{
    SCT_ASSERT( string != NULL );

    if( !siCommonWriteChar( context, SCT_QUOTATION_MARK ) )
    {
        return 0;
    }
    
    if( sctiWriteRawString( context, string ) == SCT_FALSE )
    {
        return 0;
    }
    
    if( !siCommonWriteChar( context, SCT_QUOTATION_MARK ) )
    {
        return 0;
    }

    return 1;
}

/*!
 * Write raw string to output. The string is written as is to the
 * output.
 * 
 * \param context Common system interface context.
 * \param string Zero-terminated string to write. Must be defined.
 *
 * \return Non-zero if success, zero if failure.
 */
static int sctiWriteRawString( void* context, const char* string )
{
    SCT_ASSERT( string != NULL );

    for( ; *string; string++ )
    {
        if( siCommonWriteChar( context, *string ) == 0 )
        {
            return 0;
        }
    }

    return 1;
}

/*!
 * Write string to output. Quotation marks are added around the string
 * if the string contains special characters.
 *
 * \param context Common system interface context.
 * \param str Zero-terminated string to write. Must be defined.
 *
 * \return Non-zero if success, zero if failure.
 */
static int sctiWriteString( void* context, const char* str )
{
    int         i;
    int         strLength;
    SCTBoolean  useQuote;

    SCT_ASSERT( str != NULL );

    /* Check value string for possible commas. If any are found we
     * have to put the string in quotes. */
    strLength = strlen( str );
    
    for( i = 0, useQuote = SCT_FALSE; i < strLength; ++i )
    {
        if( str[ i ] == ',' )
        {
            useQuote = SCT_TRUE;
            break;
        }
    }

    if( useQuote == SCT_TRUE )
    {
        if( sctiWriteQuotedString( context, str ) == 0 )
        {
            return 0;
        }
    }
    else
    {
        if( sctiWriteRawString( context, str ) == 0 )
        {
            return 0;
        }
    }

    return 1;
}

/*!
 * Write an attribute with value of specially formatted list of action
 * names. Names are separated by ACTION_LIST_SEPARATOR.
 *
 * \param context Common system interface context
 * \param name    Name of the attribute
 * \param actions List of actions whose names are written
 *
 * \return Non-zero if success, zero if failure.
 */
int sctiWriteActionListAttribute( void* context, const char* name, SCTActionList* actions )
{
    SCTActionListIterator   iter;
    SCTAction*              action;

    SCT_ASSERT( name != NULL );
    SCT_ASSERT( actions != NULL );
    
    sctGetActionListIterator( actions, &iter );
    action = sctGetNextActionListElement( &iter );

    if( sctiWriteAttributeName( context, name ) == 0 )
    {
        return 0;
    }
    
    if( action == NULL )
    {
        if( sctiWriteString( context, SCT_BENCHMARK_EMPTY_ACTIONS ) == 0 )
        {
            return 0;
        }
    }
    else
    {
        SCT_ASSERT( action->name != NULL );
        
        if( sctiWriteString( context, action->name ) == 0 )
        {
            return 0;
        }
        
        for( action = sctGetNextActionListElement( &iter );
             action != NULL;
             action = sctGetNextActionListElement( &iter ) )
        {
            SCT_ASSERT( action->name != NULL );

            /* Put action list separators before each of the subsequent action names. */           
            if( siCommonWriteChar( context, SCT_ACTION_LIST_SEPARATOR ) == 0 ||
                sctiWriteString( context, action->name ) == 0 )
            {
                return 0;
            }
        }
    }
    
    if( siCommonWriteChar( context, '\n' ) == 0 )
    {
        return 0;
    }
    
    return 1;
}

/*!
 * Write a list of strings. Strings are separated by comma. Quotation
 * marks are added around each string if they contain special
 * characters.
 *
 * \param iterator String list iterator. Must be defined.
 *
 * \return Non-zero if success, zero if failure.
 */
int sctiWriteStringList( SCTStringListIterator* iterator )
{
    int         valueIndex  = 0;
    char*       str;
    void*       context     = siCommonGetContext();

    SCT_ASSERT_ALWAYS( iterator != NULL );

    while( ( str = sctGetNextStringListElement( iterator ) ) != NULL )
    {
        if( valueIndex > 0 )
        {
            if( sctiWriteRawString( context, "," ) == 0 )
            {
                return 0;
            }
        }
        
        if( sctiWriteString( context, str ) == 0 )
        {
            return 0;
        }
        
        ++valueIndex;
    }
    
    if( sctiWriteRawString( context, "\n" ) == 0 )
    {
        return 0;
    }
    
    return 1;
}
