/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "sct_runner.h"
#include "sct_threadrunner.h"
#include "sct_sicommon.h"
#include "sct_benchmark.h"
#include "sct_action.h"
#include "sct_utils.h"
#include "sct_result.h"

/*!
 *
 *
 */
#define SAMPLE_LIMIT            1
#define RUNNER_MAX_SAMPLES      8

/*!
 * Maximum amount of iterations sctRunBenchmark will try running a benchmark for
 * in order to get accurate timing. 
 */
static const unsigned long MaxBenchmarkIterations = 0x40000000;

/*!
 *
 *
 */
typedef struct
{
    SCTBenchmark*               benchmark;
    double                      samples[ RUNNER_MAX_SAMPLES ];
    int                         sampleCount;
    SCTLog*                     previousErrorLog;
    SCTBenchmarkWorkerThreads*  workerThreads;
} SCTBenchmarkRun;

/*!
 *
 *
 */
static SCTBenchmarkThreadActions*   sctiSelectBenchmarkThreadActions( SCTBenchmark* benchmark, int threadIndex );
static double                       sctiExecuteBenchmarkMainThread( SCTBenchmark* benchmark );
static double                       sctiTicksToSeconds( unsigned long ticks );
static double                       sctiGetMinExecutionTime( void );
static SCTCycleStatus               sctiEnoughSamples( double* samples, int len );
#if defined( SCT_DEBUG )
static void                         sctiFormatThreadId( int threadIndex, char* buf, int bufLen );
#endif  /* defined( SCT_DEBUG ) */
/*!
 * Create a benchmark run that can be repeated by the runner until the result
 * satisfieds the benchmarking requirements. Internally, this function detects
 * the number of times the benchmark needs to be iterated in order for the
 * result to be reliable enough for benchmarking purposes, and therefore the
 * execution of this function may take some time.
 *
 * \param benchmark Benchmark to run. Must be defined.
 *
 * \return Newly created benchmark run, or NULL if failure.
 */
void* sctCreateBenchmarkRun( SCTBenchmark* benchmark )
{
    SCTBenchmarkRun*    benchmarkRun;
    double              t;
    double              minTime;

    SCT_ASSERT_ALWAYS( benchmark != NULL );
    SCT_ASSERT_ALWAYS( benchmark->repeats >= -1 );

    /* Allocate memory for benchmark run. */
    benchmarkRun = ( SCTBenchmarkRun* )( siCommonMemoryAlloc( NULL, sizeof( SCTBenchmarkRun ) ) );
    if( benchmarkRun == NULL )
    {
        return NULL;
    }
    
    memset( benchmarkRun, 0, sizeof( SCTBenchmarkRun ) );

    benchmarkRun->benchmark = benchmark;

    /* Store system error log and redirect all errors to the benchmark error
     * log. */
    benchmarkRun->previousErrorLog = sctGetErrorLog();
    sctSetErrorLog( benchmark->log );   
    
#if defined( SCT_INFINITE_BENCHMARK_LOOP )
    /* In infinite benchmark loop, log may contain errors from earlier benchmark
     * iteration. */
    sctResetLog( benchmark->log );
#endif  /* defined( SCT_INFINITE_BENCHMARK_LOOP ) */
    
    /* Check if the benchmark defines worker threads, if so, create them. */
    if( sctHasWorkerThreads( benchmark ) != SCT_FALSE )
    {
        if( sctResetThreads() == SCT_FALSE )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctCreateBenchmarkRun \"%s\" resetting threads failed", benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
            
            sctDestroyBenchmarkRun( benchmarkRun );
            return NULL;
        }
        
        benchmarkRun->workerThreads = sctCreateBenchmarkWorkerThreads( benchmark );
        
        if( benchmarkRun->workerThreads == NULL )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctCreateBenchmarkRun \"%s\" creating worker thread failed", benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
            
            sctDestroyBenchmarkRun( benchmarkRun );
            return NULL;
        }

        /* Set worker threads to do benchmark initialization. */
        sctTriggerBenchmarkWorkerThreads( benchmarkRun->workerThreads, SCT_WORKER_THREAD_COMMAND_INITIALIZE );
    }
    
    /* Initialize actions for the main thread. */
    if( sctInitializeActions( benchmark, -1 ) == SCT_FALSE )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctCreateBenchmarkRun \"%s\" initializing main thread actions failed", benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
        
        sctDestroyBenchmarkRun( benchmarkRun );
        return NULL;
    }

    if( benchmarkRun->workerThreads != NULL )
    {
        /* Synchronize main thread with worker threads after init. */
        if( sctSyncBenchmarkWorkerThreads( benchmarkRun->workerThreads, SCT_WORKER_THREAD_STATE_INITIALIZED ) == SCT_FALSE )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctCreateBenchmarkRun \"%s\" synchronizing to INITIALIZED failed", benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
        
            sctDestroyBenchmarkRun( benchmarkRun );
            return NULL;
        }
    }
    
#if defined( SCT_INFINITE_BENCHMARK_LOOP )
    /* In infinite benchmark loop, restore benchmark repeats value which might
     * have been changed during earlier benchmark iteration. Also set benchmark
     * as not-completed. */
    if( benchmark->repeats != benchmark->originalRepeats )
    {
        benchmark->repeats = benchmark->originalRepeats;       
    }
    benchmark->completed = 0;
#endif  /* defined( SCT_INFINITE_BENCHMARK_LOOP ) */

    if( benchmark->repeats < 0 )
    {
        /* Use the benchmark min repeat time, if defined, or detect a suitable
         * repeat count which makes the benchmark run long enough for
         * benchmarking purposes. */
        
        if( benchmark->minRepeatTime > 0.0f )
        {            
            minTime = benchmark->minRepeatTime;

#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctCreateBenchmarkRun \"%s\" benchmark, using defined min repeat time %f", benchmark->name, minTime );
#endif  /* defined( SCT_DEBUG ) */
        }
        else
        {
            minTime = sctiGetMinExecutionTime();
            
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctCreateBenchmarkRun \"%s\" benchmark, using system min repeat time %f", benchmark->name, minTime );
#endif  /* defined( SCT_DEBUG ) */
        }
        
        benchmark->repeats = SCT_MIN_ITERATIONS;

        if( benchmarkRun->workerThreads != NULL )
        {
            /* Let worker threads to start benchmark execution. */
            sctTriggerBenchmarkWorkerThreads( benchmarkRun->workerThreads, SCT_WORKER_THREAD_COMMAND_EXECUTE );
        }
        
        for( ; ; )
        {
            /* Execute benchmark main thread. */
            t = sctiExecuteBenchmarkMainThread( benchmark );

            if( t < 0.0 ) /* Check for errors. */
            {                
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctCreateBenchmarkRun \"%s\" benchmark execution failed", benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
                
                sctDestroyBenchmarkRun( benchmarkRun );
                return NULL;
            }
            else if( t >= minTime ) /* Check if the execution time is long enough. */
            {
                break;
            }

            if( benchmark->minRepeatTime < 0.0f )
            {
                /* Double the repeat count if the execution time is too short. */                
                benchmark->repeats *= 2;
            }
            else
            {
                /* Increase the repeat count less aggressively when min repeat
                 * time is defined. Otherwise we might easily get extremely high
                 * repeat counts with large min repeat times, and running the
                 * repeats would take ages. */
                benchmark->repeats += ( int ) ( ( minTime / ( t + 0.1f ) ) * benchmark->repeats );
            }

            if( ( unsigned long)( benchmark->repeats ) >= MaxBenchmarkIterations )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctCreateBenchmarkRun \"%s\" iteration overflow", benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
                
                SCT_LOG_ERROR( "Runner: iteration overflow" );
                sctDestroyBenchmarkRun( benchmarkRun );
                return NULL;
            }
        }
    }

    if( benchmarkRun->workerThreads != NULL )
    {   
        /* Suspend worker threads. */
        sctTriggerBenchmarkWorkerThreads( benchmarkRun->workerThreads, SCT_WORKER_THREAD_COMMAND_SUSPEND );

        /* We need to cancel all worker threads which migth be waiting the main
         * thread in Wait@Threads. */
        sctCancelWaits();
    
        /* Synchronize main thread with worker threads after execution. */
        if( sctSyncBenchmarkWorkerThreads( benchmarkRun->workerThreads, SCT_WORKER_THREAD_STATE_SUSPENDED ) == SCT_FALSE )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctCreateBenchmarkRun \"%s\" synchronizing to SUSPENDED failed", benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
        
            sctDestroyBenchmarkRun( benchmarkRun );
            return NULL;
        }
    }
        
    /* Everything went ok, return the benchmark run. */
    return benchmarkRun;
}

/*!
 * Run one benchmark cycle.
 *
 * \param benchmarkRun Benchmark run. Must be defined.
 *
 * \return Cycle status, SCT_CYCLE_CONTINUE, SCT_CYCLE_FINISHED,
 * SCT_CYCLE_ERROR, SCT_CYCLE_FATAL_ERROR.
 */
SCTCycleStatus sctRunBenchmarkCycle( void* benchmarkRun )
{
    SCTBenchmarkRun*    bmr;
    double              execTime;
    SCTBenchmarkResult* benchmarkResult;

    SCT_ASSERT_ALWAYS( benchmarkRun != NULL );

    bmr = ( SCTBenchmarkRun* )( benchmarkRun );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctRunBenchmarkCycle \"%s\" benchmark", bmr->benchmark->name );
#endif  /* defined( SCT_DEBUG ) */

    if( sctResetThreads() == SCT_FALSE )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctRunBenchmarkCycle \"%s\" benchmark reset threads failed", bmr->benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
        
        return SCT_CYCLE_ERROR;
    }
    
    if( bmr->workerThreads != NULL )
    {
        /* Let worker threads to start benchmark execution. */
        sctTriggerBenchmarkWorkerThreads( bmr->workerThreads, SCT_WORKER_THREAD_COMMAND_EXECUTE );
    }
    
    /* Execute benchmark main thread, record the execution time. */
    execTime = sctiExecuteBenchmarkMainThread( bmr->benchmark );

    /* Check for execution failure. */
    if( execTime < 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctRunBenchmarkCycle \"%s\" benchmark execution failed", bmr->benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
        
        return SCT_CYCLE_ERROR;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctRunBenchmarkCycle \"%s\" benchmark execution ok", bmr->benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
    
    if( bmr->workerThreads != NULL )
    {
        /* Suspend worker threads. */
        sctTriggerBenchmarkWorkerThreads( bmr->workerThreads, SCT_WORKER_THREAD_COMMAND_SUSPEND );

        /* We need to cancel all worker threads which migth be waiting the main
         * thread in Wait@Threads. */
        sctCancelWaits();

        /* Synchronize main thread with worker threads after execution. */
        if( sctSyncBenchmarkWorkerThreads( bmr->workerThreads, SCT_WORKER_THREAD_STATE_SUSPENDED ) == SCT_FALSE )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctRunBenchmarkCycle \"%s\" synchronizing to SUSPENDED failed", bmr->benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
        
            return SCT_CYCLE_ERROR;
        }
    }
    
    /* Create benchmark result for storing result. */
    benchmarkResult = sctCreateBenchmarkResult();
    
    if( benchmarkResult == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctRunBenchmarkCycle \"%s\" creating benchmark result failed", bmr->benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
        
        return SCT_CYCLE_FATAL_ERROR;
    }
    
    benchmarkResult->time = execTime;

    /* Add benchmark result to result list. */
    if( sctAddBenchmarkResultToList( bmr->benchmark->benchmarkResultList, benchmarkResult ) == SCT_FALSE )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctRunBenchmarkCycle \"%s\" adding benchmark result to list failed", bmr->benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
        
        SCT_LOG_ERROR( "Runner: result collection failed" );
        sctDestroyBenchmarkResult( benchmarkResult );
        
        return SCT_CYCLE_FATAL_ERROR;
    }

    /* Store execution time sample in the benchmark run data structure. */
    bmr->samples[ bmr->sampleCount++ ] = execTime;

    /* Decide when enough execution time samples has been collected. */
    return sctiEnoughSamples( bmr->samples, bmr->sampleCount );
}

/*!
 * Destroy benchmark run. 
 *
 * \param benchmarkRun Benchmark run to destroy. Ignored if NULL.
 */
void sctDestroyBenchmarkRun( void* benchmarkRun )
{
    SCTBenchmarkRun*    bmr;

    bmr = ( SCTBenchmarkRun* )( benchmarkRun );

    if( bmr != NULL )
    {
        if( bmr->workerThreads != NULL )
        {
            /* We must ensure worker threads go to terminate through
             * suspend. Otherwise, they might still be executing benchmark loop
             * actions while the main thread starts terminating its actions. */

            /* Trigger worker threads to suspend. */
            sctTriggerBenchmarkWorkerThreads( bmr->workerThreads, SCT_WORKER_THREAD_COMMAND_SUSPEND );

            /* We need to cancel all worker threads which migth be waiting the main
             * thread in Wait@Threads. */
            sctCancelWaits();

            sctSyncBenchmarkWorkerThreads( bmr->workerThreads, SCT_WORKER_THREAD_STATE_SUSPENDED );
            
            /* Beware a situation where the worker thread creation has failed
             * and the main thread has a worker thread wait in its
             * pre-terminate/terminate action list. */

            sctResetThreads();
            
            /* Trigger worker threads to terminate. */
            sctTriggerBenchmarkWorkerThreads( bmr->workerThreads, SCT_WORKER_THREAD_COMMAND_TERMINATE );
        }
        else
        {
            /* Beware the main thread waiting for the failed worker thread in
             * its pre/terminate actions. */
            sctCancelWaits();
        }
        
        /* Terminate main thread actions. */
        sctTerminateActions( bmr->benchmark, -1 );

        if( bmr->workerThreads != NULL )
        {       
            /* We need to cancel all worker threads which migth be waiting the main
             * thread in Wait@Threads. */
            sctCancelWaits();
        
            /* Synchronize main thread with worker threads after termination. */
            sctSyncBenchmarkWorkerThreads( bmr->workerThreads, SCT_WORKER_THREAD_STATE_TERMINATED );

            /* Join main thread with worker threads, i.e. destroy worker threads. */
            sctJoinBenchmarkWorkerThreads( bmr->workerThreads );
        }
        
        /* Restore the system error log. */
        sctSetErrorLog( bmr->previousErrorLog );
        
        siCommonMemoryFree( NULL, bmr );
    }
}

/*!
 * Initialize actions for the given benchmark for the given thread. This
 * function calls the action init-method, if defined. It also executes the
 * actions in the init list.
 *
 * \param benchmark     Benchmark to initialize. Must be defined.
 * \param threadIndex   Benchmark thread to initialize. -1 means main thread.
 *                      Must be < SCT_MAX_WORKER_THREADS.
 *
 * \return SCT_TRUE if all actions were initialized successfully, or
 * SCT_FALSE if not.
 */
SCTBoolean sctInitializeActions( SCTBenchmark* benchmark, int threadIndex )
{
    SCTAction*                  action;
    SCTActionListIterator       iterator;
    SCTBenchmarkThreadActions*  benchmarkThreadActions;

#if defined( SCT_DEBUG )
    char                        threadId[ 64 ];
    
    sctiFormatThreadId( threadIndex, threadId, 64 );
    
    siCommonDebugPrintf( NULL, "SPANDEX: sctInitializeActions \"%s\" %s start", benchmark->name, threadId );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT( benchmark != NULL );
    SCT_ASSERT( threadIndex >= -1 && threadIndex < SCT_MAX_WORKER_THREADS );
    
    benchmarkThreadActions = sctiSelectBenchmarkThreadActions( benchmark, threadIndex );

    /* Initialize final list. */
    if( benchmarkThreadActions->initActions != NULL )
    {
        /* Initialize init list. */
        sctGetActionListIterator( benchmarkThreadActions->initActions, &iterator );
    
        while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
        {
            if( action->init != NULL )
            {
            
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctInitializeActions \"%s\" %s initializing init list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
            
                if( action->init( action, benchmark ) == SCT_FALSE )
                {
                    return SCT_FALSE;
                }
            }
        }
    }

    SCT_ASSERT( benchmarkThreadActions->benchmarkActions != NULL );
    
    /* Initialize benchmark list. */
    sctGetActionListIterator( benchmarkThreadActions->benchmarkActions, &iterator );

    while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
    {
        if( action->init != NULL )
        {
            
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctInitializeActions \"%s\" %s initializing benchmark list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
            
            if( action->init( action, benchmark ) == SCT_FALSE )
            {
                return SCT_FALSE;
            }
        }
    }

    /* Initialize final list. */
    if( benchmarkThreadActions->finalActions != NULL )
    {
        sctGetActionListIterator( benchmarkThreadActions->finalActions, &iterator );
        while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
        {
            if( action->init != NULL )
            {
                
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctInitializeActions \"%s\" %s initializing final list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
                
                if( action->init( action, benchmark ) == SCT_FALSE )
                {
                    return SCT_FALSE;
                }
            }
        }
    }

    /* Initialize pre-terminate list. */
    if( benchmarkThreadActions->preTerminateActions != NULL )
    {
        sctGetActionListIterator( benchmarkThreadActions->preTerminateActions, &iterator );
        while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
        {
            if( action->init != NULL )
            {
                
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctInitializeActions \"%s\" %s initializing pre-terminate list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
                
                if( action->init( action, benchmark ) == SCT_FALSE )
                {
                    return SCT_FALSE;
                }
            }
        }
    }
    
    /* Initialize terminate list. */
    if( benchmarkThreadActions->terminateActions != NULL )
    {
        sctGetActionListIterator( benchmarkThreadActions->terminateActions, &iterator );
        while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
        {
            if( action->init != NULL )
            {
                
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctInitializeActions \"%s\" %s initializing terminate list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
                
                if( action->init( action, benchmark ) == SCT_FALSE )
                {
                    return SCT_FALSE;
                }
            }
        }
    }

    /* Execute actions in the init list. */
    if( benchmarkThreadActions->initActions != NULL )
    {   
        /* Execute actions in init list. */
        sctGetActionListIterator( benchmarkThreadActions->initActions, &iterator );
        while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
        {
            /* Execute action only if it has the execute-method defined. */
            if( action->execute != NULL )
            {
                
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctInitializeActions \"%s\" %s executing init list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
                            
                if( action->execute( action, -1 ) == SCT_FALSE )
                {
                    return SCT_FALSE;
                }
            }
        }
    }
    
    return SCT_TRUE;
}

/*!
 * Execute benchmark actions for the main thread.  Execution time is measured
 * only for the actions in the benchmark and final list, i.e. actions in the
 * init list are not in the timing loop.
 *
 * \param benchmark     Benchmark to execute. Must be defined.
 *
 * \return Execution time in seconds, or negative value if error.
 */
static double sctiExecuteBenchmarkMainThread( SCTBenchmark* benchmark )
{
    SCTBenchmarkThreadActions*  benchmarkThreadActions;
    SCTActionListIterator       iterator;
    unsigned long               startTick;
    unsigned long               endTick;
    void*                       context;
    int                         i;
    SCTAction*                  action;

    SCT_ASSERT( benchmark != NULL );

    context                = siCommonGetContext();
    benchmarkThreadActions = sctiSelectBenchmarkThreadActions( benchmark, -1 ); /* main thread. */
    
    /* Start timing. */
    startTick = siCommonGetTimerTick( context );

    /* Execute actions in benchmark list. */    
    for( i = 0; ; )  /* Yes, empty condition. */
    {
        sctGetActionListIterator( benchmarkThreadActions->benchmarkActions, &iterator );

        while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
        {
            SCT_ASSERT( action->execute != NULL );

#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctiExecuteBenchmarkMainThread \"%s\" main thread executing benchmark list action \"%s\"", benchmark->name, action->name );
#endif  /* defined( SCT_DEBUG ) */
            
            if( action->execute( action, i ) == SCT_FALSE )
            {
                return -1.0;
            }
        }

        ++i;
        
        if( benchmark->repeats > 0 && i >= benchmark->repeats )
        {
            break;
        }

        if( benchmark->repeats == 0 && benchmark->completed != 0 )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctiExecuteBenchmarkMainThread \"%s\" main thread completed explicitly", benchmark->name );
#endif  /* defined( SCT_DEBUG ) */
            break;
        }
    }

    /* Execute actions in final list. */
    if( benchmarkThreadActions->finalActions != NULL )
    {
        sctGetActionListIterator( benchmarkThreadActions->finalActions, &iterator );
        while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
        {
            /* Execute action only if it has the execute-method defined. */
            if( action->execute != NULL )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctiExecuteBenchmarkMainThread \"%s\" main thread executing final list action \"%s\"", benchmark->name, action->name );
#endif  /* defined( SCT_DEBUG ) */
                 
                if( action->execute( action, i ) == SCT_FALSE )
                {
                    return -1.0;
                }
            }
        }
    }
    
    /* End timing. */
    endTick = siCommonGetTimerTick( context );
    
    /* Convert ticks to seconds and return. */
    return sctiTicksToSeconds( endTick - startTick );
}

/*!
 * Execute benchmark actions for the given worker thread.
 *
 * \param benchmark     Benchmark to execute. Must be defined.
 * \param threadIndex   Benchmark thread to execute. Must be >= 0 and < SCT_MAX_WORKER_THREADS.
 * \param frameNumber   Framenumber. Must be >= 0.  
 *
 * \return SCT_TRUE if actions were executed ok, SCT_FALSE if error.
 */
SCTBoolean sctExecuteBenchmarkWorkerThread( SCTBenchmark* benchmark, int threadIndex, int frameNumber )
{
    SCTBenchmarkThreadActions*  benchmarkThreadActions;
    SCTActionListIterator       iterator;
    SCTAction*                  action;

    SCT_ASSERT( benchmark != NULL );
    SCT_ASSERT( threadIndex >= 0 && threadIndex < SCT_MAX_WORKER_THREADS );

    benchmarkThreadActions = sctiSelectBenchmarkThreadActions( benchmark, threadIndex );
    
    sctGetActionListIterator( benchmarkThreadActions->benchmarkActions, &iterator );

    while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
    {
        SCT_ASSERT( action->execute != NULL );
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctExecuteBenchmarkWorkerThread \"%s\" worker thread %d executing benchmark list action \"%s\"", benchmark->name, threadIndex, action->name );
#endif  /* defined( SCT_DEBUG ) */
            
        if( action->execute( action, frameNumber ) == SCT_FALSE )
        {
            return SCT_FALSE;
        }
    }

    /* Execute actions in final list. */
    if( benchmarkThreadActions->finalActions != NULL )
    {
        sctGetActionListIterator( benchmarkThreadActions->finalActions, &iterator );
        while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
        {
            /* Execute action only if it has the execute-method defined. */
            if( action->execute != NULL )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctExecuteBenchmarkWorkerThread \"%s\" worker thread %d executing final list action \"%s\"", benchmark->name, threadIndex, action->name );
#endif  /* defined( SCT_DEBUG ) */
                 
                if( action->execute( action, frameNumber ) == SCT_FALSE )
                {
                    return SCT_FALSE;
                }
            }
        }
    }
    
    return SCT_TRUE;
}

/*!
 * Terminate actions for the given benchmark for the given thread.
 *
 * \param benchmark     Benchmark to terminate. Must be defined.
 * \param threadIndex   Benchmark thread to execute. Must be >= 0 and < SCT_MAX_THREADS.  
 *
 */
void sctTerminateActions( SCTBenchmark* benchmark, int threadIndex )
{
    SCTActionListReverseIterator    riterator;
    SCTAction*                      action;
    SCTActionListIterator           iterator;
    SCTBenchmarkThreadActions*      benchmarkThreadActions;

#if defined( SCT_DEBUG )    
    char                            threadId[ 64 ];
    
    sctiFormatThreadId( threadIndex, threadId, 64 );
#endif  /* defined( SCT_DEBUG ) */   
   
    SCT_ASSERT( benchmark != NULL );
    SCT_ASSERT( threadIndex >= -1 && threadIndex < SCT_MAX_WORKER_THREADS );   
    
    benchmarkThreadActions = sctiSelectBenchmarkThreadActions( benchmark, threadIndex );  
    
    /* Pre-terminate list. */
    if( benchmarkThreadActions->preTerminateActions != NULL )
    {        
        /* Execute actions in pre-terminate list. */
        sctGetActionListIterator( benchmarkThreadActions->preTerminateActions, &iterator );
        
        while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
        {
            /* Execute action only if it has the execute-method defined. */
            if( action->execute != NULL )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctTerminateActions \"%s\" %s executing pre-terminate list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
            
                if( action->execute( action, -1 ) == SCT_FALSE )
                {
                    continue;
                }
            }
        }

        /* Terminate actions in pre-terminate list. */
        sctGetActionListReverseIterator( benchmarkThreadActions->preTerminateActions, &riterator );
 
        while( ( action = sctGetPrevActionListElement( &riterator ) ) != NULL )
        {
            if( action->terminate != NULL )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctTerminateActions \"%s\" %s terminating pre-terminate list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
                
                action->terminate( action );
            }
        }
    }
    
    /* Terminate final list. */
    if( benchmarkThreadActions->finalActions != NULL )
    {
        sctGetActionListReverseIterator( benchmarkThreadActions->finalActions, &riterator );
 
        while( ( action = sctGetPrevActionListElement( &riterator ) ) != NULL )
        {
            if( action->terminate != NULL )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctTerminateActions \"%s\" %s terminating final list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
                
                action->terminate( action );
            }
        }
    }
    
    /* Terminate benchmark list. */
    sctGetActionListReverseIterator( benchmarkThreadActions->benchmarkActions, &riterator );

    while( ( action = sctGetPrevActionListElement( &riterator ) ) != NULL )
    {
        if( action->terminate != NULL )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: sctTerminateActions \"%s\" %s terminating benchmark list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
            
            action->terminate( action );
        }
    }

    if( benchmarkThreadActions->initActions != NULL )
    {
        /* Terminate init list. */
        sctGetActionListReverseIterator( benchmarkThreadActions->initActions, &riterator );

        while( ( action = sctGetPrevActionListElement( &riterator ) ) != NULL )
        {
            if( action->terminate != NULL )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctTerminateActions \"%s\" %s terminating init list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
            
                action->terminate( action );
            }
        }
    }


    /* Terminate list. */
    if( benchmarkThreadActions->terminateActions != NULL )
    {        
        /* Execute actions in pre-terminate list. */
        sctGetActionListIterator( benchmarkThreadActions->terminateActions, &iterator );
        
        while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
        {
            /* Execute action only if it has the execute-method defined. */
            if( action->execute != NULL )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctTerminateActions \"%s\" %s executing terminate list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
            
                if( action->execute( action, -1 ) == SCT_FALSE )
                {
                    continue;
                }
            }
        }

        /* Terminate actions in terminate list. */
        sctGetActionListReverseIterator( benchmarkThreadActions->terminateActions, &riterator );
 
        while( ( action = sctGetPrevActionListElement( &riterator ) ) != NULL )
        {
            if( action->terminate != NULL )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: sctTerminateActions \"%s\" %s terminating terminate list action \"%s\"", benchmark->name, threadId, action->name );
#endif  /* defined( SCT_DEBUG ) */
                
                action->terminate( action );
            }
        }
    }   
}

/*!
 * Get a given benchmark thread actions from the benchmark. 
 *
 * \param benchmark     Benchmark. Must be defined.
 * \param threadIndex   Benchmark thread to initialize. -1 means main thread.
 *                      Must be < SCT_MAX_WORKER_THREADS.
 *
 * \return Pointer to benchmark thread.
 */
static SCTBenchmarkThreadActions* sctiSelectBenchmarkThreadActions( SCTBenchmark* benchmark, int threadIndex )
{
    SCT_ASSERT( benchmark != NULL );
    SCT_ASSERT( threadIndex >= -1 && threadIndex < SCT_MAX_WORKER_THREADS );
    
    if( threadIndex < 0 )
    {
        return &( benchmark->mainThread );
    }
    else
    {
        return &( benchmark->workerThreads[ threadIndex ] );
    }
}

/*!
 * Convert ticks to seconds. 
 *
 * \param ticks Number of ticks. Must be greater than zero.
 *
 * \return Tick time in seconds.
 */
static double sctiTicksToSeconds( unsigned long ticks )
{
    unsigned long   timerFrequency;

    timerFrequency = siCommonGetTimerFrequency( NULL );
    
    SCT_ASSERT( timerFrequency > 0 );

    return ( double )( ticks ) / ( double )( timerFrequency );
}

/*!
 * Get minimum required benchmark execution time in seconds. The minimum
 * execution time migth be calculated from the system timer frequency. However,
 * we currently use a static define.
 *
 * \return Minimum benchmark execution time in seconds.
 */
static double sctiGetMinExecutionTime( void )
{
#if defined( SCT_MIN_BENCHMARK_EXECUTION_TIME )
  return SCT_MIN_BENCHMARK_EXECUTION_TIME;
#else	/* defined( SCT_MIN_BENCHMARK_EXECUTION_TIME ) */
  return 1.0;
#endif	/* defined( SCT_MIN_BENCHMARK_EXECUTION_TIME ) */
}

/*!
 * Function for detecting benchmark repeat exit criteria. Benchmark run is
 * repeated so many times that the time samples satisfy certain criteria. We
 * could calculate some statistics from the samples to determine the exit
 * criteria. However, currently, we use a static sample limit.
 *
 * \param samples   Array of time samples. Must be defined.
 * \param len       Length of the time sample array. Must be greater or equal to zero.
 *
 * \return SCT_CYCLE_FINISHED if exit criteria has been met, SCT_CYCLE_CONTINUE otherwise.
 */
static SCTCycleStatus sctiEnoughSamples( double* samples, int len )
{
    SCT_ASSERT( samples != NULL );
    SCT_ASSERT( len >= 0 );
    SCT_USE_VARIABLE( samples );
    
    if( len >= SAMPLE_LIMIT )
    {
        return SCT_CYCLE_FINISHED;
    }
    else
    {
        return SCT_CYCLE_CONTINUE;
    }
}

#if defined( SCT_DEBUG )
/*!
 * Get thread id.
 *
 * \param threadIndex   Thread index.
 * \param buf           Buffer for thread id. Must be defined.
 * \param bufLen        Buffer length. Currently, must be > 32.
 *
 */
static void sctiFormatThreadId( int threadIndex, char* buf, int bufLen )
{
    SCT_ASSERT( buf != NULL );
    SCT_ASSERT( bufLen > 32 );
    SCT_USE_VARIABLE( bufLen );
    
    if( threadIndex < 0 )
    {
        strcpy( buf, "main thread" );
    }
    else
    {
        sprintf( buf, "worker thread %d", threadIndex );
    }
}
#endif  /* defined( SCT_DEBUG ) */
