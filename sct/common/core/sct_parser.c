/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_parser.h"

#include "sct_utils.h"
#include "sct_sicommon.h"
#include "sct_tokenizer.h"
#include "sct_benchmark.h"
#include "sct_module.h"
#include "sct_action.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*! Parser context */
typedef struct
{
    SCTModuleList*          modules;
    SCTBenchmarkList*       benchmarks;
    SCTActionList*          actions;
    SCTActionList*          actionHashMap[ 256 ]; /* For speeding up the action searching */
    SCTTokenizerContext*    tokenizerContext;
    char                    scratchBuffer[ 512 ]; /* For formatting error messages. */
} SCTParserContext;

/* Prototypes for module's internal functions. */
static int                  sctiFindActionListIndex( const char* attribute, const char* actionList );
static SCTBoolean           sctiDoParse( SCTParserContext* context );
static SCTBoolean           sctiParseSection( SCTParserContext* context );
static SCTBoolean           sctiParseBenchmarkSection( SCTParserContext* context, const char* sectionName );
static SCTBoolean           sctiParseActionSection( SCTParserContext* context, const char* sectionName, const char* sectionType, const char* moduleName );
static SCTActionList*       sctiParseActionList( SCTParserContext* context, SCTBoolean allowNone );
static SCTBoolean           sctiParseString( SCTParserContext* context, const char** string );
static char*                sctiParseAndDuplicateString( SCTParserContext* context );
static SCTAction*           sctiFindAction( SCTActionList** actionHashMap, const char* actionName );
static SCTModule*           sctiFindModule( SCTModuleList* modules, const char* moduleName );
static SCTBenchmark*        sctiGenerateBenchmark( SCTParserContext* context, const char* name, SCTAttributeList* attributes, int threadCount, SCTActionList* initActions[], SCTActionList* benchmarkActions[], SCTActionList* finalActions[], SCTActionList* preTerminateActions[], SCTActionList* terminateActions[] );
static SCTBoolean           sctiGenerateAction( SCTParserContext* context, const char* name, const char* sectionType, const char* moduleName, SCTAttributeList* attributes );
static SCTBoolean           sctiExpectDelimiter( SCTParserContext* context, char delimiter );
static unsigned char        sctiHashString( const char* string );
static SCTBoolean           sctiInitActionHashMap( SCTActionList** actionHashMap );
static void                 sctiDestroyActionHashMap( SCTActionList** actionHashMap );
static void                 sctiLogError_s( SCTParserContext* context, const char* error, const char* s );
static void                 sctiLogError_c( SCTParserContext* context, const char* error, char c );
static void                 sctiLogError_cs( SCTParserContext* context, const char* error, char c, const char* s );

/*!
 * Find action list index from attribute name.
 *
 * \param   attribute   attribute.
 * \param   actionList  action list to find.
 *
 * \return  action list index when successful, or less than zero value when failure.
 */
int sctiFindActionListIndex( const char* attribute, const char* actionList )
{
    int i;
    int r;
    
    SCT_ASSERT( attribute != NULL );
    SCT_ASSERT( actionList != NULL );

    for( i = 0; attribute[ i ]; ++i )
    {
        if( actionList[ i ] == 0 && attribute[ i ] != 0 )
        {
            r = atoi( &attribute[ i ] );
            if( r > 0 )
            {
                return r;
            }
            else
            {
                return -1;
            }
        }
        else if( actionList[ i ] != attribute[ i ] )
        {
            return -1;
        }
    }

    if( actionList[ i ] == 0 && attribute[ i ] == 0 )
    {
        return 0;
    }
    
    return -1;
}

/*!
 * Main parsing function.
 *
 * \param   modules     list of modules
 * \param   benchmarks  list of benchmarks
 * \param   actions     list of actions
 * \param   log         error/warning log
 *
 * \return  SCT_TRUE if parsing was succesful, SCT_FALSE otherwise.
 */
SCTBoolean sctParseInput( SCTModuleList* modules, SCTBenchmarkList* benchmarks, SCTActionList* actions )
{
    SCTParserContext* context = NULL;
    SCTBoolean        parseOk = SCT_TRUE;
    
    SCT_ASSERT_ALWAYS( modules != NULL );
    SCT_ASSERT_ALWAYS( benchmarks != NULL );
    SCT_ASSERT_ALWAYS( actions != NULL );

    /* Parser initialization */
    context = ( SCTParserContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTParserContext ) ) );
    
    if( context == NULL  )
    {
        SCT_LOG_ERROR( "Parser initialization failed: out of memory" );
        return SCT_FALSE;
    }
    
    memset( context, 0, sizeof( SCTParserContext ) ); /* Set all fields to 0 */
    
    context->modules    = modules;
    context->benchmarks = benchmarks;
    context->actions    = actions;

    /* Action hash map initialization. */
    if( sctiInitActionHashMap( context->actionHashMap ) == SCT_FALSE )
    {
        SCT_ASSERT_ERROR_REPORTED;
        siCommonMemoryFree( NULL, context );
        
        return SCT_FALSE;
    }
    
    /* Tokenizer initialization */
    context->tokenizerContext = sctTokenizerInit();
    if( context->tokenizerContext == NULL )
    {
        /* Tokenizer should log the error. */
        SCT_ASSERT_ERROR_REPORTED;
        sctiDestroyActionHashMap( context->actionHashMap );
        siCommonMemoryFree( NULL, context );
        
        return SCT_FALSE;
    }

    parseOk = sctiDoParse( context );

    /* Check that the spandex core release and input file release match. */
    sctTokernizerCheckRelease( context->tokenizerContext );
    
    /* Terminate tokenizer. */
    sctTokenizerTerminate( context->tokenizerContext );

    sctiDestroyActionHashMap( context->actionHashMap );
    siCommonMemoryFree( NULL, context );

    SCT_ASSERT( parseOk == SCT_TRUE || strlen( sctGetErrorLog()->error ) > 0 );
    
    return parseOk;
}

/*!
 * Top level parsing loop. Scans for sections.
 *
 * \param context   Parser context pointer.
 *
 * \return SCT_TRUE if all sections parsed OK, SCT_FALSE otherwise.
 */
SCTBoolean sctiDoParse( SCTParserContext* context )
{
    const char*     token;
    SCTTokenType    tokenType;
    
    /* Main parsing loop. */
    for( ;; )
    {
        tokenType = sctGetToken( context->tokenizerContext, &token );
        
        if( tokenType == TOK_END )
        {
            return SCT_TRUE;
        }
        
        if( tokenType != TOK_DELIMITER || token[ 0 ]  != SCT_SECTION_HEADER_START )
        {
            sctiLogError_s( context, "Expected section start, got %s", token );
            return SCT_FALSE;
        }
        
        if( sctiParseSection( context ) == SCT_FALSE )     
        {
            SCT_ASSERT_ERROR_REPORTED;
            return SCT_FALSE;
        }
        
        siCommonYield( NULL );       
    }
}    

/*!
 * Parses single section from the input.
 *
 * \param context   Parser context pointer.
 *
 * \return SCT_TRUE if section parsed OK, SCT_FALSE otherwise.
 */
SCTBoolean sctiParseSection( SCTParserContext* context )
{
    char*       sectionName = NULL;
    char*       sectionType = NULL;
    char*       moduleName  = NULL;
    SCTBoolean  returnValue = SCT_FALSE;

    sectionName = sctiParseAndDuplicateString( context );
    
    if( sectionName == NULL )
    {
        SCT_ASSERT_ERROR_REPORTED;
        return SCT_FALSE;
    }
    
    if( sctiExpectDelimiter( context, SCT_NAME_VALUE_SEPARATOR ) == SCT_FALSE )
    {
        SCT_ASSERT_ERROR_REPORTED;
        siCommonMemoryFree( NULL, sectionName );
        
        return SCT_FALSE;
    }
    
    sectionType = sctiParseAndDuplicateString( context );
    
    if( sectionType == NULL )
    {
        SCT_ASSERT_ERROR_REPORTED;
        siCommonMemoryFree( NULL, sectionName );
        
        return SCT_FALSE;
    }
    else if( strcmp( sectionType, SCT_BENCHMARK_TYPE_STRING ) == 0 )
    {
        if( sctiExpectDelimiter( context, SCT_SECTION_HEADER_END ) == SCT_TRUE )
        {
            returnValue = sctiParseBenchmarkSection( context, sectionName );
        }
    }
    /* Has to be an action section. */
    else if( sctiExpectDelimiter( context, SCT_TYPE_MODULE_SEPARATOR ) == SCT_TRUE &&
             ( moduleName = sctiParseAndDuplicateString( context ) )   != NULL &&
             sctiExpectDelimiter( context, SCT_SECTION_HEADER_END )    == SCT_TRUE )
    {
        returnValue = sctiParseActionSection( context, sectionName, sectionType, moduleName );
    }
    
    if( sectionName != NULL )
    {
        siCommonMemoryFree( NULL, sectionName );
    }
    
    if( sectionType != NULL )
    {
        siCommonMemoryFree( NULL, sectionType );
    }
    
    if( moduleName != NULL )
    {
        siCommonMemoryFree( NULL, moduleName );
    }
    
    if( returnValue == SCT_FALSE )
    {
        SCT_ASSERT_ERROR_REPORTED;
    }
                                              
    return returnValue;
}

/*!
 * Parses benchmark section.
 *
 * \param context     Parser context pointer.
 * \param sectionName Name of the section.
 *
 * \return SCT_TRUE if benchmark parsed OK, SCT_FALSE otherwise.
 */
SCTBoolean sctiParseBenchmarkSection( SCTParserContext* context, const char* sectionName )
{
    SCTAttributeList* attributes        = NULL;
    int               i;
    const int         maxThreadCount    = SCT_MAX_WORKER_THREADS + 1;
    SCTActionList*    initActions[ SCT_MAX_WORKER_THREADS + 1 ];
    SCTActionList*    benchmarkActions[ SCT_MAX_WORKER_THREADS + 1 ];
    SCTActionList*    finalActions[ SCT_MAX_WORKER_THREADS + 1 ];
    SCTActionList*    preTerminateActions[ SCT_MAX_WORKER_THREADS + 1 ];
    SCTActionList*    terminateActions[ SCT_MAX_WORKER_THREADS + 1 ];    
    SCTBenchmark*     benchmark;
    const char*       token;
    SCTTokenType      tokenType;
    char*             attributeName     = NULL;
    const char*       attributeValue    = NULL;

    for( i = 0; i < maxThreadCount; ++i )
    {
        initActions[ i ]         = NULL;
        benchmarkActions[ i ]    = NULL;
        finalActions[ i ]        = NULL;
        preTerminateActions[ i ] = NULL;
        terminateActions[ i ]    = NULL;
    }
        
    attributes = sctCreateAttributeList();
    
    if( attributes == NULL )
    {
        SCT_LOG_ERROR( "Parser: out of memory" );
        return SCT_FALSE;
    }
    
    /* Parsing loop. */
    for( ;; )
    {
        tokenType = sctGetToken( context->tokenizerContext, &token );
        
        if( tokenType == TOK_END )
        {
            benchmark = sctiGenerateBenchmark( context,
                                               sectionName,
                                               attributes,
                                               maxThreadCount,
                                               initActions,
                                               benchmarkActions,
                                               finalActions,
                                               preTerminateActions,
                                               terminateActions );
            if( benchmark == NULL )
            {
                SCT_ASSERT_ERROR_REPORTED;
                break;
            }
            
            if( sctAddBenchmarkToList( context->benchmarks, benchmark ) == SCT_FALSE )
            {
                SCT_LOG_ERROR( "Parser: could not add benchmark to list" );
                sctDestroyBenchmark( benchmark );
                
                return SCT_FALSE;
            }
            
            return SCT_TRUE;
        }
        else if( tokenType == TOK_DELIMITER )
        {
            if( token[ 0 ] == SCT_SECTION_HEADER_START )
            {
                sctPutBackToken( context->tokenizerContext );
                benchmark = sctiGenerateBenchmark( context,
                                                   sectionName,
                                                   attributes,
                                                   maxThreadCount,                                                   
                                                   initActions,
                                                   benchmarkActions,
                                                   finalActions,
                                                   preTerminateActions,
                                                   terminateActions );
                if( benchmark == NULL )
                {
                    SCT_ASSERT_ERROR_REPORTED;
                    break;
                }
                
                if( sctAddBenchmarkToList( context->benchmarks, benchmark ) == SCT_FALSE )
                {
                    SCT_LOG_ERROR( "Parser: could not add benchmark to list" );
                    /* Do not use break here: attribute and action lists have
                     * been transferred over to the benchmark so they will be
                     * destroyed by sctDestroyBenchmark. */
                    sctDestroyBenchmark( benchmark );
                    
                    return SCT_FALSE;
                }
                
                return SCT_TRUE;
            }
            
            sctiLogError_s( context, "Expected attribute name, got %s", token );
            break;
        }
        else if( tokenType == TOK_STRING )
        {
            attributeName = sctDuplicateString( token );
            
            if( attributeName == NULL )
            {
                SCT_LOG_ERROR( "Out of memory" );
                break;
            }
            
            if( sctiExpectDelimiter( context, SCT_NAME_VALUE_SEPARATOR ) == SCT_FALSE )
            {
                break;
            }

            if( ( i = sctiFindActionListIndex( attributeName, SCT_BENCHMARK_INIT_ACTIONS ) ) >= 0 )
            {
                if( i >= maxThreadCount )
                {
                    sctLogInputError( context->tokenizerContext, "Out of bounds InitActions attribute index" );
                    break;
                }
                
                if( initActions[ i ]  != NULL )
                {
                    sctLogInputError( context->tokenizerContext, "Duplicate InitActions attribute" );
                    break;
                }
                
                initActions[ i ] = sctiParseActionList( context, SCT_TRUE );
                
                if( initActions[ i ] == NULL )
                {
                    break;
                }
            }
            else if( ( i = sctiFindActionListIndex( attributeName, SCT_BENCHMARK_BENCHMARK_ACTIONS ) ) >= 0 )
            {
                if( i >= maxThreadCount )
                {
                    sctLogInputError( context->tokenizerContext, "Out of bounds BenchmarkActions attribute index" );
                    break;
                }

                if( benchmarkActions[ i ] != NULL )
                {
                    sctLogInputError( context->tokenizerContext, "Duplicate BenchmarkActions attribute" );
                    break;
                }
                
                benchmarkActions[ i ] = sctiParseActionList( context, SCT_FALSE );
                
                if( benchmarkActions[ i ] == NULL )
                {
                    break;
                }
            }
            else if( ( i = sctiFindActionListIndex( attributeName, SCT_BENCHMARK_FINAL_ACTIONS ) ) >= 0 )
            {
                if( i >= maxThreadCount )
                {
                    sctLogInputError( context->tokenizerContext, "Out of bounds FinalActions attribute index" );
                    break;
                }

                if( finalActions[ i ] != NULL )
                {
                    sctLogInputError( context->tokenizerContext, "Duplicate FinalActions attribute" );
                    break;
                }
                
                finalActions[ i ] = sctiParseActionList( context, SCT_TRUE );
                
                if( finalActions[ i ] == NULL )
                {
                    break;
                }
            }
            else if( ( i = sctiFindActionListIndex( attributeName, SCT_BENCHMARK_PRETERMINATE_ACTIONS ) ) >= 0 )
            {
                if( i >= maxThreadCount )
                {
                    sctLogInputError( context->tokenizerContext, "Out of bounds PreTerminateActions attribute index" );
                    break;
                }

                if( preTerminateActions[ i ] != NULL )
                {
                    sctLogInputError( context->tokenizerContext, "Duplicate PreterminateActions attribute" );
                    break;
                }
                
                preTerminateActions[ i ] = sctiParseActionList( context, SCT_TRUE );
                
                if( preTerminateActions[ i ] == NULL )
                {
                    break;
                }
            }           
            else if( ( i = sctiFindActionListIndex( attributeName, SCT_BENCHMARK_TERMINATE_ACTIONS ) ) >= 0 )
            {
                if( i >= maxThreadCount )
                {
                    sctLogInputError( context->tokenizerContext, "Out of bounds TerminateActions attribute index" );
                    break;
                }

                if( terminateActions[ i ] != NULL )
                {
                    sctLogInputError( context->tokenizerContext, "Duplicate TerminateActions attribute" );
                    break;
                }
                
                terminateActions[ i ] = sctiParseActionList( context, SCT_TRUE );
                
                if( terminateActions[ i ] == NULL )
                {
                    break;
                }
            }           
            else
            {
                /* Not an action attribute so must be a normal attribute. */
                if( sctiParseString( context, &attributeValue ) == SCT_FALSE )
                {
                    SCT_ASSERT_ERROR_REPORTED;
                    break;
                }
                
                if( sctAddNameValueToList( attributes, attributeName, attributeValue ) == SCT_FALSE )
                {
                    SCT_LOG_ERROR( "Parser: adding attribute to list failed" );
                    break;
                }
            }
            
            siCommonMemoryFree( NULL, attributeName );
            attributeName = NULL;
        }
        else
        {
            SCT_ASSERT_ALWAYS( tokenType == TOK_ERROR );
            SCT_ASSERT_ERROR_REPORTED;
            break;
        }
    }
    
    /* We come here only if something failed above. */
    sctDestroyAttributeList( attributes );

    for( i = 0; i < maxThreadCount; ++i )
    {
        if( initActions[ i ] != NULL )
        {
            sctDestroyActionList( initActions[ i ] );
            initActions[ i ] = NULL;            
        }
        if( benchmarkActions[ i ] != NULL )
        {
            sctDestroyActionList( benchmarkActions[ i ] );
            benchmarkActions[ i ] = NULL;
        }
        if( finalActions[ i ] != NULL )
        {
            sctDestroyActionList( finalActions[ i ] );
            finalActions[ i ] = NULL;
        }
        if( preTerminateActions[ i ] != NULL )
        {
            sctDestroyActionList( preTerminateActions[ i ] );
            preTerminateActions[ i ] = NULL;
        }           
        if( terminateActions[ i ] != NULL )
        {
            sctDestroyActionList( terminateActions[ i ] );
            terminateActions[ i ] = NULL;
        }   
    }
    
    if( attributeName != NULL )
    {
        siCommonMemoryFree( NULL, attributeName );
    }
    
    return SCT_FALSE;
}

/*!
 * Generates a new benchmark object.
 *
 * \param context               Parser context pointer.
 * \param name                  Benchmark name.
 * \param attributes            List of attributes.
 * \param threadCount           Number of threads, i.e. the number of elements in actions arrays.
 *                              Some array elements might contain NULL values.
 * \param initActions           Array of init actions lists, one list per thread.
 * \param benchmarkActions      Array of benchmark actions lists, one list per thread.
 * \param finalActions          Array of final actions list, one list per thread.
 * \param preTerminateActions   Array of pre-terminate actions lists, one list per thread. 
 * \param terminateActions      Array of terminate actions lists, one list per thread.
 *
 * \return Pointer to a new benchmark or NULL if an error occured.
 */
SCTBenchmark* sctiGenerateBenchmark( SCTParserContext* context, const char* name, SCTAttributeList* attributes, int threadCount, SCTActionList* initActions[], SCTActionList* benchmarkActions[], SCTActionList* finalActions[], SCTActionList* preTerminateActions[], SCTActionList* terminateActions[] )
{
    SCTBenchmark*   benchmark;
    
    SCT_ASSERT( context != NULL );
    SCT_ASSERT( attributes != NULL );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( attributes );
        
    benchmark = sctCreateBenchmark( name,
                                    attributes,
                                    threadCount,
                                    initActions,
                                    benchmarkActions,
                                    finalActions,
                                    preTerminateActions,
                                    terminateActions );
    
    if( benchmark == NULL )
    {
        SCT_ASSERT_ERROR_REPORTED;
        return NULL;
    }
    
    return benchmark;
}

/*!
 * Parses action list (i.e., the value of benchmark's action attribute).
 *
 * \param context   Parser context pointer.
 * \param allowNone Boolean flag indicating whether a None value is accepted.
 *
 * \return Pointer to list of actions or NULL if an error occured.
 */
SCTActionList* sctiParseActionList( SCTParserContext* context, SCTBoolean allowNone )
{
    SCTActionList*  actions;
    SCTAction*      action;
    SCTTokenType    tokenType;
    const char*     token       = NULL;

    actions = sctCreateActionList();
    
    if( actions == NULL )
    {
        SCT_LOG_ERROR( "Parser: out of memory" );
        return NULL;
    }
       
    for(;;)
    {
        if( sctiParseString( context, &token ) == SCT_FALSE )
        {
            SCT_ASSERT_ERROR_REPORTED;
            break;
        }
        
        if( strcmp( token, SCT_BENCHMARK_EMPTY_ACTIONS ) == 0 )
        {
            if( allowNone == SCT_FALSE )
            {
                sctLogInputError( context->tokenizerContext, "None is not allowed here" );
                break;
            }
        }
        else
        {
            action = sctiFindAction( context->actionHashMap, token );
            if( action == NULL )
            {
                sctiLogError_s( context, "Could not find action named %s", token );
                break;
            }
            
            if( sctAddActionToList( actions, action ) == SCT_FALSE )
            {
                SCT_LOG_ERROR( "Parser: could not add action to list." );
                break;
            }
        }

        tokenType = sctGetToken( context->tokenizerContext, &token );
        
        if( tokenType == TOK_END )
        {
            return actions;
        }
        else if( tokenType == TOK_DELIMITER && token[ 0 ] == SCT_ACTION_LIST_SEPARATOR )
        {
            continue;
        }
        else
        {
            sctPutBackToken( context->tokenizerContext );
            return actions;
        }
    }
    
    /* We end up here only if something failed above. */
    sctDestroyActionList( actions );
    
    return NULL;
}

/*!
 * Searches list of actions for action with the given name. A 256-item hash map
 * is used to speed up the search.
 *
 * \param actions    List of actions.
 * \param actionName Action name to look for.
 *
 * \return Pointer to action or NULL if action was not found.
 */
SCTAction* sctiFindAction( SCTActionList** actionHashMap, const char* actionName )
{
    SCTActionList*          actions;
    SCTActionListIterator   iter;
    SCTAction*              action;
    unsigned char           hash;

    SCT_ASSERT( actionHashMap != NULL );
    SCT_ASSERT( actionName != NULL );
    SCT_ASSERT( strlen( actionName ) > 0 );

    /* Calculate a hash value for the action name. */
    hash = sctiHashString( actionName );
    
    /* Select an action list based on the hash value. */
    actions = actionHashMap[ hash ];
    
    SCT_ASSERT_ALWAYS( actions != NULL );
    
    /* Search linearly through the action list. */
    sctGetActionListIterator( actions, &iter );
    
    for( action = sctGetNextActionListElement( &iter );
         action != NULL;
         action = sctGetNextActionListElement( &iter ) )
    {
        if( strcmp( action->name, actionName ) == 0 )
        {
            return action;
        }
    }
    return NULL;
}

/*!
 * Searches list of modules for module with the given name.
 *
 * \param modules    List of modules.
 * \param moduleName Module name to look for.
 *
 * \return Pointer to module or NULL if module was not found.
 */
SCTModule* sctiFindModule( SCTModuleList* modules, const char* moduleName )
{
    SCTModuleListIterator   iter;
    SCTModule*              module;
    
    SCT_ASSERT( modules != NULL );
    SCT_ASSERT( moduleName != NULL );
    SCT_ASSERT( strlen( moduleName ) > 0 );

    sctGetModuleListIterator( modules, &iter );
    
    for( module = sctGetNextModuleListElement( &iter );
         module != NULL;
         module = sctGetNextModuleListElement( &iter ) )
    {
        if( strcmp( module->name, moduleName ) == 0 )
        {
            return module;
        }
    }
    return NULL;
}

/*!
 * Parses action section.
 *
 * \param context       Parser context pointer.
 * \param sectionName   Name of the section.
 * \param actionType    Type of the action.
 * \param moduleName    Module name.
 *
 * \return SCT_TRUE if action section was parsed OK, SCT_FALSE if an error occured.
 */
SCTBoolean sctiParseActionSection( SCTParserContext* context, const char* sectionName, const char* actionType, const char* moduleName )
{
    SCTAttributeList*   attributes = NULL;
    const char*         token;
    SCTTokenType        tokenType;
    char*               attributeName;
    const char*         attributeValue;
    
    attributes = sctCreateAttributeList();
    
    if( attributes == NULL )
    {
        SCT_LOG_ERROR( "Parser: out of memory" );
        return SCT_FALSE;
    }
    
    /* Parsing loop. */
    for( ;; )
    {
        tokenType = sctGetToken( context->tokenizerContext, &token );
        
        if( tokenType == TOK_END )
        {
            /* sctiGenerateAction handles destroying of the attributes list if
             * needed. */
            return sctiGenerateAction( context, sectionName, actionType, moduleName, attributes );
        }
        else if( tokenType == TOK_DELIMITER )
        {
            if( token[ 0 ] == SCT_SECTION_HEADER_START )
            {
                sctPutBackToken( context->tokenizerContext );
                
                /* sctiGenerateAction handles destroying of the attributes list if needed. */
                return sctiGenerateAction( context, sectionName, actionType, moduleName, attributes );
            }
            
            sctiLogError_s( context, "Expected attribute name, got %s", token );
            break;
        }
        else if( tokenType == TOK_STRING )
        {
            attributeName = sctDuplicateString( token );
            
            if( attributeName == NULL )
            {
                SCT_LOG_ERROR( "Out of memory" );
                break;
            }
            
            if( sctiExpectDelimiter( context, SCT_NAME_VALUE_SEPARATOR ) == SCT_FALSE )
            {
                siCommonMemoryFree( NULL, attributeName );
                break;
            }
            else
            {
                /* Parse attribute value. */
                if( sctiParseString( context, &attributeValue ) == SCT_FALSE )
                {
                    siCommonMemoryFree( NULL, attributeName );
                    break;
                }
                
                if( sctAddNameValueToList( attributes, attributeName, attributeValue ) == SCT_FALSE )
                {
                    SCT_LOG_ERROR( "Parser: adding attribute to list failed" );
                    siCommonMemoryFree( NULL, attributeName );
                    break;
                }
            }
            
            siCommonMemoryFree( NULL, attributeName );
        }
        else
        {
            SCT_ASSERT( tokenType == TOK_ERROR );
            SCT_ASSERT_ERROR_REPORTED;
            break;
        }
    }
    
    /* We come here only if something failed above. */
    sctDestroyAttributeList( attributes );
    return SCT_FALSE;
}

/*!
 * Generates a new action object.
 *
 * \param context       Parser context pointer.
 * \param name          Action name.
 * \param type          Action type.
 * \param moduleName    Module name.
 * \param attributes    List of attributes.
 *
 * \return Pointer to a new action or NULL if an error occured.
 */
SCTBoolean sctiGenerateAction( SCTParserContext* context, const char* name, const char* type, const char* moduleName, SCTAttributeList* attributes )
{
    SCTModule*  module;
    SCTAction*  action;

    /* Check that there is no action with the same name already in the action list. */
    if( sctiFindAction( context->actionHashMap, name ) != NULL )
    {
        sctiLogError_s( context, "Action %s has already been defined", name );
        /* Semi-KLUDGE: The attribute list has to be freed here because
         * sctDestroyAction (see below) will also free it -> caller wouldn't
         * know whether it needs to free the attribute list or not if
         * sctGenerateAction fails. */
        sctDestroyAttributeList( attributes );
        
        return SCT_FALSE;
    }
    
    module = sctiFindModule( context->modules, moduleName );
    
    if( module == NULL )
    {
        sctiLogError_s( context, "Could not find module %s", moduleName );
        sctDestroyAttributeList( attributes );
        
        return SCT_FALSE;
    }
    
    action = module->createAction( module, name, type, attributes );
    
    if( action == NULL )
    {
        SCT_ASSERT_ERROR_REPORTED; 
        sctDestroyAttributeList( attributes );
        
        return SCT_FALSE;
    }
    
    /* Add the new action to the main action list. */
    if( sctAddActionToList( context->actions, action ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Parser: could not add action to list." );
        sctDestroyAction( action );
        return SCT_FALSE;
    }
    
    /* Also add the action to the action hash map. */
    if( sctAddActionToList( context->actionHashMap[ sctiHashString( name ) ], action ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Parser: could not add action to hash map." );
        /* KLUDGE: The action will be destroyed by the controlled when it
         * destroys the main action list. Destroying it here would leave the
         * invalid pointer to the main action list. */
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 * Parses a string from the input.
 *
 * \param context   Parser context pointer.
 * \param string    Output buffer for storing the string.
 *
 * \return  SCT_TRUE if parsing was succesfull, SCT_FALSE otherwise.
 */
SCTBoolean sctiParseString( SCTParserContext* context, const char** string )
{
    SCTTokenType    tokenType;

    tokenType = sctGetToken( context->tokenizerContext, string );

    switch( tokenType )
    {
    case TOK_STRING:
        return SCT_TRUE;
        
    case TOK_DELIMITER:
        sctiLogError_s( context, "Expected string, got delimiter %s", *string );
        return SCT_FALSE;
        
    case TOK_END:
        sctLogError( "Expected string, got end-of-input" );
        return SCT_FALSE;
        
    case TOK_ERROR:
        break;
    }
    
    SCT_ASSERT( tokenType == TOK_ERROR );
    SCT_ASSERT_ERROR_REPORTED;
    
    return SCT_FALSE;
}

/*!
 * Parses a string from the input and duplicates it to newly allocated memory.
 *
 * \param context   Parser context pointer.
 *
 * \return Pointer to string or NULL if failed.
 */
char* sctiParseAndDuplicateString( SCTParserContext* context )
{
    const char* parsedString;
    char*       string;

    if( sctiParseString( context, &parsedString ) == SCT_FALSE )
    {
        SCT_ASSERT_ERROR_REPORTED;
        return NULL;
    }
    
    string = sctDuplicateString( parsedString );
    
    if( string == NULL )
    {
        sctLogError( "Could not duplicate string" );
        return NULL;
    }
    
    return string;
}

/*!
 * Reads a character from the input an checks if it is the expected delimiter.
 *
 * \param context           Parser context pointer.
 * \param expectedDelimiter Expected delimiter.
 *
 * \return SCT_TRUE if the expected delimiter was read from the input, SCT_FALSE
 * if some other delimiter or token was read or there was an error.
 */
SCTBoolean sctiExpectDelimiter( SCTParserContext* context, char expectedDelimiter )
{
    const char*     token;
    SCTTokenType    tokenType;

    tokenType= sctGetToken( context->tokenizerContext, &token );

    if( tokenType == TOK_STRING )
    {
        sctiLogError_cs( context, "Expected %c, got \"%s\"", expectedDelimiter, token );
        return SCT_FALSE;
    }
    else if( tokenType == TOK_END )
    {
        sctiLogError_c( context, "Expected %c, got end-of-input", expectedDelimiter );
        return SCT_FALSE;
    }
    else if( tokenType == TOK_DELIMITER )
    {
        if( token[ 0 ] != expectedDelimiter )
        {
            sctiLogError_cs( context, "Expected %c, got %s", expectedDelimiter, token );
            return SCT_FALSE;
        }
        return SCT_TRUE;
    }
    
    SCT_ASSERT( tokenType == TOK_ERROR );
    SCT_ASSERT_ERROR_REPORTED;
    
    return SCT_FALSE;
}

/*!
 * Calculates a 8-bit hash value for the given string.
 *
 * \param string  Zero-terminated string.
 *
 * \return 8-bit hash value.
 */
unsigned char sctiHashString( const char* string )
{
    unsigned char h = 0;
    
    while( *string )
    {
        /* Cannot use += here because of the casting needed. */
        h  = ( unsigned char )( h + *string++ );
    }
    return h;
}

/*!
 * Initializes an action hash map, which is simply an array of 256 action lists.
 *
 * \param actionHashMap Pointer to a 256 item action list array.
 *
 * \return SCT_TRUE if the map was succesfully initialized, SCT_FALSE otherwise.
 */
SCTBoolean sctiInitActionHashMap( SCTActionList** actionHashMap )
{
    int i;

    SCT_ASSERT( actionHashMap != NULL );

    for( i = 0; i < 256; i++ )
    {
        actionHashMap[i] = NULL;
    }
    
    for( i = 0; i < 256; i++ )
    {
        actionHashMap[ i ] = sctCreateActionList();
        if( actionHashMap[ i ] == NULL )
        {
            sctiDestroyActionHashMap( actionHashMap );
            SCT_LOG_ERROR( "Parser: out of memory" );
            
            return SCT_FALSE;
        }
    }
    
    return SCT_TRUE;
}

/*!
 * Destroys an action hash map, i.e, frees the action lists contained in it.
 *
 * \param actionHashMap Pointer to a 256 item action list array.
 */
void sctiDestroyActionHashMap( SCTActionList** actionHashMap )
{
    int i;

    for( i = 0; i < 256; i++ )
    {
        if( actionHashMap[ i ] != NULL )
        {
            sctDestroyActionList( actionHashMap[ i ] );
        }
    }
}

/*!
 * Logs an error message with one string variable formatted to it. The string is
 * marked with %s in the error message.
 *
 * \param context   Parser context pointer.
 * \param error     Error message.
 * \param s         String to be formatted to the message.
 */
void sctiLogError_s( SCTParserContext* context, const char* error, const char* s )
{
    sprintf( context->scratchBuffer, error, s );
    sctLogInputError( context->tokenizerContext, context->scratchBuffer );
}

/*!
 * Logs an error message with one character variable formatted to it. The
 * character is marked with %c in the error message.
 *
 * \param context   Parser context pointer.
 * \param error     Error message.
 * \param c         Character to be formatted to the message.
 */
void sctiLogError_c( SCTParserContext* context, const char* error, char c )
{
    sprintf( context->scratchBuffer, error, c );
    sctLogInputError( context->tokenizerContext, context->scratchBuffer );
}

/*!
 * Logs an error message with a character and a string variable formatted to
 * it. The character is marked with %c and the string with %s in the error
 * message.
 *
 * \param context   Parser context pointer.
 * \param error     Error message.
 * \param c         Character to be formatted to the message.
 * \param s         String to be formatted to the message.
 */
void sctiLogError_cs( SCTParserContext* context, const char* error, char c, const char* s )
{
    sprintf( context->scratchBuffer, error, c, s );
    sctLogInputError( context->tokenizerContext, context->scratchBuffer );
}
