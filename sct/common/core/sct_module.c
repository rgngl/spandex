/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_module.h"
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_action.h"

#include <string.h>

/*!
 * Create a module.
 *
 * \param name Name of the module. Must be defined. Only SCT_MAX_MODULE_NAME_LEN characters are used.
 * \param context Module context.
 * \param create Pointer to module create function.
 * \param info Pointer to module info function.
 * \param createAction Pointer to module create action function.
 * \param destroy Pointer to module destroy function.
 *
 * \return Newly created module, or NULL if failure.
 *
 */
SCTModule* sctCreateModule( const char* name,
                            void* context,
                            const char* config,
                            SCTModuleInfoMethod info,
                            SCTModuleCreateActionMethod createAction,
                            SCTModuleDestroyMethod destroy )
{
    SCTModule*  module;

    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT( config != NULL );

    /* Allocate memory for the module. */
    module = ( SCTModule* )( siCommonMemoryAlloc( NULL, sizeof( SCTModule ) ) );
    if( module == NULL )
    {
        return NULL;
    }
    if( ( module->name = sctDuplicateString( name ) ) == NULL )
    {
        siCommonMemoryFree( NULL, module );
        return NULL;
    }

    module->context         = context;
    module->config          = config;
    module->info            = info;
    module->createAction    = createAction;
    module->destroy         = destroy;

    return module;
}

/*!
 * Destroy module. This function also calls the module destroy
 * function.
 *
 * \param module Module to destroy. Ignored if NULL.
 *
 */
void sctDestroyModule( SCTModule* module )
{
    if( module != NULL )
    {
        if( module->destroy != NULL )
        {
            module->destroy( module );
        }
        if( module->name != NULL )
        {
            siCommonMemoryFree( NULL, module->name );
        }
        siCommonMemoryFree( NULL, module );
    }
}

/*!
 * Create module list. List takes ownership of the module and
 * deallocates it when the list if destroyed.
 *
 * \return Newly created list, or NULL if failure.
 *
 */
SCTModuleList* sctCreateModuleList()
{
    SCTList*        list;
    SCTModuleList*  moduleList;

    list = sctCreateList();
    if( list == NULL )
    {
        return NULL;
    }
    
    moduleList = ( SCTModuleList* )( siCommonMemoryAlloc( NULL, sizeof( SCTModuleList ) ) );
    if( moduleList == NULL )
    {
        sctDestroyList( list );
        return NULL;
    }

    moduleList->list = list;

    return moduleList;
}

/*!
 * Add module to module list. List takes ownership of the module and
 * deallocates it when the list if destroyed.
 *
 * \param list Target list. Must be defined.
 * \param attribute Module to add. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctAddModuleToList( SCTModuleList* list, SCTModule* module )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( module != NULL );
    
    return sctAddToList( list->list, module );
}

/*!
 * Get module list iterator.
 *
 * \param list list. Must be defined.
 * \param iterator Target list iterator. Must be defined.
 *
 */
void sctGetModuleListIterator( SCTModuleList* list, SCTModuleListIterator* iterator )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( iterator != NULL );

    sctGetListIterator( list->list, &( iterator->listIterator ) );
}

/*!
 * Get next module.
 *
 * \param list Target list.
 *
 * \return Module, or NULL if the end of the list has been reached.
 *
 */
SCTModule* sctGetNextModuleListElement( SCTModuleListIterator* listIterator )
{
    SCT_ASSERT_ALWAYS( listIterator != NULL );

    return ( SCTModule* )sctGetNextListElement( &( listIterator->listIterator ) );
}

/*!
 * Destroy module list and all the modules within the list.
 *
 * \param list Target list. Ignored, if NULL.
 *
 */
void sctDestroyModuleList( SCTModuleList* list )
{
    SCTModuleListIterator   iterator;
    SCTModule*              module;

    if( list == NULL )
    {
        return;
    }

    sctGetModuleListIterator( list, &iterator );
    while( ( module = sctGetNextModuleListElement( &iterator ) ) != NULL )
    {
        sctDestroyModule( module );
    }

    sctDestroyList( list->list );
    siCommonMemoryFree( NULL, list );
}
