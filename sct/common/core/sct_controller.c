/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct.h"
#include "sct_types.h"
#include "sct_module.h"
#include "sct_action.h"
#include "sct_utils.h"
#include "sct_parser.h"
#include "sct_reporter.h"
#include "sct_runner.h"
#include "sct_sicommon.h"

#include <stdio.h>
#include <string.h>

#if defined( SCT_INFINITE_BENCHMARK_LOOP )
# include "sct_result.h"
#endif  /* defined( SCT_INFINITE_BENCHMARK_LOOP ) */

/*!
 * SCT context structure.
 */
typedef struct
{
    SCTModuleList*              modules;               /*!< List of system modules. */
    SCTBenchmarkList*           benchmarks;            /*!< List of benchmarks. */
    SCTBenchmarkListIterator    iterator;               /*!< Benchmark list iterator. */
    SCTActionList*              actions;               /*!< List of actions. */
    SCTLog*                     log;                   /*!< Log for reporting initialization warnings and errors. */
    SCTBenchmark*               currentBenchmark;      /*!< Pointer to current benchmark. */
    void*                       benchmarkRun;          /*!< Pointer to current benchmark run. */
    int                         benchmarkCount;         /*!< Number of benchmarks in the benchmark list. */
    int                         benchmarkRepeat;        /*!< Current benchmark repeat. */
    int                         benchmarkIndex;         /*!< Current benchmark index. */
} SCTControllerContext;

/* Local function prototypes. */
static void                     sctiReportError( const char* message, SCTLog *log );
static void                     sctiReportWarnings( SCTLog *log );

/*!
 * Initializes the synthetic content tool.
 *
 * \return Pointer to an sct context, or NULL if failure.
 */
void* sctInitialize( void )
{
    SCTControllerContext*       sctContext      = NULL;
    void*                       siContext       = siCommonGetContext();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: using debug traces" );
#endif  /* defined( SCT_DEBUG ) */
    
    /* Create sct context, set everything to zero. */
    sctContext = ( SCTControllerContext* )siCommonMemoryAlloc( siContext, sizeof( SCTControllerContext ) );
    if( sctContext == NULL )
    {
        siCommonErrorMessage( siContext, "sctInitialize: context creation failed" );
        return NULL;
    }
    memset( sctContext, 0, sizeof( SCTControllerContext ) );

    /* Create main error log. */
    sctContext->log = sctCreateLog();
    if( sctContext->log == NULL )
    {
        siCommonErrorMessage( siContext, "sctInitialize: could not create error log" );
        sctTerminate( sctContext );
        return NULL;
    }
    /* Set the main error log as the system error log. */
    if( sctSetErrorLog( sctContext->log ) == SCT_FALSE )
    {
        siCommonErrorMessage( siContext, "sctInitialize: system error log registering failed" );
        sctTerminate( sctContext );
        return NULL;
    }

    /* Create benchmark and action lists. */
    sctContext->benchmarks = sctCreateBenchmarkList();
    sctContext->actions    = sctCreateActionList();

    /* Check for errors. */
    if( sctContext->benchmarks == NULL ||
        sctContext->actions == NULL )
    {
        siCommonErrorMessage( siContext, "sctInitialize: context element creation failed" );
        sctTerminate( sctContext );
        return NULL;
    }

    /* Get system modules. */
    sctContext->modules = siCommonGetModules( siContext );
    if( sctContext->modules == NULL )
    {
        siCommonErrorMessage( siContext, "sctInitialize: get modules failed" );
        sctTerminate( sctContext );
        return NULL;
    }

    /* Parse input which creates a list of benchmarks and modules. */
    if( sctParseInput( sctContext->modules, 
                       sctContext->benchmarks, 
                       sctContext->actions ) == SCT_FALSE )
    {
        sctiReportWarnings( sctContext->log );
        sctiReportError( "sctInitialize: input parsing failed", sctContext->log );
        sctTerminate( sctContext );
        return NULL;
    }

    sctiReportWarnings( sctContext->log );
    
    /* Determine benchmark count for progress reporting. */
    sctContext->benchmarkCount = sctCountListElements( sctContext->benchmarks->list );

    /* Get benchmark list iterator. */
    sctGetBenchmarkListIterator( sctContext->benchmarks, &( sctContext->iterator ) );

#if !defined( SCT_SKIP_MODULE_INFO_REPORTING )
    /* Report module information. */
    if( sctReportModuleInformation( sctContext->modules ) == SCT_FALSE )
    {
        siCommonErrorMessage( siContext, "sctInitialize: reporting module information failed" );
        sctTerminate( sctContext );
        return NULL;
    }
#endif  /* !defined( SCT_SKIP_MODULE_INFO_REPORTING ) */
    
    /* Report actions. */
    if( sctReportActions( sctContext->actions ) == SCT_FALSE )
    {
        siCommonErrorMessage( siContext, "sctInitialize: reporting actions failed" );
        sctTerminate( sctContext );
        return NULL;
    }

    return sctContext;
}

/*!
 * Executes one benchmarking cycle.
 *
 * \param context Pointer to the internal state object returned by
 * sctInitialize.
 *
 * \return See documentation of SCTCycleStatus.
 */
SCTCycleStatus sctCycle( void *context )
{
    SCTControllerContext*       c = ( SCTControllerContext* )context;
    SCTCycleStatus              status;

#if defined( SCT_USE_PRECYCLE_SLEEP )
    siCommonSleep( NULL, SCT_PRECYCLE_SLEEP_MILLIS * 1000 );
#endif  /* defined( SCT_USE_PRECYCLE_SLEEP ) */

    /* We create benchmark run if one does not exist. */
    if( c->benchmarkRun == NULL )
    {
        /* Get next benchmark to run. */
        c->currentBenchmark = sctGetNextBenchmarkListElement( &( c->iterator ) );

        /* Check if we have reached the end of benchmark list. */
        if( c->currentBenchmark == NULL )
        {
            
#if defined( SCT_INFINITE_BENCHMARK_LOOP )
            sctGetBenchmarkListIterator( c->benchmarks, &( c->iterator ) );
            c->benchmarkIndex  = 0;
            c->benchmarkRepeat = 0;

            return SCT_CYCLE_CONTINUE;           
#else   /* defined( SCT_INFINITE_BENCHMARK_LOOP ) */
            return SCT_CYCLE_FINISHED;
#endif  /* defined( SCT_INFINITE_BENCHMARK_LOOP ) */
        }
        else
        {
            /* Increase benchmark index. */
            c->benchmarkIndex++;
            c->benchmarkRepeat = 0;

            /* Report benchmark run progress. Do this also here to spot problems
             * with benchmark initialization, repeat = 0. */
            siCommonProgressMessage( NULL, c->benchmarkCount, c->benchmarkIndex, c->benchmarkRepeat, c->currentBenchmark->name );
            
#if defined( SCT_INFINITE_BENCHMARK_LOOP )
            sctResetBenchmarkResultList( c->currentBenchmark->benchmarkResultList );
#endif  /* defined( SCT_INFINITE_BENCHMARK_LOOP ) */
                    
            /* Create benchmark run from the current benchmark. */
            c->benchmarkRun = sctCreateBenchmarkRun( c->currentBenchmark );
            if( c->benchmarkRun == NULL )
            {
                if( sctReportBenchmark( c->currentBenchmark ) == SCT_FALSE )
                {
                    return SCT_CYCLE_FATAL_ERROR;
                }
                return SCT_CYCLE_CONTINUE;
            }
        }
    }

    SCT_ASSERT( c->currentBenchmark != NULL );

    c->benchmarkRepeat++;

    /* Report benchmark run progress. */
    siCommonProgressMessage( NULL, c->benchmarkCount, c->benchmarkIndex, c->benchmarkRepeat, c->currentBenchmark->name );

    /* Run benchmark cycle. */
    status = sctRunBenchmarkCycle( c->benchmarkRun );

    /* Benchmark run terminates with cycle finished or error status. */
    if( status != SCT_CYCLE_CONTINUE )
    {
        if( sctReportBenchmark( c->currentBenchmark ) == SCT_FALSE )
        {
            siCommonErrorMessage( NULL, "sctCycle: reporting benchmark result failed" );
            status = SCT_CYCLE_FATAL_ERROR;
        }

        if( siCommonFlushOutput( NULL ) == 0 )
        {
            siCommonErrorMessage( NULL, "sctCycle: flushing output failed" );
            status = SCT_CYCLE_FATAL_ERROR;            
        }
        
        sctDestroyBenchmarkRun( c->benchmarkRun );
        c->benchmarkRun = NULL;

        if( status == SCT_CYCLE_FINISHED )
        {
            /* Benchmark run was finished, but the main cycle continues. */
            status = SCT_CYCLE_CONTINUE;
        }
    }

    return status;
}

/*!
 * Free all resources associated with the synthetic content tool.
 *
 * \param context Pointer to the internal state object returned by
 * sctInitialize.
 *
 */
void sctTerminate( void* context )
{
    SCTControllerContext* c = ( SCTControllerContext* )context;

    if( c == NULL )
    {
        return;
    }

    siCommonFlushOutput( NULL );

    sctDestroyActionListAndElements( c->actions );
    sctDestroyBenchmarkList( c->benchmarks );
    sctDestroyModuleList( c->modules );

    sctDestroyLog( c->log );
    sctDestroyBenchmarkRun( c->benchmarkRun );

    siCommonMemoryFree( NULL, c );

    sctCleanupRegistry();
}

/*!
 * Report logged error message using siCommonErrorMessage.
 *
 * \param message Error message. Must be defined. Max 64 character are reported.
 * \param log Pointer to log. Must be defined.
 *
 */
static void sctiReportError( const char* message, SCTLog* log )
{
    char buffer[ SCT_MAX_ERROR_MSG_LEN + 64 + 1 ];

    SCT_ASSERT( message != NULL );
    SCT_ASSERT( log != NULL );
    SCT_ASSERT( log->error != NULL );
    SCT_ASSERT( strlen( log->error ) > 0 );
    
    sctStrncopy( buffer, message, 64 );
    sctStrncat( buffer, ": ", SCT_MAX_ERROR_MSG_LEN );
    sctStrncat( buffer, log->error, SCT_MAX_ERROR_MSG_LEN );

    siCommonErrorMessage( NULL, buffer );
}

/*!
 * Report logged warnings message using siCommonWarningMessage.
 *
 * \param log Pointer to log. Must be defined.
 *
 */
static void sctiReportWarnings( SCTLog* log )
{
    SCTStringListIterator   logIterator;
    char*                   warningMessage;
    
    if( sctCountWarnings( log ) > 0 )
    {
        sctGetStringListIterator( log->warnings, &logIterator );

        while( ( warningMessage = sctGetNextStringListElement( &logIterator ) ) != NULL )
        {
            siCommonWarningMessage( NULL, warningMessage );
        }
    }
}

