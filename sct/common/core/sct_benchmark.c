/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_benchmark.h"
#include "sct_utils.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_utils.h"

#include <string.h>

/*!
 *
 *
 */
SCTBoolean sctiValidBenchmarkThreadActions( SCTActionList* initActions, SCTActionList* benchmarkActions, SCTActionList* finalActions, SCTActionList* preTerminateActions, SCTActionList* terminateActions );

/*!
 * Create a benchmark. 
 *
 * \param name                  Benchmark name. Must be defined. Only SCT_MAX_BENCHMARK_NAME_LEN characters are copied.
 * \param attributes            Benchmark attributes. Must be defined.
 * \param threadCount           Benchmark thread count. Must be >= 1 and less than or equal to SCT_MAX_THREADS.
 * \param initActions           Benchmark init actions for threads. All array elements must be defined.
 * \param benchmarkActions      Benchmark actions for threads. All array elements must be defined.
 * \param finalActions          Benchmark final actions for threads. Array must be defined, but it may contain all NULL items.
 * \param preTerminateActions   Benchmark pre-terminate actions for threads. Array must be defined, but it may contain all NULL items. 
 * \param terminateActions      Benchmark terminate actions for threads. Array must be defined, but it may contain all NULL items.
 *
 * \return Newly created benchmark, or NULL if failure.
 */
SCTBenchmark* sctCreateBenchmark( const char*       name,
                                  SCTAttributeList* attributes,
                                  int               threadCount,
                                  SCTActionList*    initActions[],
                                  SCTActionList*    benchmarkActions[],
                                  SCTActionList*    finalActions[],
                                  SCTActionList*    preTerminateActions[],
                                  SCTActionList*    terminateActions[] )
{
    SCTBenchmark*   benchmark;
    int             i;

    SCT_ASSERT( name != NULL );
    SCT_ASSERT( attributes != NULL );
    SCT_ASSERT( threadCount >= 1 && threadCount <= SCT_MAX_THREADS );
    SCT_ASSERT( initActions != NULL );
    SCT_ASSERT( benchmarkActions != NULL );
    SCT_ASSERT( finalActions != NULL );
    SCT_ASSERT( preTerminateActions != NULL );    
    SCT_ASSERT( terminateActions != NULL );

    /* We must do action list validation at the beginning as the action list
     * ownership is transferred to benchmark only after the benchmark is created
     * succesfully. In failure, the caller (parser) will delete the action
     * lists. */
    if( sctiValidBenchmarkThreadActions( initActions[ 0 ],
                                         benchmarkActions[ 0 ],
                                         finalActions[ 0 ],
                                         preTerminateActions[ 0 ],
                                         terminateActions[ 0 ] ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "sctCreateBenchmark: invalid main thread" );
        return NULL;
    }  

    for( i = 1; i < threadCount; ++i )
    {
        if( initActions[ i ] == NULL &&
            benchmarkActions[ i ] == NULL &&
            finalActions[ i ] == NULL &&
            preTerminateActions[ i ] == NULL &&
            terminateActions[ i ] == NULL )
        {
            /* Skip empty threads. */
            continue;
        }
        
        if( sctiValidBenchmarkThreadActions( initActions[ i ],
                                             benchmarkActions[ i ],
                                             finalActions[ i ],
                                             preTerminateActions[ i ],
                                             terminateActions[ i ] ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "sctCreateBenchmark: invalid worker thread" );
            return NULL;
        }       
    }

    /* We should be good to go based on the input. */
    
    benchmark = ( SCTBenchmark* )( siCommonMemoryAlloc( NULL, sizeof( SCTBenchmark ) ) );
    if( benchmark == NULL )
    {
        SCT_LOG_ERROR( "sctCreateBenchmark: out of memory" );
        return NULL;
    }
    memset( benchmark, 0, sizeof( SCTBenchmark ) );
    
    benchmark->benchmarkResultList  = sctCreateBenchmarkResultList();
    benchmark->log                  = sctCreateLog();

    if( benchmark->benchmarkResultList == NULL ||
        benchmark->log == NULL )
    {
        SCT_LOG_ERROR( "sctCreateBenchmark: out of memory" );
        sctDestroyBenchmark( benchmark );
        return NULL;
    }

    if( ( benchmark->name = sctDuplicateString( name ) ) == NULL )
    {
        SCT_LOG_ERROR( "sctCreateBenchmark: out of memory" );
        sctDestroyBenchmark( benchmark );
        return NULL;
    }

    /* Check for possible execution count and repeat time attribute. */
    benchmark->repeats       = -1;
    benchmark->minRepeatTime = -1.0f;
    
    if( sctCountListElements( attributes->list ) > 0 )
    {
        if( sctHasAttribute( attributes, SCT_BENCHMARK_REPEATS_ATTRIBUTE ) == SCT_TRUE &&
            sctGetAttributeAsInt( attributes, SCT_BENCHMARK_REPEATS_ATTRIBUTE, &benchmark->repeats ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "sctCreateBenchmark: invalid " SCT_BENCHMARK_REPEATS_ATTRIBUTE " attribute" );
            sctDestroyBenchmark( benchmark );
            
            return NULL;
        }

        if( sctHasAttribute( attributes, SCT_BENCHMARK_MIN_REPEAT_TIME_ATTRIBUTE ) == SCT_TRUE &&
            sctGetAttributeAsFloat( attributes, SCT_BENCHMARK_MIN_REPEAT_TIME_ATTRIBUTE, &benchmark->minRepeatTime ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "sctCreateBenchmark: invalid " SCT_BENCHMARK_MIN_REPEAT_TIME_ATTRIBUTE " attribute" );
            sctDestroyBenchmark( benchmark );
            
            return NULL;
        }

        if( ( benchmark->repeats > 0 ) && ( benchmark->minRepeatTime > 0.0f ) )
        {
            SCT_LOG_ERROR( "sctCreateBenchmark: " SCT_BENCHMARK_MIN_REPEAT_TIME_ATTRIBUTE " and " SCT_BENCHMARK_MIN_REPEAT_TIME_ATTRIBUTE " attributes are mutually exclusive" );
            sctDestroyBenchmark( benchmark );
            
            return NULL;
        }
    }

    benchmark->attributes = attributes;
    
    benchmark->mainThread.initActions           = initActions[ 0 ];
    benchmark->mainThread.benchmarkActions      = benchmarkActions[ 0 ];
    benchmark->mainThread.finalActions          = finalActions[ 0 ];
    benchmark->mainThread.preTerminateActions   = preTerminateActions[ 0 ];
    benchmark->mainThread.terminateActions      = terminateActions[ 0 ];        
    
    for( i = 0; i < ( threadCount - 1 ); ++i )
    {
        benchmark->workerThreads[ i ].initActions           = initActions[ i + 1 ];
        benchmark->workerThreads[ i ].benchmarkActions      = benchmarkActions[ i + 1 ];
        benchmark->workerThreads[ i ].finalActions          = finalActions[ i + 1 ];
        benchmark->workerThreads[ i ].preTerminateActions   = preTerminateActions[ i + 1 ];
        benchmark->workerThreads[ i ].terminateActions      = terminateActions[ i + 1 ];       
    }
        
#if defined( SCT_INFINITE_BENCHMARK_LOOP )
    benchmark->originalRepeats = benchmark->repeats;
#endif  /* defined( SCT_INFINITE_BENCHMARK_LOOP ) */
    
    return benchmark;
}

/*!
 * Check if the benchmark supports explicit complete. Only benchmarks with
 * repeats 0 can be set as complete.
 *
 * \param benchmark Benchmark to complete. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 */
SCTBoolean sctBenchmarkCanComplete( SCTBenchmark* benchmark )
{
    SCT_ASSERT( benchmark != NULL );

    if( benchmark->repeats == 0 )
    {
        return SCT_TRUE;
    }
    else
    {
        return SCT_FALSE;
    }
}

/*!
 * Set benchmark as complete. Only benchmarks with repeats 0 can be set as
 * complete.
 *
 * \param benchmark Benchmark to complete. Must be defined.
 *
 */
void sctBenchmarkComplete( SCTBenchmark* benchmark )
{
    SCT_ASSERT( benchmark != NULL );
    SCT_ASSERT( benchmark->repeats == 0 );

    benchmark->completed = 1;
}

/*!
 * Destroy benchmark. 
 *
 * \param benchmark Benchmark to destroy. Ignored if NULL.
 */
void sctDestroyBenchmark( SCTBenchmark* benchmark )
{
    int i;
    
    if( benchmark != NULL )
    {
        sctDestroyActionList( benchmark->mainThread.initActions );
        sctDestroyActionList( benchmark->mainThread.benchmarkActions );
        sctDestroyActionList( benchmark->mainThread.finalActions );
        sctDestroyActionList( benchmark->mainThread.preTerminateActions );        
        sctDestroyActionList( benchmark->mainThread.terminateActions );
        
        for( i = 0; i < SCT_MAX_WORKER_THREADS; ++i )
        {
            sctDestroyActionList( benchmark->workerThreads[ i ].initActions );
            sctDestroyActionList( benchmark->workerThreads[ i ].benchmarkActions );
            sctDestroyActionList( benchmark->workerThreads[ i ].finalActions );
            sctDestroyActionList( benchmark->workerThreads[ i ].preTerminateActions );            
            sctDestroyActionList( benchmark->workerThreads[ i ].terminateActions );
        }
        
        sctDestroyAttributeList( benchmark->attributes );
        sctDestroyBenchmarkResultList( benchmark->benchmarkResultList  );
        sctDestroyLog( benchmark->log );
        siCommonMemoryFree( NULL, benchmark->name );

        siCommonMemoryFree( NULL, benchmark );
    }
}

/*!
 * Create an empty benchmark list. List owns the benchmarks and
 * destroys them when the list is destroyed.
 *
 * \return Newly-created benchmark list, or NULL if failure.
 */
SCTBenchmarkList* sctCreateBenchmarkList()
{
    SCTList*            list;
    SCTBenchmarkList*   benchmarkList;

    list = sctCreateList();
    if( list == NULL )
    {
        return NULL;
    }
    
    benchmarkList = ( SCTBenchmarkList* )( siCommonMemoryAlloc( NULL, sizeof( SCTBenchmarkList ) ) );
    if( benchmarkList == NULL )
    {
        sctDestroyList( list );
        return NULL;
    }

    benchmarkList->list = list;
    return benchmarkList;
}

/*!
 * Add benchmark to benchmark list. List owns the benchmarks and
 * destroys them when the list is destroyed.
 *
 * \param list Target benchmark list. Must be defined.
 * \param benchmark Benchmark to add. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 */
SCTBoolean sctAddBenchmarkToList( SCTBenchmarkList* list, SCTBenchmark* benchmark )
{
    SCT_ASSERT( list != NULL );
    SCT_ASSERT( benchmark != NULL );
    
    return sctAddToList( list->list, benchmark );
}

/*!
 * Get benchmark list iterator.
 *
 * \param list Benchmark list to iterate. Must be defined.
 * \param iterator Target iterator. Must be defined.
 */
void sctGetBenchmarkListIterator( SCTBenchmarkList* list, SCTBenchmarkListIterator* iterator )
{
    SCT_ASSERT( list != NULL );
    SCT_ASSERT( iterator != NULL );

    sctGetListIterator( list->list, &( iterator->listIterator ) );
}

/*!
 * Get next benchmark list element.
 *
 * \param listIterator Benchmark list iterator. Must be defined.
 *
 * \return Next benchmark, or NULL if the end of the list has been reached.
 */
SCTBenchmark* sctGetNextBenchmarkListElement( SCTBenchmarkListIterator* listIterator )
{
    SCT_ASSERT( listIterator != NULL );

    return ( SCTBenchmark* )( sctGetNextListElement( &( listIterator->listIterator ) ) );
}

/*!
 * Destroy benchmark list and all the benchmarks in the list.
 *
 * \param list Benchmark list to destroy.
 */
void sctDestroyBenchmarkList( SCTBenchmarkList* list )
{
    SCTBenchmarkListIterator    iterator;
    SCTBenchmark*               benchmark;

    if( list == NULL )
    {
        return;
    }

    if( list->list != NULL )
    {
        sctGetBenchmarkListIterator( list, &iterator );
        while( ( benchmark = sctGetNextBenchmarkListElement( &iterator ) ) != NULL )
        {
            sctDestroyBenchmark( benchmark );
        }
        sctDestroyList( list->list );
    }
    
    siCommonMemoryFree( NULL, list );
}

/*!
 * Check if benchmark actions create a valid benchmark thread.
 *
 * \param initActions           List of init actions.
 * \param benchmarkActions      List of benchmark actions.
 * \param finalActions          List of final actions.
 * \param preTerminateActions   List of pre-terminate actions.   
 * \param terminateActions      List of terminate actions.  
 * 
 * \return SCT_TRUE if the thread is valid, SCT_FALSE if invalid.
 */
SCTBoolean sctiValidBenchmarkThreadActions( SCTActionList* initActions,
                                            SCTActionList* benchmarkActions,
                                            SCTActionList* finalActions,
                                            SCTActionList* preTerminateActions,
                                            SCTActionList* terminateActions )
{

    SCTActionListIterator   iterator;
    SCTAction*              action;

    SCT_USE_VARIABLE( initActions );
    SCT_USE_VARIABLE( finalActions );
    SCT_USE_VARIABLE( preTerminateActions );
    SCT_USE_VARIABLE( terminateActions );
    
    if( benchmarkActions == NULL )
    {
        return SCT_FALSE;
    }
    
    sctGetActionListIterator( benchmarkActions, &iterator );
    while( ( action = sctGetNextActionListElement( &iterator ) ) != NULL )
    {
        if( action->execute == NULL )
        {
            return SCT_FALSE;
        }                
    }
    
    return SCT_TRUE;
}
