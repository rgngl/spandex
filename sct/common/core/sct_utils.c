/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 * Mersene twister code adopted from
 * http://www.qbrundage.com/michaelb/pubs/essays/random_number_generation.html,
 * by Michael L. Brundage.
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_module.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include "sct_systemmodule.h"

#if defined( INCLUDE_THREAD_MODULE )
#include "sct_threadmodule.h"
#endif  /* defined( INCLUDE_THREAD_MODULE ) */
#if defined( INCLUDE_CPU_MODULE )
#include "sct_cpumodule.h"
#endif  /* defined( INCLUDE_CPU_MODULE ) */
#if defined( INCLUDE_MEMORY_MODULE )
#include "sct_memorymodule.h"
#endif  /* defined( INCLUDE_MEMORY_MODULE ) */
#if defined( INCLUDE_EGL_MODULE )
#include "sct_eglmodule.h"
#endif  /* defined( INCLUDE_EGL_MODULE ) */
#if defined( INCLUDE_VGI_MODULE )
#include "sct_vgimodule.h"
#endif  /* defined( INCLUDE_VGI_MODULE ) */
#if defined( INCLUDE_VGTEST_MODULE )
#include "sct_vgtestmodule.h"
#endif  /* defined( INCLUDE_VGTEST_MODULE ) */
#if defined( INCLUDE_GLTEST_MODULE )
#include "sct_gltestmodule.h"
#endif  /* defined( INCLUDE_GLTEST_MODULE ) */
#if defined( INCLUDE_OPENVG_MODULE )
#include "sct_openvgmodule.h"
#endif  /* defined( INCLUDE_OPENVG_MODULE ) */
#if defined( INCLUDE_VGU_MODULE )
#include "sct_vgumodule.h"
#endif  /* defined( INCLUDE_VGU_MODULE ) */
#if defined( INCLUDE_VGCTRACE_MODULE )
#include "sct_vgctracemodule.h"
#endif  /* defined( INCLUDE_VGCTRACE_MODULE ) */
#if defined( INCLUDE_GL1CTRACE_MODULE )
#include "sct_gl1ctracemodule.h"
#endif  /* defined( INCLUDE_GL1CTRACE_MODULE ) */
#if defined( INCLUDE_GL2CTRACE_MODULE )
#include "sct_gl2ctracemodule.h"
#endif  /* defined( INCLUDE_GL2CTRACE_MODULE ) */
#if defined( INCLUDE_OPENGLES1_MODULE )
#include "sct_opengles1module.h"
#endif  /* defined( INCLUDE_OPENGLES1_MODULE ) */
#if defined( INCLUDE_OPENGLES2_MODULE )
#include "sct_opengles2module.h"
#endif  /* defined( INCLUDE_OPENGLES2_MODULE ) */
#if defined( INCLUDE_IMAGES_MODULE )
#include "sct_imagesmodule.h"
#endif  /* defined( INCLUDE_IMAGES_MODULE ) */
#if defined( INCLUDE_TEST_MODULE )
#include "sct_testmodule.h"
#endif  /* defined( INCLUDE_TEST_MODULE ) */

#if defined( INCLUDE_OPENVGTRACE_MODULE )
#include "sct_openvgtracemodule.h"
#endif  /* defined( INCLUDE_OPENVGTRACE_MODULE ) */
#if defined( INCLUDE_OPENGLES1TRACE_MODULE )
#include "sct_opengles1tracemodule.h"
#endif  /* defined( INCLUDE_OPENGLES1TRACE_MODULE ) */
#if defined( INCLUDE_OPENGLES2TRACE_MODULE )
#include "sct_opengles2tracemodule.h"
#endif  /* defined( INCLUDE_OPENGLES2TRACE_MODULE ) */

/* Internal helper function declarations */
static SCTBoolean       sctiValidateInt( const char* str );
static SCTBoolean       sctiValidateFloat( const char* str );
static SCTBoolean       sctiValidateHex( const char* str );
static char*            sctiTokenize( char* buf, char c );
static char*            sctiCreateString( int length );

/*!
 * Get available spandex core modules. The availability of a particular module
 * is detected by having matching INCLUDE_*_MODULE macro defined.
 *
 * \return Module list, or NULL if failure.
 */
SCTModuleList* sctGetCoreModules()
{
    SCTModuleList*  modules;
    SCTModule*      module;

    modules = sctCreateModuleList();
    
    if( modules == NULL )
    {
        return NULL;
    }

    module = sctCreateSystemModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        return NULL;
    }

#if defined( INCLUDE_THREAD_MODULE )
    module = sctCreateThreadModule();
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_THREAD_MODULE ) */
    
#if defined( INCLUDE_CPU_MODULE )
    module = sctCreateCpuModule();
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_CPU_MODULE ) */

#if defined( INCLUDE_MEMORY_MODULE )
    module = sctCreateMemoryModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_MEMORY_MODULE ) */

#if defined( INCLUDE_EGL_MODULE )
    module = sctCreateEglModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_EGL_MODULE ) */

#if defined( INCLUDE_VGI_MODULE )
    module = sctCreateVgiModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_VGI_MODULE ) */

#if defined( INCLUDE_VGTEST_MODULE )
    module = sctCreateVgtestModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_VGTEST_MODULE ) */

#if defined( INCLUDE_GLTEST_MODULE )
    module = sctCreateGltestModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_GLTEST_MODULE ) */

#if defined( INCLUDE_OPENVG_MODULE )
    module = sctCreateOpenVGModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_OPENVG_MODULE ) */

#if defined( INCLUDE_VGU_MODULE )
    module = sctCreateVguModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_VGU_MODULE ) */
       
#if defined( INCLUDE_OPENGLES1_MODULE )
    module = sctCreateOpenGLES1Module();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_OPENGLES1_MODULE ) */
    
#if defined( INCLUDE_OPENGLES2_MODULE )
    module = sctCreateOpenGLES2Module();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_OPENGLES2_MODULE ) */

#if defined( INCLUDE_VGCTRACE_MODULE )
    module = sctCreateVgCtraceModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_VGCTRACE_MODULE ) */

#if defined( INCLUDE_GL1CTRACE_MODULE )
    module = sctCreateGl1CtraceModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_GL1CTRACE_MODULE ) */

#if defined( INCLUDE_GL2CTRACE_MODULE )
    module = sctCreateGl2CtraceModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_GL2CTRACE_MODULE ) */
    
#if defined( INCLUDE_IMAGES_MODULE )
    module = sctCreateImagesModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_IMAGES_MODULE ) */

#if defined( INCLUDE_TEST_MODULE )
    module = sctCreateTestModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_TEST_MODULE ) */

#if defined( INCLUDE_OPENVGTRACE_MODULE )
    module = sctCreateOpenVGTraceModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_OPENVGTRACE_MODULE ) */

#if defined( INCLUDE_OPENGLES1TRACE_MODULE )
    module = sctCreateOpenGLES1TraceModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_OPENGLES1TRACE_MODULE ) */

#if defined( INCLUDE_OPENGLES2TRACE_MODULE )
    module = sctCreateOpenGLES2TraceModule();
    
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        
        return NULL;
    }
#endif  /* defined( INCLUDE_OPENGLES2TRACE_MODULE ) */
    
    return modules;
}

/*!
 * Create attribute. Attribute is a {name,value} pair of zero
 * terminated strings. 
 *
 * \param name Attribute name string. Must be defined.
 * \param value Attribute value string. Must be defined.
 *
 * \return Newly created attribute, or NULL if failure.
 *
 */
SCTAttribute* sctCreateAttribute( const char* name, const char* value )
{
    SCTAttribute*   attribute;
    void*           context     = siCommonGetContext();                

    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( value != NULL );

    /* Allocate memory for attribute. */
    attribute = ( SCTAttribute* )( siCommonMemoryAlloc( context, sizeof( SCTAttribute ) ) );
    if( attribute == NULL )
    {
        return NULL;
    }
    memset( attribute, 0, sizeof( SCTAttribute ) );

    attribute->name  = sctDuplicateString( name );
    attribute->value = sctDuplicateString( value );
    if( attribute->name == NULL || attribute->value == NULL )
    {
        sctDestroyAttribute( attribute );
        return NULL;
    }
    return attribute;
}

/*!
 * Destroy attribute.
 *
 * \param attribute Attribute to destroy. Ignored, if NULL.
 *
 */
void sctDestroyAttribute( SCTAttribute* attribute )
{
    void*   context;

    if( attribute != NULL )
    {
        context = siCommonGetContext();                
    
        if( attribute->name != NULL )
        {
            siCommonMemoryFree( context, attribute->name );
        }
        if( attribute->value != NULL )
        {
            siCommonMemoryFree( context, attribute->value );
        }
        siCommonMemoryFree( context, attribute );
    }
}

/*!
 * Create a new list. List is initially empty.
 *
 * \return Newly created list, or NULL if failure.
 *
 */
SCTList* sctCreateList()
{
    /* Allocate memory for list. */
    SCTList* list = ( SCTList* )( siCommonMemoryAlloc( NULL, sizeof( SCTList ) ) );
    if( list == NULL )
    {
        return NULL;
    }
    /* Set list pointer to NULL. */
    list->head = NULL;
    list->tail = NULL;

    return list;
}

/*!
 * Add element to the list. List DOES NOT take responsibility of the
 * added element, i.e. element is not deallocated when the list is
 * destroyed.
 *
 * \param list Target list. Must be defined.
 * \param element Element to add. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctAddToList( SCTList* list, void* element )
{
    SCTListElement* listElement;

    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( element != NULL );

    /* Create internal list element which wraps the void* list element. */
    listElement = ( SCTListElement* )( siCommonMemoryAlloc( NULL, sizeof( SCTListElement ) ) );
    if( listElement == NULL )
    {
        return SCT_FALSE;
    }

    /* Assing void* list element. */
    listElement->element = element;
    listElement->next    = NULL;
    listElement->prev    = NULL;

    /* Add list element to the end of the list. */
    if( list->head == NULL ) /* Empty list. */
    {
        SCT_ASSERT_ALWAYS( list->tail == NULL );
        list->head = listElement;
        list->tail = listElement;
    }
    else
    {
        SCT_ASSERT_ALWAYS( list->tail != NULL );
        list->tail->next  = listElement;
        listElement->prev = list->tail;
        list->tail        = listElement;
    }

    return SCT_TRUE;
}

/*!
 * Remove element from the list. Only the first instance is removed in case the
 * list contains the element several times. A succesfull remove may invalidate
 * existing list iterators.
 *
 * \param list Target list. Must be defined.
 * \param element Element to remove. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctRemoveFromList( SCTList* list, void* element )
{
    SCTListElement* listElement;

    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( element != NULL );
    
    listElement = list->head;
    
    while( listElement != NULL )
    {
        if( listElement->element == element )
        {
            /* Found element, detach from the list. */
            if( listElement->prev != NULL )
            {
                listElement->prev->next = listElement->next;
            }

            if( listElement->next != NULL )
            {
                listElement->next->prev = listElement->prev;
            }
            
            if( listElement == list->head )
            {
                list->head = listElement->next;
            }
            
            if( listElement == list->tail )
            {
                list->tail = listElement->prev;
            }

            siCommonMemoryFree( NULL, listElement );
            return SCT_TRUE;
        }
        else
        {
            listElement = listElement->next;
        }
    }

    /* Not found. */
    return SCT_FALSE;
}


/*!
 * Create list iterator. Iteration starts from the list head.
 *
 * \param list Target list. Must be defined.
 * \param iterator Iterator. Must be defined.
 *
 */
void sctGetListIterator( SCTList* list, SCTListIterator* iterator )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( iterator != NULL );

    /* Setup list iterator members. */
    iterator->current = list->head;

    if( list->head == NULL )
    {
        iterator->next = NULL;
    }
    else
    {
        iterator->next = list->head->next;
    }
}

/*!
 * Create list iterator. Iteration starts from the list head.
 *
 * \param list Target list. Must be defined.
 * \param iterator Iterator. Must be defined.
 *
 */
void sctGetReverseListIterator( SCTList* list, SCTReverseListIterator* iterator )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( iterator != NULL );

    /* Setup list iterator members. */
    iterator->current = list->tail;

    if( list->tail == NULL )
    {
        iterator->prev = NULL;
    }
    else
    {
        iterator->prev = list->tail->prev;
    }
}

/*!
 * Get next list element through list iterator. After the call, the iterator
 * will point into next list element, if any.
 *
 * \param listIterator list iterator. Must be defined.
 *
 * \return Next list element, or NULL if the iterator has reached the
 * end of the list.
 *
 */
void* sctGetNextListElement( SCTListIterator* listIterator )
{
    void*   element = NULL;

    SCT_ASSERT_ALWAYS( listIterator != NULL );

    if( listIterator->current != NULL )
    {
        element = listIterator->current->element;
        listIterator->current = listIterator->next;

        if( listIterator->current == NULL )
        {
            listIterator->next = NULL;
        }
        else
        {
            listIterator->next = listIterator->current->next;
        }
    }        

    return element;
}

/*!
 * Get previous list element through list iterator. After the call, the iterator
 * will point into previous list element, if any.
 *
 * \param listIterator list iterator. Must be defined.
 *
 * \return Previous list element, or NULL if the iterator has reached
 * the start of the list.
 *
 */
void* sctGetPrevListElement( SCTReverseListIterator* listIterator )
{
    void*   element = NULL;

    SCT_ASSERT_ALWAYS( listIterator != NULL );

    if( listIterator->current != NULL )
    {
        element = listIterator->current->element;
        listIterator->current = listIterator->prev;

        if( listIterator->current == NULL )
        {
            listIterator->prev = NULL;
        }
        else
        {
            listIterator->prev = listIterator->current->prev;
        }
    }        

    return element;
}

/*! Count the number of elements in a list.
 *
 * \param list pointer to list. Must be defined.
 *
 * \return Number of elements in the list.
 * */
int sctCountListElements( SCTList* list )
{
    SCTListIterator iterator;
    int             count       = 0;

    SCT_ASSERT_ALWAYS( list != NULL );

    sctGetListIterator( list, &iterator );
    while( sctGetNextListElement( &iterator ) != NULL )
    {
        ++count;
    }
    
    return count;
}

/*!
 * Destroy list. Note that list elements are not destroyed.
 *
 * \param list Target list. Ignored, if NULL.  
 */
void sctDestroyList( SCTList* list )
{
    if( list == NULL )
    {
        return;
    }

    sctDestroyListElements( list );
    siCommonMemoryFree( NULL, list );
}

/*!
 * Destroy the list elements, the list is left empty.
 *
 * \param list target list
 */
void sctDestroyListElements( SCTList* list )
{
    void*           context     = siCommonGetContext();
    SCTListElement* listElement;

    while( list->head != NULL )
    {
        listElement = list->head;
        list->head  = list->head->next;

        siCommonMemoryFree( context, listElement );
    }
    list->tail = NULL;

    SCT_ASSERT_ALWAYS( list->head == NULL );
    SCT_ASSERT_ALWAYS( sctCountListElements( list ) == 0 );
}

/*!
 * Create attribute list. List owns the attributes and deallocates
 * them when the list is destroyed.
 *
 * \return Newly created list, or NULL, if failure.
 *
 */
SCTAttributeList* sctCreateAttributeList()
{
    SCTList*            list;
    SCTAttributeList*   attributeList;

    list = sctCreateList();
    if( list == NULL )
    {
        return NULL;
    }
    
    attributeList = ( SCTAttributeList* )( siCommonMemoryAlloc( NULL, sizeof( SCTAttributeList ) ) );
    if( attributeList == NULL )
    {
        sctDestroyList( list );
        return NULL;
    }

    attributeList->list = list;
    return attributeList;
}

/*!
 * Add attribute to attribute list. List takes ownership of the added
 * attribute and deallocates it when the list is destroyed.
 *
 * \param list Target list. Must be defined.
 * \param attribute Attribute to add. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctAddAttributeToList( SCTAttributeList* list, SCTAttribute* attribute )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( attribute != NULL );
    
    return sctAddToList( list->list, attribute );
}

/*!
 * Create a new attribute and add it to the list. List takes ownership of the
 * newly-created attribute and deallocates it when the list is destroyed. Name
 * and value are copied.
 *
 * \param list Target list. Must be defined.
 * \param name Attribute name. Must be defined.
 * \param value Attribute name. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 */
SCTBoolean sctAddNameValueToList( SCTAttributeList* list, const char* name, const char* value )
{
    SCTAttribute*   attribute;

    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( value != NULL );

    attribute = sctCreateAttribute( name, value );
    if( attribute == NULL )
    {
        return SCT_FALSE;
    }
    if( sctAddAttributeToList( list, attribute ) == SCT_FALSE )
    {
        sctDestroyAttribute( attribute );
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 * Get attribute list iterator.
 *
 * \param list Target list. Must be defined.
 * \param iterator Target iterator. Must be defined.
 *
 */
void sctGetAttributeListIterator( const SCTAttributeList* list, SCTAttributeListIterator* iterator )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( iterator != NULL );

    sctGetListIterator( list->list, &( iterator->listIterator ) );
}

/*!
 * Get next attribute.
 *
 * \param list Target list. Must be defined.
 *
 * \return Attribute, or NULL if the end of the list has been reached.
 *
 */
SCTAttribute* sctGetNextAttributeListElement( SCTAttributeListIterator* listIterator )
{
    SCT_ASSERT_ALWAYS( listIterator != NULL );

    return ( SCTAttribute* )sctGetNextListElement( &( listIterator->listIterator ) );
}

/*!
 * Destroy attribute list, along with all the attributes.
 *
 * \param list Target list. Ignored, if NULL.
 *
 */
void sctDestroyAttributeList( SCTAttributeList* list )
{
    SCTAttributeListIterator    iterator;
    SCTAttribute*               attribute;

    if( list == NULL )
    {
        return;
    }

    sctGetAttributeListIterator( list, &iterator );
    while( ( attribute = sctGetNextAttributeListElement( &iterator ) ) != NULL )
    {
        sctDestroyAttribute( attribute );
    }
    
    sctDestroyList( list->list );
    siCommonMemoryFree( NULL, list );
}

/*!
 * Creates a list for holding C strings. The list owns the strings so
 * their memory will be freed by sctDestroyStringList.
 *
 * \return Empty string list, or NULL if failure.
 */
SCTStringList* sctCreateStringList()
{
    SCTList*        list;
    SCTStringList*  stringList;

    list = sctCreateList();
    if( list == NULL )
    {
        return NULL;
    }
    
    stringList = ( SCTStringList* )( siCommonMemoryAlloc( NULL, sizeof( SCTStringList ) ) );
    if( stringList == NULL )
    {
        sctDestroyList( list );
        return NULL;
    }

    stringList->list = list;
    return stringList;
}

/*!
 * Destroys a string list and all the contained strings.
 *
 * \param list string list to be destroyed. Ignored if NULL.
 */
void sctDestroyStringList( SCTStringList* list )
{
    if( list == NULL )
    {
        return;
    }

    sctDestroyStringListElements( list );
    sctDestroyList( list->list );
    siCommonMemoryFree( NULL, list );
}

/*!
 * Destroys strings in the list, the list is left empty.
 *
 * \param list target list. Ignored if NULL.
 */
void sctDestroyStringListElements( SCTStringList* list )
{
    SCTStringListIterator   iterator;
    char*                   string;

    if( list == NULL )
    {
        return;
    }

    sctGetStringListIterator( list, &iterator );
    while( ( string = sctGetNextStringListElement( &iterator ) ) != NULL )
    {
        siCommonMemoryFree( NULL, string );
    }
    sctDestroyListElements( list->list );
}

/*!
 * Adds a string pointer to list. List takes ownership of the given
 * string.
 *
 * \param list Pointer to string list. Must be defined.
 * \param string Pointer to C string. Must be defined.
 *
 * \return  SCT_TRUE if succesfull, SCT_FALSE otherwise
 */
SCTBoolean sctAddStringToList( SCTStringList* list, char* string )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( string != NULL );

    return sctAddToList( list->list, string );
}

/*!
 * Reserves memory for a string of given length. An extra character is
 * reserved for trailing NULL character. Puts NULL character at the
 * end of the string.
 *
 * \param length length of the string to create. Must be >= 0.
 *
 * \return Pointer to the string or NULL if creation failed.
 */
char* sctiCreateString( int length )
{
    char*   string;

    SCT_ASSERT_ALWAYS( length >= 0 );

    string = ( char* )( siCommonMemoryAlloc( NULL, length + 1 ) );
    if( string == NULL )
    {
        return NULL;
    }
    string[ length ] = '\0';
    return string;
}

/*!
 *
 *
 */
char* sctDuplicateString( const char* string )
{
    char*   newString;

    SCT_ASSERT( string != NULL );

    newString = sctiCreateString( strlen( string ) );
    if( newString == NULL )
    {
        return NULL;
    }
    strcpy( newString, string );
    return newString;
}

/*!
 * Adds a copy of a string to list. List does not take ownership of
 * the given input string.
 *
 * \param list Pointer to string list. Must be defined.
 * \param string Pointer to C string. Must be defined.
 *
 * \return SCT_TRUE if succesful, SCT_FALSE otherwise.
 */
SCTBoolean sctAddStringCopyToList( SCTStringList* list, const char* string )
{
    char*   stringCopy = NULL;
    
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( string != NULL );

    stringCopy = sctiCreateString( strlen( string ) );
    if( stringCopy == NULL )
    {
        return SCT_FALSE;
    }
    strcpy( stringCopy, string );
    
    if( sctAddToList( list->list, stringCopy ) == SCT_TRUE )
    {
        return SCT_TRUE;
    }
    siCommonMemoryFree( NULL, stringCopy );
    return SCT_FALSE;
}

/*!
 * Initializes a string list iterator.
 *
 * \param list pointer to string list. Must be defined.
 * \param iterator pointer to an iterator. Must be defined.
 */
void sctGetStringListIterator( const SCTStringList* list, SCTStringListIterator* iterator )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( iterator != NULL );

    sctGetListIterator( list->list, &( iterator->listIterator ) );
}

/*!
 * Returns the next element from a string list the iterator is
 * associated with.
 *
 * \param iterator Pointer to string list iterator. Must be defined.
 *
 * \return Pointer to the next element or NULL if no more elements.
 */
char* sctGetNextStringListElement( SCTStringListIterator* iterator )
{
    SCT_ASSERT_ALWAYS( iterator != NULL );

    return ( char* )( sctGetNextListElement( &( iterator->listIterator ) ) );
}

/*!
 * Returns the string a the given index. Provides random access to a
 * string list.
 *
 * \param list Target list. Must be defined.
 * \param index 0-based index. Must be >= 0.
 *
 * \return pointer to the string at index or NULL if there are not
 * enough elements in the list
 */
char* sctGetStringAtIndex( SCTStringList* list, int index )
{
    SCTStringListIterator   iter;
    int                     i       = 0;

    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( index >= 0 );

    sctGetStringListIterator( list, &iter );
    do
    {
        if( i++ == index )
        {
            break;
        }
    } while( sctGetNextStringListElement( &iter ) != NULL );
    /* sctGetNextStringListElement returns NULL if there are no more
     * elements. */
    return sctGetNextStringListElement( &iter );
}

/*!
 * Splits a string using the given separator character and returns the
 * sub-strings in a string list. Separators are removed from the the
 * sub-strings. The functionality tries to mimic Python string.split when used
 * with single character separators. For example, splitting " " with separator '
 * ' results to two empty strings.
 *
 * \param string C string to be split. Must be defined.
 * \param len length of the string. Must be greater than or equal to 0.
 * \param sep Separator character.
 *
 * \return String list, or NULL if failure. Original string is
 * returned as first element of the string list if it does not contain
 * the separator character.
 */
SCTStringList* sctSplitString( const char* string, int len, char sep )
{
    SCTStringList*  list;
    int             i, subStringStart;
    char*           subString;
    unsigned int    subStringLength;

    SCT_ASSERT_ALWAYS( string != NULL );
    SCT_ASSERT_ALWAYS( len >= 0 );
    
    list = sctCreateStringList();
    if( list == NULL )
    {
        return NULL;
    }

    /* Scan the string looking for the separator. */
    for( i = subStringStart = 0; i < len; i++ )
    {
        if( string[ i ] == sep )
        {
            /* Create a new string for the found sub-string. Space has to be
             * reserved also for the null character. */
            subStringLength = i - subStringStart;
            subString       = sctiCreateString( subStringLength );
            if( subString == NULL )
            {
                /* Allocation failed, destroy the list that has been built so
                 * far and bail out. */
                sctDestroyStringList( list );
                return NULL;
            }
            /* Copy the sub-string if it is not empty. */
            if( subStringLength > 0 )
            {
                sctStrncopy( subString, &string[subStringStart], subStringLength );
            }
            /* Place the sub-string to the list. */
            if( sctAddStringToList( list, subString ) == SCT_FALSE )
            {
                siCommonMemoryFree( NULL, subString );
                sctDestroyStringList( list );
                return NULL;
            }
            /* Advance the sub-string start index to the next character from the
             * found separator */
            subStringStart = i + 1;
        }
    }

    /* Add the remaining portion of the string to the list. */
    subString = sctiCreateString( len - subStringStart );
    if( subString == NULL )
    {
        sctDestroyStringList( list );
        return NULL;
    }
    sctStrncopy( subString, &string[ subStringStart ], len - subStringStart );
    if( sctAddStringToList( list, subString ) == SCT_FALSE )
    {
        siCommonMemoryFree( NULL, subString );
        sctDestroyStringList( list );
        return NULL;
    }

    return list;
}

/*!
 * Searches for the first attribute with the given name.
 *
 * \param attributes List of attributes. Must be defined.
 * \param name Attribute name to look for. Must be defined.
 *
 * \return Pointer to the first attribute with the given name, or NULL.
 */
SCTAttribute* sctFindAttribute( SCTAttributeList* attributes, const char* name )
{
    SCTAttributeListIterator    iter;
    SCTAttribute*               attribute;

    SCT_ASSERT( attributes != NULL );
    SCT_ASSERT( name != NULL );
    
    sctGetAttributeListIterator( attributes, &iter );

    while( (attribute = sctGetNextAttributeListElement( &iter )) != NULL )
    {
        if( strcmp( attribute->name, name ) == 0 )
        {
            return attribute;
        }
    }
    return NULL;
}

/*!
 * Checks if the attribute list has named attribute or not.
 *
 * \param attributes Pointer to the attribute list. Must be given.
 * \param attributeName Name of the attribute. Must be given.
 *
 * \return SCT_TRUE if the attribute can be found, SCT_FALSE otherwise.
 */
SCTBoolean sctHasAttribute( SCTAttributeList* attributes, const char* attributeName )
{
    SCTAttribute*   attribute;

    SCT_ASSERT_ALWAYS( attributes != NULL );
    SCT_ASSERT_ALWAYS( attributeName != NULL );

    attribute = sctFindAttribute( attributes, attributeName );
    if( attribute == NULL )
    {
        return SCT_FALSE;
    }
    else
    {
        return SCT_TRUE;
    }
}

/*!
 * Gets an integer value from an attribute list.
 *
 * \param attributes Pointer to the attribute list. Must be given.
 * \param attributeName Name of the attribute. Must be given.
 * \param value Output argument for receiving the value. Must be given.
 *
 * \return SCT_TRUE if successful, SCT_FALSE if no attribute was
 * found, or the attribute was not a valid integer.
 *
 * \note Value is not changed on failure. 
 */
SCTBoolean sctGetAttributeAsInt( SCTAttributeList* attributes, const char* attributeName, int* value )
{
    SCTAttribute*   attribute;

    SCT_ASSERT_ALWAYS( attributes != NULL );
    SCT_ASSERT_ALWAYS( attributeName != NULL );
    SCT_ASSERT_ALWAYS( value != NULL );

    attribute = sctFindAttribute( attributes, attributeName );
    if( attribute == NULL || sctiValidateInt( attribute->value ) == SCT_FALSE )
    {
        return SCT_FALSE;
    }

    *value = atoi( attribute->value );
    return SCT_TRUE;
}

/*!
 * Gets an (unsigned) hex integer value from an attribute list.  The
 * string must be in 0x-format.
 *
 * \param attributes Pointer to the attribute list. Must be given.
 * \param attributeName Name of the attribute. Must be given.
 * \param value Output argument for receiving the value. Must be given.
 *
 * \return SCT_TRUE if successful, SCT_FALSE if no attribute was
 * found, or the attribute was not a valid integer.
 *
 * \note Value is not changed on failure. 
 */
SCTBoolean sctGetAttributeAsHex( SCTAttributeList* attributes, const char* attributeName, unsigned int* value )
{
    SCTAttribute*   attribute;
    char*           p;
    unsigned long   temp;

    SCT_ASSERT_ALWAYS( attributes != NULL );
    SCT_ASSERT_ALWAYS( attributeName != NULL );
    SCT_ASSERT_ALWAYS( value != NULL );
    
    attribute = sctFindAttribute( attributes, attributeName );
    if( attribute == NULL || sctiValidateHex( attribute->value ) == SCT_FALSE )
    {
        return SCT_FALSE;
    }

    temp = strtoul( attribute->value + 2, &p, 16 );
    if( *p )
    {
        return SCT_FALSE;
    }
    *value = temp;
    return SCT_TRUE;
}

/*!
 * Gets a floating point value from an attribute list.
 *
 * \param attributes Pointer to the attribute list. Must be given.
 * \param attributeName Name of the attribute. Must be given.
 * \param value Output argument for receiving the value. Must be given.
 *
 * \return SCT_TRUE if successfull, SCT_FALSE if no attribute was
 * found, or the attribute was not a valid floating point value.
 *
 * \note Value is not changed on failure. Exponent-format is not supported.
 */
SCTBoolean sctGetAttributeAsFloat( SCTAttributeList* attributes, const char* attributeName, float* value )
{
    SCTAttribute*   attribute;

    SCT_ASSERT_ALWAYS( attributes != NULL );
    SCT_ASSERT_ALWAYS( attributeName != NULL );
    SCT_ASSERT_ALWAYS( value != NULL );

    attribute = sctFindAttribute( attributes, attributeName );
    if( attribute == NULL || sctiValidateFloat( attribute->value ) == SCT_FALSE )
    {
        return SCT_FALSE;
    }

    *value = ( float )( sctAtof( attribute->value ) );
    return SCT_TRUE;
}

/*!
 * Gets a string value from an attribute list. Escape characters marked with
 * SCT_ESCAPE_CHAR within the string are converted into matching c escape chars.
 *
 * \param attributes    Pointer to the attribute list. Must be given.
 * \param attributeName Name of the attribute. Must be given.
 * \param buffer        Output buffer for receiving the value. Must be given.
 * \param count         Maximum number of characters to copy into the output buffer. Buffer must
 *                      have space to trailing null character. Must be > 0.
 *
 * \return SCT_TRUE if successful, SCT_FALSE if the attribute was not found, or
 * if it contains an invalid escape character.
 *
 * \note Buffer is changed in case of invalid escape character. 
 */
SCTBoolean sctGetAttributeAsString( SCTAttributeList* attributes, const char* attributeName, char* buffer, int count )
{
    SCTAttribute*   attribute;
    int             i;
    int             j;
    char            c;

    SCT_ASSERT_ALWAYS( attributes != NULL );
    SCT_ASSERT_ALWAYS( attributeName != NULL );
    SCT_ASSERT_ALWAYS( buffer != NULL );
    SCT_ASSERT_ALWAYS( count > 0 );

    attribute = sctFindAttribute( attributes, attributeName );
    if( attribute == NULL )
    {
        return SCT_FALSE;
    }

    for( i = 0, j = 0; ( c = attribute->value[ i ] ) != '\0' && j < count; ++i, ++j )
    {
        if( c == SCT_ESCAPE_CHAR )
        {
            switch( attribute->value[ ++i ] )
            {
            case 'b':
                buffer[ j ] = '\b';
                break;
            case 't':
                buffer[ j ] = '\t';
                break;
            case 'n':
                buffer[ j ] = '\n';
                break;
            case 'v':
                buffer[ j ] = '\v';
                break;
            case 'f':
                buffer[ j ] = '\f';
                break;
            case 'r':
                buffer[ j ] = '\r';
                break;
            case '\\':
                buffer[ j ] = '\\';
                break;
            case '\'':
                buffer[ j ] = '\'';
                break;
            case '\"':
                buffer[ j ] = '\"';
                break;
            case '0':
                buffer[ j ] = '\0';
                break;
            case '\a':
                buffer[ j ] = '\a';
                break;
            default:
                return SCT_FALSE;
            }               
        }
        else
        {
            buffer[ j ] = c;
        }
    }

    buffer[ j ] = '\0';
    
    return SCT_TRUE;
}

/*!
 * Gets "x*y" value from an attribute list.
 *
 * \param attributes Pointer to the attribute list. Must be given.
 * \param attributeName Name of the attribute. Must be given.
 * \param xvalue Output argument for receiving the x-value. Must be given.
 * \param yvalue Output argument for receiving the y-value. Must be given.
 *
 * \return SCT_TRUE if successfull, SCT_FALSE if the attribute was not
 * found, or it was not in valid "x*y" format.
 *
 * \note Values are not changed on failure.
 */
SCTBoolean sctGetAttributeAsXY( SCTAttributeList* attributes, const char* attributeName, int* xvalue, int* yvalue )
{
    SCTAttribute*   attribute;
    char            buf[ 32 ];  /* TODO: 32 characters is enough for string holding two integers? */
    char*           x;
    char*           y;

    SCT_ASSERT_ALWAYS( attributes != NULL );
    SCT_ASSERT_ALWAYS( attributeName != NULL );
    SCT_ASSERT_ALWAYS( xvalue != NULL );
    SCT_ASSERT_ALWAYS( yvalue != NULL );

    attribute = sctFindAttribute( attributes, attributeName );
    if( attribute == NULL )
    {
        return SCT_FALSE;
    }

    /* Make sure the attribute string to parse is not too long. */
    if( strlen( attribute->value ) >= SCT_ARRAY_LENGTH( buf ) )
    {
        return SCT_FALSE;
    }

    /* Copy string to temp buffer */
    sctStrncopy( buf, attribute->value, SCT_ARRAY_LENGTH( buf ) );            

    /* Separate x and y */
    x = buf;
    y = sctiTokenize( buf, '*' );

    /* Validate x and y */
    if( x == NULL || sctiValidateInt( x ) == SCT_FALSE )
    {
        return SCT_FALSE;
    }
    if( y == NULL || sctiValidateInt( y ) == SCT_FALSE )
    {
        return SCT_FALSE;
    }

    /* Assing and return success */
    *xvalue = atoi( x );
    *yvalue = atoi( y );

    return SCT_TRUE;
}

/*!
 * Gets a string list from an attribute list.
 *
 * \param attributes Pointer to the attribute list. Must be given.
 * \param attributeName Name of the attribute. Must be given.
 * \param stringList Pointer to store pointer to newly created string list. Must be defined.
 *
 * \return SCT_TRUE if successfull, SCT_FALSE if the attribute was not found, or was invalid.
 *
 * \note Buffer is not changed in case of failure. 
 */
SCTBoolean sctGetAttributeAsStringList( SCTAttributeList* attributes, const char* attributeName, SCTStringList** stringList )
{
    SCTAttribute*   attribute;
    int             len;
    SCTStringList*  l;

    SCT_ASSERT_ALWAYS( attributes != NULL );
    SCT_ASSERT_ALWAYS( attributeName != NULL );
    SCT_ASSERT_ALWAYS( stringList );

    attribute = sctFindAttribute( attributes, attributeName );
    if( attribute == NULL )
    {
        return SCT_FALSE;
    }
    if( attribute->value == NULL || attribute->value[ 0 ] != '{' )
    {
        return SCT_FALSE;
    }
    len = strlen( attribute->value );
    if( len < 2 || attribute->value[ len -1 ] != '}' )
    {
        return SCT_FALSE;
    }

    l = sctSplitString( attribute->value + 1, len - 2, ',' );
    if( l == NULL )
    {
        return SCT_FALSE;
    }
    
    *stringList = l;

    return SCT_TRUE;
}

/*!
 *
 *
 */
typedef enum
{
    _SCTIIntType,
    _SCTIFloatType,
    _SCTIDoubleType,
    _SCTIHexType,
} _SCTIType;

/*!
 * Get attribute as array.
 *
 * \param attributes Attribute list. Must be defined.
 * \param attributeName Name of the attribute. Must be defined.
 * \param type Type of the array. 
 * \param values Pointer to type array. Must be defined.
 * \param count Number of elements in void array. Must be > 0.
 * 
 * \return SCT_TRUE if successfull, SCT_FALSE if the attribute was not
 * found, or it was not a valid array, or not enough elements were
 * found, or some general error happened.
 *
 * \note Values may be changed on failure.
 *
 */
static SCTBoolean sctiGetAttributeAsArray( SCTAttributeList* attributes, const char* attributeName, _SCTIType type, void* values, int count )
{
    SCTAttribute*   attribute;
    char            staticBuf[ SCT_STATIC_ARRAY_BUFFER_LEN ];
    char*           dynamicBuf  = NULL;
    char*           ptr;
    char*           a;
    char*           b;
    char*           x;
    int             i;
    int             len;
    SCTBoolean      status      = SCT_TRUE;

    SCT_ASSERT( attributes != NULL );
    SCT_ASSERT( attributeName != NULL );
    SCT_ASSERT( values != NULL );
    SCT_ASSERT( count > 0 );

    attribute = sctFindAttribute( attributes, attributeName );
    if( attribute == NULL || attribute->value == NULL )
    {
        return SCT_FALSE;
    }

    /* Get attribute value length. */
    len = strlen( attribute->value );

    /* We use small static buf or arbitrary large dynamic buffer,
     * depending on the attribute value length. */
    if( len < SCT_ARRAY_LENGTH( staticBuf ) )
    {
        ptr = staticBuf;
    }
    else
    {
        /* Create a temporary buffer used for parsing. */
        dynamicBuf = ( char* )( siCommonMemoryAlloc( NULL, len + 1 ) );
        if( dynamicBuf == NULL )
        {
            return SCT_FALSE;
        }
        ptr = dynamicBuf;
    }

    /* Copy attribute value to temp buffer. */
    sctStrncopy( ptr, attribute->value, len );    

    /* First, make sure the array starts and ends correctly. */ 
    if( ptr[ 0 ] == SCT_ARRAY_START_CHAR && ptr[ len - 1 ] == SCT_ARRAY_END_CHAR )
    {
        ptr[ len - 1 ] = '\0';

        /* Start parsing one float at a time. */
        b = &ptr[ 1 ];

        for( i = 0; i < count && status != SCT_FALSE; ++i )
        {
            a = b;
            if( a == NULL )
            {
                status = SCT_FALSE;
                break;
            }

            b = sctiTokenize( a, ',' );

            switch( type )
            {
            default:
            case _SCTIIntType:
                if( sctiValidateInt( a ) == SCT_FALSE )
                {
                    status = SCT_FALSE;
                }
                else
                {
                    ( ( int* )( values ) )[ i ] = atoi( a );
                }
                break;

            case _SCTIFloatType:
                if( sctiValidateFloat( a ) == SCT_FALSE )
                {
                    status = SCT_FALSE;
                }
                else
                {
                    ( ( float* )( values ) )[ i ] = ( float )( atof( a ) );
                }
                break;
            case _SCTIDoubleType:
                if( sctiValidateFloat( a ) == SCT_FALSE )
                {
                    status = SCT_FALSE;
                }
                else
                {
                    ( ( double* )( values ) )[ i ] = atof( a );
                }
                break;
            case _SCTIHexType:
                if( sctiValidateHex( a ) == SCT_FALSE )
                {
                    status = SCT_FALSE;
                }
                else
                {
                    ( ( unsigned int* )( values ) )[ i ] = ( unsigned int )( strtoul( a, &x, 16 ) );
                }
                break;
            }
        }
        if( b != NULL )
        {
            status = SCT_FALSE;
        }
    }
    else
    {
        status = SCT_FALSE;
    }

    /* Free allocated dynamic memory. */
    if( dynamicBuf != NULL )
    {
        siCommonMemoryFree( NULL, dynamicBuf );
    }
    return status;
}

/*!
 * Get attribute as float array.
 *
 * \param attributes Attribute list. Must be defined.
 * \param attributeName Name of the attribute. Must be defined.
 * \param values Pointer to float array. Must be defined.
 * \param count Number of elements in float array. Must be > 0.
 * 
 * \return SCT_TRUE if successfull, SCT_FALSE if the attribute was not
 * found, or it was not a valid float array, or not enough floats were
 * found, or some general error happened.
 *
 * \note Values may be changed on failure.
 *
 */
SCTBoolean sctGetAttributeAsFloatArray( SCTAttributeList* attributes, const char* attributeName, float* values, int count )
{
    return sctiGetAttributeAsArray( attributes, attributeName, _SCTIFloatType, values, count );
}

/*!
 * Get attribute as double array.
 *
 * \param attributes Attribute list. Must be defined.
 * \param attributeName Name of the attribute. Must be defined.
 * \param values Pointer to float array. Must be defined.
 * \param count Number of elements in float array. Must be > 0.
 * 
 * \return SCT_TRUE if successfull, SCT_FALSE if the attribute was not
 * found, or it was not a valid float array, or not enough floats were
 * found, or some general error happened.
 *
 * \note Values may be changed on failure.
 *
 */
SCTBoolean sctGetAttributeAsDoubleArray( SCTAttributeList* attributes, const char* attributeName, double* values, int count )
{
    return sctiGetAttributeAsArray( attributes, attributeName, _SCTIDoubleType, values, count );
}

/*!
 * Get attribute as int array.
 *
 * \param attributes Attribute list. Must be defined.
 * \param attributeName Name of the attribute. Must be defined.
 * \param values Pointer to int array. Must be defined.
 * \param count Number of elements in int array. Must be > 0.
 * 
 * \return SCT_TRUE if successfull, SCT_FALSE if the attribute was not
 * found, or it was not a valid int array, or not enough ints were
 * found, or some general error happened.
 *
 * \note Values may be changed on failure.
 *
 */
SCTBoolean sctGetAttributeAsIntArray( SCTAttributeList* attributes, const char* attributeName, int* values, int count )
{
    return sctiGetAttributeAsArray( attributes, attributeName, _SCTIIntType, values, count );
}

/*!
 * Get attribute as hex array.
 *
 * \param attributes Attribute list. Must be defined.
 * \param attributeName Name of the attribute. Must be defined.
 * \param values Pointer to int array. Must be defined.
 * \param count Number of elements in int array. Must be > 0.
 * 
 * \return SCT_TRUE if successfull, SCT_FALSE if the attribute was not
 * found, or it was not a valid int array, or not enough ints were
 * found, or some general error happened.
 *
 * \note Values may be changed on failure.
 *
 */
SCTBoolean sctGetAttributeAsHexArray( SCTAttributeList* attributes, const char* attributeName, unsigned int* values, int count )
{
    return sctiGetAttributeAsArray( attributes, attributeName, _SCTIHexType, values, count );
}

/*!
 * Get attribute as vector.
 *
 * \param attributes Attribute list. Must be defined.
 * \param attributeName Name of the attribute. Must be defined.
 * \param type Vector type.
 * \param vector Pointer to vector pointer. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
static SCTBoolean sctiGetAttributeAsVector( SCTAttributeList* attributes, const char* attributeName, _SCTIType type, void** vector )
{
    SCTAttribute*           attribute;
    SCTStringList*          values;
    SCTStringListIterator   iter;
    char                    staticBuf[ SCT_STATIC_ARRAY_BUFFER_LEN ];
    char*                   dynamicBuf      = NULL;
    char*                   ptr;
    char*                   str;
    char*                   x;
    int                     i;
    int                     len;
    int                     valueCount      = 0;
    SCTFloatVector*         floatVector     = NULL;
    SCTIntVector*           intVector       = NULL;
    SCTDoubleVector*        doubleVector    = NULL;
    SCTHexVector*           hexVector       = NULL;
    SCTBoolean              status          = SCT_TRUE;

    SCT_ASSERT( attributes != NULL );
    SCT_ASSERT( attributeName != NULL );
    SCT_ASSERT( vector != NULL );

    /* First, set the incoming pointer to null. */
    *vector = NULL;

    attribute = sctFindAttribute( attributes, attributeName );
    if( attribute == NULL || attribute->value == NULL )
    {
        return SCT_FALSE;
    }

    len = strlen( attribute->value );

    /* We use small static buf or arbitrary large dynamic buffer,
     * depending on the attribute value length. */
    if( len < SCT_ARRAY_LENGTH( staticBuf ) )
    {
        ptr = staticBuf;
    }
    else
    {
        /* Create a temporary buffer used for parsing. */
        dynamicBuf = ( char* )( siCommonMemoryAlloc( NULL, len + 1 ) );
        if( dynamicBuf == NULL )
        {
            return SCT_FALSE;
        }
        ptr = dynamicBuf;
    }

    /* Copy string to temp buffer */
    sctStrncopy( ptr, attribute->value, len );            

    /* Start parsing the list. */
    if( ptr[ 0 ] != SCT_ARRAY_START_CHAR || ptr[ len - 1 ] != SCT_ARRAY_END_CHAR )
    {
        status = SCT_FALSE;
    }
    else
    {
        ptr[ len - 1 ] = '\0';

        if( strlen( &ptr[ 1 ] ) == '\0' )
        {
            values = NULL;
        }
        else
        {
            values = sctSplitString( &ptr[ 1 ], strlen( &ptr[ 1 ] ), ',' );
            if( values == NULL )
            {
                status = SCT_FALSE;
            }
            else
            {
                valueCount = sctCountListElements( values->list );
            }
        }

        /* Create a new vector with enough room for the values. */
        switch( type )
        {
        default:
        case _SCTIIntType:
            intVector = sctCreateIntVector( valueCount );
            if( intVector == NULL )
            {
                status = SCT_FALSE;
            }
            else
            {
                *vector = intVector;
            }
            break;
                
        case _SCTIFloatType:
            floatVector = sctCreateFloatVector( valueCount );
            if( floatVector == NULL )
            {
                status = SCT_FALSE;
            }
            else
            {
                *vector = floatVector;
            }
            break;

        case _SCTIDoubleType:
            doubleVector = sctCreateDoubleVector( valueCount );
            if( doubleVector == NULL )
            {
                status = SCT_FALSE;
            }
            else
            {
                *vector = doubleVector;
            }
            break;

        case _SCTIHexType:
            hexVector = sctCreateHexVector( valueCount );
            if( hexVector == NULL )
            {
                status = SCT_FALSE;
            }
            else
            {
                *vector = hexVector;
            }
            break;
        }
        if( values != NULL )
        {
            sctGetStringListIterator( values, &iter );

            for( str  = sctGetNextStringListElement( &iter ), i = 0;
                 str != NULL && status != SCT_FALSE;
                 str  = sctGetNextStringListElement( &iter ), i++ )
            {
                SCT_ASSERT( i < valueCount );
                switch( type )
                {
                default:
                case _SCTIIntType:
                    if( sctiValidateInt( str ) == SCT_FALSE )
                    {
                        status = SCT_FALSE;
                    }
                    else
                    {
                        SCT_ASSERT (intVector != NULL );
                        intVector->data[ i ] = atoi( str );
                    }
                    break;
                    
                case _SCTIFloatType:
                    if( sctiValidateFloat( str ) == SCT_FALSE )
                    {
                        status = SCT_FALSE;
                    }
                    else
                    {
                        SCT_ASSERT( floatVector != NULL );
                        floatVector->data[ i ] = ( float )( atof( str ) );
                    }
                    break;

                case _SCTIDoubleType:
                    if( sctiValidateFloat( str ) == SCT_FALSE )
                    {
                        status = SCT_FALSE;
                    }
                    else
                    {
                        SCT_ASSERT( doubleVector != NULL );
                        doubleVector->data[ i ] = atof( str );
                    }
                    break;

                case _SCTIHexType:
                    if( sctiValidateHex( str ) == SCT_FALSE )
                    {
                        status = SCT_FALSE;
                    }
                    else
                    {
                        SCT_ASSERT( hexVector != NULL );
                        hexVector->data[ i ] = ( unsigned int )( strtoul( str, &x, 16 ) );
                    }
                    break;
                }
            }
            sctDestroyStringList( values );
        }
    }
    
    if( dynamicBuf != NULL )
    {
        siCommonMemoryFree( NULL, dynamicBuf );
    }

    if( status == SCT_FALSE )
    {
        *vector = NULL;
        if( intVector != NULL )
        {
            sctDestroyIntVector( intVector );
        }
        if( floatVector != NULL )
        {
            sctDestroyFloatVector( floatVector );
        }
        if( doubleVector != NULL )
        {
            sctDestroyDoubleVector( doubleVector );
        }
        if( hexVector != NULL )
        {
            sctDestroyHexVector( hexVector );
        }
    }

    return status;
}

/*!
 * Get attribute as int vector.
 *
 * \param attributes Attribute list. Must be defined.
 * \param attributeName Name of the attribute. Must be defined.
 * \param vector Pointer to int vector pointer. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctGetAttributeAsIntVector( SCTAttributeList* attributes, const char* attributeName, SCTIntVector** vector )
{
    return sctiGetAttributeAsVector( attributes, attributeName, _SCTIIntType, ( void** )( vector ) );
}

/*!
 * Get attribute as float vector.
 *
 * \param attributes Attribute list. Must be defined.
 * \param attributeName Name of the attribute. Must be defined.
 * \param vector Pointer to float vector pointer. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctGetAttributeAsFloatVector( SCTAttributeList* attributes, const char* attributeName, SCTFloatVector** vector )
{
    return sctiGetAttributeAsVector( attributes, attributeName, _SCTIFloatType, ( void** )( vector ) );
}

/*!
 * Get attribute as double vector.
 *
 * \param attributes Attribute list. Must be defined.
 * \param attributeName Name of the attribute. Must be defined.
 * \param vector Pointer to double vector pointer. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctGetAttributeAsDoubleVector( SCTAttributeList* attributes, const char* attributeName, SCTDoubleVector** vector )
{
    return sctiGetAttributeAsVector( attributes, attributeName, _SCTIDoubleType, ( void** )( vector ) );
}

/*!
 * Get attribute as hex vector.
 *
 * \param attributes Attribute list. Must be defined.
 * \param attributeName Name of the attribute. Must be defined.
 * \param vector Pointer to hex vector pointer. Must be defined.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctGetAttributeAsHexVector( SCTAttributeList* attributes, const char* attributeName, SCTHexVector** vector )
{
    return sctiGetAttributeAsVector( attributes, attributeName, _SCTIHexType, ( void** )( vector ) );
}

/*!
 * Gets conditional integer value from an attribute list. Supported conditions
 * are '<', '<=', '>', '>=', and '='. In addition, it is possible to use a
 * single '-' to mean dont care condition in which case value will be 0 in
 * output.
 *
 * \param attributes Pointer to the attribute list. Must be given.
 * \param attributeName Name of the attribute. Must be given.
 * \param cint Output argument for receiving the conditional integer value. Must be given.
 *
 * \return SCT_TRUE if successfull, SCT_FALSE if the attribute was not
 * found, or it was not in valid <condition,value> format, or was not -.
 *
 * \note Values are not changed on failure.
 */
SCTBoolean sctGetAttributeAsCInt( SCTAttributeList* attributes, const char* attributeName, SCTConditionalInt* cint )
{
    SCTAttribute*   attribute;
    char            buf[ 32 ];
    SCTCondition    c;
    char*           v           = NULL;
    int             l;

    SCT_ASSERT_ALWAYS( attributes != NULL );
    SCT_ASSERT_ALWAYS( attributeName != NULL );
    SCT_ASSERT_ALWAYS( cint != NULL );

    attribute = sctFindAttribute( attributes, attributeName );
    if( attribute == NULL )
    {
        return SCT_FALSE;
    }

    l = strlen( attribute->value );
    
    /* Make sure the attribute string to parse is not long. */
    if( l >= SCT_ARRAY_LENGTH( buf ) )
    {
        return SCT_FALSE;
    }

    /* Copy string to temp buffer */
    sctStrncopy( buf, attribute->value, SCT_ARRAY_LENGTH( buf ) );            

    /* Check '-' for SCT_DONTCARE. */
    if( l == 1 )
    {
        if( buf[ 0 ] == '-' )
        {
            cint->condition = SCT_DONTCARE;
            cint->value     = 0;
            
            return SCT_TRUE;
        }
        else
        {
            return SCT_FALSE;
        }
    }
    
    if( buf[ 0 ] == '<' && ( buf[ 1 ] == '-' || isdigit( buf[ 1 ] ) ) )
    {
        c = SCT_LESS;
        v = &buf[ 1 ];
    }
    else if( buf[ 0 ] == '<' && buf[ 1 ] == '=' && ( buf[ 2 ] == '-' || isdigit( buf[ 2 ] ) ) )
    {
        c = SCT_LEQUAL;
        v = &buf[ 2 ];
    }   
    else if( buf[ 0 ] == '=' && ( buf[ 1 ] == '-' || isdigit( buf[ 1 ] ) ) )
    {
        c = SCT_EQUAL;
        v = &buf[ 1 ];
    }
    else if( buf[ 0 ] == '>' && ( buf[ 1 ] == '-' || isdigit( buf[ 1 ] ) ) )
    {
        c = SCT_GREATER;
        v = &buf[ 1 ];
    }
    else if( buf[ 0 ] == '>' && buf[ 1 ] == '=' && ( buf[ 2 ] == '-' || isdigit( buf[ 2 ] ) ) )
    {
        c = SCT_GEQUAL;
        v = &buf[ 2 ];
    }
    else
    {
        return SCT_FALSE;
    }
        
    /* Validate value */
    if( v == NULL || sctiValidateInt( v ) == SCT_FALSE )
    {
        return SCT_FALSE;
    }

    /* Assing and return success */
    cint->condition = c;
    cint->value     = atoi( v );

    return SCT_TRUE;
}

/*!
 * Create int vector.
 *
 * \param length Number of elements in the vector. Must be >= 0.
 *
 * \return Newly created int vector, or NULL if failure. Vector data
 * is initially set to zero.
 *
 */
SCTIntVector* sctCreateIntVector( int length )
{
    SCTIntVector*   vector;

    SCT_ASSERT_ALWAYS( length >= 0 );
    
    vector = ( SCTIntVector* )( siCommonMemoryAlloc( NULL, sizeof( SCTIntVector ) ) );
    if( vector == NULL )
    {
        return NULL;
    }
    if( length > 0 )
    {
        vector->length = length;
        vector->data   = ( int * )( siCommonMemoryAlloc( NULL, length * sizeof( int ) ) );
        if( vector->data == NULL )
        {
            siCommonMemoryFree( NULL, vector );
            return NULL;
        }
        memset( vector->data, 0, length * sizeof( int ) );
    }
    else
    {
        vector->length = 0;
        vector->data   = NULL;
    }

    SCT_ASSERT( vector != NULL );
    SCT_ASSERT( ( vector->length == 0 && vector->data == NULL ) ||
                ( vector->length > 0 && vector->data != NULL ) );

    return vector;
}

/*!
 * Destroy int vector.
 *
 * \param vector Pointer to int vector. Must be defined.
 *
 */

void sctDestroyIntVector( SCTIntVector* vector )
{
    SCT_ASSERT_ALWAYS( vector != NULL );
    SCT_ASSERT( ( vector->length == 0 && vector->data == NULL ) ||
                ( vector->length > 0 && vector->data != NULL ) );

    if( vector->length > 0 )
    {
        siCommonMemoryFree( NULL, vector->data );
    }
    siCommonMemoryFree( NULL, vector );
}

/*!
 * Create float vector.
 *
 * \param length Nunber of elements in the vector. Must be >= 0.
 *
 * \return Newly created float vector, or NULL if failure. Vector data
 * is initially set to zero.
 *
 */
SCTFloatVector* sctCreateFloatVector( int length )
{
    SCTFloatVector* vector;

    SCT_ASSERT_ALWAYS( length >= 0 );
    
    vector = ( SCTFloatVector* )( siCommonMemoryAlloc( NULL, sizeof( SCTFloatVector ) ) );
    if( vector == NULL )
    {
        return NULL;
    }
    if( length > 0 )
    {
        vector->length = length;
        vector->data   = ( float* )( siCommonMemoryAlloc( NULL, length * sizeof( float ) ) );
        if( vector->data == NULL )
        {
            siCommonMemoryFree( NULL, vector );
            return NULL;
        }
        memset( vector->data, 0, length * sizeof( float ) );
    }
    else
    {
        vector->length = 0;
        vector->data   = NULL;
    }

    SCT_ASSERT( vector != NULL );
    SCT_ASSERT( ( vector->length == 0 && vector->data == NULL ) ||
                ( vector->length > 0 && vector->data != NULL ) );

    return vector;
}

/*!
 * Create double vector.
 *
 * \param length Nunber of elements in the vector. Must be >= 0.
 *
 * \return Newly created double vector, or NULL if failure. Vector data
 * is initially set to zero.
 *
 */
SCTDoubleVector* sctCreateDoubleVector( int length )
{
    SCTDoubleVector*    vector;

    SCT_ASSERT_ALWAYS( length >= 0 );
    
    vector = ( SCTDoubleVector* )( siCommonMemoryAlloc( NULL, sizeof( SCTDoubleVector ) ) );
    if( vector == NULL )
    {
        return NULL;
    }
    if( length > 0 )
    {
        vector->length = length;
        vector->data   = ( double* )( siCommonMemoryAlloc( NULL, length * sizeof( double ) ) );
        if( vector->data == NULL )
        {
            siCommonMemoryFree( NULL, vector );
            return NULL;
        }
        memset( vector->data, 0, length * sizeof( double ) );
    }
    else
    {
        vector->length = 0;
        vector->data   = NULL;
    }

    SCT_ASSERT( vector != NULL );
    SCT_ASSERT( ( vector->length == 0 && vector->data == NULL ) ||
                ( vector->length > 0 && vector->data != NULL ) );

    return vector;
}

/*!
 * Destroy float vector.
 *
 * \param vector Pointer to float vector. Must be defined.
 *
 */
void sctDestroyFloatVector( SCTFloatVector* vector )
{
    SCT_ASSERT_ALWAYS( vector != NULL );
    SCT_ASSERT( ( vector->length == 0 && vector->data == NULL ) ||
                ( vector->length > 0 && vector->data != NULL ) );

    if( vector->length > 0 )
    {
        siCommonMemoryFree( NULL, vector->data );
    }
    siCommonMemoryFree( NULL, vector );
}

/*!
 * Destroy double vector.
 *
 * \param vector Pointer to double vector. Must be defined.
 *
 */
void sctDestroyDoubleVector( SCTDoubleVector* vector )
{
    SCT_ASSERT_ALWAYS( vector != NULL );
    SCT_ASSERT( ( vector->length == 0 && vector->data == NULL ) ||
                ( vector->length > 0 && vector->data != NULL ) );

    if( vector->length > 0 )
    {
        siCommonMemoryFree( NULL, vector->data );
    }
    siCommonMemoryFree( NULL, vector );
}

/*!
 * Create hex vector.
 *
 * \param length Number of elements in the vector. Must be >= 0.
 *
 * \return Newly created hex vector, or NULL if failure. Vector data
 * is initially set to zero.
 *
 */
SCTHexVector* sctCreateHexVector( int length )
{
    SCTHexVector*   vector;

    SCT_ASSERT_ALWAYS( length >= 0 );
    
    vector = ( SCTHexVector* )( siCommonMemoryAlloc( NULL, sizeof( SCTHexVector ) ) );
    if( vector == NULL )
    {
        return NULL;
    }
    if( length > 0 )
    {
        vector->length = length;
        vector->data   = ( unsigned int* )( siCommonMemoryAlloc( NULL, length * sizeof( unsigned int ) ) );
        if( vector->data == NULL )
        {
            siCommonMemoryFree( NULL, vector );
            return NULL;
        }
        memset( vector->data, 0, length * sizeof( unsigned int ) );
    }
    else
    {
        vector->length = 0;
        vector->data   = NULL;
    }

    SCT_ASSERT( vector != NULL );
    SCT_ASSERT( ( vector->length == 0 && vector->data == NULL ) ||
                ( vector->length > 0 && vector->data != NULL ) );

    return vector;
}

/*!
 * Destroy hex vector.
 *
 * \param vector Pointer to hex vector. Must be defined.
 *
 */
void sctDestroyHexVector( SCTHexVector* vector )
{
    SCT_ASSERT_ALWAYS( vector != NULL );
    SCT_ASSERT( ( vector->length == 0 && vector->data == NULL ) ||
                ( vector->length > 0 && vector->data != NULL ) );

    if( vector->length > 0 )
    {
        siCommonMemoryFree( NULL, vector->data );
    }
    siCommonMemoryFree( NULL, vector );
}

/*!
 * Create log for reporting warnings and error.
 *
 * \return Pointer to SCTLog, or NULL if failure. Log is initially
 * empty.
 */
SCTLog* sctCreateLog()
{
    SCTLog* log;

    log = ( SCTLog* )( siCommonMemoryAlloc( NULL, sizeof( SCTLog ) ) );
    
    if( log == NULL )
    {
        return NULL;
    }

    log->mutex = siCommonCreateMutex( NULL );

    if( log->mutex == NULL )
    {
        sctDestroyLog( log );
        return NULL;       
    }        
    
    log->warnings = sctCreateStringList();
    if( log->warnings == NULL )
    {
        sctDestroyLog( log );
        return NULL;
    }

    log->error[ 0 ] = '\0';

    return log;
}

/*!
 * Add an error to the current system error log. Only SCT_MAX_ERROR_MSG_LEN
 * characters are copied from the message to the log. Note that only one error
 * message can be logged at a time; the first error is logged and subsequent
 * error messages are reported only using debug print. Use sctResetLog() to
 * clear an error.
 *
 * \param message Error message. Must be defined.
 */
void sctLogError( const char* message )
{
    SCTLog* log;

    SCT_ASSERT_ALWAYS( message != NULL );

    log = sctGetErrorLog();
    
    SCT_ASSERT_ALWAYS( log != NULL );
    SCT_ASSERT_ALWAYS( log->mutex != NULL );    

    siCommonLockMutex( NULL, log->mutex );
    
    if( strlen( log->error ) == 0 )
    {    
        sctStrncopy( log->error, message, SCT_MAX_ERROR_MSG_LEN );
    }
#if defined( SCT_DEBUG )
    /* Reporting error overflow was plausible when the spandex core
     * was single-threaded, but it now seems to trigger undesired warnings with
     * multithread benchmarks; main thread reports an error which aborts the
     * benchmark, but worker threads still report spurious errors when being
     * stopped. */
    else
    {
        siCommonDebugPrintf( NULL, "SPANDEX: error overflow \"%s\"", message );
    }
#endif  /* defined( SCT_DEBUG ) */
    
    siCommonUnlockMutex( NULL, log->mutex );    
}

/*!
 * Logs an error with file name and line number information. It should
 * be noted that the resulting error message can be only maximum of
 * SCT_MAX_ERROR_MSG_LEN characters long. The SCT_MAX_ERROR_MSG_LEN
 * characters are used so that maximum of 120 characters are used from
 * message and file. Trailing characters are discarded from message
 * and leading characters are discarded from file. 
 *
 * The message format is "file(line): message".  
 *
 * Note that only one error message can be logged at a time; the first error is
 * logged and subsequent error messages are reported only using debug print. Use
 * sctResetLog() to clear an error.
 *
 * \param log Pointer to log. Nothing is logged if NULL.
 * \param message Pointer to error message string. Must be defined.
 * \param file Pointer to file name string. Must be defined.
 * \param line Line number
 */
void sctLogErrorFileLine( const char* message, const char* file, unsigned int line )
{
    #define     SCT_LOG_MAX_LINE_LEN      15
    SCTLog*     log;
    const int   maxMessageLen           = 120;
    const int   maxFileLen              = 120;
    char        lineBuf[ SCT_LOG_MAX_LINE_LEN ];
    int         fileLen                 = 0;
    int         fileLead                = 0;

    SCT_ASSERT_ALWAYS( message != NULL );
    SCT_ASSERT_ALWAYS( file != NULL );
    SCT_ASSERT_ALWAYS( maxMessageLen + maxFileLen + SCT_LOG_MAX_LINE_LEN <= SCT_MAX_ERROR_MSG_LEN );

    log = sctGetErrorLog();
    
    SCT_ASSERT_ALWAYS( log != NULL );
    SCT_ASSERT_ALWAYS( log->mutex != NULL );    

    siCommonLockMutex( NULL, log->mutex );
    
    sprintf( lineBuf, "(%d): ", line );

    fileLen = strlen( file );
    fileLead = sctMax( 0, fileLen - maxFileLen );

    if( strlen( log->error ) == 0 )
    {
        sctStrncopy( log->error, ( file + fileLead ), maxFileLen );
        sctStrncat( log->error, lineBuf, SCT_LOG_MAX_LINE_LEN );
        sctStrncat( log->error, message, maxMessageLen );
    }
#if defined( SCT_DEBUG )
    /* Reporting error overflow was plausible when the spandex core
     * was single-threaded, but it now seems to trigger undesired warnings with
     * multithread benchmarks; main thread reports an error which aborts the
     * benchmark, but worker threads still report spurious errors when being
     * stopped. */
    else
    {
        siCommonDebugPrintf( NULL, "SPANDEX: error overflow %s %s %s", file, lineBuf, message );
    }
#endif  /* defined( SCT_DEBUG ) */
    
    siCommonUnlockMutex( NULL, log->mutex );
}

/*!
 * Add a warning to log.
 *
 * \param log Pointer to SCTLog. Must be defined.
 * \param message  Warning message. Must be defined.
 */
SCTBoolean sctLogWarning( const char* message )
{
    SCTLog*    log;
    SCTBoolean s;
    
    SCT_ASSERT_ALWAYS( message != NULL );

    log = sctGetErrorLog();
    
    SCT_ASSERT_ALWAYS( log != NULL );
    SCT_ASSERT_ALWAYS( log->mutex != NULL );    

    siCommonLockMutex( NULL, log->mutex );
    
    s = sctAddStringCopyToList( log->warnings, message );

    siCommonUnlockMutex( NULL, log->mutex );

    return s;
}

/*!
 * Return the number of warning in the log.
 *
 * \param log Pointer to SCTLog. Must be defined.
 *
 * \return Number of warnings in the log.
 */
int sctCountWarnings( SCTLog* log )
{
    int c;

    SCT_ASSERT_ALWAYS( log != NULL );
    SCT_ASSERT_ALWAYS( log->mutex != NULL );    

    siCommonLockMutex( NULL, log->mutex );
    
    c = sctCountListElements( log->warnings->list );

    siCommonUnlockMutex( NULL, log->mutex );

    return c;
}

/*!
 * Reset log. Removes all warnings and resets the error message.
 *
 * \param log Pointer to SCTLog. Must be defined.
 *
 */
void sctResetLog( SCTLog* log )
{   
    SCT_ASSERT_ALWAYS( log != NULL );
    SCT_ASSERT_ALWAYS( log->mutex != NULL );    

    siCommonLockMutex( NULL, log->mutex );
    
    log->error[ 0 ] = '\0';
    sctDestroyStringListElements( log->warnings );

    siCommonUnlockMutex( NULL, log->mutex );    
}

/*!
 * Destroy log by deallocating its resources. Note that log should be destroyed
 * only in situations where it cannot be accessed from multiple threads.
 *
 * \param log Pointer to SCTLog. Nothing is done if log is NULL.
 *
 */
void sctDestroyLog( SCTLog* log )
{
    if( log != NULL )
    {
        sctDestroyStringList( log->warnings );
        siCommonDestroyMutex( NULL, log->mutex );       
        siCommonMemoryFree( NULL, log );
    }
}

/*!
 * Get currently active error log.
 *
 * \return Pointer to currently active error log, or NULL if none.
 */
SCTLog* sctGetErrorLog( void )
{
    return ( SCTLog* )( sctGetRegisteredPointer( SCT_ERROR_LOG_POINTER, NULL ) );
}

/*!
 * Set currently active error log. 
 *
 * \param log Pointer to error log.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 */
SCTBoolean sctSetErrorLog( SCTLog* log )
{
    return sctRegisterPointer( SCT_ERROR_LOG_POINTER, log );
}

/*!
 * Get the number of timer ticks that any benchmark should last for the 
 * resulting number of ticks to be considered accurate enough.
 *
 * \return Minimun number of ticks.
 *
 */
unsigned long sctGetMinTicks()
{
    unsigned long   minTicks;
    unsigned long   timerFrequency;

    timerFrequency = siCommonGetTimerFrequency( NULL );

    /* Calculate the minimum amount of timer ticks to achieve the desired
     * relative accuracy. */
	minTicks = ( unsigned long )( ceil( 1.0 / SCTMaxRelativeTimingError ) );

    /* For added accuracy, we require ad-hoc that each measurement takes at
     * least one second. This is because on platforms with highly accurate
     * timers (such as Win32) most tests would only run couple iterations, which
     * can skew the results if there are large execution time variations. This
     * problem should probably be solved with some clever statistical analysis
     * of incoming measurement results or just by taking "enough" samples from
     * the benchmark running time. */
    if( timerFrequency > minTicks )
    {
        minTicks = timerFrequency;
    }

    SCT_ASSERT( minTicks > 0 );

    return minTicks;
}

/*!
 * Concatenate source string to destination. At most count number of
 * characters are concatenated to destination buffer. The destination
 * buffer will always be terminated by NULL character that is
 * automatically appended to the end. Therefore, the lenght of the 
 * destination buffer must be 
 * strlen(destination) + min(strlen(source),count) + 1
 *
 * \param destination Destination buffer
 * \param source Source buffer
 * \param count Maximum number of characters to concatenate
 *
 * \return SCT_TRUE if source was not truncated, SCT_FALSE otherwise
 */
SCTBoolean sctStrncat( char* destination, const char* source, int count )
{
    int i;

    SCT_ASSERT_ALWAYS( destination != NULL );
    SCT_ASSERT_ALWAYS( source != NULL );
    SCT_ASSERT_ALWAYS( count >= 0 );

    /* Locate trailing NULL character */
    for( ; *destination; ++destination ) {};

    /* Copy until count reached or source ends */
    for( i = 0; i < count && *source ; ++i, ++destination, ++source )
    {
        *destination = *source;
    }
    
    *destination = '\0';

    return ( i == count && *source ) ? SCT_FALSE : SCT_TRUE;
}

/*!
 * Copy min(strlen(source),n) characters from source string to destination 
 * string. The destination buffer will always be terminated by NULL character 
 * that is automatically appended to the end. Therefore, the lenght of the 
 * destination buffer must be min(strlen(source),count) + 1
 *
 * \param destination Destination buffer
 * \param source Source buffer
 * \param count Maximum number of characters to copy
 *
 * \return SCT_TRUE if source was not truncated, SCT_FALSE otherwise
 */
SCTBoolean sctStrncopy( char* destination, const char* source, int count )
{
    SCT_ASSERT_ALWAYS( destination != NULL );
    SCT_ASSERT_ALWAYS( source != NULL );
    SCT_ASSERT_ALWAYS( count >= 0 );

    destination[ 0 ] = '\0';
    if( count == 0 )
    {
        /* Nothing to do. */
        return SCT_TRUE;
    }
    return sctStrncat( destination, source, count );
}

/*!
 * Convert string into floating point value
 *
 * \param str String to convert
 *
 * \return Floating point value
 *
 */
float sctAtof( const char* str )
{
    double      accum     = 0.0f;
    double      mul       = 1;
    double      sign      = 1;
    const char* point;
    const char* prepoint;
    const char* pastpoint;

    SCT_ASSERT_ALWAYS( str != NULL );

    point = strchr( str, '.' );
    if( point == NULL )
    {
        prepoint  = str + strlen( str ) - 1;
        pastpoint = str + strlen( str );
    }
    else
    {
        prepoint  = point - 1;
        pastpoint = point + 1;
    }

    mul = 1.0;

    for( ; prepoint >= str; --prepoint )
    {
        if( *prepoint == '-' )
        {
            sign = -1.0;
        }
        else
        {
            if( !isdigit( *prepoint ) )
            {
                return 0.0f;
            }
            accum += ( *prepoint - '0' ) * mul;
            mul *= 10.0;
        }
    }

    mul = 0.1;

    for( ; *pastpoint; ++pastpoint )
    {
        if( !isdigit( *pastpoint ) )
        {
            return 0.0;
        }

        accum += ( *pastpoint - '0' ) * mul;
        mul /= 10.0;
    }

    return ( float )( sign * accum );
}

/*!
 * Maps string (ON/OFF) to int
 *
 * \param str String to parse. Must be defined
 * 
 * \return 1 if str == "ON", 0 if str == "OFF", -1 otherwise.
 *
 */
int sctMapStringToBoolean( const char* str )
{
    SCT_ASSERT( str != NULL );

    if( strcmp( str, "ON" ) == 0 )
    {
        return 1;
    }
    else if( strcmp( str, "OFF" ) == 0 )
    {
        return 0;
    }
    
    return -1;
}

/*!
 * Format size_t into a hex string; sprintf with %zu does not seem to work in
 * some environments. TODO: only size_t values of 32 and 64 bits supported.
 *
 * \param buf       Buffer into which format the hex string. Must be defined.  
 * \param length    Length of buf. Must be >= 9 with 32-bit size_t, >= 17 with
 *                  64-bit size_t.
 * \param v         Value to format into buf.  
 * 
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctFormatSize_t( char* buf, int length, size_t v )
{
    SCT_ASSERT_ALWAYS( buf != NULL );

    switch( sizeof( size_t ) )
    {
    case 4:
        if( length < 9 )
        {
            return SCT_FALSE;
        }
        
        sprintf( buf, "%08x", ( unsigned int )( v ) );
        return SCT_TRUE;

    case 8:
        if( length < 17 )
        {
            return SCT_FALSE;
        }
        
        sprintf( buf, "%08x%08x", ( unsigned int )( v >> 31 ), ( unsigned int )( v ) );
        return SCT_TRUE;

    default:        
        return SCT_FALSE;
    }
}

/* Pointer registry implementation. */

struct _SCTRegistryItem
{
    struct _SCTRegistryItem*    next;
    SCTPointerID                id;
    void*                       pointer;
};

typedef struct _SCTRegistryItem SCTRegistryItem;

static SCTRegistryItem* sctiGetRegistryRoot( void );
static void             sctiSetRegistryRoot( SCTRegistryItem* root );
static SCTRegistryItem* sctiFindRegistryItem( SCTRegistryItem* root, SCTPointerID id );

/*!
 * Register pointer to pointer registry.
 *
 * \param id Pointer id.
 * \param pointer Pointer to register.
 *
 * \return SCT_TRUE if success, SCT_FALSE if failure.
 *
 */
SCTBoolean sctRegisterPointer( SCTPointerID id, void* pointer )
{
    SCTRegistryItem*    root    = sctiGetRegistryRoot();
    SCTRegistryItem*    item    = NULL;

    if( root != NULL )
    {
        /* Check if the pointer is already registered. */
        item = sctiFindRegistryItem( root, id );
        
        if( item != NULL )
        {
            /* Modify the existing item. */
            item->pointer = pointer;            
            return SCT_TRUE;
        }
    }
    
    /* Create a new item. */
    item = ( SCTRegistryItem* )( siCommonMemoryAlloc( NULL, sizeof( SCTRegistryItem ) ) );
    
    if( item == NULL )
    {
        return SCT_FALSE;
    }
    
    item->id      = id;
    item->pointer = pointer;
    
    if( root != NULL )
    {
        item->next = root;
    }
    else
    {
        item->next = NULL;
    }
    
    sctiSetRegistryRoot( item );

    return SCT_TRUE;
}

/*!
 * Unregister registry pointer.
 *
 * \param id Pointer id.
 *
 * \note Nothing is done if pointer is not registered.
 */
void sctUnregisterPointer( SCTPointerID id )
{
    SCTRegistryItem*    item    = sctiGetRegistryRoot();
    SCTRegistryItem*    prev    = NULL;

    for( ; item != NULL; prev = item, item = item->next )
    {
        if( item->id == id )
        {
            /* Unlink the item. */
            if( prev != NULL )
            {
                prev->next = item->next;
            }
            else
            {
                /* First item, set the next as the new root. */
                sctiSetRegistryRoot( item->next );
            }
            
            siCommonMemoryFree( NULL, item );
			break;
        }
    }
}

/*!
 * Get registered pointer.
 *
 * \param id Pointer id.
 * \param pointerRegistered Pointer to SCTBoolean which tells if the pointer was registered or not. Ignored if NULL.
 *
 * \return Registered pointer (NULL if NULL pointer was registered), or NULL if failure.
 */
void* sctGetRegisteredPointer( SCTPointerID id, SCTBoolean* pointerRegistered )
{
    SCTRegistryItem*    root    = sctiGetRegistryRoot();
    SCTRegistryItem*    item;

    if( root != NULL )
    {
        item = sctiFindRegistryItem( root, id );
        
        if( item != NULL )
        {
            if( pointerRegistered != NULL )
            {
                *pointerRegistered = SCT_TRUE;
            }
            return item->pointer;
        }
    }

    if( pointerRegistered != NULL )
    {
        *pointerRegistered = SCT_FALSE;
    }
    
    return NULL;
}

/*!
 * Clean up registry. Releases all memory.
 *
 */
void sctCleanupRegistry( void )
{
    SCTRegistryItem*    item    = sctiGetRegistryRoot();
    SCTRegistryItem*    prev    = NULL;

    if( item == NULL )
    {
        return;
    }
    
    for( ; item != NULL; prev = item, item = item->next )
    {
        if( prev == NULL )
        {
            continue;
        }
        
        siCommonMemoryFree( NULL, prev );
    }
    
    siCommonMemoryFree( NULL, prev );
    sctiSetRegistryRoot( NULL );
}

/* ********************************************************************** */
/* Internal helper functions */
/* ********************************************************************** */

/*!
 *  Validate integer string.
 *
 * \param str Integer string
 *
 * \return SCT_TRUE if string is valid, SCT_FALSE otherwise.
 *
 */
static SCTBoolean sctiValidateInt( const char* str )
{
    int     len = strlen( str );
    int     i;

    SCT_ASSERT( str != NULL );

    if( len == 0 ) 
    {
        return SCT_FALSE;
    }

    for( i = len - 1; i >= 0; --i )
    {
        /* Allow '-' char in the beginning of the string, but make 
           sure there is more than just '-' in the string */
        if( str[ i ] == '-' && i == 0 )
        {
            if( len == 1 ) 
            {
                return SCT_FALSE;
            }
            continue;
        }
        else if( isdigit( str[ i ] ) == 0 )
        {
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/*!
 *  Validate float string.
 *
 * \param str Float string
 *
 * \return SCT_TRUE if string is valid, SCT_FALSE otherwise.
 *
 */
static SCTBoolean sctiValidateFloat( const char* str )
{
    int     len     = strlen( str );
    int     i;
    int     points  = 0;

    SCT_ASSERT( str != NULL );

    if( len == 0 ) 
    {
        return SCT_FALSE;
    }

    for( i = len - 1; i >= 0; --i )
    {
        /* Allow '-' char in the beginning of the string, but make 
           sure there is more than just '-' in the string */
        if( str[ i ] == '-' && i == 0 )
        {
            if( len == 1 ) 
            {
                return SCT_FALSE;
            }
            continue;
        }
        else if( str[ i ] == '.' ) /* Allow one point */
        {
            if( points++ )
            {
                return SCT_FALSE;
            }
            continue;
        }
        else if( isdigit( str[ i ] ) == 0 )
        {
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/*!
 *  Validate hex string.
 *
 * \param str hex string
 *
 * \return SCT_TRUE if string is valid, SCT_FALSE otherwise.
 *
 */
static SCTBoolean sctiValidateHex( const char* str )
{
    const char* p;
    int         low;
    int         len;

    SCT_ASSERT( str != NULL );

    len = strlen( str );
    if( len < 3 ) 
    {
        return SCT_FALSE;
    }

    if( str[ 0 ] != '0' || str[ 1 ] != 'x' )
    {
        return SCT_FALSE;
    }

    for( p = str + 2; *p; p++ )
    {
        low = tolower( *p );
        if( ( low < '0' || low > '9' ) && ( low < 'a' || low > 'f' ) )
        {
            return SCT_FALSE;
        }
    }

    return SCT_TRUE;
}

/*!
 * Tokenizes a string. String buf is scanned and first occurence of c is replaced
 * with NULL character. The function returns a pointer to next character after
 * replaced c character.
 *
 * \param buf String to scan
 * \param c Separator character
 *
 * \return Pointer to next token, or NULL if no tokens found
 *
 */
static char* sctiTokenize( char* buf, char c )
{
    SCT_ASSERT( buf != NULL );

    for( ; *buf != '\0'; ++buf )
    {
        if( *buf == c )
        {
            *buf = '\0';
            return ++buf;
        }
    }

    return NULL;
}

/*!
 * Get registry root.
 * 
 * \return Pointer to registry root.
 *
 */
SCTRegistryItem* sctiGetRegistryRoot( void )
{
    return ( SCTRegistryItem* )( siCommonGetSingleton( NULL ) );
}

/*!
 * Find registry item.
 *
 * \param root Registry root.
 * \param id Pointer id.
 *
 * \return Pointer to registry item, or NULL if failure.
 */
SCTRegistryItem* sctiFindRegistryItem( SCTRegistryItem* root, SCTPointerID id )
{
    SCTRegistryItem*    item;

    SCT_ASSERT( root != NULL );
    
    for( item = root; item != NULL; item = item->next )
    {
        if( item->id == id )
        {
            return item;
        }
    }
    return NULL;
}

/*!
 * Set registry root.
 *
 * \param root Pointer to new registry root.
 */
void sctiSetRegistryRoot( SCTRegistryItem* root )
{
    siCommonSetSingleton( NULL, root );
}

/*!
 * Calculates the number of significant digits in an unsigned integer value.
 *
 * /param   value   unsigned integer number
 *
 * /return  number of significant digits in value, zero if value is zero.
 */
int sctSignificantDigitsUINT( unsigned int value )
{
    int digits  = 0;
    while( value != 0 )
    {
        value /= 10;
        digits++;
    }
    return digits;
}

/*!
 * Joins two strings with an optional separator. The result is stored to the
 * first string. Assumes that the first string has enought space to hold the
 * resulting string. If the sepator pointer is NULL or the first string is empty
 * the separator will not be used.
 *
 * \param   str1    a pointer to zero-terminated string
 * \param   str2    a pointer to zero-terminated string
 * \param   sep     a pointer to zero-terminated string or NULL
 */
void sctJoinStrings( char* str1, const char* str2, const char* sep )
{
    SCT_ASSERT( str1 != NULL );
    SCT_ASSERT( str2 != NULL );

    if( sep != NULL && strlen( str1 ) > 0 )
    {
        strcat( str1, sep );
    }
    strcat( str1, str2 );
}


/* ********************************************************************** */
/* Mersene twister */

#define MT_LEN              624
#define MT_IA               397
#define MT_IB               ( MT_LEN - MT_IA )
#define UPPER_MASK          0x80000000
#define LOWER_MASK          0x7FFFFFFF
#define MATRIX_A            0x9908B0DF
#define TWIST( b, i, j )    ( ( b )[ i ] & UPPER_MASK) | ( ( b )[ j ] & LOWER_MASK )
#define MAGIC( s )          ( ( ( s ) & 1 ) * MATRIX_A )

/*!
 *
 *
 */
const unsigned long init_mt_buffer[ MT_LEN ] = { 386595812, 1351160345, 856857864, 1683555796, 1449445869, 1552848421, 322192537, 907821991, 711449157, 906396105, 1701295160, 1231824259, 1614530694, 1178305573, 1854166517, 597373680, 392423347, 418759135, 280917359, 1727851792, 1142006433, 29291485, 503699419, 1529476625, 571797927, 222575857, 462511747, 319815864, 465598156, 999177112, 158484560, 562317229, 1462011688, 278515980, 1606400740, 737971269, 298293203, 1589936101, 805347807, 1781070624, 1435751942, 1506533388, 2130450799, 1324833802, 1756678366, 1452309404, 987777359, 1073699277, 108730164, 673222163, 1214166340, 1973950408, 2129447182, 895317243, 1572362567, 1777665526, 2142286298, 489329730, 1595459537, 578726504, 957952755, 1748941105, 2002102757, 288215144, 1279525981, 2027186745, 776947741, 792661050, 1698592786, 758095976, 940104770, 2136753403, 1040794238, 782581252, 1474666750, 1508829195, 1983298661, 1997511984, 1625068394, 1626185901, 958795597, 1342586501, 1461872083, 1867902538, 956598667, 2041918159, 1188683316, 1290365671, 227568005, 1867915623, 1746928353, 1733434385, 738311429, 456580832, 1979492113, 1764166180, 1267869859, 542018118, 1766904935, 1819789012, 2065348806, 53545799, 1755316, 1049977605, 1565632701, 208014729, 1414001959, 1313660915, 1588246764, 877592151, 1384982577, 955047842, 1185635060, 1033172506, 1637708494, 1388486268, 1974158689, 201684931, 1232727787, 1708008335, 1505673211, 716552705, 1814850705, 1503367151, 1679622602, 99826085, 2124984438, 1207054307, 1841486360, 1099376263, 955969007, 449379240, 1729798643, 1724982722, 1797517899, 1324798280, 271672219, 559989089, 1503468888, 695502689, 2131133612, 447993805, 1808674019, 1462165716, 1881935523, 882157853, 561943610, 280831468, 985098936, 762232402, 1620178854, 1315710776, 1880801981, 508060602, 1840540592, 1340312577, 1817176904, 1700766860, 552379065, 751538309, 833893543, 664962859, 423081685, 315994594, 407218992, 1365010517, 102173598, 1412355475, 966415430, 1028193335, 183617900, 892685073, 312997815, 1138636775, 263781169, 435932873, 1593894056, 1484114163, 838911856, 452302957, 1718630849, 522791966, 443995505, 245123795, 1407525261, 1273091260, 1762824117, 1320979317, 166773174, 281502855, 2084492574, 898400901, 511745492, 1381037177, 1905074602, 1441586283, 1418637396, 877589326, 1197045541, 840243706, 218531285, 871088786, 540877800, 966621116, 972635234, 629467111, 522132317, 2821543, 1785735282, 1231901392, 823406363, 1176601034, 436357352, 1029218310, 1038760456, 1226240253, 605231479, 1686091557, 1202189954, 1977492235, 1492724582, 1511798593, 392224554, 1676182216, 1925146509, 1054181122, 67654898, 605772052, 1353688361, 1700558434, 1247307414, 707187298, 1927692240, 342044615, 476818232, 935209985, 381775542, 2109670487, 907720019, 1640693630, 173535454, 1454621041, 88463926, 1229447776, 550455293, 2074858301, 1833658034, 1122960645, 1358073571, 1899310978, 866482461, 2051678331, 1031687092, 1072813303, 143430972, 1855848026, 1450148338, 1096981209, 370390430, 1549159091, 1517438995, 1159850822, 1772425539, 2116742407, 428672960, 1177234933, 1265930734, 440534348, 4528862, 1501087836, 545519821, 688468877, 1948789127, 863789516, 908980744, 516310731, 279741030, 2142506334, 589741604, 1348583665, 1500173985, 1752311359, 2092113780, 800924892, 242385140, 1112237388, 1393933359, 1643398972, 1288158251, 918644168, 1138694283, 212815443, 1331254213, 483304258, 62357880, 436719100, 1154027969, 1379709173, 2006001154, 192415673, 1293556372, 848366604, 1457192048, 292323461, 390589203, 687508172, 1122750245, 1717598859, 316907597, 1839393667, 1025748313, 946126470, 1977986552, 1412158498, 862804757, 376191499, 1232355680, 1545103134, 821031927, 1906324517, 1256723505, 318386310, 1556192002, 591661907, 1029550058, 936519623, 3635418, 2106856887, 1309712392, 857011285, 711835403, 1127497982, 206721423, 64812486, 385192933, 1125288404, 1170317953, 747255811, 1073182055, 577525160, 1502160586, 886815323, 68184949, 119264076, 1722540466, 1915490748, 2003798258, 886104846, 1798149896, 872319841, 810047806, 1558768456, 1613206939, 1853010671, 1958519345, 458923660, 255008981, 660368905, 1433070531, 904655509, 116445561, 2068724810, 350991232, 564635212, 1130416369, 1913609861, 1300274430, 1310863335, 1305577334, 231835489, 1129314035, 1156261209, 947664662, 1116070721, 965415072, 236992407, 1262560302, 1552738790, 694368793, 939861885, 864533181, 130941679, 522345986, 614881311, 1148864367, 800592693, 84378032, 1289511038, 155711023, 1715330446, 1462342561, 747928928, 1328111407, 1999608234, 1573639417, 1326492985, 1641851318, 1229023544, 119817166, 426535794, 548528524, 493918700, 1137589810, 2082615212, 757271734, 2030955600, 763855847, 2017622151, 2013958761, 1294331695, 1212924791, 1743854053, 683036161, 649969504, 816273716, 292736041, 1764403441, 1290405917, 55265531, 790660717, 544422052, 1014278603, 41228372, 1769382099, 799066382, 576270558, 492368742, 1525030821, 403019296, 261737752, 31112652, 1173689268, 1522572449, 966088562, 237223633, 151245352, 1404549802, 608340858, 934395926, 1563473173, 793470992, 1936774231, 923112594, 1867209009, 400405490, 2135762659, 429411670, 657017527, 1340226925, 1551670830, 1156986861, 1790451342, 1459934867, 1249181366, 1117081743, 461048869, 723284303, 1601340378, 837412692, 2011015839, 413983271, 1313886973, 952695789, 1474105281, 416918565, 445158723, 1338805692, 612023159, 1555605402, 1518029984, 711716762, 673883670, 1917018764, 422234052, 2091963721, 1244531982, 500697404, 1449704207, 319907354, 562692016, 1941743366, 1237721069, 1372478021, 11331368, 1353404346, 217677918, 1448614190, 1381019786, 896018191, 1417690899, 1590862628, 1961863243, 182921959, 1930102196, 1722340292, 602370756, 2122431844, 101326820, 1979473953, 325632008, 147867909, 1408649891, 1860689754, 1806496063, 1325028112, 1991774712, 1216268949, 1623317843, 1851538182, 774845118, 1150201083, 1956536662, 2011222152, 2123276558, 1990904350, 409239741, 828530661, 1927912831, 97797021, 1252477371, 1633198043, 591858037, 1293667154, 1983489228, 195677451, 1280480599, 2090082206, 261292869, 675068120, 1024534472, 1011741035, 65718713, 88579438, 979008384, 1939556508, 1656571694, 520380329, 487552774, 387547711, 1715496036, 462077150, 863274719, 132216033, 171343114, 383493680, 798403759, 1996167537, 806976303, 526264475, 1201878297, 1120869170, 82276732, 1439981633, 1776342873, 186144169, 862002722, 456636992, 614165921, 1824544166, 545369460, 2100007544, 71733506, 1153191999, 691233402, 920053134, 1259081568, 1879994845, 1732299888, 457864009, 1068686091, 1302152614, 122657946, 195255720, 971010638, 1267072189, 345566999, 1974984063, 919246472, 696541841, 1989918184, 1671846350, 1018498823, 396624265, 355581474, 2043581275, 208289914, 229683082, 601358103, 771829150, 1612775236, 1786385914, 648503399, 1791813891, 340698625, 1882034812, 1175280118, 342367355, 1606443700, 985966258, 193351044, 2023906518, 1845143966, 1943753594, 1165478057, 375896973, 1598416040, 159959334, 1295975798, 93211652, 43511591, 444301225, 2143931131, 685037958, 1188538260, 1917690341, 841903130, 115298657, 1030576364, 2080041762, 69489956, 1415167769, 1116781632, 1651519440, 2071582313 };

/*!
 *
 *
 */
void* sctCreateMerseneTwisterContext()
{
    unsigned long* b;

    b = ( unsigned long* )( siCommonMemoryAlloc( NULL, MT_LEN * sizeof( unsigned long ) ) );
    if( b == NULL )
    {
        return NULL;
    }
    memcpy(  b, init_mt_buffer, MT_LEN * sizeof( unsigned long ) );
    
    return b;
}  

/*!
 *
 *
 */
void sctDestroyMerseneTwisterContext( void* context )
{
    if( context != NULL )
    {
        siCommonMemoryFree( NULL, context );
    }
}

/*!
 *
 *
 */
double sctMerseneTwister( int* mtIndex, void* context )
{
    unsigned long*  mt_buffer;
    unsigned long*  b;
    int             idx;
    unsigned long   s;
    int             i;

    SCT_ASSERT_ALWAYS( mtIndex != NULL );
    SCT_ASSERT_ALWAYS( context != NULL );
    
    mt_buffer   = ( unsigned long* )( context );
    b           = mt_buffer;
    idx         = *mtIndex;
    
    if( idx == MT_LEN * sizeof( unsigned long ) )
    {
        idx = 0;
        i   = 0;
        for( ; i < MT_IB; i++ )
        {
            s = TWIST( b, i, i + 1 );
            b[ i ] = b[ i + MT_IA ] ^ ( s >> 1 ) ^ MAGIC( s );
        }
        
        for( ; i < MT_LEN - 1; i++ )
        {
            s = TWIST( b, i, i+1 );
            b[ i ] = b[ i - MT_IB ] ^ ( s >> 1 ) ^ MAGIC( s );
        }
        
        s = TWIST( b, MT_LEN-1, 0 );
        b[ MT_LEN - 1 ] = b[ MT_IA - 1 ] ^ ( s >> 1 ) ^ MAGIC( s );
    }
    
    *mtIndex = idx + sizeof( unsigned long );
    return *( unsigned long* )( ( unsigned char* )b + idx ) / ( double )( 0xFFFFFFFF );
}

