/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_TOKENIZER_H__ )
#define __SCT_TOKENIZER_H__

#include "sct_types.h"

/*!
 * Token types returned by sctGetToken
 *
 * TOK_SECTION_NAME, TOK_SECTION_TYPE, TOK_ITEM, TOK_VALUE are different token types 
 * TOK_ERROR is returned when error was occured
 * TOK_END is returned when there is no tokens left in the input any more
 *
 */
typedef enum
{
    TOK_DELIMITER,
    TOK_STRING,
	TOK_END,
    TOK_ERROR /* KEEP THIS LAST! */
} SCTTokenType;

typedef struct _SCTTokenizerContext SCTTokenizerContext;

static const char delimiters[]  = { SCT_SECTION_HEADER_START,
                                    SCT_SECTION_HEADER_END,
                                    SCT_NAME_VALUE_SEPARATOR,
                                    SCT_TYPE_MODULE_SEPARATOR,
                                    SCT_ACTION_LIST_SEPARATOR };
static const char quoteChar     = SCT_QUOTATION_MARK;
static const char commentChar   = SCT_COMMENT_CHAR;
static const char escapeChar    = SCT_ESCAPE_CHAR;

#if defined( __cplusplus )
extern "C" {
#endif /* defined( __cplusplus ) */

    SCTTokenizerContext*    sctTokenizerInit( void );
    void                    sctTokenizerTerminate( SCTTokenizerContext* context );
    SCTTokenType            sctGetToken( SCTTokenizerContext* context, const char** token );
    void                    sctPutBackToken( SCTTokenizerContext* context );
    void                    sctLogInputError( SCTTokenizerContext* context, const char* error );
    void                    sctTokernizerCheckRelease( SCTTokenizerContext* context );
    
#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif /* !defined( __SCT_TOKENIZER_H__ ) */
