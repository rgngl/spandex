/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_THREADRUNNER_H__ )
#define __SCT_THREADRUNNER_H__

#include "sct.h"
#include "sct_types.h"

/*!
 *
 */
typedef struct
{
    void*                           signal;                             /*! Common signal for threads to signal main thread. */
    int                             threadCount;                        /*! Number of worker threads. */
    void*                           threads[ SCT_MAX_WORKER_THREADS ];  /*! Worker threads. */
    SCTBenchmarkWorkerThreadContext contexts[ SCT_MAX_WORKER_THREADS ]; /*! Worker thread contexts. */
} SCTBenchmarkWorkerThreads;

#if defined( __cplusplus )
extern "C" {
#endif /* defined( __cplusplus ) */

    void                            sctTriggerBenchmarkWorkerThreads( SCTBenchmarkWorkerThreads* threads, SCTWorkerThreadCommand command );
    SCTBoolean                      sctSyncBenchmarkWorkerThreads( SCTBenchmarkWorkerThreads* threads, SCTWorkerThreadState expectedState );
    SCTBoolean                      sctResetThreads( void );
    void                            sctCancelWaits( void );
    SCTBoolean                      sctHasWorkerThreads( SCTBenchmark* benchmark );
    SCTBoolean                      sctHasWorkerThread( SCTBenchmark* benchmark, int threadIndex );
    SCTBenchmarkWorkerThreads*      sctCreateBenchmarkWorkerThreads( SCTBenchmark* benchmark );
    void                            sctJoinBenchmarkWorkerThreads( SCTBenchmarkWorkerThreads* threads );

#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif /* !defined( __SCT_THREADRUNNER_H__ ) */
