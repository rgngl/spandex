/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_result.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

/*!
 *
 *
 */
SCTBenchmarkResult* sctCreateBenchmarkResult()
{
    SCTBenchmarkResult* result;

    result = ( SCTBenchmarkResult* )( siCommonMemoryAlloc( NULL, sizeof( SCTBenchmarkResult ) ) );
    if( result == NULL )
    {
        return NULL;
    }
    result->time = 0.0;

    return result;
}

/*!
 *
 *
 */
void sctDestroyBenchmarkResult( SCTBenchmarkResult* result )
{
    if( result == NULL )
    {
        return;
    }
    
    siCommonMemoryFree( NULL, result );
}

/* ************************************************************ */

/*!
 *
 *
 */
SCTBenchmarkResultList* sctCreateBenchmarkResultList()
{
    SCTList*                list;
    SCTBenchmarkResultList* benchmarkResultList;

    list = sctCreateList();
    if( list == NULL )
    {
        return NULL;
    }
    
    benchmarkResultList = ( SCTBenchmarkResultList* )( siCommonMemoryAlloc( NULL, sizeof( SCTBenchmarkResultList ) ) );
    if( benchmarkResultList == NULL )
    {
        sctDestroyList( list );
        return NULL;
    }

    benchmarkResultList->list = list;

    return benchmarkResultList;
}

/*!
 *
 *
 */
void sctResetBenchmarkResultList( SCTBenchmarkResultList* list )
{
    SCTBenchmarkResultListIterator  iterator;
    SCTBenchmarkResult*             benchmarkResult;

    if( list == NULL || list->list == NULL )
    {
        return;
    }

    sctGetBenchmarkResultListIterator( list, &iterator );
    while( ( benchmarkResult = sctGetNextBenchmarkResultListElement( &iterator ) ) != NULL )
    {
        sctDestroyBenchmarkResult( benchmarkResult );
    }

    sctDestroyListElements( list->list );    
}

/*!
 *
 *
 */
SCTBoolean sctAddBenchmarkResultToList( SCTBenchmarkResultList* list, SCTBenchmarkResult* benchmarkResult )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( benchmarkResult != NULL );

    return sctAddToList( list->list, benchmarkResult );
}

/*!
 *
 *
 */
void sctGetBenchmarkResultListIterator( SCTBenchmarkResultList* list, SCTBenchmarkResultListIterator* iterator )
{
    SCT_ASSERT_ALWAYS( list != NULL );
    SCT_ASSERT_ALWAYS( iterator != NULL );

    sctGetListIterator( list->list, &( iterator->listIterator ) );
}

/*!
 *
 *
 */
SCTBenchmarkResult* sctGetNextBenchmarkResultListElement( SCTBenchmarkResultListIterator* iterator )
{
    SCT_ASSERT_ALWAYS( iterator != NULL );

    return ( SCTBenchmarkResult* )( sctGetNextListElement( &( iterator->listIterator ) ) );
}

/*!
 *
 *
 */
void sctDestroyBenchmarkResultList( SCTBenchmarkResultList* list )
{
    SCTBenchmarkResultListIterator  iterator;
    SCTBenchmarkResult*             benchmarkResult;

    if( list == NULL )
    {
        return;
    }

    sctGetBenchmarkResultListIterator( list, &iterator );
    while( ( benchmarkResult = sctGetNextBenchmarkResultListElement( &iterator ) ) != NULL )
    {
        sctDestroyBenchmarkResult( benchmarkResult );
    }

    sctDestroyList( list->list );
    siCommonMemoryFree( NULL, list );
}
