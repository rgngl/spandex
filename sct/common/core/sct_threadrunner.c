/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h>
#include <math.h>

#include "sct_threadrunner.h"
#include "sct_runner.h"
#include "sct_sicommon.h"
#include "sct_benchmark.h"
#include "sct_action.h"
#include "sct_utils.h"

#if defined( INCLUDE_THREAD_MODULE )
#include "sct_threadmodule.h"
# endif /* defined( INCLUDE_THREAD_MODULE ) */

/*!
 *
 *
 */
static SCTBoolean               sctiIsWorkerThreadCancelled( SCTBenchmarkWorkerThreadContext* context );
static void                     sctiChangeWorkerThreadState( SCTBenchmarkWorkerThreadContext* context, SCTWorkerThreadState state );
static SCTWorkerThreadCommand   sctiGetWorkerThreadCommand( SCTBenchmarkWorkerThreadContext* context );

/*!
 * Benchmark worker thread function.
 *
 * \param context   Benchmark thread context. Must be defined.
 */
int sctiBenchmarkWorkerThreadFunc( SCTBenchmarkWorkerThreadContext* context )
{
    SCTBenchmark*               benchmark;
    int                         index;
    void*                       siCommon;
    int                         r;
    int                         frame;
    SCTWorkerThreadState        currentState;
    SCTWorkerThreadCommand      command;
    SCTBoolean                  status;
    
    SCT_ASSERT( context != NULL );

    benchmark           = context->benchmark;    
    index               = context->index;
    frame               = 0;
    siCommon            = siCommonGetContext();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc entering \"%s\" thread %d event loop", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */

    sctiChangeWorkerThreadState( context, SCT_WORKER_THREAD_STATE_STARTED );    
    currentState = SCT_WORKER_THREAD_STATE_STARTED;
    
    /* Benchmark thread event loop. */
    for( ;; )
    {
        /* Exit from the benchmark worker thread function if terminated. */
        if( currentState == SCT_WORKER_THREAD_STATE_TERMINATED )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d terminated", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */

            /* Change worker thread state to terminated. */
            sctiChangeWorkerThreadState( context, SCT_WORKER_THREAD_STATE_TERMINATED );

            /* Do per-thread system cleanup. */
            siCommonThreadCleanup( siCommon );
            
            return 0;
        }

        /* Set thread state in the thread context and trigger main thread; but
         * only if the current state is different than the thread state.*/
        sctiChangeWorkerThreadState( context, currentState );

#if defined( SCT_DEBUG )
        siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d waiting for signal", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */
        
        /* Wait for thread signal. */
        r = siCommonWaitForSignal( siCommon, context->signal, SCT_THREAD_SIGNAL_TIMEOUT_MILLIS );

        if( r < 1  )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d signal wait failed", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */
            
            /* Thread signal wait failed. */
            currentState = SCT_WORKER_THREAD_STATE_INERROR;
            continue;
        }
        
        /* Get worker thread command once we get worker thread signal. */
        command = sctiGetWorkerThreadCommand( context );  
        
        /* Handle worker thread command. */
        switch( command )
        {            
        case SCT_WORKER_THREAD_COMMAND_INITIALIZE: /* ************************************************** */
            
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d got signal INITIALIZE", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */           

            if( currentState != SCT_WORKER_THREAD_STATE_STARTED )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d invalid state for INITIALIZE signal", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */           

                SCT_ASSERT( 0 );
                currentState = SCT_WORKER_THREAD_STATE_INERROR;
                break;
            }
                        
            if( sctInitializeActions( benchmark, index ) == SCT_FALSE )
            {                
                if( sctiIsWorkerThreadCancelled( context ) == SCT_FALSE )
                {
#if defined( SCT_DEBUG )
                    siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d initialization failed", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */           
                    
                    currentState = SCT_WORKER_THREAD_STATE_INERROR;
                }
            }
            else
            {
                currentState = SCT_WORKER_THREAD_STATE_INITIALIZED;
            }                   
            break;

        case SCT_WORKER_THREAD_COMMAND_EXECUTE:  /* ************************************************** */

#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d got signal EXECUTE", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */           

            if( currentState != SCT_WORKER_THREAD_STATE_INITIALIZED &&
                currentState != SCT_WORKER_THREAD_STATE_EXECUTING   &&
                currentState != SCT_WORKER_THREAD_STATE_SUSPENDED )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d invalid state for EXECUTE signal", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */           

                SCT_ASSERT( 0 );
                currentState = SCT_WORKER_THREAD_STATE_INERROR;
                break;
            }
            
            status = sctExecuteBenchmarkWorkerThread( benchmark, index, frame++ );

            if( status == SCT_FALSE )
            {
                /* Executing worker threads may fail because of genuine failure,
                 * or if Wait@Thread has been cancelled due to command to
                 * suspend or terminate.  */
                
                if( sctiIsWorkerThreadCancelled( context ) == SCT_FALSE )
                {                
#if defined( SCT_DEBUG )
                    siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d execute failed", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */           
                    
                    currentState = SCT_WORKER_THREAD_STATE_INERROR;
                }
            }
            else
            {
                currentState = SCT_WORKER_THREAD_STATE_EXECUTING;
                /* Do a self-trigger to keep the worker thread executing without
                 * the help of the main thread. */
                siCommonTriggerSignal( siCommon, context->signal );           
            }

            break;

        case SCT_WORKER_THREAD_COMMAND_SUSPEND: /* ************************************************** */

#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d got signal SUSPEND", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */                     
            
            frame = 0;
            currentState = SCT_WORKER_THREAD_STATE_SUSPENDED;
            break;
            
        case SCT_WORKER_THREAD_COMMAND_TERMINATE:  /* ************************************************** */

#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d got signal TERMINATE", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */           

            sctTerminateActions( benchmark, index );
            currentState = SCT_WORKER_THREAD_STATE_TERMINATED;
            break;

        default:

#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctiBenchmarkWorkerThreadFunc \"%s\" thread %d got unknown signal", benchmark->name, index );
#endif  /* defined( SCT_DEBUG ) */           

            currentState = SCT_WORKER_THREAD_STATE_INERROR;            
            SCT_ASSERT( 0 );
            break;
        }
    }
}

/*!
 *
 *
 */
void sctTriggerBenchmarkWorkerThreads( SCTBenchmarkWorkerThreads* threads, SCTWorkerThreadCommand command )
{
    int     i;
    void*   siCommon;
    
    if( threads == NULL )
    {
        return;
    }

    siCommon = siCommonGetContext();
    
#if defined( SCT_DEBUG )
    switch( command )
    {
    case SCT_WORKER_THREAD_COMMAND_INITIALIZE:
        siCommonDebugPrintf( siCommon, "SPANDEX: sctTriggerBenchmarkWorkerThreads, command INITIALIZE (%d)", command );
        break;
    case SCT_WORKER_THREAD_COMMAND_EXECUTE:
        siCommonDebugPrintf( siCommon, "SPANDEX: sctTriggerBenchmarkWorkerThreads, command EXECUTE (%d)", command );
        break;
    case SCT_WORKER_THREAD_COMMAND_SUSPEND:
        siCommonDebugPrintf( siCommon, "SPANDEX: sctTriggerBenchmarkWorkerThreads, command SUSPEND (%d)", command );
        break;
    case SCT_WORKER_THREAD_COMMAND_TERMINATE:
        siCommonDebugPrintf( siCommon, "SPANDEX: sctTriggerBenchmarkWorkerThreads, command TERMINATE (%d)", command );
        break;
    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }
#endif  /* defined( SCT_DEBUG ) */
        
    for( i = 0; i < threads->threadCount; ++i )
    {
        /* Skip empty threads. */
        if( threads->threads[ i ] == NULL )
        {
            continue;
        }

        /* Lock thread mutex before modifying thread state. */
        siCommonLockMutex( siCommon, threads->contexts[ i ].mutex );

        /* Skip terminated threads. */
        if( threads->contexts[ i ].state == SCT_WORKER_THREAD_STATE_TERMINATED )
        {
            siCommonUnlockMutex( siCommon, threads->contexts[ i ].mutex );
            continue;
        }
        
        /* Set thread command and trigger. */
        threads->contexts[ i ].command = command;
        siCommonTriggerSignal( siCommon, threads->contexts[ i ].signal );

        /* Unlock thread mutex. */
        siCommonUnlockMutex( siCommon, threads->contexts[ i ].mutex );
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( siCommon, "SPANDEX: sctTriggerBenchmarkWorkerThreads, command %d DONE", command );
#endif  /* defined( SCT_DEBUG ) */   
}

/*!
 * 
 *
 */
SCTBoolean sctSyncBenchmarkWorkerThreads( SCTBenchmarkWorkerThreads* threads, SCTWorkerThreadState expectedState )
{
    int         i;
    void*       siCommon;
    SCTBoolean  needToWait;
    
    if( threads == NULL )
    {
        return SCT_TRUE;
    }

    siCommon  = siCommonGetContext();
    
#if defined( SCT_DEBUG )
    switch( expectedState )
    {
    case SCT_WORKER_THREAD_STATE_INITIALIZED:
        siCommonDebugPrintf( siCommon, "SPANDEX: sctSyncBenchmarkWorkerThreads, expected state INITIALIZED (%d)", expectedState );
        break;
    case SCT_WORKER_THREAD_STATE_SUSPENDED:
        siCommonDebugPrintf( siCommon, "SPANDEX: sctSyncBenchmarkWorkerThreads, expected state SUSPENDED (%d)", expectedState );
        break;
    case SCT_WORKER_THREAD_STATE_TERMINATED:
        siCommonDebugPrintf( siCommon, "SPANDEX: sctSyncBenchmarkWorkerThreads, expected state TERMINATED (%d)", expectedState );
        break;
    case SCT_WORKER_THREAD_STATE_CREATED:
    case SCT_WORKER_THREAD_STATE_STARTED:
    case SCT_WORKER_THREAD_STATE_EXECUTING:
    case SCT_WORKER_THREAD_STATE_INERROR:
    default:        
        siCommonDebugPrintf( siCommon, "SPANDEX: sctSyncBenchmarkWorkerThreads, expected state %d", expectedState );
        break;
    }
#endif  /* defined( SCT_DEBUG ) */
        
    /* Sync loop. */
    for( ;; )
    {
        /* First, wait for main thread signal. Return with an error in case of
         * timeout. */
        if( siCommonWaitForSignal( siCommon, threads->signal, SCT_THREAD_SIGNAL_TIMEOUT_MILLIS ) < 1 )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctSyncBenchmarkThreads signal wait timeout, expected state %d", expectedState );
#endif  /* defined( SCT_DEBUG ) */
            
            return SCT_FALSE;
        }

        needToWait = SCT_FALSE;

       /* Secondly, check worker thread state. */
        for( i = 0; i < threads->threadCount; ++i )
        {
            /* Skip empty threads. */
            if( threads->threads[ i ] == NULL )
            {
                continue;
            }

            /* Lock thread mutex before modifying thread state. */
            siCommonLockMutex( siCommon, threads->contexts[ i ].mutex );

            /* Make sure the thread is not in error state or terminated. If so,
             * exit with an error.*/
            if( threads->contexts[ i ].state == SCT_WORKER_THREAD_STATE_INERROR ||
                threads->contexts[ i ].state == SCT_WORKER_THREAD_STATE_TERMINATED )
            {
                /* Unlock thread mutex. */                
                siCommonUnlockMutex( siCommon, threads->contexts[ i ].mutex );
                return SCT_FALSE;
            }
            
            /* Need to wait if the thread is not in expected state. */
            if( threads->contexts[ i ].state != expectedState )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( siCommon, "SPANDEX: sctSyncBenchmarkWorkerThreads need to wait" );
#endif  /* defined( SCT_DEBUG ) */
                
                needToWait = SCT_TRUE;
            }

            /* Unlock thread mutex. */
            siCommonUnlockMutex( siCommon, threads->contexts[ i ].mutex );
        }

        /* No need to wait if we did not find any worker threads not in the
         * expected state. */
        if( needToWait == SCT_FALSE )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctSyncBenchmarkWorkerThreads, expected state %d DONE", expectedState );;
#endif  /* defined( SCT_DEBUG ) */
            
            return SCT_TRUE;
        }
    }
}

/*!
 * Reset mutexes and signals, prevents state leaking.
 *
 */
SCTBoolean sctResetThreads()
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctResetThreads" );
#endif  /* defined( SCT_DEBUG ) */

#if defined( INCLUDE_THREAD_MODULE )
    return sctThreadModuleResetExt();
# else /* defined( INCLUDE_THREAD_MODULE ) */
    return SCT_TRUE;
# endif /* defined( INCLUDE_THREAD_MODULE ) */   
}

/*!
 * Cancel threads waiting in Wait@Thread. Also disables potential upcoming
 * waits.
 *
 */
void sctCancelWaits()
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctCancelWaits" );
#endif  /* defined( SCT_DEBUG ) */

#if defined( INCLUDE_THREAD_MODULE )   
    sctThreadModuleCancelWaitsExt();
# endif /* defined( INCLUDE_THREAD_MODULE ) */   
}

/*!
 * Check if the benchmark defines worker threads. This is determined by checking
 * if the benchmark defines benchmark actions for threads other than thread
 * index 0 (= main thread).
 *
 * \param benchmark Benchmark. Must be defined.
 *
 * \return SCT_TRUE if benchmark defines benchmark threads, SCT_FALSE otherwise.
 */
SCTBoolean sctHasWorkerThreads( SCTBenchmark* benchmark )
{
    int i;
    
    SCT_ASSERT( benchmark != NULL );

    for( i = 0; i < SCT_MAX_WORKER_THREADS; ++i )
    {
        if( benchmark->workerThreads[ i ].benchmarkActions != NULL )
        {
            return SCT_TRUE;
        }
    }

    return SCT_FALSE;
}

/*!
 * Check if the benchmark defines a worker thread with a given index.
 *
 * \param benchmark     Benchmark. Must be defined.
 * \param threadIndex   Thread index. Must be >= 0 and < SCT_MAX_WORKER_THREADS. 
 *
 * \return SCT_TRUE if benchmark defines benchmark threads, SCT_FALSE otherwise.
 */
SCTBoolean sctHasWorkerThread( SCTBenchmark* benchmark, int threadIndex )
{
    SCT_ASSERT( benchmark != NULL );
    SCT_ASSERT( threadIndex >= 0 && threadIndex < SCT_MAX_WORKER_THREADS  );    

    if( benchmark->workerThreads[ threadIndex ].benchmarkActions != NULL )
    {
        return SCT_TRUE;
    }

    return SCT_FALSE;
}


/*!
 * Create benchmark worker threads. Worker threads are started after creation.
 *
 * \param benchmark     Benchmark. Must be defined.
 *
 * \return Pointer to BenchmarkWorkerThreads, or NULL if failure.
 */
SCTBenchmarkWorkerThreads* sctCreateBenchmarkWorkerThreads( SCTBenchmark* benchmark )
{
    SCTBenchmarkWorkerThreads*  threads;
    int                         i;
    void*                       siCommon;
    
    SCT_ASSERT( benchmark != NULL );

    siCommon = siCommonGetContext();
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( siCommon, "SPANDEX: sctCreateBenchmarkWorkerThreads creating \"%s\" benchmark threads", benchmark->name );
#endif  /* defined( SCT_DEBUG ) */           
    
    /* Exclude main thread. */
    threads = ( SCTBenchmarkWorkerThreads* )( siCommonMemoryAlloc( siCommon, sizeof( SCTBenchmarkWorkerThreads ) ) );

    if( threads == NULL )
    {
        return NULL;
    }
    
    memset( threads, 0, sizeof( SCTBenchmarkWorkerThreads ) );

    threads->threadCount = SCT_MAX_WORKER_THREADS;
    
    /* Create a common signal for worker threads to signal the main thread. */
    threads->signal = siCommonCreateSignal( siCommon );

    if( threads->signal == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( siCommon, "SPANDEX: sctCreateBenchmarkWorkerThreads creating \"%s\" common signal object failed", benchmark->name );
#endif  /* defined( SCT_DEBUG ) */           
            
        sctJoinBenchmarkWorkerThreads( threads );
        return NULL;
    }
    
    for( i = 0; i < SCT_MAX_WORKER_THREADS; ++i )
    {
        /* Skip empty worker threads. */
        if( sctHasWorkerThread( benchmark, i ) == SCT_FALSE )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctCreateBenchmarkWorkerThreads skipping \"%s\" empty thread %d", benchmark->name, i );
#endif  /* defined( SCT_DEBUG ) */           
            
            continue;
        }
        
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( siCommon, "SPANDEX: sctCreateBenchmarkWorkerThreads creating \"%s\" thread %d", benchmark->name, i );
#endif  /* defined( SCT_DEBUG ) */           
        
        threads->contexts[ i ].benchmark  = benchmark;
        threads->contexts[ i ].index      = i;
        threads->contexts[ i ].state      = SCT_WORKER_THREAD_STATE_CREATED;
        threads->contexts[ i ].mainSignal = threads->signal;

        threads->contexts[ i ].mutex = siCommonCreateMutex( siCommon );
        
        if( threads->contexts[ i ].mutex == NULL )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctCreateBenchmarkWorkerThreads creating \"%s\" thread %d mutex object failed", benchmark->name, i );
#endif  /* defined( SCT_DEBUG ) */           
            
            sctJoinBenchmarkWorkerThreads( threads );
            return NULL;
        }
        
        threads->contexts[ i ].signal = siCommonCreateSignal( siCommon );
        
        if( threads->contexts[ i ].signal == NULL )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctCreateBenchmarkWorkerThreads creating \"%s\" thread %d signal object failed", benchmark->name, i );
#endif  /* defined( SCT_DEBUG ) */           
            
            sctJoinBenchmarkWorkerThreads( threads );           
            return NULL;
        }
        
        threads->threads[ i ] = siCommonCreateThread( siCommon, sctiBenchmarkWorkerThreadFunc, &( threads->contexts[ i ] ) );

        if( threads->threads[ i ] == NULL )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( siCommon, "SPANDEX: sctCreateBenchmarkWorkerThreads creating \"%s\" thread %d thread object failed", benchmark->name, i );
#endif  /* defined( SCT_DEBUG ) */           
            
            sctJoinBenchmarkWorkerThreads( threads );
            return NULL;
        }
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( siCommon, "SPANDEX: sctCreateBenchmarkWorkerThreads \"%s\" benchmark threads DONE", benchmark->name );
#endif  /* defined( SCT_DEBUG ) */           
    
    return threads;
}

/*!
 * Join benchmark worker threads.
 *
 * \param threads   Benchmark worker threads. Deallocated by the function when defined.
 *
 */
void sctJoinBenchmarkWorkerThreads( SCTBenchmarkWorkerThreads* threads )
{
    int     i;
    void*   siCommon;
    
    if( threads == NULL )
    {
        return;
    }

    siCommon = siCommonGetContext();
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( siCommon, "SPANDEX: sctJoinBenchmarkWorkerThreads joining benchmark threads" );
#endif  /* defined( SCT_DEBUG ) */           
    
    /* First, request all threads to terminate. */
    sctTriggerBenchmarkWorkerThreads( threads, SCT_WORKER_THREAD_COMMAND_TERMINATE );
    
    /* Secondly, wait threads to terminate. */
    for( i = 0; i < SCT_MAX_WORKER_THREADS; ++i )
    {
        if( threads->threads[ i ] != NULL )
        {
            siCommonJoinThread( siCommon, threads->threads[ i ] );
            threads->threads[ i ] = NULL;
        }
    }

    /* Destroy thread mutexes and signals. */
    for( i = 0; i < SCT_MAX_WORKER_THREADS; ++i )
    {
        if( threads->contexts[ i ].mutex != NULL )
        {
            siCommonDestroyMutex( siCommon, threads->contexts[ i ].mutex );    
            threads->contexts[ i ].mutex = NULL;
        }
        
        if( threads->contexts[ i ].signal != NULL )
        {
            siCommonDestroySignal( siCommon, threads->contexts[ i ].signal );    
            threads->contexts[ i ].signal = NULL;
        }       
    }
    
    /* Destroy main thread signal. */
    if( threads->signal != NULL )
    {
        siCommonDestroySignal( siCommon, threads->signal );
        threads->signal = NULL;
    }

    /* Deallocate and we are done. */
    siCommonMemoryFree( siCommon, threads );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctJoinBenchmarkWorkerThreads benchmark threads joined" );
#endif  /* defined( SCT_DEBUG ) */   
}

/*!
 *
 *
 */
static SCTBoolean sctiIsWorkerThreadCancelled( SCTBenchmarkWorkerThreadContext* context )
{
    SCTBoolean s;
    
    SCT_ASSERT( context->mutex != NULL );
    
    siCommonLockMutex( NULL, context->mutex );        
    
    if( context->command == SCT_WORKER_THREAD_COMMAND_SUSPEND ||
        context->command == SCT_WORKER_THREAD_COMMAND_TERMINATE )
    {
        s = SCT_TRUE;
    }
    else
    {
        s = SCT_FALSE;
    }

    siCommonUnlockMutex( NULL, context->mutex );

    return s;
}

/*!
 *
 *
 */
static void sctiChangeWorkerThreadState( SCTBenchmarkWorkerThreadContext* context, SCTWorkerThreadState state )
{
    SCT_ASSERT( context->mutex != NULL );
    
    siCommonLockMutex( NULL, context->mutex );        

    context->state = state;

    /* Disable potential main thread waits at Wait@Thread. */
    if( state == SCT_WORKER_THREAD_STATE_INERROR ||
        state == SCT_WORKER_THREAD_STATE_TERMINATED )
    {
        sctCancelWaits();
    }

    if( state != SCT_WORKER_THREAD_STATE_EXECUTING )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctiChangeWorkerThreadState triggering main thread signal" );
#endif  /* defined( SCT_DEBUG ) */   
    
        siCommonTriggerSignal( NULL, context->mainSignal );
    }
    
    siCommonUnlockMutex( NULL, context->mutex );
}

/*!
 *
 *
 */
static SCTWorkerThreadCommand sctiGetWorkerThreadCommand( SCTBenchmarkWorkerThreadContext* context )
{
    SCTWorkerThreadCommand  c;
    
    SCT_ASSERT( context != NULL );
    
    siCommonLockMutex( NULL, context->mutex );        
    c = context->command;
    siCommonUnlockMutex( NULL, context->mutex );

    return c;
}
 
