/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <windows.h>
#include <aygshell.h>
#include "sct.h"
#include "sct_sicommon.h"
#include "sct_types.h"

/*!
 *
 *
 */
#define SCT_WINMO_USE_SH_FULL_SCREEN

/*!
 *
 *
 */
FILE*               gInputFile          = NULL;
FILE*               gOutputFile         = NULL;
HWND				gHWnd               = NULL;

/*!
 * 
 * 
 */
enum STATE
{ 
    State_Loading,
    State_Running
};

/*!
 *
 *
 */
STATE               gAppState           = State_Loading;
int                 gWindowWidth        = 0;
int                 gWindowHeight       = 0;
void*               gSctContext         = NULL;
static const TCHAR	APP_NAME[]		    = L"Spandex_opengles2";
static const TCHAR  INPUT_FILE_NAME[]   = L"/spandex_opengles2/benchmark.txt";
static const TCHAR  OUTPUT_FILE_NAME[]  = L"/spandex_opengles2/result.txt";

/*!
 *
 *
 */
extern int          gTotalBenchmarks;
extern int          gCurrentBenchmark;

/*!
 *
 *
 */
LRESULT	CALLBACK    WndProc( HWND, UINT, WPARAM, LPARAM );
static SCTBoolean   setupDepthAndFrameBufferInfo();

/*!
 *
 *
 */
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
    MSG         msg;
  	WNDCLASS	wc;

    /*!
     * Register window class 
     */
	memset( &wc, 0, sizeof( WNDCLASS ) );

    wc.style            = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc      = ( WNDPROC )WndProc;
    wc.cbClsExtra       = 0;
    wc.cbWndExtra       = 0;
    wc.hInstance        = hInstance;
    wc.hIcon            = NULL;
    wc.hCursor          = LoadCursor( NULL, IDC_ARROW );
    wc.hbrBackground    = ( HBRUSH )GetStockObject( BLACK_BRUSH );
    wc.lpszMenuName     = NULL;
    wc.lpszClassName    = APP_NAME;

	if( !RegisterClass( &wc ) )
    {
		return 1;
    }
	
    /*!
     * Create window
     */
    gHWnd = CreateWindow(   APP_NAME, 
	    				    APP_NAME, 
							WS_VISIBLE,
							0, 0,
				  			GetSystemMetrics( SM_CXSCREEN ),
							GetSystemMetrics( SM_CYSCREEN ),
							NULL, 
							NULL, 
							hInstance, 
							NULL );

    if( !IsWindow( gHWnd ) )
    {
        return 1;
    }

#if defined( SCT_WINMO_USE_SH_FULL_SCREEN )
	/*!
     * Switch to fullscreen.
     */
	SHFullScreen( gHWnd, SHFS_HIDETASKBAR | SHFS_HIDESTARTICON | SHFS_HIDESIPBUTTON ); 
#endif  /* defined( SCT_WINMO_USE_SH_FULL_SCREEN ) */

    /*!
     * Get full screen window size and store values for future reference.
     */
    RECT crect;
    GetClientRect( gHWnd, &crect );
	gWindowWidth	= ( crect.right - crect.left );
	gWindowHeight	= ( crect.bottom - crect.top );

    ShowWindow( gHWnd, SW_SHOW );
    UpdateWindow( gHWnd );

    /*!
     * Open input and output files.
     */
	gInputFile = _wfopen( INPUT_FILE_NAME, L"r" );
	if( gInputFile == NULL )
	{
		MessageBox( NULL, L"Cannot open input file for reading.", L"Fatal Error", MB_OK | MB_ICONERROR );
	}

	gOutputFile = _wfopen( OUTPUT_FILE_NAME, L"w+" );
	if( gOutputFile == NULL )
	{
		MessageBox( NULL, L"Cannot create output file for writing.", L"Fatal Error", MB_OK | MB_ICONERROR );
    }

    /*!
     * Go to message loop if input and output files are opened ok
     */
    if( gInputFile != NULL && gOutputFile != NULL )
    {
        /*!
         * Initialize Spandex core
         */
        gSctContext = sctInitialize();
        if( gSctContext != NULL )
        {
	        gAppState = State_Running;
	        InvalidateRect( gHWnd, NULL, TRUE );
            UpdateWindow( gHWnd );

            SetTimer( gHWnd, 1, 1000, NULL );

	        while( GetMessage( &msg, NULL, 0, 0 ) ) 
	        {
		        TranslateMessage( &msg );
		        DispatchMessage( &msg );
	        }

            KillTimer( gHWnd, 1 );

            sctTerminate( gSctContext );
        }
        else
        {
            MessageBox( gHWnd, L"Init failed", L"Spandex error", MB_OK | MB_ICONERROR );
        }
    }

    fclose( gInputFile );
    fclose( gOutputFile );

    gInputFile = NULL;
    gOutputFile = NULL;

    if( IsWindow( gHWnd ) )
    {
        DestroyWindow( gHWnd );
    }

	UnregisterClass( APP_NAME, hInstance );

	return 0;
}


/*!
 *
 *
 */
LRESULT CALLBACK WndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    SCTCycleStatus status;

	switch( uMsg )
	{
		case WM_CLOSE:
			PostQuitMessage( 0 );
			return 0;

        case WM_PAINT:
			HDC hdc;
			PAINTSTRUCT ps;
			RECT rt;
			TCHAR szStatus[ 256 ];
			
			if( gAppState == State_Running )
			{
				if( gCurrentBenchmark == 0 )
                {
					wcscpy( szStatus, L"Preparing to run..." );
                }
				else
				{
					swprintf( szStatus, L"Running: (%d/%d)", gCurrentBenchmark, gTotalBenchmarks );
				}
			}
			else
			{
				wcscpy( szStatus, L"Loading..." );
			}

			hdc = BeginPaint( hWnd, &ps );
			GetClientRect( hWnd, &rt );
			SetBkColor( hdc, RGB( 0, 0, 0 ) );
			SetTextColor( hdc, RGB( 255, 255, 255 ) );
			DrawText( hdc, szStatus, _tcslen( szStatus ), &rt, DT_SINGLELINE | DT_VCENTER | DT_CENTER );
			EndPaint( hWnd, &ps );

            return 0;

        case WM_TIMER:
			SCT_ASSERT_ALWAYS( gSctContext != NULL );
	        status = sctCycle( gSctContext );
	        
            if( status == SCT_CYCLE_FATAL_ERROR )
            {
                MessageBox( hWnd, L"Cycle fatal error", L"Spandex error", MB_OK | MB_ICONERROR );
                PostQuitMessage( 0 );
            }
            else if( status == SCT_CYCLE_FINISHED )
            {
                PostQuitMessage( 0 );
            }

            return 0; 

        default:
            return DefWindowProc( hWnd, uMsg, wParam, lParam );
	}
}

