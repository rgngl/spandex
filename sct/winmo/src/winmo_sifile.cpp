/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <string.h>

#include "sct_sifile.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

/*!
 *
 *
 */
#define SCT_SIFILE_WINMO_CONTEXT        0x1234

/*!
 *
 *
 */
void* siFileCreateContext()
{
    return ( void* )( SCT_SIFILE_WINMO_CONTEXT );
}

/*!
 *
 *
 */
void siFileDestroyContext( void* context )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIFILE_WINMO_CONTEXT ) );
}

/*!
 *
 *
 */    
SCTFile siFileOpen( void* context, const char* name, const char* mode )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIFILE_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( mode != NULL );

    return ( SCTFile )( fopen( name, mode ) );
}

/*!
 *
 *
 */
void siFileClose( void* context, SCTFile file )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIFILE_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( file != NULL );

    fclose( ( FILE* )( file ) );
}

/*!
 *
 *
 */
unsigned int siFileRead( void* context, SCTFile file, char* data, unsigned int length )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIFILE_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( file != NULL );
    SCT_ASSERT_ALWAYS( data != NULL );

    return fread( data, 1, length, ( FILE* )( file ) );
}

/*!
 *
 *
 */
int siFileReadByte( void* context, SCTFile file )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIFILE_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( file != NULL );

    return getc( ( FILE* )( file ) );
}

/*!
 *
 *
 */
unsigned int siFileWrite( void* context, SCTFile file, const char* data, unsigned int length )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIFILE_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( file != NULL );
    SCT_ASSERT_ALWAYS( data != NULL );

    return ( unsigned int )( fwrite( data, 1, length, ( FILE* )( file ) ) );
}

/*!
 *
 *
 */
SCTBoolean siFileEOF( void* context, SCTFile file )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIFILE_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( file != NULL );

    return ( SCTBoolean )( feof( ( FILE* )( file ) ) );
}

/*!
 *
 *
 */
int siFileSize( void* context, SCTFile file )
{
    long    currentPosition;
    long    fileSize;
    int     status;

    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIFILE_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( file != NULL );

    // Store current file position
    currentPosition = ftell( ( FILE* )( file ) );

    // Set file position to the end of the file
    status = fseek( ( FILE* )( file ), 0, SEEK_END );
    SCT_ASSERT_ALWAYS( status == 0 );

    // Get the file position at the end of the file (=file size)
    fileSize = ftell( ( FILE* )( file ) );

    // Restore the original file position
    status = fseek( ( FILE* )( file ), currentPosition, SEEK_SET );
    SCT_ASSERT_ALWAYS( status == 0 );

    return fileSize;
}

/*!
 *
 *
 */
SCTBoolean siFileMkdir( void* context, const char* name )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIFILE_WINMO_CONTEXT ) );
	SCT_USE_VARIABLE( name );

	SCT_ASSERT_ALWAYS( 0 );
	return SCT_FALSE;
/*
    char*       buf;
    int         len;
    int         i;
    char        t;

    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIFILE_WIN32_CONTEXT ) );
    SCT_ASSERT_ALWAYS( name != NULL );

    len = strlen( name );
    buf = ( char* )( siCommonMemoryAlloc( NULL, len + 1 ) );
    if( buf == NULL )
    {
        return SCT_FALSE;
    }

    sctStrncopy( buf, name, len );

    for( i = 0; i < len; ++i )
    {
        if( buf[ i ] == '/' || buf[ i ] == '\\' )
        {
            t = buf[ i ];
            buf[ i ] = '\0';
            _mkdir( buf );
            buf[ i ] = t;
        }
    }

    siCommonMemoryFree( NULL, buf );

    return SCT_TRUE;
*/
}
