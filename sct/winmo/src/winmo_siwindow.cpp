/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/*!
 *
 *
 */
#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "sct_sicommon.h"
#include "sct_siwindow.h"

/*!
 *
 */
extern int                              gWindowWidth;
extern int                              gWindowHeight;
extern HWND                             gHWnd;

/*!
 *
 */
#define SCT_SIWINDOW_WINMO_CONTEXT      0x1234

/*!
 *
 */
void* siWindowCreateContext()
{
    return ( void* )( SCT_SIWINDOW_WINMO_CONTEXT );
}

/*!
 *
 */
void siWindowDestroyContext( void* context )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
}

/*!
 *
 */
SCTBoolean siWindowIsScreenSupported( void* context, SCTScreen screen )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );    
    
    if( screen != SCT_SCREEN_DEFAULT )
    {
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 */
void siWindowGetScreenSize( void* context, SCTScreen screen, int* width, int* height )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );
    SCT_ASSERT_ALWAYS( width != NULL );
    SCT_ASSERT_ALWAYS( height != NULL );

    *width  = gWindowWidth;
    *height = gWindowHeight;
}

/*!
 *
 *
 */
void* siWindowGetScreenEglDisplayId( void* context, SCTScreen screen )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );    

    return ( void* )( 0 );
}

/*!
 *
 *
 */
SCTBoolean siWindowSetScreenOrientation( void* context, SCTScreen screen, SCTScreenOrientation orientation )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );        
    SCT_USE_VARIABLE( orientation );

    return SCT_FALSE;
}

/*!
 *
 *
 */
void* siWindowCreateWindow( void* context, SCTScreen screen, int x, int y, int width, int height, SCTColorFormat format, void* parent )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );
    SCT_USE_VARIABLE( x );
    SCT_USE_VARIABLE( y );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( parent );    
   
    return gHWnd;
}

/*!
 *
 *
 */
void siWindowDestroyWindow( void* context, void* window )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( window == ( void* )( gHWnd ) );
}

/*!
 *
 *
 */
void* siWindowGetEglWindow( void* context, void* window )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( window == ( void* )( gHWnd ) );

    return window;
}

/*!
 *
 *
 */
SCTScreen siWindowGetWindowScreen( void* context, void* window )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( window == ( void* )( gHWnd ) );

    return SCT_SCREEN_DEFAULT;
}


/*!
 *
 *
 */
SCTBoolean siWindowToFront( void* context, void* window )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( window == ( void* )( gHWnd ) );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean siWindowResize( void* context, void* window, int x, int y, int width, int height )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( window == ( void* )( gHWnd ) );
    SCT_USE_VARIABLE( x );
    SCT_USE_VARIABLE( y );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    
    return SCT_FALSE;
}

/*!
 *
 *
 */
SCTBoolean siWindowSetOpacity( void* context, void* window, float opacity )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( window == ( void* )( gHWnd ) );
    SCT_USE_VARIABLE( opacity );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetAlpha( void* context, void* window, SCTBoolean enable )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( window == ( void* )( gHWnd ) );
    SCT_USE_VARIABLE( enable );

    return SCT_FALSE;
}

/*!
 *
 *
 */
SCTBoolean siWindowRefresh( void* context, void* window )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( window == ( void* )( gHWnd ) );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void* siWindowCreatePixmap( void* context, int width, int height, SCTColorFormat format, void* data, int length )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
	SCT_USE_VARIABLE( width );
	SCT_USE_VARIABLE( height );
	SCT_USE_VARIABLE( format );
	SCT_USE_VARIABLE( data );
	SCT_USE_VARIABLE( length );

    return NULL;
}

/*!
 *
 *
 */
void* siWindowCreateSharedPixmap( void* context, int width, int height, SCTColorFormat format, int usage, void* data, int length )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );    
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( usage );
	SCT_USE_VARIABLE( data );
	SCT_USE_VARIABLE( length );
    
    return NULL;
}

/*!
 * 
 *
 */
SCTBoolean siWindowSetPixmapData( void* context, void* pixmap, void* data, int length )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );    
    SCT_ASSERT_ALWAYS( pixmap != NULL );
    SCT_ASSERT_ALWAYS( data != NULL );
    SCT_ASSERT_ALWAYS( length > 0 );

    return SCT_FALSE;
}

/*!
 *
 *
 */
void* siWindowGetEglPixmap( void* context, void* pixmap, int* target )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( pixmap != NULL );
    SCT_USE_VARIABLE( target );
    
    return pixmap;
}

/*!
 *
 *
 */
SCTBoolean siWindowBlitPixmap( void* context, void* window, void* pixmap, int x, int y )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
    SCT_ASSERT_ALWAYS( window == ( void* )( gHWnd ) );
	SCT_USE_VARIABLE( pixmap );
	SCT_USE_VARIABLE( x );
	SCT_USE_VARIABLE( y );

    return SCT_FALSE;
}

/*!
 *
 *
 */
void siWindowDestroyPixmap( void* context, void* pixmap )
{
	SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_WINMO_CONTEXT ) );
	SCT_USE_VARIABLE( pixmap );
}
