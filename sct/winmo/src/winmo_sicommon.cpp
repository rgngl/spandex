/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/*!
 *
 *
 */
#include <windows.h>
#include <afx.h>
#include <stdio.h>
#include <stdarg.h>

#include "sct_sicommon.h"
#include "sct_utils.h"

#if defined( INCLUDE_EGL_MODULE )
/* For sctEglGetProcAddress */
#include "sct_eglmodule.h"
#endif  /* defined( INCLUDE_EGL_MODULE ) */

#define SCT_WINMO_WRITE_ERROR_TO_FILE

/*!
 *
 *
 */
int                 gTotalBenchmarks                    = 0;
int                 gCurrentBenchmark                   = 0;
int                 gCurrentRepeat                      = 0;
TCHAR               gBenchmarkName[ 256 ];
SCTLog*             gErrorLog                           = NULL;
const int           gOutputBufferSize                   = 1024;
char                gOutputBuffer[ gOutputBufferSize ];
int                 gOutputSize                         = 0;
void*               gSingleton                          = NULL;

/*!
 *
 *
 */
extern FILE*        gOutputFile;
extern FILE*        gInputFile;
extern HWND         gHWnd;

static const char*	getCPUInfo( void );
static const char*	getOsInfo( void );

/*!
 *
 *
 */
void* siCommonGetContext( void )
{
	return NULL;
}

/*!
 *
 *
 */
const char* siCommonQuerySystemString( void*, SCTSystemString id )
{
    switch( id )
    {
    case SCT_SYSSTRING_DEVICE_TYPE:
        return "WindowsMobile";
        
    case SCT_SYSSTRING_OS:
        return getOsInfo();
        
    case SCT_SYSSTRING_CPU:
        return getCPUInfo();

    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }

    return "Unexpected";
}   

/*!
 *
 *
 */
size_t siCommonQueryMemoryAttribute( void*, SCTMemoryAttribute id )
{
    MEMORYSTATUS    stat;

    GlobalMemoryStatus( &stat );

    switch( id )
    {
    case SCT_MEMATTRIB_TOTAL_SYSTEM_MEMORY:
        return stat.dwTotalPhys );
        
    case SCT_MEMATTRIB_USED_SYSTEM_MEMORY:
        return stat.dwTotalPhys - stat.dwAvailPhys;
        
    case SCT_MEMATTRIB_TOTAL_GRAPHICS_MEMORY:           /* Intentional. */
    case SCT_MEMATTRIB_USED_GRAPHICS_MEMORY:            /* Intentional. */
    case SCT_MEMATTRIB_USED_PRIVATE_GRAPHICS_MEMORY:    /* Intentional. */
    case SCT_MEMATTRIB_USED_SHARED_GRAPHICS_MEMORY:     /* Intentional. */
        /* Not implemented. */
        return 0;
        
    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }

    return 0;
}   

/*!
 *
 *
 */
void* siCommonSampleCpuLoad( void* context )
{
    SCT_USE_VARIABLE( context );

    return NULL;    /* Not supported. */
}

/*!
 *
 *
 */
float siCommonCalculateCpuLoad( void* context, void* sample, SCTCpuLoad load )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( sample );
    SCT_USE_VARIABLE( load );
    
    return -1.0f;
}

/*!
 *
 *
 */
void siCommonDestroyCpuLoadSample( void* context, void* sample )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( sample );
}

/*!
 *
 *
 */
unsigned long siCommonGetTimerFrequency( void* )
{
	LARGE_INTEGER frequency;

	QueryPerformanceFrequency( &frequency );

	return ( unsigned long )( frequency.QuadPart );
}

/*!
 *
 *
 */
unsigned long siCommonGetTimerTick( void* )
{
	LARGE_INTEGER timerTick;

	QueryPerformanceCounter( &timerTick );
	
	return ( unsigned long )( timerTick.QuadPart );
}

/*!
 *
 *
 *
 */
void siCommonDebugPrintf( void*, const char* fmt, ... )
{
	va_list ap;

    SCT_ASSERT_ALWAYS( fmt != NULL );

	va_start( ap, fmt );
	vprintf( fmt, ap );
	va_end( ap );

    printf( "\n" );
}

/*!
 *
 *
 */
void siCommonErrorMessage( void*, const char* errorString )
{
#if defined( SCT_WINMO_WRITE_ERROR_TO_FILE )
    if( gOutputFile != NULL )
    {
		fprintf( gOutputFile, "SPANDEX ERROR: %s\n", errorString );
		fflush( gOutputFile );
    }
#else   /* defined( SCT_WINMO_WRITE_ERROR_TO_FILE ) */
    /* TODO: truncate the error message to guarantee fit into fixed
     * buffer. Beware reporting OOM errors, i.e. do not use dynamic
     * allocation. */
    char    buf[ 1024 ];
	wchar_t unicodeErrStr[ 1024 ];

    sprintf( buf, "SPANDEX ERROR: %s", errorString );

	MultiByteToWideChar( CP_ACP, 0, buf, -1, unicodeErrStr, 1024 );
	MessageBox( gHWnd , unicodeErrStr, L"Spandex error", MB_OK | MB_ICONERROR );
#endif  /* defined( SCT_WINMO_WRITE_ERROR_TO_FILE ) */
}

/*!
 *
 *
 */
void siCommonWarningMessage( void*, const char* warningString )
{
#if defined( SCT_WINMO_WRITE_ERROR_TO_FILE )
    if( gOutputFile != NULL )
    {
        fprintf( gOutputFile, "#SPANDEX WARNING: %s\n", warningString );
		fflush( gOutputFile );
    }
#else   /* defined( SCT_WINMO_WRITE_ERROR_TO_FILE ) */
    /* TODO: truncate the warning message to guarantee fit into fixed
     * buffer. Beware reporting OOM errors, i.e. do not use dynamic
     * allocation. */
    char    buf[ 1024 ];
	wchar_t unicodeErrStr[ 1024 ];

    sprintf( buf, "SPANDEX WARNING: %s", warningString );

	MultiByteToWideChar( CP_ACP, 0, buf, -1, unicodeErrStr, 1024 );
	MessageBox( gHWnd , unicodeErrStr, L"Spandex warning", MB_OK | MB_ICONERROR );
#endif  /* defined( SCT_WINMO_WRITE_ERROR_TO_FILE ) */
}

/*!
 *
 *
 */
void* siCommonGetSingleton( void* )
{
    return gSingleton;
}

/*!
 *
 *
 */
void siCommonSetSingleton( void*, void* ptr )
{
    gSingleton = ptr;
}

/*!
 *
 *
 */
void siCommonProgressMessage( void*, int totalBenchmarks, int currentBenchmark, int currentRepeat, const char* benchmarkName )
{
	gTotalBenchmarks    = totalBenchmarks;
	gCurrentBenchmark   = currentBenchmark;
	gCurrentRepeat      = currentRepeat;

	MultiByteToWideChar( CP_ACP, 0, benchmarkName, -1, gBenchmarkName, sizeof( gBenchmarkName )/sizeof( gBenchmarkName[ 0 ]) );

	InvalidateRect( gHWnd, NULL, TRUE );
    UpdateWindow( gHWnd );
}

/*!
 *
 *
 */
void siCommonAssert( void*, void* expr, void* file, unsigned int line )
{
	if( gOutputFile )
    {
		fprintf( gOutputFile, "Assertion (%s) failed\nFile: %s\nLine %d", expr, file, line );
        fflush( gOutputFile );
    }

	MessageBox( gHWnd, L"Assertion failed, check output file for details", L"Spandex assert", MB_OK | MB_ICONERROR );
    TerminateProcess( GetCurrentProcess(), 1 );
}

/*!
 * 
 *
 */
void* siCommonMemoryAlloc( void*, unsigned size )
{
	return malloc( size );
}

/*!
 * 
 *
 */
void* siCommonMemoryRealloc( void*, void* aData, unsigned int aSize )
{
	return realloc( aData, aSize );
}

/*!
 * 
 *
 */
void siCommonMemoryFree( void*, void* memblock )
{
    free( memblock );
}

/*!
 * 
 *
 */
char siCommonReadChar( void* )
{
	SCT_ASSERT_ALWAYS( gInputFile != NULL );

	int ch = fgetc( gInputFile );
	if( ch == EOF )
    {
		return '\0';
    }
    SCT_ASSERT( ch >= 0 && ch < 256 );
	return ( char )ch;
}

/*!
 * 
 *
 */
int siCommonWriteChar( void* aContext, char ch )
{
    gOutputBuffer[ gOutputSize++ ] = ch;
    if( gOutputSize >= gOutputBufferSize )
    {
        return siCommonFlushOutput( aContext );
    }
    else
    {
        return 1;
    }
}

/*!
 *
 *
 */
int siCommonFlushOutput( void* )
{
    if( gOutputSize == 0 )
    {
		fflush( gOutputFile );	        
        return 1;
    }
    
	int n = fwrite( gOutputBuffer, gOutputSize, 1, gOutputFile );

    gOutputSize = 0;
    fflush( gOutputFile );

    return ( n == 1 );
}

/*!
 *
 *
 */
void* siCommonCreateThread( void*, SCTBenchmarkWorkerThreadFunc func, SCTBenchmarkWorkerThreadContext* threadContext )
{
    SCT_ASSERT_ALWAYS( func != NULL );
    SCT_ASSERT_ALWAYS( threadContext != NULL );

    return CreateThread( NULL,
                         0,
                         ( LPTHREAD_START_ROUTINE )( func ),
                         ( LPVOID )( threadContext ),
                         0,
                         NULL );
}

/*!
 *
 *
 */
void siCommonThreadCleanup( void* )
{
    /* Do nothing. */
}

/*!
 *
 *
 */
void siCommonJoinThread( void*, void* thread )
{
    HANDLE  hThread;
    
    SCT_ASSERT_ALWAYS( thread != NULL );

    hThread = ( HANDLE )( thread );

    WaitForSingleObject( hThread, INFINITE );
    CloseHandle( hThread );
}

/*!
 *
 *
 */
SCTBoolean siCommonSetThreadPriority( void*, SCTThreadPriority priority )
{
    int p;

    switch( priority )
    {
    case SCT_THREAD_PRIORITY_LOW:
        p = THREAD_PRIORITY_BELOW_NORMAL;
        break;
    case SCT_THREAD_PRIORITY_NORMAL:
        p = THREAD_PRIORITY_NORMAL;
        break;
    case SCT_THREAD_PRIORITY_HIGH:
        p = THREAD_PRIORITY_ABOVE_NORMAL;
        break;
    default:
        SCT_ASSERT_ALWAYS( 0 );
        return SCT_FALSE;
    }   

    return SetThreadPriority( GetCurrentThread(), p ) ? SCT_TRUE : SCT_FALSE;
}

/*!
 *
 *
 */
void* siCommonCreateMutex( void* )
{
    return CreateMutex( NULL, FALSE, NULL );
}

/*!
 *
 *
 */
void siCommonLockMutex( void*, void* mutex )
{
    HANDLE  hMutex;
    
    SCT_ASSERT_ALWAYS( mutex != NULL );

    hMutex = ( HANDLE )( mutex );
    
    WaitForSingleObject( hMutex, INFINITE );    
}

/*!
 *
 *
 */
void siCommonUnlockMutex( void*, void* mutex )
{
    HANDLE  hMutex;
    
    SCT_ASSERT_ALWAYS( mutex != NULL );

    hMutex = ( HANDLE )( mutex );
    ReleaseMutex( hMutex );
}
        
/*!
 *
 *
 */
void siCommonDestroyMutex( void*, void* mutex )
{
    HANDLE  hMutex;
    
    SCT_ASSERT_ALWAYS( mutex != NULL );

    hMutex = ( HANDLE )( mutex );
    CloseHandle( hMutex );    
}

/*!
 *
 *
 */ 
struct SCTWinMoSignal
{
    HANDLE          mutex;
    HANDLE          event;
    volatile int    status;
    volatile int    enabled;
};

/*!
 *
 *
 */
void* siCommonCreateSignal( void* )
{
    SCTWinMoSignal* signal;

    signal = ( SCTWinMoSignal* )( siCommonMemoryAlloc( NULL, sizeof( SCTWinMoSignal ) ) );
    
    if( signal == NULL )
    {
        return NULL;
    }

    memset( signal, 0, sizeof( SCTWinMoSignal ) );

    signal->mutex = ( HANDLE )( siCommonCreateMutex( NULL ) );
    
    if( signal->mutex == NULL )
    {
        siCommonDestroySignal( NULL, signal );
        return NULL;
    }

    signal->event = CreateEvent( NULL, TRUE, FALSE, NULL );

    if( signal->event == NULL )
    {
        siCommonDestroySignal( NULL, signal );
        return NULL;
    }

    signal->status  = 1;
    signal->enabled = 1;
    
    return signal;
}

/*!
 *
 *
 */
void siCommonDestroySignal( void*, void* signal )
{
    SCTWinMoSignal* s;
    
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTWinMoSignal* )( signal );

    if( s->event != NULL )
    {
        CloseHandle( s->event );
        s->event = NULL;
    }

    if( s->mutex != NULL )
    {
        CloseHandle( s->mutex );
        s->mutex = NULL;
    }

    siCommonMemoryFree( NULL, signal );
}

/*!
 *
 *
 */
int siCommonWaitForSignal( void*, void* signal, long timeoutMillis )
{
    SCTWinMoSignal* s;    
    DWORD           to      = INFINITE;
    DWORD           r;
    int             status;
    int             enabled;
        
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTWinMoSignal* )( signal );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonWaitForSignal %x", signal );
#endif  /* defined( SCT_DEBUG ) */
    
    if( timeoutMillis > 0 )
    {
        to = timeoutMillis;
    }       

    /* First, lock signal mutex. */
    WaitForSingleObject( s->mutex, INFINITE );

    /* Check if the signal is enabled. */
    if( s->enabled == 0 )
    {
        ReleaseMutex( s->mutex );
        return -1;
    }

    ReleaseMutex( s->mutex );   
    
    /* Wait for the signal event. */
    r = WaitForSingleObject( s->event, to );

    /* Lock signal mutex again. */
    WaitForSingleObject( s->mutex, INFINITE );
    
    /* Got signal event, get status and enabled. */
    status  = s->status;
    enabled = s->enabled;

    /* Reset the signal event in case it was signalled ok. */
    if( r == WAIT_OBJECT_0 )
    {
        ResetEvent( s->event );
    }

    /* Release signal mutex and return wait status. */
    ReleaseMutex( s->mutex );   

    if( enabled == 0 )
    {
        return -1;
    }
    
    if( r == WAIT_TIMEOUT )
    {
        return 0;
    }
    else if( r != WAIT_OBJECT_0 )
    {
        SCT_ASSERT_ALWAYS( 0 );
        return -1;
    }   
    
    return status;
}

/*!
 *
 *
 */
void siCommonTriggerSignal( void*, void* signal )
{
    SCTWinMoSignal* s;
    
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTWinMoSignal* )( signal );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonTriggerSignal %x", signal );
#endif  /* defined( SCT_DEBUG ) */
    
    /* Lock signal mutex */
    WaitForSingleObject( s->mutex, INFINITE );
    
    if( s->enabled != 0 )
    {
        s->status = 1;
        SetEvent( s->event );
    }

    /* Release signal mutex. */
    ReleaseMutex( s->mutex );   
}

/*!
 *
 *
 */
void siCommonCancelSignal( void*, void* signal )
{
    SCTWinMoSignal* s;
    
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTWinMoSignal* )( signal );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonCancelSignal %x", signal );
#endif  /* defined( SCT_DEBUG ) */
    
    /* Lock signal mutex */
    WaitForSingleObject( s->mutex, INFINITE );

    /* Set signal as disabled. */
    s->status  = -1;
    s->enabled = 0;

    /* Wake up all threads waiting for the signal event. Also set the event
     * for threads just about to start the wait. */
    PulseEvent( s->event );
    SetEvent( s->event );
    
    /* Release signal mutex. */
    ReleaseMutex( s->mutex );
} 

/*!
 *
 *
 */
SCTModuleList* siCommonGetModules( void* )
{
    /* No WinMo specific modules. */
    return sctGetCoreModules();   
}

/*!
 *
 *
 */
void siCommonSleep( void*, unsigned long )
{
    /* TODO */
}

/*!
 *
 *
 */
void siCommonYield( void* )
{
    /* TODO */   
}

/*!
 *
 *
 */
void siCommonGetTime( void*, char* buffer, int length )
{
/*
    time_t      ltime;
    struct tm*  today;

    time( &ltime );
    today = localtime( &ltime );
    if( strftime( buffer, length, "%Y-%m-%d %H:%M:%S", today ) == 0 )
    {
        strncpy( buffer, "Error", length );
    }
*/
  
    strncpy( buffer, "Not implemented", length );
}

/*!
 *
 *
 */
SCTExtensionProc siCommonGetProcAddress( void* , const char* procname )
{
#if defined( INCLUDE_EGL_MODULE )
    return ( SCTExtensionProc )( sctEglGetProcAddress( procname ) );
#else   /* defined( INCLUDE_EGL_MODULE ) */
	SCT_USE_VARIABLE( procname );
    return NULL;
#endif  /* defined( INCLUDE_EGL_MODULE ) */
}

/*!
 *
 *
 */
int siCommonGetMessage( void* context, SCTMessage* message, long timeoutMillis )
{
    SCT_USE_VARIABLE( context );
    SCT_ASSERT_ALWAYS( message != NULL );
    SCT_USE_VARIABLE( timeoutMillis );
    
    return -1;
}

/* ************************************************************ */
/* ************************************************************ */
/* ************************************************************ */

/*!
 *
 *
 */
static const char* getCPUInfo( void )
{
	SYSTEM_INFO sysinfo;

	GetSystemInfo( &sysinfo );

	switch( sysinfo.dwProcessorType )
	{
	case PROCESSOR_INTEL_386:
		return "Intel 386";

	case PROCESSOR_INTEL_486:
		return "Intel 486";

	case PROCESSOR_INTEL_PENTIUM:
		return "Intel Pentium";

	case PROCESSOR_INTEL_PENTIUMII:
		return "Intel Pentium II";

	case PROCESSOR_MIPS_R4000:
		return "MIPS R4000";

	case PROCESSOR_MIPS_R5000:
		return "MIPS R5000";

	case PROCESSOR_HITACHI_SH3:
		return "Hitachi SH3";

	case PROCESSOR_SHx_SH3DSP:
		return "Hitachi SH3-DSP";

	case PROCESSOR_HITACHI_SH4:
		return "Hitachi SH4";

	case PROCESSOR_STRONGARM:
		return "StrongArm";

	case PROCESSOR_ARM720 :
		return "ARM720";

	default:
		return "<Unknown>";
	}
}

/*!
 *
 *
 */
static const char* getOsInfo( void ) 
{ 
    OSVERSIONINFO   osvi; 
    static char     info[ 1024 ]    = "<Unknown>";

    ZeroMemory( &osvi, sizeof( OSVERSIONINFO ) ); 
    osvi.dwOSVersionInfoSize = sizeof( OSVERSIONINFO ); 

    if( !GetVersionEx( ( OSVERSIONINFO* )( &osvi ) ) ) 
    {
		return NULL; 
    }

    if( osvi.dwPlatformId == VER_PLATFORM_WIN32_CE )
	{
        sprintf( info, "Microsoft Windows CE version %d.%d %s(Build %d)", 
                        osvi.dwMajorVersion, 
                        osvi.dwMinorVersion, 
                        osvi.szCSDVersion, 
                        osvi.dwBuildNumber ); 
    }

    return info;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
void siCommonProfileEvent( void* context, const char* event )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( event );
    /* TODO */
}

