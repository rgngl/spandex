/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "sct_sicommon.h"
#include "sct_siwindow.h"

#include <jni.h>
#include <EGL/egl.h>

/*!
 * TODO: edit to match the platform.
 *
 */
#include <dlfcn.h>

/*!
 * TODO: edit to match the platform.
 *
 */
typedef EGLNativeWindowType ( *PFNandroid_createDisplaySurface )( void );
typedef void ( *PFNandroid_destroyDisplaySurface )( EGLNativeWindowType surface );

/*!
 *
 *
 */
typedef struct
{
    /* TODO: edit to match the platform. */
    void*                               libHandle;
    PFNandroid_createDisplaySurface     android_createDisplaySurface;
    PFNandroid_destroyDisplaySurface    android_destroyDisplaySurface;
} SCTAndroidSiWindowContext;

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreateContext()
{
    SCTAndroidSiWindowContext* context;
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext start" );
#endif  /* defined( SCT_DEBUG ) */

    context = ( SCTAndroidSiWindowContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTAndroidSiWindowContext ) ) );
    if( context == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext allocation failed" );
#endif  /* defined( SCT_DEBUG ) */
        return NULL;
    }
    memset( context, 0, sizeof( SCTAndroidSiWindowContext ) );

    /* TODO: edit to match the platform. */   
    context->libHandle = dlopen( "/system/lib/libui.so", RTLD_NOW );
    if( context->libHandle == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext getting libui.so handle failed" );
#endif  /* defined( SCT_DEBUG ) */        
        
        siWindowDestroyContext( context );
        return NULL;
    }

    /* TODO: edit to match the platform. */   
    context->android_createDisplaySurface = ( PFNandroid_createDisplaySurface )( dlsym( context->libHandle, "android_createDisplaySurface" ) );   
    if( context->android_createDisplaySurface == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext getting android_createDisplaySurface from libui.so handle failed" );
#endif  /* defined( SCT_DEBUG ) */        
        
        siWindowDestroyContext( context );
        return NULL;
    }

    /* TODO: edit to match the platform. */   
    context->android_destroyDisplaySurface = ( PFNandroid_destroyDisplaySurface )( dlsym( context->libHandle, "android_destroyDisplaySurface" ) );   
    if( context->android_destroyDisplaySurface == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext getting android_destroyDisplaySurface from libui.so handle failed" );
#endif  /* defined( SCT_DEBUG ) */        
        
        siWindowDestroyContext( context );
        return NULL;
    }
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext done" );
#endif  /* defined( SCT_DEBUG ) */
    
    return context;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyContext( void* context )
{
    SCTAndroidSiWindowContext* windowContext;

    windowContext = ( SCTAndroidSiWindowContext* )( context );
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyContext start" );
#endif  /* defined( SCT_DEBUG ) */

    /* TODO: edit to match the platform. */   
    if( windowContext != NULL )
    {
        if( windowContext->libHandle )
        {
            dlclose( windowContext->libHandle );
            windowContext->libHandle = NULL;
        }
        
        siCommonMemoryFree( NULL, windowContext );
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyContext done" );
#endif  /* defined( SCT_DEBUG ) */       
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowIsScreenSupported( void* context, SCTScreen screen )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowIsScreenSupported" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );

    if( screen != SCT_SCREEN_DEFAULT )
    {
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowGetScreenSize( void* context, SCTScreen screen, int* width, int* height )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetScreenSize" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( screen );
    SCT_ASSERT_ALWAYS( width != NULL );
    SCT_ASSERT_ALWAYS( height != NULL );
    
    *width  = 0;
    *height = 0;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetScreenEglDisplayId( void* context, SCTScreen screen )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetScreenEglDisplayId" );
#endif  /* defined( SCT_DEBUG ) */

    /* TODO: edit to match the platform. */
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( screen );
    
    return 0;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetScreenOrientation( void* context, SCTScreen screen, SCTScreenOrientation orientation )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetScreenOrientation" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( screen );
    SCT_USE_VARIABLE( orientation );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreateWindow( void* context, SCTScreen screen, int x, int y, int width, int height, SCTColorFormat format, void* parent )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow start" );
#endif  /* defined( SCT_DEBUG ) */

    SCTAndroidSiWindowContext*  c;
    EGLNativeWindowType         window;

    SCT_ASSERT_ALWAYS( context != NULL ); 
    SCT_USE_VARIABLE( screen );
    SCT_USE_VARIABLE( x );
    SCT_USE_VARIABLE( y );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( parent );

    c = ( SCTAndroidSiWindowContext* )( context );

#if defined( SCT_DEBUG )    
    siCommonDebugPrintf( NULL, "SPANDEX: calling android_createDisplaySurface" );
#endif  /* defined( SCT_DEBUG ) */

    /* TODO: edit to match the platform. */
    window = ( EGLNativeWindowType )( c->android_createDisplaySurface() );

#if defined( SCT_DEBUG )    
    siCommonDebugPrintf( NULL, "SPANDEX: android_createDisplaySurface returned %p", ( void* )( window ) );
#endif  /* defined( SCT_DEBUG ) */

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow done" );
#endif  /* defined( SCT_DEBUG ) */
    
    return window;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyWindow( void* context, void* window )
{
    SCTAndroidSiWindowContext*  c = ( SCTAndroidSiWindowContext* )( context );
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyWindow start" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( window != NULL );

#if defined( SCT_DEBUG )    
    siCommonDebugPrintf( NULL, "SPANDEX: calling android_destroyDisplaySurface" );
#endif  /* defined( SCT_DEBUG ) */

    /* TODO: edit to match the platform. */
    c->android_destroyDisplaySurface( ( EGLNativeWindowType )( window ) );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyWindow done" );
#endif  /* defined( SCT_DEBUG ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetEglWindow( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetEglWindow" );
#endif  /* defined( SCT_DEBUG ) */

    /* TODO: edit to match the platform. */
    SCT_ASSERT_ALWAYS( context != NULL );
    return window;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTScreen siWindowGetWindowScreen( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetWindowScreen" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( window );

    return SCT_SCREEN_DEFAULT;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowToFront( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowToFront" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( window );
    
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowResize( void* context, void* window, int x, int y, int width, int height )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowResize" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( window );
    
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetOpacity( void* context, void* window, float opacity )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetOpacity" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( window );
    SCT_USE_VARIABLE( opacity );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetAlpha( void* context, void* window, SCTBoolean enable )
{
 #if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetAlpha" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( window );
    SCT_USE_VARIABLE( enable );

    return SCT_FALSE;    
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowRefresh( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowRefresh" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( window );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreatePixmap( void* context, int width, int height, SCTColorFormat format, void* data, int length )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( length );
    
    return NULL;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreateSharedPixmap( void* context, int width, int height, SCTColorFormat format, int usage, void* data, int length )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( usage );
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( length );

    return NULL;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetEglPixmap( void* context, void* pixmap )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetEglPixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    return pixmap;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowBlitPixmap( void* context, void* window, void* pixmap, int x, int y )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowBlitPixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( window );
    SCT_USE_VARIABLE( pixmap );
    SCT_USE_VARIABLE( x );
    SCT_USE_VARIABLE( y );
    
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyPixmap( void* context, void* pixmap )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyPixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( pixmap );
}
