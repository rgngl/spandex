#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := spandex

#LOCAL_CFLAGS += -DSCT_DEBUG

LOCAL_C_INCLUDES +=                                              \
    inc                                                          \
    sct/common/include                                           \
    sct/common/modules/cpu                                       \
    sct/common/modules/memory                                    \
    sct/common/modules/thread                                    \
    sct/common/modules/test                                      \
    sct/common/modules/egl                                       \
    sct/common/modules/opengles2                                 \
    sct/common/modules/gltest                                    \
    sct/common/modules/images                                    \

LOCAL_CFLAGS +=                                                  \
	-DSCT_KHR_COMPLIANT_INCLUDES                                 \
    -DINCLUDE_CPU_MODULE                                         \
    -DINCLUDE_MEMORY_MODULE                                      \
    -DINCLUDE_THREAD_MODULE                                      \
    -DINCLUDE_TEST_MODULE                                        \
    -DINCLUDE_EGL_MODULE                                         \
    -DINCLUDE_OPENGLES2_MODULE                                   \
    -DINCLUDE_GLTEST_MODULE                                      \
    -DINCLUDE_IMAGES_MODULE                                      \

LOCAL_SRC_FILES :=                                               \
	main.c                                                       \
	android_sicommon.c                                           \
	android_siwindow_native.cpp                                  \
    unix_sicommon.c                                              \
	unix_sifile.c                                                \
	unix_simemory.c                                              \
    $(shell find sct/common/core -name "*.c")                    \
    $(shell find sct/common/modules/cpu/ -name "*.c")            \
    $(shell find sct/common/modules/memory/ -name "*.c")         \
    $(shell find sct/common/modules/thread/ -name "*.c")         \
    $(shell find sct/common/modules/test/ -name "*.c")           \
    $(shell find sct/common/modules/egl -name "*.c")             \
	$(shell find sct/common/modules/opengles2 -name "*.c")       \
    $(shell find sct/common/modules/gltest -name "*.c")          \
    $(shell find sct/common/modules/images/ -name "*.c")

LOCAL_ARM_MODE   := arm

LOCAL_LDFLAGS += \
	-Wl,--allow-shlib-undefined \
	-Lobj/local/$(TARGET_ARCH_ABI)/

# TODO: add platform-specific lib files to LOCAL_LDFLAGS
# TODO: add platfornm-specific includes to LOCAL_C_INCLUDES

LOCAL_LDLIBS     +=  -lstdc++ -lc -lm -llog -ldl -lGLESv2 -lEGL -landroid 

include $(BUILD_EXECUTABLE)


