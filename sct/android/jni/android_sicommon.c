/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <unistd.h>
#include <android/log.h>

/* Includes from Spandex core */
#include "sct.h"
#include "sct_sicommon.h"

#include "unix_globals.h"
#include "spandex_android_profile.h"

/*!
 *
 *
 */
struct _SCTCpuSample
{
    int     pidTicks;
    int     idleTicks;
    int     busyTicks;
};

typedef struct _SCTCpuSample SCTCpuSample;

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static int      _getPidTicks( pid_t pid );
static int      _getTotalTicks( int* idle, int* busy );

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
const char* siCommonQuerySystemString( void* context, SCTSystemString name )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );

    SCT_USE_VARIABLE( context );
    
    switch( name )
    {
    case SCT_SYSSTRING_DEVICE_TYPE:
        return "Android";

    case SCT_SYSSTRING_CPU:
        return "<Unknown>";
        
    case SCT_SYSSTRING_OS:
        return "Android";
        
    default:
        SCT_ASSERT_ALWAYS( 0 );
        return NULL;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
size_t siCommonQueryMemoryAttribute( void* context, SCTMemoryAttribute name )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );

    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( name );    
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( context, "SPANDEX: siCommonQueryMemoryAttributeImpl not implemented" );
#endif  /* defined( SCT_DEBUG ) */
    
    return 0;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siCommonSampleCpuLoad( void* context )
{
    SCTCpuSample*   s;

    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );

    s = ( SCTCpuSample* )( siCommonMemoryAlloc( NULL, sizeof( SCTCpuSample ) ) );
    if( s == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( context, "SPANDEX: siCommonSampleCpuLoad allocation failed" );
#endif  /* defined( SCT_DEBUG ) */

        return NULL;
    }
    memset( s, 0, sizeof( SCTCpuSample ) );

    s->pidTicks = _getPidTicks( getpid() );

    if( s->pidTicks < 0 || _getTotalTicks( &( s->idleTicks ), &( s->busyTicks ) ) < 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( context, "SPANDEX: siCommonSampleCpuLoad getting ticks failed" );
#endif  /* defined( SCT_DEBUG ) */

        siCommonDestroyCpuLoadSample( context, s );
        return NULL;
    }

#if defined( SCT_DEBUG_PRINT_CPU_LOAD )
    siCommonDebugPrintf( context, "SPANDEX: siCommonSampleCpuLoad pid=%lu, idle=%lu, busy=%lu", s->pidTicks, s->idleTicks, s->busyTicks );
#endif  /* defined( SCT_DEBUG ) */

    return s;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
float siCommonCalculateCpuLoad( void* context, void* sample, SCTCpuLoad load )
{
    SCTCpuSample*   now;
    SCTCpuSample*   then;
    float           div;
    float           r;

    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    SCT_ASSERT_ALWAYS( sample != NULL );

    then = ( SCTCpuSample* )( sample );
    now  = ( SCTCpuSample* )( siCommonSampleCpuLoad( context ) );

    if( now == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( context, "SPANDEX: siCommonCalculateCpuLoad getting a cpu sample failed" );
#endif  /* defined( SCT_DEBUG ) */

        return -1.0f;
    }

#if defined( SCT_DEBUG_PRINT_CPU_LOAD )
    siCommonDebugPrintf( context,
                         "SPANDEX: siCommonCalculateCpuLoad pid delta=%lu, idle delta=%lu, busy delta=%lu",
                         now->pidTicks  - then->pidTicks,
                         now->idleTicks - then->idleTicks,
                         now->busyTicks - then->busyTicks );
#endif  /* defined( SCT_DEBUG ) */


    if( load == SCT_CPULOAD_PROCESS2BUSY )
    {
        div = ( float )( now->busyTicks - then->busyTicks );

        if( div != 0.0f )
        {
            r = ( float )( now->pidTicks - then->pidTicks ) / div;
        }
        else
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( context, "SPANDEX: siCommonCalculateCpuLoad too fine-grained sampling" );
#endif  /* defined( SCT_DEBUG ) */

            /* Too fine-grained sampling; return full load. */
            r = 1.0f;
        }
    }
    else if( load == SCT_CPULOAD_PROCESS2TOTAL )
    {
        div = ( float )( ( now->idleTicks + now->busyTicks ) - ( then->idleTicks + then->busyTicks ) );

        if( div != 0.0f )
        {
            r = ( float )( now->pidTicks - then->pidTicks ) / div;
        }
        else
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( context, "SPANDEX: siCommonCalculateCpuLoad too fine-grained sampling" );
#endif  /* defined( SCT_DEBUG ) */

            /* Too fine-grained sampling; return full load. */
            r = 1.0f;
        }
    }
    else if( load == SCT_CPULOAD_SYSTEM )
    {
        div = ( float )( ( now->idleTicks + now->busyTicks ) - ( then->idleTicks + then->busyTicks ) );

        if( div != 0.0f )
        {
            r = ( float )( now->busyTicks - then->busyTicks ) / div;
        }
        else
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( context, "SPANDEX: siCommonCalculateCpuLoad too fine-grained sampling" );
#endif  /* defined( SCT_DEBUG ) */

            /* Too fine-grained sampling; return full load. */
            r = 1.0f;
        }
    }
    else
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( context, "SPANDEX: siCommonCalculateCpuLoad unexpected cpu load type" );
#endif  /* defined( SCT_DEBUG ) */

        r = -1.0f;
    }

    siCommonDestroyCpuLoadSample( context, now );

    return r;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonDestroyCpuLoadSample( void* context, void* sample )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    SCT_ASSERT_ALWAYS( sample != NULL );

    siCommonMemoryFree( NULL, sample );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonDebugPrintf( void* context, const char* fmt, ... )
{
	va_list ap;
    char    buf[ 2048 ];

    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    SCT_ASSERT_ALWAYS( fmt != NULL );

	va_start( ap, fmt );
	vsnprintf( buf, sizeof( buf ) - 2, fmt, ap );
	va_end( ap );

    strcat( buf, "\n" );
    
    __android_log_print( ANDROID_LOG_INFO, "Spandex", "%s", buf );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonYield( void* context )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    /* Do nothing. */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */ 
void siCommonProfileEvent( void* context, const char* event )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    SPANDEX_PROFILE_EVENT( event );
}

/* ---------------------------------------------------------------------- */
/* ---------------------------------------------------------------------- */
/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static int _getTotalTicks( int* idle, int* busy )
{
    FILE*   fp;
    char    line[ 512 ];
    size_t  e;

    SCT_ASSERT( idle != NULL );
    SCT_ASSERT( busy != NULL );

    fp = fopen( "/proc/stat", "r" );
    if( fp == NULL )
    {
        return -1;
    }

    e = fread( line, sizeof( line ), 1, fp );
    ( void )( e );  /* Supress a bit too pedantic gcc warning. */
    fclose( fp );

    line[ sizeof( line ) - 1 ] = '\0';

    *idle = 0;
    *busy = 0;

    strtok( line, " " );                    /* cpu */
    *busy += atoi( strtok( NULL, " " ) );   /* user */
    *busy += atoi( strtok( NULL, " " ) );   /* nice */
    *busy += atoi( strtok( NULL, " " ) );   /* system */

    *idle += atoi( strtok( NULL, " " ) );   /* idle */
    *idle += atoi( strtok( NULL, " " ) );   /* iowait */

    *busy += atoi( strtok( NULL, " " ) );   /* irq */
    *busy += atoi( strtok( NULL, " " ) );   /* softirq */

    return 0;
}

/*!
 *
 *
 */
static int _getPidTicks( pid_t pid )
{
    FILE*   fp;
    char    fileName[ 64 ];
    char    line[ 512 ];
    int     i;
    size_t  e;

    sprintf( fileName, "/proc/%d/stat", pid );

    fp = fopen(fileName, "r");
    if( fp == NULL )
    {
        return -1;
    }

    e = fread( line, sizeof( line ), 1, fp );
    ( void )( e );  /* Supress a bit too pedantic gcc warning. */
    fclose( fp );

    line[ sizeof( line ) - 1 ] = '\0';

    strtok( line, " " );
    for( i = 0; i < 12; ++i )
    {
        strtok( NULL, " " );
    }

    return atoi( strtok( NULL, " " ) ) + atoi( strtok( NULL, " " ) );
}
 
