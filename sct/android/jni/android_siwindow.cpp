/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "sct_sicommon.h"
#include "sct_siwindow.h"

#include "spandexmain.h"

#include <jni.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#if defined( SCT_ANDROID_FULL_SOURCE )
# include <ui/GraphicBuffer.h>
using namespace android;
#endif  /* defined( SCT_ANDROID_FULL_SOURCE )*/

/*!
 * Global variables.
 *
 */
EGLNativeWindowType     gNativeWindow       = 0;
jfieldID                gSurfaceFieldID     = 0;

/*!
 *
 *
 */
#define SCT_SIWINDOW_ANDROID_CONTEXT    0x1234

/*!
 *
 *
 */
static int32_t          _getPixmapFormat( SCTColorFormat format );
static uint32_t         _getPixmapUsage( int usage );

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreateContext()
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext" );
#endif  /* defined( SCT_DEBUG ) */

    return ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT );    
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyContext( void* context )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyContext" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowIsScreenSupported( void* context, SCTScreen screen )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowIsScreenSupported" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );

    if( screen != SCT_SCREEN_DEFAULT )
    {
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowGetScreenSize( void* context, SCTScreen screen, int* width, int* height )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetScreenSize" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_USE_VARIABLE( screen );
    SCT_ASSERT_ALWAYS( width != NULL );
    SCT_ASSERT_ALWAYS( height != NULL );
    
    *width  = 0;
    *height = 0;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetScreenEglDisplayId( void* context, SCTScreen screen )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetScreenEglDisplayId" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_USE_VARIABLE( screen );
    
    return 0;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetScreenOrientation( void* context, SCTScreen screen, SCTScreenOrientation orientation )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetScreenOrientation" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_USE_VARIABLE( screen );
    SCT_USE_VARIABLE( orientation );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreateWindow( void* context, SCTScreen screen, int x, int y, int width, int height, SCTColorFormat format, void* parent )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_USE_VARIABLE( screen );
    SCT_USE_VARIABLE( x );
    SCT_USE_VARIABLE( y );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( parent );

    SCT_ASSERT_ALWAYS( gNativeWindow != 0 );
    return gNativeWindow;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyWindow( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyWindow" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_ASSERT_ALWAYS( window == ( void* )( gNativeWindow ) );    
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetEglWindow( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetEglWindow" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );    
    return window;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTScreen siWindowGetWindowScreen( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetWindowScreen" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_USE_VARIABLE( window );

    return SCT_SCREEN_DEFAULT;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowToFront( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowToFront" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_USE_VARIABLE( window );
    
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowResize( void* context, void* window, int x, int y, int width, int height )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowResize" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_USE_VARIABLE( window );
    
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetOpacity( void* context, void* window, float opacity )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetOpacity" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_USE_VARIABLE( window );
    SCT_USE_VARIABLE( opacity );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetAlpha( void* context, void* window, SCTBoolean enable )
{
 #if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetAlpha" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_USE_VARIABLE( window );
    SCT_USE_VARIABLE( enable );

    return SCT_FALSE;    
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowRefresh( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowRefresh" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_USE_VARIABLE( window );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreatePixmap( void* context, int width, int height, SCTColorFormat format, void* data, int length )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( length );
    
    return NULL;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreateSharedPixmap( void* context, int width, int height, SCTColorFormat format, int usage, void* data, int length )
{
#if defined( SCT_ANDROID_FULL_SOURCE )
    GraphicBuffer*  buffer;
    status_t        err             = 0;
    int32_t         pixelFormat;
    void*           bdata           = NULL;
#endif  /* defined( SCT_ANDROID_FULL_SOURCE ) */

#if defined( SCT_DEBUG )
   siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );

#if defined( SCT_ANDROID_FULL_SOURCE )
    SCT_USE_VARIABLE( usage );

    pixelFormat = _getPixmapFormat( format );
    if( pixelFormat < 0 )
    {
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap, unsupported format, format=%d", format );
        return NULL;
    }

    buffer = new GraphicBuffer( width,
                                height,
                                pixelFormat,
                                _getPixmapUsage( usage ) );

    if( buffer != NULL )
    {
        err = buffer->initCheck();
    }

    if( buffer == NULL || err != NO_ERROR )
    {
       siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap, creating GraphicBuffer failed, err=%d", err );
       return NULL;
    }
    
    if( data != NULL )
    {
        buffer->lock( GRALLOC_USAGE_SW_WRITE_OFTEN, &bdata );

        if( bdata == NULL )
        {
            buffer->unlock();
            /* TODO: there is no way to destroy a graphic buffer? */
            //delete buffer;
            return NULL;
        }

#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap, copy data" );
#endif  /* defined( SCT_DEBUG ) */

        memcpy( bdata, data, length );
        buffer->unlock();
    }

    return buffer;
#else   /* defined( SCT_ANDROID_FULL_SOURCE ) */
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( usage );
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( length );

    return NULL;
#endif  /* defined( SCT_ANDROID_FULL_SOURCE ) */ 
}

/* ---------------------------------------------------------------------- */
/*!
 * 
 *
 */
SCTBoolean siWindowSetPixmapData( void* context, void* pixmap, void* data, int length )
{
#if defined( SCT_ANDROID_FULL_SOURCE )
    GraphicBuffer*  buffer;
    void*           bdata           = NULL;
#endif  /* defined( SCT_ANDROID_FULL_SOURCE ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );
    SCT_ASSERT_ALWAYS( pixmap != NULL );
    SCT_ASSERT_ALWAYS( data != NULL );
    SCT_ASSERT_ALWAYS( length > 0 );

#if defined( SCT_ANDROID_FULL_SOURCE )
    buffer = ( GraphicBuffer* )( pixmap );

    buffer->lock( GRALLOC_USAGE_SW_WRITE_OFTEN, &bdata );

    if( bdata == NULL )
    {
        buffer->unlock();
        return SCT_FALSE;
    }

    memcpy( bdata, data, length );
    buffer->unlock();

    return SCT_TRUE;
#else   /* defined( SCT_ANDROID_FULL_SOURCE ) */
    return SCT_FALSE;
#endif  /* defined( SCT_ANDROID_FULL_SOURCE ) */ 
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetEglPixmap( void* context, void* pixmap, unsigned int* target )
{
#if defined( SCT_ANDROID_FULL_SOURCE )
    GraphicBuffer*  buffer;
#endif  /* defined( SCT_ANDROID_FULL_SOURCE ) */

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetEglPixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );

#if defined( SCT_ANDROID_FULL_SOURCE )
    buffer = ( GraphicBuffer* )( pixmap );
    if( target != NULL )
    {
        *target = ( unsigned int )( EGL_NATIVE_BUFFER_ANDROID );
    }
    
    return buffer->getNativeBuffer();
#else   /* defined( SCT_ANDROID_FULL_SOURCE ) */
    SCT_USE_VARIABLE( pixmap );
    SCT_USE_VARIABLE( target );

    return NULL;
#endif  /* defined( SCT_ANDROID_FULL_SOURCE ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowBlitPixmap( void* context, void* window, void* pixmap, int x, int y )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowBlitPixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );    
    SCT_USE_VARIABLE( window );
    SCT_USE_VARIABLE( pixmap );
    SCT_USE_VARIABLE( x );
    SCT_USE_VARIABLE( y );
    
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyPixmap( void* context, void* pixmap )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyPixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT( context == ( void* )( SCT_SIWINDOW_ANDROID_CONTEXT ) );    

#if defined( SCT_ANDROID_FULL_SOURCE )
     /* TODO: there is no way to destroy a graphic buffer? */
     SCT_USE_VARIABLE( pixmap );
     //delete ( GraphicBuffer* )( pixmap );
#else   /* define( SCT_ANDROID_FULL_SOURCE ) */
    SCT_USE_VARIABLE( pixmap );
#endif  /* define( SCT_ANDROID_FULL_SOURCE ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static int32_t _getPixmapFormat( SCTColorFormat format )
{
#if defined( SCT_ANDROID_FULL_SOURCE )
    switch( format )
    {
        case SCT_COLOR_FORMAT_RGBA5551:
            return HAL_PIXEL_FORMAT_RGBA_5551;

        case SCT_COLOR_FORMAT_RGBA4444:
            return HAL_PIXEL_FORMAT_RGBA_4444;

        case SCT_COLOR_FORMAT_RGB565:
            return HAL_PIXEL_FORMAT_RGB_565;

        case SCT_COLOR_FORMAT_RGB888:
            return HAL_PIXEL_FORMAT_RGB_888;

        case SCT_COLOR_FORMAT_RGBX8888:
            return HAL_PIXEL_FORMAT_RGBX_8888;

        case SCT_COLOR_FORMAT_DEFAULT:      /* Intentional. */
        case SCT_COLOR_FORMAT_RGBA8888:
            return HAL_PIXEL_FORMAT_RGBA_8888;

        case SCT_COLOR_FORMAT_BGRA8888:
            return HAL_PIXEL_FORMAT_BGRA_8888;

        case SCT_COLOR_FORMAT_YUV12:
            return HAL_PIXEL_FORMAT_YV12;

        case SCT_COLOR_FORMAT_INVALID:      /* Intentional. */
        case SCT_COLOR_FORMAT_L8:           /* Intentional. */
        case SCT_COLOR_FORMAT_A8:           /* Intentional. */
        case SCT_COLOR_FORMAT_LA88:         /* Intentional. */
        case SCT_COLOR_FORMAT_RGBA8888_PRE: /* Intentional. */
            return -1;
    }
#else   /* defined( SCT_ANDROID_FULL_SOURCE ) */    
    return -1;
#endif  /* defined( SCT_ANDROID_FULL_SOURCE ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static uint32_t _getPixmapUsage( int usage )
{
    SCT_USE_VARIABLE( usage );

#if defined( SCT_ANDROID_FULL_SOURCE )
    return GRALLOC_USAGE_HW_TEXTURE |
           GRALLOC_USAGE_HW_RENDER |
           GRALLOC_USAGE_HW_2D |
           GRALLOC_USAGE_SW_READ_OFTEN |
           GRALLOC_USAGE_SW_WRITE_OFTEN; 
#else   /* defined( SCT_ANDROID_FULL_SOURCE ) */
    return 0;
#endif  /* defined( SCT_ANDROID_FULL_SOURCE ) */
}

/* ---------------------------------------------------------------------- */
/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
JNIEXPORT jint JNICALL Java_org_gitorious_spandex_Spandex_spandexInit( JNIEnv* env, jobject __unref obj, jstring wrapLib )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: native spandexInit" );
#endif  /* defined( SCT_DEBUG ) */
    
    gSurfaceFieldID = env->GetFieldID( env->FindClass( "android/view/Surface" ), "mNativeSurface", "I" );

    return 0;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
JNIEXPORT jint JNICALL Java_org_gitorious_spandex_Spandex_spandexMain( JNIEnv* env, jobject __unref obj, jobject eglNativeWindow )
{
    
    FILE* freo = freopen( "/sdcard/spandex-stderr.txt", "w", stderr );
    FILE* greo = freopen( "/sdcard/spandex-stdout.txt", "w", stdout );
    char  argv[ 1 ][ 16 ] = { "programname" };

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: native spandexMain" );
#endif  /* defined( SCT_DEBUG ) */
    
    gNativeWindow = ( EGLNativeWindowType )( env->GetIntField( eglNativeWindow, gSurfaceFieldID ) + 8 );

    return spandexMain( 1, ( char** )( argv ) );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
JNIEXPORT void JNICALL Java_org_gitorious_spandex_Spandex_spandexResizeWindow( JNIEnv __unref* env, jobject __unref obj, int width, int height )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: native spandexResizeWindow" );
#endif  /* defined( SCT_DEBUG ) */
    
    /* Do nothing. */
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
JNIEXPORT void JNICALL Java_org_gitorious_spandex_Spandex_spandexQuit( JNIEnv __unref* env, jobject __unref obj )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: native spandexQuit" );
#endif  /* defined( SCT_DEBUG ) */
   
    exit( 0 );
}

