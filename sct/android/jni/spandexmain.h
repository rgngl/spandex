/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SPANDEXMAIN_H__ )
#define __SPANDEXMAIN_H__

#include <jni.h>

#define __unref __attribute__( ( unused ) )

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    JNIEXPORT jint JNICALL  Java_org_gitorious_spandex_Spandex_spandexInit( JNIEnv* env, jobject __unref obj, jstring wrapLib );
    JNIEXPORT jint JNICALL  Java_org_gitorious_spandex_Spandex_spandexMain( JNIEnv* env, jobject __unref obj, jobject eglNativeWindow );
    JNIEXPORT void JNICALL  Java_org_gitorious_spandex_Spandex_spandexResizeWindow( JNIEnv __unref* env, jobject __unref obj, int width, int height );
    JNIEXPORT void JNICALL  Java_org_gitorious_spandex_Spandex_spandexQuit( JNIEnv __unref* env, jobject __unref obj );
    
    int                     spandexMain( int argc, char** argv );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SPANDEXMAIN_H__ )  */
