/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2013 Contribute.
 *
 * Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SPANDEX_ANDROID_PROFILE_H )
#define __SPANDEX_ANDROID_PROFILE_H

/* #define SPANDEX_ANDROID_PROFILE */
 
# if defined( SPANDEX_ANDROID_PROFILE )

#  include <time.h>
#  include <sys/time.h>
#  include <sys/types.h>
#  include <cutils/log.h>

extern int sched_getcpu(void);

# if !defined( LOG_TAG )
#  define LOG_TAG               "Spandex"
# endif /* !defined( LOG_TAG ) */

# define SPANDEX_LOG( ... )     ( ( void ) ( __android_log_print( ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__ ) ) ) 

#  define SPANDEX_PROFILE_START() \
{\
struct timespec now;\
\
clock_gettime( CLOCK_MONOTONIC, &now );\
SPANDEX_LOG( "SPANDEX_PROFILE %d %u %u %lu %lu START \"%s:%d\"", sched_getcpu(), ( unsigned int )( getpid() ), ( unsigned int )( gettid() ), now.tv_sec,  now.tv_nsec, __func__, __LINE__ );\
}

#  define SPANDEX_PROFILE_END() \
{\
struct timespec now;\
\
clock_gettime( CLOCK_MONOTONIC, &now );\
SPANDEX_LOG( "SPANDEX_PROFILE %d %u %u %lu %lu END \"%s:%d\"", sched_getcpu(), ( unsigned int )( getpid() ), ( unsigned int )( gettid() ), now.tv_sec,  now.tv_nsec, __func__, __LINE__ );\
}

#  define SPANDEX_PROFILE_EVENT( event ) \
{\
struct timespec now;\
\
clock_gettime( CLOCK_MONOTONIC, &now );\
SPANDEX_LOG( "SPANDEX_PROFILE %d %u %u %lu %lu %s \"%s:%d\"", sched_getcpu(), ( unsigned int )( getpid() ), ( unsigned int )( gettid() ), now.tv_sec, now.tv_nsec, ( event ), __func__, __LINE__ );\
}

#  define SPANDEX_PROFILE_EVENTI( event, i ) \
{\
struct timespec now;\
\
clock_gettime( CLOCK_MONOTONIC, &now );\
SPANDEX_LOG( "SPANDEX_PROFILE %d %u %u %lu %lu %s %d \"%s:%d\"", sched_getcpu(), ( unsigned int )( getpid() ), ( unsigned int )( gettid() ), now.tv_sec, now.tv_nsec, ( event ), ( int )( i ), __func__, __LINE__ );\
}

#  define SPANDEX_PROFILE_EVENTLU( event, lu ) \
{\
struct timespec now;\
\
clock_gettime( CLOCK_MONOTONIC, &now );\
SPANDEX_LOG( "SPANDEX_PROFILE %d %u %u %lu %lu %s %lu \"%s:%d\"", sched_getcpu(), ( unsigned int )( getpid() ), ( unsigned int )( gettid() ), now.tv_sec, now.tv_nsec, ( event ), ( unsigned long int )( lu ), __func__, __LINE__ );\
}

#  define SPANDEX_PROFILE_EVENTS( event, s ) \
{\
struct timespec now;\
\
clock_gettime( CLOCK_MONOTONIC, &now );\
SPANDEX_LOG( "SPANDEX_PROFILE %d %u %u %lu %lu %s %s \"%s:%d\"", sched_getcpu(), ( unsigned int )( getpid() ), ( unsigned int )( gettid() ), now.tv_sec, now.tv_nsec, ( event ), ( const char* )( s ), __func__, __LINE__ );\
}

# else /* defined( SPANDEX_ANDROID_PROFILE ) */

#  define SPANDEX_PROFILE_START()
#  define SPANDEX_PROFILE_END()
#  define SPANDEX_PROFILE_EVENT( event )
#  define SPANDEX_PROFILE_EVENTI( event, i )
#  define SPANDEX_PROFILE_EVENTLU( event, lu )
#  define SPANDEX_PROFILE_EVENTS( event, s )

# endif  /* defined( SPANDEX_ANDROID_PROFILE ) */


#endif  /* defined( __SPANDEX_ANDROID_PROFILE_H ) */

