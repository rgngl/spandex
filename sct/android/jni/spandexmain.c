/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <stdio.h>
#include <stdlib.h>

#include "sct.h"
#include "sct_types.h"
#include "unix_globals.h"
#include "spandexmain.h"

#include <android/log.h>

#define INPUT_FILE_NAME     "/sdcard/benchmark.txt"
#define OUTPUT_FILE_NAME    "/sdcard/result.txt"

const char*     gInputFileName  = INPUT_FILE_NAME;
const char*     gOutputFileName = OUTPUT_FILE_NAME;

/*!
 * Global variables
 */
FILE*           gInputFile      = NULL;
FILE*           gOutputFile     = NULL;

/*!
 *
 *
 */
int spandexMain( int argc, char** argv )
{
    void*           sctContext  = NULL;
    SCTCycleStatus  status;
    int             ret;

    __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX start.\n" );

    __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX opening input file: %s.\n", gInputFileName );
    gInputFile = fopen( gInputFileName, "rb" );
    if( gInputFile == NULL )
    {
        __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX failed to open input file.\n" );
        goto END;
    }

    __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX opening output file: %s.\n", gOutputFileName );
    gOutputFile = fopen( gOutputFileName, "wb+" );
    if( gOutputFile == NULL )
    {
        __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX failed to open output file.\n" );
        goto END;
    }

    __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX sctInitialize.\n" );
    sctContext = sctInitialize();

    if( sctContext == NULL )
    {
        __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX sctInitialize failed.\n" );
        goto END;
    }

    __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX benchmark cycle start.\n" );
    while( 1 )
    {
        status = sctCycle( sctContext );

        if( status == SCT_CYCLE_FATAL_ERROR )
        {
            __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX cycle fatal error.\n" );
            break;
        }
        else if( status == SCT_CYCLE_FINISHED )
        {
            break;
        }
    }

    __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX benchmark cycle end.\n" );

END:

    __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX sctTerminate.\n" );
    sctTerminate( sctContext );

    if( gInputFile != NULL )
    {
        __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX closing input file.\n" );
        fclose( gInputFile );
        gInputFile = NULL;
    }
    if( gOutputFile != NULL )
    {
        __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX closing output file.\n" );
        fclose( gOutputFile );
        gOutputFile = NULL;
    }

    __android_log_print( ANDROID_LOG_INFO, "Spandex", "SPANDEX done.\n" );
    return 0;
}
