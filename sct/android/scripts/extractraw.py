#!python
# -*- coding:utf-8 -*-
#
# Spandex benchmark and test framework.
#
# Copyright (c) 2013 Contribute.
#
# Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import fileinput
import optparse

PROFILE_TAG = 'SPANDEX_PROFILE'
START_TAG   = 'START'
END_TAG     = 'END'

# -------------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] [input file]"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--frame',
                       default = 1,
                       dest    = 'frame',
                       help    = 'Exclude a raw frame output from log [default: %default].' )

    ( options, args ) = parser.parse_args()

    inside = False
    frame  = 0 
    for line in fileinput.input( args ):
        if not inside:
            if line.find( PROFILE_TAG ) >= 0 and line.find( START_TAG ) > 0:
                frame += 1
                if frame == int( options.frame ):
                    inside = True

        if inside:
            print line,
            if line.find( PROFILE_TAG ) >= 0 and line.find( END_TAG ) > 0:
                break;

