#!/bin/sh
adb logcat -c
adb shell am start -n org.gitorious.spandex/org.gitorious.spandex.Spandex
adb logcat | python scan.py 
sleep 1
adb shell am force-stop org.gitorious.spandex
