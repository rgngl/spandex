#!python
# -*- coding:utf-8 -*-
#
# Spandex benchmark and test framework.
#
# Copyright (c) 2013 Contribute.
#
# Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import fileinput
import optparse

START_TAG   = 'START'
END_TAG     = 'END'


# -------------------------------------------------------------------
def resetCounter( c, evs ):
    for e in evs:
        counter[ e ] = {}
 

# -------------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] [input file]"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--threads',
                       default = None,
                       dest    = 'threads',
                       help    = 'Threads, separated by comma [default: %default].' )
    parser.add_option( '--events',
                       default = None,
                       dest    = 'events',
                       help    = 'Events, separated by comma [default: %default].' )

    ( options, args ) = parser.parse_args()
 
    if not options.events:
        sys.exit( 0 )

    events = options.events.split( ',' )

    threads = []
    if options.threads:
        threads = options.threads.split( ',' )
 
    counter = {}
    resetCounter( counter, events )

    m = 0
    for e in events:
        m = max( m, len( e ) )

    inside = False 
    strays = 0
    t      = 0
    for line in fileinput.input( args ):
        sline   = line[ 4 : ].split()
        index   = int( sline[ 1 ] )
        thread  = sline[ 2 ]
        name    = sline[ 7 ]

        if name == START_TAG:
            resetCounter( counter, events )
            inside  = True
            strays += t
            t       = 0
            print '%04d' % index
        elif name == END_TAG:
            inside = False
            for e in events:
                print '    %s' % ( e.rjust( m ), ), 
                tot = 0
                for k in counter[ e ].keys():
                    print '%s:%d' % ( k, counter[ e ][ k ], ),
                    tot += counter[ e ][ k ] 
                print 'tot:%d' % tot
        else:
            if not inside:
                t += 1
                continue

            if threads and thread not in threads:
                continue
    
            for e in events:
                if line.find( e ) >= 0:
                    if not counter[ e ].has_key( thread ):
                        counter[ e ][ thread ] = 1
                    else:
                        counter[ e ][ thread ] += 1

    print 'Strays: %d' % strays

