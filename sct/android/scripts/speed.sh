#!/bin/sh

CORES=$1
CPU=$2
DDR=$3
GPU=$4

#adb shell "echo 12 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_cpu_func"
#adb shell "echo 12 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_cpu_func"
#adb shell "echo 12 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_cpu_func"
adb shell "echo 1 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_ddr_mode"
adb shell "echo 1 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_cpu_mode"
adb shell "echo 1 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_ddr_mode"

adb shell "echo $CPU > /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_block_freq"
adb shell "echo 0 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_cpu_mode"

#adb shell "echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

adb shell "echo $CORES > /sys/devices/system/cpu/cpu0/cpufreq/scaling_cpu_lock_num"

adb shell "echo $DDR > /sys/devices/system/cpu/cpu0/cpufreq/scaling_ddr_block_freq"
#adb shell "echo 0 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_ddr_mode"

adb shell "echo $GPU > /sys/devices/system/cpu/cpu0/cpufreq/scaling_gpu_block_freq"
#adb shell "echo 0 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_ddr_mode"

adb shell "echo 1 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_ddr_mode"
adb shell "echo 1 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_cpu_mode"
adb shell "echo 1 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_ddr_mode"

sleep 1

adb shell "echo 0 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_ddr_mode"
adb shell "echo 0 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_cpu_mode"
adb shell "echo 0 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_ddr_mode"

adb shell "cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq"
adb shell "cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_gpu_cur_freq"
adb shell "cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_ddr_cur_freq"


