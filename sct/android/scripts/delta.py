#!python
# -*- coding:utf-8 -*-
#
# Spandex benchmark and test framework.
#
# Copyright (c) 2013 Contribute.
#
# Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import fileinput
import optparse
import sys

SLOW_INDEX_THRESHOLD  = 22
SLOW_MILLIS_THRESHOLD = 20.0

# -------------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] [input file]"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--range',
                       default = None,
                       dest    = 'line_range',
                       help    = 'Include lines with given range, separated by comma [default: %default].' )

    ( options, args ) = parser.parse_args()
 
    line_min = 1
    line_max = sys.maxint
    if options.line_range:
        r = options.line_range.split( ',' )
        line_min = int( r[ 0 ] )
        line_max = int( r[ 1 ] )

    # Multipass
    lines = []
    i     = 1
    for line in fileinput.input( args ):
        if i >= line_min and i <= line_max: 
            lines.append( line )
        i += 1   

    last_time  = 0
    total      = 0
    for line in lines:
        sl         = line[ 4 : ].split()
        line_time  = float( sl[ 4 ] )
        if last_time > 0:
            total += ( line_time - last_time )
        last_time  = line_time    

    average = total / len( lines )
 
    last_time = 0
    i         = line_min
    for line in lines:
        sl = line[ 4 : ].split()

        line_time = float( sl[ 4 ] )
        if last_time == 0:
            delta = average
        else:
            delta = line_time - last_time 

        print "%03d %06.3f %s :" % ( i, 
                                     delta,
                                     ( "%02.3f" % ( delta - average ) ).rjust( 6 ), ),  
        print line.rstrip()
        
        last_time  = line_time
        i         += 1

