#!python
# -*- coding:utf-8 -*-
#
# Spandex benchmark and test framework.
#
# Copyright (c) 2013 Contribute.
#
# Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import fileinput
import optparse
import sys

# -------------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] [input file]"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--tag',
                       default = 'sctTerminate',
                       dest    = 'tag',
                       help    = 'End tag [default: %default].' )
 
    ( options, args ) = parser.parse_args()
 
    for line in fileinput.input( args ):
        print line,
        if line.find( options.tag ) >= 0:
            sys.exit( 0 )

