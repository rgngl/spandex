#!/bin/bash

index=0

for cores in 4 ; 
do
    for cpu in 1200000 1000000 600000 ; 
    #for cpu in 1200000; 
    do
        for ddr in 58000 120000 360000 450000 ; 
        #for ddr in 450000 ; 
        do
            for gpu in 58000 120000 240000 360000 480000 ; 
            #for gpu in 480000 ; 
            do
 
                # Skip invalid combinations
                if [ $ddr == 58000 ];
                then
                    continue
                fi

                echo CORES $cores CPU $cpu DDR $ddr GPU $gpu

                 ./speed.sh $cores $cpu $ddr $gpu > /dev/null
                logfile="_log"$index".txt"
                ./runspandex.sh > /dev/null
                adb pull /sdcard/result.txt ./$logfile &> /dev/null 
                python $SPANDEX_HOME/python/wikireport.py $logfile
                echo ; echo
                let "index += 1"
            done
        done
    done 
done 
