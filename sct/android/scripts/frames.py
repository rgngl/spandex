#!python
# -*- coding:utf-8 -*-
#
# Spandex benchmark and test framework.
#
# Copyright (c) 2013 Contribute.
#
# Contact: Kari J. Kangas <kari.j.kangas@iki.fi>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import fileinput
import optparse


# -------------------------------------------------------------------
if __name__ == '__main__':
    usage = "usage: %prog [options] [input file]"
    parser = optparse.OptionParser( usage )
    parser.add_option( '--toofast',
                       default = 10.0,
                       dest    = 'toofast',
                       help    = 'Too fast frames less than in milliseconds [default: %default].' )
    parser.add_option( '--startindex',
                       default = 20,
                       dest    = 'startindex',
                       help    = 'Too fast frames start index [default: %default].' )
    parser.add_option( '--mingap',
                       default = 0,
                       dest    = 'mingap',
                       help    = 'Too fast frames min gap [default: %default].' )

    ( options, args ) = parser.parse_args()

    start_index = int( options.startindex )
    min_gap     = int( options.mingap )

    gap_time       = 0
    busy_time      = 0
    toofast_frames = 0
    last_toofast   = 0
    threads        = []
    last_time      = 0
    lines          = 0 
    for line in fileinput.input( args ):
        if not line.strip():
            continue
        pline = line[ 4 : ]
        sl = pline.split()

        frame_index = int( sl[ 1 ] )
        line_thread = sl[ 2 ]
        line_time   = float( sl[ 5 ] )
        line_delta  = 0
        lines      += 1
        if not sl[ 6 ].startswith( '+-' ):
            line_delta = float( sl[ 6 ] )

        if not threads or ( line_thread not in threads ):
            threads.append( line_thread )
 
        if line.find( '*GAP*' ) >= 0:
            #print pline,
            gap_time += line_delta
        else:
            busy_time += line_delta
        
        if line.find( ' END ' ) >= 0:
            print '%03d  events %d  gap %06.3f  busy %06.3f  frame %06.3f  threads %s' % ( frame_index, lines, gap_time, busy_time, line_time, ' '.join( threads ), ),
            toofast = False
            if line_time < float( options.toofast ):
                toofast = True
            if frame_index >= start_index and ( frame_index - last_toofast ) > min_gap and toofast:
                toofast_frames += 1
                last_toofast    = frame_index
                print ' * TOO FAST %d *' % toofast_frames
            else:
                print ''
            gap_time  = 0
            busy_time = 0
            lines     = 0
            threads   = []
            last_time = line_time

