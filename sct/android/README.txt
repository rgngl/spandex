#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

Spandex Android port is based on initial version created by Jameson Williams
<jameson.h.williams@intel.com>.

Requires Android SDK and NDK. Should work on Android 2.3+. Tested on Nexus1. 

Steps:
   - Set path, for example: export PATH=/c/ndk:/c/Program\ Files\ \(x86\)/Android/android-sdk/tools/:/c/Program\ Files\ \(x86\)/Android/android-sdk/platform-tools:$PATH
   - Set SPANDEX_HOME for example by doing '. use.sh' in spandex/
   - Edit Makefile, set NDK_HOME, RELEASE_KEY, RELEASE_KEY_ALIAS to match your setup
   - Edit local.properties to match your setup
   - Edit project.properties to match you project target API level. Altervatively, do 'android update 
   project -p $SPANDEX_HOME/sct/android -t <API LEVEL>'. Otherwise, you might get some compile errors 
   from missing khronos headers
   - In windows, you might need to make the paths extra short to avoid too
   long command line during compilation
      - Install NDK to /c
      - Move spandex to /c
   - make all
   - Push benchmark.txt to /sdcard/benchmark.txt
   - Run spandex, use 'adb logcat' to observe when the benchmark suite finishes
   - Pull /sdcard/result.txt
   
On Android 2.2, you might want to try changing spandex/sct/android/jni/android_siwindow.cpp, line 415:

   From:
       gSurfaceFieldID = env->GetFieldID( env->FindClass( "android/view/Surface" ), "mNativeSurface", "I" );
       
   To:
       gSurfaceFieldID = env->GetFieldID( env->FindClass( "android/view/Surface" ), "mSurface", "I" );

       
PURE-NATIVE ANDROID
===================

spandex/sct/android/native contains a template project which can be used as a
starting point when creating an platform-specific pure-native spandex
executable. Search the files for TODO's and implement indicated parts according
to the underlying platform. You are supposed to know what you are doing.

Compile and install: Go first to spandex/sct/android/native and 'make init' to
copy all needed files. Then rename spandex/sct/android/native as
spandex/sct/android/jni so that ndk compilation will work properly. In
Windows/Cygwin, you might need to move spandex/sct/android/jni closer to root,
e.g. to spandex/, to avoid "arm-linux-androideabi-g++.exe: CreateProcess: No
such file or directory" error. Edit install target in Makefile, 'make compile',
'make install'.
