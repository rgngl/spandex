/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gitorious.spandex;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.SurfaceView;
import android.graphics.Paint;
import android.graphics.Canvas;
import java.lang.Runnable;
import java.lang.Thread;
import java.util.Random;

/**
 *
 *
 */
public class Spandex extends Activity implements SurfaceHolder.Callback
{
    private static native int   spandexInit( String wrapLib );
	private static native int   spandexMain( Object surface );
	private static native void  spandexResizeWindow( int width, int height );
    private static native void  spandexQuit();

    private SpandexView         mSurfaceView;
    private Thread              mAppThread;
    private Surface             mSurface;
    private Boolean             mFinished;

    /**
     *
     *
     */
    private class AppRunnable implements Runnable
    {
    	public void run()
        {
       		spandexMain( mSurface );

            mFinished = true;

            runOnUiThread( new Runnable() {
                public void run() {
                    getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_VISIBLE );
                    mSurfaceView.setWillNotDraw( false );
                    mSurfaceView.invalidate();
                }
            });
       }
    }

    /**
     *
     *
     */
    private class SpandexView extends SurfaceView
    {
        private Paint           mPaint; 
        
        public SpandexView()
        {
            super( getApplication() );
            
            mPaint = new Paint();
        }

        protected void onDraw( Canvas canvas )
        {
            if( !mFinished )
            {
                super.onDraw( canvas );
            }
            else
            {
                int w = canvas.getWidth();
                int h = canvas.getHeight();

                mPaint.setARGB( 255, 255, 0, 0 );
                canvas.drawRect( 0, 0, w / 3, h, mPaint );
  
                mPaint.setARGB( 255, 0, 255, 0 );
                canvas.drawRect( w / 3, 0, 2 * w / 3, h, mPaint );
  
                mPaint.setARGB( 255, 0, 0, 255 );
                canvas.drawRect( 2 * w / 3, 0, w, h, mPaint );
           }
        }
    }

    /**
     *
     *
     */
  	private void initialize()
    {
    	System.loadLibrary( "stlport" );
        System.loadLibrary( "spandex" );

        spandexInit( "spandex" );
    }

    /**
     *
     *
     */   
    public void surfaceChanged( SurfaceHolder holder, int format, int w, int h )
    {
    	spandexResizeWindow( w, h );
    }

    /**
     *
     *
     */
	public void surfaceCreated( SurfaceHolder holder )
    {
    	mAppThread = new Thread( new AppRunnable() );
        mSurface   = holder.getSurface();
        mAppThread.start();
    }

    /**
     *
     *
     */
    public void surfaceDestroyed( SurfaceHolder holder )
    {
    	spandexQuit();
        
        try
        {
        	mAppThread.join();
        }
        catch( InterruptedException e )
        {
        	throw new RuntimeException( e );
        }
    }

    /**
     *
     *
     */
    protected void onCreate( Bundle savedInstanceState )
    {
    	super.onCreate( savedInstanceState );
        
        mSurfaceView         = new SpandexView();
        SurfaceHolder holder = mSurfaceView.getHolder();
        
        holder.addCallback( this );
        setContentView( mSurfaceView );

        mSurfaceView.setKeepScreenOn( true );        
        getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_HIDE_NAVIGATION );
        
        initialize();
    }
}

