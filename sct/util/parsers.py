#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys
import re

class Permutator:

    def __init__( self, limits ):
        assert min( limits ) > 0
        self.limits = limits[:]
        self.state = [ 0 ] * len( limits )

    def inc( self ):
        return self.rinc( self.state, self.limits, len( self.state ) - 1 );

    def rinc( self, state, limits, counter ):
        if counter < 0:
            return 0
        if state[ counter ] + 1 >= limits[ counter ]:
            state[ counter ] = 0
            return self.rinc( state, limits, counter - 1 )
        else:
            state[ counter ] += 1
            return 1

    def getState( self ):
        return self.state[ : ]

class Section( dict ):

    def __init__( self, name, type ):
        self.name = name
        self.type = type

    def __str__( self ):
        s =  "Section: name=" + self.name + ", type=" + self.type + "\n"
        for k in self.keys():
            s += k + ": " + self[ k ] + "\n"

        return s

def ParseAttributeValue( value ):
    values = []
    while value:
        if value[0] == '"':
            token, value = value[1:].split( '"', 1 )
        elif value[0] == ',':
            value = value[1:].strip()
            continue
        elif ',' in value:
            token, value = value.split( ',', 1 )
        else:
            token = value
            value = ''
        values.append( token.strip() )
    return values
    
def ParseQuotedString( string ):
    '''Returns a copy of the string with possible surrounding double quotation
    marks and white-space removed.'''

    string = string.strip()
    if not string:
        return ''
    if string[0] == '"':
        assert string[-1] == '"'
        return string[1:-1].strip()
    assert string[-1] != '"'
    return string

def ParseFloat( string ):
    '''Returns the floating point number contained in the string. The number can
    be in double quotes and have extra white-space characters around it. This
    function also tries to handle the typical decimal point and thousand
    separator cases.'''

    # Remove possible quotes.
    string  = ParseQuotedString( string )

    if not string:
        return None
    
    USFloat = True

    if string.find( ',' ) != -1:
        # Either a non-US float or comma is used as thousand separator.
        if string.find( '.' ) != -1:
            # Ok, US float with comma as a thousand separator.
            thousandSep = ','
        else:
            # Non-US style float, thousand separator can be space.
            thousandSep = ' '
            USFloat = False
    else:
        # Most likely a US-style float but does not have thousand separators.
        thousandSep = None

    if thousandSep:
        string = string.replace( thousandSep, '' )

    if not USFloat:
        string = string.replace( ',', '.' )

    return float( string )


class Parser:

    def __init__( self ):
        pass

    def parse( self, s, permutate=True ):

        newsections = []
        
        # First, we strip out all comments
        cexp = re.compile( """\s*#.*?$""", re.MULTILINE | re.DOTALL )
        s = re.sub( cexp, '', s )

        # Remove " chars
        #s = s.replace( r'"', '' )

        # Second, we find sections
        exp = re.compile( """^\[([^\]]*?)\]      # Find section name, group 1
                             ([^\[]*)               # Find section contents, group 2"""
                          , re.MULTILINE | re.DOTALL | re.VERBOSE)
        sections = re.findall( exp, s )

        # For each section, we parse its name, type, and attributes

        aexp = re.compile( """^\s*([^:]*?):(.*?)$""", re.MULTILINE | re.DOTALL )
        for s in sections:
            attribs = []

            # Split section header from : to get name and type
            t = s[ 0 ].split( ':' )

            name = t[ 0 ].strip()
            if name[0] == '"' and name[-1] == '"':
                name = name[1:-1]
            if len( t ) > 1:
                type = t[ 1 ].strip()
            else:
                type = ''

            # Find attributes and separate name from values
            as = re.findall( aexp, s[ 1 ] )
            for a in as:
                attribName  = a[0].strip()
                attribValue = a[1].strip()
                attribs.append( (attribName, ParseAttributeValue( attribValue ) ) )

            #keys = attribs.keys()

            if permutate:
                # Get counter limits for permutator

                limits = [ 0 ] * len( attribs )
                for i in range( len( attribs ) ):
                    limits[ i ] = len( attribs[ i ][1] )

                # Create permutator
                p = Permutator( limits )

                # Create section from each attribute permutation
                while 1:
                    sect = Section( name, type )

                    # Get permutator state and use it as indices to get
                    # attribute values for current permutation index

                    state = p.getState()
                    for i in range( len( attribs ) ):
                        sect[ attribs[i][0] ] = attribs[ i ][1][ state[ i ] ]

                    newsections.append( sect )

                    # Generate new permutation, quit if permutations done

                    if p.inc() == 0:
                        break;
            else:
                sect = Section( name, type )
                for i in range( len( attribs ) ):
                    if attribs[i][0] == 'Warnings':
                        sect[attribs[i][0]] = attribs[i][1]
                    else:
                        attributeCount = len( attribs[i][1] )
                        assert attributeCount == 0 or attributeCount == 1
                        if attributeCount == 0:
                            sect[attribs[i][0]] = None
                        else:
                            sect[attribs[i][0]] = attribs[i][1][0]
                            
                newsections.append( sect )

        return newsections


if __name__ == '__main__':

    if len( sys.argv ) != 2:
        print 'Missing input file.'
        sys.exit( 1 )
    try:
        f = open( sys.argv[ 1 ], 'r' )
    except:
        print 'Cannot open input file:', sys.argv[ 1 ]
        sys.exit( 1 )

    s = f.read()
    f.close()
    
    p = Parser()
    sections = p.parse( s )

    for s in sections:
        print s
    
    

