#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# Simple script for generating a SConsctruct file that builds a unittest
# executable (with CppUnitLite and mock common system interface).

sct_root    = file( '.sctroot.txt' ).read().strip()
target      = file( '.target.txt' ).read().strip()
srcs        = file( '.srcs.txt' ).read().split()
incs        = file( '.incs.txt' ).read().split()
sconscripts = file( '.sconscripts.txt' ).read().split()

template = '''
# WARNING! This file has been generated, all edits will be lost!
SConscript( '%s/devel/util/scons_envs.py' )

Import( 'Unittest' )

Unittest( %s, %s, %s, %s )
'''

print template % (sct_root, repr( target ), repr( srcs ), repr( incs ), repr( sconscripts ))
