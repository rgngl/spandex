#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

################################################################################
# Coverage analysis.
SRC_COVERAGES := $(foreach src,$(notdir $(SRCS)),$(src)_coverage.txt)
CLEAN_FILES   += $(basename $(TARGET))_pure.log coverage.cfy coverage.txt coverage_report.txt

.PHONY: coverage
coverage: coverage_report.txt

coverage_report.txt: $(SRC_COVERAGES)
	@cat $(SRC_COVERAGES) > $@
	@rm -f $(SRC_COVERAGES)
	@echo Coverage report:
	@cat $@
	@echo Coverage report saved to $@

%_coverage.txt: coverage.txt
	@grep $(subst .,\\.,$*) coverage.txt | head -n 1 | cut -f13 | sed "s/\\s*//" > $*_coverage_percent
	@grep $(subst .,\\.,$*) coverage.txt | head -n 1 | cut -f10 | sed "s/\\s*//" > $*_lines_missed
	@echo $*: > $@
	@echo -e \\t\\tcoverage:\\nlines missed: | paste - $*_coverage_percent - $*_lines_missed >> $@
	@rm $*_coverage_percent $*_lines_missed

coverage.txt: $(TARGET)
	@coverage /SaveTextData=$@ /SaveData=coverage.cfy $< $(COVERAGE_CMD_LINE)

