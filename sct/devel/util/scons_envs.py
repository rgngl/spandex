#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, os

# The base consctruction environment, common to all builds.
baseEnv = Environment( CCFLAGS='-nologo -D_DEBUG -DSCT_CHECK_LEAKS -MTd -Z7 -W4 -D_CRT_SECURE_NO_DEPRECATE',
                       LINKFLAGS='-NOLOGO -DEBUG -PDB:NONE -NODEFAULTLIB:libc.lib -FIXED:NO' )

baseEnv['SPANDEX_HOME'] = os.environ['SPANDEX_HOME']

def Unittest( target, sources, includes, sconscripts ):
    sources = sources + unittestLibNodes
    libPaths = [os.path.dirname( node[0].path ) for node in unittestLibNodes]
    
    #env = baseEnv.Copy()
    env = baseEnv.Clone()
    # This makes mogles work.
    env.Append( CCFLAGS = ' -DAPIENTRY=' )
    env.Append( CPPPATH = includes + libPaths )

    Export( 'env' )

    for sc in sconscripts:
        nodes = SConscript( sc )
        assert nodes, 'SConscript %s did not return a Node object' % sc
        sources.extend( nodes )

    env.Program( target, sources,  )
    
Export( 'baseEnv Unittest' )

# unittestLibPaths = [ '../src/lib/CppUnitLite',
#                      '../src/lib/sicommon_mock',
#                      '../src/lib/simemory_mock',
#                      '../src/lib/gles_mock',
#                      '../src/lib/siopenvg_mock',
#                      '../src/lib/sidisplay_mock' ]
unittestLibPaths = [ '../src/lib/CppUnitLite',
                     '../src/lib/sicommon_mock' ]
unittestLibNodes = []

#env = baseEnv.Copy()
env = baseEnv.Clone()
# This makes mogles work.
env.Append( CCFLAGS = ' -DAPIENTRY=' )
Export( 'env' )
# Read in the library SConscripts and store the scons nodes so that they can be
# used in Unittest function.
for path in unittestLibPaths:
    node = SConscript( path + '/SConscript' )
    assert node, 'Library SConscript in %s did not return a valid scons node' % path
    unittestLibNodes.append( node )
