#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# Makefile template for building unittests. Define the following variables
# before including this file:
#
# SCT_ROOT      path to the spandex/sct
# TARGET		name of the unittest executable
# SRCS			source files
# INCS			include paths
# CLEAN_FILES	(optional) files that should be deleted in case 

CLEAN_FILES += SConstruct .sctroot.txt .target.txt .srcs.txt .incs.txt
SCONS_OPTS  := -Qs

ifndef SCT_ROOT
$(error SCT_ROOT variable has not been defined)
endif
ifndef TARGET
$(error TARGET variable has not been defined)
endif
ifndef SRCS
$(error SRCS variable has not been defined)
endif

MKUNITTEST_PY = $(SCT_ROOT)/devel/util/mkunittest.py

.PHONY: test
test: build
	./test.exe

$(TARGET): build

.PHONY: build
build: SConstruct
	@scons.bat $(SCONS_OPTS) $(TARGET)

# Rule for generating the SConsctruct file.
SConstruct: Makefile $(MKUNITTEST_PY)
	@echo $(SCT_ROOT) > .sctroot.txt
	@echo $(TARGET) > .target.txt
	@echo $(SRCS) > .srcs.txt
	@echo $(INCS) > .incs.txt
	@echo $(SCONSCRIPTS) > .sconscripts.txt
	@python $(MKUNITTEST_PY) > $@
	@rm .sctroot.txt .target.txt .srcs.txt .incs.txt .sconscripts.txt

.PHONY: clean
clean:
	@scons.bat $(SCONS_OPTS) -c
	@rm -f $(CLEAN_FILES)

# Include coverage measurement targets.
include $(SCT_ROOT)/devel/util/coverage.mk
