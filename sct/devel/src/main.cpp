/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <crtdbg.h> /* For heap checking functions */

#include "sct.h"
#include "sct_module.h"
#include "sicommon_win32.h"
#include "sct_utils.h"

/* Global variables. */
const char     *windowClassName         = "Spandex";
void           *sctContext              = NULL;
HINSTANCE       hInstance               = NULL; // Instance Of The Application
HDC             hDC                     = NULL; // GDI Device Context
HGLRC           hRC                     = NULL; // Rendering Context
HWND            gWindowHandle           = NULL; // Window Handle
FILE*           gInputFile              = NULL;
int             gWindowWidth            = WIDTH;
int             gWindowHeight           = HEIGHT;

/* Function forward declarations. */
LRESULT CALLBACK WndProc( HWND, UINT, WPARAM, LPARAM );

/*!
 *
 *
 */
LRESULT CALLBACK WndProc(   HWND    hWnd,           // Handle For This Window
                            UINT    uMsg,           // Message For This Window
                            WPARAM  wParam,         // Additional Message Information
                            LPARAM  lParam)         // Additional Message Information
{

    switch( uMsg )
    {
        case WM_CLOSE:

            PostQuitMessage( 0 );
            return 0;

        case WM_PAINT:

            break;

        case WM_TIMER:
            KillTimer( hWnd, 1 );

            SCT_ASSERT( sctContext != NULL );
            SCTCycleStatus status = sctCycle( sctContext );
            
            if( status == SCT_CYCLE_FATAL_ERROR )
            {
                MessageBox( hWnd, "sctCycle returned SCT_CYCLE_FATAL_ERROR, aborting", "Error", MB_OK | MB_ICONERROR );
            }

            if( status == SCT_CYCLE_FINISHED || status == SCT_CYCLE_FATAL_ERROR )
            {
                PostQuitMessage( 0 );
                return 0;
            }
            SetTimer( hWnd, 1, 100, NULL );
            break; 
    }

    return DefWindowProc( hWnd, uMsg, wParam, lParam );
}

/*!
 *
 *
 */
int WINAPI WinMain( HINSTANCE   /*ahInstance*/,     // Instance
                    HINSTANCE   /*hPrevInstance*/,  // Previous Instance
                    LPSTR       lpCmdLine,          // Command Line Parameters
                    int         /*nCmdShow*/)       // Window Show State
{
    MSG         msg;
    WNDCLASS    wc;                     // Windows Class Structure
    DWORD       dwStyle;                // Window Style
    RECT        WindowRect;             // Grabs Rectangle Upper Left / Lower Right Values

#if defined( SCT_CHECK_LEAKS )

    HANDLE dumpFile = CreateFile( "leaks.dump", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
    SCT_ASSERT_ALWAYS( dumpFile != NULL );
    
    // Enable memory leak checking at program exit. 
    _CrtSetDbgFlag( _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG ) | _CRTDBG_LEAK_CHECK_DF );
    _CrtSetReportMode( _CRT_WARN, _CRTDBG_MODE_FILE );
    _CrtSetReportFile( _CRT_WARN, dumpFile );

#endif  /* defined( SCT_CHECK_LEAKS ) */

    SCT_ASSERT_ALWAYS ( strlen( lpCmdLine ) > 0 );

    if( strncmp( "--dump-config", lpCmdLine, 13 ) == 0 )
    {
        FILE *dumpFile = fopen( "config.dump", "w" );
        SCT_ASSERT_ALWAYS( dumpFile != NULL );
        
        SCTModuleList *modules = siCommonGetModules( NULL );
        SCTModuleListIterator iter;
        sctGetModuleListIterator( modules, &iter );

        for( SCTModule *module = sctGetNextModuleListElement( &iter );
             module           != NULL;
             module            = sctGetNextModuleListElement( &iter ) )
        {
            SCT_ASSERT( module->config != NULL );
            fputs( module->config, dumpFile );
        }
        fclose( dumpFile );
        return 0;
    }

    gInputFile = fopen( lpCmdLine, "r" );
    SCT_ASSERT_ALWAYS( gInputFile != NULL );

    WindowRect.left         = ( long )0;                // Set Left Value To 0
    WindowRect.right        = ( long )gWindowWidth;     // Set Right Value To Requested Width
    WindowRect.top          = ( long )0;                // Set Top Value To 0
    WindowRect.bottom       = ( long )gWindowHeight;    // Set Bottom Value To Requested Height

    hInstance = GetModuleHandle( NULL ); // Retreive the instance's handle

    memset( &wc, 0, sizeof( WNDCLASS) );

    wc.style                = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;   // Redraw On Size, And Own DC For Window.
    wc.lpfnWndProc          = WndProc;                              // WndProc Handles Messages
    wc.cbClsExtra           = 0;                                    // No Extra Window Data
    wc.cbWndExtra           = 0;                                    // No Extra Window Data
    wc.hInstance            = hInstance;                            // Set The Instance
    wc.hIcon                = LoadIcon(NULL, IDI_WINLOGO);          // Load The Default Icon
    wc.hCursor              = LoadCursor(NULL, IDC_ARROW);          // Load The Arrow Pointer
    wc.hbrBackground        = NULL;                                 // No Background Required For GL
    wc.lpszMenuName         = NULL;                                 // We Don't Want A Menu
    wc.lpszClassName        = windowClassName;                      // Set The Class Name

    if ( !RegisterClass( &wc ) )                                    // Attempt To Register The Window Class
    {
        return 1;
    }
    
    dwStyle = WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN; // Windows Style

    /* Create the frame */ 
    gWindowHandle = CreateWindow(    windowClassName, 
                                     "Spandex", 
                                     dwStyle,                            // Required Window Style
                                     0, 0,                               // Window Position
                                     WindowRect.right - WindowRect.left, // Calculate Window Width
                                     WindowRect.bottom - WindowRect.top, // Calculate Window Height
                                     NULL, 
                                     NULL, 
                                     hInstance, 
                                     NULL );

    if( !gWindowHandle )
    {
        return FALSE;
    }

    /* Resize the window so that the client area is WIDHT * HEIGHT */

    RECT wrect;
    RECT crect;
    GetWindowRect( gWindowHandle, &wrect );
    GetClientRect( gWindowHandle, &crect );

    SetWindowPos(   gWindowHandle, 
                    HWND_TOP, 
                    0, 
                    0, 
                    wrect.right - wrect.left + ( ( wrect.right - wrect.left ) - ( crect.right - crect.left ) ),
                    wrect.bottom - wrect.top + ( ( wrect.bottom - wrect.top ) - ( crect.bottom - crect.top ) ),
                    SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOZORDER | SWP_NOREDRAW 
                    );

    ShowWindow( gWindowHandle, SW_SHOW );
    UpdateWindow( gWindowHandle );
    
    // SCT can only be initialized after the window has been created, otherwise GL(ES)
    // might not be able to operate properly.
    sctContext = sctInitialize();
    if( sctContext == NULL )
    {
        fclose( gInputFile );
        return 1;
    }

//     /* Swapping buffers here once to ensure that Mesa statistics are correct for
//      * the benchmarks. Also, it seems that the frame before the first buffer
//      * swap will always be missed by Mesa for some reason. Because we do the
//      * swap here the first benchmark will have all its frames correctly
//      * reported. */
//     siDisplayStartGL( NULL, NULL );
//     siDisplaySwapBuffers( NULL );
//     siDisplayEndGL( NULL );

    /* We use timer message to start notify the system that everything is ok for benchmark run */
    SetTimer( gWindowHandle, 1, 100, NULL );

    while( GetMessage( &msg, NULL, 0, 0 ) ) 
    {
        TranslateMessage( &msg );
        DispatchMessage( &msg );
    }

    KillTimer( gWindowHandle, 1 );

    sctTerminate( sctContext );

    fclose( gInputFile );

    if ( !DestroyWindow( gWindowHandle ) )                      // Are We Able To Destroy The Window?
    {
        return 1;
    }

    if ( !UnregisterClass( windowClassName, hInstance ) )       // Are We Able To Unregister Class
    {
        return 1;
    }

#if defined( SCT_CHECK_LEAKS )

    _CrtDumpMemoryLeaks();
    CloseHandle( dumpFile );

#endif  /* defined( SCT_CHECK_LEAKS ) */

    return 0;
}

int win32WindowWidth()
{
    return gWindowWidth;
}

int win32WindowHeight()
{
    return gWindowHeight;
}
