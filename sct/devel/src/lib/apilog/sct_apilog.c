/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_types.h"
#include "sct_sicommon.h"
#include "sct_apilog.h"

#include <string.h>
#include <stdlib.h>

typedef struct APIData
{
    struct APIData *next;
    char           *apiName;
    FILE           *logFile;
} APIData;

static APIData *gRootNode = NULL;

APIData *sctiGetAPIData( char *apiName );
APIData *sctiAddAPIDataNode( char *apiName );
APIData *sctiCreateNewAPIDataNode();
APIData *sctiGetRootNode();
APIData *sctiGetLastNode();

FILE *sctAPILogGetLogFile( char *apiName )
{
    APIData *data = sctiGetAPIData( apiName );

    SCT_ASSERT( data != NULL );

    return data->logFile;
}

void sctAPILogStartLogging( char *apiName )
{
    APIData *data = sctiGetAPIData( apiName );
    SCT_ASSERT( data != NULL );
    if( data->logFile == NULL )
    {
        char *filename = (char *) malloc( strlen( apiName ) + 5 );
        SCT_ASSERT( filename != NULL );
        strcpy( filename, apiName );
        strcat( filename, ".log" );
        data->logFile = fopen( filename, "w" );
        SCT_ASSERT( data->logFile != NULL );
        free( filename );
    }
}

void  sctAPILogForEachLogFile( SCTAPILogCallback callback, void *data )
{
    APIData *rootNode = sctiGetRootNode();
    APIData *apiData;

    for( apiData = rootNode->next; apiData != NULL; apiData = apiData->next )
    {
        callback( apiData->logFile, data );
    }
}
    
void sctAPILogCleanup()
{
    APIData *node, *nextNode;

    node = gRootNode;

    while( node != NULL )
    {
        nextNode = node->next;
        if( node->logFile != NULL )
        {
            fclose( node->logFile );
        }
        free( node->apiName );
        free( node );
        node = nextNode;
    }        
    gRootNode = NULL;
}

APIData *sctiGetAPIData( char *apiName )
{
    APIData *rootNode = sctiGetRootNode();
    APIData *data;

    SCT_ASSERT( rootNode != NULL );
    SCT_ASSERT( apiName != NULL );
    SCT_ASSERT( strlen( apiName ) > 0 );

    /* Try to find an existing data node. */
    for( data = rootNode->next; data != NULL; data = data->next )
    {
        if( strcmp( data->apiName, apiName ) == 0 )
        {
            return data;
        }
    }
    /* Data node for the requested API does not exist, lets create it. */
    return sctiAddAPIDataNode( apiName );
}

APIData *sctiAddAPIDataNode( char *apiName )
{
    APIData *lastNode = sctiGetLastNode();
    APIData *newNode  = sctiCreateNewAPIDataNode();

    SCT_ASSERT( apiName != NULL );
    SCT_ASSERT( strlen( apiName ) > 0 );
    SCT_ASSERT( lastNode != NULL );
    SCT_ASSERT( newNode != NULL );

    newNode->apiName = (char *) malloc( strlen( apiName ) + 1 );
    SCT_ASSERT( newNode->apiName != NULL );
    strcpy( newNode->apiName, apiName );
    /* Link the new node to the list. */
    lastNode->next = newNode;

    return newNode;
}

APIData *sctiCreateNewAPIDataNode()
{
    APIData *newNode;

    newNode = (APIData *) malloc( sizeof( APIData ) );
    SCT_ASSERT( newNode != NULL );
    newNode->next        = NULL;
    newNode->apiName     = NULL;
    newNode->logFile     = NULL;
    return newNode;    
}

APIData *sctiGetRootNode()
{
    if( gRootNode == NULL )
    {
        gRootNode = sctiCreateNewAPIDataNode();
    }
    SCT_ASSERT( gRootNode != NULL );
    return gRootNode;
}

APIData *sctiGetLastNode()
{
    APIData *node = sctiGetRootNode();
    for( ; node->next != NULL; node = node->next );
    return node;
}
