/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <windows.h>
#include <crtdbg.h> /* For heap checking functions */
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <malloc.h>
#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <stdarg.h>

#include "sicommon_win32.h"
#include "sct_utils.h"

#if defined( SCT_USE_MOGLES )
# include "mogles.h"
#endif  /* defined( SCT_USE_MOGLES ) */

#if defined( INCLUDE_EGL_MODULE )
#include "sct_eglmodule.h"  /* for sctEglGetProcAddress */
#endif  /* defined( INCLUDE_EGL_MODULE ) */

#if defined( USE_EGLMEMORY_SCTLIB )
#include "sct_eglmemory.h"  /* for EglMemory */
#endif  /* defined( USE_EGLMEMORY_SCTLIB ) */

#define TIMER_DIVIDER_KLUDGE    1000

// Global data.
void*                           gSingleton = NULL;

// External data references.
extern FILE*                    gInputFile;
extern HWND                     gWindowHandle;

/* Static helper functions. */
static const char*              getOsInfo( void );
static const char*              getCPUInfo( void );

/*!
 *
 *
 */
void* siCommonGetContext( void )
{
	return NULL;
}

/*!
 *
 *
 */
const char* siCommonQuerySystemString( void*, SCTSystemString id )
{
    switch( id )
    {
    case SCT_SYSSTRING_DEVICE_TYPE:
        return "IBM PC compatible";
        
    case SCT_SYSSTRING_OS:
        return getOsInfo();
        
    case SCT_SYSSTRING_CPU:
        return getCPUInfo();

    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }

    return "Unexpected";
}   

/*!
 *
 *
 */
size_t siCommonQueryMemoryAttribute( void*, SCTMemoryAttribute id )
{
    MEMORYSTATUS    stat;

    /* TODO: this should be changed to use GlobalMemoryStatusEx instead. */
    GlobalMemoryStatus( &stat );


    if( id == SCT_MEMATTRIB_TOTAL_SYSTEM_MEMORY )
    {
        return stat.dwTotalPhys;
    }
    else if( id == SCT_MEMATTRIB_USED_SYSTEM_MEMORY )
    {
        return stat.dwTotalPhys - stat.dwAvailPhys;
    }
    else
    {
        size_t          v = 0;

#if defined( USE_EGLMEMORY_SCTLIB )
        void*           eglMemory;
        SCTEGLMemory    m;
        memset( &m, 0, sizeof( SCTEGLMemory ) );

        eglMemory = sctCreateEglMemoryContext();
        if( eglMemory == NULL )
        {
            return 0;
        }
        
        sctGetEglMemoryStatus( eglMemory, &m );

        switch( id )
        {
        case SCT_MEMATTRIB_TOTAL_GRAPHICS_MEMORY:
            v = m.totalGraphicsMemory;
            break;
            
        case SCT_MEMATTRIB_USED_GRAPHICS_MEMORY:
            v = m.usedGraphicsMemory;
            break;
            
        case SCT_MEMATTRIB_USED_PRIVATE_GRAPHICS_MEMORY:
            v = m.usedPrivateGraphicsMemory;
            break;
            
        case SCT_MEMATTRIB_USED_SHARED_GRAPHICS_MEMORY:
            v = m.usedSharedGraphicsMemory;
            break;

        default:
            SCT_ASSERT_ALWAYS( 0 );
            v = 0;
            break;;
        }

        sctDestroyEglMemoryContext( eglMemory );
        
#endif  /* defined( USE_EGLMEMORY_SCTLIB ) */

        return v;
    }
}   

/*!
 *
 *
 */
void* siCommonSampleCpuLoad( void* context )
{
    SCT_USE_VARIABLE( context );

    return NULL;    /* Not supported. */
}

/*!
 *
 *
 */
float siCommonCalculateCpuLoad( void* context, void* sample, SCTCpuLoad load )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( sample );
    SCT_USE_VARIABLE( load );
    
    return -1.0f;
}

/*!
 *
 *
 */
void siCommonDestroyCpuLoadSample( void* context, void* sample )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( sample );
}

/*!
 *
 *
 */
unsigned long siCommonGetTimerFrequency( void* )
{
	LARGE_INTEGER frequency;

	QueryPerformanceFrequency( &frequency );

    // Kludge kludge: performance counter is so accurate that we have to 
    // remove some of the accuracy in order to keep the tick count within
    // integer range inside the tool. See also siGetTimerTick().
	return ( unsigned long )( frequency.QuadPart / TIMER_DIVIDER_KLUDGE );
}

/*!
 *
 *
 */
unsigned long siCommonGetTimerTick( void* )
{
	LARGE_INTEGER timerTick;

	QueryPerformanceCounter( &timerTick );
	
    // Kludge kludge: performance counter is so accurate that we have to 
    // remove some of the accuracy in order to keep the tick count within
    // integer range inside the tool. See also siGetTimerFrequency().
	return ( unsigned long )( timerTick.QuadPart / TIMER_DIVIDER_KLUDGE );
}

/*!
 *
 *
 *
 */
unsigned long siCommonGetFreeMemory( void* )
{
    MEMORYSTATUS    stat;

    GlobalMemoryStatus( &stat );

    return ( unsigned long )( stat.dwAvailPhys );
}

/*!
 *
 *
 *
 */
void siCommonDebugPrintf( void*, const char* fmt, ... )
{
    char    buf[ 2048 ];
    va_list ap;

    SCT_ASSERT_ALWAYS( fmt != NULL );

	va_start( ap, fmt );
	vsnprintf( buf, 2047, fmt, ap );
	va_end( ap );

    strcat( buf, "\n" );
    
    //printf( buf );
    OutputDebugString( buf );
}

/*!
 *
 *
 */
void siCommonErrorMessage( void*, const char* errorString )
{
    printf( "SPANDEX ERROR: %s\n", errorString );

	//MessageBox( NULL , errorString, "Sct error", MB_OK|MB_ICONERROR);
    //ASSERT_ALWAYS( 0 ); // Break execution here in the debugger.
}

/*!
 *
 *
 */
void siCommonWarningMessage( void*, const char* warningString )
{
    printf( "#SPANDEX WARNING: %s\n", warningString );
}

/*!
 *
 *
 */
void* siCommonGetSingleton( void* )
{
    return gSingleton;
}

/*!
 *
 *
 */
void siCommonSetSingleton( void*, void* ptr )
{
    gSingleton = ptr;
}

/*!
 *
 *
 */
void siCommonProgressMessage( void*, int totalBenchmarks, int currentBenchmark, int currentRepeat, const char* benchmarkName )
{
    char message[ 1024 ];

    sprintf( message, "Now running: (%d/%d) %s, repeat %d", currentBenchmark, totalBenchmarks, benchmarkName, currentRepeat );
    SetWindowText( gWindowHandle, message );

#if defined( SCT_USE_MOGLES )
    // A marker in the mogles log makes it easier to separete benchmarks later
    // when the log is processed..
    fprintf( moglesGetOutputFile(), "*%s,%d/%d,%d\n", benchmarkName, currentBenchmark, totalBenchmarks, currentRepeat );
#endif  /* defined( SCT_USE_MOGLES ) */
}

/*!
 *
 *
 */
void siCommonAssert( void*, void* expr, void* file, unsigned int line )
{
#if defined( NDEBUG )
    char message[ 1024 ];
    sprintf( message, "Assertion failed on line %d of file %s.\nExpression: %s", line, file, expr );
    siCommonErrorMessage( NULL, message );
    abort();
#else   /* defined( NDEBUG ) */
#if _MSC_VER >= 1400            /* Visual studio 2005 */ 
    _wassert( ( const wchar_t* )( expr ), ( const wchar_t* )( file ), line );
#elif _MSC_VER >= 1310          /* Visual studio .NET 2003 */
    _assert( ( const char* )( expr ), (const char* )( file ), line );
#else  /* Visual studio 98 (_MSC_VER = 1200) and .NET (_MSC_VER = 1300) */
    _assert( expr, file, line );
#endif
#endif  /* defined( NDEBUG ) */
}

#if defined( SCT_CHECK_LEAKS ) // Define heap tracking versions of the memory allocation and free functions.

/*!
 *
 *
 */
void* siCommonMemoryAllocDbg( void*, unsigned int size, const char* filename, int linenumber )
{
    /* Check if the size exceeds the heap maximum request (defined in malloc.h).
    By doing this check here we should avoid some cases where the debug version
    of heap allocation would create an assert. */
    if( size > _HEAP_MAXREQ )
    {
        return NULL;
    }
    
    return _calloc_dbg( size, 1, _CLIENT_BLOCK, filename, linenumber );
}

/*!
 *
 *
 */
void* siCommonMemoryReallocDbg( void*, void* data, unsigned int size, const char* filename, int linenumber )
{
    if( size > _HEAP_MAXREQ )
    {
        return NULL;
    }
    
    return _realloc_dbg( data, size, _CLIENT_BLOCK, filename, linenumber );
}

/*!
 *
 *
 */
void siCommonMemoryFree( void*, void* memblock )
{
    _free_dbg( memblock, _CLIENT_BLOCK );
}

#else   /* defined( SCT_CHECK_LEAKS ) */

/*!
 *
 *
 */
void* siCommonMemoryAlloc( void*, unsigned size )
{
	return calloc( size, 1 );
}

/*!
 *
 *
 */
void* siCommonMemoryRealloc( void*, void* data, unsigned int size )
{
    return realloc( data, size );
}

/*!
 *
 *
 */
void siCommonMemoryFree( void*, void* memblock )
{
    free( memblock );
}

#endif  /* defined( SCT_CHECK_LEAKS ) */

/*!
 *
 *
 */
char siCommonReadChar( void* )
{
    int ch;
    
    SCT_ASSERT_ALWAYS( gInputFile != NULL );

	ch = fgetc( gInputFile );
    
	if( ch == EOF )
    {
		return '\0';
    }
    
    SCT_ASSERT_ALWAYS( ch >= 0 && ch < 256 );
    
	return ( char )( ch );
}

/*!
 *
 *
 */
int siCommonWriteChar( void*, char ch )
{
	putchar( ch );
    
	return 1;
}

/*!
 *
 *
 */
int siCommonFlushOutput( void* )
{
    return 1;
}

/*!
 *
 *
 */
SCTModuleList* siCommonGetModules( void* )
{
    /* No win32 specific modules. */
    return sctGetCoreModules();   
}

/*!
 *
 *
 */
void siCommonSleep( void*, unsigned long micros )
{
    Sleep( ( int )( micros / 1000 ) );
}

/*!
 *
 *
 */
void siCommonYield( void* )
{
    /* Do nothing on win32. */
}

/*!
 *
 *
 */
void siCommonGetTime( void*, char* buffer, int length )
{
    time_t      ltime;
    struct tm*  today;

    time( &ltime );
    today = localtime( &ltime );
    if( strftime( buffer, length, "%Y-%m-%d %H:%M:%S", today ) == 0 )
    {
        strncpy( buffer, "Error", length );
    }
}

/*!
 *
 *
 */
void ( *siCommonGetProcAddress( void* context, const char* procname ) )( void )
{
    SCT_USE_VARIABLE( context );

#if defined( INCLUDE_EGL_MODULE )
    return sctEglGetProcAddress( procname );
#else   /* defined( INCLUDE_EGL_MODULE ) */
    SCT_USE_VARIABLE( procname );
    return NULL;
#endif  /* defined( INCLUDE_EGL_MODULE ) */
}

/*!
 *
 *
 */
int siCommonGetMessage( void* context, SCTMessage* message, long timeoutMillis )
{
    SCT_USE_VARIABLE( context );
    SCT_ASSERT_ALWAYS( message != NULL );
    SCT_USE_VARIABLE( timeoutMillis );
    
    return -1;
}

/*!
 *
 *
 */
void* siCommonCreateThread( void*, SCTBenchmarkWorkerThreadFunc func, SCTBenchmarkWorkerThreadContext* threadContext )
{
    SCT_ASSERT_ALWAYS( func != NULL );
    SCT_ASSERT_ALWAYS( threadContext != NULL );

    return CreateThread( NULL,
                         0,
                         ( LPTHREAD_START_ROUTINE )( func ),
                         ( LPVOID )( threadContext ),
                         0,
                         NULL );
}

/*!
 *
 *
 */
void siCommonThreadCleanup( void* )
{
    /* Do nothing. */
}

/*!
 *
 *
 */
void siCommonJoinThread( void*, void* thread )
{
    HANDLE  hThread;
    
    SCT_ASSERT_ALWAYS( thread != NULL );

    hThread = ( HANDLE )( thread );

    WaitForSingleObject( hThread, INFINITE );
    CloseHandle( hThread );
}

/*!
 *
 *
 */
SCTBoolean siCommonSetThreadPriority( void*, SCTThreadPriority priority )
{
    int p;

    switch( priority )
    {
    case SCT_THREAD_PRIORITY_LOW:
        p = THREAD_PRIORITY_BELOW_NORMAL;
        break;
    case SCT_THREAD_PRIORITY_NORMAL:
        p = THREAD_PRIORITY_NORMAL;
        break;
    case SCT_THREAD_PRIORITY_HIGH:
        p = THREAD_PRIORITY_ABOVE_NORMAL;
        break;
    default:
        SCT_ASSERT_ALWAYS( 0 );
        return SCT_FALSE;
    }   

    return SetThreadPriority( GetCurrentThread(), p ) ? SCT_TRUE : SCT_FALSE;
}

/*!
 *
 *
 */
void* siCommonCreateMutex( void* )
{
    return CreateMutex( NULL, FALSE, NULL );
}

/*!
 *
 *
 */
void siCommonLockMutex( void*, void* mutex )
{
    HANDLE  hMutex;
    
    SCT_ASSERT_ALWAYS( mutex != NULL );

    hMutex = ( HANDLE )( mutex );
    
    WaitForSingleObject( hMutex, INFINITE );    
}

/*!
 *
 *
 */
void siCommonUnlockMutex( void*, void* mutex )
{
    HANDLE  hMutex;
    
    SCT_ASSERT_ALWAYS( mutex != NULL );

    hMutex = ( HANDLE )( mutex );
    ReleaseMutex( hMutex );
}
        
/*!
 *
 *
 */
void siCommonDestroyMutex( void*, void* mutex )
{
    HANDLE  hMutex;
    
    SCT_ASSERT_ALWAYS( mutex != NULL );

    hMutex = ( HANDLE )( mutex );
    CloseHandle( hMutex );    
}

/*!
 *
 *
 */ 
struct SCTWin32Signal
{
    HANDLE          mutex;
    HANDLE          event;
    volatile int    status;
    volatile int    enabled;
};

/*!
 *
 *
 */
void* siCommonCreateSignal( void* )
{
    SCTWin32Signal* signal;

    signal = ( SCTWin32Signal* )( siCommonMemoryAlloc( NULL, sizeof( SCTWin32Signal ) ) );
    
    if( signal == NULL )
    {
        return NULL;
    }

    memset( signal, 0, sizeof( SCTWin32Signal ) );

    signal->mutex = ( HANDLE )( siCommonCreateMutex( NULL ) );
    
    if( signal->mutex == NULL )
    {
        siCommonDestroySignal( NULL, signal );
        return NULL;
    }

    signal->event = CreateEvent( NULL, TRUE, FALSE, NULL );

    if( signal->event == NULL )
    {
        siCommonDestroySignal( NULL, signal );
        return NULL;
    }

    signal->status  = 1;
    signal->enabled = 1;
    
    return signal;
}

/*!
 *
 *
 */
void siCommonDestroySignal( void*, void* signal )
{
    SCTWin32Signal* s;
    
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTWin32Signal* )( signal );

    if( s->event != NULL )
    {
        CloseHandle( s->event );
        s->event = NULL;
    }

    if( s->mutex != NULL )
    {
        CloseHandle( s->mutex );
        s->mutex = NULL;
    }

    siCommonMemoryFree( NULL, signal );
}

/*!
 *
 *
 */
int siCommonWaitForSignal( void*, void* signal, long timeoutMillis )
{
    SCTWin32Signal* s;    
    DWORD           to      = INFINITE;
    DWORD           r;
    int             status;
    int             enabled;
        
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTWin32Signal* )( signal );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonWaitForSignal %x", signal );
#endif  /* defined( SCT_DEBUG ) */
    
    if( timeoutMillis > 0 )
    {
        to = timeoutMillis;
    }       

    /* First, lock signal mutex. */
    WaitForSingleObject( s->mutex, INFINITE );

    /* Check if the signal is enabled. */
    if( s->enabled == 0 )
    {
        ReleaseMutex( s->mutex );
        return -1;
    }

    ReleaseMutex( s->mutex );   
    
    /* Wait for the signal event. */
    r = WaitForSingleObject( s->event, to );

    /* Lock signal mutex again. */
    WaitForSingleObject( s->mutex, INFINITE );
    
    /* Got signal event, get status and enabled. */
    status  = s->status;
    enabled = s->enabled;

    /* Reset the signal event in case it was signalled ok. */
    if( r == WAIT_OBJECT_0 )
    {
        ResetEvent( s->event );
    }

    /* Release signal mutex and return wait status. */
    ReleaseMutex( s->mutex );   

    if( enabled == 0 )
    {
        return -1;
    }
    
    if( r == WAIT_TIMEOUT )
    {
        return 0;
    }
    else if( r != WAIT_OBJECT_0 )
    {
        SCT_ASSERT_ALWAYS( 0 );
        return -1;
    }   
    
    return status;
}

/*!
 *
 *
 */
void siCommonTriggerSignal( void*, void* signal )
{
    SCTWin32Signal* s;
    
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTWin32Signal* )( signal );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonTriggerSignal %x", signal );
#endif  /* defined( SCT_DEBUG ) */
    
    /* Lock signal mutex */
    WaitForSingleObject( s->mutex, INFINITE );
    
    if( s->enabled != 0 )
    {
        s->status = 1;
        SetEvent( s->event );
    }

    /* Release signal mutex. */
    ReleaseMutex( s->mutex );   
}

/*!
 *
 *
 */
void siCommonCancelSignal( void*, void* signal )
{
    SCTWin32Signal* s;
    
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTWin32Signal* )( signal );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonCancelSignal %x", signal );
#endif  /* defined( SCT_DEBUG ) */
    
    /* Lock signal mutex */
    WaitForSingleObject( s->mutex, INFINITE );

    /* Set signal as disabled. */
    s->status  = -1;
    s->enabled = 0;

    /* Wake up all threads waiting for the signal event. Also set the event
     * for threads just about to start the wait. */
    PulseEvent( s->event );
    SetEvent( s->event );
    
    /* Release signal mutex. */
    ReleaseMutex( s->mutex );
} 

/* ************************************************************ */
/* ************************************************************ */
/* ************************************************************ */

/*!
 *
 *
 */
static const char* getOsInfo( void ) 
{ 
    OSVERSIONINFOEX     osvi; 
    BOOL                bOsVersionInfoEx;
    static char         info[ 1024 ];
    char*               p;

    p = info;
    
    ZeroMemory( &osvi, sizeof( OSVERSIONINFOEX ) );
    osvi.dwOSVersionInfoSize = sizeof( OSVERSIONINFOEX );

    bOsVersionInfoEx = GetVersionEx( ( OSVERSIONINFO* )( &osvi ) );

    if( !bOsVersionInfoEx ) 
    { 
        osvi.dwOSVersionInfoSize = sizeof( OSVERSIONINFO ); 
        if( !GetVersionEx( ( OSVERSIONINFO* )( &osvi ) ) )
        {
            return NULL;
        }
    } 

    switch( osvi.dwPlatformId ) 
    { 
    case VER_PLATFORM_WIN32_NT: 
        if( osvi.dwMajorVersion <= 4 )
        {
            p += sprintf( p, "Microsoft Windows NT ");
        }
        else if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
        {
            p += sprintf( p, "Microsoft Windows 2000 ");
        }
        else if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
        {
            p += sprintf ( p, "Microsoft Windows XP ");
        }
        else 
        { 
            HKEY    hKey; 
            char    szProductType[ 256 ]; 
            DWORD   dwBufLen        = 256; 

            RegOpenKeyEx( HKEY_LOCAL_MACHINE, 
                          "SYSTEM\\CurrentControlSet\\Control\\ProductOptions", 
                          0, KEY_QUERY_VALUE, &hKey ); 
            RegQueryValueEx( hKey, "ProductType", NULL, NULL, 
                             ( LPBYTE )( szProductType ), &dwBufLen); 
            RegCloseKey( hKey );
            
            if( lstrcmpi( "WINNT", szProductType) == 0 )
            {
                p += sprintf( p, "Professional " );
            }
            else if( lstrcmpi( "LANMANNT", szProductType ) == 0 )
            {
                p += sprintf( p, "Server " );
            }
            else if( lstrcmpi( "SERVERNT", szProductType ) == 0 )
            {
                p += sprintf( p, "Advanced Server " );
            }
        } 

        break; 

    case VER_PLATFORM_WIN32_WINDOWS: 
        if( osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 0 ) 
        { 
            p += sprintf ( p, "Microsoft Windows 95 " ); 
            if( osvi.szCSDVersion[ 1 ] == 'C' || osvi.szCSDVersion[ 1 ] == 'B' )
            {
                p += sprintf( p, "OSR2 " );
            }
        } 
        else if( osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 10 ) 
        { 
            p += sprintf ( p, "Microsoft Windows 98 " ); 
            if ( osvi.szCSDVersion[ 1 ] == 'A' )
            {
                p += sprintf( p, "SE " );
            }
        } 
        else if( osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 90 )
        { 
            p += sprintf ( p, "Microsoft Windows Me " ); 
        }
        
        break; 

    case VER_PLATFORM_WIN32s: 
        p += sprintf ( p, "Microsoft Win32s " ); 
        break; 
    }
    
    return info;
} 

/*!
 *
 *
 */
static const char* getCPUInfo( void )
{
    HKEY        hKey;
    static char info[ 1024 ];
    DWORD       bufLen = 1024;    
    char*       p;
    
    RegOpenKeyEx( HKEY_LOCAL_MACHINE, "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0",
                  0, KEY_QUERY_VALUE, &hKey );
    RegQueryValueEx( hKey, "ProcessorNameString", NULL, NULL, ( LPBYTE )( info ), &bufLen );
    RegCloseKey( hKey );
    // Skip whitespace characters which some systems have at the beginning of the string.
    for( p = info; *p != NULL && iswspace( *p ); p++ );
    
    return p;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
void siCommonProfileEvent( void* context, const char* event )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( event );
    /* TODO */
}

