/*
 *  Copyright � 2009 Nokia. All rights reserved.
 *  This material, including documentation and any related 
 *  computer programs, is protected by copyright controlled by 
 *  Nokia. All rights are reserved. Copying, including 
 *  reproducing, storing, adapting or translating, any 
 *  or all of this material requires the prior written consent of 
 *  Nokia. This material also contains confidential 
 *  information which may not be disclosed to others without the 
 *  prior written consent of Nokia.
 */

#include <windows.h>
#include <stdio.h>

#include "sct_sicommon.h"
#include "sct_simemory.h"

#define WIDTH           360
#define HEIGHT          640

int initSystem( HWND hWnd, FILE *file );
int destroySystem( void );
