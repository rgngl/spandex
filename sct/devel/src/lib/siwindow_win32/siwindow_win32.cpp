/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_siwindow.h"
#include "sct_sicommon.h"
#include "sct_types.h"

#pragma warning(disable: 4115)

#include <windows.h>

#define DO_565_COLOR_SHIFT 1

extern HWND     gWindowHandle;
extern int      gWindowWidth;
extern int      gWindowHeight;

static void*    gContext        = ( void* )( 0x1234 );

HBITMAP         createBitmap( int colorBits, int width, int height );

// A whole lot of haxing here
extern int      win32WindowWidth();
extern int      win32WindowHeight();

/*!
 *
 *
 */
void* siWindowCreateContext()
{
    return gContext;
}

/*!
 *
 *
 */
void siWindowDestroyContext( void *context )
{
    SCT_ASSERT_ALWAYS( context == NULL || context == gContext );
}

/*!
 *
 *
 */
SCTBoolean siWindowIsScreenSupported( void* context, SCTScreen screen )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    
    if( screen != SCT_SCREEN_DEFAULT )
    {
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void siWindowGetScreenSize( void* context, SCTScreen screen, int* width, int* height )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );
    SCT_ASSERT_ALWAYS( width != NULL );
    SCT_ASSERT_ALWAYS( height != NULL );

    *width  = win32WindowWidth();
    *height = win32WindowHeight();
}

/*!
 *
 *
 */
void* siWindowGetScreenEglDisplayId( void* context, SCTScreen screen )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );    

    return ( void* )( 0 );
}

/*!
 *
 *
 */
SCTBoolean siWindowSetScreenOrientation( void* context, SCTScreen screen, SCTScreenOrientation orientation )
{
    SCT_ASSERT_ALWAYS( context == gContext );    
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );        
    SCT_USE_VARIABLE( orientation );

    return SCT_FALSE;
}

/*!
 *
 *
 */
void* siWindowCreateWindow( void* context, SCTScreen screen, int x, int y, int width, int height, SCTColorFormat format, void* parent )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );
    SCT_USE_VARIABLE( x );
    SCT_USE_VARIABLE( y );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( parent );

    return gWindowHandle;
}

/*!
 *
 *
 */
void siWindowDestroyWindow( void* context, void* window )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( window == gWindowHandle );
}

/*!
 *
 *
 */
void* siWindowGetEglWindow( void* context, void* window )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( window == gWindowHandle );

    return window;
}

/*!
 *
 *
 */
SCTScreen siWindowGetWindowScreen( void* context, void* window )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( window == gWindowHandle );    

    return SCT_SCREEN_DEFAULT;
}

/*!
 *
 */
SCTBoolean siWindowToFront( void* context, void* window )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( window == gWindowHandle );    

    return SCT_TRUE;
}

/*!
 *
 */
SCTBoolean siWindowResize( void* context, void* window, int x, int y, int width, int height )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( window == gWindowHandle );    
    SCT_USE_VARIABLE( x );
    SCT_USE_VARIABLE( y );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean siWindowSetOpacity( void* context, void* window, float opacity )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( window == gWindowHandle );        
    SCT_USE_VARIABLE( opacity );

    return SCT_FALSE;
}

/*!
 *
 *
 */
SCTBoolean siWindowSetAlpha( void* context, void* window, SCTBoolean enable )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( window == gWindowHandle );        
    SCT_USE_VARIABLE( enable );

    return SCT_FALSE;
}

/*!
 *
 */
SCTBoolean siWindowRefresh( void* context, void* window )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( window == gWindowHandle );        

    return SCT_TRUE;
}

/*!
 *
 *
 */
void* siWindowCreatePixmap( void* context, int width, int height, SCTColorFormat format, void* data, int length )
{
    int     color   = 0;
    HBITMAP bitmap;    
    BITMAP  bm;
    
    SCT_ASSERT_ALWAYS( context == gContext );
   
    switch( format )
    {
    case SCT_COLOR_FORMAT_RGB565:
        color  = 16;
        break;
    case SCT_COLOR_FORMAT_DEFAULT:
    case SCT_COLOR_FORMAT_RGB888:
        color = 24;
        break;
    case SCT_COLOR_FORMAT_RGBX8888:
    case SCT_COLOR_FORMAT_RGBA8888:
        color = 32;
        break;
    default:
        return NULL;
    }

    bitmap = createBitmap( color, width, height );

    if( data != NULL )
    {
        /* TODO: Something is not working here, at least with 16b colors. */
        
        int             r1;
        int             r2;
        int             stride;
        unsigned char*  p1;
        unsigned char*  p2;        
                
        if( GetObject( bitmap, sizeof( BITMAP ), &bm ) == 0 )
        {
            siWindowDestroyPixmap( context, bitmap );
            return NULL;
        }

        if( length != ( bm.bmWidth * bm.bmHeight * bm.bmBitsPixel / 8 ) )
        {
            /* Invalid data. */
            siWindowDestroyPixmap( context, bitmap );
            return NULL;
        }
        
        stride = bm.bmWidth * bm.bmBitsPixel / 8;
        for( r1 = bm.bmHeight - 1, r2 = 0; r1 >= 0; --r1, ++r2 )
        {
            p1 = ( unsigned char* )( bm.bmBits ) + stride * r1;
            p2 = ( unsigned char* )( data ) + stride * r2;

            memcpy( p1, p2, stride );
        }
    }

    return bitmap;
}

/*!
 *
 *
 */
void* siWindowCreateSharedPixmap( void* context, int width, int height, SCTColorFormat format, int usage, void* data, int length )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( usage );
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( length );

    return NULL;
}

/*!
 * 
 *
 */
SCTBoolean siWindowSetPixmapData( void* context, void* pixmap, void* data, int length )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( pixmap != NULL );
    SCT_ASSERT_ALWAYS( data != NULL );
    SCT_ASSERT_ALWAYS( length > 0 );

    return SCT_FALSE;
}

/*!
 *
 *
 */
void* siWindowGetEglPixmap( void* context, void* pixmap, unsigned int* target )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( pixmap != NULL );
    SCT_USE_VARIABLE( target );
    
    return pixmap;
}

/*!
 *
 *
 */
SCTBoolean siWindowBlitPixmap( void* context, void* window, void* pixmap, int x, int y )
{
    HBITMAP bitmap;
    BITMAP  bm;

    SCT_ASSERT_ALWAYS( context == gContext );
    SCT_ASSERT_ALWAYS( window == gWindowHandle );        
    SCT_ASSERT_ALWAYS( pixmap != NULL );

    bitmap = ( HBITMAP )( pixmap );
    if( GetObject( bitmap, sizeof( BITMAP ), &bm ) == 0 )
    {
        return SCT_FALSE;
    }

#if defined( DO_565_COLOR_SHIFT )   
    if( bm.bmBitsPixel == 16 )
    {
        unsigned short* colorp;
        unsigned short  color;
        int             pixels;

        colorp = ( unsigned short* )( bm.bmBits );
        
        for( pixels = bm.bmWidth * bm.bmHeight; pixels; pixels-- )
        {
            color   = *colorp;
            *colorp = ( unsigned short )( ( ( color & 0x1f  )       )          | 
                                          ( ( color & 0x7e0 ) >> 1  ) & 0x3e0  |     
                                          ( ( color & 0xf800 ) >> 1 ) & 0x7c00  );
            colorp++;
        }
    }
#endif  /* defined( DO_565_COLOR_SHIFT ) */

    HDC hDC         = GetDC( gWindowHandle );
    HDC hdcBitmap   = CreateCompatibleDC( ( HDC )( hDC ) );
    HDC old         = ( HDC )( SelectObject( hdcBitmap, bitmap ) );

    BitBlt( hDC, x, y, bm.bmWidth, bm.bmHeight, hdcBitmap, 0, 0, SRCCOPY );

    SelectObject( hdcBitmap, old );

    DeleteDC( hdcBitmap );
    ReleaseDC( gWindowHandle, hDC );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void siWindowDestroyPixmap( void* context, void* pixmap )
{
    SCT_ASSERT_ALWAYS( context == gContext );
    
    DeleteObject( ( HBITMAP )( pixmap ) );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
HBITMAP createBitmap( int colorBits, int width, int height )
{
    const size_t    bmiSize     = sizeof( BITMAPINFO ) + 256U * sizeof( RGBQUAD );
    BITMAPINFO*     bmi;
    HDC             hDC;
    HBITMAP         hBitmap;
    unsigned char*  surface;

    hDC = GetDC(  gWindowHandle );
    if( hDC == NULL )
    {
        return NULL;
    }

    bmi = ( BITMAPINFO* )( malloc( bmiSize ) );
    if( bmi == NULL )
    {
        return NULL;
    }
    memset( bmi, 0, bmiSize );

    bmi->bmiHeader.biSize           = sizeof( BITMAPINFOHEADER );
    bmi->bmiHeader.biWidth          = +width;
    bmi->bmiHeader.biHeight         = -height;
    bmi->bmiHeader.biPlanes         = ( short )( 1 );
    bmi->bmiHeader.biBitCount       = ( unsigned short )( colorBits );
    bmi->bmiHeader.biCompression    = BI_RGB;
    bmi->bmiHeader.biSizeImage      = 0;
    bmi->bmiHeader.biXPelsPerMeter  = 0;
    bmi->bmiHeader.biYPelsPerMeter  = 0;
    bmi->bmiHeader.biClrUsed        = 3;
    bmi->bmiHeader.biClrImportant   = 0;

    hBitmap = CreateDIBSection( hDC, bmi, DIB_RGB_COLORS, ( void** )( &surface ), NULL, 0 );
	free( bmi );

    ReleaseDC( gWindowHandle, hDC );

    return hBitmap;
}
