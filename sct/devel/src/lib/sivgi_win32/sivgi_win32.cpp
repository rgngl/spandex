/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <windows.h>
#include <crtdbg.h> /* For heap checking functions */
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <malloc.h>
#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>

#include "sct_sicommon.h"
#include "sct_sivgi.h"

#include <VG/openvg.h>
#include <VG/VGContext.h>

#define DO_565_COLOR_SHIFT

/*!
 *
 *
 */
typedef struct
{
    int                         dummy;
} Win32VgiContext;

/*!
 *
 *
 */
extern HWND                     gWindowHandle;
static Win32VgiContext*         gVGContext      = NULL;

/*!
 *
 *
 */
HBITMAP                         createBitmap( int colorBits, unsigned char **surface, int width, int height );

/*!
 *
 *
 */
void* siVgiGetContext()
{
    SCT_ASSERT_ALWAYS( gVGContext == NULL );

    gVGContext = ( Win32VgiContext* )( siCommonMemoryAlloc( NULL, sizeof( Win32VgiContext ) ) );
    if( gVGContext == NULL )
    {
        return NULL;
    }
    memset( gVGContext, 0, sizeof( Win32VgiContext ) );
    
    return gVGContext;
}

/*!
 *
 *
 */
void siVgiReleaseContext( void* context )
{
    SCT_ASSERT_ALWAYS( context != NULL && context == gVGContext );

    siCommonMemoryFree( NULL, gVGContext );
    gVGContext = NULL;
}

/*!
 *
 *
 */
SCTVgiTarget* siVgiGetTarget( void* context, int width, int height, VGIColorBufferFormat targetFormat, SCTBoolean targetMask )
{
    Win32VgiContext*            c;
    SCTVgiTarget*               target;
    int                         bits;
    SCTBoolean                  needMask        = targetMask;

    SCT_ASSERT_ALWAYS( context != NULL && context == gVGContext );

    c = ( Win32VgiContext* )( context );

    target = ( SCTVgiTarget* )( siCommonMemoryAlloc( NULL, sizeof( SCTVgiTarget ) ) );
    if( target == NULL )
    {
        return NULL;
    }
    memset( target, 0, sizeof( SCTVgiTarget ) );

    switch( targetFormat )
    {
    case VGI_COLOR_BUFFER_FORMAT_RGB565:
        bits = 16;
        break;

    case VGI_COLOR_BUFFER_FORMAT_RGB888:
        bits = 24;
        break;

    case VGI_COLOR_BUFFER_FORMAT_XRGB8888:
        bits = 32;
        break;

    case VGI_COLOR_BUFFER_FORMAT_ARGB8888:
    default:
        bits     = 32;
        needMask = SCT_FALSE;
        break;
    }

    target->format = targetFormat;
    target->width  = width;
    target->height = height;

    target->bitmapStride = ( bits / 8 ) * width;
    if( ( target->bitmapStride % 4 ) != 0 )
    {
        target->bitmapStride += target->bitmapStride % 4;
    }

    target->bitmap = createBitmap( bits, 
                                   &( target->bitmapData ), 
                                   width, height );
    
    if( target->bitmap == NULL )
    {
        siVgiReleaseTarget( c, target );
        return NULL;
    }

    if( needMask == SCT_TRUE )
    {
        target->mask = siCommonMemoryAlloc( NULL, width * height );
        if( target->mask == NULL )
        {
            siVgiReleaseTarget( c, target );
            return NULL;
        }
        target->maskData = ( unsigned char* )( target->mask );
        target->maskStride = target->width;
    }

    return target;
}

/*!
 *
 */
SCTBoolean siVgiLockTarget( void* context, SCTVgiTarget* target )
{
    SCT_ASSERT_ALWAYS( context != NULL && context == gVGContext );
    SCT_ASSERT_ALWAYS( target != NULL );

    return SCT_TRUE;
}

/*!
 *
 */
void siVgiUnlockTarget( void* context, SCTVgiTarget* target )
{
    SCT_ASSERT_ALWAYS( context != NULL && context == gVGContext );
    SCT_ASSERT_ALWAYS( target != NULL );
}


/*!
 *
 */
void siVgiReleaseTarget( void* context, SCTVgiTarget* target )
{
    SCT_ASSERT_ALWAYS( context != NULL && context == gVGContext );
    
    if( target == NULL )
    {
        return;
    }

    if( target->bitmap != NULL )
    {
        DeleteObject( target->bitmap );
        target->bitmap     = NULL;
        target->bitmapData = NULL;
    }
    if( target->mask != NULL )
    {
        siCommonMemoryFree( NULL, target->mask );
        target->mask     = NULL;
        target->maskData = NULL;
    }
    siCommonMemoryFree( NULL, target );
}

/*!
 *
 *
 */
SCTBoolean siVgiBlitTarget( void* context, SCTVgiTarget* target )
{
    SCT_ASSERT_ALWAYS( context != NULL && context == gVGContext );
    SCT_ASSERT_ALWAYS( target != NULL );

    SCT_ASSERT_ALWAYS( target->bitmap != NULL );

#if defined( DO_565_COLOR_SHIFT )
    
    if( target->format == VGI_COLOR_BUFFER_FORMAT_RGB565 )
    {
        unsigned short*     colorp;
        unsigned short      color;

        colorp = ( unsigned short* )( target->bitmapData );
        
        for( int pixels = target->width * target->height; pixels; pixels-- )
        {
            color   = *colorp;
            *colorp = ( unsigned short )( ( ( color & 0x1f  )       )          | 
                                          ( ( color & 0x7e0 ) >> 1  ) & 0x3e0  |     
                                          ( ( color & 0xf800 ) >> 1 ) & 0x7c00  );
            colorp++;
        }
    }

#endif  /* defined( DO_565_COLOR_SHIFT ) */

    HDC hDC = GetDC(  gWindowHandle );

    HDC hdcBitmap = CreateCompatibleDC( ( HDC )hDC );
    HDC old = ( HDC )SelectObject( hdcBitmap, target->bitmap );

    BitBlt( hDC, 0, 0, target->width, target->height, hdcBitmap, 0, 0, SRCCOPY );

    SelectObject( hdcBitmap, old );
    
    DeleteDC( hdcBitmap );
    ReleaseDC( gWindowHandle, hDC );

    return SCT_TRUE;
}

/*!
 *
 *
 */
HBITMAP createBitmap( int colorBits, unsigned char **surface, int width, int height )
{
    const size_t        bmiSize = sizeof( BITMAPINFO ) + 256U * sizeof( RGBQUAD );
    BITMAPINFO*         bmi;
    HDC                 hDC;
    HBITMAP             hBitmap;

    hDC = GetDC(  gWindowHandle );
    if( hDC == NULL )
    {
        return NULL;
    }

    bmi = ( BITMAPINFO* )malloc( bmiSize );
    if( bmi == NULL )
    {
        return NULL;
    }
    memset( bmi, 0, bmiSize );

    bmi->bmiHeader.biSize           = sizeof( BITMAPINFOHEADER );
    bmi->bmiHeader.biWidth          = +width;
    bmi->bmiHeader.biHeight         = -height;
    bmi->bmiHeader.biPlanes         = ( short )1;
    bmi->bmiHeader.biBitCount       = ( unsigned short )colorBits;
    bmi->bmiHeader.biCompression    = BI_RGB;
    bmi->bmiHeader.biSizeImage      = 0;
    bmi->bmiHeader.biXPelsPerMeter  = 0;
    bmi->bmiHeader.biYPelsPerMeter  = 0;
    bmi->bmiHeader.biClrUsed        = 3;
    bmi->bmiHeader.biClrImportant   = 0;

    hBitmap = CreateDIBSection( hDC, bmi, DIB_RGB_COLORS, ( void** )surface, NULL, 0 );
	free( bmi );

    ReleaseDC( gWindowHandle, hDC );

    return hBitmap;
}







