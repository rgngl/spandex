/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma warning(disable:4514)

#include "TestHarness.h"
#include "sicommon_mock.h"

#include <string.h>

static SCTBoolean   timerFuncCalled             = SCT_FALSE;
static int          timerValue                  = 0;
static int          attributeFuncCalled         = SCT_FALSE;
static char*        attributeValue              = "Attribute";
static int          progressFuncCalled          = SCT_FALSE;


int timerfunc()
{
    int t = timerValue;
    timerFuncCalled = SCT_TRUE;
    
    timerValue += 10;
    return t;
}

char* attributefunc( SCTSystemAttribute id )
{
    ( void )id;

    attributeFuncCalled = SCT_TRUE;
    return attributeValue;
}

void progressfunc( int totalBenchmarks, int currentBenchmark, const char *benchmarkName )
{
    ( void )totalBenchmarks;
    ( void )currentBenchmark;
    ( void )benchmarkName;
    progressFuncCalled = SCT_TRUE;
}


TEST( initialize, TestSystem )
{
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );
}


TEST( input, TestSystem )
{
    const char *str = "0123456789";
    int         maxlen;
    int         i, j;

    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    maxlen = strlen( str );
    for( i = 0; i <= maxlen; i++ )
    {
        tsiCommonSetInput( "0123456789", i );
        for( j = 0; j < i; j++ )
        {
            CHECK( str[ j ] == siCommonReadChar( NULL ) );
        }
        CHECK( siCommonReadChar( NULL ) == 0 );
        CHECK( siCommonReadChar( NULL ) == 0 );
        CHECK( siCommonReadChar( NULL ) == 0 );
    }

    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );
}


TEST( timer, TestSystem )
{
    int t;

    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    tsiCommonSetTimer( timerfunc, 66 );
    CHECK_EQUAL( 66, ( int )siCommonGetTimerFrequency( NULL ) );

    t = timerValue;
    CHECK_EQUAL( t, ( int )siCommonGetTimerTick( NULL ) );
    CHECK( timerFuncCalled == SCT_TRUE );

    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

}


TEST( printStream, TestSystem )
{
    FILE *f;

    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    f = tmpfile();
    if( f != NULL )
    {
        CHECK( ftell( f ) == 0 );
        tsiCommonPrintErrorMessages( f );
        siCommonErrorMessage( NULL, "0123456789" );

        CHECK( ftell( f ) > 0 );

        fclose( f );
    }
    else
    {
        printf( "libtest: ignoring tsiCommonPrintErrorMessages test\n" );
    }

    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );
}


TEST( allocation, TestSystem )
{   
    void   *b1;
    void   *b2;

    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    tsiCommonResetTestMemorySystem();
    CHECK( tsiCommonGetAllocCounter() == 0 );
    CHECK( tsiCommonGetAllocatedMemory() == 0 );
    CHECK( tsiCommonGetMaxAlloc() == 0 );

    b1 = siCommonMemoryAlloc( NULL, 102 );

    CHECK( tsiCommonGetAllocCounter() == 1 );
    CHECK( tsiCommonGetAllocatedMemory() == 102 );
    CHECK( tsiCommonGetMaxAlloc() == 102 );

    b2 = siCommonMemoryAlloc( NULL, 101 );

    CHECK( tsiCommonGetAllocCounter() == 2 );
    CHECK( tsiCommonGetAllocatedMemory() == 203 );
    CHECK( tsiCommonGetMaxAlloc() == 203 );

    siCommonMemoryFree( NULL, b1 );

    CHECK( tsiCommonGetAllocCounter() == 2 );
    CHECK( tsiCommonGetAllocatedMemory() == 101 );
    CHECK( tsiCommonGetMaxAlloc() == 203 );

    siCommonMemoryFree( NULL, b2 );

    CHECK( tsiCommonGetAllocCounter() == 2 );
    CHECK( tsiCommonGetAllocatedMemory() == 0 );
    CHECK( tsiCommonGetMaxAlloc() == 203 );

    tsiCommonResetTestMemorySystem();
    CHECK( tsiCommonGetAllocCounter() == 0 );
    CHECK( tsiCommonGetMaxAlloc() == 0 );

    tsiCommonSetAllocFailThreshold( 0, 1 );
    CHECK( siCommonMemoryAlloc( NULL, 101 ) == NULL );
    b2 = siCommonMemoryAlloc( NULL, 101 );

    CHECK( b2 != NULL );
    siCommonMemoryFree( NULL, b2 );

    tsiCommonResetTestMemorySystem();

    tsiCommonSetAllocFailThreshold( 1, 2 );
    b1 = siCommonMemoryAlloc( NULL, 101 );
    CHECK( siCommonMemoryAlloc( NULL, 101 ) == NULL );
    CHECK( siCommonMemoryAlloc( NULL, 101 ) == NULL );
    b2 = siCommonMemoryAlloc( NULL, 101 );

    CHECK( b1 != NULL );
    CHECK( b2 != NULL );
    siCommonMemoryFree( NULL, b1 );
    siCommonMemoryFree( NULL, b2 );


    tsiCommonResetTestMemorySystem();    
    tsiCommonSetAllocFailThreshold( -1, 0 );

    tsiCommonSetAllocLimit( 202 );
    b1 = siCommonMemoryAlloc( NULL, 101 );
    b2 = siCommonMemoryAlloc( NULL, 101 );
    CHECK( b1 != NULL );
    CHECK( b2 != NULL );
    CHECK( siCommonMemoryAlloc( NULL, 1 ) == NULL );
    CHECK( tsiCommonGetMaxAlloc() == 202 );

    siCommonMemoryFree( NULL, b1 );
    CHECK( siCommonMemoryAlloc( NULL, 102 ) == NULL );
    siCommonMemoryFree( NULL, b2 );

    CHECK( tsiCommonGetAllocCounter() == 4 );

    tsiCommonResetTestMemorySystem();    
    tsiCommonSetAllocLimit( 1 );
    CHECK( siCommonMemoryAlloc( NULL, 2 ) == NULL );
    b1 = siCommonMemoryAlloc( NULL, 1 );
    CHECK( b1 != NULL );
    siCommonMemoryFree( NULL, b1 );

    CHECK( tsiCommonGetAllocatedMemory() == 0 );

    tsiCommonResetTestMemorySystem();    
    tsiCommonSetAllocFailThreshold( 1, -1 );

    b1 = siCommonMemoryAlloc( NULL, 101 );
    CHECK( siCommonMemoryAlloc( NULL, 101 ) == NULL );
    CHECK( siCommonMemoryAlloc( NULL, 101 ) == NULL );
    CHECK( siCommonMemoryAlloc( NULL, 101 ) == NULL );
    CHECK( siCommonMemoryAlloc( NULL, 101 ) == NULL );
    CHECK( siCommonMemoryAlloc( NULL, 101 ) == NULL );
    CHECK( siCommonMemoryAlloc( NULL, 101 ) == NULL );

    CHECK( b1 != NULL );
    siCommonMemoryFree( NULL, b1 );

    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );
}


TEST( write, TestSystem )
{
    const char *str = "0123456789";
    char        buf[ 64 ];
    int         maxlen;
    int         i, j;

    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    tsiCommonResetWriteCounter();
    CHECK( tsiCommonGetWriteCounter() == 0 );

    maxlen = strlen( str );

    for( i = 0; i < maxlen; i++ )
    {
        for( j = 0; j <= i; j++ )
        {
            siCommonWriteChar( NULL, str[ j ] );
        }

        strcpy( buf, tsiCommonGetOutputAndReset() );
        for( j = 0; j <= i; j++ )
        {
            CHECK( buf[ j ] == str[ j ] );
        }
    }

    tsiCommonResetWriteCounter();
    tsiCommonSetWriteFailThreshold( 0, 0 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 1 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 1 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 1 );
    CHECK( tsiCommonGetWriteCounter() == 3 );

    tsiCommonResetWriteCounter();
    tsiCommonSetWriteFailThreshold( 0, 1 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 0 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 1 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 1 );

    tsiCommonResetWriteCounter();
    tsiCommonSetWriteFailThreshold( 1, 2 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 1 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 0 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 0 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 1 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 1 );

    tsiCommonResetWriteCounter();
    tsiCommonSetWriteFailThreshold( 1, -1 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 1 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 0 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 0 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 0 );
    CHECK( siCommonWriteChar( NULL, 'a' ) == 0 );

    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );
}


TEST( queryAttribute, TestSystem )
{
    const char* s;

    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    s = siCommonQueryAttribute( NULL, SCT_SYSINFO_DEVICE_TYPE );
    CHECK( s != NULL );

    tsiCommonSetQueryAttributeFunc( attributefunc );
    CHECK_EQUAL( attributeValue, siCommonQueryAttribute( NULL, SCT_SYSINFO_DEVICE_TYPE ) );
    CHECK( attributeFuncCalled == SCT_TRUE );

    tsiCommonSetQueryAttributeFunc( NULL );
    CHECK_EQUAL( s, siCommonQueryAttribute( NULL, SCT_SYSINFO_DEVICE_TYPE ) );

    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );
}

TEST( progressMessage, TestSystem )
{
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    siCommonProgressMessage( NULL, 1, 1, "Foo" );

    tsiCommonSetProgressMessageFunc( progressfunc );
    siCommonProgressMessage( NULL, 1, 1, "Foo" );

    CHECK( progressFuncCalled == SCT_TRUE );

    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );
}


TEST( moduleList, TestSystem )
{
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );
}

TEST( time, TestSystem )
{
    const char* timestring = "foobar";
    char buf[ 64 ];

    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    tsiCommonSetTime( timestring );
    siCommonGetTime( NULL, buf, 64 );
    CHECK( strcmp( timestring, buf ) == 0 );
    
    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );
}

TEST( misc, TestSystem )
{
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );

    tsiCommonInitSystem();
    CHECK( tsiCommonIsInitialized() == SCT_TRUE );

    siCommonSleep( NULL, 1 );

    tsiCommonDestroySystem();
    CHECK( tsiCommonIsInitialized() == SCT_FALSE );
}


int main( void )
{
 	TestResult tr;
	int s = !TestRegistry::runAllTests(tr);

    return s;
}

