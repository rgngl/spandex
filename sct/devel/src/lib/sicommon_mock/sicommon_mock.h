/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SICOMMON_MOCK_H__ )
#define __SICOMMON_MOCK_H__

#include "sct_sicommon.h"

#include <stdio.h>

#if defined( __cplusplus )
extern "C" {
#endif /* defined( __cplusplus ) */

    typedef int             ( *TSICOMMONTimerFunc )();
    typedef const char*     ( *TSICOMMONQuerySystemStringFunc )( SCTSystemString id );
    typedef size_t          ( *TSICOMMONQueryMemoryAttributeFunc )( SCTMemoryAttribute id );
    typedef void            ( *TSICOMMONProgressMessageFunc )( int totalBenchmarks, int currentBenchmark, int currentRepeat, const char* benchmarkName );
    typedef int             ( *TSICOMMONAllocFailFunc )( void* param );
    typedef SCTModuleList*  ( *TSICOMMONModuleListFunc ) ();

    void                    tsiCommonInitSystem( void );
    void                    tsiCommonResetSystem();
    SCTBoolean              tsiCommonIsInitialized();
    void                    tsiCommonDestroySystem();

    void                    tsiCommonSetInput( const char* input, int length );

    void                    tsiCommonSetTimer( TSICOMMONTimerFunc timerFunc, int frequency );
    
    void                    tsiCommonSetErrorStream( FILE* stream );

    void                    tsiCommonResetTestMemorySystem();
    int                     tsiCommonGetAllocatedMemory();
    int                     tsiCommonGetAllocCounter();
    void                    tsiCommonResetAllocCounter();
    void                    tsiCommonSetAllocFailThreshold( int threshold, int failCount );
    int                     tsiCommonGetMaxAlloc();
    void                    tsiCommonResetMaxAlloc();
    void                    tsiCommonSetAllocLimit( int limit );

    int                     tsiCommonGetWriteCounter();
    void                    tsiCommonResetWriteCounter();
    void                    tsiCommonSetWriteFailThreshold( int threshold, int failCount );
    void                    tsiCommonResetOutput();
    const char*             tsiCommonGetOutputAndReset();

    void                    tsiCommonSetQuerySystemStringFunc( TSICOMMONQuerySystemStringFunc func );
    void                    tsiCommonSetQueryMemoryAttributeFunc( TSICOMMONQueryMemoryAttributeFunc func );
    void                    tsiCommonSetProgressMessageFunc( TSICOMMONProgressMessageFunc func );

    const char*             tsiCommonGetErrorMessage();
    void                    tsiCommonResetErrorMessage();

    int                     tsiCommonTestAllocFail( TSICOMMONAllocFailFunc func, void* param );

    void                    tsiCommonSetModuleListFunc( TSICOMMONModuleListFunc func );

    void                    tsiCommonSetTime( const char* str );

#if defined( __cplusplus )
}
#endif /* defined( __cplusplus ) */

#endif /* !defined( __SICOMMON_MOCK_H__ ) */
