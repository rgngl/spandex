/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( SCT_CHECK_LEAKS )
/* The test system has memory leak reporting support, therefore all the modules
 * using it should be compiled with SCT_CHECK_LEAKS to enable the debug version
 * of siMemoryAlloc. */
#error SCT_CHECK_LEAKS has to be defined!
#endif  /* !defined( SCT_CHECK_LEAKS ) */

#pragma warning( disable: 4514 )
#pragma warning( disable: 4115 )

#include "sicommon_mock.h"
#include "sct_utils.h"

#include <crtdbg.h> /* For heap checking functions */
#include <assert.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h> 
#include <string.h>
#include <stdarg.h>

#define INITIAL_OUTPUT_BUFFER_LENGTH        0xFFFF
#define CONTEXT_VALUE                       0x1234
#define ASSERT_CONTEXT( X )                 if( ( X ) ) { assert( ( int )( X ) == CONTEXT_VALUE ); }


static SCTBoolean                   systemInitialized;

static TSICOMMONTimerFunc           timerFunc;
static int                          timerFrequency;
static unsigned long                timerTick;
static FILE*                        errorStream;
static void*                        singleton           = NULL;
static SCTBoolean                   destroyLog;
   
static int                          allocMemory;
static int                          allocMemoryLimit;
static int                          allocCounter;
static int                          allocFailThreshold;
static int                          allocFailCount;
static int                          allocMax;

static int                          writeCounter;
static int                          writeFailThreshold;
static int                          writeFailCount;

static TSICOMMONQuerySystemStringFunc       querySystemStringFunc;
static TSICOMMONQueryMemoryAttributeFunc    queryMemoryAttributeFunc;
static TSICOMMONProgressMessageFunc         progressMessageFunc;

static char*                        outputBuffer;
static int                          outputBufferLength;
static char*                        outputPtr;

static const char*                  inputPtr;
static int                          inputLength;

static char                         warningMessage[ 512 ];
static char                         errorMessage[ 512 ];

static TSICOMMONModuleListFunc      moduleListFunc;

static const char*                  timeString;

/* ********************************************************************** */
/*!
 * Initialize test system. Test system must be initialized before any
 * test system interface functions are called.
 *
 * \param createLog If SCT_TRUE the system error log will be created, otherwise
 *                  the log will be left to NULL.
 */
void tsiCommonInitSystem( void )
{
    assert( systemInitialized == SCT_FALSE );

    /* Enable memory leak checking at program exit. */
    _CrtSetDbgFlag( _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG ) | _CRTDBG_LEAK_CHECK_DF );
    _CrtSetReportMode( _CRT_WARN, _CRTDBG_MODE_FILE );
    _CrtSetReportFile( _CRT_WARN, _CRTDBG_FILE_STDOUT );

    outputBufferLength  = INITIAL_OUTPUT_BUFFER_LENGTH;
    outputBuffer        = ( char* )malloc( outputBufferLength );
    assert( outputBuffer != NULL );

    systemInitialized = SCT_TRUE;

    tsiCommonResetSystem();

/*     /\* Note: sctCreateLog uses the common system interface so this phase can */
/*      * only be done after the systemInitialized is set to true. Otherwise the */
/*      * internal checks in this module will fail. *\/ */
/*     if( createLog == SCT_TRUE ) */
/*     { */
/*         errorLog = sctCreateLog(); */
/*         assert( errorLog != NULL ); */
/*         destroyLog = SCT_TRUE; */
/*     } */
/*     else */
/*     { */
/*         /\* Error log is not created here so do not destroy it in the cleanup. *\/ */
/*         destroyLog = SCT_FALSE; */
/*     } */
            
}

void tsiCommonResetSystem()
{
    assert( systemInitialized == SCT_TRUE );
    
    timerFunc           =  NULL;
    timerFrequency      =  100;
    errorStream         =  NULL;
   
    allocMemory         =  0;
    allocMemoryLimit    = -1;
    allocCounter        =  0;
    allocFailThreshold  = -1;
    allocFailCount      = -1;
    allocMax            =  0;

    writeCounter        =  0;
    writeFailThreshold  = -1;
    writeFailCount      =  0;

    querySystemStringFunc       = NULL;    
    queryMemoryAttributeFunc    = NULL;
    progressMessageFunc         = NULL;

    outputPtr           = outputBuffer;
    singleton           = NULL;

    inputPtr            = NULL;
    inputLength         = 0;

    errorMessage[ 0 ]   = '\0';

    moduleListFunc      = NULL;

    timeString          = "time";
}

/*!
 * Query if the system has been initialized.
 *
 * \return SCT_TRUE if system is intialized, SCT_FALSE otherwise.
 *
 */
SCTBoolean tsiCommonIsInitialized()
{
    return systemInitialized;
}

/*!
 * Destroy test system. 
 *
 */
void tsiCommonDestroySystem()
{
    assert( systemInitialized );
    if( outputBuffer != NULL )
    {
        free( outputBuffer );
    }

    systemInitialized = SCT_FALSE;
}


/*!
 * Set the input data for siReadChar.
 *
 * \param input     Character array to be used as data in siReadChar
 * \param length    Length of the character array
 *
 */
void tsiCommonSetInput( const char* input, int length )
{
    assert( systemInitialized );
    inputPtr    = input;
    inputLength = length;
}


/*!
 * Set timer function to call when siGetTimerTick is called. It also
 * sets the timer frequency returned by the siGetTimerFrequency.
 *
 * \param func          Timer function to call, or NULL to use windows timer
 * \param frequency     Timer frequency to return when timer function is set
 *
 */
void tsiCommonSetTimer( TSICOMMONTimerFunc func, int frequency )
{
    assert( systemInitialized );
    timerFunc       = func;
    timerFrequency  = frequency;
}


/*!
 * Redirect error messages to a given stream. Error printing is disabled if stream 
 * is NULL. Error printing is initally disabled.
 *
 * \param stream        Stream to print error messages to, or NULL to disable error
 *                      printing.
 *
 */
void tsiCommonSetErrorStream( FILE* stream )
{
    assert( systemInitialized );
    errorStream = stream;
}


/*!
 * Reset the test memory system. This will reset allocation and max allocation
 * counters, and set allocation fail threshold and allocation limit to disabled.
 *
 */
void tsiCommonResetTestMemorySystem()
{
    tsiCommonResetAllocCounter();
    tsiCommonSetAllocFailThreshold( -1, -1 );
    tsiCommonResetMaxAlloc();
    tsiCommonSetAllocLimit( -1 );
}

/*!
 * Returns the amount of memory allocated by siMemoryAlloc function that is
 * not yet freed by the siMemoryFree function.
 *
 * \return      The amount of allocated memory, in bytes.
 *
 */
int tsiCommonGetAllocatedMemory()
{
    assert( systemInitialized );
    return allocMemory;
}


/*!
 * Get test system memory allocation counter value. The memory allocation counter
 * indices the number of times the siMemoryAlloc has been called since test system
 * interface was initialized or the counter was last reset.
 *
 * \return      Memory allocation counter value.
 *
 */
int tsiCommonGetAllocCounter()
{
    assert( systemInitialized );
    return allocCounter;
}


/*!
 * Reset test system memory allocation counter value to zero.
 *
 */
void tsiCommonResetAllocCounter()
{
    assert( systemInitialized );
    allocCounter = 0;
}

/*!
 * Set memory allocation fail threshold and the fail count. The threshold is 
 * the number of memory allocations (value of memory allocation counter) before 
 * the memory allocation will start to fail, i.e. siMemoryAlloc will return NULL,
 * failCount number of times. (If fail count is zero, no failure will occur) Memory 
 * allocation failure is initially disabled, i.e. siMemoryAlloc will fail only if 
 * the test system will actually run out of memory. (Real or memory limited by 
 * tsiCommonSetAllocLimit.) This function also resets the alloc counter.
 *
 * \param threshold     Memory allocation fail threshold. -1 to disable memory 
 *                      allocation failure. 
 * \param failCount     Number of times memory allocation will fail. -1 to fail
 *                      indefinedly.
 *
 */
void tsiCommonSetAllocFailThreshold( int threshold, int failCount )
{
    assert( systemInitialized );
    allocFailThreshold = threshold;
    allocFailCount     = failCount;
    allocCounter       = 0;
}


/*!
 * Get the maximum of allocated memory (in bytes) since the system initialization 
 * or since tsiCommonResetMaxAlloc was called. Allocated memory is increased when 
 * siMemoryAlloc is called, and correspondingly decremented when siMemoryFree is called.
 *
 * \return      Maximum allocated memory since system was initialized, or tsiCommonResetMaxAlloc()
 *              was called.
 *
 */
int tsiCommonGetMaxAlloc()
{
    assert( systemInitialized );
    return allocMax;
}


/*!
 * Reset the maximum allocation counter.
 *
 */
void tsiCommonResetMaxAlloc()
{
    assert( systemInitialized );
    allocMax = 0;

}


/*!
 * Sets the test system limit of available memory. If the application tries to reserve 
 * more memory than available, siMemoryAlloc fails and returns NULL.
 *
 * \param limit     Number of bytes available for siMemoryAlloc, or -1 to disable
 *                  memory limit
 *
 */
void tsiCommonSetAllocLimit( int limit )
{
    assert( systemInitialized );
    allocMemoryLimit = limit;
}

/*!
 * Get the value of the test system write counter. The write counter is incremented
 * every time siWriteChar is called. The write couter is initially set to zero.
 * 
 * \return      Value of the write counter
 *
 */
int tsiCommonGetWriteCounter()
{
    assert( systemInitialized );
    return writeCounter;
}


/*!
 * Reset the test system write counter to zero.
 *
 */
void tsiCommonResetWriteCounter()
{
    assert( systemInitialized );
    writeCounter = 0;
}


/*!
 * Set the write fail threshold. The threshold is the number of writes, i.e. calls
 * to siWriteChar since system initialization or write counter reset, after which
 * the siWriteChar failing desired number of times. Write fail threshold is initially 
 * disabled. This function also resets the write counter.
 *
 * \param threshold     Write fail threshold. -1 to disable
 * \param failCount     Number of times to fail. -1 to fail indefinedly.
 *
 */ 
void tsiCommonSetWriteFailThreshold( int threshold, int failCount )
{
    assert( systemInitialized );
    writeFailThreshold = threshold;
    writeFailCount     = failCount;
    writeCounter       = 0;
}


/*!
 * Set the siQuerySystemString function callback.
 *
 * \param func      Callback function, or NULL to disable.
 *
 */
void tsiCommonSetQuerySystemStringFunc( TSICOMMONQuerySystemStringFunc func )
{
    assert( systemInitialized );
    querySystemStringFunc = func;
}


/*!
 * Set the siQueryAttribute function callback.
 *
 * \param func      Callback function, or NULL to disable.
 *
 */
void tsiCommonSetQueryMemoryAttributeFunc( TSICOMMONQueryMemoryAttributeFunc func )
{
    assert( systemInitialized );
    queryMemoryAttributeFunc = func;
}


/*!
 * Reset the output buffer. The output buffer is used to store siWriteChar
 * output.
 *
 */
void tsiCommonResetOutput()
{
    assert( systemInitialized );
    tsiCommonGetOutputAndReset();
}


/*!
 * Returns a pointer to current output produced by siWriteChar and resets 
 * the output.
 *
 * \return  Pointer to output
 *
 */
const char* tsiCommonGetOutputAndReset()
{
    assert( systemInitialized );
    *outputPtr  = 0;
    outputPtr   = outputBuffer;

    return outputBuffer;
}


/*!
 * Set siProgressMessage callback function. Callback is initially disabled.
 *
 * \param func      Callback function, or NULL to disable callback.
 *
 */
void tsiCommonSetProgressMessageFunc( TSICOMMONProgressMessageFunc func )
{
    assert( systemInitialized );
    progressMessageFunc = func;
}


/*!
 *
 *
 */
const char* tsiCommonGetErrorMessage()
{
    if( strlen( errorMessage ) == 0 )
    {
        return NULL;
    }
    return errorMessage;
}

/*!
 *
 *
 */
void tsiCommonResetErrorMessage()
{
    errorMessage[ 0 ] = '\0';
}

/*!
 *
 *
 */
int tsiCommonTestAllocFail( TSICOMMONAllocFailFunc func, void* param )
{
    int allocCount          = 0;
    int s;
    int allocFailThreshold  = 0;

    tsiCommonResetTestMemorySystem();

    if( func( param ) != 1 )
    {
        return 0;
    }

    allocCount = tsiCommonGetAllocCounter();

    for( allocFailThreshold = 0; allocFailThreshold < allocCount; allocFailThreshold++ )
    {
        tsiCommonResetTestMemorySystem();
        tsiCommonSetAllocFailThreshold( allocFailThreshold, 1 );

        s = func( param );

        tsiCommonResetTestMemorySystem();

        if( s == 1 )
        {
            return 0;
        }
    }

    return 1;
}

/*!
 *
 *
 */
void tsiCommonSetModuleListFunc( TSICOMMONModuleListFunc func )
{
    moduleListFunc = func;
}

/*!
 *
 *
 */
void tsiCommonSetTime( const char *str)
{
    timeString = str;
}


/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */


void* siCommonGetContext()
{
    void* context = ( void * )( CONTEXT_VALUE );

    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    return context;
}


const char* siCommonQuerySystemString( void* context, SCTSystemString id )
{
    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    if( querySystemStringFunc != NULL )
    {
        return querySystemStringFunc( id );
    }

    switch( id )
    {
    case SCT_SYSSTRING_DEVICE_TYPE:
        return "Test device";
        
    case SCT_SYSSTRING_CPU:
        return "Test CPU";

    case SCT_SYSSTRING_OS:
        return "Test OS";

    default:
        assert( 0 );
        break;
    }

    return 0;
}


size_t siCommonQueryMemoryAttribute( void* context, SCTMemoryAttribute id )
{
    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    if( queryMemoryAttributeFunc != NULL )
    {
        return queryMemoryAttributeFunc( id );
    }

    switch( id )
    {
    case SCT_MEMATTRIB_TOTAL_SYSTEM_MEMORY:
        return 2000;
        
    case SCT_MEMATTRIB_USED_SYSTEM_MEMORY:
        return 1500;
        
    case SCT_MEMATTRIB_TOTAL_GRAPHICS_MEMORY:
        return 1000;
        
    case SCT_MEMATTRIB_USED_GRAPHICS_MEMORY:
        return 500;
        
    case SCT_MEMATTRIB_USED_PRIVATE_GRAPHICS_MEMORY:
        return 270;
        
    case SCT_MEMATTRIB_USED_SHARED_GRAPHICS_MEMORY:
        return 230;
        
    default:
        assert( 0 );
        break;
    }

    return 0;
}


unsigned long siCommonGetTimerFrequency( void* context )
{
    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    return timerFrequency;
}


unsigned long siCommonGetTimerTick( void* context )
{
    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    if( timerFunc )
    {
        return timerFunc();
    }
    
    timerTick += timerFrequency;
    
	return timerTick;
}

void* siCommonGetSingleton( void* context )
{
    assert( systemInitialized );
    ASSERT_CONTEXT( context );
    return singleton;
}

void siCommonSetSingleton( void* context, void* ptr )
{
    assert( systemInitialized );
    ASSERT_CONTEXT( context );
    singleton = ptr;
}

void siCommonDebugPrintf( void* context, const char* fmt, ... )
{
	va_list ap;
    
    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    SCT_ASSERT_ALWAYS( fmt != NULL );

	va_start( ap, fmt );
	vprintf( fmt, ap );
	va_end( ap );    

    printf( "\n" );
}

void siCommonWarningMessage( void* context, const char* wm )
{
    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    if( wm == NULL )
    {
        wm = "";
    }

    if( errorStream != NULL )
    {
        fprintf( errorStream, "Warning message (%s)\n", wm);
        fflush( errorStream );
    }

    assert( strlen( wm ) < SCT_ARRAY_LENGTH( warningMessage ) );
    strcpy( warningMessage, wm );
}

void siCommonErrorMessage( void* context, const char* em )
{
    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    if( em == NULL )
    {
        em = "";
    }

    if( errorStream != NULL )
    {
        fprintf( errorStream, "Error message (%s)\n", em);
        fflush( errorStream );
    }

    assert( strlen( em ) < SCT_ARRAY_LENGTH( errorMessage ) );
    strcpy( errorMessage, em );
}

void siCommonProgressMessage( void* context, int totalBenchmarks, int currentBenchmark, int currentRepeat, const char* benchmarkName )
{
    assert( systemInitialized );
    ASSERT_CONTEXT( context );
    assert( totalBenchmarks > 0 );
    assert( currentBenchmark > 0 );
    assert( currentBenchmark <= totalBenchmarks );
    assert( benchmarkName != NULL );

    if( progressMessageFunc != NULL )
    {
        progressMessageFunc( totalBenchmarks, currentBenchmark, currentRepeat, benchmarkName );
    }
}


void siCommonAssert( void* context, void* exp, void* file, unsigned int line )
{
    ASSERT_CONTEXT( context );
#if _MSC_VER >= 1400
    _wassert( exp, file, line );
#else 
    _assert( exp, file, line );
#endif
}


void* siCommonMemoryAllocDbg( void* context, unsigned int size, const char* filename, int linenumber )
{
    int*    buf;

    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    allocCounter++;

    if( allocFailThreshold >= 0 && 
        allocCounter > allocFailThreshold && 
        ( allocFailCount < 0 || allocCounter <= ( allocFailThreshold + allocFailCount ) ) )
    {

        return NULL;
    }

    if( allocMemoryLimit > 0 &&
        ( allocMemory + ( int )size ) > allocMemoryLimit )
    {
        return NULL;
    }
    
    allocMemory += size;
    if( allocMemory > allocMax )
    {
        allocMax = allocMemory;
    }

    buf = ( int* )_malloc_dbg( size + sizeof( int ), _CLIENT_BLOCK, filename, linenumber );
    if( buf == NULL )
    {
        return NULL;
    }

    buf[ 0 ] = size;

    return &( buf[ 1 ] );
}

void* siCommonMemoryReallocDbg( void* context, void* data, unsigned int size, const char* filename, int linenumber )
{
    int         oldsize         = 0;
    int*        p               = ( int* )( data );
    void*       buf;
    
    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    if( data == NULL )
    {
        return siCommonMemoryAllocDbg( context, size, filename, linenumber );
    }

    p = p - 1;
    oldsize = *p;
    assert( oldsize > 0 );

    buf = siCommonMemoryAllocDbg( context, size, filename, linenumber );
    if( buf == NULL )
    {
        siCommonMemoryFree( context, data );
        return NULL;
    }
    size = sctMin( size, oldsize );
    memcpy( buf, data, size );
    siCommonMemoryFree( context, data );

    return buf;
}

void siCommonMemoryFree( void* context, void* memblock )
{
    int  size;
    int* p = ( int* )memblock;

    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    if( memblock == NULL )
    {
		return;
    }

    p = p - 1;
    size = *p;
    assert( size > 0 );
    allocMemory -= size;

    _free_dbg( p, _CLIENT_BLOCK );
}


char siCommonReadChar( void* context )
{
    char c;

    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    if( inputPtr == NULL )
    {
        return SCT_END_OF_INPUT;
    }

    c = *inputPtr;
    inputPtr++;

    inputLength--;
    if( inputLength < 0 )
    {
        return SCT_END_OF_INPUT;
    }

    return c;
}


int siCommonWriteChar( void* context, char ch )
{
    int len;

    assert( systemInitialized );
    ASSERT_CONTEXT( context );

    writeCounter++;

    if( writeFailThreshold >= 0 &&
        writeCounter > writeFailThreshold &&
        ( writeFailCount < 0 || writeCounter <= ( writeFailThreshold + writeFailCount ) ) )
    {
       return 0;
    }

    len = outputPtr - outputBuffer;

    if( ( len + 1 ) >= outputBufferLength )
    {
        char* nbuf;

        
        nbuf = ( char* )malloc( outputBufferLength * 2 );

        assert( nbuf );

        memcpy( nbuf, outputBuffer, len );

        free( outputBuffer );

        outputBuffer = nbuf;
        outputBufferLength *= 2;
        outputPtr = outputBuffer + len;
    }
    
    *outputPtr++ = ch;

    return 1;
}

int siCommonFlushOutput( void* context )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );

    return 1;
}

SCTModuleList* siCommonGetModules( void* context )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );

    if( moduleListFunc == NULL )
    {
        return NULL;
    }
    else
    {
        return moduleListFunc();
    }
}

void siCommonSleep( void* context, unsigned long micros )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );

    ( void )micros;
}

void siCommonYield( void* context )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );
}

void siCommonGetTime( void* context, char* buffer, int length )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );
    assert( timeString != NULL );
    assert( strlen( timeString ) < ( unsigned int )length );

    strcpy( buffer, timeString );
}

void ( *siCommonGetProcAddress( void* context, const char* procname ) )()
{
    ASSERT_CONTEXT( context );
    SCT_ASSERT_ALWAYS( procname != NULL );
    return NULL;
}

int siCommonGetMessage( void* context, SCTMessage* message, long timeoutMillis )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );
    assert( message != NULL );

    ( void )( timeoutMillis );
    
    return -1;
}

void* siCommonSampleCpuLoad( void* context )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );

    return NULL;
}

float siCommonCalculateCpuLoad( void* context, void* sample, SCTCpuLoad load )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );

    assert( 0 );

    return -1.0f;
}

void siCommonDestroyCpuLoadSample( void* context, void* sample )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );

    assert( 0 );
}

void* siCommonCreateMutex( void* context )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );

    return ( void* )( 1 );
}

void siCommonUnlockMutex( void* context, void* mutex )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );

    assert( ( int )( mutex ) == 1 );
}

void siCommonLockMutex( void* context, void* mutex )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );

    assert( ( int )( mutex ) == 1 );
}

void siCommonDestroyMutex( void* context, void* mutex )
{
    ASSERT_CONTEXT( context );
    assert( systemInitialized );

    assert( ( int )( mutex ) == 1 );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
void siCommonProfileEvent( void* context, const char* event )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( event );
    /* TODO */
}

