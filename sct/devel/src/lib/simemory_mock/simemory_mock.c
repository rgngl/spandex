/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "simemory_mock.h"
#include "sct_utils.h"

#include <assert.h>

#define MAX_ALIGNMENT 0x1000000

static SiMemoryMockInput        gMockInput;
static int                      gMockStatus = 1;

static int sctiCalcAlignment( unsigned char** buffers, const int count );

/*!
 *
 *
 */
static int sctiCalcAlignment( unsigned char** buffers, const int count )
{
    unsigned char*      p;
    int                 a, i;
    
    if( buffers == NULL )
    {
        return 0;
    }

    p = buffers[ 0 ];

    for( a = MAX_ALIGNMENT; a > 0; a>>=1 )
    {
        if( ( int )p % a == 0 )
        {
            break;
        }
    }

    for( i = 0; i < count; ++i )
    {
        if( ( int )( buffers[ i ] ) % a != 0 )
        {
            return 0;
        }
    }

    return a;
}

/*!
 *
 *
 */
void tsiMemoryGetInput( SiMemoryMockInput* input )
{
    assert( input != NULL );
    *input = gMockInput;
}

/*!
 *
 *
 */
void tsiMemorySetStatus( int status )
{
    gMockStatus = status;
}

/*!
 *
 *
 */
int siMemoryBlockFillZ( void **buffers, const int bufferCount, const int lengthInBytes, int iterations )
{
    gMockInput.op                       = SIMEMORY_OP_FILLZ;
    gMockInput.destinationAlignment     = sctiCalcAlignment( ( unsigned char** )buffers, bufferCount );
    gMockInput.sourceAlignment          = 0;
    gMockInput.count                    = bufferCount;
    gMockInput.bytes                    = lengthInBytes;
    gMockInput.iterations               = iterations;

    return gMockStatus;
}

/*!
 *
 *
 */
int siMemoryBlockCopy( void **destinations, void **sources, const int bufferCount, const int lengthInBytes, int iterations )
{
    gMockInput.op                       = SIMEMORY_OP_COPY;
    gMockInput.destinationAlignment     = sctiCalcAlignment( ( unsigned char** )destinations, bufferCount );
    gMockInput.sourceAlignment          = sctiCalcAlignment( ( unsigned char** )sources, bufferCount );;
    gMockInput.count                    = bufferCount;
    gMockInput.bytes                    = lengthInBytes;
    gMockInput.iterations               = iterations;

    return gMockStatus;
}

