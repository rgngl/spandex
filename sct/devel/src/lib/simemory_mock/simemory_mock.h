/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SIMEMORY_MOCK_H__ )
#define __SIMEMORY_MOCK_H__

#include "sct_simemory.h"

typedef enum
{
    SIMEMORY_OP_INVALID = 0,
    SIMEMORY_OP_FILLZ,
    SIMEMORY_OP_COPY,
} SiMemoryMockMop;

typedef struct 
{
    SiMemoryMockMop     op;
    int                 destinationAlignment;
    int                 sourceAlignment;
    int                 count;
    int                 bytes;
    int                 iterations;
} SiMemoryMockInput;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void        tsiMemoryGetInput( SiMemoryMockInput* input );
    void        tsiMemorySetStatus( int status );
    
#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SIMEMORY_MOCK_H__ ) */
