/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_SYMBIANBITGDIHALBLITACTION_H__ )
#define __SCT_SYMBIANBITGDIHALBLITACTION_H__

#include "sct_types.h"
#include "sct_symbianbitgdimodule_parser.h"

#include <gdi.h>
#include <SVGRequestObserver.h>

class CWsBitmap;
//class CWsScreenDevice;
class CWindowGc;
//class RWindow;
class RWsSession;
class CSvgEngineInterfaceImpl;

/*!
 *
 *
 */
class CSVGHalBlitObserver : public CBase, public MSvgRequestObserver
{
public:
    CSVGHalBlitObserver();
		
    virtual void        UpdateScreen();
    virtual TBool       ScriptCall( const TDesC& aScript, CSvgElementImpl* aCallerElement );
    virtual TInt        FetchImage( const TDesC& aUri, RFs& aSession, RFile& aFileHandle );
    virtual TInt        FetchFont( const TDesC& aUri, RFs& aSession, RFile& aFileHandle );
    virtual void        GetSmilFitValue( TDes& aSmilValue );
    virtual void        UpdatePresentation(const TInt32&  aNoOfAnimation);
};

/*!
 *
 *
 */
typedef struct
{
    SCTBoolean                      initialized;
    SymbianBitGDIHalBlitActionData  data;
    CSVGHalBlitObserver*            observer;
    RWsSession*                     wsSession;
    RFs                             filesystem;
    CFbsBitmap*                     initBuffer;
    CFbsBitmap*                     buffer;
    CFbsBitmap*                     mask;
    CSvgEngineInterfaceImpl*        svgEngine;
    TInt                            domHandle;
    TPoint                          dest;
    TRect                           rect;
    HBufC*                          fileName;
    HBufC8*                         fileContents;
    TReal32                         zoomFactor;
    CWindowGc*                      windowGc;
} SCTSymbianBitGDIHalBlitActionContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                               sctiCreateSymbianBitGDIHalBlitActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroySymbianBitGDIHalBlitActionContext( void* context );

    SCTBoolean                          sctiSymbianBitGDIHalBlitActionInit( SCTAction *action, SCTBenchmark *benchmark );
    SCTBoolean                          sctiSymbianBitGDIHalBlitActionExecute( SCTAction* action, int frameNumber );
    void                                sctiSymbianBitGDIHalBlitActionTerminate( SCTAction* action );
    void                                sctiSymbianBitGDIHalBlitActionDestroy( SCTAction* action );
    SCTStringList*                      sctiSymbianBitGDIHalBlitActionGetWorkload( SCTAction *action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_SYMBIANBITGDIHALBLITACTION_H__ ) */
