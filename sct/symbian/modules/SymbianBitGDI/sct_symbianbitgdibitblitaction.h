/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_SYMBIANBITGDIBITBLIT_H__ )
#define __SCT_SYMBIANBITGDIBITBLIT_H__

#include "sct_types.h"
#include "sct_symbianbitgdimodule_parser.h"

#include <gdi.h>

class CWsBitmap;
class CWindowGc;
class RWsSession;
class CFbsBitmapDevice;
class CFbsBitGc;

/*!
 *
 *
 */
typedef struct
{
    SCTBoolean                      initialized;
    SymbianBitGDIBitblitActionData  data;
    int                             iterations;
    TDisplayMode                    sourceMode;
    TDisplayMode                    destinationMode;
    CWsBitmap*                      sourceBitmap;
    CWsBitmap*                      maskBitmap;
    CWsBitmap*                      destinationBitmap;
    CWindowGc*                      windowGc;
    RWsSession*                     wsSession;    
    CFbsBitmapDevice*               bitmapDevice;
    CFbsBitGc*                      bitGc;
} SCTSymbianBitGDIBitblitActionContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                               sctiCreateSymbianBitGDIBitblitActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroySymbianBitGDIBitblitActionContext( void* context );

    SCTBoolean                          sctiSymbianBitGDIBitblitActionInit( SCTAction *action, SCTBenchmark *benchmark );
    SCTBoolean                          sctiSymbianBitGDIBitblitActionExecute( SCTAction* action, int frameNumber );
    void                                sctiSymbianBitGDIBitblitActionTerminate( SCTAction* action );
    void                                sctiSymbianBitGDIBitblitActionDestroy( SCTAction* action );
    SCTStringList*                      sctiSymbianBitGDIBitblitActionGetWorkload( SCTAction *action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_SYMBIANBITGDIBITBLIT_H__ ) */
