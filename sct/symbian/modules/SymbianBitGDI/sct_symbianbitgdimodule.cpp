/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_symbianbitgdimodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_symbianbitgdimodule_parser.h"
#include "sct_symbianbitgdimodule_actions.h"

#include <stdio.h>
#include <string.h>

/* Local function declarations. */
SCTAttributeList*       sctiSymbianBitGDIModuleInfo( SCTModule* module );
SCTAction*              sctiSymbianBitGDICreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    sctiSymbianBitGDIDestroy( SCTModule* module );


/*!
 * Create symbian bitgdi module data structure which defines the name
 * of the module and sets up the module interface functions.
 *
 * \return Module structure for symbian bitgdi module, or NULL if failure.
 */
SCTModule* sctCreateSymbianBitGDIModule( void )
{
    SCTModule*                  module;

    /* Create module data structure. */
    module = sctCreateModule( "SymbianBitGDI",
                              NULL,                             /* NULL module context. */
#if defined( _WIN32 )
                              _SymbianBitGDI_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiSymbianBitGDIModuleInfo,
                              sctiSymbianBitGDICreateAction,
                              sctiSymbianBitGDIDestroy );

    return module;
}


/*!
 * Create attribute list containing symbian bitgdi module information.
 *
 * \param module Symbian bitgdi module. Must be defined.
 *
 * \return Symbian bitgdi information as attribute list, or NULL if
 * failure.
 */
SCTAttributeList* sctiSymbianBitGDIModuleInfo( SCTModule* module )
{
    SCTAttributeList*                   attributes;

    SCT_ASSERT_ALWAYS( module != NULL );

    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        return NULL;
    }

    return attributes;
}

/*!
 * Create symbian bitgdi module action. 
 *
 * \param module Symbian bitgdi module. Must be defined.
 * \param name Action name. Must be defined.
 * \param type Action type. Must be defined.
 * \param attributes List of action attributes. Must be defined.
 *
 * \return Action data structure, or NULL if failure.
 *
 */
SCTAction* sctiSymbianBitGDICreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    return sctCreateActionFromTemplate( name,
                                        type,
                                        "SymbianBitGDI",
                                        NULL,
                                        attributes,
                                        SymbianBitGDIActionTemplates,
                                        SCT_ARRAY_LENGTH( SymbianBitGDIActionTemplates ) );
}

/*!
 * Destroy symbian bitgdi module. This function does not deallocate
 * module data structure but instead simply does the needed module
 * cleanup, if any.
 *
 * \param module Module to destroy. Must be defined.
 */
void sctiSymbianBitGDIDestroy( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );

}




