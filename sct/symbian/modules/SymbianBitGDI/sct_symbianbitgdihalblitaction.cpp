/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_symbianbitgdihalblitaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#include "CPlatform.h"

#include <w32std.h>
#include <FBS.H>
#include <bitmap.h>
#include <coemain.h>
#include <SVGEngineInterfaceImpl.h>
#include <SVGRequestObserver.h>
#include <aknappui.h>
#include <aknviewappui.h>

#define SCT_HALBLIT_COMPRESS_BITMAP_TIMEOUT_MILLIS      30000 
#define SCT_HALBLIT_COMPRESS_BITMAP_WAIT_MILLIS         10 

#define SCT_S60_VERSION 30

#if defined ( __SERIES60_31__ )
# undef SCT_S60_VERSION
# define SCT_S60_VERSION 31
#endif  /* defined ( __SERIES60_31__ ) */

#if defined ( __SERIES60_32__ )
# undef SCT_S60_VERSION
# define SCT_S60_VERSION 32
#endif  /* defined ( __SERIES60_32__ ) */

/*!
 * Static helper funcs.
 *
 */
static int                      sctiGetFileSize( TDesC& fileName, RFs& fs );
static SCTBoolean               sctiLoadFile( TDesC& fileName, HBufC8* des, RFs& fs );
static TInt                     sctiWaitForCompress( CFbsBitmap* bitmap, TBitmapfileCompression expectedCompression, int timeoutMillis );

static TBitmapfileCompression   sctiGetExpectedBitmapCompression( EHalBlitExpectedBitmapCompression c );
static TBitmapfileCompression   sctiGetExpectedMaskCompression( EHalBlitExpectedMaskCompression c );

static const char*              sctiGetBitmapCompressionString( TBitmapfileCompression c );

static TDisplayMode             sctiSourceMode( EHalBlitSourceMode mode );

/*!
 *
 *
 */
void* sctiCreateSymbianBitGDIHalBlitActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTSymbianBitGDIHalBlitActionContext*   context;
    
    SCT_USE_VARIABLE( moduleContext );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    /* Create hal blit action context and zero memory. */
    context = ( SCTSymbianBitGDIHalBlitActionContext* )siCommonMemoryAlloc( NULL, sizeof( SCTSymbianBitGDIHalBlitActionContext ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in HalBlit@SymbianBitGDI action context creation" );
        return NULL;
    }
    memset( context, 0, sizeof( SCTSymbianBitGDIHalBlitActionContext ) );

    context->initialized = SCT_FALSE;

	context->observer = new CSVGHalBlitObserver();       /* Must use new */
	if( context->observer == NULL )
	{
        SCT_LOG_ERROR( "Allocation failed in HalBlit@SymbianBitGDI action context creation" );	
        sctiDestroySymbianBitGDIHalBlitActionContext( context );
        return NULL;
	}

    /* Parse hal blit action attributes. */
    if( sctiParseSymbianBitGDIHalBlitActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroySymbianBitGDIHalBlitActionContext( context );
        return NULL;
    }

    context->wsSession = &( CEikonEnv::Static()->WsSession() );
    context->windowGc  = &( CEikonEnv::Static()->SystemGc() );    

    SCT_ASSERT_ALWAYS( context->wsSession != NULL );
    SCT_ASSERT_ALWAYS( context->windowGc != NULL );    
    
    return context;
}

/*!
 *
 *
 */
void sctiDestroySymbianBitGDIHalBlitActionContext( void* context )
{
    SCTSymbianBitGDIHalBlitActionContext* c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTSymbianBitGDIHalBlitActionContext* )( context );

    SCT_ASSERT_ALWAYS( c->initBuffer    == NULL );
    SCT_ASSERT_ALWAYS( c->buffer        == NULL );
    SCT_ASSERT_ALWAYS( c->mask          == NULL );
    SCT_ASSERT_ALWAYS( c->svgEngine     == NULL );
    SCT_ASSERT_ALWAYS( c->domHandle     == 0 );
    SCT_ASSERT_ALWAYS( c->fileName      == NULL );
    SCT_ASSERT_ALWAYS( c->fileContents  == NULL );

	if( c->observer != NULL )
	{
		delete c->observer;       /* Must use delete */
		c->observer = NULL;
	}

    siCommonMemoryFree( NULL, context );
}


/*!
 *
 *
 */
SCTBoolean sctiSymbianBitGDIHalBlitActionInit( SCTAction *action, SCTBenchmark *benchmark )
{
    SCTSymbianBitGDIHalBlitActionContext*   context;
    int                                     err;
    char                                    buf[ 64 ];
    int                                     bitmapSize;
    int                                     maskSize;
    SEpocBitmapHeader                       hdr;
    int                                     width;
    int                                     height;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    /* Get hal blit action context. */
    context = ( SCTSymbianBitGDIHalBlitActionContext* )( action->context );
    SCT_ASSERT_ALWAYS( context != NULL );

    /* Check if the action is already initialized. */
    if( context->initialized == SCT_TRUE )
    {
        return SCT_TRUE;
    }

    /* Check. */
    SCT_ASSERT_ALWAYS( context->wsSession      != NULL );
    SCT_ASSERT_ALWAYS( context->windowGc       != NULL );    
    SCT_ASSERT_ALWAYS( context->initBuffer     == NULL );
    SCT_ASSERT_ALWAYS( context->buffer         == NULL );
    SCT_ASSERT_ALWAYS( context->mask           == NULL );
    SCT_ASSERT_ALWAYS( context->svgEngine      == NULL );
    SCT_ASSERT_ALWAYS( context->fileName       == NULL );
    SCT_ASSERT_ALWAYS( context->fileContents   == NULL );

    /* Convert c string filename to TDesC16 filename. */
    TPtrC8 d( ( TUint8* )( context->data.fileName ) );
    context->fileName = HBufC16::New( d.Length() );
    if( context->fileName == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }
    context->fileName->Des().Copy( d );

    /* Store file system connection. */
    if( context->filesystem.Connect() != KErrNone )
    {
        SCT_LOG_ERROR( "Cannot access filesystem in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }
    
    /* Get SVG file length. */
    int len = sctiGetFileSize( *( context->fileName ), context->filesystem );
    if( len  < 0 )
    {
        SCT_LOG_ERROR( "SVG file not readable in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    /* Reserve buffer for svg file contents. */
    context->fileContents = HBufC8::New( len );
    if( context->fileContents == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    /* Load svg file contents. */
    if( sctiLoadFile( *( context->fileName ), context->fileContents, context->filesystem ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "SVG file read failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    } 

    /* We create initial buffer for SVGt engine. This is not needed
     * for rendering but is just required by the SVGt engine. */
    TSize initBufferSize( 1, 1 );
    context->initBuffer = new CWsBitmap( *( context->wsSession ) );
    if( context->initBuffer == NULL || 
        context->initBuffer->Create( initBufferSize, EColor16M ) != KErrNone )
    {
        SCT_LOG_ERROR( "Init bitmap creation failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    /* Create SVG engine implementation. */
    TFontSpec fontSpec( _L( "LatinPlain12" ), 300 );
    TRAP( err, context->svgEngine = CSvgEngineInterfaceImpl::NewL( context->initBuffer,
                                                                   context->observer,
                                                                   fontSpec ) );
    if( err != KErrNone || context->svgEngine == NULL )
    {
        SCT_LOG_ERROR( "SVG engine creation failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    /* Set SVG rendering quality. */
    context->svgEngine->SetRenderQuality( KHighNoAA );

    /* Prepare DOM. */
    MSvgError* e = context->svgEngine->PrepareDom( *( context->fileContents ), context->domHandle );
    if( e->HasError() && !e->IsWarning() )
    {
        SCT_LOG_ERROR( "PrepareDom failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    e = context->svgEngine->UseDom( context->domHandle, context->initBuffer );
    if( e->HasError() && !e->IsWarning() )
    {
        SCT_LOG_ERROR( "UseDom failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    TSize sz;

    if( context->data.sourceWidth < 0 || context->data.sourceHeight < 0 )
    {
        /* Get size of the SVG icon. */
        sz = context->svgEngine->ContentDimensions();

        if( sz.iWidth <= 0 || sz.iHeight <= 0 )
        {
            SCT_LOG_ERROR( "Unknown SVG icon size in HalBlit@SymbianBitGDI action init" );
            sctiSymbianBitGDIHalBlitActionTerminate( action );
            return SCT_FALSE;
        }
    }
    else
    {
        sz.iWidth  = context->data.sourceWidth;
        sz.iHeight = context->data.sourceHeight;
    }

    CPlatform::ScreenSize( width, height );
    
    /* Scale the SVG content down if it is too large to fit the screen. */
    if( sz.iWidth > width || sz.iHeight > height )
    {
        float ratio = sctMaxf( sz.iWidth  / ( float )( width ), sz.iHeight / ( float )( height ) );
        sz.iWidth  = ( int )( sz.iWidth / ratio );
        sz.iHeight = ( int )( sz.iHeight / ratio );
    }

    context->svgEngine->SetSvgDimensionToFrameBuffer( sz.iWidth, sz.iHeight );

    /* Create framebuffer for SVGt engine */

    TDisplayMode sourceMode = sctiSourceMode( context->data.sourceMode );
    context->buffer = new CWsBitmap( *( context->wsSession ) );
    if( context->buffer == NULL || 
        context->buffer->Create( sz, sourceMode ) != KErrNone )
    {
        SCT_LOG_ERROR( "Bitmap creation failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    /* Create mask bitmap. */
    context->mask = new CWsBitmap( *( context->wsSession ) );
    if( context->mask == NULL || 
        context->mask->Create( sz, EGray256 ) != KErrNone )
    {
        SCT_LOG_ERROR( "Mask bitmap creation failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    /* Render DOM to buffer. */
    MSvgError* result;
    result = context->svgEngine->RenderDom( context->domHandle, context->buffer );
 
    if( result->HasError() && !result->IsWarning() )
    {
        SCT_LOG_ERROR( "RenderDom failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    /* Generate mask. */
    if( context->data.useMask == SCT_TRUE )
    {
        context->svgEngine->GenerateMask( context->mask );
    }

    /* Compress bitmap, if enabled. */
    if( context->data.compressBitmap == SCT_TRUE )
    {
        context->buffer->Compress();
    }

    bitmapSize = sctiWaitForCompress( context->buffer, 
                                      sctiGetExpectedBitmapCompression( context->data.expectedBitmapCompression ),
                                      SCT_HALBLIT_COMPRESS_BITMAP_TIMEOUT_MILLIS );
    if( bitmapSize < 0 )
    {
        SCT_LOG_ERROR( "Bitmap compression failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    /* Compress mask, if enabled. */
    if( context->data.compressMask == SCT_TRUE )
    {
        context->mask->Compress();
    }

    maskSize = sctiWaitForCompress( context->mask, 
                                    sctiGetExpectedMaskCompression( context->data.expectedMaskCompression ), 
                                    SCT_HALBLIT_COMPRESS_BITMAP_TIMEOUT_MILLIS );
    if( maskSize < 0 )
    {
        SCT_LOG_ERROR( "Mask compression failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    hdr = context->buffer->Header();

    if( sctAddNameValueToList( benchmark->attributes, "BitmapCompression", sctiGetBitmapCompressionString( hdr.iCompression ) ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Adding bitmap compression to benchmark failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    sprintf( buf, "%d", bitmapSize );
    if( sctAddNameValueToList( benchmark->attributes, "BitmapSize", buf ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Adding bitmap size to benchmark failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    hdr = context->mask->Header();

    if( sctAddNameValueToList( benchmark->attributes, "MaskCompression", sctiGetBitmapCompressionString( hdr.iCompression ) ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Adding mask compression to benchmark failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    sprintf( buf, "%d", maskSize );
    if( sctAddNameValueToList( benchmark->attributes, "MaskSize", buf ) == SCT_FALSE )
    {
        SCT_LOG_ERROR( "Adding mask size to benchmark failed in HalBlit@SymbianBitGDI action init" );
        sctiSymbianBitGDIHalBlitActionTerminate( action );
        return SCT_FALSE;
    }

    context->dest.iX     = context->data.destinationX;
    context->dest.iY     = context->data.destinationY;

    context->rect.iTl.iX = context->data.sourceX;
    context->rect.iTl.iY = context->data.sourceY;

    if( context->data.sourceWidth < 0 )
    {
        context->rect.iBr.iX = sz.iWidth;
    }
    else
    {
        context->rect.iBr.iX = context->data.sourceX + context->data.sourceWidth;
    }
    if( context->data.sourceHeight < 0 )
    {
        context->rect.iBr.iY = sz.iHeight;
    }
    else
    {
        context->rect.iBr.iY = context->data.sourceY + context->data.sourceHeight;
    }

     /* We are done, set action to initialized. */
    context->initialized = SCT_TRUE;

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiSymbianBitGDIHalBlitActionExecute( SCTAction* action, int frameNumber )
{
    SCTSymbianBitGDIHalBlitActionContext*       context;
    int                                         i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTSymbianBitGDIHalBlitActionContext* )( action->context );

    SCT_ASSERT( context->initialized == SCT_TRUE );

    /* Repeat. */
    for( i = 0; i < context->data.iterations; ++i )
    {
        /* Blit bitmap to window. */
        if( context->data.useMask == SCT_FALSE )
        {
            context->windowGc->BitBlt( context->dest, context->buffer, context->rect );
        }
        else
        {
            context->windowGc->BitBltMasked( context->dest, context->buffer, context->rect, context->mask, ETrue );
        }
        if( context->data.flush == SCT_TRUE )
        {
            context->wsSession->Flush();
        }
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiSymbianBitGDIHalBlitActionTerminate( SCTAction* action )
{
    SCTSymbianBitGDIHalBlitActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTSymbianBitGDIHalBlitActionContext* )( action->context );
    
    if( context->svgEngine != NULL )
    {
        if( context->domHandle != 0 )
        {
            context->svgEngine->DeleteDom( context->domHandle );
            context->domHandle = 0;
        }
        context->svgEngine->Destroy();
        delete context->svgEngine;
        context->svgEngine = NULL;
    }

    delete context->initBuffer;
    context->initBuffer = NULL;

    delete context->buffer;
    context->buffer = NULL;
    
    delete context->mask;
    context->mask = NULL;

    delete context->fileName;
    context->fileName = NULL;

    delete context->fileContents;
    context->fileContents = NULL;

    context->filesystem.Close();

    context->initialized = SCT_FALSE;
}


/*!
 *
 *
 */
void sctiSymbianBitGDIHalBlitActionDestroy( SCTAction* action )
{
    SCTSymbianBitGDIHalBlitActionContext*       context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTSymbianBitGDIHalBlitActionContext* )( action->context );

    sctiDestroySymbianBitGDIHalBlitActionContext( context );
}


/*!
 *
 *
 */
SCTStringList* sctiSymbianBitGDIHalBlitActionGetWorkload( SCTAction *action )
{
    SCTSymbianBitGDIHalBlitActionContext*       context;
    SCTStringList*                              result;
    char                                        buf[ 32 ];

    SCT_ASSERT_ALWAYS( action != NULL );
    
    context = ( SCTSymbianBitGDIHalBlitActionContext* )( action->context );
    SCT_ASSERT_ALWAYS( context != NULL );
    
    result = sctCreateStringList();
    if( result == NULL )
    {
        return NULL;
    }

    sprintf( buf, "%d bitmaps", context->data.iterations );
    if( sctAddStringCopyToList( result, buf ) == SCT_FALSE )
    {
        sctDestroyStringList( result );
        return NULL;
    }

    return result;
}


/*!
 *
 *
 */
static TBitmapfileCompression sctiGetExpectedBitmapCompression( EHalBlitExpectedBitmapCompression c )
{
    switch( c )
    {
    case HALBLIT_EXPECTEDBITMAPCOMPRESSION_ENOBITMAPCOMPRESSION:
        return ENoBitmapCompression;

    case HALBLIT_EXPECTEDBITMAPCOMPRESSION_EBYTERLECOMPRESSION:
        return EByteRLECompression;

    case HALBLIT_EXPECTEDBITMAPCOMPRESSION_ETWELVEBITRLECOMPRESSION:
        return ETwelveBitRLECompression;

    case HALBLIT_EXPECTEDBITMAPCOMPRESSION_ESIXTEENBITRLECOMPRESSION:
        return ESixteenBitRLECompression;

    case HALBLIT_EXPECTEDBITMAPCOMPRESSION_ETWENTYFOURBITRLECOMPRESSION:
        return ETwentyFourBitRLECompression;

    case HALBLIT_EXPECTEDBITMAPCOMPRESSION_ETHIRTYTWOUBITRLECOMPRESSION:
        return EThirtyTwoUBitRLECompression;

#if SCT_S60_VERSION > 30
    case HALBLIT_EXPECTEDBITMAPCOMPRESSION_EGENERICPALETTECOMPRESSION:
        return EGenericPaletteCompression;
#endif  /* SCT_S60_VERSION > 30 */

    default:
    case HALBLIT_EXPECTEDBITMAPCOMPRESSION_ANYBITMAPCOMPRESSION:
        return ERLECompressionLast;
    }
}

/*!
 *
 *
 */
static TBitmapfileCompression sctiGetExpectedMaskCompression( EHalBlitExpectedMaskCompression c )
{
    switch( c )
    {
    case HALBLIT_EXPECTEDMASKCOMPRESSION_ENOBITMAPCOMPRESSION:
        return ENoBitmapCompression;

    case HALBLIT_EXPECTEDMASKCOMPRESSION_EBYTERLECOMPRESSION:
        return EByteRLECompression;

    case HALBLIT_EXPECTEDMASKCOMPRESSION_ETWELVEBITRLECOMPRESSION:
        return ETwelveBitRLECompression;

    case HALBLIT_EXPECTEDMASKCOMPRESSION_ESIXTEENBITRLECOMPRESSION:
        return ESixteenBitRLECompression;

    case HALBLIT_EXPECTEDMASKCOMPRESSION_ETWENTYFOURBITRLECOMPRESSION:
        return ETwentyFourBitRLECompression;

    case HALBLIT_EXPECTEDMASKCOMPRESSION_ETHIRTYTWOUBITRLECOMPRESSION:
        return EThirtyTwoUBitRLECompression;

#if SCT_S60_VERSION > 30
    case HALBLIT_EXPECTEDMASKCOMPRESSION_EGENERICPALETTECOMPRESSION:
        return EGenericPaletteCompression;
#endif  /* SCT_S60_VERSION > 30 */

    default:
    case HALBLIT_EXPECTEDMASKCOMPRESSION_ANYBITMAPCOMPRESSION:
        return ERLECompressionLast;
    }
}

/*!
 *
 *
 */
static const char* sctiGetBitmapCompressionString( TBitmapfileCompression c )
{
    switch( c )
    {
    case ENoBitmapCompression:
        return "ENoBitmapCompression";

    case EByteRLECompression:
        return "EByteRLECompression";

    case ETwelveBitRLECompression:
        return "ETwelveBitRLECompression";

    case ESixteenBitRLECompression:
        return "ESixteenBitRLECompression";

    case ETwentyFourBitRLECompression:
        return "ETwentyFourBitRLECompression";

    case EThirtyTwoUBitRLECompression:
        return "EThirtyTwoUBitRLECompression";

#if SCT_S60_VERSION > 30
    case EGenericPaletteCompression:
        return "EGenericPaletteCompression";
#endif  /* SCT_S60_VERSION > 30 */

    default:
        return "Unknown bitmap compression";
    }
}

/*!
 *
 *
 */
static int sctiGetFileSize( TDesC& fileName, RFs& fs )
{
    RFile           f;
    TInt            size        = -1;

    if( f.Open( fs, fileName, EFileRead | EFileStream | EFileShareReadersOnly ) == KErrNone )
    {
        if( f.Size( size ) != KErrNone )
        {
            size = -1;
        }
        f.Close();
    }


    return size;
}

/*!
 *
 *
 */
static SCTBoolean sctiLoadFile( TDesC& fileName, HBufC8* buf, RFs& fs )
{
    RFile               f;
    SCTBoolean          status          = SCT_FALSE;
    TPtr8               des             = buf->Des();

    if( f.Open( fs, fileName, EFileRead | EFileStream | EFileShareReadersOnly ) == KErrNone )
    {
    	TInt len = des.MaxSize();
    	TInt err = f.Read( des, len );
        if( err == KErrNone )
        {
            status = SCT_TRUE;
        }
        
        f.Close();
    }

    return status;
}


/*!
 *
 *
 */
static TInt sctiWaitForCompress( CFbsBitmap* bitmap, TBitmapfileCompression expectedCompression, int timeoutMillis )
{
    SCT_ASSERT( bitmap != NULL );

    TUint32             startTime = User::NTickCount();
    TUint32             endTime   = startTime + timeoutMillis;
    SEpocBitmapHeader   hdr;
    
    while( 1 )
    {
        hdr = bitmap->Header();
        
        if( hdr.iCompression == expectedCompression ||  /* expect no compression */
            hdr.iCompression != ENoBitmapCompression || /* some compression done */
            User::NTickCount() < endTime )              /* timeout */
        {
            break;
        }
        User::After( SCT_HALBLIT_COMPRESS_BITMAP_WAIT_MILLIS * 1000 );
    }

    if( ( expectedCompression == ERLECompressionLast && hdr.iCompression == ENoBitmapCompression ) ||   /* any compression */
        ( expectedCompression != ERLECompressionLast && hdr.iCompression != expectedCompression ) )     /* unexpected compression */
    {
        return -1;
    }

    return hdr.iBitmapSize;
}


/* ************************************************************ */
/*!
 *
 *
 */
static TDisplayMode sctiSourceMode( EHalBlitSourceMode mode )
{
    switch( mode )
    {
    case HALBLIT_SOURCEMODE_ECOLOR64K:
        return EColor64K;
    case HALBLIT_SOURCEMODE_ECOLOR16M:
        return EColor16M;
    case HALBLIT_SOURCEMODE_ECOLOR16MU:
        return EColor16MU;
#if defined( EKA2 )
    case HALBLIT_SOURCEMODE_ECOLOR16MA:
        return EColor16MA;
#endif  /* defined( EKA2 ) */
    default:
        return ENone;
    };
}


/* -------------------------------------------------- */

/*!
 *
 *
 */
CSVGHalBlitObserver::CSVGHalBlitObserver()
{
}


/*!
 *
 *
 */
void CSVGHalBlitObserver::UpdateScreen()
{
}

/*!
 *
 *
 */
TBool CSVGHalBlitObserver::ScriptCall( const TDesC& /*aScript*/, CSvgElementImpl* /*aCallerElement*/ )
{
    return EFalse;
}

/*!
 *
 *
 */
TInt CSVGHalBlitObserver::FetchImage( const TDesC& /*aUri*/, RFs& /*aSession*/, RFile& /*aFileHandle*/ )
{
    return KErrNone;
}

/*!
 *
 *
 */
TInt CSVGHalBlitObserver::FetchFont( const TDesC& /*aUri*/, RFs& /*aSession*/, RFile& /*aFileHandle*/ )
{
    return KErrNone;
}

/*!
 *
 *
 */
void CSVGHalBlitObserver::GetSmilFitValue( TDes& /*aSmilValue*/ )
{
}

/*!
 *
 *
 */
void CSVGHalBlitObserver::UpdatePresentation( const TInt32& /*aNoOfAnimation*/ )
{
}


