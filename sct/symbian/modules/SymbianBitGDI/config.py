#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
SetModuleName( 'SymbianBitGDI' )

######################################################################
AddInclude( 'sct_symbianbitgdimodule.h' )

######################################################################
AddActionConfig( 'Bitblit',
                 'Init'+'Execute'+'Terminate'+'Destroy'+'GetWorkload',
                 [ IntAttribute(        'Iterations', 1 ),
                   AutoEnumAttribute(   'SourceMode', [ 'EColor64K',
                                                        'EColor16M',
                                                        'EColor16MU',
                                                        'EColor16MA' ] ),
                   IntAttribute(        'SourceWidth', 1 ),
                   IntAttribute(        'SourceHeight', 1 ),
                   OnOffAttribute(      'CompressSource' ),
                   AutoEnumAttribute(   'Mask', [ 'OFF', 'BINARY', 'ALPHA' ] ),
                   IntAttribute(        'MaskWidth', 1 ),
                   IntAttribute(        'MaskHeight', 1 ),
                   FloatAttribute(      'MaskValueStart', 0, 1 ),
                   FloatAttribute(      'MaskValueEnd', 0, 1 ),                                      
                   IntAttribute(        'MaskGradientStartX', 0 ),
                   IntAttribute(        'MaskGradientEndX', 0 ),
                   IntAttribute(        'MaskGradientStartY', 0 ),
                   IntAttribute(        'MaskGradientEndY', 0 ),                   
                   OnOffAttribute(      'CompressMask' ),              
                   AutoEnumAttribute(   'DestinationSurface', [ 'BITMAP', 'WINDOW' ] ),
                   AutoEnumAttribute(   'DestinationMode', [ 'ENone',
                                                             'EColor64K',
                                                             'EColor16M',
                                                             'EColor16MU',
                                                             'EColor16MA' ] ),
                   IntAttribute(        'DestinationX', 0 ),
                   IntAttribute(        'DestinationY', 0 ),
                   IntAttribute(        'DestinationWidth', 1 ),
                   IntAttribute(        'DestinationHeight', 1 ),
                   OnOffAttribute(      'CompressDestination' ),
                   OnOffAttribute(      'ClearDestination' ),
                   OnOffAttribute(      'Fading' ),
                   OnOffAttribute(      'Flush' ) ],
                 [ ValidatorConfig( 'DestinationSurface ==  BITBLIT_DESTINATIONSURFACE_BITMAP && DestinationMode == BITBLIT_DESTINATIONMODE_ENONE',
                                    'Destination mode must not be ENone with destination surface BITMAP' ),
                   ValidatorConfig( 'DestinationSurface ==  BITBLIT_DESTINATIONSURFACE_WINDOW && CompressDestination == SCT_TRUE',
                                    'Destination compress not supported with destination surface WINDOW' ),
                   ValidatorConfig( 'MaskGradientStartX > MaskGradientEndX || MaskGradientStartY > MaskGradientEndY',
                                    'Mask gradient end must be larger or equal to mask gradient start' ),
                   ValidatorConfig( 'MaskGradientStartX > ( MaskWidth / 2 ) || MaskGradientStartY > ( MaskHeight / 2 ) || MaskGradientEndX > ( MaskWidth / 2 ) || MaskGradientEndY > ( MaskHeight / 2 )',
                                    'Mask gradient start or end out of range' ),
                   ValidatorConfig( 'SourceMode == BITBLIT_SOURCEMODE_ECOLOR16MA && Mask != BITBLIT_MASK_ALPHA && Mask != BITBLIT_MASK_OFF',
                                    'Mask must be ALPHA with EColor16MA source type' ),
                   ValidatorConfig( 'SourceMode == BITBLIT_SOURCEMODE_ECOLOR16MA && Mask == BITBLIT_MASK_ALPHA && ( SourceWidth != MaskWidth || SourceHeight != MaskHeight )',
                                    'Mask size must equal source size with EColor16MA source type' ) ]
                 )

######################################################################
AddActionConfig( 'HalBlit',
                 'Init'+'Execute'+'Terminate'+'Destroy'+'GetWorkload',
                 [ IntAttribute(        'Iterations', 1 ),
                   StringAttribute(     'FileName',                     256 ),
                   AutoEnumAttribute(   'SourceMode', [ 'EColor64K',
                                                        'EColor16M',
                                                        'EColor16MU',
                                                        'EColor16MA' ] ),                 
                   OnOffAttribute(      'CompressBitmap' ),
                   AutoEnumAttribute(   'ExpectedBitmapCompression',    [ 'ENoBitmapCompression', 'EByteRLECompression', 'ETwelveBitRLECompression', 'ESixteenBitRLECompression', 'ETwentyFourBitRLECompression', 'EThirtyTwoUBitRLECompression', 'EThirtyTwoABitRLECompression', 'EGenericPaletteCompression', 'AnyBitmapCompression' ] ),
                   OnOffAttribute(      'UseMask' ),
                   OnOffAttribute(      'CompressMask' ),
                   AutoEnumAttribute(   'ExpectedMaskCompression',      [ 'ENoBitmapCompression', 'EByteRLECompression', 'ETwelveBitRLECompression', 'ESixteenBitRLECompression', 'ETwentyFourBitRLECompression', 'EThirtyTwoUBitRLECompression', 'EThirtyTwoABitRLECompression', 'EGenericPaletteCompression', 'AnyBitmapCompression' ] ),
                   IntAttribute(        'SourceX', 0 ),
                   IntAttribute(        'SourceY', 0 ),
                   IntAttribute(        'SourceWidth', -1 ),
                   IntAttribute(        'SourceHeight', -1 ),
                   IntAttribute(        'DestinationX', 0 ),
                   IntAttribute(        'DestinationY', 0 ),
                   OnOffAttribute(      'Flush' ) ],
                 [ ValidatorConfig( 'CompressBitmap == SCT_FALSE && ExpectedBitmapCompression != HALBLIT_EXPECTEDBITMAPCOMPRESSION_ENOBITMAPCOMPRESSION',
                                    'Expectedbitmapcompression must be ENoBitmapCompression when CompressBitmap is OFF' ),
                   ValidatorConfig( 'UseMask == SCT_FALSE && CompressMask != SCT_FALSE',
                                    'CompressMask must be OFF when UseMask is OFF' ),
                   ValidatorConfig( 'CompressMask == SCT_FALSE && ExpectedMaskCompression != HALBLIT_EXPECTEDMASKCOMPRESSION_ENOBITMAPCOMPRESSION',
                                    'ExpectedMaskCompression must be ENoBitmapCompression when CompressMask is OFF' )
                   ]
                 )
