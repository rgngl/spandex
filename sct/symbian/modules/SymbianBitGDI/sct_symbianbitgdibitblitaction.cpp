/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_symbianbitgdibitblitaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#include "CPlatform.h"

#include <w32std.h>
#include <coemain.h>
#include <aknappui.h>
#include <aknviewappui.h>

/*!
 *
 *
 */
static TDisplayMode     sctiSourceMode( EBitblitSourceMode mode );
static TDisplayMode     sctiDestinationMode( EBitblitDestinationMode mode );
static void             sctiInitializeSourceBitmap( CWsBitmap& bitmap, float maskValueStart, float maskValueEnd, int maskGradientStartX, int maskGradientEndX, int maskGradientStartY, int maskGradientEndY );
static void             sctiInitializeMaskBitmap( CWsBitmap& bitmap, float maskValueStart, float maskValueEnd, int maskGradientStartX, int maskGradientEndX, int maskGradientStartY, int maskGradientEndY );
static unsigned char    sctiGetAlpha( int x, int y, int width, int height, int maskGradientStartX, int maskGradientEndX, int maskGradientStartY, int maskGradientEndY, float maskValueStart, float maskValueEnd );

/*!
 *
 *
 */
void* sctiCreateSymbianBitGDIBitblitActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTSymbianBitGDIBitblitActionContext*   context;
    SymbianBitGDIBitblitActionData*         data;
    
    SCT_ASSERT_ALWAYS( moduleContext == NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    /* Create bitblit action context and zero memory. */
    context = ( SCTSymbianBitGDIBitblitActionContext* )siCommonMemoryAlloc( NULL, sizeof( SCTSymbianBitGDIBitblitActionContext ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Bitblit@SymbianBitGDI action context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTSymbianBitGDIBitblitActionContext ) );

    data = &( context->data );

    /* Parse bitblit action attributes. */
    if( sctiParseSymbianBitGDIBitblitActionAttributes( data, attributes ) == SCT_FALSE )
    {
        siCommonMemoryFree( NULL, context );
        return NULL;
    }

    context->initialized        = SCT_FALSE;

    context->sourceMode          = sctiSourceMode( data->sourceMode );
    context->destinationMode     = sctiDestinationMode( data->destinationMode );

    context->wsSession = &( CEikonEnv::Static()->WsSession() );
    context->windowGc  = &( CEikonEnv::Static()->SystemGc() );    

    SCT_ASSERT_ALWAYS( context->wsSession != NULL );
    SCT_ASSERT_ALWAYS( context->windowGc != NULL );    
   
    if( data->destinationSurface == BITBLIT_DESTINATIONSURFACE_WINDOW )
    {
        int width;
        int height;

        CPlatform::ScreenSize( width, height );        
        if( width < data->destinationWidth || height < data->destinationHeight )
        {
            SCT_LOG_ERROR( "Destination size exceeds screen size in Bitblit@SymbianBitGDI action" );
            siCommonMemoryFree( NULL, context );
            return NULL;
        }
    }
        
    /* Generate warning if destination surface is window and
     * destination mode is not the same as window mode. */
    if( data->destinationSurface == BITBLIT_DESTINATIONSURFACE_WINDOW && context->destinationMode != ENone )
    {
        TDisplayMode displayMode;
        
        displayMode = CPlatform::ScreenMode();
        if( displayMode != context->destinationMode )
        {
            sctLogWarning( "Incompatible destination mode ignored in Bitblit@SymbianBitGDI action" );
        }
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroySymbianBitGDIBitblitActionContext( void* context )
{
    SCTSymbianBitGDIBitblitActionContext*   c;

    if( context == NULL )
    {
        return;
    }
    
    c = ( SCTSymbianBitGDIBitblitActionContext* )( context );

    delete c->sourceBitmap;
    c->sourceBitmap = NULL;

    delete c->maskBitmap;
    c->maskBitmap = NULL;

    delete c->destinationBitmap;
    c->destinationBitmap = NULL;

    siCommonMemoryFree( NULL, c );
}

/*!
 *
 *
 */
SCTBoolean sctiSymbianBitGDIBitblitActionInit( SCTAction *action, SCTBenchmark *benchmark )
{
    SCTSymbianBitGDIBitblitActionContext*   context;
    SymbianBitGDIBitblitActionData*         data;
    TInt                                    width;
    TInt                                    height;
    
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    /* Get bitblit action context. */
    context = ( SCTSymbianBitGDIBitblitActionContext* )( action->context );
    SCT_ASSERT_ALWAYS( context != NULL );

    data = &( context->data );

    /* Check if the action is already initialized. */
    if( context->initialized == SCT_TRUE )
    {
        return SCT_TRUE;
    }

    /* Debug check. */
    SCT_ASSERT( context->sourceBitmap == NULL );
    SCT_ASSERT( context->maskBitmap == NULL );
    SCT_ASSERT( context->destinationBitmap == NULL );
    SCT_ASSERT( context->windowGc == NULL );
    SCT_ASSERT( context->bitmapDevice == NULL );
    SCT_ASSERT( context->bitGc == NULL );

    CPlatform::ScreenSize( width, height );

    /* Create source bitmap. */
    TSize sourceSize( data->sourceWidth, data->sourceHeight );
    context->sourceBitmap = new CWsBitmap( *( context->wsSession ) );
    if( context->sourceBitmap == NULL || 
        context->sourceBitmap->Create( sourceSize, context->sourceMode ) != KErrNone )
    {
        SCT_LOG_ERROR( "Source bitmap creation failed in Bitblit@SymbianBitGDI action init" );
        return SCT_FALSE;
    }

    /* Set source bitmap content. */
    sctiInitializeSourceBitmap( *( context->sourceBitmap ), 
                                data->maskValueStart, 
                                data->maskValueEnd,
                                data->maskGradientStartX,
                                data->maskGradientEndX,
                                data->maskGradientStartY,
                                data->maskGradientEndY );

    /* Compress source bitmap. */
    if( data->compressSource == SCT_TRUE )
    {
        context->sourceBitmap->Compress();
    }

    /* Create mask bitmap, if needed. */
    if( data->mask != BITBLIT_MASK_OFF )
    {
        TSize           maskSize( data->maskWidth, data->maskHeight );
        TDisplayMode    maskMode;

        /* Decide mask bitmap mode. */
        if( data->mask == BITBLIT_MASK_BINARY )
        {
            maskMode = EGray2;
        }
        else
        {
            maskMode = EGray256;
        }

        /* Create mask bitmap. */
        context->maskBitmap = new CWsBitmap( *( context->wsSession ) );
        if( context->maskBitmap == NULL || context->maskBitmap->Create( maskSize, maskMode ) != KErrNone )
        {
            SCT_LOG_ERROR( "Mask bitmap creation failed in Bitblit@SymbianBitGDI action init" );
            return SCT_FALSE;
        }

        /* Set mask bitmap content. */
        sctiInitializeMaskBitmap( *( context->maskBitmap ), 
                                  data->maskValueStart,
                                  data->maskValueEnd,
                                  data->maskGradientStartX,
                                  data->maskGradientEndX,
                                  data->maskGradientStartY,
                                  data->maskGradientEndY );

        /* Compress mask bitmap. */
        if( data->compressMask == SCT_TRUE )
        {
            context->maskBitmap->Compress();
        }
    }

    /* Create destination bitmap (and associated bitmap device and
     * bitGc), if needed. */
    if( data->destinationSurface == BITBLIT_DESTINATIONSURFACE_BITMAP )
    {
        TSize   destinationSize( data->destinationWidth, data->destinationHeight );

        /* Create destination bitmap. */
        context->destinationBitmap = new CWsBitmap( *( context->wsSession ) );
        if( context->destinationBitmap == NULL || 
            context->destinationBitmap->Create( destinationSize, context->destinationMode ) != KErrNone )
        {
            SCT_LOG_ERROR( "Destination bitmap creation failed in Bitblit@SymbianBitGDI action init" );
            return SCT_FALSE;
        }

        /* Compress destination bitmap. */
        if( data->compressDestination == SCT_TRUE )
        {
            context->destinationBitmap->Compress();
        }

        /* Create bitmap device. */
        int err;
        TRAP( err, context->bitmapDevice = CFbsBitmapDevice::NewL( context->destinationBitmap ) );
        if( context->bitmapDevice == NULL || err != KErrNone )
        {
            SCT_LOG_ERROR( "Bitmap device creation failed in Bitblit@SymbianBitGDI action init" );
            return SCT_FALSE;
        }

        if( context->bitmapDevice->GraphicsAccelerator() == NULL )
        {
            sctLogWarning( "Bitmap device not accelerated" );
        }

        /* Create bitmap context. */
        if( context->bitmapDevice->CreateContext( context->bitGc ) != KErrNone )
        {
            SCT_LOG_ERROR( "Bitmap context creation failed in Bitblit@SymbianBitGDI action init" );
            return SCT_FALSE;
        }
       
        /* Set fading. */
        if( data->fading == SCT_TRUE )
        {
            context->bitGc->SetFaded( ETrue );
        }
        else
        {
            context->bitGc->SetFaded( EFalse );
        }
    }
    else
    {
        /* Set fading. */
        if( data->fading == SCT_TRUE )
        {
            context->windowGc->SetFaded( ETrue );
        }
        else
        {
            context->windowGc->SetFaded( EFalse );
        }
    }

    /* We are done, set action to initialized. */
    context->initialized = SCT_TRUE;

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiSymbianBitGDIBitblitActionExecute( SCTAction* action, int frameNumber )
{
    SCTSymbianBitGDIBitblitActionContext*   context;
    SymbianBitGDIBitblitActionData*         data;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTSymbianBitGDIBitblitActionContext* )( action->context );
    data = &( context->data );

    SCT_ASSERT( context->initialized == SCT_TRUE );

    TPoint      origin( data->destinationX, data->destinationY );
    TSize       destinationSize( data->destinationWidth, data->destinationHeight );
    int         i;

    SCT_ASSERT( data->mask == BITBLIT_MASK_OFF || context->maskBitmap != NULL );

    if( data->clearDestination == SCT_TRUE )
    {
        if( data->destinationSurface == BITBLIT_DESTINATIONSURFACE_WINDOW )
        {
            context->windowGc->Clear();
        }
        else
        {
            context->bitGc->Clear();
        }
    }

    /* Blit. */
    for( i = 0; i < data->iterations; ++i )
    {
        if( data->destinationSurface == BITBLIT_DESTINATIONSURFACE_WINDOW )
        {
            if( context->maskBitmap == NULL )
            {
                context->windowGc->BitBlt( origin, 
                                           context->sourceBitmap,
                                           destinationSize );
            }
            else
            {
                context->windowGc->BitBltMasked( origin, 
                                                 context->sourceBitmap,
                                                 destinationSize, 
                                                 context->maskBitmap, 
                                                 true );
            }
        }
        else
        {
            if( context->maskBitmap == NULL )
            {
                context->bitGc->BitBlt( origin, 
                                        context->sourceBitmap,
                                        destinationSize );
            }
            else
            {
                context->bitGc->BitBltMasked( origin, 
                                              context->sourceBitmap,
                                              destinationSize, 
                                              context->maskBitmap, 
                                              true );
            }
        }
    }

    if( data->flush == SCT_TRUE )
    {
        context->wsSession->Flush();
    }
        
    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiSymbianBitGDIBitblitActionTerminate( SCTAction* action )
{
    SCTSymbianBitGDIBitblitActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( action->context != NULL );

    context = ( SCTSymbianBitGDIBitblitActionContext* )( action->context );

    delete context->sourceBitmap; 
    context->sourceBitmap = NULL;

    delete context->maskBitmap; 
    context->maskBitmap = NULL;

    delete context->destinationBitmap;
    context->destinationBitmap = NULL;

    delete context->bitGc;
    context->bitGc = NULL;

    delete context->bitmapDevice;
    context->bitmapDevice = NULL;

    context->initialized = SCT_FALSE;
}

/*!
 *
 *
 */
void sctiSymbianBitGDIBitblitActionDestroy( SCTAction* action )
{
    SCTSymbianBitGDIBitblitActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTSymbianBitGDIBitblitActionContext* )( action->context );
    if( context != NULL )
    {
        siCommonMemoryFree( NULL, context );
    }
}

/*!
 *
 *
 */
SCTStringList* sctiSymbianBitGDIBitblitActionGetWorkload( SCTAction *action )
{
    SCTSymbianBitGDIBitblitActionContext*   context;
    SCTStringList*                          result;
    char                                    buf[ 32 ];
    int                                     pixels;
    int                                     w, h;

    SCT_ASSERT_ALWAYS( action != NULL );
    
    context = ( SCTSymbianBitGDIBitblitActionContext* )( action->context );
    SCT_ASSERT_ALWAYS( context != NULL );
    
    result = sctCreateStringList();
    if( result == NULL )
    {
        return NULL;
    }

    w = sctMin( context->data.sourceWidth, context->data.destinationWidth );
    h = sctMin( context->data.sourceHeight, context->data.destinationHeight );

    pixels = w * h * context->data.iterations;
    sprintf( buf, "%d Pixels", pixels );
    if( sctAddStringCopyToList( result, buf ) == SCT_FALSE )
    {
        sctDestroyStringList( result );
        return NULL;
    }

    return result;
}

/* ************************************************************ */
/*!
 *
 *
 */
static TDisplayMode sctiSourceMode( EBitblitSourceMode mode )
{
    switch( mode )
    {
    case BITBLIT_SOURCEMODE_ECOLOR64K:
        return EColor64K;
    case BITBLIT_SOURCEMODE_ECOLOR16M:
        return EColor16M;
    case BITBLIT_SOURCEMODE_ECOLOR16MU:
        return EColor16MU;
#if defined(EKA2)
    case BITBLIT_SOURCEMODE_ECOLOR16MA:
        return EColor16MA;
#endif
    default:
        return ENone;
    };
}

/*!
 *
 *
 */
static TDisplayMode sctiDestinationMode( EBitblitDestinationMode mode )
{
    switch( mode )
    {
    case BITBLIT_DESTINATIONMODE_ENONE:
        return ENone;
    case BITBLIT_DESTINATIONMODE_ECOLOR64K:
        return EColor64K;
    case BITBLIT_DESTINATIONMODE_ECOLOR16M:
        return EColor16M;
    case BITBLIT_DESTINATIONMODE_ECOLOR16MU:
        return EColor16MU;
#if defined(EKA2)
    case BITBLIT_DESTINATIONMODE_ECOLOR16MA:
        return EColor16MA;
#endif
    default:
        return ENone;
    };
}

/*!
 *
 *
 */
static void sctiInitializeSourceBitmap( CWsBitmap& bitmap, float maskValueStart, float maskValueEnd, int maskGradientStartX, int maskGradientEndX, int maskGradientStartY, int maskGradientEndY )
{
    int             x, y;
    int             bytesPerPixel;
    unsigned char*  pixels;
    unsigned char*  pointer;
    unsigned char*  bytePointer;
    unsigned short* shortPointer;
    unsigned long*  longPointer;
    int             red, green, blue, alpha;
    TSize           size                        = bitmap.SizeInPixels();
    TDisplayMode    mode                        = bitmap.DisplayMode();

    if( mode == EColor64K )
    {
        bytesPerPixel = 2;
    }
    else if( mode == EColor16M )
    {
        bytesPerPixel = 3;
    }
    else
    {
        bytesPerPixel = 4;
    }
    
    bitmap.LockHeap();

    pixels = ( unsigned char* )( bitmap.DataAddress() ); 

    for( y = 0; y < size.iHeight; y++ )
    {
        for( x = 0; x < size.iWidth; x++ )
        {
            red         = ( y % 256 );
            green       = ( x % 256 );
            blue        = ( ( size.iHeight - y ) % 256 );
            pointer     = &( pixels[ ( size.iWidth * y + x ) * bytesPerPixel ] );

            bytePointer  = ( unsigned char* )( pointer );
            shortPointer = ( unsigned short* )( pointer );
            longPointer  = ( unsigned long* )( pointer );

            alpha = sctiGetAlpha( x, y, size.iWidth, size.iHeight, maskGradientStartX, maskGradientEndX, maskGradientStartY, maskGradientEndY, maskValueStart, maskValueEnd );

            switch( mode )
            {
            case EColor64K:
                *shortPointer = ( unsigned short )( ( ( red << 11 ) & 0xF800 ) | ( ( green << 5 ) & 0x7E0 ) | ( ( blue ) & 0x1F ) );
                break;
            case EColor16M:
                *bytePointer++  = ( unsigned char )( blue );
                *bytePointer++  = ( unsigned char )( green );
                *bytePointer    = ( unsigned char )( red );
                break;
            case EColor16MU:
                *longPointer = ( unsigned long )( ( red << 16 ) | ( green << 8 ) | ( blue ) );
                break;
#if defined( EKA2 )
            case EColor16MA:
                *longPointer = ( unsigned long )( ( red << 24 ) | ( green << 16 ) | ( blue << 8 ) | ( alpha ) );
                break;
#endif  /* defined(EKA2) */
            default:
                break;
            }
        }
    }

    bitmap.UnlockHeap();
}

/*!
 *
 *
 */
static void sctiInitializeMaskBitmap( CWsBitmap& bitmap, float maskValueStart, float maskValueEnd, int maskGradientStartX, int maskGradientEndX, int maskGradientStartY, int maskGradientEndY )
{
    int             x, y;
    int             scanlineLen;
    TSize           size            = bitmap.SizeInPixels();
    TDisplayMode    mode            = bitmap.DisplayMode();
    unsigned char   alpha;
    unsigned char*  data;
    unsigned char*  ptr1;
    unsigned char*  ptr2;    
    int             bpp;

    bitmap.LockHeap();

    data = ( unsigned char* )( bitmap.DataAddress() );

    scanlineLen = bitmap.ScanLineLength( size.iWidth, mode );

    SCT_ASSERT_ALWAYS( mode == EGray2 || mode == EGray256 );

    if( mode == EGray2 )
    {
        bpp = 1;
    }
    else
    {
        bpp = 8;
    }

    for( y = 0; y < size.iHeight; ++ y )
    {
        ptr1 = data + ( scanlineLen * y );
        
        for( x = 0; x < size.iWidth; ++x )
        {
            ptr2 = ptr1 + ( ( x * bpp ) / 8 );            
        
            alpha = sctiGetAlpha( x, y, size.iWidth, size.iHeight, maskGradientStartX, maskGradientEndX, maskGradientStartY, maskGradientEndY, maskValueStart, maskValueEnd );

            if( bpp == 1 )
            {
                unsigned char mask = ( unsigned char )( 1 << ( x % 8 ) );
                if( alpha == maskValueStart )
                {
                    *ptr2 &= ~mask;
                }
                else
                {
                    *ptr2 |= mask;
                }
            }
            else
            {
                *ptr2 = alpha;
            }
        }
    }

    bitmap.UnlockHeap();
}

/*!
 *
 *
 */
static unsigned char sctiGetAlpha( int x, int y, int width, int height, int maskGradientStartX, int maskGradientEndX, int maskGradientStartY, int maskGradientEndY, float maskValueStart, float maskValueEnd )
{
    /*
       ______________
      |1             |
      |   ________   |
      |  |2       |  |
      |  |   __   |  |
      |  |  |3 |  |  |
      |  |  |__|  |  |
      |  |        |  |
      |  |________|  |
      |              |
      |______________|
    */

    if( (   ( x < maskGradientStartX ) || 
            ( x > width - maskGradientStartX ) ) 
        ||
        (   ( y < maskGradientStartY ) || 
            ( y > height - maskGradientStartY ) ) )
    {
        /* Case 1*/
        return ( unsigned char )( 0xFF * maskValueStart );
    }
    else if( ( ( x >= maskGradientEndX ) &&
               ( x <= width - maskGradientEndX ) ) 
             &&
             ( ( y >= maskGradientEndY ) && 
               ( y <= height - maskGradientEndY ) ) )
    {
        /* Case 3 */
        return ( unsigned char )( 0xFF * maskValueEnd );
    }
    else
    {
        /* Case 2 */
        double  rx  = ( x - maskGradientStartX ) / ( double )( width / 2  - maskGradientStartX );
        double  ry  = ( y - maskGradientStartY ) / ( double )( height / 2 - maskGradientStartY );
        double  r;

        rx = rx > 1.0 ? 2.0 - rx : rx;
        ry = ry > 1.0 ? 2.0 - ry : ry;

        r = rx > ry ? rx : ry;

        return ( unsigned char )( ( r * maskValueEnd + ( 1.0 - r ) * maskValueStart ) * 0xFF );
    }
}
