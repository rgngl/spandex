/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_symbiansvgbitmapaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include <stdio.h>
#include <string.h>

#include "CPlatform.h"

#include <w32std.h>
#include <SVGEngineInterfaceImpl.h>
#include <SVGRequestObserver.h>
#include <aknappui.h>
#include <aknviewappui.h>

/*!
 * Static helper funcs.
 *
 */
static int              sctiGetFileSize( TDesC& fileName, RFs& fs );
static SCTBoolean       sctiLoadFile( TDesC& fileName, HBufC8* des, RFs& fs );
static TDisplayMode     sctiBufferMode( EBitmapBufferMode mode );
static TDisplayMode     sctiMaskMode( EBitmapMask mode );

/*!
 *
 *
 */
void* sctiCreateSymbianSVGBitmapActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTSymbianSVGBitmapActionContext*   context;

    SCT_USE_VARIABLE( moduleContext );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    /* Create bitmap action context and zero memory. */
    context = ( SCTSymbianSVGBitmapActionContext* )siCommonMemoryAlloc( NULL, sizeof( SCTSymbianSVGBitmapActionContext ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Bitmap@SymbianSVG action context creation" );
        return NULL;
    }
    memset( context, 0, sizeof( SCTSymbianSVGBitmapActionContext ) );

    context->initialized = SCT_FALSE;

	context->observer = new CSVGBitmapObserver();       /* Must use new */
	if( context->observer == NULL )
	{
        SCT_LOG_ERROR( "Allocation failed in Bitmap@SymbianSVG action context creation" );	
        sctiDestroySymbianSVGBitmapActionContext( context );
        return NULL;
	}

    /* Parse bitmap action attributes. */
    if( sctiParseSymbianSVGBitmapActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroySymbianSVGBitmapActionContext( context );
        return NULL;
    }

    context->wsSession = &( CEikonEnv::Static()->WsSession() );
    context->windowGc  = &( CEikonEnv::Static()->SystemGc() );    

    SCT_ASSERT_ALWAYS( context->wsSession != NULL );
    SCT_ASSERT_ALWAYS( context->windowGc != NULL );    

    return context;
}

/*!
 *
 *
 */
void sctiDestroySymbianSVGBitmapActionContext( void* context )
{
    SCTSymbianSVGBitmapActionContext* c;

    if( context == NULL )
    {
        return;
    }

    c = ( SCTSymbianSVGBitmapActionContext* )( context );

    SCT_ASSERT( c->buffer         == NULL );
    SCT_ASSERT( c->mask           == NULL );
    SCT_ASSERT( c->svgEngine      == NULL );
    SCT_ASSERT( c->domHandle      == 0 );
    SCT_ASSERT( c->fileName       == NULL );
    SCT_ASSERT( c->fileContents   == NULL );

	if( c->observer != NULL )
	{
		delete c->observer;       /* Must use delete */
		c->observer = NULL;
	}

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiSymbianSVGBitmapActionInit( SCTAction *action, SCTBenchmark *benchmark )
{
    SCTSymbianSVGBitmapActionContext*   context;
    int                                 err;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_ASSERT_ALWAYS( benchmark != NULL );

    /* Get bitblit action context. */
    context = ( SCTSymbianSVGBitmapActionContext* )( action->context );
    SCT_ASSERT_ALWAYS( context != NULL );

    /* Check if the action is already initialized. */
    if( context->initialized == SCT_TRUE )
    {
        return SCT_TRUE;
    }

    /* Debug check. */
    SCT_ASSERT( context->wsSession      != NULL );
    SCT_ASSERT( context->windowGc       != NULL );    
    SCT_ASSERT( context->buffer         == NULL );
    SCT_ASSERT( context->mask           == NULL );
    SCT_ASSERT( context->svgEngine      == NULL );
    SCT_ASSERT( context->fileName       == NULL );
    SCT_ASSERT( context->fileContents   == NULL );

    context->rect = TRect( 0, 0, context->data.bufferWidth, context->data.bufferHeight );

    /* Convert c string filename to TDesC16 filename. */
    TPtrC8 d( ( TUint8* )( context->data.fileName ) );
    context->fileName = HBufC16::New( d.Length() );
    if( context->fileName == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Bitmap@SymbianSVG action init" );
        sctiSymbianSVGBitmapActionTerminate( action );
        return SCT_FALSE;
    }
    context->fileName->Des().Copy( d );

    /* Store file system connection. */
    if( context->filesystem.Connect() != KErrNone )
    {
        SCT_LOG_ERROR( "Cannot access filesystem in Bitmap@SymbianSVG action init" );
        sctiSymbianSVGBitmapActionTerminate( action );
        return SCT_FALSE;
    }
    
    /* Get SVG file length. */
    int len = sctiGetFileSize( *( context->fileName ), context->filesystem );
    if( len  < 0 )
    {
        SCT_LOG_ERROR( "SVG file not readable in Bitmap@SymbianSVG action init" );
        sctiSymbianSVGBitmapActionTerminate( action );
        return SCT_FALSE;
    }

    /* Reserve buffer for svg file contents. */
    context->fileContents = HBufC8::New( len );
    if( context->fileContents == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Bitmap@Symbian action init" );
        sctiSymbianSVGBitmapActionTerminate( action );
        return SCT_FALSE;
    }

    /* Load file contents. */
    if( context->data.loadSVG == BITMAP_LOADSVG_INIT )
    {
        if( sctiLoadFile( *( context->fileName ), context->fileContents, context->filesystem ) == SCT_FALSE )
        {
            SCT_LOG_ERROR( "SVG file read failed in Bitmap@SymbianSVG action init" );
            sctiSymbianSVGBitmapActionTerminate( action );
            return SCT_FALSE;
        } 
    }

    /* Create buffer bitmap. */
    TSize bufferSize( context->data.bufferWidth, context->data.bufferHeight );
    context->buffer = new CWsBitmap( *( context->wsSession ) );
    if( context->buffer == NULL || 
        context->buffer->Create( bufferSize, sctiBufferMode( context->data.bufferMode ) ) != KErrNone )
    {
        SCT_LOG_ERROR( "Buffer bitmap creation failed in Bitmap@SymbianSVG action init" );
        sctiSymbianSVGBitmapActionTerminate( action );
        return SCT_FALSE;
    }

    /* Create mask mode. */
    if( context->data.mask != BITMAP_MASK_OFF )
    {
        /* Create mask bitmap. */
        context->mask = new CWsBitmap( *( context->wsSession ) );
        if( context->mask == NULL || 
            context->mask->Create( bufferSize, sctiMaskMode( context->data.mask ) ) != KErrNone )
        {
            SCT_LOG_ERROR( "Mask bitmap creation failed in Bitmap@SymbianSVG action init" );
            sctiSymbianSVGBitmapActionTerminate( action );
            return SCT_FALSE;
        }
    }

    /* Create SVG engine implementation. */
    TFontSpec fontSpec( _L( "LatinPlain12" ), 300 );
    TRAP( err, context->svgEngine = CSvgEngineInterfaceImpl::NewL( context->buffer,
                                                                   context->observer,
                                                                   fontSpec ) );
    if( err != KErrNone || context->svgEngine == NULL )
    {
        SCT_LOG_ERROR( "SVG engine creation failed in Bitmap@SymbianSVG action init" );
        sctiSymbianSVGBitmapActionTerminate( action );
        return SCT_FALSE;
    }

    /* Set svg rendering quality. */
    TRenderingQuality q;
    switch( context->data.quality )
    {
    default:
    case BITMAP_QUALITY_LOW:
        q = KLowNoAA;
        break;
    case BITMAP_QUALITY_HIGH:
        q = KHighNoAA;
        break;
    }
    context->svgEngine->SetRenderQuality( q );

    /* Set background color to opaque white */
    context->svgEngine->SetBackgroundColor( 0xFFFFFFFF );

    /* Prepare DOM in init. */
    if( context->data.parseSVG == BITMAP_PARSESVG_INIT )
    {
        MSvgError* e = context->svgEngine->PrepareDom( *( context->fileContents ), context->domHandle );
        if( ( e->HasError() != ESvgNoError ) && ( e->IsWarning() == EFalse ) )
        {
            SCT_LOG_ERROR( "SVG parsing failed in Bitmap@SymbianSVG action init" );
            sctiSymbianSVGBitmapActionTerminate( action );
            return SCT_FALSE;
        }
        e = context->svgEngine->UseDom( context->domHandle, context->buffer );
        if( ( e->HasError() != ESvgNoError ) && ( e->IsWarning() == EFalse ) )
        {
            SCT_LOG_ERROR( "SVG engine error in Bitmap@SymbianSVG action init" );
            sctiSymbianSVGBitmapActionTerminate( action );
            return SCT_FALSE;
        }
        if( context->data.scaleToFit == SCT_TRUE )
        {	
            context->svgEngine->SetSvgDimensionToFrameBuffer( context->data.bufferWidth, context->data.bufferHeight );
        }
    }

     /* We are done, set action to initialized. */
    context->initialized = SCT_TRUE;

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiSymbianSVGBitmapActionExecute( SCTAction* action, int frameNumber )
{
    SCTSymbianSVGBitmapActionContext*   context;
    int                                 i;

    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTSymbianSVGBitmapActionContext* )( action->context );

    SCT_ASSERT( context->initialized == SCT_TRUE );

    /* Create origin for possible bitblit. */
    TPoint origin( context->data.windowX, context->data.windowY );

    /* Repeat. */
    for( i = 0; i < context->data.iterations; ++i )
    {
        /* Load SVG file. */
        if( context->data.loadSVG == BITMAP_LOADSVG_EXEC )
        {
            if( sctiLoadFile( *( context->fileName ), context->fileContents, context->filesystem ) == SCT_FALSE )
            {
                SCT_LOG_ERROR( "SVG file read failed in Bitmap@SymbianSVG action execute" );
                return SCT_FALSE;
            } 
        }

        /* Parse SVG file. */
        if( context->data.parseSVG == BITMAP_PARSESVG_EXEC )
        {
            SCT_ASSERT( context->domHandle == 0 );

            MSvgError* e = context->svgEngine->PrepareDom( *( context->fileContents ), context->domHandle );
            if( ( e->HasError() != ESvgNoError ) && ( e->IsWarning() == EFalse ) )
            {
                SCT_LOG_ERROR( "SVG parsing failed in Bitmap@SymbianSVG action execute" );
                return SCT_FALSE;
            }
            e = context->svgEngine->UseDom( context->domHandle, context->buffer );
            if( ( e->HasError() != ESvgNoError ) && ( e->IsWarning() == EFalse ) )
            {
                SCT_LOG_ERROR( "SVG engine error in Bitmap@SymbianSVG action execute" );
                sctiSymbianSVGBitmapActionTerminate( action );
                return SCT_FALSE;
            }
            if( context->data.scaleToFit == SCT_TRUE )
            {	
                context->svgEngine->SetSvgDimensionToFrameBuffer( context->data.bufferWidth, context->data.bufferHeight );
            }
        }

        /* Render SVG file. */
        if( context->data.renderSVG == BITMAP_RENDERSVG_REDRAW )
        {
            context->svgEngine->Redraw();
        }
        else if( context->data.renderSVG == BITMAP_RENDERSVG_HANDLE )
        {
            //context->svgEngine->RenderDom( context->domHandle, NULL );
            context->svgEngine->RenderDom( context->domHandle, context->buffer );
        }
 
        /* Generate mask, if used. */
        if( context->data.mask != BITMAP_MASK_OFF )
        {
            context->svgEngine->GenerateMask( context->mask );
        }

        /* Blit bitmap to window. */
        if( context->data.blitToWindow == SCT_TRUE )
        {
            if( context->data.useMask == SCT_FALSE )
            {
                context->windowGc->BitBlt( origin, context->buffer );
            }
            else
            {
                context->windowGc->BitBltMasked( origin, context->buffer, context->rect, context->mask, ETrue );
            }
        }

        if( context->data.parseSVG == BITMAP_PARSESVG_EXEC )
        {
            context->svgEngine->DeleteDom( context->domHandle );
            context->domHandle = 0;
        }
    }

    /* Flush window server. */
    if( context->data.flush == SCT_TRUE )
    {
        context->wsSession->Flush();
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiSymbianSVGBitmapActionTerminate( SCTAction* action )
{
    SCTSymbianSVGBitmapActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTSymbianSVGBitmapActionContext* )( action->context );
    
    if( context->svgEngine != NULL )
    {
        if( context->domHandle != 0 )
        {
            context->svgEngine->DeleteDom( context->domHandle );
            context->domHandle = 0;
        }
        context->svgEngine->Destroy();
        delete context->svgEngine;
        context->svgEngine = NULL;
    }

    delete context->buffer;
    context->buffer = NULL;
    
    delete context->mask;
    context->mask = NULL;

    delete context->fileName;
    context->fileName = NULL;

    delete context->fileContents;
    context->fileContents = NULL;

    context->filesystem.Close();

    context->initialized = SCT_FALSE;
}

/*!
 *
 *
 */
void sctiSymbianSVGBitmapActionDestroy( SCTAction* action )
{
    SCTSymbianSVGBitmapActionContext*   context;

    SCT_ASSERT_ALWAYS( action != NULL );

    context = ( SCTSymbianSVGBitmapActionContext* )( action->context );

    sctiDestroySymbianSVGBitmapActionContext( context );
}

/*!
 *
 *
 */
SCTStringList* sctiSymbianSVGBitmapActionGetWorkload( SCTAction *action )
{
    SCTSymbianSVGBitmapActionContext*           context;
    SCTStringList*                              result;
    char                                        buf[ 32 ];

    SCT_ASSERT_ALWAYS( action != NULL );
    
    context = ( SCTSymbianSVGBitmapActionContext* )( action->context );
    SCT_ASSERT_ALWAYS( context != NULL );
    
    result = sctCreateStringList();
    if( result == NULL )
    {
        return NULL;
    }

    sprintf( buf, "%d bitmaps", context->data.iterations );
    if( sctAddStringCopyToList( result, buf ) == SCT_FALSE )
    {
        sctDestroyStringList( result );
        return NULL;
    }

    return result;
}

/*!
 *
 *
 */
int sctiGetFileSize( TDesC& fileName, RFs& fs )
{
    RFile           f;
    TInt            size        = -1;

    if( f.Open( fs, fileName, EFileRead | EFileStream | EFileShareReadersOnly ) == KErrNone )
    {
        if( f.Size( size ) != KErrNone )
        {
            size = -1;
        }
        f.Close();
    }


    return size;
}

/*!
 *
 *
 */
SCTBoolean sctiLoadFile( TDesC& fileName, HBufC8* buf, RFs& fs )
{
    RFile               f;
    SCTBoolean          status          = SCT_FALSE;
    TPtr8               des             = buf->Des();

    if( f.Open( fs, fileName, EFileRead | EFileStream | EFileShareReadersOnly ) == KErrNone )
    {
    	TInt len = des.MaxSize();
    	TInt err = f.Read( des, len );
        if( err == KErrNone )
        {
            status = SCT_TRUE;
        }
        
        f.Close();
    }

    return status;
}

/*!
 *
 *
 */
static TDisplayMode sctiBufferMode( EBitmapBufferMode mode )
{
    switch( mode )
    {
    case BITMAP_BUFFERMODE_ECOLOR64K:
        return EColor64K;
    case BITMAP_BUFFERMODE_ECOLOR16M:
        return EColor16M;
    case BITMAP_BUFFERMODE_ECOLOR16MU:
        return EColor16MU;
#if defined( EKA2 )
    case BITMAP_BUFFERMODE_ECOLOR16MA:
        return EColor16MA;
#endif  /* defined( EKA2 ) */
    default:
        return ENone;
    };
}

/*!
 *
 *
 */
static TDisplayMode sctiMaskMode( EBitmapMask mode )
{
    switch( mode )
    {
    case BITMAP_MASK_EGRAY2:
        return EGray2;
    case BITMAP_MASK_EGRAY256:
        return EGray256;
    default:
        return ENone;
    };
}

/* -------------------------------------------------- */

/*!
 *
 *
 */
CSVGBitmapObserver::CSVGBitmapObserver()
{
}

/*!
 *
 *
 */
void CSVGBitmapObserver::UpdateScreen()
{
}

/*!
 *
 *
 */
TBool CSVGBitmapObserver::ScriptCall( const TDesC& /*aScript*/, CSvgElementImpl* /*aCallerElement*/ )
{
    return EFalse;
}

/*!
 *
 *
 */
TInt CSVGBitmapObserver::FetchImage( const TDesC& /*aUri*/, RFs& /*aSession*/, RFile& /*aFileHandle*/ )
{
    return KErrNone;
}

/*!
 *
 *
 */
TInt CSVGBitmapObserver::FetchFont( const TDesC& /*aUri*/, RFs& /*aSession*/, RFile& /*aFileHandle*/ )
{
    return KErrNone;
}

/*!
 *
 *
 */
void CSVGBitmapObserver::GetSmilFitValue( TDes& /*aSmilValue*/ )
{
}

/*!
 *
 *
 */
void CSVGBitmapObserver::UpdatePresentation( const TInt32& /*aNoOfAnimation*/ )
{
}


