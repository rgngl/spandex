#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
SetModuleName( 'SymbianSVG' )

######################################################################
AddInclude( 'sct_symbiansvgmodule.h' )

######################################################################
AddActionConfig( 'Bitmap',
                 'Init'+'Execute'+'Terminate'+'Destroy'+'GetWorkload',
                 [ IntAttribute(        'Iterations',                   1 ),
                   StringAttribute(     'FileName',                     256 ),
                   AutoEnumAttribute(   'LoadSVG',                      [ 'INIT', 'EXEC' ] ),
                   AutoEnumAttribute(   'ParseSVG',                     [ 'OFF', 'INIT', 'EXEC' ] ),
                   AutoEnumAttribute(   'RenderSVG',                    [ 'OFF', 'REDRAW', 'HANDLE' ] ),
                   AutoEnumAttribute(   'Mask',                         [ 'OFF', 'EGray2', 'EGray256' ] ),
                   AutoEnumAttribute(   'BufferMode',                   [ 'EColor64K','EColor16M','EColor16MU','EColor16MA' ] ),
                   IntAttribute(        'BufferWidth',                  1 ),
                   IntAttribute(        'BufferHeight',                 1 ),
                   OnOffAttribute(      'ScaleToFit' ),
                   AutoEnumAttribute(   'Quality',                      [ 'LOW', 'HIGH' ] ),
                   OnOffAttribute(      'BlitToWindow' ),
                   OnOffAttribute(      'UseMask' ),
                   IntAttribute(        'WindowX',                      0 ),
                   IntAttribute(        'WindowY',                      0 ),
                   OnOffAttribute(      'Flush' ) ],
                 [ ValidatorConfig( 'LoadSVG == BITMAP_LOADSVG_EXEC && ParseSVG == BITMAP_PARSESVG_INIT',
                                    'SVG file must be loaded before parse' ),
                   ValidatorConfig( 'RenderSVG == BITMAP_RENDERSVG_OFF && Mask != BITMAP_MASK_OFF',
                                    'Mask must be off when rendering is disabled' ) ] 
                 )
