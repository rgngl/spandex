/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __SCT_SYMBIANSVGSVGBITMAP_H__ )
#define __SCT_SYMBIANSVGSVGBITMAP_H__

#include "sct_types.h"
#include "sct_symbiansvgmodule_parser.h"

#include <gdi.h>
#include <SVGRequestObserver.h>

class CWsBitmap;
class CWindowGc;
class RWsSession;
class CSvgEngineInterfaceImpl;

/*!
 *
 *
 */
class CSVGBitmapObserver : public CBase, public MSvgRequestObserver
{
public:
    CSVGBitmapObserver();
		
    virtual void        UpdateScreen();
    virtual TBool       ScriptCall( const TDesC& aScript, CSvgElementImpl* aCallerElement );
    virtual TInt        FetchImage( const TDesC& aUri, RFs& aSession, RFile& aFileHandle );
    virtual TInt        FetchFont( const TDesC& aUri, RFs& aSession, RFile& aFileHandle );
    virtual void        GetSmilFitValue( TDes& aSmilValue );
    virtual void        UpdatePresentation(const TInt32&  aNoOfAnimation);
};

/*!
 *
 *
 */
typedef struct
{
    SCTBoolean                          initialized;
    SymbianSVGBitmapActionData          data;
    CSVGBitmapObserver*                 observer;
    RWsSession*                         wsSession;
    RFs                                 filesystem;
    CFbsBitmap*                         buffer;
    CFbsBitmap*                         mask;
    CSvgEngineInterfaceImpl*            svgEngine;
    TInt                                domHandle;
    TRect                               rect;
    HBufC*                              fileName;
    HBufC8*                             fileContents;
    TReal32                             zoomFactor;
    CWindowGc*                          windowGc;
} SCTSymbianSVGBitmapActionContext;

#if defined( __cplusplus )
extern "C" {
#endif  /* defined( __cplusplus ) */

    void*                               sctiCreateSymbianSVGBitmapActionContext( void* moduleContext, SCTAttributeList* attributes );
    void                                sctiDestroySymbianSVGBitmapActionContext( void* context );

    SCTBoolean                          sctiSymbianSVGBitmapActionInit( SCTAction *action, SCTBenchmark *benchmark );
    SCTBoolean                          sctiSymbianSVGBitmapActionExecute( SCTAction* action, int frameNumber );
    void                                sctiSymbianSVGBitmapActionTerminate( SCTAction* action );
    void                                sctiSymbianSVGBitmapActionDestroy( SCTAction* action );
    SCTStringList*                      sctiSymbianSVGBitmapActionGetWorkload( SCTAction *action );

#if defined( __cplusplus )
}
#endif  /* defined( __cplusplus ) */

#endif /* !defined( __SCT_SYMBIANSVGSVGBITMAP_H__ ) */
