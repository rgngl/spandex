/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_symbiansvgmodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_symbiansvgmodule_parser.h"
#include "sct_symbiansvgmodule_actions.h"
#include "sct_symbiansvgbitmapaction.h"

#include <stdio.h>
#include <string.h>

/* Local function declarations. */
SCTAttributeList*       sctiSymbianSVGModuleInfo( SCTModule* module );
SCTAction*              sctiSymbianSVGCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    sctiSymbianSVGDestroy( SCTModule* module );

/*!
 * Create symbian svg module data structure which defines the name
 * of the module, symbian svg context, and sets up the module
 * interface functions.
 *
 * \return Module structure for symbian svg module, or NULL if failure.
 */
SCTModule* sctCreateSymbianSVGModule( void )
{
    SCTModule*                  module;

    /* Create module data structure. */
    module = sctCreateModule( "SymbianSVG",
                              NULL,                             /* NULL module context. */
#if defined( _WIN32 )
                              _SymbianSVG_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiSymbianSVGModuleInfo,
                              sctiSymbianSVGCreateAction,
                              sctiSymbianSVGDestroy );

    return module;
}

/*!
 * Create attribute list containing symbian svg module information.
 *
 * \param module Symbian svg module. Must be defined.
 *
 * \return Symbian svg information as attribute list, or NULL if
 * failure.
 */
SCTAttributeList* sctiSymbianSVGModuleInfo( SCTModule* module )
{
    SCTAttributeList*                   attributes;

    SCT_ASSERT_ALWAYS( module != NULL );

    attributes = sctCreateAttributeList();
    if( attributes == NULL )
    {
        return NULL;
    }

    return attributes;
}

/*!
 * Create symbian svg module action. 
 *
 * \param module Symbian svg module. Must be defined.
 * \param name Action name. Must be defined.
 * \param type Action type. Must be defined.
 * \param attributes List of action attributes. Must be defined.
 *
 * \return Action data structure, or NULL if failure.
 *
 */
SCTAction* sctiSymbianSVGCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    return sctCreateActionFromTemplate( name,
                                        type,
                                        "SymbianSVG",
                                        NULL,
                                        attributes,
                                        SymbianSVGActionTemplates,
                                        SCT_ARRAY_LENGTH( SymbianSVGActionTemplates ) );
}
/*!
 * Destroy symbian svg module. This function does not deallocate
 * module data structure but instead simply does the needed module
 * cleanup, if any.
 *
 * \param module Module to destroy. Must be defined.
 */
void sctiSymbianSVGDestroy( SCTModule* module )
{
    SCT_ASSERT_ALWAYS( module != NULL );

}




