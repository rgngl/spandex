/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "CSpandexContainer.h" 
#include "CSpandexCommon.h"
#include "CPlatform.h"

#include <AknLayout2Def.h>
#include <aknappui.h>
#include <aknnotewrappers.h> 
#include <hal.h>

#include <string.h>

#include "sct_utils.h"
#include "sct_sicommon.h"

#if defined( SCT_SYMBIAN_FORCE_IO_DRIVE_C )
# define SCT_SYMBIAN_FORCE_IO_DRIVE  'c'
#endif  /* defined( SCT_SYMBIAN_FORCE_IO_DRIVE_C ) */

#if defined( SCT_SYMBIAN_FORCE_IO_DRIVE_E )
# define SCT_SYMBIAN_FORCE_IO_DRIVE  'e'
#endif  /* defined( SCT_SYMBIAN_FORCE_IO_DRIVE_E ) */

#define SCT_INPUT_FILE_FORMAT_STRING    "%c:\\benchmark.txt"
#define SCT_OUTPUT_FILE_FORMAT_STRING   "%c:\\result.txt"
#define SCT_DEVICE_ID_FORMAT_STRING     "%c:\\spandex_did.txt"

CSpandexCommon*     gSpandexCommon  = NULL;
CSpandexContainer*  gContainer      = NULL;
SCTLog*             gLog            = NULL;

/*!
 *
 *
 */
static int getMemoryCardDrive( char* drive );

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::SetSpandexCommon( CSpandexCommon* aCommon )
{
    gSpandexCommon = aCommon;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
CSpandexCommon* CPlatform::SpandexCommon()
{
    return gSpandexCommon;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::SetContainer( void* aContainer )
{
    gContainer = ( CSpandexContainer* )( aContainer );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTLog* CPlatform::GetLog()
{
    return gLog;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::SetLog( SCTLog* log )
{
    gLog = log;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::ScreenSize( TInt& aWidth, TInt& aHeight )
{
    TSize screenSize = gContainer->Rect().Size();;
	aWidth  = screenSize.iWidth;
	aHeight = screenSize.iHeight;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
TDisplayMode CPlatform::ScreenMode()
{
	int numColor = CPlatform::ScreenColorBits();

    switch( numColor )
    {
    case 12:
        return EColor4K;
    case 16:
        return EColor64K;
    case 24:
        return EColor16M;
    case 32:
        return EColor16MA;
    default:
        return ENone;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
int CPlatform::ScreenColorBits()
{
	TInt numColor;
	HAL::Get( HAL::EDisplayColors, numColor );

	if( numColor == ( 1 << 12 ) )
	{
        return 12;
	}
	else if( numColor == ( 1 << 16 ) )
	{
        return 16;
	}
	else if( numColor == ( 1 << 24 ) )
    {
        return 24;
    }
    else
    {
        return 32;
    }    
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::DisplayError( const char* aTitle, const char* aMessage )
{
    SCT_USE_VARIABLE( aTitle );

    const int   LENGTH = SCT_MAX_ERROR_MSG_LEN * 2;
    TPtrC8      message8( ( const TUint8* )aMessage );
    TInt        error;

    SCT_ASSERT_ALWAYS( message8.Length() < LENGTH );
    TBuf< LENGTH > messagebuf;

    messagebuf.Copy( message8 );

    RDebug::Printf( "SPANDEX error: %s: %s\n", aTitle, aMessage );
    
	CAknInformationNote *note = new CAknInformationNote;
    if( note == NULL )
    {
        return;
    }
	TRAP( error, note->ExecuteLD( messagebuf ) );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::DisplayMessage( const char* aTitle, const char* aMessage )
{
    SCT_USE_VARIABLE( aTitle );

    const int   LENGTH = SCT_MAX_ERROR_MSG_LEN * 2;
    TPtrC8      message8( ( const TUint8* )aMessage );
    int         error;

    SCT_ASSERT_ALWAYS( message8.Length() < LENGTH );
    TBuf< LENGTH > messagebuf;

    messagebuf.Copy( message8 );

    RDebug::Printf( "SPANDEX message: %s: %s\n", aTitle, aMessage );
    
	CAknInformationNote* note = new CAknInformationNote;
    if( note == NULL )
    {
        return;
    }
	note->SetTimeout( CAknNoteDialog::ELongTimeout );
	TRAP( error, note->ExecuteLD( messagebuf ) );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::ReportProgress( TInt aTotal, TInt aCurrent, TInt aRepeat, const char* aName )
{
    RDebug::Printf( "SPANDEX benchmark %d/%d, %d: %s\n", aCurrent, aTotal, aRepeat, aName );
    
    gContainer->ReportProgress( aTotal, aCurrent, aRepeat, aName );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static int getMemoryCardDrive( char* drive )
{
#if !defined( SCT_SYMBIAN_FORCE_IO_DRIVE )
    RFs         fs;
    TDriveInfo  info;
    int         status = 0;
 
    SCT_ASSERT_ALWAYS( drive != NULL );
    
    fs.Connect();

    for( TInt d = 0; d < ( EDriveZ - EDriveA ); ++d )
    {
        if( fs.Drive( info, EDriveA + d ) == KErrNone )
        {
            if( ( info.iDriveAtt & KDriveAttRemovable ) != 0 )
            {
                /* Make sure the removable drive is indeed a removable memory
                 * card with the input file. */
                const unsigned int  filenamelen = 128;
                char                filename[ filenamelen ];
                char                tempdrive;
                FILE*               fp;

                SCT_ASSERT_ALWAYS( strlen( SCT_INPUT_FILE_FORMAT_STRING ) < filenamelen );
                
                tempdrive = 'a' + d;
                sprintf( filename, SCT_INPUT_FILE_FORMAT_STRING, tempdrive );

                fp = fopen( filename, "r" );
                if( fp != NULL )
                {
                    fclose( fp );
                    *drive = tempdrive;
                    status  = 1;
                    break;
                }
            }
        }
    }

    fs.Close();

    return status;
#else   /* !defined( SCT_SYMBIAN_FORCE_IO_DRIVE ) */
    SCT_ASSERT_ALWAYS( drive != NULL );
    *drive = SCT_SYMBIAN_FORCE_IO_DRIVE;
#endif  /* !defined( SCT_SYMBIAN_FORCE_IO_DRIVE ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
const char* CPlatform::InputFileName()
{
#if defined( __WINS__ )
    return "C:\\benchmark.txt";
#else   /* defined( __WINS__ ) */
    static char     buf[ 128 ];
    char            drive;

    SCT_ASSERT_ALWAYS( getMemoryCardDrive( &drive ) != 0 );
    sprintf( buf, SCT_INPUT_FILE_FORMAT_STRING, drive );
    return buf;
#endif  /* defined( __WINS__ ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
const char* CPlatform::OutputFileName()
{
#if defined( __WINS__ )
    return "C:\\result.txt";
#else   /* defined( __WINS__ ) */
    static char     buf[ 128 ];
    char            drive;

    SCT_ASSERT_ALWAYS( getMemoryCardDrive( &drive ) != 0 );
    sprintf( buf, SCT_OUTPUT_FILE_FORMAT_STRING, drive );
    return buf;
#endif  /* defined( __WINS__ ) */
}
