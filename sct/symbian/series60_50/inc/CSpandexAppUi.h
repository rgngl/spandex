/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __CSPANDEXAPPUI_H__ )
#define __CSPANDEXAPPUI_H__

#include <AknLayout2Def.h>
#include <aknappui.h>

class CSpandexContainer;
class CSendAppUi;

/*!
 *
 *
 */
class CSpandexAppUi : public CAknAppUi
{
public:

    void                ConstructL();
    virtual             ~CSpandexAppUi();
    
private:

    void                DynInitMenuPaneL( TInt aResourceId, CEikMenuPane *aMenuPane );
    void                HandleCommandL( TInt aCommand );
    void                DoSendReportL( TInt aCommand );

private:

    CSpandexContainer   *iAppContainer; 
	CSendAppUi			*iSendUi;
};

#endif /* !defined( __CSPANDEXAPPUI_H__ ) */
