/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <e32std.h>
#include <w32std.h>

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <hal.h>

#include "sct_sicommon.h"
#include "sct_sivgi.h"

#include "CPlatform.h"

/* S60 stuff */
#include <aknappui.h>
#include <aknviewappui.h>

/*!
 *
 */
typedef struct
{
    CWindowGc*  windowGc;
} SCTSymbianVgiContext;

/*!
 *
 *
 */
void* siVgiGetContext()
{
    SCTSymbianVgiContext*   context;

    context = ( SCTSymbianVgiContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTSymbianVgiContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTSymbianVgiContext ) );

    context->windowGc = &( CEikonEnv::Static()->SystemGc() );
    SCT_ASSERT_ALWAYS( context->windowGc != NULL );

    return context;
}

/*!
 *
 *
 */
void siVgiReleaseContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTVgiTarget* siVgiGetTarget( void* context, int width, int height, VGIColorBufferFormat targetFormat, SCTBoolean targetMask )
{
    SCTVgiTarget*           target;
    TDisplayMode            displayMode;
    SCTBoolean              needMask        = targetMask;

    SCT_ASSERT_ALWAYS( context != NULL );

    target = ( SCTVgiTarget* )( siCommonMemoryAlloc( NULL, sizeof( SCTVgiTarget ) ) );
    if( target == NULL )
    {
        return NULL;
    }
    memset( target, 0, sizeof( SCTVgiTarget ) );

    switch( targetFormat )
    {
    case VGI_COLOR_BUFFER_FORMAT_RGB565:
        displayMode = EColor64K;
        break;

    case VGI_COLOR_BUFFER_FORMAT_RGB888:
        displayMode = EColor16M;
        break;

    case VGI_COLOR_BUFFER_FORMAT_XRGB8888:
        displayMode = EColor16MU;
        break;

    case VGI_COLOR_BUFFER_FORMAT_ARGB8888:
    default:
        displayMode = EColor16MA;
        needMask    = SCT_FALSE;
        break;
    }

    target->format = targetFormat;
    target->width  = width;
    target->height = height;
    TSize size( target->width, target->height );
    target->bitmap = new CWsBitmap( CEikonEnv::Static()->WsSession() );
    if( target->bitmap == NULL || ( ( CWsBitmap* )( target->bitmap ) )->Create( size, displayMode ) != KErrNone )
    {        
        siVgiReleaseTarget( context, target );
        return NULL;
    }

    target->bitmapStride = CFbsBitmap::ScanLineLength( target->width, displayMode );

    if( needMask == SCT_TRUE )
    {
        target->mask = new CWsBitmap( CEikonEnv::Static()->WsSession() );
        if( target->mask == NULL || ( ( CWsBitmap* )( target->mask ) )->Create( size, EColor256 ) != KErrNone )
        {        
            siVgiReleaseTarget( context, target );
            return NULL;
        }
        target->maskStride = CFbsBitmap::ScanLineLength( width, EColor256 );
    }

    return target;
}

/*!
 *
 *
 */
SCTBoolean siVgiLockTarget( void* context, SCTVgiTarget* target )
{
    CWsBitmap*  bitmap;
    CWsBitmap*  mask;

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( target != NULL );
    SCT_ASSERT_ALWAYS( target->bitmap != NULL );

    bitmap = ( CWsBitmap* )( target->bitmap ); 
    bitmap->LockHeap();

    target->bitmapData  = ( unsigned char* )( bitmap->DataAddress() );

    if( target->mask != NULL )
    {
        mask = ( CWsBitmap* )( target->mask ); 
        mask->LockHeap();
        target->maskData  = ( unsigned char* )( mask->DataAddress() );
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void siVgiUnlockTarget( void* context, SCTVgiTarget* target )
{
    CWsBitmap*  bitmap;
    CWsBitmap*  mask;
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( target != NULL );
    SCT_ASSERT_ALWAYS( target->bitmap != NULL );

    bitmap = ( CWsBitmap* )( target->bitmap ); 
    bitmap->UnlockHeap();
    target->bitmapData = NULL;

    if( target->mask != NULL )
    {
        mask = ( CWsBitmap* )( target->mask ); 
        mask->UnlockHeap();
        target->maskData  = NULL;
    }
}

/*!
 *
 *
 */
void siVgiReleaseTarget( void* context, SCTVgiTarget* target )
{
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( target != NULL );

    if( target == NULL )
    {
        return;
    }

    if( target->bitmap != NULL )
    {
        delete ( CWsBitmap* )( target->bitmap );
        target->bitmap = NULL;
    }
    if( target->mask != NULL )
    {
        delete ( CWsBitmap* )( target->mask );
        target->mask = NULL;
    }
    siCommonMemoryFree( NULL, target );
}

/*!
 *
 *
 */
SCTBoolean siVgiBlitTarget( void* context, SCTVgiTarget* target )
{
    SCTSymbianVgiContext*   c;
    CWsBitmap*              bitmap;
    CWsBitmap*              mask;
    TSize                   size;

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( target != NULL );
    SCT_ASSERT_ALWAYS( target->bitmap != NULL );

    c = ( SCTSymbianVgiContext* )( context );
    
    bitmap = ( CWsBitmap* )( target->bitmap );
    mask   = ( CWsBitmap* )( target->mask );

    size.iWidth  = target->width;
    size.iHeight = target->height;

    if( mask == NULL )
    {
        c->windowGc->BitBlt( TPoint( 0, 0 ),  bitmap );
    }
    else
    {
        c->windowGc->BitBltMasked( TPoint( 0, 0 ),  bitmap, size, mask, EFalse );
    }
    CEikonEnv::Static()->WsSession().Flush();

    return SCT_TRUE;
}
