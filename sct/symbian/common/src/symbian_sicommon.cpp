/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Symbian includes */
#include <e32def.h>
#include <e32std.h>
#include <w32std.h>
#include <e32debug.h>
#include <hal.h>

/* Stdlib includes*/
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

/* Includes from Spandex core */
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_module.h"
#include "sct_systemmodule.h"

#include "CSpandexCommon.h"
#include "CPlatform.h"

#if defined( INCLUDE_SYMBIANBITGDI_MODULE )
# include "sct_symbianbitgdimodule.h"
#endif  /* defined( INCLUDE_SYMBIANBITGDI_MODULE ) */ 

#if defined( INCLUDE_SYMBIANSVG_MODULE )
# include "sct_symbiansvgmodule.h"
#endif  /* defined( INCLUDE_SYMBIANSVG_MODULE ) */

#if defined( INCLUDE_EGL_MODULE )
#include "sct_eglmodule.h"  /* for sctEglGetProcAddress */
#include "sct_eglmemory.h"  /* for EglMemory */
#endif  /* defined( INCLUDE_EGL_MODULE ) */

#define EMULATOR_TIMER_MULTIPLIER       1000
#define DEBUG_PRINT_OUTPUT_BUFFER_SIZE  1024

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siCommonGetContext( void )
{
    return CPlatform::SpandexCommon();
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
const char* siCommonQuerySystemString( void* aContext, SCTSystemString aName )
{
	CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
	
    SCT_ASSERT( spandex == siCommonGetContext() );
    SCT_ASSERT_ALWAYS( spandex != NULL );

	switch( aName )
	{
	case SCT_SYSSTRING_DEVICE_TYPE:
		return spandex->DeviceInfo().deviceName;

	case SCT_SYSSTRING_CPU:
		return spandex->DeviceInfo().cpu;
		
    case SCT_SYSSTRING_OS:
		return spandex->DeviceInfo().OS;

	default:
        SCT_ASSERT_ALWAYS( 0 );
		return NULL;
	}
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
size_t siCommonQueryMemoryAttribute( void* aContext, SCTMemoryAttribute id )
{
	CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    TInt            v1      = 0;
    TInt            v2      = 0;

    if( id == SCT_MEMATTRIB_TOTAL_SYSTEM_MEMORY )
    {
        HAL::Get( HALData::EMemoryRAM, v1 );
        return ( size_t )( v1 );
    }
    else if( id == SCT_MEMATTRIB_USED_SYSTEM_MEMORY )
    {
        HAL::Get( HALData::EMemoryRAM, v1 );
        HAL::Get( HALData::EMemoryRAMFree, v2 );
        return ( size_t )( v1 - v2 );
    }
    else if( spandex->EglMemory() != NULL )
    {
#if defined( INCLUDE_EGL_MODULE )
        SCTEGLMemory    m;
        memset( &m, 0, sizeof( SCTEGLMemory ) );

        sctGetEglMemoryStatus( spandex->EglMemory(), &m );

        switch( id )
        {
        case SCT_MEMATTRIB_TOTAL_GRAPHICS_MEMORY:
            return m.totalGraphicsMemory;
            
        case SCT_MEMATTRIB_USED_GRAPHICS_MEMORY:
            return m.usedGraphicsMemory;
            
        case SCT_MEMATTRIB_USED_PRIVATE_GRAPHICS_MEMORY:
            return m.usedPrivateGraphicsMemory;
            
        case SCT_MEMATTRIB_USED_SHARED_GRAPHICS_MEMORY:
            return m.usedSharedGraphicsMemory;

        default:
            SCT_ASSERT_ALWAYS( 0 );
            return 0;
        }       
#endif  /* defined( INCLUDE_EGL_MODULE ) */
    }

    return 0;
}   

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siCommonSampleCpuLoad( void* context )
{
    SCT_USE_VARIABLE( context );

    return NULL;    /* Not supported. */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
float siCommonCalculateCpuLoad( void* context, void* sample, SCTCpuLoad load )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( sample );
    SCT_USE_VARIABLE( load );
    
    return -1.0f;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siCommonDestroyCpuLoadSample( void* context, void* sample )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( sample );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
unsigned long siCommonGetTimerFrequency( void* /*aContext*/ )
{	
#if defined( EKA2 )
    return 1000;
#else   /* defined( EKA2 ) */
	TInt timerPeriod; //in microseconds
	TInt timerFreq;

	HAL::Get( HALData::ESystemTickPeriod, timerPeriod );

	timerFreq = 1000000 / timerPeriod; // in Hz

/* Make Symbian timer in emulator to appear more accurate than what it really is */
# if defined( __WINS__ )
    timerFreq *= EMULATOR_TIMER_MULTIPLIER;
# endif /* defined( __WINS__ ) */

	return timerFreq;
#endif  /* defined( EKA2 ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
unsigned long siCommonGetTimerTick( void* /*aContext*/ )
{
#if defined( EKA2 )
    return User::NTickCount();
#else   /* defined( EKA2 ) */
# if defined( __WINS__ )
	return ( unsigned long )( User::TickCount() * EMULATOR_TIMER_MULTIPLIER );
# else  /* defined( __WINS__ ) */
	return ( unsigned long )( User::TickCount() );
# endif /* defined( __WINS__ ) */
#endif  /* defined( EKA2 ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonDebugPrintf( void* aContext, const char* fmt, ... )
{
	CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    char            buf[ DEBUG_PRINT_OUTPUT_BUFFER_SIZE ];
    va_list         ap;

    SCT_ASSERT( spandex == siCommonGetContext() );
    SCT_ASSERT_ALWAYS( spandex != NULL );
    SCT_ASSERT_ALWAYS( fmt != NULL );   
    
    /* TODO FIXME: following code has potential buffer overflow, in which case
     * make the debug print messages shorter. Too bad symbian does not seem to
     * support vnsprintf. */
	va_start( ap, fmt );
	vsprintf( buf, fmt, ap );
	va_end( ap );

    RDebug::Printf( buf );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonErrorMessage( void* aContext, const char* aErrorMessage )
{
	CSpandexCommon* spandex     = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    FILE*           outputFile  = NULL;
    
    SCT_ASSERT( spandex == siCommonGetContext() );
    SCT_ASSERT_ALWAYS( spandex != NULL );
           
    outputFile = spandex->OutputFile();
	if( outputFile )
	{
		fputs( "SPANDEX ERROR: ", outputFile );
		fputs( aErrorMessage, outputFile );
        fputs( "\n", outputFile );
		fflush( outputFile );
	}

    CPlatform::DisplayError( "Spandex error", aErrorMessage );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonWarningMessage( void* aContext, const char* aWarningMessage )
{
	CSpandexCommon* spandex     = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    FILE*           outputFile  = NULL;
    
    SCT_ASSERT( spandex == siCommonGetContext() );
    SCT_ASSERT_ALWAYS( spandex != NULL );
           
    outputFile = spandex->OutputFile();
	if( outputFile )
	{
		fputs( "#SPANDEX WARNING: ", outputFile );
		fputs( aWarningMessage, outputFile );
        fputs( "\n", outputFile );
		fflush( outputFile );
	}
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
SCTLog* siCommonGetErrorLog( void* /*context*/ )
{
    return CPlatform::GetLog();
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonSetErrorLog( void* /*context*/, SCTLog* log )
{
    CPlatform::SetLog( log );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonProgressMessage( void* /*aContext*/, int aTotalBenchmark, int aCurrentBenchmark, int aCurrentRepeat, const char* aBenchmarkName )
{
	CPlatform::ReportProgress( aTotalBenchmark, aCurrentBenchmark, aCurrentRepeat, aBenchmarkName );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
void siCommonAssert( void* aContext, void* aExp, void* aFile, unsigned int aLine )
{
 	CSpandexCommon* spandex     = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    FILE*           outputFile  = NULL;

    /* Assert strings are potentially very long so do formatted print only to a
     * file and avoid potential for buffer overflow due to missing snprintf. */
	if( spandex != NULL )
	{
	    outputFile = spandex->OutputFile();
		if( outputFile )
		{
            siCommonFlushOutput( aContext );
            fprintf( outputFile,
                     "\nSpandex assert failed.\nExpresion: ( %s )\nFile: %s\nLine: %d\n",
                     ( char* )( aExp ), ( char* )( aFile ), ( int )( aLine ) ); 
    	    fflush( outputFile );
            fclose( outputFile );
    	}
	}

    CPlatform::DisplayError( "Spandex assert", "See the output file for details" );
    User::Panic( _L( "Spandex assert" ), 0 );
}

/* ---------------------------------------------------------------------- */
/*!
 * 
 *
 */
void* siCommonMemoryAlloc( void* /*aContext*/, unsigned int aSize )
{
	return User::Alloc( aSize );
}

/* ---------------------------------------------------------------------- */
/*!
 * 
 *
 */
void* siCommonMemoryRealloc( void* /*aContext*/, void* aData, unsigned int aSize )
{
	return User::ReAlloc( aData, aSize );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siCommonMemoryFree( void* /*aContext*/, void* aMemblock )
{
	User::Free( aMemblock );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
char siCommonReadChar( void* aContext )
{
	char            ch;
	CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );

    SCT_ASSERT_ALWAYS( spandex != NULL );

	return ( fread( &ch, sizeof( char ), 1, spandex->InputFile() ) == 1 ) ? ch : SCT_END_OF_INPUT;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
int siCommonWriteChar( void* aContext, char aCh )
{
	CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );

    SCT_ASSERT( spandex == siCommonGetContext() );
    SCT_ASSERT_ALWAYS( spandex != NULL );

    spandex->outputBuffer[ spandex->outputSize++ ] = aCh;
    if( spandex->outputSize >= EOutputBufferSize )
    {
        return siCommonFlushOutput( aContext );
    }
    else
    {
        return 1;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
int siCommonFlushOutput( void* aContext )
{
 	CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    int             n;
    
    SCT_ASSERT( spandex == siCommonGetContext() );
    SCT_ASSERT_ALWAYS( spandex != NULL );
    SCT_ASSERT_ALWAYS( spandex->OutputFile() != NULL );

    if( spandex->outputSize == 0 )
    {
        fflush( spandex->OutputFile() );
        return 1;
    }

#if defined( SCT_SYMBIAN_DEBUGPRINT_OUTPUTFILE )
    {
        const int BLOCKSIZE = 128;
        char* p             = spandex->outputBuffer;
        char  c;
        int   index         = 0;

        SCT_ASSERT( BLOCKSIZE < DEBUG_PRINT_OUTPUT_BUFFER_SIZE / 2 );
        
        while( 1 )
        {
            index = sctMin( index + BLOCKSIZE, spandex->outputSize );
            
            c = spandex->outputBuffer[ index ];
            spandex->outputBuffer[ index ] = '\0';
            siCommonDebugPrintf( NULL, "[SPANDEX] %s", p );
            spandex->outputBuffer[ index ] = c;

            if( index >= spandex->outputSize )
            {
                break;
            }
            p = &spandex->outputBuffer[ index ];           
        }
    }
#endif  /* SCT_SYMBIAN_DEBUGPRINT_OUTPUTFILE */

#if !defined( SCT_SYMBIAN_SKIP_WRITING_TO_OUTPUTFILE )
    n = fwrite( spandex->outputBuffer, spandex->outputSize, 1, spandex->OutputFile() );
#else   /* !defined( SCT_SYMBIAN_SKIP_WRITING_TO_OUTPUTFILE ) */
    n = 1;
#endif  /* !defined( SCT_SYMBIAN_SKIP_WRITING_TO_OUTPUTFILE ) */
    
    spandex->outputSize = 0;
    fflush( spandex->OutputFile() );

    return ( n == 1 );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
SCTModuleList* siCommonGetModules( void* /*context*/ )
{
    SCTModuleList*  modules;
    SCTModule*      module;

    modules = sctGetCoreModules();   
    if( modules == NULL )
    {
        return NULL;
    }

#if defined( INCLUDE_SYMBIANBITGDI_MODULE )
    module = sctCreateSymbianBitGDIModule();
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        return NULL;
    }
#else  /* defined( INCLUDE_SYMBIANBITGDI_MODULE ) */
    SCT_USE_VARIABLE( module );
#endif  /* defined( INCLUDE_SYMBIANBITGDI_MODULE ) */

#if defined( INCLUDE_SYMBIANSVG_MODULE )
    module = sctCreateSymbianSVGModule();
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        return NULL;
    }
#endif  /* defined( INCLUDE_SYMBIANSVG_MODULE ) */
    
    return modules;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonSleep( void* /*context*/, unsigned long micros )
{
    User::AfterHighRes( ( int )( micros ) );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonYield( void* /*context*/ )
{
    User::ResetInactivityTime();
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonGetTime( void* /*context*/, char* buffer, int length )
{
    time_t      ltime;
    struct tm*  today;

    SCT_ASSERT_ALWAYS( length >= 1 );

    time( &ltime );
    today = localtime( &ltime );
    if( strftime( buffer, length, "%Y-%m-%d %H:%M:%S", today ) == 0 )
    {
        buffer[ 0 ] = '\0';
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siCommonGetSingleton( void* aContext )
{
    CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );

    SCT_ASSERT_ALWAYS( spandex != NULL );

    return spandex->GetSingleton();
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siCommonSetSingleton( void* aContext, void* ptr )
{
    CSpandexCommon* spandex     = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );

    SCT_ASSERT_ALWAYS( spandex != NULL );

    return spandex->SetSingleton( ptr );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTExtensionProc siCommonGetProcAddress( void* context, const char* procname )
{
    SCT_USE_VARIABLE( context );

#if defined( INCLUDE_EGL_MODULE )
    return ( SCTExtensionProc )( sctEglGetProcAddress( procname ) );
#else   /* defined( INCLUDE_EGL_MODULE ) */
    SCT_USE_VARIABLE( procname );
    return NULL;
#endif  /* defined( INCLUDE_EGL_MODULE ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
int siCommonGetMessage( void* context, SCTMessage* message, long timeoutMillis )
{
    SCT_USE_VARIABLE( context );
    SCT_ASSERT_ALWAYS( message != NULL );
    SCT_USE_VARIABLE( timeoutMillis );

    return -1;
}

/*!
 *
 *
 */
void* siCommonCreateThread( void* aContext, SCTBenchmarkWorkerThreadFunc aFunc, SCTBenchmarkWorkerThreadContext* aThreadContext )
{
    CSpandexCommon* spandex     = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    RThread*        thread;
    HBufC*          threadName;
    TInt            r;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateThread start" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( spandex != NULL );    
    SCT_ASSERT_ALWAYS( aFunc != NULL );
    SCT_ASSERT_ALWAYS( aThreadContext != NULL );

    thread = new RThread;
    if( thread == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateThread thread allocation failed" );
#endif  /* defined( SCT_DEBUG ) */       
        return NULL;
    }

    threadName = HBufC::New( 128 );
    if( threadName == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateThread allocation failed" );
#endif  /* defined( SCT_DEBUG ) */        
        delete thread;
        return NULL;
    }

    threadName->Des().Format( _L( "Spandex worker thread %x%u" ), aThreadContext, siCommonGetTimerTick( NULL ) );    
    
    r = thread->Create( *threadName,
                        ( TThreadFunction )( aFunc ),
                        0x14000,
                        &( User::Heap() ),
                        aThreadContext );

    delete threadName;
    
    if( r != KErrNone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateThread create thread failed, %d", r );
#endif  /* defined( SCT_DEBUG ) */
        delete thread;
        return NULL;
    }

    thread->Resume();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateThread end %x", thread );
#endif  /* defined( SCT_DEBUG ) */
    
    return thread;
}

/*!
 *
 */
void siCommonThreadCleanup( void* aContext )
{
    SCT_USE_VARIABLE( aContext );
    
    /* Fix symbian. */
    CloseSTDLIB();
}

/*!
 *
 *
 */
void siCommonJoinThread( void* aContext, void* aThread )
{
    CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    RThread*        thread;
    TRequestStatus  status;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonJoinThread start %x", aThread );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( spandex != NULL );        
    SCT_ASSERT_ALWAYS( aThread != NULL );

    thread = ( RThread* )( aThread );
    
    thread->Logon( status );
    User::WaitForRequest( status );

    thread->Close();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonJoinThread end %x", aThread );
#endif  /* defined( SCT_DEBUG ) */
    
    delete thread;
}

/*!
 *
 *
 */
SCTBoolean siCommonSetThreadPriority( void* aContext, SCTThreadPriority aPriority )
{
    CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonSetThreadPriority start" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( spandex != NULL );        

    RThread         t;
    TThreadPriority p;

    switch( aPriority )
    {
    case SCT_THREAD_PRIORITY_LOW:
        p = EPriorityLess;
        break;
    case SCT_THREAD_PRIORITY_NORMAL:
        p = EPriorityNormal;
        break;
    case SCT_THREAD_PRIORITY_HIGH:
        p = EPriorityMore;
        break;
    default:
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonSetThreadPriority unexpected priority" );
#endif  /* defined( SCT_DEBUG ) */       
        SCT_ASSERT_ALWAYS( 0 );
        return SCT_FALSE;
    }   

    t.SetPriority( p );
    t.Close();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonSetThreadPriority end" );
#endif  /* defined( SCT_DEBUG ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void* siCommonCreateMutex( void* aContext )
{
    CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    RMutex*         mutex;
    TInt            r;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateMutex start" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( spandex != NULL );        

    mutex = new RMutex;
    if( mutex == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateMutex mutex allocation failed" );
#endif  /* defined( SCT_DEBUG ) */
        return NULL;
    }

    r = mutex->CreateLocal();
    
    if( r != KErrNone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateMutex mutex creation failed" );
#endif  /* defined( SCT_DEBUG ) */
        delete mutex;
        return NULL;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateMutex end %x", mutex );
#endif  /* defined( SCT_DEBUG ) */
    
    return mutex;
}

/*!
 *
 *
 */
void siCommonLockMutex( void* aContext, void* aMutex )
{
    CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    RMutex*         mutex;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonLockMutex start %x", aMutex );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( spandex != NULL );            
    SCT_ASSERT_ALWAYS( aMutex != NULL );

    mutex = ( RMutex* )( aMutex );
    
    mutex->Wait();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonLockMutex end %x", aMutex );
#endif  /* defined( SCT_DEBUG ) */   
}

/*!
 *
 *
 */
void siCommonUnlockMutex( void* aContext, void* aMutex )
{
    CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    RMutex*         mutex;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonUnlockMutex start %x", aMutex );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( spandex != NULL );            
    SCT_ASSERT_ALWAYS( aMutex != NULL );

    mutex = ( RMutex* )( aMutex );
    
    mutex->Signal();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonUnlockMutex end %x", aMutex );
#endif  /* defined( SCT_DEBUG ) */
    
}
        
/*!
 *
 *
 */
void siCommonDestroyMutex( void* aContext, void* aMutex )
{
    CSpandexCommon* spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    RMutex*         mutex;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonDestroyMutex start %x", aMutex );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( spandex != NULL );            
    SCT_ASSERT_ALWAYS( aMutex != NULL );

    mutex = ( RMutex* )( aMutex );

    mutex->Close();
    delete mutex;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonDestroyMutex end %x", aMutex );
#endif  /* defined( SCT_DEBUG ) */
}

/*!
 *
 *
 */ 
struct SCTSymbianSignal
{
    RMutex*         mutex;
    RSemaphore*     semaphore;
    volatile int    enabled;
    volatile int    waiting;
};

/*!
 *
 *
 */
void* siCommonCreateSignal( void* aContext )
{
    CSpandexCommon*     spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    SCTSymbianSignal*   signal;
    TInt                r;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateSignal start" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( spandex != NULL );            
    
    signal = ( SCTSymbianSignal* )( siCommonMemoryAlloc( spandex, sizeof( SCTSymbianSignal ) ) );
    if( signal == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateSignal signal allocation failed" );
#endif  /* defined( SCT_DEBUG ) */
        return NULL;
    }

    memset( signal, 0, sizeof( SCTSymbianSignal ) );

    signal->mutex = ( RMutex* )( siCommonCreateMutex( spandex ) );
    if( signal->mutex == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateSignal mutex allocation failed" );
#endif  /* defined( SCT_DEBUG ) */        
        siCommonDestroySignal( spandex, signal );
        return NULL;
    }

    signal->semaphore = new RSemaphore;
    if( signal->semaphore == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateSignal semaphore allocation failed" );
#endif  /* defined( SCT_DEBUG ) */        
        siCommonDestroySignal( spandex, signal );
        return NULL;
    }

    r = signal->semaphore->CreateLocal( 0 );

    if( r != KErrNone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateSignal semaphore creation failed" );
#endif  /* defined( SCT_DEBUG ) */        
        siCommonDestroySignal( spandex, signal );
        return NULL;
    }
    
    signal->enabled = 1;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonCreateSignal end %x", signal );
#endif  /* defined( SCT_DEBUG ) */
    
    return signal;
}

/*!
 *
 *
 */
void siCommonDestroySignal( void* aContext, void* aSignal )
{
    CSpandexCommon*     spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    SCTSymbianSignal*   signal;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonDestroySignal start %x", aSignal );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( spandex != NULL );                
    SCT_ASSERT_ALWAYS( aSignal != NULL );

    signal = ( SCTSymbianSignal* )( aSignal );
    
    if( signal->semaphore != NULL )
    {
        signal->semaphore->Close();
        delete signal->semaphore;
        signal->semaphore = NULL;
    }

    if( signal->mutex != NULL )
    {
        signal->mutex->Close();
        delete signal->mutex;
        signal->mutex = NULL;
    }

    siCommonMemoryFree( NULL, signal );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonDestroySignal end %x", aSignal );
#endif  /* defined( SCT_DEBUG ) */    
}

/*!
 *
 *
 */
int siCommonWaitForSignal( void* aContext, void* aSignal, long aTimeoutMillis )
{
    CSpandexCommon*     spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    SCTSymbianSignal*   signal;    
    TInt                r;
    int                 enabled;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonWaitForSignal start %x", aSignal );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( spandex != NULL );                
    SCT_ASSERT_ALWAYS( aSignal != NULL );

    signal = ( SCTSymbianSignal* )( aSignal );

    /* First, lock signal mutex. */    
    signal->mutex->Wait();
    
    /* Check if the signal is enabled. */
    if( signal->enabled == 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonWaitForSignal signal %x disabled", aSignal );
#endif  /* defined( SCT_DEBUG ) */
        signal->mutex->Signal();        
        return -1;
    }
    signal->waiting++;
    
    signal->mutex->Signal();        
    
    /* Wait for the signal event. */
    if( aTimeoutMillis > 0 )
    {
        r = signal->semaphore->Wait( aTimeoutMillis * 1000 );
    }
    else
    {
        signal->semaphore->Wait();
        r = KErrNone;
    }

    /* Lock signal mutex again. */
    signal->mutex->Wait();
    
    /* Got signal event, get enabled. */
    enabled = signal->enabled;
    signal->waiting--;
    
    /* Release signal mutex and return wait status. */
    signal->mutex->Signal();   

    /* Signal was disabled, or semaphore is about to be destroyed. */
    if( enabled == 0 || r == KErrGeneral )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonWaitForSignal signal %x wait failed", aSignal );
#endif  /* defined( SCT_DEBUG ) */
        return -1;
    }

    /* Wait was ok. */
    if( r == KErrNone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonWaitForSignal signal %x wait ok", aSignal );
#endif  /* defined( SCT_DEBUG ) */        
        return 1;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonWaitForSignal signal %x wait timeout", aSignal );
#endif  /* defined( SCT_DEBUG ) */   
    
    /* Timeout must have happened. */
    return 0;
}

/*!
 *
 *
 */
void siCommonTriggerSignal( void* aContext, void* aSignal )
{
    CSpandexCommon*     spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    SCTSymbianSignal*   signal;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonTriggerSignal start %x", aSignal );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_ASSERT_ALWAYS( spandex != NULL );                
    SCT_ASSERT_ALWAYS( aSignal != NULL );

    signal = ( SCTSymbianSignal* )( aSignal );

    /* Lock signal mutex */
    signal->mutex->Wait();
    
    if( signal->enabled != 0 )
    {
        signal->semaphore->Signal();
    }

    /* Release signal mutex. */
    signal->mutex->Signal();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonTriggerSignal end %x", aSignal );
#endif  /* defined( SCT_DEBUG ) */    
}

/*!
 *
 *
 */
void siCommonCancelSignal( void* aContext, void* aSignal )
{
    CSpandexCommon*     spandex = ( CSpandexCommon* )( aContext ? aContext : siCommonGetContext() );
    SCTSymbianSignal*   signal;

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonCancelSignal start %x", aSignal );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( spandex != NULL );                
    SCT_ASSERT_ALWAYS( aSignal != NULL );

    signal = ( SCTSymbianSignal* )( aSignal );

    /* Lock signal mutex */
    signal->mutex->Wait();

    /* Set signal as disabled. */
    signal->enabled = 0;

    /* Wake up all threads waiting for the signal event. Also set the event
     * for threads just about to start the wait. */
    if( signal->waiting > 0 )
    {
        signal->semaphore->Signal( signal->waiting );
    }
    
    /* Release signal mutex. */
    signal->mutex->Signal();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonCancelSignal end %x", aSignal );
#endif  /* defined( SCT_DEBUG ) */    
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
void siCommonProfileEvent( void* context, const char* event )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( event );
    /* TODO */
}

