/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_types.h"
#include "sct_utils.h"
#include "sct_sicommon.h"
#include "sct_sifile.h"
#include <stdio.h>
#include <string.h>
#include <f32file.h>

/*!
 *
 */
typedef struct
{
    RFs                 fs;

} SCTSymbianSiFileContext;

/*!
 *
 */
void* siFileCreateContext()
{
    SCTSymbianSiFileContext*    context;

    context = ( SCTSymbianSiFileContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTSymbianSiFileContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTSymbianSiFileContext ) );

    context->fs = RFs();
    if( context->fs.Connect() != KErrNone )
    {
        siFileDestroyContext( context );
        return NULL;
    }
    
    return context;
}

/*!
 *
 */
void siFileDestroyContext( void* context )
{
    SCTSymbianSiFileContext*    c;

    if( context != NULL )
    {
        c = ( SCTSymbianSiFileContext* )( context );
        c->fs.Close();
        
        siCommonMemoryFree( NULL, c );
    }
}

/*!
 *
 */    
SCTFile siFileOpen( void* context, const char* name, const char* mode )
{
    SCT_ASSERT_ALWAYS( context != NULL );
    return ( SCTFile )( fopen( name, mode ) );
}

/*!
 *
 */
void siFileClose( void* context, SCTFile file )
{
    SCT_ASSERT_ALWAYS( context != NULL );
    fclose( ( FILE* )( file ) );
}

/*!
 *
 */
unsigned int siFileRead( void* context, SCTFile file, char* data, unsigned int length )
{
    SCT_ASSERT_ALWAYS( context != NULL );
    return ( fread( data, 1, length, ( FILE* )( file ) ) );
}

/*!
 *
 */
int siFileReadByte( void* context, SCTFile file )
{
    SCT_ASSERT_ALWAYS( context != NULL );
    return getc( ( FILE* )( file ) );
}

/*!
 *
 */
unsigned int siFileWrite( void* context, SCTFile file, const char* data, unsigned int length )
{
    SCT_ASSERT_ALWAYS( context != NULL );
    return ( unsigned int )( fwrite( data, length, 1, ( FILE* )( file ) ) * length );
}

/*!
 *
 */
SCTBoolean siFileEOF( void* context, SCTFile file )
{
    SCT_ASSERT_ALWAYS( context != NULL );
    return ( SCTBoolean )( feof( ( FILE* )( file ) ) );
}

/*!
 *
 */
int siFileSize( void* context, SCTFile file )
{
    long        currentPosition;
    long        fileSize;
    int         status;

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( file != NULL );

    // Store current file position
    currentPosition = ftell( ( FILE* )( file ) );

    // Set file position to the end of the file
    status = fseek( ( FILE* )( file ), 0, SEEK_END );
    SCT_ASSERT_ALWAYS( status == 0 );

    // Get the file position at the end of the file (=file size)
    fileSize = ftell( ( FILE* )( file ) );

    // Restore the original file position
    status = fseek( ( FILE* )( file ), currentPosition, SEEK_SET );
    SCT_ASSERT_ALWAYS( status == 0 );

    return fileSize;
}

/*!
 *
 */
SCTBoolean siFileMkdir( void* context, const char* name )
{
    SCTSymbianSiFileContext*    c;
    char*                       buf;
    HBufC*                      hbuf;
    int                         len, newlen;
    int                         i;
    SCTBoolean                  status                  = SCT_TRUE;
    TInt                        err;

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );

    c = ( SCTSymbianSiFileContext* )( context );

    /* Create temporary buffer for dir name manipulation. */
    len    = strlen( name );
    buf = ( char* )( siCommonMemoryAlloc( NULL, len + 1 ) );
    if( buf == NULL )
    {
        return SCT_FALSE;
    }
    sctStrncopy( buf, name, len );
    newlen = len;

    /* Replace all slashes with backslashes. Find the last backslash. */
    for( i = 0; i < len; ++i )
    {
        if( buf[ i ] == '/' || buf[ i ] == '\\' )
        {
            buf[ i ] = '\\';
            newlen = i;
        }
    }

    /* Make sure the path ends with a backslash, i.e. remove any file
     * name after last backslash. */
    buf[ newlen + 1 ] = '\0';

    hbuf = HBufC::New( newlen + 1 );
    if( hbuf != NULL )
    {
        /* Fix synpian. */
        TPtrC8 tname( ( const TUint8* )( buf ) );
        hbuf->Des().Copy( tname );
    }
    else
    {
        status = SCT_FALSE;
    }

    if( status == SCT_TRUE )
    {
        err = c->fs.MkDirAll( *hbuf );
        if( err != KErrNone && err != KErrAlreadyExists )
        {
            status = SCT_FALSE;
        }
    }

    /* Dealloc and return. */
    siCommonMemoryFree( NULL, buf );
    delete hbuf;

    return status;
}
