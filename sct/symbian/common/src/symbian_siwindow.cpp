/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Note: this same file should work in both S60 and NCP mingui releases. */

#include <e32base.h>
#include <e32cons.h>
#include <e32std.h>
#include <w32std.h>

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <hal.h>

#if defined( SCT_S60_SUPPORT_SCREEN_ORIENTATION )
# include <aknappui.h>
# include <aknviewappui.h>
#endif  /* defined( SCT_S60_SUPPORT_SCREEN_ORIENTATION ) */

#if defined( SCT_S60_SUPPORT_COMPOSITION )
# include <platform/mw/alf/alfcompositionutility.h>
#endif  /* defined( SCT_S60_SUPPORT_COMPOSITION ) */

#include <platform/graphics/displaycontrol.h>
#include <sgresource/sgimage.h>

#include "sct_utils.h"
#include "sct_sicommon.h"
#include "sct_siwindow.h"

#include "CPlatform.h"

/*!
 *
 */
static void         sctiSetScreenOrientationL( SCTScreenOrientation orientation );
static TDisplayMode sctiConvertColorFormat( SCTColorFormat format );
static TInt         sctiConvertSgColorFormat( SCTColorFormat format );
static TInt         sctiColorFormatPixelSize( SCTColorFormat format );
static TUint32      sctiConvertSgUsage( int usage );
static SCTBoolean   sctiGetHDMIDisplaySize( void* context, int* width, int* height  );

/*!
 *
 */
typedef struct
{
    RWindow*            window;
	RWsSession*         wsSession;
	RWindowGroup*       windowGroup;
    SCTScreen           screen;
    CWsScreenDevice*    device;
    CWindowGc*          gc;
    SCTBoolean          isChild;
} SCTSymbianWindow;

/*!
 *
 *
 */
typedef enum
{
    SCT_SYMBIAN_PIXMAP_FBSBITMAP,
    SCT_SYMBIAN_PIXMAP_SGIMAGE,    
} SCTSymbianPixmapType;

/*!
 *
 */
typedef struct
{
    SCTSymbianPixmapType    type;    
    void*                   pixmap;
} SCTSymbianPixmap;

/*!
 *
 */
typedef struct
{
    TUint64             mainThreadId;
    RSgDriver*          sgDriver;
} SCTSymbianSiWindowContext;

/*!
 *
 *
 */
void* siWindowCreateContext()
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext start" );
#endif  /* defined( SCT_DEBUG ) */

    SCTSymbianSiWindowContext* context;

    context = ( SCTSymbianSiWindowContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTSymbianSiWindowContext ) ) );
    if( context == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext allocation failed" );
#endif  /* defined( SCT_DEBUG ) */
        return NULL;
    }
    memset( context, 0, sizeof( SCTSymbianSiWindowContext ) );
    
    /* NOTE: it seems that some of the symbian window server/bitgdi/whatever
     * functionality works only from the main thread, hence the following hack:
     * we store the thread id of the thread that creates the window system
     * context with the assumption that it is the main thread. Later on, we can
     * compare the current thread id to the main thread id to detect calls from
     * worker threads.  */
    context->mainThreadId = RThread().Id().Id();

    /* Create and open SgDriver. */
    context->sgDriver = new RSgDriver;
    if( context->sgDriver == NULL || context->sgDriver->Open() != KErrNone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext opening sgdriver failed" );
#endif  /* defined( SCT_DEBUG ) */

        siWindowDestroyContext( context );
        return NULL;
    }
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext done" );
#endif  /* defined( SCT_DEBUG ) */
    
    return context;   
}

/*!
 *
 *
 */
void siWindowDestroyContext( void* context )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyContext start" );
#endif  /* defined( SCT_DEBUG ) */

    SCTSymbianSiWindowContext* windowContext;

    windowContext = ( SCTSymbianSiWindowContext* )( context );

    if( windowContext != NULL )
    {
        if( windowContext->sgDriver != NULL )
        {
            windowContext->sgDriver->Close();
            delete windowContext->sgDriver;
            windowContext->sgDriver = NULL;
        }
    }
    
    if( windowContext != NULL )
    {
        siCommonMemoryFree( NULL, windowContext );
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyContext done" );
#endif  /* defined( SCT_DEBUG ) */   
}

/*!
 *
 *
 */
SCTBoolean siWindowIsScreenSupported( void* context, SCTScreen screen )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowIsScreenSupported" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );    

    if( screen == SCT_SCREEN_HDMI )
    {
        return sctiGetHDMIDisplaySize( context, NULL, NULL );
    }
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void siWindowGetScreenSize( void* context, SCTScreen screen, int* width, int* height )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetScreenSize" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );        
    SCT_ASSERT_ALWAYS( width != NULL );
    SCT_ASSERT_ALWAYS( height != NULL );

    if( screen == SCT_SCREEN_HDMI )
    {
        SCT_ASSERT_ALWAYS( sctiGetHDMIDisplaySize( context, width, height ) != SCT_FALSE );
    }
    
    CPlatform::ScreenSize( *width, *height );
}

/*!
 *
 *
 */
void* siWindowGetScreenEglDisplayId( void* context, SCTScreen screen )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetScreenEglDisplay" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( screen );

    return ( void* )( 0 );
}

/*!
 *
 *
 */
SCTBoolean siWindowSetScreenOrientation( void* context, SCTScreen screen, SCTScreenOrientation orientation )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetScreenOrientation start" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );

    if( screen == SCT_SCREEN_HDMI )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetScreenOrientation HDMI orientation not supported" );
#endif  /* defined( SCT_DEBUG ) */
        return SCT_FALSE;
    }
    
#if defined( SCT_S60_SUPPORT_SCREEN_ORIENTATION )
    SCTScreenOrientation    o;   
    CAknAppUi*              appUi = ( ( CAknAppUi* )( CEikonEnv::Static()->EikAppUi() ) );

    if( orientation == SCT_SCREEN_ORIENTATION_DEFAULT )
    {
        int w, h;
        CPlatform::ScreenSize( w, h );
        if( w > h )
        {
            o = SCT_SCREEN_ORIENTATION_LANDSCAPE;
        }
        else
        {
            o = SCT_SCREEN_ORIENTATION_PORTRAIT;
        }
    }
    else
    {
        o = orientation;
    }
    
    if( o != SCT_SCREEN_ORIENTATION_PORTRAIT &&
        o != SCT_SCREEN_ORIENTATION_LANDSCAPE )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctiSetScreenOrientationL unexpected orientation" );
#endif  /* defined( SCT_DEBUG ) */
        
        return SCT_FALSE;
    }

	TRAPD( error, sctiSetScreenOrientationL( o ) );
    if( error != KErrNone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctiSetScreenOrientationL failed" );
#endif  /* defined( SCT_DEBUG ) */

        return SCT_FALSE;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetScreenOrientation done" );
#endif  /* defined( SCT_DEBUG ) */
    
    return SCT_TRUE;
#else   /* defined( SCT_S60_SUPPORT_SCREEN_ORIENTATION ) */
    SCT_USE_VARIABLE( orientation );
    SCT_USE_VARIABLE( sctiSetScreenOrientationL );
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetScreenOrientation not supported" );
#endif  /* defined( SCT_DEBUG ) */
    
    return SCT_FALSE;   
#endif  /* defined( SCT_S60_SUPPORT_SCREEN_ORIENTATION ) */
}

/*!
 *
 *
 */
void* siWindowCreateWindow( void* context, SCTScreen screen, int x, int y, int width, int height, SCTColorFormat format, void* parent )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow start" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );            
    SCT_ASSERT_ALWAYS( width >= 0 );
    SCT_ASSERT_ALWAYS( height >= 0 );

    SCTSymbianWindow*           parentWindow;
    TDisplayMode                mode;
    TDisplayMode                actualMode;
    SCTSymbianWindow*           window;
    SCTSymbianSiWindowContext*  c;

    c = ( SCTSymbianSiWindowContext* )( context );
    
    window = ( SCTSymbianWindow* )( siCommonMemoryAlloc( NULL, sizeof( SCTSymbianWindow ) ) );
    if( window == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow allocation failed" );
#endif  /* defined( SCT_DEBUG ) */        
        return NULL;
    }
    memset( window, 0, sizeof( SCTSymbianWindow ) );

    window->isChild = SCT_FALSE;
    
    mode = sctiConvertColorFormat( format );
    if( mode == ENone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow unsupported format" );
#endif  /* defined( SCT_DEBUG ) */
        siWindowDestroyWindow( context, window );            
        return NULL;       
    }

    parentWindow = ( SCTSymbianWindow* )( parent );   
    
    if( parentWindow == NULL )  /* Main window */
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow main window" );
#endif  /* defined( SCT_DEBUG ) */
        
        window->isChild = SCT_FALSE;
        window->screen  = screen;
    
        window->wsSession = new RWsSession();
        if( ( window == NULL ) || ( window->wsSession->Connect() != KErrNone ) )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow failed to setup window server session" );
#endif  /* defined( SCT_DEBUG ) */
            siWindowDestroyWindow( context, window );                    
            return NULL;
        }

        if( screen == SCT_SCREEN_HDMI ) /* HDMI */
        {
            /* HACK: support HDMI only from the main thread. */
            if( c->mainThreadId != RThread().Id().Id() )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow HDMI supported only from the main thread" );
#endif  /* defined( SCT_DEBUG ) */
                siWindowDestroyWindow( context, window );
                return NULL;
            }

#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow HDMI create screen device" );
#endif  /* defined( SCT_DEBUG ) */
            
            window->device = new CWsScreenDevice( *( window->wsSession ) );
            if( window->device == NULL || window->device->Construct( 1 ) != KErrNone )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow creating HDMI screen device failed" );
#endif  /* defined( SCT_DEBUG ) */
                siWindowDestroyWindow( context, window );
                return NULL;            
            }
        
            window->device->SetAppScreenMode( window->device->CurrentScreenMode() );
            
            MDisplayControl*        displayControl;
            TDisplayConfiguration   displayConfig;
            TSize                   displaySize( 0,0 );
            
            displayControl = ( MDisplayControl* )( window->device->GetInterface( MDisplayControl::ETypeId ) );
            displayControl->GetConfiguration( displayConfig );
            displayConfig.GetResolution( displaySize );
            
            if( displaySize.iWidth <= 0 || displaySize.iHeight <= 0 )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow HDMI not connected" );
#endif  /* defined( SCT_DEBUG ) */
                siWindowDestroyWindow( context, window );
                return NULL;            
            } 

#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow HDMI create window group" );
#endif  /* defined( SCT_DEBUG ) */
            
            window->windowGroup = new RWindowGroup( *( window->wsSession ) );
            if( ( window->windowGroup == NULL ) ||
                ( window->windowGroup->Construct( ( TUint32 )( window ), window->device ) != KErrNone ) )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow HDMI failed to create window group" );
#endif  /* defined( SCT_DEBUG ) */
                siWindowDestroyWindow( context, window );
                return NULL;
            }       
        }
        else    /* !HDMI */
        {
            if( c->mainThreadId == RThread().Id().Id() )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow create screen device" );
#endif  /* defined( SCT_DEBUG ) */
                
                window->device = new CWsScreenDevice( *( window->wsSession ) );
                if( ( window->device == NULL ) || ( window->device->Construct() != KErrNone ) )
                {
#if defined( SCT_DEBUG )
                    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow creating screen device for window failed" );
#endif  /* defined( SCT_DEBUG ) */        
                    siWindowDestroyWindow( context, window );                
                    return NULL;
                }
            }

#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow create window group" );
#endif  /* defined( SCT_DEBUG ) */
            
            window->windowGroup = new RWindowGroup( *( window->wsSession ) );
            if( ( window->windowGroup == NULL ) || ( window->windowGroup->Construct( ( TUint32 )( window ) ) != KErrNone ) )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow failed to create window group" );
#endif  /* defined( SCT_DEBUG ) */
                siWindowDestroyWindow( context, window );
                return NULL;
            }
        }
    }
    else    /* Child window */
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow child window" );
#endif  /* defined( SCT_DEBUG ) */
        
        window->isChild     = SCT_TRUE;
        window->screen      = parentWindow->screen;

        /* Child uses the context of the parent, where applicable. */
        window->windowGroup = parentWindow->windowGroup;
        window->wsSession   = parentWindow->wsSession;
        window->device      = parentWindow->device;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow create RWindow" );
#endif  /* defined( SCT_DEBUG ) */
    
    window->window = new RWindow( *( window->wsSession ) );
	if( ( window->window == NULL ) ||
        ( window->window->Construct( *( window->windowGroup ), ( TUint32 )( window ) ) != KErrNone ) )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow window construction failed" );
#endif  /* defined( SCT_DEBUG ) */        
        siWindowDestroyWindow( context, window );        
        return NULL;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow set window display mode" );
#endif  /* defined( SCT_DEBUG ) */
    
    actualMode = ( TDisplayMode )( window->window->SetRequiredDisplayMode( mode ) );
    if( ( format != SCT_COLOR_FORMAT_DEFAULT ) && ( actualMode != mode ) )    
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow setting RWindow display mode failed, requested=%d actual=%d", mode, actualMode );
#endif  /* defined( SCT_DEBUG ) */        
        siWindowDestroyWindow( context, window );                
        return NULL;
    }

    if( window->gc == NULL )
    {
        /* HACK: support bitmap blitting only from the main thread. */
        if( c->mainThreadId == RThread().Id().Id() )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow create window GC" );
#endif  /* defined( SCT_DEBUG ) */
            
            window->gc = new CWindowGc( window->device );
            if( ( window->gc == NULL ) || ( window->gc->Construct() != KErrNone ) )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow creating window gc for window failed" );
#endif  /* defined( SCT_DEBUG ) */        
                siWindowDestroyWindow( context, window );                
                return NULL;
            }
        }
        else
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow bitmap blitting disabled from the worker thread" );
#endif  /* defined( SCT_DEBUG ) */        
        }
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow setting window extent and background color" );
#endif  /* defined( SCT_DEBUG ) */
    
	window->window->SetExtent( TPoint( x, y ), TSize( width, height ) );
	window->window->SetBackgroundColor();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow activating window" );
#endif  /* defined( SCT_DEBUG ) */
    
	window->window->Activate();
    window->wsSession->Flush();    

    if( window->gc != NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow activating window GC" );
#endif  /* defined( SCT_DEBUG ) */
        
        window->gc->Activate( *( window->window ) );
        window->wsSession->Flush();
    }

    window->wsSession->Finish();    
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow done" );
#endif  /* defined( SCT_DEBUG ) */

    return window;
}

/*!
 *
 *
 */
void siWindowDestroyWindow( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyWindow start" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );    
    SCT_ASSERT_ALWAYS( window != NULL );    

    SCTSymbianWindow*   w = ( SCTSymbianWindow* )( window );

    if( w->gc != NULL )
    {
        w->gc->Deactivate();
        delete w->gc;
        w->gc = NULL;
    }

    if( w->isChild == SCT_TRUE )
    {
        w->device = NULL;
    }   
    else if( w->device != NULL )
    {
        delete w->device;
        w->device = NULL;
    }

    if( w->window != NULL )
    {
        w->window->Close();
        SCT_ASSERT_ALWAYS( w->wsSession != NULL );
        w->wsSession->Flush();        
        delete w->window;
        w->window = NULL;
    }

    if( w->isChild == SCT_TRUE )
    {
        w->windowGroup = NULL;
    }   
    else if( w->windowGroup != NULL )
    {
        w->windowGroup->Close();
        SCT_ASSERT_ALWAYS( w->wsSession != NULL );
        w->wsSession->Flush();        
        delete w->windowGroup;
        w->windowGroup = NULL;
    }

    if( w->isChild == SCT_TRUE )
    {
        w->wsSession = NULL;
    }   
    else if( w->wsSession != NULL  )
    {
        w->wsSession->Flush();
        w->wsSession->Close();
        delete w->wsSession;
        w->wsSession = NULL;
    }

    siCommonMemoryFree( NULL, w );
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyWindow done" );
#endif  /* defined( SCT_DEBUG ) */    
}

/*!
 *
 *
 */
void* siWindowGetEglWindow( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetEglWindow" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( window != NULL );
    
    SCTSymbianWindow* w = ( SCTSymbianWindow* )( window );

    return w->window;
}

/*!
 *
 *
 */
SCTScreen siWindowGetWindowScreen( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowQueryWindowScreen" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );        
    SCT_ASSERT_ALWAYS( window != NULL );

    SCTSymbianWindow* w = ( SCTSymbianWindow* )( window );
    
    return w->screen;
}

/*!
 *
 *
 */
SCTBoolean siWindowToFront( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowToFront start" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );            
    SCT_ASSERT_ALWAYS( window != NULL );

    SCTSymbianWindow* w = ( SCTSymbianWindow* )( window );    

    SCT_ASSERT( w->window != NULL );
    SCT_ASSERT( w->wsSession != NULL );
    
    w->window->SetOrdinalPosition( 0 );
    w->wsSession->Finish();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowToFront done" );
#endif  /* defined( SCT_DEBUG ) */

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean siWindowResize( void* context, void* window, int x, int y, int width, int height )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowResize start" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( window != NULL );

    SCTSymbianWindow* w = ( SCTSymbianWindow* )( window );        

    SCT_ASSERT( w->window != NULL );
    SCT_ASSERT( w->wsSession != NULL );
    
    w->window->SetExtent( TPoint( x, y ), TSize( width, height ) );
    w->wsSession->Finish();

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowResize done" );
#endif  /* defined( SCT_DEBUG ) */
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean siWindowSetOpacity( void* context, void* window, float opacity )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetOpacity start" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( window != NULL );

#if defined( SCT_S60_SUPPORT_COMPOSITION )
    SCTSymbianWindow*       w       = ( SCTSymbianWindow* )( window );            
    CAlfCompositionSource*  alfCS   = NULL;

    SCT_ASSERT( w->window != NULL );
    TRAPD( error, alfCS = CAlfCompositionSource::NewL( *( w->window ) ) );
    
    if( alfCS == NULL || error != KErrNone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetOpacity invalid composition source" );
#endif  /* defined( SCT_DEBUG ) */

        if( alfCS != NULL )
        {
            delete alfCS;
            alfCS = NULL;
        }
        
        return SCT_FALSE;
    }

    /* Global alpha. */
    alfCS->SetOpacity( opacity );
    delete alfCS;
    alfCS = NULL;

    if( opacity < 1.0f )
    {
        w->window->SetSurfaceTransparency( ETrue );
    }
    else
    {
        w->window->SetSurfaceTransparency( EFalse );
    }
    
    w->wsSession->Finish();
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetOpacity done" );
#endif  /* defined( SCT_DEBUG ) */
    
    return SCT_TRUE;
#else   /* defined( SCT_S60_SUPPORT_COMPOSITION ) */
    SCT_USE_VARIABLE( opacity );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetOpacity not supported" );
#endif  /* defined( SCT_DEBUG ) */
    
    return SCT_FALSE;
#endif  /* defined( SCT_S60_SUPPORT_COMPOSITION ) */
}

/*!
 *
 *
 */
SCTBoolean siWindowSetAlpha( void* context, void* window, SCTBoolean enable )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetAlpha start" );
#endif  /* defined( SCT_DEBUG ) */

    SCTSymbianWindow*   w   = ( SCTSymbianWindow* )( window );            
    TBool               b   = ( enable == SCT_TRUE ? ETrue : EFalse );
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( window != NULL );
    SCT_ASSERT_ALWAYS( w->window != NULL );

    w->window->SetSurfaceTransparency( b );
    w->wsSession->Finish();   

    return SCT_TRUE;   
}

/*!
 *
 *
 */
SCTBoolean siWindowRefresh( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowRefresh" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( window != NULL );

    SCTSymbianWindow*   w   = ( SCTSymbianWindow* )( window );            
    
    w->window->Invalidate();
    w->wsSession->Finish();
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void* siWindowCreatePixmap( void* context, int width, int height, SCTColorFormat format, void* data, int length )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap start" );
#endif  /* defined( SCT_DEBUG ) */

    SCT_ASSERT_ALWAYS( context != NULL );

    TDisplayMode        displayMode;
    CFbsBitmap*         bitmap;
    SCTSymbianPixmap*   symbianPixmap;

    symbianPixmap = ( SCTSymbianPixmap* )( siCommonMemoryAlloc( NULL, sizeof( SCTSymbianPixmap ) ) );
    if( symbianPixmap == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap allocation failed" );
#endif  /* defined( SCT_DEBUG ) */
        return NULL;       
    }
    memset( symbianPixmap, 0, sizeof(  SCTSymbianPixmap ) );
    symbianPixmap->type = SCT_SYMBIAN_PIXMAP_FBSBITMAP;
    
    displayMode = sctiConvertColorFormat( format );

    if( displayMode == ENone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap unsupported format" );
#endif  /* defined( SCT_DEBUG ) */
        
        siWindowDestroyPixmap( context, symbianPixmap ); 
        return NULL;       
    }
    
    TSize size( width, height );
    bitmap = new CFbsBitmap;
    symbianPixmap->pixmap = bitmap;
    
    if( bitmap == NULL || bitmap->Create( size, displayMode ) != KErrNone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap bitmap creation failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        siWindowDestroyPixmap( context, symbianPixmap );
        return NULL;
    }

    if( data != NULL )
    {
        TInt pixelSize = sctiColorFormatPixelSize( format );
        
        if( pixelSize < 0 )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap unsupported data format" );
#endif  /* defined( SCT_DEBUG ) */
          
            siWindowDestroyPixmap( context, symbianPixmap );
            return NULL;
        }

        if( width * height * pixelSize != length )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap incompatible data" );
#endif  /* defined( SCT_DEBUG ) */
          
            siWindowDestroyPixmap( context, symbianPixmap );
            return NULL;
        }
        
        unsigned char* p    = ( unsigned char* )( bitmap->DataAddress() );
        int dataStride      = pixelSize * width;
        int bmStride        = bitmap->ScanLineLength( width, displayMode );
        int r1;
        int r2;

        /* Data is upside-down for CFbsBitmap. */
        for( r1 = height - 1, r2 = 0; r1 >= 0; --r1, ++r2 )
        {
            memcpy( p + bmStride * r2, ( unsigned char* )( data ) + dataStride * r1, bmStride );
        }
    }
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap done" );
#endif  /* defined( SCT_DEBUG ) */
    
    return symbianPixmap;
}

/*!
 *
 *
 */
void* siWindowCreateSharedPixmap( void* context, int width, int height, SCTColorFormat format, int usage, void* data, int length )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap start" );
#endif  /* defined( SCT_DEBUG ) */

    SCTSymbianPixmap*   symbianPixmap;

    symbianPixmap = ( SCTSymbianPixmap* )( siCommonMemoryAlloc( NULL, sizeof( SCTSymbianPixmap ) ) );
    if( symbianPixmap == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap allocation failed" );
#endif  /* defined( SCT_DEBUG ) */
        return NULL;       
    }
    memset( symbianPixmap, 0, sizeof(  SCTSymbianPixmap ) );
    symbianPixmap->type = SCT_SYMBIAN_PIXMAP_SGIMAGE;
    
    TSize   sz( width, height );
    TInt    sgPixelFormat   = sctiConvertSgColorFormat( format );
    TUint32 sgUsage         = sctiConvertSgUsage( usage );
    TInt    pixelSize       = sctiColorFormatPixelSize( format );
    TInt    stride          = 0;
    
    if( sgPixelFormat < 0 || pixelSize < 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap unsupported format" );
#endif  /* defined( SCT_DEBUG ) */

        siWindowDestroyPixmap( context, symbianPixmap );
        return NULL;
    }

    if( data != NULL )
    {
        if( width * height * pixelSize != length )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap incompatible data" );
#endif  /* defined( SCT_DEBUG ) */
          
            siWindowDestroyPixmap( context, symbianPixmap );
            return NULL;
        }

        /* Data points to lower-left corner of pixels on the screen; so it is
         * upside down for SgImage. Define negative stride to fix this
         * issue. NOTE: SgImage::Create does the upside-down flipping internally
         * do you do not need to calculate new data address. */
        stride = -width * pixelSize;
    }
    
    RSgImage* sgImage = new RSgImage;
    symbianPixmap->pixmap = sgImage;
    
    if( sgImage == NULL || sgImage->Create( TSgImageInfo( sz, sgPixelFormat, sgUsage ), data, stride ) != KErrNone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap creating sgimage failed" );
#endif  /* defined( SCT_DEBUG ) */

        siWindowDestroyPixmap( context, symbianPixmap );
        return NULL;
    }
          
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap done" );
#endif  /* defined( SCT_DEBUG ) */

    return symbianPixmap;
}

/*!
 * 
 *
 */
SCTBoolean siWindowSetPixmapData( void* context, void* pixmap, void* data, int length )
{
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( pixmap != NULL );
    SCT_ASSERT_ALWAYS( data != NULL );
    SCT_ASSERT_ALWAYS( length > 0 );

    return SCT_FALSE;
}

/*!
 *
 *
 */
void* siWindowGetEglPixmap( void* context, void* pixmap, unsigned int* target )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetEglPixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCTSymbianPixmap*   symbianPixmap;
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( pixmap != NULL );

    symbianPixmap = ( SCTSymbianPixmap* )( pixmap );

    if( target != NULL )
    {
        *target = ( unsigned int )( EGL_NATIVE_PIXMAP_KHR );
    }
    
    return symbianPixmap->pixmap;
}

/*!
 *
 *
 */
SCTBoolean siWindowBlitPixmap( void* context, void* window, void* pixmap, int x, int y )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowBlitPixmap start" );
#endif  /* defined( SCT_DEBUG ) */         

    SCTSymbianPixmap*   symbianPixmap;
    SCTSymbianWindow*   symbianWindow;
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( window != NULL );
    SCT_ASSERT_ALWAYS( pixmap != NULL );   

    symbianPixmap = ( SCTSymbianPixmap* )( pixmap );
    symbianWindow = ( SCTSymbianWindow* )( window );

    if( symbianPixmap->type != SCT_SYMBIAN_PIXMAP_FBSBITMAP )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowBlitPixmap unsupported pixmap type" );
#endif  /* defined( SCT_DEBUG ) */         
        return SCT_FALSE;
    }
    
    if( symbianWindow->gc == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowBlitPixmap blitting not supported, worker thread?" );
#endif  /* defined( SCT_DEBUG ) */         
        return SCT_FALSE;
    }

	symbianWindow->window->Invalidate();
	symbianWindow->window->BeginRedraw();

    symbianWindow->gc->BitBlt( TPoint( x, y ),  ( CFbsBitmap* )( symbianPixmap->pixmap ) );
    
	symbianWindow->window->EndRedraw();
    symbianWindow->wsSession->Flush();
    
#if defined( SCT_SYMBIAN_FINISH_BLITBITMAP )
    symbianWindow->wsSession->Finish();
#endif  /* defined( SCT_SYMBIAN_FINISH_BLITBITMAP ) */
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowBlitPixmap done" );
#endif  /* defined( SCT_DEBUG ) */         
    
    return SCT_TRUE;
}

/*!
 *
 *
 */
void siWindowDestroyPixmap( void* context, void* pixmap )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyPixmap" );
#endif  /* defined( SCT_DEBUG ) */

    SCTSymbianPixmap*   symbianPixmap;
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( pixmap != NULL );

    symbianPixmap = ( SCTSymbianPixmap* )( pixmap );

    if( symbianPixmap->pixmap != NULL )
    {
        if( symbianPixmap->type == SCT_SYMBIAN_PIXMAP_FBSBITMAP )
        {
            delete ( CFbsBitmap* )( symbianPixmap->pixmap );
        }
        else if( symbianPixmap->type == SCT_SYMBIAN_PIXMAP_SGIMAGE )
        {
            ( ( RSgImage* )( symbianPixmap->pixmap ) )->Close();
            delete ( RSgImage* )( symbianPixmap->pixmap );
        }
        else
        {
            SCT_ASSERT_ALWAYS( 0 );           
        }

        symbianPixmap->pixmap = NULL;        
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static void sctiSetScreenOrientationL( SCTScreenOrientation orientation )
{
  
#if defined( SCT_S60_SUPPORT_SCREEN_ORIENTATION )    
    CAknAppUi* appUi = ( ( CAknAppUi* )( CEikonEnv::Static()->EikAppUi() ) );

    if( orientation == SCT_SCREEN_ORIENTATION_PORTRAIT )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetScreenOrientation TO-PORTRAIT" );
#endif  /* defined( SCT_DEBUG ) */      

        appUi->SetOrientationL( CAknViewAppUi::EAppUiOrientationPortrait );
    }
    else if( orientation == SCT_SCREEN_ORIENTATION_LANDSCAPE )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetScreenOrientation TO-LANDSCAPE" );
#endif  /* defined( SCT_DEBUG ) */      
        
        appUi->SetOrientationL( CAknViewAppUi::EAppUiOrientationLandscape );
    }

    CEikonEnv::Static()->WsSession().Finish();
#else  /* defined( SCT_S60_SUPPORT_SCREEN_ORIENTATION ) */
    SCT_USE_VARIABLE( orientation );
#endif  /* defined( SCT_S60_SUPPORT_SCREEN_ORIENTATION ) */   
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static TDisplayMode sctiConvertColorFormat( SCTColorFormat format )
{
    switch( format )
    {
    case SCT_COLOR_FORMAT_BW1:
        return EGray2;
        
    case SCT_COLOR_FORMAT_L8:
        return EGray256;
        
    case SCT_COLOR_FORMAT_RGB565:
        return EColor64K;
        
    case SCT_COLOR_FORMAT_RGB888:
        return EColor16M;
        
    case SCT_COLOR_FORMAT_RGBX8888:
        return EColor16MU;
        
    case SCT_COLOR_FORMAT_RGBA8888:
        return EColor16MA;
        
    case SCT_COLOR_FORMAT_RGBA8888_PRE:
        return EColor16MAP;
        
    case SCT_COLOR_FORMAT_DEFAULT:
        return CPlatform::ScreenMode();
        
    default:
        return ENone;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static TInt sctiConvertSgColorFormat( SCTColorFormat format )
{
    switch( format )
    {
    case SCT_COLOR_FORMAT_A8:
        return ESgPixelFormatA_8;
        
    case SCT_COLOR_FORMAT_RGB565:
        return ESgPixelFormatRGB_565;
        
    case SCT_COLOR_FORMAT_RGBX8888:
        return ESgPixelFormatXRGB_8888;
        
    case SCT_COLOR_FORMAT_RGBA8888:
        return ESgPixelFormatARGB_8888;
        
    case SCT_COLOR_FORMAT_RGBA8888_PRE:
        return ESgPixelFormatARGB_8888_PRE;
        
    default:
        return -1;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static TInt sctiColorFormatPixelSize( SCTColorFormat format )
{
    switch( format )
    {
    case SCT_COLOR_FORMAT_A8:           /* Intentional. */
    case SCT_COLOR_FORMAT_L8:
        return 1;

    case SCT_COLOR_FORMAT_RGBA5551:     /* Intentional. */
    case SCT_COLOR_FORMAT_RGBA4444:     /* Intentional. */
    case SCT_COLOR_FORMAT_RGB565:
        return 2;
        
    case SCT_COLOR_FORMAT_RGB888:
        return 3;
        
    case SCT_COLOR_FORMAT_RGBX8888:     /* Intentional. */
    case SCT_COLOR_FORMAT_RGBA8888:     /* Intentional. */
    case SCT_COLOR_FORMAT_RGBA8888_PRE:
        return 4;

    case SCT_COLOR_FORMAT_DEFAULT:      /* Intentional. */
    default:
        return -1;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static TUint32 sctiConvertSgUsage( int usage )
{
    TUint32 sgUsage = 0;

    if( ( usage & SCT_USAGE_OPENVG_IMAGE ) != 0 )
    {
        sgUsage |= ESgUsageBitOpenVgImage;
    }
    if( ( usage & SCT_USAGE_OPENGLES1_TEXTURE_2D ) != 0 )
    {
        sgUsage |= ESgUsageBitOpenGlesTexture2D;
    }
    if( ( usage & SCT_USAGE_OPENGLES2_TEXTURE_2D ) != 0 )
    {
        sgUsage |= ESgUsageBitOpenGles2Texture2D;
    }
    if( ( usage & SCT_USAGE_OPENGLES1_RENDERBUFFER ) != 0 )
    {
        /* Renderbuffer usage flag missing from the SgImage spec. Try using
         * texture usage flag instead. */
        sgUsage |= ESgUsageBitOpenGlesTexture2D;
    }
    if( ( usage & SCT_USAGE_OPENGLES2_RENDERBUFFER ) != 0 )
    {
        /* Renderbuffer usage flag missing from the SgImage spec. Try using
         * texture usage flag instead. */
        sgUsage |= ESgUsageBitOpenGles2Texture2D;
    }    
    if( ( usage & SCT_USAGE_OPENVG_SURFACE ) != 0 )
    {
        sgUsage |= ESgUsageBitOpenVgSurface;
    }
    if( ( usage & SCT_USAGE_OPENGLES1_SURFACE ) != 0 )
    {
        sgUsage |= ESgUsageBitOpenGlesSurface;
    }
    if( ( usage & SCT_USAGE_OPENGLES2_SURFACE ) != 0 )
    {
        sgUsage |= ESgUsageBitOpenGles2Surface;
    }
    if( ( usage & SCT_USAGE_OPENGL_SURFACE ) != 0 )
    {
        sgUsage |= ESgUsageBitOpenGlSurface;
    }

    return sgUsage;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static SCTBoolean sctiGetHDMIDisplaySize( void* context, int* width, int* height )
{
    CWsScreenDevice*            device;
    RWsSession*                 wsSession;
    MDisplayControl*            displayControl;
    TDisplayConfiguration       displayConfig;
    TSize                       displaySize;
    SCTSymbianSiWindowContext*  c;

    SCT_ASSERT( context != NULL );
    
    c = ( SCTSymbianSiWindowContext* )( context );
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctiGetHDMIDisplaySize start" );
#endif  /* defined( SCT_DEBUG ) */
    
    /* HACK: support HDMI only from the main thread. */
    if( c->mainThreadId != RThread().Id().Id() )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctiGetHDMIDisplaySize HDMI supported only from the main thread" );
#endif  /* defined( SCT_DEBUG ) */
        return SCT_FALSE;
    }

    wsSession = new RWsSession();
    if( ( wsSession == NULL ) || ( wsSession->Connect() != KErrNone ) )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctiGetHDMIDisplaySize failed to create ws session" );
#endif  /* defined( SCT_DEBUG ) */
        delete wsSession;
        return SCT_FALSE;
    }
    
    device = new CWsScreenDevice( *( wsSession ) );
    if( device == NULL || device->Construct( 1 ) != KErrNone )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: sctiGetHDMIDisplaySize creating HDMI screen device failed" );
#endif  /* defined( SCT_DEBUG ) */
        delete device;
        wsSession->Flush();
        wsSession->Close();
        delete wsSession;
        return SCT_FALSE;
    }
        
    device->SetAppScreenMode( device->CurrentScreenMode() );
    displayControl = ( MDisplayControl* )( device->GetInterface( MDisplayControl::ETypeId ) );
    displayControl->GetConfiguration( displayConfig );
    displayConfig.GetResolution( displaySize );

    delete device;
    wsSession->Flush();
    wsSession->Close();
    delete wsSession;
            
    if( displaySize.iWidth <= 0 || displaySize.iHeight <= 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowIsScreenSupported HDMI not connected" );
#endif  /* defined( SCT_DEBUG ) */
        return SCT_FALSE;            
    } 

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctiGetHDMIDisplaySize HDMI display size %dx%d", displaySize.iWidth, displaySize.iHeight );
#endif  /* defined( SCT_DEBUG ) */

    if( width != NULL )
    {
        *width = displaySize.iWidth;
    }
    
    if( height != NULL )
    {
        *height = displaySize.iHeight;
    }
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: sctiGetHDMIDisplaySize end" );
#endif  /* defined( SCT_DEBUG ) */
    
    return SCT_TRUE;
}
