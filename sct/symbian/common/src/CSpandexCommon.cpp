/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "CSpandexCommon.h"
#include "CPlatform.h"

#include "sct.h"
#include "sct_utils.h"
#include "sct_sicommon.h"

#if defined( INCLUDE_EGL_MODULE )
# include "sct_eglmemory.h"
#endif  /* defined( INCLUDE_EGL_MODULE ) */

#include <w32std.h>
#include <sys\reent.h>
#include <hal.h>
#include <string.h>

/*!
 * Structure to map machine UID into phone model, OS, and memory capacity.
 */
typedef struct
{
    int         machineUID;     
    const char* name;
    const char* os;
} MachineId;

/*!
 * Structure to map uid into name/string.
 */
typedef struct
{
    int         uid;
    const char* name;
} UidName;

/* These implement an obsolete idea. */
#define NOKIA_7650_HW_ID                0x101f4fc3
#define NOKIA_3650_HW_ID                0x101F466A
#define NOKIA_NGAGE_HW_ID               0x101f8c19
#define NOKIA_NGAGE_QD_HW_ID            0x101FB2B1
#define NOKIA_6600_HW_ID                0x101FB3DD
#define NOKIA_6620_HW_ID                0x101F3EE3
#define NOKIA_6630_HW_ID                0x101FBB55
#define NOKIA_6680_HW_ID                0x10200F99
#define NOKIA_7610_HW_ID                0x101fb3f3
#define NOKIA_9210_HW_ID                0x10005e33
#define NOKIA_9500_HW_ID                0x101F8DDB
#define NOKIA_7710_HW_ID                0x101FBE09
#define SENDO_X_HW_ID                   0x10005F60
#define SIEMENS_SX1_HW_ID               0x101F9071
#define SONYERICSSON_P910_HW_ID         0x10200AC6

/* Used to identify the used platform. OBSOLETE. */
static const MachineId machineIds[] =
{ 
    { NOKIA_7650_HW_ID,         "Nokia 7650",           "Symbian 6.1" },
    { NOKIA_3650_HW_ID,         "Nokia 3650",           "Symbian 6.1" },
    { NOKIA_NGAGE_HW_ID,        "Nokia N-Gage",         "Symbian 6.1" },
    { NOKIA_NGAGE_QD_HW_ID,     "Nokia N-Gage QD",      "Symbian 6.1" },
    { NOKIA_6600_HW_ID,         "Nokia 6600",           "Symbian 7.0s" },
    { NOKIA_6620_HW_ID,         "Nokia 6620",           "Symbian 7.0s" },
    { NOKIA_6630_HW_ID,         "Nokia 6630",           "Symbian 8.0a" },
    { NOKIA_6680_HW_ID,         "Nokia 6680",           "Symbian 8.0a" },
    { NOKIA_7610_HW_ID,         "Nokia 7610",           "Symbian 7.0s" },
    { NOKIA_9210_HW_ID,         "Nokia 9210",           "Symbian 6.0" },
    { NOKIA_9500_HW_ID,         "Nokia 9500",           "Symbian 7.0s" },
    { NOKIA_7710_HW_ID,         "Nokia 7710",           "Symbian 7.0s" },
    { SENDO_X_HW_ID,            "Sendo-X",              "Symbian 6.1" },
    { SIEMENS_SX1_HW_ID,        "Siemens SX1",          "Symbian 6.1" },
    { SONYERICSSON_P910_HW_ID,  "Sony-Ericsson P910",   "Symbian 7.0" }
};

/* Used to identify the used CPU */
static const UidName cpus[] =
{
    { 0,    "ARM" },
	{ 1,    "MCORE" },
    { 2,    "X86" } 
};

/* Used to identify the used CPU ABI */
static const UidName abis[] =
{
	{ 0, "ARM4" },
    { 1, "ARMI" },
    { 2, "THUMB" },
    { 3, "MCORE" },
    { 4, "MSVC" },
    { 5, "ARM5T" },
    { 6, "X86" }
};

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexCommon::ConstructL()
{
    CPlatform::SetSpandexCommon( this );

    /* First, we open input and output files. Output file is important as
       it will be used also for error reporting */
    iInputFile  = fopen( CPlatform::InputFileName(), "rb" );
    iOutputFile = fopen( CPlatform::OutputFileName(), "wb" );

    if( iInputFile == NULL || iOutputFile == NULL )
    {
        CPlatform::DisplayError( "Spandex", "Cannot open input/output files" );
        User::Leave( KErrNotFound );
    }

    /* Query device info (machine uid, cpu, etc.) */
    iDeviceInfo = QueryDeviceInfo();

#if defined( INCLUDE_EGL_MODULE )    
    iEglMemory = sctCreateEglMemoryContext();
#else   /* defined( INCLUDE_EGL_MODULE ) */
    iEglMemory = NULL;    
#endif  /* defined( INCLUDE_EGL_MODULE ) */    
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
CSpandexCommon::~CSpandexCommon()
{

#if defined( INCLUDE_EGL_MODULE )
    sctDestroyEglMemoryContext( iEglMemory );
    iEglMemory = NULL;
#endif  /* defined( INCLUDE_EGL_MODULE ) */
    
    if( iSCTContext != NULL )
    {
        sctTerminate( iSCTContext );
    }

    CPlatform::SetSpandexCommon( NULL );

    if( iInputFile != NULL )
    {
        fclose( iInputFile );
    }

    if( iOutputFile != NULL )
    {
        fclose( iOutputFile );
    }
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
int CSpandexCommon::Cycle()
{
    /* First cycle does the init */
    if( iSCTContext == NULL )
    {
        iSCTContext = sctInitialize();
        if( iSCTContext == NULL )
        {
            CPlatform::DisplayError( "Spandex init error", "See output file for details" );
            return 0;
        }
        else
        {
            return 1;
        }
    }

    SCT_ASSERT_ALWAYS( iSCTContext != NULL );

    int status;
    status = sctCycle( iSCTContext );
    
    if( status == SCT_CYCLE_FATAL_ERROR )
    {
        CPlatform::DisplayError( "Spandex cycle fatal error", "See output file for details" );
        return 0;
    }
    else if( status == SCT_CYCLE_FINISHED )
    {
        CPlatform::DisplayMessage( "Spandex", "Done!" );
        return 0;
    }
    return 1;
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
TSctDeviceInfo CSpandexCommon::QueryDeviceInfo()
{
	TInt            deviceInt;
    TSctDeviceInfo  deviceInfo;
    unsigned int    i;

    /* Resolve machine uid */
	HAL::Get( HAL::EMachineUid, deviceInt );

    sprintf( deviceInfo.deviceName, "%X", deviceInt );
    strcpy( deviceInfo.OS, "<Unknown>" );

    for( i = 0; i < SCT_ARRAY_LENGTH( machineIds ); ++i )
    {
        if( machineIds[ i ].machineUID == deviceInt )
        {
            strcpy( deviceInfo.deviceName, machineIds[ i ].name );
            strcpy( deviceInfo.OS, machineIds[ i ].os );
        }
    }

    /* Resolve CPU. */
	TInt        cpuInt;
	const char* cpuStr = "<Unknown>";

	HAL::Get( HALData::ECPU, cpuInt );

    for( i = 0; i < SCT_ARRAY_LENGTH( cpus ); ++i )
    {
        if( cpus[ i ].uid == cpuInt )
        {
            cpuStr = cpus[ i ].name;
        }
    }

	/* Resolve CPU ABI. */
	TInt        cpuAbiInt;
	const char* cpuAbiStr = "<Unknown>";

	HAL::Get( HALData::ECPUABI, cpuAbiInt );

    for( i = 0; i < SCT_ARRAY_LENGTH( abis ); ++i )
    {
        if( abis[ i ].uid == cpuAbiInt )
        {
            cpuAbiStr = abis[ i ].name;
        }
    }

	/* Resolve CPU speed. */
	TInt cpuSpeed = 0;
	HAL::Get( HALData::ECPUSpeed, cpuSpeed );

    /* Format CPU info */
	sprintf( deviceInfo.cpu, "%s ABI %s %dMHz", cpuStr, cpuAbiStr, ( cpuSpeed / 1000 ) );

    return deviceInfo;
}
