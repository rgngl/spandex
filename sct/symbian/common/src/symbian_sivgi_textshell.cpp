/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Symbian includes */
#include <e32std.h>

/* Stdlib includes*/
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <hal.h>

/* Includes from Spandex core */
#include "sct_sicommon.h"
#include "sct_sivgi.h"

#include "CPlatform.h"

#define SYMBIAN_SIVGI_TEXTSHELL_CONTEXT         0xBABE

/*!
 *
 *
 */
void* siVgiGetContext()
{
    return ( void* )( SYMBIAN_SIVGI_TEXTSHELL_CONTEXT );
}

/*!
 *
 *
 */
void siVgiReleaseContext( void* context )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SYMBIAN_SIVGI_TEXTSHELL_CONTEXT ) );
}

/*!
 *
 *
 */
SCTVgiTarget* siVgiGetTarget( void* context, int width, int height, VGIColorBufferFormat targetFormat, SCTBoolean targetMask )
{
    SCTVgiTarget*   target;
    int             pixelSize;
    SCTBoolean      needMask    = targetMask;

    SCT_ASSERT_ALWAYS( context == ( void* )( SYMBIAN_SIVGI_TEXTSHELL_CONTEXT ) );

    target = ( SCTVgiTarget* )( siCommonMemoryAlloc( NULL, sizeof( SCTVgiTarget ) ) );
    if( target == NULL )
    {
        return NULL;
    }
    memset( target, 0, sizeof( SCTVgiTarget ) );

    switch( targetFormat )
    {
    case VGI_COLOR_BUFFER_FORMAT_RGB565:
        pixelSize = 2;
        break;

    case VGI_COLOR_BUFFER_FORMAT_RGB888:
        pixelSize = 3;
        break;

    case VGI_COLOR_BUFFER_FORMAT_XRGB8888:
        pixelSize = 4;
        break;

    case VGI_COLOR_BUFFER_FORMAT_ARGB8888:
    default:
        pixelSize = 4;
        needMask  = SCT_FALSE;
        break;
    }

    target->format = targetFormat;
    target->width  = width;
    target->height = height;

    target->bitmapData = ( unsigned char* )( siCommonMemoryAlloc( NULL, width * height * pixelSize ) );
    if( target->bitmapData == NULL )
    {
        siVgiReleaseTarget( context, target );
        return NULL;
    }

    target->bitmapStride = width * pixelSize;

    if( needMask == SCT_TRUE )
    {
        target->maskData = ( unsigned char* )( siCommonMemoryAlloc( NULL, width * height ) );
        if( target->maskData == NULL )
        {        
            siVgiReleaseTarget( context, target );
            return NULL;
        }
 
        target->maskStride = width;
    }

    return target;
}

/*!
 *
 *
 */
SCTBoolean siVgiLockTarget( void* context, SCTVgiTarget* target )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SYMBIAN_SIVGI_TEXTSHELL_CONTEXT ) );
    SCT_ASSERT_ALWAYS( target != NULL );

    return SCT_TRUE;
}

/*!
 *
 *
 */
void siVgiUnlockTarget( void* context, SCTVgiTarget* target )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SYMBIAN_SIVGI_TEXTSHELL_CONTEXT ) );
    SCT_ASSERT_ALWAYS( target != NULL );
}

/*!
 *
 *
 */
void siVgiReleaseTarget( void* context, SCTVgiTarget* target )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SYMBIAN_SIVGI_TEXTSHELL_CONTEXT ) );
    SCT_ASSERT_ALWAYS( target != NULL );

    if( target->bitmapData != NULL )
    {
        siCommonMemoryFree( NULL, target->bitmapData );
        target->bitmapData = NULL;
    }

    if( target->maskData != NULL )
    {
        siCommonMemoryFree( NULL, target->maskData );
        target->maskData = NULL;
    }

    siCommonMemoryFree( NULL, target );
}

/*!
 *
 *
 */
SCTBoolean siVgiBlitTarget( void* context, SCTVgiTarget* target )
{
    SCT_ASSERT_ALWAYS( context == ( void* )( SYMBIAN_SIVGI_TEXTSHELL_CONTEXT ) );
    SCT_ASSERT_ALWAYS( target != NULL );

    return SCT_TRUE;
}
