/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "CSpandexContainer.h"
#include "CSpandexCommon.h"
#include "CSpandexAppUi.h"
#include "CPlatform.h"

#include "sct_utils.h"
#include "sct_sicommon.h"

#include <eikenv.h>
#include <stdlib.h>

static void OutputText( CWindowGc &aGc, const TDesC &aString, const TPoint &aPosition, const TRgb &aColor, const CFont *aFont );
static void OutputCenteredText( CWindowGc &aGc, const TDesC &aString, const TSize &aScreenSize, const TRgb &aColor, const CFont *aFont );
static void OutputCenteredText( CWindowGc &aGc, const TDesC &aString, const TSize &aScreenSize, const TRgb &aColor, const CFont *aFont );

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::ConstructL( const TRect& aRect )
{
    CPlatform::SetContainer( this );
    
    CreateWindowL();
    SetRect( aRect );
    ActivateL();
    
    iWindowRect = aRect;

    iFont = CEikonEnv::Static()->SymbolFont();

	iTimer = new ( ELeave ) CSpandexTimer( *this );
	iTimer->ConstructL();

    iAppState   = EState_Idle;
    iDrawStatus = 1;
    
    RProcess process;
    process.SetPriority( EPriorityHigh );
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
CSpandexContainer::~CSpandexContainer()
{
    CPlatform::SetContainer( NULL );
	delete iSpandexCommon;
    delete iTimer;
    
    CloseSTDLIB();
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::SizeChanged()
{
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
TInt CSpandexContainer::CountComponentControls() const
{
   return 0;
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
CCoeControl* CSpandexContainer::ComponentControl(TInt /*aIndex*/) const
{
	return NULL;
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::HandleControlEventL( CCoeControl* /*aControl*/,TCoeEvent /*aEventType*/)
{
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::ResetBackgroundLight()
{
    User::ResetInactivityTime();
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::Start()
{
    iAppState = EState_Start;
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::Stop()
{
    iAppState = EState_Stop;
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::GoRunStateL()
{
    SCT_ASSERT_ALWAYS( iSpandexCommon == NULL );

    iCurrentBenchmark = iCurrentRepeat = iTotalBenchmark = 0;
    SetExtentToWholeScreen();

	iSpandexCommon = new( ELeave ) CSpandexCommon();
	iSpandexCommon->ConstructL();

    ResetBackgroundLight();        
    iAppState = EState_Run;

	DrawNow();
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::GoIdleState()
{
    delete iSpandexCommon;
    iSpandexCommon = NULL;

	SetRect( iWindowRect );
    iAppState = EState_Idle;

    DrawNow();
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::OnRunState()
{
    SCT_ASSERT_ALWAYS( iSpandexCommon != NULL );

    iDrawStatus = 0;
    DrawNow();
    
    ResetBackgroundLight();
    int status  = iSpandexCommon->Cycle();
    ResetBackgroundLight();

    iDrawStatus = 1;
    
    if( status == 0 )
    {
        GoIdleState();
    }
    else
    {
        DrawNow();
    }
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::OnTimerEvent()
{
    switch( iAppState )
    {
	case EState_Start:
        TRAPD( error, GoRunStateL() );
        if( error != KErrNone )
        {
            GoIdleState();
        }
        break;

    case EState_Run:
        OnRunState();
        break;

    case EState_Stop:
    case EState_Idle:
    default:
        GoIdleState();
        break;
    }
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::ReportProgress( TInt aTotal, TInt aCurrent, TInt aRepeat, const char * /*aName*/ )
{
    iTotalBenchmark   = aTotal;
    iCurrentBenchmark = aCurrent;
    iCurrentRepeat    = aRepeat;
    DrawNow();
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void CSpandexContainer::Draw( const TRect& aRect ) const
{
    CWindowGc &aGc = SystemGc();

    if( iDrawStatus != 0 )
    {
    	aGc.SetBrushColor( KRgbBlack ); 
        aGc.SetBrushStyle( CGraphicsContext::ESolidBrush );
        aGc.DrawRect(aRect );
        
        if( iAppState == EState_Start )
        {
            OutputCenteredText( aGc, _L( "Starting..." ), aRect.Size(), KRgbWhite, iFont );
        }
        else if( iAppState == EState_Run )
        {
            TBuf<50> progressStr;
            
            if( iTotalBenchmark == 0 )
            {
                progressStr.Format( _L( "Preparing to run..." ) );	
            }
            else
            {
                progressStr.Format( _L( "Running ( %d/%d, %d ) " ),
                                    iCurrentBenchmark, 
                                    iTotalBenchmark,
                                    iCurrentRepeat );	
            }
            
            OutputCenteredText( aGc, progressStr, aRect.Size(), KRgbWhite, iFont );
        }
        else if( iAppState == EState_Idle )
        {
            OutputCenteredText( aGc, _L( "Ready" ), aRect.Size(), KRgbWhite, iFont );
        }
    }
    else
    {
    	aGc.SetBrushColor( TRgb( 0xFF000000 ) ); 
        aGc.SetBrushStyle( CGraphicsContext::ESolidBrush );
        aGc.Clear();
    }

    ControlEnv()->Flush();
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void OutputText( CWindowGc &aGc, 
                 const TDesC &aString, 
                 const TPoint &aPosition, 
                 const TRgb &aColor, 
                 const CFont *aFont )
{
	aGc.SetPenColor( aColor );
	aGc.UseFont( aFont );
	aGc.DrawText( aString, aPosition );
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void OutputCenteredText( CWindowGc &aGc, 
                         const TDesC &aString, 
                         const TSize &aScreenSize, 
                         const TRgb &aColor, 
                         const CFont *aFont )
{
	int textWidth  = aFont->TextWidthInPixels( aString );
	int textHeight = aFont->HeightInPixels();
	int x = ( aScreenSize.iWidth  - textWidth ) / 2;
	int y = ( aScreenSize.iHeight - textHeight ) / 2;

	OutputText( aGc, aString, TPoint( x, y ), aColor, aFont );
}

/* ----------------------------------------------------------------------*/
/*!
 *
 *
 */
void OutputCenteredText( CWindowGc &aGc, 
                         const TDesC &aString, 
                         const TSize &aScreenSize, 
                         const TRgb &aColor )
{
	OutputCenteredText( aGc, aString, aScreenSize, aColor, CEikonEnv::Static()->TitleFont() );
}
