/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Symbian includes */
#include <e32std.h>
#include <w32std.h>

/* Stdlib includes*/
#include <math.h>
#include <string.h>
#include <hal.h>

/* Includes from PeccoG core */
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_simemory.h"


/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
int siMemoryBlockFillZ( void **aBuffers, 
                        const int aBufferCount, 
                        const int aLengthInBytes, 
                        int aIterations )
{
    SCT_ASSERT( aBuffers != NULL );
    SCT_ASSERT( aBufferCount > 0 );
    SCT_ASSERT( aLengthInBytes > 0 );
    SCT_ASSERT( aIterations > 0 );

	do
	{
        for( int i = 0; i < aBufferCount; i++ )
        {
            Mem::FillZ( aBuffers[ i ], aLengthInBytes );
        }
	} while( --aIterations );

	return 1;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
int siMemoryBlockCopy( void **aDestinations, 
                       void **aSources, 
                       const int aBufferCount, 
                       const int aLengthInBytes, 
                       int aIterations )
{
    SCT_ASSERT( aDestinations != NULL );
    SCT_ASSERT( aSources != NULL );
    SCT_ASSERT( aBufferCount > 0 );
    SCT_ASSERT( aLengthInBytes > 0 );
    SCT_ASSERT( aIterations > 0 );

	do
	{
        for( int i = 0; i < aBufferCount; i++ )
        {
            Mem::Copy( aDestinations[ i ], aSources[ i ], aLengthInBytes );
        }
	} while( --aIterations );

	return 1;
}

