/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __CSPANDEXCONTAINER_H__ )
#define __CSPANDEXCONTAINER_H__

#include "CSpandexTimer.h"

#include <coecntrl.h>
#include <coemain.h>

class CSpandexCommon;

/*!
 *
 *
 */
class CSpandexContainer : public CCoeControl, public MCoeControlObserver, public CSpandexTimer::MCallback
{
public:

    enum TAppState
    {
		EState_Idle,
		EState_Start,
		EState_Run,
        EState_Stop
    };
        
    void            ConstructL( const TRect& aRect );
    virtual         ~CSpandexContainer();
    void            OnTimerEvent();
    int             IsReadyToRun() { return iAppState == EState_Idle ? 1 : 0; }
    void            Start();
    void            Stop();
    void            GoRunStateL();
    void            GoIdleState();
    void            OnRunState();
    void            ReportProgress( TInt aTotal, TInt aCurrent, TInt aRepeat, const char* aName );
    void            ResetBackgroundLight();
    
private:

    void            SizeChanged();
    TInt            CountComponentControls() const;
    CCoeControl*    ComponentControl(TInt aIndex) const;
    void            Draw( const TRect& aRect ) const;
    void            HandleControlEventL( CCoeControl* aControl, TCoeEvent aEventType );

private:

	CSpandexCommon* iSpandexCommon;
    CSpandexTimer*  iTimer;
    TAppState       iAppState;
    const CFont*    iFont;
    TRect           iWindowRect;   
    TInt            iCurrentBenchmark;
    TInt            iCurrentRepeat;
    TInt            iTotalBenchmark;
    TInt            iDrawStatus;
};

#endif /* !defined( __CSPANDEXCONTAINER_H__ ) */
