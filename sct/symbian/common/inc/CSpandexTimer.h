/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __CSPANDEXTIMER_H__ )
#define __CSPANDEXTIMER_H__

#include <e32base.h>

class CSpandexTimer : public CTimer
{
private:

	enum { Interval = 1000000 }; // Interval (in microseconds ) between timer events 

private:

    virtual void RunL();

public:

	/*! Timer callback interface */
    class MCallback
    {
    public:
        virtual void OnTimerEvent() = 0;
    };

                        CSpandexTimer( MCallback &aCallback );
	virtual             ~CSpandexTimer();

    void                ConstructL();

private:

    MCallback           &iCallback; // Timer callback
};

#endif /* !defined( __CSPANDEXTIMER_H__ ) */
