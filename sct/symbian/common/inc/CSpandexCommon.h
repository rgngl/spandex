/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __CSPANDEXCOMMON_H__ )
#define __CSPANDEXCOMMON_H__

#include <e32base.h>
#include <stdio.h>
#include <w32std.h>

/* ---------------------------------------------------------------------- */
enum 
{
    EBufferSize         = 64,
    EDeviceNameSize     = 128,
    EOsNameSize         = 128,
    ECpuNameSize        = 64,
    EOutputBufferSize   = 32766,
};				

/* ---------------------------------------------------------------------- */
/*! 
 * Device information 
 *
 */
typedef struct
{
	char                deviceName[ EDeviceNameSize ];
	char                OS[ EOsNameSize ];
    char                cpu[ ECpuNameSize ];
} TSctDeviceInfo;

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
class CSpandexCommon : public CBase
{
public:

    void                    ConstructL();
    virtual                 ~CSpandexCommon();

    int                     Cycle();
	TSctDeviceInfo&         DeviceInfo()            { return iDeviceInfo; }	
    FILE*                   InputFile()             { return iInputFile;}
    FILE*                   OutputFile()            { return iOutputFile; }
    void*                   EglMemory()             { return iEglMemory; }
    void*                   GetSingleton()          { return iSingleton; }
    void                    SetSingleton( void* singleton ) { iSingleton = singleton; }
    char                    outputBuffer[ EOutputBufferSize + 1 ];
    int                     outputSize;

protected:

    static TSctDeviceInfo   QueryDeviceInfo();

private:

    TSctDeviceInfo          iDeviceInfo;
    FILE*                   iInputFile;
	FILE*                   iOutputFile;
    void*                   iSCTContext;
    void*                   iSingleton;
    void*                   iEglMemory;
};

#endif /* !defined( __CSPANDEXCOMMON_H__ ) */
