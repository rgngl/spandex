/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#if !defined( __CPLATFORM_H__ )
#define __CPLATFORM_H__

#include <e32base.h>
#include <gdi.h>

#include "sct_siwindow.h"

class CSpandexCommon;
class RWindow;
class RWsSession;
class CFbsBitmap;
class CWindowGc;

/*!
 *
 *
 */
class CPlatform : public CBase
{
public:

    static void             SetSpandexCommon( CSpandexCommon* aSpandexCommon );
    static CSpandexCommon*  SpandexCommon();
    static void             SetContainer( void* aContainer );   
    static SCTLog*          GetLog();
    static void             SetLog( SCTLog* log );
    static void             ScreenSize( TInt& aWidth, TInt& aHeight );
    static TDisplayMode     ScreenMode();
    static int              ScreenColorBits();
    static void             DisplayError( const char* aTitle, const char* aMessage );
    static void             DisplayMessage( const char* aTitle, const char* aMessage );
	static void             ReportProgress( TInt aTotal, TInt aCurrent, TInt aRepeat, const char* aName );
    static const char*      InputFileName();
    static const char*      OutputFileName();
};

#endif  /* !defined( __CPLATFORM_H__ ) */

