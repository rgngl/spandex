REM 
REM  Spandex benchmark and test framework.
REM 
REM  Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
REM 
REM  Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
REM 
REM    This framework is free software; you can redistribute it and/or modify it
REM  under the terms of the GNU Lesser General Public License as published by the
REM  Free Software Foundation, version 2.1 of the License.
REM 
REM    This framework is distributed in the hope that it will be useful, but
REM  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
REM  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
REM  for more details.
REM 
REM    You should have received a copy of the GNU Lesser General Public License
REM  along with this framework; if not, see <http://www.gnu.org/licenses/>.
REM 

REM Press any key to start.
getkey.exe

REM ----- CLEANUP ------

del f:\MobileCrash*.*
del f:\vgout\*.*
rd f:\vgout
del f:\gles1out\*.*
rd f:\gles1out
del f:\gles2out\*.*
rd f:\gles2out
del f:\vg11_vt_result\*.*
rd f:\vg11_vt_result
del f:\gl11_vt_result\*.*
rd f:\gl11_vt_result
del f:\gl20_vt_result\*.*
rd f:\gl20_vt_result
del f:\vg11_perf_result\*.*
rd f:\vg11_perf_result
del f:\gl11_perf_result\*.*
rd f:\gl11_perf_result
del f:\gl20_perf_result\*.*
rd f:\gl20_perf_result
del f:\vg11_gl11_perf_result\*.*
rd f:\vg11_gl11_perf_result
del f:\vg11_gl20_perf_result\*.*
rd f:\vg11_gl20_perf_result
del f:\gl11_ui_result\*.*
rd f:\gl11_ui_result
del f:\gl20_ui_result\*.*
rd f:\gl20_ui_result
del f:\vg11_ui_result\*.*
rd f:\vg11_ui_result
del f:\gl20_ui2_result\*.*
rd f:\gl20_ui2_result
rd f:\running

md f:\running

REM ----- Spandex visual test -----

REM ----- VG11 visual test -----
del f:\benchmark.txt
copy f:\vg11_vt_benchmark.txt f:\benchmark.txt
spandex_opengles2_openvg_mingui.exe
md f:\vg11_vt_result
move f:\result.txt f:\vg11_vt_result\vg11_vt_result.txt
move f:\MobileCrash*.* f:\vg11_vt_result\

REM ----- GL11 visual test -----
del f:\benchmark.txt
copy f:\gl11_vt_benchmark.txt f:\benchmark.txt
spandex_opengles1_openvg_mingui.exe
md f:\gl11_vt_result
move f:\result.txt f:\gl11_vt_result\gl11_vt_result.txt
move f:\MobileCrash*.* f:\gl11_vt_result\

REM ----- GL20 visual test -----
del f:\benchmark.txt
copy f:\gl20_vt_benchmark.txt f:\benchmark.txt
spandex_opengles2_openvg_mingui.exe
md f:\gl20_vt_result
copy f:\result.txt f:\gl20_vt_result\gl20_vt_result.txt
move f:\MobileCrash*.* f:\gl20_vt_result\

REM ----- Spandex performance test -----

REM ----- OpenVG performance test -----
del f:\benchmark.txt
copy f:\vg11_perf_benchmark.txt f:\benchmark.txt
spandex_opengles2_openvg_mingui.exe
md f:\vg11_perf_result
copy f:\result.txt f:\vg11_perf_result\vg11_perf_result.txt
move f:\MobileCrash*.* f:\vg11_perf_result\

REM ----- GL11 performance test -----
del f:\benchmark.txt
copy f:\gl11_perf_benchmark.txt f:\benchmark.txt
spandex_opengles1_openvg_mingui.exe
md f:\gl11_perf_result
copy f:\result.txt f:\gl11_perf_result\gl11_perf_result.txt
move f:\MobileCrash*.* f:\gl11_perf_result\

REM ----- GL20 performance test -----
del f:\benchmark.txt
copy f:\gl20_perf_benchmark.txt f:\benchmark.txt
spandex_opengles2_openvg_mingui.exe
md f:\gl20_perf_result
copy f:\result.txt f:\gl20_perf_result\gl20_perf_result.txt
move f:\MobileCrash*.* f:\gl20_perf_result\

REM ----- VG11 + GL11 performance test -----
del f:\benchmark.txt
copy f:\vg11_gl11_perf_benchmark.txt f:\benchmark.txt
spandex_opengles1_openvg_mingui.exe
md f:\vg11_gl11_perf_result
copy f:\result.txt f:\vg11_gl11_perf_result\vg11_gl11_perf_result.txt
move f:\MobileCrash*.* f:\vg11_gl11_perf_result\

REM ----- VG11 + GL20 performance test -----
del f:\benchmark.txt
copy f:\vg11_gl20_perf_benchmark.txt f:\benchmark.txt
spandex_opengles2_openvg_mingui.exe
md f:\vg11_gl20_perf_result
copy f:\result.txt f:\vg11_gl20_perf_result\vg11_gl20_perf_result.txt
move f:\MobileCrash*.* f:\vg11_gl20_perf_result\

REM ----- GL11 ui performance test -----
del f:\benchmark.txt
copy f:\gl11_ui_benchmark.txt f:\benchmark.txt
spandex_opengles1_openvg_mingui.exe
md f:\gl11_ui_result
copy f:\result.txt f:\gl11_ui_result\gl11_ui_result.txt
move f:\MobileCrash*.* f:\gl11_ui_result\

REM ----- GL20 ui performance test -----
del f:\benchmark.txt
copy f:\gl20_ui_benchmark.txt f:\benchmark.txt
spandex_opengles2_openvg_mingui.exe
md f:\gl20_ui_result
copy f:\result.txt f:\gl20_ui_result\gl20_ui_result.txt
move f:\MobileCrash*.* f:\gl20_ui_result\

REM ----- VG11 ui performance test -----
del f:\benchmark.txt
copy f:\vg11_ui_benchmark.txt f:\benchmark.txt
spandex_opengles2_openvg_mingui.exe
md f:\vg11_ui_result
copy f:\result.txt f:\vg11_ui_result\vg11_ui_result.txt
move f:\MobileCrash*.* f:\vg11_ui_result\

REM ----- GL20 ui2 performance test -----
del f:\benchmark.txt
copy f:\gl20_ui2_benchmark.txt f:\benchmark.txt
spandex_opengles2_openvg_mingui.exe
md f:\gl20_ui2_result
copy f:\result.txt f:\gl20_ui2_result\gl20_ui2_result.txt
move f:\MobileCrash*.* f:\gl20_ui2_result\

rd f:\running
REM Release test suite finished.