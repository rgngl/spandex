#!python

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys, os

if len( sys.argv ) < 5:
    print 'Invalid number of command line parameters'
    print 'Usage: %s [INPUT] [OUTPUT] [APPNAME] [APPUID] {RELEASE}' % sys.argv[ 0 ]
    sys.exit( 1 )


# Get input file
try:
    input = open( sys.argv[ 1 ], 'r' ).read()
except:
    sys.exit( 'Invalid input file: ' + str( sys.argv[ 1 ] ) )

# Get output file
try:
    output = open( sys.argv[ 2 ], 'w' )
except:
    sys.exit( 'Invalid output file: ' + str( sys.argv[ 2 ] ) )

# Get app name
app_name = sys.argv[ 3 ]
if not app_name:
    sys.exit( 'Invalid application name' )

# Get app uid
app_uid = sys.argv[ 4 ]
if not app_uid:
    sys.exit( 'Invalid application uid' )

release = None
if len( sys.argv ) > 5:
    release = sys.argv[ 5 ]

input = input.replace( '$APP_NAME$', app_name )
input = input.replace( '$APP_UID$',  app_uid )

if release:
    input = input.replace( '$RELEASE$', release )

output.write( input )
output.close()

