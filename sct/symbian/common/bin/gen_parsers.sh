#!/bin/bash

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/cpu/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/memory/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/opengles1/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/opengles2/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/openvg/config.py
#python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/pvr/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/vgi/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/vgu/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/vgtest/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/gltest/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/egl/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/images/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/thread/config.py
python $SPANDEX_HOME/python/mkactionparser.py $SPANDEX_HOME/sct/common/modules/test/config.py

python $SPANDEX_HOME/python/mkactionparser.py --cpp $SPANDEX_HOME/sct/symbian/modules/SymbianBitGDI/config.py 
python $SPANDEX_HOME/python/mkactionparser.py --cpp $SPANDEX_HOME/sct/symbian/modules/SymbianSVG/config.py
