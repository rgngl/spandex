#!/bin/sh

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

#echo START
#set -x

COMMAND=$1
MAKEFILE_FILE=$2
SERIES_ID=$3
SERIES_VERSION=$4
SDK_PATH=$5
TOOLS_PATH=$6
APP_NAME=$7
APP_UID=$8
GROUP_DIR=$9

SDK_CYGPATH=$(cygpath $SDK_PATH)
TOOLS_CYGPATH=$(cygpath $TOOLS_PATH)

if [ ! -d $SDK_CYGPATH ]
    then
    echo "$SERIES_ID SDK $SERIES_VERSION not installed in $SDK_CYGPATH, skipped."
    exit 0
fi

if [ ! -d $TOOLS_CYGPATH ]
    then
    echo Invalid SDK tools root path $TOOLS_PATH.
    exit 1
fi

if [ ! -d $TOOLS_CYGPATH/epoc32/gcc/bin ]
    then
    echo Invalid gcc path $TOOLS_CYGPATH/epoc32/gcc/bin
    exit 1
fi

if [ ! -d $TOOLS_CYGPATH/epoc32/tools ]
    then
    echo Invalid SDK tools path $TOOLS_CYGPATH/epoc32/tools
    exit 1
fi

PATH=$TOOLS_CYGPATH/epoc32/gcc/bin:$TOOLS_CYGPATH/epoc32/tools:"$PATH"
export PATH="$PATH"

EPOCROOT=$(cygpath -d $SDK_CYGPATH)
EPOCROOT=\\${EPOCROOT#?:\\}

if [ $EPOCROOT != \\ ]
    then
    EPOCROOT=${EPOCROOT}\\
fi

export MAKEFILE_FILE="$MAKEFILE_FILE"
export EPOCROOT="$EPOCROOT"
export CYG_EPOCROOT=$SDK_CYGPATH/
export SERIES_ID="$SERIES_ID"
export SERIES_VERSION="$SERIES_VERSION"
export APP_NAME="$APP_NAME"
export APP_UID="$APP_UID"
export GROUP_DIR="$GROUP_DIR"

OUTPUT=log_${COMMAND}_${SERIES_ID}_${SERIES_VERSION}.txt

rm -f $OUTPUT
if make -f $MAKEFILE_FILE "$1" > $OUTPUT 2>&1
    then
    echo $SERIES_ID $SERIES_VERSION ok.
    rm -f $OUTPUT
else
    echo 
    echo $SERIES_ID $SERIES_VERSION FAILED!
    echo 
    echo Output:
    echo ====================
    cat $OUTPUT
    echo ====================
fi
