#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys
import optparse
import os

if __name__ == '__main__':

    usage = "usage: %prog [options] <gl2ctrace module folder> <group folder> "
    parser = optparse.OptionParser( usage )
    ( options, args ) = parser.parse_args()

    if len( args ) != 2:
        parser.error( 'incorrect number of arguments' )
        sys.exit( -1 )

    gl2ctraceModuleFolder   = args[ 0 ]
    groupFolder             = args[ 1 ]
    
    if gl2ctraceModuleFolder[ -1: 0 ] != '/':
        gl2ctraceModuleFolder += '/'

    if groupFolder[ -1: 0 ] != '/':
        groupFolder += '/'

    mmptemplate = open( groupFolder + 'Spandex.mmp.template.template' ).read()

    cFiles = filter( lambda x: x.find( 'sct_gl2ctraceAnimated' ) != -1, os.listdir( gl2ctraceModuleFolder ) )
    cFiles = filter( lambda x: x.find( '.c' ) != -1, cFiles )
    cFiles.sort()

    cFiles = map( lambda x: 'SOURCE          ' + x, cFiles )

    wrapperFile =  'SOURCEPATH      ..\\..\\..\\common\\lib\\eglwrapper_win\n'
    wrapperFile += 'SOURCE          sct_eglwrapper_win.c'

    files = '\n'.join( cFiles ) + '\n'
    files += '\n\n' + wrapperFile

    mmptemplate = mmptemplate.replace( '$GL2CTRACE_FILES', files )

    open( groupFolder + 'Spandex.mmp.template', 'w' ).write( mmptemplate )
   
