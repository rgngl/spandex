#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys
import optparse
import os

if __name__ == '__main__':

    usage = "usage: %prog [options] <glctrace module folder> <group folder> "
    parser = optparse.OptionParser( usage )
    parser.add_option( '--wrapper',
                       type    = 'choice',
                       dest    = 'wrapper',
                       default = 'Egl',
                       choices = [ 'Egl' ],
                       help    = 'Wrapper type' )

    ( options, args ) = parser.parse_args()

    if len( args ) != 2:
        parser.error( 'incorrect number of arguments' )
        sys.exit( -1 )

    glctraceModuleFolder        = args[ 0 ]
    groupFolder                 = args[ 1 ]
    
    if glctraceModuleFolder[ -1: 0 ] != '/':
        glctraceModuleFolder += '/'

    if groupFolder[ -1: 0 ] != '/':
        groupFolder += '/'

    mmptemplate = open( groupFolder + 'Spandex.mmp.template.template' ).read()

    animatedCFiles = filter( lambda x: x.find( 'sct_glctraceAnimated' ) != -1, os.listdir( glctraceModuleFolder ) )
    animatedCFiles = filter( lambda x: x.find( '.c' ) != -1, animatedCFiles )
    animatedCFiles.sort()

    animatedCFiles = map( lambda x: 'SOURCE          ' + x, animatedCFiles )
 
    animatedAsmFiles = filter( lambda x: x.find( 'sct_glctrace_Animated' ) != -1, os.listdir( glctraceModuleFolder ) )
    animatedAsmFiles = filter( lambda x: x.find( '.s' ) != -1, animatedAsmFiles )
    animatedAsmFiles.sort()

    animatedAsmFiles = map( lambda x: 'SOURCE          ' + x, animatedAsmFiles )

    files3 = '\n'.join( animatedCFiles ) + '\n' + '\n'.join( animatedAsmFiles )

    if options.wrapper == 'Egl':
        wrapperFile =  'SOURCEPATH      ..\\..\\..\\common\\lib\\glcwrapper\n'
        wrapperFile += 'SOURCE          sct_glcwrapper.c'
    else:
        assert( 0 )
        
    files = files3 + '\n\n' + wrapperFile

    mmptemplate = mmptemplate.replace( '$GLCTRACE_FILES', files )

    open( groupFolder + 'Spandex.mmp.template', 'w' ).write( mmptemplate )
   
