/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "CSpandexCommon.h"
#include "CPlatform.h"

#include "sct_utils.h"
#include "sct_sicommon.h"
#include "sct_siwindow.h"

#include <e32base.h>
#include <e32cons.h>
#include <hal.h>
#include <driveinfo.h>
#include <string.h>

#if defined( SCT_SYMBIAN_FORCE_IO_DRIVE_C )
# define SCT_SYMBIAN_FORCE_IO_DRIVE  'c'
#endif  /* defined( SCT_SYMBIAN_FORCE_IO_DRIVE_C ) */

#if defined( SCT_SYMBIAN_FORCE_IO_DRIVE_E )
# define SCT_SYMBIAN_FORCE_IO_DRIVE  'e'
#endif  /* defined( SCT_SYMBIAN_FORCE_IO_DRIVE_E ) */

#define SCT_INPUT_FILE_FORMAT_STRING    "%c:\\benchmark.txt"
#define SCT_OUTPUT_FILE_FORMAT_STRING   "%c:\\result.txt"
#define SCT_DEVICE_ID_FORMAT_STRING     "%c:\\spandex_did.txt"

extern CSpandexCommon*  gSpandexCommon;
extern CConsoleBase*    gConsole;
SCTLog*                 gLog                    = NULL;

/*!
 *
 *
 */
static int getMemoryCardDrive( char* drive );

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::SetSpandexCommon( CSpandexCommon* aCommon )
{
    gSpandexCommon = aCommon;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
CSpandexCommon* CPlatform::SpandexCommon()
{
    return gSpandexCommon;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::SetContainer( void* aContainer )
{
    SCT_USE_VARIABLE( aContainer );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTLog* CPlatform::GetLog()
{
    return gLog;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::SetLog( SCTLog* log )
{
    gLog = log;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::ScreenSize( TInt& aWidth, TInt& aHeight )
{
	HAL::Get( HALData::EDisplayXPixels, aWidth );
	HAL::Get( HALData::EDisplayYPixels, aHeight );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
TDisplayMode CPlatform::ScreenMode()
{
	int numColor = CPlatform::ScreenColorBits();

    switch( numColor )
    {
    case 12:
        return EColor4K;
    case 16:
        return EColor64K;
    case 24:
        return EColor16M;
    case 32:
        return EColor16MA;
    default:
        return ENone;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
int CPlatform::ScreenColorBits()
{
	TInt numColor;
	HAL::Get( HAL::EDisplayColors, numColor );

	if( numColor == ( 1 << 12 ) )
	{
        return 12;
	}
	else if( numColor == ( 1 << 16 ) )
	{
        return 16;
	}
	else if( numColor == ( 1 << 24 ) )
    {
        return 24;
    }
    else
    {
        return 32;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::DisplayError( const char* aTitle, const char* aMessage )
{
    SCT_ASSERT_ALWAYS( gConsole != NULL );

    TPtrC8 title8( ( const TUint8* )aTitle );
    TPtrC8 message8( ( const TUint8* )aMessage );

    HBufC* titlebuf   = HBufC::New( strlen( aTitle ) + 1 );
    HBufC* messagebuf = HBufC::New( strlen( aMessage ) + 1 );
    if( titlebuf == NULL || messagebuf == NULL )
    {
        delete titlebuf;
        delete messagebuf;
        return;
    }
    titlebuf->Des().Copy( title8 );
    messagebuf->Des().Copy( message8 );

	gConsole->Printf( *titlebuf );
	gConsole->Printf( _L(" : ") );
	gConsole->Printf( *messagebuf );
	gConsole->Printf( _L("\n") );

    RDebug::Printf( "SPANDEX error: %s: %s\n", aTitle, aMessage );
    
    delete titlebuf;
    delete messagebuf;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::DisplayMessage( const char* aTitle, const char* aMessage )
{
    SCT_USE_VARIABLE( aTitle );

    SCT_ASSERT_ALWAYS( gConsole != NULL );
    
    TPtrC8 title8( ( const TUint8* )aTitle );
    TPtrC8 message8( ( const TUint8* )aMessage );

    HBufC* titlebuf   = HBufC::New( strlen( aTitle ) + 1 );
    HBufC* messagebuf = HBufC::New( strlen( aMessage ) + 1 );
    if( titlebuf == NULL || messagebuf == NULL )
    {
        delete titlebuf;
        delete messagebuf;
        return;
    }
    titlebuf->Des().Copy( title8 );
    messagebuf->Des().Copy( message8 );

	gConsole->Printf( *titlebuf );
	gConsole->Printf( _L(" : ") );
	gConsole->Printf( *messagebuf );
	gConsole->Printf( _L("\n") );

    RDebug::Printf( "SPANDEX message: %s: %s\n", aTitle, aMessage );
    
    delete titlebuf;
    delete messagebuf;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void CPlatform::ReportProgress( TInt aTotal, TInt aCurrent, TInt aRepeat, const char* aName )
{
    HBufC* buf = HBufC::New( strlen( aName ) + 64 );
    if( buf == NULL )
    {
        return;
    }
    HBufC* benchmarkName = HBufC::New( strlen( aName ) + 1 );
    if( benchmarkName == NULL )
    {
        delete buf;
        return;
    }
    TPtrC8 name8( ( const TUint8* )( aName ) );

    SCT_ASSERT_ALWAYS( gConsole != NULL );

    benchmarkName->Des().Copy( name8 );

    buf->Des().Format( _L( "Benchmark %d/%d, %d: %S\n"), aCurrent, aTotal, aRepeat, benchmarkName );
    gConsole->Printf( *buf );

    RDebug::Printf( "SPANDEX benchmark %d/%d, %d: %s\n", aCurrent, aTotal, aRepeat, aName );
    
    delete buf;
    delete benchmarkName;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static int getMemoryCardDrive( char* drive )
{
#if !defined( SCT_SYMBIAN_FORCE_IO_DRIVE )
    RFs         fs;
    TDriveInfo  info;
    int         status = 0;
 
    SCT_ASSERT_ALWAYS( drive != NULL );
    
    fs.Connect();

    for( TInt d = 0; d < ( EDriveZ - EDriveA ); ++d )
    {
        if( fs.Drive( info, EDriveA + d ) == KErrNone )
        {
            if( ( info.iDriveAtt & KDriveAttRemovable ) != 0 )
            {
                /* Make sure the removable drive is indeed a removable memory
                 * card with the input file. */
                const int   filenamelen = 128;
                char        filename[ filenamelen ];
                char        tempdrive;
                FILE*       fp;

                SCT_ASSERT_ALWAYS( strlen( SCT_INPUT_FILE_FORMAT_STRING ) < filenamelen );
                
                tempdrive = 'a' + d;
                sprintf( filename, SCT_INPUT_FILE_FORMAT_STRING, tempdrive );

                fp = fopen( filename, "r" );
                if( fp != NULL )
                {
                    fclose( fp );
                    *drive = tempdrive;
                    status  = 1;
                    break;
                }
            }
        }
    }

    fs.Close();

    return status;
#else   /* !defined( SCT_SYMBIAN_FORCE_IO_DRIVE ) */
    SCT_ASSERT_ALWAYS( drive != NULL );
    *drive = SCT_SYMBIAN_FORCE_IO_DRIVE;

    return 1;
#endif  /* !defined( SCT_SYMBIAN_FORCE_IO_DRIVE ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
const char* CPlatform::InputFileName()
{
#if defined( __WINS__ )
    return "C:\\benchmark.txt";
#else   /* defined( __WINS__ ) */
    static char     buf[ 128 ];
    char            drive;

    SCT_ASSERT_ALWAYS( getMemoryCardDrive( &drive ) != 0 );
    sprintf( buf, SCT_INPUT_FILE_FORMAT_STRING, drive );
    return buf;
#endif  /* defined( __WINS__ ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
const char* CPlatform::OutputFileName()
{
#if defined( __WINS__ )
    return "C:\\result.txt";
#else   /* defined( __WINS__ ) */
    static char     buf[ 128 ];
    char            drive;

    SCT_ASSERT_ALWAYS( getMemoryCardDrive( &drive ) != 0 );
    sprintf( buf, SCT_OUTPUT_FILE_FORMAT_STRING, drive );
    return buf;
#endif  /* defined( __WINS__ ) */
}

