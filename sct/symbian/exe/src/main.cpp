/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <e32base.h>
#include <e32cons.h>
#include <stdlib.h>

#include "CSpandexCommon.h"
#include "CPlatform.h"
#include "sct_sicommon.h"

#define ERROR_DELAY     5000000

/*!
 *
 */
CSpandexCommon*         gSpandexCommon  = NULL;
CConsoleBase*           gConsole        = NULL;

/*!
 *
 */
void                    runAppL();

/* ------------------------------------------------------------ */
/*!
 *
 *
 */
void runAppL()
{
    int error;

    RDebug::Printf( "SPANDEX: start" );       

	RProcess theProcess;					
    theProcess.SetPriority( EPriorityHigh );

    gConsole = Console::NewL( _L( "Spandex" ), TSize( KConsFullScreen, KConsFullScreen ) );
    CleanupStack::PushL( gConsole );

	gSpandexCommon = new( ELeave ) CSpandexCommon;
    CleanupStack::PushL( gSpandexCommon );

    RDebug::Printf( "SPANDEX: Running Spandex" );       
    gConsole->Printf( _L( "Running Spandex\n" ) );

    TRAP( error, gSpandexCommon->ConstructL() );
    if( error != KErrNone )
    {
        RDebug::Printf( "SPANDEX: Construction failed!" );
        gConsole->Printf( _L( "Construction failed!\n" ) );
        User::After( ERROR_DELAY );
        User::Leave( 0 );
    }

    RDebug::Printf( "SPANDEX: benchmark cycle start\n" );       
    while( gSpandexCommon->Cycle() ) {};
    RDebug::Printf( "SPANDEX: benchmark cycle end\n" );
    
    User::After( 1000000 );
  
    CleanupStack::PopAndDestroy(); /* gSpandexCommon */
    CleanupStack::PopAndDestroy(); /* gConsole */

}

/* ------------------------------------------------------------ */
/*!
 *
 *
 */
GLDEF_C TInt E32Main()
{
    int error;

#if defined( SCT_USE_ACTIVE_SCHEDULER )
    CActiveScheduler* activeScheduler = new CActiveScheduler;
    CActiveScheduler::Install( activeScheduler );
#endif  /* defined( SCT_USE_ACTIVE_SCHEDULER ) */   
    
#if !defined( SCT_NO_HEAP_CHECK )
	__UHEAP_MARK;
#endif  /* !defined( SCT_NO_HEAP_CHECK ) */
	CTrapCleanup* cleanupStack = CTrapCleanup::New();
	TRAP( error, runAppL() );
	delete cleanupStack;

    CloseSTDLIB();
    
#if !defined( SCT_NO_HEAP_CHECK )
	__UHEAP_MARKEND;
#endif  /* !defined( SCT_NO_HEAP_CHECK ) */

#if defined( SCT_USE_ACTIVE_SCHEDULER )
    CActiveScheduler::Install( NULL );
    delete activeScheduler;
#endif  /* defined( SCT_USE_ACTIVE_SCHEDULER ) */   
    
	return 0;
}
