/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_aglmodule.h"
#include "sct_sicommon.h"
#include "sct_action.h"
#include "sct_result.h"
#include "sct_module.h"
#include "sct_utils.h"

#include "sct_agl.h"

#include "sct_aglmodule_parser.h"
#include "sct_aglmodule_actions.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
SCTAttributeList*       sctiAglModuleInfo( SCTModule* module );
SCTAction*              sctiAglModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes );
void                    sctiAglModuleDestroy( SCTModule* module );

/*!
 *
 *
 */
SCTModule* sctCreateAglModule( void )
{
    SCTModule*                  module;
    SCTAglModuleContext*        context;

    context = ( SCTAglModuleContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTAglModuleContext ) ) );
    if( context == NULL )
    {
        return NULL;
    }
    memset( context, 0, sizeof( SCTAglModuleContext ) );

	context->aglContext = siAglGetContext();
	if( context->aglContext == NULL )
	{
        siCommonMemoryFree( NULL, context );
        return NULL;		
	}
	
    module = sctCreateModule( "Agl",
                              context,
#if defined( _WIN32 )
                              _agl_parser_config,
#else   /* defined( _WIN32 ) */
                              "",
#endif  /* defined( _WIN32 ) */
                              sctiAglModuleInfo,
                              sctiAglModuleCreateAction,
                              sctiAglModuleDestroy );

    if( module == NULL )
    {
		siAglReleaseContext( context->aglContext );
		context->aglContext = NULL;
		
        siCommonMemoryFree( NULL, context );
        return NULL;
    }
	

    return module;
}

/*!
 *
 *
 */
SCTAttributeList* sctiAglModuleInfo( SCTModule* module )
{
    SCTAglModuleContext*        context;
    SCTAttributeList*           attributes;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( module->context != NULL );

    context = ( SCTAglModuleContext* )( module->context );

    /* Start collecting data to attribute list. */
    attributes = sctCreateAttributeList();
	
	return attributes;
}

/*!
 *
 *
 */
SCTAction* sctiAglModuleCreateAction( SCTModule* module, const char* name, const char* type, SCTAttributeList* attributes )
{
    SCTAglModuleContext*        moduleContext;

    SCT_ASSERT_ALWAYS( module != NULL );
    SCT_ASSERT_ALWAYS( name != NULL );
    SCT_ASSERT_ALWAYS( type != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    moduleContext = ( SCTAglModuleContext* )( module->context );

    SCT_ASSERT_ALWAYS( moduleContext != NULL );

    return sctCreateActionFromTemplate( name, type, "Agl", moduleContext, attributes, AglActionTemplates, SCT_ARRAY_LENGTH( AglActionTemplates ) );
}

/*!
 *
 *
 */
void sctiAglModuleDestroy( SCTModule* module )
{
    SCTAglModuleContext*        context;

    SCT_ASSERT_ALWAYS( module != NULL );

    context = ( SCTAglModuleContext* )( module->context );

    if( context == NULL )
    {
        return;
    }

	if( context->aglContext == NULL )
    {
		siAglReleaseContext( context->aglContext );
		context->aglContext = NULL;
	}
	
    siCommonMemoryFree( NULL, context );
    module->context = NULL;
}

