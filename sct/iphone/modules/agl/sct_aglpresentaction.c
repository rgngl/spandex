/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_aglpresentaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_agl.h"
#include "sct_aglmodule_parser.h"
#include "sct_aglmodule.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateAglPresentActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTAglPresentActionContext*     context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTAglPresentActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTAglPresentActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Present@Agl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTAglPresentActionContext ) );

    context->moduleContext = ( SCTAglModuleContext* )( moduleContext );

    if( sctiParseAglPresentActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyAglPresentActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyAglPresentActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiAglPresentActionExecute( SCTAction* action, int frameNumber )
{
    SCTAglPresentActionContext*		context;
	void*							aglContext;

    SCT_ASSERT( action != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTAglPresentActionContext* )( action->context );

    aglContext = context->moduleContext->aglContext;
    SCT_ASSERT( aglContext != NULL );

    if( siAglPresent( aglContext ) == SCT_FALSE )
    {
	    SCT_LOG_ERROR( "Call to siAglPresent failed in Present@Agl action execute." );		
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiAglPresentActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyAglPresentActionContext( ( SCTAglPresentActionContext* )( action->context ) );
    action->context = NULL;
}

