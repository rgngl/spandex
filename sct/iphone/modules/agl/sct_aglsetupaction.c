/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_aglsetupaction.h"
#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_agl.h"
#include "sct_aglmodule_parser.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateAglSetupActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTAglSetupActionContext*		context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTAglSetupActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTAglSetupActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Setup@Agl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTAglSetupActionContext ) );

    context->moduleContext = ( SCTAglModuleContext* )( moduleContext );

    if( sctiParseAglSetupActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyAglSetupActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyAglSetupActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiAglSetupActionInit( SCTAction *action, SCTBenchmark *benchmark )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_USE_VARIABLE( benchmark );

    return SCT_TRUE;
}

/*!
 *
 *
 */
SCTBoolean sctiAglSetupActionExecute( SCTAction *action, int framenumber )
{
    SCTAglSetupActionContext*		context;
	void*							aglContext;
	
    SCT_ASSERT_ALWAYS( action != NULL );
    SCT_USE_VARIABLE( framenumber );

    context = ( SCTAglSetupActionContext* )( action->context );

    aglContext = context->moduleContext->aglContext;
    SCT_ASSERT( aglContext != NULL );
	
    if( siAglInitialize( aglContext ) == SCT_FALSE )
    {
	    SCT_LOG_ERROR( "Call to siAglInitialize failed in Setup@Agl action execute." );		
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiAglSetupActionTerminate( SCTAction* action )
{
    SCTAglSetupActionContext*		context;
	void*							aglContext;
	
    SCT_ASSERT_ALWAYS( action != NULL );
    if( action->context == NULL )
    {
        return;
    }

    context = ( SCTAglSetupActionContext* )( action->context );

    aglContext = context->moduleContext->aglContext;
    SCT_ASSERT( aglContext != NULL );
	
    siAglTerminate( aglContext );
}

/*!
 *
 *
 */
void sctiAglSetupActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );

    sctiDestroyAglSetupActionContext( ( SCTAglSetupActionContext* )( action->context ) );
    action->context = NULL;
}

