/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_aglflushaction.h"

#include "sct_sicommon.h"
#include "sct_utils.h"

#include "sct_agl.h"
#include "sct_aglmodule_parser.h"
#include "sct_aglmodule.h"

#include <stdio.h>
#include <string.h>

/*!
 *
 *
 */
void* sctiCreateAglFlushActionContext( void* moduleContext, SCTAttributeList* attributes )
{
    SCTAglFlushActionContext*		context;

    SCT_ASSERT_ALWAYS( moduleContext != NULL );
    SCT_ASSERT_ALWAYS( attributes != NULL );

    context = ( SCTAglFlushActionContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTAglFlushActionContext ) ) );
    if( context == NULL )
    {
        SCT_LOG_ERROR( "Allocation failed in Flush@Agl context creation." );
        return NULL;
    }
    memset( context, 0, sizeof( SCTAglFlushActionContext ) );

    context->moduleContext = ( SCTAglModuleContext* )( moduleContext );

    if( sctiParseAglFlushActionAttributes( &( context->data ), attributes ) == SCT_FALSE )
    {
        sctiDestroyAglFlushActionContext( context );
        return NULL;
    }

    return context;
}

/*!
 *
 *
 */
void sctiDestroyAglFlushActionContext( void* context )
{
    if( context == NULL )
    {
        return;
    }

    siCommonMemoryFree( NULL, context );
}

/*!
 *
 *
 */
SCTBoolean sctiAglFlushActionExecute( SCTAction* action, int frameNumber )
{
    SCTAglFlushActionContext*		context;
	void*							aglContext;

    SCT_ASSERT( action != NULL );
    SCT_USE_VARIABLE( frameNumber );

    context = ( SCTAglFlushActionContext* )( action->context );

    aglContext = context->moduleContext->aglContext;
    SCT_ASSERT( aglContext != NULL );

    if( siAglFlush( aglContext ) == SCT_FALSE )
    {
	    SCT_LOG_ERROR( "Call to siAglFlush failed in Flush@Agl action execute." );		
        return SCT_FALSE;
    }

    return SCT_TRUE;
}

/*!
 *
 *
 */
void sctiAglFlushActionDestroy( SCTAction* action )
{
    SCT_ASSERT_ALWAYS( action != NULL );
    
    sctiDestroyAglFlushActionContext( ( SCTAglFlushActionContext* )( action->context ) );
    action->context = NULL;
}

