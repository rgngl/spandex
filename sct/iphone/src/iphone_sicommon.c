/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/* */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>

#include "iphone_globals.h"

/* Includes from Spandex core */
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_module.h"

#if defined( INCLUDE_AGL_MODULE )
# include "sct_aglmodule.h"
#endif  /* defined( INCLUDE_AGL_MODULE ) */

#define		SCT_SICOMMON_IPHONE_CONTEXT				0x1234
#define		OUTPUT_BUFFER_SIZE						1024

SCTLog*		gErrorLog								= NULL;	 
char		gOutputBuffer[ OUTPUT_BUFFER_SIZE ];
int			gOutputSize								= 0;
void*		gSingleton								= NULL;

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void* siCommonGetContext( void )
{
	return ( void* )( SCT_SICOMMON_IPHONE_CONTEXT );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
const char* siCommonQuerySystemString( void* context, SCTSystemString id )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );    
    
    switch( id )
    {
    case SCT_SYSSTRING_DEVICE_TYPE:
        return "iPhone";

    case SCT_SYSSTRING_CPU:
        return "<Unknown>";
        
    case SCT_SYSSTRING_OS:
        return "iOS";
        
    default:
        SCT_ASSERT_ALWAYS( 0 );
        return NULL;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
size_t siCommonQueryMemoryAttribute( void* context, SCTMemoryAttribute id )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( id );
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( context, "SPANDEX: siCommonQueryMemoryAttribute not implemented" );
#endif  /* defined( SCT_DEBUG ) */
    
    return 0;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siCommonSampleCpuLoad( void* context )
{
    SCT_USE_VARIABLE( context );

    return NULL;    /* Not supported. */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
float siCommonCalculateCpuLoad( void* context, void* sample, SCTCpuLoad load )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( sample );
    SCT_USE_VARIABLE( load );
    
    return -1.0f;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siCommonDestroyCpuLoadSample( void* context, void* sample )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( sample );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
unsigned long siCommonGetTimerFrequency( void* context )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	return 1000000;	/* Microseconds */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
unsigned long siCommonGetTimerTick( void* context )
{
	struct timeval	tv;
	
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	gettimeofday( &tv, NULL );
	
	return ( unsigned long )( tv.tv_sec * 1000000 + tv.tv_usec );	/* Microseconds */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonDebugPrintf( void* context, const char* fmt, ... )
{
	va_list ap;

	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );    
    SCT_ASSERT_ALWAYS( fmt != NULL );

	va_start( ap, fmt );
	vprintf( fmt, ap );
	va_end( ap );

    printf( "\n" );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonWarningMessage( void* context, const char* warningMessage )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    
    if( gOutputFile )
    {
        siCommonFlushOutput( context );
        fprintf( gOutputFile, "#SPANDEX WARNING: %s\n", warningMessage );
        fflush( gOutputFile );     
    }
    
    printf( "SPANDEX WARNING: %s\n", warningMessage );
}


/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonErrorMessage( void* context, const char* errorMessage )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	if( gOutputFile )
    {
		siCommonFlushOutput( context );
        fprintf( gOutputFile, "SPANDEX ERROR: %s\n", errorMessage );
 		fflush( gOutputFile );     
    }
  
	printf( "SPANDEX error: %s.\n", errorMessage );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
SCTLog* siCommonGetErrorLog( void* context )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	return gErrorLog;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonSetErrorLog( void* context, SCTLog* log )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	gErrorLog = log;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonProgressMessage( void* context, int totalBenchmark, int currentBenchmark, int currentRepeat, const char* benchmarkName )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
  
	printf( "Benchmark %d/%d, %d: %s.\n", currentBenchmark, totalBenchmark, currentRepeat, benchmarkName );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
void siCommonAssert( void* context, void* exp, void* file, unsigned int line )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	if( gOutputFile )
    {
		siCommonFlushOutput( context );
		fprintf( gOutputFile, 
				 "\nSpandex assert failed.\nExpresion: ( %s )\nFile: %s\nLine: %d\n",
				 ( char* )exp, ( char* )file, ( int )line ); 
		fflush( gOutputFile );
    }

	printf( "\nSpandex assert failed.\nExpresion: ( %s )\nFile: %s\nLine: %d\n",  ( char* )exp, ( char* )file, ( int )line ); 
	assert( 0 );
}

/* ---------------------------------------------------------------------- */
/*!
 * 
 *
 */
void* siCommonMemoryAlloc( void* context, unsigned int size )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	return malloc( size );
}

/* ---------------------------------------------------------------------- */
/*!
 * 
 *
 */
void* siCommonMemoryRealloc( void* context, void* data, unsigned int size )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	return realloc( data, size );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siCommonMemoryFree( void* context, void* memblock )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	free( memblock );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
char siCommonReadChar( void* context )
{
	char	ch;

	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
	
	if( gInputFile == NULL )
	{
		SCT_ASSERT( gBenchmarkSuite != NULL );
		return *gBenchmarkSuite++;
	}
	else
	{
		SCT_ASSERT( gInputFile != NULL );

		return ( fread( &ch, sizeof( char ), 1, gInputFile ) == 1 ) ? ch : SCT_END_OF_INPUT;
	}
}
/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
int siCommonWriteChar( void* context, char aCh )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	gOutputBuffer[ gOutputSize++ ] = aCh;
	if( gOutputSize >= OUTPUT_BUFFER_SIZE )
    {
		return siCommonFlushOutput( context );
    }
	else
	{
		return 1;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
int siCommonFlushOutput( void* context )
{
	int	n;

	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
	SCT_ASSERT( gOutputFile != NULL );

	if( gOutputSize == 0 ) // Beware empty buffer
	{
		fflush( gOutputFile );	
		return 1;
	}
	
	n = fwrite( gOutputBuffer, gOutputSize, 1, gOutputFile );
	gOutputSize = 0;
	fflush( gOutputFile );

	return ( n == 1 );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
SCTModuleList* siCommonGetModules( void* context )
{
	SCTModuleList*	modules;
	SCTModule*		module;

	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

    modules = sctGetCoreModules();   
    if( modules == NULL )
    {
		return NULL;
    }
	   
#if defined( INCLUDE_AGL_MODULE )
    module = sctCreateAglModule();
    if( module == NULL || sctAddModuleToList( modules, module ) == SCT_FALSE )
    {
        sctDestroyModuleList( modules );
        return NULL;
    }
#endif  /* defined( INCLUDE_AGL_MODULE ) */
	
    return modules;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonSleep( void* context, unsigned long micros )
{
	struct timespec rqtp;
	
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	rqtp.tv_sec		= 0;
	rqtp.tv_nsec	= micros * 1000;
	
	nanosleep( &rqtp, NULL );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonYield( void* context )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
	// Do nothing
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonGetTime( void* context, char* buffer, int length )
{
	time_t      ltime;
	struct tm*	today;

	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
	SCT_ASSERT_ALWAYS( length >= 1 );

	time( &ltime );
	today = localtime( &ltime );
	if( strftime( buffer, length, "%Y-%m-%d %H:%M:%S", today ) == 0 )
    {
		buffer[ 0 ] = '\0';
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siCommonGetSingleton( void* context )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	return gSingleton;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siCommonSetSingleton( void* context, void* ptr )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	gSingleton = ptr;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTExtensionProc siCommonGetProcAddress( void* context, const char* procname )
{
	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );

	return NULL;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
int siCommonGetMessage( void* context, SCTMessage* message, long timeoutMillis )
{
    SCT_USE_VARIABLE( context );
    SCT_ASSERT_ALWAYS( message != NULL );
    SCT_USE_VARIABLE( timeoutMillis );

    return -1;
}

/* Don't support threads in iOS at the moment. */
void* siCommonCreateThread( void* context, SCTBenchmarkWorkerThreadFunc func, SCTBenchmarkWorkerThreadContext* threadContext )
{
   	SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( func );
    SCT_USE_VARIABLE( threadContext );
    
    return NULL;
}

void siCommonThreadCleanup( void* context )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );
}

void siCommonJoinThread( void* context, void* thread )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( thread );
}

SCTBoolean siCommonSetThreadPriority( void* context, SCTThreadPriority priority )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( priority );

    return SCT_FALSE;
}

void* siCommonCreateMutex( void* context )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );

    return ( void* )( 1 );
}

void siCommonLockMutex( void* context, void* mutex )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( mutex );
}

void siCommonUnlockMutex( void* context, void* mutex )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( mutex );
}

void siCommonDestroyMutex( void* context, void* mutex )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( mutex );
}

void* siCommonCreateSignal( void* context )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );

    return ( void* )( 2 );
}

void siCommonDestroySignal( void* context, void* signal )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( signal );
}

int siCommonWaitForSignal( void* context, void* signal, long timeoutMillis )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( signal );
    SCT_USE_VARIABLE( timeoutMillis );
    
    return -1;
}

void siCommonTriggerSignal( void* context, void* signal )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( signal );
}

void siCommonCancelSignal( void* context, void* signal )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_IPHONE_CONTEXT ) );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( signal );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
void siCommonProfileEvent( void* context, const char* event )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( event );
    /* TODO */
}

