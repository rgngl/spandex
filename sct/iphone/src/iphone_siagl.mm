/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/*!
 *
 */
#include "sct_types.h"
#include "sct_utils.h"
#include "sct_sicommon.h"
#include "sct_siagl.h"

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

#include <stdio.h>

/*!
 *
 *
 */
#import "ES2Renderer.h"

/*!
 *
 */
#define		SCT_SIAGL_IPHONE_CONTEXT		0x1235

/* ---------------------------------------------------------------------- */
/*!
 *
 */
void* siAglGetContext()
{
	return ( void* )( SCT_SIAGL_IPHONE_CONTEXT );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 */
void siAglReleaseContext( void* context )
{
	SCT_ASSERT_ALWAYS( context == ( void* )( SCT_SIAGL_IPHONE_CONTEXT ) );
}

/*!
 *
 *
 */
SCTBoolean siAglInitialize( void* context )
{
	SCT_ASSERT( context == ( void* )( SCT_SIAGL_IPHONE_CONTEXT ) );
	SCT_ASSERT( gES2Renderer != NULL );
	
	if( [gES2Renderer initialize] )
	{
		return SCT_TRUE;
	}
	return SCT_FALSE;
}

/*!
 *
 *
 */
void siAglTerminate( void* context )
{
	SCT_ASSERT( context == ( void* )( SCT_SIAGL_IPHONE_CONTEXT ) );
	SCT_ASSERT( gES2Renderer != NULL );
	
    [gES2Renderer uninitialize];
}

/* ---------------------------------------------------------------------- */
/*!
 *
 */    
SCTBoolean siAglPresent( void* context )
{
	SCT_ASSERT( context == ( void* )( SCT_SIAGL_IPHONE_CONTEXT ) );
	SCT_ASSERT( gES2Renderer != NULL );
	
	[gES2Renderer present];
	
	return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 */    
SCTBoolean siAglFlush( void* context )
{
	SCT_ASSERT( context == ( void* )( SCT_SIAGL_IPHONE_CONTEXT ) );
	SCT_ASSERT( gES2Renderer != NULL );
	
	[gES2Renderer flush];
	
	return SCT_TRUE;
}
