//
//  main.m
//  spandex_gl2
//
//  Created by Kari J. Kangas on 10/9/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import "ES2Renderer.h"

#include <stdio.h>
#include "iphone_globals.h"
#include "sct.h"

/*!
 *
 *
 */
FILE*           gInputFile                    = NULL;
const char*     gBenchmarkSuite               = NULL;
FILE*           gOutputFile                   = NULL;
void*           gSctContext                   = NULL;
ES2Renderer*    gES2Renderer                  = NULL;

//#define SCT_IPHONE
#define SCT_IPHONE4
//#define SCT_IPAD

#if defined( SCT_IPHONE )
/* iPhone/iPod touch */
int             gScreenWidth                  = 320; 
int             gScreenHeight                 = 480;
#endif 

#if defined( SCT_IPHONE4 )
/* iPhone4/iPod4 touch */
int             gScreenWidth                  = 640; 
int             gScreenHeight                 = 960;
#endif

#if defined( SCT_IPAD )
/* iPhone4/iPod4 touch */
int             gScreenWidth                  = 768; 
int             gScreenHeight                 = 1024;
#endif

/*!
 *
 *
 */
int main(int argc, char *argv[]) 
{	
	int                 retVal                = 1;
	
	printf( "SPANDEX: start\n" );
	
	/* First, get application documents directory */
	CFStringEncoding    encoding              = kCFStringEncodingMacRoman; // =  0;
	NSArray*            paths                 = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
	const int           documentsDirectoryLen = 512;
	char                documentsDirectory[ documentsDirectoryLen ];
	
	CFStringGetCString( ( CFStringRef )( [paths objectAtIndex:0] ), documentsDirectory, documentsDirectoryLen, encoding );	
	printf( "SPANDEX: documents directory %s\n", documentsDirectory );
	
	/* Secondly, create input file name */
	const int           fileLen              = documentsDirectoryLen + 16;
	char                file[ fileLen ];
	
	snprintf( file, fileLen, "%s/%s", documentsDirectory, "benchmark.txt" );
	printf( "SPANDEX: input file=%s.\n", file );
	
	gInputFile = fopen( file, "r" );
	if( gInputFile == NULL )
	{
		printf( "SPANDEX: opening input file failed.\n" );
		goto END;
	}

	/* Secondly, create output file name */	
	snprintf( file, fileLen, "%s/%s", documentsDirectory, "result.txt" );
	printf( "SPANDEX: output file=%s.\n", file );
	
	gOutputFile = fopen( file, "w+" );
	if( gOutputFile == NULL )
	{
		printf( "SPANDEX: opening output file failed.\n" );
		goto END;
	}
	
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
	
END:	
		
	if( gInputFile == NULL )
	{
		fclose( gInputFile );
		gInputFile = NULL;
	}
	if( gOutputFile == NULL )
	{
		fclose( gOutputFile );
		gOutputFile = NULL;
	}
	
	printf( "SPANDEX: end\n" );
	
    return retVal;
}
