/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/*!
 *
 *
 */
#import "EAGLView.h"
#include "sct.h"
/*!
 *
 *
 */
@implementation EAGLView

/*!
 *
 *
 */
+ (Class) layerClass
{
    return [CAEAGLLayer class];
}

/*!
 *
 *
 */
- (id) initWithCoder:(NSCoder*)coder
{    
    if( ( self = [super initWithCoder:coder] ) )
	{
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
        
        eaglLayer.opaque				= TRUE;
        eaglLayer.drawableProperties	= [NSDictionary dictionaryWithObjectsAndKeys:
										   [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
								
        iRenderer = [[ES2Renderer alloc] init];
		if( !iRenderer )
		{
			printf( "SPANDEX: failed to create ES2Renderer.\n" );
			[self release];
			return nil;		
		}
		gES2Renderer = iRenderer;
		
		printf( "SPANDEX: sctInitialize.\n" );
		iSctContext = sctInitialize();
		
		if( iSctContext == NULL )
		{
			printf( "SPANDEX: sctInitialize failed.\n" );
			[self release];
			return nil;
		}
		
		animating				= FALSE;
		finished				= FALSE;
		animationTimer			= nil;
    }
	
	[[UIApplication sharedApplication] setStatusBarHidden:YES];	
	
    return self;
}

/*!
 *
 *
 */
- (void) drawView:(id)sender
{
	[self cycleBenchmark];
}

/*!
 *
 *
 */
- (void) layoutSubviews
{
}

/*!
 *
 *
 */
- (void) cycleBenchmark
{
	assert( gES2Renderer );
	assert( iSctContext );
	
	SCTCycleStatus status;
		
	printf( "SPANDEX: benchmark cycle start.\n" );

	[self stopAnimation];	
	[gES2Renderer setLayer:(CAEAGLLayer*)self.layer];
	
	if( finished )
	{
		printf( "SPANDEX: benchmark finished.\n" );
		
		[gES2Renderer initialize];
		
		glClearColor( 1.0, 1.0, 1.0, 1.0 );
		glClear( GL_COLOR_BUFFER_BIT );
		
		[gES2Renderer present];
		[gES2Renderer uninitialize];
		
		[[UIApplication sharedApplication] setStatusBarHidden:NO];	
		
		return;
	}
	
	status = sctCycle( iSctContext );

	if( status == SCT_CYCLE_FATAL_ERROR )
	{
        printf( "SPANDEX: cycle fatal error.\n" );
		finished = TRUE;
	}
    else if( status == SCT_CYCLE_FINISHED )
    {
        printf( "SPANDEX: cycle finished.\n" );        
		finished = TRUE;
    }
	
	printf( "SPANDEX: benchmark cycle end.\n" );
	[self triggerAnimation];
}

/*!
 *
 *
 */
- (void) triggerAnimation
{
	if( !animating )
	{
		printf( "SPANDEX: triggerAnimation.\n" );
		animationTimer	= [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)(1.0 / 60.0) target:self selector:@selector(drawView:) userInfo:nil repeats:FALSE];		
		animating		= TRUE;
	}
}

- (void)stopAnimation
{
	if( animating )
	{
		printf( "SPANDEX: stopAnimation.\n" );
		[animationTimer invalidate];
		animationTimer	= nil;
		animating		= FALSE;
	}
}

/*!
 *
 *
 */
- (void) dealloc
{	
	if( iSctContext != NULL )
	{
		printf( "SPANDEX: sctTerminate.\n" );
		sctTerminate( iSctContext );
		iSctContext = NULL;
	}
	if( iRenderer )
	{
		[iRenderer release];
	}
	gES2Renderer = NULL;
	
    [super dealloc];
}

@end
