/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#import "spandex_gl2AppDelegate.h"
#import "EAGLView.h"

@implementation spandex_gl2AppDelegate

@synthesize window;
@synthesize glView;

/*!
 *
 *
 */
- (void) applicationDidFinishLaunching:(UIApplication *)application
{
}

/*!
 *
 *
 */
- (void) applicationWillResignActive:(UIApplication *)application
{
	[glView stopAnimation];
}

/*!
 *
 *
 */
- (void) applicationDidBecomeActive:(UIApplication *)application
{
	[glView triggerAnimation];
}

/*!
 *
 *
 */
- (void)applicationWillTerminate:(UIApplication *)application
{
}

/*!
 *
 *
 */
- (void) dealloc
{
	[window release];
	[glView release];
	
	[super dealloc];
}

@end
