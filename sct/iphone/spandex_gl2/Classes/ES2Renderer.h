/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/*!
 *
 *
 */
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/EAGLDrawable.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

/*!
 *
 *
 */
@interface ES2Renderer : NSObject
{
@private
	EAGLContext*	iContext;
	CAEAGLLayer*    iLayer;
	void*           iSctContext;
	
	GLint			iBackingWidth;
	GLint			iBackingHeight;
	
	GLuint			iFramebuffer;
	GLuint          iColorRenderbuffer;	
	
	char*           iPixelBuffer;	
	int             iPixelBufferX;
	int             iPixelBufferY;
	int             iPixelBufferWidth;
	int				iPixelBufferHeight;
}

/*!
 *
 *
 */
- (BOOL) initialize;
- (void) uninitialize;
- (void) setLayer:(CAEAGLLayer *)layer;
- (void) present;
- (void) flush;

@end

/*!
 *
 *
 */
extern ES2Renderer*     gES2Renderer;
