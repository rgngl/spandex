/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#import "ES2Renderer.h"
#import "iphone_globals.h"

#define SCT_ONE_PIXEL_READBACK
//#define SCT_FULL_SCREEN_READBACK
//#define SCT_MULTIPLE_ONE_PIXEL_READS	16
//#define SCT_CLUSTER_PIXEL_READS

@implementation ES2Renderer

/*!
 *
 *
 */
- (id) init
{
	self = [super init];
	
	iPixelBuffer = NULL;
	
	return self;
}

/*!
 *
 *
 */
- (BOOL) initialize
{
	iContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
	
	if( !iContext || ![EAGLContext setCurrentContext:iContext] )
	{
		printf( "SPANDEX: EAGL context creation failed in ES2Renderer.initialize.\n" );
		return NO;
	}
	
	if( !iLayer )
	{
		printf( "SPANDEX: no layer defined in ES2Renderer.initialize.\n" );
		return NO;
	}

	glGenFramebuffers( 1, &iFramebuffer );
	glGenRenderbuffers( 1, &iColorRenderbuffer );
	glBindFramebuffer( GL_FRAMEBUFFER, iFramebuffer );
	glBindRenderbuffer( GL_RENDERBUFFER, iColorRenderbuffer );
	glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, iColorRenderbuffer );
	
    [iContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:iLayer];

	glGetRenderbufferParameteriv( GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &iBackingWidth );
    glGetRenderbufferParameteriv( GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &iBackingHeight );
	
    if( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
	{
        printf( "SPANDEX: failed to make complete framebuffer object in ES2Renderer.initialize, error %x.\n", glCheckFramebufferStatus( GL_FRAMEBUFFER ) );
        return NO;
    }
	
    // iPhone4/iPod4
    if( iBackingWidth != gScreenWidth || iBackingHeight != gScreenHeight )
    {
        printf( "SPANDEX: unexpected screen size %dx%d\n", iBackingWidth, iBackingHeight );
        return NO;

    }
    
	if( iPixelBuffer != NULL )
	{
		free( iPixelBuffer );
		iPixelBuffer = NULL;
	}
	
#if defined( SCT_ONE_PIXEL_READBACK )
	iPixelBufferX			= iBackingWidth / 2;
	iPixelBufferY			= iBackingHeight / 2;
	iPixelBufferWidth		= 1;
	iPixelBufferHeight		= 1;
#endif	/* defined( SCT_ONE_PIXEL_READBACK ) */

#if defined( SCT_FULL_SCREEN_READBACK )
	iPixelBufferX			= 0;
	iPixelBufferY			= 0;
	iPixelBufferWidth		= iBackingWidth;
	iPixelBufferHeight		= iBackingHeight;
#endif	/* defined( SCT_FULL_SCREEN_READBACK ) */
	
	iPixelBuffer = malloc( 4 * iPixelBufferWidth * iPixelBufferHeight );
	if( iPixelBuffer == NULL )
	{
		printf( "SPANDEX: failed to allocate memory for the pixel buffer.\n" );
		return NO;
	}
	
	glViewport( 0, 0, iBackingWidth, iBackingHeight );
	
	return YES;
}

/*!
 *
 *
 */
- (void) uninitialize
{
	if( iPixelBuffer != NULL )
	{
		free( iPixelBuffer );
		iPixelBuffer = NULL;
	}
		
	if( iFramebuffer )
	{
		glDeleteFramebuffers( 1, &iFramebuffer );
		iFramebuffer = 0;
	}
	
	if( iColorRenderbuffer )
	{
		glDeleteRenderbuffers( 1, &iColorRenderbuffer );
		iColorRenderbuffer = 0;
	}
	
	if( [EAGLContext currentContext] == iContext )
	{
        [EAGLContext setCurrentContext:nil];
	}
	
	[iContext release];
	iContext = nil;	
}

/*!
 *
 *
 */
- (void) present
{
	[iContext presentRenderbuffer:GL_RENDERBUFFER];
}

/*!
 *
 *
 */
- (void) flush
{
	//glFlush(); // DOES NOT SEEM TO TRIGGER RENDERING.
	
	// To trigger rendering, read pixels using glReadPixels.
	glPixelStorei( GL_PACK_ALIGNMENT, 1 );

#if !defined( SCT_IOS_MULTIPLE_ONE_PIXEL_READS )
	glReadPixels(	iPixelBufferX,
					iPixelBufferY,
					iPixelBufferWidth, 
					iPixelBufferHeight,
					GL_RGBA,
					GL_UNSIGNED_BYTE,
					iPixelBuffer );
#else   /* !defined( SCT_IOS_MULTIPLE_ONE_PIXEL_READS ) */
# if !defined( SCT_IOS_CLUSTER_PIXEL_READS )
	for( int i = 0; i < SCT_IOS_MULTIPLE_ONE_PIXEL_READS; ++i )
	{
		glReadPixels(	iBackingWidth / ( i + 2 ),
					    iBackingHeight / ( i + 2 ),
						1, 
						1,
						GL_RGBA,
						GL_UNSIGNED_BYTE,
						iPixelBuffer );
	}
# else  /* !defined( SCT_IOS_CLUSTER_PIXEL_READS ) */
	for( int i = 0; i < SCT_IOS_MULTIPLE_ONE_PIXEL_READS; ++i )
	{
		glReadPixels(	( iBackingWidth - SCT_MULTIPLE_ONE_PIXEL_READS ) / 2 + i	,
					    iBackingHeight / 2,
						1, 
						1,
						GL_RGBA,
						GL_UNSIGNED_BYTE,
						iPixelBuffer );
	}	
# endif /* !defined( SCT_IOS_CLUSTER_PIXEL_READS ) */
#endif	/* !defined( SCT_IOS_MULTIPLE_ONE_PIXEL_READS ) */
}

/*!
 *
 *
 */
- (void) setLayer:(CAEAGLLayer *)layer
{	
	iLayer = layer;
}

/*!
 *
 *
 */
- (void) dealloc
{
	[self uninitialize];
			
	[super dealloc];
}

@end
