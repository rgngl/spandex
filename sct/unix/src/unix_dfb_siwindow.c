/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "sct_sicommon.h"
#include "sct_siwindow.h"

#include <EGL/egl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SCREEN_WIDTH    320
#define SCREEN_HEIGHT   480

/* ---------------------------------------------------------------------- */
/*!
 *
 */
typedef struct
{
    EGLDisplay      eglDisplay;
} SCTDirectWindowContext;

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreateContext()
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext start" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCTDirectWindowContext* context;

    context = ( SCTDirectWindowContext* )( siCommonMemoryAlloc( NULL, sizeof( SCTDirectWindowContext ) ) );
    if( context == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext memory allocation failed" );
#endif  /* defined( SCT_DEBUG ) */

        return NULL;
    }
    memset( context, 0, sizeof( SCTDirectWindowContext ) );

    context->eglDisplay = eglGetDisplay( ( EGLNativeDisplayType )( 0 ) );
    if( context->eglDisplay == EGL_NO_DISPLAY )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext getting egl display failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        siWindowDestroyContext( context );
        return NULL;
    }
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext end" );
#endif  /* defined( SCT_DEBUG ) */   
    
    return context;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyContext( void* context )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyContext" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTDirectWindowContext*  c = ( SCTDirectWindowContext* )( context );

    if( c != NULL )
    {
        siCommonMemoryFree( NULL, c );
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowIsScreenSupported( void* context, SCTScreen screen )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowIsScreenSupported" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCT_ASSERT_ALWAYS( context != NULL );

    if( screen != SCT_SCREEN_DEFAULT )
    {
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowGetScreenSize( void* context, SCTScreen screen, int* width, int* height )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetScreenSize" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTDirectWindowContext*  c   = ( SCTDirectWindowContext* )( context );
  
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );
    SCT_ASSERT_ALWAYS( width != NULL );
    SCT_ASSERT_ALWAYS( height != NULL );
    
    *width  = SCREEN_WIDTH;
    *height = SCREEN_HEIGHT;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetScreenEglDisplayId( void* context, SCTScreen screen )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetScreenEglDisplayId" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTDirectWindowContext* c = ( SCTDirectWindowContext* )( context );
  
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );    

    return 0;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetScreenOrientation( void* context, SCTScreen screen, SCTScreenOrientation orientation )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetScreenOrientation" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCT_USE_VARIABLE( context );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );        
    SCT_USE_VARIABLE( screen );    
    SCT_USE_VARIABLE( orientation );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreateWindow( void* context, SCTScreen screen, int x, int y, int width, int height, SCTColorFormat format, void* parent )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow start" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );
    SCT_ASSERT_ALWAYS( width >= 0 );
    SCT_ASSERT_ALWAYS( height >= 0 );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( parent );

    return NULL;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyWindow( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyWindow" );
#endif  /* defined( SCT_DEBUG ) */   

    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( window );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetEglWindow( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetEglWindow" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCT_USE_VARIABLE( context );
    /* Do nothing. */
    return window;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTScreen siWindowGetWindowScreen( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetWindowScreen" );
#endif  /* defined( SCT_DEBUG ) */   

    SCT_ASSERT_ALWAYS( context != NULL );    
    SCT_USE_VARIABLE( window );

    return SCT_SCREEN_DEFAULT;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowToFront( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowToFront" );
#endif  /* defined( SCT_DEBUG ) */   

    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( window );
    
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowResize( void* context, void* window, int x, int y, int width, int height )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowResize" );
#endif  /* defined( SCT_DEBUG ) */   

    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( window );
    SCT_USE_VARIABLE( x );
    SCT_USE_VARIABLE( y );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetOpacity( void* context, void* window, float opacity )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetOpacity" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( window );
    SCT_USE_VARIABLE( opacity );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetAlpha( void* context, void* window, SCTBoolean enable )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetAlpha" );
#endif  /* defined( SCT_DEBUG ) */   

    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( window );
    SCT_USE_VARIABLE( enable );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowRefresh( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowRefresh" );
#endif  /* defined( SCT_DEBUG ) */   

    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( window );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreatePixmap( void* context, int width, int height, SCTColorFormat format, void* data, int length )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap start" );
#endif  /* defined( SCT_DEBUG ) */   

    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( length );
    
    return NULL;
}

/*!
 *
 *
 */
void* siWindowCreateSharedPixmap( void* context, int width, int height, SCTColorFormat format, int usage, void* data, int length )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap start" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( usage );
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( length );    

    return NULL;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetEglPixmap( void* context, void* pixmap, unsigned int* target )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetEglPixmap" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_USE_VARIABLE( context );

    if( target != NULL )
    {
        *target = ( unsigned int )( EGL_NATIVE_PIXMAP_KHR );
    }    

    return pixmap;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowBlitPixmap( void* context, void* window, void* pixmap, int x, int y )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowBlitPixmap" );
#endif  /* defined( SCT_DEBUG ) */           
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( window );
    SCT_ASSERT_ALWAYS( pixmap );
    
    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyPixmap( void* context, void* pixmap )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyPixmap" );
#endif  /* defined( SCT_DEBUG ) */           

    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( pixmap );
}

