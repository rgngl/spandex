/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>

#include "sct_unix_message_support.h"

#if defined( SCT_DEBUG )
# define SPANDEX_DEBUG
#endif

/*!
 *
 *
 */
unsigned long spandexGetMicros()
{
    struct timespec ts;

    clock_gettime( CLOCK_REALTIME, &ts );

    return ( unsigned long )( ts.tv_sec * 1000000 + ts.tv_nsec / 1000 );  /* Microseconds */
}

/*!
 *
 *
 */
int spandexPostMessage( SCTMessage m )
{
    static mqd_t mq = ( mqd_t )( -1 );

#if defined( SPANDEX_DEBUG )
    const char* msg;
    switch( m.type )
    {
    case SCT_MESSAGE_POST_BUFFER:
        msg = "POST_BUFFER";
        break;

    case SCT_MESSAGE_GET_BUFFER:
        msg = "GET_BUFFER_ENTER";
        break;

    default:
        msg = "UNEXPECTED";
        break;
    }

    printf( "spandexPostMessage: %s %lu %f\n", msg, m.millis, m.value );
#endif  /* defined( SPANDEX_DEBUG ) */
    
    if( mq == ( mqd_t )( -1 ) )
    {
        mq = mq_open( SCT_UNIX_MESSAGE_QUEUE_NAME, O_WRONLY | O_NONBLOCK );
        if( mq == ( mqd_t )( -1 ) )
        {
            printf( "spandexPostMessage: failed to open message queue %s.\n", SCT_UNIX_MESSAGE_QUEUE_NAME );
            return -1;
        }
    }

    if( mq_send( mq, ( const char* )( &m ), sizeof( SCTMessage ), 0 ) != 0 )
    {
        printf( "spandexPostMessage: failed to post a message to message queue.\n" );
        return -1;
    }

    return 0;
}

