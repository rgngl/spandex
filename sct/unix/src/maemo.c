/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdarg.h>

/* Includes from Spandex core */
#include "sct.h"
#include "sct_sicommon.h"

#include "unix_globals.h"

#if defined( SCT_USE_LIBOSSO )
# include <libosso.h>
#endif  /* defined( SCT_USE_LIBOSSO ) */

#if defined( SCT_USE_LIBOSSO )
static osso_context_t* gOssoContext    = NULL;
#endif  /* defined( SCT_USE_LIBOSSO ) */

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonDebugPrintf( void* context, const char* fmt, ... )
{
	va_list ap;

    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    SCT_ASSERT_ALWAYS( fmt != NULL );

	va_start( ap, fmt );
	vprintf( fmt, ap );
	va_end( ap );

    printf( "\n" );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
const char* siCommonQuerySystemString( void* context, SCTSystemString name )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    switch( name )
    {
    case SCT_SYSSTRING_DEVICE_TYPE:
        return "Maemo";

    case SCT_SYSSTRING_CPU:
        return "<Unknown>";
        
    case SCT_SYSSTRING_OS:
        return "Maemo";
        
    default:
        SCT_ASSERT_ALWAYS( 0 );
        return NULL;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
size_t siCommonQueryMemoryAttribute( void* context, SCTMemoryAttribute name )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    SCT_USE_VARIABLE( name );
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( context, "SPANDEX: siCommonQueryMemoryAttribute not implemented" );
#endif  /* defined( SCT_DEBUG ) */
    
    return 0;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siCommonSampleCpuLoad( void* context )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    return NULL;    /* Not supported. */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
float siCommonQueryCpuLoad( void* context, void* sample )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    SCT_USE_VARIABLE( sample );    

    return 0.0f;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonYield( void* context )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    siUnblankScreen();
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siInitializeUnblanker( void )
{
#if defined( SCT_USE_LIBOSSO )
    printf( "SPANDEX: initializing osso.\n" );
    gOssoContext = osso_initialize( "Spandex", SCT_VERSION, 0, NULL );
    if( gOssoContext == NULL )
    {
        printf( "SPANDEX: warning, osso initialization failed.\n" );
    }
#endif /* defined( SCT_USE_LIBOSSO ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siTerminateUnblanker( void )
{
#if defined( SCT_USE_LIBOSSO )
    if( gOssoContext != NULL )
    {
        printf( "SPANDEX: deinitializing osso.\n" );
        osso_deinitialize( gOssoContext );
        gOssoContext = NULL;
    }
#endif /* defined( SCT_USE_LIBOSSO ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siUnblankScreen( void )
{
#if defined( SCT_USE_LIBOSSO )
    if ( gOssoContext )
    {
        osso_display_blanking_pause( gOssoContext );
    }
#endif /* defined( SCT_USE_LIBOSSO ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */ 
void siCommonProfileEvent( void* context, const char* event )
{
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( event );
    /* TODO */
}
 
