/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES )
# include "sct_unix_message_support.h"
# include <fcntl.h>
# include <sys/stat.h>
# include <mqueue.h>
# include <errno.h>
#endif  /* defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES ) */

#include "sct.h"
#include "sct_types.h"
#include "unix_globals.h"

#define INPUT_FILE_NAME     "benchmark.txt"
#define OUTPUT_FILE_NAME    "result.txt"

/*!
 * Global variables
 */
const char*     gInputFileName  = INPUT_FILE_NAME;
const char*     gOutputFileName = OUTPUT_FILE_NAME;
FILE*           gInputFile      = NULL;
FILE*           gOutputFile     = NULL;

/*!
 *
 */
static void showUsage( const char* binary )
{
    printf( "Spandex benchmark and test system\n"
            "Usage: %s [OPTION]... [BENCHMARK FILE]\n"
            "\n"
            "Default benchmark file: " INPUT_FILE_NAME "\n"
            "Options:\n"
            "    -h, --help      Command line options\n"
            "    -o FILE         Output file name (" OUTPUT_FILE_NAME ")\n",
            binary );
}

/*!
 *
 */
static int processArguments( int argc, char** argv )
{
    int i;

    for( i = 1; i < argc; i++ )
    {
        if( !strcmp( argv[ i ], "-h" ) || !strcmp( argv[ i ], "--help" ) )
        {
            showUsage( argv[ 0 ] );
            return 1;
        }
        else if( !strcmp( argv[ i ], "-o" ) && i < argc - 1 )
        {
            gOutputFileName = argv[ ++i ];
        }
        else if( argv[ i ][ 0 ] != '-' )
        {
            gInputFileName = argv[ i ];
        }
        else
        {
            showUsage( argv[ 0 ] );
            return 1;
        }
    }
    return 0;
}

#if defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES )
/*!
 *
 *
 */
static void drainMessageQueue( mqd_t mq )
{
    SCTMessage  m;
    
    while( 1 )
    {
        if( mq_receive( mq, ( char* )( &m ), sizeof( SCTMessage ), NULL ) != sizeof( SCTMessage ) )
        {
            if( errno == EINTR )
            {
                continue;
            }
                
            break;
        }
    }
}
#endif  /* defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES ) */

/*!
 *
 *
 */
int main( int argc, char** argv )
{
    void*           sctContext  = NULL;
    SCTCycleStatus  status;
    int             ret;

#if defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES )    
    mqd_t           mq          = ( mqd_t )( -1 );
    struct mq_attr  attr;
#endif  /* defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES ) */
    
    if( ( ret = processArguments( argc, argv ) ) )
    {
        return ret;
    }

    printf( "SPANDEX start.\n" );
    
    siInitializeUnblanker();

    printf( "SPANDEX opening input file: %s.\n", gInputFileName );
    gInputFile = fopen( gInputFileName, "r" );
    if( gInputFile == NULL )
    {
        perror( "SPANDEX failed to open input file" );
        goto END;
    }
    
    printf( "SPANDEX opening output file: %s.\n", gOutputFileName );
    gOutputFile = fopen( gOutputFileName, "w+" );
    if( gOutputFile == NULL )
    {
        perror( "SPANDEX failed to open output file" );
        goto END;
    }

#if defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES )        
    printf( "SPANDEX closing previous message queue, if any.\n" );
    mq_unlink( SCT_UNIX_MESSAGE_QUEUE_NAME );

    printf( "SPANDEX creating message queue: %s.\n", SCT_UNIX_MESSAGE_QUEUE_NAME );

    attr.mq_flags   = O_NONBLOCK;
    attr.mq_maxmsg  = 3;
    attr.mq_msgsize = sizeof( SCTMessage );
    attr.mq_curmsgs = 0;
    
    mq = mq_open( SCT_UNIX_MESSAGE_QUEUE_NAME,
                  O_CREAT | O_EXCL | O_RDWR | O_NONBLOCK,
                  S_IRWXU | S_IRWXG | S_IRWXO,
                  &attr);

    if( mq == ( mqd_t )( -1 ) )
    {
        perror( "SPANDEX failed to open message queue" );
        goto END;
    }
#endif  /* defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES ) */
    
    printf( "SPANDEX sctInitialize.\n" );
    sctContext = sctInitialize();
    
    if( sctContext == NULL )
    {
        printf( "SPANDEX sctInitialize failed.\n" );
        goto END;
    }
    
    printf( "SPANDEX benchmark cycle start.\n" );
    while( 1 ) 
    {
#if defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES )        
        drainMessageQueue( mq );
#endif  /* defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES ) */

        status = sctCycle( sctContext );
        
        if( status == SCT_CYCLE_FATAL_ERROR )
        {
            printf( "SPANDEX cycle fatal error.\n" );
            break;            
        }
        else if( status == SCT_CYCLE_FINISHED )
        {
            break;
        }
    }
    
    printf( "SPANDEX benchmark cycle end.\n" );
    
END:
    
    printf( "SPANDEX sctTerminate.\n" );
    sctTerminate( sctContext );

#if defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES )    
    if( mq != ( mqd_t )( -1 ) )
    {
        printf( "SPANDEX closing message queue.\n" );
        mq_unlink( SCT_UNIX_MESSAGE_QUEUE_NAME );
        mq = ( mqd_t )( -1 );
    }
#endif  /* defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES ) */
    
    if( gInputFile != NULL )
    {
        printf( "SPANDEX closing input file.\n" );
        fclose( gInputFile );
        gInputFile = NULL;
    }
    if( gOutputFile != NULL )
    {
        printf( "SPANDEX closing output file.\n" );
        fclose( gOutputFile );
        gOutputFile = NULL;
    }

    siTerminateUnblanker();

    printf( "SPANDEX done.\n" );
    return 0;
}

