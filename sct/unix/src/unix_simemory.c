/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sct_simemory.h"
#include "sct_types.h"
#include "sct_sicommon.h"

#include <string.h>

/*!
 *
 *
 */
int siMemoryBlockFillZ( void **buffers, const int bufferCount, const int lengthInBytes, int iterations )
{
    int i, j;

    SCT_ASSERT( buffers != NULL );
    SCT_ASSERT( bufferCount > 0 );
    SCT_ASSERT( lengthInBytes > 0 );
    SCT_ASSERT( iterations > 0 );

    for( i = iterations; i ; --i )
    {
        for( j = 0; j < bufferCount; ++j )
        {
            memset( buffers[ j ], 0, lengthInBytes );
        }
    }

    return 1;
}

/*!
 *
 *
 */
int siMemoryBlockCopy( void **destinations, void **sources, const int bufferCount, const int lengthInBytes, int iterations )
{
    int i, j;

    SCT_ASSERT( destinations != NULL );
    SCT_ASSERT( sources != NULL );
    SCT_ASSERT( bufferCount > 0 );
    SCT_ASSERT( lengthInBytes > 0 );
    SCT_ASSERT( iterations > 0 );

    for( i = 0; i < iterations; i++ )
    {
        for( j = 0; j < bufferCount; j++ )
        {
            memcpy( destinations[ j ], sources[ j ], lengthInBytes );
        }
    }

    return 1;
}
