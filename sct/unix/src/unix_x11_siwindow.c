/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "sct_sicommon.h"
#include "sct_siwindow.h"

#include <EGL/egl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

/* ---------------------------------------------------------------------- */
/*!
 *
 */
#define WINDOW_TITLE    "Spandex"

/* ---------------------------------------------------------------------- */
/*!
 *
 */
static SCTBoolean       sctiColorFormatToConfig( EGLDisplay eglDisplay, SCTColorFormat format, EGLConfig* eglConfig );
static int              sctiColorFormatToColorDepth( SCTColorFormat format );

/* ---------------------------------------------------------------------- */
/*!
 *
 */
typedef struct
{
    Display*        nativeDisplay;
    Screen*         screen;
    Window          rootWindow;
    EGLDisplay      eglDisplay;
} SCTUnixX11Context;

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreateContext()
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext start" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCTUnixX11Context*  context;

    context = ( SCTUnixX11Context* )( siCommonMemoryAlloc( NULL, sizeof( SCTUnixX11Context ) ) );
    if( context == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext memory allocation failed" );
#endif  /* defined( SCT_DEBUG ) */

        return NULL;
    }
    memset( context, 0, sizeof( SCTUnixX11Context ) );

    if( !XInitThreads() )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext XInitThreads failed" );
#endif  /* defined( SCT_DEBUG ) */

        siWindowDestroyContext( context );
        return NULL;        
    }
    
    context->nativeDisplay = XOpenDisplay( NULL );
    if( context->nativeDisplay == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext XOpenDisplay failed. Is the DISPLAY variable set correctly?" );
#endif  /* defined( SCT_DEBUG ) */
        
        siWindowDestroyContext( context );
        return NULL;
    }
    
    context->screen = XDefaultScreenOfDisplay( context->nativeDisplay );
    if( context->screen == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext getting display screen failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        siWindowDestroyContext( context );
        return NULL;
    }
  
    context->rootWindow = DefaultRootWindow( context->nativeDisplay );
    if( !context->rootWindow )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext getting default root window failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        siWindowDestroyContext( context );
        return NULL;
    }
   
    context->eglDisplay = eglGetDisplay( ( EGLNativeDisplayType )( context->nativeDisplay ) );
    if( context->eglDisplay == EGL_NO_DISPLAY )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext getting egl display failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        siWindowDestroyContext( context );
        return NULL;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateContext end" );
#endif  /* defined( SCT_DEBUG ) */   
    
    return context;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyContext( void* context )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyContext" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTUnixX11Context*  c = ( SCTUnixX11Context* )( context );

    // TODO: Need to check if windows are still used before closing display
    // in case closing the display invalidates the windows.

    if( c != NULL )
    {
        if( c->nativeDisplay != NULL )
        {
#if !defined( SCT_UNIX_X11_SKIP_XCLOSEDISPLAY )
            XCloseDisplay( c->nativeDisplay );
#endif  /* !defined( SCT_UNIX_X11_SKIP_XCLOSEDISPLAY ) */
            c->nativeDisplay = NULL;
        }
        siCommonMemoryFree( NULL, c );
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowIsScreenSupported( void* context, SCTScreen screen )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowIsScreenSupported" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCT_ASSERT_ALWAYS( context != NULL );

    if( screen != SCT_SCREEN_DEFAULT )
    {
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowGetScreenSize( void* context, SCTScreen screen, int* width, int* height )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetScreenSize" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTUnixX11Context*  c           = ( SCTUnixX11Context* )( context );
    XWindowAttributes       rootAttrs;
  
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );
    SCT_ASSERT_ALWAYS( width != NULL );
    SCT_ASSERT_ALWAYS( height != NULL );
    
    XGetWindowAttributes( c->nativeDisplay, c->rootWindow, &rootAttrs );
    
    *width  = rootAttrs.width;
    *height = rootAttrs.height;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetScreenEglDisplayId( void* context, SCTScreen screen )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetScreenEglDisplayId" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTUnixX11Context* c = ( SCTUnixX11Context* )( context );
  
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );    
 
    return c->nativeDisplay;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetScreenOrientation( void* context, SCTScreen screen, SCTScreenOrientation orientation )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetScreenOrientation" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCT_USE_VARIABLE( context );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );        
    SCT_USE_VARIABLE( screen );    
    SCT_USE_VARIABLE( orientation );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 * 
 *
 */
void* siWindowCreateWindow( void* context, SCTScreen screen, int x, int y, int width, int height, SCTColorFormat format, void* parent )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow start" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTUnixX11Context*  c                   = ( SCTUnixX11Context* )( context );
    EGLConfig               eglConfig;
    EGLint                  visualId;
    XVisualInfo*            visual;
    XVisualInfo             visualInfo;
    int                     visualCount         = 0;
    XSetWindowAttributes    winAttrs;
    Window                  parentWindow;
    Window                  window;
    XTextProperty           windowTitle;
    XWindowAttributes       rootAttrs;
    const char*             windowTitleString   = WINDOW_TITLE;
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( siWindowIsScreenSupported( context, screen ) == SCT_TRUE );
    SCT_ASSERT_ALWAYS( width >= 0 );
    SCT_ASSERT_ALWAYS( height >= 0 );

    if( width == 0 || height == 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow width/height 0 not supported" );
#endif  /* defined( SCT_DEBUG ) */   
        return NULL;
    }
    
    if( parent != NULL )
    {
        parentWindow = ( Window )( parent );
    }
    else
    {
        parentWindow = c->rootWindow;
    }

    if( sctiColorFormatToConfig( c->eglDisplay, format, &eglConfig ) == SCT_FALSE )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow color format to config failed" );
#endif  /* defined( SCT_DEBUG ) */   
        return NULL;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow getting EGL config that matches visual" );
#endif  /* defined( SCT_DEBUG ) */   
    
    if( eglGetConfigAttrib( c->eglDisplay, eglConfig, EGL_NATIVE_VISUAL_ID, &visualId ) != EGL_TRUE )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow getting EGL config for visual failed" );
#endif  /* defined( SCT_DEBUG ) */   
        return EGL_FALSE;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow getting screen and visual info" );
#endif  /* defined( SCT_DEBUG ) */   
    
    memset( &visualInfo, 0, sizeof( XVisualInfo ) );
    visualInfo.visualid     = visualId;
    visualInfo.screen       = DefaultScreen( c->nativeDisplay );
    visual                  = XGetVisualInfo( c->nativeDisplay, 
                                              VisualIDMask | VisualScreenMask, 
                                              &visualInfo, 
                                              &visualCount);
    
    if( !visualCount )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow no visual" );
#endif  /* defined( SCT_DEBUG ) */   
        return NULL;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow creating x color map" );
#endif  /* defined( SCT_DEBUG ) */   
    
    memset( &winAttrs, 0, sizeof( XSetWindowAttributes ) );
    winAttrs.background_pixel   = 0;
    winAttrs.border_pixel       = 0;
    winAttrs.colormap           = XCreateColormap( c->nativeDisplay,
                                                   parentWindow,
                                                   visual->visual,
                                                   AllocNone );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow creating x window" );
#endif  /* defined( SCT_DEBUG ) */   
    
    window                      = XCreateWindow( c->nativeDisplay, 
                                                 parentWindow, 
                                                 x, y,
                                                 width, height, 
                                                 0, 
                                                 visual->depth,
                                                 InputOutput, 
                                                 visual->visual,
                                                 CWBackPixmap | CWBorderPixel | CWColormap,
                                                 &winAttrs );
    
    if( !window ) 
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow creating x window failed" );
#endif  /* defined( SCT_DEBUG ) */           
        return NULL;
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow setting x window title" );
#endif  /* defined( SCT_DEBUG ) */   
    
    memset( &windowTitle, 0, sizeof( XTextProperty ) );
    windowTitle.value       = ( char* )( windowTitleString );
    windowTitle.encoding    = XA_STRING;
    windowTitle.format      = 8;
    windowTitle.nitems      = strlen( windowTitleString );
    XSetWMName( c->nativeDisplay, window, &windowTitle );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow map x window and flush" );
#endif  /* defined( SCT_DEBUG ) */   
    
    XMapWindow( c->nativeDisplay, window);
    XFlush( c->nativeDisplay );

    if( parent == NULL )
    {
        /* NOTE: don't try to set child windows as full screen at the
         * moment. This restriction could be lifted in the future if full screen
         * indeed works ok with child windows also. */

#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow getting x window attributes for full screen detection" );
#endif  /* defined( SCT_DEBUG ) */   
        
        /* Set window to fullscreen mode if it matches the screen size */
        XGetWindowAttributes( c->nativeDisplay, c->rootWindow, &rootAttrs );
    
        if( rootAttrs.width == width && rootAttrs.height == height )
        {
            XEvent  xev;
            Atom    wmState             = XInternAtom( c->nativeDisplay, "_NET_WM_STATE", False );
            Atom    wmStateFullscreen   = XInternAtom( c->nativeDisplay, "_NET_WM_STATE_FULLSCREEN", False );
            Atom    nonCompWindow       = XInternAtom( c->nativeDisplay, "_HILDON_NON_COMPOSITED_WINDOW", False );
            Atom    windowType          = XInternAtom( c->nativeDisplay, "_NET_WM_WINDOW_TYPE", False );
            Atom    windowTypeOverride  = XInternAtom( c->nativeDisplay, "_KDE_NET_WM_WINDOW_TYPE_OVERRIDE", False );
            int     one                 = 1;

#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow setting x window to full screen" );
#endif  /* defined( SCT_DEBUG ) */   
            
            memset( &xev, 0, sizeof( XEvent ) );
            xev.type                    = ClientMessage;
            xev.xclient.window          = window;
            xev.xclient.message_type    = wmState;
            xev.xclient.format          = 32;
            xev.xclient.data.l[ 0 ]     = 1;
            xev.xclient.data.l[ 1 ]     = wmStateFullscreen;
            xev.xclient.data.l[2]       = 0;
            XSendEvent( c->nativeDisplay, c->rootWindow, False, SubstructureNotifyMask, &xev );
           
            /* Disable composition if the window color depth matches screen color depth */
            if( XVisualIDFromVisual( rootAttrs.visual ) == visualId )
            {
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow disabling composition for full screen window" );
#endif  /* defined( SCT_DEBUG ) */   
                
                XChangeProperty( c->nativeDisplay,
                                 window,
                                 nonCompWindow,
                                 XA_INTEGER,
                                 32,
                                 PropModeReplace,
                                 ( unsigned char* )( &one ),
                                 1 );
            }
            
            XChangeProperty( c->nativeDisplay,
                             window,
                             windowType,
                             XA_ATOM,
                             32,
                             PropModeReplace,
                             ( unsigned char* )( &windowTypeOverride ),
                             1);
            
            XFlush( c->nativeDisplay );            
        }
    }

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateWindow end" );
#endif  /* defined( SCT_DEBUG ) */   
    
    return ( void* )( window );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyWindow( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyWindow" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTUnixX11Context*  c   = ( SCTUnixX11Context* )( context );
    Window                  w   = ( Window )( window );
    
    SCT_ASSERT_ALWAYS( c != NULL );
    SCT_ASSERT_ALWAYS( w );
    
    XDestroyWindow( c->nativeDisplay, w );
    XFlush( c->nativeDisplay );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetEglWindow( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetEglWindow" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCT_USE_VARIABLE( context );
    /* Do nothing. */
    return window;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTScreen siWindowGetWindowScreen( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetWindowScreen" );
#endif  /* defined( SCT_DEBUG ) */   

    SCT_ASSERT_ALWAYS( context != NULL );    
    SCT_USE_VARIABLE( window );

    return SCT_SCREEN_DEFAULT;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowToFront( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowToFront" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTUnixX11Context*  c   = ( SCTUnixX11Context* )( context );
    Window                  w   = ( Window )( window );
    
    SCT_ASSERT_ALWAYS( c != NULL );
    SCT_ASSERT_ALWAYS( w );
    
    XRaiseWindow( c->nativeDisplay, w );
    XFlush( c->nativeDisplay );
    
    return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowResize( void* context, void* window, int x, int y, int width, int height )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowResize" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTUnixX11Context*  c   = ( SCTUnixX11Context* )( context );
    Window                  w   = ( Window )( window );
    
    SCT_ASSERT_ALWAYS( c != NULL );
    SCT_ASSERT_ALWAYS( w );
    
    XMoveResizeWindow( c->nativeDisplay, w, x, y, width, height );
    XFlush( c->nativeDisplay );
    
    return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetOpacity( void* context, void* window, float opacity )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetOpacity" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( window );
    SCT_USE_VARIABLE( opacity );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowSetAlpha( void* context, void* window, SCTBoolean enable )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowSetAlpha" );
#endif  /* defined( SCT_DEBUG ) */   

    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( window );
    SCT_USE_VARIABLE( enable );

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowRefresh( void* context, void* window )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowRefresh" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTUnixX11Context*  c   = ( SCTUnixX11Context* )( context );
    
    SCT_ASSERT_ALWAYS( c != NULL );
    SCT_ASSERT_ALWAYS( window );
    
    XFlush( c->nativeDisplay );
    
    return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowCreatePixmap( void* context, int width, int height, SCTColorFormat format, void* data, int length )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap start" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCTUnixX11Context*  c       = ( SCTUnixX11Context* )( context );
    int                 depth;
    Pixmap              pixmap;

    SCT_ASSERT_ALWAYS( c != NULL );
    SCT_USE_VARIABLE( length );
    
    if( data != NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap initial pixmap data not supported" );
#endif  /* defined( SCT_DEBUG ) */   
        return NULL;
    }
    
    depth = sctiColorFormatToColorDepth( format );
    if( depth < 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap color format to color depth failed" );
#endif  /* defined( SCT_DEBUG ) */          
        return NULL;
    }
    
    pixmap = XCreatePixmap( c->nativeDisplay, c->rootWindow, width, height, depth );
    
    if( !pixmap )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap create pixmap failed" );
#endif  /* defined( SCT_DEBUG ) */           
        return NULL;
    }
    
    XFlush( c->nativeDisplay );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreatePixmap end" );
#endif  /* defined( SCT_DEBUG ) */           
    
    return ( void* )( pixmap );
}

/*!
 *
 *
 */
void* siWindowCreateSharedPixmap( void* context, int width, int height, SCTColorFormat format, int usage, void* data, int length )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap start" );
#endif  /* defined( SCT_DEBUG ) */   
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_USE_VARIABLE( width );
    SCT_USE_VARIABLE( height );
    SCT_USE_VARIABLE( format );
    SCT_USE_VARIABLE( usage );
    SCT_USE_VARIABLE( data );
    SCT_USE_VARIABLE( length );    

    // TODO
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowCreateSharedPixmap NOT IMPLEMENTED" );
#endif  /* defined( SCT_DEBUG ) */           
    
    return NULL;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siWindowGetEglPixmap( void* context, void* pixmap, unsigned int* target )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowGetEglPixmap" );
#endif  /* defined( SCT_DEBUG ) */
    
    SCT_USE_VARIABLE( context );
    
    if( target != NULL )
    {
        *target = ( unsigned int )( EGL_NATIVE_PIXMAP_KHR );
    }

    return pixmap;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTBoolean siWindowBlitPixmap( void* context, void* window, void* pixmap, int x, int y )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowBlitPixmap" );
#endif  /* defined( SCT_DEBUG ) */           
    
    SCT_ASSERT_ALWAYS( context != NULL );
    SCT_ASSERT_ALWAYS( window );
    SCT_ASSERT_ALWAYS( pixmap );
    
    // TODO
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowBlitPixmap NOT IMPLEMENTED" );
#endif  /* defined( SCT_DEBUG ) */           

    return SCT_FALSE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siWindowDestroyPixmap( void* context, void* pixmap )
{
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siWindowDestroyPixmap" );
#endif  /* defined( SCT_DEBUG ) */           
    
    SCTUnixX11Context*  c   = ( SCTUnixX11Context* )( context );
    Pixmap                  p   = ( Pixmap )( pixmap );

    SCT_ASSERT_ALWAYS( c != NULL );
    SCT_ASSERT_ALWAYS( p );
    
    XFreePixmap( c->nativeDisplay, p );
    XFlush( c->nativeDisplay );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static SCTBoolean sctiColorFormatToConfig( EGLDisplay eglDisplay, SCTColorFormat format, EGLConfig* eglConfig )
{
    const int   BUFFER_SIZE_INDEX   = 0;
    const int   FINAL_INDEX         = 2;
    
    EGLint      attribs[ FINAL_INDEX + 1 ];
    int         size;
    EGLint      numConfigs;
    
    SCT_ASSERT_ALWAYS( eglConfig != NULL);

    switch( format )
    {
    case SCT_COLOR_FORMAT_RGBA4444:
        size = 12;
        break;
        
    case SCT_COLOR_FORMAT_RGB565:   /* Intentional */
    case SCT_COLOR_FORMAT_DEFAULT:
        size = 16;
        break;
        
    case SCT_COLOR_FORMAT_RGB888: 
        size = 24;
        break;

    case SCT_COLOR_FORMAT_RGBX8888: /* Intentional */
    case SCT_COLOR_FORMAT_RGBA8888:
    case SCT_COLOR_FORMAT_RGBA8888_PRE:        
        size = 32;
        break;
        
    default:
        SCT_ASSERT_ALWAYS( 0 );
        break;
    }
    
    attribs[ BUFFER_SIZE_INDEX ]        = EGL_BUFFER_SIZE;
    attribs[ BUFFER_SIZE_INDEX + 1 ]    = size;   
    attribs[ FINAL_INDEX ]              = EGL_NONE;

    /* Initialize EGL, just in case. */
    eglInitialize( eglDisplay, NULL, NULL );   
    
    if( eglChooseConfig( eglDisplay, attribs, eglConfig, 1, &numConfigs ) == EGL_FALSE )
    {
        return SCT_FALSE;
    }
    
    return SCT_TRUE;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
static int sctiColorFormatToColorDepth( SCTColorFormat format )
{
    switch( format )
    {
    case SCT_COLOR_FORMAT_RGBA4444:
        return 12;
        
    case SCT_COLOR_FORMAT_RGB565:
        return 16;
        
    case SCT_COLOR_FORMAT_RGB888:   /* Intentional */
    case SCT_COLOR_FORMAT_RGBX8888:
        return 24;
        
    case SCT_COLOR_FORMAT_RGBA8888:
    case SCT_COLOR_FORMAT_RGBA8888_PRE:
        return 32;
        
    default:
        SCT_ASSERT_ALWAYS( 0 );
        return -1;
    }
}
