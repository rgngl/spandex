/*
 * Spandex benchmark and test framework.
 *
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
 *
 *   This framework is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, version 2.1 of the License.
 *
 *   This framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 * along with this framework; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/* */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <pthread.h>
#include <errno.h>
#include <sys/time.h>

#if defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES )
# include "sct_unix_message_support.h"
# include <fcntl.h>
# include <sys/stat.h>
# include <mqueue.h>
# include <errno.h>
#endif  /* defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES ) */

#include "unix_globals.h"

/* Includes from Spandex core */
#include "sct_sicommon.h"
#include "sct_utils.h"
#include "sct_module.h"

#if defined( INCLUDE_EGL_MODULE )
# include "sct_eglmodule.h"
#endif  /* defined( INCLUDE_EGL_MODULE ) */

#define OUTPUT_BUFFER_SIZE                      1024

SCTLog* gErrorLog                               = NULL;  
char    gOutputBuffer[ OUTPUT_BUFFER_SIZE ];
int     gOutputSize                             = 0;
void*   gSingleton                              = NULL;


/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void* siCommonGetContext( void )
{
    return ( void* )( SCT_SICOMMON_UNIX_CONTEXT );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
unsigned long siCommonGetTimerFrequency( void* context )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    return 1000;    /* Milliseconds */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
unsigned long siCommonGetTimerTick( void* context )
{
    struct timespec   ts;
    
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    clock_gettime( CLOCK_REALTIME, &ts );
    
    return ( unsigned long )( ts.tv_sec * 1000 + ts.tv_nsec / 1000000 );  /* Milliseconds */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonErrorMessage( void* context, const char* errorMessage )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    if( gOutputFile )
    {
        siCommonFlushOutput( context );
        fprintf( gOutputFile, "SPANDEX ERROR: %s\n", errorMessage );
        fflush( gOutputFile );     
    }
    
    siCommonDebugPrintf( context, "SPANDEX ERROR: %s\n", errorMessage );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonWarningMessage( void* context, const char* warningMessage )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    if( gOutputFile )
    {
        siCommonFlushOutput( context );
        fprintf( gOutputFile, "#SPANDEX WARNING: %s\n", warningMessage );
        fflush( gOutputFile );     
    }
    
    siCommonDebugPrintf( context, "SPANDEX WARNING: %s\n", warningMessage );
}


/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
SCTLog* siCommonGetErrorLog( void* context )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    return gErrorLog;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonSetErrorLog( void* context, SCTLog* log )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    gErrorLog = log;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonProgressMessage( void* context, int totalBenchmark, int currentBenchmark, int currentRepeat, const char* benchmarkName )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    siCommonDebugPrintf( context, "Benchmark %d/%d, %d: %s.", currentBenchmark, totalBenchmark, currentRepeat, benchmarkName );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
void siCommonAssert( void* context, void* exp, void* file, unsigned int line )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    if( gOutputFile )
    {
        siCommonFlushOutput( context );
        fprintf( gOutputFile, 
                 "\nSpandex assert failed.\nExpresion: ( %s )\nFile: %s\nLine: %d\n",
                 ( char* )exp, ( char* )file, ( int )line ); 
        fflush( gOutputFile );
    }
    
    siCommonDebugPrintf( context, "\nSpandex assert failed.\nExpresion: ( %s )\nFile: %s\nLine: %d\n",  ( char* )exp, ( char* )file, ( int )line ); 
    assert( 0 );
}

/* ---------------------------------------------------------------------- */
/*!
 * 
 *
 */
void* siCommonMemoryAlloc( void* context, unsigned int size )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );

    return malloc( size );
}

/* ---------------------------------------------------------------------- */
/*!
 * 
 *
 */
void* siCommonMemoryRealloc( void* context, void* data, unsigned int size )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    return realloc( data, size );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siCommonMemoryFree( void* context, void* memblock )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );

    free( memblock );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
char siCommonReadChar( void* context )
{
    char    ch;

    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    SCT_ASSERT( gInputFile != NULL );
    
    return ( fread( &ch, sizeof( char ), 1, gInputFile ) == 1 ) ? ch : SCT_END_OF_INPUT;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
int siCommonWriteChar( void* context, char aCh )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    gOutputBuffer[ gOutputSize++ ] = aCh;
    if( gOutputSize >= OUTPUT_BUFFER_SIZE )
    {
        return siCommonFlushOutput( context );
    }
    else
    {
        return 1;
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 * 
 *
 */
int siCommonFlushOutput( void* context )
{
    int   n;
    
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    SCT_ASSERT( gOutputFile != NULL );
    
    if( gOutputSize == 0 )
    {
		fflush( gOutputFile );         
        return 1;
    }
    
    n = fwrite( gOutputBuffer, gOutputSize, 1, gOutputFile );
        
#if defined( SCT_DEBUG )
    if( n != 1 )
    {
        siCommonDebugPrintf( NULL, "SPANDEX: siCommonFlushOutput failed, output size=%d, n=%d", gOutputSize, n );
    }
#endif  /* defined( SCT_DEBUG ) */   
        
    gOutputSize = 0;
    fflush( gOutputFile );
    
    return ( n == 1 );
}

/*!
 *
 *
 */
SCTModuleList* siCommonGetModules( void* context )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    /* No unix-x11 specific modules. */
    return sctGetCoreModules();   
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonSleep( void* context, unsigned long micros )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    usleep( micros );
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 *
 */
void siCommonGetTime( void* context, char* buffer, int length )
{
    time_t      ltime;
    struct tm*  today;

    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    SCT_ASSERT_ALWAYS( length >= 1 );
    
    time( &ltime );
    today = localtime( &ltime );
    if( strftime( buffer, length, "%Y-%m-%d %H:%M:%S", today ) == 0 )
    {
        buffer[ 0 ] = '\0';
    }
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void* siCommonGetSingleton( void* context )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
    return gSingleton;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
void siCommonSetSingleton( void* context, void* ptr )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );

    gSingleton = ptr;
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
SCTExtensionProc siCommonGetProcAddress( void* context, const char* procname )
{
    SCT_ASSERT( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    
#if defined( INCLUDE_EGL_MODULE )
    return ( SCTExtensionProc )( sctEglGetProcAddress( procname ) );
#else   /* defined( INCLUDE_EGL_MODULE ) */
    return NULL;
#endif  /* defined( INCLUDE_EGL_MODULE ) */
}

/* ---------------------------------------------------------------------- */
/*!
 *
 *
 */
int siCommonGetMessage( void* context, SCTMessage* message, long timeoutMillis )
{
#if defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES )
    static mqd_t    mq = ( mqd_t )( -1 );
    SCTMessage      m;
    
    SCT_USE_VARIABLE( context );
    SCT_ASSERT_ALWAYS( message != NULL );

    if( mq == ( mqd_t )( -1 ) )
    {
        mq = mq_open( SCT_UNIX_MESSAGE_QUEUE_NAME, O_RDONLY );
        if( mq == ( mqd_t )( -1 ) )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siCommonGetMessage opening message queue %s failed, errno %d", SCT_UNIX_MESSAGE_QUEUE_NAME, errno );
#endif  /* defined( SCT_DEBUG ) */   
            
            return -1;
        }
    }

    if( timeoutMillis > 0 )
    {
        struct timeval  now;
        struct timespec timeout;

        gettimeofday( &now, NULL );
        
        timeout.tv_sec  = now.tv_sec + ( timeoutMillis / 1000 );
        timeout.tv_nsec = ( ( now.tv_usec + ( timeoutMillis % 1000 ) * 1000 ) * 1000 ) % 1000000000;    

        while( 1 )
        {
            if( mq_timedreceive( mq, ( char* )( &m ), sizeof( SCTMessage ), NULL, &timeout ) != sizeof( SCTMessage ) )
            {
                if( errno == EINTR )
                {
                    continue;
                }
                else if( errno == ETIMEDOUT )
                {
                    return 0;
                }
                
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: siCommonGetMessage timed receive from message queue %s failed, errno %d", SCT_UNIX_MESSAGE_QUEUE_NAME, errno );
#endif  /* defined( SCT_DEBUG ) */
                
                return -1;
            }
            else
            {
                break;
            }
        }
    }
    else
    {
        while( 1 )
        {
            if( mq_receive( mq, ( char* )( &m ), sizeof( SCTMessage ), NULL ) != sizeof( SCTMessage ) )
            {
                if( errno == EINTR )
                {
                    continue;
                }
                
#if defined( SCT_DEBUG )
                siCommonDebugPrintf( NULL, "SPANDEX: siCommonGetMessage receive from message queue %s failed, errno %d", SCT_UNIX_MESSAGE_QUEUE_NAME, errno );
#endif  /* defined( SCT_DEBUG ) */   

                return -1;
            }
            else
            {
                break;
            }
        }
    }

    *message = m;
    return 1;
    
#else   /* defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES ) */

    SCT_USE_VARIABLE( context );
    SCT_ASSERT_ALWAYS( message != NULL );
    SCT_USE_VARIABLE( timeoutMillis );
    
    return -1;

#endif  /* defined( SCT_SICOMMON_UNIX_SUPPORT_MESSAGES ) */    
}

/*!
 *
 *
 */ 
typedef struct
{
    pthread_t   thread;
} SCTPthread;

typedef void* ( *SCTPThreadsFunc )( void * );

typedef struct
{
    pthread_mutex_t mutex;
} SCTPthreadMutex;

/*!
 *
 *
 */ 
typedef struct
{
    pthread_mutex_t mutex;    
    pthread_cond_t  condition;
    volatile int    signaled;
    volatile int    enabled;
} SCTPthreadSignal;

/*!
 *
 *
 */
void* siCommonCreateThread( void* context, SCTBenchmarkWorkerThreadFunc func, SCTBenchmarkWorkerThreadContext* threadContext )
{
    SCTPthread* thread;
    int         r;
    
    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );    
    SCT_ASSERT_ALWAYS( func != NULL );
    SCT_ASSERT_ALWAYS( threadContext != NULL );

    thread = ( SCTPthread* )( siCommonMemoryAlloc( context, sizeof( SCTPthread ) ) );
    if( thread == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( context, "SPANDEX: siCommonCreateThread allocation failed" );
#endif  /* defined( SCT_DEBUG ) */
        return NULL;
    }         
    memset( thread, 0, sizeof( SCTPthread ) );
    
    r = pthread_create( &( thread->thread ), NULL, ( SCTPThreadsFunc )( func ), threadContext );

    if( r != 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( context, "SPANDEX: siCommonCreateThread create thread failed" );
#endif  /* defined( SCT_DEBUG ) */
        siCommonMemoryFree( context, thread );
        return NULL;
    }
    
    return thread;
}

/*!
 *
 *
 */
void siCommonThreadCleanup( void* context )
{
    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );    
    /* Do nothing. */
}

/*!
 *
 *
 */
void siCommonJoinThread( void* context, void* thread )
{
    SCTPthread* t;
   
    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );    
    SCT_ASSERT_ALWAYS( thread != NULL );

    t = ( SCTPthread* )( thread );
    
    pthread_join( t->thread, NULL );
    siCommonMemoryFree( context, t );
}

/*!
 *
 *
 */
SCTBoolean siCommonSetThreadPriority( void* context, SCTThreadPriority priority )
{
    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );
    SCT_USE_VARIABLE( context );
    SCT_USE_VARIABLE( priority );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( context, "SPANDEX: siCommonSetThreadPriority not supported" );
#endif  /* defined( SCT_DEBUG ) */

    return SCT_FALSE;
}

/*!
 *
 *
 */
void* siCommonCreateMutex( void* context )
{
    SCTPthreadMutex*    mutex;

    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );    

    mutex = ( SCTPthreadMutex* )( siCommonMemoryAlloc( context, sizeof( SCTPthreadMutex ) ) );
    if( mutex == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( context, "SPANDEX: siCommonCreateMutex allocation failed" );
#endif  /* defined( SCT_DEBUG ) */
        return NULL;
    }

    if( pthread_mutex_init( &( mutex->mutex ), NULL ) != 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( context, "SPANDEX: siCommonCreateMutex mutex init failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        siCommonDestroyMutex( context, mutex );
        return NULL;
    }
    
    return mutex;
}

/*!
 *
 *
 */
void siCommonLockMutex( void* context, void* mutex )
{
    SCTPthreadMutex*    m;

    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );    
    SCT_ASSERT_ALWAYS( mutex != NULL );

    m = ( SCTPthreadMutex* )( mutex );

    pthread_mutex_lock( &( m->mutex ) );
}

/*!
 *
 *
 */
void siCommonUnlockMutex( void* context, void* mutex )
{
    SCTPthreadMutex*    m;

    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );    
    SCT_ASSERT_ALWAYS( mutex != NULL );

    m = ( SCTPthreadMutex* )( mutex );

    pthread_mutex_unlock( &( m->mutex ) );
}
        
/*!
 *
 *
 */
void siCommonDestroyMutex( void* context, void* mutex )
{
    SCTPthreadMutex*    m;
    
    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );    
    SCT_ASSERT_ALWAYS( mutex != NULL );

    m = ( SCTPthreadMutex* )( mutex );
    
    pthread_mutex_destroy( &( m->mutex ) );
    siCommonMemoryFree( context, m );
}

/*!
 *
 *
 */
void* siCommonCreateSignal( void* context )
{
    SCTPthreadSignal*   signal;

    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );    
    
    signal = ( SCTPthreadSignal* )( siCommonMemoryAlloc( NULL, sizeof( SCTPthreadSignal ) ) );
    if( signal == NULL )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( context, "SPANDEX: siCommonCreateSignal allocation failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        return NULL;
    }

    memset( signal, 0, sizeof( SCTPthreadSignal ) );

    if( pthread_mutex_init( &( signal->mutex ), NULL ) != 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( context, "SPANDEX: siCommonCreateSignal mutex init failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        siCommonDestroySignal( context, signal );
        return NULL; 
    }

    if( pthread_cond_init( &( signal->condition ), NULL ) != 0 )
    {
#if defined( SCT_DEBUG )
        siCommonDebugPrintf( context, "SPANDEX: siCommonCreateSignal condition init failed" );
#endif  /* defined( SCT_DEBUG ) */
        
        siCommonDestroySignal( context, signal );
        return NULL; 
    }

    signal->signaled = 0;
    signal->enabled  = 1;
    
    return signal;
}

/*!
 *
 *
 */
void siCommonDestroySignal( void* context, void* signal )
{
    SCTPthreadSignal*   s;

    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );        
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTPthreadSignal* )( signal );

    pthread_cond_destroy( &( s->condition ) );
    pthread_mutex_destroy( &( s->mutex ) );    

    siCommonMemoryFree( NULL, signal );
}

/*!
 *
 *
 */
int siCommonWaitForSignal( void* context, void* signal, long timeoutMillis )
{
    SCTPthreadSignal*   s;
    struct timeval      now;
    struct timespec     timeout;
    int                 r;
    int                 status;
    int                 enabled;

    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );     
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTPthreadSignal* )( signal );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonWaitForSignal %x", signal );
#endif  /* defined( SCT_DEBUG ) */

    /* First, lock signal mutex. */    
    pthread_mutex_lock( &( s->mutex ) );
  
    /* Check if the signal is enabled. */
    if( s->enabled == 0 )
    {
        pthread_mutex_unlock( &( s->mutex ) );
        return -1;
    }

    r = 0;

    if( timeoutMillis > 0 )
    {
        gettimeofday( &now, NULL );
        timeout.tv_sec  = now.tv_sec + ( timeoutMillis / 1000 );
        timeout.tv_nsec = ( ( now.tv_usec + ( timeoutMillis % 1000 ) * 1000 ) * 1000 ) % 1000000000;    

        while( ( s->signaled == 0 ) && ( s->enabled > 0 ) && r != ETIMEDOUT )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siCommonWaitForSignal %x going into timed wait", signal );
#endif  /* defined( SCT_DEBUG ) */
            
            r = pthread_cond_timedwait( &( s->condition ), &( s->mutex ), &timeout);
        }
    }
    else
    {
        while( ( s->signaled == 0 ) && ( s->enabled > 0 )  )
        {
#if defined( SCT_DEBUG )
            siCommonDebugPrintf( NULL, "SPANDEX: siCommonWaitForSignal %x going into wait", signal );
#endif  /* defined( SCT_DEBUG ) */
            
            r = pthread_cond_wait( &( s->condition ), &( s->mutex ) );
        }
    }

    if( r == ETIMEDOUT )
    {
        status = 0;
    }
    else
    {   
        status = 1;
    }

    enabled     = s->enabled;
    s->signaled = 0;
    
    pthread_mutex_unlock( &( s->mutex ) );

    if( enabled == 0 )
    {
        return -1;
    }
    
    return status;
}

/*!
 *
 *
 */
void siCommonTriggerSignal( void* context, void* signal )
{
    SCTPthreadSignal*   s;

    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );        
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTPthreadSignal* )( signal );
    
#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonTriggerSignal %x", signal );
#endif  /* defined( SCT_DEBUG ) */

    /* Lock signal mutex. */    
    pthread_mutex_lock( &( s->mutex ) );
    
    if( s->enabled != 0 )
    {
        s->signaled = 1;
        pthread_cond_signal( &( s->condition ) );
    }

    /* Release signal mutex. */
    pthread_mutex_unlock( &( s->mutex ) );   
}

/*!
 *
 *
 */
void siCommonCancelSignal( void* context, void* signal )
{
    SCTPthreadSignal*   s;

    SCT_ASSERT_ALWAYS( context == NULL || context == ( void* )( SCT_SICOMMON_UNIX_CONTEXT ) );        
    SCT_ASSERT_ALWAYS( signal != NULL );

    s = ( SCTPthreadSignal* )( signal );

#if defined( SCT_DEBUG )
    siCommonDebugPrintf( NULL, "SPANDEX: siCommonCancelSignal %x", signal );
#endif  /* defined( SCT_DEBUG ) */

    /* Lock signal mutex. */    
    pthread_mutex_lock( &( s->mutex ) );
    
    /* Set signal as disabled. Increase signal counter so that waiting threads
     * know the wakeup is for real. */
    s->enabled  = 0;
    s->signaled = 1;

    pthread_cond_broadcast( &( s->condition ) );
    
    /* Release signal mutex. */
    pthread_mutex_unlock( &( s->mutex ) );    
} 

