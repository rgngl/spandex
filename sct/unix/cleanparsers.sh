#!/bin/sh

# Copyright (c) 2011 Linaro Limited
#
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#
# Author: Alexandros Frantzis <alexandros.frantzis@linaro.org>

# Script to clean up files produced by ./mkparsers.sh

SPANDEX_HOME=../..

DIRS=$(find ../common/modules/* -maxdepth 1 -type d)

for dir in $DIRS;
do
	module=$(basename $dir)
	rm ../common/modules/$module/sct_${module}module_parser.* 2> /dev/null
	rm ../common/modules/$module/sct_${module}module_actions.h 2> /dev/null
done




