#!/bin/bash

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

SPANDEX_HOME=../..
PYTHON=python

$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/cpu/config.py
$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/memory/config.py
$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/opengles1/config.py
$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/gltest/config.py
$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/opengles2/config.py
$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/egl/config.py
$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/openvg/config.py
$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/vgu/config.py
$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/vgtest/config.py
$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/images/config.py
$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/thread/config.py
$PYTHON $SPANDEX_HOME/python/mkactionparser.py ../common/modules/test/config.py


