#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

How to build Spandex
--------------------

The following make targets are available for building Spandex. In the
instructions $PLATFORM refers to the unix-like platform you are targeting
(e.g., linux).

Spandex with OpenGL ES 1.1 support:

	$ make -f Makefile.$PLATFORM spandex_gl

Spandex with OpenGL ES 2.0 support:

	$ make -f Makefile.$PLATFORM spandex_gl2

Spandex with OpenVG support:

	$ make -f Makefile.$PLATFORM spandex_vg

Spandex with OpenGL ES 2.0 and OpenVG support:

	$ make -f Makefile.$PLATFORM spandex_gl2_vg

Spandex with all of the above:

	$ make -f Makefile.$PLATFORM all

Contact kari.j.kangas@nokia.com for more information.
