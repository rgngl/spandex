# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#          Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys
sys.path.append( '../../../python' )
sys.path.append( '../../lib' )
sys.path.append( '../../lib/testcases/opengles1' )
sys.path.append( '../../lib/targets' )

import vt

suite, command = vt.init()

import info
suite.addTestCase( info.Info )
suite.addTestCase( info.EglInfo, 'EGL_LIMITED' )

# ----------------------------------------------------------------------
# Legacy visual tests
# ----------------------------------------------------------------------

import op_vertex_arrays
suite.addTestCase( op_vertex_arrays.Triangle )
suite.addTestCase( op_vertex_arrays.TriangleFan )
suite.addTestCase( op_vertex_arrays.TriangleStrip )
suite.addTestCase( op_vertex_arrays.Lines )
suite.addTestCase( op_vertex_arrays.Points )
suite.addTestCase( op_vertex_arrays.DrawArrays )
suite.addTestCase( op_vertex_arrays.ColorPointer )
suite.addTestCase( op_vertex_arrays.NormalPointer )
suite.addTestCase( op_vertex_arrays.TexCoordPointer )

import op_buffer_objects
suite.addTestCase( op_buffer_objects.BufferObjects )

import op_coordinate_transform
suite.addTestCase( op_coordinate_transform.DepthRange )
suite.addTestCase( op_coordinate_transform.ViewPort )
suite.addTestCase( op_coordinate_transform.LoadMatrix )
suite.addTestCase( op_coordinate_transform.MultMatrix )
suite.addTestCase( op_coordinate_transform.Rotate )
suite.addTestCase( op_coordinate_transform.Scale )
suite.addTestCase( op_coordinate_transform.Translate )
suite.addTestCase( op_coordinate_transform.Frustum )
suite.addTestCase( op_coordinate_transform.Ortho )
suite.addTestCase( op_coordinate_transform.PushPop )

import op_clipping
suite.addTestCase( op_clipping.ClipPlane )
suite.addTestCase( op_clipping.ClipPlaneRectangle )

import op_coloring
suite.addTestCase( op_coloring.FrontFace )
suite.addTestCase( op_coloring.Light )
suite.addTestCase( op_coloring.ShadeModel )
suite.addTestCase( op_coloring.ColorMaterial )

import frag_op_fragment
suite.addTestCase( frag_op_fragment.Scissor )
suite.addTestCase( frag_op_fragment.SampleCoverage )
suite.addTestCase( frag_op_fragment.StencilFunc )
suite.addTestCase( frag_op_fragment.StencilOp )
suite.addTestCase( frag_op_fragment.BlendFunc )
suite.addTestCase( frag_op_fragment.Dither )
suite.addTestCase( frag_op_fragment.LogicOp )
suite.addTestCase( frag_op_fragment.AlphaFunc )

import frag_op_framebuffer
suite.addTestCase( frag_op_framebuffer.ColorMask )
suite.addTestCase( frag_op_framebuffer.DepthMask )
suite.addTestCase( frag_op_framebuffer.Clear )
suite.addTestCase( frag_op_framebuffer.ClearDepth )

import raster_fog
suite.addTestCase( raster_fog.Fog )

import raster_polygons
suite.addTestCase( raster_polygons.CullFace )
suite.addTestCase( raster_polygons.PolygonOffset )

import raster_texturing
suite.addTestCase( raster_texturing.TextureFiltering )
suite.addTestCase( raster_texturing.SubTexture )
suite.addTestCase( raster_texturing.MultiTexturing )
suite.addTestCase( raster_texturing.TextureFormats )
suite.addTestCase( raster_texturing.TextureWrapping )
suite.addTestCase( raster_texturing.CopyTexImage2D )
suite.addTestCase( raster_texturing.CopyTexSubImage2D )
suite.addTestCase( raster_texturing.TexEnvModeAndColor )
suite.addTestCase( raster_texturing.TexEnvCombineRGB )
suite.addTestCase( raster_texturing.TexEnvCombineAlpha )
suite.addTestCase( raster_texturing.GradientTexture )
suite.addTestCase( raster_texturing.SolidTexture )
suite.addTestCase( raster_texturing.BarTexture )

import spec_func_hints
suite.addTestCase( spec_func_hints.Perspective )

import AllpairsOperations
suite.addTestCase( AllpairsOperations.BufferObjects )
suite.addTestCase( AllpairsOperations.BufferObjectsVertex )
suite.addTestCase( AllpairsOperations.BufferObjectsTextCoord )
suite.addTestCase( AllpairsOperations.BufferObjectsNormal )
suite.addTestCase( AllpairsOperations.BufferObjectsPoints )
suite.addTestCase( AllpairsOperations.BufferObjectsMatrixModeTexture )

import AllpairsRasterization
suite.addTestCase( AllpairsRasterization.PointsSizeBuffer )
suite.addTestCase( AllpairsRasterization.PointsSizeRasterization )
suite.addTestCase( AllpairsRasterization.PointParameter )
suite.addTestCase( AllpairsRasterization.PointSprites )
suite.addTestCase( AllpairsRasterization.PointSprites2 )
suite.addTestCase( AllpairsRasterization.PointSpritesGradient )

# ----------------------------------------------------------------------
# New visual tests
# ----------------------------------------------------------------------

import clear
suite.addTestCase( clear.ClearColor )
suite.addTestCase( clear.ClearColorWithMask )

import texturing
suite.addTestCase( texturing.TextureWrap )
suite.addTestCase( texturing.NpotTextureWrapExt )
suite.addTestCase( texturing.TextureMinFilter )
suite.addTestCase( texturing.NpotTextureMinFilterExt )
suite.addTestCase( texturing.TextureMagFilter )
suite.addTestCase( texturing.NpotTextureMagFilterExt )
suite.addTestCase( texturing.TextureGenerateMipmap )
suite.addTestCase( texturing.NpotTextureGenerateMipmap )
suite.addTestCase( texturing.TextureSubImage )
suite.addTestCase( texturing.CopyTexImage, 'EGL_FULL' )                              # Requires EGL
suite.addTestCase( texturing.CopyTexSubImage, 'EGL_FULL' )                           # Requires EGL
suite.addTestCase( texturing.GenerateMipmaps )
suite.addTestCase( texturing.NpotGenerateMipmaps )

import palette
suite.addTestCase( palette.PaletteTextureWrap )
suite.addTestCase( palette.NpotPaletteTextureWrapExt )
suite.addTestCase( palette.PaletteTextureMinFilter )
suite.addTestCase( palette.NpotPaletteTextureMinFilterExt )

import etc
suite.addTestCase( etc.EtcTextureWrap )
suite.addTestCase( etc.EtcTextureMinFilter )
suite.addTestCase( etc.NonSquareEtcTexture )
suite.addTestCase( etc.NpotEtcTexture )
suite.addTestCase( etc.NonSquareNpotEtcTexture )

import pvrtc
suite.addTestCase( pvrtc.PvrtcTextureWrap )
suite.addTestCase( pvrtc.PvrtcTextureMinFilter )

import vertices
suite.addTestCase( vertices.VertexType )
suite.addTestCase( vertices.VertexPointerOffset )
suite.addTestCase( vertices.VertexBuffer )
suite.addTestCase( vertices.VertexBufferOffset )
suite.addTestCase( vertices.InterleavedArrays )
suite.addTestCase( vertices.InterleavedBufferArrays )
suite.addTestCase( vertices.VertexBufferUsage )
suite.addTestCase( vertices.VertexBufferSubdata )

import elements
suite.addTestCase( elements.Points )
suite.addTestCase( elements.Lines )
suite.addTestCase( elements.LineStrips )
suite.addTestCase( elements.LineLoops )
suite.addTestCase( elements.TriangleFans )
suite.addTestCase( elements.TriangleStrip )
suite.addTestCase( elements.Triangles )

import pixels
suite.addTestCase( pixels.SmoothPixels )
suite.addTestCase( pixels.FlatTexels )
suite.addTestCase( pixels.SmoothTexels )
suite.addTestCase( pixels.FlatMultitexels )
suite.addTestCase( pixels.SmoothMultitexels )

import viewport
suite.addTestCase( viewport.TrianglesViewportCulling )
suite.addTestCase( viewport.PointsViewportCulling )
suite.addTestCase( viewport.LinesViewportCulling )
suite.addTestCase( viewport.LineStripsViewportCulling )
suite.addTestCase( viewport.LineLoopsViewportCulling )
suite.addTestCase( viewport.TriangleFansViewportCulling )

import depth
suite.addTestCase( depth.DepthFunc )
suite.addTestCase( depth.DepthFuncWithEqualDepth )
suite.addTestCase( depth.DepthMask )
suite.addTestCase( depth.ClearDepth )
suite.addTestCase( depth.DepthRange )
suite.addTestCase( depth.PolygonOffset )
suite.addTestCase( depth.PolygonOffsetUnit )

import scissor
suite.addTestCase( scissor.ClearWithScissor )
suite.addTestCase( scissor.Scissor )
suite.addTestCase( scissor.ScissorAndViewport )
suite.addTestCase( scissor.TrianglesScissorCulling )
suite.addTestCase( scissor.PointsScissorCulling )
suite.addTestCase( scissor.LinesScissorCulling )
suite.addTestCase( scissor.LineStripsScissorCulling )
suite.addTestCase( scissor.LineLoopsScissorCulling )
suite.addTestCase( scissor.TriangleFansScissorCulling )

import stencil
suite.addTestCase( stencil.BasicStencil )
suite.addTestCase( stencil.StencilFunc )
suite.addTestCase( stencil.StencilOpMask )
suite.addTestCase( stencil.StencilMask )
suite.addTestCase( stencil.StencilDpassOpWithNoDepthTest )

import blending
suite.addTestCase( blending.Blending )

import extensions
suite.addTestCase( extensions.Extensions )

# ----------------------------------------------------------------------
# EGL image and surfaces: REQUIRES EGL
# TODO: test EGL image orphaning, egl image mipmap level.

import egl
suite.addTestCase( egl.EglImageAsTextureExt, 'EGL_LIMITED' )                         # Requires EGL
suite.addTestCase( egl.NpotEglImageAsTextureExt, 'EGL_LIMITED' )                     # Requires EGL
suite.addTestCase( egl.EglImageFromTextureExt, 'EGL_LIMITED' )                       # Requires EGL
suite.addTestCase( egl.EglImageFromNpotTextureExt, 'EGL_LIMITED' )                   # Requires EGL
suite.addTestCase( egl.PixmapSurfaceAsTextureExt, 'EGL_LIMITED' )                    # Requires EGL
suite.addTestCase( egl.NpotPixmapSurfaceAsTextureExt, 'EGL_LIMITED' )                # Requires EGL

suite.addTestCase( egl.PbufferAsTexture, 'EGL_LIMITED' )                             # Requires EGL
suite.addTestCase( egl.NpotPbufferAsTextureExt, 'EGL_LIMITED' )                      # Requires EGL
suite.addTestCase( egl.PbufferWithMipmapsAsTexture, 'EGL_LIMITED' )                  # Requires EGL
suite.addTestCase( egl.NpotPbufferWithMipmapsAsTextureExt, 'EGL_LIMITED' )           # Requires EGL
suite.addTestCase( egl.GenerateMipmapsForPbufferAsTexture, 'EGL_LIMITED' )           # Requires EGL
suite.addTestCase( egl.GenerateMipmapsForNpotPbufferAsTextureExt, 'EGL_LIMITED' )    # Requires EGL

suite.addTestCase( egl.WindowSurface, 'EGL_FULL' )                                   # Requires EGL
suite.addTestCase( egl.PbufferSurface, 'EGL_FULL' )                                  # Requires EGL
suite.addTestCase( egl.PixmapSurface, 'EGL_FULL' )                                   # Requires EGL
suite.addTestCase( egl.SharedPixmapSurface, 'EGL_FULL' )                             # Requires EGL

suite.addTestCase( egl.Extensions, 'EGL_LIMITED' )                                   # Requires EGL

# ----------------------------------------------------------------------
command()
