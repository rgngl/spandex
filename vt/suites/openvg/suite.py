#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>,
#          Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import sys
sys.path.append( '../../../python' )
sys.path.append( '../../lib' )
sys.path.append( '../../lib/testcases/openvg' )
sys.path.append( '../../lib/targets' )

import vt

suite, command = vt.init()

import info
suite.addTestCase( info.Info )
suite.addTestCase( info.EglInfo, 'EGL_LIMITED' )

# ----------------------------------------------------------------------
# OpenVG 1.0

import context_blendmodes
suite.addTestCase( context_blendmodes.TransparentColor )
suite.addTestCase( context_blendmodes.TransparentColors )
suite.addTestCase( context_blendmodes.VaryingColors )

import context_fillrules
suite.addTestCase( context_fillrules.FillRuleCCW )
suite.addTestCase( context_fillrules.FillRuleCW )

import context_matrices
suite.addTestCase( context_matrices.PathUserToSurfaceRotateStroke )
suite.addTestCase( context_matrices.PathUserToSurfaceTranslateAndRotateStroke )
suite.addTestCase( context_matrices.PathUserToSurfaceRotateFill )
suite.addTestCase( context_matrices.PathUserToSurfaceTranslateAndRotateFill )

import context_pixellayouts
suite.addTestCase( context_pixellayouts.PixelLayout )

import context_scissors
suite.addTestCase( context_scissors.Rectangles )
suite.addTestCase( context_scissors.OnOff )

import context_strokestyles
suite.addTestCase( context_strokestyles.EndCapStyles )
suite.addTestCase( context_strokestyles.JoinStyles )
suite.addTestCase( context_strokestyles.CapAndJoinStylesInPathClosing )
suite.addTestCase( context_strokestyles.EndCapStylesInSeparateSegments )

import image_drawing
suite.addTestCase( image_drawing.DrawingModes )
suite.addTestCase( image_drawing.MultiplyDrawingMode )
suite.addTestCase( image_drawing.StencilDrawingMode )

import image_filters
suite.addTestCase( image_filters.ColorMatrix )
suite.addTestCase( image_filters.ConvolutionConvolve )
suite.addTestCase( image_filters.ConvolutionGaussianBlur )
suite.addTestCase( image_filters.ConvolutionSeparableConvolve )
suite.addTestCase( image_filters.GroupOpacity )
suite.addTestCase( image_filters.Lookup )
suite.addTestCase( image_filters.LookupSingle )

import image_formats
suite.addTestCase( image_formats.Formats )

import image_matrices
suite.addTestCase( image_matrices.ImageUserToSurfaceTranslateRotateShearScale )

import image_pixels
suite.addTestCase( image_pixels.GetPixels )
suite.addTestCase( image_pixels.SetPixels )
suite.addTestCase( image_pixels.ReadPixels )
suite.addTestCase( image_pixels.WritePixels )

import paint_types
suite.addTestCase( paint_types.ColorInStrokes )
suite.addTestCase( paint_types.ColorInFills )
suite.addTestCase( paint_types.LinearGradientInStrokes )
suite.addTestCase( paint_types.LinearGradientInFills )
suite.addTestCase( paint_types.RadialGradientInStrokes )
suite.addTestCase( paint_types.RadialGradientInFills )
suite.addTestCase( paint_types.PatternInStrokes )
suite.addTestCase( paint_types.PatternInFills )
suite.addTestCase( paint_types.Gradient )

import path_segments
suite.addTestCase( path_segments.Arc )
suite.addTestCase( path_segments.ArcDashing )
suite.addTestCase( path_segments.LineAbsoluteInsideSurface )
suite.addTestCase( path_segments.LineRelativeInsideSurface )
suite.addTestCase( path_segments.LineRelativeOutsideSurface )
suite.addTestCase( path_segments.Line )
suite.addTestCase( path_segments.LineDashing )
suite.addTestCase( path_segments.Cubic )
suite.addTestCase( path_segments.CubicDashing )
suite.addTestCase( path_segments.Quad )
suite.addTestCase( path_segments.QuadDashing )
suite.addTestCase( path_segments.Scubic )
suite.addTestCase( path_segments.ScubicDashing )
suite.addTestCase( path_segments.Squad )
suite.addTestCase( path_segments.SquadDashing )
suite.addTestCase( path_segments.DiscreteSQuadBeziers )
suite.addTestCase( path_segments.ConnectedSQuadBeziers )
suite.addTestCase( path_segments.DiscreteSCubicBeziers )
suite.addTestCase( path_segments.ConnectedSCubicBeziers )
suite.addTestCase( path_segments.DiscreteQuadraticBeziers )
suite.addTestCase( path_segments.ConnectedQuadraticBeziers )

import tiger
suite.addTestCase( tiger.Tiger )

import AllpairsPaint
suite.addTestCase( AllpairsPaint.AllpairsPaintStrokedLines )
suite.addTestCase( AllpairsPaint.AllpairsPaintStrokedLines2 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFilledLines )
suite.addTestCase( AllpairsPaint.AllpairsPaintFilledLines2 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFilledQuadraticBeziers )
suite.addTestCase( AllpairsPaint.AllpairsPaintFilledQuadraticBeziers2 )
suite.addTestCase( AllpairsPaint.AllpairsPaintStrokedQuadraticBeziers )
suite.addTestCase( AllpairsPaint.AllpairsPaintStrokedQuadraticBeziers2 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFilledArcs )
suite.addTestCase( AllpairsPaint.AllpairsPaintStrokedArcs )
suite.addTestCase( AllpairsPaint.AllpairsPaintFilledScubicBeziers )
suite.addTestCase( AllpairsPaint.AllpairsPaintStrokedScubicBeziers )
suite.addTestCase( AllpairsPaint.AllpairsPaintStrokedSquadBeziers )
suite.addTestCase( AllpairsPaint.AllpairsPaintFilledSquadBeziers )
suite.addTestCase( AllpairsPaint.AllpairsPaintFunctions1 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFunctions2 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFunctions3 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFunctions4 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFunctions5 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFunctions6 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFunctions7 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFunctions8 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFunctions9 )
suite.addTestCase( AllpairsPaint.AllpairsPaintFunctions10 )

import AllpairsPath
suite.addTestCase( AllpairsPath.AllpairsPathHandlingFilledLines )
suite.addTestCase( AllpairsPath.AllpairsPathHandlingStrokedLines )
suite.addTestCase( AllpairsPath.AllpairsPathHandlingInterpolation )
suite.addTestCase( AllpairsPath.AllpairsPathHandlingInterpolation2 )

import AllpairsRendQ
suite.addTestCase( AllpairsRendQ.AllpairsRenQHandleImage )
suite.addTestCase( AllpairsRendQ.AllpairsRenQHandleFillPath )

import AllpairsScisMaskClr
suite.addTestCase( AllpairsScisMaskClr.AllpairsMasking )
suite.addTestCase( AllpairsScisMaskClr.AllpairsScissoring )

import AllpairsImageHandling
suite.addTestCase( AllpairsImageHandling.ImageHandling1 )
suite.addTestCase( AllpairsImageHandling.ImageHandling2 )

import vgu_functions
suite.addTestCase( vgu_functions.Line, 'VGU' )
suite.addTestCase( vgu_functions.DashedLine, 'VGU' )
suite.addTestCase( vgu_functions.Polygon, 'VGU' )
suite.addTestCase( vgu_functions.Rect, 'VGU' )
suite.addTestCase( vgu_functions.RoundRect, 'VGU' )
suite.addTestCase( vgu_functions.Ellipse, 'VGU' )
suite.addTestCase( vgu_functions.Arc, 'VGU' )
suite.addTestCase( vgu_functions.ComputeWarpQuadToSquare, 'VGU' )
suite.addTestCase( vgu_functions.ComputeWarpSquareToQuad, 'VGU' )
suite.addTestCase( vgu_functions.ComputeWarpQuadToQuad, 'VGU' )

# ----------------------------------------------------------------------
# OpenVG 1.1

import path_fonts
suite.addTestCase( path_fonts.DrawGlyphsStroked )
suite.addTestCase( path_fonts.DrawGlyphsFilled )
suite.addTestCase( path_fonts.DrawGlyphsStrokedOverlapping )
suite.addTestCase( path_fonts.DrawGlyphsFilledOverlapping )
suite.addTestCase( path_fonts.DrawSeveralGlyphsOverlapping )
suite.addTestCase( path_fonts.SetGlyphToPathOrig )
suite.addTestCase( path_fonts.SetGlyphToPathEscapement )
suite.addTestCase( path_fonts.DrawGlyphsFiguresSeveralGlyphs )
suite.addTestCase( path_fonts.DrawGlyphsFiguresSingleGlyph )
suite.addTestCase( path_fonts.MatrixGlyphUserToSurfaceRotate )
suite.addTestCase( path_fonts.MatrixGlyphUserToSurfaceShear )
suite.addTestCase( path_fonts.MatrixGlyphUserToSurfaceScale )
suite.addTestCase( path_fonts.MatrixGlyphUserToSurfaceMultMatrix )
suite.addTestCase( path_fonts.MatrixGlyphUserToSurfaceLoadMatrix )

import image_fonts
suite.addTestCase( image_fonts.SetGlyphToImage )
suite.addTestCase( image_fonts.SetGlyphToImageOverlapping )
suite.addTestCase( image_fonts.SetGlyphToImageOrig )
suite.addTestCase( image_fonts.SetGlyphToImageEscapement )

import mask_functions
suite.addTestCase( mask_functions.MaskingImage )
suite.addTestCase( mask_functions.MaskingRenderToMask )
suite.addTestCase( mask_functions.MaskingFillMaskLayer )
suite.addTestCase( mask_functions.MaskingCopyMask )

import extensions
suite.addTestCase( extensions.Extensions )

# ----------------------------------------------------------------------
# EGL image and surfaces: REQUIRES EGL

import egl
suite.addTestCase( egl.EglImageAsImageExt, 'EGL_LIMITED' )                           # Requires EGL
suite.addTestCase( egl.EglImageAsImageWithChildImageExt, 'EGL_LIMITED' )             # Requires EGL
suite.addTestCase( egl.DestroyEglImageUsedAsImageExt, 'EGL_LIMITED' )                # Requires EGL
suite.addTestCase( egl.EglImageFromImageExt, 'EGL_LIMITED' )                         # Requires EGL
suite.addTestCase( egl.OrphaningEglImageFromImageExt, 'EGL_LIMITED' )                # Requires EGL
suite.addTestCase( egl.PbufferFromImage, 'EGL_LIMITED' )                             # Requires EGL
suite.addTestCase( egl.PixmapSurfaceAsImageExt, 'EGL_LIMITED' )                      # Requires EGL

suite.addTestCase( egl.WindowSurface, 'EGL_FULL' )                                   # Requires EGL
suite.addTestCase( egl.PbufferSurface, 'EGL_FULL' )                                  # Requires EGL
suite.addTestCase( egl.PixmapSurface, 'EGL_FULL' )                                   # Requires EGL
suite.addTestCase( egl.SharedPixmapSurface, 'EGL_FULL' )                             # Requires EGL

suite.addTestCase( egl.SwapBuffersPreserved, 'EGL_FULL' )                            # Requires EGL

suite.addTestCase( egl.Extensions, 'EGL_LIMITED' )                                   # Requires EGL

# ----------------------------------------------------------------------
command()
