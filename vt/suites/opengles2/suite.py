# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

# TODO: More stencil tests
# TODO: Stencil op sfail, zfail, zpass, separate
# TODO: Stencil with non-trianglular primitives

# TODO: Some blending cases draw all black image; develop separate tests
# TODO: Blending with non-trianglular primitives
# TODO: Use vertex attrib functions to load matrices

# TODO: Reload buffer data
# TODO: Reload shader source
# TODO: Modify program shaders and relink
# TODO: Delete active program
# TODO: Primitive drawing quality
# TODO: Sample coverage
# TODO: Dither
# TODO: Vertex attrib normalize
# TODO: Overlapping primitives
# TODO: Use texture with blending
# TODO: Shader-specific tests
# TODO: Default values in vertex attributes, uniforms, vertex attribute arrays
# TODO: Current vertex attrib (default 0,0,0,1)
# TODO: Ignored line segment with odd vertices
# TODO: One vertex line
# TODO: Incomplete triangle, triangle strip, triangle fan
# TODO: Ignored triangle with missing vertices
# TODO: Uniform name variations
# TODO: Ignored uniform values
# TODO: Samplers (texture access) in vertex shader
# TODO: Stencil operation without stencil buffer
# TODO: Active texture
# TODO: Normalized integer vertex attributes
# TODO: Assure attribute bound to index i also work after program switch
# TODO: Attribute aliasing
# TODO: Uniform values restored with program

import sys
sys.path.append( '../../../python' )
sys.path.append( '../../lib' )
sys.path.append( '../../lib/testcases/opengles2' )
sys.path.append( '../../lib/targets' )

import vt

suite, command = vt.init()

import info
suite.addTestCase( info.Info )
suite.addTestCase( info.EglInfo, 'EGL_LIMITED' )

import clear
suite.addTestCase( clear.ClearColor )
suite.addTestCase( clear.ClearColorWithMask )
suite.addTestCase( clear.ClearFramebuffer )

import objects
suite.addTestCase( objects.TexturedCubeRotated )

import vertices
suite.addTestCase( vertices.VertexType )
suite.addTestCase( vertices.HalfFloatVertex )
suite.addTestCase( vertices.VertexPointerOffset )
suite.addTestCase( vertices.VertexBuffer )
suite.addTestCase( vertices.VertexBufferOffset )
suite.addTestCase( vertices.InterleavedArrays )
suite.addTestCase( vertices.InterleavedBufferArrays )
suite.addTestCase( vertices.VertexBufferUsage )
suite.addTestCase( vertices.VertexBufferSubdata )

import elements
suite.addTestCase( elements.Points )
suite.addTestCase( elements.RoundPoints )
suite.addTestCase( elements.TexturedPoints )
suite.addTestCase( elements.Lines )
suite.addTestCase( elements.LineStrips )
suite.addTestCase( elements.LineLoops )
suite.addTestCase( elements.TriangleFans )
suite.addTestCase( elements.TriangleStrip )
suite.addTestCase( elements.Triangles )

import pixels
suite.addTestCase( pixels.SmoothPixels )
suite.addTestCase( pixels.FlatTexels )
suite.addTestCase( pixels.SmoothTexels )
suite.addTestCase( pixels.FlatMultitexels )
suite.addTestCase( pixels.SmoothMultitexels )
suite.addTestCase( pixels.SmoothTexelsAlpha8 )
suite.addTestCase( pixels.TextureExtensions )

import texturing
suite.addTestCase( texturing.TextureWrap )
suite.addTestCase( texturing.NpotTextureWrap )
suite.addTestCase( texturing.NpotTextureWrapExt )
suite.addTestCase( texturing.TextureMinFilter )
suite.addTestCase( texturing.NpotTextureMinFilter )
suite.addTestCase( texturing.NpotTextureMinFilterExt )
suite.addTestCase( texturing.TextureMagFilter )
suite.addTestCase( texturing.NpotTextureMagFilter )
suite.addTestCase( texturing.NpotTextureMagFilterExt )
suite.addTestCase( texturing.TextureGenerateMipmap )
suite.addTestCase( texturing.NpotTextureGenerateMipmap )
suite.addTestCase( texturing.TextureSubImage )
suite.addTestCase( texturing.CopyTexImage )
suite.addTestCase( texturing.CopyTexSubImage )
suite.addTestCase( texturing.GenerateMipmaps )
suite.addTestCase( texturing.NpotGenerateMipmaps )

import cubemap
suite.addTestCase( cubemap.CubeMap )
suite.addTestCase( cubemap.NpotCubeMap )
suite.addTestCase( cubemap.NpotCubeMapExt )
suite.addTestCase( cubemap.CubeMapSphere )
suite.addTestCase( cubemap.NpotCubeMapSphere )
suite.addTestCase( cubemap.NpotCubeMapSphereExt )
suite.addTestCase( cubemap.CubeMapMinFilter )
suite.addTestCase( cubemap.NpotCubeMapMinFilter )
suite.addTestCase( cubemap.NpotCubeMapMinFilterExt )
suite.addTestCase( cubemap.CubeMapSubImage )
suite.addTestCase( cubemap.CubeMapCopyTexImage )
suite.addTestCase( cubemap.CubeMapCopyTexSubImage )
suite.addTestCase( cubemap.CubeMapGenerateMipmaps )
suite.addTestCase( cubemap.NpotCubeMapGenerateMipmaps )

import palette
suite.addTestCase( palette.PaletteTextureWrap )
suite.addTestCase( palette.NpotPaletteTextureWrap )
suite.addTestCase( palette.NpotPaletteTextureWrapExt )
suite.addTestCase( palette.PaletteTextureMinFilter )
suite.addTestCase( palette.NpotPaletteTextureMinFilter )
suite.addTestCase( palette.NpotPaletteTextureMinFilterExt )     # Crashes in AMD win32 GLES2 wrapper
suite.addTestCase( palette.PaletteCubeMapWrap )
suite.addTestCase( palette.NpotPaletteCubeMapWrap )
suite.addTestCase( palette.NpotPaletteCubeMapWrapExt )
suite.addTestCase( palette.PaletteCubeMapMinFilter )
suite.addTestCase( palette.NpotPaletteCubeMapMinFilter )        # Crashes in AMD win32 GLES2 wrapper?
suite.addTestCase( palette.NpotPaletteCubeMapMinFilterExt )

import etc
suite.addTestCase( etc.EtcTextureWrap )
suite.addTestCase( etc.EtcTextureMinFilter )
suite.addTestCase( etc.NonSquareEtcTextureMinFilter )
suite.addTestCase( etc.EtcCubeMapMinFilter )
suite.addTestCase( etc.NonSquareEtcTexture )
suite.addTestCase( etc.NpotEtcTexture )                         # Crashes in N900
suite.addTestCase( etc.NonSquareNpotEtcTexture )                # Crashes in N900

import pvrtc
suite.addTestCase( pvrtc.PvrtcTextureWrap )
suite.addTestCase( pvrtc.PvrtcTextureMinFilter )
suite.addTestCase( pvrtc.PvrtcCubeMapMinFilter )

import blending
suite.addTestCase( blending.Blending )
suite.addTestCase( blending.SeparateBlending )
suite.addTestCase( blending.BlendingEquation )
suite.addTestCase( blending.SeparateBlendingEquation )

import culling
suite.addTestCase( culling.PolygonCulling )
suite.addTestCase( culling.PolygonFrontFace )

import viewport
suite.addTestCase( viewport.TrianglesViewportCulling )
suite.addTestCase( viewport.TrianglesNearFarCulling )
suite.addTestCase( viewport.PointsViewportCulling )
suite.addTestCase( viewport.PointsNearFarCulling )
suite.addTestCase( viewport.LinesViewportCulling )
suite.addTestCase( viewport.LinesNearFarCulling )
suite.addTestCase( viewport.LineStripsViewportCulling )
suite.addTestCase( viewport.LineStripsNearFarCulling )
suite.addTestCase( viewport.LineLoopsViewportCulling )
suite.addTestCase( viewport.LineLoopsNearFarCulling )
suite.addTestCase( viewport.TriangleFansViewportCulling )
suite.addTestCase( viewport.TriangleFansNearFarCulling )

import depth
suite.addTestCase( depth.DepthFunc )
suite.addTestCase( depth.DepthFuncWithEqualDepth )
suite.addTestCase( depth.DepthMask )
suite.addTestCase( depth.ClearDepth )
suite.addTestCase( depth.DepthRange )
suite.addTestCase( depth.PolygonOffset )
suite.addTestCase( depth.PolygonOffsetUnit )

import scissor
suite.addTestCase( scissor.ClearWithScissor )
suite.addTestCase( scissor.Scissor )
suite.addTestCase( scissor.ScissorAndViewport )
suite.addTestCase( scissor.TrianglesScissorCulling )
suite.addTestCase( scissor.PointsScissorCulling )
suite.addTestCase( scissor.LinesScissorCulling )
suite.addTestCase( scissor.LineStripsScissorCulling )
suite.addTestCase( scissor.LineLoopsScissorCulling )
suite.addTestCase( scissor.TriangleFansScissorCulling )

import stencil
suite.addTestCase( stencil.BasicStencil )
suite.addTestCase( stencil.BasicStencilSeparate )
suite.addTestCase( stencil.StencilFunc )
suite.addTestCase( stencil.StencilFuncSeparate )
suite.addTestCase( stencil.StencilOpMask )
suite.addTestCase( stencil.StencilMask )
suite.addTestCase( stencil.StencilDpassOpWithNoDepthTest )

import buffers
suite.addTestCase( buffers.FramebufferWithDepth )
suite.addTestCase( buffers.FramebufferWithStencil )
suite.addTestCase( buffers.FramebufferWithDepthAndStencil )
suite.addTestCase( buffers.FramebufferTexture2DColor )
suite.addTestCase( buffers.FramebufferTexture2DColorNpot )
suite.addTestCase( buffers.FramebufferTexture2DColorNpotExt )
suite.addTestCase( buffers.FramebufferTexture2DColorCubemap )
suite.addTestCase( buffers.FramebufferTexture2DColorCubemapNpot )
suite.addTestCase( buffers.FramebufferTexture2DColorCubemapNpotExt )
suite.addTestCase( buffers.FramebufferTexture2DWithDepth )
suite.addTestCase( buffers.FramebufferTexture2DWithOES_depth )
suite.addTestCase( buffers.FramebufferTexture2DDepthExt )
suite.addTestCase( buffers.FramebufferTexture2DDepthCubemapExt )
suite.addTestCase( buffers.TwoFramebuffers )
suite.addTestCase( buffers.DeleteFramebufferInUse )
suite.addTestCase( buffers.FramebufferOES_rgb8_rgba8 )
suite.addTestCase( buffers.FramebufferOES_depth )
suite.addTestCase( buffers.FramebufferOES_stencil4 )
suite.addTestCase( buffers.FramebufferSize )
suite.addTestCase( buffers.FramebufferTexture2DSize )

import extensions
suite.addTestCase( extensions.Extensions )

# ----------------------------------------------------------------------
# EGL image and surfaces: REQUIRES EGL

import egl
suite.addTestCase( egl.EglImageAsTextureExt, 'EGL_LIMITED' )                                 # Requires EGL image
suite.addTestCase( egl.EglImageFromTextureExt, 'EGL_LIMITED' )                               # Requires EGL image
suite.addTestCase( egl.EglImageFromTextureMipmapExt, 'EGL_LIMITED' )                         # Requires EGL image
suite.addTestCase( egl.TexImage2DOrphaningEglImageFromTextureExt, 'EGL_LIMITED' )            # Requires EGL image
suite.addTestCase( egl.CopyTexImage2DOrphaningEglImageFromTextureExt, 'EGL_LIMITED' )        # Requires EGL image
suite.addTestCase( egl.CompressedTexImage2DOrphaningEglImageFromTextureExt, 'EGL_LIMITED' )  # Requires EGL image
suite.addTestCase( egl.MipmapOrphaningEglImageFromTextureExt, 'EGL_LIMITED' )                # Requires EGL image
suite.addTestCase( egl.EglImageExternalAsTextureExt, 'EGL_LIMITED' )                         # Requires EGL

suite.addTestCase( egl.PixmapSurfaceAsTextureExt, 'EGL_LIMITED' )                            # Requires EGL image
suite.addTestCase( egl.EglImageAsRenderbufferExt, 'EGL_LIMITED' )                            # Requires EGL image
suite.addTestCase( egl.TextureFromRenderbufferExt, 'EGL_LIMITED' )                           # Requires EGL image
suite.addTestCase( egl.EglImageFromCubemapExt, 'EGL_LIMITED' )                               # Requires EGL image

suite.addTestCase( egl.PbufferAsTexture, 'EGL_LIMITED' )                                     # Requires EGL
suite.addTestCase( egl.PbufferWithMipmapsAsTexture, 'EGL_LIMITED' )                          # Requires EGL
suite.addTestCase( egl.NpotPbufferWithMipmapsAsTextureExt, 'EGL_LIMITED' )                   # Requires EGL
suite.addTestCase( egl.PbufferWithGeneratedMipmapsAsTexture, 'EGL_LIMITED' )                 # Requires EGL
suite.addTestCase( egl.NpotPbufferWithGeneratedMipmapsAsTextureExt, 'EGL_LIMITED' )          # Requires EGL

suite.addTestCase( egl.WindowSurface, 'EGL_FULL' )                                           # Requires EGL
suite.addTestCase( egl.PbufferSurface, 'EGL_FULL' )                                          # Requires EGL
suite.addTestCase( egl.PixmapSurface, 'EGL_FULL' )                                           # Requires EGL
suite.addTestCase( egl.SharedPixmapSurface, 'EGL_FULL' )                                     # Requires EGL

suite.addTestCase( egl.SwapBuffersPreserved, 'EGL_FULL' )                                    # Requires EGL

suite.addTestCase( egl.Extensions, 'EGL_LIMITED' )                                           # Requires EGL

#----------------------------------------------------------------------
command()
