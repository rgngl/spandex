# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles1common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 2, 2 )

# ----------------------------------------------------------------------
def createPlane( meshtype, gridx, gridy, tx, ty, sx, sy ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                         2,
                                                         [ float( 2.0 ),
                                                           float( 2.0 ) ] ) )

        if meshtype == 'TriangleStrip':
                mesh = MeshStripPlane( gridx,
                                       gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       texCoordAttribute,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        else:
                mesh = MeshTrianglePlane( gridx,
                                          gridy,
                                          vertexAttribute,
                                          colorAttribute,
                                          None,
                                          texCoordAttribute,
                                          indexAttribute,
                                          None,
                                          MESH_CCW_WINDING,
                                          False )
                
        mesh.translate( [ tx, ty, 0 ] )
        mesh.scale( [ sx, sy ] )

        return mesh
       
# ----------------------------------------------------------------------
def setupTexture( modules, indexTracker, w, h, c1, c2 ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
    
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                            c1,
                                            c2,
                                            2, 2,
                                            w, h,
                                            'OFF',
                                            'OPENGLES1_RGBA8888' )

        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_REPEAT',
                                'GL_REPEAT',
                                'OFF' )

        return TextureSetup( textureDataIndex, textureIndex )


# ----------------------------------------------------------------------
class TrianglesViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test triangles viewport culling.

        Expected output: in the center location, a 2x2 checker board with
        up-left and bottom-right rectangles magenta, up-right and bottom-left
        rectangles blue. The checker board is drawn into center of the gray
        background. Different locations displace the checker board so that only
        one rectangle is visible. UpLeft=magenta, UpRight=blue,
        BottomRight=magenta, BottomLeft=blue. Viewport size 320*240.
        '''

        testValues = { ( 'location', 'meshtype' ) : [ ( 'Center',      'Triangles' ),
                                                      ( 'Center',      'TriangleStrip' ),
                                                      ( 'UpLeft',      'Triangles' ),
                                                      ( 'UpLeft',      'TriangleStrip' ),
                                                      ( 'UpRight',     'Triangles' ),
                                                      ( 'UpRight',     'TriangleStrip' ),
                                                      ( 'BottomRight', 'Triangles' ),
                                                      ( 'BottomRight', 'TriangleStrip' ),
                                                      ( 'BottomLeft',  'Triangles' ),
                                                      ( 'BottomLeft',  'TriangleStrip' ),
                                          ]
                       }
        
        def __init__( self, location, meshtype ):
                vt.VisualTestCase.__init__( self )
                self.location    = location
                self.meshtype    = meshtype
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.name = "OPENGLES1 triangles viewport culling, location=%s, mesh type=%s" % ( self.location,
                                                                                                  self.meshtype, )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                if self.location == 'Center':
                        tx = -0.5
                        ty = -0.5
                        sx = 4.0
                        sy = 4.0
                elif self.location == 'UpLeft':
                        tx = -1.0
                        ty = 0.0
                        sx = 4.0
                        sy = 4.0
                elif self.location == 'UpRight':
                        tx = 0.0
                        ty = 0.0
                        sx = 4.0
                        sy = 4.0
                elif self.location == 'BottomRight':
                        tx = 0.0
                        ty = -1.0
                        sx = 4.0
                        sy = 4.0
                elif self.location == 'BottomLeft':
                        tx = -1.0
                        ty = -1.0
                        sx = 4.0
                        sy = 4.0
                else:
                        raise 'Unexpected location'
                        
                mesh              = createPlane( self.meshtype, self.gridx, self.gridy, tx, ty, sx, sy )
                mesh.gradientColors( [ 1.0, 0.0, 1.0, 1.0 ], [ 1.0, 0.0, 1.0, 1.0 ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )               
                useVertexData( modules, indexTracker, vertexDataSetup )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                textureSetup      = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  [ 0.0, 0.0, 1.0, 1.0 ],
                                                  [ 1.0, 0.0, 1.0, 1.0 ] )

                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )

                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES1.Viewport( x, y, w, h );
                OpenGLES1.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                offset  = 1.0
                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES1.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
# ----------------------------------------------------------------------
def createPlane2( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh


# ----------------------------------------------------------------------
class PointsViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test points viewport culling.

        Expected output: 6x6 grid of points drawn using vertex color. Vertex
        colors are defined so that the lower-left vertex has color
        [0.0,0.0,0.5,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Points are drawn
        into center of the gray background. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.pointSize = 10
                self.gridx     = 8
                self.gridy     = 8
                self.overdraw  = 1
                self.name = "OPENGLES1 points viewport culling"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 1.1, 1.1 ] )
                
                mesh.indices = []
                for i in range( self.gridx * self.gridy ):
                        mesh.indices.append( i )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )                
                useVertexData( modules, indexTracker, vertexDataSetup )

                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2
                
                OpenGLES1.Viewport( x, y, w, h );
                OpenGLES1.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                OpenGLES1.PointSize( self.pointSize )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.DrawElements( 'GL_POINTS',
                                        vertexDataSetup.indexArrayLength,
                                        0,
                                        vertexDataSetup.indexArrayIndex )    

                OpenGLES1.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------
class LinesViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test lines viewport culling.

        Expected output: 6 horizontal lines and 6 vertical lines, spaced evenly
        from top to bottom, left to right. Vertex colors are defined so that the
        lower-left vertex has color [0.0,0.0,0.5,1.0] and the upper-right vertex
        has color [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Lines are drawn
        into center of the gray background. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = 1
                self.gridx     = 8
                self.gridy     = 8
                self.overdraw  = 1
                self.name = "OPENGLES1 lines viewport culling"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 1.1, 1.1 ] )
                
                mesh.indices = []
                for i in range( self.gridy ):
                        mesh.indices.append( i * self.gridx )
                        mesh.indices.append( i * self.gridx + ( self.gridx - 1 ) )

                for i in range( self.gridx ):
                        mesh.indices.append( i )
                        mesh.indices.append( ( self.gridx - 1 ) * self.gridy + i )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                OpenGLES1.LineWidth( self.lineWidth );
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES1.Viewport( x, y, w, h );               
                OpenGLES1.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )                                
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES1.DrawElements( 'GL_LINES',
                                        vertexDataSetup.indexArrayLength,
                                        0,
                                        vertexDataSetup.indexArrayIndex )    

                OpenGLES1.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
# ----------------------------------------------------------------------
class LineStripsViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test line strips viewport culling.

        Expected output: 7 brackets pointing to top-right corner. Vertex colors
        are defined so that the lower-left vertex has color [0.0,0.0,0.5,1.0]
        and the upper-right vertex has color [1.0,1.0,1.0,1.0]. Between those
        the vertex color is linearly interpolated from left to right, from
        bottom to top. Brackets are drawn into center of the gray
        background. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = 1
                self.gridx     = 9
                self.gridy     = 9
                self.overdraw  = 1
                self.name = "OPENGLES1 line strips viewport culling"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 1.1, 1.1 ] )

                indicesPerStrip = 8
                mesh.indices = []
                for i in range( 1, self.gridy ):
                        if i % 2 == 1:
                                mesh.indices.append( i - 1 )                                
                                mesh.indices.append( i )
                                mesh.indices.append( i )                  # Add degerated segment
                                mesh.indices.append( i * self.gridx + i )
                                mesh.indices.append( i * self.gridx )
                        else:
                                mesh.indices.append( i * self.gridx )
                                mesh.indices.append( i * self.gridx + i )
                                mesh.indices.append( i )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                OpenGLES1.LineWidth( self.lineWidth );
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES1.Viewport( x, y, w, h );
                OpenGLES1.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerStrip ):
                        OpenGLES1.DrawElements( 'GL_LINE_STRIP',
                                                indicesPerStrip,
                                                i * indicesPerStrip,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------
class LineLoopsViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test line loops viewport culling.

        Expected output: 7 brackets pointing to top-right corner. Vertex colors
        are defined so that the lower-left vertex has color [0.0,0.0,0.5,1.0]
        and the upper-right vertex has color [1.0,1.0,1.0,1.0]. Between those
        the vertex color is linearly interpolated from left to right, from
        bottom to top. Brackets are drawn into center of the gray
        background. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = 1
                self.gridx     = 9
                self.gridy     = 9
                self.overdraw  = 1
                self.name = "OPENGLES1 line loops viewport culling"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 1.1, 1.1 ] )

                indicesPerLoop = 7
                mesh.indices = []
                for i in range( 1, self.gridy - 1 ):
                        mesh.indices.append( i )
                        mesh.indices.append( i + 1 )
                        mesh.indices.append( i + 1 )   # Denerated segment                        
                        mesh.indices.append( ( i + 1 ) * self.gridx + i + 1 )
                        mesh.indices.append( ( i + 1 ) * self.gridx )
                        mesh.indices.append( i * self.gridx )
                        mesh.indices.append( i * self.gridx + i )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                OpenGLES1.LineWidth( self.lineWidth );
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES1.Viewport( x, y, w, h );
                OpenGLES1.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerLoop ):
                        OpenGLES1.DrawElements( 'GL_LINE_LOOP',
                                                indicesPerLoop,
                                                i * indicesPerLoop,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
# ----------------------------------------------------------------------
class TriangleFansViewportCulling( vt.VisualTestCase ):

        ''' The purpose of the test: test triangle fans viewport culling.

        Expected output: A rectangle on gray background. The bottom-left corner
        of the rectangle should be blueish, the top-right corner
        grayish. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.gridx     = 9
                self.gridy     = 9
                self.overdraw  = 1
                self.name = "OPENGLES1 triangle fans viewport culling"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane2( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                
                indicesPerFan = 11
                mesh.indices = []
                for i in range( 0, self.gridx - 2, 2 ):
                        for j in range( 0, self.gridy - 2, 2 ):
                                mesh.indices.append( j + ( i + 1 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx )   # Degenerated triangle
                                mesh.indices.append( j + ( i + 0 ) * self.gridx )
                                mesh.indices.append( j + ( i + 0 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 0 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 2 )
                                                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2 
                OpenGLES1.Viewport( x, y, w, h );
                OpenGLES1.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerFan ):
                        OpenGLES1.DrawElements( 'GL_TRIANGLE_FAN',
                                                indicesPerFan,
                                                i * indicesPerFan,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
