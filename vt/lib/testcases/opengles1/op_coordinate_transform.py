#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

import math

WIDTH, HEIGHT = ( 320, 240, )

# ----------------------------------------------------------------------
class DepthRange( vt.VisualTestCase ):
    
    """ Testing glDepthRange. Drawing two rectangles, the second (rotated) one
    further away than the first one. When depth range is inverted (near=1.0,
    far=0.0), the rotated rectangle should be drawn in front (GL_DEPTH_TEST
    enabled)."""
    
    testValues = { 'mode' : [ [ 1.0, 0.0 ],
                              [ 0.0, 1.0 ] ]
                   }
    
    def __init__( self, mode ):
        vt.VisualTestCase.__init__( self )
        self.near = mode[ 0 ]
        self.far  = mode[ 1 ]
        self.name = "OPENGLES1 depth range zNear=%.1f zFar=%.1f" % ( self.near, self.far, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )
        
	OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 0.5, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Translate( [ 0, 0, -1 ] )
        OpenGLES1.DepthRange( self.near, self.far )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE'  )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        data = triangleQuad()
        
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Scale( [ 4, 4, 4 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.Translate( [ 0, 0, -1 ] )
        OpenGLES1.Rotate( 45.0, [ 0.0, 0.0, 1.0 ] )
        OpenGLES1.Scale( [ 8, 8, 8 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------
class ViewPort( vt.VisualTestCase ):
    
    """ Testing glViewPort. The same object is drawn on each 1/4th of the blue
    surface, and finally to the center. In each draw, the viewport is used to
    limit drawing to 1/4th of the surface. Object is drawn with varying scaling
    to test that drawing does not overflow the viewport."""

    testValues = { 'scale' : [ 5,
                               20 ]
                   }
    
    def __init__( self, scale ):
        vt.VisualTestCase.__init__( self )
        self.scale = scale
        self.name  = "OPENGLES1 viewport scale=%d" % ( self.scale, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 0.5, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        data = triangleQuad()
        
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ self.scale ] * 3 )
        
        for x in ( 0, WIDTH / 2 ):
            for y in ( 0, HEIGHT / 2 ):
                OpenGLES1.Viewport( x, y, WIDTH / 2, HEIGHT / 2 )
                OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.Viewport( WIDTH / 4, HEIGHT / 4, WIDTH / 2, HEIGHT / 2)
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------
class LoadMatrix( vt.VisualTestCase ):
    
    """ Testing LoadMatrix. First translation is -2 units away from the camera,
    then the specified transformation matrix is applied. All matrix
    transformations in this test are made with external transformation matrix
    and applied with glLoadMatrix."""
    
    testValues = { 'action' : [ 'translate', 'rotate', 'scale' ]
                   }
    
    def __init__( self, action ):
        vt.VisualTestCase.__init__(self)
        self.action = action
        self.matrix = [ 1.0, 0.0, 0.0, 0.0,
                        0.0, 1.0, 0.0, 0.0,
                        0.0, 0.0, 1.0, 0.0,
                        0.0, 0.0, 0.0, 1.0,
                        ]
        
        self.name = "OPENGLES1 load matrix %s" % ( self.action, )
    
    def multmatrix( self, b ):
        a = self.matrix
        if len( a ) != 16 or len( b ) != 16:
            raise ValueError( "Expecting matrices of 16 elements (got %d and %d)" % ( len( a ), len( b ) ) )
        
        r = [ 0 for c in xrange( 16 ) ]
        for j in xrange( 4 ):
            for i in xrange( 4 ):
                r[ ( 4 * i ) + j ] = a[ ( 4 * i ) + 0 ] * b[ ( 4 * 0 ) + j ] + a[ ( 4 * i ) + 1 ] * b[ ( 4 * 1 ) + j ] + a[ ( 4 * i ) + 2 ] * b[ ( 4 * 2 ) + j ] + a[ ( 4 * i ) + 3 ] * b[ ( 4 * 3 ) + j ]
        self.matrix = r
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 20.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        self.multmatrix( [ 1.0, 0.0,  0.0, 0.0,
                           0.0, 1.0,  0.0, 0.0,
                           0.0, 0.0,  1.0, 0.0,
                           0.0, 0.0, -2.0, 1.0,
                           ] )
        OpenGLES1.LoadMatrix( self.matrix )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )

        if self.action == "translate":
            matrix = [ 1.0, 0.0, 0.0, 0.0,
                       0.0, 1.0, 0.0, 0.0,
                       0.0, 0.0, 1.0, 0.0,
                       0.6, 0.5, 0.0, 1.0,
                       ]
        elif self.action == "rotate":
            # rotating around z axis
            angle = math.radians( 45.0 )
            matrix = [ math.cos( angle ), -math.sin( angle ),    0.0,    0.0,
                       math.sin( angle ),  math.cos( angle ),    0.0,    0.0,
                                     0.0,                0.0,    1.0,    0.0,
                                     0.0,                0.0,    0.0,    1.0,
                       ]
        elif self.action == "scale":
            matrix = [ 1.8, 0.0, 0.0, 0.0,
                       0.0, 0.5, 0.0, 0.0,
                       0.0, 0.0, 1.0, 0.0,
                       0.0, 0.0, 0.0, 1.0,
                       ]
        else:
            raise ValueError( "Unexpected matrix action type" )

        self.multmatrix( matrix )
        
        data = triangleQuad()
        
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
	
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        # Not using push/pop here, since we are using our own matrix
        OpenGLES1.Scale( [ 5, 5, 5 ])
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.LoadMatrix( self.matrix )
        OpenGLES1.Scale([ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len(data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------        
class MultMatrix( vt.VisualTestCase ):
    
    """ Testing glMultMatrix. First draws the square on default position, then in
    a translated/scaled/rotated position."""
    
    testValues = { 'action' : [ 'translate', 'rotate', 'scale' ]
                   }
    
    def __init__( self, action ):
        vt.VisualTestCase.__init__(self)
        self.action = action
        self.name = "OPENGLES1 mult matrix %s" % ( self.action, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 20.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )

        if self.action == "translate":
            matrix = [ 1.0, 0.0, 0.0, 0.0,
                       0.0, 1.0, 0.0, 0.0,
                       0.0, 0.0, 1.0, 0.0,
                       0.6, 0.5, 0.0, 1.0,
                       ]
        elif self.action == "rotate":
            # rotating around z axis
            angle = math.radians( 45.0 )
            matrix = [ math.cos( angle ), -math.sin( angle ),    0.0,    0.0,
                       math.sin( angle ),  math.cos( angle ),    0.0,    0.0,
                                     0.0,                0.0,    1.0,    0.0,
                                     0.0,                0.0,    0.0,    1.0,
                       ]
        elif self.action == "scale":
            matrix = [ 1.8, 0.0, 0.0, 0.0,
                       0.0, 0.5, 0.0, 0.0,
                       0.0, 0.0, 1.0, 0.0,
                       0.0, 0.0, 0.0, 1.0,
                       ]
        else:
            raise ValueError( "Unexpected matrix action type" )
        
        data = triangleQuad()
        
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.PushMatrix()
        OpenGLES1.MultMatrix( matrix )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data['indices'] ), 0, 2 )
        OpenGLES1.PopMatrix()

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------
class Rotate( vt.VisualTestCase ):
    
    """ Testing glRotate. Rotations are made in xyz order."""
    
    testValues = { 'x' : [ 0.0, 45.0 ],
                   'y' : [ 0.0, 45.0 ],
                   'z' : [ 0.0, 45.0 ],
                   }
        
    def __init__( self, x = 0.0, y = 0.0, z = 0.0 ):
        vt.VisualTestCase.__init__( self )
        self.rotx = x
        self.roty = y
        self.rotz = z
        self.name = 'OPENGLES1 rotate x=%.1f y=%.1f z=%.1f' % ( self.rotx, self.roty, self.rotz, )
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 0.5, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Rotate( self.rotx, [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Rotate( self.roty, [ 0.0, 1.0, 1.0 ] )
        OpenGLES1.Rotate( self.rotz, [ 0.0, 0.0, 1.0 ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------        
class Scale( vt.VisualTestCase ):
    
    """ Testing glScale."""
    
    testValues = {'x' : [ 1.0, 1.5 ],
                  'y' : [ 0.05, 1.0, 1.5 ],
                  'z' : [ 1.0 ],
                  }
        
    def __init__( self, x = 0.0, y = 0.0, z = 0.0 ):
        vt.VisualTestCase.__init__( self )
        self.scale = [ x, y, z ]
        self.name = 'OPENGLES1 scale x=%.2f y=%.2f z=%.2f' % ( self.scale[ 0 ], self.scale[ 1 ], self.scale[ 2 ], )
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        data = triangleQuad()
        
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
	
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( self.scale )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------        
class Translate( vt.VisualTestCase ):
    
    """ Testing glTranslate. Initial translation is [0, 0, -2.5], then
    translating by the given parameters. Near clip plane is set to 1.0 and far
    clip plane to 10.0."""
    
    testValues = { 'x' : [ 0.0, 0.5, -1.0 ],
                   'y' : [ 0.0, 0.5, -1.0 ],
                   'z' : [ 0.0, 0.5, -1.0 ],
                   }
        
    def __init__( self, x = 0.0, y = 0.0, z = 0.0 ):
        vt.VisualTestCase.__init__( self )
        self.move = [ x, y, z ]
        self.name = 'OPENGLES1 translate x=%.1f y=%.1f z=%.1f' % ( self.move[ 0 ], self.move[ 1 ], self.move[ 2 ], )
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2.0 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Translate( self.move )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------        
class Frustum( vt.VisualTestCase ):
    
    """ Testing glFrustum. Draws objects inside and just outside the near and far
    clipping planes, only the red square should be visible."""
    
    testValues = { 'width'  : [ 1.0 ],
                   'height' : [ 1.0 ],
                   'znear'  : [ 1.5, 1.0, 0.2 ],
                   'zfar'   : [ 3.0 ]
                   }
        
    def __init__( self, width, height, znear, zfar ):
        vt.VisualTestCase.__init__( self )
        self.width  = width
        self.height = height
        self.znear  = znear
        self.zfar   = zfar
        self.name = 'OPENGLES1 frustum w=%.1f h=%.1f zNear=%.1f zFar=%.1f' % ( self.width, self.height, self.znear, self.zfar, )
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )
	
        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Frustum( -self.width, self.width, -self.height, self.height, self.znear, self.zfar )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        
        data = triangleQuad()
        
        vertexData = data[ 'vertices' ]
        indexData  = data[ 'indices' ]
        OpenGLES1.AppendToArray( 0, vertexData )
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.AppendToArray( 1, indexData )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        # On the visible area
        OpenGLES1.Color( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Translate( [ 0.0, 0.0, -1.6 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 1 )
        # Just outside zfar...
        OpenGLES1.Color( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Translate( [ 0.0, 0.0, -( self.zfar + 0.01 ) ] )
        OpenGLES1.Scale( [ 7, 7, 7 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len(data[ 'indices' ] ), 0, 1 )
        # ...and just outside znear
        OpenGLES1.Color( [ 0.0, 0.0, 1.0, 1.0 ] )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Translate( [ 0.0, 0.0, -( self.znear - 0.01 ) ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 1 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        

# ----------------------------------------------------------------------        
class Ortho( vt.VisualTestCase ):
    
    """ Testing glOrtho. Two objects are drawn, near and far from the
    camera. Both objects should be the same size. """
    
    testValues = { 'width'  : [ 1.0, 2.0 ],
                   'height' : [ 1.0, 2.0 ],
                   'znear'  : [ 1.0 ],
                   'zfar'   : [ 3.0 ]
                   }
    
    def __init__( self, width, height, znear, zfar ):
        vt.VisualTestCase.__init__( self )
        self.width  = width
        self.height = height
        self.znear  = znear
        self.zfar   = zfar
        self.name = 'OPENGLES1 ortho w=%.1f h=%.1f zNear=%.1f zFar=%.1f' % ( self.width, self.height, self.znear, self.zfar, )
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Ortho( -self.width, self.width, -self.height, self.height, self.znear, self.zfar )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        
        data = triangleQuad()
        
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'indices' ] )
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        # Draw near
        OpenGLES1.Color( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Translate( [ -0.3, 0.0, -( self.znear + 0.1 ) ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 1 )
        # Draw far
        OpenGLES1.Color( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.LoadIdentity()
        OpenGLES1.Translate( [ 0.3, 0.0, -( self.zfar - 0.1 ) ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 1 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------        
class PushPop( vt.VisualTestCase ):
    
    """ Testing glPushMatrix and glPopMatrix. Pushes some matrices to the stack
    and then pops them while drawing squares."""
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.name = "OPENGLES1 push and pop matrix"
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 0.5, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        # 1
        OpenGLES1.Translate( [ -2.0, -2.0, 0.0 ] )
        OpenGLES1.PushMatrix()
        # 2
        OpenGLES1.Translate( [ 2.5, 1.0, 0.0 ] )
        OpenGLES1.Rotate( 30.0, [ 0.0, 0.0, 1.0 ])
        OpenGLES1.PushMatrix()
        # 3
        OpenGLES1.Translate( [ 2.5, 1.0, 0.0 ] )
        OpenGLES1.Rotate( 30.0, [ 0.0, 0.0, 1.0 ] )
        # 3
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        # 2
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        # 1
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        
        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
