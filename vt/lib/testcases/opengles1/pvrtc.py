# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles1common import *

import math

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )
TYPES         = { 'IMAGES_PVRTC_RGB4'  : 'GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG',
                  'IMAGES_PVRTC_RGB2'  : 'GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG',
                  'IMAGES_PVRTC_RGBA4' : 'GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG',
                  'IMAGES_PVRTC_RGBA2' : 'GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG' }

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
                                       
######################################################################
class PvrtcTextureWrap( vt.VisualTestCase ):

        ''' The purpose of the test: test PVRTC texture wrapping. Requires
        GL_IMG_texture_compression_pvrtc extension. NOTE: might not be
        supported.

        Expected output: a full screen plane with a 4x4 vertex grid. A PVRTC
        flower texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Dithering is
        disabled. Viewport size 320*240.
        '''
       
        testValues = { ( 'format', 'wrapS', 'wrapT' ) : [ ( 'IMAGES_PVRTC_RGB4', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGB4', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB4', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGB4', 'GL_REPEAT', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_REPEAT', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_REPEAT', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_REPEAT', 'GL_REPEAT' ),
                                             ]
                       }
        
        def __init__( self, format, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.format      = format
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 PVRTC texture, format=%s, wrapS=%s, wrapT=%s" % ( self.format[ 7 : ],
                                                                                         wrapToStr( self.wrapS ),
                                                                                         wrapToStr( self.wrapT ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES1.CheckExtension( 'GL_IMG_texture_compression_pvrtc' )                
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                textureSize = max( textureW, textureH )

                imageDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
                Images.CreateData( imageDataIndex,
                                   'IMAGES_FLOWER',
                                   self.format,
                                   textureSize, textureSize )

                dataIndex = indexTracker.allocIndex( 'OPENGLES1_DATA_INDEX' )
                Images.CopyToOpenGLES1( imageDataIndex,
                                        dataIndex )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES1_COMPRESSED_DATA_INDEX' )
                OpenGLES1.CreateCompressedTextureData( compressedTextureDataIndex,
                                                       dataIndex,
                                                       textureSize, textureSize,
                                                       TYPES[ self.format ] )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT,
                                        'OFF' )

                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Disable( 'GL_DITHER' )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                       
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
######################################################################
class PvrtcTextureMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test PVRTC texture min filtering. Requires
        GL_IMG_texture_compression_pvrtc extension. NOTE: might not be
        supported.

        Expected output: a full screen plane with a 4x4 vertex grid. A PVRTC
        flower texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Dithering is
        disabled. Viewport size 320*240.
        '''

        testValues = { ( 'format', 'minFilter', 'wrap' ) : [ ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ]
                       }
        
        def __init__( self, format, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.format      = format
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 PVRTC texture min filter, format=%s, filter=%s, wrap=%s" % ( self.format[ 7 : ],
                                                                                                    filterToStr( self.minFilter ),
                                                                                                    wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES1.CheckExtension( 'GL_IMG_texture_compression_pvrtc' )
                                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                textureSize = max( textureW, textureH )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )

                mmlevel = 0
                while textureSize >= 1:
                        imageDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
                        Images.CreateData( imageDataIndex,
                                           'IMAGES_FLOWER',
                                           self.format,
                                           textureSize, textureSize )

                        dataIndex = indexTracker.allocIndex( 'OPENGLES1_DATA_INDEX' )
                        Images.CopyToOpenGLES1( imageDataIndex,
                                                dataIndex )

                        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES1_COMPRESSED_DATA_INDEX' )
                        OpenGLES1.CreateCompressedTextureData( compressedTextureDataIndex,
                                                               dataIndex,
                                                               textureSize, textureSize,
                                                               TYPES[ self.format ] )
                
                        OpenGLES1.CompressedTexImage2DMML( compressedTextureDataIndex, 'GL_TEXTURE_2D', mmlevel )
                        mmlevel += 1
                        textureSize /= 2

                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap,
                                        'OFF' )
                        
                OpenGLES1.Disable( 'GL_DITHER' )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                       
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

