# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles1common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )

TYPES = { 'GL_PALETTE4_RGB8_OES'      : 'OPENGLES1_RGB888',
          'GL_PALETTE4_RGBA8_OES'     : 'OPENGLES1_RGBA8888',
          'GL_PALETTE4_R5_G6_B5_OES'  : 'OPENGLES1_RGB565',
          'GL_PALETTE4_RGBA4_OES'     : 'OPENGLES1_RGBA4444',
          'GL_PALETTE4_RGB5_A1_OES'   : 'OPENGLES1_RGBA5551',
          'GL_PALETTE8_RGB8_OES'      : 'OPENGLES1_RGB888',
          'GL_PALETTE8_RGBA8_OES'     : 'OPENGLES1_RGBA8888',
          'GL_PALETTE8_R5_G6_B5_OES'  : 'OPENGLES1_RGB565',
          'GL_PALETTE8_RGBA4_OES'     : 'OPENGLES1_RGBA4444',
          'GL_PALETTE8_RGB5_A1_OES'   : 'OPENGLES1_RGBA5551' }

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
                                       
######################################################################
class PaletteTextureWrap( vt.VisualTestCase ):

        ''' The purpose of the test: test palette compressed texture
        wrapping. Requires GL_OES_compressed_paletted_texture extension,
        required for OpenGLES1.1.

        Expected output: a full screen plane with a 4x4 vertex grid. A palette
        compressed red-and-white 2x2 checker texture is drawn across the plane
        with texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). Dithering is disabled. Viewport size 320*240.
        '''

        testValues = { ( 'type', 'wrapS', 'wrapT' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ]
                       }
        
        def __init__( self, type, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 palette compressed texture, type=%s, wrapS=%s, wrapT=%s" % ( paletteTypeToStr( self.type ),
                                                                                                    wrapToStr( self.wrapS ),
                                                                                                    wrapToStr( self.wrapT ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES1.CheckExtension( 'GL_OES_compressed_paletted_texture' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )               
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                                    [ 1.0, 0.0, 0.0, 1.0 ],
                                                    [ 1.0, 1.0, 1.0, 1.0 ],
                                                    2, 2,
                                                    textureW, textureH,
                                                    'OFF',
                                                    TYPES[ self.type ] )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES1_COMPRESSED_DATA_INDEX' )
                OpenGLES1.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                              textureDataIndex,
                                                              self.type )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT,
                                        'OFF' )

                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

                OpenGLES1.Disable( 'GL_DITHER' )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                       
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                

######################################################################
class NpotPaletteTextureWrapExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT palette compressed texture
        wrapping. Requires GL_OES_compressed_paletted_texture extension,
        required for OpenGLES1.1, and GL_OES_texture_npot extension. NOTE: might
        not be supported.

        Expected output: a full screen plane with a 4x4 vertex grid. A palette
        compressed red-and-white 4x4 checker texture is drawn across the plane
        with texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). Dithering is disabled. Viewport size 320*240.
        '''

        testValues = { ( 'type', 'wrapS', 'wrapT' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ]
                       }
        
        def __init__( self, type, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 NPOT palette compressed texture ext, type=%s, wrapS=%s, wrapT=%s" % ( paletteTypeToStr( self.type ),
                                                                                                             wrapToStr( self.wrapS ),
                                                                                                             wrapToStr( self.wrapT ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES1.CheckExtension( 'GL_OES_compressed_paletted_texture' )
                OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )                
                useVertexData( modules, indexTracker, vertexDataSetup )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                                    [ 1.0, 0.0, 0.0, 1.0 ],
                                                    [ 1.0, 1.0, 1.0, 1.0 ],
                                                    2, 2,
                                                    textureW, textureH,
                                                    'OFF',
                                                    TYPES[ self.type ] )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES1_COMPRESSED_DATA_INDEX' )
                OpenGLES1.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                              textureDataIndex,
                                                              self.type )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT,
                                        'OFF' )

                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

                OpenGLES1.Disable( 'GL_DITHER' )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                       
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
                
######################################################################
class PaletteTextureMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test palette compressed texture min
        filtering. Requires GL_OES_compressed_paletted_texture extension,
        required for OpenGLES1.1.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        palette compressed red-and-white 2x2 checker texture is drawn across the
        plane with texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). Dithering is disabled. Viewport size 320*240.
        '''

        testValues = { ( 'type', 'minFilter', 'wrap' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ]
                       }
        
        def __init__( self, type, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 palette compressed texture min filter, type=%s, filter=%s, wrap=%s" % ( paletteTypeToStr( self.type ),
                                                                                                               filterToStr( self.minFilter ),
                                                                                                               wrapToStr( self.wrap ), )

        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES1.CheckExtension( 'GL_OES_compressed_paletted_texture' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )                
                useVertexData( modules, indexTracker, vertexDataSetup )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]

                level = 0
                w = textureW
                h = textureH
                for i in range( max( w, h ) ):
                        w /= 2
                        h /= 2
                        if w < 1 and h < 1:
                                break
                        level += 1                       
                        
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                                    [ 1.0, 0.0, 0.0, 1.0 ],
                                                    [ 1.0, 1.0, 1.0, 1.0 ],
                                                    2, 2,
                                                    textureW, textureH,
                                                    'ON',
                                                    TYPES[ self.type ] )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES1_COMPRESSED_DATA_INDEX' )
                OpenGLES1.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                              textureDataIndex,
                                                              self.type )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap,
                                        'OFF' )
                
                OpenGLES1.CompressedTexImage2DMML( compressedTextureDataIndex, 'GL_TEXTURE_2D', -level )
                
                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

                OpenGLES1.Disable( 'GL_DITHER' )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                       
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
               

######################################################################
class NpotPaletteTextureMinFilterExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT palette compressed texture min
        filtering. Requires GL_OES_compressed_paletted_texture, required by
        OpenGLES1.1, and GL_OES_texture_npot extensions. NOTE: might not be
        supported.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        palette compressed red-and-white 2x2 checker texture is drawn across the
        plane with texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). Dithering is disabled. Viewport size 320*240.
        '''

        testValues = { ( 'type', 'minFilter', 'wrap' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ]
                       }
        
        def __init__( self, type, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 NPOT palette compressed texture min filter ext, type=%s, filter=%s, wrap=%s" % ( paletteTypeToStr( self.type ),
                                                                                                                        filterToStr( self.minFilter ),
                                                                                                                        wrapToStr( self.wrap ), )

        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES1.CheckExtension( 'GL_OES_compressed_paletted_texture' )
                OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1

                level = 0
                w = textureW
                h = textureH
                for i in range( max( w, h ) ):
                        w /= 2
                        h /= 2
                        if w < 1 and h < 1:
                                break
                        level += 1                       
                        
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                                    [ 1.0, 0.0, 0.0, 1.0 ],
                                                    [ 1.0, 1.0, 1.0, 1.0 ],
                                                    2, 2,
                                                    textureW, textureH,
                                                    'ON',
                                                    TYPES[ self.type ] )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES1_COMPRESSED_DATA_INDEX' )
                OpenGLES1.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                              textureDataIndex,
                                                              self.type )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap,
                                        'OFF' )
                
                OpenGLES1.CompressedTexImage2DMML( compressedTextureDataIndex, 'GL_TEXTURE_2D', -level )
                
                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )

                OpenGLES1.Disable( 'GL_DITHER' )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                       
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

