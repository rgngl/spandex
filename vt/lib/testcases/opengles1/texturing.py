# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles1common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
                                       
######################################################################
class TextureWrap( vt.VisualTestCase ):

        ''' The purpose of the test: test texture wrapping.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'wrapS', 'wrapT' ) : [ ( 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                ( 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                ( 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                ( 'GL_REPEAT', 'GL_REPEAT' ),
                                             ]
                       }
        
        def __init__( self, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 texture wrap, wrapS=%s, wrapT=%s" % ( wrapToStr( self.wrapS ),
                                                                             wrapToStr( self.wrapT ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes   = target.getDefaultAttributes( apis )
                state               = target.setup( modules,
                                                    indexTracker,
                                                    WIDTH, HEIGHT,
                                                    apis,
                                                    defaultAttributes )

                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
                
                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES1_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT,
                                        'OFF' )
                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
######################################################################
class NpotTextureWrapExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT texture wrapping. Requires
        GL_OES_texture_npot extension. NOTE: might not be supported.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top).  Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'wrapS', 'wrapT' ) : [ ( 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                ( 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                ( 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                ( 'GL_REPEAT', 'GL_REPEAT' ),
                                             ]
                       }
        
        def __init__( self, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 NPOT texture wrap ext, wrapS=%s, wrapT=%s" % ( wrapToStr( self.wrapS ),
                                                                                      wrapToStr( self.wrapT ), )
                
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )                
                
                mesh                = createPlane( self.gridx, self.gridy , 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
                
                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES1_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT,
                                        'OFF' )
                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class TextureMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test texture min filtering.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'minFilter', 'wrap' ) : [ ( 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 texture min filter, filter=%s, wrap=%s" % ( filterToStr( self.minFilter ),
                                                                                   wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes   = target.getDefaultAttributes( apis )
                state               = target.setup( modules,
                                                    indexTracker,
                                                    WIDTH, HEIGHT,
                                                    apis,
                                                    defaultAttributes )

                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
                
                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'ON',
                                                     'OPENGLES1_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap,
                                        'OFF' )
                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class NpotTextureMinFilterExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT texture min filtering. Requires
        GL_OES_texture_npot extension. NOTE: might not be supported.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'minFilter', 'wrap' ) : [ ( 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 NPOT texture min filter ext, filter=%s, wrap=%s" % ( filterToStr( self.minFilter ),
                                                                                            wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )
                
                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'ON',
                                                     'OPENGLES1_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap,
                                        'OFF' )
                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class TextureMagFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test texture mag filtering.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'magFilter', 'wrap' ) : [ ( 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR', 'GL_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, magFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.magFilter   = magFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 texture mag filter, filter=%s, wrap=%s" % ( filterToStr( self.magFilter ),
                                                                                   wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [API_OPENGLES1 ]
                defaultAttributes   = target.getDefaultAttributes( apis )
                state               = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

                textureW = toLowerPot( ( WIDTH - 1 ) / 2.5, True )[ 0 ]
                textureH = toLowerPot( ( HEIGHT - 1 )/ 2.5, True )[ 0 ]

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES1_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        self.magFilter,
                                        self.wrap,
                                        self.wrap,
                                        'OFF' )
                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class NpotTextureMagFilterExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT texture mag filtering. Requires
        GL_OES_texture_npot extension. NOTE: might not be supported.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'magFilter', 'wrap' ) : [ ( 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR', 'GL_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, magFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.magFilter   = magFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 NPOT texture mag filter ext, filter=%s, wrap=%s" % ( filterToStr( self.magFilter ),
                                                                                            wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )
                
                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

                textureW = toLowerPot( ( WIDTH - 1 ) / 2.5, True )[ 0 ]
                textureH = toLowerPot( ( HEIGHT - 1 )/ 2.5, True )[ 0 ]

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES1_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        self.magFilter,
                                        self.wrap,
                                        self.wrap,
                                        'OFF' )
                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class TextureGenerateMipmap( vt.VisualTestCase ):

        ''' The purpose of the test: test texture mipmap generation.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'minFilter', 'wrap' ) : [ ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 texture generate mipmap, filter=%s, wrap=%s" % ( filterToStr( self.minFilter ),
                                                                                         wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes   = target.getDefaultAttributes( apis )
                state               = target.setup( modules,
                                                    indexTracker,
                                                    WIDTH, HEIGHT,
                                                    apis,
                                                    defaultAttributes )

                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES1_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap,
                                        'ON' )
                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class NpotTextureGenerateMipmap( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT texture generate mipmap. Requires
        GL_OES_texture_npot extension. NOTE: might not be supported.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be black, upper-right corner should be
        white. Viewport size 320*240.
        '''

        testValues = { ( 'minFilter', 'wrap' ) : [ ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                   ( 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                   ]
                       }
        
        def __init__( self, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 NPOT texture generate mipmap, filter=%s, wrap=%s" % ( filterToStr( self.minFilter ),
                                                                                             wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes   = target.getDefaultAttributes( apis )
                state               = target.setup( modules,
                                                    indexTracker,
                                                    WIDTH, HEIGHT,
                                                    apis,
                                                    defaultAttributes )

                OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )
                
                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     'OPENGLES1_RGBA8888' )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap,
                                        'ON' )
                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
               
                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class TextureSubImage( vt.VisualTestCase ):

        ''' The purpose of the test: test texture subimage.

        Expected output: a full screen plane with a 4x4 vertex grid. A texture
        is drawn across the plane with GL_REPEAT wrapping mode, texture
        coordinates running from 0.0 to 2.5 (left-right, bottom-top). Lower-left
        corner of the texture should be black, upper-right quarter should
        contain a subtexture with a gradient color; lower-left corner of the
        subtexture should be white and the top-right corner should be blue
        (black with luminance textures). Viewport size 320*240.
        '''
        testValues = { ( 'textureType' ) : [ ( 'OPENGLES1_LUMINANCE8' ),
                                             ( 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                             ( 'OPENGLES1_RGB565' ),
                                             ( 'OPENGLES1_RGB888' ),
                                             ( 'OPENGLES1_RGBA4444' ),
                                             ( 'OPENGLES1_RGBA5551' ),
                                             ( 'OPENGLES1_RGBA8888' ),
                                             ]
                       }

        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 texture subimage, texture type=%s" % ( textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes   = target.getDefaultAttributes( apis )
                state               = target.setup( modules,
                                                    indexTracker,
                                                    WIDTH, HEIGHT,
                                                    apis,
                                                    defaultAttributes )

                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
                
                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureData1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureData1Index,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 0.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_REPEAT',
                                        'GL_REPEAT',
                                        'OFF' )
                OpenGLES1.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )

                textureData2Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureData2Index,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     [ 0.0, 0.0, 1.0, 1.0 ],
                                                     textureW / 2, textureH / 2,
                                                     'OFF',
                                                     self.textureType )
                
                OpenGLES1.TexSubImage2D( textureData2Index,
                                         'GL_TEXTURE_2D',
                                         textureW / 2, textureH / 2 )
                
                textureSetup = TextureSetup( textureData1Index, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )                
                
######################################################################
class CopyTexImage( vt.VisualTestCase ):

        ''' The purpose of the test: test copy texture image. Requires
        EGL. NOTE: some color formats might not be supported.

        Expected output: a full screen plane with a 4x4 vertex grid. A texture
        is drawn across the plane with GL_REPEAT wrapping mode, texture
        coordinates running from 0.0 to 2.5 (left-right, bottom-top). Texture is
        divided in four rectangles, red in bottom-left (alpha 0.2), green in
        bottom-right (alpha 0.4), blue in top-left (alpha 0.6), and white in
        top-right (alpha 0.8). With luminance textures, bottom-left and
        top-right rectangles are white, others black. Textures with alpha are
        blended to [0.6,0.6,0.6,0.6] background. Dithering is disabled. Viewport
        size 320*240.
        '''
        testValues = { ( 'bufferFormat', 'textureType' ) : [ ( 'SCT_COLOR_FORMAT_RGBA4444', 'OPENGLES1_ALPHA8' ),
                                                             ( 'SCT_COLOR_FORMAT_RGB565',   'OPENGLES1_LUMINANCE8' ),
                                                             ( 'SCT_COLOR_FORMAT_RGBA4444', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                             ( 'SCT_COLOR_FORMAT_RGBA4444', 'OPENGLES1_RGBA4444' ),
                                                             ( 'SCT_COLOR_FORMAT_RGBA5551', 'OPENGLES1_RGBA5551' ),
                                                             ( 'SCT_COLOR_FORMAT_RGB565',   'OPENGLES1_RGB565' ),
                                                             ]
                       }

        def __init__( self, bufferFormat, textureType ):
                vt.VisualTestCase.__init__( self )
                self.bufferFormat = bufferFormat
                self.textureType  = textureType
                self.repeats      = 1
                self.gridx        = GRIDX
                self.gridy        = GRIDY
                self.overdraw     = 1
                self.name = "OPENGLES1 copy teximage, buffer format=%s, texture type=%s" % ( self.bufferFormat[ 17: ],
                                                                                             textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Egl       = modules[ 'Egl' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]                
                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
                Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
                Egl.Initialize( displayIndex )
        
                Egl.BindApi( 'EGL_OPENGL_ES_API' )

                configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )

                a = mapColorFormatToAttributes( self.bufferFormat )
        
                configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
                Egl.RGBConfig( displayIndex,
                               configIndex,
                               a[ CONFIG_BUFFERSIZE ],
                               a[ CONFIG_REDSIZE ],
                               a[ CONFIG_GREENSIZE ],
                               a[ CONFIG_BLUESIZE ],
                               a[ CONFIG_ALPHASIZE ],
                               '-',
                               '-',
                               0,
                               '>=0',
                               '-',                       
                               'EGL_DONT_CARE',
                               'EGL_DONT_CARE',
                               [ 'EGL_PBUFFER_BIT' ],
                               [ 'EGL_OPENGL_ES_BIT' ] )

                surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                Egl.CreatePbufferSurface( displayIndex,
                                          surfaceIndex,
                                          configIndex,
                                          textureW,
                                          textureH,
                                          'EGL_NO_TEXTURE',
                                          'EGL_NO_TEXTURE',
                                          'OFF',
                                          'OFF',
                                          'EGL_NONE',
                                          'EGL_NONE' )
                
                contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
                Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, -1 )
                Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

                OpenGLES1.Viewport( 0, 0, textureW, textureH );
                OpenGLES1.Disable( 'GL_DITHER' )
                
                w = textureW / 2
                h = textureH / 2
                x = ( textureW - w ) / 2
                y = ( textureH - h ) / 2 

                OpenGLES1.Enable( 'GL_SCISSOR_TEST' )                
                OpenGLES1.Scissor( 0, 0, textureW / 2, textureH / 2 )
                OpenGLES1.ClearColor( [ 1.0, 0.0, 0.0, 0.20 ] )
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.Scissor( textureW / 2, 0, textureW / 2, textureH / 2 )
                OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 0.4 ] )
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.Scissor( 0, textureH / 2, textureW / 2, textureH / 2 )
                OpenGLES1.ClearColor( [ 0.0, 0.0, 1.0, 0.6 ] )
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                
                OpenGLES1.Scissor( textureW / 2, textureH / 2, textureW / 2, textureH / 2 )
                OpenGLES1.ClearColor( [ 1.0, 1.0, 1.0, 0.8 ] )
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.Disable( 'GL_SCISSOR_TEST' )
                
                textureData1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateNullTextureData( textureData1Index,
                                                 textureW, textureH,
                                                 'OFF',
                                                 self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_REPEAT',
                                        'GL_REPEAT',
                                        'OFF' )
                OpenGLES1.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )

                internalFormats = { 'OPENGLES1_LUMINANCE8'        : 'GL_LUMINANCE',
                                    'OPENGLES1_ALPHA8'            : 'GL_ALPHA',
                                    'OPENGLES1_LUMINANCE_ALPHA88' : 'GL_LUMINANCE_ALPHA',
                                    'OPENGLES1_RGB565'            : 'GL_RGB',
                                    'OPENGLES1_RGB888'            : 'GL_RGB',
                                    'OPENGLES1_RGBA4444'          : 'GL_RGBA',
                                    'OPENGLES1_RGBA5551'          : 'GL_RGBA',
                                    'OPENGLES1_RGBA8888'          : 'GL_RGBA',
                                    }
                
                OpenGLES1.CopyTexImage2D( 'GL_TEXTURE_2D',
                                          0,
                                          internalFormats[ self.textureType ],
                                          0, 0,
                                          textureW, textureH )
                                          
                Egl.MakeCurrent( displayIndex, -1, -1, -1 )
                
                config2Index = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
                defaultAttributes = target.getDefaultAttributes( apis )
                Egl.RGBConfig( displayIndex,
                               config2Index,
                               defaultAttributes[ CONFIG_BUFFERSIZE ],
                               defaultAttributes[ CONFIG_REDSIZE ],
                               defaultAttributes[ CONFIG_GREENSIZE ],
                               defaultAttributes[ CONFIG_BLUESIZE ],
                               defaultAttributes[ CONFIG_ALPHASIZE ],
                               '-',
                               '-',
                               0,
                               '>=0',
                               '-',                       
                               'EGL_DONT_CARE',
                               'EGL_DONT_CARE',
                               [ 'EGL_PBUFFER_BIT' ],
                               [ 'EGL_OPENGL_ES_BIT' ] )

                surface2Index = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                Egl.CreatePbufferSurface( displayIndex,
                                          surface2Index,
                                          config2Index,
                                          WIDTH,
                                          HEIGHT,
                                          'EGL_NO_TEXTURE',
                                          'EGL_NO_TEXTURE',
                                          'OFF',
                                          'OFF',
                                          'EGL_NONE',
                                          'EGL_NONE' )
                
                context2Index = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
                Egl.CreateContext( displayIndex, context2Index, config2Index, contextIndex, -1 )
                Egl.MakeCurrentExt( displayIndex, surface2Index, surface2Index, context2Index )

                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
                
                textureSetup = TextureSetup( textureData1Index, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.Disable( 'GL_DITHER' )                
                OpenGLES1.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
                OpenGLES1.Enable( 'GL_BLEND' )
                OpenGLES1.ClearColor( [ 0.6, 0.6, 0.6, 0.6 ] )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions() 
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.LoadIdentity()
                drawMeshBufferData( modules, meshBufferDataSetup )
                
                OpenGLES1.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class CopyTexSubImage( vt.VisualTestCase ):

        ''' The purpose of the test: test copy texture subimage. NOTE: some
        color formats might not be supported.

        Expected output: a full screen plane with a 4x4 vertex grid. A gradient
        texture (ranging from [0.0,0.0,1.0,0.4] lower-left to [1.0,1.0,1.0,1.0]
        top-right) is drawn across the plane with GL_REPEAT wrapping mode,
        texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). The gradient texture shows a subimage in upper-right
        quarter, divided in four rectangles, red in bottom-left (alpha 0.2),
        green in bottom-right (alpha 0.4), blue in top-left (alpha 0.6), and
        white in top-right (alpha 0.8). With luminance textures, bottom-left and
        top-right rectangles are white, others black. Textures with alpha are
        blended to [0.6,0.6,0.6,0.6] background. Dithering is disabled. Viewport
        size 320*240.
        '''
        testValues = { ( 'bufferFormat', 'textureType' ) : [ ( 'SCT_COLOR_FORMAT_RGBA4444', 'OPENGLES1_ALPHA8' ),
                                                             ( 'SCT_COLOR_FORMAT_RGB565',   'OPENGLES1_LUMINANCE8' ),
                                                             ( 'SCT_COLOR_FORMAT_RGBA4444', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                             ( 'SCT_COLOR_FORMAT_RGBA4444', 'OPENGLES1_RGBA4444' ),
                                                             ( 'SCT_COLOR_FORMAT_RGBA5551', 'OPENGLES1_RGBA5551' ),
                                                             ( 'SCT_COLOR_FORMAT_RGB565',   'OPENGLES1_RGB565' ),
                                                             ]
                       }

        def __init__( self, bufferFormat, textureType ):
                vt.VisualTestCase.__init__( self )
                self.bufferFormat = bufferFormat
                self.textureType  = textureType
                self.repeats      = 1
                self.gridx        = GRIDX
                self.gridy        = GRIDY
                self.overdraw     = 1
                self.name = "OPENGLES1 copy tex subimage, buffer format=%s, texture type=%s" % ( self.bufferFormat[ 17: ],
                                                                                                 textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Egl       = modules[ 'Egl' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]                
                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
                Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
                Egl.Initialize( displayIndex )
        
                Egl.BindApi( 'EGL_OPENGL_ES_API' )

                configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )

                a = mapColorFormatToAttributes( self.bufferFormat )
        
                configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
                Egl.RGBConfig( displayIndex,
                               configIndex,
                               a[ CONFIG_BUFFERSIZE ],
                               a[ CONFIG_REDSIZE ],
                               a[ CONFIG_GREENSIZE ],
                               a[ CONFIG_BLUESIZE ],
                               a[ CONFIG_ALPHASIZE ],
                               '-',
                               '-',
                               0,
                               '>=0',
                               '-',                       
                               'EGL_DONT_CARE',
                               'EGL_DONT_CARE',
                               [ 'EGL_PBUFFER_BIT' ],
                               [ 'EGL_OPENGL_ES_BIT' ] )

                surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                Egl.CreatePbufferSurface( displayIndex,
                                          surfaceIndex,
                                          configIndex,
                                          textureW,
                                          textureH,
                                          'EGL_NO_TEXTURE',
                                          'EGL_NO_TEXTURE',
                                          'OFF',
                                          'OFF',
                                          'EGL_NONE',
                                          'EGL_NONE' )
                
                contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
                Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, -1 )
                Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

                OpenGLES1.Viewport( 0, 0, textureW, textureH );
                OpenGLES1.Disable( 'GL_DITHER' )                
                
                w = textureW / 2
                h = textureH / 2
                x = ( textureW - w ) / 2
                y = ( textureH - h ) / 2 

                OpenGLES1.Enable( 'GL_SCISSOR_TEST' )                
                OpenGLES1.Scissor( 0, 0, textureW / 4, textureH / 4 )
                OpenGLES1.ClearColor( [ 1.0, 0.0, 0.0, 0.20 ] )
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.Scissor( textureW / 4, 0, textureW / 4, textureH / 4 )
                OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 0.4 ] )
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.Scissor( 0, textureH / 4, textureW / 4, textureH / 4 )
                OpenGLES1.ClearColor( [ 0.0, 0.0, 1.0, 0.6 ] )
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                
                OpenGLES1.Scissor( textureW / 4, textureH / 4, textureW / 4, textureH / 4 )
                OpenGLES1.ClearColor( [ 1.0, 1.0, 1.0, 0.8 ] )
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.Disable( 'GL_SCISSOR_TEST' )
                               
                textureData1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureData1Index,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 1.0, 0.4 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'ON',
                                                     self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_REPEAT',
                                        'GL_REPEAT',
                                        'OFF' )
                OpenGLES1.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )

                OpenGLES1.CopyTexSubImage2D( 'GL_TEXTURE_2D',
                                             0,
                                             textureW / 2, textureH / 2,
                                             0, 0,
                                             textureW / 2, textureH / 2 )               
                                          
                Egl.MakeCurrent( displayIndex, -1, -1, -1 )
                
                config2Index = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
                defaultAttributes = target.getDefaultAttributes( apis )
                Egl.RGBConfig( displayIndex,
                               config2Index,
                               defaultAttributes[ CONFIG_BUFFERSIZE ],
                               defaultAttributes[ CONFIG_REDSIZE ],
                               defaultAttributes[ CONFIG_GREENSIZE ],
                               defaultAttributes[ CONFIG_BLUESIZE ],
                               defaultAttributes[ CONFIG_ALPHASIZE ],
                               '-',
                               '-',
                               0,
                               '>=0',
                               '-',                       
                               'EGL_DONT_CARE',
                               'EGL_DONT_CARE',
                               [ 'EGL_PBUFFER_BIT' ],
                               [ 'EGL_OPENGL_ES_BIT' ] )

                surface2Index = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
                Egl.CreatePbufferSurface( displayIndex,
                                          surface2Index,
                                          config2Index,
                                          WIDTH,
                                          HEIGHT,
                                          'EGL_NO_TEXTURE',
                                          'EGL_NO_TEXTURE',
                                          'OFF',
                                          'OFF',
                                          'EGL_NONE',
                                          'EGL_NONE' )
                
                context2Index = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
                Egl.CreateContext( displayIndex, context2Index, config2Index, contextIndex, -1 )
                Egl.MakeCurrentExt( displayIndex, surface2Index, surface2Index, context2Index )

                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
                
                textureSetup = TextureSetup( textureData1Index, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.Disable( 'GL_DITHER' )
                OpenGLES1.BlendFunc( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' )
                OpenGLES1.Enable( 'GL_BLEND' )
                OpenGLES1.ClearColor( [ 0.6, 0.6, 0.6, 0.6 ] )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions() 
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.LoadIdentity()
                drawMeshBufferData( modules, meshBufferDataSetup )
                
                OpenGLES1.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class GenerateMipmaps( vt.VisualTestCase ):

        ''' The purpose of the test: test mipmap generation.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be white, upper-right corner should be
        white. Viewport size 320*240.
        '''
        testValues = { ( 'textureType', 'hint' ) : [ ( 'OPENGLES1_LUMINANCE8',        'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_LUMINANCE_ALPHA88', 'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_RGB565',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_RGB565',            'GL_NICEST' ),
                                                     ( 'OPENGLES1_RGB565',            'GL_FASTEST' ),
                                                     ( 'OPENGLES1_RGB888',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_RGBA4444',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_RGBA5551',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_RGBA8888',          'GL_DONT_CARE' ),
                                                     ]
                       }
        
        def __init__( self, textureType, hint ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.hint        = hint
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 generate mipmaps, texture type=%s, hint=%s" % ( textureTypeToStr( self.textureType ),
                                                                                       self.hint[ 3: ], )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes   = target.getDefaultAttributes( apis )
                state               = target.setup( modules,
                                                    indexTracker,
                                                    WIDTH, HEIGHT,
                                                    apis,
                                                    defaultAttributes )

                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
                
                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 1.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES1.Hint( 'GL_GENERATE_MIPMAP_HINT', self.hint )
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR_MIPMAP_LINEAR',
                                        'GL_LINEAR',
                                        'GL_REPEAT',
                                        'GL_REPEAT',
                                        'ON' )
                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )                
                
                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class NpotGenerateMipmaps( vt.VisualTestCase ):

        ''' The purpose of the test: test mipmap generation with NPOT texture
        size. Requires GL_OES_texture_npot extension.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        gradient texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Lower-left corner of
        the texture should be blue, upper-right corner should be white. Viewport
        size 320*240.
        '''
        testValues = { ( 'textureType', 'hint' ) : [ ( 'OPENGLES1_LUMINANCE8',        'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_LUMINANCE_ALPHA88', 'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_RGB565',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_RGB565',            'GL_NICEST' ),
                                                     ( 'OPENGLES1_RGB565',            'GL_FASTEST' ),
                                                     ( 'OPENGLES1_RGB888',            'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_RGBA4444',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_RGBA5551',          'GL_DONT_CARE' ),
                                                     ( 'OPENGLES1_RGBA8888',          'GL_DONT_CARE' ),
                                                     ]
                       }
        
        def __init__( self, textureType, hint ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.hint        = hint
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 generate NPOT mipmaps, texture type=%s, hint=%s" % ( textureTypeToStr( self.textureType ),
                                                                                            self.hint[ 3: ], )
                
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )
                
                mesh                = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
                
                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
                OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                                     'OPENGLES1_GRADIENT_DIAGONAL',
                                                     [ 0.0, 0.0, 1.0, 1.0 ],
                                                     [ 1.0, 1.0, 1.0, 1.0 ],
                                                     textureW, textureH,
                                                     'OFF',
                                                     self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
                OpenGLES1.GenTexture( textureIndex )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )

                OpenGLES1.Hint( 'GL_GENERATE_MIPMAP_HINT', self.hint )                
                OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR_MIPMAP_LINEAR',
                                        'GL_LINEAR',
                                        'GL_REPEAT',
                                        'GL_REPEAT',
                                        'ON' )
                OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
                
                textureSetup = TextureSetup( textureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
