#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

WIDTH, HEIGHT = ( 320, 240, )

# ----------------------------------------------------------------------
class Scissor( vt.VisualTestCase ):
    
    """ Testing glScissor. The scissor rectangle is placed on the lower left and
    upper right corners. The image should be cut into 4 pieces, 2 for each quad."""
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.name = 'OPENGLES1 scissor'
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # quad vertices
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )  # quad colors
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' ) # quad indices
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES1.ClearColor( [ 0, 0, 0, 1 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Color( [ 0.3, 0.8, 0.8, 1.0 ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.Scissor(0, 0, WIDTH / 2, HEIGHT / 2 )
        OpenGLES1.Enable( 'GL_SCISSOR_TEST' )
        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.5, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT','GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Color( [ 0.0, 0.0, 1.0, 1.0 ] )
        OpenGLES1.Rotate( 40.0, [ 0.0, 0.0, 1.0 ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.Scissor( WIDTH / 2, HEIGHT / 2, WIDTH / 2, HEIGHT / 2 )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT','GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------       
class SampleCoverage( vt.VisualTestCase ):
    
    """Testing glSampleCoverage."""
    
    testValues = { 'mode' : [ True, False ]
                   }
    
    def __init__( self, mode ):
        vt.VisualTestCase.__init__( self )
        self.mode = mode
        self.name = "OPENGLES1 sample coverage GL_MULTISAMPLE %s" % ( { True : 'enabled', False : 'disabled' }[ self.mode ] )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable( 'GL_LIGHTING' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Enable( 'GL_NORMALIZE' )
        OpenGLES1.Enable( 'GL_CULL_FACE' )
        
        if self.mode == True:
            OpenGLES1.Enable( 'GL_MULTISAMPLE' )
        else:
            OpenGLES1.Disable( 'GL_MULTISAMPLE' )
            
        OpenGLES1.Enable( 'GL_SAMPLE_COVERAGE' )
        OpenGLES1.SampleCoverage( 1.0, 'OFF' )
        
        light = { 'Ambient'              : [ 0.0, 0.0, 0.0, 1.0 ],
                  'Diffuse'              : [ 0.3, 0.3, 0.3, 1.0 ],
                  'Specular'             : [ 0.5, 0.5, 0.0, 1.0 ],
                  'Position'             : [ 1.0, 1.0, 1.0, 0.0 ],
                  'SpotDirection'        : [ 0.0, 0.0, -1.0 ],
                  'SpotExponent'         : 0.0,
                  'SpotCutoff'           : 180.0,
                  'ConstantAttenuation'  : 1.0,
                  'LinearAttenuation'    : 0.0,
                  'QuadraticAttenuation' : 0.0,
                  }
        OpenGLES1.Enable( 'GL_LIGHT0' )
        OpenGLES1.Light( 0,
                         light[ 'Ambient' ],
                         light[ 'Diffuse' ],
                         light[ 'Specular' ],
                         light[ 'Position' ],
                         light[ 'SpotDirection' ],
                         light[ 'SpotExponent' ],
                         light[ 'SpotCutoff' ],
                         light[ 'ConstantAttenuation' ],
                         light[ 'LinearAttenuation' ],
                         light[ 'QuadraticAttenuation' ]
                         )
                
        OpenGLES1.LightModelTwoSided( 'ON' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )
        
        data = loadMesh( "sphere.mesh" )
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'normals' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.NormalPointer( 1, 0 )
        OpenGLES1.Material( data[ 'material' ][ 'ambient' ],
                            data[ 'material' ][ 'diffuse' ],
                            data[ 'material' ][ 'specular' ],
                            data[ 'material' ][ 'emission' ],
                            data[ 'material' ][ 'shininess' ]
                            )
        OpenGLES1.Translate( [ 0, 0.0, -3.1 ] )
        OpenGLES1.Rotate( 20.0, [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Rotate( 20.0, [ 0.0, 1.0, 0.0 ] )
        OpenGLES1.Scale( [ 6.1, 6.1, 6.1 ] )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------        
class StencilFunc( vt.VisualTestCase ):
    
    """ Testing StencilFunc. The mesh is first drawn normally with lighting to
    create stencil data, then a smaller and a bigger mesh are drawn with stencil
    testing enabled."""
    
    testValues = { 'func' : [ 'GL_NEVER',
                              'GL_LESS',
                              'GL_LEQUAL',
                              'GL_GREATER',
                              'GL_GEQUAL',
                              'GL_EQUAL',
                              'GL_NOTEQUAL',
                              'GL_ALWAYS' ]
                   }
       
    def __init__( self, func ):
        vt.VisualTestCase.__init__( self )
        self.func = func
        self.name = 'OPENGLES1 stencil func %s' % ( self.func, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis    = [ API_OPENGLES1 ]
        attribs = target.getDefaultAttributes( apis )
        attribs[ CONFIG_STENCILSIZE ] = '>=8'
        
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              attribs )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable('GL_LIGHTING')
        OpenGLES1.Enable('GL_NORMALIZE')
        OpenGLES1.Enable('GL_LIGHT0')
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )          # normals 
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )
        
        data = loadMesh( "sphere.mesh" )
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'normals' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.NormalPointer( 1,0 )
        OpenGLES1.StencilMask( '0xffffffff' )
        OpenGLES1.ClearStencil( 0x01 )
        OpenGLES1.Translate( [ 0, 0.0, -3.1 ] )
        OpenGLES1.Rotate( 20.0, [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Rotate( 20.0, [ 0.0, 1.0, 0.0 ] )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT', 'GL_STENCIL_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.Enable( 'GL_STENCIL_TEST' )
        
        # Set up stencil mask
        OpenGLES1.StencilFunc( 'GL_ALWAYS', 0x0, '0x00000001' )
        OpenGLES1.StencilMask( '0x00000001' )
        OpenGLES1.StencilOp( 'GL_KEEP', 'GL_KEEP', 'GL_REPLACE' )
        
        # Draw the main mesh
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.Disable( 'GL_DEPTH_TEST' )
        OpenGLES1.Disable( 'GL_LIGHTING' )
       
        # Enable blending
        OpenGLES1.Enable( 'GL_BLEND' )
        OpenGLES1.BlendFunc( 'GL_ONE','GL_ONE_MINUS_SRC_COLOR' )
        
        # Set up stencil mask again
        OpenGLES1.StencilFunc( self.func, 0x01, '0x00000001' )
        OpenGLES1.StencilMask( '0x00000001' )
        OpenGLES1.StencilOp( 'GL_KEEP','GL_KEEP','GL_REPLACE' )
        
        # Draw a small mesh
        OpenGLES1.PushMatrix()
        OpenGLES1.Scale( [ 0.5, 0.5, 0.5 ] )
        OpenGLES1.Color( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        
        # Draw a big mesh
        OpenGLES1.Color( [ 0.0, 1.0, 0.0, 1.0 ])
        OpenGLES1.Scale( [ 1.2, 1.2, 1.2 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
         
        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------
class StencilOp( vt.VisualTestCase ):
    
    """ Testing StencilOp. The mesh is first drawn normally with lighting to
    create stencil data, then a smaller and a bigger mesh are drawn with stencil
    testing enabled."""
    
    testValues = { 'fail' : [ 'GL_KEEP',
                              'GL_ZERO',
                              'GL_REPLACE',
                              'GL_INCR',
                              'GL_DECR',
                              'GL_INVERT'
                              ],
                   'zfail' : [ 'GL_KEEP',
                               'GL_ZERO',
                               'GL_REPLACE',
                               'GL_INCR',
                               'GL_DECR',
                               'GL_INVERT'
                               ],
                   'zpass' : [ 'GL_KEEP',
                               'GL_ZERO',
                               'GL_REPLACE',
                               'GL_INCR',
                               'GL_DECR',
                               'GL_INVERT'
                               ],
                   }
       
    def __init__( self, fail, zfail, zpass ):
        vt.VisualTestCase.__init__( self )
        self.fail  = fail
        self.zfail = zfail
        self.zpass = zpass
        self.name = 'OPENGLES1 stencil op %s %s %s' % ( self.fail, self.zfail, self.zpass, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis    = [ API_OPENGLES1 ]
        attribs = target.getDefaultAttributes( apis )
        attribs[ CONFIG_STENCILSIZE ] = '>=8'
        
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              attribs )
        
        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable('GL_LIGHTING')
        OpenGLES1.Enable('GL_NORMALIZE')
        OpenGLES1.Enable( 'GL_LIGHT0' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )
        
        data = loadMesh( "sphere.mesh" )
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'normals' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.NormalPointer( 1,0 )
        
        OpenGLES1.StencilMask( '0xffffffff' )
        OpenGLES1.ClearStencil( 0x01 )
        OpenGLES1.Translate( [ 0, 0.0, -3.1 ] )
        OpenGLES1.Rotate( 20.0, [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Rotate( 20.0, [ 0.0, 1.0, 0.0 ] )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT', 'GL_STENCIL_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.Enable( 'GL_STENCIL_TEST' )
        
        # Set up stencil mask
        OpenGLES1.StencilFunc( 'GL_ALWAYS', 0x0, '0x00000001' )
        OpenGLES1.StencilMask( '0x00000001' )
        OpenGLES1.StencilOp( 'GL_KEEP', 'GL_KEEP', 'GL_REPLACE' )
        
        # Draw the main mesh
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.Disable( 'GL_DEPTH_TEST' )
        OpenGLES1.Disable( 'GL_LIGHTING' )
        
        # Enable blending
        OpenGLES1.Enable( 'GL_BLEND' )
        OpenGLES1.BlendFunc( 'GL_ONE','GL_ONE_MINUS_SRC_COLOR' )
        
        # Set up stencil mask again
        OpenGLES1.StencilFunc( 'GL_NOTEQUAL', 0x01, '0x00000001' )
        OpenGLES1.StencilMask( '0x00000001' )
        OpenGLES1.StencilOp( self.fail, self.zfail, self.zpass )
        
        # Draw a small mesh
        OpenGLES1.PushMatrix()
        OpenGLES1.Scale( [ 0.5, 0.5, 0.5 ] )
        OpenGLES1.Color( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        
        # Draw a big mesh
        OpenGLES1.Color( [ 0.0, 1.0, 0.0, 1.0 ])
        OpenGLES1.Scale( [ 1.2, 1.2, 1.2 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
        
# ----------------------------------------------------------------------
class BlendFunc( vt.VisualTestCase ):
    
    """ Testing BlendFunc. Both triangles have alpha of 127 (as vertex
    colors). Destination color is (0.33,0.33,0.33,0.66)."""
    
    testValues = { 'sfactor' : [ 'GL_ZERO',
                                 'GL_ONE',
                                 'GL_DST_COLOR',
                                 'GL_ONE_MINUS_DST_COLOR',
                                 'GL_SRC_ALPHA',
                                 'GL_ONE_MINUS_SRC_ALPHA',
                                 'GL_DST_ALPHA',
                                 'GL_ONE_MINUS_DST_ALPHA',
                                 'GL_SRC_ALPHA_SATURATE'
                                 ],
                   'dfactor' : [ 'GL_ZERO',
                                 'GL_ONE',
                                 'GL_SRC_COLOR',
                                 'GL_ONE_MINUS_SRC_COLOR',
                                 'GL_SRC_ALPHA',
                                 'GL_ONE_MINUS_SRC_ALPHA',
                                 'GL_DST_ALPHA',
                                 'GL_ONE_MINUS_DST_ALPHA'
                                 ]
                   }
    
    def __init__( self, sfactor, dfactor ):
        vt.VisualTestCase.__init__( self )
        self.sfactor = sfactor
        self.dfactor = dfactor
        self.name = 'OPENGLES1 blend func %s %s' % ( self.sfactor, self.dfactor, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.ClearColor( [ 0.33, 0.33, 0.33, 0.66 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Ortho( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Enable( 'GL_BLEND' )
        OpenGLES1.BlendFunc( self.sfactor, self.dfactor )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        tri = triangle()
        colors = [ 128, 128, 128, 255,
                   255, 255, 255, 255,
                   128, 128, 128, 255,
                   ]
        
        for c in xrange( len( colors ) / 4 ):
            i           = ( c * 4 ) + 3
            colors[ i ] = 127
            
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, colors )
        OpenGLES1.AppendToArray( 2, tri[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ 0, 0, -0.1 ] )
        OpenGLES1.Rotate( 180.0, [ 0.0, 0.0, 1.0 ] )
        OpenGLES1.Scale( [ 4, 4, 4 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.Scale( [ 4, 4, 4 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------        
class Dither( vt.VisualTestCase ):
    
    """ Testing dithering. Surface color precision is set to 16 bits (RGB565)
    for this test."""
    
    testValues = { 'enable' : [ True, False ],
                   }
        
    def __init__( self, enable ):
        vt.VisualTestCase.__init__( self )
        self.enable = enable
        self.name = 'OPENGLES1 dither %s' % ( { True : 'enabled', False : 'disabled' }[ self.enable ] )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis    = [ API_OPENGLES1 ]
        attribs = target.getDefaultAttributes( apis )
        
        attribs[ CONFIG_BUFFERSIZE ]     = 16
        attribs[ CONFIG_REDSIZE ]        = 5
        attribs[ CONFIG_GREENSIZE ]      = 6
        attribs[ CONFIG_BLUESIZE ]       = 5
        attribs[ CONFIG_ALPHASIZE ]      = 0       
        
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              attribs )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        if self.enable:
            OpenGLES1.Enable( 'GL_DITHER' )
        else:
            OpenGLES1.Disable( 'GL_DITHER' )
            
        tri = triangle()
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------
class LogicOp( vt.VisualTestCase ):
    
    """Testing LogicOp. Destination color is (0.33,0.33,0.33,0.66)."""
    
    testValues = { 'opcode' : [ 'GL_CLEAR',
                                'GL_SET',
                                'GL_COPY',
                                'GL_COPY_INVERTED',
                                'GL_NOOP',
                                'GL_INVERT',
                                'GL_AND',
                                'GL_NAND',
                                'GL_OR',
                                'GL_NOR',
                                'GL_XOR',
                                'GL_EQUIV',
                                'GL_AND_REVERSE',
                                'GL_AND_INVERTED',
                                'GL_OR_REVERSE',
                                'GL_OR_INVERTED' ],
                   'alpha' : [ True, False ]
                   }
    
    def __init__( self, opcode, alpha ):
        vt.VisualTestCase.__init__( self )
        self.opcode = opcode
        self.alpha  = alpha        
        self.name = 'OPENGLES1 logic op %s alpha %s' % ( self.opcode, { True : 'enabled', False : 'disabled' }[ self.alpha ], )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0.33, 0.3, 0.33, 0.66 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Ortho( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.LogicOp( self.opcode )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        tri = triangle()
        
        # Change the alpha values
        if self.alpha:
            for c in xrange( len( tri[ 'colorsb' ] ) / 4 ):
                i = c * 3 + 3
                tri[ 'colorsb' ][ i ] = 127
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Enable( 'GL_COLOR_LOGIC_OP' )
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ 0, 0, -0.1 ] )
        OpenGLES1.Rotate( 180.0, [ 0.0, 0.0, 1.0 ])
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------        
class AlphaFunc( vt.VisualTestCase ):
    
    """ Testing AlphaFunc. Two triangles are drawn with alpha testing
    enabled. The triangles have alpha as vertex colors, increasing for each
    vertex (0-255). For the GL_EQUAL/GL_NOTEQUAL cases, the triangle alpha
    channels are filled with the reference alpha value."""
    
    testValues = { 'func' : [ 'GL_NEVER',
                              'GL_ALWAYS',
                              'GL_LESS',
                              'GL_LEQUAL',
                              'GL_EQUAL',
                              'GL_GEQUAL',
                              'GL_GREATER',
                              'GL_NOTEQUAL'
                              ],
                   'ref' : [ 0.0, 1.0 ],
                   }
    
    def __init__( self, func, ref ):
        vt.VisualTestCase.__init__( self )
        self.func = func
        self.ref  = ref
        self.name = 'OPENGLES1 alpha func %s %s' % ( self.func, self.ref, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Ortho( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Enable( 'GL_ALPHA_TEST' )
        OpenGLES1.AlphaFunc( self.func, self.ref )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        vertices = [ -0.5, -0.5, 0.0,
                      0.0,  0.5, 0.0,
                      0.5, -0.5, 0.0,
                        
                     -0.5,  0.5, 0.0,
                      0.0, -0.5, 0.0,
                      0.5,  0.5, 0.0
                      ]
                        
        indices = [ 0, 1, 2, 3, 4, 5 ]

        colors = [ 128, 128, 128, 255,
                   255, 255, 255, 255,
                   128, 128, 128, 255,
                   
                   128, 128, 128, 0,
                   255, 255, 255, 0,
                   128, 128, 128, 0,
                   ]

        OpenGLES1.AppendToArray( 0, vertices )
        OpenGLES1.AppendToArray( 1, colors )
        OpenGLES1.AppendToArray( 2, indices )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ 0, 0, -0.1 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', 6, 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
