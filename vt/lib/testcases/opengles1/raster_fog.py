#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

WIDTH, HEIGHT = ( 320, 240, )

# ----------------------------------------------------------------------
class Fog( vt.VisualTestCase ):
    
    """Testing glFog. NOTE: OpenGLES1 fog implementations may vary considerably."""
    
    testValues = { 'mode' : [ 'GL_LINEAR',
                              'GL_EXP',
                              'GL_EXP2' ],
                   'fogcolor' : [ [ 0.0, 0.0, 0.0, 0.5 ],
                                  [ 0.0, 0.0, 0.5, 0.5 ],
                                  [ 0.0, 0.5, 0.0, 0.5 ],
                                  [ 0.5, 0.0, 0.0, 0.5 ],
                                  [ 0.5, 0.5, 0.0, 0.5 ],
                                  [ 0.5, 0.0, 0.5, 0.5 ],
                                  [ 0.0, 0.5, 0.5, 0.5 ],
                                  [ 1.0, 1.0, 1.0, 0.5 ] ]
                   }
    
    def __init__( self, mode, fogcolor ):
        vt.VisualTestCase.__init__( self )
        self.mode = mode
        self.fogcolor = fogcolor
        self.name = "OPENGLES1 fog %s %s" % ( self.mode, self.fogcolor, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )
        OpenGLES1.Color( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.Enable( 'GL_FOG' )
        OpenGLES1.Enable( 'GL_LIGHTING' )
        OpenGLES1.Enable('GL_LIGHT0')
        OpenGLES1.Enable( 'GL_COLOR_MATERIAL' )
        OpenGLES1.Enable( 'GL_CULL_FACE' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Fog( self.mode, 1.0, 0.1, 5.0, self.fogcolor )
        
        cube = loadMesh( "tube.mesh" )
        OpenGLES1.AppendToArray( 0, cube[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, cube[ 'normals' ] )
        OpenGLES1.AppendToArray( 2, cube[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.NormalPointer( 1, 0 )
        
        OpenGLES1.Material( cube[ 'material' ][ 'ambient' ],
                            cube[ 'material' ][ 'diffuse' ],
                            cube[ 'material' ][ 'specular' ],
                            cube[ 'material' ][ 'emission' ],
                            cube[ 'material' ][ 'shininess' ]
                            )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Translate( [ 0.0, 0.0, -1.0 ] )
        y = 0.0
        
        for x in [ -1.2, 1.2 ]:
            OpenGLES1.PushMatrix()
            OpenGLES1.Translate( [ x, y, 0.0 ] )
            for z in range( 40 ):
                OpenGLES1.Translate( [ 0.0, 0.0, -1.0 ] )
                OpenGLES1.PushMatrix()
                OpenGLES1.Rotate( 40.0, [ 1.0, 0.0, 0.0 ] )
                OpenGLES1.DrawElements( 'GL_TRIANGLES', len( cube[ 'indices' ] ), 0, 2 )
                OpenGLES1.PopMatrix()
            OpenGLES1.PopMatrix()

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
