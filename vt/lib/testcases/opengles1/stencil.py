# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles1common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )

# ----------------------------------------------------------------------
def createPlane( gridx, gridy ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh


# ----------------------------------------------------------------------
class BasicStencil( vt.VisualTestCase ):

        ''' The purpose of the test: test basic stencil function.

        Expected output: a blue rectangle in the middle of a red
        rectangle. First, the buffers are cleared, color to [0,0,0,1] and
        stencil to 1. Secondly a rectangle at the center of stencil buffer is
        cleared to 2 using scissoring. Stencil function is set as GL_EQUAL, 2
        0xFF, stencil operation is set as GL_INCR, GL_KEEP, GL_INCR. Stencil and
        depth tests are enabled, depth test is set GL_LEQUAL so drawing always
        passes. Finally, the test first draws a blue full screen rect, followed
        by a red full screen screen rect. Viewport size 320*240.
        '''

        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats  = 1
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.overdraw = 1
                self.name = "OPENGLES1 basic stencil"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_STENCILSIZE ] = '>=8'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                useVertexData( modules, indexTracker, vertexDataSetup1 )

                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                OpenGLES1.ClearStencil( 1 )

                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_STENCIL_BUFFER_BIT' ] )
                
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2
                
                OpenGLES1.Scissor( x, y, w, h )
                OpenGLES1.Enable( 'GL_SCISSOR_TEST' )

                OpenGLES1.ClearStencil( 2 )
                OpenGLES1.Clear( [ 'GL_STENCIL_BUFFER_BIT' ] )
                OpenGLES1.Disable( 'GL_SCISSOR_TEST' )

                OpenGLES1.StencilFunc( 'GL_EQUAL', 2, '0xFF' )
                OpenGLES1.StencilOp( 'GL_INCR', 'GL_KEEP', 'GL_INCR' )
                OpenGLES1.Enable( 'GL_STENCIL_TEST' )

                OpenGLES1.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0, 0, 0.7 ] )
                drawVertexData( modules, vertexDataSetup1 )

                useVertexData( modules, indexTracker, vertexDataSetup2 )
                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0, 0, 0.5 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES1.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
# ----------------------------------------------------------------------
class StencilFunc( vt.VisualTestCase ):

        ''' The purpose of the test: test stencil function.

        Expected output: First, the buffers are cleared, color to
        [0.5,0.5,0.5,1.0] and stencil to 1. Secondly a rectangle at the center
        of stencil buffer is cleared to 2. Stencil function is set as specified
        by the test, stencil reference is set to 2 and mask as 0xFF. Stencil
        operations are all set to GL_INCR. Stencil and depth tests are enabled,
        depth function is set as GL_LEQUAL so that depth test should always
        pass. Finally, the test first draws a blue full screen rect, followed by
        a red full screen rect. Viewport size 320*240.
        '''
        testValues = { ( 'stencilFunc' ) : [ ( 'GL_NEVER' ),
                                             ( 'GL_LESS' ),
                                             ( 'GL_LEQUAL' ),
                                             ( 'GL_GREATER' ),
                                             ( 'GL_GEQUAL' ),
                                             ( 'GL_EQUAL' ),
                                             ( 'GL_NOTEQUAL' ),
                                             ( 'GL_ALWAYS' )
                                             ]
                       }

        def __init__( self, stencilFunc ):
                vt.VisualTestCase.__init__( self )
                self.repeats     = 1
                self.stencilFunc = stencilFunc
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 stencil func, stencil func=%s" % ( stencilFuncToStr( self.stencilFunc ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_STENCILSIZE ] = '>=8'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                useVertexData( modules, indexTracker, vertexDataSetup1 )

                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                OpenGLES1.ClearStencil( 1 )

                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_STENCIL_BUFFER_BIT' ] )
                
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2
                
                OpenGLES1.Scissor( x, y, w, h )
                OpenGLES1.Enable( 'GL_SCISSOR_TEST' )

                OpenGLES1.ClearStencil( 2 )
                OpenGLES1.Clear( [ 'GL_STENCIL_BUFFER_BIT' ] )
                OpenGLES1.Disable( 'GL_SCISSOR_TEST' )

                OpenGLES1.StencilFunc( self.stencilFunc, 2, '0xFF' )
                OpenGLES1.StencilOp( 'GL_INCR', 'GL_INCR', 'GL_INCR' )
                OpenGLES1.Enable( 'GL_STENCIL_TEST' )

                OpenGLES1.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0, 0, 0.7 ] )
                drawVertexData( modules, vertexDataSetup1 )

                useVertexData( modules, indexTracker, vertexDataSetup2 )

                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0, 0, 0.5 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES1.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
# ----------------------------------------------------------------------
def createPlane2( gridx, gridy, winding ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        mesh = MeshTrianglePlane( gridx,
                                  gridy,
                                  vertexAttribute,
                                  colorAttribute,
                                  None,
                                  texCoordAttribute,
                                  indexAttribute,
                                  None,
                                  winding,
                                  False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh


# ----------------------------------------------------------------------
class StencilOpMask( vt.VisualTestCase ):

        ''' The purpose of the test: test stencil mask.

        Expected output: First, the buffers are cleared, color to
        [0.5,0.5,0.5,1.0] and stencil to 13. Secondly a rectangle at the center
        of stencil buffer is cleared to 15. Stencil function is set as specified
        by the test, stencil reference is set to 2 and mask as 0x3. Stencil
        operations are all set to GL_INCR. Stencil test is enabled. Finally, the
        test first draws a blue full screen rect, followed by a red full screen
        rect. Rectangles use alternating winding, i.e. even columns use CCW, odd
        columns CW. Viewport size 320*240.
        '''

        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats     = 1
                self.gridx       = 8
                self.gridy       = 8
                self.overdraw    = 1
                self.name = "OPENGLES1 stencil op mask"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_STENCILSIZE ] = '>=8'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh1 = createPlane2( self.gridx, self.gridy, MESH_ALTERNATING_WINDING )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                mesh2 = createPlane2( self.gridx, self.gridy, MESH_ALTERNATING_WINDING )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                useVertexData( modules, indexTracker, vertexDataSetup1 )

                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
                OpenGLES1.ClearStencil( 13 )

                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_STENCIL_BUFFER_BIT' ] )
                
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2
                
                OpenGLES1.Scissor( x, y, w, h )
                OpenGLES1.Enable( 'GL_SCISSOR_TEST' )

                OpenGLES1.ClearStencil( 15 )
                OpenGLES1.Clear( [ 'GL_STENCIL_BUFFER_BIT' ] )
                OpenGLES1.Disable( 'GL_SCISSOR_TEST' )

                OpenGLES1.StencilFunc( 'GL_LEQUAL', 2, '0x3' )
                OpenGLES1.StencilOp( 'GL_INCR', 'GL_INCR', 'GL_INCR' )
                
                OpenGLES1.Enable( 'GL_STENCIL_TEST' )

                OpenGLES1.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES1.LoadIdentity()
                drawVertexData( modules, vertexDataSetup1 )

                useVertexData( modules, indexTracker, vertexDataSetup2 )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES1.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
# ----------------------------------------------------------------------
class StencilMask( vt.VisualTestCase ):

        ''' The purpose of the test: test stencil mask.

        Expected output: a blue rectangle in the middle of a red
        rectangle. Viewport size 320*240.
        '''

        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats  = 1
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.overdraw = 1
                self.name = "OPENGLES1 stencil mask"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_STENCILSIZE ] = '>=8'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                useVertexData( modules, indexTracker, vertexDataSetup1 )

                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES1.ClearStencil( 0 )
                OpenGLES1.Clear( [ 'GL_STENCIL_BUFFER_BIT' ] )
                
                OpenGLES1.StencilMask( '0x3' )
                OpenGLES1.ClearStencil( 13 )

                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_STENCIL_BUFFER_BIT' ] )
                
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2
                
                OpenGLES1.Scissor( x, y, w, h )
                OpenGLES1.Enable( 'GL_SCISSOR_TEST' )

                OpenGLES1.ClearStencil( 14 )
                OpenGLES1.Clear( [ 'GL_STENCIL_BUFFER_BIT' ] )
                OpenGLES1.Disable( 'GL_SCISSOR_TEST' )

                OpenGLES1.StencilFunc( 'GL_EQUAL', 2, '0xFF' )
                OpenGLES1.StencilOp( 'GL_INCR', 'GL_KEEP', 'GL_INCR' )
                OpenGLES1.Enable( 'GL_STENCIL_TEST' )

                OpenGLES1.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES1.LoadIdentity()
                drawVertexData( modules, vertexDataSetup1 )

                useVertexData( modules, indexTracker, vertexDataSetup2 )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES1.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
# ----------------------------------------------------------------------
class StencilDpassOpWithNoDepthTest( vt.VisualTestCase ):

        ''' The purpose of the test: test stencil dpass option with depth test
        disabled.

        Expected output: a blue rectangle in the middle of a red
        rectangle. First, the buffers are cleared, color to [0,0,0,1] and
        stencil to 1. Secondly a rectangle at the center of stencil buffer is
        cleared to 2 using scissoring. Stencil function is set as GL_EQUAL, 2
        0xFF, stencil operation is set as GL_INCR, GL_KEEP, GL_INCR. Stencil
        test is enabled. Depth test is disabled so depth test should always
        pass. Finally, the test first draws a blue full screen rect, followed by
        a red full screen screen rect. Viewport size 320*240.
        '''

        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats  = 1
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.overdraw = 1
                self.name = "OPENGLES1 stencil dpass option with depth test disabled"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_STENCILSIZE ] = '>=8'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                useVertexData( modules, indexTracker, vertexDataSetup1 )

                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                OpenGLES1.ClearStencil( 1 )

                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_STENCIL_BUFFER_BIT' ] )
                
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2
                
                OpenGLES1.Scissor( x, y, w, h )
                OpenGLES1.Enable( 'GL_SCISSOR_TEST' )

                OpenGLES1.ClearStencil( 2 )
                OpenGLES1.Clear( [ 'GL_STENCIL_BUFFER_BIT' ] )
                OpenGLES1.Disable( 'GL_SCISSOR_TEST' )

                OpenGLES1.StencilFunc( 'GL_EQUAL', 2, '0xFF' )
                OpenGLES1.StencilOp( 'GL_INCR', 'GL_KEEP', 'GL_INCR' )
                OpenGLES1.Enable( 'GL_STENCIL_TEST' )

                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0, 0, 0.7 ] )
                drawVertexData( modules, vertexDataSetup1 )

                useVertexData( modules, indexTracker, vertexDataSetup2 )
                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0, 0, 0.5 ] )                
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES1.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
