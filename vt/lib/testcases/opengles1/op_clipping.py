#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

WIDTH, HEIGHT = ( 320, 240, )

# ----------------------------------------------------------------------
class ClipPlane( vt.VisualTestCase ):
    
    """ Testing glClipPlane. A sphere is dissected into a smaller object by
    different number of clipping planes. NOTE: OpenGLES1 specification requires
    support for only one clipping plane."""
    
    testValues = { 'numplanes' : [ 0,
                                   1,
                                   2,
                                   3,
                                   4,
                                   ]
                   }
    
    def __init__( self, numplanes ):
        vt.VisualTestCase.__init__( self )
        self.numplanes = numplanes
        self.clipplanes = [ [ 1.0, 0.0,  0.0,  0.1  ],
                            [ 1.0, 1.0,  0.0,  0.1  ],
                            [ 0.0, 1.0, -1.0, -0.01 ],
                            [ 0.0, 1.0,  1.0,  0.1  ],
                            ]
            
        self.name = "OPENGLES1 clip plane %d planes" % ( self.numplanes, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.ShadeModel( 'GL_SMOOTH' )
        OpenGLES1.Enable( 'GL_LIGHTING' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Enable( 'GL_NORMALIZE' )
        light = { 'Ambient'              : [ 0.0, 0.0, 0.0, 1.0 ],
                  'Diffuse'              : [ 0.3, 0.3, 0.3, 1.0 ],
                  'Specular'             : [ 0.5, 0.5, 0.0, 1.0 ],
                  'Position'             : [ 1.0, 1.0, 1.0, 0.0 ],
                  'SpotDirection'        : [ 0.0, 0.0, -1.0 ],
                  'SpotExponent'         : 0.0,
                  'SpotCutoff'           : 180.0,
                  'ConstantAttenuation'  : 1.0,
                  'LinearAttenuation'    : 0.0,
                  'QuadraticAttenuation' : 0.0,
                  }
        OpenGLES1.Light( 0,
                         light[ 'Ambient' ],
                         light[ 'Diffuse' ],
                         light[ 'Specular' ],
                         light[ 'Position' ],
                         light[ 'SpotDirection' ],
                         light[ 'SpotExponent' ],
                         light[ 'SpotCutoff' ],
                         light[ 'ConstantAttenuation' ],
                         light[ 'LinearAttenuation' ],
                         light[ 'QuadraticAttenuation' ]
                         )
        OpenGLES1.Enable('GL_LIGHT0')
        OpenGLES1.LightModelTwoSided( 'OFF' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )
        
        data = loadMesh( "sphere.mesh" )
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'normals' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.NormalPointer( 1, 0 )
        
        # Set up material
        OpenGLES1.Material( data[ 'material' ][ 'ambient' ],
                            data[ 'material' ][ 'diffuse' ],
                            [1.0, 1.0, 1.0, 1.0],
                            data[ 'material' ][ 'emission' ],
                            data[ 'material' ][ 'shininess' ]
                            )
        OpenGLES1.Translate( [ 0, 0.0, -3.1 ] )
        OpenGLES1.Rotate( 30.0, [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Rotate( 60.0, [ 0.0, 1.0, 0.0 ] )
        OpenGLES1.Scale( [ 6, 6, 6 ] )

        if self.numplanes > 0:
            OpenGLES1.CheckValue( 'GL_MAX_CLIP_PLANES', 'SCT_GEQUAL', [ self.numplanes ] )
        
        # Set up clipping planes
        for p in xrange( self.numplanes ):
            OpenGLES1.Enable( 'GL_CLIP_PLANE%d' % ( p, ) )
            OpenGLES1.ClipPlane( p, self.clipplanes[ p ] )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        
        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        

# ----------------------------------------------------------------------
class ClipPlaneRectangle( vt.VisualTestCase ):
    
    """ the purpose of the test: testing glClipPlane. 
    
    The test steps:
    1. The test defines vertices for 4 triangles (forming a rectangle).
    2. The test defines nomals for the triangles.
    3. The test defines colors for the triangles.
    4. The test set pointers.
    5. The test set clip plane according to the predefined table.
    6. The test draws the triangles.
    
    The expected output: four triangles forming a rectangle which is then clipped.
    """

    testValues = { 'clipplanes':[ ( [  0.0,  0.0, 0.0, 0.0 ] ),
                                  ( [ -1.0, -1.0, 0.0, 0.0 ] ),
                                  ( [  1.0,  1.0, 0.0, 0.0 ] ),
                                  ( [  1.0,  0.0, 0.0, 0.0 ] ),
                                  ( [ -1.0,  0.0, 0.0, 0.0 ] ),
                                  ( [  0.0,  1.0, 0.0, 0.0 ] ),
                                  ( [  0.0, -1.0, 0.0, 0.0 ] ),         
                                  ]
                   }
    
    def __init__( self, clipplanes ):
        vt.VisualTestCase.__init__( self )
        self.clipplanes = clipplanes
        self.name = "OPENGLES1 clipping area %s" % ( str( clipplanes ), )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Enable( 'GL_NORMALIZE' )
        
        # Create needed arrays
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_BYTE' ) # colors
        
        # Enable needed client states
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        # Define needed arrays
        vertices = []
        colors   = []
        normals  = []
        
        # Set width and height for the triangles
        width  = 1.0
        height = 1.0
        
        # Define vertices for four triangles
        vertices.extend( [ -width, -height, -0.5,
                           -width,  height, -0.5,
                              0.0, -height, -0.5,
                                
                              0.0, -height, -0.5,
                              0.0,  height, -0.5,
                           -width,  height, -0.5,
                                
                              0.0, -height, -0.5,
                              0.0,  height, -0.5,
                            width, -height, -0.5,
                                
                            width, -height, -0.5,
                            width,  height, -0.5,
                              0.0,  height, -0.5,
                            ] )
        
        # Define normals
        normals.extend([ 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0 ] )
        
        # Define colors
        colors.extend ( [ 255, 0, 0, 255,
                          255, 0, 0, 255,
                          255, 0, 0, 255,
                                
                          255, 0, 0, 255,
                          255, 0, 0, 255,
                          255, 0, 0, 255,
                                
                          0, 0, 255, 255,
                          0, 0, 255, 255,
                          0, 0, 255, 255,
                          
                          0, 0, 255, 255,
                          0, 0, 255, 255,
                          0, 0, 255, 255,
                          ] )
        
        # Append vertices, normals and color data
        OpenGLES1.AppendToArray( 0, vertices )
        OpenGLES1.AppendToArray( 1, normals )
        OpenGLES1.AppendToArray( 2, colors  )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.NormalPointer( 1, 0 )
        OpenGLES1.ColorPointer( 2, 4, 0 )
        
        OpenGLES1.Translate( [ 0.0, 0.0, -1.0 ] )

        # Set up clipping plane
        planeIndex = 0
        OpenGLES1.Enable( 'GL_CLIP_PLANE0' )
        OpenGLES1.ClipPlane( planeIndex, self.clipplanes )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.DrawArrays( 'GL_TRIANGLES', 0, 12 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
