# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles1common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 8, 8 )

# ----------------------------------------------------------------------
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               [ 1.0, 1.0, 1.0, 1.0 ],
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh


# ----------------------------------------------------------------------
class Blending( vt.VisualTestCase ):

        ''' The purpose of the test: test blending.

        Expected output: a 8x8 vertex grid is drawn full screen. The vertex
        colors are defined so that the lower-left vertex has color
        [0.0,0.0,0.5,0.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Surface is cleared
        to [0.5,0.5,0.5,0.5] before drawing the vertex grid. Viewport size
        320*240.
        '''

        testValues = { ( 'sfactor', 'dfactor' ) : [ ( 'GL_ZERO', 'GL_ZERO' ),
                                                    ( 'GL_ZERO', 'GL_ONE' ),
                                                    ( 'GL_ZERO', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ZERO', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ZERO', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ZERO', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ZERO', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ZERO', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_ONE', 'GL_ZERO' ),
                                                    ( 'GL_ONE', 'GL_ONE' ),
                                                    ( 'GL_ONE', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ONE', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ONE', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ONE', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ONE', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ONE', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_DST_COLOR', 'GL_ZERO' ),
                                                    ( 'GL_DST_COLOR', 'GL_ONE' ),
                                                    ( 'GL_DST_COLOR', 'GL_SRC_COLOR' ),
                                                    ( 'GL_DST_COLOR', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_DST_COLOR', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_DST_COLOR', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_DST_COLOR', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_DST_COLOR', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ZERO' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_ZERO' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_ONE' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_SRC_COLOR' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ZERO' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_DST_ALPHA', 'GL_ZERO' ),
                                                    ( 'GL_DST_ALPHA', 'GL_ONE' ),
                                                    ( 'GL_DST_ALPHA', 'GL_SRC_COLOR' ),
                                                    ( 'GL_DST_ALPHA', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_DST_ALPHA', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_DST_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_DST_ALPHA', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_DST_ALPHA', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ZERO' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ONE' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ZERO' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ONE' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_SRC_COLOR' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ]
                       }
                                                                                          
        def __init__( self, sfactor, dfactor ):
                vt.VisualTestCase.__init__( self )
                self.sfactor  = sfactor
                self.dfactor  = dfactor
                self.repeats  = 1
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.name = "OPENGLES1 blending, sfactor=%s, dfactor=%s" % ( blendToStr( self.sfactor ),
                                                                             blendToStr( self.dfactor ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 0.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );

                clearColor = [ 0.5, 0.5, 0.5, 0.5 ]
                OpenGLES1.ClearColor( clearColor )
                OpenGLES1.BlendFunc( self.sfactor, self.dfactor )
                OpenGLES1.Enable( 'GL_BLEND' )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                offset  = 1.0
                OpenGLES1.LoadIdentity()
                OpenGLES1.Translate( [ 0, 0, offset ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES1.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
