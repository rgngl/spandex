#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

WIDTH, HEIGHT = ( 320, 240, )

# ----------------------------------------------------------------------
class ColorMask( vt.VisualTestCase ):
    
    """ Testing ColorMask. Surface is first cleared to black, then with a
    different color and finally two triangles are drawn."""
    
    testValues = { 'red'   : [ 'ON', 'OFF' ],
                   'green' : [ 'ON', 'OFF' ],
                   'blue'  : [ 'ON', 'OFF' ],
                   'alpha' : [ 'ON', 'OFF' ],
                   }
        
    def __init__( self, red, green, blue, alpha ):
        vt.VisualTestCase.__init__( self )
        self.red   = red
        self.green = green
        self.blue  = blue
        self.alpha = alpha
        self.name = 'OPENGLES1 color mask red %s green %s blue %s alpha %s' % ( self.red, self.green, self.blue, self.alpha, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Ortho( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        tri = triangle()
        
        # Change the alpha values
        for c in xrange( len( tri[ 'colorsb' ] ) / 4 ):
            i = c * 4 + 3
            tri[ 'colorsb' ][ i ] = 127
            
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.ColorMask( self.red, self.green, self.blue, self.alpha )
        
        OpenGLES1.ClearColor( [ 0.33, 0.33, 0.33, 0.66 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        
        OpenGLES1.Rotate( 180.0, [ 0.0, 0.0, 1.0 ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Scale( [ 4, 4, 4 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.Rotate( 180.0, [ 0.0, 0.0, 1.0 ] )
        OpenGLES1.Scale( [ 4, 4, 4 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------        
class DepthMask( vt.VisualTestCase ):
    
    """ Testing DepthMask. Triangles are drawn translating away from the
    camera. With DepthMask disabled, the nearest triangle should be overdrawn by
    the others."""
    
    testValues = { 'flag' : [ 'ON', 'OFF' ],
                   }
        
    def __init__( self, flag ):
        vt.VisualTestCase.__init__( self )
        self.flag = flag
        self.name = 'OPENGLES1 depth mask %s' % ( self.flag, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 10 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -1 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        tri = triangle()
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'indices' ] )
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.DepthMask( self.flag )
        
        for x in xrange( 6 ):
            OpenGLES1.Rotate( 45.0, [ 0.0, 0.0, 1.0 ])
            OpenGLES1.Translate( [ 0.0, 0.0, -0.5 ] )
            OpenGLES1.PushMatrix()
            OpenGLES1.Scale( [ 4, 4, 4 ] )
            OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2 )
            OpenGLES1.PopMatrix()

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------
class Clear( vt.VisualTestCase ):
    
    """Testing Clear and ClearColor."""
    
    testValues = { 'alpha' : [ 1.0, 0.825, 0.75, 0.66, 0.5, 0.33, 0.25, 0.165, 0.0 ],
                   }
        
    def __init__( self, alpha):
        vt.VisualTestCase.__init__( self )
        self.alpha = alpha
        self.name = 'OPENGLES1 clear alpha=%.3f' % ( self.alpha, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        tri = triangle()
        
        # Change the alpha values
        for c in xrange( len( tri[ 'colorsb' ] ) / 4 ):
            i = c * 3 + 3
            tri[ 'colorsb' ][ i ] = 127
            
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2 )
        OpenGLES1.ClearColor( [ 0.33, 0.33, 0.0, self.alpha ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------        
class ClearDepth( vt.VisualTestCase ):
    
    """ Testing ClearDepth. A big triangle is drawn first, then depth buffer is
    cleared to the given value. More smaller triangles become visible as clear
    depth approaches 1.0."""
    
    testValues = { 'value' : [ 1.0, 0.825, 0.66, 0.5, 0.33, 0.165, 0.01 ],
                   }
    
    def __init__( self, value ):
        vt.VisualTestCase.__init__( self )
        self.value = value
        self.zfar  = 10.0
        self.znear = 1.0
        self.steps = 5
        self.name = 'OPENGLES1 clear depth %.2f' % ( self.value, )
        self.__doc__ += " %d small triangles are drawn in this test." % ( self.steps, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Ortho( -1, 1, -1, 1, self.znear, self.zfar )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        tri = triangle()
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.ClearDepth( self.value )
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ 0.0, 0.0, -( self.znear + 1 ) ] )
        OpenGLES1.Scale( [ 4, 4, 4 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ 0.5, -0.6, -( self.zfar - 0.1 ) ] )
        OpenGLES1.Rotate( 90.0, [ 0.0, 0.0, 1.0 ] )
        step = ( self.zfar - self.znear ) / ( self.steps + 1 )
        
        for z in xrange( self.steps ):
            OpenGLES1.Translate( [ 0.2, 0.2, step ] )
            OpenGLES1.PushMatrix()
            OpenGLES1.Scale( [ 1.2, 1.2, 1.0 ] )
            OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2 )
            OpenGLES1.PopMatrix()
        OpenGLES1.PopMatrix()
        
        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
