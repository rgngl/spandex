#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

WIDTH, HEIGHT = ( 320, 240, )


# ----------------------------------------------------------------------
class TextureFiltering( vt.VisualTestCase ):
    
    """ Texture filtering. Different min and mag filters are used to test
    mipmapping."""
    
    testValues = { 'min_filter' : [ 'GL_NEAREST',
                                    'GL_LINEAR',
                                    'GL_NEAREST_MIPMAP_NEAREST',
                                    'GL_LINEAR_MIPMAP_NEAREST',
                                    'GL_NEAREST_MIPMAP_LINEAR',
                                    'GL_LINEAR_MIPMAP_LINEAR',
                                    ],
                   'mag_filter' : [ 'GL_NEAREST',
                                    'GL_LINEAR',
                                    ],
                   }
    
    def __init__( self, min_filter, mag_filter ):
        vt.VisualTestCase.__init__( self )
        self.min_filter       = min_filter
        self.mag_filter       = mag_filter
        self.persp_correct    = 'GL_NICEST'
        self.wrap             = 'GL_REPEAT'
        self.generate_mipmaps = 'ON'
        self.tex_format       = 'OPENGLES1_RGB888'
        self.texenv           = 'GL_MODULATE'
        self.color1           = [ 0.3, 0.3, 0.3, 1.0 ]
        self.color2           = [ 1.0, 1.0, 1.0, 1.0 ]        
        self.name = "OPENGLES1 texturing min=%s mag=%s" % ( self.min_filter, self.mag_filter, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 1000 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', self.persp_correct )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )

        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.CreateCheckerTextureData( 0, self.color1, self.color2, 64, 64, 128, 128, 'ON', self.tex_format )

        OpenGLES1.GenTexture( 1 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 1 )
        OpenGLES1.CreateCheckerTextureData( 1, [ 1.0, 1.0, 1.0, 1.0 ], [ 0.0, 1.0, 0.0, 1.0 ], 4, 4, 32, 32, 'ON', self.tex_format )

        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.TexImage2D( 0, 'GL_TEXTURE_2D' )
        OpenGLES1.TexSubImage2D( 1, 'GL_TEXTURE_2D', 10, 20 )
        
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', self.min_filter, self.mag_filter, self.wrap, self.wrap, self.generate_mipmaps )
     
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.TexCoordPointer( 1, 2, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Translate( [ 0.0, -1.0, -60.0 ] )
        OpenGLES1.Rotate( 120.0, [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Scale( [ 300.0, 300.0, 300.0 ] )
        OpenGLES1.Rotate( 90, [ 0, 0, 1 ])
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        

# ----------------------------------------------------------------------        
class SubTexture( vt.VisualTestCase ):
    
    """ Subtexture test. Placing smaller decal textures on the bigger
    texture."""
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.color1 = [ 1.0, 0.0, 0.0, 1.0 ]
        self.color2 = [ 1.0, 1.0, 1.0, 1.0 ]
        self.color3 = [ 1.0, 0.0, 1.0, 1.0 ]
        self.color4 = [ 0.0, 1.0, 1.0, 1.0 ]       
        self.name = "OPENGLES1 sub texture"
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', 'GL_NICEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )

        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.CreateCheckerTextureData( 0, self.color1, self.color2, 4, 4, 64, 64, 'ON', 'OPENGLES1_RGBA4444' )

        OpenGLES1.GenTexture( 1 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 1 )
        OpenGLES1.CreateCheckerTextureData( 1, self.color3, self.color4, 4, 4, 32, 32, 'ON', 'OPENGLES1_RGBA4444' )
        
        OpenGLES1.GenTexture( 2 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 2 )
        OpenGLES1.CreateCheckerTextureData( 2, self.color3, self.color4, 2, 2, 16, 16, 'ON', 'OPENGLES1_RGBA4444' )
        
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.TexImage2D( 0, 'GL_TEXTURE_2D' )
        OpenGLES1.TexSubImage2D( 1, 'GL_TEXTURE_2D', 5, 5 )
        OpenGLES1.TexSubImage2D( 2, 'GL_TEXTURE_2D', 48, 40 )
     
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.TexCoordPointer( 1, 2, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------        
class MultiTexturing( vt.VisualTestCase ):
    
    """ Multitexturing. First texture is always drawn with GL_REPLACE, other
    texture modes vary. Texture 1 is red/white 4x4 checker pattern, and texture
    2 is blue/white 8x8 checker pattern. Both textures are 64x64 pixels."""

    testValues = { 'numtextures' : [ 2 ], # Be sure to define enough textures first (see above)
                   'mode' : [ 'GL_REPLACE',
                              'GL_MODULATE',
                              'GL_ADD',
                              'GL_ADD_SIGNED',
                              'GL_INTERPOLATE',
                              'GL_SUBTRACT',
                              ]
                   }
        
    def __init__( self, numtextures, mode ):
        vt.VisualTestCase.__init__( self )
        self.numtextures = numtextures
        self.mode        = mode
        self.textures    = [ { 'colors' : ( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] ),
                               'cx'     : 4,
                               'cy'     : 4,
                               'width'  : 64,
                               'height' : 64,
                               },
                             { 'colors' : ( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] ),
                               'cx'     : 8,
                               'cy'     : 8,
                               'width'  : 64,
                               'height' : 64,
                               } ]        
        self.name = "OPENGLES1 multitexturing %d textures %s" % ( self.numtextures, self.mode, )
        self.__doc__ += " Using %d texture units with mode %s." % ( self.numtextures, self.mode, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', 'GL_NICEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        texid = 0
        for tid in xrange( self.numtextures ):
                OpenGLES1.ActiveTexture( tid )
                OpenGLES1.ClientActiveTexture( tid )
                OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
                OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
                OpenGLES1.Enable( 'GL_TEXTURE_2D' )
                OpenGLES1.TexCoordPointer( 1, 2, 0 )
                OpenGLES1.GenTexture( tid )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', tid )
                OpenGLES1.CreateCheckerTextureData( tid,
                                                    self.textures[ tid ][ 'colors' ][ 0 ],
                                                    self.textures[ tid ][ 'colors' ][ 1 ],
                                                    self.textures[ tid ][ 'cx' ],
                                                    self.textures[ tid ][ 'cy' ],
                                                    self.textures[ tid ][ 'width' ],
                                                    self.textures[ tid ][ 'height' ],
                                                    'ON',
                                                    'OPENGLES1_RGBA8888' )
                OpenGLES1.TexImage2D( tid, 'GL_TEXTURE_2D' )
                OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texid )
                
                if tid == 0:
                    mode = 'GL_REPLACE'
                else:
                    mode = self.mode
                
                OpenGLES1.TexEnvCombineRGB( mode,
                                            'GL_TEXTURE',
                                            'GL_SRC_COLOR',
                                            'GL_PREVIOUS',
                                            'GL_SRC_COLOR',
                                            'GL_PREVIOUS',
                                            'GL_SRC_COLOR' )
                texid += 1
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       

        
# ----------------------------------------------------------------------        
class TextureFormats( vt.VisualTestCase ):
    
    """ Texture format test. Creates a texture in given format and uses it for
    texturing the quad. For alpha and luminance textures, alpha blending is
    enabled. Texture is black/white 4x4 checker pattern, 64x64 pixels."""

    testValues = { 'tex_format' : [ 'OPENGLES1_LUMINANCE8',
                                    'OPENGLES1_ALPHA8',
                                    'OPENGLES1_LUMINANCE_ALPHA88',
                                    'OPENGLES1_RGB565',
                                    'OPENGLES1_RGB888',
                                    'OPENGLES1_RGBA4444',
                                    'OPENGLES1_RGBA5551',
                                    'OPENGLES1_RGBA8888'
                                    ],
                   }
    
    def __init__( self,
                  perspective_correction = 'GL_NICEST',
                  min_filter             = 'GL_NEAREST_MIPMAP_LINEAR',
                  mag_filter             = 'GL_LINEAR',
                  wrap                   = 'GL_REPEAT',
                  generate_mipmaps       = 'OFF',
                  tex_format             = 'OPENGLES_RGBA4444',
                  texenv                 = 'GL_MODULATE'
                  ):
        vt.VisualTestCase.__init__( self )
        self.persp_correct    = perspective_correction
        self.min_filter       = min_filter
        self.mag_filter       = mag_filter
        self.wrap             = wrap
        self.generate_mipmaps = generate_mipmaps
        self.tex_format       = tex_format
        self.texenv           = texenv
        self.color1           = [ 0.0, 0.0, 0.0, 0.3 ]
        self.color2           = [ 1.0, 1.0, 1.0, 1.0 ]        
        self.name = "OPENGLES1 texture format %s" % ( self.tex_format, )
        self.__doc__ += " Color1=%s, Color2=%s, format='%s'" % ( self.color1, self.color2, self.tex_format, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        if self.tex_format in ( 'OPENGLES1_ALPHA8',
                                'OPENGLES1_LUMINANCE8',
                                'OPENGLES1_LUMINANCE_ALPHA88',
                                'OPENGLES1_RGBA4444',
                                'OPENGLES1_RGBA5551',
                                'OPENGLES1_RGBA8888' ):
            OpenGLES1.Enable( 'GL_BLEND' )
            OpenGLES1.BlendFunc( 'GL_SRC_ALPHA','GL_ONE_MINUS_SRC_COLOR' )
        
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.CreateCheckerTextureData( 0, self.color1, self.color2, 4, 4, 64, 64, 'ON', self.tex_format )
        OpenGLES1.TexImage2D( 0,'GL_TEXTURE_2D' )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.TexCoordPointer( 1, 2, 0 )
        OpenGLES1.ClearColor( [0.0, 0.0, 1.0, 1.0] )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       


# ----------------------------------------------------------------------        
class TextureWrapping( vt.VisualTestCase ):
    
    """Texture wrapping. Texture coordinate matrix is modified to make the
    effect more visible."""
    
    testValues = { 'wrap_s' : [ 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ],
                   'wrap_t' : [ 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ],
                   }
    
    def __init__( self, wrap_s, wrap_t ):
        vt.VisualTestCase.__init__( self )
        self.min_filter       = 'GL_NEAREST_MIPMAP_LINEAR'
        self.mag_filter       = 'GL_LINEAR'
        self.persp_correct    = 'GL_NICEST'
        self.wrap_s           = wrap_s
        self.wrap_t           = wrap_t
        self.generate_mipmaps = 'ON'
        self.tex_format       = 'OPENGLES1_RGB888'
        self.texenv           = 'GL_MODULATE'
        self.color1           = [ 0.3, 0.3, 0.3, 1.0 ]
        self.color2           = [ 1.0, 1.0, 1.0, 1.0 ]
        self.name = "OPENGLES1 texture wrapping s=%s t=%s" % ( self.wrap_s, self.wrap_t, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 1000 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.CreateCheckerTextureData( 0, self.color1, self.color2, 8, 8, 64, 64, 'ON', self.tex_format )
        OpenGLES1.GenTexture( 1 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 1 )
        OpenGLES1.CreateCheckerTextureData( 1, [ 1.0, 1.0, 1.0, 1.0 ], [ 0.0, 1.0, 0.0, 1.0 ], 4, 4, 32, 32, 'ON', self.tex_format )

        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', self.min_filter, self.mag_filter, self.wrap_s, self.wrap_t, self.generate_mipmaps )        
        OpenGLES1.TexImage2D( 0,'GL_TEXTURE_2D' )
        OpenGLES1.TexSubImage2D( 1, 'GL_TEXTURE_2D', 10, 20 )
                
        data = triangleQuad()
        
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.TexCoordPointer( 1, 2 ,0 )
        # Modify the texture matrix to make the effects more visible
        OpenGLES1.MatrixMode( 'GL_TEXTURE' )
        OpenGLES1.Translate( [ -0.5, -0.5, 0.0 ] )
        OpenGLES1.Scale( [ 2.0, 2.0, 2.0 ] )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Translate( [ 0.0, 0.0, -2.0 ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       

        
# ----------------------------------------------------------------------        
class CopyTexImage2D( vt.VisualTestCase ):
    
    """ Testing CopyTexImage2D. Color buffer is first cleared to [1.0, 1.0, 0.0,
    1.0] (yellow) then a checker textured quad is drawn slightly
    rotated. CopyTexImage2D is used to copy a 128x128 part of the color buffer
    to texture data. Color buffer is then cleared again with [0.5, 0.5, 0.5,
    1.0] (grey) and the quad is drawn again, this time with the new texture
    data. """
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.color1 = [ 0.3, 0.3, 0.3, 1.0 ]
        self.color2 = [ 1.0, 1.0, 1.0, 1.0 ]
        self.w      = 128
        self.h      = 128
        self.name = "OPENGLES1 CopyTexImage2D"
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )
        
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 1000 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', 'GL_NICEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        
        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.CreateCheckerTextureData( 0, self.color1, self.color2, 8, 8, 64, 64, 'ON', 'OPENGLES1_RGBA4444' )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D' , 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )
        
        OpenGLES1.TexImage2D( 0, 'GL_TEXTURE_2D' )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.TexCoordPointer( 1, 2, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.ClearColor( [ 1, 1, 0, 1 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Translate( [ 0.0, 0.0, -2.0 ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ 0.0, 0.0, -1.0 ] )
        OpenGLES1.Rotate( 30, [ 1, 0, 0 ] )
        OpenGLES1.Rotate( 30, [ 0, 1, 0 ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.CopyTexImage2D( 'GL_TEXTURE_2D', 0, 'GL_RGBA', ( WIDTH - self.w ) / 2, ( HEIGHT - self.h ) / 2, self.w, self.h )
        OpenGLES1.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len(data['indices']), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       


# ----------------------------------------------------------------------        
class CopyTexSubImage2D( vt.VisualTestCase ):
    
    """ Testing CopyTexSubImage2D. Color buffer is first cleared to [1.0, 1.0,
    0.0, 1.0] (yellow) then a checker textured quad is drawn slightly
    rotated. CopyTexSubImage2D is used to copy a 64x64 part of the color buffer
    to texture data. Color buffer is then cleared again with [0.5, 0.5, 0.5,
    1.0] (grey) and the quad is drawn again, this time with the new texture
    data."""

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.color1 = [ 0.3, 0.3, 0.3, 1.0 ]
        self.color2 = [ 1.0, 1.0, 1.0, 1.0 ]
        self.w      = 96
        self.h      = 96
        self.name = "OPENGLES1 CopyTexSubImage2D"
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 1000 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Hint('GL_PERSPECTIVE_CORRECTION_HINT', 'GL_NICEST')
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.CreateCheckerTextureData( 0, self.color1, self.color2, 8, 8, 128, 128, 'ON', 'OPENGLES1_RGB888' )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_LINEAR', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )
        OpenGLES1.TexImage2D(0,'GL_TEXTURE_2D' )

        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.TexCoordPointer( 1, 2, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.ClearColor( [ 1, 1, 0, 1 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Translate( [ 0.0, 0.0, -2.0 ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ 0.0, 0.0, -1.0 ] )
        OpenGLES1.Rotate( 45, [ 1, 0, 0 ] )
        OpenGLES1.Rotate( 45, [ 0, 1, 0 ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.CopyTexSubImage2D( 'GL_TEXTURE_2D', 0, WIDTH / 40, HEIGHT / 40, ( WIDTH - self.w ) / 2, ( HEIGHT - self.h ) / 2, self.w, self.h )
        OpenGLES1.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       


# ----------------------------------------------------------------------        
class TexEnvModeAndColor( vt.VisualTestCase ):
    
    """ Multitexturing mode and color texture environments. Texture 1 is
    red/white 4x4 checker pattern, and texture 2 is blue/white 8x8 checker
    pattern. Both textures are 64x64 pixels. TexEnv color is (1.0,1.0,0.0,1.0)
    (yellow)."""
    
    testValues = { 'numtextures' : [ 2 ], # Be sure to define enough textures first (see above)
                   'mode'        : [ 'GL_MODULATE',
                                     'GL_DECAL',
                                     'GL_BLEND',
                                     'GL_REPLACE',
                                     'GL_ADD',
                                     ],
                   'colors'      : [ [ 0.5, 0.5, 0.0, 0.5 ],
                                     [ 0.0, 0.5, 0.5, 0.5 ],
                                     [ 0.5, 0.0, 0.5, 0.5 ],
                                     ]
                   }
        
    def __init__( self, numtextures, mode, colors ):
        vt.VisualTestCase.__init__( self )
        self.numtextures = numtextures
        self.mode        = mode
        self.colors      = colors
        self.textures = [ { 'colors' : ( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] ),
                            'cx'     : 4,
                            'cy'     : 4,
                            'width'  : 64,
                            'height' : 64,
                            },
                          { 'colors' :( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] ),
                            'cx'     : 8,
                            'cy'     : 8,
                            'width'  : 64,
                            'height' : 64,
                            },
                          ]
        
        self.name = "OPENGLES1 texenv mode and color %s %s" % ( self.mode, self.colors, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', 'GL_NICEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        
        texid = 0
        for tid in xrange( self.numtextures ):
            OpenGLES1.ActiveTexture( tid )
            OpenGLES1.ClientActiveTexture( tid )
            OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
            OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
            OpenGLES1.Enable( 'GL_TEXTURE_2D' )
            OpenGLES1.TexCoordPointer( 1, 2, 0 )
            OpenGLES1.GenTexture( tid )
            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', tid )
            OpenGLES1.CreateCheckerTextureData( tid,
                                                self.textures[ tid ][ 'colors' ][ 0 ],
                                                self.textures[ tid ][ 'colors' ][ 1 ],
                                                self.textures[ tid ][ 'cx' ],
                                                self.textures[ tid ][ 'cy' ],
                                                self.textures[ tid ][ 'width' ],
                                                self.textures[ tid ][ 'height' ],
                                                'ON',
                                                'OPENGLES1_RGBA8888' )
            OpenGLES1.TexImage2D( texid, 'GL_TEXTURE_2D')
            
            if tid == 0:
                mode = 'GL_REPLACE'
            else:
                mode = self.mode
                
            OpenGLES1.TexEnv( mode, self.colors, 'OFF' )
            texid += 1
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       
        
        
# ----------------------------------------------------------------------
class TexEnvCombineRGB( vt.VisualTestCase ):
    
    """ Multitexturing RGB combine texture environment. First texture is always
    drawn with GL_REPLACE, other texture modes vary. Texture 1 is red/white 2x2
    checker pattern, and texture 2 is blue/white 4x4 checker pattern. Both
    textures are 64x64 pixels. TexEnv color is (0.0,1.0,0.0,0.5) (green). """
    
    testValues = { 'numtextures' : [ 2 ], # Be sure to define enough textures first (see above)
                   'mode'        : [ 'GL_REPLACE',
                                     'GL_MODULATE',
                                     'GL_ADD',
                                     'GL_ADD_SIGNED',
                                     'GL_INTERPOLATE',
                                     'GL_SUBTRACT',
                                     'GL_DOT3_RGB',
                                     'GL_DOT3_RGBA' ],
                   'src'          : [ 'GL_TEXTURE',
                                      'GL_CONSTANT',
                                      'GL_PRIMARY_COLOR',
                                      'GL_PREVIOUS' ],
                   'operand'      : [ 'GL_SRC_COLOR',
                                      'GL_ONE_MINUS_SRC_COLOR',
                                      'GL_SRC_ALPHA',
                                      'GL_ONE_MINUS_SRC_ALPHA' ],
                   }
        
    def __init__( self, numtextures, mode, src, operand ):
        vt.VisualTestCase.__init__( self )
        self.numtextures = numtextures
        self.mode        = mode
        self.src         = src
        self.operand     = operand
        # First texture is always drawn with mode = GL_REPLACE
        self.textures    = [ { 'colors' : ( [ 1.0, 0.0, 0.0, 0.66 ], [ 1.0, 1.0, 1.0, 0.33 ] ),
                               'cx'     : 2,
                               'cy'     : 2,
                               'width'  : 64,
                               'height' : 64,
                               },
                             { 'colors' : ( [ 0.0, 0.0, 1.0, 0.66 ], [ 1.0, 1.0, 1.0, 0.33 ] ),
                               'cx'     : 4,
                               'cy'     : 4,
                               'width'  : 64,
                               'height' : 64,
                               } ]
        self.color       = [ 0.0, 1.0, 0.0, 0.5 ]
        self.name = "OPENGLES1 texenv RGB combine %d textures mode=%s src=%s operand=%s" % ( self.numtextures, self.mode, self.src, self.operand, )
        self.__doc__ += " Using %d texture units with mode %s." % ( self.numtextures, self.mode, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0.5, 0.5, 0.5, 0.66 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', 'GL_NICEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        
        texid = 0
        for tid in xrange( self.numtextures ):
            OpenGLES1.ActiveTexture( tid )
            OpenGLES1.ClientActiveTexture( tid )
            OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
            OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
            OpenGLES1.Enable( 'GL_TEXTURE_2D' )
            OpenGLES1.TexCoordPointer( 1, 2, 0 )
            OpenGLES1.GenTexture( tid )
            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', tid )
            OpenGLES1.CreateCheckerTextureData( tid,
                                                self.textures[ tid ][ 'colors' ][ 0 ],
                                                self.textures[ tid ][ 'colors' ][ 1 ],
                                                self.textures[ tid ][ 'cx' ],
                                                self.textures[ tid ][ 'cy' ],
                                                self.textures[ tid ][ 'width' ],
                                                self.textures[ tid ][ 'height' ],
                                                'ON',
                                                'OPENGLES1_RGBA8888' )
            OpenGLES1.TexImage2D( texid, 'GL_TEXTURE_2D' )
            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texid )
            OpenGLES1.TexEnv('GL_REPLACE', self.color, 'OFF')
            
            if tid == 0:
                mode = 'GL_REPLACE'
            else:
                mode = self.mode
                
            OpenGLES1.TexEnvCombineRGB( mode,           # func
                                        'GL_TEXTURE',   # src0
                                        self.operand,   # operand0
                                        self.src,       # src1
                                        self.operand,   # operand1
                                        self.src,       # src2
                                        self.operand)   # operand2
            texid += 1
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
            
        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       
        

# ----------------------------------------------------------------------        
class TexEnvCombineAlpha( vt.VisualTestCase ):
    
    """ Multitexturing alpha combine texture environment. First texture is
    always drawn with GL_REPLACE, other texture modes vary. Texture 1 is
    red/white 4x4 checker pattern, and texture 2 is blue/white 8x8 checker
    pattern. Both textures are 64x64 pixels.  TexEnv color is (0.0,1.0,0.0,0.5)
    (green). """

    testValues = { 'numtextures' : [ 2 ], # Be sure to define enough textures first (see above)
                   'mode'        : [ 'GL_REPLACE',
                                     'GL_MODULATE',
                                     'GL_ADD',
                                     'GL_ADD_SIGNED',
                                     'GL_INTERPOLATE',
                                     'GL_SUBTRACT' ],
                   'src'         : [ 'GL_TEXTURE',
                                     'GL_CONSTANT',
                                     'GL_PRIMARY_COLOR',
                                     'GL_PREVIOUS' ],
                   'operand'     : [ 'GL_SRC_ALPHA',
                                     'GL_ONE_MINUS_SRC_ALPHA' ],
                   }
        
    def __init__( self, numtextures, mode, src, operand ):
        vt.VisualTestCase.__init__( self )
        self.numtextures = numtextures
        self.mode        = mode
        self.src         = src
        self.operand     = operand
        # First texture is always drawn with mode = GL_REPLACE
        self.textures    = [ { 'colors' : ( [ 1.0, 0.0, 0.0, 0.66 ], [ 1.0, 1.0, 1.0, 0.33 ] ),
                               'cx'     : 4,
                               'cy'     : 4,
                               'width'  : 64,
                               'height' : 64,
                               },
                             { 'colors' : ( [ 0.0, 0.0, 1.0, 0.66 ], [ 1.0, 1.0, 1.0, 0.33 ] ),
                               'cx'     : 8,
                               'cy'     : 8,
                               'width'  : 64,
                               'height' : 64,
                               } ]
        self.color       = [ 0.0, 1.0, 0.0, 0.5 ]
        self.name = "OPENGLES1 texenv alpha combine %d textures mode=%s src=%s operand=%s" % ( self.numtextures, self.mode, self.src, self.operand, )
        self.__doc__ += " Using %d texture units with mode %s." % ( self.numtextures, self.mode, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', 'GL_NICEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        
        texid = 0
        for tid in xrange( self.numtextures ):
            OpenGLES1.ActiveTexture( tid )
            OpenGLES1.ClientActiveTexture( tid )
            OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
            OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
            OpenGLES1.Enable( 'GL_TEXTURE_2D' )
            OpenGLES1.TexCoordPointer( 1, 2, 0 )
            OpenGLES1.GenTexture( tid )
            OpenGLES1.BindTexture( 'GL_TEXTURE_2D' , tid )
            OpenGLES1.CreateCheckerTextureData( tid,
                                                self.textures[ tid ][ 'colors' ][ 0 ],
                                                self.textures[ tid ][ 'colors' ][ 1 ],
                                                self.textures[ tid ][ 'cx' ],
                                                self.textures[ tid ][ 'cy' ],
                                                self.textures[ tid ][ 'width' ],
                                                self.textures[ tid ][ 'height' ],
                                                'ON',
                                                'OPENGLES1_RGBA8888' )
                
            OpenGLES1.TexImage2D( texid, 'GL_TEXTURE_2D' )
            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texid )
            OpenGLES1.TexEnv( 'GL_REPLACE', self.color, 'OFF' )
            
            if tid == 0:
                mode = 'GL_REPLACE'
            else:
                mode = self.mode
                
            OpenGLES1.TexEnvCombineAlpha( mode,           # func
                                          'GL_TEXTURE',   # src0
                                          self.operand,   # operand0
                                          self.src,       # src1
                                          self.operand,   # operand1
                                          self.src,       # src2
                                          self.operand)   # operand2
            texid += 1
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       

        
# ----------------------------------------------------------------------        
class GradientTexture( vt.VisualTestCase ):
    
    """ Gradient texture test. Purpose of the test is to spot texture alignment
    issues. Dithering is disabled. """
    
    testValues = { 'color1'        : [ [ 0.75, 0.0, 0.75, 1.0 ], [ 0.0,0.75,0.0,1.0 ] ],
                   'color2'        : [ [ 0.0, 0.5, 0.5, 1.0 ], [ 0.5, 0.0, 0.5, 1.0 ] ],
                   'textureformat' : [ 'OPENGLES1_RGB565',
                                       'OPENGLES1_RGB888',
                                       'OPENGLES1_RGBA4444',
                                       'OPENGLES1_RGBA5551',
                                       'OPENGLES1_RGBA8888' ],
                   'Gradient'      : [ 'OPENGLES1_GRADIENT_VERTICAL', 
                                       'OPENGLES1_GRADIENT_HORIZONTAL' ]
                   }
    
    def __init__( self, color1, color2, textureformat, Gradient ):
        vt.VisualTestCase.__init__( self )
        self.color1        = color1
        self.color2        = color2
        self.textureformat = textureformat
        self.Gradient      = Gradient
        self.name = "OPENGLES1 gradient texture %s %s %s %s"  % ( color1, color2, textureformat, Gradient, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', 'GL_NICEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.CreateArray( 3, 'GL_UNSIGNED_BYTE' )
       
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.CreateGradientTextureData(0, self.Gradient, self.color1, self.color2, 256, 256, 'ON', self.textureformat )
        OpenGLES1.TexImage2D( 0, 'GL_TEXTURE_2D')
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        OpenGLES1.AppendToArray( 3, data[ 'colorsb' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.TexCoordPointer( 1, 2, 0 )
        OpenGLES1.ColorPointer( 3 , 4, 0 )

        OpenGLES1.Disable( 'GL_DITHER' )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       
        

# ----------------------------------------------------------------------        
class SolidTexture( vt.VisualTestCase ):
    
    """ Solid texture test."""
    
    testValues = { 'color'         : [ [ 1.0, 0.0, 0.0, 1.0 ],
                                       [ 1.0, 1.0, 1.0, 1.0 ],
                                       [ 1.0, 0.0, 1.0, 1.0 ],
                                       [ 0.0, 1.0, 1.0, 1.0 ] ],
                   'textureformat' : [ 'OPENGLES1_RGB565',
                                       'OPENGLES1_RGB888',
                                       'OPENGLES1_RGBA4444',
                                       'OPENGLES1_RGBA5551',
                                       'OPENGLES1_RGBA8888' ],
                   }
                
    def __init__( self, color, textureformat ):
        vt.VisualTestCase.__init__( self )
        self.color         = color
        self.textureformat = textureformat
        self.name = "OPENGLES1 solid texture %s %s"  % ( color, textureformat, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', 'GL_NICEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )

        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )

        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.CreateSolidTextureData( 0, self.color, 256, 256, 'ON', self.textureformat )
        OpenGLES1.TexImage2D(0, 'GL_TEXTURE_2D')
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )

        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.TexCoordPointer( 1, 2, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       
        

# ----------------------------------------------------------------------        
class BarTexture( vt.VisualTestCase ):
    
    """ Bar texture test."""
    
    testValues = { 'color1'        : [ [ 1.0, 0.0, 0.0, 1.0 ],
                                       [ 1.0, 1.0, 1.0, 1.0 ] ],
                   'color2'        : [ [ 0.5, 1.0, 0.5, 1.0 ],
                                       [ 0.0, 0.0, 1.0, 1.0 ] ],
                   'bars'          : [ 2,4,8 ],
                   'textureformat' : [ 'OPENGLES1_RGBA5551',
                                       'OPENGLES1_RGBA8888' ],
                   }
                
    def __init__( self, color1, color2, bars, textureformat ):
        vt.VisualTestCase.__init__( self )
        self.color1        = color1
        self.color2        = color2
        self.bars          = bars
        self.textureformat = textureformat
        self.name = "OPENGLES1 bar texture %s %s %s %s"  % ( color1, color2, bars, textureformat, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', 'GL_NICEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
       
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )

        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.CreateBarTextureData( 0, self.color1, self.color2, self.bars, 256, 256, 'ON', self.textureformat )
        OpenGLES1.TexImage2D( 0, 'GL_TEXTURE_2D')
      
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )

        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.TexCoordPointer( 1, 2, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       
        
