# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from lib.egl     import *
from gles1common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )

IMAGES_FORMAT = { 'SCT_COLOR_FORMAT_RGB565'       : 'IMAGES_RGB565',
                  'SCT_COLOR_FORMAT_RGBX8888'     : 'IMAGES_BGRX8' ,
                  'SCT_COLOR_FORMAT_RGBA8888'     : 'IMAGES_BGRA8' }

SURFACE_PARAMETERS = { 'OPENGLES1_RGB565'   : [ 16, 5, 6, 5, 0 ],
                       'OPENGLES1_RGB888'   : [ 24, 8, 8, 8, 0 ],
                       'OPENGLES1_RGBA8888' : [ 32, 8, 8, 8, 8 ] }

SURFACE_PARAMETERS2 = { 'SCT_COLOR_FORMAT_RGB565'       : [ 16, 5, 6, 5, 0 ],
                        'SCT_COLOR_FORMAT_RGB888'       : [ 24, 8, 8, 8, 0 ],
                        'SCT_COLOR_FORMAT_RGBX8888'     : [ 24, 8, 8, 8, 0 ],
                        'SCT_COLOR_FORMAT_RGBA8888'     : [ 32, 8, 8, 8, 8 ],
                        'SCT_COLOR_FORMAT_RGBA8888_PRE' : [ 32, 8, 8, 8, 8 ],
                        }

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh


######################################################################            
class EglImageAsTextureExt( vt.VisualTestCase ):
    
    ''' Test creating an OpenGLES1 texture from a shared pixmap EGL image. Draws
    a flower scaled to the surface. Texture size and format varies. Requires EGL
    and EGL_KHR_image, EGL_KHR_image_base, EGL_KHR_image_pixmap,
    EGL_KHR_gl_texture_2D_image, and GL_OES_EGL_image extensions. Requires
    EGL_NOK_pixmap_type_rsgimage extension on Symbian.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBX8888' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBA8888' ) ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES1 egl image as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]

        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )

        OpenGLES1.CheckExtension( 'GL_OES_EGL_image' )

        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       

        # Create shared pixmap and egl image.
        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex,
                           'IMAGES_FLOWER',
                           IMAGES_FORMAT[ self.format ],
                           self.width, self.height )

        eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
        Images.CopyToEgl( imagesDataIndex, eglDataIndex )

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENGLES1_TEXTURE_2D' ],
                                eglDataIndex )

        imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]

        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            -1,
                            'EGL_NATIVE_PIXMAP_KHR',
                            eglPixmapIndex,
                            imageAttributes )

        # Create OpenGLES1 data/objects.
        mesh                = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
                
        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )

        # Set texture data from egl image.
        OpenGLES1.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES1.CheckError( 'OpenGLES1.EglImageTargetTexture2D' )

        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES1.LoadIdentity()
        drawMeshBufferData( modules, meshBufferDataSetup )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################            
class NpotEglImageAsTextureExt( vt.VisualTestCase ):
    
    ''' Test creating an OpenGLES1 texture from an NPOT shared pixmap EGL
    image. Draws a flower scaled to the surface. Texture size and format
    varies. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_image_pixmap, EGL_KHR_gl_texture_2D_image, GL_OES_EGL_image, and
    GL_OES_texture_npot extensions. Requires EGL_NOK_pixmap_type_rsgimage
    extension in Symbian.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 127, 129, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 255, 255, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 257, 'SCT_COLOR_FORMAT_RGBX8888' ),
                         ( 257, 256, 'SCT_COLOR_FORMAT_RGBA8888' ) ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES1 npot egl image as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]

        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )

        OpenGLES1.CheckExtension( 'GL_OES_EGL_image' )
        OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )

        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       

        # Create shared pixmap and egl image.
        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex,
                           'IMAGES_FLOWER',
                           IMAGES_FORMAT[ self.format ],
                           self.width, self.height )

        eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
        Images.CopyToEgl( imagesDataIndex, eglDataIndex )

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENGLES1_TEXTURE_2D' ],
                                eglDataIndex )

        imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]

        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            -1,
                            'EGL_NATIVE_PIXMAP_KHR',
                            eglPixmapIndex,
                            imageAttributes )

        # Create OpenGLES1 data/objects.
        mesh                = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
                
        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )

        # Set texture data from egl image.
        OpenGLES1.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES1.CheckError( 'OpenGLES1.EglImageTargetTexture2D' )                        
        
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES1.LoadIdentity()
        drawMeshBufferData( modules, meshBufferDataSetup )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       
        
######################################################################            
class EglImageFromTextureExt( vt.VisualTestCase ):
    
    ''' Test creating an EGL image from an existing texture. The test should
    draw a black and white gradient texture with [ 0, 1, 0, 0.3 ] rectangle
    (green) at the lower-left corner scaled to the surface. Texture size and
    format varies. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_gl_texture_2D_image, and GL_OES_EGL_image extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'OPENGLES1_LUMINANCE8' ),
                         ( 256, 256, 'OPENGLES1_ALPHA8' ),
                         ( 128, 128, 'OPENGLES1_RGB565' ),
                         ( 64, 128, 'OPENGLES1_RGB565' ),
                         ( 128, 64, 'OPENGLES1_RGB565' ),
                         ( 256, 256, 'OPENGLES1_RGB565' ),
                         ( 256, 256, 'OPENGLES1_RGB888' ),
                         ( 256, 256, 'OPENGLES1_RGBA4444' ),
                         ( 256, 256, 'OPENGLES1_RGBA5551' ),
                         ( 256, 256, 'OPENGLES1_RGBA8888' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES1 egl image from texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]

        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES1.CheckExtension( 'GL_OES_EGL_image' )        

        # Create OpenGLES1 data/objects.
        mesh                = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
        
        textureData1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateNullTextureData( textureData1Index,
                                         self.width, self.height,
                                         'OFF',
                                         self.format )
        
        textureData2Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateGradientTextureData( textureData2Index,
                                             'OPENGLES1_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             self.format )

        textureData3Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateSolidTextureData( textureData3Index,
                                          [ 0, 1, 0, 0.3 ],
                                          self.width / 2, self.height / 2,
                                          'OFF',
                                          self.format )
        
        texture1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture1Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )
        OpenGLES1.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
        
        # Create egl image from texture.
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            contextIndex,
                            'EGL_GL1_TEXTURE_2D_KHR',
                            texture1Index,
                            [] )

        # Create a second texture from egl image.
        texture2Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture2Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )

        OpenGLES1.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )

        # Modify original texture data.
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexSubImage2D( textureData2Index, 'GL_TEXTURE_2D', 0, 0 )
        OpenGLES1.TexSubImage2D( textureData3Index, 'GL_TEXTURE_2D', 0, 0 )

        # Use the second texture for drawing.
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture2Index )

        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES1.LoadIdentity()
        drawMeshBufferData( modules, meshBufferDataSetup )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################            
class EglImageFromNpotTextureExt( vt.VisualTestCase ):
    
    ''' Test creating an EGL image from a NPOT texture. Texture size and format
    varies. Requires EGL, EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_gl_texture_2D_image, GL_OES_EGL_image, and GL_OES_texture_npot
    extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 63, 63, 'OPENGLES1_RGB565' ),
                         ( 63, 64, 'OPENGLES1_RGB565' ),
                         ( 64, 65, 'OPENGLES1_RGB565' ),
                         ( 65, 65, 'OPENGLES1_RGB565' ),                         
                         ( 255, 255, 'OPENGLES1_RGB888' ),
                         ( 255, 257, 'OPENGLES1_RGBA4444' ),
                         ( 257, 255, 'OPENGLES1_RGBA5551' ),
                         ( 257, 257, 'OPENGLES1_RGBA8888' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES1 egl image from npot texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]

        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES1.CheckExtension( 'GL_OES_EGL_image' )        
        OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )

        # Create OpenGLES1 data/objects.
        mesh                = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
        meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
        useMeshBufferData( modules, indexTracker, meshBufferDataSetup )
        
        textureData1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateNullTextureData( textureData1Index,
                                         self.width, self.height,
                                         'OFF',
                                         self.format )
        
        textureData2Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateGradientTextureData( textureData2Index,
                                             'OPENGLES1_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             self.format )

        textureData3Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateSolidTextureData( textureData3Index,
                                          [ 0, 1, 0, 0.3 ],
                                          self.width / 2, self.height / 2,
                                          'OFF',
                                          self.format )
        
        texture1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture1Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )
        OpenGLES1.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )       
        
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            contextIndex,
                            'EGL_GL1_TEXTURE_2D_KHR',
                            texture1Index,
                            [] )

        texture2Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture2Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )

        OpenGLES1.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )

        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexSubImage2D( textureData2Index, 'GL_TEXTURE_2D', 0, 0 )
        OpenGLES1.TexSubImage2D( textureData3Index, 'GL_TEXTURE_2D', 0, 0 )

        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture2Index )

        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES1.LoadIdentity()
        drawMeshBufferData( modules, meshBufferDataSetup )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################            
class PixmapSurfaceAsTextureExt( vt.VisualTestCase ):
    
    ''' Test creating a pixmap surface from a shared pixmap. The test first
    creates a shared pixmap. Secondly, the test creates a texture from the
    shared pixmap through EGL image. Thirdly, creates a pixmap surface from the
    shared pixmap, render some graphics, and then uses the texture for
    rendering. The test should draw a black and white gradient to the
    surface. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_gl_texture_2D_image, and GL_OES_EGL_image extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBX8888' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBA8888' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES1 pixmap surface as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]

        # Check required extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES1.CheckExtension( 'GL_OES_EGL_image' )

        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       
        
        # Create OpenGLES1 data/objects.
        mesh                = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        useVertexData( modules, indexTracker, vertexDataSetup )

        # Create shared pixmap and egl image.
        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENGLES1_SURFACE', 'SCT_USAGE_OPENGLES1_TEXTURE_2D' ],
                                -1 )

        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, -1, 'EGL_NATIVE_PIXMAP_KHR', eglPixmapIndex, [] )

        # Create a texture from egl image.
        texture1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture1Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )

        OpenGLES1.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES1.CheckError( 'OpenGLES1.EglImageTargetTexture2D' )

        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENGL_ES_BIT' ]
                       ]
        
        # Create pixmap surface from the shared pixmap.
        pixmapConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex, pixmapConfigIndex, eglPixmapIndex, attributes )

        pixmapSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurfaceExt( displayIndex, pixmapSurfaceIndex, pixmapConfigIndex, eglPixmapIndex, [] )

        pixmapContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pixmapContextIndex, pixmapConfigIndex, -1, 1 )

        Egl.MakeCurrent( displayIndex, pixmapSurfaceIndex, pixmapSurfaceIndex, pixmapContextIndex )

        useVertexData( modules, indexTracker, vertexDataSetup )
        
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                             'OPENGLES1_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             'OPENGLES1_RGB565' )

        texture2Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture2Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, self.width, self.height );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        
        OpenGLES1.LoadIdentity()
        drawVertexData( modules, vertexDataSetup )

        # Restore the original surface and draw the texture from the egl
        # image/pixmap surface.
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################            
class NpotPixmapSurfaceAsTextureExt( vt.VisualTestCase ):
    
    ''' Test creating a pixmap surface from an NPOT shared pixmap. The test
    first creates a shared pixmap. Secondly, the test creates a texture from the
    shared pixmap through EGL image. Thirdly, creates a pixmap surface from the
    shared pixmap, render some graphics, and then uses the texture for
    rendering. The test should draw a black and white gradient to the
    surface. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_gl_texture_2D_image, GL_OES_texture_npot and GL_OES_EGL_image
    extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 255, 255, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 257, 257, 'SCT_COLOR_FORMAT_RGBX8888' ),
                         ( 256, 257, 'SCT_COLOR_FORMAT_RGBA8888' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES1 npot pixmap surface as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]

        # Check required extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES1.CheckExtension( 'GL_OES_EGL_image' )
        OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )
        
        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       
        
        # Create OpenGLES1 data/objects.
        mesh                = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        useVertexData( modules, indexTracker, vertexDataSetup )

        # Create shared pixmap and egl image.
        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENGLES1_SURFACE', 'SCT_USAGE_OPENGLES1_TEXTURE_2D' ],
                                -1 )

        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, -1, 'EGL_NATIVE_PIXMAP_KHR', eglPixmapIndex, [] )

        # Create a texture from egl image.
        texture1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture1Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )

        OpenGLES1.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES1.CheckError( 'OpenGLES1.EglImageTargetTexture2D' )

        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENGL_ES_BIT' ]
                       ]
        
        # Create pixmap surface from the shared pixmap.
        pixmapConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex, pixmapConfigIndex, eglPixmapIndex, attributes )

        pixmapSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurfaceExt( displayIndex, pixmapSurfaceIndex, pixmapConfigIndex, eglPixmapIndex, [] )

        pixmapContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pixmapContextIndex, pixmapConfigIndex, -1, 1 )

        Egl.MakeCurrent( displayIndex, pixmapSurfaceIndex, pixmapSurfaceIndex, pixmapContextIndex )

        useVertexData( modules, indexTracker, vertexDataSetup )
        
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                             'OPENGLES1_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             'OPENGLES1_RGB565' )

        texture2Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture2Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, self.width, self.height );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        
        OpenGLES1.LoadIdentity()
        drawVertexData( modules, vertexDataSetup )

        # Restore the original surface and draw the texture from the egl
        # image/pixmap surface.
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################            
class PbufferAsTexture( vt.VisualTestCase ):
    
    ''' Test using an EGL pbuffer surface as an OpenGLES1 texture. The test
    should draw back-and-white diagonal gradient to the surface; black on the
    lower-left, white on the upper-right. Requires EGL.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'OPENGLES1_RGB565' ),
                         ( 256, 256, 'OPENGLES1_RGB888' ),
                         ( 256, 256, 'OPENGLES1_RGBA8888' ) ]
                   }
    
    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES1 pbuffer as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]
        
        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        if p[ 4 ] == 0:
            rgbTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        else:
            rgbaTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufSurfaceIndex,
                                  pbufConfigIndex,
                                  self.width, self.height,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )

        mesh                = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        useVertexData( modules, indexTracker, vertexDataSetup )

        textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                             'OPENGLES1_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             self.format )
        
        texture1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture1Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, self.width, self.height );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES1.DeleteTexture( texture1Index )
        
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        useVertexData( modules, indexTracker, vertexDataSetup )
        
        texture2Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture2Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )

        Egl.BindTexImage( displayIndex, pbufSurfaceIndex, 'EGL_BACK_BUFFER' )
        
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )        
        OpenGLES1.LoadIdentity()
        drawVertexData( modules, vertexDataSetup )       

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################            
class NpotPbufferAsTextureExt( vt.VisualTestCase ):
    
    ''' Test using a NPOT EGL pbuffer surface as an OpenGLES1 texture. The test
    should draw back-and-white diagonal gradient to the surface; black on the
    lower-left, white on the upper-right. Requires EGL and GL_OES_texture_npot
    extension.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 255, 255, 'OPENGLES1_RGB565' ),
                         ( 255, 257, 'OPENGLES1_RGB565' ),
                         ( 257, 255, 'OPENGLES1_RGB565' ),                         
                         ( 255, 257, 'OPENGLES1_RGB888' ),
                         ( 257, 255, 'OPENGLES1_RGBA8888' ) ]
                   }
    
    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES1 npot pbuffer as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]

        OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )
        
        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        if p[ 4 ] == 0:
            rgbTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        else:
            rgbaTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufSurfaceIndex,
                                  pbufConfigIndex,
                                  self.width, self.height,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )

        mesh                = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        useVertexData( modules, indexTracker, vertexDataSetup )

        textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                             'OPENGLES1_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             self.format )
        
        texture1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture1Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )        
        OpenGLES1.Viewport( 0, 0, self.width, self.height );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES1.DeleteTexture( texture1Index )
        
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        useVertexData( modules, indexTracker, vertexDataSetup )
        
        texture2Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture2Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )

        Egl.BindTexImage( displayIndex, pbufSurfaceIndex, 'EGL_BACK_BUFFER' )
        
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )        
        OpenGLES1.LoadIdentity()
        drawVertexData( modules, vertexDataSetup )       

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################            
class PbufferWithMipmapsAsTexture( vt.VisualTestCase ):
    
    ''' Test using an EGL surface with mipmaps as an OpenGLES1 texture. Draws a
    solid green texture scaled across the surface. Requires EGL.
    '''
   
    testValues = { ( 'format' ) : [ ( 'OPENGLES1_RGB565' ),
                                    ( 'OPENGLES1_RGB888' ),
                                    ( 'OPENGLES1_RGBA8888' ) ]
                   }
    
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES1 pbuffer with mipmaps as texture %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        WIDTH    = 240
        HEIGHT   = 240       
        textureW = 512
        textureH = 512
               
        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]

        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        if p[ 4 ] == 0:
            rgbTexture    = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        else:
            rgbaTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufSurfaceIndex,
                                  pbufConfigIndex,
                                  textureW, textureH,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'ON',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )

        w     = textureW
        h     = textureH
        level = 0
        
        while True:
                Egl.SurfaceAttrib( displayIndex, pbufSurfaceIndex, EGL_DEFINES[ 'EGL_MIPMAP_LEVEL' ], level )
                OpenGLES1.Viewport( 0, 0, w, h );
                if level == 1:
                        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                else:
                        OpenGLES1.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                if w == 1 and h == 1:
                        break
                
                w     = w / 2
                h     = h / 2
                level = level + 1

                if w < 1:
                        w = 1
                        
                if h < 1:
                        h = 1

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        rx = 1.0
        ry = 1.0
        
        mesh                = createPlane( self.gridx, self.gridy, rx, ry, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        useVertexData( modules, indexTracker, vertexDataSetup )
        
        texture1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture1Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )

        Egl.BindTexImage( displayIndex, pbufSurfaceIndex, 'EGL_BACK_BUFFER' )
       
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )        
        OpenGLES1.LoadIdentity()
        drawVertexData( modules, vertexDataSetup )       

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################            
class NpotPbufferWithMipmapsAsTextureExt( vt.VisualTestCase ):
    
    ''' Test using a NPOT EGL surface with mipmaps as an OpenGLES1
    texture. Draws a solid green texture scaled across the surface. Requires EGL
    and GL_OES_texture_npot extension.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) : [ ( 511, 512, 'OPENGLES1_RGB565' ),
                                                       ( 512, 511, 'OPENGLES1_RGB888' ),
                                                       ( 511, 512, 'OPENGLES1_RGBA8888' ) ]
                   }
    
    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES1 npot pbuffer with mipmaps as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        WIDTH     = 240
        HEIGHT    = 240
        
        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]

        OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )

        textureW = self.width
        textureH = self.height
        
        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        if p[ 4 ] == 0:
            rgbTexture    = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        else:
            rgbaTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufSurfaceIndex,
                                  pbufConfigIndex,
                                  textureW, textureH,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'ON',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )

        w     = textureW
        h     = textureH
        level = 0
        
        while True:
                Egl.SurfaceAttrib( displayIndex, pbufSurfaceIndex, EGL_DEFINES[ 'EGL_MIPMAP_LEVEL' ], level )
                OpenGLES1.Viewport( 0, 0, w, h );
                if level == 1:
                        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                else:
                        OpenGLES1.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                if w == 1 and h == 1:
                        break
                
                w     = w / 2
                h     = h / 2
                level = level + 1

                if w < 1:
                        w = 1
                        
                if h < 1:
                        h = 1

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        rx = 1.0
        ry = 1.0
        
        mesh                = createPlane( self.gridx, self.gridy, rx, ry, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        useVertexData( modules, indexTracker, vertexDataSetup )
        
        texture1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture1Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'OFF' )

        Egl.BindTexImage( displayIndex, pbufSurfaceIndex, 'EGL_BACK_BUFFER' )
       
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )        
        OpenGLES1.LoadIdentity()
        drawVertexData( modules, vertexDataSetup )       

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################            
class GenerateMipmapsForPbufferAsTexture( vt.VisualTestCase ):
    
    ''' Test generating mipmaps when using an EGL surface as an OpenGLES1
    texture. Draws a solid green texture scaled across the surface. Requires
    EGL.
    '''
   
    testValues = { ( 'format' ) : [ ( 'OPENGLES1_RGB565' ),
                                    ( 'OPENGLES1_RGB888' ),
                                    ( 'OPENGLES1_RGBA8888' ) ]
                   }
    
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES1 generate mipmaps for pbuffer as texture %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]

        textureW = toPot( WIDTH, True )[ 0 ]
        textureH = toPot( HEIGHT, True )[ 0 ]    
        
        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        if p[ 4 ] == 0:
            rgbTexture    = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        else:
            rgbaTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufSurfaceIndex,
                                  pbufConfigIndex,
                                  textureW, textureH,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'ON',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )

        OpenGLES1.Viewport( 0, 0, textureW, textureH );
        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        rx = 2.0 * float( textureW ) / float( WIDTH )
        ry = 2.0 * float( textureH ) / float( HEIGHT )
        
        mesh                = createPlane( self.gridx, self.gridy, rx, ry, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        useVertexData( modules, indexTracker, vertexDataSetup )
        
        texture1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture1Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'ON' )

        Egl.BindTexImage( displayIndex, pbufSurfaceIndex, 'EGL_BACK_BUFFER' )
       
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )        
        OpenGLES1.LoadIdentity()
        drawVertexData( modules, vertexDataSetup )       

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################            
class GenerateMipmapsForNpotPbufferAsTextureExt( vt.VisualTestCase ):
    
    ''' Test generating mipmaps when using a NPOT EGL surface as an OpenGLES1
    texture. Draws a solid green texture scaled across the surface. Requires EGL
    and GL_OES_texture_npot extension.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) : [ ( 255, 256, 'OPENGLES1_RGB565' ),
                                                       ( 256, 255, 'OPENGLES1_RGB888' ),
                                                       ( 255, 257, 'OPENGLES1_RGBA8888' ) ]
                   }
    
    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES1 generate mipmaps for npot pbuffer as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]

        OpenGLES1.CheckExtension( 'GL_OES_texture_npot' )
        
        textureW = self.width
        textureH = self.height
        
        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        if p[ 4 ] == 0:
            rgbTexture    = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        else:
            rgbaTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufSurfaceIndex,
                                  pbufConfigIndex,
                                  textureW, textureH,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'ON',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, -1 )
        Egl.MakeCurrent( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )

        OpenGLES1.Viewport( 0, 0, textureW, textureH );
        OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        rx = 2.0 * float( textureW ) / float( WIDTH )
        ry = 2.0 * float( textureH ) / float( HEIGHT )
        
        mesh                = createPlane( self.gridx, self.gridy, rx, ry, False, [ True ] )
        vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
        useVertexData( modules, indexTracker, vertexDataSetup )
        
        texture1Index = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( texture1Index )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'ON' )

        Egl.BindTexImage( displayIndex, pbufSurfaceIndex, 'EGL_BACK_BUFFER' )
       
        OpenGLES1.TexEnvMode( 'GL_REPLACE' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )        
        OpenGLES1.LoadIdentity()
        drawVertexData( modules, vertexDataSetup )       

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################
class WindowSurface( vt.VisualTestCase ):
    
    ''' Test for window surfaces. Test draws a green surface. Requires EGL.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGB888',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENGLES1 window surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        windowIndex = 0
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, WIDTH, HEIGHT, 'SCT_COLOR_FORMAT_DEFAULT' )

        # Create pbuffer from the image.
        p = SURFACE_PARAMETERS2[ self.format ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT'] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )       
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, 1 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES1.ClearColor( [ 0, 1, 0, 1 ] )       
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES1.CheckError( '' )               
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class PbufferSurface( vt.VisualTestCase ):
    
    ''' Test for pbuffer surfaces. Test draws a green surface. Requires EGL.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGB888',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENGLES1 pbuffer surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        p = SURFACE_PARAMETERS2[ self.format ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES_BIT'] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  surfaceIndex,
                                  configIndex,
                                  WIDTH,
                                  HEIGHT,
                                  'EGL_NO_TEXTURE', 'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, 1 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )       

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES1.ClearColor( [ 0, 1, 0, 1 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES1.CheckError( '' )               
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################
class PixmapSurface( vt.VisualTestCase ):
    
    ''' Test for pixmap surfaces. Test draws a green surface. Requires EGL.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGB888',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENGLES1 pixmap surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex, WIDTH, HEIGHT, self.format, -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENGL_ES_BIT' ]
                       ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex,  configIndex,  pixmapIndex, attributes )
        
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 pixmapIndex,
                                 'EGL_NONE',
                                 'EGL_NONE' )
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, 1 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES1.ClearColor( [ 0, 1, 0, 1 ] )        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES1.CheckError( '' )               
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################
class SharedPixmapSurface( vt.VisualTestCase ):
    
    ''' Test for shared pixmap surfaces. Test draws a green surface. Requires
    EGL. Requires EGL_NOK_pixmap_type_rsgimage extension in Symbian.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENGLES1 shared pixmap surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        # Check extensions.
        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )

        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( pixmapIndex,
                                WIDTH, HEIGHT,
                                self.format,
                                [ 'SCT_USAGE_OPENGLES1_SURFACE' ],
                                -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENGL_ES_BIT' ]
                       ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex, configIndex, pixmapIndex, attributes )
        
        attributes = []
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurfaceExt( displayIndex, surfaceIndex, configIndex, pixmapIndex, attributes )
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, 1 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES1.ClearColor( [ 0, 1, 0, 1 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES1.CheckError( '' )               
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class Extensions( vt.VisualTestCase ):

        ''' The purpose of the test: test OpenGLES1 EGL extensions.

        Expected output: surface cleared to green if the particular OpenGLES1
        EGL extension is supported. Viewport size 320*240.
        '''
        
        testValues = { ( 'extension', 'functions' ) : [ ( 'EGL_KHR_lock_surface', [ 'eglLockSurfaceKHR',
                                                                                    'eglUnlockSurfaceKHR' ] ),
                                                        ( 'EGL_KHR_image', [] ),
                                                        ( 'EGL_KHR_vg_parent_image', [] ),
                                                        ( 'EGL_KHR_gl_texture_2D_image', [] ),
                                                        ( 'EGL_KHR_gl_texture_cubemap_image', [] ),
                                                        ( 'EGL_KHR_gl_renderbuffer_image', [] ),
                                                        ( 'EGL_KHR_image_base', [ 'eglCreateImageKHR',
                                                                                  'eglDestroyImageKHR' ] ),
                                                        ( 'EGL_KHR_image_pixmap', [] ),
                                                        ( 'EGL_KHR_lock_surface2', [ 'eglLockSurfaceKHR',
                                                                                     'eglUnlockSurfaceKHR' ] ),
                                                        ( 'EGL_NOK_resource_profiling', [ 'eglQueryProfilingDataNOK' ] ),
                                                        ( 'EGL_NOK_resource_profiling2', [ 'eglQueryProfilingDataNOK' ] ),
                                                        ( 'EGL_NOK_surface_scaling', [ 'eglSetSurfaceScalingNOK',
                                                                                       'eglQuerySurfaceScalingCapabilityNOK' ] ),
                                                        ( 'EGL_NOK_pixmap_type_rsgimage', [] ),
                                                        ( 'EGL_NOK_window_type_eglwindowbase', [] ),
                                                        ( 'EGL_SYMBIAN_COMPOSITION', [] ),
                                       ]
                       }
                                                                             
        def __init__( self, extension, functions ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.extension = extension
                self.functions = functions
                self.name    = "OPENGLES1 egl extensions, extension=%s" % ( self.extension, )
        
        def build( self, target, modules ):
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Egl       = modules[ 'Egl' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]                
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )                
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()               

                Egl.CheckExtension( state[ 0 ], self.extension )
                
                for f in self.functions:
                        Egl.CheckFunction( f )
                        
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.CheckError( '' )               
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
