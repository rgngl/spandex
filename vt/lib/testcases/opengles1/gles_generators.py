#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import math
import os

# TODO: this is not the greatest trick
MESHPATH = os.path.join( os.getcwd().replace( "suites", "lib/testcases" ), "meshes" )

# ----------------------------------------------------------------------
def defaultMaterial():
    return {
        'shininess' : 102.400001526,
        'specular'  : [ 2.0, 2.0, 2.0, 1.0 ],
        'ambient'   : [ 0.0, 0.5, 0.14467704296112061, 1.0 ],
        'diffuse'   : [ 0.0, 1.0, 0.28935408592224121, 1.0 ],
        'emission'  : [ 0.0, 0.0, 0.0, 1.0 ],
      }

# ----------------------------------------------------------------------
def loadMesh( filename ):
    global MESHPATH
    filename = os.path.join( MESHPATH, filename )
    mesh = {}
    try:
        execfile( filename, mesh )
    except ImportError:
        raise Exception( "Mesh load failed" )
    
    mesh[ 'material' ] = mesh[ 'materials' ][ 0 ]
    return mesh

# ----------------------------------------------------------------------
def smoothCube():
    return loadMesh( "cube_smooth.mesh" )

# ----------------------------------------------------------------------
def triangleFan( slices = 10 ):
    # start with the center vertex
    vertices = [ 0.0, 0.0, 0.0 ]
    indices  = [ 0 ]
    colorsb  = [ 127, 127, 127, 255 ]
    colorsf  = [ 0.5, 0.5, 0.5, 1.0 ]
    normals  = []
    
    for s in xrange( slices ):
        a = math.radians( 360.0 / slices ) * s
        x = math.sin( a )
        y = math.cos( a )
        vertices.extend( [ x, y, 0.0 ] )
        normals.extend( [ 0.0, 0.0, 1.0 ] )
        indices.append( s + 1 )
        cf = float( s ) / slices
        cb = int( cf * 255 )
        colorsb.extend( [ 255 - cb, cb, cb, 255 ] )
        colorsf.extend( [ 1.0 - cf, cf, cf, 1.0 ] )
        
    # close the fan
    indices.append( 1 )
    
    return { 'vertices' : vertices,
             'indices'  : indices,
             'colorsb'  : colorsb,
             'colorsf'  : colorsf,
             'normals'  : normals,
             'material' : defaultMaterial()
             }

# ----------------------------------------------------------------------
def point():
    return { 'vertices'  : [ 0.0, 0.0, 0.0 ],
             'indices'   : [ 0 ],
             'colorsb'   : [ 255, 0, 0, 255 ],
             'colorsf'   : [ 1.0, 0.0, 0.0, 1.0 ],
             'texcoords' : [ 0.0 ],
             'material'  : defaultMaterial()
             }

# ----------------------------------------------------------------------
def points( num = 10, radius = 1.0 ):
    vertices = []
    indices  = []
    colorsb  = []
    colorsf  = []
    
    for p in xrange( num ):
        a = math.radians( 360.0 / num ) * p
        x = math.sin( a ) * radius
        y = math.cos( a ) * radius
        vertices.extend( [ x, y, 0.0 ] )
        cf = float( p ) / num
        cb = int( cf * 255 )
        colorsb.extend( [ cb, 255 - cb, cb, 255 ] )
        colorsf.extend( [ cf, 1.0 - cf, cf, 1.0 ] )
        indices.append( p )
        
    return { 'vertices' : vertices,
             'indices'  : indices,
             'colorsb'  : colorsb,
             'colorsf'  : colorsf,
             'material' : defaultMaterial(),
             }

# ----------------------------------------------------------------------
def lines( num = 10, inner_radius = 0.5, outer_radius = 1.0 ):
    vertices = []
    indices  = []
    colorsb  = []
    colorsf  = []
    
    for p in xrange( num ):
        a  = math.radians( 360.0 / num ) * p
        x1 = math.sin( a ) * inner_radius
        y1 = math.cos( a ) * inner_radius
        x2 = math.sin( a ) * outer_radius
        y2 = math.cos( a ) * outer_radius
        
        vertices.extend( [ x1, y1, 0.0 ] )
        cf = float( p ) / num
        cb = int( cf * 255 )
        colorsb.extend( [ cb, 255 - cb, cb, 255 ] )
        colorsf.extend( [ cf, 1.0 - cf, cf, 1.0 ] )
        vertices.extend( [ x2, y2, 0.0 ] )
        cf = float( p ) / num
        cb = int( cf * 255 )
        colorsb.extend( [ 255 - cb, cb, 255 - cb, 255 ] )
        colorsf.extend( [ 1.0 - cf, cf, 1.0 - cf, 1.0 ] )
        indices.append( p * 2 )
        indices.append( p * 2 + 1 )
        
    return { 'vertices' : vertices,
             'indices'  : indices,
             'colorsb'  : colorsb,
             'colorsf'  : colorsf,
             'material' : defaultMaterial(),
             }

# ----------------------------------------------------------------------
def triangleQuad():
    return loadMesh( "plane.mesh" )

# ----------------------------------------------------------------------
def triangle():
    return loadMesh( "triangle.mesh" )

# ----------------------------------------------------------------------
def polygon( width = 1.0, height = 1.0 ):
    vertices = []
    indices  = []
    colorsb  = []
    colorsf  = []
    normals  = []
        
    vertices.extend( [  -width, -height, 0.0,
                        -width,  height, 0.0,
                         width, -height, 0.0,
                         width,  height, 0.0,
                        -width,  height, 0.0,
                         width, -height, 0.0 ] )
    normals.extend( [ 0.0, 0.0, 1.0 ] )
    colorsb.extend( [ 255, 0, 0, 255 ] )
    colorsf.extend( [ 255, 0, 0, 255 ] )

    indices.append( 0 )
    indices.append( 1 )
    indices.append( 2 )
    indices.append( 3 )
    indices.append( 4 )
    indices.append( 5 )
    
    return { 'vertices' : vertices,
             'indices'  : indices,
             'colorsb'  : colorsb,
             'colorsf'  : colorsf,
             'normals'  : normals,
             'material' : defaultMaterial(),
             }

# ----------------------------------------------------------------------        
def triangleStrip( num = 10, inner_radius = 0.5, outer_radius = 1.0 ):
    vertices = []
    indices  = []
    colorsb  = []
    colorsf  = []
    normals  = []
    
    for p in xrange( num ):
        a  = math.radians( 360.0 / num ) * p
        x1 = math.sin( a ) * inner_radius
        y1 = math.cos( a ) * inner_radius
        x2 = math.sin( a ) * outer_radius
        y2 = math.cos( a ) * outer_radius
        
        vertices.extend( [ x1, y1, 0.0 ] )
        normals.extend( [ 0.0, 0.0, 1.0 ] )
        cf = float( p ) / num
        cb = int( cf * 255 )
        colorsb.extend( [ cb, cb, 255 - cb, 255 ] )
        colorsf.extend( [ cf, 1.0 - cf, cf, 1.0 ] )
        vertices.extend( [ x2, y2, 0.0 ] )
        normals.extend( [ 0.0, 0.0, 1.0 ] )
        cf = float( p ) / num
        cb = int( cf * 255 )
        colorsb.extend( [ 255 - cb, cb, 255 - cb, 255 ] )
        colorsf.extend( [ 1.0 - cf, 1.0 - cf, 1.0 - cf, 1.0 ] )
        indices.append( p * 2 )
        indices.append( p * 2 + 1 )
        
    # close the loop
    indices.append( 0 )
    indices.append( 1 )
    
    return { 'vertices' : vertices,
             'indices'  : indices,
             'colorsb'  : colorsb,
             'colorsf'  : colorsf,
             'normals'  : normals,
             'material' : defaultMaterial(),
             }
