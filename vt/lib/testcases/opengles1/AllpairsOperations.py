#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

WIDTH, HEIGHT = ( 320, 240, )

# ----------------------------------------------------------------------
class BufferObjects( vt.VisualTestCase ):
    
    """ Testing buffer objects VertexBuffer and ColorBuffer. A triangle is drawn
    twice, first with original data, then with another vertex buffer using
    BindBuffer and a modified color buffer using BufferSubData."""
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.name = "OPENGLES1 buffer objects vertex and color"
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )           # vertices 1
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )   # colors 
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )  # indices
        OpenGLES1.CreateArray( 3, 'GL_FLOAT' )           # vertices 2
        OpenGLES1.CreateArray( 4, 'GL_UNSIGNED_BYTE' )   # replacement colors
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        tri = triangle()
        # Alter data for the second set of vertices
        verts2 = []
        verts2.extend( tri[ 'vertices' ] )
        verts2[ 0 * 3 ]    -= 0.1
        verts2[ 1 * 3 ]    += 0.1
        verts2[ 2 * 3 +1 ] -= 0.1
        newcolors = [ 0, 255, 255, 255,
                      0, 255, 255, 255,
                      0, 255, 255, 255,
                      ]
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, [ 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255 ] )
        OpenGLES1.AppendToArray( 2, tri[ 'indices' ] )
        OpenGLES1.AppendToArray( 3, verts2 )
        OpenGLES1.AppendToArray( 4, newcolors )

        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.GenBuffer( 0 )
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 0,  0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0  )
        
        OpenGLES1.GenBuffer( 1 )
        OpenGLES1.BufferData( 1, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 1, 0, 0 )
        OpenGLES1.ColorBuffer( 1, 'ON', 4, 0 )
        
        OpenGLES1.GenBuffer( 2 )
        OpenGLES1.BufferData( 2, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW',  2, 0, 0 )
        
        OpenGLES1.GenBuffer( 3 )
        OpenGLES1.BufferData( 3, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 3, 0, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.BufferDrawElements( 2, 'ON', 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0 )
        
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 3, 0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0, ) # Change the vertex buffer
        
        OpenGLES1.BufferData( 1, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 4, 0, 0 )
        OpenGLES1.BufferSubData( 1, 'ON', 'GL_ARRAY_BUFFER', 0, 4, 0, 12 ) # Modify color buffer
        OpenGLES1.ColorBuffer( 1, 'ON', 4, 0 )
        
        OpenGLES1.BufferDrawElements( 2, 'ON', 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0 )
        
        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------
class BufferObjectsVertex( vt.VisualTestCase ):
    
    """ This test is for testing buffer objects. Test draws 2 triangles. First
    one trough VertexPointer with DrawElements and second one trough
    VertexBuffer with DrawElementsBuffer. 1. triangle is created by calling
    function triangle() 2. triangle is created by altering values from
    1. triangle. Expected output: Two ovelapping triangles with differing
    vertices and coloring.  """

    testValues = { ( 'DrawMode', 'ArrayType' ) : [ ( 'GL_STATIC_DRAW','GL_ARRAY_BUFFER' ),
                                                   ( 'GL_DYNAMIC_DRAW','GL_ELEMENT_ARRAY_BUFFER' ) ],
                   }
    
    def __init__( self, DrawMode, ArrayType ):
        vt.VisualTestCase.__init__( self )
        self.DrawMode  = DrawMode
        self.ArrayType = ArrayType
        self.name = "OPENGLES1 buffer objects vertex %s %s" % ( DrawMode, ArrayType, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices 1
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )  # colors 
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.CreateArray( 3, 'GL_FLOAT' )          # vertices 1
        OpenGLES1.CreateArray( 4, 'GL_UNSIGNED_BYTE' )  # colors
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )

        tri = triangle()
        verts2 = []
        verts2.extend( tri[ 'vertices' ] )
        verts2[ 0 * 3 ]     -= 0.1
        verts2[ 1 * 3 ]     += 0.1
        verts2[ 2 * 3 + 1 ] -= 0.1
        newcolors = [ 255, 255, 255, 255,
                      255,   0, 255, 255,
                        0, 255, 255, 255 ]
           
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'indices' ] )
        OpenGLES1.AppendToArray( 3, verts2 )
        OpenGLES1.AppendToArray( 4, newcolors )
        
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 2)
        
        OpenGLES1.GenBuffer( 0 )
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 3, 0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0  )
        
        OpenGLES1.GenBuffer( 1 )
        OpenGLES1.BufferData( 1, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 4, 0, 0 )
        OpenGLES1.ColorBuffer( 1, 'ON', 4, 0 )
        
        OpenGLES1.GenBuffer( 2 )
        OpenGLES1.BufferData( 2, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 2, 0, 0 )
        
        OpenGLES1.BufferDrawElements( 2, 'ON', 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0 )
 
        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------        
class BufferObjectsTextCoord( vt.VisualTestCase ):
    
    """ This test is for testing buffer objects TexCoordbuffer and
    VertexBuffer. Test draws two triangles. One with enabled
    GL_TEXTURE_COORD_ARRAY and with with disabled GL_TEXTURE_COORD_ARRAY and
    altered vertices. Expected output: A triangle filled with CheckerTextureData
    overlapped by triangle without filling. """
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.name = "OPENGLES1 buffer objects texcoord"
  
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 0, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 10 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
 
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices 1
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' ) # textcoords 
        OpenGLES1.CreateArray( 2, 'GL_FLOAT' )          # indices
        OpenGLES1.CreateArray( 3, 'GL_FLOAT' )          # vertices2
        
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )      
        OpenGLES1.CreateCheckerTextureData( 0, [ 1, 1, 1, 1 ], [ 0, 0, 0, 1 ], 32, 32, 128, 128, 'ON', 'OPENGLES1_RGB888' )
        OpenGLES1.TexImage2D( 0, 'GL_TEXTURE_2D' )
        
        tri = triangle()

        verts2 = []
        verts2.extend( tri[ 'vertices' ] )
        verts2[ 0 * 3 ]     -= 0.1
        verts2[ 1 * 3 ]     += 0.1
        verts2[ 2 * 3 + 1 ] -= 0.1
        
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'indices' ] )
        OpenGLES1.AppendToArray( 3, verts2 )
                
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )  

        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.GenBuffer( 0 )
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 0, 0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0 )
        
        OpenGLES1.GenBuffer( 1 )
        OpenGLES1.BufferData( 1, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 2, 0, 0 )
        OpenGLES1.TexCoordBuffer( 1, 'ON', 2, 0 )
        
        OpenGLES1.GenBuffer( 2 )
        OpenGLES1.BufferData( 2, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 1, 0, 0 )
        
        OpenGLES1.GenBuffer( 3 )
        OpenGLES1.BufferData( 3, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 3, 0, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.BufferDrawElements( 2, 'ON', 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0 )

        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 3, 0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0 )
        OpenGLES1.DisableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        OpenGLES1.BufferDrawElements( 2, 'ON', 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0 )
        
        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------        
class BufferObjectsNormal( vt.VisualTestCase ):
    
    """ Testing NormalBufffer. The test draws a mesh which is lit with the spot
    light. The lighting on the object should be visible only when normal arrays
    are enabled."""
    
    testValues = { 'enable' : [ True, False ]
                   }
    
    def __init__( self, enable ):
        vt.VisualTestCase.__init__( self )
        self.enable = enable
        self.name = 'OPENGLES1 buffer objects normal %s' % ( { True : 'enabled', False : 'disabled' }[ self.enable ] )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.Enable( 'GL_LIGHTING' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Enable( 'GL_NORMALIZE' )
        light = { 'Ambient'              : [ 0.0, 0.0, 0.0, 1.0 ],
                  'Diffuse'              : [ 0.3, 0.3, 0.3, 1.0 ],
                  'Specular'             : [ 0.5, 0.5, 0.0, 1.0 ],
                  'Position'             : [ 1.0, 1.0, 1.0, 0.0 ],
                  'SpotDirection'        : [ 0.0, 0.0, -1.0 ],
                  'SpotExponent'         : 0.0,
                  'SpotCutoff'           : 180.0,
                  'ConstantAttenuation'  : 1.0,
                  'LinearAttenuation'    : 0.0,
                  'QuadraticAttenuation' : 0.0,
                  }
        OpenGLES1.Enable('GL_LIGHT0' )
        OpenGLES1.Light( 0,
                         light[ 'Ambient' ],
                         light[ 'Diffuse' ],
                         light[ 'Specular' ],
                         light[ 'Position' ],
                         light[ 'SpotDirection' ],
                         light[ 'SpotExponent' ],
                         light[ 'SpotCutoff' ],
                         light[ 'ConstantAttenuation' ],
                         light[ 'LinearAttenuation' ],
                         light[ 'QuadraticAttenuation' ] )
        OpenGLES1.LightModelTwoSided( 'ON' )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [0, 0, -2] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        
        data = loadMesh("sphere.mesh")
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'normals' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        OpenGLES1.Material( data[ 'material' ][ 'ambient' ],
                            data[ 'material' ][ 'diffuse' ],
                            data[ 'material' ][ 'specular' ],
                            data[ 'material' ][ 'emission' ],
                            data[ 'material' ][ 'shininess' ]
                            )
        
        # might change the pointer.
        OpenGLES1.GenBuffer( 0 )
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 0, 0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0 )
        
        OpenGLES1.GenBuffer( 1 )
        OpenGLES1.BufferData( 1, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 1, 0, 0 )
        
        if self.enable:
            OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )
            OpenGLES1.NormalBuffer( 1, 'ON', 0 )
         
        OpenGLES1.GenBuffer( 2 )
        OpenGLES1.BufferData( 2, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 2, 0, 0 )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 4, 4, 4 ] )
        OpenGLES1.Rotate( 180, [ 0, 0, 1 ] )
        
        OpenGLES1.BufferDrawElements( 2, 'ON', 'GL_TRIANGLES', len( data[ 'indices' ] ), 0 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------        
class BufferObjectsPoints( vt.VisualTestCase ):
    
    """ Testing buffer for points. Expected output: Incremental sized points
    with gradient color."""
    
    def __init__(self):
        vt.VisualTestCase.__init__( self )
        self.point_sizes = ( 0.1, 0.2, 0.4, 0.8, 1.6, 3.2 )
        self.start_scale = [ 0.1, 0.1, 0.1 ]
        self.scale_step  = [ 1.75, 1.75, 1.75 ]
        self.rot_step    = 5.0
        self.name = 'OPENGLES1 buffer objects (GL_POINTS)'
        self.__doc__ += " Point sizes: %s" % ( str( self.point_sizes ), )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' ) # vertices
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_BYTE' ) # colors
        
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
     
        OpenGLES1.Disable( 'GL_POINT_SMOOTH' )
        
        data = points(10) # create simple quad
        OpenGLES1.AppendToArray( 0, data['vertices'] )
        OpenGLES1.AppendToArray( 2, data['colorsb'] )
        OpenGLES1.AppendToArray( 1, data['indices'] )

        # might change the pointer.
        OpenGLES1.GenBuffer( 0 )
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 0, 0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0 )
        
        OpenGLES1.GenBuffer( 1 )
        OpenGLES1.BufferData( 1, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 1, 0, 0 )
        
        OpenGLES1.GenBuffer( 2 )
        OpenGLES1.BufferData( 2, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 2, 0, 0 )
        OpenGLES1.ColorBuffer( 2, 'ON', 4, 0 )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( self.start_scale )
        
        for size in self.point_sizes:
                OpenGLES1.PointSize( size )
                OpenGLES1.BufferDrawElements( 1, 'ON', 'GL_POINTS', len( data[ 'indices' ] ), 0 )
                OpenGLES1.Rotate( self.rot_step, [ 0.0, 0.0, 1.0 ])
                OpenGLES1.Scale( self.scale_step )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                

# ----------------------------------------------------------------------        
class BufferObjectsMatrixModeTexture( vt.VisualTestCase ):
    
    """ This test is for testing GL_TEXTURE matrixmode. Test draws a triangle
    which is filled with checkertexture. Matrixmode is set to GL_TEXTURE and
    texture is then altered with one of the following functions: Rotate, Scale,
    Frustum, Ortho, MultMatrix and Translate. Expected output: A triangle filled
    with checkertexture which is variating depending on the used matrix
    manipulation function. """

    testValues = { 'Function' : [ 'Unmodified',
                                  'Scale',
                                  'Rotate',
                                  'Ortho',
                                  'Translate',
                                  'MultMatrix' ],
                   }
    
    def __init__( self, Function ):
        vt.VisualTestCase.__init__( self )
        self.Function = Function
        self.name = "OPENGLES1 matrix objects GL_TEXTURE %s" % ( Function, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 10 )
        
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices 1
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' ) # textcoords 
        OpenGLES1.CreateArray( 2, 'GL_FLOAT' )          # indices
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.CreateCheckerTextureData( 0, [ 1, 1, 1, 1 ], [ 0, 0, 0 ,1 ], 16, 16, 128, 128, 'ON', 'OPENGLES1_RGB888' )
        OpenGLES1.TexImage2D( 0, 'GL_TEXTURE_2D'  )
        
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.MatrixMode( 'GL_TEXTURE' )

        matrix = [ 0.25,  0.0, 0.0, 0.0,
                    0.0, 0.25, 0.0, 0.0,
                    0.0,  0.0, 1.0, 0.0,
                    0.0,  0.0, 0.0, 1.0,
                   ]
        
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )  
        
        tri = triangle()
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'indices' ] )      
   
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.TexCoordPointer( 2, 2, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        ## Modify texture matrix ############
        if self.Function == 'Rotate':
            OpenGLES1.Rotate( 45, [ 0, 0, 1 ] )
        if self.Function == 'Scale':
            OpenGLES1.Scale( [ 0.75, 0.75, 0.75 ] )
        if self.Function == 'Translate':
            OpenGLES1.Translate( [ 0.1, 0.1, 2 ] )
        if self.Function == 'Ortho':
            OpenGLES1.Ortho( -1, 2, -1, 2, 1, 100 )
        if self.Function == 'MultMatrix':
            OpenGLES1.MultMatrix( matrix )
                   
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 1 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
