#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

WIDTH, HEIGHT = ( 320, 240, )

# ----------------------------------------------------------------------
class PointsSizeBuffer( vt.VisualTestCase ):
    
    """ Testing PointSizeBuffer. Test strores pointsizes to buffer and draws
    them with DrawElementBuffer.Expected output: Incremental sized points with
    radiant color."""
    
    def __init__(self):
        vt.VisualTestCase.__init__( self )
        self.PointSizes = [ 2, 4, 8, 14, 22 ]
        self.name = 'OPENGLES1 buffer objects pointsizebuffer (GL_POINTS)'
        self.__doc__ += " Point sizes: %s" % ( str( self.PointSizes, ) )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )
        
        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_BYTE' )  # colors
        OpenGLES1.CreateArray( 3, 'GL_FLOAT' )          # point sizes
        
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_POINT_SIZE_ARRAY_OES' )
        
        data = points( 5 ) # create simple quad
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 2, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 1, data[ 'indices' ] )
        OpenGLES1.AppendToArray( 3, self.PointSizes )

        # might change the pointer.
        OpenGLES1.GenBuffer( 0 )
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 0, 0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0 )
        
        OpenGLES1.GenBuffer( 1 )
        OpenGLES1.BufferData( 1, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 1, 0, 0 )
        
        OpenGLES1.GenBuffer( 2 )
        OpenGLES1.BufferData( 2, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 2, 0, 0 )
        OpenGLES1.ColorBuffer( 2, 'ON', 4, 2 )
        
        OpenGLES1.GenBuffer( 3 )
        OpenGLES1.BufferData( 3, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 3, 0, 0 )
        OpenGLES1.PointSizeBuffer( 3, 'ON', 3 )
       
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        
        OpenGLES1.BufferDrawElements( 1, 'ON', 'GL_POINTS', len( data[ 'indices' ] ), 0 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
        
# ----------------------------------------------------------------------        
class PointsSizeRasterization( vt.VisualTestCase ):
    
    ''' The purpose of the test: Testing point size rasterization.
        
    The test steps:
    1. The test define vertices colors and indices for a single point.
    2. The test changes the point size with OpenGLES1.PoinSize according PointSizes array.
    3. The test draws points translating to right avoiding overlapping.

    Expected output: Six left to right incremental sizing points. Note that the
    point is not smoothed (looks like polygon). This is because the smoothing
    supports only size 1.'''
    
    def __init__(self):
        vt.VisualTestCase.__init__( self )
        self.PointSizes      = [ 2, 4, 8, 14, 22, 32 ]
        self.TranslateValues = [ 0.25, 0.5, 1, 1.5, 2, 2.5 ]
        self.name = 'OPENGLES1 rasterization basic point (GL_POINTS)'
        self.__doc__ += " Point sizes: %s TranslateValues %s" % ( str( self.PointSizes ), str( self.TranslateValues ), )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ -4.5, -1, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_BYTE' )  # colors
        
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
                
        data = points( 1 ) # create simple quad
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 2, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 1, data[ 'indices' ] )
                
        # might change the pointer.
        OpenGLES1.GenBuffer( 0 )
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 0, 0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0 )

        OpenGLES1.GenBuffer( 1 )
        OpenGLES1.BufferData( 1, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 1, 0, 0 )
                
        OpenGLES1.GenBuffer( 2 )
        OpenGLES1.BufferData( 2, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 2, 0, 0 )
        OpenGLES1.ColorBuffer( 2, 'ON', 4, 0 )
                
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Translate( [ 1.5, 0, -2 ] )
        TranslateIndex = 0
        for size in self.PointSizes:
            OpenGLES1.PointSize( size )
            OpenGLES1.BufferDrawElements( 1, 'ON', 'GL_POINTS', len( data[ 'indices' ] ), 0 )
            OpenGLES1.Translate( [ self.TranslateValues[ TranslateIndex ], 0, 0 ] )
            TranslateIndex += 1

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------        
class PointParameter( vt.VisualTestCase ):

    """ The purpose of the test: Testing the limitation of point size with
    OpenGLES1.PointParameter. Optional feature.
                
    The test steps:
    1. The test define vertices colors and indices for a single point.
    2. The test changes the point size with OpenGLES1.PoinSize according PointSizes array.
    3. If the PointParameter is enabled the point sizes are limited with min of 8 and max on 20.
    4. The test draws points translating to right avoiding overlapping.
                
    Expected output: When PointParameter is disabled, 4 increasing size point
    should appear to screen and when PointParameter is enabled first point
    should be forced to size 8, second one should be size 12, third one size 20,
    and the last one should be forced to size 20."""
    
    testValues = { 'PointParameter' : [ 'DISABLED', 'ENABLED' ], }
    
    def __init__(self, PointParameter):
        vt.VisualTestCase.__init__( self )
        # One value over the minimun value and the rest below or equal 
        self.PointSizes      = [ 4, 12, 20, 28 ]
        self.TranslateValues = [ 0.6, 1.4, 2.2, 3.2 ]
        self.PointParameter  = PointParameter
        self.name = 'OPENGLES1 rasterization  pointparameter %s (GL_POINTS) ' % ( PointParameter, )
        self.__doc__ += " Point sizes: %s TranslateValues %s PointParameter %s" % ( str( self.PointSizes ),
                                                                                    str( self.TranslateValues ),
                                                                                    self.PointParameter, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ -4.5, -1, -3 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_BYTE' )  # colors
                
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
                
        data = points( 1 ) # create simple quad
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 2, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 1, data[ 'indices' ] )
        
        # might change the pointer.
        OpenGLES1.GenBuffer( 0 )
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 0, 0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0)
                
        OpenGLES1.GenBuffer( 1 )
        OpenGLES1.BufferData( 1, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 1, 0, 0 )
                
        OpenGLES1.GenBuffer( 2 )
        OpenGLES1.BufferData( 2, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 2, 0, 0 )
        OpenGLES1.ColorBuffer( 2, 'ON', 4, 0 )
                
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Translate( [ 0, 0, -1.0 ] )

        TranslateIndex = 0
        for size in self.PointSizes:
            OpenGLES1.Translate( [ self.TranslateValues[ TranslateIndex ], 0.0, -1.0 ] )
            TranslateIndex += 1
                        
            OpenGLES1.PointSize( size )
                       
            if self.PointParameter == 'ENABLED':
                OpenGLES1.PointParameter( 8.0, 20.0, 0.0, [ 1.0, 0.0, 0.0 ] )
                
            OpenGLES1.BufferDrawElements( 1, 'ON', 'GL_POINTS', len( data[ 'indices' ] ), 0 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

    
# ----------------------------------------------------------------------
class PointSprites( vt.VisualTestCase ):
    
    """ This test is for testing point sprite texturing with
    OpenGLES1.TextEnv. The test draws a triangle. (GL_POINTS -> The triangle is
    formed by three end points.) The points are then filled with texture 1 (2x2
    checker) which is modified with texture 2 (4x4 checker). The modification is
    done by OpenGLES1.TexEnv with varying modes. Expected output: A triangle
    formed by three end points with varying texture."""
      
    testValues = { 'Colors' : [ [ 0.5, 0.5, 0.0, 0.5 ],
                                [ 0.0, 0.5, 0.5, 0.5 ],
                                [ 0.5, 0.0, 0.5, 0.5 ],
                                ],
                   'Mode'   : [ 'GL_MODULATE', 'GL_BLEND' , 'GL_ADD' ]
                   }
    
    def __init__(self, Colors, Mode):
        vt.VisualTestCase.__init__( self )
        self.Colors    = Colors
        self.Mode     = Mode
        self.textures = [ { 'colors' : ( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] ),
                            'cx'     : 2,
                            'cy'     : 2,
                            'width'  : 64,
                            'height' : 64,
                            },
                          { 'colors' : ( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] ),
                            'cx'     : 4,
                            'cy'     : 4,
                            'width'  : 64,
                            'height' : 64,
                            } ]
        
        self.name = 'OPENGLES1 rasterization pointsprites (GL_POINTS) colors %s mode %s' % ( str( Colors ), Mode, )
        self.__doc__ += " Point size: 20"
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [0, 0, -1] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )          # texture
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' ) # indices
       
        OpenGLES1.Enable( 'GL_POINT_SPRITE_OES' )

        data = triangle()   
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # might change the pointer.
        OpenGLES1.GenBuffer( 0 )
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 0, 0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0 )

        OpenGLES1.GenBuffer( 1 )
        OpenGLES1.BufferData( 1, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 1, 0, 0 )
        
        OpenGLES1.GenBuffer( 2 )
        OpenGLES1.BufferData( 2, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 2, 0, 0 )

        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        
        for index in range( 2 ):
            OpenGLES1.ActiveTexture( index )
            OpenGLES1.ClientActiveTexture( index )
            OpenGLES1.TexCoordBuffer( 1, 'ON', 2, 0 )
            OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )               
            OpenGLES1.Enable( 'GL_TEXTURE_2D' )
                
            OpenGLES1.GenTexture( index )
            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', index )
            OpenGLES1.CreateCheckerTextureData( index,
                                                self.textures[ index ][ 'colors' ][ 0 ],
                                                self.textures[ index ][ 'colors' ][ 1 ],
                                                self.textures[ index ][ 'cx' ],
                                                self.textures[ index ][ 'cy' ],
                                                self.textures[ index ][ 'width' ],
                                                self.textures[ index ][ 'height' ],
                                                'ON',
                                                'OPENGLES1_RGBA8888' )
            OpenGLES1.TexImage2D( index, 'GL_TEXTURE_2D' )
      
            if index == 0:
                OpenGLES1.TexEnv( 'GL_REPLACE', self.Colors,'ON' )
            else:
                OpenGLES1.TexEnv( self.Mode, self.Colors, 'ON' )
            
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        OpenGLES1.PointSize( 20 )
        OpenGLES1.BufferDrawElements( 2, 'ON', 'GL_POINTS', len( data[ 'indices' ] ), 0 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        

# ----------------------------------------------------------------------        
class PointSprites2( vt.VisualTestCase ):
    
    """ This test is for testing point sprite texturing with
    OpenGLES1.TextEnv. The test draws a polygon with two checker textures which
    are overlapping each other. """
  
    testValues = { 'Colors' : [ [ 0.5, 0.5, 0.0, 0.5 ],
                                [ 0.0, 0.5, 0.5, 0.5 ],
                                [ 0.5, 0.0, 0.5, 0.5 ],
                                ],
                   'Mode' : [ 'GL_MODULATE', 'GL_BLEND', 'GL_ADD' ]
                   }
    
    def __init__(self, Colors, Mode):
        vt.VisualTestCase.__init__( self )
        self.Colors   = Colors
        self.Mode     = Mode
        self.textures = [ { 'colors' : ( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] ),
                            'cx'     : 4,
                            'cy'     : 4,
                            'width'  : 32,
                            'height' : 32,
                            },
                          { 'colors' :( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] ),
                            'cx'     : 2,
                            'cy'     : 2,
                            'width'  : 16,
                            'height' : 16,
                            } ]
        
        self.name = 'OPENGLES1 rasterization pointsprites2 (GL_POINTS) colors %s mode %s' % ( str( Colors ), Mode, )
        self.__doc__ += " Point size: 32"
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )        
        
        vertices = [ -0.32, -0.32, 0.0,
                     -0.32,  0.32, 0.0,
                      0.32, -0.32, 0.0,
                      0.32, -0.32, 0.0,
                      0.32,  0.32, 0.0,
                     -0.32,  0.32, 0.0,
                      ]

        texCoords = [ 0.0, 0.0,
                      0.0, 1.0,
                      1.0, 0.0,
                      1.0, 0.0,
                      1.0, 1.0,
                      0.0, 1.0,
                      ]
                        
        ps_coords = [  0.16,  0.16, 0.0,
                       0.16, -0.16, 0.0,
                      -0.16,  0.16, 0.0,
                      -0.16,  0.16, 0.0,
                       ]

        indices = [ 0, 1, 2, 3, 4, 5 ]
                                
        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -1 ] )
        
        OpenGLES1.ShadeModel( 'GL_FLAT' )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', 'GL_NICEST' )
        
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )          # texture
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.CreateArray( 3, 'GL_FLOAT' )          # vertices

        OpenGLES1.AppendToArray( 0, vertices )
        OpenGLES1.AppendToArray( 1, texCoords )
        OpenGLES1.AppendToArray( 2, indices )
        OpenGLES1.AppendToArray( 3, ps_coords )
                
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )       
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )

        OpenGLES1.GenTexture( 0 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.CreateCheckerTextureData( 0,
                                            self.textures[ 0 ][ 'colors' ][ 0 ],
                                            self.textures[ 0 ][ 'colors' ][ 1 ],
                                            self.textures[ 0 ][ 'cx' ],
                                            self.textures[ 0 ][ 'cy' ],
                                            self.textures[ 0 ][ 'width' ],
                                            self.textures[ 0 ][ 'height' ],
                                            'ON',
                                            'OPENGLES1_RGB888' )
        OpenGLES1.TexImage2D( 0, 'GL_TEXTURE_2D' )
        
        OpenGLES1.GenTexture( 1 )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 1 )
        OpenGLES1.CreateCheckerTextureData( 1,
                                            self.textures[ 1 ][ 'colors' ][ 0 ],
                                            self.textures[ 1 ][ 'colors' ][ 1 ],
                                            self.textures[ 1 ][ 'cx' ],
                                            self.textures[ 1 ][ 'cy' ],
                                            self.textures[ 1 ][ 'width' ],
                                            self.textures[ 1 ][ 'height' ],
                                            'ON',
                                            'OPENGLES1_RGB888' )
        OpenGLES1.TexImage2D( 1, 'GL_TEXTURE_2D' )
        
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.TexCoordPointer( 1, 2, 0 )
                
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

        #Draw a textured quad
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 0 )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'ON' )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( indices ), 0, 2 )
        
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', 1 )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE', 'ON' )
        OpenGLES1.Enable( 'GL_POINT_SPRITE_OES' )
        OpenGLES1.TexEnv( 'GL_REPLACE', self.Colors, 'ON' )
        OpenGLES1.VertexPointer( 3, 3, 0 )
        OpenGLES1.PointSize( 16 )
        OpenGLES1.DrawArrays( 'GL_POINTS', 0, 4 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------        
class PointSpritesGradient( vt.VisualTestCase ):
    
    """ This test is for testing point sprite texturing with
    OpenGLES1.TextEnv. The test draws points with two overlapping gradient textures."""
      
    testValues = { 'Color1'       : [ [ 1.0, 0.0, 0.0, 1.0 ], ],
                   'Color2'       : [ [ 0.0, 0.0, 1.0, 1.0 ], ],
                   'Mode'         : [ 'GL_MODULATE', 'GL_BLEND' ],
                   'GradientType' : [ 'OPENGLES1_GRADIENT_VERTICAL', 'OPENGLES1_GRADIENT_HORIZONTAL' ],
                   }

    def __init__( self, Color1, Color2, Mode, GradientType ):
        vt.VisualTestCase.__init__( self )
        self.Color1       = Color1
        self.Color2       = Color2
        self.Mode         = Mode
        self.GradientType = GradientType
        self.name = 'OPENGLES1 rasterization pointsprites gradient (GL_POINTS) colors %s %s mode %s gradient %s' % ( str( Color1 ),
                                                                                                                     str( Color2 ),
                                                                                                                     Mode,
                                                                                                                     GradientType, )
        self.__doc__ += " Point size: 32"
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )       
        
        vertices = [ -0.32, -0.32, 0.0,
                     -0.32,  0.32, 0.0,
                      0.32, -0.32, 0.0,
                      0.32, -0.32, 0.0,
                      0.32,  0.32, 0.0,
                     -0.32,  0.32, 0.0, ]

        texCoords = [ 0.0, 0.0,
                      0.0, 1.0,
                      1.0, 0.0,
                      1.0, 0.0,
                      1.0, 1.0,
                      0.0, 1.0 ]
                        
        indices = [ 0, 1, 2, 3, 4, 5 ]
                                
        OpenGLES1.ClearColor( [ 0, 0, 0, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -1 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )          # texture
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' ) # indices

        OpenGLES1.Enable( 'GL_POINT_SPRITE_OES' )
   
        OpenGLES1.AppendToArray( 0, vertices )
        OpenGLES1.AppendToArray( 1, texCoords )
        OpenGLES1.AppendToArray( 2, indices )
        
        # might change the pointer.
        OpenGLES1.GenBuffer( 0 )
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 0, 0, 0 )
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0  )
        
        OpenGLES1.GenBuffer( 1 )
        OpenGLES1.BufferData( 1, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 1, 0, 0 )
        
        OpenGLES1.GenBuffer( 2 )
        OpenGLES1.BufferData( 2, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 2, 0, 0 )

        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        
        for index in range( 2 ):
            OpenGLES1.ActiveTexture( index )
            OpenGLES1.ClientActiveTexture( index )
            
            OpenGLES1.TexCoordBuffer( 1, 'ON', 2, 0 )
            OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
            OpenGLES1.Enable( 'GL_TEXTURE_2D' )

            OpenGLES1.GenTexture( index )
            OpenGLES1.BindTexture( 'GL_TEXTURE_2D', index  )

            if index == 0:
                OpenGLES1.CreateGradientTextureData( index,
                                                     self.GradientType,
                                                     self.Color1,
                                                     self.Color2,
                                                     64,
                                                     64,
                                                     'ON',
                                                     'OPENGLES1_RGBA8888' )
            else:
                OpenGLES1.CreateGradientTextureData( index,
                                                     self.GradientType,
                                                     self.Color2,
                                                     self.Color1,
                                                     64,
                                                     64,
                                                     'ON',
                                                     'OPENGLES1_RGBA8888' )
                
            OpenGLES1.TexImage2D( index, 'GL_TEXTURE_2D' )

            if index == 0:
                OpenGLES1.TexEnv( 'GL_REPLACE', self.Color1, 'ON' )
            else:
                OpenGLES1.TexEnv( self.Mode, self.Color2, 'ON' )                
                
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        OpenGLES1.PointSize( 32 )
        OpenGLES1.BufferDrawElements( 2, 'ON', 'GL_POINTS', len( indices ), 0 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
