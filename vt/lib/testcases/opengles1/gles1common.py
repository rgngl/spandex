#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

######################################################################
from lib.common import *
import math

######################################################################
def stencilFuncToStr( f ):
    return f[ 3 : ]

######################################################################
def depthFuncToStr( f ):
    return f[ 3 : ]

######################################################################
def colorArrayToStr( ca ):
    s = "["
    for i in range( len( ca ) ):
        s += ( "%0.2f" % ca[ i ] )
        if i < ( len( ca ) - 1 ):
            s += ','

    s += "]"
    return s

######################################################################
def typeToStr( t ):
    return t[ 3 : ]

######################################################################
def wrapToStr( wrap ):
    return wrap[ 3: ] 

######################################################################
def filterToStr( f ):
    return f[ 3 : ]

######################################################################
def paletteTypeToStr( t ):
    return t[ 3 : -4 ]

######################################################################
def defaultOrientation( w, h ):
    if w > h:
        return 'SCT_SCREEN_ORIENTATION_LANDSCAPE'
    else:
        return 'SCT_SCREEN_ORIENTATION_PORTRAIT'

######################################################################
def orientationToStr( o ):
    return o[ 23 : ]

######################################################################
def bufferTargetToStr( b ):
    return b[ 3 : -7 ]

######################################################################
def bufferUsageToStr( b ):
    return b[ 3 : -5 ]

######################################################################
def arrayToStr( t ):
    return str( t ).replace( ' ', '' )

######################################################################
def bufferToStr( f ):
    return f[ 4 : ]

######################################################################
def bufferFormatToStr( f ):
    return f[ 17 : ]

######################################################################
def pixmapFormatToStr( f ):
    return f[ 17 : ]

######################################################################
def swapBehaviorToStr( s ):
    return s[ 11 : ]

######################################################################
def toPot( v, pot ):
    if pot:
        nv = 2 ** math.ceil( math.log( v, 2 ) )
        return ( int( nv ), v / nv, )
    else:
        return ( int( v ), 1.0, )

######################################################################
def toLowerPot( v, pot ):
    if pot:
        nv = 2 ** math.floor( math.log( v, 2 ) )
        return ( int( nv ), v / nv, )
    else:
        return ( int( v ), 1.0, )
    
######################################################################        
def mapAttributesToColorFormat( a ):
    bits  = a[ CONFIG_REDSIZE ] + a[ CONFIG_GREENSIZE ] + a[ CONFIG_BLUESIZE ] + a[ CONFIG_ALPHASIZE ]
    abits = a[ CONFIG_ALPHASIZE ]
    
    if bits == 16:
        if abits == 0:
            return 'SCT_COLOR_FORMAT_RGB565'
        else:
            raise 'Invalid attributes'
    elif bits == 24:
        return 'SCT_COLOR_FORMAT_RGB888'
    elif bits == 32:
        return 'SCT_COLOR_FORMAT_RGBA8888'
    else:
        raise 'Invalid attributes'

######################################################################        
def mapColorFormatToAttributes( f ):
    formats = { 'SCT_COLOR_FORMAT_RGBA5551' : [ 16, 5, 5, 5, 1 ],
                'SCT_COLOR_FORMAT_RGBA4444' : [ 16, 4, 4, 4, 4 ],
                'SCT_COLOR_FORMAT_RGB565'   : [ 16, 5, 6, 5, 0 ],
                'SCT_COLOR_FORMAT_RGB888'   : [ 24, 8, 8, 8, 0 ],
                'SCT_COLOR_FORMAT_RGBA8888' : [ 32, 8, 8, 8, 8 ] }

    format = formats[ f ]
    
    a = {}
    a[ CONFIG_BUFFERSIZE ] = format[ 0 ]
    a[ CONFIG_REDSIZE ]    = format[ 1 ]
    a[ CONFIG_GREENSIZE ]  = format[ 2 ]
    a[ CONFIG_BLUESIZE ]   = format[ 3 ]
    a[ CONFIG_ALPHASIZE ]  = format[ 4 ]
    
    return a

######################################################################
def arrayToStr( a ):
    return str( a ).replace( ' ', '' )

######################################################################
def bufferTargetToStr( b ):
    return b[ 3 : -7 ]

######################################################################
def bufferUsageToStr( b ):
    return b[ 3 : -5 ]

######################################################################
def blendToStr( b ):
    return b[ 3: ]
    
######################################################################
def boolToStr( b ):
    if b:
        return 'Yes'
    else:
        return 'No'

######################################################################
def maskToStr( mask ):
    return '+'.join( mask ).replace( 'GL_', '' ).replace( '_BUFFER_BIT', '' )

######################################################################
def filteringToStr( filtering ):
    return filtering[ 3: ] 

######################################################################
def textureTypeToStr( type ):
    if not type:
        return 'none'
    else:
        return type[ 10 : ]

######################################################################
class VertexDataSetup:
    def __init__( self,
                  vertexArrayIndex,
                  vertexSize,
                  colorArrayIndex,
                  colorSize,
                  normalArrayIndex,
                  texCoordArrayIndices,
                  texCoordSizes,
                  indexArrayIndex,
                  indexArrayLength,
                  glMode ):
        self.vertexArrayIndex        = vertexArrayIndex
        self.vertexSize              = vertexSize
        self.colorArrayIndex         = colorArrayIndex
        self.colorSize               = colorSize
        self.normalArrayIndex        = normalArrayIndex
        self.texCoordArrayIndices    = texCoordArrayIndices
        self.texCoordSizes           = texCoordSizes[ : ]
        self.indexArrayIndex         = indexArrayIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode
        
######################################################################
def setupVertexData( modules, indexTracker, mesh ):
    OpenGLES1 = modules[ 'OpenGLES1' ]

    if not mesh.vertices:
        raise 'Undefined vertices'

    if not mesh.indices:
        raise 'Undefined indices'

    vertexArrayIndex        = -1
    colorArrayIndex         = -1
    normalArrayIndex        = -1
    texCoordArrayIndices    = []
    indexArrayIndex         = -1

    vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
    OpenGLES1.CreateArray( vertexArrayIndex,
                           mesh.vertexGLType )
    OpenGLES1.AppendToArray( vertexArrayIndex, mesh.vertices )

    if mesh.colors:
        colorArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( colorArrayIndex,
                               mesh.colorGLType )
        OpenGLES1.AppendToArray( colorArrayIndex, mesh.colors )

    if mesh.normals:
        normalArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
        OpenGLES1.CreateArray( normalArrayIndex,
                               mesh.normalGLType )
        OpenGLES1.AppendToArray( normalArrayIndex, mesh.normals )
   
    if mesh.texCoords:
        for i in range( len( mesh.texCoords ) ):
            if mesh.texCoords:
                texCoordArrayIndices.append( indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' ) )
                OpenGLES1.CreateArray( texCoordArrayIndices[ i ],
                                       mesh.texCoordsGLTypes[ i ] )
                OpenGLES1.AppendToArray( texCoordArrayIndices[ i ], mesh.texCoords[ i ] )
            else:
                texCoordArrayIndices.append( -1 )
            
    indexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
    OpenGLES1.CreateArray( indexArrayIndex, mesh.indexGLType );
    OpenGLES1.AppendToArray( indexArrayIndex, mesh.indices )

    return VertexDataSetup( vertexArrayIndex,
                            mesh.vertexComponents,
                            colorArrayIndex,
                            mesh.colorComponents,
                            normalArrayIndex,
                            texCoordArrayIndices,
                            mesh.texCoordsComponents,
                            indexArrayIndex,
                            len( mesh.indices ),
                            mesh.glMode )       

######################################################################
class VertexBufferDataSetup:
    def __init__( self,
                  vertexBufferIndex,
                  vertexSize,
                  colorBufferIndex,
                  colorSize,
                  normalBufferIndex,
                  texCoordBufferIndices,
                  texCoordSizes,
                  indexBufferIndex,
                  indexArrayLength,
                  glMode ):
        self.vertexBufferIndex       = vertexBufferIndex
        self.vertexSize              = vertexSize
        self.colorBufferIndex        = colorBufferIndex
        self.colorSize               = colorSize
        self.normalBufferIndex       = normalBufferIndex
        self.texCoordBufferIndices   = texCoordBufferIndices
        self.texCoordSizes           = texCoordSizes[ : ]
        self.indexBufferIndex        = indexBufferIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode

######################################################################
def setupVertexBufferData( modules, indexTracker, vertexDataSetup ):
    OpenGLES1 = modules[ 'OpenGLES1' ]

    vertexBufferIndex       = -1
    colorBufferIndex        = -1
    normalBufferIndex       = -1
    texCoordBufferIndices   = []
    indexArrayIndex         = -1

    vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
    OpenGLES1.GenBuffer( vertexBufferIndex )
    OpenGLES1.BufferData( vertexBufferIndex,
                          'ON',
                          'GL_ARRAY_BUFFER',
                          'GL_STATIC_DRAW',
                          vertexDataSetup.vertexArrayIndex,
                          0,
                          0 )
    
    if vertexDataSetup.colorArrayIndex >= 0:
        colorBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
        OpenGLES1.GenBuffer( colorBufferIndex )
        OpenGLES1.BufferData( colorBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              vertexDataSetup.colorArrayIndex,
                              0,
                              0 )
        
    if vertexDataSetup.normalArrayIndex >= 0:
        normalBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
        OpenGLES1.GenBuffer( normalBufferIndex )
        OpenGLES1.BufferData( normalBufferIndex,
                              'ON',
                              'GL_ARRAY_BUFFER',
                              'GL_STATIC_DRAW',
                              vertexDataSetup.normalArrayIndex,
                              0,
                              0 )

    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices[ i ] >= 0:
                texCoordBufferIndices.append( indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' ) )
                OpenGLES1.GenBuffer( texCoordBufferIndices[ i ] )
                OpenGLES1.BufferData( texCoordBufferIndices[ i ],
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      vertexDataSetup.texCoordArrayIndices[ i ],
                                      0,
                                      0 )
            else:
                texCoordBufferIndices.append( -1 )

    indexBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
    OpenGLES1.GenBuffer( indexBufferIndex )
    OpenGLES1.BufferData( indexBufferIndex,
                          'ON',
                          'GL_ELEMENT_ARRAY_BUFFER',
                          'GL_STATIC_DRAW',
                          vertexDataSetup.indexArrayIndex,
                          0,
                          0 )

    return VertexBufferDataSetup( vertexBufferIndex,
                                  vertexDataSetup.vertexSize,
                                  colorBufferIndex,
                                  vertexDataSetup.colorSize,                                  
                                  normalBufferIndex,
                                  texCoordBufferIndices,
                                  vertexDataSetup.texCoordSizes,
                                  indexBufferIndex,
                                  vertexDataSetup.indexArrayLength,
                                  vertexDataSetup.glMode )

######################################################################
class MeshDataSetup:
    def __init__( self,
                  meshIndex,
                  vertexArrayIndex,
                  vertexSize,
                  colorArrayIndex,
                  colorSize,
                  normalArrayIndex,
                  texCoordArrayIndices,
                  texCoordSizes,
                  indexArrayIndex,
                  indexArrayLength,
                  glMode ):
        self.meshIndex               = meshIndex
        self.vertexArrayIndex        = vertexArrayIndex
        self.vertexSize              = vertexSize
        self.colorArrayIndex         = colorArrayIndex
        self.colorSize               = colorSize
        self.normalArrayIndex        = normalArrayIndex
        self.texCoordArrayIndices    = texCoordArrayIndices
        self.texCoordSizes           = texCoordSizes[ : ]
        self.indexArrayIndex         = indexArrayIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode

######################################################################
def setupMeshData( modules, indexTracker, vertexDataSetup ):
    OpenGLES1 = modules[ 'OpenGLES1' ]

    vertexArrayIndex         = -1
    colorArrayIndex          = -1
    normalArrayIndex         = -1
    texCoordArrayIndices     = []
    nextIndex                = 0
    meshIndices              = []
    sizes                    = []
    
    vertexArrayIndex = nextIndex
    nextIndex += 1
    meshIndices.append( vertexArrayIndex )
    sizes += [ vertexDataSetup.vertexSize ]
    
    if vertexDataSetup.colorArrayIndex >= 0:
        colorArrayIndex = nextIndex
        nextIndex += 1
        meshIndices.append( colorArrayIndex )
        sizes += [ vertexDataSetup.colorSize ]
        
    if vertexDataSetup.normalArrayIndex >= 0:
        normalArrayIndex = nextIndex
        nextIndex += 1
        meshIndices.append( normalArrayIndex )
        sizes += [ 3 ]
        
    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices >= 0:
                texCoordArrayIndices.append( nextIndex )
                nextIndex += 1
                meshIndices.append( texCoordArrayIndices[ i ] )
                sizes += [ vertexDataSetup.texCoordSizes[ i ] ]
            else:
                texCoordArrayIndices.append( -1 )
                meshIndices.append( -1 )
                sizes += [ 0 ]
            
    meshIndex = indexTracker.allocIndex( 'OPENGLES1_MESH_INDEX' )
    OpenGLES1.CreateMesh( meshIndex, meshIndices, sizes )

    return MeshDataSetup( meshIndex,
                          vertexArrayIndex,
                          vertexDataSetup.vertexSize,
                          colorArrayIndex,
                          vertexDataSetup.colorSize,
                          normalArrayIndex,
                          texCoordArrayIndices,
                          vertexDataSetup.texCoordSizes,
                          vertexDataSetup.indexArrayIndex,
                          vertexDataSetup.indexArrayLength,
                          vertexDataSetup.glMode )

######################################################################
class MeshBufferDataSetup:
    def __init__( self,
                  meshBufferIndex,
                  vertexArrayIndex,
                  colorArrayIndex,
                  normalArrayIndex,
                  texCoordArrayIndices,
                  indexBufferIndex,
                  indexArrayLength,
                  glMode ):
        self.meshBufferIndex         = meshBufferIndex
        self.vertexArrayIndex        = vertexArrayIndex
        self.colorArrayIndex         = colorArrayIndex
        self.normalArrayIndex        = normalArrayIndex
        self.texCoordArrayIndices    = texCoordArrayIndices
        self.indexBufferIndex        = indexBufferIndex
        self.indexArrayLength        = indexArrayLength
        self.glMode                  = glMode

######################################################################
def setupMeshBufferData( modules, indexTracker, meshDataSetup ):
    OpenGLES1 = modules[ 'OpenGLES1' ]

    meshBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
    OpenGLES1.GenBuffer( meshBufferIndex )
    OpenGLES1.MeshBufferData( meshBufferIndex,
                              'ON',
                              meshDataSetup.meshIndex,
                              'GL_STATIC_DRAW' )

    indexBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
    OpenGLES1.GenBuffer( indexBufferIndex )
    OpenGLES1.BufferData( indexBufferIndex,
                          'ON',
                          'GL_ELEMENT_ARRAY_BUFFER',
                          'GL_STATIC_DRAW',
                          meshDataSetup.indexArrayIndex,
                          0,
                          0 )

    return MeshBufferDataSetup( meshBufferIndex,
                                meshDataSetup.vertexArrayIndex,
                                meshDataSetup.colorArrayIndex,
                                meshDataSetup.normalArrayIndex,
                                meshDataSetup.texCoordArrayIndices[ : ],
                                indexBufferIndex,
                                meshDataSetup.indexArrayLength,
                                meshDataSetup.glMode )

######################################################################
def useVertexData( modules, indexTracker, vertexDataSetup ):
    OpenGLES1 = modules[ 'OpenGLES1' ]

    OpenGLES1.VertexPointer( vertexDataSetup.vertexArrayIndex,
                             vertexDataSetup.vertexSize,
                             0 )
    OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )

    if vertexDataSetup.colorArrayIndex >= 0:
        OpenGLES1.ColorPointer( vertexDataSetup.colorArrayIndex,
                                vertexDataSetup.colorSize,
                                0 )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )

    if vertexDataSetup.normalArrayIndex >= 0:
        OpenGLES1.NormalPointer(vertexDataSetup.normalArrayIndex,
                                0 )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )

    if vertexDataSetup.texCoordArrayIndices:
        for i in range( len( vertexDataSetup.texCoordArrayIndices ) ):
            if vertexDataSetup.texCoordArrayIndices >= 0:
                OpenGLES1.ClientActiveTexture( i )
                OpenGLES1.TexCoordPointer( vertexDataSetup.texCoordArrayIndices[ i ],
                                           vertexDataSetup.texCoordSizes[ i ],
                                           0 )
                OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )

######################################################################
def useVertexBufferData( modules, indexTracker, vertexBufferDataSetup ):
    OpenGLES1 = modules[ 'OpenGLES1' ]

    OpenGLES1.VertexBuffer( vertexBufferDataSetup.vertexBufferIndex,
                            'ON',
                            vertexBufferDataSetup.vertexSize,
                            0 )
    OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )

    if vertexBufferDataSetup.colorBufferIndex >= 0:
        OpenGLES1.ColorBuffer( vertexBufferDataSetup.colorBufferIndex,
                               'ON',
                               vertexBufferDataSetup.colorSize,
                               0 )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )

    if vertexBufferDataSetup.normalBufferIndex >= 0:
        OpenGLES1.NormalBuffer( vertexBufferDataSetup.normalBufferIndex,
                                'ON',
                                0 )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )

    if vertexBufferDataSetup.texCoordBufferIndices:
        for i in range( len( vertexBufferDataSetup.texCoordBufferIndices ) ):
            if vertexBufferDataSetup.texCoordBufferIndices[ i ] >= 0:
                OpenGLES1.ClientActiveTexture( i )               
                OpenGLES1.TexCoordBuffer( vertexBufferDataSetup.texCoordBufferIndices[ i ],
                                          'ON',
                                          vertexBufferDataSetup.texCoordSizes[ i ],
                                          0 )
                OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )

######################################################################
def useMeshData( modules, indexTracker, meshDataSetup ):
    OpenGLES1 = modules[ 'OpenGLES1' ]

    OpenGLES1.MeshVertexPointer( meshDataSetup.meshIndex,
                                 meshDataSetup.vertexArrayIndex )
    OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )

    if meshDataSetup.colorArrayIndex >= 0:
        OpenGLES1.MeshColorPointer( meshDataSetup.meshIndex,
                                    meshDataSetup.colorArrayIndex )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )

    if meshDataSetup.normalArrayIndex >= 0:
        OpenGLES1.MeshNormalPointer( meshDataSetup.meshIndex,
                                     meshDataSetup.normalArrayIndex )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )

    if meshDataSetup.texCoordArrayIndices:
        for i in range( len( meshDataSetup.texCoordArrayIndices ) ):
            if meshDataSetup.texCoordArrayIndices[ i ] >= 0:
                OpenGLES1.ClientActiveTexture( i )
                OpenGLES1.MeshTexCoordPointer( meshDataSetup.meshIndex,
                                               meshDataSetup.texCoordArrayIndices[ i ] )
                OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )

######################################################################
def useMeshBufferData( modules, indexTracker, meshBufferDataSetup ):
    OpenGLES1 = modules[ 'OpenGLES1' ]
   
    OpenGLES1.BufferMeshVertexPointer( meshBufferDataSetup.meshBufferIndex,
                                       'ON',
                                       meshBufferDataSetup.vertexArrayIndex )
    OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )    

    if meshBufferDataSetup.colorArrayIndex >= 0:
        OpenGLES1.BufferMeshColorPointer( meshBufferDataSetup.meshBufferIndex,
                                          'ON',
                                          meshBufferDataSetup.colorArrayIndex )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )        

    if meshBufferDataSetup.normalArrayIndex >= 0:
        OpenGLES1.BufferMeshNormalPointer( meshBufferDataSetup.meshBufferIndex,
                                           'ON',
                                           meshBufferDataSetup.normalArrayIndex )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )        

    if meshBufferDataSetup.texCoordArrayIndices:
        for i in range( len( meshBufferDataSetup.texCoordArrayIndices ) ):
            if meshBufferDataSetup.texCoordArrayIndices[ i ] >= 0:
                OpenGLES1.ClientActiveTexture( i )
                OpenGLES1.BufferMeshTexCoordPointer( meshBufferDataSetup.meshBufferIndex,
                                                     'ON',
                                                     meshBufferDataSetup.texCoordArrayIndices[ i ] )
                OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )

######################################################################
def drawVertexData( modules, vertexDataSetup ):
    OpenGLES1 = modules[ 'OpenGLES1' ]

    OpenGLES1.DrawElements( vertexDataSetup.glMode,
                            vertexDataSetup.indexArrayLength,
                            0,
                            vertexDataSetup.indexArrayIndex )
    
######################################################################
def drawVertexBufferData( modules, vertexBufferDataSetup ):
    OpenGLES1 = modules[ 'OpenGLES1' ]

    OpenGLES1.BufferDrawElements( vertexBufferDataSetup.indexBufferIndex,
                                  'ON',
                                  vertexBufferDataSetup.glMode,
                                  vertexBufferDataSetup.indexArrayLength,
                                  0 )    

######################################################################
def drawMeshBufferData( modules, meshBufferDataSetup ):
    OpenGLES1 = modules[ 'OpenGLES1' ]

    OpenGLES1.BufferDrawElements( meshBufferDataSetup.indexBufferIndex,
                                  'ON',
                                  meshBufferDataSetup.glMode,
                                  meshBufferDataSetup.indexArrayLength,
                                  0 )    

######################################################################
class TextureSetup:
    def __init__( self,
                  textureDataIndex,
                  textureIndex ):
        self.textureDataIndex = textureDataIndex
        self.textureIndex     = textureIndex

######################################################################
def useTexture( modules, textureSetup, textureUnit, texEnv ):
    OpenGLES1 = modules[ 'OpenGLES1' ]

    OpenGLES1.ActiveTexture( textureUnit )
    OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureSetup.textureIndex )
    OpenGLES1.TexEnvMode( texEnv )
    OpenGLES1.Enable( 'GL_TEXTURE_2D' )
