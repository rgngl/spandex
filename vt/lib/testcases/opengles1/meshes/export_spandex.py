#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

"""
This script exports Mesh objects from Blender to format used by Spandex OpenGL ES test cases
Made with Blender version 2.44, older versions will probably work too.

Usage:
	1. In this script, change the path after the imports to a location you want
	2. In object mode, select all the meshes you want to export,
	3. Run this script by pressing alt+p on this window
	4. Check the console for possible messages about the exporting process
	
Rules for exporting:
- Meshes must be triangulated
	-> Select all vertices in edit mode and press control+t
	(or do it manually if you like that sort of thing)
- Meshes must have vertex colors
	-> Press v, then F9, then set the color in Paint window to white, press "Set VertCol"
- Meshes must have UV coordinates set with the UV editor (procedural textures are not supported)
	-> Press f twice in object mode to create default UV coordinates

Known features (faults):
- All materials are exported, but indices are not grouped by material
- Meshes are not optimized for rendering speed in any way
- Exporting big meshes (thousands of polys) takes alot of time and space

PAQ (Probably Asked Questions):

1. Why the need for texture coordinates and vertex colors?
	-> This is just to ensure that each exported mesh can be used in any test case
2. More vertices seem to be exported than there are in the model?
	-> Blender can use more than one texture coordinate for a single vertex object,
	so some vertices must be exported more than once so that texture coordinate (and color)
	arrays can be used properly.

"""

# Set this to the path you want meshes to be exported
export_path = "c:\\USERS\\thaapoja\\"

import Blender
from Blender import NMesh
import os
import sys
import string
import time

class ExportMesh:
	def __init__(self, object):
		self.object = object
		self.data = {
			'vertices':[],
			'indices':[],
			'normals':[],
			'colorsf':[],
			'colorsb':[],
			'materials':[],
			'texcoords':[],
			}
		self.__prepare()
		
	def __prepare(self):
		# Read and prepare mesh data for writing
		self.mesh = self.object.getData(mesh=True) 
		vertices = []
		normals = []
		faces = []
		texcoords = []
		colorsf = []
		colorsb = []
		verts_temp = []
		
		# Assert some mesh attributes, raise an error to interrupt script execution
		if not self.mesh.faceUV:
			Blender.Draw.PupMenu('Object does not have UV coordinates')
			raise RuntimeError()
		if not self.mesh.vertexColors:
			Blender.Draw.PupMenu('Object does not have vertex colors')
			raise RuntimeError()
		
		for f in self.mesh.faces:
			indices = []
			if len(f.verts) > 3:
				Blender.Draw.PupMenu('Object must be triangulated first')
				raise RuntimeError()
				
			for i in xrange(len(f.verts)):
				key = [ f.verts[i], f.uv[i], f.col[i] ]
				if key not in verts_temp:
					verts_temp.append( key )
				indices.append( verts_temp.index(key) )
			faces.append( indices )
			
		# Extract data from the temporary list
		for k in verts_temp:
			vertices.append( k[0] )
			normals.append( k[0].no )
			texcoords.append( k[1] )
			colorsf.append( [k[2].r / 255.0, k[2].g / 255.0, k[2].b / 255.0, k[2].a / 255.0] )
			colorsb.append( [k[2].r, k[2].g, k[2].b, k[2].a] )
		
		# Save the mesh data to this class instance
		[self.data['vertices'].append([v.co[0],v.co[1],v.co[2]]) for v in vertices]
		[self.data['normals'].append([n.x, n.y, n.z]) for n in normals]
		
		[self.data['colorsf'].append(c) for c in colorsf]
		[self.data['colorsb'].append(c) for c in colorsb]
		[self.data['indices'].append( [i[0],i[1],i[2]]) for i in faces]
		[self.data['texcoords'].append([tc for tc in c]) for c in texcoords]
		
	def writeFile(self, filename, flatmode=False):
		f = open(filename,'w')
		self.write_material(f)
		self.write_vertices(f, flatmode)
		self.write_indices(f, flatmode)
		self.write_normals(f, flatmode)
		self.write_texcoords(f, flatmode)
		self.write_vertcolorsf(f, flatmode)
		self.write_vertcolorsb(f, flatmode)
		f.close()
	
	def write_material(self, f):
		f.write("materials = (\n")
		for mat in self.mesh.materials:
			material = {
				'diffuse':mat.getRGBCol()+[1.0],
				'specular':[col * mat.getSpec() for col in mat.getSpecCol()]+[1.0],
				'emission':[col * mat.emit for col in mat.getRGBCol()]+[1.0],
				'ambient':[col * mat.amb for col in mat.getRGBCol()]+[1.0],
				'shininess':128.0*mat.ref,
			}
			f.write("  {\n")
			for k,v in material.items():
				f.write("    '%s':%s,\n" % (k,v))
			f.write("  },\n")
		f.write(")\n")

	def write_vertices(self, f,flatmode):
		f.write("vertices = [\n")
		for v in self.data["vertices"]:
			if flatmode == False:
				f.write(" [")
			f.write( "%.6f, %.6f, %.6f" % (v[0],v[1],v[2]) )
			if flatmode == False:
				f.write("]")
			f.write(",\n")
		f.write("]\n\n")
	
	def write_indices(self,f, flatmode):
		f.write("indices = [\n")
		for v in self.data["indices"]:
			if flatmode == False:
				f.write(" [")
			f.write( "%d, %d, %d" % (v[0],v[1],v[2]) )
			if flatmode == False:
				f.write("]")
			f.write(",\n")
		f.write("]\n\n")
		
	def write_normals(self, f,flatmode):
		f.write("normals = [\n")
		for v in self.data["normals"]:
			if flatmode == False:
				f.write(" [")
			f.write( "%.6f, %.6f, %.6f" % (v[0],v[1],v[2]) )
			if flatmode == False:
				f.write("]")
			f.write(",\n")
		f.write("]\n\n")
	
	def write_vertcolorsf(self, f,flatmode):
		f.write("colorsf = [\n")
		for v in self.data["colorsf"]:
			if flatmode == False:
				f.write(" [")
			f.write( "%.6f, %.6f, %.6f, %.6f" % (v[0],v[1],v[2],v[3]) )
			if flatmode == False:
				f.write("]")
			f.write(",\n")
		f.write("]\n\n")
	
	def write_vertcolorsb(self, f,flatmode):
		f.write("colorsb = [\n")
		for v in self.data["colorsb"]:
			if flatmode == False:
				f.write(" [")
			f.write( "%d, %d, %d, %d" % (v[0],v[1],v[2],v[3]) )
			if flatmode == False:
				f.write("]")
			f.write(",\n")
		f.write("]\n\n")
	
	def write_texcoords(self, f,flatmode):
		f.write("texcoords = [\n")
		for v in self.data["texcoords"]:
			if flatmode == False:
				f.write(" [")
			f.write( "%.6f, %.6f" % (v[0],v[1]) )
			if flatmode == False:
				f.write("]")
			f.write(",\n")
		f.write("]\n\n")

scene = Blender.Scene.GetCurrent()
objs = [o for o in scene.objects.context if o.type == "Mesh"]
if not objs:
	Blender.Draw.PupMenu('No meshes selected')
else:
	print "-"*20, "Exporting","-"*20
	print time.asctime()
	meshes = {}
	for o in objs:
		filename = "%s.mesh" % (os.path.join(export_path,o.name.lower()))
		
		print "Exporting object '%s' to %s" % (o.name, filename)
		meshes[filename] = ExportMesh(o)
	for filename,mesh in meshes.items():
		mesh.writeFile(filename,True)
	print "-"*20, "All done.","-"*20
