# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles1common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )

# ----------------------------------------------------------------------
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh


# ----------------------------------------------------------------------
def setupTexture( modules, indexTracker, w, h, gradient, c1, c2, textureType, minF, magF ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
    
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_DATA_INDEX' )
        OpenGLES1.CreateGradientTextureData( textureDataIndex,
                                             gradient,
                                             c1,
                                             c2,
                                             w, h,
                                             'OFF',
                                             textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES1_TEXTURE_INDEX' )
        OpenGLES1.GenTexture( textureIndex )
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                minF,
                                magF,
                                'GL_REPEAT',
                                'GL_REPEAT',
                                'OFF' )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        return TextureSetup( textureDataIndex, textureIndex )


# ----------------------------------------------------------------------
class SmoothPixels( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing smooth pixels.

        Expected output: full screen plane with a 4x4 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''

        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats  = 1
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.overdraw = 1
                self.name = "OPENGLES1 smooth pixels"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                OpenGLES1.ShadeModel( 'GL_SMOOTH' )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0, 0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
# ----------------------------------------------------------------------
class FlatTexels( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing flat texels with different
        texture types.

        Expected output: a full screen plane with a 4x4 vertex grid. A gradient
        texture is scaled across each grid rectangle. Lower-left corner of the
        rectangle should be blue (black with luminance textures), upper-right
        corner should be white. Viewport size 320*240.
        '''

        testValues = { ( 'textureType' ) : [ ( 'OPENGLES1_LUMINANCE8' ),
                                             ( 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                             ( 'OPENGLES1_RGB565' ),
                                             ( 'OPENGLES1_RGB888' ),
                                             ( 'OPENGLES1_RGBA4444' ),
                                             ( 'OPENGLES1_RGBA5551' ),
                                             ( 'OPENGLES1_RGBA8888' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 flat texels, texture type=%s" % ( textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                textureSetup      = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  'OPENGLES1_GRADIENT_DIAGONAL',
                                                  [ 0.0, 0.0, 1.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.textureType,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                useTexture( modules, textureSetup, 0, 'GL_REPLACE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0, 0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

                
# ----------------------------------------------------------------------
class SmoothTexels( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing smooth texels with different
        texture types.

        Expected output: a full screen plane with a 4x4 vertex grid. A gradient
        texture is scaled across each grid rectangle, with [ 0.0, 0.0, 1.0, 1.0
        ] color at the bottom-left, [ 1.0, 1.0, 1.0, 1.0 ] at the top-right. The
        vertex colors are defined so that the lower-left vertex has color [0.5,
        0.0, 0.0, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top.  The texture color is modulated with the
        vertex colors. The net result at the screen should be so that the
        bottom-left corner of surface is black, top-right corner is white. The
        top-left corner of the rectangle at the surface bottom-left should be
        red. Viewport size 320*240.
        '''

        testValues = { ( 'textureType' ) : [ ( 'OPENGLES1_LUMINANCE8' ),
                                             ( 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                             ( 'OPENGLES1_RGB565' ),
                                             ( 'OPENGLES1_RGB888' ),
                                             ( 'OPENGLES1_RGBA4444' ),
                                             ( 'OPENGLES1_RGBA5551' ),
                                             ( 'OPENGLES1_RGBA8888' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES1 smooth texels, texture type=%s" % ( textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, self.gridx, self.gridy, True, [ True ] )
                mesh.gradientColors( [ 0.5, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                textureSetup      = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  'OPENGLES1_GRADIENT_DIAGONAL',
                                                  [ 0.0, 0.0, 1.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.textureType,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                useTexture( modules, textureSetup, 0, 'GL_MODULATE' )

                OpenGLES1.ShadeModel( 'GL_SMOOTH' )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0, 0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
# ----------------------------------------------------------------------
class FlatMultitexels( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing flat multitexels (2) with
        different texture types.

        Expected output: a full screen plane with a 4x4 vertex grid. Two
        gradient textures (horizontal and vertical) are scaled across each grid
        rectangle. Lower-left corner of the rectangle should be black,
        upper-right corner should be white. Gradient should run diagonally
        within each rectangle. Viewport size 320*240.
        '''

        testValues = { ( 'texture1Type', 'texture2Type' ) : [ ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_RGBA8888' ),
                                                              ]
                       }
        
        def __init__( self, texture1Type, texture2Type ):
                vt.VisualTestCase.__init__( self )
                self.texture1Type = texture1Type
                self.texture2Type = texture2Type
                self.repeats      = 1
                self.gridx        = GRIDX
                self.gridy        = GRIDY
                self.overdraw     = 1
                self.name = "OPENGLES1 flat multitexels, texture1 type=%s, texture2 type=%s" % ( textureTypeToStr( self.texture1Type ),
                                                                                                 textureTypeToStr( self.texture2Type ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, self.gridx, self.gridy, False, [ True, True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                texture1Setup     = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  'OPENGLES1_GRADIENT_HORIZONTAL',
                                                  [ 0.0, 0.0, 0.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.texture1Type,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                texture2Setup     = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,                                                  
                                                  'OPENGLES1_GRADIENT_VERTICAL',
                                                  [ 0.0, 0.0, 0.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.texture2Type,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                useTexture( modules, texture1Setup, 0, 'GL_REPLACE' )
                useTexture( modules, texture2Setup, 1, 'GL_MODULATE' )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0, 0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
# ----------------------------------------------------------------------
class SmoothMultitexels( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing smooth multitexels (2) with
        different texture types.

        Expected output: a full screen plane with a 4x4 vertex grid. Two
        gradient textures (horizontal and vertical) are scaled across each grid
        rectangle. Lower-left corner of the combined texture should be black,
        upper-right corner should be white. Gradient should run diagonally
        within each rectangle.  The final color is modulated with the vertex
        color. The vertex colors are defined so that the lower-left vertex has
        color [0.5, 0.0, 0.0, 1.0 ] and the upper-right vertex has color [ 1.0,
        1.0, 1.0, 1.0]. Between those the vertex color is linearly interpolated
        from left to right, from bottom to top.  The net result at the screen
        should be so that the bottom-left corner of surface is black, top-right
        corner is white. The top-left corner of the rectangle at the surface
        bottom-left should be red. Viewport size 320*240.
        '''
        
        testValues = { ( 'texture1Type', 'texture2Type' ) : [ ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_LUMINANCE8', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_LUMINANCE_ALPHA88', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_RGB565', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_RGB888', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_RGBA4444', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_RGBA5551', 'OPENGLES1_RGBA8888' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_LUMINANCE8' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_RGB565' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_RGB888' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_RGBA4444' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_RGBA5551' ),
                                                              ( 'OPENGLES1_RGBA8888', 'OPENGLES1_RGBA8888' ),
                                                              ]
                       }
        
        def __init__( self, texture1Type, texture2Type ):
                vt.VisualTestCase.__init__( self )
                self.texture1Type = texture1Type
                self.texture2Type = texture2Type
                self.repeats      = 1
                self.gridx        = GRIDX
                self.gridy        = GRIDY
                self.overdraw     = 1
                self.name = "OPENGLES1 smooth multitexels, texture1 type=%s, texture2 type=%s" % ( textureTypeToStr( self.texture1Type ),
                                                                                                 textureTypeToStr( self.texture2Type ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, self.gridx, self.gridy, True, [ True, True ] )
                mesh.gradientColors( [ 0.5, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                texture1Setup     = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  'OPENGLES1_GRADIENT_HORIZONTAL',
                                                  [ 0.0, 0.0, 0.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.texture1Type,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                texture2Setup     = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,                                                  
                                                  'OPENGLES1_GRADIENT_VERTICAL',
                                                  [ 0.0, 0.0, 0.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.texture2Type,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                useTexture( modules, texture1Setup, 0, 'GL_MODULATE' )
                useTexture( modules, texture2Setup, 1, 'GL_MODULATE' )

                OpenGLES1.ShadeModel( 'GL_SMOOTH' )
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0, 0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

