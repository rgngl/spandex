# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from gles1common import *

WIDTH, HEIGHT = ( 320, 240, )

######################################################################
class Extensions( vt.VisualTestCase ):

        ''' The purpose of the test: test OpenGLES1 extensions.

        Expected output: surface cleared to green if the particular OpenGLES1
        extension is supported. Viewport size 320*240.
        '''
        
        testValues = { ( 'extension', 'functions' ) : [ ( 'GL_OES_byte_coordinates', [] ),
                                                        ( 'GL_OES_fixed_point', [] ),
                                                        ( 'GL_OES_single_precision', [] ),
                                                        ( 'GL_OES_matrix_get', [] ),
                                                        ( 'GL_OES read format', [] ),
                                                        ( 'GL_OES_compressed_paletted_texture', [] ),
                                                        ( 'GL_OES_point_size_array', [ 'glPointSizePointerOES' ] ),
                                                        ( 'GL_OES_point_sprite', [] ),
                                                        ( 'GL_OES_matrix_palette', [ 'glLoadPaletteFromModelViewMatrixOES',
                                                                                     'glMatrixIndexPointerOES',
                                                                                     'glWeightPointerOES' ] ),
                                                        ( 'GL_OES_draw_texture', [ 'glDrawTexfOES',
                                                                                   'glDrawTexxOES',
                                                                                   'glDrawTexiOES',
                                                                                   'glDrawTexsOES',
                                                                                   'glDrawTexfvOES',
                                                                                   'glDrawTexxvOES',
                                                                                   'glDrawTexivOES',
                                                                                   'glDrawTexsvOES' ] ),
                                                        ( 'GL_OES_compressed_ETC1_RGB8_texture', [] ),
                                                        ( 'GL_OES_EGL_image', [ 'glEGLImageTargetTexture2DOES',
                                                                                'glEGLImageTargetRenderbufferStorageOES' ] ),
                                                        ( 'GL_OES_rgb8_rgba8', [] ),
                                                        ( 'GL_OES_texture_npot', [] ),
                                                        ( 'GL_EXT_texture_format_BGRA8888', [] ),
                                       ]
                       }
                                                                             
        def __init__( self, extension, functions ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.extension = extension
                self.functions = functions
                self.name    = "OPENGLES1 extensions, extension=%s" % ( self.extension, )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]               

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]                
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                
                OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()               

                OpenGLES1.CheckExtension( self.extension )
                
                for f in self.functions:
                        OpenGLES1.CheckFunction( f )
                        
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.CheckError( '' )               
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

