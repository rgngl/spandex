#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

WIDTH, HEIGHT = ( 320, 240, )

# ----------------------------------------------------------------------
class BufferObjects( vt.VisualTestCase ):
    
    """ Testing buffer objects functionality. A triangle is drawn twice, first
    with original data, then with another vertex buffer using BindBuffer and a
    modified color buffer using BufferSubData. """
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.name = "OPENGLES1 buffer objects"
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )
        
        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT')           # vertices 1
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )  # colors 
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.CreateArray( 3, 'GL_FLOAT' )          # vertices 2
        OpenGLES1.CreateArray( 4, 'GL_UNSIGNED_BYTE' )  # replacement colors
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        tri = triangle()
        
        # Alter data for the second set of vertices
        verts2 = []
        verts2.extend( tri[ 'vertices' ] )
        verts2[ 0 * 3 ]     -= 0.1
        verts2[ 1 * 3 ]     += 0.1
        verts2[ 2 * 3 + 1 ] -= 0.1
        
        newcolors = [ 255, 255, 255, 255,
                      255, 0, 255, 255 ]
        
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'indices' ] )
        OpenGLES1.AppendToArray( 3, verts2 )
        OpenGLES1.AppendToArray( 4, newcolors )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        
        OpenGLES1.GenBuffer( 0 )
        OpenGLES1.BufferData( 0, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 0, 0, 0 )        
        OpenGLES1.VertexBuffer( 0, 'ON', 3, 0  )
        
        OpenGLES1.GenBuffer( 1 )
        OpenGLES1.BufferData( 1, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 1, 0, 0 )        
        OpenGLES1.ColorBuffer( 1, 'ON', 4, 0 )
        
        OpenGLES1.GenBuffer( 2 )
        OpenGLES1.BufferData( 2, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 2, 0, 0 )
        
        OpenGLES1.GenBuffer( 3 )
        OpenGLES1.BufferData( 3, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 4, 0, 0 )
        
        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.BufferDrawElements( 2, 'ON', 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0 )
        
        OpenGLES1.BufferData( 1, 'ON', 'GL_ARRAY_BUFFER', 'GL_STATIC_DRAW', 3, 0, 0 )
        OpenGLES1.VertexBuffer( 1, 'ON', 3, 0, ) # Change the vertex buffer
                
        OpenGLES1.BufferData( 3, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 'GL_STATIC_DRAW', 1, 0, 0 )
        OpenGLES1.BufferSubData( 3, 'ON', 'GL_ELEMENT_ARRAY_BUFFER', 4, 4, 0, 0 ) # Modify color buffer
                
        OpenGLES1.BufferDrawElements( 2, 'ON', 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0 )
        
        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
