#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

WIDTH, HEIGHT = ( 320, 240, )

# ----------------------------------------------------------------------
class FrontFace( vt.VisualTestCase ):
    
    """ Testing FrontFace. Drawing some lines, points and two objects, of which
    the left one is facing towards and the right one is facing away from the
    camera. Lines and points should always be drawn. """
    
    testValues = { 'frontface' : [ 'GL_CW', 'GL_CCW' ]
                   }
        
    def __init__( self, frontface ):
        vt.VisualTestCase.__init__( self )
        self.frontface = frontface
        self.name = 'OPENGLES1 front face %s' % ( self.frontface, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -3 ] )
        OpenGLES1.FrontFace( self.frontface )
        OpenGLES1.Enable( 'GL_CULL_FACE' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # quad vertices
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )  # quad colors
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' ) # quad indices
        OpenGLES1.CreateArray( 3, 'GL_FLOAT' )          # line vertices
        OpenGLES1.CreateArray( 4, 'GL_UNSIGNED_BYTE' )  # line indices
        OpenGLES1.CreateArray( 5, 'GL_FLOAT' )          # points vertices
        OpenGLES1.CreateArray( 6, 'GL_UNSIGNED_BYTE' )  # points indices
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        linedata = lines()
        OpenGLES1.AppendToArray( 3, linedata[ 'vertices' ] )
        OpenGLES1.AppendToArray( 4, linedata[ 'indices' ] )
        
        pointdata = points( radius = 1.5 )
        OpenGLES1.AppendToArray( 5, pointdata[ 'vertices' ] )
        OpenGLES1.AppendToArray( 6, pointdata[ 'indices' ] )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ -1.0, 0.0, 0.0 ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Rotate( 180, [ 0.0, 1.0, 0.0 ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.DisableClientState( 'GL_COLOR_ARRAY' )
        # draw some lines
        OpenGLES1.VertexPointer( 3, 3, 0 )
        OpenGLES1.DrawElements( 'GL_LINE_LOOP', len( linedata[ 'indices' ] ), 0, 4 )
        # some points
        OpenGLES1.VertexPointer( 5, 3, 0 )
        OpenGLES1.DrawElements( 'GL_POINTS', len( pointdata[ 'indices' ] ), 0, 6 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
        
# ----------------------------------------------------------------------
class Light( vt.VisualTestCase ):
    
    """ Testing glLight."""
    
    testValues = { 'lights'  : [ 1, 2, 3 ],
                   'twoside' : [ 'ON', 'OFF' ]
                   }
    
    def __init__( self, lights, twoside ):
        vt.VisualTestCase.__init__( self )
        self.light_count = lights
        self.twoside     = twoside
        self.lights      = [ { 'Ambient'              : [ 0.0, 0.0, 0.0, 1.0 ],
                               'Diffuse'              : [ 0.8, 0.8, 0.8, 1.0 ],
                               'Specular'             : [ 0.5, 0.5, 0.5, 1.0 ],
                               'Position'             : [ 0.5, 0.0, 0.0, 0.0 ],
                               'SpotDirection'        : [ 0.0, 0.0, -1.0 ],
                               'SpotExponent'         : 0.0,
                               'SpotCutoff'           : 180.0,
                               'ConstantAttenuation'  : 1.0,
                               'LinearAttenuation'    : 1.0,
                               'QuadraticAttenuation' : 1.0,
                               },
                             { 'Ambient'              : [ 0.0, 0.0, 0.0, 1.0 ],
                               'Diffuse'              : [ 0.5, 1.0, 0.0, 1.0 ],
                               'Specular'             : [ 0.5, 0.9, 0.5, 1.0 ],
                               'Position'             : [ 1.0, 0.0, 1.0, 0.0 ],
                               'SpotDirection'        : [ 0.0, 0.0, -1.0 ],
                               'SpotExponent'         : 0.0,
                               'SpotCutoff'           : 180.0,
                               'ConstantAttenuation'  : 1.0,
                               'LinearAttenuation'    : 1.0,
                               'QuadraticAttenuation' : 1.0,
                               },
                             { 'Ambient'              : [ 0.4, 0.4, 0.4, 1.0 ],
                               'Diffuse'              : [ 0.5, 0.5, 1.0, 1.0 ],
                               'Specular'             : [ 1.0, 1.0, 1.0, 1.0 ],
                               'Position'             : [ 1.0, 0.5, -5.0, 0.0 ],
                               'SpotDirection'        : [ 0.0, 0.0, 1.0 ],
                               'SpotExponent'         : 2.0,
                               'SpotCutoff'           : 180.0,
                               'ConstantAttenuation'  : 1.0,
                               'LinearAttenuation'    : 1.0,
                               'QuadraticAttenuation' : 1.0,
                               },
                             ]
        
        self.name = 'OPENGLES1 light %d lights twosided %s' % ( lights, twoside, )
        for x in range( len( self.lights ) ):
            self.__doc__ += " light%d in position %s" % ( x + 1, tuple( self.lights[ x ][ 'Position' ] ), )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 10 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable( 'GL_NORMALIZE' )
        OpenGLES1.Enable( 'GL_LIGHTING' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.LightModelTwoSided( self.twoside )
        
        # Set up lights
        for l in xrange( self.light_count ):
            OpenGLES1.Enable( 'GL_LIGHT%s' % ( l, ) )
            light = self.lights[ l ]
            OpenGLES1.Light( l,
                             light[ 'Ambient' ],
                             light[ 'Diffuse' ],
                             light[ 'Specular' ],
                             light[ 'Position' ],
                             light[ 'SpotDirection' ],
                             light[ 'SpotExponent' ],
                             light[ 'SpotCutoff' ],
                             light[ 'ConstantAttenuation' ],
                             light[ 'LinearAttenuation' ],
                             light[ 'QuadraticAttenuation' ]
                             )
            
        # Create vertex, normal and index arrays
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )          # normals
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )
        OpenGLES1.Color( [ 1.0, 0.0, 0.0, 1.0 ] )
        
        # Create a cube
        cube = loadMesh( "twosided.mesh" )
        OpenGLES1.AppendToArray( 0, cube[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, cube[ 'normals' ] )
        OpenGLES1.AppendToArray( 2, cube[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.NormalPointer( 1,0 )
        
        # Set up material
        OpenGLES1.Material( cube[ 'material' ][ 'ambient' ],
                            cube[ 'material' ][ 'diffuse' ],
                            cube[ 'material' ][ 'specular' ],
                            cube[ 'material' ][ 'emission' ],
                            cube[ 'material' ][ 'shininess' ]
                            )
        OpenGLES1.Translate( [ 0, 0.0, -3.1 ] )
        OpenGLES1.Rotate( 30.0, [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Rotate( 50.0, [ 0.0, 1.0, 0.0 ] )
        OpenGLES1.Scale( [ 6.1, 6.1, 6.1 ] )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( cube[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------
class ShadeModel( vt.VisualTestCase ):
    
    """Testing glShadeModel."""
    
    testValues = { 'model' : [ 'GL_FLAT',
                               'GL_SMOOTH' ]
                   }
    
    def __init__( self, model ):
        vt.VisualTestCase.__init__( self )
        self.model = model
        self.name = "OPENGLES1 shade model %s" % ( self.model, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.ShadeModel( self.model )
        OpenGLES1.Enable( 'GL_LIGHTING' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Enable( 'GL_NORMALIZE' )
        light = { 'Ambient'              : [ 0.0, 0.0, 0.0, 1.0 ],
                  'Diffuse'              : [ 0.3, 0.3, 0.3, 1.0 ],
                  'Specular'             : [ 0.5, 0.5, 0.0, 1.0 ],
                  'Position'             : [ 1.0, 1.0, 1.0, 0.0 ],
                  'SpotDirection'        : [ 0.0, 0.0, -1.0 ],
                  'SpotExponent'         : 0.0,
                  'SpotCutoff'           : 180.0,
                  'ConstantAttenuation'  : 1.0,
                  'LinearAttenuation'    : 0.0,
                  'QuadraticAttenuation' : 0.0,
                  }
	OpenGLES1.Enable( 'GL_LIGHT0' )
        OpenGLES1.Light( 0,
                         light[ 'Ambient' ],
                         light[ 'Diffuse' ],
                         light[ 'Specular' ],
                         light[ 'Position' ],
                         light[ 'SpotDirection' ],
                         light[ 'SpotExponent' ],
                         light[ 'SpotCutoff' ],
                         light[ 'ConstantAttenuation' ],
                         light[ 'LinearAttenuation' ],
                         light[ 'QuadraticAttenuation' ]
                         )
        OpenGLES1.LightModelTwoSided( 'ON' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )
        
        data = loadMesh( "sphere.mesh" )
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'normals' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.NormalPointer( 1,0 )
        # Set up material
        OpenGLES1.Material( data[ 'material' ][ 'ambient' ],
                            data[ 'material' ][ 'diffuse' ],
                            data[ 'material' ][ 'specular' ],
                            data[ 'material' ][ 'emission' ],
                            data[ 'material' ][ 'shininess' ]
                            )
        OpenGLES1.Translate( [ 0, 0.0, -3.1 ] )
        OpenGLES1.Rotate( 20.0, [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Rotate( 20.0, [ 0.0, 1.0, 0.0 ] )
        OpenGLES1.Scale( [ 6.1, 6.1, 6.1 ] )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------
class ColorMaterial( vt.VisualTestCase ):
    
    """ Testing color material."""
    
    testValues = { 'enable' : [ True, False ]
                   }
    
    def __init__( self, enable ):
        vt.VisualTestCase.__init__( self )
        self.light_count = 2
        self.twoside = 'OFF'
        self.enable = enable
        self.color = [ 1.0, 0.0, 0.0, 1.0 ]
        self.lights = [ { 'Ambient'              : [ 0.0, 0.0, 0.0, 1.0 ],
                          'Diffuse'              : [ 0.8, 0.8, 0.8, 1.0 ],
                          'Specular'             : [ 0.5, 0.5, 0.5, 1.0 ],
                          'Position'             : [ 0.5, 0.0, 0.0, 0.0 ],
                          'SpotDirection'        : [ 0.0, 0.0, -1.0 ],
                          'SpotExponent'         : 0.0,
                          'SpotCutoff'           : 180.0,
                          'ConstantAttenuation'  : 1.0,
                          'LinearAttenuation'    : 1.0,
                          'QuadraticAttenuation' : 1.0,
                          },
                        { 'Ambient'              : [ 0.0, 0.0, 0.0, 1.0 ],
                          'Diffuse'              : [ 0.5, 1.0, 0.0, 1.0 ],
                          'Specular'             : [ 0.5, 0.9, 0.5, 1.0 ],
                          'Position'             : [ 1.0, 0.0, 1.0, 0.0 ],
                          'SpotDirection'        : [ 0.0, 0.0, -1.0 ],
                          'SpotExponent'         : 0.0,
                          'SpotCutoff'           : 180.0,
                          'ConstantAttenuation'  : 1.0,
                          'LinearAttenuation'    : 1.0,
                          'QuadraticAttenuation' : 1.0,
                          },
                        ]
            
        self.name = 'OPENGLES1 color material %s' % ( { True : 'enabled', False : 'disabled' }[ enable ] )
        self.__doc__ += " color = %s" % ( self.color, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 10 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable( 'GL_NORMALIZE' )
        OpenGLES1.Enable( 'GL_LIGHTING' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.LightModelTwoSided( self.twoside )
        
        if self.enable:
            OpenGLES1.Enable( 'GL_COLOR_MATERIAL' )
        else:
            OpenGLES1.Disable( 'GL_COLOR_MATERIAL' )
            
        OpenGLES1.Color( self.color )
        
        # Set up lights
        for l in xrange( self.light_count ):
	    OpenGLES1.Enable( 'GL_LIGHT%s' % ( l, ) )
            light = self.lights[ l ]
            OpenGLES1.Light( l,
                             light[ 'Ambient' ],
                             light[ 'Diffuse' ],
                             light[ 'Specular' ],
                             light[ 'Position' ],
                             light[ 'SpotDirection' ],
                             light[ 'SpotExponent' ],
                             light[ 'SpotCutoff' ],
                             light[ 'ConstantAttenuation' ],
                             light[ 'LinearAttenuation' ],
                             light[ 'QuadraticAttenuation' ]
                             )
            
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices
        OpenGLES1.CreateArray( 1, 'GL_FLOAT' )          # normals
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )
        OpenGLES1.Color( [ 1.0, 0.0, 0.0, 1.0 ] )
        
        cube = loadMesh( "twosided.mesh" )
        OpenGLES1.AppendToArray( 0, cube[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, cube[ 'normals' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.NormalPointer( 1, 0 )
        OpenGLES1.AppendToArray( 2, cube[ 'indices' ] )
        
        # Set up material
        OpenGLES1.Material( cube[ 'material' ][ 'ambient' ],
                            cube[ 'material' ][ 'diffuse' ],
                            cube[ 'material' ][ 'specular' ],
                            cube[ 'material' ][ 'emission' ],
                            cube[ 'material' ][ 'shininess' ]
                            )
        OpenGLES1.Translate( [ 0, 0.0, -3.1 ] )
        OpenGLES1.Rotate( 30.0, [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Rotate( 50.0, [ 0.0, 1.0, 0.0 ] )
        OpenGLES1.Scale( [ 6.1, 6.1, 6.1 ] )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( cube[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
