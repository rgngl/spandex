#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

WIDTH, HEIGHT = ( 320, 240, )

# ----------------------------------------------------------------------
class CullFace( vt.VisualTestCase ):
    
    """Testing glCullFace. Drawing some lines, points and two objects, of which
    the left one is facing towards and the right one is facing away from the
    camera. Lines and points should always be drawn. """
    
    testValues = { 'mode' : [ 'GL_FRONT', 'GL_BACK', 'GL_FRONT_AND_BACK' ],
                   }
    
    def __init__( self, mode ):
        vt.VisualTestCase.__init__( self )
        self.mode = mode
        self.name = 'OPENGLES1 cull face %s' % ( self.mode, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -3 ] )
        OpenGLES1.CullFace( self.mode )
        OpenGLES1.Enable( 'GL_CULL_FACE' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )           # quad vertices
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )   # quad colors
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )  # quad indices
        OpenGLES1.CreateArray( 3, 'GL_FLOAT' )           # line vertices
        OpenGLES1.CreateArray( 4, 'GL_UNSIGNED_BYTE' )   # line indices
        OpenGLES1.CreateArray( 5, 'GL_FLOAT' )           # points vertices
        OpenGLES1.CreateArray( 6, 'GL_UNSIGNED_BYTE' )   # points indices
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        linedata = lines()
        OpenGLES1.AppendToArray( 3, linedata[ 'vertices' ] )
        OpenGLES1.AppendToArray( 4, linedata[ 'indices' ] )
        
        pointdata = points( radius = 1.5 )
        OpenGLES1.AppendToArray( 5, pointdata[ 'vertices' ] )
        OpenGLES1.AppendToArray( 6, pointdata[ 'indices' ] )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ -1.0, 0.0, 0.0 ] )
        OpenGLES1.Scale( [ 4, 4, 4 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Rotate( 180, [ 0.0, 1.0, 0.0 ] )
        OpenGLES1.Scale( [ 4, 4, 4 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.DisableClientState( 'GL_COLOR_ARRAY' )
        # draw some lines
        OpenGLES1.VertexPointer( 3, 3, 0 )
        OpenGLES1.DrawElements( 'GL_LINE_LOOP', len( linedata[ 'indices' ] ), 0, 4 )
        # some points
        OpenGLES1.VertexPointer( 5, 3, 0 )
        OpenGLES1.DrawElements( 'GL_POINTS', len( pointdata[ 'indices' ] ), 0, 6 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        

# ----------------------------------------------------------------------        
class PolygonOffset( vt.VisualTestCase ):
    
    """ Testing glPolygonOffset. All quads are drawn at the exact same Z
    position, but the smaller quads are offsetted, left quad is above and right
    quad is below the bigger quad."""
        
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.name = 'OPENGLES1 polygon offset'
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )
        
        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1.0, 1.0, -1.0, 1.0, 1.0, 10.0 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )           # triangle vertices
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )   # triangle colors
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )  # triangle indices
        OpenGLES1.CreateArray( 3, 'GL_FLOAT' )           # line vertices
        OpenGLES1.CreateArray( 4, 'GL_UNSIGNED_SHORT' )  # line indices
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        data = triangleQuad()
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 1, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.Enable( 'GL_POLYGON_OFFSET_FILL' )
        OpenGLES1.PushMatrix()
        OpenGLES1.PolygonOffset( 1.0, -1 )
        OpenGLES1.Translate( [ -0.8, 0.0, 0.0 ] )
        OpenGLES1.Scale( [ 2, 2, 2 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.PushMatrix()
        OpenGLES1.PolygonOffset( 1.0, 1 )
        OpenGLES1.Translate( [ 0.8, 0.0, 0.0 ] )
        OpenGLES1.Scale( [ 2, 2, 2 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 2 )
        OpenGLES1.PopMatrix()
        OpenGLES1.Disable( 'GL_POLYGON_OFFSET_FILL' )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
