# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from gles1common import *

WIDTH, HEIGHT = ( 320, 240, )

######################################################################
class ClearColor( vt.VisualTestCase ):

        ''' The purpose of the test: test surface color buffer clearing.

        Expected output: surface cleared to RGBA color. Viewport size 320*240.
        '''
        
        testValues = { ( 'color' ) : [ ( [ 0.0, 0.0, 0.0, 1.0 ] ),
                                       ( [ 1.0, 0.0, 0.0, 1.0 ] ),
                                       ( [ 0.0, 1.0, 0.0, 1.0 ] ),
                                       ( [ 0.0, 0.0, 1.0, 1.0 ] ),
                                       ( [ 1.0, 1.0, 0.0, 1.0 ] ),
                                       ( [ 1.0, 0.0, 1.0, 1.0 ] ),
                                       ( [ 1.0, 1.0, 1.0, 1.0 ] ),
                                       ( [ 0.5, 0.5, 0.5, 0.5 ] ),
                                       ]
                       }
                                                                             
        def __init__( self, color ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.color   = color
                self.name    = "OPENGLES1 clear color, color=%s" % ( colorArrayToStr( self.color ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]               

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]                
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                
                OpenGLES1.ClearColor( self.color )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()               

                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.CheckError( '' )               
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class ClearColorWithMask( vt.VisualTestCase ):

        ''' The purpose of the test: test surface color buffer clearing with
        mask. The surface is first cleared to [0.0,0.0,0.0,1.0] with a default
        color mask. After that, clear color [1.0,1.0,1.0,1.0] is used with
        [0,0,1,1] color mask.

        Expected output: surface cleared to blue. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.name    = "OPENGLES1 clear with mask"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]               

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.ClearColor( [ 1.0, 1.0, 1.0, 1.0 ] )                
                OpenGLES1.ColorMask( 'OFF', 'OFF', 'ON', 'ON' )               
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES1.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
