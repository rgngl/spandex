#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common      import *
from lib.mesh        import *
from gles1common     import *
from gles_generators import *

WIDTH, HEIGHT = ( 320, 240, )

# ----------------------------------------------------------------------
class Triangle( vt.VisualTestCase ):
    
    """ Testing DrawElements(GL_TRIANGLES). A triangle with a gradient coloring
    should be drawn to the green surface, lower-left corner red, lower-right
    corner blue, and top green."""
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.name = 'OPENGLES1 draw elements (GL_TRIANGLES)'
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )
        
        OpenGLES1.ClearColor( [ 0.0, 1, 0, 1.0 ] )
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        tri = triangle() # Create a simple triangle
        vertexData = tri[ 'vertices' ]
        colorData  = tri[ 'colorsb' ]
        indexData  = tri[ 'indices' ]
        
        OpenGLES1.AppendToArray( 0, vertexData )
        OpenGLES1.AppendToArray( 1, colorData  )
        OpenGLES1.AppendToArray( 2, indexData )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3 ,0 ) # x, y, z
        OpenGLES1.ColorPointer( 1, 4, 0 )  # r, g, b, a
                
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', 3, 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------        
class TriangleFan( vt.VisualTestCase ):
    
    """Testing DrawElements(GL_TRIANGLE_FAN). A round triangle fan with 12 flat
    shaded segments should be drawn to the green surface. 11 o'clock should be
    red, 10 o'clock should be cyan, between them the segments should have
    distinct colors from red to cyan. """
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.name = 'OPENGLES1 draw elements (GL_TRIANGLE_FAN)'
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )
        
        OpenGLES1.ClearColor( [ 0, 1, 0, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.ShadeModel( 'GL_FLAT' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        tri = triangleFan( 12 )
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because
        # AppendDataToArray might change the pointer.
        OpenGLES1.VertexPointer( 0, 3 ,0 ) # x, y, z
        OpenGLES1.ColorPointer( 1, 4, 0 )  # r, g, b, a
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLE_FAN', len( tri[ 'indices' ] ), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------    
class TriangleStrip( vt.VisualTestCase ):
    
    """Testing DrawElements(GL_TRIANGLE_STRIP). A hollow triangle strip wheel
    with 12 flat shaded segments should be drawn to the green surface. 11
    o'clock should be magenta-blue, 10 o'clock should be green-yellow, between
    them the segments should have distinct colors from magenta-blue to
    green-yellow."""
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.name = 'OPENGLES1 draw elements (GL_TRIANGLE_STRIP)'
    
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 1,0, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.ShadeModel( 'GL_FLAT' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        tri = triangleStrip( 12 )
        vertexData = tri['vertices']
        colorData  = tri['colorsb']
        indexData  = tri['indices']        
        OpenGLES1.AppendToArray( 0, vertexData )
        OpenGLES1.AppendToArray( 1, colorData )
        OpenGLES1.AppendToArray( 2, indexData )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        
        OpenGLES1.VertexPointer( 0, 3 ,0 ) # x, y, z
        OpenGLES1.ColorPointer( 1, 4, 0 )  # r, g, b, a

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( ['GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT'] )
        OpenGLES1.DrawElements( 'GL_TRIANGLE_STRIP', len(tri['indices']), 0, 2 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        

# ----------------------------------------------------------------------
class Lines( vt.VisualTestCase ):
    
    """ Testing DrawElements(GL_LINES, GL_LINE_STRIP, GL_LINE_LOOP)."""
    
    testValues = { 'mode'   : [ 'GL_LINES', 'GL_LINE_STRIP', 'GL_LINE_LOOP' ],
                   'width'  : [ 1.0, 8.0 ],
                   'smooth' : [ False, True ]
                   }
    
    def __init__( self, mode, width, smooth = False ):
        vt.VisualTestCase.__init__( self )
        self.mode      = mode
        self.lineWidth = width
        self.smooth    = smooth
        self.lineCount = 12
        self.__doc__   += " Line width = %.2f." % ( self.lineWidth )
        self.__doc__   += " GL_LINE_SMOOTH %s" % ( { False : 'disabled', True : 'enabled' } [ smooth ], )
        self.name = 'OPENGLES1 draw elements (%s) width %.2f GL_LINE_SMOOTH %s' % ( self.mode,
                                                                                    self.lineWidth,
                                                                                    { False : 'disabled', True : 'enabled' } [ smooth ], )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 1, 0, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        
        if self.smooth:
            OpenGLES1.Enable( 'GL_LINE_SMOOTH' )
        else:
            OpenGLES1.Disable( 'GL_LINE_SMOOTH' )
            
        OpenGLES1.LineWidth( self.lineWidth )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_BYTE' )  # colors
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        data = lines( self.lineCount, 0.5, 1.5 )
        
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 2, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 1, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 2, 4, 0 )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.PushMatrix()
        OpenGLES1.Translate( [ -0.2, 0.0, 0.0 ] )
        OpenGLES1.DrawElements( self.mode, len( data[ 'indices' ] ), 0, 1 )
        OpenGLES1.PopMatrix()
        OpenGLES1.Translate( [ 0.2, 0.0, 0.0 ] )
        OpenGLES1.Rotate( 25.0, [ 0.0, 0.0, 1.0 ] )
        OpenGLES1.DrawElements( self.mode, len(data[ 'indices' ] ), 0, 1 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------
class Points( vt.VisualTestCase ):
    
    """ Testing DrawElements(GL_POINTS). Six circles of 12 points on a blue
    background. Each circle uses different point size. 11 o'clock points should
    be green, 10 o'clock points should be magenta."""
    
    testValues = { 'smooth' : [ False, True ]
                   }
       
    def __init__( self, smooth ):
        vt.VisualTestCase.__init__( self )
        self.smooth      = smooth
        self.point_sizes = ( 1.0, 2.0, 4.0, 8.0, 16.0, 32.0 )
        self.start_scale = [ 0.1, 0.1, 0.1 ]
        self.scale_step  = [ 1.75, 1.75, 1.75 ]
        self.rot_step    = 5.0

        smoothstr = "disabled"        
        if smooth:
            smoothstr = "enabled"

        self.__doc__ += " Point sizes: %s" % ( str( self.point_sizes ), )       
        self.name = 'OPENGLES1 draw elements (GL_POINTS) GL_POINT_SMOOTH %s' % ( smoothstr, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )          # vertices
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_BYTE' )  # colors
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
        
        if self.smooth:
            OpenGLES1.Enable( 'GL_POINT_SMOOTH' )
        else:
            OpenGLES1.Disable( 'GL_POINT_SMOOTH' )
            
        data = points( 12 ) # create simple quad
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 2, data[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 1, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        
        OpenGLES1.VertexPointer( 0, 3, 0 )
        OpenGLES1.ColorPointer( 2, 4, 0 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( ['GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT'] )
        OpenGLES1.Scale( self.start_scale )
        for size in self.point_sizes:
            OpenGLES1.PointSize( size )
            OpenGLES1.DrawElements( 'GL_POINTS', len(data[ 'indices' ] ), 0, 1 )
            OpenGLES1.Rotate( self.rot_step, [ 0.0, 0.0, 1.0 ] )
            OpenGLES1.Scale( self.scale_step )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
            

# ----------------------------------------------------------------------        
class DrawArrays( vt.VisualTestCase ):
    
    """Testing DrawArrays with different primitive types."""
    
    testValues = { 'mode' : [ 'GL_POINTS',
                              'GL_LINES',
                              'GL_LINE_STRIP',
                              'GL_LINE_LOOP',
                              'GL_TRIANGLES',
                              'GL_TRIANGLE_STRIP',
                              'GL_TRIANGLE_FAN'
                              ] }
    
    def __init__( self, mode ):
        vt.VisualTestCase.__init__( self )
        self.mode              = mode
        self.point_sizes       = ( 1.0, 2.0, 4.0, 8.0, 16.0, 32.0 )
        self.point_start_scale = [ 0.1, 0.1, 0.1 ]
        self.point_scale_step  = [ 1.75, 1.75, 1.75 ]
        self.point_rot_step    = 5.0
        self.__doc__ += { 'GL_POINTS':'',
                          'GL_LINES':'',
                          'GL_LINE_STRIP':'',
                          'GL_LINE_LOOP':'',
                          'GL_TRIANGLES':'',
                          'GL_TRIANGLE_STRIP':' Last segment is not drawn since indices are not used.',
                          'GL_TRIANGLE_FAN':' Last segment is not drawn since indices are not used.'
                          }[ self.mode ]
        self.name = 'OPENGLES1 draw arrays %s' % ( self.mode, )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        
        # Create the geometry
        if self.mode == 'GL_TRIANGLES':
            data = triangle()
        elif self.mode == 'GL_TRIANGLE_STRIP':
            data = triangleStrip()
        elif self.mode == 'GL_TRIANGLE_FAN':
            data = triangleFan()
        elif self.mode in ( 'GL_LINES','GL_LINE_LOOP','GL_LINE_STRIP' ):
            data = lines()
        elif self.mode == 'GL_POINTS':
            data = points()
            
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'indices' ] )
        if data.has_key( 'colorsb' ):
            OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
            OpenGLES1.AppendToArray( 2, data[ 'colorsb' ] )
            OpenGLES1.ColorPointer( 2, 4, 0 )
            
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
           
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( ['GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT'] )
        
        if self.mode in ( 'GL_TRIANGLES', 'GL_TRIANGLE_FAN', 'GL_TRIANGLE_STRIP' ):
            if self.mode == 'GL_TRIANGLES':
                OpenGLES1.Scale( [ 5, 5, 5 ] )
            OpenGLES1.DrawArrays( self.mode, 0, len( data[ 'vertices' ] ) / 3 )
        elif self.mode in ( 'GL_LINES', 'GL_LINE_LOOP', 'GL_LINE_STRIP' ):
            OpenGLES1.PushMatrix()
            OpenGLES1.Translate( [ -0.2, 0.0, 0.0 ] )
            OpenGLES1.DrawArrays( self.mode, 0, len( data[ 'vertices' ] ) / 3 )
            OpenGLES1.PopMatrix()
            OpenGLES1.Translate( [ 0.2, 0.0, 0.0 ] )
            OpenGLES1.Rotate( 25.0, [ 0.0, 0.0, 1.0 ] )
            OpenGLES1.DrawArrays( self.mode, 0, len( data[ 'vertices' ] ) / 3 )
        elif self.mode in ( 'GL_POINTS', ):
            OpenGLES1.Scale( self.point_start_scale )
            for size in self.point_sizes:
                OpenGLES1.PointSize( size )
                OpenGLES1.DrawArrays( self.mode, 0, len( data[ 'vertices' ] ) / 3 )
                OpenGLES1.Rotate( self.point_rot_step, [ 0.0, 0.0, 1.0 ] )
                OpenGLES1.Scale( self.point_scale_step )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                

# ----------------------------------------------------------------------        
class ColorPointer( vt.VisualTestCase ):
    
    """ Testing ColorPointer. A gray triangle on top of blue surface should show
    gradient coloring when the color pointer is enabled."""
    
    testValues = { 'enable' : [ True, False ]
                   }
    
    def __init__( self, enable ):
        vt.VisualTestCase.__init__( self )
        self.enable = enable
        self.name = 'OPENGLES1 color pointer %s' % ( { True : 'enabled', False : 'disabled' }[ self.enable ] )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.CreateArray( 2, 'GL_UNSIGNED_BYTE' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        
        tri = triangle()
        
        OpenGLES1.AppendToArray( 0, tri[ 'vertices' ] )
        OpenGLES1.AppendToArray( 2, tri[ 'colorsb' ] )
        OpenGLES1.AppendToArray( 1, tri[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        
        if self.enable:
            OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
            OpenGLES1.ColorPointer( 2, 4, 0 )
        else:
            OpenGLES1.DisableClientState( 'GL_COLOR_ARRAY' )

        OpenGLES1.Color( [ 0.5, 0.5, 0.5, 1.0 ] )            

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.Scale( [ 5, 5, 5 ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( tri[ 'indices' ] ), 0, 1 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
# ----------------------------------------------------------------------        
class NormalPointer( vt.VisualTestCase ):
    
    """ Testing NormalPointer. The sphere on the blue surface should be shaded
    to constant dull green when normal pointer is disabled. Lighting should
    become visible when the normal pointer is enabled."""
    
    testValues = { 'enable' : [ True, False ]
                   }
    
    def __init__( self, enable ):
        vt.VisualTestCase.__init__( self )
        self.enable = enable
        self.name = 'OPENGLES1 normal pointer %s' % ( { True : 'enabled', False : 'disabled' }[ self.enable ], )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.Enable( 'GL_LIGHTING' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Enable( 'GL_NORMALIZE' )
        light = { 'Ambient'              : [ 0.0, 0.0, 0.0, 1.0 ],
                  'Diffuse'              : [ 0.3, 0.3, 0.3, 1.0 ],
                  'Specular'             : [ 0.5, 0.5, 0.0, 1.0 ],
                  'Position'             : [ 1.0, 1.0, 1.0, 0.0 ],
                  'SpotDirection'        : [ 0.0, 0.0, -1.0 ],
                  'SpotExponent'         : 0.0,
                  'SpotCutoff'           : 180.0,
                  'ConstantAttenuation'  : 1.0,
                  'LinearAttenuation'    : 0.0,
                  'QuadraticAttenuation' : 0.0,
                  }
        OpenGLES1.Enable('GL_LIGHT0')
        OpenGLES1.Light( 0,
                         light[ 'Ambient' ],
                         light[ 'Diffuse' ],
                         light[ 'Specular' ],
                         light[ 'Position' ],
                         light[ 'SpotDirection' ],
                         light[ 'SpotExponent' ],
                         light[ 'SpotCutoff' ],
                         light[ 'ConstantAttenuation' ],
                         light[ 'LinearAttenuation' ],
                         light[ 'QuadraticAttenuation' ]
                         )
        
        OpenGLES1.LightModelTwoSided( 'ON' )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 100 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Translate( [ 0, 0, -2 ] )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' )
        OpenGLES1.CreateArray( 2, 'GL_FLOAT' )
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        
        data = loadMesh( "sphere.mesh" )
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 1, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        
        if self.enable:
            OpenGLES1.EnableClientState( 'GL_NORMAL_ARRAY' )
            OpenGLES1.AppendToArray( 2, data[ 'normals' ] )
            OpenGLES1.NormalPointer( 2, 0 )
        else:
            OpenGLES1.DisableClientState( 'GL_NORMAL_ARRAY' )
            
        OpenGLES1.Material( data[ 'material' ][ 'ambient' ],
                            data[ 'material' ][ 'diffuse' ],
                            data[ 'material' ][ 'specular' ],
                            data[ 'material' ][ 'emission' ],
                            data[ 'material' ][ 'shininess' ]
                            )
        
        OpenGLES1.Scale( [ 4, 4, 4 ] )
        OpenGLES1.Normal( [ 1, 1, 1 ] )
                
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 1 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        

# ----------------------------------------------------------------------        
class TexCoordPointer( vt.VisualTestCase ):
    
    """ Testing TexCoordPointer. The sphere on the blue surface should be shaded
    to constant gray when texcoord pointer is disabled. A black-and-white
    checker texture should become visible on the object once the texcoord
    pointer is enabled."""
    
    testValues = { 'enable' : [ True, False ]
                   }
    
    def __init__( self, enable ):
        vt.VisualTestCase.__init__( self )
        self.enable = enable
        self.name = 'OPENGLES1 texcoord pointer %s' % ( { True : 'enabled', False : 'disabled' }[ self.enable ] )
        
    def build( self, target, modules ):
        OpenGLES1 = modules[ 'OpenGLES1' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()
                
        # ------------------------------------------------------------
        # Init actions
        self.beginInitActions()

        apis = [ API_OPENGLES1 ]
        defaultAttributes   = target.getDefaultAttributes( apis )
        state               = target.setup( modules,
                                            indexTracker,
                                            WIDTH, HEIGHT,
                                            apis,
                                            defaultAttributes )

        OpenGLES1.ClearColor( [ 0, 0, 1, 1 ] )
        OpenGLES1.MatrixMode( 'GL_PROJECTION' )
        OpenGLES1.Frustum( -1, 1, -1, 1, 1, 10 )
        OpenGLES1.MatrixMode( 'GL_MODELVIEW' )
        OpenGLES1.Enable( 'GL_DEPTH_TEST' )
        OpenGLES1.Enable( 'GL_CULL_FACE' )
        OpenGLES1.Hint( 'GL_PERSPECTIVE_CORRECTION_HINT', 'GL_FASTEST' )
        OpenGLES1.CreateArray( 0, 'GL_FLOAT' )
        OpenGLES1.CreateArray( 1, 'GL_UNSIGNED_SHORT' ) # indices
        OpenGLES1.CreateArray( 2, 'GL_FLOAT' )          # texture coordinates
        OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )
        OpenGLES1.Enable( 'GL_TEXTURE_2D' )
        
        textureIndex = 0
        OpenGLES1.GenTexture( textureIndex )
        
        textureDataIndex = 0
        OpenGLES1.CreateCheckerTextureData( textureDataIndex,
                                            [ 1, 1, 1, 1 ],
                                            [ 0, 0, 0, 1 ],
                                            32, 32,
                                            128, 128,
                                            'ON',
                                            'OPENGLES1_RGB888' )
        
        OpenGLES1.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES1.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES1.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE',
                                'OFF' )
        
        data = loadMesh( "sphere.mesh" )
        OpenGLES1.AppendToArray( 0, data[ 'vertices' ] )
        OpenGLES1.AppendToArray( 2, data[ 'texcoords' ] )
        OpenGLES1.AppendToArray( 1, data[ 'indices' ] )
        
        # NOTE! Set pointers *after* creating the geometry because AppendDataToArray
        # might change the pointer.
        OpenGLES1.VertexPointer( 0, 3, 0 )
        
        if self.enable:
            OpenGLES1.EnableClientState( 'GL_TEXTURE_COORD_ARRAY' )
            OpenGLES1.TexCoordPointer( 2 , 2 ,0 )
        else:
            OpenGLES1.DisableClientState( 'GL_TEXTURE_COORD_ARRAY' )
        
        OpenGLES1.Translate( [ 0, 0.0, -3.1 ] )
        OpenGLES1.Rotate( 30.0, [ 1.0, 0.0, 0.0 ] )
        OpenGLES1.Scale( [ 6, 6, 6 ] )

        OpenGLES1.MultiTexCoord( 0, [ 0.5, 0.5, 0, 1.0 ] )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )
        OpenGLES1.DrawElements( 'GL_TRIANGLES', len( data[ 'indices' ] ), 0, 1 )

        OpenGLES1.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
