# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from gles1common import *

WIDTH, HEIGHT = ( 320, 240, )

######################################################################
class Info( vt.VisualTestCase ):

        ''' The purpose of the test: print OpenGLES1 information to the result
        file.

        Expected output: green surface. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.name    = "OPENGLES1 info"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]               

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                
                OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );                
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                # Clear the screen
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                OpenGLES1.Info()
                OpenGLES1.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class EglInfo( vt.VisualTestCase ):

        ''' The purpose of the test: print egl information to the result
        file. Requires EGL.

        Expected output: green surface. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.name    = "OPENGLES1 egl info"
        
        def build( self, target, modules ):
                Egl       = modules[ 'Egl' ]                
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]               

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                displayIndex = state[ 0 ]

                OpenGLES1.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );                
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                # Clear the screen
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                Egl.Info( displayIndex )
                
                OpenGLES1.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

