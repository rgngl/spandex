# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles1common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 8, 8 )

# ----------------------------------------------------------------------
class VertexType( vt.VisualTestCase ):

        ''' The purpose of the test: test different vertex types.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        testValues = { ( 'vertexType' ) : [ ( 'GL_FIXED' ),
                                            ( 'GL_FLOAT' ),
                                            ]
                       }
        
        def __init__( self, vertexType ):
                vt.VisualTestCase.__init__( self )
                self.vertexType = vertexType
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES1 vertex type, type=%s" % ( typeToStr( self.vertexType ), )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                meshTypes = { 'GL_FIXED' : MESH_TYPE_FIXED,
                              'GL_FLOAT' : MESH_TYPE_FLOAT }
                
                vertexAttribute = MeshVertexAttribute( meshTypes[ self.vertexType ], 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                useVertexData( modules, indexTracker, vertexDataSetup )

                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                               
                                
# ----------------------------------------------------------------------
class VertexPointerOffset( vt.VisualTestCase ):

        ''' The purpose of the test: test vertex pointer offset. Padding is
        added at the beginning of vertex data which is skipped using offset.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        testValues = { ( 'offset' ) : [ ( 0 ),
                                        ( 2 ),
                                        ( 5 ),                                        
                                        ]
                       }
        
        def __init__( self, offset ):
                vt.VisualTestCase.__init__( self )
                self.offset     = offset
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.name = "OPENGLES1 vertex pointer offset, offset=%d" % ( self.offset, )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()               
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshTrianglePlane( self.gridx,
                                          self.gridy,
                                          vertexAttribute,
                                          colorAttribute,
                                          None,
                                          None,
                                          indexAttribute,
                                          None,
                                          MESH_CCW_WINDING,
                                          False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                paddingVertex = []
                paddingColor  = []
                paddingIndex  = []
                for i in range( self.offset ):
                        paddingVertex += [ i * 10 ] * mesh.vertexComponents
                        paddingColor  += [ ( i * 10 ) % 255 ] * mesh.colorComponents
                        paddingIndex  += [ i, i + 1, i + 2 ]
                mesh.vertices = paddingVertex + mesh.vertices
                mesh.colors   = paddingColor + mesh.colors
                mesh.indices  = paddingIndex + mesh.indices
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )

                OpenGLES1.VertexPointer( vertexDataSetup.vertexArrayIndex,
                                         vertexDataSetup.vertexSize,
                                         self.offset )
                OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )

                OpenGLES1.ColorPointer( vertexDataSetup.colorArrayIndex,
                                        vertexDataSetup.colorSize,
                                        self.offset )
                OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )                

                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES1.DrawElements( vertexDataSetup.glMode,
                                        vertexDataSetup.indexArrayLength - self.offset * 3, 
                                        self.offset * 3,
                                        vertexDataSetup.indexArrayIndex )
                        
                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
# ----------------------------------------------------------------------
class VertexBuffer( vt.VisualTestCase ):

        ''' The purpose of the test: test vertex buffer.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES1 vertex buffer"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                               
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )

                vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )
                useVertexBufferData( modules, indexTracker, vertexBufferDataSetup )
                
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta  = 1.0 / float( self.overdraw )                
                offset = 1.0
                
                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawVertexBufferData( modules, vertexBufferDataSetup )
                        offset -= delta
                        
                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------
class VertexBufferOffset( vt.VisualTestCase ):

        ''' The purpose of the test: test vertex buffer offset. Padding is added
        at the beginning of vertex data which is skipped using offset.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        testValues = { ( 'offset' ) : [ ( 0 ),
                                        ( 2 ),
                                        ( 5 ),                                        
                                        ]
                       }
        
        def __init__( self, offset ):
                vt.VisualTestCase.__init__( self )
                self.offset     = offset
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES1 vertex buffer offset, offset=%d" % ( self.offset, )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                paddingVertex = []
                paddingColor  = []
                paddingIndex  = []
                for i in range( self.offset ):
                        paddingVertex += [ i * 10 ] * mesh.vertexComponents
                        paddingColor  += [ ( i * 10 ) % 255 ] * mesh.colorComponents
                        paddingIndex  += [ i, i + 1, i + 2 ]
                mesh.vertices = paddingVertex + mesh.vertices
                mesh.colors   = paddingColor + mesh.colors
                mesh.indices  = paddingIndex + mesh.indices
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )

                vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
                OpenGLES1.GenBuffer( vertexBufferIndex )
                OpenGLES1.BufferData( vertexBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      vertexDataSetup.vertexArrayIndex,
                                      0,
                                      0 )
    
                colorBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
                OpenGLES1.GenBuffer( colorBufferIndex )
                OpenGLES1.BufferData( colorBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      vertexDataSetup.colorArrayIndex,
                                      0,
                                      0 )
        
                indexBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
                OpenGLES1.GenBuffer( indexBufferIndex )
                OpenGLES1.BufferData( indexBufferIndex,
                                      'ON',
                                      'GL_ELEMENT_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      vertexDataSetup.indexArrayIndex,
                                      0,
                                      0 )

                OpenGLES1.VertexBuffer( vertexBufferIndex,
                                        'ON',
                                        vertexDataSetup.vertexSize,
                                        self.offset )
                OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )

                OpenGLES1.ColorBuffer( colorBufferIndex,
                                       'ON',
                                       vertexDataSetup.colorSize,
                                       self.offset )
                OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )
               
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta  = 1.0 / float( self.overdraw )                
                offset = 1.0
                
                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        OpenGLES1.BufferDrawElements( indexBufferIndex,
                                                      'ON',
                                                      vertexDataSetup.glMode,
                                                      vertexDataSetup.indexArrayLength - self.offset * 3, 
                                                      self.offset * 3 )                           
                        offset -= delta
                        
                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
                
# ----------------------------------------------------------------------
class InterleavedArrays( vt.VisualTestCase ):

        ''' The purpose of the test: test interleaved vertex attribute arrays.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES1 interleaved arrays"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                               
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup   = setupMeshData( modules, indexTracker, vertexDataSetup )
                useMeshData( modules, indexTracker, meshDataSetup )

                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta  = 1.0 / float( self.overdraw )                
                offset = 1.0
                
                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta
                        
                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
               

# ----------------------------------------------------------------------
class InterleavedBufferArrays( vt.VisualTestCase ):

        ''' The purpose of the test: test interleaved vertex attribute arrays
        from a buffer.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES1 interleaved buffer arrays"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                               
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup     = setupVertexData( modules, indexTracker, mesh )
                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup )               

                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta  = 1.0 / float( self.overdraw )                
                offset = 1.0
                
                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta
                        
                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
                
# ----------------------------------------------------------------------
class VertexBufferUsage( vt.VisualTestCase ):

        ''' The purpose of the test: test vertex buffer usage.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        testValues = { ( 'usage' ) : [ ( 'GL_STATIC_DRAW' ),
                                       ( 'GL_DYNAMIC_DRAW' ),
                                       ]
                       }       
        
        def __init__( self, usage ):
                vt.VisualTestCase.__init__( self )
                self.repeats  = 1
                self.usage    = usage
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.overdraw = 1
                self.name = "OPENGLES1 vertex buffer usage, usage=%s" % ( self.usage[ 3: ], )
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )

                vertexBufferIndex       = -1
                vertexArrayNormalized   = False
                colorBufferIndex        = -1
                colorArrayNormalized    = False
                normalBufferIndex       = -1
                texCoordBufferIndices   = []
                texCoordArrayNormalized = []
                indexArrayIndex         = -1

                vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
                OpenGLES1.GenBuffer( vertexBufferIndex )
                OpenGLES1.BufferData( vertexBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      self.usage,
                                      vertexDataSetup.vertexArrayIndex,
                                      0,
                                      0 )
    
                colorBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
                OpenGLES1.GenBuffer( colorBufferIndex )
                OpenGLES1.BufferData( colorBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      self.usage,
                                      vertexDataSetup.colorArrayIndex,
                                      0,
                                      0 )
        
                indexBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
                OpenGLES1.GenBuffer( indexBufferIndex )
                OpenGLES1.BufferData( indexBufferIndex,
                                      'ON',
                                      'GL_ELEMENT_ARRAY_BUFFER',
                                      self.usage,
                                      vertexDataSetup.indexArrayIndex,
                                      0,
                                      0 )

                vertexBufferDataSetup =  VertexBufferDataSetup( vertexBufferIndex,
                                                                vertexDataSetup.vertexSize,
                                                                colorBufferIndex,
                                                                vertexDataSetup.colorSize,
                                                                normalBufferIndex,
                                                                texCoordBufferIndices,
                                                                vertexDataSetup.texCoordSizes,
                                                                indexBufferIndex,
                                                                vertexDataSetup.indexArrayLength,
                                                                vertexDataSetup.glMode )
                
                useVertexBufferData( modules, indexTracker, vertexBufferDataSetup )

                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta  = 1.0 / float( self.overdraw )                
                offset = 1.0
                
                for i in range( self.overdraw ):
                        OpenGLES1.LoadIdentity()
                        OpenGLES1.Translate( [ 0.0, 0.0, offset ] )
                        drawVertexBufferData( modules, vertexBufferDataSetup )                        
                        offset -= delta
                        
                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


# ----------------------------------------------------------------------
class VertexBufferSubdata( vt.VisualTestCase ):

        ''' The purpose of the test: test vertex buffer subdata.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats    = 1
                self.offset     = 8
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES1 vertex buffer subdata"
        
        def build( self, target, modules ):       
                OpenGLES1 = modules[ 'OpenGLES1' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES1 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshTrianglePlane( self.gridx,
                                          self.gridy,
                                          vertexAttribute,
                                          colorAttribute,
                                          None,
                                          None,
                                          indexAttribute,
                                          None,
                                          MESH_CCW_WINDING,
                                          False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                subbedVertices = []
                subbedColors   = []
                subbedIndices  = []
                vertexCount = len( mesh.vertices ) / mesh.vertexComponents
                for i in range( self.offset + vertexCount ):
                        subbedVertices += [ 0 ] * mesh.vertexComponents
                        subbedColors   += [ 0 ] * mesh.colorComponents

                for i in range( len( mesh.indices ) / 3 + self.offset ):
                        subbedIndices  += [ 0, 0, 0 ]

                subbedVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
                OpenGLES1.CreateArray( subbedVertexArrayIndex,
                                       mesh.vertexGLType )
                OpenGLES1.AppendToArray( subbedVertexArrayIndex, subbedVertices )

                vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
                OpenGLES1.CreateArray( vertexArrayIndex,
                                       mesh.vertexGLType )
                OpenGLES1.AppendToArray( vertexArrayIndex, mesh.vertices )
                
                subbedColorArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
                OpenGLES1.CreateArray( subbedColorArrayIndex,
                                       mesh.colorGLType )
                OpenGLES1.AppendToArray( subbedColorArrayIndex, subbedColors )

                colorArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
                OpenGLES1.CreateArray( colorArrayIndex,
                                       mesh.colorGLType )
                OpenGLES1.AppendToArray( colorArrayIndex, mesh.colors )
                
                subbedIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
                OpenGLES1.CreateArray( subbedIndexArrayIndex, mesh.indexGLType );
                OpenGLES1.AppendToArray( subbedIndexArrayIndex, subbedIndices )

                indexArrayIndex = indexTracker.allocIndex( 'OPENGLES1_ARRAY_INDEX' )
                OpenGLES1.CreateArray( indexArrayIndex, mesh.indexGLType );
                OpenGLES1.AppendToArray( indexArrayIndex, mesh.indices )
                
                vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
                OpenGLES1.GenBuffer( vertexBufferIndex )
                OpenGLES1.BufferData( vertexBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      subbedVertexArrayIndex,
                                      0,
                                      0 )

                OpenGLES1.BufferSubData( vertexBufferIndex,
                                         'ON',
                                         'GL_ARRAY_BUFFER',
                                         self.offset * mesh.vertexComponents,
                                         vertexArrayIndex,
                                         0,
                                         0 )
                
                colorBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
                OpenGLES1.GenBuffer( colorBufferIndex )
                OpenGLES1.BufferData( colorBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      subbedColorArrayIndex,
                                      0,
                                      0 )
                
                OpenGLES1.BufferSubData( colorBufferIndex,
                                         'ON',
                                         'GL_ARRAY_BUFFER',
                                         self.offset * mesh.colorComponents,
                                         colorArrayIndex,
                                         0,
                                         0 )

                indexBufferIndex = indexTracker.allocIndex( 'OPENGLES1_BUFFER_INDEX' )
                OpenGLES1.GenBuffer( indexBufferIndex )
                OpenGLES1.BufferData( indexBufferIndex,
                                      'ON',
                                      'GL_ELEMENT_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      subbedIndexArrayIndex,
                                      0,
                                      0 )

                OpenGLES1.BufferSubData( indexBufferIndex,
                                         'ON',
                                         'GL_ELEMENT_ARRAY_BUFFER',
                                         self.offset * 3,
                                         indexArrayIndex,
                                         0,
                                         0 )
                
                OpenGLES1.VertexBuffer( vertexBufferIndex,
                                        'ON',
                                        mesh.vertexComponents,
                                        self.offset )
                OpenGLES1.EnableClientState( 'GL_VERTEX_ARRAY' )

                OpenGLES1.ColorBuffer( colorBufferIndex,
                                       'ON',
                                       mesh.colorComponents,
                                       self.offset )
                OpenGLES1.EnableClientState( 'GL_COLOR_ARRAY' )                
              
                OpenGLES1.Enable( 'GL_DEPTH_TEST' )
                OpenGLES1.DepthFunc( 'GL_LEQUAL' )
                OpenGLES1.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES1.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES1.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES1.BufferDrawElements( indexBufferIndex,
                                              'ON',
                                              mesh.glMode,
                                              len( subbedIndices ) - self.offset * 3, 
                                              self.offset * 3 )
                
                OpenGLES1.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

