#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class ColorInStrokes( vt.VisualTestCase ):
    
    '''
    Test for VG_PAINT_TYPE_COLOR paint type in strokes. Test draws one path
    stroke (VG_LINE_TO[60,0]) in a loop with different paints. Paints include 50
    different color values from 0 to 1 for red, green, blue and alpha channels.
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1        
        self.name = 'OPENVG paint color in strokes'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 3, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        
        OpenVG.Lines( pathDataIndex, [ 60, 0 ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        pathspercolor = 50

        paints = []
        for i in range( pathspercolor * 3 ):
            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
            
            paints.append( i )
            
        for i in range( pathspercolor ):
            alpha = float( i ) / float( 50 )                                                             # alpha
            OpenVG.ColorPaint( paints[ i ],                    [ i / float( pathspercolor ), 0, 0, alpha ] ) # red
            OpenVG.ColorPaint( paints[ i + pathspercolor ],    [ 0, i / float( pathspercolor ), 0, alpha ] ) # green
            OpenVG.ColorPaint( paints[ i + 2* pathspercolor ], [ 0, 0, i / float( pathspercolor ), alpha ] ) # blue
    
        # ----------------------------------------------------------------------           
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( pathspercolor ):
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )
            OpenVG.LoadIdentity( )
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.Translate( 0, i * HEIGHT / pathspercolor )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
            OpenVG.SetPaint( paints[ i + pathspercolor ], [ 'VG_STROKE_PATH' ] )
            OpenVG.Translate( 70, 0 )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
            OpenVG.SetPaint( paints[ i + 2 * pathspercolor ], [ 'VG_STROKE_PATH' ] )
            OpenVG.Translate( 70, 0 )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################        
class ColorInFills( vt.VisualTestCase ):
    
    '''
    Test for VG_PAINT_TYPE_COLOR paint type in fills. Test draws one path
    fill (VG_LINE_TO[60,0], VG_LINE_TO[0,3], VG_LINE_TO[-60,0],
    VG_LINE_TO[0,-3]) in a loop with different paints. Paints include 50
    different color values from 0 to 1 for red, green, blue and alpha channels.
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG paint color in fills'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 3, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
        
        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        
        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        
        OpenVG.Lines( pathDataIndex, [ 60, 0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ 0,  3 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ -60, 0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ 0, -3 ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        pathspercolor = 50

        paints = []
        for i in range( pathspercolor * 3 ):
            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
            
            paints.append( paintIndex )

        for i in range( pathspercolor ):
            alpha = float( i ) / float( 50 )                                                              # alpha
            OpenVG.ColorPaint( paints[ i ],                     [ i / float( pathspercolor ), 0, 0, alpha ] ) # red
            OpenVG.ColorPaint( paints[ i + pathspercolor ],     [ 0, i / float( pathspercolor ), 0, alpha ] ) # green
            OpenVG.ColorPaint( paints[ i + 2 * pathspercolor ], [ 0, 0, i / float( pathspercolor ), alpha ] ) # blue
    
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( pathspercolor ):
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )
            OpenVG.LoadIdentity( )
            OpenVG.SetPaint( paints[ i ], [ 'VG_FILL_PATH' ] )
            OpenVG.Translate( 0, i * HEIGHT / pathspercolor )
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
            OpenVG.SetPaint( paints[ i + pathspercolor ], [ 'VG_FILL_PATH' ] )
            OpenVG.Translate( 70, 0 )
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
            OpenVG.SetPaint( paints[ i + 2 * pathspercolor ], [ 'VG_FILL_PATH' ] )
            OpenVG.Translate( 70, 0 )
            OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class LinearGradientInStrokes( vt.VisualTestCase ):
    
    '''
    Test for linear gradients with different spread modes in strokes. Test
    creates color ramps (including stops and colors) for the linear gradient,
    applies 75 separate VG_LINE_TO segments to a path and finally draws the
    path. Linear gradient is from coordinates [50,50] to [150,150] and includes
    red color [alpha 1.0] at startpoint, green color [alpha 0.5] in the middle
    and blue color [alpha 1.0] at endpoint.
    '''

    testValues = { 'spreadmode' : [ 'VG_COLOR_RAMP_SPREAD_PAD',
                                    'VG_COLOR_RAMP_SPREAD_REPEAT',
                                    'VG_COLOR_RAMP_SPREAD_REFLECT' ]
                   }

    def __init__( self, spreadmode ):
        vt.VisualTestCase.__init__( self )
        self.spreadmode = spreadmode
        self.repeats    = 1        
        self.name = 'OPENVG paint linear gradient in strokes %s' % ( spreadmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        
        OpenVG.LinearGradientPaint( paintIndex, [ 50, 50 ], [ 150, 150 ] )
        OpenVG.ColorRamps( paintIndex,
                           self.spreadmode,
                           'ON',
                           [ 0.0, 1.0, 0.0, 0.0, 1.0,
                             0.5, 0.0, 1.0, 0.0, 0.5,
                             1.0, 0.0, 0.0, 1.0, 1.0 ] )
        
        for i in range( 25 ):
            OpenVG.MoveTo( pathDataIndex, [ 0,i*8 ], 'OFF' ) 
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ 10,0 ], 'ON' ) 
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ 10,0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class LinearGradientInFills( vt.VisualTestCase ):
    
    '''
    Test for linear gradients with different spread modes in fills. Test
    creates color ramps for the linear gradient and applies 75 separate segments
    to a path (one segment is VG_LINE_TO[60,0], VG_LINE_TO[0,5],
    VG_LINE_TO[-60,0], VG_LINE_TO[0,-5]). The gradient [60,60][125,125] includes
    red color [alpha 1.0] at the startpoint, green color [alpha 0.5] in the
    middle and blue color [alpha 1.0] at the endpoint.
    '''

    testValues = { 'spreadmode' : [ 'VG_COLOR_RAMP_SPREAD_PAD',
                                    'VG_COLOR_RAMP_SPREAD_REPEAT',
                                    'VG_COLOR_RAMP_SPREAD_REFLECT' ]
                   }

    def __init__( self, spreadmode ):
        vt.VisualTestCase.__init__( self )
        self.spreadmode = spreadmode
        self.repeats    = 1
        self.name = 'OPENVG paint linear gradient in fills %s' % ( spreadmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        
        OpenVG.LinearGradientPaint( paintIndex, [ 60, 60 ], [ 125, 125 ] )
        OpenVG.ColorRamps( paintIndex,
                           self.spreadmode,
                           'ON',
                           [ 0.0, 1.0, 0.0, 0.0, 1.0,
                             0.5, 0.0, 1.0, 0.0, 0.5,
                             1.0, 0.0, 0.0, 1.0, 1.0 ] )
        
        for i in range( 25 ):
            OpenVG.MoveTo( pathDataIndex, [ 0,i *8 ], 'OFF' ) 
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0,  5 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ -60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0, -5 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ 70, 0 ], 'ON' ) 
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0, 5 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ -60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0, -5 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ 70, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0,  5 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ -60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0, -5 ], 'ON' )
            
        OpenVG.AppendPathData( pathIndex, pathDataIndex )
       
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex , [ 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        

######################################################################
class RadialGradientInStrokes( vt.VisualTestCase ):
    
    '''
    Test for radial gradients with different spread modes in strokes. Test
    creates color ramps for the radial gradient and applies 75 separate
    VG_LINE_TO segments to a path and finally draws the path. Radial gradient is
    [cx,cy,fx,fy,r]=[75,75,30,30,75] and includes red color [alpha 1.0] at
    startpoint, green color [alpha 0.5] in the middle and blue color [alpha 1.0]
    at endpoint.
    '''

    testValues = { 'spreadmode' : [ 'VG_COLOR_RAMP_SPREAD_PAD',
                                    'VG_COLOR_RAMP_SPREAD_REPEAT',
                                    'VG_COLOR_RAMP_SPREAD_REFLECT' ]
                   }

    def __init__( self, spreadmode ):
        vt.VisualTestCase.__init__( self )
        self.spreadmode = spreadmode
        self.repeats    = 1
        self.name = 'OPENVG paint radial gradient in strokes %s' % ( spreadmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()
        
        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
       
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        
        OpenVG.RadialGradientPaint( paintIndex, [ 75, 75 ], [ 30, 30 ], 75 )
        OpenVG.ColorRamps( paintIndex,
                       self.spreadmode,
                       'ON',
                       [ 0.0, 1.0, 0.0, 0.0, 1.0,
                         0.5, 0.0, 1.0, 0.0, 0.5,
                         1.0, 0.0, 0.0, 1.0, 1.0 ] )
        
        for i in range( 25 ):
            OpenVG.MoveTo( pathDataIndex, [ 0, i * 8 ], 'OFF' ) 
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ 10, 0 ], 'ON' ) 
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ 10, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
            
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( 0, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( 0, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class RadialGradientInFills( vt.VisualTestCase ):
    
    '''
    Test for radial gradients with different spread modes in fills. Test
    creates color ramps for the radial gradient and applies 75 separate
    VG_LINE_TO segments to a path and finally draws the path. Radial gradient is
    [cx,cy,fx,fy,r]=[75,75,30,30,75] and includes red color [alpha 1.0] at
    startpoint, green color [alpha 0.5] in the middle and blue color [alpha 1.0]
    at endpoint.
    '''

    testValues = { 'spreadmode' : [ 'VG_COLOR_RAMP_SPREAD_PAD',
                                    'VG_COLOR_RAMP_SPREAD_REPEAT',
                                    'VG_COLOR_RAMP_SPREAD_REFLECT' ]
                   }

    def __init__( self, spreadmode ):
        vt.VisualTestCase.__init__( self )
        self.spreadmode = spreadmode
        self.repeats    = 1
        self.name = 'OPENVG paint radial gradient in fills %s' % ( spreadmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 1, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        
        OpenVG.RadialGradientPaint( paintIndex, [ 75, 75 ], [ 30, 30 ], 75 )
        OpenVG.ColorRamps( paintIndex,
                           self.spreadmode,
                           'ON',
                           [ 0.0, 1.0, 0.0, 0.0, 1.0,
                             0.5, 0.0, 1.0, 0.0, 0.5,
                             1.0, 0.0, 0.0, 1.0, 1.0 ] )
        
        for i in range( 25 ):
            OpenVG.MoveTo( pathDataIndex, [ 0, i * 8 ], 'OFF' ) 
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0, 5 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ -60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0, -5 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ 70, 0 ], 'ON' ) 
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0, 5 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ -60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0, -5 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ 70, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0, 5 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ -60, 0 ], 'ON' )
            OpenVG.Lines ( pathDataIndex, [ 0, -5 ], 'ON' )
            
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ---------------------------------------------------------------------- 
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        

######################################################################
class PatternInStrokes( vt.VisualTestCase ):
    
    '''
    Test for pattern paints with different tiling modes in strokes.
    '''

    testValues = { 'tilingmode'  : [ 'VG_TILE_FILL',
                                     'VG_TILE_PAD',
                                     'VG_TILE_REPEAT',
                                     'VG_TILE_REFLECT' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, tilingmode, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.tilingmode  = tilingmode
        self.imageformat = imageformat
        self.repeats     = 1
        self.name = 'OPENVG paint pattern in strokes %s %s' % ( imageformat, tilingmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH / 2, HEIGHT / 2 )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.imageformat, WIDTH / 2, HEIGHT / 2, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.imageformat, WIDTH / 2, HEIGHT / 2 )
        
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH / 2, HEIGHT / 2 )
        OpenVG.ImageSubData( imageDataIndex, imageIndex, 0, 0, WIDTH / 2, HEIGHT / 2 )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        
        OpenVG.PatternPaint( paintIndex, imageIndex, self.tilingmode )
        OpenVG.LoadIdentity()
        
        for i in range( 25 ):
            OpenVG.MoveTo( pathDataIndex, [ 0, i * 8 ], 'OFF' ) 
            OpenVG.Lines( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ 10, 0 ], 'ON' ) 
            OpenVG.Lines( pathDataIndex, [ 60, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ 10, 0 ], 'ON' )
            OpenVG.Lines( pathDataIndex, [ 60, 0 ], 'ON' )
            
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ---------------------------------------------------------------------- 
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( 0, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( 0, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class PatternInFills( vt.VisualTestCase ):
    
    '''
    Test for pattern paints with different tiling modes in fills.
    '''

    testValues = { 'tilingmode'  : [ 'VG_TILE_FILL',
                                     'VG_TILE_PAD',
                                     'VG_TILE_REPEAT',
                                     'VG_TILE_REFLECT' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, tilingmode, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.tilingmode  = tilingmode
        self.imageformat = imageformat
        self.repeats     = 1
        self.name = 'OPENVG paint pattern in fills %s %s' % ( imageformat, tilingmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()
        
        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )

        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH / 2, HEIGHT / 2 )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.imageformat, WIDTH / 2, HEIGHT / 2, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.imageformat, WIDTH / 2, HEIGHT / 2 )
        
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH / 2, HEIGHT / 2 )
        OpenVG.ImageSubData( imageDataIndex, imageIndex, 0, 0, WIDTH / 2, HEIGHT / 2 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        
        OpenVG.PatternPaint( paintIndex, imageIndex, self.tilingmode )
        OpenVG.LoadIdentity()
        
        for i in range( 25 ):
                OpenVG.MoveTo( 0, [ 0, i * 8 ], 'OFF' ) 
                OpenVG.Lines ( 0, [ 60, 0 ], 'ON' )
                OpenVG.Lines ( 0, [ 0, 5 ], 'ON' )
                OpenVG.Lines ( 0, [ -60, 0 ], 'ON' )
                OpenVG.Lines ( 0, [ 0, -5 ], 'ON' )
                OpenVG.MoveTo( 0, [ 70, 0 ], 'ON' ) 
                OpenVG.Lines ( 0, [ 60, 0 ], 'ON' )
                OpenVG.Lines ( 0, [ 0, 5 ], 'ON' )
                OpenVG.Lines ( 0, [ -60, 0 ], 'ON' )
                OpenVG.Lines ( 0, [ 0, -5 ], 'ON' )
                OpenVG.MoveTo( 0, [ 70, 0 ], 'ON' )
                OpenVG.Lines ( 0, [ 60, 0 ], 'ON' )
                OpenVG.Lines ( 0, [ 0, 5 ], 'ON' )
                OpenVG.Lines ( 0, [ -60, 0 ], 'ON' )
                OpenVG.Lines ( 0, [ 0, -5 ], 'ON' )
        OpenVG.AppendPathData( 0, 0 )

        # ---------------------------------------------------------------------- 
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( 0, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( 0, [ 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################        
class Gradient( vt.VisualTestCase ):
    
    '''
    This test is for testing linear and radial gradients.
    '''
    
    testValues = {  'StrokeCapStyle'  : [ 'VG_CAP_BUTT', 'VG_CAP_ROUND', 'VG_CAP_SQUARE' ],
                    'StrokeJoinStyle' : [ 'VG_JOIN_MITER', 'VG_JOIN_ROUND', 'VG_JOIN_BEVEL' ],
                    'GradientType'    : [ 'VG_PAINT_TYPE_LINEAR_GRADIENT', 'VG_PAINT_TYPE_RADIAL_GRADIENT' ],
                    'SpreadMode'      : [ 'VG_COLOR_RAMP_SPREAD_REPEAT', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'VG_COLOR_RAMP_SPREAD_PAD' ]
                    }
        
    def __init__( self, StrokeCapStyle, StrokeJoinStyle, GradientType, SpreadMode ):
        vt.VisualTestCase.__init__( self )
        self.StrokeCapStyle  = StrokeCapStyle
        self.StrokeJoinStyle = StrokeJoinStyle
        self.GradientType    = GradientType
        self.SpreadMode      = SpreadMode
        self.repeats         = 1
        self.name = 'OPENVG gradient %s %s %s %s' % ( StrokeCapStyle, StrokeJoinStyle, GradientType, SpreadMode )                
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
            
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )               
        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )
        OpenVG.Stroke( 40, self.StrokeCapStyle, self.StrokeJoinStyle, 4 )
                
        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint1Index )

        paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint2Index )
                       
        if self.GradientType == 'VG_PAINT_TYPE_RADIAL_GRADIENT':
            OpenVG.RadialGradientPaint( paint1Index, [ 80, 80 ], [ 50, 50 ], 100 )
                        
            ColorRamps = ( [ 0.000000, 0.000000, 0.999985, 0.000000,
                             1.000000, 0.099991, 0.000000, 0.999985,
                             0.000000, 1.000000, 0.999008, 0.501953,
                             0.000000, 0.501953, 1.000000, 1.000000,
                             0.000000, 0.000000, 0.000000, 1.000000 ]
                           )
        else:
            OpenVG.LinearGradientPaint( paint1Index, [ 60, 60 ], [ 30, 30 ] )
            
            ColorRamps = ( [ 0.000000, 0.000000, 0.999985,
                             0.000000, 1.000000, 0.099991,
                             0.000000, 0.999985, 0.000000,
                             1.000000, 0.999008, 0.501953,
                             0.000000, 0.501953, 1.000000 ] 
                           )
                
        OpenVG.MoveTo( pathIndex, [ 20, 20 ], 'OFF' )
        OpenVG.Lines( pathIndex, [ 0, 120 ], 'ON' )
        OpenVG.Lines( pathIndex, [ 120, 0 ], 'ON' )
        OpenVG.Lines( pathIndex, [ 0, -120 ], 'ON' )
        OpenVG.Lines( pathIndex, [ -120, 0 ], 'ON' )
                                
        OpenVG.ColorRamps( paint1Index, self.SpreadMode, 'ON', ColorRamps )
        OpenVG.PaintType( paint1Index, self.GradientType)
        
        OpenVG.PaintType( paint2Index, 'VG_PAINT_TYPE_COLOR' )
        OpenVG.PaintColor( paint2Index, [ 0.0, 0.0, 0.0, 100.0 / 256.0 ] )
                
        OpenVG.AppendPathData( pathIndex, pathDataIndex )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
                                
        OpenVG.SetPaint( paint1Index, [ 'VG_FILL_PATH' ] )
        OpenVG.SetPaint( paint2Index, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
