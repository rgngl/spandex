#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class AllpairsMasking( vt.VisualTestCase ):
    
    ''' This test is for testing functionality of masking and clearing of an
    image. 1.The Test creates an image1 which is filled with blue color
    ClearColor( [0,0,0.8,0.8]). 2. The buffer is Cleared. 3. The test creates
    and image2 and draws a yellow triangle inside of the red box. 4. The Lower
    halve of the image2 is masked with lighter shade of blue ClearColor(
    [0,0,0.8,0.4]). The type of masking is based on pre-defined allpair-testing
    table. ( 1. Masking, 2. MaskingType ).
    '''
    
    testValues = { ( 'Masking', 'Type' ) :
                       [ (  'NO', 'VG_SET_MASK' ),
                         ( 'YES', 'VG_CLEAR_MASK' ),
                         ( 'YES', 'VG_FILL_MASK' ),
                         ( 'YES', 'VG_SET_MASK' ),
                         ( 'YES', 'VG_UNION_MASK' ),
                         ( 'YES', 'VG_INTERSECT_MASK' ),
                         ( 'YES', 'VG_SUBTRACT_MASK' ) ]
                   }
    
    def __init__( self, Masking, Type ):
        vt.VisualTestCase.__init__( self )
        self.Masking = Masking
        self.Type    = Type
        self.repeats = 1               
        self.name = 'OPENVG masking and clearing %s %s' % ( Masking, Type, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()
        
        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, 0, 0 )

        ####### Image 1 - Empty image for masking purposes #################
        OpenVG.ImageMode( 'VG_DRAW_IMAGE_NORMAL' )

        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, 'VG_sRGBA_8888', WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData1Index, 'VG_sRGBA_8888', WIDTH, HEIGHT )

        OpenVG.ReadPixels( imageData1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageData1Index, image1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ClearColor( [ 0, 0, 0.8, 0.8 ] )
        
        if self.Masking == 'YES':
            OpenVG.Mask( image1Index, 'IMAGE', 'VG_SET_MASK', 0, 0, 200, 100 )
        
        ###################################################################
        OpenVG.Clear( 0, 0, 0, 0 )
        
        ####### Image 2 - box and triangle ################################
        OpenVG.ImageMode( 'VG_DRAW_IMAGE_NORMAL' )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint1Index )
        OpenVG.ColorPaint( paint1Index, [ 0.75, 0.0, 0, 0.7 ] )

        paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint2Index )
        OpenVG.ColorPaint( paint2Index, [ 0, 0.75, 0.0, 0.7 ] )

        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image2Index, 'VG_sRGBA_8888', WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageData2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData2Index, 'VG_sRGBA_8888', WIDTH, HEIGHT )

        if self.Masking == 'YES':
            OpenVG.Mask( image2Index, 'IMAGE', self.Type, 0, 0, 200, 100 )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.8, 0.4 ] )

        path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path1Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathData1Index, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path2Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathData2Index, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        
        OpenVG.MoveTo( pathData1Index, [ 30, 30 ], 'OFF' ) 
        OpenVG.Lines( pathData1Index, [ 0, 140 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ 140, 0 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ 0, -140 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ -140, 0 ], 'ON' )
        OpenVG.AppendPathData( path1Index, pathData1Index )
        
        OpenVG.SetPaint( paint1Index, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( path1Index, [ 'VG_STROKE_PATH' ] )
        
        OpenVG.MoveTo( pathData2Index, [ 60, 60 ], 'OFF' ) 
        OpenVG.Lines( pathData2Index, [ 40, 80 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ 40, -80 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ -80, 0 ], 'ON' )
        OpenVG.AppendPathData( path2Index, pathData2Index )
        
        OpenVG.SetPaint( paint2Index, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( path2Index, [ 'VG_FILL_PATH' ] )
        OpenVG.ReadPixels( imageData2Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageData2Index, image2Index, 0, 0, WIDTH, HEIGHT )
       
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
 
        if self.Masking == 'YES':
            OpenVG.Masking( 'ON' )

        OpenVG.DrawImage( image2Index )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################x
class AllpairsScissoring( vt.VisualTestCase ):
    
    ''' This test is for testing functionality of scissoring. 1. The test draws
    tiger with black and white rectangles. 2. The image is scissored using
    different amounts of scissoring rects.
    '''
    
    testValues = { ( 'Scissoring', 'NBR' ) :
                       [ (  'NO',  '0', 'VG_sRGBX_8888' ),
                         ( 'YES', '0',  'VG_sRGBX_8888' ),
                         ( 'YES', '1',  'VG_sRGBX_8888' ),                         
                         ( 'YES', '32', 'VG_sRGBX_8888' ),
                         ( 'YES', '33', 'VG_sRGBX_8888' ) ],
                   'ImageFormat' : [ 'VG_sRGBX_8888' ]
                   }
    
    def __init__( self, Scissoring, NBR, ImageFormat ):
        vt.VisualTestCase.__init__( self )
        self.Scissoring  = Scissoring
        self.NBR         = NBR
        self.ImageFormat = ImageFormat
        self.repeats     = 1
        self.name = 'OPENVG scissor rects %s %s %s' % ( Scissoring, NBR, ImageFormat, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, 0, 0 )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.ImageFormat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.ImageFormat, WIDTH, HEIGHT ) 
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageDataIndex, imageIndex, 0, 0, WIDTH, HEIGHT )       
        
        ####### ScissorRects #########
        if self.NBR == '32':
            scissors = [ 20,  20,  15,  25,
                         40,  20,  15,  25,
                         60,  20,  15,  25,
                         80,  20,  15,  25,
                         100, 20,  15,  25,
                         120, 20,  15,  25,
                         140, 20,  15,  25,
                         160, 20,  15,  25,
                         20,  60,  15,  25,
                         40,  60,  15,  25,
                         60,  60,  15,  25,
                         80,  60,  15,  25,
                         100, 60,  15,  25,
                         120, 60,  15,  25,
                         140, 60,  15,  25,
                         160, 60,  15,  25,
                         20,  100, 15,  25,
                         40,  100, 15,  25,
                         60,  100, 15,  25,
                         80,  100, 15,  25,
                         100, 100, 15,  25,
                         120, 100, 15,  25,
                         140, 100, 15,  25,
                         160, 100, 15,  25,
                         20,  140, 15,  25,
                         40,  140, 15,  25,
                         60,  140, 15,  25,
                         80,  140, 15,  25,
                         100, 140, 15,  25,
                         120, 140, 15,  25,
                         140, 140, 15,  25,
                         160, 140, 15,  25 ]
        elif self.NBR == '33':
            OpenVG.CheckValue( 'VG_MAX_SCISSOR_RECTS', 'SCT_GEQUAL', [ 33 ] )
            scissors = [ 20,  20,  15,  25,
                         40,  20,  15,  25,
                         60,  20,  15,  25,
                         80,  20,  15,  25,
                         100, 20,  15,  25,
                         120, 20,  15,  25,
                         140, 20,  15,  25,
                         160, 20,  15,  25,
                         20,  60,  15,  25,
                         40,  60,  15,  25,
                         60,  60,  15,  25,
                         80,  60,  15,  25,
                         100, 60,  15,  25,
                         120, 60,  15,  25,
                         140, 60,  15,  25,
                         160, 60,  15,  25,
                         20,  100, 15,  25,
                         40,  100, 15,  25,
                         60,  100, 15,  25,
                         80,  100, 15,  25,
                         100, 100, 15,  25,
                         120, 100, 15,  25,
                         140, 100, 15,  25,
                         160, 100, 15,  25,
                         20,  140, 15,  25,
                         40,  140, 15,  25,
                         60,  140, 15,  25,
                         80,  140, 15,  25,
                         100, 140, 15,  25,
                         120, 140, 15,  25,
                         140, 140, 15,  25,
                         160, 140, 15,  25,
                         20,  170, 15,  20 ]
        elif self.NBR == '0':
                 scissors = [ 0, 0, 0, 0 ]
        elif self.NBR == '1':
                 scissors = [ 0, 0, WIDTH / 2, HEIGHT / 2 ]
                 
        OpenVG.ScissorRects( scissors )

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )

        if self.Scissoring == 'YES':
            OpenVG.Scissoring( 'ON' )
        else:
            OpenVG.Scissoring( 'OFF' )

        OpenVG.DrawImage( imageIndex )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        

     
        
