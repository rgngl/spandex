# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from lib.egl    import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class Extensions( vt.VisualTestCase ):

        ''' The purpose of the test: test OpenVG extensions.

        Expected output: surface cleared to green if the particular OpenVG
        extension is supported.
        '''
        
        testValues = { ( 'extension', 'functions' ) : [ ( 'VG_KHR_EGL_image', [ 'vgCreateEGLImageTargetKHR' ] ),
                                                        ]
                       }
                                                                             
        def __init__( self, extension, functions ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.extension = extension
                self.functions = functions
                self.name    = "OPENVG extensions, extension=%s" % ( self.extension, )
        
        def build( self, target, modules ):       
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]               

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()               

                OpenVG.CheckExtension( self.extension )
                
                for f in self.functions:
                        OpenVG.CheckFunction( f )

                OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )        
                OpenVG.Clear( 0, 0, 0, 0 )
        
                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
