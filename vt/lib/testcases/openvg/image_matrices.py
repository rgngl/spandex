#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class ImageUserToSurfaceTranslateRotateShearScale( vt.VisualTestCase ):
    
    ''' Test for image matrix. Test draws tiger with black and white rectangles,
    reads surface content to an image and clears the surface. Image is then
    translated [100,100], rotated [35], scaled [0.8,0.6] and sheared [0.25,0.25]
    using VG_MATRIX_IMAGE_USER_TO_SURFACE, and finally drawn to the surface.
    '''

    testValues = { 'quality'      : [ 'VG_IMAGE_QUALITY_NONANTIALIASED',
                                      'VG_IMAGE_QUALITY_FASTER',
                                      'VG_IMAGE_QUALITY_BETTER' ],
                   'imageformat'  : [ 'VG_sRGBX_8888',
                                      'VG_sRGBA_8888',
                                      'VG_sRGBA_8888_PRE',
                                      'VG_sRGB_565',
                                      'VG_sRGBA_5551',
                                      'VG_sRGBA_4444',
                                      'VG_sL_8',
                                      'VG_lRGBX_8888',
                                      'VG_lRGBA_8888',
                                      'VG_lRGBA_8888_PRE',
                                      'VG_lL_8',
                                      'VG_A_8',
                                      'VG_BW_1' ]
                   }

    def __init__( self, quality, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.quality     = quality
        self.imageformat = imageformat
        self.repeats     = 1
        self.name = 'OPENVG image matrix manipulation %s %s' % ( imageformat, quality, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ImageQuality( self.quality )
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.imageformat, WIDTH, HEIGHT )
        
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageDataIndex, imageIndex, 0, 0, WIDTH, HEIGHT )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        OpenVG.Translate( 100, 100 )
        OpenVG.Rotate( 35 )
        OpenVG.Scale( 0.8, 0.6 )
        OpenVG.Shear( 0.25, 0.25 )
        OpenVG.Translate( -100, -100 )
        OpenVG.DrawImage( imageIndex )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

