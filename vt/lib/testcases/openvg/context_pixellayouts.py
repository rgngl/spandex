#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class PixelLayout( vt.VisualTestCase ):
    
    ''' Test for pixel layouts. Test draws tiger with black and white rectangles
    with different pixel layouts and image formats.
    '''

    testValues = { 'pixellayout' : [ 'VG_PIXEL_LAYOUT_UNKNOWN',
                                     'VG_PIXEL_LAYOUT_RGB_VERTICAL',
                                     'VG_PIXEL_LAYOUT_BGR_VERTICAL',
                                     'VG_PIXEL_LAYOUT_RGB_HORIZONTAL',
                                     'VG_PIXEL_LAYOUT_BGR_HORIZONTAL' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }
                   
    def __init__( self, pixellayout, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.pixellayout = pixellayout
        self.imageformat = imageformat
        self.repeats     = 1        
        self.name = 'OPENVG pixel layout %s in %s image format' % ( pixellayout, imageformat, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.PixelLayout( self.pixellayout )
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )        
        OpenVG.CreateImage( imageIndex, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.imageformat, WIDTH, HEIGHT ) 
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageDataIndex, imageIndex, 0, 0, WIDTH, HEIGHT )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( imageIndex )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

