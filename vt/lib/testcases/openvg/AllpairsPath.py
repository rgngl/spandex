#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class AllpairsPathHandlingFilledLines( vt.VisualTestCase ):
    
    '''
    This test is for testing filled lines. Test draws three vertical lines
    on top of three horizontal lines. The following functions/parameters are
    changing based on predefined allpair-testing table.(1. APPEND_PATH,
    2. MODIFY_PATH_COORDS, 3. TRANSFORM_PATH, 4. DATATYPE ).
    '''
    
    testValues = { ( 'APPEND', 'MODIFY_COORDS', 'TRANSFORM', 'DATATYPE' ) :
                       [ (  'NO', 'YES',  'NO', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'YES', 'YES',  'NO', 'VG_PATH_DATATYPE_S_32' ),
                         (  'NO',  'NO', 'YES', 'VG_PATH_DATATYPE_S_32' ),
                         ( 'YES',  'NO',  'NO', 'VG_PATH_DATATYPE_F' ) ]
                   }
    
    def __init__( self, APPEND, MODIFY_COORDS, TRANSFORM, DATATYPE ):
        vt.VisualTestCase.__init__( self )
        self.APPEND        = APPEND
        self.MODIFY_COORDS = MODIFY_COORDS
        self.TRANSFORM     = TRANSFORM
        self.DATATYPE      = DATATYPE
        self.repeats       = 1
        self.name = 'OPENVG path handling functions (filled lines) %s %s %s %s' % ( APPEND,
                                                                                    MODIFY_COORDS,
                                                                                    TRANSFORM,
                                                                                    DATATYPE, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.25, 0.0, 0.75, 1.0 ] )
        OpenVG.Stroke( 2, 'VG_CAP_ROUND', 'VG_JOIN_BEVEL', 0 )

        path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
        OpenVG.CreatePath( path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData1Index, self.DATATYPE, 1, 1 )

        path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData2Index, self.DATATYPE, 1, 1 )

        path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData3Index, self.DATATYPE, 1, 1 )

        path4Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path4Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData4Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData4Index, self.DATATYPE, 1, 1 )

        path5Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path5Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData5Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData5Index, self.DATATYPE, 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.5, 0.1, 0.1, 1.0 ] )

        ###### HORIZONTAL ############
        OpenVG.MoveTo( pathData1Index, [ 30, 50 ], 'OFF')
        OpenVG.Lines( pathData1Index, [ 140, 0 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ 0, 20 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ -140, 0 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ 0, -20 ], 'ON' )
        
        OpenVG.MoveTo( pathData1Index, [ 30, 90 ], 'OFF')
        OpenVG.Lines( pathData1Index, [ 140, 0 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ 0, 20 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ -140, 0 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ 0, -20 ], 'ON' )

        OpenVG.MoveTo( pathData1Index, [ 30, 130 ], 'OFF')
        OpenVG.Lines( pathData1Index, [ 140, 0 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ 0, 20 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ -140, 0 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ 0, -20 ], 'ON' )
        ##############################

        ###### VERTICAL ##############
        OpenVG.MoveTo( pathData2Index, [ 50, 30 ], 'OFF')
        OpenVG.Lines( pathData2Index, [ 0, 140 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ 20, 0 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ 0, -140 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ -20, 0 ], 'ON' )
        
        OpenVG.MoveTo( pathData2Index, [ 90, 30 ], 'OFF')
        OpenVG.Lines( pathData2Index, [ 0, 140 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ 20, 0 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ 0, -140 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ -20, 0 ], 'ON' )

        OpenVG.MoveTo( pathData2Index, [ 130, 30 ], 'OFF')
        OpenVG.Lines( pathData2Index, [ 0, 140 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ 20, 0 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ 0, -140 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ -20, 0 ], 'ON' ) 
        ##############################

        ###### HORIZONTAL ############
        OpenVG.MoveTo( pathData3Index, [ 15, 25 ], 'OFF')
        OpenVG.Lines( pathData3Index, [ 70, 0 ], 'ON' )
        OpenVG.Lines( pathData3Index, [ 0, 10 ], 'ON' )
        OpenVG.Lines( pathData3Index, [ -70, 0 ], 'ON' )
        OpenVG.Lines( pathData3Index, [ 0, -10 ], 'ON' )
        
        OpenVG.MoveTo( pathData3Index, [ 15, 45 ], 'OFF')
        OpenVG.Lines( pathData3Index, [ 70, 0 ], 'ON' )
        OpenVG.Lines( pathData3Index, [ 0, 10 ], 'ON' )
        OpenVG.Lines( pathData3Index, [ -70, 0 ], 'ON' )
        OpenVG.Lines( pathData3Index, [ 0, -10 ], 'ON' )

        OpenVG.MoveTo( pathData3Index, [ 15, 65 ], 'OFF')
        OpenVG.Lines( pathData3Index, [ 70, 0 ], 'ON' )
        OpenVG.Lines( pathData3Index, [ 0, 10 ], 'ON' )
        OpenVG.Lines( pathData3Index, [ -70, 0 ], 'ON' )
        OpenVG.Lines( pathData3Index, [ 0, -10 ], 'ON' )
        ##############################

        ###### VERTICAL ##############
        OpenVG.MoveTo( pathData4Index, [ 25, 15 ], 'OFF')
        OpenVG.Lines( pathData4Index, [ 0, 70 ], 'ON' )
        OpenVG.Lines( pathData4Index, [ 10, 0 ], 'ON' )
        OpenVG.Lines( pathData4Index, [ 0, -70 ], 'ON' )
        OpenVG.Lines( pathData4Index, [ -10, 0 ], 'ON' )
        
        OpenVG.MoveTo( pathData4Index, [ 45, 15 ], 'OFF')
        OpenVG.Lines( pathData4Index, [ 0, 70 ], 'ON' )
        OpenVG.Lines( pathData4Index, [ 10, 0 ], 'ON' )
        OpenVG.Lines( pathData4Index, [ 0, -70 ], 'ON' )
        OpenVG.Lines( pathData4Index, [ -10, 0 ], 'ON' )

        OpenVG.MoveTo( pathData4Index, [ 65, 15 ], 'OFF')
        OpenVG.Lines( pathData4Index, [ 0, 70 ], 'ON' )
        OpenVG.Lines( pathData4Index, [ 10, 0 ], 'ON' )
        OpenVG.Lines( pathData4Index, [ 0, -70 ], 'ON' )
        OpenVG.Lines( pathData4Index, [ -10, 0 ], 'ON' ) 
        #############################
                
        OpenVG.AppendPathData( path1Index, pathData1Index )
        OpenVG.AppendPathData( path2Index, pathData2Index )
        OpenVG.AppendPathData( path3Index, pathData3Index)
        OpenVG.AppendPathData( path4Index, pathData4Index)
        OpenVG.AppendPathData( path5Index, pathData1Index )
        OpenVG.AppendPathData( path5Index, pathData2Index )
              
        if self.MODIFY_COORDS == 'YES':
            OpenVG.ClearPath( path5Index, [ 'VG_PATH_CAPABILITY_ALL' ] )
            
            OpenVG.ModifyPathCoords(path1Index, pathData3Index, 0, 0 )
            OpenVG.ModifyPathCoords(path2Index, pathData4Index, 0, 0 )
            OpenVG.AppendPath( path5Index, pathData1Index )
            OpenVG.AppendPath( path5Index, pathData2Index )
            
        if self.APPEND == 'YES':
            OpenVG.ClearPath( path5Index, [ 'VG_PATH_CAPABILITY_ALL' ] )
            OpenVG.AppendPath( path2Index, pathData1Index )
            OpenVG.AppendPath( path5Index, pathData2Index )

        if self.TRANSFORM == 'YES':
            OpenVG.TransformPath( path5Index, path5Index )

        # ----------------------------------------------------------------------            
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( path5Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
   
######################################################################        
class AllpairsPathHandlingStrokedLines( vt.VisualTestCase ):
    
    '''
    This test is for testing stroked lines. Test draws three vertical lines
    on top of three horizontal lines. The following functions/parameters are
    changing based on predefined allpair-testing table.(1. APPEND_PATH,
    2. MODIFY_PATH_COORDS, 3. TRANSFORM_PATH, 4. DATATYPE ).
    '''
    
    testValues = { ( 'APPEND', 'MODIFY_COORDS', 'TRANSFORM', 'DATATYPE') :
                       [ (  'NO', 'YES',  'NO', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'YES', 'YES',  'NO', 'VG_PATH_DATATYPE_S_32' ),
                         (  'NO',  'NO', 'YES', 'VG_PATH_DATATYPE_S_32' ),
                         ( 'YES',  'NO',  'NO', 'VG_PATH_DATATYPE_F' ) ]
                   }
    
    def __init__( self, APPEND, MODIFY_COORDS, TRANSFORM, DATATYPE ):
        vt.VisualTestCase.__init__( self )
        self.APPEND        = APPEND
        self.MODIFY_COORDS = MODIFY_COORDS
        self.TRANSFORM     = TRANSFORM
        self.DATATYPE      = DATATYPE
        self.repeats       = 1
        self.name = 'OPENVG path handling functions (stroked lines) %s %s %s %s' % ( APPEND,
                                                                                     MODIFY_COORDS,
                                                                                     TRANSFORM,
                                                                                     DATATYPE, )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 5, 'VG_CAP_ROUND', 'VG_JOIN_BEVEL', 0 )

        path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData1Index, self.DATATYPE, 1, 1 )

        path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData2Index, self.DATATYPE, 1, 1 )

        path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData3Index, self.DATATYPE, 1, 1 )

        path4Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path4Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData4Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData4Index, self.DATATYPE, 1, 1 )

        path5Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path5Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData5Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData5Index, self.DATATYPE, 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.5, 0.1, 0.1, 1.0 ] )

        ######## VERTICAL ############
        OpenVG.MoveTo( pathData1Index, [ 20, 40 ], 'OFF')
        OpenVG.Lines( pathData1Index, [ 80, 0 ], 'ON' )
        
        OpenVG.MoveTo( pathData1Index, [ 20, 60 ], 'OFF')
        OpenVG.Lines( pathData1Index, [ 80, 0 ], 'ON' )

        OpenVG.MoveTo( pathData1Index, [ 20, 80 ], 'OFF')
        OpenVG.Lines( pathData1Index, [ 80, 0 ], 'ON' )
        ##############################

        ####### HORIZONTAL ###########
        OpenVG.MoveTo( pathData2Index, [ 40, 20 ], 'OFF')
        OpenVG.Lines( pathData2Index, [ 0, 80 ], 'ON' )
        
        OpenVG.MoveTo( pathData2Index, [ 60, 20 ], 'OFF')
        OpenVG.Lines( pathData2Index, [ 0, 80 ], 'ON' )

        OpenVG.MoveTo( pathData2Index, [ 80, 20 ], 'OFF')
        OpenVG.Lines( pathData2Index, [ 0, 80 ], 'ON' )
        ##############################

        ####### VERTICAL #############
        OpenVG.MoveTo( pathData3Index, [ 80, 100 ], 'OFF')
        OpenVG.Lines( pathData3Index, [ 80, 0 ], 'ON' )
        
        OpenVG.MoveTo( pathData3Index, [ 80, 120 ], 'OFF')
        OpenVG.Lines( pathData3Index, [ 80, 0 ], 'ON' )

        OpenVG.MoveTo( pathData3Index, [ 80, 140 ], 'OFF')
        OpenVG.Lines( pathData3Index, [ 80, 0 ], 'ON' )
        ##############################

        ###### HORIZONTAL ############
        OpenVG.MoveTo( pathData4Index, [ 100, 80 ], 'OFF')
        OpenVG.Lines( pathData4Index, [ 0, 80 ], 'ON' )
        
        OpenVG.MoveTo( pathData4Index, [ 120, 80 ], 'OFF')
        OpenVG.Lines( pathData4Index, [ 0, 80 ], 'ON' )

        OpenVG.MoveTo( pathData4Index, [ 140, 80 ], 'OFF')
        OpenVG.Lines( pathData4Index, [ 0, 80 ], 'ON' )
        ##############################

        OpenVG.AppendPathData( path1Index, pathData1Index )
        OpenVG.AppendPathData( path2Index, pathData2Index )
        OpenVG.AppendPathData( path3Index, pathData3Index )
        OpenVG.AppendPathData( path4Index, pathData4Index )
        OpenVG.AppendPathData( path5Index, pathData1Index )
        OpenVG.AppendPathData( path5Index, pathData2Index )
            
        if self.MODIFY_COORDS == 'YES':
            OpenVG.ClearPath( path5Index, [ 'VG_PATH_CAPABILITY_ALL' ] )
            OpenVG.ModifyPathCoords( path3Index, pathData1Index, 0, 0 )
            OpenVG.ModifyPathCoords( path4Index, pathData2Index, 0, 0 )
            OpenVG.AppendPathData( path5Index, pathData3Index )
            OpenVG.AppendPathData( path5Index, pathData4Index )
            
        if self.APPEND == 'YES':
            OpenVG.ClearPath( path5Index, [ 'VG_PATH_CAPABILITY_ALL' ] )
            if self.MODIFY_COORDS == 'YES':
                OpenVG.AppendPath( path2Index, path1Index )
                OpenVG.AppendPath( path5Index, path2Index )
            else:
                OpenVG.AppendPath( path4Index, path3Index )
                OpenVG.AppendPath( path5Index, path4Index )
        
        if self.TRANSFORM == 'YES':
            OpenVG.TransformPath( path5Index, path5Index )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( path5Index, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPathHandlingInterpolation( vt.VisualTestCase ):
    
    '''
    This test is for testing path interpolation on stroked lines. This test
    draws a green horizontal line (Path1) on red vertical line (Path0) forming a
    viewfinder. Then Path0 and Path1 are interpolated to path2 which is finally
    drawn on to the black surface.
    '''
    
    testValues = { ( 'DATATYPE' ) : [ ( 'VG_PATH_DATATYPE_S_16' ),
                                      ( 'VG_PATH_DATATYPE_S_32' ),
                                      ( 'VG_PATH_DATATYPE_F' ) ]
                   }
    
    def __init__( self, DATATYPE):
        vt.VisualTestCase.__init__( self )
        self.DATATYPE = DATATYPE        
        self.repeats  = 1
        self.name = 'OPENVG path handling functions (interpolating stroked lines) %s' % ( DATATYPE, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
                
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 5, 'VG_CAP_ROUND', 'VG_JOIN_BEVEL', 0 )

        path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData1Index, self.DATATYPE, 1, 1 )

        path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData2Index, self.DATATYPE, 1, 1 )

        path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData3Index, self.DATATYPE, 1, 1 )

        paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint1Index )
        
        OpenVG.ColorPaint( paint1Index, [ 0.5, 0.1, 0.1, 1.0 ] )

        ####### vertical line ##########
        OpenVG.MoveTo( pathData1Index, [ 100, 75 ], 'OFF')
        OpenVG.Lines( pathData1Index, [ 0, 50 ], 'ON' )
        ##############################

        paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint2Index )
        OpenVG.ColorPaint( paint2Index, [ 0.0, 0.5, 0.25, 1.0 ] )
        
        ####### Horizontal line ########
        OpenVG.MoveTo( pathData2Index, [ 75, 100 ], 'OFF')
        OpenVG.Lines( pathData2Index, [ 50, 0 ], 'ON' )
        ##############################
        
        OpenVG.AppendPathData( path1Index, pathData1Index )
        OpenVG.AppendPathData( path2Index, pathData2Index )

        paint3Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint3Index )
        OpenVG.ColorPaint( paint3Index, [ 0.0, 0.25, 0.5, 1.0 ] )
        
        OpenVG.InterpolatePath( path3Index, path1Index, path2Index, 2 )

        paths  = []
        paints = []

        paths.append( path1Index )
        paths.append( path2Index )
        paths.append( path3Index )

        paints.append( paint1Index )
        paints.append( paint2Index )
        paints.append( paint3Index )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 3 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPathHandlingInterpolation2( vt.VisualTestCase ):
    
    '''
    This test is for testing path interpolation on filled lines. This test
    draws a green horizontal filled line (Path1) on red vertical line (Path0)
    forming a viewfinder. Then Path0 and Path1 are interpolated to path2 which
    is finally drawn on to the black surface.
    '''
    
    testValues = { ( 'DATATYPE' ) : [ ( 'VG_PATH_DATATYPE_S_16' ),
                                      ( 'VG_PATH_DATATYPE_S_32' ),
                                      ( 'VG_PATH_DATATYPE_F' ) ]
                   }
    
    def __init__( self, DATATYPE):
        vt.VisualTestCase.__init__( self )
        self.DATATYPE = DATATYPE
        self.repeats  = 1        
        self.name = 'OPENVG path handling functions (interpolating filled lines) %s' % ( DATATYPE, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
               
        OpenVG.ClearColor( [0.0,0.0,0.0,1.0] )
        OpenVG.Stroke( 2, 'VG_CAP_ROUND', 'VG_JOIN_BEVEL', 0 )

        path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData1Index, self.DATATYPE, 1, 1 )

        path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData2Index, self.DATATYPE, 1, 1 )

        path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        pathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
       	OpenVG.CreatePathData( pathData3Index, self.DATATYPE, 1, 1 )

        paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint1Index )
        OpenVG.ColorPaint( paint1Index, [ 0.5, 0.1, 0.1, 1.0 ] )

        ####### vertical line ##########
        OpenVG.MoveTo( pathData1Index, [ 100, 70 ], 'OFF')
        OpenVG.Lines( pathData1Index, [ 0, 50 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ -10, 0 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ 0, -50 ], 'ON' )
        OpenVG.Lines( pathData1Index, [ 10, 0 ], 'ON' )
        ##############################

        paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint2Index )
        OpenVG.ColorPaint( paint2Index, [ 0.0, 0.5, 0.25, 0.75 ] )
        
        ####### Horizontal line ########
        OpenVG.MoveTo( pathData2Index, [ 70, 100 ], 'OFF')
        OpenVG.Lines( pathData2Index, [ 50, 0 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ 0, -10 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ -50, 0 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ 0, 10 ], 'ON' )
        ##############################
        
        OpenVG.AppendPathData( path1Index, pathData1Index )
        OpenVG.AppendPathData( path2Index, path2Index )

        paint3Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint3Index )
        OpenVG.ColorPaint( paint3Index, [ 0.0, 0.0, 1, 0.5 ] )
        
        OpenVG.InterpolatePath( path3Index, path1Index, path2Index, 2 )

        paths  = []
        paints = []

        paths.append( path1Index )
        paths.append( path2Index )
        paths.append( path3Index )

        paints.append( paint1Index )
        paints.append( paint2Index )
        paints.append( paint3Index )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 3 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_FILL_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

