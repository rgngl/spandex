#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class AllpairsRenQHandleImage( vt.VisualTestCase ):
    
    ''' This test is for testing image VG_MATRIX_IMAGE_TO_SURFACE. These are the
    steps of the test. 1. The test creates image0. The test draws a tiger with
    black and white rectangles which is pasted to image0.  2. The test set
    matrixmode to VG_MATRIX_IMAGE_USER_TO_SURFACE and translate the
    image. 3. Test either scale, rotates or shears the image if the function is
    enabled. 4. The test draws a image 0. Following functions/parameters are
    changing based on predefined allpair-testing table.(1. ImageQuality,
    2. Format, 3. Scale(0.5), 4. Shear(0.5), 5. Rotate(35).
    '''

    testValues = { ( 'Quality', 'Format', 'Scale', 'Shear', 'Rotate' ) :
                       [ ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_sRGBX_8888',     'YES', 'YES', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_sRGBX_8888',      'NO',  'NO',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_sRGBA_8888',     'YES',  'NO', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_sRGBA_8888',      'NO', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_sRGBA_8888_PRE', 'YES',  'NO',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_sRGBA_8888_PRE',  'NO', 'YES', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_sRGB_565',        'NO',  'NO',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_sRGB_565',       'YES', 'YES', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_sRGBA_5551',      'NO', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_sRGBA_5551',     'YES',  'NO', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_sRGBA_4444',      'NO', 'YES', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_sRGBA_4444',     'YES',  'NO',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_sL_8',           'YES',  'NO',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_sL_8',            'NO', 'YES', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_lRGBX_8888',     'YES', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_lRGBX_8888',      'NO', 'YES', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_lRGBA_8888',      'NO',  'NO', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_lRGBA_8888',     'YES', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_lRGBA_8888_PRE',  'NO',  'NO', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_lRGBA_8888_PRE', 'YES', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_lL_8',           'YES', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_lL_8',            'NO',  'NO', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_A_8',             'NO', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_A_8',             'NO', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_BW_1',            'NO', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_BW_1',           'YES',  'NO', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_sRGBX_8888',      'NO', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_sRGBA_8888',     'YES',  'NO', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_sRGBA_8888_PRE', 'YES', 'YES', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_sRGBA_8888_PRE',  'NO',  'NO',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_sRGB_565',        'NO',  'NO',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_sRGBA_5551',     'YES', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_sRGBA_4444',      'NO',  'NO',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_sL_8',           'YES', 'YES',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_lRGBX_8888',     'YES', 'YES', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_lRGBA_8888',      'NO', 'YES', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_lRGBA_8888_PRE',  'NO',  'NO',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_BETTER',         'VG_lL_8',           'YES',  'NO', 'YES' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_A_8',            'YES',  'NO',  'NO' ),
                         ( 'VG_IMAGE_QUALITY_FASTER',         'VG_BW_1',            'NO', 'YES', 'YES' ) ]
                   }

    def __init__( self, Quality, Format, Scale, Shear, Rotate ):
        vt.VisualTestCase.__init__( self )
        self.Quality = Quality
        self.Format  = Format
        self.Scale   = Scale
        self.Shear   = Shear
        self.Rotate  = Rotate
        self.repeats = 1        
        self.name = 'OPENVG VG_MATRIX_IMAGE_TO_SURFACE %s %s %s %s %s' % ( Quality, Format, Scale, Shear, Rotate, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ImageQuality( self.Quality )
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.Format, WIDTH, HEIGHT, [ self.Quality ] )
        
        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.Format, WIDTH, HEIGHT ) 
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageDataIndex, imageIndex, 0, 0, WIDTH, HEIGHT )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        OpenVG.Translate( 100, 100 )
        if self.Rotate == 'YES':
            OpenVG.Rotate( 35 )
        if self.Scale == 'YES':
            OpenVG.Scale( 0.8, 0.6 )
        if self.Shear == 'YES':
            OpenVG.Shear( 0.25, 0.25 )
        OpenVG.Translate( -100, -100 )
        OpenVG.DrawImage( imageIndex )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class AllpairsRenQHandleFillPath( vt.VisualTestCase ):
    
    '''
    This test is for testing VG_MATRIX_FILL_PAINT_TO_USER. Test draws a box
    filled with linear gradient paint (VG_COLOR_RAMP_REFLECT). The filling is
    then altered by matrices. These are steps of the test. 1. The test defines
    pathtype and stroketype and set the matrixmode to
    VG_MATRIX_FILL_PAINT_TO_USER. 2. The test defines linear gradient paint with
    the colorramp 3. The test draws a box which is then filled with the
    previously defined linear gradient. 4. The color is altered by rotating,
    shearing, translating or scaling if the funtion is enabled. 5. The box is
    drawn. Following functions/parameters are changing based on predifined
    allpair-testing table. 1. Translate(50,50), 2. Scale(0.5), 3. Shear(0.5),
    4. Rotate(90).
    '''

    testValues = { ( 'Translate', 'Scale', 'Shear', 'Rotate' ) :
                       [ ( 'YES', 'YES', 'YES', 'YES' ),
                         (  'NO',  'NO',  'NO',  'NO' ),
                         (  'NO', 'YES',  'NO', 'YES' ),
                         ( 'YES',  'NO', 'YES',  'NO' ),
                         ( 'YES',  'NO',  'NO', 'YES' ),
                         (  'NO', 'YES', 'YES',  'NO' ),
                         (  'NO',  'NO', 'YES',  'NO' ),
                         ( 'YES', 'YES',  'NO', 'YES' ) ]
                   }
    
    def __init__( self, Translate, Scale, Shear, Rotate ):
        vt.VisualTestCase.__init__( self )
        self.Translate = Translate
        self.Scale     = Scale
        self.Shear     = Shear
        self.Rotate    = Rotate
        self.repeats   = 1
        self.name = 'OPENVG VG_MATRIX_FILL_PAINT_TO_USER %s %s %s %s' % ( Translate, Scale, Shear, Rotate, )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0, 0.0, 0.75 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
        OpenVG.MatrixMode( 'VG_MATRIX_FILL_PAINT_TO_USER' )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,  'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 60, 60 ], [ 125, 125 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REFLECT',
                           'ON',
                           [ 0.0, 1.0, 0.0, 0.0, 1.0,
                             0.5, 0.0, 1.0, 0.0, 0.5,
                             1.0, 0.0, 0.0, 1.0, 1.0 ] )

        OpenVG.MoveTo( pathDataIndex, [ 20, 20 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ 160, 0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ 0, 160 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ -160, 0 ], 'ON' )
        OpenVG.Lines( pathDataIndex ,[ 0, -160 ], 'ON' )

        if self.Scale == 'YES' :
            OpenVG.Scale( 0.5, 0.5 )
        if self.Rotate == 'YES':
            OpenVG.Rotate( 90 )
        if self.Shear == 'YES':
            OpenVG.Shear( 0.5, 0.5 )
        if self.Translate == 'YES':
            OpenVG.Translate( 50, 50 )
             
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
             
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


