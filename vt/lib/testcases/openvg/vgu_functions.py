#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

import random

WIDTH  = 200
HEIGHT = 200

######################################################################
class Line( vt.VisualTestCase ):
        
    '''
    The purpose of the test: This test is for testing vgu modules line
    function.
        
    The test steps:
    Test defines x and y starting and ending points for 18 lines.
    Test defines colors for 18 lines.
    Test strokes lines. (VGJoinStyle and VGCapStyle and Alpha value from testValues).
        
    Expected output: 18 lines forms a grid to the surface.
    '''
    
    testValues = { 'AlphaValue' : [ 0.33, 0.66, 1.0 ], 
                   'VGJoinStyle': [ 'VG_JOIN_MITER', 'VG_JOIN_ROUND', 'VG_JOIN_BEVEL' ],
                   'VGCapStyle' : [ 'VG_CAP_BUTT', 'VG_CAP_ROUND', 'VG_CAP_SQUARE' ]
                   }

    def __init__( self, AlphaValue, VGJoinStyle, VGCapStyle ):
        vt.VisualTestCase.__init__( self )
        self.AlphaValue  = AlphaValue
        self.VGJoinStyle = VGJoinStyle
        self.VGCapStyle  = VGCapStyle
        self.repeats     = 1  
        self.name = 'OPENVG vgu stroked line %s %s %s' % ( AlphaValue, VGJoinStyle, VGCapStyle, )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]           
        Vgu    = modules[ 'Vgu' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 10, self.VGCapStyle, self.VGJoinStyle, 0 )
                
        StartPointX = []
        StartPointY = []
        EndPointX   = []
        EndPointY   = []
                
        StartPointX.extend( [ 20, 40, 60, 80, 100, 120, 140, 160, 180, 20, 20, 20, 20, 20, 20, 20, 20, 20 ] )
        StartPointY.extend( [ 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 40, 60, 80, 100, 120, 140, 160, 180 ] )
        EndPointX.extend( [ 20, 40, 60, 80, 100, 120, 140, 160, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180 ] )
        EndPointY.extend( [ 180, 180, 180, 180, 180, 180, 180, 180, 180, 20, 40, 60, 80, 100, 120, 140, 160, 180 ] )
        
        RedArray   = []
        GreenArray = []
        BlueArray  = []
        
        RedArray.extend( [ 0.25, 0.5, 0.75, 0.2, 0.4, 0.7, 0.9, 0.2, 0.4, 0.6, 0.4 ,0.5, 0.7, 0.9, 0.3, 0.6, 0.4, 0.5 ] )
        BlueArray.extend( [ 0.5, 0.25, 0.75, 0.4, 0.2, 0.6, 0.5, 0.7, 0.4, 0.2, 0.5 ,0.0, 0.1, 0.8, 0.4, 0.1, 0.8, 0.5 ] )
        GreenArray.extend( [ 0.25, 0.9, 0.75, 0.0, 0.4, 0.5, 0.7, 0.2, 0.4, 0.6, 0.4 ,0.2, 0.7, 0.9, 0.5, 0.6, 0.4, 0.5 ] )               

        paths  = []
        paints = []
        
        for i in range( len( StartPointX ) ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )    
            OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
            
            Vgu.Line( pathIndex, StartPointX[ i ], StartPointY[ i ], EndPointX[ i ], EndPointY[ i ] )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )    
            OpenVG.CreatePaint( paintIndex )
            
            red   = RedArray[ i ]
            green = GreenArray[ i ]
            blue  = BlueArray[ i ]
            alpha = self.AlphaValue
            OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )

            paths.append( pathIndex )
            paints.append( paintIndex )
                
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
                
        for i in range( len( StartPointX ) ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class DashedLine( vt.VisualTestCase ):

    '''
    The purpose of the test: This test is for testing vgu modules line function
        
    The test steps:
    Test defines x and y starting and ending points for 10 lines.
    Test defines colors for 10 lines.
    Test defines dash pattern and dash phase ( testValues ).
    Test strokes lines. 
    
    Expected output: 10 lines grid with dashed lines.
    '''
    
    testValues = { 'DashPhase'   : [ 20, 30, 40 ],
                   'DashPattern' : [ 15, 20, 25 ]
                   }
        
    def __init__( self, DashPhase, DashPattern ):
        vt.VisualTestCase.__init__( self )
        self.DashPhase   = DashPhase
        self.DashPattern = DashPattern
        self.VGJoinStyle = 'VG_JOIN_MITER'
        self.VGCapStyle  = 'VG_CAP_ROUND'
        self.repeats     = 1        
        self.name = 'OPENVG vgu stroked dashed line %s %s' % ( DashPhase, DashPattern, )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        Vgu    = modules[ 'Vgu' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 8, self.VGCapStyle, self.VGJoinStyle, 0 )               
                
        StartPointX = []
        StartPointY = []
        EndPointX   = []
        EndPointY   = []
                
        StartPointX.extend( [ 20, 60, 100, 140, 180, 20, 20, 20, 20, 20 ] )
        StartPointY.extend( [ 20, 20, 20, 20, 20, 20 , 60, 100, 140, 180 ] )
        EndPointX.extend( [ 20, 60, 100, 140, 180, 180, 180, 180, 180, 180 ] )
        EndPointY.extend( [ 180, 180, 180, 180, 180, 20, 60, 100, 140, 180 ] )
        
        RedArray   = []
        GreenArray = []
        BlueArray  = []
                
        RedArray.extend( [ 0.25, 0.5, 0.75, 0.2, 0.4, 0.7, 0.9, 0.2, 0.4, 0.6 ] )
        BlueArray.extend( [ 0.5, 0.25, 0.75, 0.4, 0.2, 0.6, 0.5, 0.7, 0.4, 0.8] )
        GreenArray.extend( [ 0.25, 0.9, 0.75, 0.0, 0.4, 0.5, 0.7, 0.2, 0.4, 0.3] )               
                
        dashpattern = [ self.DashPattern, self.DashPattern, self.DashPattern, self.DashPattern ]
        dashphase   = self.DashPhase
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        paths  = []
        paints = []
        
        for i in range( len( StartPointX ) ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )            
            OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
            Vgu.Line( pathIndex , StartPointX[ i ], StartPointY[ i ], EndPointX[ i ], EndPointY[ i ] )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
            
            red   = RedArray[ i]
            green = GreenArray[ i ]
            blue  = BlueArray[ i]
            alpha = 1.0
            OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )

            paths.append( pathIndex )
            paints.append( paintIndex )

        # ----------------------------------------------------------------------            
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
                
        for i in range( len( StartPointX ) ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class Polygon( vt.VisualTestCase ):

    '''
    The purpose of the test: This test is for testing vgu modules polygon function
        
    The test steps:
    Test defines 2 paths. One for rectangle and another for triangle.
    If Subject of testvalues is Single, only path 1 is drawn, else both paths are drawn.
    If Draw of testvalues is Stroke, only stroke path is drawn. If Draw is Fill, only fill
    path is drawn. If Draw is Both, stroke and fill paths are both drawn.
        
    Expected output: If Subject is single, rectangle with blue filling should appear
    to surface. If Subject is Overlapped, a triangle with green filling should appear on the
    top of the rectangle with blue filling.
    '''
    
    testValues = { 'Subject' : [ 'Single', 'Overlapped' ],
                   'Draw'    : [ 'Stroke', 'Fill', 'Both' ],
                   }
                
    def __init__( self, Subject, Draw ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle = 'VG_JOIN_MITER'
        self.VGCapStyle  = 'VG_CAP_ROUND'
        self.Subject     = Subject
        self.Draw        = Draw
        self.repeats     = 1
        self.name = 'OPENVG vgu polygon %s %s' % ( Subject, Draw, ) 
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        Vgu    = modules[ 'Vgu' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenVG.Stroke( 8, self.VGCapStyle, self.VGJoinStyle, 0 )
                        
        if self.Subject == 'Single':
            path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( path1Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
            Vgu.Polygon( path1Index, [ 25, 25, 25, 175, 175, 175, 175, 25 ], 'ON' )
        
        if self.Subject == 'Overlapped':
            path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )            
            OpenVG.CreatePath( path1Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
            Vgu.Polygon( path1Index, [ 25, 25, 25, 175, 175, 175, 175, 25 ], 'ON' )

            path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )            
            OpenVG.CreatePath( path2Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
            Vgu.Polygon( path2Index, [ 75, 75, 100, 125, 125, 75, 75, 75 ], 'ON' )
                
        StrokeColor = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor)
        OpenVG.ColorPaint( StrokeColor, [ 1.0, 0.0, 0.0, 1.0 ] )
                
        FillColor = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor)
        OpenVG.ColorPaint( FillColor, [ 0.0, 0.0, 1.0, 1.0 ] )
                
        FillColor2 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor2)
        OpenVG.ColorPaint( FillColor2, [ 0.0, 1.0, 0.0, 1.0 ] )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
        
        OpenVG.SetPaint( StrokeColor, [ 'VG_STROKE_PATH' ] )
        OpenVG.SetPaint( FillColor, [ 'VG_FILL_PATH' ] )                
                
        if self.Subject == 'Single':
            if self.Draw == 'Stroke':
                OpenVG.DrawPath( path1Index, [ 'VG_STROKE_PATH' ] )
            if self.Draw == 'Fill':
                OpenVG.DrawPath( path1Index, [ 'VG_FILL_PATH' ] )
            if self.Draw == 'Both':
                OpenVG.DrawPath( path1Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )
        else:
            if self.Draw == 'Stroke':
                OpenVG.DrawPath( path1Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( path2Index, [ 'VG_STROKE_PATH' ] )
            if self.Draw == 'Fill':
                OpenVG.DrawPath( path1Index, [ 'VG_FILL_PATH' ] )
                OpenVG.SetPaint( FillColor2, [ 'VG_FILL_PATH' ] )
                OpenVG.DrawPath( path2Index, [ 'VG_FILL_PATH' ] )
            if self.Draw == 'Both':
                OpenVG.DrawPath( path1Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )
                OpenVG.SetPaint( FillColor2, [ 'VG_FILL_PATH' ] )
                OpenVG.DrawPath( path2Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class Rect( vt.VisualTestCase ):

    '''
    The purpose of the test: This test is for testing vgu modules rect function.
        
    The test steps:
    Test defines 3 paths forming one bigger and two smaller rectangles.
    AlphaValues variates ( testValues ).
    If Draw of testvalues is Stroke, only stroke path is drawn. If Draw
    is Fill, only fill path is drawn. If Draw is Both, stroke and fill paths
    are both drawn.
        
    Expected output: Two smaller rectangles on the top of the bigger rectangle.
    '''
    
    testValues = { 'Draw'      : [ 'Stroke', 'Fill', 'Both' ],
                   'AlphaValue': [ 0.33, 0.5, 0.66, 1.0 ],
                   }
                
    def __init__( self, Draw, AlphaValue ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle = 'VG_JOIN_MITER'
        self.VGCapStyle  = 'VG_CAP_ROUND'
        self.Draw        = Draw
        self.AlphaValue  = AlphaValue
        self.repeats     = 1
        self.name = 'OPENVG vgu rect %s %s' % ( Draw, AlphaValue, ) 
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        Vgu    = modules[ 'Vgu' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenVG.Stroke( 8, self.VGCapStyle, self.VGJoinStyle, 0 )
               
        # Create for partially overlapping rectangles
        path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path1Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        Vgu.Rect( path1Index, 25, 25, 150, 150 )

        path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path2Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        Vgu.Rect( path2Index, 25, 100, 75, 75 )

        path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path3Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        Vgu.Rect( path3Index, 100, 25, 75, 75 )       
                
        StrokeColor = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor)
        OpenVG.ColorPaint( StrokeColor, [ 1.0, 0.0, 0.0, self.AlphaValue ] )
                
        FillColor = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor)
        OpenVG.ColorPaint( FillColor, [ 0.0, 0.0, 1.0, self.AlphaValue ] )
                
        FillColor2 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor2)
        OpenVG.ColorPaint( FillColor2, [ 0.0, 1.0, 0.0, self.AlphaValue ] )
                
        FillColor3 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor3 )
        OpenVG.ColorPaint( FillColor3, [ 0.0, 1.0, 1.0, self.AlphaValue ] )
                
        StrokeColor2 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor2 )
        OpenVG.ColorPaint( StrokeColor2, [ 1.0, 0.0, 0.0, self.AlphaValue ] )
                
        StrokeColor3 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor3 )
        OpenVG.ColorPaint( StrokeColor3, [ 0.0, 1.0, 1.0, self.AlphaValue ] )
                
        StrokeColor4 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor4 )
        OpenVG.ColorPaint( StrokeColor4, [ 0.0, 1.0, 0.0, self.AlphaValue ] )

        paths = []

        paths.append( path1Index )
        paths.append( path2Index )
        paths.append( path3Index )
        
        paints = []
        paints.append( StrokeColor )
        paints.append( FillColor )
        paints.append( FillColor2 )
        paints.append( FillColor3 )
        paints.append( StrokeColor2 )
        paints.append( StrokeColor3 )
        paints.append( StrokeColor4 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
                
        if self.Draw == 'Stroke':
            for i in range( 3 ):
                OpenVG.SetPaint( paints[ i + 4 ], [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( paints[ i ], [ 'VG_STROKE_PATH' ] )
                        
        if self.Draw == 'Fill':
            for i in range( 3 ):
                OpenVG.SetPaint( paints[ i + 1 ], [ 'VG_FILL_PATH' ] )
                OpenVG.DrawPath( paths[ i ], [ 'VG_FILL_PATH' ] )

        if self.Draw == 'Both':
            for i in range( 3 ):
                OpenVG.SetPaint( paints[ i + 4 ], [ 'VG_STROKE_PATH' ] )
                OpenVG.SetPaint( paints[ i + 1 ], [ 'VG_FILL_PATH' ] )
                OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class RoundRect( vt.VisualTestCase ):

    '''
    The purpose of the test: This test is for testing vgu modules RoundRect function.
        
    The test steps:
    Test defines 3 paths forming one bigger and two smaller round rectangles. Each
    rectangle has different arcwidths.
    VGJoinStyle, VGCapStyle and Alpha values are variating ( testValues ).
        
    Expected output: Two smaller rectangles on the top of the bigger rectangle.
    '''
    
    testValues =  { 'AlphaValue'  : [ 0.33, 0.5, 0.66, 1.0 ],
                    'VGJoinStyle' : [ 'VG_JOIN_MITER', 'VG_JOIN_ROUND', 'VG_JOIN_BEVEL' ],
                    'VGCapStyle'  : ['VG_CAP_BUTT', 'VG_CAP_ROUND', 'VG_CAP_SQUARE' ],
                    }

    def __init__( self, AlphaValue,VGJoinStyle, VGCapStyle ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle = VGJoinStyle
        self.VGCapStyle  = VGCapStyle
        self.AlphaValue  = AlphaValue
        self.repeats     = 1
        self.name = 'OPENVG vgu roundrect %s %s %s' % ( AlphaValue, VGJoinStyle, VGCapStyle, ) 
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        Vgu    = modules[ 'Vgu' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenVG.Stroke( 8, self.VGCapStyle, self.VGJoinStyle, 0 )
        
        # Create for partially overlapping rectangles
        path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path1Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        Vgu.RoundRect( path1Index, 25, 25, 150, 150, 75, 50 )

        path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path2Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        Vgu.RoundRect( path2Index, 25, 100, 75, 75, 50, 25 )

        path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path3Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        Vgu.RoundRect( path3Index, 100, 25, 75, 75, 25, 15 )
                
        StrokeColor = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor )
        OpenVG.ColorPaint( StrokeColor, [ 1.0, 0.0, 0.0, self.AlphaValue ] )
                
        FillColor = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor )
        OpenVG.ColorPaint( FillColor, [ 0.0, 0.0, 1.0, self.AlphaValue ] )
                
        FillColor2 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor2 )
        OpenVG.ColorPaint( FillColor2, [ 0.0, 1.0, 0.0, self.AlphaValue ] )
                
        FillColor3 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor3 )
        OpenVG.ColorPaint( FillColor3, [ 0.0, 1.0, 1.0, self.AlphaValue ] )
                
        StrokeColor2 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor2 )
        OpenVG.ColorPaint( StrokeColor2, [ 1.0, 0.0, 0.0, self.AlphaValue ] )
                
        StrokeColor3 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor3 )
        OpenVG.ColorPaint( StrokeColor3, [ 0.0, 1.0, 1.0, self.AlphaValue ] )
                
        StrokeColor4 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor4 )
        OpenVG.ColorPaint( StrokeColor4, [ 0.0, 1.0, 0.0, self.AlphaValue ] )

        paths = []

        paths.append( path1Index )
        paths.append( path2Index )
        paths.append( path3Index )
        
        paints = []
        paints.append( StrokeColor )
        paints.append( FillColor )
        paints.append( FillColor2 )
        paints.append( FillColor3 )
        paints.append( StrokeColor2 )
        paints.append( StrokeColor3 )
        paints.append( StrokeColor4 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
                
        for i in range( 3 ):
            OpenVG.SetPaint( paints[ i + 4 ], [ 'VG_STROKE_PATH' ] )
            OpenVG.SetPaint( paints[ i +1 ], [ 'VG_FILL_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class Ellipse( vt.VisualTestCase ):

    '''
    The purpose of the test: This test is for testing vgu modules Ellipse function.
        
    The test steps:
    Test defines 3 paths forming one bigger and two smaller Ellipses. 
    VGJoinStyle, VGCapStyle and Alpha values are variating ( testValues ).
        
    Expected output: Two smaller ellipses on the top of the bigger ellipse.
    '''
    
    testValues = { 'AlphaValue'  : [ 0.33, 0.5, 0.66, 1.0 ],
                   'VGJoinStyle' : [ 'VG_JOIN_MITER', 'VG_JOIN_ROUND', 'VG_JOIN_BEVEL' ],
                   'VGCapStyle'  : [ 'VG_CAP_BUTT', 'VG_CAP_ROUND', 'VG_CAP_SQUARE' ],
                   }
        
    def __init__( self, AlphaValue,VGJoinStyle, VGCapStyle ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle = VGJoinStyle
        self.VGCapStyle  = VGCapStyle
        self.AlphaValue  = AlphaValue
        self.repeats     = 1
        self.name = 'OPENVG vgu ellipse %s %s %s' % ( AlphaValue, VGJoinStyle, VGCapStyle, ) 
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        Vgu    = modules[ 'Vgu' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenVG.Stroke( 8, self.VGCapStyle, self.VGJoinStyle, 0 )
        
        # Create for partially overlapping rectangles
        path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path1Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        Vgu.Ellipse( path1Index, 50, 50, 150, 150 )

        path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path2Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        Vgu.Ellipse( path2Index, 25, 100, 75, 75 )

        path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path3Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        Vgu.Ellipse( path3Index, 100, 25, 50, 90 )
                                
        StrokeColor = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor )
        OpenVG.ColorPaint( StrokeColor, [ 1.0, 0.0, 0.0, self.AlphaValue ] )
                
        FillColor = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor )
        OpenVG.ColorPaint( FillColor, [ 0.0, 0.0, 1.0, self.AlphaValue ] )
                
        FillColor2 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor2 )
        OpenVG.ColorPaint( FillColor2, [ 0.0, 1.0, 0.0, self.AlphaValue ] )
                
        FillColor3 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor3 )
        OpenVG.ColorPaint( FillColor3, [ 0.0, 1.0, 1.0, self.AlphaValue ] )
                
        StrokeColor2 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor2 )
        OpenVG.ColorPaint( StrokeColor2, [ 1.0, 0.0, 0.0, self.AlphaValue ] )
                
        StrokeColor3 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor3 )
        OpenVG.ColorPaint( StrokeColor3, [ 0.0, 1.0, 1.0, self.AlphaValue ] )
                
        StrokeColor4 = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor4 )
        OpenVG.ColorPaint( StrokeColor4, [ 0.0, 1.0, 0.0, self.AlphaValue ] )

        paths = []

        paths.append( path1Index )
        paths.append( path2Index )
        paths.append( path3Index )
        
        paints = []
        paints.append( StrokeColor )
        paints.append( FillColor )
        paints.append( FillColor2 )
        paints.append( FillColor3 )
        paints.append( StrokeColor2 )
        paints.append( StrokeColor3 )
        paints.append( StrokeColor4 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
                
        OpenVG.Translate( 50, 50 )
                
        for i in range( 3 ):
            OpenVG.SetPaint( paints[ i + 4 ], [ 'VG_STROKE_PATH' ] )
            OpenVG.SetPaint( paints[ i + 1 ], [ 'VG_FILL_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class Arc( vt.VisualTestCase ):

    '''
    The purpose of the test: This test is for testing vgu modules Arc function.
        
    The test steps:
    Test defines an arc ( type of arc defined in testValues ).      
        
    Expected output: 4 Filled arc is drawn to the center of the surface. Arc type variates.
    '''
        
    testValues = { 'Form' : [ 'Chord', 'Pie', 'Open' ],
                   }
                
    def __init__( self, Form,  ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle = 'VG_JOIN_MITER'
        self.VGCapStyle  = 'VG_CAP_BUTT'
        self.AlphaValue  = 0.5
        self.Form        = Form
        self.repeats     = 1        
        self.name = 'OPENVG vgu arc %s' % ( self.Form, ) 
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        Vgu    = modules[ 'Vgu' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenVG.Stroke( 8, self.VGCapStyle, self.VGJoinStyle, 0 )
        
        StartAngle = []
        StartAngle.extend ( [ 90, -90, -90, 90 ] )               
        AngleExtent = []
        AngleExtent.extend( [ 90, -90, 90, -90 ] )

        paths = []
        
        for i in range( 4 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
            
            if self.Form == 'Pie':
                Vgu.Arc( pathIndex, 100, 100, 150, 150, StartAngle[ i ], AngleExtent[ i ], 'VGU_ARC_PIE' )

            if self.Form == 'Chord':
                Vgu.Arc( pathIndex, 100, 100, 150, 150, StartAngle[ i ], AngleExtent[ i ], 'VGU_ARC_CHORD' )

            if self.Form == 'Open':
                Vgu.Arc( pathIndex, 100, 100, 150, 150, StartAngle[ i ], AngleExtent[ i ], 'VGU_ARC_OPEN' )

            paths.append( pathIndex )
                
        StrokeColor = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( StrokeColor )
        OpenVG.ColorPaint( StrokeColor, [ 1.0, 0.0, 0.0, self.AlphaValue ] )
                
        FillColor = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( FillColor )
        OpenVG.ColorPaint( FillColor, [ 0.0, 0.0, 1.0, self.AlphaValue ] )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
                
        OpenVG.SetPaint( StrokeColor, [ 'VG_STROKE_PATH' ] )
        OpenVG.SetPaint( FillColor, [ 'VG_FILL_PATH' ] )
        
        for i in range( 4 ):
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class ComputeWarpQuadToSquare(vt.VisualTestCase ):

    '''
    The purpose of the test: This test is for testing vgu modules
    ComputeWarpQuadToSquare function.
        
    The test steps:
    1. Test draws tiger.
    2. The test gets the pixels.
    2. The Warp is computed to a matrix and matrix is loaded.
    3. The surface is cleared.
    4. The image are drawn to the surface.
                
    Expected output: a warped tiger. sx and sy values are variating.
    '''
        
    testValues = { ( 'Sx0', 'Sy0', 'Sx1', 'Sy1', 'Sx2', 'Sy2', 'Sx3', 'Sy3' ) :
                       [ ( -1.0, -1.0, 199.0, -1.0, -1.0, 199.0, 199.0, 199.0 ),
                         ( -1.0, -1.0,  99.0, -1.0, -1.0,  99.0,  99.0,  99.0 ),
                         ( -1.0, -1.0, 399.0, -1.0, -1.0, 399.0, 399.0, 399.0 ) ]
                   }
        
    def __init__( self,  Sx0, Sy0, Sx1, Sy1, Sx2, Sy2, Sx3, Sy3 ):
        vt.VisualTestCase.__init__( self )
        self.Sx0 = Sx0
        self.Sy0 = Sy0
        self.Sx1 = Sx1
        self.Sy1 = Sy1
        self.Sx2 = Sx2
        self.Sy2 = Sy2
        self.Sx3 = Sx3
        self.Sy3 = Sy3
        self.ImageFormat  = "VG_sRGBX_8888"
        self.ImageQuality = 'VG_IMAGE_QUALITY_BETTER'
        self.repeats      = 1                
        self.name = 'OPENVG vgu ComputeWarpQuadToSquare %s %s %s %s %s %s %s %s' % ( Sx0, Sy0, Sx1, Sy1, Sx2, Sy2, Sx3, Sy3 )  
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        Vgu    = modules[ 'Vgu' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )

        sx0, sy0 = self.Sx0, self.Sy0
        sx1, sy1 = self.Sx1, self.Sy1
        sx2, sy2 = self.Sx2, self.Sy2             
        sx3, sy3 = self.Sx3, self.Sy3

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
                
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
        
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )
        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )        
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )

        OpenVG.GetPixels( imageIndex, 0, 0, 0, 0, 200, 200 )

        matrixIndex = indexTracker.allocIndex( 'OPENVG_MATRIX_INDEX' )
        Vgu.ComputeWarpQuadToSquare( matrixIndex, sx0, sy0, sx1, sy1, sx2, sy2, sx3, sy3 )
                
        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        Vgu.LoadMatrix( matrixIndex )
        
        OpenVG.Translate( 50, 50 );
        OpenVG.Scale( 100, 100 );
        OpenVG.Translate( -1.0, -1.0 );
                
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( imageIndex )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class ComputeWarpSquareToQuad(vt.VisualTestCase ):
        
    '''
    The purpose of the test: This test is for testing vgu modules
    ComputeWarpSquareToQuad function.
        
    The test steps:
    1. Test draws tiger.
    2. The test gets the pixels.
    2. The Warp is computed to a matrix and matrix is loaded. 
    3. The surface is cleared.
    4. The image are drawn to the surface.
        
    Expected output: a warped tiger. dx and dy values are variating.
    '''
                                                                                                                                                        
    testValues = { ( 'Dx0', 'Dy0', 'Dx1', 'Dy1', 'Dx2', 'Dy2', 'Dx3', 'Dy3' ) :
                       [ ( 50.0, 50.0, 150.0, 50.0, 50.0, 150.0, 150.0, 150.0 ),
                         ( 50.0, 50.0, 150.0, 50.0, 75.0, 150.0, 125.0, 150.0 ),
                         ( 75.0, 50.0, 125.0, 50.0, 50.0, 150.0, 150.0, 150.0 ),
                         ( 50.0, 50.0, 150.0, 75.0, 50.0, 150.0, 150.0, 125.0 ),
                         ( 50.0, 75.0, 150.0, 50.0, 50.0, 125.0, 150.0, 150.0 ) ]
                   }
        
    def __init__( self,  Dx0, Dy0, Dx1, Dy1, Dx2, Dy2, Dx3, Dy3 ):
        vt.VisualTestCase.__init__( self )
        self.Dx0 = Dx0
        self.Dy0 = Dy0
        self.Dx1 = Dx1
        self.Dy1 = Dy1
        self.Dx2 = Dx2
        self.Dy2 = Dy2
        self.Dx3 = Dx3
        self.Dy3 = Dy3
        self.ImageFormat  = "VG_sRGBX_8888"
        self.ImageQuality = 'VG_IMAGE_QUALITY_BETTER'
        self.repeats      = 1
        self.name = 'OPENVG vgu ComputeWarpSquareToQuad %s %s %s %s %s %s %s %s' % ( Dx0, Dy0, Dx1, Dy1, Dx2, Dy2, Dx3, Dy3 )  
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        Vgu    = modules[ 'Vgu' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
                
        dx0, dy0 = self.Dx0, self.Dy0
        dx1, dy1 = self.Dx1, self.Dy1
        dx2, dy2 = self.Dx2, self.Dy2             
        dx3, dy3 = self.Dx3, self.Dy3

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
                
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
        
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )
        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )        
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' ) 
        OpenVG.CreateImage( imageIndex, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
                
        OpenVG.GetPixels( imageIndex, 0, 0, 0, 0, 200, 200 )               

        matrixIndex = indexTracker.allocIndex( 'OPENVG_MATRIX_INDEX' ) 
        Vgu.ComputeWarpSquareToQuad( matrixIndex, dx0, dy0, dx1, dy1, dx2, dy2, dx3, dy3 )
                               
        OpenVG.MatrixMode('VG_MATRIX_IMAGE_USER_TO_SURFACE')
        Vgu.LoadMatrix( matrixIndex )
                
        OpenVG.Scale( 1.0 / 200.0, 1.0 / 200.0 );
                
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( imageIndex )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class ComputeWarpQuadToQuad(vt.VisualTestCase ):

    '''
    The purpose of the test: This test is for testing vgu modules
    ComputeWarpQuadToQuad function.
        
    The test steps:
    1. Test draws a tiger.
    2. The test gets the pixels.
    2. The Warp is computed to a matrix and matrix is loaded.
    3. The surface is cleared.
    4. The image are drawn to the surface.
                
    Expected output: a warped tiger. dx and dy values are variating but sx and sy values are allways the same
    '''
                                                                                                                                                           
    testValues = { ( 'Dx0', 'Dy0', 'Dx1', 'Dy1', 'Dx2', 'Dy2', 'Dx3', 'Dy3' ) :
                       [ ( 100.0,  25.0, 200.0,   0.0, 100.0,  75.0, 200.0, 100.0 ),
                         ( 150.0,  25.0,   0.0,   0.0, 150.0,  75.0,   0.0, 150.0 ),
                         ( 200.0,  50.0,   0.0,   0.0, 200.0, 125.0,   0.0, 200.0 ),
                         (   0.0,  50.0, 200.0,   0.0,   0.0, 125.0, 200.0, 200.0 ),
                         (  50.0, 200.0,   0.0,   0.0, 150.0, 200.0, 200.0,   0.0 ),
                         (  50.0,   0.0,   0.0, 200.0, 150.0,   0.0, 200.0, 200.0 ) ]
                   }
        
    def __init__( self,  Dx0, Dy0, Dx1, Dy1, Dx2, Dy2, Dx3, Dy3   ):
        vt.VisualTestCase.__init__( self )
        self.Dx0 = Dx0
        self.Dy0 = Dy0
        self.Dx1 = Dx1
        self.Dy1 = Dy1
        self.Dx2 = Dx2
        self.Dy2 = Dy2
        self.Dx3 = Dx3
        self.Dy3 = Dy3
        self.ImageFormat  = "VG_sRGBX_8888"
        self.ImageQuality = 'VG_IMAGE_QUALITY_BETTER'
        self.repeats      = 1
        self.name = 'OPENVG vgu ComputeWarpQuadToQuad %s %s %s %s %s %s %s %s' % ( Dx0, Dy0, Dx1, Dy1, Dx2, Dy2, Dx3, Dy3 )  
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        Vgu    = modules[ 'Vgu' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )

        sx0, sy0 =   0.0,   0.0
        sx1, sy1 = 200.0,   0.0
        sx2, sy2 =   0.0, 200.0
        sx3, sy3 = 200.0, 200.0
        
        dx0, dy0 = self.Dx0, self.Dy0
        dx1, dy1 = self.Dx1, self.Dy1
        dx2, dy2 = self.Dx2, self.Dy2             
        dx3, dy3 = self.Dx3, self.Dy3

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
                
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
        
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )
        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )        
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
                
        OpenVG.GetPixels( imageIndex, 0,0, 0,0, 200, 200 )

        matrixIndex = indexTracker.allocIndex( 'OPENVG_MATRIX_INDEX' )        
        Vgu.ComputeWarpQuadToQuad( matrixIndex, dx0, dy0, dx1, dy1, dx2, dy2, dx3, dy3, sx0, sy0, sx1, sy1, sx2, sy2, sx3, sy3 )
                
        OpenVG.MatrixMode('VG_MATRIX_IMAGE_USER_TO_SURFACE')
        Vgu.LoadMatrix( matrixIndex )
                
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( imageIndex )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
