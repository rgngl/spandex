#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class AllpairsPaintStrokedLines( vt.VisualTestCase ):

    '''
    This test is for stroked lines. Test is drawing 7 different sized boxes
    on black canvas in 7 loops. The following function/parameters are changing
    with every TestCount based on predefined allpair-testing table
    (AllpairTesting):1. VGJoinStyle, 2. VGCapStyle, 3. GradientType,
    4. SpreadMode, 5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_BEVEL', 'VG_CAP_SQUARE', 'NONE',   'VG_COLOR_RAMP_SPREAD_PAD',     'NO',  'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_ROUND',  'RADIAL', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'RADIAL', 'NONE',                         'NO',  'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'LINEAR', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'NO',  'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_BUTT',   'LINEAR', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'YES', 'VG_PATH_DATATYPE_S_32' ) ]
                   }
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1        
        self.name = 'OPENVG stroked lines (no overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                  VGCapStyle,
                                                                                  GradientType,
                                                                                  SpreadMode,
                                                                                  Dashing,
                                                                                  DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        LeftDownX   = 10
        LeftDownY   = 10
        LeftUpX     = 10
        LeftUpY     = 190
        RightUpX    = 190
        RightUpY    = 190
        RightDownX  = 190
        RightDownY  = 10
        Add1        = 180
        Add2        = -180

        if self.Dashing == 'YES':
            dashpattern = [ 10, 10, 10, 10 ]
            dashphase   = 40
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        paths  = []
        paints = []
        
        for i in range( 7 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
           
            if self.GradientType != 'NONE':
                if self.GradientType == 'LINEAR':
                    OpenVG.LinearGradientPaint( paintIndex, [ 60, 60 ], [ 125, 125 ] )
                else:
                    OpenVG.RadialGradientPaint( paintIndex, [ 75, 75 ], [ 30, 30 ], 75 )

                if self.SpreadMode != 'NONE':
                    OpenVG.ColorRamps( paintIndex,
                                       self.SpreadMode,
                                       'ON',
                                       [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                         0.5, 0.0, 1.0, 0.0, 0.5,
                                         1.0, 0.0, 0.0, 1.0, 1.0 ] )
            else:
                red   = 1 - float( i ) / float( 8.0 ) % 1.0
                green = float( i ) / float( 2.0 ) % 1.0
                blue  = float( i ) / float( 4.0 ) % 1.0
                alpha = 0.75
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
            OpenVG.MoveTo( pathDataIndex, [ LeftDownX, LeftDownY ], 'OFF' )
            OpenVG.Lines( pathDataIndex, [ 0, Add1 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ LeftUpX, LeftUpY ], 'OFF' )
            OpenVG.Lines( pathDataIndex, [ Add1, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ RightUpX, RightUpY ], 'OFF' )
            OpenVG.Lines( pathDataIndex, [ 0, Add2 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ RightDownX, RightDownY ], 'OFF' )
            OpenVG.Lines( pathDataIndex, [ Add2, 0 ], 'ON' )
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

            LeftDownX  += 10
            LeftDownY  += 10
            LeftUpX    += 10
            LeftUpY    -= 10
            RightUpX   -= 10
            RightUpY   -= 10
            RightDownX -= 10
            RightDownY += 10
            Add1       -= 20
            Add2       += 20

            paths.append( pathIndex )
            paints.append( paintIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 7 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class AllpairsPaintStrokedLines2( vt.VisualTestCase ):
    
    '''
    This test is for testing stroked lines. Test is drawing 12 Stroked lines
    ( 6 horizontal lines on 6 vertical lines ) on black canvas in 12
    loops. These function/parameters are changing with every TestCount based on
    pre-defined allpair-testing table:1. VGJoinStyle, 2. VGCapStyle,
    3. GradientType, 4. SpreadMode, 5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_BEVEL', 'VG_CAP_SQUARE', 'NONE',   'VG_COLOR_RAMP_SPREAD_PAD',     'NO',  'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_ROUND',  'RADIAL', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'RADIAL', 'NONE',                         'NO',  'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'LINEAR', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'NO',  'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_BUTT',   'LINEAR', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'YES', 'VG_PATH_DATATYPE_S_32' ) ]
                   }
                                                                             
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1
        self.name = 'OPENVG stroked lines (overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                               VGCapStyle,
                                                                               GradientType,
                                                                               SpreadMode,
                                                                               Dashing,
                                                                               DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        if self.Dashing == 'YES':
            dashpattern = [ 10, 5, 10, 5 ]
            dashphase   = 30
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        VerticalStartX, VerticalStartY      = 20, 30
        HorizontalStartX, HorizontalStartY  = 20, 180

        paths  = []
        paints = []
        
        for i in range( 12 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
                 
            if self.GradientType != 'NONE':
                if self.GradientType == 'LINEAR':
                    OpenVG.LinearGradientPaint( paintIndex, [ 60, 60 ], [ 125, 125 ] )
                else:
                    OpenVG.RadialGradientPaint( paintIndex, [ 75, 75 ], [ 30, 30 ], 75 )

                if self.SpreadMode != 'NONE':
                    OpenVG.ColorRamps( paintIndex,
                                       self.SpreadMode,
                                       'ON',
                                       [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                         0.5, 0.0, 1.0, 0.0, 0.5,
                                         1.0, 0.0, 0.0, 1.0, 1.0 ] )
            else:
                red   = 1 - float( i ) / float( 8.0 ) % 1.0
                green = float( i ) / float( 2.0 ) % 1.0
                blue  = float( i ) / float( 4.0 ) % 1.0
                alpha = 0.75
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )

            if i < 6:       
                OpenVG.MoveTo( pathDataIndex,[ VerticalStartX, VerticalStartY ], 'OFF' )
                OpenVG.Lines( pathDataIndex, [ 0, 150 ], 'ON' )
                VerticalStartX += 30
            if i > 5:
                OpenVG.MoveTo( pathDataIndex, [ HorizontalStartX, HorizontalStartY ], 'OFF' )
                OpenVG.Lines( pathDataIndex, [ 150, 0 ], 'ON' )
                HorizontalStartY -= 30
                
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

            paths.append( pathIndex )
            paints.append( paintIndex )

        # ----------------------------------------------------------------------            
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFilledLines( vt.VisualTestCase ):
    
    '''
    This test is for testing filled lines. Test is drawing 6 vertical filled
    lines on purple canvas in 6 loops. These functions/parameters are changing
    with every TestCount based on pre-defined allpair-testing
    table:1. VGJoinStyle, 2. VGCapStyle, 3. GradientType, 4. SpreadMode,
    5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_BEVEL', 'VG_CAP_SQUARE', 'NONE',   'VG_COLOR_RAMP_SPREAD_PAD',     'NO',  'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_ROUND',  'RADIAL', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'RADIAL', 'NONE',                         'NO',  'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'LINEAR', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'NO',  'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_BUTT',   'LINEAR', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'YES', 'VG_PATH_DATATYPE_S_32' ) ]
                   }
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1
        self.name = 'OPENVG filled lines (no overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                 VGCapStyle,
                                                                                 GradientType,
                                                                                 SpreadMode,
                                                                                 Dashing,
                                                                                 DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        if self.Dashing == 'YES':
            dashpattern = [ 10.0, 10, 10.0, 10 ]
            dashphase   = 40
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.25, 0.0, 0.33, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        StartX, StartY = 10, 20

        paths  = []
        paints = []
        
        for i in range( 6 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
      
            if self.GradientType != 'NONE':
                if self.GradientType == 'LINEAR':
                    OpenVG.LinearGradientPaint( paintIndex, [ 60, 60 ], [ 125, 125 ] )
                else:
                    OpenVG.RadialGradientPaint( paintIndex, [ 75, 75 ], [ 30, 30 ], 75 )

                if self.SpreadMode != 'NONE':
                    OpenVG.ColorRamps( paintIndex,
                                       self.SpreadMode,
                                       'ON',
                                       [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                         0.5, 0.0, 1.0, 0.0, 0.5,
                                         1.0, 0.0, 0.0, 1.0, 1.0 ] )
            else:
                red   = 1 - float( i ) / float( 8.0 ) % 1.0
                green = float( i ) / float( 2.0 ) % 1.0
                blue  = float( i ) / float( 4.0 ) % 1.0
                alpha = 0.75
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
            OpenVG.MoveTo( pathDataIndex, [ StartX, StartY ], 'OFF' )
            OpenVG.Lines( pathDataIndex, [ 0, 160 ], 'ON' )
            OpenVG.Lines( pathDataIndex, [ 20, 0 ], 'ON' )
            OpenVG.Lines( pathDataIndex, [ 0, -160 ], 'ON' )
            OpenVG.Lines( pathDataIndex, [ -20, 0 ], 'ON' )
            StartX += 30
         
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

            paths.append( pathIndex )
            paints.append( paintIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 6 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_FILL_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFilledLines2( vt.VisualTestCase ):
    
    '''
    This test is for testing filled lines. Test is drawing 12 filled lines (
    6 horizontal lines on 6 vertical lines ) on purple canvas in 12 loops. These
    functions/parameters are changing with every TestCount based on pre-defined
    allpair-testing table:1. VGJoinStyle, 2. VGCapStyle, 3. GradientType,
    4. SpreadMode, 5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_BEVEL', 'VG_CAP_SQUARE', 'NONE',   'VG_COLOR_RAMP_SPREAD_PAD',     'NO',  'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_ROUND',  'RADIAL', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'RADIAL', 'NONE',                         'NO',  'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'LINEAR', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'NO',  'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_BUTT',   'LINEAR', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'YES', 'VG_PATH_DATATYPE_S_32' ) ]
                   }
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1        
        self.name = 'OPENVG filled lines (overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                              VGCapStyle,
                                                                              GradientType,
                                                                              SpreadMode,
                                                                              Dashing,
                                                                              DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
                       
        if self.Dashing == 'YES':
            dashpattern = [ 10.0, 10, 10, 10 ]
            dashphase   = 40
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.25, 0.0, 0.33, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        VerticalStartX, VerticalStartY      = 15, 20
        HorizontalStartX, HorizontalStartY  = 15, 170

        paths  = []
        paints = []
        
        for i in range( 12 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
      
            if self.GradientType != 'NONE':
                if self.GradientType == 'LINEAR':
                    OpenVG.LinearGradientPaint( paintIndex, [ 60, 60 ], [ 125, 125 ] )
                else:
                    OpenVG.RadialGradientPaint( paintIndex, [ 75, 75 ], [ 30, 30 ], 75 )

                if self.SpreadMode != 'NONE':
                    OpenVG.ColorRamps( paintIndex,
                                       self.SpreadMode,
                                       'ON',
                                       [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                         0.5, 0.0, 1.0, 0.0, 0.5,
                                         1.0, 0.0, 0.0, 1.0, 1.0 ] )
            else:
                red   = 1 - float( i ) / float( 8.0 ) % 1.0
                green = float( i ) / float( 2.0 ) % 1.0
                blue  = float( i ) / float( 4.0 ) % 1.0
                alpha = 0.75
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )

            if i < 6:       
                OpenVG.MoveTo( pathDataIndex,[ VerticalStartX, VerticalStartY ], 'OFF' )
                OpenVG.Lines( pathDataIndex, [ 0, 160 ], 'ON' )
                OpenVG.Lines( pathDataIndex, [ 15, 0 ], 'ON' )
                OpenVG.Lines( pathDataIndex, [ 0, -160 ], 'ON' )
                OpenVG.Lines( pathDataIndex, [ -15, 0 ], 'ON' )
                VerticalStartX += 30
            if i > 5:
                OpenVG.MoveTo( pathDataIndex, [ HorizontalStartX, HorizontalStartY ], 'OFF' )
                OpenVG.Lines( pathDataIndex, [ 165, 0 ], 'ON' )
                OpenVG.Lines( pathDataIndex, [ 0, -15 ], 'ON' )
                OpenVG.Lines( pathDataIndex, [ -165, 0 ], 'ON' )
                OpenVG.Lines( pathDataIndex, [ 0, 15 ], 'ON' )
                HorizontalStartY -= 25
                
            OpenVG.AppendPathData( pathIndex, pathDataIndex )
            
            paths.append( pathIndex )
            paints.append( paintIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_FILL_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFilledQuadraticBeziers( vt.VisualTestCase ):
    
    '''
    This test is for testing filled quadratic beziers. Test is drawing 2
    Quadratic Beziers forming a drop on purple canvas. These
    functions/parameters are changing with every TestCount based on pre-defined
    allpair-testing table:1. VGJoinStyle, 2. VGCapStyle, 3. GradientType,
    4. SpreadMode, 5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'LINEAR', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'NO',  'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'NONE',   'NONE',                         'YES', 'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_SQUARE', 'LINEAR', 'VG_COLOR_RAMP_SPREAD_PAD',     'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_BUTT',   'RADIAL', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'NO',  'VG_PATH_DATATYPE_S_16' ) ]
                   }
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1
        self.name = 'OPENVG filled quadratic beziers (no overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                             VGCapStyle,
                                                                                             GradientType,
                                                                                             SpreadMode,
                                                                                             Dashing,
                                                                                             DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
                
        if self.Dashing == 'YES':
            dashpattern = [ 10, 10, 10, 10 ]
            dashphase   = 40
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.25, 0.0, 0.33, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
      
        if self.GradientType != 'NONE':
            if self.GradientType == 'LINEAR':
                OpenVG.LinearGradientPaint( paintIndex, [ 60, 60 ], [ 125, 125 ] )
            else:
                OpenVG.RadialGradientPaint( paintIndex, [ 75, 75 ], [ 30, 30 ], 75 )

            if self.SpreadMode != 'NONE':
                OpenVG.ColorRamps( paintIndex,
                                   self.SpreadMode,
                                   'ON',
                                   [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                     0.5, 0.0, 1.0, 0.0, 0.5,
                                     1.0, 0.0, 0.0, 1.0, 1.0 ] )
        else:
            red   = 1 - float( 0 ) / float( 8.0 ) % 1.0
            green = float( 0 ) / float( 2.0 ) % 1.0
            blue  = float( 0 ) / float( 4.0 ) % 1.0
            alpha = 0.75
            OpenVG.ColorPaint( 0, [ red, green, blue, alpha ] )
            
        OpenVG.MoveTo( pathDataIndex, [ 100, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ 170, 70, 0, 170 ], 'ON' )
        OpenVG.MoveTo( pathDataIndex, [ 100, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ -170, 70, 0, 170 ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear(0,0,0,0)
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class AllpairsPaintFilledQuadraticBeziers2( vt.VisualTestCase ):
    
    '''
    This test is for testing filled quadratic beziers. Test is drawing 4
    Quadratic Beziers forming 2 partially overlapped drops on purple
    canvas. These functions/parameters are changing with every TestCount based
    on pre-defined allpair-testing table (AllpairTesting):1. VGJoinStyle,
    2. VGCapStyle, 3. GradientType, 4. SpreadMode, 5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'LINEAR', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'NO',  'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'NONE',   'NONE',                         'YES', 'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_SQUARE', 'LINEAR', 'VG_COLOR_RAMP_SPREAD_PAD',     'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_BUTT',   'RADIAL', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'NO',  'VG_PATH_DATATYPE_S_16' ) ]
                   }
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1    
        self.name = 'OPENVG filled quadratic beziers (overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                          VGCapStyle,
                                                                                          GradientType,
                                                                                          SpreadMode,
                                                                                          Dashing,
                                                                                          DataType, )

    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
              
        if self.Dashing == 'YES':
            dashpattern = [ 10, 10, 10, 10 ]
            dashphase   = 40
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.25, 0.0, 0.33, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        StartX, StartY = 85, 15

        paths  = []
        paints = []
        
        for i in range( 2 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
      
            if self.GradientType != 'NONE':
                if self.GradientType == 'LINEAR':
                    OpenVG.LinearGradientPaint( paintIndex, [ 60, 60 ], [ 125, 125 ] )
                else:
                    OpenVG.RadialGradientPaint( paintIndex, [ 75, 75 ], [ 30, 30 ], 75 )

                if self.SpreadMode != 'NONE':
                    OpenVG.ColorRamps( paintIndex,
                                       self.SpreadMode,
                                       'ON',
                                       [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                         0.5, 0.0, 1.0, 0.0, 0.5,
                                         1.0, 0.0, 0.0, 1.0, 1.0 ] )
            else:
                red   = 1 - float( i ) / float( 8.0 ) % 1.0
                green = float( i ) / float( 2.0 ) % 1.0
                blue  = float( i ) / float( 4.0 ) % 1.0
                alpha = 0.75
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
    
            OpenVG.MoveTo( pathDataIndex, [ StartX, StartY ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex, [ 150, 70, 0, 150 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ StartX, StartY ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex, [ -150, 70, 0, 150 ], 'ON' )
            OpenVG.AppendPathData( pathIndex, pathDataIndex )
       
            StartX += 25
            StartY += 25

            paths.append( pathIndex )
            paints.append( paintIndex )

        # ----------------------------------------------------------------------            
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 2 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_FILL_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class AllpairsPaintStrokedQuadraticBeziers( vt.VisualTestCase ):
    
    '''
    This test is for testing stroked quadratic beziers. Test draws 12
    quadratic beziers forming 6 drops on black canvas. These
    functions/parameters are changing with every TestCount based on pre-defined
    allpair-testing table:1. VGJoinStyle, 2. VGCapStyle, 3. GradientType,
    4. SpreadMode, 5. Dashing, 6. DataType.
    '''

    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'LINEAR', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'NO',  'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'NONE',   'NONE',                         'YES', 'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_SQUARE', 'LINEAR', 'VG_COLOR_RAMP_SPREAD_PAD',     'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_BUTT',   'RADIAL', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'NO',  'VG_PATH_DATATYPE_S_16' ) ]
                   }
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1              
        self.name = 'OPENVG stroked quadratic beziers (no overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                              VGCapStyle,
                                                                                              GradientType,
                                                                                              SpreadMode,
                                                                                              Dashing,
                                                                                              DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
                

        if( self.Dashing == 'YES'):
            dashpattern = [ 10, 10, 10, 10 ]
            dashphase   = 40
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        X1 = 100
        X2 = 170
        X3 = -170
        Y1 = 15

        paths  = []
        paints = []
        
        for i in range( 6 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( i, self.DataType, 1, 1 )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
                 
            if self.GradientType != 'NONE':
                if self.GradientType == 'LINEAR':
                    OpenVG.LinearGradientPaint( paintIndex, [ 60, 60 ], [ 125, 125 ] )
                else:
                    OpenVG.RadialGradientPaint( paintIndex, [ 75, 75 ], [ 30, 30 ], 75 )

                if self.SpreadMode != 'NONE':
                    OpenVG.ColorRamps( paintIndex,
                                       self.SpreadMode,
                                       'ON',
                                       [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                         0.5, 0.0, 1.0, 0.0, 0.5,
                                         1.0, 0.0, 0.0, 1.0, 1.0 ] )
            else:
                red   = 1 - float( i ) / float( 8.0 ) % 1.0
                green = float( i ) / float( 2.0 ) % 1.0
                blue  = float( i ) / float( 4.0 ) % 1.0
                alpha = 0.75
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )

            OpenVG.MoveTo( pathDataIndex, [ X1, Y1 ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex,[ X2, 70, 0, X2 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ X1, Y1 ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex, [ X3, 70, 0, X2 ], 'ON' )
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

            X2 -= 20
            X3 += 20
            Y1 += 5

            paths.append( pathIndex )
            paints.append( paintIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 6 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintStrokedQuadraticBeziers2( vt.VisualTestCase ):
    
    '''
    This test is for testing stroked quadratic beziers. Test draws 6
    quadratic beziers forming 3 partially overlapping drops on black
    canvas. These functions/parameters are changing with every TestCount based
    on pre-defined allpair-testing table:1. VGJoinStyle, 2. VGCapStyle,
    3. GradientType, 4. SpreadMode, 5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType') :
                       [ ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'LINEAR', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'NO',  'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'NONE',   'NONE',                         'YES', 'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_SQUARE', 'LINEAR', 'VG_COLOR_RAMP_SPREAD_PAD',     'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_BUTT',   'RADIAL', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'NO',  'VG_PATH_DATATYPE_S_16' ) ]
                   }
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1        
        self.name = 'OPENVG stroked quadratic beziers (overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                           VGCapStyle,
                                                                                           GradientType,
                                                                                           SpreadMode,
                                                                                           Dashing,
                                                                                           DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
                
        if( self.Dashing == 'YES'):
            dashpattern = [ 10, 10, 10, 10 ]
            dashphase   = 40
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        X1 = 80
        X2 = 140
        X3 = -140
        Y1 = 50

        paths  = []
        paints = []
        
        for i in range( 3 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
           
            if self.GradientType != 'NONE':
                if self.GradientType == 'LINEAR':
                    OpenVG.LinearGradientPaint( paintIndex, [ 60, 60 ], [ 125, 125 ] )
                else:
                    OpenVG.RadialGradientPaint( paintIndex, [ 75, 75 ], [ 30, 30 ], 75 )

                if self.SpreadMode != 'NONE':
                    OpenVG.ColorRamps( paintIndex,
                                       self.SpreadMode,
                                       'ON',
                                       [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                         0.5, 0.0, 1.0, 0.0, 0.5,
                                         1.0, 0.0, 0.0, 1.0, 1.0 ] )
            else:
                red   = 1 - float( i ) / float( 8.0 ) % 1.0
                green = float( i ) / float( 2.0 ) % 1.0
                blue  = float( i ) / float( 4.0 ) % 1.0
                alpha = 0.75
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )

            OpenVG.MoveTo( pathDataIndex, [ X1, Y1 ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex, [ X2, 70, 0, X2 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ X1, Y1 ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex, [ X3, 70, 0, X2 ], 'ON' )
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

            X2 -= 20
            X3 += 20
            Y1 -= 15
            X1 += 30

            paths.append( pathIndex )
            paints.append( paintIndex )
    
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 3 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class AllpairsPaintFilledArcs( vt.VisualTestCase ):
    
    '''
    This test is for testing filled arcs.Test draws 4 filled, partially
    overlapping circles on purple canvas. These functions/parameters are
    changing with every TestCount based on pre-defined allpair-testing
    table:1. VGJoinStyle, 2. VGCapStyle, 3. GradientType, 4. SpreadMode,
    5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'RADIAL', 'VG_COLOR_RAMP_SPREAD_PAD',     'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'LINEAR', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'NO',  'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_ROUND',  'NONE',   'VG_COLOR_RAMP_SPREAD_REFLECT', 'YES', 'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_SQUARE', 'LINEAR', 'NONE',                         'NO',  'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_ROUND',  'RADIAL', 'NONE',                         'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_BUTT',   'NONE',   'VG_COLOR_RAMP_SPREAD_REPEAT',  'YES', 'VG_PATH_DATATYPE_F' ) ]
                   }                                                          
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1   
        self.name = 'OPENVG filled arcs (partially overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                       VGCapStyle,
                                                                                       GradientType,
                                                                                       SpreadMode,
                                                                                       Dashing,
                                                                                       DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        if self.Dashing == 'YES':
            dashpattern = [ 10, 10, 10, 10 ]
            dashphase   = 40
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.25, 0.0, 0.33, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        StartX,StartY = 10, 140

        paths  = []
        paints = []
        c      = 0
        
        for i in range( 2 ):
            for x in range( 2 ):
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
           
                if self.GradientType != 'NONE':
                    if self.GradientType == 'LINEAR':
                        OpenVG.LinearGradientPaint( paintIndex, [ 60, 40 ], [ 125, 125 ] )
                    else:
                        OpenVG.RadialGradientPaint( paintIndex, [ 70, 40 ], [ 75, 75 ], 180 )

                    if self.SpreadMode != 'NONE':
                        OpenVG.ColorRamps( paintIndex,
                                           self.SpreadMode,
                                           'ON',
                                           [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                             0.5, 0.0, 1.0, 0.0, 0.5,
                                             1.0, 0.0, 0.0, 1.0, 1.0 ] )
                else:
                    red   = 1 - float( c ) / float( 8.0 ) % 1.0
                    green = float( c ) / float( 2.0 ) % 1.0
                    blue  = float( c ) / float( 4.0 ) % 1.0
                    alpha = 0.75
                    OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                OpenVG.MoveTo( pathDataIndex, [ StartX, StartY ], 'OFF' )
                OpenVG.Arcs( pathDataIndex, [ 50, 50, 5 ,5, 20 ], 'LCCWARC', 'ON')
                
                OpenVG.AppendPathData( pathIndex, pathDataIndex )
                
                StartX += 75

                c +=1
                paths.append( pathIndex )
                paints.append( paintIndex )

            StartX = 10
            StartY -= 75

        # ----------------------------------------------------------------------            
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 4 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_FILL_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class AllpairsPaintStrokedArcs( vt.VisualTestCase ):
    
    '''
    This test is for testing stroked arcs. Test draws 9 stroked partially
    overlapping arcs on black canvas. These functions/parameters are changing
    with every TestCount based on pre-defined allpair-testing
    table:1. VGJoinStyle, 2. VGCapStyle, 3. GradientType, 4. SpreadMode,
    5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType') :
                       [ ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'RADIAL', 'VG_COLOR_RAMP_SPREAD_PAD',     'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'LINEAR', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'NO',  'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_ROUND',  'NONE',   'VG_COLOR_RAMP_SPREAD_REFLECT', 'YES', 'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_SQUARE', 'LINEAR', 'NONE',                         'NO',  'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_ROUND',  'RADIAL', 'NONE',                         'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_BUTT',   'NONE',   'REPEAT',                       'YES', 'VG_PATH_DATATYPE_F' ) ]
                   }                                                          
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1
        self.name = 'OPENVG stroked arcs (partially overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                        VGCapStyle,
                                                                                        GradientType,
                                                                                        SpreadMode,
                                                                                        Dashing,
                                                                                        DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        if self.Dashing == 'YES':
            dashpattern = [ 7.5, 7.5, 7.5, 7.5 ]
            dashphase   = 30
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        StartX,StartY = 25, 140

        paths  = []
        paints = []
        c      = 0
        
        for i in range( 3 ):
            for x in range( 3 ):
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
      
                if self.GradientType != 'NONE':
                    if self.GradientType == 'LINEAR':
                        OpenVG.LinearGradientPaint( paintIndex, [ 60, 40 ], [ 125, 125 ] )
                    else:
                        OpenVG.RadialGradientPaint( paintIndex, [ 130, 130 ], [ 130, 130 ], 150 )

                    if self.SpreadMode != 'NONE':
                        OpenVG.ColorRamps( paintIndex,
                                           self.SpreadMode,
                                           'ON',
                                           [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                             0.5, 0.0, 1.0, 0.0, 0.5,
                                             1.0, 0.0, 0.0, 1.0, 1.0 ] )
                else:
                    red   = 1 - float( c ) / float( 8.0 ) % 1.0
                    green = float( c ) / float( 2.0 ) % 1.0
                    blue  = float( c ) / float( 4.0 ) % 1.0
                    alpha = 0.75
                    OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                OpenVG.MoveTo( pathDataIndex, [ StartX, StartY ], 'OFF' )
                OpenVG.Arcs( pathDataIndex, [ 25, 25, 0 ,50 ,0], 'SCWARC', 'ON')
            
                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                StartX += 50                
                c +=1

                paths.append( pathIndex )
                paints.append( paintIndex )

            StartX = 25
            StartY -= 50

        # ----------------------------------------------------------------------            
        self.beginBenchmarkActions()
        
        OpenVG.Clear(0,0,0,0)
        
        for i in range( 9 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class AllpairsPaintFilledScubicBeziers( vt.VisualTestCase ):
    
    ''' This test is for testing filled scubic beziers. Test draws 2 filled,
    partially overlapping ScubicBeziers. These parameters are changing with
    every TestCount based on pre-defined allpair-testing table
    (AllpairTesting):1. VGJoinStyle, 2. VGCapStyle, 3. GradientType,
    4. SpreadMode, 5. Dashing, 6. DataType.
    '''

    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_ROUND', 'VG_CAP_BUTT',   'NONE',   'NONE',                         'NO',  'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_ROUND',  'LINEAR', 'VG_COLOR_RAMP_SPREAD_PAD',     'NO',  'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_SQUARE', 'RADIAL', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'NO',  'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_SQUARE', 'RADIAL', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'NONE',   'VG_COLOR_RAMP_SPREAD_REFLECT', 'YES', 'VG_PATH_DATATYPE_S_16' ) ]
                   }
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1        
        self.name = 'OPENVG filled scubic beziers (partially overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                                 VGCapStyle,
                                                                                                 GradientType,
                                                                                                 SpreadMode,
                                                                                                 Dashing,
                                                                                                 DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
               
        if self.Dashing == 'YES':
            dashpattern = [ 7.5, 7.5, 7.5, 7.5 ]
            dashphase   = 30
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.25, 0.0, 0.33, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
      
        if self.GradientType != 'NONE':
            if self.GradientType == 'LINEAR':
                OpenVG.LinearGradientPaint( paintIndex, [ 60, 40 ], [ 125, 125 ] )
            else:
                OpenVG.RadialGradientPaint( paintIndex, [ 130, 130 ], [ 130, 130 ], 150 )

            if self.SpreadMode != 'NONE':
                OpenVG.ColorRamps( paintIndex,
                                   self.SpreadMode,
                                   'ON',
                                   [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                     0.5, 0.0, 1.0, 0.0, 0.5,
                                     1.0, 0.0, 0.0, 1.0, 1.0 ] )
        else:
            red   = 1 - float( 0 ) / float( 8.0 ) % 1.0
            green = float( 0 ) / float( 2.0 ) % 1.0
            blue  = float( 0 ) / float( 4.0 ) % 1.0
            alpha = 0.75
            OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )

        OpenVG.MoveTo( pathDataIndex, [ 45, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ 110, 30, 30, 110 ], 'ON' )
        OpenVG.SCubicBeziers( pathDataIndex, [ 60, 50, 40, 60 ], 'ON' )

        OpenVG.MoveTo( pathDataIndex, [ 145, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ -110, 30, -30, 110 ], 'ON' )
        OpenVG.SCubicBeziers( pathDataIndex, [ -60, 50, -40, 60 ], 'ON' )
            
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class AllpairsPaintStrokedScubicBeziers( vt.VisualTestCase ):
    
    ''' This test is for testing stroked scubic beziers. Test draws 2 stroked,
    partially overlapping ScubicBeziers. These functions/parameters are changing
    with every TestCount based on pre-defined allpair-testing
    table:1. VGJoinStyle, 2. VGCapStyle, 3. GradientType, 4. SpreadMode,
    5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_ROUND', 'VG_CAP_BUTT',   'NONE',   'NONE', 'NO', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_ROUND',  'LINEAR', 'VG_COLOR_RAMP_SPREAD_PAD',     'NO',  'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_SQUARE', 'RADIAL', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'NO',  'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_SQUARE', 'RADIAL', 'VG_COLOR_RAMP_SPREAD_REPEAT',  'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_ROUND',  'NONE',   'VG_COLOR_RAMP_SPREAD_REFLECT', 'YES', 'VG_PATH_DATATYPE_S_16' ) ]
                   }
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1        
        self.name = 'OPENVG stroked scubic bezier (partially overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                                 VGCapStyle,
                                                                                                 GradientType,
                                                                                                 SpreadMode,
                                                                                                 Dashing,
                                                                                                 DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        if self.Dashing == 'YES':
            dashpattern = [ 7.5, 7.5, 7.5, 7.5 ]
            dashphase   = 30
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
      
        if self.GradientType != 'NONE':
            if self.GradientType == 'LINEAR':
                OpenVG.LinearGradientPaint( paintIndex, [ 60, 40 ], [ 125, 125 ] )
            else:
                OpenVG.RadialGradientPaint( paintIndex, [ 130, 130 ], [ 130, 130 ], 150 )

            if self.SpreadMode != 'NONE':
                OpenVG.ColorRamps( paintIndex,
                                   self.SpreadMode,
                                   'ON',
                                   [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                     0.5, 0.0, 1.0, 0.0, 0.5,
                                     1.0, 0.0, 0.0, 1.0, 1.0 ] )
        else:
            red   = 1 - float( 0 ) / float( 8.0 ) % 1.0
            green = float( 0 ) / float( 2.0 ) % 1.0
            blue  = float( 0 ) / float( 4.0 ) % 1.0
            alpha = 0.75
            OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )

        OpenVG.MoveTo( pathDataIndex, [ 45, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ 110, 30, 30, 110 ], 'ON' )
        OpenVG.SCubicBeziers( pathDataIndex, [ 60, 50, 40, 60 ], 'ON' )
            
        OpenVG.MoveTo( pathDataIndex, [ 145, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ -110, 30, -30, 110 ], 'ON' )
        OpenVG.SCubicBeziers( pathDataIndex, [ -60, 50, -40, 60 ], 'ON' )
            
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintStrokedSquadBeziers( vt.VisualTestCase ):
    
    ''' This test is for testing stroked squad beziers. Test draws 2 stroked,
    partially overlapping Squad Beziers. These functions/parameters are changing
    with every TestCount based on pre-defined allpair-testing
    table:1. VGJoinStyle, 2. VGCapStyle, 3. GradientType, 4. SpreadMode,
    5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_ROUND', 'VG_CAP_SQUARE', 'RADIAL', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'YES', 'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_SQUARE', 'LINEAR', 'NONE',                         'YES', 'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'NONE',   'VG_COLOR_RAMP_SPREAD_REPEAT',  'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_ROUND',  'NONE',   'VG_COLOR_RAMP_SPREAD_PAD',     'NO',  'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_BUTT',   'RADIAL', 'VG_COLOR_RAMP_SPREAD_PAD',     'NO',  'VG_PATH_DATATYPE_S_32' ) ]
                   }                                                          
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1        
        self.name = 'OPENVG stroked squad beziers (partially overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                                 VGCapStyle,
                                                                                                 GradientType,
                                                                                                 SpreadMode,
                                                                                                 Dashing,
                                                                                                 DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        if self.Dashing == 'YES':
            dashpattern = [ 7.5, 7.5, 7.5, 7.5 ]
            dashphase   = 30
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
      
        if self.GradientType != 'NONE':
            if self.GradientType == 'LINEAR':
                OpenVG.LinearGradientPaint( paintIndex, [ 60, 40 ], [ 125, 125 ] )
            else:
                OpenVG.RadialGradientPaint( paintIndex, [ 130, 130 ], [ 130, 130 ], 150 )

            if self.SpreadMode != 'NONE':
                OpenVG.ColorRamps( paintIndex,
                                   self.SpreadMode,
                                   'ON',
                                   [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                     0.5, 0.0, 1.0, 0.0, 0.5,
                                     1.0, 0.0, 0.0, 1.0, 1.0 ] )
        else:
            red   = 1 - float( 0 ) / float( 8.0 ) % 1.0
            green = float( 0 ) / float( 2.0 ) % 1.0
            blue  = float( 0 ) / float( 4.0 ) % 1.0
            alpha = 0.75
            OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )

        OpenVG.MoveTo( pathDataIndex, [ 25, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ 110, 30, 30, 110 ], 'ON' )
        OpenVG.SQuadBeziers( pathDataIndex, [ 70, 70 ], 'ON' )

        OpenVG.MoveTo( pathDataIndex, [ 170, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ -110, 30, -30, 110 ], 'ON' )
        OpenVG.SQuadBeziers( pathDataIndex, [ -70, 70 ], 'ON' )
        
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFilledSquadBeziers( vt.VisualTestCase ):
    
    ''' This test is for testing filled squad beziers. Test draws 2 filled,
    partially overlapping Squad Beziers. These functions/parameters are changing
    with every TestCount based on pre-defined allpair-testing
    table:1. VGJoinStyle, 2. VGCapStyle, 3. GradientType, 4. SpreadMode,
    5. Dashing, 6. DataType.
    '''
    
    testValues = { ( 'VGJoinStyle', 'VGCapStyle', 'GradientType', 'SpreadMode', 'Dashing', 'DataType' ) :
                       [ ( 'VG_JOIN_ROUND', 'VG_CAP_SQUARE', 'RADIAL', 'VG_COLOR_RAMP_SPREAD_REFLECT', 'YES', 'VG_PATH_DATATYPE_F' ),
                         ( 'VG_JOIN_BEVEL', 'VG_CAP_SQUARE', 'LINEAR', 'NONE',                         'YES', 'VG_PATH_DATATYPE_S_32' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_BUTT',   'NONE',   'VG_COLOR_RAMP_SPREAD_REPEAT',  'YES', 'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_MITER', 'VG_CAP_ROUND',  'NONE',   'VG_COLOR_RAMP_SPREAD_PAD',     'NO',  'VG_PATH_DATATYPE_S_16' ),
                         ( 'VG_JOIN_ROUND', 'VG_CAP_BUTT',   'RADIAL', 'VG_COLOR_RAMP_SPREAD_PAD',     'NO',  'VG_PATH_DATATYPE_S_32' ) ]
                   }                                                                             
    
    def __init__( self, VGJoinStyle, VGCapStyle, GradientType, SpreadMode, Dashing, DataType ):
        vt.VisualTestCase.__init__( self )
        self.VGJoinStyle  = VGJoinStyle
        self.VGCapStyle   = VGCapStyle
        self.GradientType = GradientType
        self.SpreadMode   = SpreadMode
        self.Dashing      = Dashing
        self.DataType     = DataType
        self.repeats      = 1        
        self.name = 'OPENVG filled squad beziers (partially overlapping) %s %s %s %s %s %s' % ( VGJoinStyle,
                                                                                                VGCapStyle,
                                                                                                GradientType,
                                                                                                SpreadMode,
                                                                                                Dashing,
                                                                                                DataType, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
                
        if self.Dashing == 'YES':
            dashpattern = [ 7.5, 7.5, 7.5, 7.5 ]
            dashphase   = 30
            OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        OpenVG.ClearColor( [ 0.25, 0.0, 0.33, 1.0 ] )
        OpenVG.Stroke( 5, self.VGCapStyle, self.VGJoinStyle, 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, self.DataType, 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
      
        if self.GradientType != 'NONE':
            if self.GradientType == 'LINEAR':
                OpenVG.LinearGradientPaint( paintIndex, [ 60, 40 ], [ 125, 125 ] )
            else:
                OpenVG.RadialGradientPaint( paintIndex, [ 130, 130 ], [ 130, 130 ], 150 )

            if self.SpreadMode != 'NONE':
                OpenVG.ColorRamps( paintIndex,
                                   self.SpreadMode,
                                   'ON',
                                   [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                     0.5, 0.0, 1.0, 0.0, 0.5,
                                     1.0, 0.0, 0.0, 1.0, 1.0 ] )
        else:
            red   = 1 - float( 0 ) / float( 8.0 ) % 1.0
            green = float( 0 ) / float( 2.0 ) % 1.0
            blue  = float( 0 ) / float( 4.0 ) % 1.0
            alpha = 0.75
            OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )

        OpenVG.MoveTo( pathDataIndex, [ 25, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ 110, 30, 30, 110 ], 'ON' )
        OpenVG.SQuadBeziers( pathDataIndex, [ 70, 70 ], 'ON' )

        OpenVG.MoveTo( pathDataIndex, [ 170, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ -110, 30, -30, 110 ], 'ON' )
        OpenVG.SQuadBeziers( pathDataIndex, [ -70, 70 ], 'ON' )
        
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH', 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################        
class AllpairsPaintFunctions1( vt.VisualTestCase ):
    
    ''' Test draws a 8 arcs forming four circles on black canvas. The circles
    are then filled with radial gradient. Test is using following parameters:
    Pathtype = Stroke, VgJoinStyle = Miter, VGCapStyle = BUTT, GradientType =
    Radial, SpreadingMode = Pad, PathDataType = S_8, Dashing = Yes. This test is
    a part of allpair-testing testset (Paint - 1/25).
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG dashed arc (stroked radial gradient)'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 35
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_MITER', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
        
        x,x1 = 0,67
        y,y1 = 40,40

        paths  = []
        paints = []
        c      = 0
        
        for i in range( 4 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_8', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_8', 1, 1 )
            
            OpenVG.MoveTo( pathDataIndex, [ x, y ], 'OFF' )
            OpenVG.Arcs( pathDataIndex, [ 10, 10, 50, x1, y ], 'SCCWARC' , 'OFF' )
            OpenVG.MoveTo ( pathDataIndex, [ x1, y1 ], 'OFF' )
            OpenVG.Arcs( pathDataIndex, [ 10, 10, 50, x, y1 ], 'SCCWARC' , 'OFF' )
            
            x  += 20
            x1 += 20
            y  += 20
            y1 += 20
            
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
            
            OpenVG.RadialGradientPaint( paintIndex, [ 75, 75 ], [ 30, 30 ], 75 )
            OpenVG.ColorRamps( paintIndex,
                               'VG_COLOR_RAMP_SPREAD_PAD',
                               'ON',
                               [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                 0.5, 0.0, 1.0, 0.0, 0.5,
                                 1.0, 0.0, 0.0, 1.0, 1.0 ] )

            paths.append( pathIndex )
            paints.append( paintIndex )
            c += 1

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear(0,0,0,0)
        
        for i in range( 4 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFunctions2( vt.VisualTestCase ):
    
    '''
    Test is drawing seven stroked boxes on black canvas. Each box is drawn
    with different color.Test is using following parameters: Pathtype = Stroke,
    VgJoinStyle = Miter, VGCapStyle = BUTT, PathDataType = S_16, Dashing =
    No. This test is a part of allpair-testing testset (Paint - 3/25).
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG lines (stroked)'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )       
        OpenVG.Stroke( 5,'VG_CAP_SQUARE', 'VG_JOIN_BEVEL', 0 )
        
        LeftDownX  = 10
        LeftDownY  = 10
        LeftUpX    = 10
        LeftUpY    = 190
        RightUpX   = 190
        RightUpY   = 190
        RightDownX = 190
        RightDownY = 10
        Add1       = 180
        Add2       = -180

        paths  = []
        paints = []
        
        for i in  range( 7 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_16', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_16', 1, 1 )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
            
            red   = 1 - float( i ) / float( 8.0 ) % 1.0
            green = float( i ) / float( 2.0 ) % 1.0
            blue  = float( i ) / float( 4.0 ) % 1.0
            alpha = 0.75            
            OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
            
            OpenVG.MoveTo( pathDataIndex,[ LeftDownX, LeftDownY ], 'OFF' )
            OpenVG.Lines( pathDataIndex, [ 0, Add1 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ LeftUpX, LeftUpY ], 'OFF' )
            OpenVG.Lines( pathDataIndex, [ Add1, 0 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ RightUpX, RightUpY ], 'OFF' )
            OpenVG.Lines( pathDataIndex, [ 0, Add2 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ RightDownX, RightDownY ], 'OFF' )
            OpenVG.Lines( pathDataIndex, [ Add2, 0 ], 'ON' )
            
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

            LeftDownX  += 10
            LeftDownY  += 10
            LeftUpX    += 10
            LeftUpY    -= 10
            RightUpX   -= 10
            RightUpY   -= 10
            RightDownX -= 10
            RightDownY += 10
            Add1       -= 20
            Add2       += 20

            paths.append( pathIndex )
            paints.append( paintIndex )
    
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 7 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class AllpairsPaintFunctions3( vt.VisualTestCase ):
    
    '''
    Test is drawing a box with black dashed line on purple canvas.The box is
    then filled with radial gradient. Test is using following parameters:
    Pathtype1 = Stroke (Dashed Line), Pathtype2 = Fill (Inside the lines),
    VgJoinStyle = Bevel, VGCapStyle = ROUND, GradientType = Radial,
    SpreadingMode = Repeat, PathDataType = S_8, Dashing = Yes. This test is a
    part of allpair-testing testset (Paint - 4/25).
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG dashed lines (filled radial gradient)'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 35
        OpenVG.ClearColor( [ 0.33, 0.0, 0.66, 1.0 ] )
        OpenVG.Stroke( 3, 'VG_CAP_ROUND', 'VG_JOIN_BEVEL', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_8', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_8', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        
        OpenVG.RadialGradientPaint( paintIndex, [ 50, 50 ], [ 20, 20 ], 50 )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0.0, 1.0, 0.0, 0.0, 1.0,
                             0.5, 0.0, 1.0, 0.0, 0.5,
                             1.0, 0.0, 0.0, 1.0, 1.0 ] )
        
        OpenVG.MoveTo( pathDataIndex, [ 10, 10 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ 0, 125 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ 125, 0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ 0, -125 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ -125, 0 ], 'ON' )
        
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFunctions4( vt.VisualTestCase ):
    
    '''
    Test draws two quadratic beziers forming a drop on black canvas. The
    drop is then filled with linear gradient. Test is using following
    parameters: Pathtype = Fill, VgJoinStyle = Miter, VGCapStyle = BUTT,
    GradientType = Linear, SpreadingMode = Reflect, PathDataType = S_32, Dashing
    = No. This test is a part of allpair-testing testset (Paint - 5/25).
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG quadratic beziers (filled linear gradient)'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 3, 'VG_CAP_BUTT', 'VG_JOIN_MITER', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        
        OpenVG.LinearGradientPaint( paintIndex, [ 150, 150 ], [ 80, 80 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REFLECT',
                           'ON',
                           [ 0.0, 1.0, 0.0, 0.0, 1.0,
                             0.5, 0.0, 1.0, 0.0, 0.5,
                             1.0, 0.0, 0.0, 1.0, 1.0 ] )
        
        OpenVG.MoveTo( pathDataIndex, [ 100, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex,[ 170, 70, 0, 170 ], 'ON' )
        OpenVG.MoveTo( pathDataIndex, [ 100, 15 ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex,[ -170, 70, 0, 170 ], 'ON' )
        
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------         
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFunctions5( vt.VisualTestCase ):
    
    '''
    Test draws 12 quadratic beziers forming 6 drops on black canvas. Each
    drop are drawn with different color. Test is using following parameters:
    Pathtype = Stroke, VgJoinStyle = Round, VGCapStyle = Round, GradientType =
    None, SpreadingMode = None, PathDataType = F, Dashing = Yes. This test is a
    part of allpair-testing testset (Paint - 6/25).
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1        
        self.name = 'OPENVG dashed quadratic beziers (stroked)'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
                        
        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 13.5
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 5, 'VG_CAP_ROUND', 'VG_JOIN_ROUND', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )

        X1 = 100
        X2 = 170
        X3 = -170
        Y1 = 15

        paths  = []
        paints = []
        
        for i in range( 6 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_F', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_F', 1, 1 )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
            
            red   = 1 - float( i ) / float( 8.0 ) % 1.0
            green = float( i ) / float( 2.0 ) % 1.0
            blue  = float( i ) / float( 4.0 ) % 1.0
            alpha = 0.75
            OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
        
            OpenVG.MoveTo( pathDataIndex, [ X1, Y1 ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex, [ X2, 70, 0, X2 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ X1, Y1 ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex, [ X3, 70, 0, X2 ], 'ON' )
            
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

            X2 -= 20
            X3 += 20
            Y1 += 5

            paths.append( pathIndex )
            paints.append( paintIndex )

        # ----------------------------------------------------------------------             
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 6 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFunctions6( vt.VisualTestCase ):
    
    '''
    Test draws six filled squad beziers with incremental size on purple
    canvas. Test is using following parameters: Pathtype1 = Stroke (Dashed
    Line), Pathtype2 = Fill (Inside the lines), VgJoinStyle = Bevel, VGCapStyle
    = SQUARE, GradientType = Radial, SpreadingMode = REFLECT, PathDataType = F,
    Dashing = Yes. This test is a part of allpair-testing testset (Paint -
    7/25).
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1        
        self.name = 'OPENVG dashed squad bezier (stroked line filled inside with radial gradient)'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 35
        OpenVG.ClearColor( [ 0.33, 0.0, 0.66, 1.0 ] )
        OpenVG.Stroke( 3, 'VG_CAP_SQUARE', 'VG_JOIN_BEVEL', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
        
        StartX,StartY       = 20, 10
        AdditionX,AdditionY = 50, 90
        AddSize             = 0

        paths  = []
        paints = []
        
        for x in range( 2 ):
            for i in range( 3 ):
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_F', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_F', 1, 1 )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                OpenVG.RadialGradientPaint( paintIndex, [ 50, 50 ], [ 20, 20 ], 50 )
                OpenVG.ColorRamps( paintIndex,
                                   'VG_COLOR_RAMP_SPREAD_REFLECT',
                                   'ON',
                                   [ 0.0, 1.0, 0.0, 0.0, 1.0,
                                     0.5, 0.0, 1.0, 0.0, 0.5,
                                     1.0, 0.0, 0.0, 1.0, 1.0 ] )
    
                OpenVG.MoveTo( pathDataIndex, [ StartX, StartY ], 'OFF' )
                OpenVG.QuadraticBeziers( pathDataIndex, [ 50 + AddSize, 20, 20, 50 + AddSize ], 'ON' )
                OpenVG.SQuadBeziers( pathDataIndex, [ 10, 10 ], 'ON' )
                
                OpenVG.AppendPathData( pathIndex, pathDataIndex )
       
                StartX  = StartX + AdditionX
                AddSize = AddSize + 3 

                paths.append( pathIndex )
                paints.append( paintIndex )
                
            StartX = 20
            StartY = StartY + AdditionY        

        # ----------------------------------------------------------------------             
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 6 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_FILL_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFunctions7( vt.VisualTestCase ):
    
    '''
    Test draws six squad beziers with incremental size on purple canvas.Test
    is using following parameters: Pathtype1 = Stroke, VgJoinStyle = Bevel,
    VGCapStyle = SQUARE, GradientType = Linear, SpreadingMode = None,
    PathDataType = S_32, Dashing = Yes. This test is a part of allpair-testing
    testset (Paint - 8/25).
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG dashed squad bezier (stroked line linear gradient)'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
               
        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 35
        
        OpenVG.ClearColor( [ 0.33, 0.0, 0.66, 1.0 ] )
        OpenVG.Stroke( 3, 'VG_CAP_SQUARE', 'VG_JOIN_BEVEL', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
        
        StartX,StartY       = 20, 10
        AdditionX,AdditionY = 50, 90
        AddSize             = 0

        paths  = []
        paints = []
        
        for x in range( 2 ):
            for i in range( 3 ):
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                OpenVG.LinearGradientPaint( paintIndex, [ 100, 100 ], [ 50, 50 ] )
                
                OpenVG.MoveTo( pathDataIndex, [ StartX, StartY ], 'OFF' )
                OpenVG.QuadraticBeziers( pathDataIndex, [ 50 + AddSize, 20, 20, 50 + AddSize ], 'ON' )
                OpenVG.SQuadBeziers( pathDataIndex, [ 10, 10 ], 'ON' )
                
                OpenVG.AppendPathData( pathIndex, pathDataIndex )
       
                StartX  = StartX + AdditionX
                AddSize = AddSize + 3 

                paths.append( pathIndex )
                paints.append( paintIndex )
                
            StartX = 20
            StartY = StartY + AdditionY

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 6 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFunctions8( vt.VisualTestCase ):
    
    '''
    Test draws filled ScubicBezier on black canvas. Test is using following
    parameters: Pathtype1 = Fill, VgJoinStyle = Round, VGCapStyle = BUTT,
    GradientType = none, PathDataType = S_8, Dashing = No. This test is a part
    of allpair-testing testset (Paint - 9/25).
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG scubic bezier (filled)'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 13.5
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 3, 'VG_CAP_BUTT', 'VG_JOIN_ROUND', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
        
        StartX, StartY = 65, 15

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_8', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_8', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        
        red   = 1 - float( 0 ) / float( 8.0 )
        green = float( 0 ) / float( 2.0 ) % 1.0
        blue  = float( 0 ) / float( 4.0 ) % 1.0
        alpha = 0.75
        OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
        
        OpenVG.MoveTo( pathDataIndex, [ StartX, StartY ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ 110, 30, 30, 110 ], 'ON' )
        OpenVG.SCubicBeziers( pathDataIndex, [ 60, 50, 40, 60 ], 'ON' )
        
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFunctions9( vt.VisualTestCase ):
    
    '''
    Test draws stroked ScubicBezier on black canvas. Test is using following
    parameters: Pathtype1 = Stroke, VgJoinStyle = Miter, VGCapStyle = Round,
    GradientType = Linear, Spreadmode = Pad, PathDataType = F, Dashing =
    No. This test is a part of allpair-testing testset (Paint - 10/25).
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1        
        self.name = 'OPENVG scubic bezier (stroked linear gradient)'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
                
        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 13.5
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 3, 'VG_CAP_BUTT', 'VG_JOIN_ROUND', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
        
        StartX, StartY = 65, 15

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_F', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
        
        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_F', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 50, 50 ], [ 125, 50 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_REPEAT',
                           'ON',
                           [ 0.0,  0.22, 0.33, 0.44, 0.55,
                             0.66, 0.77, 0.88, 0.99, 1.00,
                             1.0,   0.0,  0.0,  1.0,  1.0 ] )
        
        OpenVG.MoveTo( pathDataIndex, [ StartX, StartY ], 'OFF' )
        OpenVG.QuadraticBeziers( pathDataIndex, [ 110, 30, 30, 110 ], 'ON' )
        OpenVG.SCubicBeziers( pathDataIndex, [ 60, 50, 40, 60 ], 'ON' )
        
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class AllpairsPaintFunctions10( vt.VisualTestCase ):
    
    '''
    Test draws 6 Stroked ScubicBeziers forming 3 different sized ficure
    8s. on black canvas. Test is using following parameters: Pathtype1 = Stroke,
    VgJoinStyle = Miter, VGCapStyle = BUTT, GradientType = none, PathDataType =
    S_16, Dashing = yes. This test is a part of allpair-testing testset (Paint -
    11/25).
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1        
        self.name = 'OPENVG squad bezier (stroked)'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 13.5
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Stroke( 3, 'VG_CAP_BUTT', 'VG_JOIN_MITER', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
        
        Reduce                       = 20
        ReducedX, ReducedY, ReducedZ = 150, 80, 120
        StartX, StartY               = 95, 15

        paths  = []
        paints = []
                
        for i in range( 3 ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_16', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_16', 1, 1 )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
            
            red   = 1 - float( i ) / float( 2.0 ) % 2.0
            green = float( i ) / float( 2.0 ) % 2.0
            blue  = float( i ) / float( 4.0 ) % 2.0
            alpha = 0.75
            OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )

            OpenVG.MoveTo( pathDataIndex, [ StartX, StartY ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex, [ ReducedX, ReducedY, 0, ReducedZ ], 'ON' )
            OpenVG.SQuadBeziers( pathDataIndex, [ 50, 50 ], 'ON' )
            OpenVG.MoveTo( pathDataIndex, [ StartX, StartY ], 'OFF' )
            OpenVG.QuadraticBeziers( pathDataIndex, [ -ReducedX, ReducedY, 0, ReducedZ ], 'ON' )
            OpenVG.SQuadBeziers( pathDataIndex, [ 50, 50 ], 'ON' )
            
            ReducedX = ReducedX - Reduce
            ReducedY = ReducedY - Reduce
            ReducedZ = ReducedZ - Reduce
       
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

            paths.append( pathIndex )
            paints.append( paintIndex )
            
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 3 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
