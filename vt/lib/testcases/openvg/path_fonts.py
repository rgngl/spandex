#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 240
HEIGHT = 320

######################################################################
class DrawGlyphsStroked( vt.VisualTestCase ):

    ''' 
    The test description: This test is for testing drawing of glyphs with
    differing drawing modes.
        
    The test steps:
    1. The test creates paint and font.
    2. The test creates for path data forming letters A, X, E, O.
    3. The test set glyphs for created paths.
    4. The test draws glyphs with differing drawing modes ( testValues ).
        
    Expected output: Text AXEO.
    '''

    testValues = { ( 'DataType', 'LineWidth', 'CapStyle', 'JoinStyle' ) :
                           [ ( 'VG_PATH_DATATYPE_S_8',  2,  'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                             ( 'VG_PATH_DATATYPE_S_8',  5,  'VG_CAP_ROUND',  'VG_JOIN_ROUND' ),
                             ( 'VG_PATH_DATATYPE_S_8',  10, 'VG_CAP_SQUARE', 'VG_JOIN_BEVEL' ),
                             ( 'VG_PATH_DATATYPE_S_16', 2,  'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                             ( 'VG_PATH_DATATYPE_S_16', 5,  'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                             ( 'VG_PATH_DATATYPE_S_16', 10, 'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                             ( 'VG_PATH_DATATYPE_S_32', 2,  'VG_CAP_SQUARE', 'VG_JOIN_ROUND' ),
                             ( 'VG_PATH_DATATYPE_S_32', 5,  'VG_CAP_BUTT',   'VG_JOIN_BEVEL' ),
                             ( 'VG_PATH_DATATYPE_S_32', 10, 'VG_CAP_ROUND',  'VG_JOIN_MITER' ),
                             ( 'VG_PATH_DATATYPE_F',    5,  'VG_CAP_SQUARE', 'VG_JOIN_MITER' ),
                             ( 'VG_PATH_DATATYPE_F',    2,  'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                             ( 'VG_PATH_DATATYPE_F',    10, 'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                             ( 'VG_PATH_DATATYPE_S_16', 2,  'VG_CAP_SQUARE', 'VG_JOIN_MITER' ) ],
                   'DrawGlyphMode' :
                           [ 'DrawGlyph', 'DrawGlyphs' ]
                   }
        
    def __init__( self, DataType, LineWidth, CapStyle, JoinStyle, DrawGlyphMode ):
        vt.VisualTestCase.__init__( self )
        self.DATATYPE      = DataType
        self.LineWidth     = LineWidth
        self.CapStyle      = CapStyle
        self.JoinStyle     = JoinStyle
        self.DrawGlyphMode = DrawGlyphMode
        self.repeats       = 1
        self.name = 'OPENVG DrawGlyph stroked %s %s %s %s %s' % ( DataType, LineWidth, CapStyle, JoinStyle, DrawGlyphMode, )
            
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
                
        OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
        OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )
        
        # Create paint 
        PaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( PaintIndex )
        OpenVG.ColorPaint( PaintIndex, [ 0.5, 0.5, 0.1, 1.0 ] )
                
        # Create a font
        FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
        OpenVG.CreateFont( FontIndex, 0 )
                
        # Define letter width
        LetterWidth = 40

        # letter A
        Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
        
        OpenVG.MoveTo( PathData1Index, [ 0, 0 ], 'OFF' )
        OpenVG.Lines( PathData1Index, [ LetterWidth / 2, LetterWidth ], 'ON' )
        OpenVG.Lines( PathData1Index, [ LetterWidth / 2, -LetterWidth ], 'ON' )
        OpenVG.MoveTo( PathData1Index, [ LetterWidth / 4, LetterWidth / 2 ], 'OFF' )
        OpenVG.Lines( PathData1Index, [ LetterWidth / 2, 0 ], 'ON' )
        Glyph1Index = 0
        OpenVG.SetGlyphToPath( FontIndex, Glyph1Index, Path1Index, 'OFF', [ 0, 0.0 ], [ LetterWidth + 15, 0 ] )
                
        # letter X
        Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
        
        OpenVG.MoveTo( PathData2Index, [ 0, 0 ], 'OFF' )
        OpenVG.Lines( PathData2Index, [ LetterWidth, LetterWidth ], 'ON' )
        OpenVG.MoveTo( PathData2Index, [ 0, LetterWidth ], 'OFF' )
        OpenVG.Lines( PathData2Index, [ LetterWidth, -LetterWidth ], 'ON' )
        Glyph2Index = 1
        OpenVG.SetGlyphToPath( FontIndex, Glyph2Index, Path2Index, 'OFF', [ 0.0, 0.0 ], [ LetterWidth + 15, 0 ] )
        
        # letter E
        Path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( Path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        PathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( PathData3Index, self.DATATYPE, 1, 1 )
        
        OpenVG.MoveTo( PathData3Index, [ LetterWidth, 0 ], 'OFF' )
        OpenVG.Lines( PathData3Index, [ -LetterWidth, 0 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ 0, LetterWidth / 2], 'ON' )
        OpenVG.Lines( PathData3Index, [ LetterWidth, 0 ], 'ON' )
        OpenVG.MoveTo( PathData3Index, [ 0, LetterWidth / 2 ], 'OFF' )
        OpenVG.Lines( PathData3Index, [ 0, LetterWidth / 2 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ LetterWidth, 0 ], 'ON' )
        Glyph3Index = 2
        OpenVG.SetGlyphToPath( FontIndex, Glyph3Index, Path3Index, 'OFF', [ 0.0, 0.0 ], [ LetterWidth + 15, 0 ] )
                
        # letter O
        Path4Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( Path4Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        PathData4Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( PathData4Index, self.DATATYPE, 1, 1 )
        
        OpenVG.MoveTo( PathData4Index, [ 0, 0 ], 'OFF' )
        OpenVG.Lines( PathData4Index, [ LetterWidth, 0 ], 'ON')
        OpenVG.Lines( PathData4Index, [ 0,LetterWidth ], 'ON' )
        OpenVG.Lines( PathData4Index, [ -LetterWidth, 0 ], 'ON' )
        OpenVG.Lines( PathData4Index, [ 0,-LetterWidth ], 'ON' )
        OpenVG.ClosePath( PathData4Index )
        Glyph4Index = 3
        OpenVG.SetGlyphToPath( FontIndex, Glyph4Index, Path4Index, 'OFF', [ 0.0, 0.0 ], [ LetterWidth + 15, 0 ] )
                
        # append path data
        OpenVG.AppendPathData( Path1Index, PathData1Index )
        OpenVG.AppendPathData( Path2Index, PathData2Index )
        OpenVG.AppendPathData( Path3Index, PathData3Index )
        OpenVG.AppendPathData( Path4Index, PathData4Index )

        # Define glypth indices
        GlyphCount   = 4
        GlyphIndices = []
        for i in range( GlyphCount ):
                GlyphIndices.extend( [ i ] )
                
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
                
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( PaintIndex, [ 'VG_STROKE_PATH' ] )
                
        # Count starting point of first letter
        GlyphOriginX = WIDTH / GlyphCount - LetterWidth
        GlyphOriginY = HEIGHT / 2
        
        # Set start point(  Glyph origin )
        OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )
        
        # Draw Glyph
        if self.DrawGlyphMode == 'DrawGlyph':
                OpenVG.DrawGlyph( FontIndex, Glyph1Index, [ 'VG_STROKE_PATH' ], 'ON' )
                OpenVG.DrawGlyph( FontIndex, Glyph2Index, [ 'VG_STROKE_PATH' ], 'ON' )
                OpenVG.DrawGlyph( FontIndex, Glyph3Index, [ 'VG_STROKE_PATH' ], 'ON' )
                OpenVG.DrawGlyph( FontIndex, Glyph4Index, [ 'VG_STROKE_PATH' ], 'ON' )

        # Draw Glyphs
        if self.DrawGlyphMode ==  'DrawGlyphs':
                OpenVG.DrawGlyphs( FontIndex,
                                   GlyphCount,
                                   GlyphIndices,
                                   [ 0.0, 0.0, 0.0, 0.0 ],
                                   [ 0.0, 0.0, 0.0, 0.0 ],
                                   [ 'VG_STROKE_PATH' ],
                                   'ON' )
                
        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                

######################################################################
class DrawGlyphsFilled( vt.VisualTestCase ):

    ''' 
    The test description: This test is for testing drawing of glyphs with
    differing drawing modes.
        
    The test steps:
    1. The test creates paint and font.
    2. The test creates for path data forming letters N, O, K, I, A.
    3. The test set glyphs for created paths
    4. The test draws glyphs with differing drawing modes ( testValues ).
        
    Expected output: Text NOKIA.
    '''

    testValues = { ( 'DataType', 'LineWidth', 'CapStyle', 'JoinStyle' ) :
                           [ ( 'VG_PATH_DATATYPE_S_8',  2, 'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                             ( 'VG_PATH_DATATYPE_S_8',  4, 'VG_CAP_ROUND',  'VG_JOIN_ROUND' ),
                             ( 'VG_PATH_DATATYPE_S_8',  6, 'VG_CAP_SQUARE', 'VG_JOIN_BEVEL' ),
                             ( 'VG_PATH_DATATYPE_S_16', 2, 'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                             ( 'VG_PATH_DATATYPE_S_16', 4, 'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                             ( 'VG_PATH_DATATYPE_S_16', 6, 'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                             ( 'VG_PATH_DATATYPE_S_32', 2, 'VG_CAP_SQUARE', 'VG_JOIN_ROUND' ),
                             ( 'VG_PATH_DATATYPE_S_32', 4, 'VG_CAP_BUTT',   'VG_JOIN_BEVEL' ),
                             ( 'VG_PATH_DATATYPE_S_32', 6, 'VG_CAP_ROUND',  'VG_JOIN_MITER' ),
                             ( 'VG_PATH_DATATYPE_F',    2, 'VG_CAP_SQUARE', 'VG_JOIN_MITER' ),
                             ( 'VG_PATH_DATATYPE_F',    4, 'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                             ( 'VG_PATH_DATATYPE_F',    6, 'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                             ( 'VG_PATH_DATATYPE_S_16', 2, 'VG_CAP_SQUARE', 'VG_JOIN_MITER' ) ],
                   'DrawGlyphMode' :
                           [ 'DrawGlyph', 'DrawGlyphs' ]
                       }
        
    def __init__( self, DataType, LineWidth, CapStyle, JoinStyle, DrawGlyphMode ):
        vt.VisualTestCase.__init__( self )
        self.DATATYPE      = DataType
        self.LineWidth     = LineWidth
        self.CapStyle      = CapStyle
        self.JoinStyle     = JoinStyle
        self.DrawGlyphMode = DrawGlyphMode
        self.repeats   = 1
        self.name = 'OPENVG DrawGlyph filled %s %s %s %s %s' % ( DataType, LineWidth, CapStyle, JoinStyle, DrawGlyphMode, )
        
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
                
        OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
        OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )
        
        # Create paint 
        Paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( Paint1Index )
        OpenVG.ColorPaint( Paint1Index, [ 0.5, 0.5, 0.1, 1.0 ] )
                
        Paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( Paint2Index )
        OpenVG.ColorPaint( Paint2Index, [ 0.0, 0.5, 0.5, 1.0 ] )
                
        # Create a font
        FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
        OpenVG.CreateFont( FontIndex, 0 )

        # letter N
        Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
        
        OpenVG.MoveTo( PathData1Index, [ 0, 0 ], 'OFF' )
        OpenVG.Lines( PathData1Index, [ 0, 40 ], 'ON' )
        OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
        OpenVG.Lines( PathData1Index, [ 25, -25 ], 'ON' )
        OpenVG.Lines( PathData1Index, [ 0, 25 ], 'ON' )
        OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
        OpenVG.Lines( PathData1Index, [ 0, -40 ], 'ON' )
        OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
        OpenVG.Lines( PathData1Index, [ -25, 25 ], 'ON' )
        OpenVG.Lines( PathData1Index, [ 0, -25 ], 'ON' )
        OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
        Glyph1Index = 0
        OpenVG.SetGlyphToPath( FontIndex, Glyph1Index, Path1Index, 'OFF', [ 0.0, 0.0 ], [ 62.5, 0 ] )
                
        # letter O
        Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
        OpenVG.MoveTo( PathData2Index, [ 0, 0 ], 'OFF' )
        OpenVG.Lines( PathData2Index, [ 40, 0 ], 'ON' )
        OpenVG.Lines( PathData2Index,[ 0, 40 ], 'ON' )
        OpenVG.Lines( PathData2Index,[ -40, 0 ], 'ON' )
        OpenVG.Lines( PathData2Index,[ 0, -40 ], 'ON' )
        OpenVG.MoveTo( PathData2Index, [ 10, 10 ], 'OFF' )
        OpenVG.Lines( PathData2Index, [ 20, 0 ], 'ON' )
        OpenVG.Lines( PathData2Index,[ 0, 20 ], 'ON' )
        OpenVG.Lines( PathData2Index,[ -20, 0 ], 'ON' )
        OpenVG.Lines( PathData2Index,[ 0, -20 ], 'ON' )
        OpenVG.ClosePath( PathData2Index )
        Glyph2Index = 1
        OpenVG.SetGlyphToPath( FontIndex, Glyph2Index, Path2Index, 'OFF', [ 0.0, 0.0 ], [ 47.5, 0 ] )
                
        # Letter K
        Path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( Path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        PathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( PathData3Index, self.DATATYPE, 1, 1 )
        
        OpenVG.MoveTo( PathData3Index, [ 0, 0 ], 'OFF' )
        OpenVG.Lines( PathData3Index, [ 0, 40 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ 15, 0 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ 20, 40 / 3 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ 20, 0 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ -25,-20 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ 25, -20 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ -20, 0 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ -20,15 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
        OpenVG.Lines( PathData3Index, [ -15, 0 ], 'ON' )
        Glyph3Index = 2
        OpenVG.SetGlyphToPath( FontIndex, Glyph3Index, Path3Index, 'OFF', [ 0.0, 0.0 ], [ 60, 0 ] )
                
        # letter I
        Path4Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( Path4Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        PathData4Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
        OpenVG.CreatePathData( PathData4Index, self.DATATYPE, 1, 1 )
        
        OpenVG.MoveTo( PathData4Index, [ 0, 0 ], 'OFF' )
        OpenVG.Lines( PathData4Index, [ 0, 40 ], 'ON' )
        OpenVG.Lines( PathData4Index, [ 15, 0 ], 'ON' )
        OpenVG.Lines( PathData4Index, [ 0, -40 ], 'ON' )
        OpenVG.Lines( PathData4Index, [ -15, 0 ], 'ON' )
        Glyph4Index = 3
        OpenVG.SetGlyphToPath( FontIndex, Glyph4Index, Path4Index, 'OFF', [ 0, 0.0 ], [ 20, 0 ] )
                
        # letter A
        Path5Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( Path5Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

        PathData5Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
        OpenVG.CreatePathData( PathData5Index, self.DATATYPE, 1, 1 )
        
        OpenVG.MoveTo( PathData5Index, [ 0, 0 ], 'OFF' )
        OpenVG.Lines( PathData5Index, [ 40 / 3, 40 ], 'ON' )
        OpenVG.Lines( PathData5Index, [ 40 / 3, 0 ], 'ON' )
        OpenVG.Lines( PathData5Index, [ 40 / 3, -40 ], 'ON' )
        OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
        OpenVG.Lines( PathData5Index, [ 0, 40 / 6 ], 'ON' )
        OpenVG.Lines( PathData5Index, [ -10, 0 ], 'ON' )
        OpenVG.Lines( PathData5Index, [ 0, -40 / 6 ], 'ON' )
        OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
        OpenVG.MoveTo( PathData5Index, [ 16, 15 ], 'OFF' )
        OpenVG.Lines( PathData5Index, [ 7.5, 0 ], 'ON' )
        OpenVG.Lines( PathData5Index, [ 0, 10 ], 'ON' )
        OpenVG.Lines( PathData5Index, [ -7.5, 0 ], 'ON' )
        OpenVG.Lines( PathData5Index, [ 0, -10 ], 'ON' )
        Glyph5Index = 4
        OpenVG.SetGlyphToPath( FontIndex, Glyph5Index, Path5Index, 'OFF', [ 0, 0.0 ], [ 60, 0 ] )
        
        # append path data
        OpenVG.AppendPathData( Path1Index, PathData1Index )
        OpenVG.AppendPathData( Path2Index, PathData2Index )
        OpenVG.AppendPathData( Path3Index, PathData3Index )
        OpenVG.AppendPathData( Path4Index, PathData4Index )
        OpenVG.AppendPathData( Path5Index, PathData5Index )

        # Define glypth indices
        GlyphCount   = 5
        GlyphIndices = []
        for i in range( GlyphCount ):
                GlyphIndices.extend( [ i ] )

        # ----------------------------------------------------------------------                
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        # Set paint into use
        OpenVG.SetPaint( Paint2Index, [ 'VG_FILL_PATH' ] )
        OpenVG.SetPaint( Paint1Index, [ 'VG_STROKE_PATH' ] )
                
        # Count starting point of first letter
        GlyphOriginX = WIDTH / GlyphCount - 40
        GlyphOriginY = HEIGHT / 2
        
        # Set start point(  Glyph origin )
        OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )
                
        # Draw Glyph
        if self.DrawGlyphMode == 'DrawGlyph':
                OpenVG.DrawGlyph( FontIndex, Glyph1Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ], 'ON' )
                OpenVG.DrawGlyph( FontIndex, Glyph2Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ], 'ON' )
                OpenVG.DrawGlyph( FontIndex, Glyph3Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ], 'ON' )
                OpenVG.DrawGlyph( FontIndex, Glyph4Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ], 'ON' )
                OpenVG.DrawGlyph( FontIndex, Glyph5Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ], 'ON' )

        # Draw Glyphs
        if self.DrawGlyphMode ==  'DrawGlyphs':
                OpenVG.DrawGlyphs( FontIndex,
                                   GlyphCount,
                                   GlyphIndices,
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ],
                                   'ON' )
                
        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
######################################################################
class DrawGlyphsStrokedOverlapping( vt.VisualTestCase ):
        
        ''' 
        The test description: This test is for testing drawing of overlapped
        glyphs with differing drawing modes.
        
        The test steps:
        1. The test creates paint and font.
        2. The test creates for path data forming letter O.
        3. The test set glyphs for created paths.
        4. The test buffer full with that glyph (outerlines are overlapping) with
        differing drawing modes ( testValues ).
        
        Expected output: Grid appears.
        '''
        
        testValues = { ( 'DataType', 'LineWidth', 'CapStyle', 'JoinStyle' ) :
                               [ ( 'VG_PATH_DATATYPE_S_8',  2,  'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_S_8',  5,  'VG_CAP_ROUND',  'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_8',  10, 'VG_CAP_SQUARE', 'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 2,  'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 5,  'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 10, 'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 2,  'VG_CAP_SQUARE', 'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 5,  'VG_CAP_BUTT',   'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 10, 'VG_CAP_ROUND',  'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_F',    5,  'VG_CAP_SQUARE', 'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_F',    2,  'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_F',    10, 'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 2,  'VG_CAP_SQUARE', 'VG_JOIN_MITER' ) ],
                       'DrawGlyphMode' :
                               [ 'DrawGlyph', 'DrawGlyphs' ]
                       }
        
        def __init__( self, DataType, LineWidth, CapStyle, JoinStyle, DrawGlyphMode ):
                vt.VisualTestCase.__init__( self )
                self.DATATYPE      = DataType
                self.LineWidth     = LineWidth
                self.CapStyle      = CapStyle
                self.JoinStyle     = JoinStyle
                self.DrawGlyphMode = DrawGlyphMode
                self.repeats       = 1
                self.name = 'OPENVG DrawGlyph stroked (overlap) %s %s %s %s %s' % ( DataType, LineWidth, CapStyle, JoinStyle, DrawGlyphMode, )
    
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                               
                # Set bg color
                OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )
                
                # Define letter width
                LetterWidth = 40

                RedColor = ( [ 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.0, 0.1, 0.2, 0.3, 0.4,
                               0.5, 0.6, 0.7, 0.8, 0.9, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
                               0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.0, 0.1, 0.2, 0.3, 0.4,
                               0.5, 0.6, 0.7, 0.8, 0.9, 0.0 ] )
                GreenColor = ( [ 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0, 0.1, 0.2, 0.3, 0.4,
                                 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1,
                                 0.0, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0.9, 0.8, 0.7,
                                 0.6, 0.5, 0.4, 0.3, 0.2, 0.1 ] ) 
                BlueColor = ( [ 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0.9, 0.8, 0.7, 0.6,
                                0.5, 0.4, 0.3, 0.2, 0.1, 0.0, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8,
                                0.9, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.0, 0.1, 0.2, 0.3,
                                0.4, 0.5, 0.6, 0.7, 0.8, 0.9 ] ) 
                
                # Create paint 
                paints = []
                c      = 0
                for i in range( HEIGHT / LetterWidth ):
                        for j in range( WIDTH / LetterWidth ):
                                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                                OpenVG.CreatePaint( paintIndex )
                                
                                OpenVG.ColorPaint( paintIndex, [ RedColor[ c ], GreenColor[ c ], BlueColor[ c ], 1.0 ] )

                                paints.append( paintIndex )
                                c += 1
                
                # Create a font
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )

                # letter O
                PathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( PathIndex, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathDataIndex, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathDataIndex, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathDataIndex, [ LetterWidth, 0 ], 'ON' )
                OpenVG.Lines( PathDataIndex, [ 0, LetterWidth ],'ON' )
                OpenVG.Lines( PathDataIndex, [ -LetterWidth, 0 ],'ON' )
                OpenVG.Lines( PathDataIndex, [ 0, -LetterWidth ],'ON' )
                OpenVG.ClosePath( PathDataIndex )
                GlyphIndex = 0
                OpenVG.SetGlyphToPath( FontIndex, GlyphIndex, PathIndex, 'OFF', [ 0.0, 0.0 ], [ LetterWidth + 15, 0 ] )
                
                # append path data
                OpenVG.AppendPathData( PathIndex, PathDataIndex )
                
                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.Clear( 0, 0, 0, 0 )     
                GlyphOriginY = 0
        
                c = 0
                for i in range( HEIGHT / LetterWidth ):
                        GlyphOriginX = 0
                        for j in range( WIDTH / LetterWidth ):
                                # Set paint into use
                                OpenVG.SetPaint( paints[ c ], [ 'VG_STROKE_PATH' ] )
                                c += 1
                                
                                # Set start point(  Glyph origin )
                                OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )
                        
                                # Draw Glyph
                                OpenVG.DrawGlyph( FontIndex, GlyphIndex, [ 'VG_STROKE_PATH' ], 'ON' )
                        
                                GlyphOriginX += LetterWidth
                        
                        GlyphOriginY += LetterWidth

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                        

######################################################################                
class DrawGlyphsFilledOverlapping( vt.VisualTestCase ):
        
        ''' 
        The test description: This test is for testing drawing of overlapped glyphs
        with differing drawing modes.
        
        The test steps:
        1. The test creates paint and font.
        2. The test creates for path data forming a box. 
        3. The test set a glyph for created paths.
        4. The test buffer full with that glyph (outerlines are overlapping) with differing
        drawing modes ( testValues ).
        
        Expected output: Grid with different color squares.
        '''
        
        testValues = { ( 'DataType', 'LineWidth', 'CapStyle', 'JoinStyle') :
                               [ ( 'VG_PATH_DATATYPE_S_8',  2,  'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_S_8',  5,  'VG_CAP_ROUND',  'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_8',  10, 'VG_CAP_SQUARE', 'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 2,  'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 5,  'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 10, 'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 2,  'VG_CAP_SQUARE', 'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 5,  'VG_CAP_BUTT',   'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 10, 'VG_CAP_ROUND',  'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_F',    5,  'VG_CAP_SQUARE', 'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_F',    2,  'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_F',    10, 'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 2,  'VG_CAP_SQUARE', 'VG_JOIN_MITER' ) ],
                       'DrawGlyphMode' :
                               [ 'DrawGlyph', 'DrawGlyphs' ]
                       }
        
        def __init__( self, DataType, LineWidth, CapStyle, JoinStyle, DrawGlyphMode ):
                vt.VisualTestCase.__init__( self )
                self.DATATYPE      = DataType
                self.LineWidth     = LineWidth
                self.CapStyle      = CapStyle
                self.JoinStyle     = JoinStyle
                self.DrawGlyphMode = DrawGlyphMode
                self.repeats       = 1                
                self.name = 'OPENVG DrawGlyph filled (overlap) %s %s %s %s %s' % ( DataType, LineWidth, CapStyle, JoinStyle, DrawGlyphMode, )
    
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                               
                OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )
                
                # Define letter width
                LetterWidth = 40

                RedColor = ( [ 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.0,
                               0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.0, 0.1,
                               0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.0, 0.1, 0.2,
                               0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.0, 0.1, 0.2, 0.3,
                               0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.0 ] )
                GreenColor = ( [ 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0,
                                 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0.9,
                                 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0, 0.0, 0.1,
                                 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0.9, 0.8,
                                 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1 ] ) 
                BlueColor = ( [ 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0,
                                0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0, 0.0,
                                0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.0, 0.1,
                                0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.0, 0.1, 0.2,
                                0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9 ] ) 
                
                # Create paint
                paints = []
                c      = 0
                for i in range( HEIGHT / LetterWidth ):
                        for j in range( WIDTH / LetterWidth ):
                                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                                OpenVG.CreatePaint( paintIndex )
                                OpenVG.ColorPaint( paintIndex, [ RedColor[ c ], GreenColor[ c ], BlueColor[ c ], 1.0 ] )

                                paints.append( paintIndex )
                                c += 1
                
                # Create a font
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )

                # letter O
                PathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( PathIndex, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathDataIndex, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathDataIndex, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathDataIndex, [ LetterWidth, 0 ], 'ON' )
                OpenVG.Lines( PathDataIndex, [ 0,LetterWidth ], 'ON' )
                OpenVG.Lines( PathDataIndex, [ -LetterWidth, 0 ], 'ON' )
                OpenVG.Lines( PathDataIndex, [ 0, -LetterWidth ], 'ON' )
                OpenVG.ClosePath( PathDataIndex )
                
                GlyphIndex = 0
                OpenVG.SetGlyphToPath( FontIndex, GlyphIndex, PathIndex, 'OFF', [ 0.0, 0.0 ], [ LetterWidth + 15, 0 ] )
                
                # append path data
                OpenVG.AppendPathData( PathIndex, PathDataIndex )
                
                # ----------------------------------------------------------------------                
                self.beginBenchmarkActions()
                
                OpenVG.Clear( 0, 0, 0, 0 )
                GlyphOriginY = 0
        
                c = 0
                for i in range( HEIGHT / LetterWidth ):
                        GlyphOriginX = 0
                        for j in range( WIDTH / LetterWidth ):
                                # Set paint into use
                                OpenVG.SetPaint( paints[ c ], [ 'VG_FILL_PATH' ] )
                                c += 1
                                
                                # Set start point(  Glyph origin )
                                OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )
                        
                                # Draw Glyph
                                OpenVG.DrawGlyph( FontIndex, GlyphIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ], 'ON' )
                        
                                GlyphOriginX += LetterWidth
                        
                        GlyphOriginY += LetterWidth

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                  

######################################################################
class DrawSeveralGlyphsOverlapping( vt.VisualTestCase ):
        
        ''' 
        The test description: This test is for testing drawing of glyphs with
        differing drawing modes.
        
        The test steps:
        1. The test creates paint and font.
        2. The test creates for path data forming a letter.
        3. The test set glyphs for created paths.
        4. The test draws glyphs with differing drawing modes ( testValues ).
        
        Expected output: Overlapping grid.
        '''

        testValues = { ('DataType', 'LineWidth', 'CapStyle', 'JoinStyle') :
                               [ ( 'VG_PATH_DATATYPE_S_8',   2, 'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_S_8',   5, 'VG_CAP_ROUND',  'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_8',  10, 'VG_CAP_SQUARE', 'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16',  2, 'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16',  5, 'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 10, 'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_32',  2, 'VG_CAP_SQUARE', 'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_32',  5, 'VG_CAP_BUTT',   'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 10, 'VG_CAP_ROUND',  'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_F',     5, 'VG_CAP_SQUARE', 'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_F',     2, 'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_F',    10, 'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16',  2, 'VG_CAP_SQUARE', 'VG_JOIN_MITER' ) ],
                       }
        
        def __init__( self, DataType, LineWidth, CapStyle, JoinStyle):
                vt.VisualTestCase.__init__( self )
                self.DATATYPE  = DataType
                self.LineWidth = LineWidth
                self.CapStyle  = CapStyle
                self.JoinStyle = JoinStyle
                self.repeats   = 1
                self.name = 'OPENVG DrawGlyph stroked %s %s %s %s' % ( DataType, LineWidth, CapStyle, JoinStyle, )
           
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                                
                OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )
        
                # Create paint 
                PaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( PaintIndex )
                OpenVG.ColorPaint( PaintIndex, [ 0.5, 0.5, 0.1, 1.0 ] )
                
                # Create a font
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )
                
                GlyphCount = 2
                        
                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData1Index, [ 50, 0 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ 0, 100 ], 'ON' )

                OpenVG.SetGlyphToPath( FontIndex, 0, Path1Index, 'OFF', [ 0, 0.0 ], [ 0, 0 ] )
                        
                # append path data
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )                
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData2Index, [ 0, 50 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 100, 0 ], 'ON' )

                OpenVG.SetGlyphToPath( FontIndex, 1, Path2Index, 'OFF', [ 0, 0.0 ], [ 0, 0 ] )
                        
                # append path data
                OpenVG.AppendPathData( Path2Index, PathData2Index )
                
                # Define glypth indices
                GlyphIndices = []
                for i in range( GlyphCount ):
                        GlyphIndices.extend( [ i ] )
                
                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.Clear(0,0,0,0)
                OpenVG.SetPaint( PaintIndex, [ 'VG_STROKE_PATH' ] )
                
                GlyphOriginY = 20
                for x in range( 2 ):
                        GlyphOriginX = 10
                        GlyphOriginY += x * ( HEIGHT / 2 )
                
                        for i in range( 2 ):
                                GlyphOriginX += i * ( WIDTH  / 2 )
                                
                                for j in range( 2 ):
                                        OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )
                                        OpenVG.DrawGlyph( FontIndex, j, [ 'VG_STROKE_PATH' ], 'ON' )

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################                 
class SetGlyphToPathOrig( vt.VisualTestCase ):
        
        ''' 
        The test description: This test is for testing SetGlyphToPath functions
        glyph_origin.
        
        The test steps: 
        1. The test draws letter A and a custom figure that is suitable demonstrate the
        functionality.
        2. The test alters the glyph_origin x and y values based on testValues.
        3. The test draws the glyphs.

        Expected output: "A" letter along with custom figure is drawn, the custom figure
        should move in relation to letter A, depending glyph_origins X and Y values.
        '''
        
        testValues = { 'OrigX' : [ 0, 10, 20, 30 ],
                       'OrigY' : [ 0, 10, 20, 30 ]
                       }
        
        def __init__( self, OrigX, OrigY ):
                vt.VisualTestCase.__init__( self )
                self.DATATYPE  = 'VG_PATH_DATATYPE_S_32'
                self.LineWidth = 4
                self.CapStyle  = 'VG_CAP_ROUND'
                self.JoinStyle = 'VG_JOIN_MITER'
                self.OrigX     = OrigX
                self.OrigY     = OrigY
                self.repeats   = 1                
                self.name = 'OPENVG DrawGlyphs glyph origin %s %s' % ( OrigX, OrigY, ) 
                
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]

                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()
                
                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                               
                OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )
        
                # Create paint 
                PaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint(PaintIndex)
                OpenVG.ColorPaint( PaintIndex, [ 0.5, 0.5, 0.1, 1.0 ] )
                
                # Create a font
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )
                
                # Define letter width
                LetterWidth = 40

                # letter A
                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData1Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ LetterWidth / 2, LetterWidth ], 'ON' )
                OpenVG.Lines( PathData1Index, [ LetterWidth / 2, -LetterWidth ], 'ON' )
                OpenVG.MoveTo( PathData1Index, [ LetterWidth/ 4, LetterWidth / 2 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ LetterWidth / 2, 0 ], 'ON' )
                OpenVG.MoveTo( PathData1Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ 100, 0 ], 'ON' )
                
                Glyph1Index = 0
                OpenVG.SetGlyphToPath( FontIndex, Glyph1Index, Path1Index, 'OFF', [ 0, 0.0 ], [ LetterWidth + 15,0 ] )
                
                # Some figure
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData2Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 30, 0 ], 'OFF' )
                OpenVG.Arcs( PathData2Index, [ 20, 70, 0, 10, 30 ], 'SCWARC', 'OFF' )
                OpenVG.Arcs( PathData2Index, [ 20, 20, 0, 40, 55 ], 'SCWARC', 'OFF')
                OpenVG.Arcs( PathData2Index, [-20, -20, 0, 10, 30], 'SCWARC', 'OFF' )

                GlyphIndex2 = 1
                OpenVG.SetGlyphToPath( FontIndex, GlyphIndex2, Path2Index, 'OFF', [ self.OrigX, self.OrigY ], [ LetterWidth + 15,0 ] )
                        
                # append path data
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )

                # Define glypth indices
                GlyphCount   = 2
                GlyphIndices = []
                for i in range( GlyphCount ):
                        GlyphIndices.extend( [ i ] )
                
                #----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( PaintIndex, [ 'VG_STROKE_PATH' ] )
                
                # Count starting point of first letter
                GlyphOriginX = ( WIDTH / GlyphCount ) - LetterWidth
                GlyphOriginY = HEIGHT / 2
        
                # Set start point(  Glyph origin )
                OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )
        
                # Draw the glyphs
                OpenVG.DrawGlyphs( FontIndex,
                                   GlyphCount,
                                   GlyphIndices,
                                   [ 0.0, 0.0, 0.0, 0.0 ],
                                   [ 0.0, 0.0, 0.0, 0.0 ],
                                   [ 'VG_STROKE_PATH' ],
                                   'OFF' )
                
                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################                
class SetGlyphToPathEscapement( vt.VisualTestCase ):
        
        ''' 
        The test description: This test is for testing SetGlyphToPath
        functions escapement.
        
        The test steps: 
        1. The test draws letters N,O,K,I,A.
        2. The test alters the escapement x and y values based on testValues.
        3. The test draws the glyphs to the surface.

        Expected output: Text NOKIA should appear. Letters relational placement
        against each other alters based on testValues.
        '''
        
        testValues = { 'EscapementX' : [ 0, 2,   6,  -2,  -6 ],
                       'EscapementY' : [ 0, 16, 24, -16, -24 ],
                       }
        
        def __init__( self, EscapementX, EscapementY ):
                vt.VisualTestCase.__init__( self )
                self.DATATYPE    = 'VG_PATH_DATATYPE_S_32'
                self.LineWidth   = 4
                self.CapStyle    = 'VG_CAP_ROUND'
                self.JoinStyle   = 'VG_JOIN_MITER'
                self.EscapementX = EscapementX
                self.EscapementY = EscapementY
                self.repeats     = 1                
                self.name = 'OPENVG DrawGlyphs escapement %s %s' % ( EscapementX, EscapementY, )

        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]

                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                # Set bg color
                OpenVG.ClearColor( [0.25,0.0,0.5,1.0] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )
                
                # Create paint 
                PaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( PaintIndex )
                OpenVG.ColorPaint( PaintIndex, [ 0.5, 0.5, 0.1, 1.0 ] )
                
                # Create a font
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )
                
                LetterWidth = 40

                # Letter N
                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData1Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ 0.0, LetterWidth ], 'ON' )
                OpenVG.Lines( PathData1Index, [ LetterWidth, -LetterWidth ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0.0, LetterWidth ], 'ON' )
                Glyph1Index = 0
                OpenVG.SetGlyphToPath( FontIndex,
                                       Glyph1Index,
                                       Path1Index,
                                       'OFF',
                                       [ 0, 0.0 ],
                                       [ LetterWidth + 15 + self.EscapementX, self.EscapementY ] )
                
                # Letter 0
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData2Index, [ 0, LetterWidth / 2 ], 'OFF' )
                OpenVG.Arcs( PathData2Index, [ LetterWidth / 2, LetterWidth / 2, 0, LetterWidth, 0 ], 'SCWARC', 'ON' )
                OpenVG.Arcs( PathData2Index, [ -LetterWidth / 2, -LetterWidth / 2, 0, -LetterWidth, -0 ], 'SCWARC', 'ON' )
                Glyph2Index = 1
                OpenVG.SetGlyphToPath( FontIndex,
                                       Glyph2Index,
                                       Path2Index,
                                       'OFF',
                                       [ 0, 0 ],
                                       [ LetterWidth + 15 + self.EscapementX, self.EscapementY ] )
                
                # Letter K
                Path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData3Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData3Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ 0, LetterWidth ], 'ON' )
                OpenVG.MoveTo( PathData3Index, [ 0, LetterWidth / 2 ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ LetterWidth, LetterWidth / 2], 'ON' )
                OpenVG.MoveTo( PathData3Index, [ 0, LetterWidth / 2 ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ LetterWidth, -LetterWidth / 2], 'ON' )
                Glyph3Index = 2
                OpenVG.SetGlyphToPath( FontIndex,
                                       Glyph3Index,
                                       Path3Index,
                                       'OFF',
                                       [ 0, 0 ],
                                       [ LetterWidth + 15 + self.EscapementX, self.EscapementY ] )
                
                # Letter I
                Path4Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path4Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData4Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData4Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData4Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData4Index, [ 0, LetterWidth ], 'ON' )
                Glyph4Index = 3
                OpenVG.SetGlyphToPath( FontIndex,
                                       Glyph4Index,
                                       Path4Index,
                                       'OFF',
                                       [ 0, 0 ],
                                       [ LetterWidth - ( LetterWidth - 15 ) + self.EscapementX, self.EscapementY ] )
                
                # letter A
                Path5Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path5Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData5Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData5Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData5Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ LetterWidth / 2, LetterWidth ], 'ON' )
                OpenVG.Lines( PathData5Index, [ LetterWidth / 2, -LetterWidth ], 'ON' )
                OpenVG.MoveTo( PathData5Index, [ LetterWidth / 4, LetterWidth / 2 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ LetterWidth / 2, 0 ], 'ON' )
                Glyph5Index = 4
                OpenVG.SetGlyphToPath( FontIndex,
                                       Glyph5Index,
                                       Path5Index,
                                       'OFF',
                                       [ 0, 0.0 ],
                                       [ LetterWidth + 15 + self.EscapementX , self.EscapementY ] )
        
                # append path data
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )
                OpenVG.AppendPathData( Path3Index, PathData3Index )
                OpenVG.AppendPathData( Path4Index, PathData4Index )
                OpenVG.AppendPathData( Path5Index, PathData5Index )

                # Define glypth indices
                GlyphCount   = 5
                GlyphIndices = []
                for i in range( GlyphCount ):
                        GlyphIndices.extend( [ i ] )
                
                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()

                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( PaintIndex, [ 'VG_STROKE_PATH' ] )
                
                # Count starting point of first letter
                GlyphOriginX = ( WIDTH / GlyphCount ) - LetterWidth
                GlyphOriginY = HEIGHT / 2
        
                # Set start point(  Glyph origin )
                OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )
        
                # Draw the glyphs
                OpenVG.DrawGlyphs( FontIndex,
                                   GlyphCount,
                                   GlyphIndices,
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 'VG_STROKE_PATH' ],
                                   'OFF' )

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################                
class DrawGlyphsFiguresSeveralGlyphs( vt.VisualTestCase ):
        
        '''
        The test description: This test is for testing drawing of glyphs
        with differing drawing modes.
        
        The test steps:
        1. The test creates paint and font.
        2. The test creates 12 paths.
        3. The test set some data (lines) to each path.
        3. The test set glyphs for created paths.
        4. The test draws glyphs with differing drawing modes ( testValues ).
        
        Expected output: The test draws glyphs repeating surtain pattern.
        '''
        
        testValues = { ( 'DataType', 'LineWidth', 'CapStyle', 'JoinStyle' ) :
                               [ ( 'VG_PATH_DATATYPE_S_8',  2, 'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_S_8',  5, 'VG_CAP_ROUND',  'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_8',  8, 'VG_CAP_SQUARE', 'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 2, 'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 5, 'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 8, 'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 2, 'VG_CAP_SQUARE', 'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 5, 'VG_CAP_BUTT',   'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 8, 'VG_CAP_ROUND',  'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_F',    5, 'VG_CAP_SQUARE', 'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_F',    2, 'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_F',    8, 'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 2, 'VG_CAP_SQUARE', 'VG_JOIN_MITER' ) ],
                       }
        
        def __init__( self, DataType, LineWidth, CapStyle, JoinStyle ):
                vt.VisualTestCase.__init__( self )
                self.DATATYPE      = DataType
                self.LineWidth     = LineWidth
                self.CapStyle      = CapStyle
                self.JoinStyle     = JoinStyle
                self.DrawGlyphMode = 'DrawGlyph'
                self.EscapementX   = 0.0
                self.EscapementY   = 0.0
                self.repeats       = 1                
                self.name = 'OPENVG DrawGlyphs stroked several glyphs %s %s %s %s' % ( DataType, LineWidth, CapStyle, JoinStyle, )
                
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]

                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                                
                OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )
        
                # Create paint 
                PaintIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePaint( PaintIndex )
                OpenVG.ColorPaint( PaintIndex, [ 0.5, 0.5, 0.1, 1.0 ] )
                
                # Create a font
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )
                
                # Create a lists for vertical and horizontal lines, PathIndex, GlyphIndex
                Vertical    = [ 10, -20, 10, -20, 10 ]
                Horizontal  = [ -20, 20, -20, 20, -20, 20, -20, 20, -20, 20, -20, 20, -20, 20, -20, 20 ]
                PathIndex   = []
                GlyphIndex  = []
                
                # Number of columns and rows
                Columns = 5
                Rows    = 16
                                                                
                # Create paths and pathdatas, draw data to path and set glyph  pointing to path and finally append path data
                paths  = []
                glyphs = []
                c      = 0
                for i in range( Rows ):
                        for j in range( Columns ):
                                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                                OpenVG.CreatePath( pathIndex, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                                OpenVG.CreatePathData( pathDataIndex, self.DATATYPE, 1, 1 )
                                
                                OpenVG.MoveTo( pathDataIndex, [ 0, 0 ], 'OFF' )
                                OpenVG.Lines( pathDataIndex, [ Vertical[ j ], Horizontal[ i ] ], 'ON' )
                                OpenVG.SetGlyphToPath( FontIndex, c, pathIndex, 'OFF', [ 0.0, 0.0 ], [ 15 , 0 ] )
                                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                                paths.append( pathIndex )
                                glyphs.append( c )
                                c += 1

                # Define glypth indices
                GlyphCount   = Rows * Columns
                GlyphIndices = []
                
                for i in range( GlyphCount ):
                        GlyphIndices.extend( [ i ] )

                # ----------------------------------------------------------------------                        
                self.beginBenchmarkActions()
                
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( PaintIndex, [ 'VG_STROKE_PATH' ] )
                
                # Count starting point of first letter
                GlyphOriginX = 10
                GlyphOriginY = HEIGHT - 30
        
                # Set start point(  Glyph origin )
                OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )

                # Draw Glyphs
                for j in range( 4 ):
                        for i in range( GlyphCount / 4 ):
                                OpenVG.DrawGlyph( FontIndex, GlyphIndices[ j * 4 + i ], [ 'VG_STROKE_PATH' ], 'ON' )
                                
                        OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY - ( ( j + 1 ) * 80 ) ] )
        
                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################                
class DrawGlyphsFiguresSingleGlyph( vt.VisualTestCase ):
        
        '''
        The test description: This test is for testing drawing of single glyph
        with lost of path data.
        
        The test steps:
        1. The test creates paint and font.
        2. The test creates a single path.
        3. The test draws 24 lines to single path.
        3. The test set glyph pointing to created path.
        4. The test draws glyph to the surface ( note that the path datatype and stroke type
        are varying based on testValues ).
        
        Expected output: The test draws a glyph which contains 24 lines
        forming figure of butterfly wings.
        '''
        
        testValues = { ( 'DataType', 'LineWidth', 'CapStyle', 'JoinStyle' ) :
                               [ ( 'VG_PATH_DATATYPE_S_8',  2, 'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_S_8',  5, 'VG_CAP_ROUND',  'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_8',  8, 'VG_CAP_SQUARE', 'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 2, 'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 5, 'VG_CAP_BUTT',   'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 8, 'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 2, 'VG_CAP_SQUARE', 'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 5, 'VG_CAP_BUTT',   'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_32', 8, 'VG_CAP_ROUND',  'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_F',    5, 'VG_CAP_SQUARE', 'VG_JOIN_MITER' ),
                                 ( 'VG_PATH_DATATYPE_F',    2, 'VG_CAP_BUTT',   'VG_JOIN_ROUND' ),
                                 ( 'VG_PATH_DATATYPE_F',    8, 'VG_CAP_ROUND',  'VG_JOIN_BEVEL' ),
                                 ( 'VG_PATH_DATATYPE_S_16', 2, 'VG_CAP_SQUARE', 'VG_JOIN_MITER' ) ],
                       }
        
        def __init__( self, DataType, LineWidth, CapStyle, JoinStyle):
                vt.VisualTestCase.__init__( self )
                self.DATATYPE      = DataType
                self.LineWidth     = LineWidth
                self.CapStyle      = CapStyle
                self.JoinStyle     = JoinStyle
                self.DrawGlyphMode = 'DrawGlyph'
                self.EscapementX   = 0.0
                self.EscapementY   = 0.0
                self.repeats       = 1                
                self.name = 'OPENVG DrawGlyphs stroked single glyph %s %s %s %s' % ( DataType, LineWidth, CapStyle, JoinStyle, )
                
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                
                OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )
        
                # Create paint 
                PaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( PaintIndex )
                OpenVG.ColorPaint( PaintIndex, [ 0.5, 0.5, 0.1, 1.0 ] )
                
                # Create a font
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )
                
                # Define needed index values
                PathIndex  = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                GlyphIndex = 0
        
                # Create a path
                OpenVG.CreatePath( PathIndex, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathDataIndex, self.DATATYPE, 1, 1 )
                
                # Create a lists for coordinate points
                ValuesX = []
                ValuesY = []
        
                # First set of 12 coordinate points
                ValuesX.extend( [ -100, -100, -100, -100, -100, -100, 100, 100, 100, 100, 100, 100 ] )
                ValuesY.extend( [ 0,-100, -80, -60, -40, -20, 0, 20, 40, 60 ,80, 100 ] )
                
                # Second set of 12 coordinate points ( this set is a "mirror" to previous set )
                ValuesX.extend( [ 100, 100, 100, 100, 100, 100, -100, -100, -100, -100, -100, -100 ] )
                ValuesY.extend( [ 0,-100, -80, -60, -40, -20, 0, 20, 40, 60 ,80, 100 ] )
                
                # Draw lines to path
                for i in range( 24 ):
                        OpenVG.MoveTo( PathDataIndex, [ 0, 0 ], 'OFF' )
                        OpenVG.Lines( PathDataIndex, [ ValuesX[ i ], ValuesY[ i ] ], 'ON' )
                
                # Set glyph pointing to created path
                OpenVG.SetGlyphToPath( FontIndex, GlyphIndex, PathIndex, 'OFF', [ 0.0, 0.0 ], [ 15 , 0 ] )
                # Append path data 
                OpenVG.AppendPathData( PathIndex, PathDataIndex )
                        
                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.Clear( 0, 0, 0, 0 )
                
                # Set paint into use
                OpenVG.SetPaint( PaintIndex, [ 'VG_STROKE_PATH' ] )
                
                # Count starting point
                GlyphOriginX = WIDTH / 2
                GlyphOriginY = HEIGHT / 2
        
                # Set start point(  Glyph origin )
                OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )

                # Draw a Glyph
                OpenVG.DrawGlyph( FontIndex, GlyphIndex, [ 'VG_STROKE_PATH' ], 'ON' )
        
                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################                
class MatrixGlyphUserToSurfaceRotate( vt.VisualTestCase ):
        
        '''
        The purpose of the test: This test is for testing
        MatrixGlyphUserToSurface ( Rotation + Translate ).
        
        The test steps:
        1. The test creates two paints. One for border lines, and another for fillings.
        2. The test creates paths for filled letters N, O, K, I, A.
        3. The test sets glyphs to paths.
        4. The test append path data.
        5. The test clears the surface.
        6. The test set MatrixMode to VG_MATRIX_GLYPH_USER_TO_SURFACE.
        7. The test set rotation and translation with values given in testValues.
        8. The test draws the glyphs.
        9. The test saves the image and destroy used resources.
        
        Expected output: Text NOKIA rotated by angle which is given in testValues.
        '''
                
        testValues = { ( 'RotationAngle', 'TranslationX', 'TranslationY') :
                               [ (   0.0,  10.0,   140 ),
                                 (  45.0,  45.0, 100.0 ),
                                 (  90.0, 140.0,  60.0 ),
                                 ( 135.0,   200,   100 ),
                                 (   180,   230,   180 ),
                                 ( 225.0, 180.0, 240.0 ),
                                 ( 270.0, 100.0, 280.0 ),
                                 ( 315.0,  30.0, 220.0 ) ]
                       }
        
        def __init__( self, RotationAngle, TranslationX, TranslationY):
                vt.VisualTestCase.__init__( self, )
                self.DATATYPE      = 'VG_PATH_DATATYPE_S_32'
                self.LineWidth     = 5
                self.CapStyle      = 'VG_CAP_BUTT'
                self.JoinStyle     = 'VG_JOIN_MITER'
                self.RotationAngle = RotationAngle
                self.TranslationX  = TranslationX
                self.TranslationY  = TranslationY
                self.repeats       = 1
                self.name = 'OPENVG MatrixGlyphUserToSurface (rotation) %s %s %s' % ( RotationAngle, TranslationX, TranslationY, )
    
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                                
                OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )

                Paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint1Index )
                OpenVG.ColorPaint( Paint1Index, [ 0.5, 0.5, 0.1, 1.0 ] )
                
                Paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint2Index )
                OpenVG.ColorPaint( Paint2Index, [ 0.0, 0.5, 0.5, 1.0 ] )
                
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )

                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData1Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 25, -25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, 25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -40 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -25, 25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
                Glyph1Index = 0
                OpenVG.SetGlyphToPath( FontIndex, Glyph1Index, Path1Index, 'OFF', [ 0.0, 0.0 ], [ 62.5, 0 ] )
                
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData2Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 40, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ -40, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, -40 ], 'ON' )
                OpenVG.MoveTo( PathData2Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 20, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, 20 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ -20, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, -20 ], 'ON' )
                OpenVG.ClosePath( PathData2Index )
                Glyph2Index = 1
                OpenVG.SetGlyphToPath( FontIndex, Glyph2Index, Path2Index, 'OFF', [ 0.0, 0.0 ], [ 47.5, 0 ] )
                
                Path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData3Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData3Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 20, 40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 20, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -25,-20 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 25, -20 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -20, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -20,15 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -15, 0 ], 'ON' )
                Glyph3Index = 2
                OpenVG.SetGlyphToPath( FontIndex, Glyph3Index, Path3Index, 'OFF', [ 0.0, 0.0 ], [ 60, 0 ] )
                
                Path4Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path4Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData4Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
                OpenVG.CreatePathData( PathData4Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData4Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData4Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ 0, -40 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ -15, 0 ], 'ON' )
                Glyph4Index = 3
                OpenVG.SetGlyphToPath( FontIndex, Glyph4Index, Path4Index, 'OFF', [ 0, 0.0 ], [ 20, 0 ] )
                
                Path5Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path5Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData5Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
                OpenVG.CreatePathData( PathData5Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData5Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, 40 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, -40 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, 40 / 6 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -10, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, -40 / 6 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
                OpenVG.MoveTo( PathData5Index, [ 16, 15 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ 7.5, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, 10 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -7.5, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, -10 ], 'ON' )
                Glyph5Index = 4
                OpenVG.SetGlyphToPath( FontIndex, Glyph5Index, Path5Index, 'OFF', [ 0, 0.0 ], [ 60, 0 ] )
        
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )
                OpenVG.AppendPathData( Path3Index, PathData3Index )
                OpenVG.AppendPathData( Path4Index, PathData4Index )
                OpenVG.AppendPathData( Path5Index, PathData5Index )

                GlyphCount   = 5
                GlyphIndices = []
                for i in range( GlyphCount ):
                        GlyphIndices.extend( [ i ] )

                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.MatrixMode( 'VG_MATRIX_GLYPH_USER_TO_SURFACE' )
                
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( Paint2Index, [ 'VG_FILL_PATH' ] )
                OpenVG.SetPaint( Paint1Index, [ 'VG_STROKE_PATH' ] )
                
                # Count starting point of first letter
                GlyphOriginX = ( WIDTH / GlyphCount ) - 40
                GlyphOriginY = HEIGHT / 2
        
                # Translate and rotate glyphs
                OpenVG.Translate( self.TranslationX, self.TranslationY )
                OpenVG.Rotate( self.RotationAngle )
                
                # Draw Glyphs
                OpenVG.DrawGlyphs( FontIndex,
                                   GlyphCount,
                                   GlyphIndices,
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 'VG_STROKE_PATH','VG_FILL_PATH' ],
                                   'OFF' )

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                

######################################################################                
class MatrixGlyphUserToSurfaceShear( vt.VisualTestCase ):
        
        '''
        The purpose of the test: This test is for testing MatrixGlyphUserToSurface
        ( Shear + Translate ).
        
        The test steps:
        1. The test creates two paints. One for border lines, and another for fillings.
        2. The test creates paths for filled letters N, O, K, I, A.
        3. The test sets glyphs to paths.
        4. The test append path data.
        5. The test clears the surface.
        6. The test set MatrixMode to VG_MATRIX_GLYPH_USER_TO_SURFACE.
        7. The test set shear and translation with values given in testValues.
        8. The test draws the glyphs.
        9. The test saves the image and destroy used resources.
        
        Expected output: Text NOKIA is sheared with values given in testValues.
        '''
        
        testValues = { ( 'TranslateX', 'TranslateY', 'ShearX', 'ShearY' ) :
                               [ (  0, 100,  0.5,   0.5 ),
                                 (  0, 100, 0.25,  0.25 ),
                                 (  0, 100, 0.25,   0.5 ),
                                 (  0, 100, 0.25,  0.75 ),
                                 (  0, 100, 0.75,  0.25 ),
                                 (  0, 100,  0.5,  0.25 ),
                                 ( 20, 100, -0.5,  0.25 ),
                                 (  0, 150,  0.5, -0.25 ),
                                 (  0, 180,  0.5, -0.50 ),
                                 (  0, 180, 0.25, -0.50 ) ],
                       }
        
        def __init__( self, TranslateX, TranslateY, ShearX, ShearY ):
                vt.VisualTestCase.__init__( self, )
                self.DATATYPE   = 'VG_PATH_DATATYPE_S_32'
                self.LineWidth  = 5
                self.CapStyle   = 'VG_CAP_BUTT'
                self.JoinStyle  = 'VG_JOIN_MITER'
                self.TranslateX = TranslateX
                self.TranslateY = TranslateY
                self.ShearX     = ShearX
                self.ShearY     = ShearY
                self.repeats    = 1
                self.name = 'OPENVG MatrixGlyphUserToSurface (shear) %s %s %s %s' % ( TranslateX, TranslateY, ShearX, ShearY, )
    
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )

                Paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint1Index )
                OpenVG.ColorPaint( Paint1Index, [ 0.5, 0.5, 0.1, 1.0 ] )
                
                Paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint2Index )
                OpenVG.ColorPaint( Paint2Index, [ 0.0, 0.5, 0.5, 1.0 ] )
                
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )

                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData1Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 25, -25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, 25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -40 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -25, 25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
                Glyph1Index = 0
                OpenVG.SetGlyphToPath( FontIndex, Glyph1Index, Path1Index, 'OFF', [ 0.0, 0.0 ], [ 62.5, 0 ] )
                
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData2Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 40, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ -40, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, -40 ], 'ON' )
                OpenVG.MoveTo( PathData2Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 20, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, 20 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ -20, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, -20 ], 'ON' )
                OpenVG.ClosePath( PathData2Index )
                Glyph2Index = 1
                OpenVG.SetGlyphToPath( FontIndex, Glyph2Index, Path2Index, 'OFF', [ 0.0, 0.0 ], [ 47.5, 0 ] )
                
                Path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData3Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData3Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 20, 40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 20, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -25,-20 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 25, -20 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -20, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -20,15 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -15, 0 ], 'ON' )
                Glyph3Index = 2
                OpenVG.SetGlyphToPath( FontIndex, Glyph3Index, Path3Index, 'OFF', [ 0.0, 0.0 ], [ 60, 0 ] )
                
                Path4Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path4Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData4Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
                OpenVG.CreatePathData( PathData4Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData4Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData4Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ 0, -40 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ -15, 0 ], 'ON' )
                Glyph4Index = 3
                OpenVG.SetGlyphToPath( FontIndex, Glyph4Index, Path4Index, 'OFF', [ 0, 0.0 ], [ 20, 0 ] )
                
                Path5Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path5Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData5Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
                OpenVG.CreatePathData( PathData5Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData5Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, 40 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, -40 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, 40 / 6 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -10, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, -40 / 6 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
                OpenVG.MoveTo( PathData5Index, [ 16, 15 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ 7.5, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, 10 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -7.5, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, -10 ], 'ON' )
                Glyph5Index = 4
                OpenVG.SetGlyphToPath( FontIndex, Glyph5Index, Path5Index, 'OFF', [ 0, 0.0 ], [ 60, 0 ] )
        
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )
                OpenVG.AppendPathData( Path3Index, PathData3Index )
                OpenVG.AppendPathData( Path4Index, PathData4Index )
                OpenVG.AppendPathData( Path5Index, PathData5Index )

                GlyphCount   = 5
                GlyphIndices = []
                for i in range( GlyphCount ):
                        GlyphIndices.extend( [ i ] )

                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.MatrixMode( 'VG_MATRIX_GLYPH_USER_TO_SURFACE' )
                
                OpenVG.Clear(0,0,0,0)
                OpenVG.SetPaint( Paint2Index, [ 'VG_FILL_PATH' ] )
                OpenVG.SetPaint( Paint1Index, [ 'VG_STROKE_PATH' ] )
                
                # Count starting point of first letter
                GlyphOriginX = ( WIDTH / GlyphCount ) - 40
                GlyphOriginY = HEIGHT / 2
        
                OpenVG.Translate( self.TranslateX, self.TranslateY )
                OpenVG.Shear( self.ShearX, self.ShearY )
                
                # Draw Glyphs
                OpenVG.DrawGlyphs( FontIndex,
                                   GlyphCount,
                                   GlyphIndices,
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 'VG_STROKE_PATH','VG_FILL_PATH' ],
                                   'OFF' )

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
######################################################################
class MatrixGlyphUserToSurfaceScale( vt.VisualTestCase ):
        
        '''
        The purpose of the test: This test is for testing MatrixGlyphUserToSurface
        ( Scale + Translate ).
        
        The test steps:
        1. The test creates two paints. One for border lines, and another for fillings.
        2. The test creates paths for filled letters N, O, K, I, A.
        3. The test sets glyphs to paths.
        4. The test append path data.
        5. The test clears the surface.
        6. The test set MatrixMode to VG_MATRIX_GLYPH_USER_TO_SURFACE.
        7. The test set scaling and translation with values given in testValues.
        8. The test draws the glyphs.
        9. The test saves the image and destroy used resources.
        
        Expected output: Text NOKIA is scaled with values given in testValues.
        '''
        
        testValues = { ( 'TranslateX', 'TranslateY', 'ScaleX', 'ScaleY' ) :
                               [ (  65, 150,  0.5,   0.5 ),
                                 (  90, 150, 0.25,  0.25 ),
                                 (  90, 150, 0.25,   0.5 ),
                                 (  90, 150, 0.25,  0.75 ),
                                 (  40, 150, 0.75,  0.25 ),
                                 (  60, 150,  0.5,  0.25 ),
                                 ( 180, 150, -0.5,  0.25 ),
                                 (  60, 150,  0.5, -0.25 ) ],
                       }
        
        def __init__( self, TranslateX, TranslateY, ScaleX, ScaleY ):
                vt.VisualTestCase.__init__( self, )
                self.DATATYPE   = 'VG_PATH_DATATYPE_S_32'
                self.LineWidth  = 5
                self.CapStyle   = 'VG_CAP_BUTT'
                self.JoinStyle  = 'VG_JOIN_MITER'
                self.TranslateX = TranslateX
                self.TranslateY = TranslateY
                self.ScaleX     = ScaleX
                self.ScaleY     = ScaleY
                self.repeats    = 1
                self.name = 'OPENVG MatrixGlyphUserToSurface (scale) %s %s %s %s' % ( TranslateX, TranslateY, ScaleX, ScaleY, )
    
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]

                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                
                OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )

                Paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint1Index )
                OpenVG.ColorPaint( Paint1Index, [ 0.5, 0.5, 0.1, 1.0 ] )
                
                Paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint2Index )
                OpenVG.ColorPaint( Paint2Index, [ 0.0, 0.5, 0.5, 1.0 ] )
                
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )

                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData1Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 25, -25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, 25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -40 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -25, 25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
                Glyph1Index = 0
                OpenVG.SetGlyphToPath( FontIndex, Glyph1Index, Path1Index, 'OFF', [ 0.0, 0.0 ], [ 62.5, 0 ] )
                
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData2Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 40, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ -40, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, -40 ], 'ON' )
                OpenVG.MoveTo( PathData2Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 20, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, 20 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ -20, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, -20 ], 'ON' )
                OpenVG.ClosePath( PathData2Index )
                Glyph2Index = 1
                OpenVG.SetGlyphToPath( FontIndex, Glyph2Index, Path2Index, 'OFF', [ 0.0, 0.0 ], [ 47.5, 0 ] )
                
                Path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData3Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData3Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 20, 40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 20, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -25,-20 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 25, -20 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -20, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -20,15 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -15, 0 ], 'ON' )
                Glyph3Index = 2
                OpenVG.SetGlyphToPath( FontIndex, Glyph3Index, Path3Index, 'OFF', [ 0.0, 0.0 ], [ 60, 0 ] )
                
                Path4Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path4Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData4Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
                OpenVG.CreatePathData( PathData4Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData4Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData4Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ 0, -40 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ -15, 0 ], 'ON' )
                Glyph4Index = 3
                OpenVG.SetGlyphToPath( FontIndex, Glyph4Index, Path4Index, 'OFF', [ 0, 0.0 ], [ 20, 0 ] )
                
                Path5Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path5Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData5Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
                OpenVG.CreatePathData( PathData5Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData5Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, 40 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, -40 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, 40 / 6 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -10, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, -40 / 6 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
                OpenVG.MoveTo( PathData5Index, [ 16, 15 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ 7.5, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, 10 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -7.5, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, -10 ], 'ON' )
                Glyph5Index = 4
                OpenVG.SetGlyphToPath( FontIndex, Glyph5Index, Path5Index, 'OFF', [ 0, 0.0 ], [ 60, 0 ] )
        
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )
                OpenVG.AppendPathData( Path3Index, PathData3Index )
                OpenVG.AppendPathData( Path4Index, PathData4Index )
                OpenVG.AppendPathData( Path5Index, PathData5Index )

                GlyphCount   = 5
                GlyphIndices = []
                for i in range( GlyphCount ):
                        GlyphIndices.extend( [ i ] )

                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.MatrixMode( 'VG_MATRIX_GLYPH_USER_TO_SURFACE' )
                
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( Paint2Index, [ 'VG_FILL_PATH' ] )
                OpenVG.SetPaint( Paint1Index, [ 'VG_STROKE_PATH' ] )
                
                # Count starting point of first letter
                GlyphOriginX = ( WIDTH / GlyphCount ) - 40
                GlyphOriginY = HEIGHT / 2
        
                OpenVG.Translate( self.TranslateX, self.TranslateY )
                OpenVG.Scale( self.ScaleX, self.ScaleY )
                
                # Draw Glyphs
                OpenVG.DrawGlyphs( FontIndex,
                                   GlyphCount,
                                   GlyphIndices,
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ],
                                   'OFF' )

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                

######################################################################                
class MatrixGlyphUserToSurfaceMultMatrix( vt.VisualTestCase ):
        
        '''
        The purpose of the test: This test is for testing MatrixGlyphUserToSurface
        ( MultMatrix ).
        
        The test steps:
        1. The test creates two paints. One for border lines, and another for fillings.
        2. The test creates paths for filled letters N, O, K, I, A.
        3. The test sets glyphs to paths.
        4. The test append path data.
        5. The test clears the surface.
        6. The test set MatrixMode to VG_MATRIX_GLYPH_USER_TO_SURFACE.
        7. The test creates an translation matrix. ( 9 float array, Tx and Ty values from testValues ).
        8. The test set scaling to 0.5, 0.5 in order to make some space to translate with MultMatrix.
        9. The test multiplies current matrix by previously created translation matrice.
        10. The test draws the glyphs.
        11. The test saves the image and destroy used resources.
        
        Expected output: Text NOKIA is translated by Tx and Ty values ( testValues ).
        '''
        
        testValues = { ( 'Tx', 'Ty') :
                               [ (   0,   0 ),
                                 (  50, 100 ),
                                 ( 100, 200 ),
                                 ( 200, 400 ), ],
                       }
        
        def __init__( self, Tx, Ty):
                vt.VisualTestCase.__init__( self, )
                self.DATATYPE  = 'VG_PATH_DATATYPE_S_32'
                self.LineWidth = 5
                self.CapStyle  = 'VG_CAP_BUTT'
                self.JoinStyle = 'VG_JOIN_MITER'
                self.Tx        = Tx
                self.Ty        = Ty
                self.repeats   = 1
                self.name = 'OPENVG MatrixGlyphUserToSurface (multmatrix) %s %s' % ( Tx, Ty, ) 
                
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]

                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )

                Paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint1Index )
                OpenVG.ColorPaint( Paint1Index, [ 0.5, 0.5, 0.1, 1.0 ] )
                
                Paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint2Index )
                OpenVG.ColorPaint( Paint2Index, [ 0.0, 0.5, 0.5, 1.0 ] )
                
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )

                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData1Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 25, -25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, 25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -40 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -25, 25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
                Glyph1Index = 0
                OpenVG.SetGlyphToPath( FontIndex, Glyph1Index, Path1Index, 'OFF', [ 0.0, 0.0 ], [ 62.5, 0 ] )
                
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData2Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 40, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ -40, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, -40 ], 'ON' )
                OpenVG.MoveTo( PathData2Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 20, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, 20 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ -20, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, -20 ], 'ON' )
                OpenVG.ClosePath( PathData2Index )
                Glyph2Index = 1
                OpenVG.SetGlyphToPath( FontIndex, Glyph2Index, Path2Index, 'OFF', [ 0.0, 0.0 ], [ 47.5, 0 ] )
                
                Path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData3Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData3Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 20, 40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 20, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -25,-20 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 25, -20 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -20, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -20,15 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -15, 0 ], 'ON' )
                Glyph3Index = 2
                OpenVG.SetGlyphToPath( FontIndex, Glyph3Index, Path3Index, 'OFF', [ 0.0, 0.0 ], [ 60, 0 ] )
                
                Path4Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path4Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData4Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
                OpenVG.CreatePathData( PathData4Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData4Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData4Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ 0, -40 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ -15, 0 ], 'ON' )
                Glyph4Index = 3
                OpenVG.SetGlyphToPath( FontIndex, Glyph4Index, Path4Index, 'OFF', [ 0, 0.0 ], [ 20, 0 ] )
                
                Path5Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path5Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData5Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
                OpenVG.CreatePathData( PathData5Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData5Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, 40 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, -40 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, 40 / 6 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -10, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, -40 / 6 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
                OpenVG.MoveTo( PathData5Index, [ 16, 15 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ 7.5, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, 10 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -7.5, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, -10 ], 'ON' )
                Glyph5Index = 4
                OpenVG.SetGlyphToPath( FontIndex, Glyph5Index, Path5Index, 'OFF', [ 0, 0.0 ], [ 60, 0 ] )
        
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )
                OpenVG.AppendPathData( Path3Index, PathData3Index )
                OpenVG.AppendPathData( Path4Index, PathData4Index )
                OpenVG.AppendPathData( Path5Index, PathData5Index )

                GlyphCount   = 5
                GlyphIndices = []
                for i in range( GlyphCount ):
                        GlyphIndices.extend( [ i ] )

                # ----------------------------------------------------------------------                        
                self.beginBenchmarkActions()
                
                OpenVG.MatrixMode( 'VG_MATRIX_GLYPH_USER_TO_SURFACE' )
                
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( Paint2Index, [ 'VG_FILL_PATH' ] )
                OpenVG.SetPaint( Paint1Index, [ 'VG_STROKE_PATH' ] )
                
                # Count starting point of first letter
                GlyphOriginX = ( WIDTH / GlyphCount ) - 40
                GlyphOriginY = HEIGHT / 2
                
                # Scale to 1/2, so there is space for translate
                OpenVG.Scale( 0.5, 0.5 )
                
                # Defining Translation Matrix
                Matrix = [     1.0,     0.0, 0.0,
                                0.0,     1.0, 0.0,
                            self.Tx, self.Ty, 1.0 ]
                
                # Multiple current matrix with defined translation matrix
                OpenVG.MultMatrix( Matrix )
                
                # Draw Glyphs
                OpenVG.DrawGlyphs( FontIndex,
                                   GlyphCount,
                                   GlyphIndices,
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ],
                                   'OFF' )

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
######################################################################
class MatrixGlyphUserToSurfaceLoadMatrix( vt.VisualTestCase ):
        
        '''
        The purpose of the test: This test is for testing MatrixGlyphUserToSurface
        (LoadMatrix).
        
        The test steps:
        1. The test creates two paints. One for border lines, and another for fillings.
        2. The test creates paths for filled letters N, O, K, I, A.
        3. The test sets glyphs to paths.
        4. The test append path data.
        5. The test clears the surface.
        6. The test set MatrixMode to VG_MATRIX_GLYPH_USER_TO_SURFACE.
        7. The test loads application defined matrix ( testvalues ) to current matrix.
        8. The test translate the result by 0, 100.
        10. The test draws the glyphs.
        11. The test saves the image and destroy used resources.
        
        Expected output: Text NOKIA size is varying ( 0.5x - 2x ) based on used matrix
        given in testValues.
        '''
        
        testValues = { 'Matrix' : 
                       [ [ 1.0, 0.0, 0.0,  0.0, 1.0, 0.0,  0.0, 0.0, 1.0 ],
                         [ 0.5, 0.0, 0.0,  0.0, 0.5, 0.0,  0.0, 0.0, 1.0 ],
                         [ 1.5, 0.0, 0.0,  0.0, 1.5, 0.0,  0.0, 0.0, 1.0 ],
                         [ 2.0, 0.0, 0.0,  0.0, 2.0, 0.0,  0.0, 0.0, 1.0 ],       
                         [ 1.0, 0.0, 0.0,  0.0, 2.0, 0.0,  0.0, 0.0, 1.0 ],       
                         [ 2.0, 0.0, 0.0,  0.0, 1.0, 0.0,  0.0, 0.0, 1.0 ], ],
                       }
        
        def __init__( self, Matrix ):
                vt.VisualTestCase.__init__( self, )
                self.DATATYPE  = 'VG_PATH_DATATYPE_S_32'
                self.LineWidth = 5
                self.CapStyle  = 'VG_CAP_BUTT'
                self.JoinStyle = 'VG_JOIN_MITER'
                self.Matrix    = Matrix
                self.repeats   = 1                
                self.name = 'OPENVG MatrixGlyphUserToSurface (loadmatrix) %s' % ( Matrix, ) 
    
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]

                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                

                OpenVG.ClearColor( [ 0.25, 0.0, 0.5, 1.0 ] )
                OpenVG.Stroke( self.LineWidth, self.CapStyle, self.JoinStyle, 0.0 )

                Paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint1Index )
                OpenVG.ColorPaint( Paint1Index, [ 0.5, 0.5, 0.1, 1.0 ] )
                
                Paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint2Index )
                OpenVG.ColorPaint( Paint2Index, [ 0.0, 0.5, 0.5, 1.0 ] )
                
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )

                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData1Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 25, -25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, 25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -40 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -25, 25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -25 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -15, 0 ], 'ON' )
                Glyph1Index = 0
                OpenVG.SetGlyphToPath( FontIndex, Glyph1Index, Path1Index, 'OFF', [ 0.0, 0.0 ], [ 62.5, 0 ] )
                
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData2Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 40, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ -40, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, -40 ], 'ON' )
                OpenVG.MoveTo( PathData2Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 20, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, 20 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ -20, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index,[ 0, -20 ], 'ON' )
                OpenVG.ClosePath( PathData2Index )
                Glyph2Index = 1
                OpenVG.SetGlyphToPath( FontIndex, Glyph2Index, Path2Index, 'OFF', [ 0.0, 0.0 ], [ 47.5, 0 ] )
                
                Path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData3Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData3Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 20, 40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 20, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -25,-20 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 25, -20 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -20, 0 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -20,15 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ 0, -40 / 3 ], 'ON' )
                OpenVG.Lines( PathData3Index, [ -15, 0 ], 'ON' )
                Glyph3Index = 2
                OpenVG.SetGlyphToPath( FontIndex, Glyph3Index, Path3Index, 'OFF', [ 0.0, 0.0 ], [ 60, 0 ] )
                
                Path4Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path4Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData4Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
                OpenVG.CreatePathData( PathData4Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData4Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData4Index, [ 0, 40 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ 15, 0 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ 0, -40 ], 'ON' )
                OpenVG.Lines( PathData4Index, [ -15, 0 ], 'ON' )
                Glyph4Index = 3
                OpenVG.SetGlyphToPath( FontIndex, Glyph4Index, Path4Index, 'OFF', [ 0, 0.0 ], [ 20, 0 ] )
                
                Path5Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path5Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData5Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
                OpenVG.CreatePathData( PathData5Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData5Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, 40 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 40 / 3, -40 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, 40 / 6 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -10, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, -40 / 6 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -40 / 3, 0 ], 'ON' )
                OpenVG.MoveTo( PathData5Index, [ 16, 15 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ 7.5, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, 10 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ -7.5, 0 ], 'ON' )
                OpenVG.Lines( PathData5Index, [ 0, -10 ], 'ON' )
                Glyph5Index = 4
                OpenVG.SetGlyphToPath( FontIndex, Glyph5Index, Path5Index, 'OFF', [ 0, 0.0 ], [ 60, 0 ] )
        
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )
                OpenVG.AppendPathData( Path3Index, PathData3Index )
                OpenVG.AppendPathData( Path4Index, PathData4Index )
                OpenVG.AppendPathData( Path5Index, PathData5Index )

                GlyphCount   = 5
                GlyphIndices = []
                for i in range( GlyphCount ):
                        GlyphIndices.extend( [ i ] )

                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.MatrixMode( 'VG_MATRIX_GLYPH_USER_TO_SURFACE' )
                
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( Paint2Index, [ 'VG_FILL_PATH' ] )
                OpenVG.SetPaint( Paint1Index, [ 'VG_STROKE_PATH' ] )
                
                # Count starting point of first letter
                GlyphOriginX = ( WIDTH / GlyphCount ) - 40
                GlyphOriginY = HEIGHT / 2
                
                # Set aplication defined matrix to current matrix 
                OpenVG.LoadMatrix( self.Matrix )
                
                # Translate upper
                OpenVG.Translate( 0, 100 )
                
                # Draw Glyphs
                OpenVG.DrawGlyphs( FontIndex,
                                   GlyphCount,
                                   GlyphIndices,
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 0.0, 0.0, 0.0, 0.0, 0.0 ],
                                   [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ],
                                   'OFF' )

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
