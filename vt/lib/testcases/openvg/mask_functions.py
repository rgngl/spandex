#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class MaskingImage( vt.VisualTestCase ):
        
        '''
        The purpose of the test: This test is for testing masking operation with different
        imageformats and image quality type ( testValues ).
        
        The test steps:
        1. The test creates image 1 and sets mask layer on the lower left side of the
        picture ( imageformat and quality from testValues ).
        2. The surface is cleared.
        3. The test creates image 2 and sets mask layer with varying masking operations
        ( operation type, imageformat and quality type from testValues ).
        4. The test clears the surface and enables the masking.
        5. The test draws tiger to the surface.
        
        Expected output: A tiger which are partially covered by two masking images.
        Masking should vary depending on used masking operation.
        '''

        testValues = { ( 'MaskOperation', 'ImageFormat', 'ImageQuality' ) :
                               [ ( 'VG_CLEAR_MASK',     'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_FILL_MASK',      'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_CLEAR_MASK',     'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SET_MASK',       'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_CLEAR_MASK',     'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_FILL_MASK',      'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sRGB_565',       'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SET_MASK',       'VG_sRGB_565',       'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SET_MASK',       'VG_sRGB_565',       'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SET_MASK',       'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SET_MASK',       'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_UNION_MASK',     'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SET_MASK',       'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_UNION_MASK',     'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sL_8',           'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sL_8',           'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sL_8',           'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_UNION_MASK',     'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_lL_8',           'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_lL_8',           'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_CLEAR_MASK',     'VG_lL_8',           'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_A_8',            'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_CLEAR_MASK',     'VG_A_8',            'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_FILL_MASK',      'VG_A_8',            'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_CLEAR_MASK',     'VG_BW_1',           'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_FILL_MASK',      'VG_BW_1',           'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_FILL_MASK',      'VG_BW_1',           'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SET_MASK',       'VG_sXRGB_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SET_MASK',       'VG_sXRGB_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sARGB_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SET_MASK',       'VG_sARGB_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SET_MASK',       'VG_sARGB_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SET_MASK',       'VG_sARGB_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SET_MASK',       'VG_sARGB_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sARGB_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_UNION_MASK',     'VG_sARGB_1555',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sARGB_1555',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sARGB_1555',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sARGB_4444',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sARGB_4444',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sARGB_4444',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_UNION_MASK',     'VG_lXRGB_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lXRGB_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lXRGB_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lARGB_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_lARGB_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_lARGB_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lARGB_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_lARGB_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_lARGB_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sBGRX_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sBGRX_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_CLEAR_MASK',     'VG_sBGRX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_CLEAR_MASK',     'VG_sBGRA_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sBGRA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_FILL_MASK',      'VG_sBGRA_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_CLEAR_MASK',     'VG_sBGRA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sBGRA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sBGRA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_FILL_MASK',      'VG_sBGR_565',       'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sBGR_565',       'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SET_MASK',       'VG_sBGR_565',       'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SET_MASK',       'VG_sBGRA_5551',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SET_MASK',       'VG_sBGRA_5551',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_UNION_MASK',     'VG_sBGRA_5551',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SET_MASK',       'VG_sBGRA_4444',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SET_MASK',       'VG_sBGRA_4444',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sBGRA_4444',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SET_MASK',       'VG_lBGRX_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_UNION_MASK',     'VG_lBGRX_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_UNION_MASK',     'VG_lBGRX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_UNION_MASK',     'VG_lBGRA_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lBGRA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lBGRA_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_UNION_MASK',     'VG_lBGRA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lBGRA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_lBGRA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sXBGR_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sXBGR_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sXBGR_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sABGR_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sABGR_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_CLEAR_MASK',     'VG_sABGR_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sABGR_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sABGR_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_CLEAR_MASK',     'VG_sABGR_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sABGR_1555',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_CLEAR_MASK',     'VG_sABGR_1555',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sABGR_1555',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_FILL_MASK',      'VG_sABGR_4444',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sABGR_4444',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SET_MASK',       'VG_sABGR_4444',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_FILL_MASK',      'VG_lXBGR_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_FILL_MASK',      'VG_lXBGR_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SET_MASK',       'VG_lXBGR_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_FILL_MASK',      'VG_lABGR_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SET_MASK',       'VG_lABGR_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SET_MASK',       'VG_lABGR_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SET_MASK',       'VG_lABGR_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_UNION_MASK',     'VG_lABGR_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_UNION_MASK',     'VG_lABGR_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SET_MASK',       'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_UNION_MASK',     'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sRGB_565',       'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sRGB_565',       'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sRGB_565',       'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_sL_8',           'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_CLEAR_MASK',     'VG_sL_8',           'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_FILL_MASK',      'VG_sL_8',           'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SUBTRACT_MASK',  'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_CLEAR_MASK',     'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_FILL_MASK',      'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_CLEAR_MASK',     'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_FILL_MASK',      'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_FILL_MASK',      'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_FILL_MASK',      'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SET_MASK',       'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SET_MASK',       'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_FILL_MASK',      'VG_lL_8',           'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SET_MASK',       'VG_lL_8',           'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_SET_MASK',       'VG_lL_8',           'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_SET_MASK',       'VG_A_8',            'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_SET_MASK',       'VG_A_8',            'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_UNION_MASK',     'VG_A_8',            'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_UNION_MASK',     'VG_BW_1',           'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_UNION_MASK',     'VG_BW_1',           'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sXRGB_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sXRGB_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                                 ( 'VG_INTERSECT_MASK', 'VG_sXRGB_8888',     'VG_IMAGE_QUALITY_FASTER' ),
                                 ( 'VG_UNION_MASK',     'VG_sARGB_8888',     'VG_IMAGE_QUALITY_BETTER' ),
                                 #('VG_INTERSECT_MASK','VG_sARGB_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_sARGB_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_sARGB_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_sARGB_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_sARGB_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_sARGB_1555','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_sARGB_1555','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_CLEAR_MASK','VG_sARGB_1555','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_sARGB_4444','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_CLEAR_MASK','VG_sARGB_4444','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_CLEAR_MASK','VG_lXRGB_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_lXRGB_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_lXRGB_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_lARGB_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_lARGB_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_lARGB_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_lARGB_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_lARGB_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_lARGB_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SET_MASK','VG_sBGRX_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SET_MASK','VG_sBGRX_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_sBGRX_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_sBGRA_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_sBGRA_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_sBGRA_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_sBGRA_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_sBGRA_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_sBGRA_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_sBGR_565','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_sBGR_565','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_sBGRA_5551','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_sBGRA_5551','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_sBGRA_4444','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_sBGRA_4444','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_lBGRX_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_lBGRX_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_CLEAR_MASK','VG_lBGRX_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_CLEAR_MASK','VG_lBGRA_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_lBGRA_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_CLEAR_MASK','VG_lBGRA_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_lBGRA_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_lBGRA_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_sXBGR_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_sXBGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sXBGR_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_sABGR_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SET_MASK','VG_sABGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sABGR_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_sABGR_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_sABGR_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sABGR_1555','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_sABGR_1555','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_sABGR_1555','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_sABGR_4444','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_sABGR_4444','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_lXBGR_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_lXBGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_lXBGR_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_lABGR_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_lABGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_lABGR_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_lABGR_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_sRGBX_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_sRGBX_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_sRGBA_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SET_MASK','VG_sRGBA_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_sRGBA_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_sRGBA_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sRGBA_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_sRGBA_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_CLEAR_MASK','VG_sRGB_565','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_sRGB_565','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_sRGB_565','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_CLEAR_MASK','VG_sRGBA_5551','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_sRGBA_5551','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_sRGBA_5551','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_CLEAR_MASK','VG_sRGBA_4444','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_sRGBA_4444','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_sRGBA_4444','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sL_8','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_sL_8','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_sL_8','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_lRGBX_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_lRGBX_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_lRGBX_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_lRGBA_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_lRGBA_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_lRGBA_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_lRGBA_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_lL_8','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_lL_8','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_lL_8','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_A_8','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_A_8','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_A_8','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_BW_1','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_BW_1','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_BW_1','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_sXRGB_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_sXRGB_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_CLEAR_MASK','VG_sXRGB_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_CLEAR_MASK','VG_sARGB_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_sARGB_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_CLEAR_MASK','VG_sARGB_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_sARGB_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_sARGB_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_sARGB_1555','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_sARGB_1555','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sARGB_1555','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_sARGB_4444','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sARGB_4444','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_sARGB_4444','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SET_MASK','VG_lXRGB_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SET_MASK','VG_lXRGB_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_lXRGB_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_lARGB_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_lARGB_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_lARGB_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_lARGB_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_CLEAR_MASK','VG_lARGB_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_sBGRX_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_sBGRX_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_sBGRX_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_sBGRA_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_sBGRA_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_sBGRA_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_sBGRA_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_sBGRA_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sBGRA_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_CLEAR_MASK','VG_sBGR_565','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sBGR_565','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_CLEAR_MASK','VG_sBGRA_5551','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_sBGRA_5551','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_sBGRA_5551','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_sBGRA_4444','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_sBGRA_4444','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_sBGRA_4444','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_lBGRX_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_lBGRX_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_lBGRX_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_lBGRA_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SET_MASK','VG_lBGRA_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_lBGRA_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_lBGRA_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_sXBGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_sXBGR_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_CLEAR_MASK','VG_sXBGR_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_sABGR_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_sABGR_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_sABGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_sABGR_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_sABGR_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_sABGR_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_sABGR_1555','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_sABGR_1555','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_sABGR_1555','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_sABGR_4444','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_sABGR_4444','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_CLEAR_MASK','VG_sABGR_4444','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_lXBGR_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_CLEAR_MASK','VG_lXBGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_lXBGR_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_lABGR_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_lABGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_lABGR_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_lABGR_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_lABGR_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_sRGBX_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_sRGBX_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_sRGBA_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_sRGBA_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_sRGBA_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_sRGB_565','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_sRGB_565','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_sRGBA_5551','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_sRGBA_5551','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sRGBA_4444','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_sRGBA_4444','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_FILL_MASK','VG_sL_8','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_lRGBX_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SET_MASK','VG_lRGBX_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_lRGBA_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SET_MASK','VG_lRGBA_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_lRGBA_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_CLEAR_MASK','VG_lRGBA_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_lL_8','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_A_8','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_BW_1','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_BW_1','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_INTERSECT_MASK','VG_sXRGB_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_sXRGB_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_sARGB_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_sARGB_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_UNION_MASK','VG_sARGB_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_sARGB_1555','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_sARGB_4444','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_sARGB_4444','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_lXRGB_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_lXRGB_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_CLEAR_MASK','VG_lARGB_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_lARGB_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_lARGB_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_lARGB_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_sBGRX_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_FILL_MASK','VG_sBGRX_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sBGRA_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_sBGRA_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_sBGRA_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_sBGRA_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_sBGR_565','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_sBGR_565','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SUBTRACT_MASK','VG_sBGRA_5551','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_sBGRA_5551','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_CLEAR_MASK','VG_sBGRA_4444','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_sBGRA_4444','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_lBGRX_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_INTERSECT_MASK','VG_lBGRX_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_lBGRA_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_lBGRA_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SET_MASK','VG_lBGRA_8888_PRE','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_lBGRA_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_sXBGR_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SET_MASK','VG_sXBGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_sABGR_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_sABGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_FILL_MASK','VG_sABGR_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_UNION_MASK','VG_sABGR_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_SET_MASK','VG_sABGR_1555','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_sABGR_1555','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SET_MASK','VG_sABGR_4444','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_sABGR_4444','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_UNION_MASK','VG_lXBGR_8888','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_SUBTRACT_MASK','VG_lXBGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_CLEAR_MASK','VG_lABGR_8888','VG_IMAGE_QUALITY_NONANTIALIASED'),
                                 #('VG_SUBTRACT_MASK','VG_lABGR_8888','VG_IMAGE_QUALITY_BETTER'),
                                 #('VG_CLEAR_MASK','VG_lABGR_8888_PRE','VG_IMAGE_QUALITY_FASTER'),
                                 #('VG_INTERSECT_MASK','VG_lABGR_8888_PRE','VG_IMAGE_QUALITY_BETTER'),
                                 ],
                       }

        def __init__( self, MaskOperation, ImageFormat, ImageQuality ):
                vt.VisualTestCase.__init__( self )
                self.MaskOperation = MaskOperation
                self.ImageFormat   = ImageFormat
                self.ImageQuality  = ImageQuality
                self.repeats       = 1
                self.name = 'OPENVG masking %s in %s %s' % ( MaskOperation, ImageFormat, ImageQuality, )
    
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                        
                OpenVG.ClearColor( [ 0.25, 0.5, 0.75, 0.25 ] )
                OpenVG.Clear( 0, 0, 0, 0 )

                OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
                OpenVG.Clear( 5, 5, 25, 25 )
                OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
                OpenVG.Clear( 30, 30, 25, 25 )
                OpenVG.ClearColor( [ 0.25, 0.5, 0.75, 0.25 ] )
                
                # Create an image
                Image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image1Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )

                ImageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                OpenVG.CreateNullImageData( ImageData1Index, self.ImageFormat, WIDTH, HEIGHT )
                
                # Read pixels 
                OpenVG.ReadPixels( ImageData1Index, 0, 0, WIDTH, HEIGHT )
                OpenVG.ImageSubData( ImageData1Index, Image1Index, 0, 0, WIDTH, HEIGHT )
        
                # Set solid masking for image 1
                OpenVG.Mask( Image1Index, 'IMAGE', 'VG_SET_MASK', 25, 25, 100, 100 ) 

                OpenVG.ClearColor( [ 0.75, 0.5, 0.25, 0.75 ] )
                OpenVG.Clear( 0, 0, 0, 0 )
                
                # Create another image
                Image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )                
                OpenVG.CreateImage( Image2Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )

                ImageData2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                OpenVG.CreateNullImageData( ImageData2Index, self.ImageFormat, WIDTH, HEIGHT ) 
        
                # Read pixels
                OpenVG.ReadPixels( ImageData2Index, 0, 0, WIDTH, HEIGHT )
                OpenVG.ImageSubData( ImageData2Index, Image2Index, 0, 0, WIDTH, HEIGHT )
                
                # Set masking for image2 to operation determined in testValues
                OpenVG.Mask( Image2Index, 'IMAGE', self.MaskOperation, 75, 75, 100, 100 )
        
                # Create a tiger
                TigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
                OpenVG.CreateTiger( TigerIndex )

                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.Masking( 'ON' )

                # Draw the tiger
                OpenVG.DrawTiger( TigerIndex, 1, WIDTH, HEIGHT )

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                

######################################################################                
class MaskingRenderToMask( vt.VisualTestCase ):
        
        '''
        The Purpose of the test: this test is for testing RenderToMask function.
        
        The test steps:
        1. The test defines 4 paints.
        2. The test creates path data for two rectangles. The first one is covering whole
        surface and the second one small portion of the surface ( on the lower right ).
        3. The test append path datas for both rectangles.
        4. The test set masking with 'VG_SET_MASK' for the first path.
        5. The test set masking with varying operation for the second path ( testValues ).
        6. The test clears the surface.
        7. The test draws the first rectangle.
        8. The test enables masking.
        9. The test draws the second rectangle.
        
        Expected output: The smaller rectangle ( yellow borders and blue filling ) should
        be masking the bigger rectangle ( green borders and red filling ) with mask operation
        given in testValues.
        '''
        
        testValues = { 'MaskOperation' : [ 'VG_CLEAR_MASK', 
                                           'VG_FILL_MASK', 
                                           'VG_SET_MASK', 
                                           'VG_UNION_MASK', 
                                           'VG_INTERSECT_MASK', 
                                           'VG_SUBTRACT_MASK' ]
                       }

        def __init__( self, MaskOperation ):
                vt.VisualTestCase.__init__( self )
                self.MaskOperation = MaskOperation
                self.ImageFormat   = 'VG_sRGBA_8888'
                self.ImageQuality  = 'VG_IMAGE_QUALITY_BETTER'
                self.DataType      = 'VG_PATH_DATATYPE_S_32'
                self.repeats       = 1                
                self.name = 'OPENVG RenderToMask %s' % ( MaskOperation, )
                
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                        
                OpenVG.ClearColor( [ 0.75, 0.5, 0.25, 0.75 ] )
                OpenVG.Clear( 0, 0, 0, 0 )                
                OpenVG.Stroke( 10, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
                
                # Create color paints for two rectangles
                Paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint1Index )
                OpenVG.ColorPaint( Paint1Index, [ 0.0, 1.0, 0.0, 1.0 ] )
                
                Paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint2Index )
                OpenVG.ColorPaint( Paint2Index, [ 1.0, 0.0, 0.0, 1.0 ] )
                
                Paint3Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint3Index )
                OpenVG.ColorPaint( Paint3Index, [ 0.0, 0.0, 1.0, 1.0 ] )
                
                Paint4Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint4Index )
                OpenVG.ColorPaint( Paint4Index, [ 1.0, 1.0, 0.0, 1.0 ] )
                
                # Create a path
                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DataType, 1, 1 )
                
                # Create surface covering rectangle
                OpenVG.MoveTo( PathData1Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ WIDTH, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, HEIGHT ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -WIDTH, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -HEIGHT ], 'ON' )
                OpenVG.ClosePath( PathData1Index )
                
                # Create a path
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DataType, 1, 1 )
                
                # Create smaller rectangle to lower right
                OpenVG.MoveTo( PathData2Index, [ 75, 25 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 100, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index, [ 0, 100 ], 'ON' )
                OpenVG.Lines( PathData2Index, [ -100, 0 ], 'ON' )
                OpenVG.Lines( PathData2Index, [ 0, -100 ], 'ON' )
                OpenVG.ClosePath( PathData2Index )
                
                # Append path datas for both rectangles
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )
                
                OpenVG.RenderToMask( Path1Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ], 'VG_SET_MASK' )
                OpenVG.RenderToMask( Path2Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ], self.MaskOperation )

                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
                OpenVG.Clear( 0, 0, 0, 0 )
                
                # Set paints for surface covering rectangle
                OpenVG.SetPaint( Paint2Index, [ 'VG_FILL_PATH' ] )
                OpenVG.SetPaint( Paint1Index, [ 'VG_STROKE_PATH' ] )
                
                # Draw the rectangle
                OpenVG.DrawPath( Path1Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )
                
                OpenVG.Masking( 'ON' )
                
                # Set paints for smaller rectangle
                OpenVG.SetPaint( Paint3Index, [ 'VG_FILL_PATH' ] )
                OpenVG.SetPaint( Paint4Index, [ 'VG_STROKE_PATH' ] )
                
                # Draw the smaller rectangle
                OpenVG.DrawPath( Path2Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
######################################################################
class MaskingFillMaskLayer( vt.VisualTestCase ):
        
        '''
        The purpose of the test: This test is for testing FillMaskLayer.
        
        The test steps:
        1. The test creates path for whole surface covering rectangle.
        2. The test appends path data.
        3. The test draws path data to surface.
        4. The test creates an image.
        5. The test reads pixels from surface.
        6. The test passes pixels from surface to image.
        7. The test masks with resulting image ( VG_SET_MASK ).
        8. The test clears the surface.
        9. The test creates two mask layers ( 100x100 ).
        10. The test fill mask layers ( 0.66 and 0.33 ).
        11. The test masks with resulting mask layers ( masking operation from testValues ).
        12. The test clears the surface.
        13. The test enables masking.
        14. The test draws the image.
        15. The test destroys used resources.
        
        Expected output: a whole surface covering rectangle which is masked with two smaller
        rectangles. Output depends on masking operation given in testValues.
        '''
        
        testValues = { 'MaskOperation' : [ 'VG_CLEAR_MASK', 
                                           'VG_FILL_MASK', 
                                           'VG_SET_MASK', 
                                           'VG_UNION_MASK', 
                                           'VG_INTERSECT_MASK', 
                                           'VG_SUBTRACT_MASK' ]
                       }

        def __init__( self, MaskOperation ):
                vt.VisualTestCase.__init__( self )
                self.MaskOperation = MaskOperation
                self.ImageFormat   = 'VG_sRGBA_8888'
                self.ImageQuality  = 'VG_IMAGE_QUALITY_BETTER'
                self.DataType      = 'VG_PATH_DATATYPE_S_32'
                self.repeats       = 1
                self.name = 'OPENVG FillMaskLayer %s' % ( MaskOperation, )
                
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]

                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                
                OpenVG.ClearColor( [ 0.25, 0.5, 0.75, 0.25 ] )
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.Stroke( 10, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
                
                # Create color paints for two rectangles
                Paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint1Index )
                OpenVG.ColorPaint( Paint1Index, [ 1.0, 0.0, 1.0, 1.0 ] )
                
                Paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint(Paint2Index)
                OpenVG.ColorPaint( Paint2Index, [ 0.8, 0.8, 0.8, 1.0 ] )
                
                # Create a path
                PathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( PathIndex, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathDataIndex, self.DataType, 1, 1 )
                
                # Create surface covering rectangle
                OpenVG.MoveTo( PathDataIndex, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathDataIndex, [ WIDTH, 0 ], 'ON' )
                OpenVG.Lines( PathDataIndex, [ 0,HEIGHT ], 'ON' )
                OpenVG.Lines( PathDataIndex, [ -WIDTH, 0 ], 'ON' )
                OpenVG.Lines( PathDataIndex, [ 0, -HEIGHT ], 'ON' )
                OpenVG.ClosePath( PathDataIndex )
                
                # Append path data
                OpenVG.AppendPathData( PathIndex, PathDataIndex )
                
                # Set paint and draw path
                OpenVG.SetPaint( Paint1Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.SetPaint( Paint2Index, [ 'VG_FILL_PATH' ] )
                OpenVG.DrawPath( PathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )
                
                # Create an image
                ImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( ImageIndex, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )

                ImageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
                OpenVG.CreateNullImageData( ImageDataIndex, self.ImageFormat, WIDTH, HEIGHT )
                
                OpenVG.ReadPixels( ImageDataIndex, 0, 0, WIDTH, HEIGHT )
                OpenVG.ImageSubData( ImageDataIndex, ImageIndex, 0, 0, WIDTH, HEIGHT )
                
                # Mask image with VG_SET_MASK
                OpenVG.Mask( ImageIndex, 'IMAGE', 'VG_SET_MASK', 0, 0, WIDTH, HEIGHT )
                
                OpenVG.Clear( 0, 0, 0, 0 )
                
                # Create first mask layer
                maskLayer1Index = indexTracker.allocIndex( 'OPENVG_MASK_LAYER_INDEX' )
                OpenVG.CreateMaskLayer( maskLayer1Index, 100, 100 )
                
                # Create a fill mask layer 1
                OpenVG.FillMaskLayer( maskLayer1Index, 0,0, 100, 100, 0.66 )
                
                # Create second mask layer
                maskLayer2Index = indexTracker.allocIndex( 'OPENVG_MASK_LAYER_INDEX' )
                OpenVG.CreateMaskLayer( maskLayer2Index, 100, 100 )
                
                # Create a fill mask layer 2
                OpenVG.FillMaskLayer( maskLayer2Index, 0,0, 100, 100, 0.33 )
                
                # Mask with created fill mask layers
                OpenVG.Mask( maskLayer1Index, 'MASK_LAYER', self.MaskOperation, 80, 80, 100, 100 )
                OpenVG.Mask( maskLayer2Index, 'MASK_LAYER', self.MaskOperation, 20, 20, 100, 100 )

                # ---------------------------------------------------------------------- 
                self.beginBenchmarkActions()
                
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.Masking( 'ON' )
                OpenVG.DrawImage( ImageIndex )
                
                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
######################################################################
class MaskingCopyMask( vt.VisualTestCase ):
        
        '''
        The purpose of the test: This test is for testing Masking with copied mask layer.
        
        The test steps:
        1. The test creates four paints ( green, red, blue and yellow ).
        2. The test creates a path1 for whole surface covering rectangle.
        3. The test creates a path2 for triangle ( On top of the rectangle ).
        4. The test append path data.
        5. The test renders path1 to mask with vg.RenderToMask ( VG_SET_MASK ).
        6. The test renders path2 to mask with vg.RenderToMask ( Masking operation from TestValues ).
        7. The test creates a mask layer.
        8. The test copies pixels from masking surface to mask layer with vg.CopyMask.
        9. The test masks with resulting mask layer ( Masking operation from testValues ).
        10. The test clears the surface.
        11. The test draws the first path.
        12. The test enables masking.
        13. The test draws the second path.
        
        Expected output. A square, that is masked with triangle and another square.
        Output varies depending on the masking operation given in testValues.
        '''
        
        testValues = { 'MaskOperation' : [ 'VG_CLEAR_MASK', 
                                           'VG_FILL_MASK', 
                                           'VG_SET_MASK', 
                                           'VG_UNION_MASK', 
                                           'VG_INTERSECT_MASK', 
                                           'VG_SUBTRACT_MASK' ]
                       }

        def __init__( self, MaskOperation ):
                vt.VisualTestCase.__init__( self )
                self.MaskOperation = MaskOperation
                self.ImageFormat   = 'VG_sRGBA_8888'
                self.ImageQuality  = 'VG_IMAGE_QUALITY_BETTER'
                self.DataType      = 'VG_PATH_DATATYPE_S_32'
                self.repeats       = 1
                self.name = 'OPENVG CopyMask %s' % ( MaskOperation, )                
                
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]

                indexTracker = IndexTracker()

                # ---------------------------------------------------------------------- 
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                        
                OpenVG.ClearColor( [ 0.75, 0.5, 0.25, 0.75 ] )
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.Stroke( 10, 'VG_CAP_ROUND', 'VG_JOIN_MITER', 0 )
                
                # Create color paints for two rectangles
                Paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint1Index )
                OpenVG.ColorPaint( Paint1Index, [ 0.0, 1.0, 0.0, 1.0 ] )
                
                Paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint2Index )
                OpenVG.ColorPaint( Paint2Index, [ 1.0, 0.0, 0.0, 1.0 ] )
                
                Paint3Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint3Index )
                OpenVG.ColorPaint( Paint3Index, [ 0.0, 0.0, 1.0, 1.0 ] )
                
                Paint4Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint4Index )
                OpenVG.ColorPaint( Paint4Index, [ 1.0, 1.0, 0.0, 1.0 ] )
                
                # Create a path
                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DataType, 1, 1 )
                
                # Create surface covering rectangle
                OpenVG.MoveTo( PathData1Index, [ 0,0 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ 0, HEIGHT ], 'ON' )
                OpenVG.Lines( PathData1Index, [ WIDTH, 0 ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0, -HEIGHT ], 'ON' )
                OpenVG.Lines( PathData1Index, [ -WIDTH, 0 ], 'ON' )
                
                # Create a path
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DataType, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DataType, 1, 1 )
                
                # Create a triangle
                OpenVG.MoveTo( PathData2Index, [ 0, 0 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ WIDTH / 2, HEIGHT ], 'ON' )
                OpenVG.Lines( PathData2Index, [ WIDTH /2, -HEIGHT ], 'ON' )
                OpenVG.Lines( PathData2Index, [ -WIDTH, 0 ], 'ON' )
                OpenVG.ClosePath( PathData2Index )
                
                # Append path datas
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )
                
                OpenVG.RenderToMask( Path1Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ], 'VG_SET_MASK' )
                OpenVG.RenderToMask( Path2Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ], self.MaskOperation )
        
                # Create a mask layer
                MaskLayerIndex = indexTracker.allocIndex( 'OPENVG_MASK_LAYER_INDEX' )
                OpenVG.CreateMaskLayer( MaskLayerIndex, WIDTH / 2,HEIGHT / 2 )
                
                # Copy pixels from surface mask to masklayer
                OpenVG.CopyMask( MaskLayerIndex, WIDTH / 2, 0, 0 , 0, WIDTH / 2,  HEIGHT / 2 )
                
                # Masking with mask layer
                OpenVG.Mask( MaskLayerIndex, 'MASK_LAYER', self.MaskOperation, WIDTH / 4, 0, WIDTH / 2, HEIGHT / 2 ) 
                
                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )                
                OpenVG.Clear( 0, 0, 0, 0 )
                
                # Set paints for surface covering rectangle
                OpenVG.SetPaint( Paint2Index, [ 'VG_FILL_PATH' ] )
                OpenVG.SetPaint( Paint1Index, [ 'VG_STROKE_PATH' ] )
                
                # Draw the rectangle
                OpenVG.DrawPath( Path1Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )
                
                OpenVG.Masking( 'ON' )
                
                # Set paints for smaller rectangle
                OpenVG.SetPaint( Paint3Index, [ 'VG_FILL_PATH' ] )
                OpenVG.SetPaint( Paint4Index, [ 'VG_STROKE_PATH' ] )
                
                # Draw the smaller rectangle
                OpenVG.DrawPath( Path2Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
