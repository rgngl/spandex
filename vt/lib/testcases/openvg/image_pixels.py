#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class GetPixels( vt.VisualTestCase ):
    
    ''' Test for getting pixels from the surface to the images. Test draws tiger
    with black and white rectangles, reads pixel data to images and clears the
    surface to color [0,0,0,0.5]. Image pixels are then drawn to different
    positions in the drawing surface.
    '''

    testValues = { 'quality'     : [ 'VG_IMAGE_QUALITY_NONANTIALIASED',
                                     'VG_IMAGE_QUALITY_FASTER',
                                     'VG_IMAGE_QUALITY_BETTER' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, quality, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.quality     = quality
        self.imageformat = imageformat
        self.repeats     = 1
        self.name = 'OPENVG image get pixels %s %s' % ( imageformat, quality, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ImageQuality( self.quality )
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        
        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )
        
        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image2Index, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )

        image3Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image3Index, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )

        image4Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image4Index, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )

        image5Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image5Index, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )
        
        OpenVG.GetPixels( image1Index, 10, 10, 10, 10, 80, 80 )
        OpenVG.GetPixels( image2Index, 110, 10, 110, 0, 80, 80 )
        OpenVG.GetPixels( image3Index, 110, 110, 110, 110, 80, 80 )
        OpenVG.GetPixels( image4Index, 10, 110, 10, 110, 80, 80 )
        OpenVG.GetPixels( image5Index, 50, 50, 50, 50, 100, 100 )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPixels( 10, 10, image1Index, 10, 10, 80, 80 ) 
        OpenVG.SetPixels( 110, 10, image2Index, 110, 10, 80, 80 ) 
        OpenVG.SetPixels( 110, 110, image3Index, 110, 110, 80, 80 ) 
        OpenVG.SetPixels( 10, 110, image4Index, 10, 110, 80, 80 ) 
        OpenVG.SetPixels( 50, 50, image5Index, 50, 50, 100, 100 )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class SetPixels( vt.VisualTestCase ):
    
    ''' Test for setting pixels from the images to the surface. Test draws tiger
    with black and white rectangles, reads surface content to the image and
    clears the surface to color [0,0,0,0.5]. Image pixels are then set to
    different positions of the drawing surface.
    '''

    testValues = { 'quality'     : [ 'VG_IMAGE_QUALITY_NONANTIALIASED',
                                     'VG_IMAGE_QUALITY_FASTER',
                                     'VG_IMAGE_QUALITY_BETTER' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, quality, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.quality     = quality
        self.imageformat = imageformat
        self.repeats     = 1        
        self.name = 'OPENVG image set pixels %s %s' % ( imageformat, quality, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ImageQuality( self.quality )
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.imageformat, WIDTH, HEIGHT )
        
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageDataIndex, imageIndex, 0, 0, WIDTH, HEIGHT )

        # ----------------------------------------------------------------------        
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPixels( 100, 0, imageIndex, 0, 0, 100, 100 )
        OpenVG.SetPixels( 100, 100, imageIndex, 100, 0, 100, 100 )
        OpenVG.SetPixels( 0, 100, imageIndex, 100, 100, 100, 100 )
        OpenVG.SetPixels( 0, 0, imageIndex, 0, 100, 100, 100 )
        OpenVG.SetPixels( 50, 50, imageIndex, 50, 50, 100, 100 )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class ReadPixels( vt.VisualTestCase ):
    
    ''' Test for reading pixels from the surface. Test draws tiger with black
    and white rectangles, reads drawing surface content to images and clears the
    surface to color [0,0,0,0.5]. Images are then drawn to different positions
    of the drawing surface.
    '''

    testValues = { 'quality'     : [ 'VG_IMAGE_QUALITY_NONANTIALIASED',
                                     'VG_IMAGE_QUALITY_FASTER',
                                     'VG_IMAGE_QUALITY_BETTER' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, quality, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.quality     = quality
        self.imageformat = imageformat
        self.repeats     = 1        
        self.name = 'OPENVG image read pixels %s %s' % ( imageformat, quality, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        
        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )

        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image2Index, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )

        image3Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image3Index, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )

        image4Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image4Index, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )

        image5Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image5Index, self.imageformat, WIDTH, HEIGHT, [ self.quality ] )

        imageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData1Index, self.imageformat, WIDTH, HEIGHT )

        imageData2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData2Index, self.imageformat, WIDTH, HEIGHT )

        imageData3Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData3Index, self.imageformat, WIDTH, HEIGHT )

        imageData4Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData4Index, self.imageformat, WIDTH, HEIGHT )

        imageData5Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData5Index, self.imageformat, WIDTH, HEIGHT )
        
        OpenVG.ReadPixels( imageData5Index, 10, 10, 80, 80 )
        OpenVG.ReadPixels( imageData4Index, 110, 10, 80, 80 )
        OpenVG.ReadPixels( imageData3Index, 110, 110, 80, 80 )
        OpenVG.ReadPixels( imageData2Index, 10, 110, 80, 80 )
        OpenVG.ReadPixels( imageData1Index, 60, 60, 80, 80 )
        OpenVG.ImageSubData( imageData1Index, image1Index, 10, 10, 80, 80 ) 
        OpenVG.ImageSubData( imageData2Index, image2Index, 110, 10, 80, 80 )
        OpenVG.ImageSubData( imageData3Index, image3Index, 110, 110, 80, 80 )
        OpenVG.ImageSubData( imageData4Index, image4Index, 10, 110, 80, 80 )
        OpenVG.ImageSubData( imageData5Index, image5Index, 60, 60, 80, 80 ) 

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( image1Index )
        OpenVG.DrawImage( image2Index )
        OpenVG.DrawImage( image3Index )
        OpenVG.DrawImage( image4Index )
        OpenVG.DrawImage( image5Index )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class WritePixels( vt.VisualTestCase ):
    
    ''' Test for writing pixels to the surface. Test draws tiger with black and
    white rectangles, reads drawing surface content to the image and clears the
    surface to color [0,0,0,0.5]. Pixel data is then written to the different
    positions of the drawing surface.
    '''

    testValues = { 'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.imageformat = imageformat
        self.repeats     = 1
        self.name = 'OPENVG image write pixels %s' % ( imageformat, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        
        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.imageformat, WIDTH, HEIGHT )
        
        OpenVG.ReadPixels( imageDataIndex, 50, 50, 100, 100 )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.WritePixels( imageDataIndex, 10, 10, 80, 80 )
        OpenVG.WritePixels( imageDataIndex, 110, 10, 80, 80 )
        OpenVG.WritePixels( imageDataIndex, 110, 110, 80, 80 )
        OpenVG.WritePixels( imageDataIndex, 10, 110, 80, 80 )
        OpenVG.WritePixels( imageDataIndex, 50, 50, 100, 100 )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
