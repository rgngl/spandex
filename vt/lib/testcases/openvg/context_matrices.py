#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class PathUserToSurfaceRotateStroke( vt.VisualTestCase ):
    
    '''
    Test for VG_MATRIX_PATH_USER_TO_SURFACE rotation for a stroke. Origin is
    in [100,100]. Path geometry is line[50,0]. Path is drawn 20 times
    (translate[30,0], rotate[+35], translate[-30,0]). Rotation is tested to 700
    degrees and symmetry prevented by 35 degree angle. Paint is defined only for
    the stroke. Both stroke and fill are drawn.
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG matrix VG_MATRIX_PATH_USER_TO_SURFACE rotation for stroke'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        OpenVG.Lines( pathDataIndex, [ 50, 0 ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.75, 0.50, 0.25, 0.50 ] )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.Translate( 100, 100 )
        
        for i in range( 21 ):
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )
            OpenVG.Rotate( 35 )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class PathUserToSurfaceTranslateAndRotateStroke( vt.VisualTestCase ):
    
    '''
    Test for VG_MATRIX_PATH_USER_TO_SURFACE rotation and translation for a
    stroke. Origin is in [100,100]. Path geometry is line[50,0]. Path is drawn
    20 times (translate[30,0], rotate[+35], translate[-30,0]). Rotation is
    tested to 700 degrees and symmetry prevented by 35 degree angle. Paint is
    defined only for the stroke. Both stroke and fill are drawn.
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG matrix VG_MATRIX_PATH_USER_TO_SURFACE rotation and translation for stroke'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.75, 0.50, 0.25, 0.5 ] )
        
        OpenVG.Lines( pathDataIndex, [ 50, 0 ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.Translate( 100, 100 )

        for i in range( 21 ):
            OpenVG.Translate( 30, 0 )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )
            OpenVG.Translate( -30, 0 )
            OpenVG.Rotate( 35 )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class PathUserToSurfaceRotateFill( vt.VisualTestCase ):
    
    '''
    Test for VG_MATRIX_PATH_USER_TO_SURFACE rotation for a fill. Origin is in
    [100,100]. Path geometry contains lines to [0.25],[50,0],[0,-5],[-50,0] and
    [0,2.5]. Path is drawn 20 times (translate [30,0], rotate[+35],
    translate[-30,0]) Rotation is tested to to 700 degrees and symmetry
    prevented by 35 degree angle. Paint is defined only for the fill, and both
    stroke and fill are drawn.
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.name = 'OPENVG matrix VG_MATRIX_PATH_USER_TO_SURFACE rotation for fill'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 1, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        OpenVG.Lines( pathDataIndex, [ 0, 2.5 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ 50, 0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ 0, -5.0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ -50, 0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ 0, 2.5 ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.75, 0.50, 0.25, 0.50 ] )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.Translate( 100, 100 )
        
        for i in range( 21 ):
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )
            OpenVG.Rotate( 35 )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class PathUserToSurfaceTranslateAndRotateFill( vt.VisualTestCase ):
    
    '''
    Test for VG_MATRIX_PATH_USER_TO_SURFACE rotation and translation for a
    fill. Origin is in [100,100]. Path geometry contains lines to
    [0.25],[50,0],[0,-5],[-50,0] and [0,2.5]. Path is drawn 20 times (translate
    [30,0], rotate[+35], translate[-30,0]) Rotation is tested to to 700 degrees
    and symmetry prevented by 35 degree angle. Paint is defined only for the
    fill, and both stroke and fill are drawn.
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.name = 'OPENVG matrix VG_MATRIX_PATH_USER_TO_SURFACE rotation and translation for fill'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 1, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.75, 0.50, 0.25, 0.5 ] )
        
        OpenVG.Lines( pathDataIndex, [ 0, 2.5 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ 50, 0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ 0, -5.0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ -50, 0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ 0, 2.5 ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.Translate( 100, 100 )

        for i in range( 21 ):
            OpenVG.Translate( 30, 0 )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )
            OpenVG.Translate( -30, 0 )
            OpenVG.Rotate( 35 )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

