#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class Rectangles( vt.VisualTestCase ):
    
    '''
    Test for scissoring. Test draws red, green and blue lines with
    scissoring enabled. Scissor rectangles are
    [-10,-10,120,120][10,105,40,90][60,60,30,100][80,80,80,80][150,75,-100,50].
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats   = 1
        self.name = 'OPENVG scissoring rectangles' 
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 3, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        OpenVG.Lines( pathDataIndex, [ 60, 0 ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        pathspercolor = 50

        paints = []
        for i in range( pathspercolor * 3 ):
            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
            OpenVG.CreatePaint( paintIndex )
            paints.append( paintIndex )
            
        for i in range( pathspercolor ):
            alpha = float( i ) / float( 50 )                                                                  # alpha
            OpenVG.ColorPaint( paints[ i ],                     [ i / float( pathspercolor ), 0, 0, alpha ] ) # red
            OpenVG.ColorPaint( paints[ i + pathspercolor ],     [ 0, i / float( pathspercolor ), 0, alpha ] ) # green
            OpenVG.ColorPaint( paints[ i + 2 * pathspercolor ], [ 0, 0, i / float( pathspercolor ), alpha ] ) # blue

        scissors = [ -10, -10,  120, 120,
                      10, 105,   40,  90,
                      60,  60,   30, 100,
                      80,  80,   80,  80,
                     150,  75, -100,  50 ]
        OpenVG.ScissorRects( scissors )

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.Scissoring( 'ON' )
    
        for i in range( pathspercolor ):
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )
            OpenVG.LoadIdentity( )
            # red
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.Translate( 0, i * HEIGHT / pathspercolor )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
            # green
            OpenVG.SetPaint( paints[ i + pathspercolor ], [ 'VG_STROKE_PATH' ] )
            OpenVG.Translate( 70, 0 )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
            # blue
            OpenVG.SetPaint( paints[ i + 2 * pathspercolor ], [ 'VG_STROKE_PATH' ] )
            OpenVG.Translate( 70, 0 )
            OpenVG.DrawPath( 0, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class OnOff( vt.VisualTestCase ):
    
    '''
    Test for scissoring. Test draws red, green and blue lines with scissoring
    enabled only for greens. Scissor rectangles are
    [-10,-10,120,120][10,105,40,90][60,60,30,100][80,80,80,80][150,75,-100,50].
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats   = 1
        self.name = 'OPENVG scissoring on and off' 
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 3, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        OpenVG.Lines( pathDataIndex, [ 60, 0 ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        pathspercolor = 50

        paints = []
        for i in range( pathspercolor * 3 ):
            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )            
            OpenVG.CreatePaint( paintIndex )
            paints.append( paintIndex )

        for i in range( pathspercolor ):
            alpha = float( i ) / float( 50 )                                                                  # alpha
            OpenVG.ColorPaint( paints[ i ],                     [ i / float( pathspercolor ), 0, 0, alpha ] ) # red
            OpenVG.ColorPaint( paints[ i + pathspercolor ],     [ 0, i / float( pathspercolor ), 0, alpha ] ) # green
            OpenVG.ColorPaint( paints[ i + 2 * pathspercolor ], [ 0, 0, i / float( pathspercolor ), alpha ] ) # blue

        scissors = [ -10, -10,  120, 120,
                      10, 105,   40,  90,
                      60,  60,   30, 100,
                      80,  80,   80,  80,
                     150,  75, -100,  50 ]
        OpenVG.ScissorRects( scissors )
    
        ################################################################################
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0 ,0 )
        OpenVG.Scissoring( 'ON' )
    
        for i in range( pathspercolor ):
            OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )
            OpenVG.LoadIdentity()
            # red
            OpenVG.Scissoring( 'OFF' )
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.Translate( 0, i * HEIGHT / pathspercolor )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
            # green
            OpenVG.Scissoring( 'ON' )
            OpenVG.SetPaint( paints[ i + pathspercolor ], [ 'VG_STROKE_PATH' ] )
            OpenVG.Translate( 70, 0 )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
            # blue
            OpenVG.Scissoring( 'OFF' )
            OpenVG.SetPaint( paints[ i + 2 * pathspercolor ], [ 'VG_STROKE_PATH' ] )
            OpenVG.Translate( 70, 0 )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
