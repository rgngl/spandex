#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class DrawingModes( vt.VisualTestCase ):
    
    ''' Test for image drawing modes. Test draws a tiger with black and white
    rectangles and reads surface content to the image. Test clears the surface
    to [0.25,0.25,0.25,0.5] and draws the image with different drawing
    modes. Current fill paint is linear gradient: [50,50,100,100] with
    VG_COLOR_RAMP_SPREAD_PAD and [0.0,0.25,0.0,0.0,0.75,
    0.5,0.0,0.50,0.0,0.50,1.0,0.0,0.0,0.75,0.25].
    '''

    testValues = { 'drawingmode' : [ 'VG_DRAW_IMAGE_NORMAL',
                                     'VG_DRAW_IMAGE_MULTIPLY',
                                     'VG_DRAW_IMAGE_STENCIL' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }
   
    def __init__( self, drawingmode, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.drawingmode = drawingmode
        self.imageformat = imageformat
        self.repeats     = 1
        self.name = 'OPENVG image drawing mode %s %s' % ( imageformat, drawingmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ImageMode( self.drawingmode )
        OpenVG.ClearColor( [ 0.25, 0.25, 0.25, 0.5 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )        
        OpenVG.CreateTiger( tigerIndex )
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.25, 0.25, 0.25, 0.5 ] )
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.imageformat, WIDTH, HEIGHT )
        
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageDataIndex, imageIndex, 0, 0, WIDTH, HEIGHT )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 50, 50 ], [ 150, 150 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_PAD',
                           'ON',
                           [ 0.0, 0.25,  0.0,  0.0, 0.75,
                             0.5,  0.0, 0.50,  0.0, 0.50,
                             1.0,  0.0,  0.0, 0.75, 0.25 ] )

        # ----------------------------------------------------------------------        
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawImage( imageIndex )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class MultiplyDrawingMode( vt.VisualTestCase ):
    
    ''' Test for image multiply drawing mode. Test draws a tiger with black and
    white rectangles and reads the surface content to the image. Test clears the
    surface to [0.25,0.25,0.25,0.5]. Image is drawn with image multiply drawing
    mode. Fill paint varies.
    '''

    testValues = { 'fill'        : [ 'COLOR',
                                     'LINEARGRADIENT',
                                     'RADIALGRADIENT',
                                     'PATTERN' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, fill, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.fill        = fill
        self.imageformat = imageformat
        self.repeats     = 1
        self.name = 'OPENVG image draw multiply drawing mode %s %s' % ( fill, imageformat, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ImageMode( 'VG_DRAW_IMAGE_MULTIPLY' )
        OpenVG.ClearColor( [ 0.25, 0.25, 0.25, 0.5 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )        
        OpenVG.CreateTiger( tigerIndex )
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.25, 0.25, 0.25, 0.5 ] )
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.imageformat, WIDTH, HEIGHT )
        
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageDataIndex, imageIndex, 0, 0, WIDTH, HEIGHT )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paintIndex )

        if self.fill == 'COLOR':
            paintColor = [ 0.5, 1.0, 0.25, 0.75 ]
            OpenVG.ColorPaint( paintIndex, paintColor )
        elif self.fill == 'LINEARGRADIENT':
            OpenVG.LinearGradientPaint( paintIndex, [ 50, 50 ], [ 150, 150 ] )
            OpenVG.ColorRamps( paintIndex,
                               'VG_COLOR_RAMP_SPREAD_REPEAT',
                               'ON',
                               [ 0.0, 0.25,  0.0,  0.0, 0.75,
                                 0.5,  0.0, 0.50,  0.0, 0.50,
                                 1.0,  0.0,  0.0, 0.75, 0.25 ] )
        elif self.fill == 'RADIALGRADIENT':
            OpenVG.RadialGradientPaint( paintIndex, [ 20, 20 ], [ 50, 50 ], 100 )
            OpenVG.ColorRamps( paintIndex,
                               'VG_COLOR_RAMP_SPREAD_REPEAT',
                               'ON',
                               [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        elif self.fill == 'PATTERN':
            patternImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )        
            OpenVG.CreateImage( patternImageIndex, self.imageformat, 20, 20, [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )
            OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
            OpenVG.ClearImage( patternImageIndex, 0, 0, 20, 20 )
            OpenVG.ClearColor( [ 0.25, 1.0, 0.5, 0.5 ] )
            OpenVG.ClearImage( patternImageIndex, 0, 0, 10, 10 )
            OpenVG.ClearColor( [ 0.0, 0.5, 1.0, 0.5 ] )
            OpenVG.ClearImage( patternImageIndex, 10, 10, 10, 10 )
            OpenVG.PatternPaint( paintIndex, patternImageIndex, 'VG_TILE_REPEAT' )     
            
        OpenVG.ClearColor( [ 0.25, 0.25, 0.25, 0.5 ] )
        
        # ----------------------------------------------------------------------        
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawImage( imageIndex )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class StencilDrawingMode( vt.VisualTestCase ):
    
    ''' Test for image stencil drawing mode. Test draws a tiger with black and
    white rectangles and reads the surface content to the image. Test clears the
    surface to [0.25,0.25,0.25,0.5]. Image is drawn with image stencil drawing
    mode. Fill paint varies.
    '''

    testValues = { 'fill'        : [ 'COLOR',
                                     'LINEARGRADIENT',
                                     'RADIALGRADIENT',
                                     'PATTERN' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, fill, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.fill        = fill
        self.imageformat = imageformat
        self.repeats     = 1
        self.name = 'OPENVG image draw stencil drawing mode %s %s' % ( fill, imageformat, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ImageMode( 'VG_DRAW_IMAGE_STENCIL' )
        OpenVG.ClearColor( [ 0.25, 0.25, 0.25, 0.5 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )        
        OpenVG.CreateTiger( tigerIndex )
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.25, 0.25, 0.25, 0.5 ] )
        
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.imageformat, WIDTH, HEIGHT )
        
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageDataIndex, imageIndex, 0, 0, WIDTH, HEIGHT )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paintIndex )

        if self.fill == 'COLOR':
            paintColor = [ 0.5, 1.0, 0.25, 0.75 ]
            OpenVG.ColorPaint( paintIndex, paintColor )
        elif self.fill == 'LINEARGRADIENT':
            OpenVG.LinearGradientPaint( paintIndex, [ 50, 50 ], [ 150, 150 ] )
            OpenVG.ColorRamps( paintIndex,
                               'VG_COLOR_RAMP_SPREAD_REPEAT',
                               'ON',
                               [ 0.0, 0.25,  0.0,  0.0, 0.75,
                                 0.5,  0.0, 0.50,  0.0, 0.50,
                                 1.0,  0.0,  0.0, 0.75, 0.25 ] )
        elif self.fill == 'RADIALGRADIENT':
            OpenVG.RadialGradientPaint( paintIndex, [ 20, 20 ], [ 50, 50 ], 100 )
            OpenVG.ColorRamps( paintIndex,
                               'VG_COLOR_RAMP_SPREAD_REPEAT',
                               'ON',
                               [ 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 ] )
        elif self.fill == 'PATTERN':
            patternImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )        
            OpenVG.CreateImage( patternImageIndex, self.imageformat, 20, 20, [ 'VG_IMAGE_QUALITY_NONANTIALIASED' ] )
            OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
            OpenVG.ClearImage( patternImageIndex, 0, 0, 20, 20 )
            OpenVG.ClearColor( [ 0.25, 1.0, 0.5, 0.5 ] )
            OpenVG.ClearImage( patternImageIndex, 0, 0, 10, 10 )
            OpenVG.ClearColor( [ 0.0, 0.5, 1.0, 0.5 ] )
            OpenVG.ClearImage( patternImageIndex, 10, 10, 10, 10 )
            OpenVG.PatternPaint( paintIndex, patternImageIndex, 'VG_TILE_REPEAT' )     
            
        OpenVG.ClearColor( [ 0.25, 0.25, 0.25, 0.5 ] )
        
        # ----------------------------------------------------------------------        
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawImage( imageIndex )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
