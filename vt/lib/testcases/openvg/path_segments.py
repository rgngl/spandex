#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class Arc( vt.VisualTestCase ):
    
    '''
    Test arc segments with varying angle.
    '''

    testValues = { 'arc' : [ 'SCWARC',
                             'LCWARC',
                             'SCCWARC',
                             'LCCWARC' ]
                   }

    def __init__( self, arc ):
        vt.VisualTestCase.__init__( self )
        self.arc     = arc
        self.repeats = 1        
        self.name = 'OPENVG path arc %s' % ( arc, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 10, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 5 )
        
        angle = 0
        limit = 6

        paths  = []
        paints = []
        
        for i in range( limit ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
            
            OpenVG.MoveTo( pathDataIndex, [ 50, 50 ], 'OFF' )
            OpenVG.Arcs( pathDataIndex, [ 50, 75, angle, 150, 150 ], self.arc, 'OFF' )
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
            
            red   = 1 - float( i ) / float( limit )
            green = float( i ) / float( limit * 0.66 ) % 1.0
            blue  = float( i ) / float( limit * 0.33 ) % 1.0
            alpha = 0.5
            
            OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
            
            angle += 30

            paths.append( pathIndex )
            paints.append( paintIndex )

        # ----------------------------------------------------------------------            
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        for i in range( limit ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class ArcDashing( vt.VisualTestCase ):
    
    '''
    Test dashing in arc segments. Pattern is [12.5,7.5,10.0,5.0] and phase
    [13.5].
    '''

    testValues = { 'capstyle' : [ 'VG_CAP_BUTT',
                                  'VG_CAP_ROUND',
                                  'VG_CAP_SQUARE' ],
                   'arc'      : [ 'SCWARC',
                                  'LCWARC',
                                  'SCCWARC',
                                  'LCCWARC' ] }

    def __init__( self, capstyle, arc ):
        vt.VisualTestCase.__init__( self )
        self.capstyle = capstyle
        self.arc      = arc
        self.repeats  = 1        
        self.name = 'OPENVG path arc dashing %s %s' % ( arc, capstyle, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
       
        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 13.5
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 5, self.capstyle, 'VG_JOIN_ROUND', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
       
        angle = 0
        limit = 6

        paths  = []
        paints = []
        
        for i in range( limit ):
            pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
            OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

            pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
            OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
            
            OpenVG.MoveTo( pathDataIndex, [ 50, 100 ], 'OFF' )
            OpenVG.Arcs( pathDataIndex, [ 50, 75, angle, 150, 100 ], self.arc, 'OFF' )
            OpenVG.AppendPathData( pathIndex, pathDataIndex )

            paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
            OpenVG.CreatePaint( paintIndex )
            
            red   = 1 - float( i ) / float( limit )
            green = float( i ) / float( limit * 0.66 ) % 1.0
            blue  = float( i ) / float( limit * 0.33 ) % 1.0
            alpha = 0.5
            
            OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
            
            angle += 30

            paths.append( pathIndex )
            paints.append( paintIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( limit ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class LineAbsoluteInsideSurface( vt.VisualTestCase ):
    
    '''
    Test for line segments in different absolute coordinates inside the
    surface.
    '''
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1        
        self.name = 'OPENVG path line absolute inside surface'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.75, 0.50, 0.25, 0.5 ] )
        
        originx, originy = 50, 100
        x, y             = 250, -50
        xadd, yadd       = 0, 60
        
        for i in range( 4 ):
            for j in range( 5 ):
                OpenVG.MoveTo( pathDataIndex, [ originx, originy ], 'OFF' )
                OpenVG.Lines( pathDataIndex, [ x, y ], 'OFF' )
                x += xadd
                y += yadd
                
            xadd, yadd = -yadd, xadd
            
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        

######################################################################
class LineRelativeInsideSurface( vt.VisualTestCase ):
    
    '''
    Test for line segments in different relative coordinates inside the
    surface.
    '''
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG path line relative inside surface'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.75, 0.50, 0.25, 0.5 ] )

        originx, originy = 100, 100
        x, y             = 90, -90
        xadd, yadd       = 0, 36
        
        for i in range( 4 ):
            for j in range( 5 ):
                OpenVG.MoveTo( pathDataIndex, [ originx, originy ], 'OFF' )
                OpenVG.Lines( pathDataIndex, [ x, y ], 'ON' )
                x += xadd
                y += yadd
                
            xadd, yadd = -yadd, xadd
            
        OpenVG.AppendPathData( pathIndex, pathDataIndex )
       
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0,0,0,0)
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class LineRelativeOutsideSurface( vt.VisualTestCase ):
    
    '''
    Test for line segments in different relative coordinates outside the
    surface.
    '''
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats   = 1
        self.name = 'OPENVG path line relative outside surface'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.75, 0.50, 0.25, 0.50 ] )
        
        originx, originy = 100, 100
        x, y             = 150, -150
        xadd, yadd       = 0, 60
        
        for i in range( 4 ):
            for j in range( 5 ):
                OpenVG.MoveTo( pathDataIndex, [ originx, originy ], 'OFF' )
                OpenVG.Lines( pathDataIndex, [ x, y ], 'ON' )
                x += xadd
                y += yadd
                
            xadd, yadd = -yadd, xadd
            
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, ['VG_STROKE_PATH'] )
        OpenVG.DrawPath( pathIndex, ['VG_STROKE_PATH'] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class Line( vt.VisualTestCase ):
    
    '''
    Test for line segments in strokes.
    '''
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG path line'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 10, 'VG_CAP_BUTT', 'VG_JOIN_ROUND', 0 )
        
        ix, iy           = 175, 40
        ox, oy           = 190, 10
        ex, ey           = 150, 50
        ixadd, iyadd     = 0, 50
        oxadd, oyadd     = 0, 60
        exadd, eyadd     = 0, 10

        firstx, firsty   = 150, 150

        paths  = []
        paints = []
        c      = 0
        
        for i in range( 4 ):
            for j in range( 3 ):
                
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
                
                OpenVG.MoveTo( pathDataIndex, [ ox, oy ], 'OFF' )
                OpenVG.Lines( pathDataIndex, [ ix, iy ], 'OFF' )
                OpenVG.Lines( pathDataIndex, [ ex, ey ], 'OFF' )
                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                red   = 1 - float( c ) / float( 12.0 )
                green = float( c ) / float( 8.0 ) % 1.0
                blue  = float( c ) / float( 4.0 ) % 1.0
                alpha = 0.5
                
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                ox += oxadd
                oy += oyadd
                ix += ixadd
                iy += iyadd
                ex += exadd
                ey += eyadd
                c  += 1

                paints.append( paintIndex )
                paths.append( pathIndex )
                
            oxadd, oyadd = -oyadd, oxadd
            ixadd, iyadd = -iyadd, ixadd
            exadd, eyadd = -eyadd, exadd

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class LineDashing( vt.VisualTestCase ):
    
    '''
    Test dashing in line segments. Pattern is [12.5,7.5,10.0,5.0] and phase
    [13.5].
    '''

    testValues = { 'capstyle' : [ 'VG_CAP_BUTT',
                                  'VG_CAP_ROUND',
                                  'VG_CAP_SQUARE' ]
                   }

    def __init__( self, capstyle ):
        vt.VisualTestCase.__init__( self )
        self.capstyle = capstyle
        self.repeats  = 1        
        self.name = 'OPENVG path line dashing %s' % ( capstyle, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 13.5
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 5, self.capstyle, 'VG_JOIN_ROUND', 0 )        
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
        
        ix, iy           = 175, 40
        ox, oy           = 190, 10
        ex, ey           = 150, 50
        ixadd, iyadd     = 0, 50
        oxadd, oyadd     = 0, 60
        exadd, eyadd     = 0, 10
        firstx, firsty   = 150, 150

        paths  = []
        paints = []
        c      = 0
        
        for i in range( 4 ):
            for j in range( 3 ):

                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
                
                OpenVG.MoveTo( pathDataIndex, [ ox, oy ], 'OFF' )
                OpenVG.Lines( pathDataIndex, [ ix, iy ], 'OFF' )
                OpenVG.Lines( pathDataIndex, [ ex, ey ], 'OFF' )
                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                red   = 1 - float( c ) / float( 12.0 )
                green = float( c) / float( 8.0 ) % 1.0
                blue  = float( c ) / float( 4.0 ) % 1.0
                alpha = 0.5
                
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                ox += oxadd
                oy += oyadd
                ix += ixadd
                iy += iyadd
                ex += exadd
                ey += eyadd
                c  += 1

                paths.append( pathIndex )
                paints.append( paintIndex )
                
            oxadd, oyadd = -oyadd, oxadd
            ixadd, iyadd = -iyadd, ixadd
            exadd, eyadd = -eyadd, exadd

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0 ,0 )
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class Cubic( vt.VisualTestCase ):
    
    '''
    Test for cubic segments in strokes. First path (RED) is move [190,10], cubic
    [175,25][150,150][125,125] and cubic [50,50][190,10][175,25]. Coordinates
    and color change in forthcoming paths.
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1        
        self.name = 'OPENVG path cubic'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 10, 'VG_CAP_BUTT', 'VG_JOIN_ROUND', 0 )
        
        ix, iy           = 175, 25
        ox, oy           = 190, 10
        ixadd, iyadd     = 0, 50
        oxadd, oyadd     = 0, 60
        firstx, firsty   = 150, 150
        secondx, secondy = 125, 125
        thirdx, thirdy   = 50, 50

        paths  = []
        paints = []
        c      = 0 
        
        for i in range( 4 ):
            for j in range( 3 ):

                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
                OpenVG.MoveTo( pathDataIndex, [ ox, oy ], 'OFF' )
                OpenVG.CubicBeziers( pathDataIndex, [ ix, iy, firstx, firsty, secondx, secondy ], 'OFF' )
                OpenVG.CubicBeziers( pathDataIndex, [ thirdx, thirdy, ox, oy, ix, iy ], 'OFF' )
                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                red   = 1 - float( c ) / float( 12.0 )
                green = float( c ) / float( 8.0 ) % 1.0
                blue  = float( c ) / float( 4.0 ) % 1.0
                alpha = 0.5
                
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                ox += oxadd
                oy += oyadd
                ix += ixadd
                iy += iyadd
                c  += 1

                paths.append( pathIndex )
                paints.append( paintIndex )
                
            oxadd, oyadd = -oyadd, oxadd
            ixadd, iyadd = -iyadd, ixadd

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class CubicDashing( vt.VisualTestCase ):
    
    '''
    Test dashing in cubic segments. First path (RED) is move [190,10], cubic
    [175,25][150,150][125,125] and cubic [50,50][190,10][175,25]. Coordinates
    and color change in forthcoming paths. Dashpattern is [12.5,7.5,10.0,5.0]
    and dashphase is [13.5].
    '''

    testValues = { 'capstyle' : [ 'VG_CAP_BUTT',
                                  'VG_CAP_ROUND',
                                  'VG_CAP_SQUARE' ]
                   }

    def __init__( self, capstyle ):
        vt.VisualTestCase.__init__( self )
        self.capstyle = capstyle
        self.repeats  = 1        
        self.name = 'OPENVG path cubic dashing %s' % ( capstyle, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 13.5
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 5, self.capstyle, 'VG_JOIN_ROUND', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
        
        ix, iy           = 175, 25
        ox, oy           = 190, 10
        ixadd, iyadd     = 0, 50
        oxadd, oyadd     = 0, 60
        firstx, firsty   = 150, 150
        secondx, secondy = 125, 125
        thirdx, thirdy   = 50, 50

        paths  = []
        paints = []
        c      = 0
        
        for i in range( 4 ):
            for j in range( 3 ):
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
                
                OpenVG.MoveTo( pathDataIndex, [ ox, oy ], 'OFF' )
                OpenVG.CubicBeziers( pathDataIndex, [ ix, iy, firstx, firsty, secondx, secondy ], 'OFF' )
                OpenVG.CubicBeziers( pathDataIndex, [ thirdx, thirdy, ox, oy, ix, iy ], 'OFF' )
                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                red   = 1 - float( c ) / float( 12.0 )
                green = float( c ) / float( 8.0 ) % 1.0
                blue  = float( c ) / float( 4.0 ) % 1.0
                alpha = 0.5
                
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                ox += oxadd
                oy += oyadd
                ix += ixadd
                iy += iyadd
                c  += 1

                paths.append( pathIndex )
                paints.append( paintIndex )
                
            oxadd, oyadd = -oyadd, oxadd
            ixadd, iyadd = -iyadd, ixadd

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear(0,0,0,0)
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class Quad( vt.VisualTestCase ):
    
    '''
    Test for quad segments in different coordinates inside the surface.
    '''
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG path quad'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 10, 'VG_CAP_BUTT', 'VG_JOIN_ROUND', 0 )
        
        ix, iy           = 175, 25
        ox, oy           = 190, 10
        ixadd, iyadd     = 0, 50
        oxadd, oyadd     = 0, 60
        firstx, firsty   = 150, 150
        secondx, secondy = 100, 100
        thirdx, thirdy   = 50, 50

        paths  = []
        paints = []
        c      = 0
        
        for i in range( 4 ):
            for j in range( 3 ):

                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
                
                OpenVG.MoveTo( pathDataIndex, [ ox, oy ], 'OFF' )
                OpenVG.QuadraticBeziers( pathDataIndex, [ firstx, firsty, ix, iy ], 'OFF' )
                OpenVG.QuadraticBeziers( pathDataIndex, [ secondx, secondy, thirdx, thirdy ], 'OFF' )
                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                red   = 1 - float( c ) / float( 12.0 )
                green = float( c ) / float( 8.0 ) % 1.0
                blue  = float( c ) / float( 4.0 ) % 1.0
                alpha = 0.5
                
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                ox += oxadd
                oy += oyadd
                ix += ixadd
                iy += iyadd
                c  += 1

                paths.append( pathIndex )
                paints.append( paintIndex )
                
            oxadd, oyadd = -oyadd, oxadd
            ixadd, iyadd = -iyadd, ixadd

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class QuadDashing( vt.VisualTestCase ):
    
    '''
    Test dashing in quad segments. Pattern is [12.5,7.5,10.0,5.0] and phase
    [13.5].
    '''

    testValues = { 'capstyle' : [ 'VG_CAP_BUTT',
                                  'VG_CAP_ROUND',
                                  'VG_CAP_SQUARE' ]
                   }

    def __init__( self, capstyle ):
        vt.VisualTestCase.__init__( self )
        self.capstyle = capstyle
        self.repeats  = 1
        self.name = 'OPENVG path quad dashing %s' % ( capstyle, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        dashpattern = [ 12.5,7.5,10.0,5.0 ]
        dashphase   = 13.5
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 5, self.capstyle, 'VG_JOIN_ROUND', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
        
        ix, iy           = 175, 25
        ox, oy           = 190, 10
        ixadd, iyadd     = 0, 50
        oxadd, oyadd     = 0, 60
        firstx, firsty   = 150, 150
        secondx, secondy = 100, 100
        thirdx, thirdy   = 50, 50

        paths  = []
        paints = []
        c      = 0
        
        for i in range( 4 ):
            for j in range( 3 ):
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
                
                OpenVG.MoveTo( pathDataIndex, [ ox, oy ], 'OFF' )
                OpenVG.QuadraticBeziers( pathDataIndex, [ firstx, firsty, ix, iy ], 'OFF' )
                OpenVG.QuadraticBeziers( pathDataIndex, [ secondx, secondy, thirdx, thirdy ], 'OFF' )
                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                red   = 1 - float( c ) / float( 12.0 )
                green = float( c ) / float( 8.0 ) % 1.0
                blue  = float( c ) / float( 4.0 ) % 1.0
                alpha = 0.5
                
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                ox += oxadd
                oy += oyadd
                ix += ixadd
                iy += iyadd
                c  += 1

                paths.append( pathIndex )
                paints.append( paintIndex )
                
            oxadd, oyadd = -oyadd, oxadd
            ixadd, iyadd = -iyadd, ixadd

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear(0,0,0,0)
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class Scubic( vt.VisualTestCase ):
    
    '''
    Test for scubic segments in different coordinates inside the surface.
    '''
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG path scubic'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 10, 'VG_CAP_BUTT', 'VG_JOIN_ROUND', 0 )
        
        ix, iy           = 175, 25
        ox, oy           = 190, 10
        ixadd, iyadd     = 0, 50
        oxadd, oyadd     = 0, 60
        firstx, firsty   = 150, 150
        secondx, secondy = 125, 125
        thirdx, thirdy   = 50, 50

        paths  = []
        paints = []
        c      = 0
        
        for i in range( 4 ):
            for j in range( 3 ):
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
                
                OpenVG.MoveTo( pathDataIndex, [ ox, oy ], 'OFF' )
                OpenVG.CubicBeziers( pathDataIndex, [ ix, iy, firstx, firsty, secondx, secondy ], 'OFF' )
                OpenVG.SCubicBeziers( pathDataIndex, [ thirdx, thirdy, ix, iy ], 'OFF' )
                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                red   = 1 - float( c ) / float( 12.0 )
                green = float( c ) / float( 8.0 ) % 1.0
                blue  = float( c ) / float( 4.0 ) % 1.0
                alpha = 0.5
                
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                ox += oxadd
                oy += oyadd
                ix += ixadd
                iy += iyadd
                c  += 1

                paths.append( pathIndex )
                paints.append( paintIndex )
                
            oxadd, oyadd = -oyadd, oxadd
            ixadd, iyadd = -iyadd, ixadd

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class ScubicDashing( vt.VisualTestCase ):
    
    '''
    Test dashing in scubic segments. Pattern is [12.5,7.5,10.0,5.0] and phase
    [13.5].
    '''

    testValues = { 'capstyle' : [ 'VG_CAP_BUTT',
                                  'VG_CAP_ROUND',
                                  'VG_CAP_SQUARE' ]
                   }

    def __init__( self, capstyle ):
        vt.VisualTestCase.__init__( self )
        self.capstyle = capstyle
        self.repeats  = 1        
        self.name = 'OPENVG path scubic dashing %s' % ( capstyle, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 13.5
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 5, self.capstyle, 'VG_JOIN_ROUND', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
        
        ix, iy           = 175, 25
        ox, oy           = 190, 10
        ixadd, iyadd     = 0, 50
        oxadd, oyadd     = 0, 60
        firstx, firsty   = 150, 150
        secondx, secondy = 125, 125
        thirdx, thirdy   = 50, 50
        
        paths  = []
        paints = []
        c      = 0
        
        for i in range( 4 ):
            for j in range( 3 ):
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
                
                OpenVG.MoveTo( pathDataIndex, [ ox, oy ], 'OFF' )
                OpenVG.CubicBeziers( pathDataIndex, [ ix, iy, firstx, firsty, secondx, secondy ], 'OFF' )
                OpenVG.SCubicBeziers( pathDataIndex, [ thirdx, thirdy, ix, iy ], 'OFF' )
                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                red   = 1 - float( c ) / float( 12.0 )
                green = float( c ) / float( 8.0 ) % 1.0
                blue  = float( c ) / float( 4.0 ) % 1.0
                alpha = 0.5
                
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                ox += oxadd
                oy += oyadd
                ix += ixadd
                iy += iyadd
                c  += 1

                paths.append( pathIndex )
                paints.append( paintIndex )
                
            oxadd, oyadd = -oyadd, oxadd
            ixadd, iyadd = -iyadd, ixadd

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0 ,0, 0 )
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class Squad( vt.VisualTestCase ):
    
    '''
    Test for squad segments in different coordinates inside the surface.
    '''
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1
        self.name = 'OPENVG path squad'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 10, 'VG_CAP_BUTT', 'VG_JOIN_ROUND', 0 )
        
        ix, iy           = 175, 25
        ox, oy           = 190, 10
        ixadd, iyadd     = 0, 50
        oxadd, oyadd     = 0, 60
        firstx, firsty   = 150, 150
        secondx, secondy = 100, 100

        paths  = []
        paints = []
        c      = 0
        
        for i in range( 4 ):
            for j in range( 3 ):
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
                
                OpenVG.MoveTo( pathDataIndex, [ ox, oy ], 'OFF' )
                OpenVG.QuadraticBeziers( pathDataIndex, [ firstx, firsty, ix, iy ], 'OFF' )
                OpenVG.SQuadBeziers( pathDataIndex, [ secondx, secondy ], 'OFF' )
                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                red   = 1 - float( c ) / float( 12.0 )
                green = float( c ) / float( 8.0 ) % 1.0
                blue  = float( c ) / float( 4.0 ) % 1.0
                alpha = 0.5
                
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                ox += oxadd
                oy += oyadd
                ix += ixadd
                iy += iyadd
                c  += 1

                paths.append( pathIndex )
                paints.append( paintIndex )
                
            oxadd, oyadd = -oyadd, oxadd
            ixadd, iyadd = -iyadd, ixadd

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class SquadDashing( vt.VisualTestCase ):
    
    '''
    Test dashing in squad segments. Pattern is [12.5,7.5,10.0,5.0] and phase
    [13.5].
    '''

    testValues = { 'capstyle' : [ 'VG_CAP_BUTT',
                                  'VG_CAP_ROUND',
                                  'VG_CAP_SQUARE' ]
                   }

    def __init__( self, capstyle ):
        vt.VisualTestCase.__init__( self )
        self.capstyle = capstyle
        self.repeats  = 1
        self.name = 'OPENVG path squad dashing %s' % ( capstyle, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        dashpattern = [ 12.5, 7.5, 10.0, 5.0 ]
        dashphase   = 13.5
        
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        OpenVG.Stroke( 5, self.capstyle, 'VG_JOIN_ROUND', 0 )
        OpenVG.DashPattern( dashpattern, dashphase, 'OFF' )
        
        ix, iy           = 175, 25
        ox, oy           = 190, 10
        ixadd, iyadd     = 0, 50
        oxadd, oyadd     = 0, 60
        firstx, firsty   = 150, 150
        secondx, secondy = 100, 100

        paths  = []
        paints = []
        c      = 0
        
        for i in range( 4 ):
            for j in range( 3 ):
                pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

                pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
                
                OpenVG.MoveTo( pathDataIndex, [ ox, oy ], 'OFF' )
                OpenVG.QuadraticBeziers( pathDataIndex, [ firstx, firsty, ix, iy ], 'OFF' )
                OpenVG.SQuadBeziers( pathDataIndex, [ secondx, secondy ], 'OFF' )
                OpenVG.AppendPathData( pathIndex, pathDataIndex )

                paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( paintIndex )
                
                red   = 1 - float( c ) / float( 12.0 )
                green = float( c ) / float( 8.0 ) % 1.0
                blue  = float( c ) / float( 4.0 ) % 1.0
                alpha = 0.5
                
                OpenVG.ColorPaint( paintIndex, [ red, green, blue, alpha ] )
                
                ox += oxadd
                oy += oyadd
                ix += ixadd
                iy += iyadd
                c  += 1

                paths.append( pathIndex )
                paints.append( paintIndex )
                
            oxadd, oyadd = -oyadd, oxadd
            ixadd, iyadd = -iyadd, ixadd

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        
        for i in range( 12 ):
            OpenVG.SetPaint( paints[ i ], [ 'VG_STROKE_PATH' ] )
            OpenVG.DrawPath( paths[ i ], [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
#####################################################################
class DiscreteSQuadBeziers( vt.VisualTestCase  ): 
	
    '''
    The purpose of the test:
    Testing SQuadBeziers function by drawing discrete squad beziers
	
    The test steps:
    1. Create path data for squad beziers
    2. Draws translated squad beziers in a loop ( beziercount )
	
    expected output: Discrete squad beziers ( beziercount )
    '''
 
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.beziercount = 10
        self.repeats     = 1                
        self.name = "OPENVG discrete squad beziers %d" % ( self.beziercount, )
	
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
            		
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
		
        strokePaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )
		
        OpenVG.StrokeLineWidth( 10 )
        OpenVG.StrokeCapStyle( 'VG_CAP_ROUND' )
        OpenVG.StrokeJoinStyle( 'VG_JOIN_ROUND' )
		
        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
		
        scale   = 65535.0
        xoffset = 20
        yoffset = 10
        OpenVG.SQuadBeziers( pathDataIndex,
                             [ scale * ( WIDTH / 3 ),               scale * yoffset,
                               scale * ( WIDTH - xoffset * 2 ),     0 ],
                             'OFF' )
		
        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           2,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
		
        OpenVG.AppendPathData( pathIndex, pathDataIndex )
            		
        # ------------------------------------------------------------
        self.beginBenchmarkActions()
		
        OpenVG.Clear( 0, 0, 0, 0 )
		
        ytrans = HEIGHT / ( self.beziercount + 1 )

        OpenVG.LoadIdentity()
        OpenVG.Translate( xoffset, 0 )
        for l in range( self.beziercount ):
            OpenVG.Translate( 0, ytrans )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                    
	
#####################################################################
class ConnectedSQuadBeziers( vt.VisualTestCase  ):
    
    '''
    The purpose of the test:
    Testing SQuadBeziers function by drawing connected squad beziers
	
    The test steps:
    1. Create path data for squad bezier
    2. Draws squad beziers ( beziercount )
	
    expected output: Connected squad beziers ( beziercount )
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.beziercount = 10
        self.repeats     = 1            
        self.name = "OPENVG connected squad beziers %d" % ( self.beziercount, )
	
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions() 

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
		
        strokePaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.StrokeLineWidth( 10 )
        OpenVG.StrokeCapStyle( 'VG_CAP_ROUND' )
        OpenVG.StrokeJoinStyle( 'VG_JOIN_ROUND' )
		
        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
		
        scale   = 65535.0
        xoffset = 20
        yoffset = HEIGHT / ( self.beziercount + 1 )
        y       = yoffset
        
        OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
        pathCoordinates = []
        
        for i in range( self.beziercount ):
            pathCoordinates += [ scale * ( WIDTH / 4 ), scale * ( y - yoffset / 2 ),
                                 scale * ( WIDTH / 4 * 3 ), scale * ( y - yoffset / 2 ) ]
            if i < ( self.beziercount - 1 ):
                pathCoordinates += [ scale * ( WIDTH / 4 * 3 ) , scale * ( y + yoffset / 2 ),
                                     scale * ( WIDTH / 4  ), scale * ( y + yoffset ) ]
		
            y += yoffset

        OpenVG.SQuadBeziers( pathDataIndex,
                             pathCoordinates,
                             'OFF' )

        OpenVG.ClosePath( pathDataIndex )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
		
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ------------------------------------------------------------
        self.beginBenchmarkActions()
		
        OpenVG.Clear( 0, 0, 0, 0 )
		
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
		
        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


#####################################################################
class DiscreteSCubicBeziers( vt.VisualTestCase ):  
	
    '''
    The purpose of the test:
    Testing SCubicBeziers function by drawing discrete scubic beziers
	
    The test steps:
    1. Create path data for scubic beziers
    2. Draws translated scubic beziers in a loop ( beziercount )
	
    expected output: Discrete scubic beziers ( beziercount )
    '''
	
    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.beziercount = 10
        self.repeats     = 1        
        self.name = "OPENVG discrete scubic beziers %d" % ( self.beziercount, )
		
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()
		
        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
		  
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
		
        strokePaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )
		
        OpenVG.StrokeLineWidth( 10 )
        OpenVG.StrokeCapStyle( 'VG_CAP_ROUND' )
        OpenVG.StrokeJoinStyle( 'VG_JOIN_ROUND' )
		
        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
		
        scale   = 65535.0
        xoffset = 20
        yoffset = 20
        OpenVG.SCubicBeziers( pathDataIndex,
                              [ scale * ( WIDTH / 3 ),               scale * yoffset,
                                scale * ( WIDTH - xoffset * 2 ),     0 ],
                              'OFF' )
		
        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           2,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
		
        OpenVG.AppendPathData( pathIndex, pathDataIndex )
				
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
		
        OpenVG.Clear( 0, 0, 0, 0 )
		
        ytrans = HEIGHT / ( self.beziercount + 1 )
        OpenVG.LoadIdentity()
        OpenVG.Translate( xoffset, 0 )
        for l in range( self.beziercount ):
            OpenVG.Translate( 0, ytrans )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
#####################################################################
class ConnectedSCubicBeziers( vt.VisualTestCase ):  

    '''
    The purpose of the test:
    Testing SCubicBeziers function by drawing connected scubic beziers
	
    The test steps:
    1. Create path data for scubic beziers
    2. Draws scubic beziers ( beziercount )
	
    expected output: Connected scubic beziers ( beziercount )
    '''
    
    def __init__( self ):
        vt.VisualTestCase.__init__( self)
        self.beziercount =  10
        self.repeats     = 1        
        self.name = "OPENVG connected scubic beziers %d" % ( self.beziercount, )
		
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()        

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
	
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
		
        strokePaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )
		
        OpenVG.StrokeLineWidth( 10 )
        OpenVG.StrokeCapStyle( 'VG_CAP_ROUND' )
        OpenVG.StrokeJoinStyle( 'VG_JOIN_ROUND' )
		
        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
		
        #scale   = 65535.0
        scale   = 1
        xoffset = 20
        yoffset = HEIGHT / ( self.beziercount + 1 )
        y       = yoffset
        
        OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
        pathCoordinates = []
        for i in range( self.beziercount ):
            pathCoordinates += [ scale * ( WIDTH / 4 ), scale * ( y - yoffset / 2 ),
					 scale * ( WIDTH / 4 * 3 ), scale * ( y - yoffset / 2 ) ]
            if i < ( self.beziercount - 1 ):
                pathCoordinates += [ scale * ( WIDTH / 4 * 3 ) , scale * ( y + yoffset / 2 ),
					     scale * ( WIDTH / 4  ), scale * ( y + yoffset ) ]
		
            y += yoffset
            
        OpenVG.SCubicBeziers( pathDataIndex,
                              pathCoordinates,
                              'OFF' )
		    
        OpenVG.ClosePath( pathDataIndex )
		
        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           1,
                           1,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
		
        OpenVG.AppendPathData( pathIndex, pathDataIndex )
        
        # ------------------------------------------------------------
        self.beginBenchmarkActions()
		
        OpenVG.Clear( 0, 0, 0, 0 )
		
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


#####################################################################
class DiscreteQuadraticBeziers( vt.VisualTestCase ):
    
    '''
    The purpose of the test:
    Testing QuadraticBeziers function by drawing discrete quadratic beziers
	
    The test steps:
    1. Create path data for quadratic beziers
    2. Draws translated the quadratic beziers in a loop ( beziercount )
	
    expected output: Discrete quadratic beziers ( beziercount )
    '''
	
    def __init__( self ):
        vt.VisualTestCase.__init__( self)
        self.beziercount =  10
        self.repeats   = 1                
        self.name = "OPENVG discrete quadratic beziers %d" % (  self.beziercount, )
	
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
	
        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
		
        strokePaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )
		
        OpenVG.StrokeLineWidth( 10 )
        OpenVG.StrokeCapStyle( 'VG_CAP_ROUND' )
        OpenVG.StrokeJoinStyle( 'VG_JOIN_ROUND' )
		
        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
		
        scale   = 65535.0
        xoffset = 20
        yoffset = 20
        OpenVG.QuadraticBeziers( pathDataIndex,
                                 [ scale * ( WIDTH / 3 ),               scale * yoffset * 2,
                                   scale * ( WIDTH - xoffset * 2 ),     0 ],
                                 'OFF' )
		
        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                           'VG_PATH_DATATYPE_S_32',
                           1.0 / scale,
                           0,
                           2,
                           4,
                           [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
		
        OpenVG.AppendPathData( pathIndex, pathDataIndex )
        	
        # ------------------------------------------------------------
        self.beginBenchmarkActions()
		
        OpenVG.Clear(0,0,0,0)
		
        ytrans = HEIGHT / ( self.beziercount + 1 )
        OpenVG.LoadIdentity()
        OpenVG.Translate( xoffset, 0 )
        for l in range( self.beziercount ):
            OpenVG.Translate( 0, ytrans )
            OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )
		
        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
#####################################################################
class ConnectedQuadraticBeziers( vt.VisualTestCase ):
    
    '''
    The purpose of the test:
    Testing QuadraticBeziers function by drawing connected quadratic beziers
	
    The test steps:
    1. Create path data for quadratic bezier
    2. Draws quadratic beziers ( beziercount )
	
    expected output: connected quadratic beziers ( beziercount )
    '''
	
    def __init__( self ):
        vt.VisualTestCase.__init__( self)
        self.beziercount =  10
        self.repeats   = 1
        self.name = "OPENVG connected quadratic beziers %d" % ( self.beziercount, )
		
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()
		
        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
		
        strokePaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( strokePaintIndex )
        OpenVG.ColorPaint( strokePaintIndex, [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenVG.SetPaint( strokePaintIndex, [ 'VG_STROKE_PATH' ] )
		
        OpenVG.StrokeLineWidth( 10 )
        OpenVG.StrokeCapStyle( 'VG_CAP_ROUND' )
        OpenVG.StrokeJoinStyle( 'VG_JOIN_ROUND' )
		
        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
		
        scale   = 65535.0
        xoffset = 20
        yoffset = HEIGHT / ( self.beziercount + 1 )
        y       = yoffset
        OpenVG.MoveTo( pathDataIndex, [ scale * xoffset, y * scale ], 'OFF' )
        pathCoordinates = []
        for i in range( self.beziercount ):           
            pathCoordinates += [ scale * ( WIDTH / 3 ), scale * ( y - yoffset ),
                                 scale * ( WIDTH - xoffset ), scale * y ]
            y += yoffset
            if i < ( self.beziercount - 1 ):               
                pathCoordinates += [ scale * ( WIDTH / 3 ) , scale * ( y + yoffset ),
                                     scale * xoffset, scale * y ]
			
        OpenVG.QuadraticBeziers( pathDataIndex,
                             pathCoordinates,
                             'OFF' )
		    
        OpenVG.ClosePath( pathDataIndex )
		
        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex,
                       'VG_PATH_DATATYPE_S_32',
                       1.0 / scale,
                       0,
                       1,
                       1,
                       [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )
		
        OpenVG.AppendPathData( pathIndex, pathDataIndex )
		
        # ------------------------------------------------------------
        self.beginBenchmarkActions()
		
        OpenVG.Clear(0,0,0,0)
		
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

