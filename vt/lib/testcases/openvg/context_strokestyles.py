#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

import math
WIDTH  = 200
HEIGHT = 200

######################################################################
class EndCapStyles( vt.VisualTestCase ):
    
    '''
    Test for stroke endcap styles. Test draws a very wide line with different
    endcaps.
    '''

    testValues = { 'cap' : [ 'VG_CAP_BUTT',
                             'VG_CAP_ROUND',
                             'VG_CAP_SQUARE' ] }

    def __init__( self, cap ):
        vt.VisualTestCase.__init__( self )
        self.cap     = cap
        self.repeats = 1        
        self.name = 'OPENVG stroke endcap style %s' % ( cap, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 125, self.cap, 'VG_JOIN_BEVEL', 0 )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.75, 0.50, 0.25, 0.5 ] )
        
        OpenVG.MoveTo( pathDataIndex, [ 75, 100 ], 'OFF') 
        OpenVG.Lines( pathDataIndex, [ 50, 0 ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------        
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class JoinStyles( vt.VisualTestCase ):
    
    '''
    Test for stroke join styles. Test draws two lines and changes the join
    style and the angle between the lines. Stroke width is 40 and miter length
    5.
    '''

    testValues = { 'join' : [ 'VG_JOIN_BEVEL',
                              'VG_JOIN_ROUND',
                              'VG_JOIN_MITER' ],
                   'angle' : range( 0, 380, 13 )
                   }

    def __init__( self, join, angle ):
        vt.VisualTestCase.__init__( self )
        self.join    = join
        self.angle   = angle
        self.repeats = 1        
        self.name = 'OPENVG stroke join style %s with angle %s' % ( join, angle, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 40, 'VG_CAP_BUTT', self.join, 5 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.75, 0.50, 0.25, 0.5 ] )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        
        r = 50
        h = r * math.sin( self.angle * math.pi / 180 )
        w = math.sqrt( math.pow( r, 2 ) - math.pow( h, 2 ) )
                
        if self.angle > 90 and self.angle < 270:
            w = -w
            
        OpenVG.MoveTo( pathDataIndex, [ 25, 100 ], 'OFF') 
        OpenVG.Lines( pathDataIndex, [ r, 0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ w, h ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class CapAndJoinStylesInPathClosing( vt.VisualTestCase ):
    
    '''
    Test for stroke cap and join styles in path closing (lower left
    corner). Test draws two line segments [50,50,50,150] and [50,150,150,150]
    and close segment.
    '''

    testValues = { 'capstyle' : [ 'VG_CAP_BUTT',
                                  'VG_CAP_ROUND',
                                  'VG_CAP_SQUARE' ],
                   'joinstyle' : [ 'VG_JOIN_BEVEL',
                                   'VG_JOIN_ROUND',
                                   'VG_JOIN_MITER' ]
                   }

    def __init__( self, capstyle, joinstyle ):
        vt.VisualTestCase.__init__( self )
        self.capstyle  = capstyle
        self.joinstyle = joinstyle
        self.repeats   = 1        
        self.name = 'OPENVG path closing with endcap %s and join %s styles' % ( capstyle, joinstyle, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 25, self.capstyle, self.joinstyle, 5 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.75, 0.50, 0.25, 0.5 ] )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        OpenVG.MoveTo( pathDataIndex, [ 50, 50 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ 150, 50 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ 150, 150 ], 'OFF' )
        OpenVG.ClosePath( pathDataIndex )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )
        
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawPath( pathIndex, ['VG_STROKE_PATH'] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class EndCapStylesInSeparateSegments( vt.VisualTestCase ):
    
    '''
    Test for stroke endcap styles. Test draws two lines and changes the
    endcap style and the angle between the lines. Stroke width is 40 and miter
    length 5.
    '''

    testValues = { 'endcap' : [ 'VG_CAP_BUTT',
                                'VG_CAP_ROUND',
                                'VG_CAP_SQUARE' ],
                   'angle' : range( 0, 380, 13 )
                   }

    def __init__( self, endcap, angle ):
        vt.VisualTestCase.__init__( self )
        self.endcap  = endcap
        self.angle   = angle
        self.repeats = 1        
        self.name = 'OPENVG stroke endcap style %s with angle %s' % ( endcap, angle, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 40, self.endcap, 'VG_JOIN_BEVEL', 5 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [0.75,0.50,0.25,0.5] )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        
        r = 50
        h = r * math.sin( self.angle * math.pi / 180 )
        w = math.sqrt( math.pow( r, 2 ) - math.pow( h, 2 ) )
                
        if self.angle > 90 and self.angle < 270:
            w = -w
            
        OpenVG.MoveTo( pathDataIndex, [ 25, 100 ], 'OFF') 
        OpenVG.Lines( pathDataIndex, [ r, 0 ], 'ON' )
        OpenVG.Lines( pathDataIndex, [ w, h ], 'ON' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paintIndex, [ 'VG_STROKE_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
