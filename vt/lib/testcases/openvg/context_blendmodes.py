#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class TransparentColor( vt.VisualTestCase ):
    
    '''
    Test for blendmodes with same transparent color. Surface clearcolor is
    [0.5,0.5,0.5,0.5]. Both rectangles are drawn with color [0.5,0.5,0.5,0.5].
    '''

    testValues = { 'blendmode' : [ 'VG_BLEND_SRC',
                                   'VG_BLEND_SRC_OVER',
                                   'VG_BLEND_DST_OVER',
                                   'VG_BLEND_SRC_IN',
                                   'VG_BLEND_DST_IN',
                                   'VG_BLEND_MULTIPLY',
                                   'VG_BLEND_SCREEN',
                                   'VG_BLEND_DARKEN',
                                   'VG_BLEND_LIGHTEN',
                                   'VG_BLEND_ADDITIVE' ]
                   }
    
    def __init__( self, blendmode ):
        vt.VisualTestCase.__init__( self )
        self.blendmode = blendmode
        self.repeats   = 1
        self.name = 'OPENVG blendmode with same transparent color %s' % ( blendmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenVG.BlendMode( self.blendmode )
        
        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        
        OpenVG.Lines( pathDataIndex, [ WIDTH / 2, 0 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ WIDTH / 2, HEIGHT / 2 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ 0, HEIGHT / 2 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ 0, 0 ], 'OFF' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.ColorPaint( paintIndex, [ 0.5, 0.5, 0.5, 0.5 ] )
       
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.Translate( 25, 25 )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.Translate( 50, 50 )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
        
######################################################################
class TransparentColors( vt.VisualTestCase ):
    
    '''
    Test for blendmodes with different transparent colors. Surface
    clear color is [0.5,0.5,0.5,0.5]. Lower left rectangle is drawn with color
    [0.25,0.5,0.75,0.25] and upper right with [0.75,0.5,0.25,0.75].
    '''

    testValues = { 'blendmode' : [ 'VG_BLEND_SRC',
                                   'VG_BLEND_SRC_OVER',
                                   'VG_BLEND_DST_OVER',
                                   'VG_BLEND_SRC_IN',
                                   'VG_BLEND_DST_IN',
                                   'VG_BLEND_MULTIPLY',
                                   'VG_BLEND_SCREEN',
                                   'VG_BLEND_DARKEN',
                                   'VG_BLEND_LIGHTEN',
                                   'VG_BLEND_ADDITIVE' ]
                   }
    
    def __init__( self, blendmode ):
        vt.VisualTestCase.__init__( self )
        self.blendmode = blendmode
        self.repeats   = 1
        self.name = 'OPENVG blendmode with different transparent colors %s' % ( blendmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenVG.BlendMode( self.blendmode )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        OpenVG.Lines( pathDataIndex, [ WIDTH / 2, 0 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ WIDTH / 2, HEIGHT / 2 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ 0, HEIGHT / 2 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ 0, 0 ], 'OFF' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint1Index )
        OpenVG.ColorPaint( paint1Index, [ 0.25, 0.5, 0.75, 0.25 ] )

        paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint2Index )
        OpenVG.ColorPaint( paint2Index, [ 0.75, 0.5, 0.25, 0.75 ] )
       
        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.Translate( 25, 25 )
        OpenVG.SetPaint( paint1Index, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.Translate( 50, 50 )
        OpenVG.SetPaint( paint2Index, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class VaryingColors( vt.VisualTestCase ):
    
    '''
    Test for blendmodes with varying colors and alphas. Surface clear color is
    [0.5,0.5,0.5,0.5]. Both rectangles are drawn with linear gradient
    (gradient[0,0,0,100], spread[pad] and colors[0.0,0.75,0.50,0.25,1.0,0.5,
    0.0,0.0,0.0,0.0,1.0,0.25,0.50,0.75,1.0].'''

    testValues = { 'blendmode' : [ 'VG_BLEND_SRC',
                                   'VG_BLEND_SRC_OVER',
                                   'VG_BLEND_DST_OVER',
                                   'VG_BLEND_SRC_IN',
                                   'VG_BLEND_DST_IN',
                                   'VG_BLEND_MULTIPLY',
                                   'VG_BLEND_SCREEN',
                                   'VG_BLEND_DARKEN',
                                   'VG_BLEND_LIGHTEN',
                                   'VG_BLEND_ADDITIVE' ]
                   }
    
    def __init__( self, blendmode ):
        vt.VisualTestCase.__init__( self )
        self.blendmode = blendmode
        self.repeats   = 1
        self.name = 'OPENVG blendmode with varying colors %s' % ( blendmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ---------------------------------------------------------------------- 
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        OpenVG.BlendMode( self.blendmode )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        OpenVG.Lines( pathDataIndex, [ WIDTH / 2, 0 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ WIDTH / 2, HEIGHT / 2 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ 0, HEIGHT / 2 ], 'OFF' )
        OpenVG.Lines( pathDataIndex, [ 0, 0 ], 'OFF' )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
        OpenVG.LinearGradientPaint( paintIndex, [ 0, 0 ], [ 100, 0 ] )
        OpenVG.ColorRamps( paintIndex,
                           'VG_COLOR_RAMP_SPREAD_PAD',
                           'ON',
                           [ 0.0, 0.75, 0.50, 0.25, 1.0,
                             0.5, 0.00, 0.00, 0.00, 0.0,
                             1.0, 0.25, 0.50, 0.75, 1.0 ] )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenVG.Clear(0,0,0,0)
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.Translate( 25, 25 )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )
        OpenVG.Translate( 50, 50 )
        OpenVG.DrawPath( pathIndex, [ 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
