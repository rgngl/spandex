# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class Info( vt.VisualTestCase ):

        ''' The purpose of the test: print OpenVG information to the result
        file.

        Expected output: green surface.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.name    = "OPENVG info"
        
        def build( self, target, modules ):       
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                
                OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                # Clear the screen
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.Info()
                OpenVG.CheckError( '' )
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class EglInfo( vt.VisualTestCase ):

        ''' The purpose of the test: print egl information to the result
        file. Requires EGL.

        Expected output: green surface.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.name    = "OPENVG egl info"
        
        def build( self, target, modules ):
                Egl    = modules[ 'Egl' ]                
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]               

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                displayIndex = state[ 0 ]

                OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                # Clear the screen
                OpenVG.Clear( 0, 0, 0, 0 )

                Egl.Info( displayIndex )
                
                OpenVG.CheckError( '' )
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

