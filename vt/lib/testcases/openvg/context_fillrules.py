#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
def rectangle_ccw( OpenVG, pathDataIndex, x, y, w, h ):
    OpenVG.MoveTo( pathDataIndex, [ x, y ], 'OFF' )
    OpenVG.Lines( pathDataIndex, [ w, 0 ], 'ON' )
    OpenVG.Lines( pathDataIndex, [ 0, h ], 'ON' )
    OpenVG.Lines( pathDataIndex, [ -w, 0 ], 'ON' )
    OpenVG.Lines( pathDataIndex, [ 0, -h ], 'ON' )

def rectangle_cw( OpenVG, pathDataIndex, x, y, w, h ):
    OpenVG.MoveTo( pathDataIndex, [ x, y ], 'OFF' )
    OpenVG.Lines( pathDataIndex, [ 0, h ], 'ON' )
    OpenVG.Lines( pathDataIndex, [ w, 0 ], 'ON' )
    OpenVG.Lines( pathDataIndex, [ 0, -h ], 'ON' )
    OpenVG.Lines( pathDataIndex, [ -w, 0 ], 'ON' )

def triangle( OpenVG, pathDataIndex, x1, y1, x2, y2, x3, y3 ):
    OpenVG.MoveTo( pathDataIndex, [ x1, y1 ], 'OFF' )
    OpenVG.Lines( pathDataIndex, [ x2, y2 ], 'OFF' )
    OpenVG.Lines( pathDataIndex, [ x3, y3 ], 'OFF' )
    OpenVG.Lines( pathDataIndex, [ x1, y1 ], 'OFF' )


######################################################################
class FillRuleCCW( vt.VisualTestCase ):
    
    '''
    Test for fillrules. Test draws several paths above each other (outer
    rectangle CCW, middle rectangle CCW, inner rectangle CCW, triangle CW).
    '''

    testValues = { 'fillrule' : [ 'VG_EVEN_ODD',
                                  'VG_NON_ZERO' ]
                   } 
    
    def __init__( self, fillrule ):
        vt.VisualTestCase.__init__( self )
        self.fillrule = fillrule
        self.repeats  = 1
        self.name = 'OPENVG fillrule with ccw paths %s' % ( fillrule, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------        
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
        OpenVG.FillRule( self.fillrule )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paint1Index )
        OpenVG.ColorPaint( paint1Index, [0.75,0.50,0.25,0.5] )

        paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint2Index )
        OpenVG.ColorPaint( paint2Index, [0.25,0.50,0.75,0.5] )

        rectangle_ccw( OpenVG, pathDataIndex, 25, 25, 150, 150 )
        rectangle_ccw( OpenVG, pathDataIndex, 50, 50, 100, 100 )
        rectangle_ccw( OpenVG, pathDataIndex, 75, 75, 50, 50 )
        triangle( OpenVG, pathDataIndex, 100, 0, 65, 190, 135, 190 )
        
        OpenVG.AppendPathData( pathIndex, pathDataIndex ) 

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.SetPaint( paint1Index, [ 'VG_STROKE_PATH' ] ) 
        OpenVG.SetPaint( paint2Index, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class FillRuleCW( vt.VisualTestCase ):
    
    '''
    Test for fillrules. Test draws several paths above each other (outer
    rectangle CCW, middle rectangle CW, inner rectangle CCW, triangle CW).
    '''
    
    testValues = { 'fillrule' : [ 'VG_EVEN_ODD',
                                  'VG_NON_ZERO' ]
                   } 

    def __init__( self, fillrule ):
        vt.VisualTestCase.__init__( self )
        self.fillrule = fillrule
        self.repeats  = 1
        self.name = 'OPENVG fillrule with ccw and cw paths %s' % ( fillrule, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
        OpenVG.FillRule( self.fillrule )

        pathIndex = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( pathIndex, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathDataIndex = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathDataIndex, 'VG_PATH_DATATYPE_S_32', 1, 1 )

        paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paint1Index )
        OpenVG.ColorPaint( paint1Index, [ 0.75, 0.50, 0.25, 0.5 ] )

        paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )        
        OpenVG.CreatePaint( paint2Index )
        OpenVG.ColorPaint( paint2Index, [ 0.25, 0.50, 0.75, 0.5 ] )

        rectangle_ccw( OpenVG, pathDataIndex, 25, 25, 150, 150 )
        rectangle_cw ( OpenVG, pathDataIndex, 50, 50, 100, 100 )
        rectangle_ccw( OpenVG, pathDataIndex, 75, 75, 50, 50 )
        triangle( OpenVG, pathDataIndex, 100, 0, 65, 190, 135, 190 )
        OpenVG.AppendPathData( pathIndex, pathDataIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        OpenVG.Clear(0,0,0,0)
        OpenVG.SetPaint( paint1Index, [ 'VG_STROKE_PATH' ] ) 
        OpenVG.SetPaint( paint2Index, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( pathIndex, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
