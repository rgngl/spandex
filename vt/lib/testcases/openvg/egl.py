#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from lib.egl    import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

IMAGES_FORMAT = { 'SCT_COLOR_FORMAT_RGB565'       : 'IMAGES_RGB565',
                  'SCT_COLOR_FORMAT_RGBX8888'     : 'IMAGES_BGRX8' ,
                  'SCT_COLOR_FORMAT_RGBA8888'     : 'IMAGES_BGRA8',
                  'SCT_COLOR_FORMAT_RGBA8888_PRE' : 'IMAGES_BGRA8' }

SURFACE_PARAMETERS = { 'VG_sRGB_565'        : [ 16, 5, 6, 5, 0 ],
                       'VG_sRGBA_5551'      : [ 16, 5, 5, 5, 1 ],
                       'VG_sRGBA_4444'      : [ 16, 4, 4, 4, 4 ],
                       'VG_sRGBX_8888'      : [ 24, 8, 8, 8, 0 ],
                       'VG_sRGBA_8888'      : [ 32, 8, 8, 8, 8 ],
                       'VG_sRGBA_8888_PRE'  : [ 32, 8, 8, 8, 8 ],
                       'VG_lRGBX_8888'      : [ 24, 8, 8, 8, 0 ],
                       'VG_lRGBA_8888'      : [ 32, 8, 8, 8, 8 ],
                       'VG_lRGBA_8888_PRE'  : [ 32, 8, 8, 8, 8 ],
                       'VG_sXRGB_8888'      : [ 24, 8, 8, 8, 0 ],
                       'VG_sARGB_8888'      : [ 32, 8, 8, 8, 8 ],
                       'VG_sARGB_8888_PRE'  : [ 32, 8, 8, 8, 8 ],
                       'VG_lARGB_8888'      : [ 32, 8, 8, 8, 8 ],
                       'VG_sBGRA_8888'      : [ 32, 8, 8, 8, 8 ],
                       'VG_sBGR_565'        : [ 16, 5, 6, 5, 0 ],
                       'VG_lBGRA_8888'      : [ 32, 8, 8, 8, 8 ],
                       'VG_sABGR_8888'      : [ 32, 8, 8, 8, 8 ],
                       'VG_lABGR_8888'      : [ 32, 8, 8, 8, 8 ],
                       }

SURFACE_PARAMETERS2 = { 'SCT_COLOR_FORMAT_RGB565'       : [ 16, 5, 6, 5, 0 ],
                        'SCT_COLOR_FORMAT_RGB888'       : [ 24, 8, 8, 8, 0 ],
                        'SCT_COLOR_FORMAT_RGBX8888'     : [ 24, 8, 8, 8, 0 ],
                        'SCT_COLOR_FORMAT_RGBA8888'     : [ 32, 8, 8, 8, 8 ],
                        'SCT_COLOR_FORMAT_RGBA8888_PRE' : [ 32, 8, 8, 8, 8 ],
                        }

######################################################################            
class EglImageAsImageExt( vt.VisualTestCase ):
    
    ''' Test creating an OpenVG image from a shared pixmap EGL image. Draws a
    flower image on a green surface. Image size and format varies. Requires EGL
    and EGL_KHR_image, EGL_KHR_image_base, EGL_KHR_image_pixmap,
    EGL_KHR_vg_parent_image, and VG_KHR_EGL_image extensions. Requires
    EGL_NOK_pixmap_type_rsgimage extension on Symbian.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( WIDTH, HEIGHT, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( WIDTH, HEIGHT, 'SCT_COLOR_FORMAT_RGBX8888' ),
                         ( WIDTH, HEIGHT, 'SCT_COLOR_FORMAT_RGBA8888' ),
                         ( WIDTH, HEIGHT, 'SCT_COLOR_FORMAT_RGBA8888_PRE' ),
                         ( 127, 127, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 127, 127, 'SCT_COLOR_FORMAT_RGBX8888' ),
                         ( 127, 127, 'SCT_COLOR_FORMAT_RGBA8888' ),
                         ( 127, 127, 'SCT_COLOR_FORMAT_RGBA8888_PRE' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.repeats   = 1
        self.name = 'OPENVG egl image as image %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Images = modules[ 'Images' ]        
        Vgtest = modules[ 'Vgtest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]

        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_vg_parent_image' )
        
        OpenVG.CheckExtension( 'VG_KHR_EGL_image' )
        
        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       

        # Create shared pixmap and egl image.
        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex,
                           'IMAGES_FLOWER',
                           IMAGES_FORMAT[ self.format ],
                           self.width, self.height )

        eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
        Images.CopyToEgl( imagesDataIndex, eglDataIndex )

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENVG_IMAGE' ],
                                eglDataIndex )

        imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]

        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            -1,
                            'EGL_NATIVE_PIXMAP_KHR',
                            eglPixmapIndex,
                            imageAttributes )

        vgImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateEglImageTarget( vgImageIndex, eglImageIndex )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( vgImageIndex )
        
        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################            
class EglImageAsImageWithChildImageExt( vt.VisualTestCase ):
    
    ''' Test creating an OpenVG image and child images from a shared pixmap EGL
    image. Draws a flower image on a green surface. Lower-left corner of the
    flower image is covered with red-blue rectangle. Image size and format
    varies. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_image_pixmap, EGL_KHR_vg_parent_image, and VG_KHR_EGL_image
    extensions. Requires EGL_NOK_pixmap_type_rsgimage extension on Symbian.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( WIDTH, HEIGHT, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 127, 127, 'SCT_COLOR_FORMAT_RGBA8888_PRE' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.repeats   = 1
        self.name = 'OPENVG egl image as image with child image %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Images = modules[ 'Images' ]        
        Vgtest = modules[ 'Vgtest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]

        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_vg_parent_image' )
        
        OpenVG.CheckExtension( 'VG_KHR_EGL_image' )
        
        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       

        # Create shared pixmap and egl image.
        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex,
                           'IMAGES_FLOWER',
                           IMAGES_FORMAT[ self.format ],
                           self.width, self.height )

        eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
        Images.CopyToEgl( imagesDataIndex, eglDataIndex )

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENVG_IMAGE' ],
                                eglDataIndex )

        imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]

        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            -1,
                            'EGL_NATIVE_PIXMAP_KHR',
                            eglPixmapIndex,
                            imageAttributes )

        vgImage1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateEglImageTarget( vgImage1Index, eglImageIndex )

        vgImage2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateEglImageTarget( vgImage2Index, eglImageIndex )
        
        vgChildImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.ChildImage( vgChildImageIndex, vgImage2Index, 0, 0, self.width / 4, self.height / 4 )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )        
        OpenVG.ClearImage( vgChildImageIndex, 0, 0, self.width / 8, self.height / 4 )

        OpenVG.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )        
        OpenVG.ClearImage( vgChildImageIndex, self.width / 8, 0, self.width / 8, self.height / 4 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( vgImage1Index )
        
        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################            
class DestroyEglImageUsedAsImageExt( vt.VisualTestCase ):
    
    ''' Test destroying an EGL image after it has been used to create OpenVG
    image. Draws a flower image on a green surface. Image size and format
    varies. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_image_pixmap, EGL_KHR_vg_parent_image, and VG_KHR_EGL_image
    extensions. Requires EGL_NOK_pixmap_type_rsgimage extension on Symbian.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( WIDTH, HEIGHT, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 127, 127, 'SCT_COLOR_FORMAT_RGBA8888_PRE' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.repeats   = 1
        self.name = 'OPENVG destroy egl image used as image %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Images = modules[ 'Images' ]        
        Vgtest = modules[ 'Vgtest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]

        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_vg_parent_image' )
        
        OpenVG.CheckExtension( 'VG_KHR_EGL_image' )
        
        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       

        # Create shared pixmap and egl image.
        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex,
                           'IMAGES_FLOWER',
                           IMAGES_FORMAT[ self.format ],
                           self.width, self.height )

        eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
        Images.CopyToEgl( imagesDataIndex, eglDataIndex )

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENVG_IMAGE' ],
                                eglDataIndex )

        imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]

        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            -1,
                            'EGL_NATIVE_PIXMAP_KHR',
                            eglPixmapIndex,
                            imageAttributes )

        # Create vgimage from egl image
        vgImageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateEglImageTarget( vgImageIndex, eglImageIndex )

        # Destroy egl image
        Egl.DestroyImage( displayIndex, eglImageIndex )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( vgImageIndex )
        
        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
        
######################################################################            
class EglImageFromImageExt( vt.VisualTestCase ):
    
    ''' Test creating an EGL image from an existing vg image. Draws a tiger with
    upper-right quarter cleared to [0,1,0,0.3]. Image size and format
    varies. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_vg_parent_image, and VG_KHR_EGL_image extensions.
    '''

    testValues = { ( 'width', 'height', 'format', 'quality' ) :
                       [ ( 63, 63, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( 63, 64, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( 64, 64, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( 64, 65, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( 65, 65, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_5551', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_4444', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sL_8', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lL_8', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_A_8', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_BW_1', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_A_1', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_A_4', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sXRGB_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sARGB_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sARGB_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sARGB_1555', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sARGB_4444', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lXRGB_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lARGB_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lARGB_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sBGRX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sBGRA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sBGRA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sBGR_565', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sBGRA_5551', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sBGRA_4444', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lBGRX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lBGRA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lBGRA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sXBGR_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sABGR_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sABGR_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sABGR_1555', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sABGR_4444', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lXBGR_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lABGR_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lABGR_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ) ]
                   }

    def __init__( self, width, height, format, quality ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.quality   = quality
        self.repeats   = 1
        self.name = 'OPENVG egl image from image %dx%d %s %s' % ( self.width, self.height, self.format, self.quality, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]

        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_vg_parent_image' )
        
        OpenVG.CheckExtension( 'VG_KHR_EGL_image' )

        # Draw tiger and get the pixels to image.
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.Clear( 0, 0, 0, 0 )

        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.format, WIDTH, HEIGHT, [ self.quality ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.format, WIDTH, HEIGHT )
        
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH, HEIGHT )

        # Create egl image and another image from the egl image.
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            contextIndex,
                            'EGL_VG_PARENT_IMAGE_KHR',
                            image1Index,
                            [] )
        
        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateEglImageTarget( image2Index,
                                     eglImageIndex )

        # Change image pixel data through the original image.
        OpenVG.ImageSubData( imageDataIndex, image1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ClearColor( [ 0, 1, 0, 0.3 ] )
        OpenVG.ClearImage( image1Index, WIDTH / 2, HEIGHT / 2, WIDTH / 2, HEIGHT / 2 )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( image2Index )
        
        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       
        
######################################################################            
class OrphaningEglImageFromImageExt( vt.VisualTestCase ):
    
    ''' Test orphaning an EGL image from an existing vg image. Draws a tiger with
    upper-right quarter cleared to [0,1,0,0.3]. Image size and format
    varies. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_vg_parent_image, and VG_KHR_EGL_image extensions.
    '''

    testValues = { ( 'width', 'height', 'format', 'quality' ) :
                       [ ( WIDTH, HEIGHT, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ), ]
                   }

    def __init__( self, width, height, format, quality ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.quality   = quality
        self.repeats   = 1
        self.name = 'OPENVG orphaning egl image from image %dx%d %s %s' % ( self.width, self.height, self.format, self.quality, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]

        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_vg_parent_image' )
        
        OpenVG.CheckExtension( 'VG_KHR_EGL_image' )

        # Draw tiger and get the pixels to image.
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.Clear( 0, 0, 0, 0 )

        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.format, WIDTH, HEIGHT, [ self.quality ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.format, WIDTH, HEIGHT )
        
        OpenVG.ReadPixels( imageDataIndex, 0, 0, WIDTH, HEIGHT )

        # Create egl image and another image from the egl image.
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            contextIndex,
                            'EGL_VG_PARENT_IMAGE_KHR',
                            image1Index,
                            [] )
        
        # Change image pixel data through the original image.
        OpenVG.ImageSubData( imageDataIndex, image1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ClearColor( [ 0, 1, 0, 0.3 ] )
        OpenVG.ClearImage( image1Index, WIDTH / 2, HEIGHT / 2, WIDTH / 2, HEIGHT / 2 )

        # Delete original
        OpenVG.DestroyImage( image1Index )

        # Create a new openvg image from the orphaned egl image
        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateEglImageTarget( image2Index,
                                     eglImageIndex )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( image2Index )
        
        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       

        
######################################################################            
class PbufferFromImage( vt.VisualTestCase ):
    
    ''' Test creating an EGL pbuffer from a vg image. The test fist draws a
    tiger and clears the upper-right corner to [0,1,0,0.3]. It then reads the
    pixels into an image and creates an EGL pbuffer surface from the
    image. After that, the test clears lower-left corner of the surface to
    [0,0,1,0.3] and saves the resulting image. The end result should show a
    tiger overlapped by blue (lower-left) and green (upper-right) rectangles.
    '''

    testValues = { ( 'width', 'height', 'format', 'quality' ) :
                       [ ( WIDTH, HEIGHT, 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_FASTER' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_8888', 'VG_IMAGE_QUALITY_BETTER' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sRGB_565', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_5551', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sRGBA_4444', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lRGBA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sXRGB_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sARGB_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sARGB_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lARGB_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sBGRA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sBGR_565', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lBGRA_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_sABGR_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ),
                         ( WIDTH, HEIGHT, 'VG_lABGR_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED' ), ]
                   }

    def __init__( self, width, height, format, quality ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.quality   = quality
        self.repeats   = 1
        self.name = 'OPENVG egl pbuffer from image %dx%d %s %s' % ( self.width, self.height, self.format, self.quality, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              self.width, self.height,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        configIndex  = state[ 1 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]

        # Draw tiger and get the pixels to image.
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.Clear( 0, 0, 0, 0 )

        OpenVG.DrawTiger( tigerIndex, 1, self.width, self.height )
        OpenVG.ClearColor( [ 0, 1, 0, 0.3 ] )
        OpenVG.Clear( self.width / 2, self.height / 2, self.width / 2, self.height / 2 )

        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( imageIndex, self.format, self.width, self.height, [ self.quality ] )

        imageDataIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageDataIndex, self.format, self.width, self.height )
        
        OpenVG.ReadPixels( imageDataIndex, 0, 0, self.width, self.height )              
        OpenVG.ImageSubData( imageDataIndex, imageIndex, 0, 0, self.width, self.height )

        OpenVG.CheckError( '' ) 

        # Create pbuffer from the image.
        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENVG_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferFromClientBufferExt( displayIndex,
                                              pbufSurfaceIndex,
                                              pbufConfigIndex,
                                              'EGL_OPENVG_IMAGE',
                                              imageIndex,
                                              [] )
                                           
        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, -1 )
        Egl.MakeCurrentExt( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        # Clear a small rect and read the pixels.
        OpenVG.ClearColor( [ 0, 0, 1, 0.3 ] )        
        OpenVG.Clear( 0, 0, self.width / 4, self.height / 4 )       
        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, self.width, self.height, target.outputPath( apis ) )

        # Ensure more proper cleanup
        Egl.MakeCurrentExt( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

######################################################################            
class PixmapSurfaceAsImageExt( vt.VisualTestCase ):
    
    ''' Test creating a pixmap surface from a shared pixmap. The test first
    creates a shared pixmap. Secondly, the test creates an image from the shared
    pixmap through EGL image. Thirdly, creates a pixmap surface from the shared
    pixmap, renders a tiger, and then uses the image for rendering. At the end,
    the surface should show a tiger, with lower-left corner covered with green
    rectangle. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_image_pixmap, EGL_KHR_vg_parent_image, and
    VG_KHR_EGL_image. Requires EGL_NOK_pixmap_type_rsgimage extension on
    Symbian.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 255, 255, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 255, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 255, 'SCT_COLOR_FORMAT_RGB565' ),                         
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 257, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 257, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 257, 257, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBX8888' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBA8888' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENVG pixmap surface as image %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Images = modules[ 'Images' ]        
        Vgtest = modules[ 'Vgtest' ]
        
        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]
        
        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_vg_parent_image' )
        
        OpenVG.CheckExtension( 'VG_KHR_EGL_image' )
        
        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       

        # Create shared pixmap and egl image.
        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENVG_SURFACE', 'SCT_USAGE_OPENVG_IMAGE' ],
                                -1 )
       
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, -1, 'EGL_NATIVE_PIXMAP_KHR', eglPixmapIndex, [] )

        # Create image from egl image.
        imageIndex = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateEglImageTarget( imageIndex, eglImageIndex )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENVG_BIT' ]
                       ]
        
        # Create pixmap surface from the shared pixmap.
        pixmapConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex, pixmapConfigIndex, eglPixmapIndex, attributes )

        pixmapSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurfaceExt( displayIndex, pixmapSurfaceIndex, pixmapConfigIndex, eglPixmapIndex, [] )

        pixmapContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pixmapContextIndex, pixmapConfigIndex, -1, -1 )

        Egl.MakeCurrent( displayIndex, pixmapSurfaceIndex, pixmapSurfaceIndex, pixmapContextIndex )

        # Draw graphics to pixmap surface.
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, self.width, self.height )       

        # Restore the original surface and draw the texture from the egl
        # image/pixmap surface.
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( imageIndex )
        OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )        
        OpenVG.Clear( 0, 0, WIDTH / 2, HEIGHT / 2 )
        
        OpenVG.CheckError( '' )                        
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################
class WindowSurface( vt.VisualTestCase ):
    
    ''' Test for window surfaces. Test draws a tiger. Requires EGL.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGB888',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888',
                                'SCT_COLOR_FORMAT_RGBA8888_PRE' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENVG window surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        windowIndex = 0
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, WIDTH, HEIGHT, 'SCT_COLOR_FORMAT_DEFAULT' )

        Egl.BindApi( 'EGL_OPENVG_API' )
        
        # Create pbuffer from the image.
        p = SURFACE_PARAMETERS2[ self.format ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT'] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )       
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, -1 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class PbufferSurface( vt.VisualTestCase ):
    
    ''' Test for pbuffer surfaces. Test draws a tiger. Requires EGL.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGB888',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888',
                                'SCT_COLOR_FORMAT_RGBA8888_PRE' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENVG pbuffer surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )
        
        p = SURFACE_PARAMETERS2[ self.format ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT' ],
                       [ 'EGL_OPENVG_BIT'] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  surfaceIndex,
                                  configIndex,
                                  WIDTH,
                                  HEIGHT,
                                  'EGL_NO_TEXTURE', 'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',
                                  'EGL_COLORSPACE_sRGB',
                                  'EGL_ALPHA_FORMAT_PRE' )
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, -1 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################
class PixmapSurface( vt.VisualTestCase ):
    
    ''' Test for pixmap surfaces. Test draws a tiger. Requires EGL.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGB888',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888',
                                'SCT_COLOR_FORMAT_RGBA8888_PRE' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENVG pixmap surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )

        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex, WIDTH, HEIGHT, self.format, -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENVG_BIT' ]
                       ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex,  configIndex,  pixmapIndex, attributes )
        
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 pixmapIndex,
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, -1 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################
class SharedPixmapSurface( vt.VisualTestCase ):
    
    ''' Test for shared pixmap surfaces. Test draws a tiger. Requires
    EGL. Requires EGL_NOK_pixmap_type_rsgimage extension in Symbian.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888',
                                'SCT_COLOR_FORMAT_RGBA8888_PRE' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENVG shared pixmap surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        Egl.BindApi( 'EGL_OPENVG_API' )

        # Check extensions.
        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )

        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( pixmapIndex,
                                WIDTH, HEIGHT,
                                self.format,
                                [ 'SCT_USAGE_OPENVG_SURFACE' ],
                                -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENVG_BIT' ]
                       ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex, configIndex, pixmapIndex, attributes )
        
        attributes = []
        if self.format in [ 'SCT_COLOR_FORMAT_RGBA8888_PRE' ]:
            attributes.append( EGL_DEFINES[ 'EGL_VG_ALPHA_FORMAT' ] )
            attributes.append( EGL_DEFINES[ 'EGL_ALPHA_FORMAT_PRE' ] )
        
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurfaceExt( displayIndex, surfaceIndex, configIndex, pixmapIndex, attributes )
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, -1 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################
class SwapBuffersPreserved( vt.VisualTestCase ):
    
    ''' Test egl swap buffers preserved. Test draws a tiger with a red rectangle
    on the lower-left and blue rectangle on the upper-right corner. Requires
    EGL.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGB888',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENVG swap buffers preserved %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl    = modules[ 'Egl' ]
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        windowIndex = 0
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, WIDTH, HEIGHT, 'SCT_COLOR_FORMAT_DEFAULT' )

        Egl.BindApi( 'EGL_OPENVG_API' )
        
        # Create pbuffer from the image.
        p = SURFACE_PARAMETERS2[ self.format ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_VG_ALPHA_FORMAT_PRE_BIT', 'EGL_SWAP_BEHAVIOR_PRESERVED_BIT' ],
                       [ 'EGL_OPENVG_BIT'] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )       
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_COLORSPACE_sRGB',
                                 'EGL_ALPHA_FORMAT_PRE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, -1 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        Egl.SurfaceAttrib( displayIndex,
                           surfaceIndex,
                           EGL_DEFINES[ 'EGL_SWAP_BEHAVIOR' ],
                           EGL_DEFINES[ 'EGL_BUFFER_PRESERVED' ] ) 
        
        OpenVG.ClearColor( [ 0, 0, 0, 1.0 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        OpenVG.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
        OpenVG.Clear( 0, 0, 20, 20 )

        Egl.SwapBuffers( displayIndex, surfaceIndex )
        
        OpenVG.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )
        OpenVG.Clear( WIDTH - 20, HEIGHT - 20, 20, 20 )

        Egl.SwapBuffers( displayIndex, surfaceIndex )       
                
        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################
class Extensions( vt.VisualTestCase ):

        ''' The purpose of the test: test OpenVG EGL extensions.

        Expected output: surface cleared to green if the particular OpenVG
        EGL extension is supported. Viewport size 320*240.
        '''
        
        testValues = { ( 'extension', 'functions' ) : [ ( 'EGL_KHR_lock_surface', [ 'eglLockSurfaceKHR',
                                                                                    'eglUnlockSurfaceKHR' ] ),
                                                        ( 'EGL_KHR_image', [] ),
                                                        ( 'EGL_KHR_vg_parent_image', [] ),
                                                        ( 'EGL_KHR_image_base', [ 'eglCreateImageKHR',
                                                                                  'eglDestroyImageKHR' ] ),
                                                        ( 'EGL_KHR_image_pixmap', [] ),
                                                        ( 'EGL_KHR_lock_surface2', [ 'eglLockSurfaceKHR',
                                                                                     'eglUnlockSurfaceKHR' ] ),
                                                        ( 'EGL_NOK_resource_profiling', [ 'eglQueryProfilingDataNOK' ] ),
                                                        ( 'EGL_NOK_resource_profiling2', [ 'eglQueryProfilingDataNOK' ] ),
                                                        ( 'EGL_NOK_surface_scaling', [ 'eglSetSurfaceScalingNOK',
                                                                                       'eglQuerySurfaceScalingCapabilityNOK' ] ),
                                                        ( 'EGL_NOK_pixmap_type_rsgimage', [] ),
                                                        ( 'EGL_NOK_window_type_eglwindowbase', [] ),
                                                        ( 'EGL_SYMBIAN_COMPOSITION', [] ),
                                       ]
                       }
                                                                             
        def __init__( self, extension, functions ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.extension = extension
                self.functions = functions
                self.name    = "OPENVG egl extensions, extension=%s" % ( self.extension, )
        
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Egl    = modules[ 'Egl' ]
                Vgtest = modules[ 'Vgtest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()               

                Egl.CheckExtension( state[ 0 ], self.extension )
                
                for f in self.functions:
                        Egl.CheckFunction( f )

                OpenVG.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )        
                OpenVG.Clear( 0, 0, 0, 0 )
        
                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

