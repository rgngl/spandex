#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class ColorMatrix( vt.VisualTestCase ):
    
    ''' Test for colormatrix image filter. Test applies colormatrix
    [0.25,0,0,0][0,0.5,0,0][0,0,0.75,0][0,0,0,0.5][0,0,0,0] to all color
    channels when drawing the tiger with black and white rectangles. Image
    format varies.
    '''

    testValues = { 'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.imageformat = imageformat
        self.repeats     = 1        
        self.name = 'OPENVG image filter colormatrix %s' % ( imageformat, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )       
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )
        
        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData1Index, self.imageformat, WIDTH, HEIGHT )

        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )        
        OpenVG.CreateImage( image2Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        OpenVG.ReadPixels( imageData1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageData1Index, image1Index, 0, 0, WIDTH, HEIGHT )

        OpenVG.FilterChannelMask( [ 'VG_RED', 'VG_GREEN', 'VG_BLUE', 'VG_ALPHA' ] )
        OpenVG.ColorMatrix( image2Index, image1Index,
                            [ 0.25,   0,    0,    0,
                                 0, 0.5,    0,    0,
                                 0,   0, 0.75,    0,
                                 0,   0,    0, 0.50,
                                 0,   0,    0,    0 ] )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( image2Index )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class ConvolutionConvolve( vt.VisualTestCase ):
    
    ''' Test for convolve convolution image filter. Test applies convolve filter
    for tiger with black and white rectangles. Convolution kernel is
    [1,1,1,1,1,1,1,1,1], kernel width and height [3,3], convolution tile fill
    color is [1,0,0,1], shiftx and shift y are [50,50], bias is [0]. Tiling mode
    and image format varies.
    '''

    testValues = { 'tilingmode'  : [ 'VG_TILE_FILL',
                                     'VG_TILE_PAD',
                                     'VG_TILE_REPEAT',
                                     'VG_TILE_REFLECT' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, tilingmode, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.tilingmode  = tilingmode
        self.imageformat = imageformat
        self.repeats     = 1
        self.name = 'OPENVG image filter convolution convolve %s %s' % ( imageformat, tilingmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )        
        OpenVG.CreateTiger( tigerIndex )        
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )
        
        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData1Index, self.imageformat, WIDTH, HEIGHT )

        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )        
        OpenVG.CreateImage( image2Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        OpenVG.ReadPixels( imageData1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageData1Index, image1Index, 0, 0, WIDTH, HEIGHT )

        kernel = [ 1, 1, 1,
                   1, 1, 1,
                   1, 1, 1 ]
        OpenVG.FilterChannelMask( [ 'VG_RED', 'VG_GREEN', 'VG_BLUE', 'VG_ALPHA' ] )
        OpenVG.TileFillColor( [ 1, 0, 0, 1 ] )
        OpenVG.Convolve( image2Index, image1Index, WIDTH / 4, HEIGHT / 4, kernel, 3, 3, 1.0, 0.0, self.tilingmode )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( image2Index )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class ConvolutionGaussianBlur( vt.VisualTestCase ):
    
    ''' Test for gaussian blur convolution image filter. Test applies gaussian
    blur filter [deviationX=2.0,deviationY=0.5] for tiger with black and white
    rectangles. Tiling mode and image format varies.
    '''

    testValues = { 'tilingmode'  : [ 'VG_TILE_FILL',
                                     'VG_TILE_PAD',
                                     'VG_TILE_REPEAT',
                                     'VG_TILE_REFLECT' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, tilingmode, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.tilingmode  = tilingmode
        self.imageformat = imageformat
        self.repeats     = 1       
        self.name = 'OPENVG image filter convolution gaussian blur %s %s' % ( imageformat, tilingmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )
        
        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData1Index, self.imageformat, WIDTH, HEIGHT )

        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )        
        OpenVG.CreateImage( image2Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        OpenVG.ReadPixels( imageData1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageData1Index, image1Index, 0, 0, WIDTH, HEIGHT )

        OpenVG.FilterChannelMask( [ 'VG_RED', 'VG_GREEN', 'VG_BLUE', 'VG_ALPHA' ] )
        OpenVG.GaussianBlur( image2Index, image1Index, 2.0, 0.5, self.tilingmode )

        # ----------------------------------------------------------------------        
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( image2Index )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class ConvolutionSeparableConvolve( vt.VisualTestCase ):
    
    ''' Test for separableconvolve convolution image filter. Test applies
    separableconvolve filter for tiger with black and white
    rectangles. Convolution kernel is [1,1,1,1,1,1,1,1,1], kernelX width 1,
    kernelY width 3, kernelX and kernelY are [1,2,1][1,2,1], convolution tile
    fill color is [1,0,0,1], shiftx and shift y are [50,50], bias is 0. Tiling
    mode and image format varies.
    '''

    testValues = { 'tilingmode'  : [ 'VG_TILE_FILL',
                                     'VG_TILE_PAD',
                                     'VG_TILE_REPEAT',
                                     'VG_TILE_REFLECT' ],
                   'imageformat' : [ 'VG_sRGBX_8888',
                                     'VG_sRGBA_8888',
                                     'VG_sRGBA_8888_PRE',
                                     'VG_sRGB_565',
                                     'VG_sRGBA_5551',
                                     'VG_sRGBA_4444',
                                     'VG_sL_8',
                                     'VG_lRGBX_8888',
                                     'VG_lRGBA_8888',
                                     'VG_lRGBA_8888_PRE',
                                     'VG_lL_8',
                                     'VG_A_8',
                                     'VG_BW_1' ]
                   }

    def __init__( self, tilingmode, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.tilingmode   = tilingmode
        self.imageformat = imageformat
        self.repeats     = 1        
        self.name = 'OPENVG image filter convolution separable convolve %s %s' % ( imageformat, tilingmode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )        
        
        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData1Index, self.imageformat, WIDTH, HEIGHT )

        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )        
        OpenVG.CreateImage( image2Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )
        
        OpenVG.ReadPixels( imageData1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageData1Index, image1Index, 0, 0, WIDTH, HEIGHT )

        kernelX = [ 1, 2, 1 ]
        kernelY = [ 1, 2, 1 ]
        OpenVG.FilterChannelMask( [ 'VG_RED', 'VG_GREEN', 'VG_BLUE', 'VG_ALPHA' ] )
        OpenVG.TileFillColor( [ 1, 0, 0, 1 ] )
        OpenVG.SeparableConvolve( image2Index, image1Index, WIDTH / 4, HEIGHT / 4, kernelX, kernelY, 1, 3, 1.0, 0.0, self.tilingmode )

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( image2Index )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class GroupOpacity( vt.VisualTestCase ):
    
    '''
    A test for colormatrix image filter (group opacity). Test draws path with
    alpha[1] and reads it back to image and clears the surface. Upper path is
    drawn with alpha [1] and middle path with alpha [0.5] for both stroke and
    fill. Lower path is drawn via image while applying colormatrix [0.5] for
    alphas (all color and alpha filter channels are [ON]). Background opacities
    in color stripes are [0.5]. Image format is [VG_sRGBA_8888].
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats = 1        
        self.name = 'OPENVG image filter group opacity'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.Stroke( 10, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )

        path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
        OpenVG.CreatePath( path1Index, 'VG_PATH_DATATYPE_S_32', 1, 0, 1, 0, ['VG_PATH_CAPABILITY_APPEND_TO'] )

        pathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
        OpenVG.CreatePathData( pathData1Index, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        OpenVG.MoveTo( pathData1Index, [ 0, 0 ], 'OFF' )
        OpenVG.CubicBeziers( pathData1Index, [ 100, 50, 100, -50, 0, 0 ], 'ON' )
        OpenVG.CubicBeziers( pathData1Index, [ -100, -50, -100, 50, 0, 0 ], 'ON' )
        OpenVG.AppendPathData( path1Index, pathData1Index )

        path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )        
        OpenVG.CreatePath( path2Index, 'VG_PATH_DATATYPE_S_32', 1, 1, 1, 0, [ 'VG_PATH_CAPABILITY_APPEND_TO' ] )

        pathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )        
        OpenVG.CreatePathData( pathData2Index, 'VG_PATH_DATATYPE_S_32', 1, 1 )
        OpenVG.Lines( pathData2Index, [ 0, 200 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ 60, 0 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ 0, -200 ], 'ON' )
        OpenVG.Lines( pathData2Index, [ -60, 0 ], 'ON' )
        OpenVG.AppendPathData( path2Index, pathData2Index )

        paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint1Index )
        OpenVG.ColorPaint( paint1Index, [ 1.0, 0.0, 0.0, 0.5 ] )

        paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint2Index )
        OpenVG.ColorPaint( paint2Index, [ 0.0, 1.0, 0.0, 0.5 ] )

        paint3Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint3Index )
        OpenVG.ColorPaint( paint3Index, [ 0.0, 0.0, 1.0, 0.5 ] )

        paint4Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint4Index )
        OpenVG.ColorPaint( paint4Index, [ 0.75, 0.50, 0.25, 1.0 ] )

        paint5Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint5Index )
        OpenVG.ColorPaint( paint5Index, [ 0.25, 0.50, 0.75, 1.0 ] )

        paint6Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint6Index )
        OpenVG.ColorPaint( paint6Index, [ 0.75, 0.50, 0.25, 0.5 ] )

        paint7Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paint7Index )
        OpenVG.ColorPaint( paint7Index, [ 0.25, 0.50, 0.75, 0.5 ] )

        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, 'VG_sRGBA_8888', 200, 100, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData1Index, 'VG_sRGBA_8888', 200, 100 )

        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image2Index, 'VG_sRGBA_8888', 200, 100, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageData2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData2Index, 'VG_sRGBA_8888', 200, 100 )

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.ClearColor( [ 0, 0, 0, 0 ] ) 
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )
        OpenVG.Translate( 100, 50 )
        OpenVG.SetPaint( paint4Index, [ 'VG_STROKE_PATH' ] )
        OpenVG.SetPaint( paint5Index, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( path1Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )
        OpenVG.ReadPixels( imageData1Index, 0, 0, 200, 100 )
        OpenVG.ImageSubData( imageData1Index, image1Index, 0, 0, 200, 100 )
        OpenVG.FilterChannelMask( [ 'VG_RED', 'VG_GREEN', 'VG_BLUE', 'VG_ALPHA' ] )
        OpenVG.ColorMatrix( image2Index, image1Index,
                            [ 1, 0, 0,    0,
                              0, 1, 0,    0,
                              0, 0, 1,    0,
                              0, 0, 0, 0.50,
                              0, 0, 0,    0 ] )
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )
        OpenVG.LoadIdentity( )
        OpenVG.SetPaint( paint1Index, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( path2Index, [ 'VG_FILL_PATH' ] )
        OpenVG.Translate( 70, 0 )
        OpenVG.SetPaint( paint2Index, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( path2Index, [ 'VG_FILL_PATH' ] )
        OpenVG.Translate( 70, 0 )
        OpenVG.SetPaint( paint3Index, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( path2Index, [ 'VG_FILL_PATH' ] )
        
        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )
        OpenVG.LoadIdentity( )
        OpenVG.Translate( 100, 150 )
        OpenVG.SetPaint( paint4Index, [ 'VG_STROKE_PATH' ] )
        OpenVG.SetPaint( paint5Index, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( path1Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )

        OpenVG.MatrixMode( 'VG_MATRIX_PATH_USER_TO_SURFACE' )
        OpenVG.Translate( 0, -50 )
        OpenVG.SetPaint( paint6Index, [ 'VG_STROKE_PATH' ] )
        OpenVG.SetPaint( paint7Index, [ 'VG_FILL_PATH' ] )
        OpenVG.DrawPath( path1Index, [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ] )
        
        OpenVG.MatrixMode( 'VG_MATRIX_IMAGE_USER_TO_SURFACE' )
        OpenVG.ImageMode( 'VG_DRAW_IMAGE_NORMAL' )
        OpenVG.Translate( 0, 0 )
        OpenVG.DrawImage( image2Index )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class Lookup( vt.VisualTestCase ):
    
    ''' A test for lookup image filter. Test clears the surface to color
    [0,0,0,1] and draws the tiger with black and white rectangles with lookup
    tables RedLUT=[255,...,0], GreenLUT=[255,...,0], BlueLUT=[255,...,0],
    AlphaLUT=[0,...,255] to (non-)linear and (non-)premultiplied output formats.
    '''

    testValues = { 'linear'        : [ 'OFF',
                                       'ON' ],
                   'premultiplied' : [ 'OFF',
                                       'ON' ],
                   'imageformat'   : [ 'VG_sRGBX_8888',
                                       'VG_sRGBA_8888',
                                       'VG_sRGBA_8888_PRE',
                                       'VG_sRGB_565',
                                       'VG_sRGBA_5551',
                                       'VG_sRGBA_4444',
                                       'VG_sL_8',
                                       'VG_lRGBX_8888',
                                       'VG_lRGBA_8888',
                                       'VG_lRGBA_8888_PRE',
                                       'VG_lL_8',
                                       'VG_A_8',
                                       'VG_BW_1' ]
                   }

    def __init__( self, linear, premultiplied, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.linear        = linear
        self.premultiplied = premultiplied
        self.imageformat   = imageformat
        self.repeats       = 1        
        self.name = 'OPENVG image filter lookup %s linear %s premultiplied %s' % ( imageformat, linear, premultiplied, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )        
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )        
        
        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData1Index, self.imageformat, WIDTH, HEIGHT )
 
        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image2Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )
        
        OpenVG.ReadPixels( imageData1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageData1Index, image1Index, 0, 0, WIDTH, HEIGHT )

        OpenVG.FilterChannelMask( [ 'VG_RED', 'VG_GREEN', 'VG_BLUE', 'VG_ALPHA' ] )

        lut1Index = indexTracker.allocIndex( 'OPENVG_LUT_INDEX' )
        OpenVG.CreateLut( lut1Index, 'SCT_OPENVG_LUT_UBYTE8' )

        lut2Index = indexTracker.allocIndex( 'OPENVG_LUT_INDEX' )        
        OpenVG.CreateLut( lut2Index, 'SCT_OPENVG_LUT_UBYTE8' )

        lut3Index = indexTracker.allocIndex( 'OPENVG_LUT_INDEX' )        
        OpenVG.CreateLut( lut3Index, 'SCT_OPENVG_LUT_UBYTE8' )

        lut4Index = indexTracker.allocIndex( 'OPENVG_LUT_INDEX' )        
        OpenVG.CreateLut( lut4Index, 'SCT_OPENVG_LUT_UBYTE8' )
        
        OpenVG.LutData( lut1Index, [ '0x%02x' % ( 255 - value ) for value in range( 256 ) ] )
        OpenVG.LutData( lut2Index, [ '0x%02x' % ( 255 - value ) for value in range( 256 ) ] )
        OpenVG.LutData( lut3Index, [ '0x%02x' % ( 255 - value ) for value in range( 256 ) ] )
        OpenVG.LutData( lut4Index, [ '0x%02x' % value for value in range( 256 ) ] )
        OpenVG.Lookup( image2Index, image1Index, 0, 1, 2, 3, self.linear, self.premultiplied )

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( image2Index )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class LookupSingle( vt.VisualTestCase ):
    
    ''' A test for lookupsingle image filter. Test clears the surface to color
    [0,0,0,1] and draws the tiger with black and white rectangles using red,
    green, and blue channels with single lookuptable (red[255->0],
    green[0->255], blue[255->0], alpha[0->255]) to (non-)linear and
    (non-)premultiplied output formats. All source channels (red, green, blue)
    are tested.
    '''

    testValues = { 'channel'       : [ 'VG_RED',
                                       'VG_GREEN',
                                       'VG_BLUE' ],
                   'linear'        : [ 'OFF',
                                       'ON' ],
                   'premultiplied' : [ 'OFF',
                                       'ON' ],
                   'imageformat'   : [ 'VG_sRGBX_8888',
                                       'VG_sRGBA_8888',
                                       'VG_sRGBA_8888_PRE',
                                       'VG_sRGB_565',
                                       'VG_sRGBA_5551',
                                       'VG_sRGBA_4444',
                                       'VG_sL_8',
                                       'VG_lRGBX_8888',
                                       'VG_lRGBA_8888',
                                       'VG_lRGBA_8888_PRE',
                                       'VG_lL_8',
                                       'VG_A_8',
                                       'VG_BW_1' ]
                   }

    def __init__( self, channel, linear, premultiplied, imageformat ):
        vt.VisualTestCase.__init__( self )
        self.channel       = channel
        self.linear        = linear
        self.premultiplied = premultiplied
        self.imageformat   = imageformat
        self.repeats       = 1        
        self.name = 'OPENVG image filter lookup single %s linear %s premulti %s channel %s' % ( imageformat, linear, premultiplied, channel, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )

        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )
        OpenVG.ClearColor( [ 0, 0, 0, 1 ] )        
        
        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        imageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData1Index, self.imageformat, WIDTH, HEIGHT )

        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image2Index, self.imageformat, WIDTH, HEIGHT, [ 'VG_IMAGE_QUALITY_BETTER' ] )

        OpenVG.ReadPixels( imageData1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageData1Index, image1Index, 0, 0, WIDTH, HEIGHT )

        lutIndex = indexTracker.allocIndex( 'OPENVG_LUT_INDEX' )
        OpenVG.CreateLut( lutIndex, 'SCT_OPENVG_LUT_UINT32' )
        OpenVG.LutData( lutIndex, [ '0x%02x%02x%02x%02x' % ( 255 - value, value, 255 - value, value ) for value in range( 256 ) ] )
        OpenVG.LookupSingle( image2Index, image1Index, self.channel, 0, self.linear, self.premultiplied )

        #----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( image2Index )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

