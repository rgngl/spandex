#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class ImageHandling1( vt.VisualTestCase ):
    
    '''
    This test is for testing following functions:
    1. ClearImage
    2. ImageSubData
    3. GetImageSubData
    4. ChildImage
    5. CopyImage

    These are the steps of the test: 1. test draws a tiger with black and white
    rectangles to the surface and copies surface data to image1. 2. test creates
    image2 and copies image1 to image2 with GetImageSubData. 3. test creates
    image3 and copies image2 to image3 with ImageCopy. 4. test creates image4
    which is a child image to image3. 5. test clears image4 area 50,50,10,10
    with ClearImage, clear color [0,0,0,0]. 6. test clears surface to [ 0.5,
    0.5, 0.5, 0.5 ] and draws image4. Fill paint color is set as [ 0.5, 1.0,
    0.25, 0.75 ]. The following values are changing with every testcount based
    on predefined allpair testing table. 1. ImageFormat 2. ImageQuality
    3. ImageMode.
    '''

    testValues = { ( 'ImageFormat', 'ImageQuality', 'ImageMode' ) :
                       [ ( 'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sRGB_565',       'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGB_565',       'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGB_565',       'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sL_8',           'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sL_8',           'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sL_8',           'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_lL_8',           'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_lL_8',           'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_lL_8',           'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_A_8',            'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_A_8',            'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_A_8',            'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_BW_1',           'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_BW_1',           'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_BW_1',           'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ) ]
                   }
    
    def __init__( self, ImageFormat, ImageQuality, ImageMode ):
        vt.VisualTestCase.__init__( self )
        self.ImageFormat  = ImageFormat
        self.ImageQuality = ImageQuality
        self.ImageMode    = ImageMode
        self.repeats      = 1
        self.name = 'OPENVG image handling1 %s %s %s' % ( ImageFormat, ImageQuality, ImageMode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
       
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )

        OpenVG.ImageQuality( self.ImageQuality )
        OpenVG.ImageMode( self.ImageMode )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )
        
        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.5 ] )
        
        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )

        imageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData1Index, self.ImageFormat, WIDTH, HEIGHT ) 
        
        OpenVG.ReadPixels( imageData1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageData1Index, image1Index, 0, 0, WIDTH, HEIGHT )

        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image2Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )

        imageData2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData2Index, self.ImageFormat, WIDTH, HEIGHT )
        
        OpenVG.GetImageSubData( image1Index, imageData2Index, 0, 0, HEIGHT,WIDTH )
        OpenVG.ImageSubData( imageData2Index, image2Index, 0, 0, WIDTH, HEIGHT )

        image3Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image3Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
        OpenVG.CopyImage( image3Index, 0, 0, image2Index, 0, 0, WIDTH, HEIGHT, 'ON' )

        image4Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.ChildImage( image4Index, image3Index, 0, 0, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.ClearImage( image4Index, 50, 50, 10, 10 )
        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        
        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
       
        paintColor = [ 0.5, 1.0, 0.25, 0.75 ]
        OpenVG.ColorPaint( paintIndex, paintColor )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( image4Index )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################        
class ImageHandling2( vt.VisualTestCase ):
    
    '''
    This test is for testing following functions:
    1. ReadPixels
    2. SetPixels
    3. CopyPixels
    4. GetPixels

    These are the steps of the test: 1. test draws a tiger with black and white
    rectangles to surface and copies pixels to image1 with ReadPixels. 2. test
    creates image2 and copies content of image1 to image2 by SetPixels. 3. test
    creates image3. The content of drawing surface is suffled with
    CopyPixels. The drawing surface is pasted to image3 with
    GetPixels. 4. image3 is drawn. The following values are changing with every
    testcount based on predefined allpair testing table. 1. ImageFormat
    2. ImageQuality 3. ImageMode.
    '''

    testValues = { ( 'ImageFormat', 'ImageQuality', 'ImageMode' ) :
                       [ ( 'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sRGB_565',       'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGB_565',       'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGB_565',       'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_sL_8',           'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_sL_8',           'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_sL_8',           'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_lL_8',           'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_lL_8',           'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_lL_8',           'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_A_8',            'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_A_8',            'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_A_8',            'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ),
                         ( 'VG_BW_1',           'VG_IMAGE_QUALITY_NONANTIALIASED', 'VG_DRAW_IMAGE_NORMAL' ),
                         ( 'VG_BW_1',           'VG_IMAGE_QUALITY_FASTER',         'VG_DRAW_IMAGE_MULTIPLY' ),
                         ( 'VG_BW_1',           'VG_IMAGE_QUALITY_BETTER',         'VG_DRAW_IMAGE_STENCIL' ) ]
                   }
    
    def __init__( self, ImageFormat, ImageQuality, ImageMode ):
        vt.VisualTestCase.__init__( self )
        self.ImageFormat  = ImageFormat
        self.ImageQuality = ImageQuality
        self.ImageMode    = ImageMode
        self.repeats      = 1
        self.name = 'OPENVG image handling2 %s %s %s' % ( ImageFormat, ImageQuality, ImageMode, )
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
                
        OpenVG.ClearColor( [ 0, 0, 0, 0.5 ] )

        OpenVG.ImageQuality( self.ImageQuality )
        OpenVG.ImageMode( self.ImageMode )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.ClearColor( [ 0, 0, 0, 0 ] )
        OpenVG.Clear( 5, 5, 25, 25 )
        OpenVG.ClearColor( [ 1, 1, 1, 1 ] )
        OpenVG.Clear( 30, 30, 25, 25 )       
        OpenVG.ClearColor( [ 0.5, 0.5, 0.5, 0.5 ] )
        
        image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image1Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )

        imageData1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData1Index, self.ImageFormat, WIDTH, HEIGHT )

        ######### Step 1 ###########################       
        OpenVG.ReadPixels( imageData1Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageData1Index, image1Index, 0, 0, WIDTH, HEIGHT )
        
        OpenVG.Clear( 0, 0, 0, 0 )

        ######## Step 2 ############################
        image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
        OpenVG.CreateImage( image2Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )

        imageData2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData2Index, self.ImageFormat, WIDTH, HEIGHT )
        
        OpenVG.SetPixels( 25, 25, image1Index, 25, 25, 150, 150 )

        OpenVG.ReadPixels( imageData2Index, 0, 0, WIDTH, HEIGHT )
        OpenVG.ImageSubData( imageData2Index, image2Index, 0, 0, WIDTH, HEIGHT )
        
        ####### Step 3 ##############################
        image3Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )        
        OpenVG.CreateImage( image3Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )

        imageData3Index = indexTracker.allocIndex( 'OPENVG_IMAGE_DATA_INDEX' )
        OpenVG.CreateNullImageData( imageData3Index, self.ImageFormat, WIDTH, HEIGHT )

        OpenVG.CopyPixels( 100, 25, 25, 100, 75, 75 )        
        OpenVG.CopyPixels( 25, 100, 100, 100, 75, 75 )
        OpenVG.CopyPixels( 100, 100, 25, 25, 75, 75 )  
        
        OpenVG.GetPixels( image3Index, 25, 25, 25, 25, 150, 150 )

        OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.0 ] )
        OpenVG.ClearImage( image3Index, 50, 50, 10, 10 )

        paintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
        OpenVG.CreatePaint( paintIndex )
       
        paintColor = [ 0.5, 1.0, 0.25, 0.75 ]
        OpenVG.ColorPaint( paintIndex, paintColor )
        OpenVG.SetPaint( paintIndex, [ 'VG_FILL_PATH' ] )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawImage( image3Index )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
