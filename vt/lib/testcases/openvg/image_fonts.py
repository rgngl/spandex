#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 240
HEIGHT = 320

######################################################################
class SetGlyphToImage( vt.VisualTestCase ):
        
        '''
        The test description: This test is for testing SetGlyphToImage.
        
        The test steps:
        1. The test creates five different color paints.
        2. The test creates five different paths and images.
                - Image format and image quality from testvalues.
        3. The test set glyphs to each image.
        4. The test draws letters N, O, K, I, A to surface, one letter at the time.  The content
        of surface is then "copied" to image with vg.GetPixels.
                - Each letter is drawn with different path and different paint.
                - After each letter is copied to current image, the surface will be cleared.
        5. The test draws the glyphs either one at the time or all at once, depending on the DrawMethod value in testValues-
        6. The test clears the used glypths and delete the used font-

        Expected output: The text NOKIA should appear to the center with varying image format and image quality.
        '''
        
        testValues = { ( 'ImageFormat', 'ImageQuality', 'DrawMethod' ) :
                               [  ( 'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_sRGB_565',       'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_sRGB_565',       'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_sL_8',           'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_sL_8',           'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_lL_8',           'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_lL_8',           'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_A_8',            'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_BW_1',           'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_BW_1',           'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_sXRGB_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_sXRGB_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_sARGB_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_sARGB_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_sRGBX_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_sRGBA_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_sRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_sRGB_565',       'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_sRGBA_5551',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_sRGBA_4444',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_sL_8',           'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_lRGBX_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_lRGBA_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_lRGBA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_lL_8',           'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_A_8',            'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_BW_1',           'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_sXRGB_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_sARGB_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_sARGB_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_sARGB_8888_PRE', 'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_sARGB_1555',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_sARGB_1555',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_sARGB_4444',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_sARGB_4444',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_lXRGB_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_lXRGB_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_lARGB_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_lARGB_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_lARGB_8888_PRE', 'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_lARGB_8888_PRE', 'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_sBGRX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_sBGRX_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_sBGRA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_sBGRA_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_sBGRA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_sBGRA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_sBGR_565',       'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_sBGR_565',       'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_sBGRA_5551',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_sBGRA_5551',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_sBGRA_4444',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_sBGRA_4444',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_lBGRX_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_lBGRX_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_lBGRA_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_lBGRA_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_lBGRA_8888_PRE', 'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_lBGRA_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_sARGB_8888_PRE', 'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_sARGB_1555',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_sARGB_4444',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_lXRGB_8888',     'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_lARGB_8888',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_lARGB_8888_PRE', 'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_sBGRX_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_sBGRA_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ),
                                  ( 'VG_sBGRA_8888_PRE', 'VG_IMAGE_QUALITY_FASTER',         'DrawGlyph'  ),
                                  ( 'VG_sBGR_565',       'VG_IMAGE_QUALITY_FASTER',         'DrawGlyphs' ),
                                  ( 'VG_sBGRA_5551',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyphs' ),
                                  ( 'VG_sBGRA_4444',     'VG_IMAGE_QUALITY_NONANTIALIASED', 'DrawGlyph'  ),
                                  ( 'VG_lBGRX_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyphs' ),
                                  ( 'VG_lBGRA_8888',     'VG_IMAGE_QUALITY_BETTER',         'DrawGlyph'  ), ]
                       }
        
        def __init__( self, ImageFormat, ImageQuality, DrawMethod ):
                vt.VisualTestCase.__init__( self )
                self.DATATYPE     =  'VG_PATH_DATATYPE_S_32'
                self.EscapementX  = 0.0
                self.EscapementY  = 0.0
                self.ImageFormat  = ImageFormat
                self.ImageQuality = ImageQuality
                self.DrawMethod   = DrawMethod
                self.repeats      = 1
                self.name = 'OPENVG SetGlyphToImage %s %s %s' % ( DrawMethod, ImageFormat, ImageQuality, ) 
                
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                
                OpenVG.ImageMode( 'VG_DRAW_IMAGE_NORMAL' )                
                OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
                
                # Create first paint
                Paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint1Index )
                OpenVG.ColorPaint( Paint1Index, [ 1.0, 0.2, 0, 1.0 ] )
                
                # Create second paint
                Paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint2Index )
                OpenVG.ColorPaint( Paint2Index, [ 0.0, 0.75, 0, 1.0 ] )
                
                # Create third paint
                Paint3Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint3Index )
                OpenVG.ColorPaint( Paint3Index, [ 0.0, 0.0, 0.75, 1.0 ] )
                
                # Create fourth paint
                Paint4Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint4Index )
                OpenVG.ColorPaint( Paint4Index, [ 1.0, 1.0, 1.0, 1.0 ] )
                
                # Create fifth paint
                Paint5Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint5Index )
                OpenVG.ColorPaint( Paint5Index, [ 0.75, 0.0, 0.75, 1.0 ] )
                
                OpenVG.ClearColor( [ 1.0, 1.0, 1.0, 1.0 ] )
                OpenVG.Clear( 0, 0, 0, 0 )

                # Create first image
                Image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image1Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
                
                # Create second image
                Image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image2Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )

                # Create third image
                Image3Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image3Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
 
                # Create fourth image
                Image4Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image4Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )

                # Create fifth image
                Image5Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image5Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
                
                # Define letterwidth and the number of used glypths
                LetterWidth = 35 
                GlyphCount  = 5
                
                # Count starting point of first letter
                GlyphOriginX = ( WIDTH / GlyphCount ) - LetterWidth
                GlyphOriginY = HEIGHT / 2
        
                # Set start point(  Glyph origin )
                OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )
                
                # Define indices
                Indices = []
                for i in range( GlyphCount ):
                        Indices.extend( [ i ] )
                        
                # Create a font
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )
                
                # Add some content to the path
                # Letter N
                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                OpenVG.MoveTo( PathData1Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ 0.0, LetterWidth ], 'ON' )
                OpenVG.Lines( PathData1Index, [ LetterWidth, -LetterWidth ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0.0, LetterWidth ], 'ON' )
                Glyph1Index = 0
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph1Index,
                                        Image1Index,
                                        [ 0.0, 0.0 ],
                                        [ LetterWidth + 15 + self.EscapementX, self.EscapementY ] )
                
                # Letter 0
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                OpenVG.MoveTo( PathData2Index, [ 10, 10 + ( LetterWidth / 2 ) ], 'OFF' )
                OpenVG.Arcs( PathData2Index, [ LetterWidth / 2, LetterWidth / 2, 0, LetterWidth, 0 ], 'SCWARC', 'ON' )
                OpenVG.Arcs( PathData2Index, [ -LetterWidth / 2, -LetterWidth / 2, 0, -LetterWidth, -0 ], 'SCWARC', 'ON' )
                Glyph2Index = 1
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph2Index,
                                        Image2Index,
                                        [ 0.0, 0.0 ],
                                        [ LetterWidth + 15 + self.EscapementX, self.EscapementY ] )
                
                # Letter K
                Path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData3Index, self.DATATYPE, 1, 1 )
                OpenVG.MoveTo( PathData3Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ 0, LetterWidth ], 'ON' )
                OpenVG.MoveTo( PathData3Index, [ 10, 10 + ( LetterWidth / 2 ) ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ LetterWidth, LetterWidth / 2 ], 'ON' )
                OpenVG.MoveTo( PathData3Index, [ 10, 10 + ( LetterWidth / 2 ) ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ LetterWidth, -LetterWidth / 2 ], 'ON' )
                Glyph3Index = 2
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph3Index,
                                        Image3Index,
                                        [ 0.0, 0.0 ],
                                        [ LetterWidth + 15 + self.EscapementX, self.EscapementY ] )
                
                # Letter I
                Path4Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path4Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )
                
                PathData4Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData4Index, self.DATATYPE, 1, 1 )
                OpenVG.MoveTo( PathData4Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData4Index, [ 0, LetterWidth ], 'ON' )
                Glyph4Index = 3
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph4Index,
                                        Image4Index,
                                        [ 0.0, 0.0 ],
                                        [ LetterWidth - ( LetterWidth - 15 ) + self.EscapementX, self.EscapementY ] )
                
                # letter A
                Path5Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path5Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData5Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData5Index, self.DATATYPE, 1, 1 )
                OpenVG.MoveTo( PathData5Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData5Index, [ LetterWidth / 2, LetterWidth ], 'ON' )
                OpenVG.Lines( PathData5Index,[ LetterWidth / 2, -LetterWidth ], 'ON' )
                OpenVG.MoveTo( PathData5Index,[ 10 + LetterWidth / 4, 10 + LetterWidth / 2 ], 'OFF' )
                OpenVG.Lines( PathData5Index,[ LetterWidth / 2, 0 ], 'ON' )
                Glyph5Index = 4
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph5Index,
                                        Image5Index,
                                        [ 0.0, 0.0 ],
                                        [ LetterWidth + self.EscapementX , self.EscapementY ] )
                
                # Append path data
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )
                OpenVG.AppendPathData( Path3Index, PathData3Index )
                OpenVG.AppendPathData( Path4Index, PathData4Index )
                OpenVG.AppendPathData( Path5Index, PathData5Index )

                OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.0 ] )
                
                # Set used paint, draw the path and get pixels from surface to first image
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( Paint1Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path1Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image1Index, 0, 0, 0, 0, WIDTH, HEIGHT )
                
                # Clear the surface. Set used paint, draw the path and get pixels from surface to second image
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( Paint2Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path2Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image2Index, 0, 0, 0, 0, WIDTH, HEIGHT )
                
                # Clear the surface. Set used paint, draw the path and get pixels from surface to third image
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( Paint3Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path3Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image3Index, 0, 0, 0, 0, WIDTH, HEIGHT )
                
                # Clear the surface. Set used paint, draw the path and get pixels from surface to fourth image
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( Paint4Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path4Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image4Index, 0, 0, 0, 0, WIDTH, HEIGHT )
                
                # Clear the surface. Set used paint, draw the path and get pixels from surface to fifth image
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( Paint5Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path5Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image5Index, 0, 0, 0, 0, WIDTH, HEIGHT )

                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()

                OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 0.0 ] )                
                OpenVG.Clear( 0, 0, 0, 0 )
                
                # Draw the glyphs one by one
                if self.DrawMethod == 'DrawGlyph':
                        OpenVG.DrawGlyph( FontIndex, Glyph1Index, [ 'VG_STROKE_PATH' ], 'ON' )
                        OpenVG.DrawGlyph( FontIndex, Glyph2Index, [ 'VG_STROKE_PATH' ], 'ON' )
                        OpenVG.DrawGlyph( FontIndex, Glyph3Index, [ 'VG_STROKE_PATH' ], 'ON' )
                        OpenVG.DrawGlyph( FontIndex, Glyph4Index, [ 'VG_STROKE_PATH' ], 'ON' )
                        OpenVG.DrawGlyph( FontIndex, Glyph5Index, [ 'VG_STROKE_PATH' ], 'ON' )
                
                # Draw all the glyphs at once
                if self.DrawMethod == 'DrawGlyphs':
                        AdjustmentX = [ 0.0, 0.0, 0.0, 0.0, 0.0 ]
                        AdjustmentY = [ 0.0, 0.0, 0.0, 0.0, 0.0 ]
                        OpenVG.DrawGlyphs( FontIndex,
                                           GlyphCount,
                                           Indices,
                                           AdjustmentX,
                                           AdjustmentY,
                                           [ 'VG_STROKE_PATH' ],
                                           'ON' )
                
                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                        
######################################################################
class SetGlyphToImageOverlapping( vt.VisualTestCase ):
        
        '''
        The test description: This test is for testing SetGlyphToImage.
        
        The test steps:
        1. The test creates five different color paints.
        2. The test creates five different paths and images.
                - Image format and image quality from testvalues.
        3. The test set glyphs to each image.
        4. The test draws letters O and K to surface, one letter at the time.  The content of
        surface is then "copied" to image with vg.GetPixels.
                - Each letter is drawn with different path and different paint
                - After each letter is copied to current image, the surface will be cleared.
        5. The test draws the glyphs moving starting point, so overlapping behaviour can be seen.
        6. The test clears the used glypths and delete the used font.

        Expected output: The text NOKIA should appear to the center with varying image format and
        image quality.
        '''
        
        testValues = { ( 'ImageFormat', 'ImageQuality', 'Space' ) :
                               [ ( 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 0 ),
                                 ( 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 20 ),
                                 ( 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 40 ),
                                 ( 'VG_sRGBX_8888', 'VG_IMAGE_QUALITY_NONANTIALIASED', 60 ) ]
                       }
        
        def __init__( self, ImageFormat, ImageQuality, Space ):
                vt.VisualTestCase.__init__( self )
                self.DATATYPE     =  'VG_PATH_DATATYPE_S_32'
                self.EscapementX  = 0.0
                self.EscapementY  = 0.0
                self.ImageFormat  = ImageFormat
                self.ImageQuality = ImageQuality
                self.Space        = Space
                self.repeats      = 1                
                self.name = 'OPENVG SetGlyphToImage (overlapping) %s %s %s' % ( ImageFormat, ImageQuality, Space, ) 
                
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                OpenVG.ImageMode( 'VG_DRAW_IMAGE_NORMAL' )
                OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
                
                # Create first paint
                Paint1Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint1Index )
                OpenVG.ColorPaint( Paint1Index, [ 0.75, 0.0, 0, 1.0 ] )
                
                # Create second paint
                Paint2Index = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( Paint2Index )
                OpenVG.ColorPaint( Paint2Index, [ 0.0, 0.75, 0, 1.0 ] )
                
                OpenVG.ClearColor( [ 1.0, 1.0, 1.0, 1.0 ] )
                OpenVG.Clear( 0, 0, 0, 0 )

                # Create first image
                Image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image1Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
                
                # Create second image
                Image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image2Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
                
                # Define letterwidth and the number of used glypths
                LetterWidth = 35 
                GlyphCount  = 2
                
                # Define indices
                Indices = []
                for i in range( GlyphCount ):
                        Indices.extend( [ i ] )
                        
                # Create a font
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )
                
                # Letter 0
                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                OpenVG.MoveTo( PathData1Index, [ 10, 10 + ( LetterWidth / 2 ) ], 'OFF' )
                OpenVG.Arcs( PathData1Index, [ LetterWidth / 2, LetterWidth / 2, 0, LetterWidth, 0 ], 'SCWARC', 'ON' )
                OpenVG.Arcs( PathData1Index, [ -LetterWidth / 2, -LetterWidth / 2, 0, -LetterWidth, -0 ], 'SCWARC', 'ON' )
                Glyph1Index = 0
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph1Index,
                                        Image1Index,
                                        [ 0.0, 0.0 ],
                                        [ self.EscapementX, self.EscapementY ] )
                
                # Letter K
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )                
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData2Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 0, LetterWidth ], 'ON' )
                OpenVG.MoveTo( PathData2Index, [ 10, 10 + ( LetterWidth / 2 ) ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ LetterWidth, LetterWidth/ 2 ], 'ON' )
                OpenVG.MoveTo( PathData2Index, [ 10, 10 + ( LetterWidth/ 2 ) ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ LetterWidth, -LetterWidth / 2 ], 'ON' )
                Glyph2Index = 1
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph2Index,
                                        Image2Index,
                                        [ 0.0, 0.0 ],
                                        [ self.EscapementX, self.EscapementY ] )
                
                # Append path data
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )
                
                # Set used paint, draw the path and get pixels from surface to first image
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( Paint1Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path1Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image1Index, 0, 0, 0, 0, WIDTH, HEIGHT )
                
                # Clear the surface. Set used paint, draw the path and get pixels from surface to second image
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( Paint2Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path2Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image2Index, 0, 0, 0, 0, WIDTH, HEIGHT )               
                
                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.Clear( 0, 0, 0, 0 )
                
                # Count starting point of first letter
                GlyphOriginX = ( WIDTH / 2 ) - ( 2 * LetterWidth )
                GlyphOriginY = HEIGHT / 2
                        
                # Draw the glyphs one by one
                for i in range( GlyphCount ):               
                        OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )
                        OpenVG.DrawGlyph( FontIndex, i, [ 'VG_STROKE_PATH' ], 'ON' )
                        GlyphOriginX += self.Space

                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                        

######################################################################                
class SetGlyphToImageOrig( vt.VisualTestCase ):
        
        '''
        The test description: This test is for testing SetGlyphToImage glyph_origin
        
        The test steps:
        1. The test creates two images.
        2. The test draws a letter N onto first image.
        3. The test draws some suitable figure onto second image.
        4. The test set images to glyph.
        5. The test draws the glyphs varying the glyph_origin value.

        Expected output: Two figures is drawn to the surface. Figures aligment against each other
        is varying depending on the glyph_origin value from testValues. Note that the first figure has
        also long line attached to it ( sort of 0-level ).
        '''
        
        testValues = { 'OrigX' : [ 0, 4, 8, 16 ],
                       'OrigY' : [ 0, 4, 8, 16 ]
                       }
        
        def __init__( self, OrigX, OrigY ):
                vt.VisualTestCase.__init__( self )
                self.DATATYPE     =  'VG_PATH_DATATYPE_S_32'
                self.EscapementX  = 0.0
                self.EscapementY  = 0.0
                self.DrawMethod   = 'DrawGlyph'
                self.ImageFormat  = 'VG_sRGBA_8888'
                self.ImageQuality = 'VG_IMAGE_QUALITY_BETTER'
                self.OrigX        = OrigX
                self.OrigY        = OrigY
                self.name = 'OPENVG SetGlyphToImage (glyph origin) %s %s' % ( OrigX, OrigY, )
                
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                
                OpenVG.ImageMode( 'VG_DRAW_IMAGE_NORMAL' )
                OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
                
                # Create first paint
                PaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( PaintIndex )
                OpenVG.ColorPaint( PaintIndex, [ 0.75, 0.0, 0, 1.0 ] )
  
                OpenVG.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                OpenVG.Clear( 0, 0, 0, 0 )

                # Create first image
                Image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image1Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
                
                # Create second image
                Image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image2Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
                
                # Define letterwidth and the number of used glypths
                LetterWidth = 35 
                GlyphCount  = 2
                
                # Count starting point of first letter
                GlyphOriginX = ( WIDTH / GlyphCount ) - LetterWidth
                GlyphOriginY = HEIGHT / 2
        
                # Set start point(  Glyph origin )
                OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )
                
                # Define indices
                Indices = []
                for i in range( GlyphCount ):
                        Indices.extend( [ i ] )
                        
                # Create a font
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )
                OpenVG.CreateFont( FontIndex, 0 )
                
                # Add some content to the path
                # Letter N
                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                OpenVG.MoveTo( PathData1Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData1Index, [ 0.0, LetterWidth ], 'ON' )
                OpenVG.Lines( PathData1Index, [ LetterWidth, -LetterWidth ], 'ON' )
                OpenVG.Lines( PathData1Index, [ 0.0, LetterWidth ], 'ON' )
                Glyph1Index = 0
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph1Index,
                                        Image1Index,
                                        [ 0.0, 0.0 ],
                                        [ LetterWidth + 30 + self.EscapementX, self.EscapementY ] )
                
                # Custom figure
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData2Index, [ 0,0 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ 30,0 ],'OFF' )
                OpenVG.Arcs( PathData2Index, [ 20, 70, 0, 10, 30 ], 'SCWARC', 'OFF' )
                OpenVG.Arcs( PathData2Index, [ 20, 20, 0, 40, 55 ], 'SCWARC', 'OFF' )
                OpenVG.Arcs( PathData2Index, [ -20, -20, 0, 10, 30 ], 'SCWARC', 'OFF' )
                Glyph2Index = 1
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph2Index,
                                        Image2Index,
                                        [ self.OrigX, self.OrigY ],
                                        [ LetterWidth + 30 + self.EscapementX, self.EscapementY ] )
                
                # Append path data
                OpenVG.AppendPathData( Path1Index, PathData1Index )
                OpenVG.AppendPathData( Path2Index, PathData2Index )

                # Set used paint, draw the path and get pixels from surface to first image
                OpenVG.SetPaint( PaintIndex, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path1Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image1Index, 0, 0, 0, 0, WIDTH, HEIGHT )
                
                # Clear the surface. Set used paint, draw the path and get pixels from surface to second image
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( PaintIndex, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path2Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image2Index, 0, 0, 0, 0, WIDTH, HEIGHT )

                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()
                
                OpenVG.Clear( 0, 0, 0, 0 )
                
                # Draw all the glyphs at once
                AdjustmentX = [ 0.0, 0.0, 0.0, 0.0, 0.0 ]
                AdjustmentY = [ 0.0, 0.0, 0.0, 0.0, 0.0 ]
                
                OpenVG.DrawGlyphs( FontIndex,
                                   GlyphCount,
                                   Indices,
                                   AdjustmentX,
                                   AdjustmentY,
                                   [ 'VG_STROKE_PATH' ],
                                   'ON' )
                
                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

                
######################################################################
class SetGlyphToImageEscapement( vt.VisualTestCase ):
        
        '''
        The test description: This test is for testing SetGlyphToImage escapement.
        
        The test steps:
        1. The test creates three images.
        2. The test draws a figure on each image.
        3. The test set images to glyph.
        4. The test draws the glyphs varying the escapement values.

        Expected output: Three figures is drawn to the surface. Figures aligment against
        each other is varying depending on the escapement value from testValues.
        '''
        
        testValues = { 'EscapementX' : [ 0, 4, 8, 12, -4, -8, -12 ],
                       'EscapementY' : [ 0, 4, 8, 12, -4, -8, -12 ]
                       }
                        
        def __init__( self, EscapementX, EscapementY ):
                vt.VisualTestCase.__init__( self )
                self.DATATYPE     = 'VG_PATH_DATATYPE_S_32'
                self.DrawMethod   = 'DrawGlyph'
                self.ImageFormat  = 'VG_sRGBA_8888'
                self.ImageQuality = 'VG_IMAGE_QUALITY_BETTER'
                self.EscapementX  = EscapementX
                self.EscapementY  = EscapementY
                self.repeats      = 1                
                self.name = 'OPENVG SetGlyphToImage (escapement) %s %s' % ( EscapementX, EscapementY, )
                
        def build( self, target, modules ):
                OpenVG = modules[ 'OpenVG' ]
                Vgtest = modules[ 'Vgtest' ]
        
                indexTracker = IndexTracker()
        
                # ----------------------------------------------------------------------
                self.beginInitActions()

                apis = [ API_OPENVG ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                OpenVG.ImageMode( 'VG_DRAW_IMAGE_NORMAL' )
                OpenVG.Stroke( 5, 'VG_CAP_BUTT', 'VG_JOIN_BEVEL', 0 )
                
                # Create first paint
                PaintIndex = indexTracker.allocIndex( 'OPENVG_PAINT_INDEX' )
                OpenVG.CreatePaint( PaintIndex )
                OpenVG.ColorPaint( PaintIndex, [ 0.75, 0.0, 0, 1.0 ] )
  
                OpenVG.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )
                OpenVG.Clear( 0, 0, 0, 0 )

                # Create first image
                Image1Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image1Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
                
                # Create second image
                Image2Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image2Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
                
                # Create third image
                Image3Index = indexTracker.allocIndex( 'OPENVG_IMAGE_INDEX' )
                OpenVG.CreateImage( Image3Index, self.ImageFormat, WIDTH, HEIGHT, [ self.ImageQuality ] )
                
                # Define letterwidth and the number of used glypths
                LetterWidth = 35 
                GlyphCount  = 3
                
                # Count starting point of first letter
                GlyphOriginX = ( WIDTH / GlyphCount ) - LetterWidth
                GlyphOriginY = HEIGHT / 2
        
                # Set start point(  Glyph origin )
                OpenVG.GlyphOrigin( [ GlyphOriginX, GlyphOriginY ] )
                
                # Define indices
                Indices = []
                for i in range( GlyphCount ):
                        Indices.extend( [ i ] )
                        
                # Create a font
                FontIndex = indexTracker.allocIndex( 'OPENVG_FONT_INDEX' )  
                OpenVG.CreateFont( FontIndex, 0 )
                
                # Custom figure
                Path1Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path1Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData1Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData1Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData1Index, [ 10, 10 ], 'OFF')
                OpenVG.Lines( PathData1Index, [ LetterWidth / 2, LetterWidth ], 'ON' )
                OpenVG.Lines( PathData1Index, [ LetterWidth / 2, -LetterWidth ], 'ON' )
                Glyph1Index = 0
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph1Index,
                                        Image1Index,
                                        [ 0.0, 0.0 ],
                                        [ LetterWidth + 15 + self.EscapementX, self.EscapementY ] )
                
                # Custom figure
                Path2Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path2Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData2Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData2Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData2Index, [ 10, LetterWidth + 10 ], 'OFF' )
                OpenVG.Lines( PathData2Index, [ LetterWidth / 2, -LetterWidth ], 'ON' )
                OpenVG.Lines( PathData2Index, [ LetterWidth / 2, LetterWidth ], 'ON' )
                Glyph2Index = 1
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph2Index,
                                        Image2Index,
                                        [ 0.0, 0.0 ],
                                        [ LetterWidth + 15 + self.EscapementX, self.EscapementY ] )
                
                # Custom figure
                Path3Index = indexTracker.allocIndex( 'OPENVG_PATH_INDEX' )
                OpenVG.CreatePath( Path3Index, self.DATATYPE, 1, 0, 1, 0, [ 'VG_PATH_CAPABILITY_ALL' ] )

                PathData3Index = indexTracker.allocIndex( 'OPENVG_PATH_DATA_INDEX' )
                OpenVG.CreatePathData( PathData3Index, self.DATATYPE, 1, 1 )
                
                OpenVG.MoveTo( PathData3Index, [ 10, 10 ], 'OFF' )
                OpenVG.Lines( PathData3Index, [ LetterWidth / 2, LetterWidth ], 'ON' )
                OpenVG.Lines( PathData3Index, [ LetterWidth / 2, -LetterWidth ], 'ON' )
                Glyph3Index = 2
                OpenVG.SetGlyphToImage( FontIndex,
                                        Glyph3Index,
                                        Image3Index,
                                        [ 0.0, 0.0 ],
                                        [ LetterWidth + 15 + self.EscapementX, self.EscapementY ] )
                
                # Append path data
                OpenVG.AppendPathData( Path1Index, Path1Index )
                OpenVG.AppendPathData( Path2Index, Path2Index )
                OpenVG.AppendPathData( Path3Index, Path3Index )
                
                # Set used paint, draw the path and get pixels from surface to first image
                OpenVG.SetPaint( PaintIndex, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path1Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image1Index, 0, 0, 0, 0, WIDTH, HEIGHT )
                
                # Clear the surface. Set used paint, draw the path and get pixels from surface to second image
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( PaintIndex, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path2Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image2Index, 0, 0, 0, 0, WIDTH, HEIGHT )
                
                # Clear the surface. Set used paint, draw the path and get pixels from surface to second image
                OpenVG.Clear( 0, 0, 0, 0 )
                OpenVG.SetPaint( PaintIndex, [ 'VG_STROKE_PATH' ] )
                OpenVG.DrawPath( Path3Index, [ 'VG_STROKE_PATH' ] )
                OpenVG.GetPixels( Image3Index, 0, 0, 0, 0, WIDTH, HEIGHT )
                
                # ----------------------------------------------------------------------
                self.beginBenchmarkActions()

                OpenVG.Clear( 0, 0, 0, 0 )
                
                # Draw all the glyphs at once
                AdjustmentX = [ 0.0, 0.0, 0.0, 0.0, 0.0 ]
                AdjustmentY = [ 0.0, 0.0, 0.0, 0.0, 0.0 ]
                OpenVG.DrawGlyphs( FontIndex,
                                   GlyphCount,
                                   Indices,
                                   AdjustmentX,
                                   AdjustmentY,
                                   [ 'VG_STROKE_PATH', 'VG_FILL_PATH' ],
                                   'ON' )
                
                OpenVG.CheckError( '' )               
                Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
