#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Ville-Veikko Tolonen <ville-veikko.tolonen@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common import *
from vgcommon   import *

WIDTH  = 200
HEIGHT = 200

######################################################################
class Tiger( vt.VisualTestCase ):
    
    '''
    Test for drawing the tiger image from the OpenVG reference engine.
    '''

    def __init__( self ):
        vt.VisualTestCase.__init__( self )
        self.repeats   = 1
        self.name = 'OPENVG tiger'
    
    def build( self, target, modules ):
        OpenVG = modules[ 'OpenVG' ]
        Vgtest = modules[ 'Vgtest' ]

        indexTracker = IndexTracker()
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENVG ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )
        
        OpenVG.ClearColor( [0,0,0,0.5] )
        
        tigerIndex = indexTracker.allocIndex( 'OPENVG_TIGER_INDEX' )
        OpenVG.CreateTiger( tigerIndex )
       
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()
        
        OpenVG.Clear( 0, 0, 0, 0 )
        OpenVG.DrawTiger( tigerIndex, 1, WIDTH, HEIGHT )

        OpenVG.CheckError( '' )               
        Vgtest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
