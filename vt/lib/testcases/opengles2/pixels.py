# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, gradient, c1, c2, textureType, minF, magF ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
    
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                             gradient,
                                             c1,
                                             c2,
                                             w, h,
                                             'OFF',
                                             textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                minF,
                                magF,
                                'GL_REPEAT',
                                'GL_REPEAT' )

        return TextureSetup( textureDataIndex, textureIndex )

######################################################################
class SmoothPixels( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing smooth pixels.

        Expected output: full screen plane with a 4x4 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''

        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats  = 1
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.overdraw = 1
                self.name = "OPENGLES2 smooth pixels"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                                
######################################################################
class FlatTexels( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing flat texels with different
        texture types.

        Expected output: a full screen plane with a 4x4 vertex grid. A gradient
        texture is scaled across each grid rectangle. Lower-left corner of the
        rectangle should be blue (black with luminance textures), upper-right
        corner should be white. Viewport size 320*240.
        '''

        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_LUMINANCE8' ),
                                             ( 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                             ( 'OPENGLES2_RGB565' ),
                                             ( 'OPENGLES2_RGB888' ),
                                             ( 'OPENGLES2_RGBA4444' ),
                                             ( 'OPENGLES2_RGBA5551' ),
                                             ( 'OPENGLES2_RGBA8888' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 flat texels, texture type=%s" % ( textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                textureSetup      = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  'OPENGLES2_GRADIENT_DIAGONAL',
                                                  [ 0.0, 0.0, 1.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.textureType,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class SmoothTexels( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing smooth texels with different
        texture types.

        Expected output: a full screen plane with a 4x4 vertex grid. A gradient
        texture is scaled across each grid rectangle, with [ 0.0, 0.0, 1.0, 1.0
        ] color at the bottom-left, [ 1.0, 1.0, 1.0, 1.0 ] at the top-right. The
        vertex colors are defined so that the lower-left vertex has color [0.5,
        0.0, 0.0, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top.  The texture color is modulated with the
        vertex colors. The net result at the screen should be so that the
        bottom-left corner of surface is black, top-right corner is white. The
        top-left corner of the rectangle at the surface bottom-left should be
        red. Viewport size 320*240.
        '''

        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_LUMINANCE8' ),
                                             ( 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                             ( 'OPENGLES2_RGB565' ),
                                             ( 'OPENGLES2_RGB888' ),
                                             ( 'OPENGLES2_RGBA4444' ),
                                             ( 'OPENGLES2_RGBA5551' ),
                                             ( 'OPENGLES2_RGBA8888' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 smooth texels, texture type=%s" % ( textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSColor;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;        

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSColor;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = gVSColor * texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, self.gridx, self.gridy, True, [ True ] )
                mesh.gradientColors( [ 0.5, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                textureSetup      = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  'OPENGLES2_GRADIENT_DIAGONAL',
                                                  [ 0.0, 0.0, 1.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.textureType,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class FlatMultitexels( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing flat multitexels (2) with
        different texture types.

        Expected output: a full screen plane with a 4x4 vertex grid. Two
        gradient textures (horizontal and vertical) are scaled across each grid
        rectangle. Lower-left corner of the rectangle should be black,
        upper-right corner should be white. Gradient should run diagonally
        within each rectangle. Viewport size 320*240.
        '''

        testValues = { ( 'texture1Type', 'texture2Type' ) : [ ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_RGBA8888' ),
                                                              ]
                       }
        
        def __init__( self, texture1Type, texture2Type ):
                vt.VisualTestCase.__init__( self )
                self.texture1Type = texture1Type
                self.texture2Type = texture2Type
                self.repeats      = 1
                self.gridx        = GRIDX
                self.gridy        = GRIDY
                self.overdraw     = 1
                self.name = "OPENGLES2 flat multitexels, texture1 type=%s, texture2 type=%s" % ( textureTypeToStr( self.texture1Type ),
                                                                                                 textureTypeToStr( self.texture2Type ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    attribute vec4 gTexCoord1;
                    varying vec4 gVSTexCoord0;
                    varying vec4 gVSTexCoord1;
                    uniform float gOffset;        
         
                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                        gVSTexCoord1 = gTexCoord1;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    uniform sampler2D gTexture1;        
                    varying vec4 gVSTexCoord0;
                    varying vec4 gVSTexCoord1;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy ) * texture2D( gTexture1, gVSTexCoord1.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, self.gridx, self.gridy, False, [ True, True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                texture1Setup     = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  'OPENGLES2_GRADIENT_HORIZONTAL',
                                                  [ 0.0, 0.0, 0.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.texture1Type,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                texture2Setup     = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,                                                  
                                                  'OPENGLES2_GRADIENT_VERTICAL',
                                                  [ 0.0, 0.0, 0.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.texture2Type,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                useTexture( modules, texture1Setup, 0, program )
                useTexture( modules, texture2Setup, 1, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class SmoothMultitexels( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing smooth multitexels (2) with
        different texture types.

        Expected output: a full screen plane with a 4x4 vertex grid. Two
        gradient textures (horizontal and vertical) are scaled across each grid
        rectangle. Lower-left corner of the combined texture should be black,
        upper-right corner should be white. Gradient should run diagonally
        within each rectangle.  The final color is modulated with the vertex
        color. The vertex colors are defined so that the lower-left vertex has
        color [0.5, 0.0, 0.0, 1.0 ] and the upper-right vertex has color [ 1.0,
        1.0, 1.0, 1.0]. Between those the vertex color is linearly interpolated
        from left to right, from bottom to top.  The net result at the screen
        should be so that the bottom-left corner of surface is black, top-right
        corner is white. The top-left corner of the rectangle at the surface
        bottom-left should be red. Viewport size 320*240.
        '''
        
        testValues = { ( 'texture1Type', 'texture2Type' ) : [ ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_LUMINANCE8', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_LUMINANCE_ALPHA88', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_RGB565', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_RGB888', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_RGBA4444', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_RGBA5551', 'OPENGLES2_RGBA8888' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_LUMINANCE8' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_LUMINANCE_ALPHA88' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_RGB565' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_RGB888' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_RGBA4444' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_RGBA5551' ),
                                                              ( 'OPENGLES2_RGBA8888', 'OPENGLES2_RGBA8888' ),
                                                              ]
                       }
        
        def __init__( self, texture1Type, texture2Type ):
                vt.VisualTestCase.__init__( self )
                self.texture1Type = texture1Type
                self.texture2Type = texture2Type
                self.repeats      = 1
                self.gridx        = GRIDX
                self.gridy        = GRIDY
                self.overdraw     = 1
                self.name = "OPENGLES2 smooth multitexels, texture1 type=%s, texture2 type=%s" % ( textureTypeToStr( self.texture1Type ),
                                                                                                 textureTypeToStr( self.texture2Type ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;        
                    attribute vec4 gTexCoord0;
                    attribute vec4 gTexCoord1;
                    varying vec4 gVSTexCoord0;
                    varying vec4 gVSTexCoord1;
                    varying vec4 gVSColor;
                    uniform float gOffset;        

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                        gVSTexCoord0 = gTexCoord0;
                        gVSTexCoord1 = gTexCoord1;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    uniform sampler2D gTexture1;
                    varying vec4 gVSColor;        
                    varying vec4 gVSTexCoord0;
                    varying vec4 gVSTexCoord1;

                    void main()
                    {
                        gl_FragColor = gVSColor * texture2D( gTexture0, gVSTexCoord0.xy ) * texture2D( gTexture1, gVSTexCoord1.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy, self.gridx, self.gridy, True, [ True, True ] )
                mesh.gradientColors( [ 0.5, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                texture1Setup     = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  'OPENGLES2_GRADIENT_HORIZONTAL',
                                                  [ 0.0, 0.0, 0.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.texture1Type,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                texture2Setup     = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,                                                  
                                                  'OPENGLES2_GRADIENT_VERTICAL',
                                                  [ 0.0, 0.0, 0.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.texture2Type,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                useTexture( modules, texture1Setup, 0, program )
                useTexture( modules, texture2Setup, 1, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class SmoothTexelsAlpha8( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing smooth texels with alpha8
        texture.

        Expected output: a full screen plane with a 4x4 vertex grid. A gradient
        alpha texture is scaled across each grid rectangle. Lower-left corner of
        the texture has alpha 0, upper-right corner has alpha 1. The vertex
        colors are modulated with texture alpha value. The vertex colors are
        defined so that the lower-left vertex has color [0.5, 0.0, 0.0, 1.0 ]
        and the upper-right vertex has color [ 1.0, 1.0, 1.0, 1.0]. Between
        those the vertex color is linearly interpolated from left to right, from
        bottom to top.  The net result at the screen should be so that the
        bottom-left corner of surface is black, top-right corner is white. The
        top-left corner of the rectangle at the surface bottom-left should be
        red. Viewport size 320*240.
        '''
       
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 smooth texels alpha 8"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSColor;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;        

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSColor;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = gVSColor * texture2D( gTexture0, gVSTexCoord0.xy ).a;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                mesh              = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, True, [ True ] )
                mesh.gradientColors( [ 0.5, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                textureSetup      = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  'OPENGLES2_GRADIENT_DIAGONAL',
                                                  [ 0.0, 0.0, 0.0, 0.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  'OPENGLES2_ALPHA8',
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class TextureExtensions( vt.VisualTestCase ):

        ''' The purpose of the test: test texture extensions.

        OPENGLES2_BGRA8888 texture type requires GL_EXT_texture_format_BGRA8888
        extension.

        Expected output: a full screen plane with a 4x4 vertex grid. A gradient
        texture is scaled across each grid rectangle. Lower-left corner of the
        rectangle should be blue (black with luminance textures), upper-right
        corner should be white. Viewport size 320*240.
        '''

        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_BGRA8888' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 texture extensions, texture type=%s" % ( textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                extensions = { 'OPENGLES2_BGRA8888' : 'GL_EXT_texture_format_BGRA8888' }
                
                OpenGLES2.CheckExtension( extensions[ self.textureType ] )
                
                mesh              = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH / self.gridx, True )[ 0 ]
                textureH = toPot( HEIGHT / self.gridy, True )[ 0 ]
                
                textureSetup      = setupTexture( modules,
                                                  indexTracker,
                                                  textureW, textureH,
                                                  'OPENGLES2_GRADIENT_DIAGONAL',
                                                  [ 0.0, 0.0, 1.0, 1.0 ],
                                                  [ 1.0, 1.0, 1.0, 1.0 ],
                                                  self.textureType,
                                                  'GL_LINEAR',
                                                  'GL_LINEAR' )

                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
