# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 8, 8 )

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FLOAT,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               [ 1.0, 1.0, 1.0, 1.0 ],
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh

######################################################################
class Blending( vt.VisualTestCase ):

        ''' The purpose of the test: test blending.

        Expected output: a 8x8 vertex grid is drawn full screen. The vertex
        colors are defined so that the lower-left vertex has color
        [0.0,0.0,0.5,0.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Surface is cleared
        to [0.5,0.5,0.5,0.5] before drawing the vertex grid. Blend color is
        defined as [1.0,0.5,0.5,0.5]. Viewport size 320*240.
        '''

        testValues = { ( 'sfactor', 'dfactor' ) : [ ( 'GL_ZERO', 'GL_ZERO' ),
                                                    ( 'GL_ZERO', 'GL_ONE' ),
                                                    ( 'GL_ZERO', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ZERO', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ZERO', 'GL_DST_COLOR' ),
                                                    ( 'GL_ZERO', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_ZERO', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ZERO', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ZERO', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ZERO', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_ZERO', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_ZERO', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_ZERO', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_ZERO', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE', 'GL_ZERO' ),
                                                    ( 'GL_ONE', 'GL_ONE' ),
                                                    ( 'GL_ONE', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ONE', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ONE', 'GL_DST_COLOR' ),
                                                    ( 'GL_ONE', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_ONE', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ONE', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ONE', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ONE', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_ONE', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_SRC_COLOR', 'GL_ZERO' ),
                                                    ( 'GL_SRC_COLOR', 'GL_ONE' ),
                                                    ( 'GL_SRC_COLOR', 'GL_SRC_COLOR' ),
                                                    ( 'GL_SRC_COLOR', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_SRC_COLOR', 'GL_DST_COLOR' ),
                                                    ( 'GL_SRC_COLOR', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_SRC_COLOR', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_SRC_COLOR', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_SRC_COLOR', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_SRC_COLOR', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_SRC_COLOR', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_SRC_COLOR', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_SRC_COLOR', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_SRC_COLOR', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_DST_COLOR', 'GL_ZERO' ),
                                                    ( 'GL_DST_COLOR', 'GL_ONE' ),
                                                    ( 'GL_DST_COLOR', 'GL_SRC_COLOR' ),
                                                    ( 'GL_DST_COLOR', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_DST_COLOR', 'GL_DST_COLOR' ),
                                                    ( 'GL_DST_COLOR', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_DST_COLOR', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_DST_COLOR', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_DST_COLOR', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_DST_COLOR', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_DST_COLOR', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_DST_COLOR', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_DST_COLOR', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_DST_COLOR', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ZERO' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_DST_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_COLOR', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_ZERO' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_ONE' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_SRC_COLOR' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_DST_COLOR' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ZERO' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_DST_COLOR' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_DST_ALPHA', 'GL_ZERO' ),
                                                    ( 'GL_DST_ALPHA', 'GL_ONE' ),
                                                    ( 'GL_DST_ALPHA', 'GL_SRC_COLOR' ),
                                                    ( 'GL_DST_ALPHA', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_DST_ALPHA', 'GL_DST_COLOR' ),
                                                    ( 'GL_DST_ALPHA', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_DST_ALPHA', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_DST_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_DST_ALPHA', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_DST_ALPHA', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_DST_ALPHA', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_DST_ALPHA', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_DST_ALPHA', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_DST_ALPHA', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ZERO' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ONE' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_DST_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_DST_ALPHA', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_ZERO' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_ONE' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_SRC_COLOR' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_DST_COLOR' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_CONSTANT_COLOR', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_CONSTANT_COLOR', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_ZERO' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_ONE' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_DST_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_COLOR', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_ZERO' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_ONE' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_SRC_COLOR' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_DST_COLOR' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_CONSTANT_ALPHA', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_ZERO' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_ONE' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_DST_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_ONE_MINUS_CONSTANT_ALPHA', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ZERO' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ONE' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_SRC_COLOR' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ONE_MINUS_SRC_COLOR' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_DST_COLOR' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ONE_MINUS_DST_COLOR' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_SRC_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_DST_ALPHA' ), 
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ONE_MINUS_DST_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_CONSTANT_COLOR' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ONE_MINUS_CONSTANT_COLOR' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_CONSTANT_ALPHA' ),
                                                    ( 'GL_SRC_ALPHA_SATURATE', 'GL_ONE_MINUS_CONSTANT_ALPHA' ),
                                                    ]
                       }
                                                                                          
        def __init__( self, sfactor, dfactor ):
                vt.VisualTestCase.__init__( self )
                self.sfactor  = sfactor
                self.dfactor  = dfactor
                self.repeats  = 1
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.name = "OPENGLES2 blending, sfactor=%s, dfactor=%s" % ( blendToStr( self.sfactor ),
                                                                             blendToStr( self.dfactor ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 0.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );

                clearColor = [ 0.5, 0.5, 0.5, 0.5 ]
                blendColor = [ 1.0, 0.5, 0.5, 0.5 ]
                OpenGLES2.ClearColor( clearColor )
                OpenGLES2.BlendColor( blendColor )                
                OpenGLES2.BlendFunc( self.sfactor, self.dfactor )
                OpenGLES2.Enable( 'GL_BLEND' )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                offset  = 1.0

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class SeparateBlending( vt.VisualTestCase ):

        ''' The purpose of the test: test separate blending.

        Expected output: a 8x8 vertex grid is drawn full screen. The vertex
        colors are defined so that the lower-left vertex has color
        [0.0,0.0,0.5,0.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Surface is cleared
        to [0.5,0.5,0.5,0.5] before drawing the vertex grid. Blend color is
        defined as [1.0,0.5,0.5,0.5]. Viewport size 320*240.
        '''

        testValues = { ( 'srcRGB', 'dstRGB', 'srcAlpha', 'dstAlpha' ) : [ ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA', 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                                          ( 'GL_SRC_COLOR', 'GL_ZERO', 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                                          ( 'GL_ZERO', 'GL_DST_COLOR', 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA' ),
                                                                          ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ONE', 'GL_ZERO' ),
                                                                          ( 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA', 'GL_ZERO', 'GL_ONE' ),
                                                                          ]
                       }
                                                                                          
        def __init__( self, srcRGB, dstRGB, srcAlpha, dstAlpha ):
                vt.VisualTestCase.__init__( self )
                self.srcRGB   = srcRGB
                self.dstRGB   = dstRGB
                self.srcAlpha = srcAlpha
                self.dstAlpha = dstAlpha
                self.repeats  = 1
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.name = "OPENGLES2 separate blending, srcRGB=%s, dstRGB=%s, srcAlpha=%s, dstAlpha=%s" % ( blendToStr( self.srcRGB ),
                                                                                                              blendToStr( self.dstRGB ),
                                                                                                              blendToStr( self.srcAlpha ),
                                                                                                              blendToStr( self.dstAlpha ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, True, [] )                
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 0.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );

                clearColor = [ 0.5, 0.5, 0.5, 0.5 ]
                blendColor = [ 1.0, 0.5, 0.5, 0.5 ]
                OpenGLES2.ClearColor( clearColor )
                OpenGLES2.BlendColor( blendColor )                
                OpenGLES2.BlendFuncSeparate( self.srcRGB,
                                             self.dstRGB,
                                             self.srcAlpha,
                                             self.dstAlpha )
                OpenGLES2.Enable( 'GL_BLEND' )               
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                offset  = 1.0

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class BlendingEquation( vt.VisualTestCase ):

        ''' The purpose of the test: test blending equations.

        Expected output: a 8x8 vertex grid is drawn full screen. The vertex
        colors are defined so that the lower-left vertex has color
        [0.0,0.0,0.5,0.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Surface is cleared
        to [0.5,0.5,0.5,0.5] before drawing the vertex grid. Blend functions are
        set as GL_SRC_COLOR, GL_DST_COLOR. Viewport size 320*240.
        '''

        
        testValues = { ( 'mode' ) : [ ( 'GL_FUNC_ADD' ),
                                      ( 'GL_FUNC_SUBTRACT' ),
                                      ( 'GL_FUNC_REVERSE_SUBTRACT' ),
                                      ]
                       }
                                                                                          
        def __init__( self, mode ):
                vt.VisualTestCase.__init__( self )
                self.mode     = mode
                self.repeats  = 1
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.name = "OPENGLES2 blending equation, mode=%s" % ( blendEqToStr( self.mode ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, True, [] )                
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 0.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );

                clearColor = [ 0.5, 0.5, 0.5, 0.5 ]
                blendColor = [ 1.0, 0.5, 0.5, 0.5 ]
                OpenGLES2.ClearColor( clearColor )
                OpenGLES2.BlendColor( blendColor )
                OpenGLES2.BlendFunc( 'GL_SRC_COLOR',
                                     'GL_DST_COLOR' )
                OpenGLES2.BlendEquation( self.mode )
                OpenGLES2.Enable( 'GL_BLEND' )               
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                offset  = 1.0

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
               
######################################################################
class SeparateBlendingEquation( vt.VisualTestCase ):

        ''' The purpose of the test: test separate blending equations.

        Expected output: a 8x8 vertex grid is drawn full screen. The vertex
        colors are defined so that the lower-left vertex has color
        [0.0,0.0,0.5,0.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Surface is cleared
        to [0.5,0.5,0.5,0.5] before drawing the vertex grid. Blend functions
        (separate) are set as GL_SRC_COLOR, GL_DST_COLOR, GL_SRC_ALPHA,
        GL_DST_ALPHA. Viewport size 320*240.
        '''

        
        testValues = { ( 'modeRGB', 'modeAlpha' ) : [ ( 'GL_FUNC_ADD',  'GL_FUNC_ADD' ),
                                                      ( 'GL_FUNC_ADD', 'GL_FUNC_SUBTRACT' ),
                                                      ( 'GL_FUNC_ADD', 'GL_FUNC_REVERSE_SUBTRACT' ),
                                                      ( 'GL_FUNC_SUBTRACT', 'GL_FUNC_ADD' ),
                                                      ( 'GL_FUNC_SUBTRACT', 'GL_FUNC_SUBTRACT' ),
                                                      ( 'GL_FUNC_SUBTRACT', 'GL_FUNC_REVERSE_SUBTRACT' ),
                                                      ( 'GL_FUNC_REVERSE_SUBTRACT', 'GL_FUNC_ADD' ),
                                                      ( 'GL_FUNC_REVERSE_SUBTRACT', 'GL_FUNC_SUBTRACT' ),
                                                      ( 'GL_FUNC_REVERSE_SUBTRACT', 'GL_FUNC_REVERSE_SUBTRACT' ),
                                                      ]
                       }
                                                                                          
        def __init__( self, modeRGB, modeAlpha ):
                vt.VisualTestCase.__init__( self )
                self.modeRGB   = modeRGB
                self.modeAlpha = modeAlpha
                self.repeats  = 1
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.name = "OPENGLES2 separate blending equation, modeRGB=%s, modeAlpha=%s" % ( blendEqToStr( self.modeRGB ),
                                                                                                 blendEqToStr( self.modeAlpha ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, True, [] )                
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 0.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );

                clearColor = [ 0.5, 0.5, 0.5, 0.5 ]
                blendColor = [ 1.0, 0.5, 0.5, 0.5 ]
                OpenGLES2.ClearColor( clearColor )
                OpenGLES2.BlendColor( blendColor )
                OpenGLES2.BlendFuncSeparate( 'GL_SRC_COLOR',
                                             'GL_DST_COLOR',
                                             'GL_SRC_ALPHA',                                             
                                             'GL_DST_ALPHA' )
                OpenGLES2.BlendEquationSeparate( self.modeRGB, self.modeAlpha )
                OpenGLES2.Enable( 'GL_BLEND' )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                offset  = 1.0

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                                
