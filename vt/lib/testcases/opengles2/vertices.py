# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 8, 8 )

######################################################################
class VertexType( vt.VisualTestCase ):

        ''' The purpose of the test: test different vertex types.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        testValues = { ( 'vertexType' ) : [ ( 'GL_FIXED' ),
                                            ( 'GL_FLOAT' ),
                                            ]
                       }
        
        def __init__( self, vertexType ):
                vt.VisualTestCase.__init__( self )
                self.vertexType = vertexType
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES2 vertex type, type=%s" % ( typeToStr( self.vertexType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                meshTypes = { 'GL_FIXED' : MESH_TYPE_FIXED,
                              'GL_FLOAT' : MESH_TYPE_FLOAT }
                
                vertexAttribute = MeshVertexAttribute( meshTypes[ self.vertexType ], 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                                
######################################################################
class HalfFloatVertex( vt.VisualTestCase ):

        ''' The purpose of the test: test half float vertex type. Requires
        GL_OES_vertex_half_float extension.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES2 half float vertex"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_vertex_half_float' )
                
                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexArrayIndex        = -1
                vertexArrayNormalized   = False
                colorArrayIndex         = -1
                colorArrayNormalized    = False
                indexArrayIndex         = -1

                vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                vertexArrayNormalized = mesh.vertexNormalized
                OpenGLES2.CreateArray( vertexArrayIndex,
                                       'GL_HALF_FLOAT_OES' )
                OpenGLES2.AppendToArray( vertexArrayIndex, mesh.vertices )

                colorArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                colorArrayNormalized = mesh.colorNormalized
                OpenGLES2.CreateArray( colorArrayIndex,
                                       mesh.colorGLType )
                OpenGLES2.AppendToArray( colorArrayIndex, mesh.colors )
           
                indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( indexArrayIndex, mesh.indexGLType );
                OpenGLES2.AppendToArray( indexArrayIndex, mesh.indices )

                vertexDataSetup = VertexDataSetup( vertexArrayIndex,
                                                   mesh.vertexComponents,
                                                   vertexArrayNormalized,                            
                                                   colorArrayIndex,
                                                   mesh.colorComponents,
                                                   colorArrayNormalized,
                                                   -1,
                                                   [],
                                                   [],
                                                   False,
                                                   indexArrayIndex,
                                                   len( mesh.indices ),
                                                   mesh.glMode )       
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                # Clear the screen
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                                
######################################################################
class VertexPointerOffset( vt.VisualTestCase ):

        ''' The purpose of the test: test vertex pointer offset. Padding is
        added at the beginning of vertex data which is skipped using offset.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        testValues = { ( 'offset' ) : [ ( 0 ),
                                        ( 2 ),
                                        ( 5 ),                                        
                                        ]
                       }
        
        def __init__( self, offset ):
                vt.VisualTestCase.__init__( self )
                self.offset     = offset
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.name = "OPENGLES2 vertex pointer offset, offset=%d" % ( self.offset, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshTrianglePlane( self.gridx,
                                          self.gridy,
                                          vertexAttribute,
                                          colorAttribute,
                                          None,
                                          None,
                                          indexAttribute,
                                          None,
                                          MESH_CCW_WINDING,
                                          False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                paddingVertex = []
                paddingColor  = []
                paddingIndex  = []
                for i in range( self.offset ):
                        paddingVertex += [ i * 10 ] * mesh.vertexComponents
                        paddingColor  += [ ( i * 10 ) % 255 ] * mesh.colorComponents
                        paddingIndex  += [ i, i + 1, i + 2 ]
                mesh.vertices = paddingVertex + mesh.vertices
                mesh.colors   = paddingColor + mesh.colors
                mesh.indices  = paddingIndex + mesh.indices
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )

                OpenGLES2.UseProgram( program.programIndex )

                OpenGLES2.VertexAttribPointer( program.vertexLocationIndex,
                                               vertexDataSetup.vertexArrayIndex,
                                               vertexDataSetup.vertexSize,
                                               boolToSpandex( vertexDataSetup.vertexArrayNormalized ),
                                               self.offset )
                OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

                OpenGLES2.VertexAttribPointer( program.colorLocationIndex,
                                               vertexDataSetup.colorArrayIndex,
                                               vertexDataSetup.colorSize,
                                               boolToSpandex( vertexDataSetup.colorArrayNormalized ),
                                               self.offset )
                OpenGLES2.EnableVertexAttribArray( program.colorLocationIndex )

                OpenGLES2.ValidateProgram( program.programIndex )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                offset  = 1.0

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                OpenGLES2.DrawElements( vertexDataSetup.glMode,
                                        vertexDataSetup.indexArrayLength - self.offset * 3, 
                                        self.offset * 3,
                                        vertexDataSetup.indexArrayIndex )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class VertexBuffer( vt.VisualTestCase ):

        ''' The purpose of the test: test vertex buffer.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES2 vertex buffer"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )

                vertexBufferDataSetup = setupVertexBufferData( modules, indexTracker, vertexDataSetup )
                useVertexBufferData( modules, indexTracker, vertexBufferDataSetup, program )
                
                offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexBufferData( modules, vertexBufferDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class VertexBufferOffset( vt.VisualTestCase ):

        ''' The purpose of the test: test vertex buffer offset. Padding is added
        at the beginning of vertex data which is skipped using offset.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        testValues = { ( 'offset' ) : [ ( 0 ),
                                        ( 2 ),
                                        ( 5 ),                                        
                                        ]
                       }
        
        def __init__( self, offset ):
                vt.VisualTestCase.__init__( self )
                self.offset     = offset
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES2 vertex buffer offset, offset=%d" % ( self.offset, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                paddingVertex = []
                paddingColor  = []
                paddingIndex  = []
                for i in range( self.offset ):
                        paddingVertex += [ i * 10 ] * mesh.vertexComponents
                        paddingColor  += [ ( i * 10 ) % 255 ] * mesh.colorComponents
                        paddingIndex  += [ i, i + 1, i + 2 ]
                mesh.vertices = paddingVertex + mesh.vertices
                mesh.colors   = paddingColor + mesh.colors
                mesh.indices  = paddingIndex + mesh.indices
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )

                vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
                OpenGLES2.GenBuffer( vertexBufferIndex )
                OpenGLES2.BufferData( vertexBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      vertexDataSetup.vertexArrayIndex,
                                      0,
                                      0 )
                vertexArrayNormalized = vertexDataSetup.vertexArrayNormalized
    
                colorBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
                OpenGLES2.GenBuffer( colorBufferIndex )
                OpenGLES2.BufferData( colorBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      vertexDataSetup.colorArrayIndex,
                                      0,
                                      0 )
                colorArrayNormalized = vertexDataSetup.colorArrayNormalized
        
                indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
                OpenGLES2.GenBuffer( indexBufferIndex )
                OpenGLES2.BufferData( indexBufferIndex,
                                      'ON',
                                      'GL_ELEMENT_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      vertexDataSetup.indexArrayIndex,
                                      0,
                                      0 )

                OpenGLES2.UseProgram( program.programIndex )

                OpenGLES2.BufferVertexAttribPointer( vertexBufferIndex,
                                                     'ON',
                                                     program.vertexLocationIndex,
                                                     vertexDataSetup.vertexSize,
                                                     boolToSpandex( vertexDataSetup.vertexArrayNormalized ),
                                                     self.offset )
                OpenGLES2.EnableVertexAttribArray( program.vertexLocationIndex )

                OpenGLES2.BufferVertexAttribPointer( colorBufferIndex,
                                                     'ON',
                                                     program.colorLocationIndex,
                                                     vertexDataSetup.colorSize,
                                                     boolToSpandex( vertexDataSetup.colorArrayNormalized ),
                                                     self.offset )
                OpenGLES2.EnableVertexAttribArray( program.colorLocationIndex )

                OpenGLES2.ValidateProgram( program.programIndex )
                
                offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        OpenGLES2.BufferDrawElements( indexBufferIndex,
                                                      'ON',
                                                      vertexDataSetup.glMode,
                                                      vertexDataSetup.indexArrayLength - self.offset * 3, 
                                                      self.offset * 3 )    
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class InterleavedArrays( vt.VisualTestCase ):

        ''' The purpose of the test: test interleaved vertex attribute arrays.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES2 interleaved arrays"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                meshDataSetup    = setupMeshData( modules, indexTracker, vertexDataSetup )
                useMeshData( modules, indexTracker, meshDataSetup, program )
                
                offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class InterleavedBufferArrays( vt.VisualTestCase ):

        ''' The purpose of the test: test interleaved vertex attribute arrays
        from a buffer.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats    = 1
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES2 interleaved buffer arrays"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )

                meshDataSetup       = setupMeshData( modules, indexTracker, vertexDataSetup )
                meshBufferDataSetup = setupMeshBufferData( modules, indexTracker, meshDataSetup )
                useMeshBufferData( modules, indexTracker, meshBufferDataSetup, program )
                
                offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawMeshBufferData( modules, meshBufferDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class VertexBufferUsage( vt.VisualTestCase ):

        ''' The purpose of the test: test vertex buffer usage.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        testValues = { ( 'usage' ) : [ ( 'GL_STATIC_DRAW' ),
                                       ( 'GL_DYNAMIC_DRAW' ),
                                       ( 'GL_STREAM_DRAW' ),
                                       ]
                       }       
        
        def __init__( self, usage ):
                vt.VisualTestCase.__init__( self )
                self.repeats  = 1
                self.usage    = usage
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.overdraw = 1
                self.name = "OPENGLES2 vertex buffer usage, usage=%s" % ( self.usage[ 3: ], )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshStripPlane( self.gridx,
                                       self.gridy,
                                       vertexAttribute,
                                       colorAttribute,
                                       None,
                                       None,
                                       indexAttribute,
                                       None,
                                       MESH_CCW_WINDING,
                                       False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )

                vertexBufferIndex       = -1
                vertexArrayNormalized   = False
                colorBufferIndex        = -1
                colorArrayNormalized    = False
                normalBufferIndex       = -1
                texCoordBufferIndices   = []
                texCoordArrayNormalized = []
                indexArrayIndex         = -1

                vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
                OpenGLES2.GenBuffer( vertexBufferIndex )
                OpenGLES2.BufferData( vertexBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      self.usage,
                                      vertexDataSetup.vertexArrayIndex,
                                      0,
                                      0 )
                vertexArrayNormalized = vertexDataSetup.vertexArrayNormalized
    
                colorBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
                OpenGLES2.GenBuffer( colorBufferIndex )
                OpenGLES2.BufferData( colorBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      self.usage,
                                      vertexDataSetup.colorArrayIndex,
                                      0,
                                      0 )
                colorArrayNormalized = vertexDataSetup.colorArrayNormalized
        
                indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
                OpenGLES2.GenBuffer( indexBufferIndex )
                OpenGLES2.BufferData( indexBufferIndex,
                                      'ON',
                                      'GL_ELEMENT_ARRAY_BUFFER',
                                      self.usage,
                                      vertexDataSetup.indexArrayIndex,
                                      0,
                                      0 )

                vertexBufferDataSetup =  VertexBufferDataSetup( vertexBufferIndex,
                                                                vertexDataSetup.vertexSize,
                                                                vertexArrayNormalized,
                                                                colorBufferIndex,
                                                                vertexDataSetup.colorSize,
                                                                colorArrayNormalized,
                                                                normalBufferIndex,
                                                                texCoordBufferIndices,
                                                                vertexDataSetup.texCoordSizes,
                                                                texCoordArrayNormalized,
                                                                indexBufferIndex,
                                                                vertexDataSetup.indexArrayLength,
                                                                vertexDataSetup.glMode )
                
                useVertexBufferData( modules, indexTracker, vertexBufferDataSetup, program )
                
                offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexBufferData( modules, vertexBufferDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class VertexBufferSubdata( vt.VisualTestCase ):

        ''' The purpose of the test: test vertex buffer subdata.

        Expected output: full screen plane with a 8x8 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats    = 1
                self.offset     = 8
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.overdraw   = 1
                self.name = "OPENGLES2 vertex buffer subdata"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                vertexAttribute = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
                colorAttribute  = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
                indexAttribute  = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )

                mesh = MeshTrianglePlane( self.gridx,
                                          self.gridy,
                                          vertexAttribute,
                                          colorAttribute,
                                          None,
                                          None,
                                          indexAttribute,
                                          None,
                                          MESH_CCW_WINDING,
                                          False )
        
                mesh.translate( [ -0.5, -0.5, 0 ] )
                mesh.scale( [ 2.0, 2.0 ] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                subbedVertices = []
                subbedColors   = []
                subbedIndices  = []
                vertexCount = len( mesh.vertices ) / mesh.vertexComponents
                for i in range( self.offset + vertexCount ):
                        subbedVertices += [ 0 ] * mesh.vertexComponents
                        subbedColors   += [ 0 ] * mesh.colorComponents

                for i in range( len( mesh.indices ) / 3 + self.offset ):
                        subbedIndices  += [ 0, 0, 0 ]

                subbedVertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( subbedVertexArrayIndex,
                                       mesh.vertexGLType )
                OpenGLES2.AppendToArray( subbedVertexArrayIndex, subbedVertices )

                vertexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( vertexArrayIndex,
                                       mesh.vertexGLType )
                OpenGLES2.AppendToArray( vertexArrayIndex, mesh.vertices )
                
                subbedColorArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( subbedColorArrayIndex,
                                       mesh.colorGLType )
                OpenGLES2.AppendToArray( subbedColorArrayIndex, subbedColors )

                colorArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( colorArrayIndex,
                                       mesh.colorGLType )
                OpenGLES2.AppendToArray( colorArrayIndex, mesh.colors )
                
                subbedIndexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( subbedIndexArrayIndex, mesh.indexGLType );
                OpenGLES2.AppendToArray( subbedIndexArrayIndex, subbedIndices )

                indexArrayIndex = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( indexArrayIndex, mesh.indexGLType );
                OpenGLES2.AppendToArray( indexArrayIndex, mesh.indices )
                
                vsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                fsDataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
    
                OpenGLES2.SetData( vsDataIndex, vsSource )
                OpenGLES2.SetData( fsDataIndex, fsSource )
    
                vsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
                OpenGLES2.CreateShader( 'GL_VERTEX_SHADER', vsIndex )
                OpenGLES2.ShaderSource( vsIndex, vsDataIndex )
                OpenGLES2.CompileShader( vsIndex )

                fsIndex = indexTracker.allocIndex( 'OPENGLES2_SHADER_INDEX' )
                OpenGLES2.CreateShader( 'GL_FRAGMENT_SHADER', fsIndex )
                OpenGLES2.ShaderSource( fsIndex, fsDataIndex )
                OpenGLES2.CompileShader( fsIndex )

                programIndex = indexTracker.allocIndex( 'OPENGLES2_PROGRAM_INDEX' )
                OpenGLES2.CreateProgram( programIndex )

                OpenGLES2.AttachShader( programIndex, vsIndex )
                OpenGLES2.AttachShader( programIndex, fsIndex )

                vertexAttributeName   = 'gVertex'
                colorAttributeName    = 'gColor'

                locationValue = 0
                vertexLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.SetRegister( vertexLocationIndex, locationValue )
                OpenGLES2.BindAttribLocation( programIndex, vertexLocationIndex, vertexAttributeName )
                locationValue += 1

                colorLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.SetRegister( colorLocationIndex, locationValue )
                OpenGLES2.BindAttribLocation( programIndex, colorLocationIndex, colorAttributeName )
                locationValue += 1
           
                OpenGLES2.LinkProgram( programIndex )

                OpenGLES2.DeleteShader( vsIndex )
                OpenGLES2.DeleteShader( fsIndex )

                vertexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
                OpenGLES2.GenBuffer( vertexBufferIndex )
                OpenGLES2.BufferData( vertexBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      subbedVertexArrayIndex,
                                      0,
                                      0 )

                OpenGLES2.BufferSubData( vertexBufferIndex,
                                         'ON',
                                         'GL_ARRAY_BUFFER',
                                         self.offset * mesh.vertexComponents,
                                         vertexArrayIndex,
                                         0,
                                         0 )
                
                colorBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
                OpenGLES2.GenBuffer( colorBufferIndex )
                OpenGLES2.BufferData( colorBufferIndex,
                                      'ON',
                                      'GL_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      subbedColorArrayIndex,
                                      0,
                                      0 )
                
                OpenGLES2.BufferSubData( colorBufferIndex,
                                         'ON',
                                         'GL_ARRAY_BUFFER',
                                         self.offset * mesh.colorComponents,
                                         colorArrayIndex,
                                         0,
                                         0 )

                indexBufferIndex = indexTracker.allocIndex( 'OPENGLES2_BUFFER_INDEX' )
                OpenGLES2.GenBuffer( indexBufferIndex )
                OpenGLES2.BufferData( indexBufferIndex,
                                      'ON',
                                      'GL_ELEMENT_ARRAY_BUFFER',
                                      'GL_STATIC_DRAW',
                                      subbedIndexArrayIndex,
                                      0,
                                      0 )

                OpenGLES2.BufferSubData( indexBufferIndex,
                                         'ON',
                                         'GL_ELEMENT_ARRAY_BUFFER',
                                         self.offset * 3,
                                         indexArrayIndex,
                                         0,
                                         0 )
                
                OpenGLES2.UseProgram( programIndex )

                OpenGLES2.BufferVertexAttribPointer( vertexBufferIndex,
                                                     'ON',
                                                     vertexLocationIndex,
                                                     mesh.vertexComponents,
                                                     'OFF',
                                                     self.offset )
                OpenGLES2.EnableVertexAttribArray( vertexLocationIndex )               

                OpenGLES2.BufferVertexAttribPointer( colorBufferIndex,
                                                     'ON',
                                                     colorLocationIndex,
                                                     mesh.colorComponents,
                                                     'ON',
                                                     self.offset )
                OpenGLES2.EnableVertexAttribArray( colorLocationIndex )

                OpenGLES2.ValidateProgram( programIndex )
                
                offsetLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                OpenGLES2.BufferDrawElements( indexBufferIndex,
                                              'ON',
                                              mesh.glMode,
                                              len( subbedIndices ) - self.offset * 3, 
                                              self.offset * 3 )    

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
