# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )

######################################################################
def createPlane( gridx, gridy ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FLOAT, 3 )
        colorAttribute    = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )
        
        return mesh
       
######################################################################
class DepthFunc( vt.VisualTestCase ):

        ''' The purpose of the test: test depth func.

        Expected output: first the test draws a full screen mesh with a 4x4
        vertex grid. The vertex colors are defined so that the lower-left vertex
        has color [0.0,0.0,1.0,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. In between, the vertex color is linearly interpolated
        from left to right, from bottom to top. The vertex depth values are
        defined -1.0 and 1.0 in a similar fashion. The first mesh is drawn using
        GL_ALWAYS depth func, after which the depth function is set according to
        the test definition. Secondly, the test draws a full screen mesh with a
        4x4 vertex grid. The vertex colors are defined [1.0,1.0,1.0,1.0]
        (lower-left) and [1.0,0.0,0.0,1.0] (upper-right). In between, the vertex
        color is linearly interpolated from left to right, from bottom to
        top. The vertex depth values are defined 1.0 and 0.0 in a similar
        fashion. Viewport size 320*240.
        '''

        testValues = { ( 'depthFunc' ) : [ ( 'GL_NEVER' ),
                                           ( 'GL_LESS' ),
                                           ( 'GL_LEQUAL' ),
                                           ( 'GL_GREATER' ),
                                           ( 'GL_GEQUAL' ),
                                           ( 'GL_EQUAL' ),
                                           ( 'GL_NOTEQUAL' ),
                                           ( 'GL_ALWAYS' ),
                                           ]
                       }       
        
        def __init__( self, depthFunc ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.depthFunc = depthFunc
                self.gridx     = GRIDX
                self.gridy     = GRIDY
                self.name = "OPENGLES2 depth func, depth func=%s" % ( depthFuncToStr( self.depthFunc ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh1.depthGradient( -1.0, 1.0 )
                
                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 1.0, 1.0, 1.0 ], [ 1.0, 0.0, 0.0, 1.0 ] )
                mesh2.depthGradient( 1.0, -1.0 )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )                
                program          = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_ALWAYS' )                                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )
                
                OpenGLES2.DepthFunc( self.depthFunc )

                useVertexData( modules, indexTracker, vertexDataSetup2, program )
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                                
######################################################################
class DepthFuncWithEqualDepth( vt.VisualTestCase ):

        ''' The purpose of the test: test depth func with equal depth.

        Expected output: first the test draws a full screen mesh with a 4x4
        vertex grid. The vertex colors are defined so that the lower-left vertex
        has color [0.0,0.0,1.0,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. In between, the vertex color is linearly interpolated
        from left to right, from bottom to top. The vertex depth values are
        defined -1.0 and 1.0 in a similar fashion. The first mesh is drawn using
        GL_ALWAYS depth func, after which the depth function is set according to
        the test definition. Secondly, the test draws a full screen mesh with a
        4x4 vertex grid. The vertex colors are defined [1.0,1.0,1.0,1.0]
        (lower-left) and [1.0,0.0,0.0,1.0] (upper-right). In between, the vertex
        color is linearly interpolated from left to right, from bottom to
        top. The vertex depth values are defined -1.0 and 1.0 in a similar
        fashion so the second mesh has the same vertex position data as the
        first mesh. Viewport size 320*240..
        '''

        testValues = { ( 'depthFunc' ) : [ ( 'GL_NEVER' ),
                                           ( 'GL_LESS' ),
                                           ( 'GL_LEQUAL' ),
                                           ( 'GL_GREATER' ),
                                           ( 'GL_GEQUAL' ),
                                           ( 'GL_EQUAL' ),
                                           ( 'GL_NOTEQUAL' ),
                                           ( 'GL_ALWAYS' ),
                                           ]
                       }

        def __init__( self, depthFunc ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.depthFunc = depthFunc
                self.gridx     = GRIDX
                self.gridy     = GRIDY
                self.name = "OPENGLES2 depth func with equal depth, depth func=%s" % ( depthFuncToStr( self.depthFunc ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh1.depthGradient( -1.0, 1.0 )
                
                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 1.0, 1.0, 1.0 ], [ 1.0, 0.0, 0.0, 1.0 ] )
                mesh2.depthGradient( -1.0, 1.0 )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )                
                program          = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_ALWAYS' )                                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )
                
                OpenGLES2.DepthFunc( self.depthFunc )

                useVertexData( modules, indexTracker, vertexDataSetup2, program )
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                                
######################################################################
class DepthMask( vt.VisualTestCase ):

        ''' The purpose of the test: test depth mask.

        Expected output: first a blue rect is drawn with GL_LESS depth func and
        depth mask set as defined by the test. Secondly, the depth buffer is
        cleared if set by the test. Finally a red rectangle is drawn with
        identical vertex positions as in the blue rectangle. Red rectangle
        should be visible when depth mask is OFF, or when the depth buffer is
        cleared after the blue rect. Viewport size 320*240.
        '''

        testValues = { ( 'mask', 'clear' ) : [ ( 'ON', False, ),
                                               ( 'ON', True, ),
                                               ( 'OFF', False ),
                                               ( 'OFF', True ),                                               
                                               ]
                       }
        
        def __init__( self, mask, clear ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.mask      = mask
                self.clear     = clear
                self.gridx     = GRIDX
                self.gridy     = GRIDY
                self.name = "OPENGLES2 depth mask, mask=%s, clear=%s" % ( self.mask,
                                                                          boolToStr( self.clear ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh1.depthGradient( -1.0, 1.0 )

                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh2.depthGradient( -1.0, 1.0 )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LESS' )
                OpenGLES2.DepthMask( self.mask )
                useVertexData( modules, indexTracker, vertexDataSetup1, program )                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )

                if self.clear:
                        OpenGLES2.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )                
                
                useVertexData( modules, indexTracker, vertexDataSetup2, program )
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup2 )               

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################
class ClearDepth( vt.VisualTestCase ):

        ''' The purpose of the test: test clear depth.

        Expected output: test draws a full screen mesh with a 4x4 vertex
        grid. The vertex colors are defined so that the lower-left vertex has
        color [0.0,0.0,1.0,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. In between, the vertex color is linearly interpolated
        from left to right, from bottom to top. The vertex depth values are
        defined -1.0 and 1.0 in a similar fashion. Depth function is set as
        GL_LESS and the depth buffer is cleared to clear depth value before
        drawing the mesh. Viewport size 320*240.
        '''

        testValues = { ( 'clearDepth' ) : [ ( 1.0 ),
                                            ( 0.25 ),
                                            ( 0.50 ),
                                            ( 0.75 ),
                                            ( 0.0 ),                                            
                                            ]
                       }
        
        def __init__( self, clearDepth ):
                vt.VisualTestCase.__init__( self )
                self.repeats    = 1
                self.clearDepth = clearDepth
                self.gridx      = GRIDX
                self.gridy      = GRIDY
                self.name = "OPENGLES2 clear depth, clear depth=%.2f" % ( self.clearDepth, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh = createPlane( self.gridx, self.gridy )
                mesh.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.depthGradient( -1.0, 1.0 )

                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LESS' )
                OpenGLES2.ClearDepth( self.clearDepth )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                useVertexData( modules, indexTracker, vertexDataSetup, program )                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                

######################################################################
class DepthRange( vt.VisualTestCase ):

        ''' The purpose of the test: test depth range.

        Expected output: first the test draws a full screen mesh with a 4x4
        vertex grid. The vertex colors are defined so that the lower-left vertex
        has color [0.0,0.0,1.0,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. In between, the vertex color is linearly interpolated
        from left to right, from bottom to top. The vertex depth values are
        defined -1.0 and 1.0 in a similar fashion. The first mesh is drawn using
        GL_ALWAYS depth func with depth range set according to the test
        specification. Secondly, the depth function is set GL_LESS and the test
        draws a full screen mesh with a 4x4 vertex grid with the vertex colors
        defined as [1.0,1.0,1.0,1.0] (lower-left) and [1.0,0.0,0.0,1.0]
        (upper-right). In between, the vertex color is linearly interpolated
        from left to right, from bottom to top. The vertex depth values are
        defined 1.0 and -1.0 in a similar fashion. Viewport size 320*240.
        '''

        testValues = { ( 'near', 'far' ) : [ ( 0.0, 1.0 ),
                                             ( 1.0, 0.0 ),                                     
                                             ]
                       }
        
        def __init__( self, near, far ):
                vt.VisualTestCase.__init__( self )
                self.repeats  = 1
                self.near     = near
                self.far      = far
                self.gridx    = GRIDX
                self.gridy    = GRIDY
                self.name = "OPENGLES2 depth range, near=%.2f, far=%.2f" % ( self.near,
                                                                             self.far, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh1.depthGradient( -1.0, 1.0 )

                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh2.depthGradient( 1.0, -1.0 )

                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_ALWAYS' )
                OpenGLES2.DepthRange( self.near, self.far )
                                
                useVertexData( modules, indexTracker, vertexDataSetup1, program )                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )

                OpenGLES2.DepthFunc( 'GL_LESS' )                
                useVertexData( modules, indexTracker, vertexDataSetup2, program )                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )        

######################################################################
class PolygonOffset( vt.VisualTestCase ):

        ''' The purpose of the test: test polygon offset.

        Expected output: the test draws two full screen meshes with a 4x4 vertex
        grid. The vertex depth values are interpolated between -0.2 in the
        lower-left and 0.2 in the top-right. Polygon offset fill and depth test
        are enabled. First, a blue mesh is drawn using depth func GL_ALWAYS with
        polygon offset parameters set as factor1 and unit1. Secondly, a red mesh
        is drawn with depth function set to GL_GREATER and with polygon offset
        parameters set as factor2 and unit2. Viewport size 320*240.
        '''
        testValues = { ( 'factor1', 'units1', 'factor2', 'units2' ) : [ ( 0.0, 0.0, 0.0, 0.0 ),
                                                                        ( 1.0, 0.0, 1.0, 0.0 ),
                                                                        ( 1.0, 16.0, 1.0, 0.0 ),
                                                                        ( 1.0, 1.0, 2.0, 1.0 ),
                                                 ]
                       }

        
        def __init__( self, factor1, units1, factor2, units2 ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.factor1   = factor1
                self.units1    = units1
                self.factor2   = factor2
                self.units2    = units2               
                self.gridx     = GRIDX
                self.gridy     = GRIDY
                self.name = "OPENGLES2 polygon offset, factor1=%.2f, units1=%.2f, factor2=%.2f, units2=%.2f" % ( self.factor1,
                                                                                                                 self.units1,
                                                                                                                 self.factor2,
                                                                                                                 self.units2, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh1.depthGradient( -0.2, 0.2 )
                
                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh2.depthGradient( -0.2, 0.2 )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )                
                program          = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Enable( 'GL_POLYGON_OFFSET_FILL' )
                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                
                OpenGLES2.DepthFunc( 'GL_ALWAYS' )
                OpenGLES2.PolygonOffset( self.factor1, self.units1 )
                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )
                
                OpenGLES2.DepthFunc( 'GL_GREATER' )
                OpenGLES2.PolygonOffset( self.factor2, self.units2 )
                
                useVertexData( modules, indexTracker, vertexDataSetup2, program )
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class PolygonOffsetUnit( vt.VisualTestCase ):

        ''' The purpose of the test: test polygon offset unit.

        Expected output: the test draws two full screen meshes with a 4x4 vertex
        grid. The vertex depth values are all 0.5 so depth gradient should be
        zero. Polygon offset fill and depth test are enabled. First, a blue mesh
        is drawn using depth func GL_ALWAYS with polygon offset parameters set
        as factor1 and unit1. Secondly, a red mesh is drawn with depth function
        set to GL_GREATER and with polygon offset parameters set as factor2 and
        unit2. Viewport size 320*240.
        '''
        testValues = { ( 'units1', 'units2' ) : [ ( 1.0, 1.0 ),
                                                  ( 1.0, 16.0 ),
                                                 ]
                       }

        
        def __init__( self, units1, units2 ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.units1    = units1
                self.units2    = units2               
                self.gridx     = GRIDX
                self.gridy     = GRIDY
                self.name = "OPENGLES2 polygon offset unit, units1=%.2f, units2=%.2f" % ( self.units1,
                                                                                          self.units2, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh1.depthGradient( 0.5, 0.5 )
                
                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh2.depthGradient( 0.5, 0.5 )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )                
                program          = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Enable( 'GL_POLYGON_OFFSET_FILL' )
                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                
                OpenGLES2.DepthFunc( 'GL_ALWAYS' )
                OpenGLES2.PolygonOffset( 1.0, self.units1 )
                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )
                
                OpenGLES2.DepthFunc( 'GL_GREATER' )
                OpenGLES2.PolygonOffset( 1.0, self.units2 )
                
                useVertexData( modules, indexTracker, vertexDataSetup2, program )
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
