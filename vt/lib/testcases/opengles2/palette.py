# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )

TYPES = { 'GL_PALETTE4_RGB8_OES'      : 'OPENGLES2_RGB888',
          'GL_PALETTE4_RGBA8_OES'     : 'OPENGLES2_RGBA8888',
          'GL_PALETTE4_R5_G6_B5_OES'  : 'OPENGLES2_RGB565',
          'GL_PALETTE4_RGBA4_OES'     : 'OPENGLES2_RGBA4444',
          'GL_PALETTE4_RGB5_A1_OES'   : 'OPENGLES2_RGBA5551',
          'GL_PALETTE8_RGB8_OES'      : 'OPENGLES2_RGB888',
          'GL_PALETTE8_RGBA8_OES'     : 'OPENGLES2_RGBA8888',
          'GL_PALETTE8_R5_G6_B5_OES'  : 'OPENGLES2_RGB565',
          'GL_PALETTE8_RGBA4_OES'     : 'OPENGLES2_RGBA4444',
          'GL_PALETTE8_RGB5_A1_OES'   : 'OPENGLES2_RGBA5551' }

import math

######################################################################
RAD = 360.0 / ( 2.0 * math.pi )

######################################################################
def mvMatrix( angle ):
        angle = angle /  RAD
        mv       = [ 0 ] * 16
        mv[ 0 ]  = +math.cos( angle )
        mv[ 2 ]  = +math.sin( angle )
        mv[ 5 ]  = 1.0
        mv[ 8 ]  = -math.sin( angle )
        mv[ 10 ] = +math.cos( angle )
        mv[ 14 ] = -5.0
        mv[ 15 ] = 1.0

        return mv

######################################################################
def pMatrix( aspectRatio ):
        mp       = [ 0 ] * 16
        mp[ 0 ]  = math.cos( 0.25 ) / math.sin( 0.25 )
        mp[ 5 ]  = mp[ 0 ] * aspectRatio
        mp[ 10 ] = - 10.0 / 9.0
        mp[ 11 ] = - 1.0
        mp[ 14 ] = - 10.0 / 9.0
    
        return mp

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
                                       
######################################################################
class PaletteTextureWrap( vt.VisualTestCase ):

        ''' The purpose of the test: test palette compressed texture
        wrapping. Requires GL_OES_compressed_paletted_texture extension.

        Expected output: a full screen plane with a 4x4 vertex grid. A palette
        compressed red-and-white 2x2 checker texture is drawn across the plane
        with texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). Viewport size 320*240.
        '''

        testValues = { ( 'type', 'wrapS', 'wrapT' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ]
                       }
        
        def __init__( self, type, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 palette compressed texture, type=%s, wrapS=%s, wrapT=%s" % ( paletteTypeToStr( self.type ),
                                                                                                    wrapToStr( self.wrapS ),
                                                                                                    wrapToStr( self.wrapT ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_compressed_paletted_texture' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                    [ 1.0, 0.0, 0.0, 1.0 ],
                                                    [ 1.0, 1.0, 1.0, 1.0 ],
                                                    2, 2,
                                                    textureW, textureH,
                                                    'OFF',
                                                    TYPES[ self.type ] )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                              textureDataIndex,
                                                              self.type )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT )

                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               


######################################################################
class NpotPaletteTextureWrap( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT palette compressed texture
        wrapping. Requires GL_OES_compressed_paletted_texture extension.

        Expected output: a full screen plane with a 4x4 vertex grid. A palette
        compressed red-and-white 4x4 checker texture is drawn across the plane
        with texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). Viewport size 320*240.
        '''

        testValues = { ( 'type', 'wrapS', 'wrapT' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ]
                       }
        
        def __init__( self, type, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT palette compressed texture, type=%s, wrapS=%s, wrapT=%s" % ( paletteTypeToStr( self.type ),
                                                                                                         wrapToStr( self.wrapS ),
                                                                                                         wrapToStr( self.wrapT ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_compressed_paletted_texture' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                    [ 1.0, 0.0, 0.0, 1.0 ],
                                                    [ 1.0, 1.0, 1.0, 1.0 ],
                                                    2, 2,
                                                    textureW, textureH,
                                                    'OFF',
                                                    TYPES[ self.type ] )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                              textureDataIndex,
                                                              self.type )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT )

                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class NpotPaletteTextureWrapExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT palette compressed texture
        wrapping. Requires GL_OES_compressed_paletted_texture and
        GL_OES_texture_npot extensions.

        Expected output: a full screen plane with a 4x4 vertex grid. A palette
        compressed red-and-white 4x4 checker texture is drawn across the plane
        with texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). Viewport size 320*240.
        '''

        testValues = { ( 'type', 'wrapS', 'wrapT' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ]
                       }
        
        def __init__( self, type, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT palette compressed texture ext, type=%s, wrapS=%s, wrapT=%s" % ( paletteTypeToStr( self.type ),
                                                                                                             wrapToStr( self.wrapS ),
                                                                                                             wrapToStr( self.wrapT ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_compressed_paletted_texture' )
                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )                
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                    [ 1.0, 0.0, 0.0, 1.0 ],
                                                    [ 1.0, 1.0, 1.0, 1.0 ],
                                                    2, 2,
                                                    textureW, textureH,
                                                    'OFF',
                                                    TYPES[ self.type ] )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                              textureDataIndex,
                                                              self.type )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT )

                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class PaletteTextureMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test palette compressed texture min
        filtering. Requires GL_OES_compressed_paletted_texture extension.

        Expected output: a full screen plane with a 4x4 vertex grid. A POT
        palette compressed red-and-white 2x2 checker texture is drawn across the
        plane with texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). Viewport size 320*240.
        '''

        testValues = { ( 'type', 'minFilter', 'wrap' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' )
                                                           ]
                       }
        
        def __init__( self, type, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 palette compressed texture min filter, type=%s, filter=%s, wrap=%s" % ( paletteTypeToStr( self.type ),
                                                                                                               filterToStr( self.minFilter ),
                                                                                                               wrapToStr( self.wrap ), )

        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_compressed_paletted_texture' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]

                level = 0
                w = textureW
                h = textureH
                for i in range( max( w, h ) ):
                        w /= 2
                        h /= 2
                        if w < 1 and h < 1:
                                break
                        level += 1                       
                        
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                    [ 1.0, 0.0, 0.0, 1.0 ],
                                                    [ 1.0, 1.0, 1.0, 1.0 ],
                                                    2, 2,
                                                    textureW, textureH,
                                                    'ON',
                                                    TYPES[ self.type ] )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                              textureDataIndex,
                                                              self.type )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )
                
                OpenGLES2.CompressedTexImage2DMML( compressedTextureDataIndex, 'GL_TEXTURE_2D', -level )
                
                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

                
######################################################################
class NpotPaletteTextureMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT palette compressed texture min
        filtering. Requires GL_OES_compressed_paletted_texture extension.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        palette compressed red-and-white 2x2 checker texture is drawn across the
        plane with texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). Viewport size 320*240.
        '''

        testValues = { ( 'type', 'minFilter', 'wrap' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ]
                       }
        
        def __init__( self, type, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT palette compressed texture min filter, type=%s, filter=%s, wrap=%s" % ( paletteTypeToStr( self.type ),
                                                                                                                    filterToStr( self.minFilter ),
                                                                                                                    wrapToStr( self.wrap ), )

        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_compressed_paletted_texture' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1

                level = 0
                w = textureW
                h = textureH
                for i in range( max( w, h ) ):
                        w /= 2
                        h /= 2
                        if w < 1 and h < 1:
                                break
                        level += 1                       
                        
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                    [ 1.0, 0.0, 0.0, 1.0 ],
                                                    [ 1.0, 1.0, 1.0, 1.0 ],
                                                    2, 2,
                                                    textureW, textureH,
                                                    'OFF',
                                                    TYPES[ self.type ] )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                              textureDataIndex,
                                                              self.type )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )
                
                OpenGLES2.CompressedTexImage2DMML( compressedTextureDataIndex, 'GL_TEXTURE_2D', -level )
                
                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                       
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class NpotPaletteTextureMinFilterExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT palette compressed texture min
        filtering. Requires GL_OES_compressed_paletted_texture and
        GL_OES_texture_npot extensions.

        Expected output: a full screen plane with a 4x4 vertex grid. An NPOT
        palette compressed red-and-white 2x2 checker texture is drawn across the
        plane with texture coordinates running from 0.0 to 2.5 (left-right,
        bottom-top). Viewport size 320*240.
        '''

        testValues = { ( 'type', 'minFilter', 'wrap' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' )
                                                           ]
                       }
        
        def __init__( self, type, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT palette compressed texture min filter ext, type=%s, filter=%s, wrap=%s" % ( paletteTypeToStr( self.type ),
                                                                                                                        filterToStr( self.minFilter ),
                                                                                                                        wrapToStr( self.wrap ), )

        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_compressed_paletted_texture' )
                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1

                level = 0
                w = textureW
                h = textureH
                for i in range( max( w, h ) ):
                        w /= 2
                        h /= 2
                        if w < 1 and h < 1:
                                break
                        level += 1                       
                        
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateCheckerTextureData( textureDataIndex,
                                                    [ 1.0, 0.0, 0.0, 1.0 ],
                                                    [ 1.0, 1.0, 1.0, 1.0 ],
                                                    2, 2,
                                                    textureW, textureH,
                                                    'ON',
                                                    TYPES[ self.type ] )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                              textureDataIndex,
                                                              self.type )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )
                
                OpenGLES2.CompressedTexImage2DMML( compressedTextureDataIndex, 'GL_TEXTURE_2D', -level )
                
                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                       
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class PaletteCubeMapWrap( vt.VisualTestCase ):

        ''' The purpose of the test: test palette compressed cube map
        wrapping. Requires GL_OES_compressed_paletted_texture extension.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Viewport size 320*240.
        '''

        testValues = { ( 'type', 'wrapS', 'wrapT' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ]
                       }
        
        def __init__( self, type, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 palette compressed cubemap, type=%s, wrapS=%s, wrapT=%s" % ( paletteTypeToStr( self.type ),
                                                                                                    wrapToStr( self.wrapS ),
                                                                                                    wrapToStr( self.wrapT ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_compressed_paletted_texture' )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                textureSize = min( textureW, textureH )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT )

                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE
               
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                                          colors[ i ],
                                                          textureSize, textureSize,
                                                          'OFF',
                                                          TYPES[ self.type ] )

                        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                        OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                                      textureDataIndex,
                                                                      self.type )

                        OpenGLES2.CompressedTexImage2D( compressedTextureDataIndex, faces[ i ] )
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                

######################################################################
class NpotPaletteCubeMapWrap( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT palette compressed cube map
        wrapping.  Requires GL_OES_compressed_paletted_texture extension.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Viewport size 320*240.
        '''

        testValues = { ( 'type', 'wrapS', 'wrapT' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                        ]
                       }
        
        def __init__( self, type, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT palette compressed cubemap, type=%s, wrapS=%s, wrapT=%s" % ( paletteTypeToStr( self.type ),
                                                                                                         wrapToStr( self.wrapS ),
                                                                                                         wrapToStr( self.wrapT ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_compressed_paletted_texture' )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                textureSize = min( textureW, textureH )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT )

                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE
               
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                                          colors[ i ],
                                                          textureSize, textureSize,
                                                          'OFF',
                                                          TYPES[ self.type ] )

                        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                        OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                                      textureDataIndex,
                                                                      self.type )

                        OpenGLES2.CompressedTexImage2D( compressedTextureDataIndex, faces[ i ] )
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class NpotPaletteCubeMapWrapExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT palette compressed cube map
        wrapping.  Requires GL_OES_compressed_paletted_texture and
        GL_OES_texture_npot extensions.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Viewport size 320*240.
        '''

        testValues = { ( 'type', 'wrapS', 'wrapT' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA8_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGBA4_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                        ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                        ]
                       }
        
        def __init__( self, type, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.type        = type
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 NPOT palette compressed cubemap ext, type=%s, wrapS=%s, wrapT=%s" % ( paletteTypeToStr( self.type ),
                                                                                                             wrapToStr( self.wrapS ),
                                                                                                             wrapToStr( self.wrapT ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_compressed_paletted_texture' )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                textureSize = min( textureW, textureH )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT )

                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE
               
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                                          colors[ i ],
                                                          textureSize, textureSize,
                                                          'OFF',
                                                          TYPES[ self.type ] )

                        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                        OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                                      textureDataIndex,
                                                                      self.type )

                        OpenGLES2.CompressedTexImage2D( compressedTextureDataIndex, faces[ i ] )
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class PaletteCubeMapMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test palette compressed cube map min
        filtering.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Viewport size 320*240.
        '''
        
        testValues = { ( 'type', 'minFilter', 'wrap' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' )
                                                           ]
                       }

        def __init__( self, type, minFilter, wrap ):
                vt.VisualTestCase.__init__( self)
                self.type      = type
                self.minFilter = minFilter
                self.wrap      = wrap
                self.name = "OPENGLES2 palette compressed cube mapped sphere min filter, type=%s, minFilter=%s, wrap=%s" % ( paletteTypeToStr( self.type ),
                                                                                                                             filterToStr( self.minFilter ),
                                                                                                                             wrapToStr( self.wrap ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_compressed_paletted_texture' )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                textureSize = min( textureW, textureH )
                
                level = 0
                ts = textureSize
                for i in range( ts ):
                        ts /= 2
                        if ts < 1:
                                break
                        level += 1                       
                                        
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )

                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE
               
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                                          colors[ i ],
                                                          textureSize, textureSize,
                                                          'ON',
                                                          TYPES[ self.type ] )

                        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                        OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                                      textureDataIndex,
                                                                      self.type )

                        OpenGLES2.CompressedTexImage2DMML( compressedTextureDataIndex, faces[ i ], -level )
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                              

######################################################################
class NpotPaletteCubeMapMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT palette compressed cube map min
        filtering. Requires GL_OES_compressed_paletted_texture extension.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Viewport size 320*240.
        '''
        testValues = { ( 'type', 'minFilter', 'wrap' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ]
                       }
        
        def __init__( self, type, minFilter, wrap ):
                vt.VisualTestCase.__init__( self)
                self.type      = type
                self.minFilter = minFilter
                self.wrap      = wrap
                self.name = "OPENGLES2 NPOT palette compressed cube mapped sphere min filter, type=%s, minFilter=%s, wrap=%s" % ( paletteTypeToStr( self.type ),
                                                                                                                                  filterToStr( self.minFilter ),
                                                                                                                                  wrapToStr( self.wrap ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                textureSize = min( textureW, textureH )
                
                level = 0
                ts = textureSize
                for i in range( textureSize ):
                        ts /= 2
                        if ts < 1:
                                break
                        level += 1                       
                                        
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )

                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE
               
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                                          colors[ i ],
                                                          textureSize, textureSize,                                                            
                                                          'OFF',
                                                          TYPES[ self.type ] )

                        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                        OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                                      textureDataIndex,
                                                                      self.type )

                        OpenGLES2.CompressedTexImage2DMML( compressedTextureDataIndex, faces[ i ], -level )
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class NpotPaletteCubeMapMinFilterExt( vt.VisualTestCase ):

        ''' The purpose of the test: test NPOT palette compressed cube map min
        filtering. Requires GL_OES_compressed_paletted_texture and
        GL_OES_texture_npot extensions.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. With luminance textures
        the sphere colors should be center=white, left=black, up=white,
        right=white, down=black, border=white. Viewport size 320*240.
        '''
        
        testValues = { ( 'type', 'minFilter', 'wrap' ) : [ ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE4_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA8_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_R5_G6_B5_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGBA4_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                           ( 'GL_PALETTE8_RGB5_A1_OES', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' )
                                                           ]
                       }       

        def __init__( self, type, minFilter, wrap ):
                vt.VisualTestCase.__init__( self)
                self.type      = type
                self.minFilter = minFilter
                self.wrap      = wrap
                self.name = "OPENGLES2 NPOT palette compressed cube mapped sphere min filter ext, type=%s, minFilter=%s, wrap=%s" % ( paletteTypeToStr( self.type ),
                                                                                                                                      filterToStr( self.minFilter ),
                                                                                                                                      wrapToStr( self.wrap ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_compressed_paletted_texture' )
                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )

                textureW = toPot( WIDTH, True )[ 0 ] - 1
                textureH = toPot( HEIGHT, True )[ 0 ] - 1
                textureSize = min( textureW, textureH )
                
                level = 0
                ts = textureSize
                for i in range( textureSize ):
                        ts /= 2
                        if ts < 1:
                                break
                        level += 1                       
                                        
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )

                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE
               
                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                                          colors[ i ],
                                                          textureSize, textureSize,                                                            
                                                          'ON',
                                                          TYPES[ self.type ] )

                        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                        OpenGLES2.CreatePaletteCompressedTextureData( compressedTextureDataIndex,
                                                                      textureDataIndex,
                                                                      self.type )

                        OpenGLES2.CompressedTexImage2DMML( compressedTextureDataIndex, faces[ i ], -level )
                        
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
