# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

import math

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )

######################################################################
def createPlane( gridx, gridy ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, gradient, c1, c2, textureType, minF, magF ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
    
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                             gradient,
                                             c1,
                                             c2,
                                             w, h,
                                             'OFF',
                                             textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                minF,
                                magF,
                                'GL_REPEAT',
                                'GL_REPEAT' )

        return TextureSetup( textureDataIndex, textureIndex )

######################################################################
class FramebufferWithDepth( vt.VisualTestCase ):

        ''' The purpose of the test: test framebuffer with depth.

        Expected output: first the test draws a full screen mesh with a 4x4
        vertex grid. The vertex colors are defined so that the lower-left vertex
        has color [0.0,0.0,1.0,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. In between, the vertex color is linearly interpolated
        from left to right, from bottom to top. The vertex depth values are
        defined -1.0 and 1.0 in a similar fashion. The first mesh is drawn using
        GL_ALWAYS depth func, after which the depth function is set
        GL_LESS. Secondly, the test draws a full screen mesh with a 4x4 vertex
        grid. The vertex colors are defined [1.0,1.0,1.0,1.0] (lower-left) and
        [1.0,0.0,0.0,1.0] (upper-right). In between, the vertex color is
        linearly interpolated from left to right, from bottom to top. The vertex
        depth values are defined 1.0 and 0.0 in a similar fashion. Viewport size
        320*240.
        '''
        testValues = { ( 'color0Format', 'depthFormat' ) : [ ( 'GL_RGBA4', 'GL_DEPTH_COMPONENT16' ),
                                                             ( 'GL_RGB5_A1', 'GL_DEPTH_COMPONENT16' ),
                                                             ( 'GL_RGB565', 'GL_DEPTH_COMPONENT16' ),
                                                             ]
                       }
        
        def __init__( self, color0Format, depthFormat ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.color0Format  = color0Format
                self.depthFormat   = depthFormat
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.overdraw      = 1
                self.name = "OPENGLES2 basic framebuffer, color0=%s, depth=%s" % ( self.color0Format[ 3: ],
                                                                                   self.depthFormat[ 3: ], )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      1, 1,
                                      apis,
                                      defaultAttributes )

                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.color0Format,
                                               WIDTH, HEIGHT )
                
                depthRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( depthRenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            depthRenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.depthFormat,
                                               WIDTH, HEIGHT )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_DEPTH_ATTACHMENT',
                                                   'GL_RENDERBUFFER',
                                                   depthRenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                
                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh1.depthGradient( -1.0, 1.0 )
                
                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh2.depthGradient( 1.0, -1.0 )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                
                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_ALWAYS' )                                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )
                
                OpenGLES2.DepthFunc( 'GL_LESS' )

                useVertexData( modules, indexTracker, vertexDataSetup2, program )
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class FramebufferWithStencil( vt.VisualTestCase ):

        ''' The purpose of the test: test framebuffer with stencil.

        Expected output: a blue rectangle in the middle of a red
        rectangle. First, the buffers are cleared, color to [0,0,0,1] and
        stencil to 1. Secondly a rectangle at the center of stencil buffer is
        cleared to 2 using scissoring. Stencil function is set as GL_EQUAL, 2
        0xFF, stencil operation is set as GL_INCR, GL_KEEP, GL_INCR. Stencil and
        depth tests are enabled, depth test is set GL_LEQUAL so drawing always
        passes. Finally, the test first draws a blue full screen rect, followed
        by a red full screen screen rect. Viewport size 320*240.
        '''
        testValues = { ( 'color0Format', 'stencilFormat' ) : [ ( 'GL_RGBA4', 'GL_STENCIL_INDEX8' ),
                                                               ( 'GL_RGB5_A1', 'GL_STENCIL_INDEX8' ),
                                                               ( 'GL_RGB565', 'GL_STENCIL_INDEX8' ),
                                                               ]
                       }
        
        def __init__( self, color0Format, stencilFormat ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.color0Format  = color0Format
                self.stencilFormat = stencilFormat
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.overdraw      = 1
                self.name = "OPENGLES2 framebuffer with stencil, color0=%s, stencil=%s" % ( self.color0Format[ 3: ],
                                                                                            self.stencilFormat[ 3: ], )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      1, 1,
                                      apis,
                                      defaultAttributes )

                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.color0Format,
                                               WIDTH, HEIGHT )
                
                stencilRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( stencilRenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            stencilRenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.stencilFormat,
                                               WIDTH, HEIGHT )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_STENCIL_ATTACHMENT',
                                                   'GL_RENDERBUFFER',
                                                   stencilRenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                
                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                OpenGLES2.ClearStencil( 1 )

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_STENCIL_BUFFER_BIT' ] )
                
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2
                
                OpenGLES2.Scissor( x, y, w, h )
                OpenGLES2.Enable( 'GL_SCISSOR_TEST' )

                OpenGLES2.ClearStencil( 2 )
                OpenGLES2.Clear( [ 'GL_STENCIL_BUFFER_BIT' ] )
                OpenGLES2.Disable( 'GL_SCISSOR_TEST' )

                OpenGLES2.StencilFunc( 'GL_EQUAL', 2, '0xFF' )
                OpenGLES2.StencilOp( 'GL_INCR', 'GL_KEEP', 'GL_INCR' )
                OpenGLES2.Enable( 'GL_STENCIL_TEST' )

                OpenGLES2.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )
                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                
                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.7 ] )
                drawVertexData( modules, vertexDataSetup1 )

                useVertexData( modules, indexTracker, vertexDataSetup2, program )               
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.5 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class FramebufferWithDepthAndStencil( vt.VisualTestCase ):

        ''' The purpose of the test: test framebuffer with depth and stencil.

        Expected output: a blue rectangle in the middle of a red
        rectangle. First, the buffers are cleared, color to [0,0,0,1] and
        stencil to 1. Secondly a rectangle at the center of stencil buffer is
        cleared to 2 using scissoring. Stencil function is set as GL_EQUAL, 2
        0xFF, stencil operation is set as GL_INCR, GL_KEEP, GL_INCR. Stencil and
        depth tests are enabled, depth test is set GL_LEQUAL so drawing always
        passes. Finally, the test first draws a blue full screen rect, followed
        by a red full screen screen rect. Viewport size 320*240.
        '''
        testValues = { ( 'color0Format', 'depthFormat', 'stencilFormat' ) : [ ( 'GL_RGBA4', 'GL_DEPTH_COMPONENT16', 'GL_STENCIL_INDEX8' ),
                                                                              ( 'GL_RGB5_A1', 'GL_DEPTH_COMPONENT16', 'GL_STENCIL_INDEX8' ),
                                                                              ( 'GL_RGB565', 'GL_DEPTH_COMPONENT16', 'GL_STENCIL_INDEX8' ),
                                                                              ]
                       }
        
        def __init__( self, color0Format, depthFormat, stencilFormat ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.color0Format  = color0Format
                self.depthFormat   = depthFormat
                self.stencilFormat = stencilFormat
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.overdraw      = 1
                self.name = "OPENGLES2 framebuffer with depth and stencil, color0=%s, depth=%s, stencil=%s" % ( self.color0Format[ 3: ],
                                                                                                                self.depthFormat[ 3: ],
                                                                                                                self.stencilFormat[ 3: ], )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      1, 1,
                                      apis,
                                      defaultAttributes )

                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.color0Format,
                                               WIDTH, HEIGHT )
                
                depthRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( depthRenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            depthRenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.depthFormat,
                                               WIDTH, HEIGHT )

                stencilRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( stencilRenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            stencilRenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.stencilFormat,
                                               WIDTH, HEIGHT )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_DEPTH_ATTACHMENT',
                                                   'GL_RENDERBUFFER',
                                                   depthRenderbufferIndex )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_STENCIL_ATTACHMENT',
                                                   'GL_RENDERBUFFER',
                                                   stencilRenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                OpenGLES2.ClearStencil( 1 )

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_STENCIL_BUFFER_BIT' ] )
                
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2
                
                OpenGLES2.Scissor( x, y, w, h )
                OpenGLES2.Enable( 'GL_SCISSOR_TEST' )

                OpenGLES2.ClearStencil( 2 )
                OpenGLES2.Clear( [ 'GL_STENCIL_BUFFER_BIT' ] )
                OpenGLES2.Disable( 'GL_SCISSOR_TEST' )

                OpenGLES2.StencilFunc( 'GL_EQUAL', 2, '0xFF' )
                OpenGLES2.StencilOp( 'GL_INCR', 'GL_KEEP', 'GL_INCR' )
                OpenGLES2.Enable( 'GL_STENCIL_TEST' )

                OpenGLES2.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )
                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.7 ] )
                drawVertexData( modules, vertexDataSetup1 )

                useVertexData( modules, indexTracker, vertexDataSetup2, program )               
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.5 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
def createPlane2( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, gradient, c1, c2, textureType, minF, magF ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
    
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                             gradient,
                                             c1,
                                             c2,
                                             w, h,
                                             'OFF',
                                             textureType )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                minF,
                                magF,
                                'GL_REPEAT',
                                'GL_REPEAT' )

        return TextureSetup( textureDataIndex, textureIndex )
                
######################################################################
class FramebufferTexture2DColor( vt.VisualTestCase ):

        ''' The purpose of the test: test using texture as framebuffer color
        attachment.

        Expected output: full screen textured rectangle. The texture comprises
        four rectangles, red in bottom-left, green in bottom-right, blue in
        top-left, and white in top-right. The texture data is drawn using
        texture as framebuffer color attachment. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'pot' ) : [ ( 'OPENGLES2_RGB565', True ),
                                                    ( 'OPENGLES2_RGB565', False ),
                                                    ( 'OPENGLES2_RGB888', True ),
                                                    ( 'OPENGLES2_RGB888', False ),
                                                    ( 'OPENGLES2_RGBA4444', True ),
                                                    ( 'OPENGLES2_RGBA4444', False ),                                             
                                                    ( 'OPENGLES2_RGBA5551', True ),
                                                    ( 'OPENGLES2_RGBA5551', False ),                                             
                                                    ( 'OPENGLES2_RGBA8888', True ),
                                                    ( 'OPENGLES2_RGBA8888', False ),                                             
                                                    ]
                       }
        
        def __init__( self, textureType, pot ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.pot         = pot
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 FramebufferTexture2D color, texture type=%s, pot=%s" % ( textureTypeToStr( self.textureType ),
                                                                                                boolToStr( self.pot ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]

                if not self.pot:
                        textureW -= 1
                        textureH -= 1

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                        
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                 textureW, textureH,
                                                 'OFF',
                                                 self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D',
                                       textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex,
                                      'GL_TEXTURE_2D' )

                OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                'GL_COLOR_ATTACHMENT0',
                                                'GL_TEXTURE_2D',
                                                textureIndex,
                                                0 )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                OpenGLES2.Viewport( 0, 0, textureW, textureH );
                
                w = textureW / 2
                h = textureH / 2
                x = ( textureW - w  ) / 2
                y = ( textureH - h ) / 2 

                OpenGLES2.Enable( 'GL_SCISSOR_TEST' )                
                OpenGLES2.Scissor( 0, 0, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Scissor( textureW / 2, 0, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Scissor( 0, textureH /2, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                
                OpenGLES2.Scissor( textureW / 2, textureH / 2, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 1.0, 1.0, 1.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Disable( 'GL_SCISSOR_TEST' )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                
                mesh              = createPlane2( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.ActiveTexture( 0 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )
                
                OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0 ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class FramebufferTexture2DColorNpot( vt.VisualTestCase ):

        ''' The purpose of the test: test using npot texture as framebuffer
        color attachment, with GL_CLAMP_TO_EDGE wrapping mode.

        Expected output: full screen textured rectangle with texture coordinates
        running from 0 to 1.0 with GL_CLAMP_TO_EDGE wrapping mode. The texture
        comprises four rectangles, red in bottom-left, green in bottom-right,
        blue in top-left, and white in top-right. The texture data is drawn
        using texture as framebuffer color attachment. Viewport size 320*240.
        '''

        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_RGB565' ),
                                             ( 'OPENGLES2_RGB888' ),
                                             ( 'OPENGLES2_RGBA4444' ),
                                             ( 'OPENGLES2_RGBA5551' ),
                                             ( 'OPENGLES2_RGBA8888' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 FramebufferTexture2D color NPOT texture, texture type=%s" % ( textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )
               
                textureW = toPot( WIDTH, True )[ 0 ] - 1 
                textureH = toPot( HEIGHT, True )[ 0 ] - 1

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                 textureW, textureH,
                                                 'OFF',
                                                 self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D',
                                       textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex,
                                      'GL_TEXTURE_2D' )

                OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                'GL_COLOR_ATTACHMENT0',
                                                'GL_TEXTURE_2D',
                                                textureIndex,
                                                0 )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                OpenGLES2.Viewport( 0, 0, textureW, textureH );
                
                w = textureW / 2
                h = textureH / 2
                x = ( textureW - w  ) / 2
                y = ( textureH - h ) / 2 

                OpenGLES2.Enable( 'GL_SCISSOR_TEST' )                
                OpenGLES2.Scissor( 0, 0, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Scissor( textureW / 2, 0, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Scissor( 0, textureH /2, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                
                OpenGLES2.Scissor( textureW / 2, textureH / 2, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 1.0, 1.0, 1.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Disable( 'GL_SCISSOR_TEST' )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                
                mesh              = createPlane2( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.ActiveTexture( 0 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )
                
                OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0 ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class FramebufferTexture2DColorNpotExt( vt.VisualTestCase ):

        ''' The purpose of the test: test using npot texture as framebuffer
        color attachment, with GL_MIRRORED_REPEAT wrapping mode. Requires
        GL_OES_texture_npot extension.

        Expected output: full screen textured rectangle with texture coordinates
        running from 0 to 2.5 with GL_MIRRORED_REPEAT wrapping mode. The texture
        comprises four rectangles, red in bottom-left, green in bottom-right,
        blue in top-left, and white in top-right. The texture data is drawn
        using texture as framebuffer color attachment. Viewport size 320*240.
        '''

        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_RGB565' ),
                                             ( 'OPENGLES2_RGB888' ),
                                             ( 'OPENGLES2_RGBA4444' ),
                                             ( 'OPENGLES2_RGBA5551' ),
                                             ( 'OPENGLES2_RGBA8888' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 FramebufferTexture2D color NPOT texture ext, texture type=%s" % ( textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
                
                textureW = toPot( WIDTH, True )[ 0 ] - 1 
                textureH = toPot( HEIGHT, True )[ 0 ] - 1

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )

                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                 textureW, textureH,
                                                 'OFF',
                                                 self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D',
                                       textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex,
                                      'GL_TEXTURE_2D' )

                OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                'GL_COLOR_ATTACHMENT0',
                                                'GL_TEXTURE_2D',
                                                textureIndex,
                                                0 )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                OpenGLES2.Viewport( 0, 0, textureW, textureH );
                
                w = textureW / 2
                h = textureH / 2
                x = ( textureW - w  ) / 2
                y = ( textureH - h ) / 2 

                OpenGLES2.Enable( 'GL_SCISSOR_TEST' )                
                OpenGLES2.Scissor( 0, 0, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Scissor( textureW / 2, 0, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Scissor( 0, textureH /2, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 0.0, 0.0, 1.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                
                OpenGLES2.Scissor( textureW / 2, textureH / 2, textureW / 2, textureH / 2 )
                OpenGLES2.ClearColor( [ 1.0, 1.0, 1.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Disable( 'GL_SCISSOR_TEST' )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                
                mesh              = createPlane2( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.ActiveTexture( 0 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_MIRRORED_REPEAT',
                                        'GL_MIRRORED_REPEAT' )
                
                OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0 ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class FramebufferTexture2DWithDepth( vt.VisualTestCase ):

        ''' The purpose of the test: test framebuffer texture2D color with
        depth.

        Expected output: first the test draws a full screen mesh with a 4x4
        vertex grid to the texture. The vertex colors are defined so that the
        lower-left vertex has color [0.0,0.0,1.0,1.0] and the upper-right vertex
        has color [1.0,1.0,1.0,1.0]. In between, the vertex color is linearly
        interpolated from left to right, from bottom to top. The vertex depth
        values are defined -1.0 and 1.0 in a similar fashion. The first mesh is
        drawn using GL_ALWAYS depth func, after which the depth function is set
        GL_LESS. Secondly, the test draws a full screen mesh with a 4x4 vertex
        grid to the texture. The vertex colors are defined [1.0,1.0,1.0,1.0]
        (lower-left) and [1.0,0.0,0.0,1.0] (upper-right). In between, the vertex
        color is linearly interpolated from left to right, from bottom to
        top. The vertex depth values are defined 1.0 and 0.0 in a similar
        fashion. Finally, the test draws the texture. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'depthFormat' ) : [ ( 'OPENGLES2_RGB565', 'GL_DEPTH_COMPONENT16' ),
                                                            ( 'OPENGLES2_RGB888', 'GL_DEPTH_COMPONENT16' ),
                                                            ( 'OPENGLES2_RGBA4444', 'GL_DEPTH_COMPONENT16' ),
                                                            ( 'OPENGLES2_RGBA5551', 'GL_DEPTH_COMPONENT16' ),
                                                            ( 'OPENGLES2_RGBA8888', 'GL_DEPTH_COMPONENT16' ),
                                                            ]
                       }

        def __init__( self, textureType, depthFormat ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.textureType   = textureType
                self.depthFormat   = depthFormat
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.overdraw      = 1
                self.name = "OPENGLES2 FramebufferTexture2D color with depth, texture type=%s depth=%s" % ( textureTypeToStr( self.textureType ),
                                                                                                            self.depthFormat[ 3: ], )

        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                
                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                blitVertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                blitFragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                blitVsSource = formatShader( blitVertexShaderSource )
                blitFsSource = formatShader( blitFragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      1, 1,
                                      apis,
                                      defaultAttributes )

                blitMesh            = createPlane2( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )                
                blitVertexDataSetup = setupVertexData( modules, indexTracker, blitMesh )
                blitProgram         = createProgram( modules, indexTracker, blitVsSource, blitFsSource, blitVertexDataSetup )
                
                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                 textureW, textureH,
                                                 'OFF',
                                                 self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D',
                                       textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex,
                                      'GL_TEXTURE_2D' )

                OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                'GL_COLOR_ATTACHMENT0',
                                                'GL_TEXTURE_2D',
                                                textureIndex,
                                                0 )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )
                
                depthRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( depthRenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            depthRenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.depthFormat,
                                               textureW, textureH )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_DEPTH_ATTACHMENT',
                                                   'GL_RENDERBUFFER',
                                                   depthRenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                OpenGLES2.Viewport( 0, 0, textureW, textureH );
                
                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh1.depthGradient( -1.0, 1.0 )
                
                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh2.depthGradient( 1.0, -1.0 )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                
                OpenGLES2.Disable( 'GL_DITHER' )                
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_ALWAYS' )                                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )
                
                OpenGLES2.DepthFunc( 'GL_LESS' )

                useVertexData( modules, indexTracker, vertexDataSetup2, program )
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT )

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                 
                useVertexData( modules, indexTracker, blitVertexDataSetup, blitProgram )                
                OpenGLES2.Uniformi( blitProgram.textureLocationIndices[ 0 ], 1, [ 0 ] )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )                
                
                OpenGLES2.Disable( 'GL_DEPTH_TEST' )

                drawVertexData( modules, blitVertexDataSetup )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class FramebufferTexture2DWithDepth( vt.VisualTestCase ):

        ''' The purpose of the test: test framebuffer texture2D color attachment
        with depth attachment.

        Expected output: first the test draws a full screen mesh with a 4x4
        vertex grid to the texture. The vertex colors are defined so that the
        lower-left vertex has color [0.0,0.0,1.0,1.0] and the upper-right vertex
        has color [1.0,1.0,1.0,1.0]. In between, the vertex color is linearly
        interpolated from left to right, from bottom to top. The vertex depth
        values are defined -1.0 and 1.0 in a similar fashion. The first mesh is
        drawn using GL_ALWAYS depth func, after which the depth function is set
        GL_LESS. Secondly, the test draws a full screen mesh with a 4x4 vertex
        grid to the texture. The vertex colors are defined [1.0,1.0,1.0,1.0]
        (lower-left) and [1.0,0.0,0.0,1.0] (upper-right). In between, the vertex
        color is linearly interpolated from left to right, from bottom to
        top. The vertex depth values are defined 1.0 and 0.0 in a similar
        fashion. Finally, the test draws the texture. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'depthFormat' ) : [ ( 'OPENGLES2_RGB565', 'GL_DEPTH_COMPONENT16' ),
                                                            ( 'OPENGLES2_RGB888', 'GL_DEPTH_COMPONENT16' ),
                                                            ( 'OPENGLES2_RGBA4444', 'GL_DEPTH_COMPONENT16' ),
                                                            ( 'OPENGLES2_RGBA5551', 'GL_DEPTH_COMPONENT16' ),
                                                            ( 'OPENGLES2_RGBA8888', 'GL_DEPTH_COMPONENT16' ),
                                                            ]
                       }

        def __init__( self, textureType, depthFormat ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.textureType   = textureType
                self.depthFormat   = depthFormat
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.overdraw      = 1
                self.name = "OPENGLES2 FramebufferTexture2D color with depth, texture type=%s depth=%s" % ( textureTypeToStr( self.textureType ),
                                                                                                            self.depthFormat[ 3: ], )

        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                
                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                blitVertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                blitFragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                blitVsSource = formatShader( blitVertexShaderSource )
                blitFsSource = formatShader( blitFragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      1, 1,
                                      apis,
                                      defaultAttributes )

                blitMesh            = createPlane2( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )                
                blitVertexDataSetup = setupVertexData( modules, indexTracker, blitMesh )
                blitProgram         = createProgram( modules, indexTracker, blitVsSource, blitFsSource, blitVertexDataSetup )
                
                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                 textureW, textureH,
                                                 'OFF',
                                                 self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D',
                                       textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex,
                                      'GL_TEXTURE_2D' )

                OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                'GL_COLOR_ATTACHMENT0',
                                                'GL_TEXTURE_2D',
                                                textureIndex,
                                                0 )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )
                
                depthRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( depthRenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            depthRenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.depthFormat,
                                               textureW, textureH )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_DEPTH_ATTACHMENT',
                                                   'GL_RENDERBUFFER',
                                                   depthRenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                OpenGLES2.Viewport( 0, 0, textureW, textureH );
                
                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh1.depthGradient( -1.0, 1.0 )
                
                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh2.depthGradient( 1.0, -1.0 )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                
                OpenGLES2.Disable( 'GL_DITHER' )                
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_ALWAYS' )                                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )
                
                OpenGLES2.DepthFunc( 'GL_LESS' )

                useVertexData( modules, indexTracker, vertexDataSetup2, program )
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT )

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                 
                useVertexData( modules, indexTracker, blitVertexDataSetup, blitProgram )                
                OpenGLES2.Uniformi( blitProgram.textureLocationIndices[ 0 ], 1, [ 0 ] )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )                
                
                OpenGLES2.Disable( 'GL_DEPTH_TEST' )

                drawVertexData( modules, blitVertexDataSetup )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class FramebufferTexture2DWithOES_depth( vt.VisualTestCase ):

        ''' The purpose of the test: test framebuffer texture2D color attachment
        with OES_depth. Requires GL_OES_depth24 or GL_OES_depth32 extensions.

        Expected output: first the test draws a full screen mesh with a 4x4
        vertex grid to the texture. The vertex colors are defined so that the
        lower-left vertex has color [0.0,0.0,1.0,1.0] and the upper-right vertex
        has color [1.0,1.0,1.0,1.0]. In between, the vertex color is linearly
        interpolated from left to right, from bottom to top. The vertex depth
        values are defined -1.0 and 1.0 in a similar fashion. The first mesh is
        drawn using GL_ALWAYS depth func, after which the depth function is set
        GL_LESS. Secondly, the test draws a full screen mesh with a 4x4 vertex
        grid to the texture. The vertex colors are defined [1.0,1.0,1.0,1.0]
        (lower-left) and [1.0,0.0,0.0,1.0] (upper-right). In between, the vertex
        color is linearly interpolated from left to right, from bottom to
        top. The vertex depth values are defined 1.0 and 0.0 in a similar
        fashion. Finally, the test draws the texture. Viewport size 320*240.
        '''

        testValues = { ( 'textureType', 'depthFormat' ) : [ ( 'OPENGLES2_RGBA8888', 'GL_DEPTH_COMPONENT24_OES' ),
                                                            ( 'OPENGLES2_RGBA8888', 'GL_DEPTH_COMPONENT32_OES' ),
                                                            ]
                       }
        
        def __init__( self, textureType, depthFormat ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.textureType   = textureType
                self.depthFormat   = depthFormat
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.overdraw      = 1
                self.name = "OPENGLES2 FramebufferTexture2D color with depth ext, texture type=%s depth=%s" % ( textureTypeToStr( self.textureType ),
                                                                                                                self.depthFormat[ 3: ], )

        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                
                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                blitVertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                blitFragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                blitVsSource = formatShader( blitVertexShaderSource )
                blitFsSource = formatShader( blitFragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      1, 1,
                                      apis,
                                      defaultAttributes )

                extensions = { 'GL_DEPTH_COMPONENT24_OES' : 'GL_OES_depth24',
                               'GL_DEPTH_COMPONENT32_OES' : 'GL_OES_depth32' }
                
                OpenGLES2.CheckExtension( extensions[ self.depthFormat ] )
                
                blitMesh            = createPlane2( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )                
                blitVertexDataSetup = setupVertexData( modules, indexTracker, blitMesh )
                blitProgram         = createProgram( modules, indexTracker, blitVsSource, blitFsSource, blitVertexDataSetup )
                
                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                 textureW, textureH,
                                                 'OFF',
                                                 self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D',
                                       textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex,
                                      'GL_TEXTURE_2D' )

                OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                'GL_COLOR_ATTACHMENT0',
                                                'GL_TEXTURE_2D',
                                                textureIndex,
                                                0 )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )
                
                depthRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( depthRenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            depthRenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.depthFormat,
                                               textureW, textureH )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_DEPTH_ATTACHMENT',
                                                   'GL_RENDERBUFFER',
                                                   depthRenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                OpenGLES2.Viewport( 0, 0, textureW, textureH );
                
                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh1.depthGradient( -1.0, 1.0 )
                
                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh2.depthGradient( 1.0, -1.0 )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                
                OpenGLES2.Disable( 'GL_DITHER' )                
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_ALWAYS' )                                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )
                
                OpenGLES2.DepthFunc( 'GL_LESS' )

                useVertexData( modules, indexTracker, vertexDataSetup2, program )
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT )

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
                 
                useVertexData( modules, indexTracker, blitVertexDataSetup, blitProgram )                
                OpenGLES2.Uniformi( blitProgram.textureLocationIndices[ 0 ], 1, [ 0 ] )

                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )                
                
                OpenGLES2.Disable( 'GL_DEPTH_TEST' )

                drawVertexData( modules, blitVertexDataSetup )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class FramebufferTexture2DDepthExt( vt.VisualTestCase ):

        ''' The purpose of the test: test using depth texture as framebuffer
        depth attachment. Requires GL_OES_depth_texture extension.

        Expected output: surface divided in four rectangles, black in
        bottom-left, dark grey in bottom-right, light grey in top-left, and
        white in top-right. Viewport size 320*240.
        '''

        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_DEPTH' ),
                                             ( 'OPENGLES2_DEPTH32' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 FramebufferTexture2D depth ext, texture type=%s" % ( textureTypeToStr( self.textureType ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_depth_texture' )

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                 textureW, textureH,
                                                 'OFF',
                                                 self.textureType )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D',
                                       textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex,
                                      'GL_TEXTURE_2D' )
                                
                OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                'GL_DEPTH_ATTACHMENT',
                                                'GL_TEXTURE_2D',
                                                textureIndex,
                                                0 )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                OpenGLES2.Viewport( 0, 0, textureW, textureH );
                
                w = textureW / 2
                h = textureH / 2
                x = ( textureW - w ) / 2
                y = ( textureW - h ) / 2 

                OpenGLES2.Enable( 'GL_SCISSOR_TEST' )                
                OpenGLES2.Scissor( 0, 0, textureW / 2, textureH / 2 )
                OpenGLES2.ClearDepth( 0.0 )
                OpenGLES2.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Scissor( textureW / 2, 0, textureW / 2, textureH / 2 )
                OpenGLES2.ClearDepth( 0.33 )
                OpenGLES2.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Scissor( 0, textureH /2, textureW / 2, textureH / 2 )
                OpenGLES2.ClearDepth( 0.66 )
                OpenGLES2.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )
                
                OpenGLES2.Scissor( textureW / 2, textureH / 2, textureW / 2, textureH / 2 )
                OpenGLES2.ClearDepth( 1.0 )
                OpenGLES2.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Disable( 'GL_SCISSOR_TEST' )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                
                mesh              = createPlane2( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.ActiveTexture( 0 )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )
                
                OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0 ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

                
######################################################################
class FramebufferTexture2DDepthCubemapExt( vt.VisualTestCase ):

        ''' The purpose of the test: test using cubemap as framebuffer depth
        attachment. Requires GL_OES_depth_texture extension.

        Expected output: a gray-shaded sphere on a gray background. Center tile
        should have the lightest gray, tile on the right should have black
        color. Top tile should have lighter gray than bottom tile. Viewport size
        320*240.
        '''
        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_DEPTH' ),
                                             ( 'OPENGLES2_DEPTH32' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.name = "OPENGLES2 FramebufferTexture2D cubemap depth ext, texture type=%s" % ( textureTypeToStr( self.textureType ), )
                
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_depth_texture' )

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )

                depths = [ 0.0,     ## positive x
                           0.20,    ## negative x
                           0.40,    ## positive y
                           0.60,    ## negative y
                           0.80,    ## positive z
                           1.0      ## negative z, WHITE
                           ]

                textureW = 64
                textureH = 64
                textureSize = max( textureW, textureH )
                                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                         textureSize,
                                                         textureSize,
                                                         'OFF',
                                                         self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                                                
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', -1 )
                        
                for i in range( 6 ):
                        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                        'GL_DEPTH_ATTACHMENT',
                                                        faces[ i ],
                                                        textureIndex,
                                                        0 )

                        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

                        OpenGLES2.ClearDepth( depths[ i ] )
                        OpenGLES2.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )                        
                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )
                
                vertices = [ -10.000000,  10.000000,  10.000000,
                              10.000000,  10.000000,  10.000000,
                              10.000000, -10.000000,  10.000000,
                              -10.000000, -10.000000,  10.000000,
                              10.000000,  10.000000, -10.000000,
                              -10.000000,  10.000000, -10.000000,
                              -10.000000, -10.000000, -10.000000,
                              10.000000, -10.000000, -10.000000 ]
        
                indices = [ 1, 4, 7, 7, 2, 1,  # positive x
                            5, 0, 3, 3, 6, 5,  # negative x
                            5, 4, 1, 1, 0, 5,
                            3, 2, 7, 7, 6, 3, 
                            0, 1, 2, 2, 3, 0,
                            4, 5, 6, 6, 7, 4 ]

                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) ) 

                
######################################################################
def mvMatrix( angle ):
        RAD = 360.0 / ( 2.0 * math.pi )
        angle = angle /  RAD
        mv       = [ 0 ] * 16
        mv[ 0 ]  = +math.cos( angle )
        mv[ 2 ]  = +math.sin( angle )
        mv[ 5 ]  = 1.0
        mv[ 8 ]  = -math.sin( angle )
        mv[ 10 ] = +math.cos( angle )
        mv[ 14 ] = -5.0
        mv[ 15 ] = 1.0

        return mv

######################################################################
def pMatrix( aspectRatio ):
        mp       = [ 0 ] * 16
        mp[ 0 ]  = math.cos( 0.25 ) / math.sin( 0.25 )
        mp[ 5 ]  = mp[ 0 ] * aspectRatio
        mp[ 10 ] = - 10.0 / 9.0
        mp[ 11 ] = - 1.0
        mp[ 14 ] = - 10.0 / 9.0
    
        return mp

######################################################################
class FramebufferTexture2DColorCubemap( vt.VisualTestCase ):

        ''' The purpose of the test: test using cubemap as framebuffer color
        attachment.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. Viewport size 320*240.
        '''
        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_RGB565' ),
                                             ( 'OPENGLES2_RGB888' ),
                                             ( 'OPENGLES2_RGBA4444' ),
                                             ( 'OPENGLES2_RGBA5551' ),
                                             ( 'OPENGLES2_RGBA8888' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.name = "OPENGLES2 FramebufferTexture2D cubemap color, texture type=%s" % ( textureTypeToStr( self.textureType ), )
                
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )

                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureW = 64
                textureH = 64
                textureSize = max( textureW, textureH )
                                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                         textureSize,
                                                         textureSize,
                                                         'OFF',
                                                         self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                        
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', -1 )
                        
                for i in range( 6 ):
                        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                        'GL_COLOR_ATTACHMENT0',
                                                        faces[ i ],
                                                        textureIndex,
                                                        0 )

                        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                        OpenGLES2.Viewport( 0, 0, textureSize, textureSize );
                        
                        OpenGLES2.ClearColor( colors[ i ] )
                        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )                        
                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )
                
                vertices = [ -10.000000,  10.000000,  10.000000,
                              10.000000,  10.000000,  10.000000,
                              10.000000, -10.000000,  10.000000,
                              -10.000000, -10.000000,  10.000000,
                              10.000000,  10.000000, -10.000000,
                              -10.000000,  10.000000, -10.000000,
                              -10.000000, -10.000000, -10.000000,
                              10.000000, -10.000000, -10.000000 ]
        
                indices = [ 1, 4, 7, 7, 2, 1,  # positive x
                            5, 0, 3, 3, 6, 5,  # negative x
                            5, 4, 1, 1, 0, 5,
                            3, 2, 7, 7, 6, 3, 
                            0, 1, 2, 2, 3, 0,
                            4, 5, 6, 6, 7, 4 ]

                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                OpenGLES2.Disable( 'GL_DITHER' )
                                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class FramebufferTexture2DColorCubemapNpot( vt.VisualTestCase ):

        ''' The purpose of the test: test using NPOT cubemap as framebuffer
        color attachment.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. Viewport size 320*240.
        '''
        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_RGB565' ),
                                             ( 'OPENGLES2_RGB888' ),
                                             ( 'OPENGLES2_RGBA4444' ),
                                             ( 'OPENGLES2_RGBA5551' ),
                                             ( 'OPENGLES2_RGBA8888' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.name = "OPENGLES2 NPOT FramebufferTexture2D cubemap color, texture type=%s" % ( textureTypeToStr( self.textureType ), )
                
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )

                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureW = 64 - 1
                textureH = 64 - 1
                textureSize = max( textureW, textureH )
                                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                         textureSize,
                                                         textureSize,
                                                         'OFF',
                                                         self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                                                
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', -1 )
                        
                for i in range( 6 ):
                        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                        'GL_COLOR_ATTACHMENT0',
                                                        faces[ i ],
                                                        textureIndex,
                                                        0 )

                        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

                        OpenGLES2.ClearColor( colors[ i ] )
                        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )                        
                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_CLAMP_TO_EDGE',
                                        'GL_CLAMP_TO_EDGE' )
                
                vertices = [ -10.000000,  10.000000,  10.000000,
                              10.000000,  10.000000,  10.000000,
                              10.000000, -10.000000,  10.000000,
                              -10.000000, -10.000000,  10.000000,
                              10.000000,  10.000000, -10.000000,
                              -10.000000,  10.000000, -10.000000,
                              -10.000000, -10.000000, -10.000000,
                              10.000000, -10.000000, -10.000000 ]
        
                indices = [ 1, 4, 7, 7, 2, 1,  # positive x
                            5, 0, 3, 3, 6, 5,  # negative x
                            5, 4, 1, 1, 0, 5,
                            3, 2, 7, 7, 6, 3, 
                            0, 1, 2, 2, 3, 0,
                            4, 5, 6, 6, 7, 4 ]

                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class FramebufferTexture2DColorCubemapNpotExt( vt.VisualTestCase ):

        ''' The purpose of the test: test using NPOT cubemap as framebuffer
        color attachment. Requires GL_OES_texture_npot extension.

        Expected output: a multicolored sphere on a gray background. With RGB
        textures, the sphere colors should be center=magenta, left=green,
        up=yellow, right=red, down=blue, border=white. Viewport size 320*240.
        '''
        testValues = { ( 'textureType' ) : [ ( 'OPENGLES2_RGB565' ),
                                             ( 'OPENGLES2_RGB888' ),
                                             ( 'OPENGLES2_RGBA4444' ),
                                             ( 'OPENGLES2_RGBA5551' ),
                                             ( 'OPENGLES2_RGBA8888' ),
                                             ]
                       }
        
        def __init__( self, textureType ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.name = "OPENGLES2 NPOT FramebufferTexture2D cubemap color ext, texture type=%s" % ( textureTypeToStr( self.textureType ), )
                
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )

                defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )

                colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                           [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                           [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                           [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                           [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                           [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

                textureW = 64 - 1
                textureH = 64 - 1
                textureSize = max( textureW, textureH )
                                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                          'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                          'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                for i in range( 6 ):
                        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                        OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                         textureSize,
                                                         textureSize,
                                                         'OFF',
                                                         self.textureType )
             
                        OpenGLES2.TexImage2D( textureDataIndex, faces[ i ] );
                                                
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', -1 )
                        
                for i in range( 6 ):
                        OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                        'GL_COLOR_ATTACHMENT0',
                                                        faces[ i ],
                                                        textureIndex,
                                                        0 )

                        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

                        OpenGLES2.ClearColor( colors[ i ] )
                        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
                
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )                        
                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        'GL_MIRRORED_REPEAT',
                                        'GL_MIRRORED_REPEAT' )
                
                vertices = [ -10.000000,  10.000000,  10.000000,
                              10.000000,  10.000000,  10.000000,
                              10.000000, -10.000000,  10.000000,
                              -10.000000, -10.000000,  10.000000,
                              10.000000,  10.000000, -10.000000,
                              -10.000000,  10.000000, -10.000000,
                              -10.000000, -10.000000, -10.000000,
                              10.000000, -10.000000, -10.000000 ]
        
                indices = [ 1, 4, 7, 7, 2, 1,  # positive x
                            5, 0, 3, 3, 6, 5,  # negative x
                            5, 4, 1, 1, 0, 5,
                            3, 2, 7, 7, 6, 3, 
                            0, 1, 2, 2, 3, 0,
                            4, 5, 6, 6, 7, 4 ]

                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                
                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
               

######################################################################
class TwoFramebuffers( vt.VisualTestCase ):

        ''' The purpose of the test: test using multiple framebuffers.

        Expected output: green surface. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.color0Format  = 'GL_RGB565'
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.overdraw      = 1
                self.name = "OPENGLES2 two framebuffers"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      1, 1,
                                      apis,
                                      defaultAttributes )

                framebuffer1Index = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebuffer1Index )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebuffer1Index )

                color0Renderbuffer1Index = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0Renderbuffer1Index )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0Renderbuffer1Index )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.color0Format,
                                               WIDTH, HEIGHT )
                
                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0Renderbuffer1Index )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

                framebuffer2Index = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebuffer2Index )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebuffer2Index )

                color0Renderbuffer2Index = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0Renderbuffer2Index )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0Renderbuffer2Index )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.color0Format,
                                               WIDTH, HEIGHT )
                
                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0Renderbuffer2Index )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebuffer1Index )

                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )               
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
               
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebuffer2Index )
                
                OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )               
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebuffer1Index )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class DeleteFramebufferInUse( vt.VisualTestCase ):

        ''' The purpose of the test: test deleting framebuffer in use.

        Expected output: green surface. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.color0Format  = 'GL_RGB565'
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.overdraw      = 1
                self.name = "OPENGLES2 delete framebuffer in use"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )               
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )               
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )

                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.color0Format,
                                               WIDTH, HEIGHT )
                
                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

                OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )               
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES2.DeleteFramebuffer( framebufferIndex )

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class FramebufferOES_rgb8_rgba8( vt.VisualTestCase ):

        ''' The purpose of the test: test framebuffer with RGB8 and RGBA8 color
        formats. Requires GL_OES_rgb8_rgba8 extension.

        Expected output: full screen plane with a 4x4 vertex grid. The vertex
        colors are defined so that the lower-left vertex has color
        [0.0,0.0,0.5,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Viewport size
        320*240.
        '''
        testValues = { ( 'color0Format' ) : [ ( 'GL_RGB8_OES' ),
                                              ( 'GL_RGBA8_OES' ),
                                              ]
                       }
        
        def __init__( self, color0Format ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.color0Format  = color0Format
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.overdraw      = 1
                self.name = "OPENGLES2 framebuffer OES_rgb8_rgba8 extension, color0=%s" % ( self.color0Format[ 3: ], )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      1, 1,
                                      apis,
                                      defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_rgb8_rgba8' )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.color0Format,
                                               WIDTH, HEIGHT )
                
                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                
                mesh = createPlane( self.gridx, self.gridy )
                mesh.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )               
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class FramebufferOES_depth( vt.VisualTestCase ):

        ''' The purpose of the test: test framebuffer with DEPTH
        attachments. Requires GL_OES_depth24 or GL_OES_depth32 extensions.

        Expected output: first the test draws a full screen mesh with a 4x4
        vertex grid. The vertex colors are defined so that the lower-left vertex
        has color [0.0,0.0,1.0,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. In between, the vertex color is linearly interpolated
        from left to right, from bottom to top. The vertex depth values are
        defined -1.0 and 1.0 in a similar fashion. The first mesh is drawn using
        GL_ALWAYS depth func, after which the depth function is set
        GL_LESS. Secondly, the test draws a full screen mesh with a 4x4 vertex
        grid. The vertex colors are defined [1.0,1.0,1.0,1.0] (lower-left) and
        [1.0,0.0,0.0,1.0] (upper-right). In between, the vertex color is
        linearly interpolated from left to right, from bottom to top. The vertex
        depth values are defined 1.0 and 0.0 in a similar fashion. Viewport size
        320*240.
        '''
        testValues = { ( 'color0Format', 'depthFormat' ) : [ ( 'GL_RGB565', 'GL_DEPTH_COMPONENT24_OES' ),
                                                             ( 'GL_RGB565', 'GL_DEPTH_COMPONENT32_OES' ),
                                                             ]
                       }
        
        def __init__( self, color0Format, depthFormat ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.color0Format  = color0Format
                self.depthFormat   = depthFormat
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.overdraw      = 1
                self.name = "OPENGLES2 framebuffer OES_depth extension, color0=%s, depth=%s" % ( self.color0Format[ 3: ],
                                                                                                 self.depthFormat[ 3: ], )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      1, 1,
                                      apis,
                                      defaultAttributes )

                extensions = { 'GL_DEPTH_COMPONENT24_OES' : 'GL_OES_depth24',
                               'GL_DEPTH_COMPONENT32_OES' : 'GL_OES_depth32' }
                
                OpenGLES2.CheckExtension( extensions[ self.depthFormat ] )
                        
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.color0Format,
                                               WIDTH, HEIGHT )
                
                depthRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( depthRenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            depthRenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.depthFormat,
                                               WIDTH, HEIGHT )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_DEPTH_ATTACHMENT',
                                                   'GL_RENDERBUFFER',
                                                   depthRenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                
                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh1.depthGradient( -1.0, 1.0 )
                
                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh2.depthGradient( 1.0, -1.0 )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_ALWAYS' )                                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.7 ] )
                drawVertexData( modules, vertexDataSetup1 )
                
                OpenGLES2.DepthFunc( 'GL_LESS' )

                useVertexData( modules, indexTracker, vertexDataSetup2, program )
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.5 ] )
                drawVertexData( modules, vertexDataSetup2 )

                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class FramebufferOES_stencil4( vt.VisualTestCase ):

        ''' The purpose of the test: test framebuffer with stencil. Requires
        GL_OES_stencil4 extension.

        Expected output: a blue rectangle in the middle of a red
        rectangle. Viewport size 320*240.
        '''
        testValues = { ( 'color0Format', 'stencilFormat' ) : [ ( 'GL_RGB565', 'GL_STENCIL_INDEX4_OES' ),
                                                               ]
                       }
        
        def __init__( self, color0Format, stencilFormat ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.color0Format  = color0Format
                self.stencilFormat = stencilFormat
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.overdraw      = 1
                self.name = "OPENGLES2 framebuffer OES_stencil4 extension, color0=%s, stencil=%s" % ( self.color0Format[ 3: ],
                                                                                                      self.stencilFormat[ 3: ], )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      1, 1,
                                      apis,
                                      defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_OES_stencil4' )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.color0Format,
                                               WIDTH, HEIGHT )
                
                stencilRenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( stencilRenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            stencilRenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.stencilFormat,
                                               WIDTH, HEIGHT )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_STENCIL_ATTACHMENT',
                                                   'GL_RENDERBUFFER',
                                                   stencilRenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                
                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                mesh2 = createPlane( self.gridx, self.gridy )
                mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                OpenGLES2.ClearStencil( 1 )

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_STENCIL_BUFFER_BIT' ] )
                
                w = WIDTH  / 2
                h = HEIGHT / 2
                x = ( WIDTH - w  ) / 2
                y = ( HEIGHT - h ) / 2
                
                OpenGLES2.Scissor( x, y, w, h )
                OpenGLES2.Enable( 'GL_SCISSOR_TEST' )

                OpenGLES2.ClearStencil( 2 )
                OpenGLES2.Clear( [ 'GL_STENCIL_BUFFER_BIT' ] )
                OpenGLES2.Disable( 'GL_SCISSOR_TEST' )

                OpenGLES2.StencilFunc( 'GL_EQUAL', 2, '0xFF' )
                OpenGLES2.StencilOp( 'GL_INCR', 'GL_KEEP', 'GL_INCR' )
                OpenGLES2.Enable( 'GL_STENCIL_TEST' )

                OpenGLES2.Clear( [ 'GL_DEPTH_BUFFER_BIT' ] )
                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.7 ] )
                drawVertexData( modules, vertexDataSetup1 )

                useVertexData( modules, indexTracker, vertexDataSetup2, program )               
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.5 ] )
                drawVertexData( modules, vertexDataSetup2 )
                
                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                

######################################################################
class FramebufferSize( vt.VisualTestCase ):

        ''' The purpose of the test: test a framebuffer with different sizes.

        Expected output: the test draws a full screen mesh with a 4x4 vertex
        grid. The vertex colors are defined so that the lower-left vertex has
        color [0.0,0.0,1.0,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. In between, the vertex color is linearly interpolated
        from left to right, from bottom to top.
        '''
        testValues = { ( 'color0Format', 'size' ) : [ ( 'GL_RGBA4', [ 255, 255 ] ),
                                                      ( 'GL_RGBA4', [ 256, 256 ] ),
                                                      ( 'GL_RGBA4', [ 257, 257 ] ),
                                                      ( 'GL_RGB5_A1', [ 255, 255 ] ),
                                                      ( 'GL_RGB5_A1', [ 256, 256 ] ),
                                                      ( 'GL_RGB5_A1', [ 257, 257 ] ),
                                                      ( 'GL_RGB565', [ 255, 255 ] ),
                                                      ( 'GL_RGB565', [ 256, 256 ] ),
                                                      ( 'GL_RGB565', [ 257, 257 ] ),
                                                      ]
                       }

        def __init__( self, color0Format, size ):
                vt.VisualTestCase.__init__( self )
                self.repeats       = 1
                self.color0Format  = color0Format
                self.gridx         = GRIDX
                self.gridy         = GRIDY
                self.size          = size
                self.overdraw      = 1
                self.name = "OPENGLES2 framebuffer sizes, color0=%s, size=%dx%s" % ( self.color0Format[ 3: ],
                                                                                     self.size[ 0 ],
                                                                                     self.size[ 1 ], )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      1, 1,
                                      apis,
                                      defaultAttributes )

                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )
                
                color0RenderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
                OpenGLES2.GenRenderbuffer( color0RenderbufferIndex )
                OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER',
                                            color0RenderbufferIndex )
                OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER',
                                               self.color0Format,
                                               self.size[ 0 ], self.size[ 1 ] )
                
                OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER',
                                                   'GL_COLOR_ATTACHMENT0',
                                                   'GL_RENDERBUFFER',
                                                   color0RenderbufferIndex )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                
                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, self.size[ 0 ], self.size[ 1 ] );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )
                
                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, self.size[ 0 ], self.size[ 1 ], target.outputPath( apis ) )

                
######################################################################
class FramebufferTexture2DSize( vt.VisualTestCase ):

        ''' The purpose of the test: test using different sized textures as
        framebuffer color attachment.

        Expected output: the test draws a full screen mesh with a 4x4 vertex
        grid. The vertex colors are defined so that the lower-left vertex has
        color [0.0,0.0,1.0,1.0] and the upper-right vertex has color
        [1.0,1.0,1.0,1.0]. In between, the vertex color is linearly interpolated
        from left to right, from bottom to top.
        '''

        testValues = { ( 'textureType', 'size' ) : [ ( 'OPENGLES2_RGB565', [ 255, 255 ] ),
                                                     ( 'OPENGLES2_RGB565', [ 256, 256 ] ),
                                                     ( 'OPENGLES2_RGB565', [ 257, 257 ] ),
                                                     ( 'OPENGLES2_RGB888', [ 255, 255 ] ),
                                                     ( 'OPENGLES2_RGB888', [ 256, 256 ] ),
                                                     ( 'OPENGLES2_RGB888', [ 257, 257 ] ),
                                                     ( 'OPENGLES2_RGBA4444', [ 255, 255 ] ),
                                                     ( 'OPENGLES2_RGBA4444', [ 256, 256 ] ),
                                                     ( 'OPENGLES2_RGBA4444', [ 257, 257 ] ),
                                                     ( 'OPENGLES2_RGBA5551', [ 255, 255 ] ),
                                                     ( 'OPENGLES2_RGBA5551', [ 256, 256 ] ),
                                                     ( 'OPENGLES2_RGBA5551', [ 257, 257 ] ),
                                                     ( 'OPENGLES2_RGBA8888', [ 255, 255 ] ),
                                                     ( 'OPENGLES2_RGBA8888', [ 256, 256 ] ),
                                                     ( 'OPENGLES2_RGBA8888', [ 257, 257 ] ),
                                                     ]
                       }
        
        def __init__( self, textureType, size ):
                vt.VisualTestCase.__init__( self )
                self.textureType = textureType
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.size        = size
                self.name = "OPENGLES2 FramebufferTexture2D sizes, texture type=%s, size=%dx%d" % ( textureTypeToStr( self.textureType ),
                                                                                                    self.size[ 0 ],
                                                                                                    self.size[ 1 ], )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  1, 1,
                                                  apis,
                                                  defaultAttributes )
                
                framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
                OpenGLES2.GenFramebuffer( framebufferIndex )
                OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER',
                                           framebufferIndex )

                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateNullTextureData( textureDataIndex,
                                                 self.size[ 0 ], self.size[ 1 ],
                                                 'OFF',
                                                 self.textureType )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D',
                                       textureIndex )
                OpenGLES2.TexImage2D( textureDataIndex,
                                      'GL_TEXTURE_2D' )
                
                OpenGLES2.FramebufferTexture2D( 'GL_FRAMEBUFFER',
                                                'GL_COLOR_ATTACHMENT0',
                                                'GL_TEXTURE_2D',
                                                textureIndex,
                                                0 )

                OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )
                OpenGLES2.Viewport( 0, 0, self.size[ 0 ], self.size[ 1 ] );
                
                mesh1 = createPlane( self.gridx, self.gridy )
                mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
                vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
                
                program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
                useVertexData( modules, indexTracker, vertexDataSetup1, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, self.size[ 0 ], self.size[ 1 ] );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Disable( 'GL_DITHER' )
                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ 0.0 ] )
                drawVertexData( modules, vertexDataSetup1 )
                
                OpenGLES2.CheckError( '' )
                Gltest.SaveTarga( -1, 0, 0, self.size[ 0 ], self.size[ 1 ], target.outputPath( apis ) )

                
