# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )

######################################################################
class Extensions( vt.VisualTestCase ):

        ''' The purpose of the test: test OpenGLES2 extensions.

        Expected output: surface cleared to green if the particular OpenGLES2
        extension is supported. Viewport size 320*240.
        '''
        
        testValues = { ( 'extension', 'functions' ) : [ ( 'GL_OES_compressed_paletted_texture', [] ),
                                                        ( 'GL_OES_compressed_ETC1_RGB8_texture', [] ),
                                                        ( 'GL_OES_EGL_image', [ 'glEGLImageTargetTexture2DOES',
                                                                                'glEGLImageTargetRenderbufferStorageOES' ] ),
                                                        ( 'GL_OES_rgb8_rgba8', [] ),
                                                        ( 'GL_OES_texture_npot', [] ),
                                                        ( 'GL_EXT_texture_format_BGRA8888', [] ),
                                                        ( 'GL_OES_depth24', [] ),
                                                        ( 'GL_OES_depth32', [] ),
                                                        ( 'GL_OES_stencil1', [] ),
                                                        ( 'GL_OES_stencil4', [] ),
                                                        ( 'GL_OES_stencil8', [] ),
                                                        ( 'GL_OES_vertex_half_float', [] ),
                                                        ( 'GL_OES_depth_texture', [] ),
                                       ]
                       }
                                                                             
        def __init__( self, extension, functions ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.extension = extension
                self.functions = functions
                self.name    = "OPENGLES2 extensions, extension=%s" % ( self.extension, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]               

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]                
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )
                
                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()               

                OpenGLES2.CheckExtension( self.extension )
                
                for f in self.functions:
                        OpenGLES2.CheckFunction( f )
                        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.CheckError( '' )               
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

