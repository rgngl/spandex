# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

import math

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )
TYPES         = { 'IMAGES_PVRTC_RGB4'  : 'GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG',
                  'IMAGES_PVRTC_RGB2'  : 'GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG',
                  'IMAGES_PVRTC_RGBA4' : 'GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG',
                  'IMAGES_PVRTC_RGBA2' : 'GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG' }
RAD           = 360.0 / ( 2.0 * math.pi )

######################################################################
def mvMatrix( angle ):
        angle = angle /  RAD
        mv       = [ 0 ] * 16
        mv[ 0 ]  = +math.cos( angle )
        mv[ 2 ]  = +math.sin( angle )
        mv[ 5 ]  = 1.0
        mv[ 8 ]  = -math.sin( angle )
        mv[ 10 ] = +math.cos( angle )
        mv[ 14 ] = -5.0
        mv[ 15 ] = 1.0

        return mv

######################################################################
def pMatrix( aspectRatio ):
        mp       = [ 0 ] * 16
        mp[ 0 ]  = math.cos( 0.25 ) / math.sin( 0.25 )
        mp[ 5 ]  = mp[ 0 ] * aspectRatio
        mp[ 10 ] = - 10.0 / 9.0
        mp[ 11 ] = - 1.0
        mp[ 14 ] = - 10.0 / 9.0
    
        return mp

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
                                       
######################################################################
class PvrtcTextureWrap( vt.VisualTestCase ):

        ''' The purpose of the test: test PVRTC texture wrapping. Requires
        GL_IMG_texture_compression_pvrtc extension.

        Expected output: a full screen plane with a 4x4 vertex grid. A PVRTC
        flower texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Viewport size 320*240.
        '''
       
        testValues = { ( 'format', 'wrapS', 'wrapT' ) : [ ( 'IMAGES_PVRTC_RGB4', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGB4', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB4', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB4', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGB4', 'GL_REPEAT', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB4', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB4', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGB4', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB4', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_REPEAT', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGB2', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_REPEAT', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA4', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_CLAMP_TO_EDGE', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_CLAMP_TO_EDGE', 'GL_MIRRORED_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_REPEAT', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_MIRRORED_REPEAT', 'GL_CLAMP_TO_EDGE' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_MIRRORED_REPEAT', 'GL_REPEAT' ),
                                                          ( 'IMAGES_PVRTC_RGBA2', 'GL_MIRRORED_REPEAT', 'GL_MIRRORED_REPEAT' ),
                                             ]
                       }
        
        def __init__( self, format, wrapS, wrapT ):
                vt.VisualTestCase.__init__( self )
                self.format      = format
                self.wrapS       = wrapS
                self.wrapT       = wrapT
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 PVRTC texture, format=%s, wrapS=%s, wrapT=%s" % ( self.format[ 7 : ],
                                                                                         wrapToStr( self.wrapS ),
                                                                                         wrapToStr( self.wrapT ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_IMG_texture_compression_pvrtc' )                
                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                textureSize = max( textureW, textureH )

                imageDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
                Images.CreateData( imageDataIndex,
                                   'IMAGES_FLOWER',
                                   self.format,
                                   textureSize, textureSize )

                dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                Images.CopyToOpenGLES2( imageDataIndex,
                                        dataIndex )

                compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                OpenGLES2.CreateCompressedTextureData( compressedTextureDataIndex,
                                                       dataIndex,
                                                       textureSize, textureSize,
                                                       TYPES[ self.format ] )
                
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrapS,
                                        self.wrapT )

                textureSetup = TextureSetup( compressedTextureDataIndex, textureIndex )
                useTexture( modules, textureSetup, 0, program )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
                
######################################################################
class PvrtcTextureMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test PVRTC texture min filtering. Requires
        GL_IMG_texture_compression_pvrtc extension.

        Expected output: a full screen plane with a 4x4 vertex grid. A PVRTC
        flower texture is drawn across the plane with texture coordinates
        running from 0.0 to 2.5 (left-right, bottom-top). Viewport size 320*240.
        '''

        testValues = { ( 'format', 'minFilter', 'wrap' ) : [ ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ]
                       }
        
        def __init__( self, format, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.format      = format
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.repeats     = 1
                self.gridx       = GRIDX
                self.gridy       = GRIDY
                self.overdraw    = 1
                self.name = "OPENGLES2 PVRTC texture min filter, format=%s, filter=%s, wrap=%s" % ( self.format[ 7 : ],
                                                                                                    filterToStr( self.minFilter ),
                                                                                                    wrapToStr( self.wrap ), )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gTexCoord0;
                    varying vec4 gVSTexCoord0;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSTexCoord0 = gTexCoord0;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    varying vec4 gVSTexCoord0;

                    void main()
                    {
                        gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_IMG_texture_compression_pvrtc' )
                                
                mesh              = createPlane( self.gridx, self.gridy, 2.5, 2.5, False, [ True ] )
                
                vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
                program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                textureSize = max( textureW, textureH )

                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )

                mmlevel = 0
                while textureSize >= 1:
                        imageDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
                        Images.CreateData( imageDataIndex,
                                           'IMAGES_FLOWER',
                                           self.format,
                                           textureSize, textureSize )

                        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                        Images.CopyToOpenGLES2( imageDataIndex,
                                                dataIndex )

                        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                        OpenGLES2.CreateCompressedTextureData( compressedTextureDataIndex,
                                                               dataIndex,
                                                               textureSize, textureSize,
                                                               TYPES[ self.format ] )
                
                        OpenGLES2.CompressedTexImage2DMML( compressedTextureDataIndex, 'GL_TEXTURE_2D', mmlevel )
                        mmlevel += 1
                        textureSize /= 2

                OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                        'GL_LINEAR',
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )
                        
                OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )
                
                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Enable( 'GL_DEPTH_TEST' )
                OpenGLES2.DepthFunc( 'GL_LEQUAL' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                delta   = 1.0 / float( self.overdraw )
                offset  = 1.0

                for i in range( self.overdraw ):
                        OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                        drawVertexData( modules, vertexDataSetup )
                        offset -= delta

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

######################################################################
class PvrtcCubeMapMinFilter( vt.VisualTestCase ):

        ''' The purpose of the test: test PVRTC cube map min filtering. Requires
        GL_IMG_texture_compression_pvrtc extension.

        Expected output: a sphere in a gray background, with a flower texture
        applied to each cubemap face. Viewport size 320*240.
        '''

        testValues = { ( 'format', 'minFilter', 'wrap' ) : [ ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGB2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA4', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_NEAREST', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_NEAREST_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_CLAMP_TO_EDGE' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_REPEAT' ),
                                                             ( 'IMAGES_PVRTC_RGBA2', 'GL_LINEAR_MIPMAP_LINEAR', 'GL_MIRRORED_REPEAT' ),
                                                             ]
                       }

        def __init__( self, format, minFilter, wrap ):
                vt.VisualTestCase.__init__( self )
                self.format      = format
                self.minFilter   = minFilter
                self.wrap        = wrap
                self.name = "OPENGLES2 PVRTC cube mapped sphere min filter, format=%s, minFilter=%s, wrap=%s" % ( self.format[ 7 : ],
                                                                                                                  filterToStr( self.minFilter ),
                                                                                                                  wrapToStr( self.wrap ), )

        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Images    = modules[ 'Images' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
               
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec3 gNormal;
                    uniform mat4 gModelView;
                    uniform mat4 gProj;
                    varying vec3 gVSTexCoord;
                                                        
                    void main()
                    {
                        vec4 pos = gModelView * gVertex;
                        vec3 normal = normalize( gModelView * vec4( gNormal, 0.0 ) ).xyz;
                        gl_Position = gProj * pos;
                        vec3 view = -pos.xyz;
                        gVSTexCoord = reflect( -view, normal );
                        gVSTexCoord.y = -gVSTexCoord.y;
                    } """
                                
                fragmentShaderSource = """
                    precision mediump float;                                                       
                    uniform samplerCube gCubeMap0;
                    varying vec3 gVSTexCoord;

                    void main()
                    {
                        gl_FragColor = textureCube( gCubeMap0, gVSTexCoord );
                    } """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                state             = target.setup( modules,
                                                  indexTracker,
                                                  WIDTH, HEIGHT,
                                                  apis,
                                                  defaultAttributes )

                OpenGLES2.CheckExtension( 'GL_IMG_texture_compression_pvrtc' )
                
                textureW = toPot( WIDTH, True )[ 0 ]
                textureH = toPot( HEIGHT, True )[ 0 ]
                textureSize = min( textureW, textureH )
        
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )            
                OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', textureIndex )

                OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP',
                                        self.minFilter,
                                        'GL_LINEAR',
                                        self.wrap,
                                        self.wrap )

                mmlevel = 0
                while textureSize >= 1:
                        imageDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
                        Images.CreateData( imageDataIndex,
                                           'IMAGES_FLOWER',
                                           self.format,
                                           textureSize, textureSize )

                        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
                        Images.CopyToOpenGLES2( imageDataIndex,
                                                dataIndex )
                        
                        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
                        OpenGLES2.CreateCompressedTextureData( compressedTextureDataIndex,
                                                               dataIndex,
                                                               textureSize, textureSize,
                                                               TYPES[ self.format ] )
                
                        faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                                  'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                                  'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

                        for i in range( 6 ):
                                OpenGLES2.CompressedTexImage2DMML( compressedTextureDataIndex, faces[ i ], mmlevel )                        

                        mmlevel += 1
                        textureSize /= 2
                                
                long = 32
                lat  = 32
                rad  = 1.0

                sphere      = Sphere( long, lat, rad )
                objectSetup = setupObject( modules, indexTracker, sphere )
                program     = createCubeProgram( modules, indexTracker, vsSource, fsSource, objectSetup )
                useObject( modules, indexTracker, objectSetup, program )

                modelviewLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelviewLocationIndex, program[ 0 ], "gModelView" )
                matMV = mvMatrix( 0 )
                OpenGLES2.UniformMatrix( modelviewLocationIndex, 1, matMV )
               
                projectionLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( projectionLocationIndex, program[ 0 ], "gProj" )
                matProj = pMatrix( WIDTH / HEIGHT )
                OpenGLES2.UniformMatrix( projectionLocationIndex, 1, matProj )                
                
                OpenGLES2.ClearColor( [ 0.5, 0.5, 0.5, 1.0 ] )
        
                OpenGLES2.Enable( 'GL_DEPTH_TEST')
                OpenGLES2.Enable( 'GL_CULL_FACE' )
                OpenGLES2.CullFace( 'GL_BACK' )
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT', 'GL_DEPTH_BUFFER_BIT' ] )

                drawObject( modules, objectSetup )                

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
