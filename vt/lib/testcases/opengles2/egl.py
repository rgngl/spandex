# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from lib.egl     import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 4, 4 )

IMAGES_FORMAT = { 'SCT_COLOR_FORMAT_RGB565'       : 'IMAGES_RGB565',
                  'SCT_COLOR_FORMAT_RGBX8888'     : 'IMAGES_BGRX8' ,
                  'SCT_COLOR_FORMAT_RGBA8888'     : 'IMAGES_BGRA8',
                  'SCT_COLOR_FORMAT_YUV12'        : 'IMAGES_YUV12'}

SURFACE_PARAMETERS = { 'OPENGLES2_RGB565'   : [ 16, 5, 6, 5, 0 ],
                       'OPENGLES2_RGB888'   : [ 24, 8, 8, 8, 0 ],
                       'OPENGLES2_RGBA8888' : [ 32, 8, 8, 8, 8 ] }

SURFACE_PARAMETERS2 = { 'SCT_COLOR_FORMAT_RGB565'       : [ 16, 5, 6, 5, 0 ],
                        'SCT_COLOR_FORMAT_RGB888'       : [ 24, 8, 8, 8, 0 ],
                        'SCT_COLOR_FORMAT_RGBX8888'     : [ 24, 8, 8, 8, 0 ],
                        'SCT_COLOR_FORMAT_RGBA8888'     : [ 32, 8, 8, 8, 8 ],
                        'SCT_COLOR_FORMAT_RGBA8888_PRE' : [ 32, 8, 8, 8, 8 ],
                        }

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh

######################################################################            
class EglImageAsTextureExt( vt.VisualTestCase ):
    
    ''' Test creating an OpenGLES2 texture from a shared pixmap EGL image,
    preserving EGL image buffer pixels. Draws a flower scaled to the
    surface. Texture size and format varies. Requires EGL and EGL_KHR_image,
    EGL_KHR_image_base, EGL_KHR_image_pixmap, EGL_KHR_gl_texture_2D_image, and
    GL_OES_EGL_image extensions. In addition, requires
    EGL_NOK_pixmap_type_rsgimage extension in Symbian.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 255, 255, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 255, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 255, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 257, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 257, 'SCT_COLOR_FORMAT_RGB565' ),                         
                         ( 257, 257, 'SCT_COLOR_FORMAT_RGB565' ),                         
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBX8888' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBA8888' ) ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES2 egl image as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]

        # Check required extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )

        OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )
        
        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       

        # Create image data, copy to egl, and create a shared pixmap from the
        # data.
        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex,
                           'IMAGES_FLOWER',
                           IMAGES_FORMAT[ self.format ],
                           self.width, self.height )

        eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
        Images.CopyToEgl( imagesDataIndex, eglDataIndex )

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENGLES2_TEXTURE_2D' ],
                                eglDataIndex )

        # Create egl image from shared pixmap.
        imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            -1,
                            'EGL_NATIVE_PIXMAP_KHR',
                            eglPixmapIndex,
                            imageAttributes )

        # Create OpenGLES2 data/objects.
        mesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )
                       
        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        # Set texture data from egl image. 
        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )

        # Destroy original egl image, orphaning the egl image used as a texture.
        Egl.DestroyImage( displayIndex, eglImageIndex )
        
        textureSetup = TextureSetup( 0, textureIndex )
        useTexture( modules, textureSetup, 0, program )

        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES2.CheckError( '' )
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

        
######################################################################            
class EglImageFromTextureExt( vt.VisualTestCase ):
    
    ''' Test creating an EGL image from a texture, then creating a second
    texture from the EGL image, and checking that the texture data update done
    in the first texture are reflected in the second. The test should draw a
    gradient texture with [ 0, 1, 0, 0.3 ] rectangle (green) at the lower-left
    corner scaled to the surface. Texture size and format varies. Requires EGL
    and EGL_KHR_image, EGL_KHR_image_base, EGL_KHR_gl_texture_2D_image, and
    GL_OES_EGL_image extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'OPENGLES2_LUMINANCE8' ),
                         ( 256, 256, 'OPENGLES2_ALPHA8' ),
                         ( 128, 128, 'OPENGLES2_RGB565' ),
                         ( 64, 128, 'OPENGLES2_RGB565' ),
                         ( 128, 64, 'OPENGLES2_RGB565' ),
                         ( 255, 255, 'OPENGLES2_RGB565' ),
                         ( 256, 255, 'OPENGLES2_RGB565' ),
                         ( 255, 256, 'OPENGLES2_RGB565' ),
                         ( 256, 256, 'OPENGLES2_RGB565' ),
                         ( 257, 256, 'OPENGLES2_RGB565' ),
                         ( 256, 257, 'OPENGLES2_RGB565' ),
                         ( 257, 257, 'OPENGLES2_RGB888' ),
                         ( 256, 256, 'OPENGLES2_RGBA4444' ),
                         ( 256, 256, 'OPENGLES2_RGBA5551' ),
                         ( 256, 256, 'OPENGLES2_RGBA8888' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES2 egl image from texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]

        # Check required extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )        

        # Create OpenGLES2 data/objects.
        mesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( textureData1Index,
                                         self.width, self.height,
                                         'OFF',
                                         self.format )
        
        textureData2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureData2Index,
                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             self.format )

        textureData3Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateSolidTextureData( textureData3Index,
                                          [ 0, 1, 0, 0.3 ],
                                          self.width / 2, self.height / 2,
                                          'OFF',
                                          self.format )

        # Create first texture and set its size. Use NULL data.
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
        
        # Create EGL image from the first texture.
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, contextIndex, 'EGL_GL2_TEXTURE_2D_KHR', texture1Index, [] )

        # Create a second texture from the egl image.
        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )

        # Set the first texture active and update its data. These changes should
        # be reflected also in the second texture as they are based on the same
        # EGL image.
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexSubImage2D( textureData2Index, 'GL_TEXTURE_2D', 0, 0 )
        OpenGLES2.TexSubImage2D( textureData3Index, 'GL_TEXTURE_2D', 0, 0 )

        # Bind the second texture and use it for drawing.
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
       
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################            
class EglImageFromTextureMipmapExt( vt.VisualTestCase ):
    
    ''' Test creating an EGL image from a texture mipmap, then creating a second
    texture from the EGL image, and checking that the texture data update done
    in the first texture are reflected in the second. The test should draw a
    blue surface with a gradient texture at the lower-left corner scaled to the
    surface. Texture size and format varies. Requires EGL and EGL_KHR_image,
    EGL_KHR_image_base, EGL_KHR_gl_texture_2D_image, and GL_OES_EGL_image
    extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'OPENGLES2_RGB565' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES2 egl image from texture mipmap %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]

        # Check required extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )        

        # Create OpenGLES2 data/objects.
        mesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        # All mipmap levels red by default
        textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateSolidTextureData( textureData1Index,
                                          [ 1, 0, 0, 1.0 ],
                                          128, 128,
                                          'ON',
                                          self.format )

        # Mipmap level 1 blue
        textureData2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateSolidTextureData( textureData2Index,
                                          [ 0, 0, 1, 1.0 ],
                                          64, 64,
                                          'OFF',
                                          self.format )

        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        # NOTE: it seems we need to use a texture filtering mode that supports
        # mipmaps in some hardware. Default is GL_NEAREST_MIPMAP_LINEAR so leave
        # it as is.
        OpenGLES2.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
        OpenGLES2.TexImage2DMML( textureData2Index, 'GL_TEXTURE_2D', 1 )
       
        # Create EGL image from the first texture, mipmap level 1.
        attributes = [ EGL_DEFINES[ 'EGL_GL_TEXTURE_LEVEL_KHR' ], 1 ]
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, contextIndex, 'EGL_GL2_TEXTURE_2D_KHR', texture1Index, attributes )

        # Create a second texture from the egl image.
        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )

        # Set the first texture active and update its mipmap level 1 data. These
        # changes should be reflected into the second texture.
        textureData3Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureData3Index,
                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             32, 32,
                                             'OFF',
                                             self.format )
        
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexSubImage2DMML( textureData3Index, 'GL_TEXTURE_2D', 0, 0, 1 )

        # Bind the second texture and use it for drawing.
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
       
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################            
class TexImage2DOrphaningEglImageFromTextureExt( vt.VisualTestCase ):
    
    ''' Test orphaning an EGL image from a texture through TexImage2D, then
    creating a second texture from the EGL image, and checking that the texture
    data update done in the first texture is not reflected in the second. The
    test should draw a gradient texture with [ 0, 1, 0, 0.3 ] rectangle (green)
    at the lower-left corner scaled to the surface. Texture size and format
    varies. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_gl_texture_2D_image, and GL_OES_EGL_image extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'OPENGLES2_RGB565' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES2 teximage2d orphaning egl image from texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]

        # Check required extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )        

        # Create OpenGLES2 data/objects.
        mesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( textureData1Index,
                                         self.width, self.height,
                                         'OFF',
                                         self.format )
        
        textureData2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureData2Index,
                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             self.format )

        textureData3Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateSolidTextureData( textureData3Index,
                                          [ 0, 1, 0, 0.3 ],
                                          self.width / 2, self.height / 2,
                                          'OFF',
                                          self.format )
        
        # Create first texture and set its size. Use NULL data.
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
        
        # Create EGL image from the first texture.
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, contextIndex, 'EGL_GL2_TEXTURE_2D_KHR', texture1Index, [] )

        # Create a second texture from the egl image.
        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )

        # Set the first texture active and update its data. These changes should
        # be reflected also in the second texture as they are based on the same
        # EGL image.
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexSubImage2D( textureData2Index, 'GL_TEXTURE_2D', 0, 0 )
        OpenGLES2.TexSubImage2D( textureData3Index, 'GL_TEXTURE_2D', 0, 0 )

        # Respecify the first texture data, orphaning the egl image. Changes
        # done to texture data after this should not become visible on the
        # second texture.
        OpenGLES2.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
        OpenGLES2.TexSubImage2D( textureData2Index, 'GL_TEXTURE_2D', 0, 0 )
        
        # Bind the second texture and use it for drawing.
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
       
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################            
class CopyTexImage2DOrphaningEglImageFromTextureExt( vt.VisualTestCase ):
    
    ''' Test orphaning an EGL image from a texture through CopyTexImage2D, then
    creating a second texture from the EGL image, and checking that the texture
    data update done in the first texture is not reflected in the second. The
    test should draw a gradient texture with [ 0, 1, 0, 0.3 ] rectangle (green)
    at the lower-left corner scaled to the surface. Texture size and format
    varies. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_gl_texture_2D_image, and GL_OES_EGL_image extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'OPENGLES2_RGB565' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES2 copyteximage2d orphaning egl image from texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]

        # Check required extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )        

        # Create OpenGLES2 data/objects.
        mesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( textureData1Index,
                                         self.width, self.height,
                                         'OFF',
                                         self.format )
        
        textureData2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureData2Index,
                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             self.format )

        textureData3Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateSolidTextureData( textureData3Index,
                                          [ 0, 1, 0, 0.3 ],
                                          self.width / 2, self.height / 2,
                                          'OFF',
                                          self.format )
        
        # Create first texture and set its size. Use NULL data.
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
        
        # Create EGL image from the first texture.
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, contextIndex, 'EGL_GL2_TEXTURE_2D_KHR', texture1Index, [] )

        # Create a second texture from the egl image.
        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )

        # Set the first texture active and update its data. These changes should
        # be reflected also in the second texture as they are based on the same
        # EGL image.
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexSubImage2D( textureData2Index, 'GL_TEXTURE_2D', 0, 0 )
        OpenGLES2.TexSubImage2D( textureData3Index, 'GL_TEXTURE_2D', 0, 0 )

        # Respecify the first texture data, orphaning the egl image. Changes
        # done to texture data after this should not become visible on the
        # second texture.
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )        
        OpenGLES2.CopyTexImage2D( 'GL_TEXTURE_2D', 0, 'GL_RGBA', 0, 0, 64, 64 )
        
        # Bind the second texture and use it for drawing.
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
       
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )


######################################################################            
class CompressedTexImage2DOrphaningEglImageFromTextureExt( vt.VisualTestCase ):
    
    ''' Test orphaning an EGL image from a texture through CompressedTexImage2D,
    then creating a second texture from the EGL image, and checking that the
    texture data update done in the first texture is not reflected in the
    second. The test should draw a gradient texture with [ 0, 1, 0, 0.3 ]
    rectangle (green) at the lower-left corner scaled to the surface. Texture
    size and format varies. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_gl_texture_2D_image, GL_OES_EGL_image, and
    GL_OES_compressed_ETC1_RGB8_texture extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'OPENGLES2_RGB565' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.cformat   = 'IMAGES_ETC1_RGB8'
        self.name = 'OPENGLES2 compressedteximage2d orphaning egl image from texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]

        # Check required extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )        
        OpenGLES2.CheckExtension( 'GL_OES_compressed_ETC1_RGB8_texture' )
        
        # Create OpenGLES2 data/objects.
        mesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( textureData1Index,
                                         self.width, self.height,
                                         'OFF',
                                         self.format )
        
        textureData2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureData2Index,
                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             self.format )

        textureData3Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateSolidTextureData( textureData3Index,
                                          [ 0, 1, 0, 0.3 ],
                                          self.width / 2, self.height / 2,
                                          'OFF',
                                          self.format )
        
        # Create first texture and set its size. Use NULL data.
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
        
        # Create EGL image from the first texture.
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, contextIndex, 'EGL_GL2_TEXTURE_2D_KHR', texture1Index, [] )

        # Create a second texture from the egl image.
        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )

        # Set the first texture active and update its data. These changes should
        # be reflected also in the second texture as they are based on the same
        # EGL image.
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexSubImage2D( textureData2Index, 'GL_TEXTURE_2D', 0, 0 )
        OpenGLES2.TexSubImage2D( textureData3Index, 'GL_TEXTURE_2D', 0, 0 )

        # Respecify the first texture data, orphaning the egl image. Changes
        # done to texture data after this should not become visible on the
        # second texture.
        imageDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imageDataIndex,
                           'IMAGES_FLOWER',
                           self.cformat,
                           64, 64 )

        dataIndex = indexTracker.allocIndex( 'OPENGLES2_DATA_INDEX' )
        Images.CopyToOpenGLES2( imageDataIndex,
                                dataIndex )

        TYPES = { 'IMAGES_ETC1_RGB8' : 'GL_ETC1_RGB8_OES' }        
        compressedTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_COMPRESSED_DATA_INDEX' )
        OpenGLES2.CreateCompressedTextureData( compressedTextureDataIndex,
                                               dataIndex,
                                               64, 64,
                                               TYPES[ self.cformat ] )
                
        OpenGLES2.CompressedTexImage2D( compressedTextureDataIndex, 'GL_TEXTURE_2D' )
        
        # Bind the second texture and use it for drawing.
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
       
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################            
class MipmapOrphaningEglImageFromTextureExt( vt.VisualTestCase ):
    
    ''' Test mipmap orphaning an EGL image from a texture, then creating a
    second texture from the EGL image, and checking that the texture data update
    done in the first texture is not reflected in the second. The test should
    draw a gradient texture with [ 0, 1, 0, 0.3 ] rectangle (green) at the
    lower-left corner scaled to the surface. However, it it acceptable that
    calling glGenerateMipmap does not cause orphaning; in such case the test
    should draw a full-screen gradient. Texture size and format varies. Requires
    EGL and EGL_KHR_image, EGL_KHR_image_base, EGL_KHR_gl_texture_2D_image, and
    GL_OES_EGL_image extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'OPENGLES2_RGB565' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES2 mipmap orphaning egl image from texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]

        # Check required extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )        

        # Create OpenGLES2 data/objects.
        mesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        textureData1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( textureData1Index,
                                         self.width, self.height,
                                         'OFF',
                                         self.format )
        
        textureData2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureData2Index,
                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             self.format )

        textureData3Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateSolidTextureData( textureData3Index,
                                          [ 0, 1, 0, 0.3 ],
                                          self.width / 2, self.height / 2,
                                          'OFF',
                                          self.format )
        
        # Create first texture and set its size. Use NULL data.
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( textureData1Index, 'GL_TEXTURE_2D' )
        
        # Create EGL image from the first texture.
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, contextIndex, 'EGL_GL2_TEXTURE_2D_KHR', texture1Index, [] )

        # Create a second texture from the egl image.
        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )

        # Set the first texture active and update its data. These changes should
        # be reflected also in the second texture as they are based on the same
        # EGL image.
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexSubImage2D( textureData2Index, 'GL_TEXTURE_2D', 0, 0 )
        OpenGLES2.TexSubImage2D( textureData3Index, 'GL_TEXTURE_2D', 0, 0 )

        # Respecify the first texture data by generating mipmaps, orphaning the
        # egl image. Changes done to texture data after this should not become
        # visible on the second texture.
        OpenGLES2.GenerateMipmap( 'GL_TEXTURE_2D' )
        OpenGLES2.TexSubImage2D( textureData2Index, 'GL_TEXTURE_2D', 0, 0 )
        
        # Bind the second texture and use it for drawing.
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
       
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################            
class PixmapSurfaceAsTextureExt( vt.VisualTestCase ):
    
    ''' Test creating a pixmap surface from a shared pixmap. The test first
    creates a shared pixmap. Secondly, the test creates a texture from the
    shared pixmap through EGL image. Thirdly, creates a pixmap surface from the
    shared pixmap, render some graphics, and then uses the texture for
    rendering. The test should draw back-and-white diagonal gradient to the
    surface; black on the lower-left, white on the upper-right.  Requires EGL
    and EGL_KHR_image, EGL_KHR_image_base, EGL_KHR_gl_texture_2D_image, and
    GL_OES_EGL_image extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBX8888' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBA8888' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES2 pixmap surface as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]

        # Check required extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )

        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       
        
        # Create OpenGLES2 data/objects.
        mesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program1 = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program1 )

        # Create shared pixmap and egl image.
        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENGLES2_SURFACE', 'SCT_USAGE_OPENGLES2_TEXTURE_2D' ],
                                -1 )

        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, -1, 'EGL_NATIVE_PIXMAP_KHR', eglPixmapIndex, [] )

        # Create a texture from egl image.
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )

        texture1Setup = TextureSetup( 0, texture1Index )
        useTexture( modules, texture1Setup, 0, program1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENGL_ES2_BIT' ]
                       ]
        
        # Create pixmap surface from the shared pixmap.
        pixmapConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex, pixmapConfigIndex, eglPixmapIndex, attributes )
        
        pixmapSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurfaceExt( displayIndex, pixmapSurfaceIndex, pixmapConfigIndex, eglPixmapIndex, [] )

        pixmapContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pixmapContextIndex, pixmapConfigIndex, -1, 2 )

        Egl.MakeCurrent( displayIndex, pixmapSurfaceIndex, pixmapSurfaceIndex, pixmapContextIndex )

        # Draw graphics to pixmap surface.
        program2 = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program2 )

        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             'OPENGLES2_RGB565' )

        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        texture1Setup = TextureSetup( 0, texture1Index )
        useTexture( modules, texture1Setup, 0, program1 )
        
        OpenGLES2.Viewport( 0, 0, self.width, self.height );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        # Restore the original surface and draw the texture from the egl
        # image/pixmap surface.
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################            
class EglImageAsRenderbufferExt( vt.VisualTestCase ):
    
    ''' Test creating an OpenGLES2 renderbuffer from a shared pixmap EGL
    image. Image size and format varies. Requires EGL and EGL_KHR_image,
    EGL_KHR_image_base, EGL_KHR_image_pixmap, EGL_KHR_gl_renderbuffer_image,
    EGL_KHR_gl_texture_2D_image, and GL_OES_EGL_image extensions. Requires
    EGL_NOK_pixmap_type_rsgimage extension in Symbian.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 255, 255, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 255, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 255, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 257, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 257, 'SCT_COLOR_FORMAT_RGB565' ),                         
                         ( 257, 257, 'SCT_COLOR_FORMAT_RGB565' ),                         
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBX8888' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBA8888' ) ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES2 egl image as renderbuffer %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]

        # Check required extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_pixmap' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_renderbuffer_image' )        
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )

        if target.supportFeature( 'SYMBIAN' ) == True:
                Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )       

        # Create shared pixmap and egl image.
        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        Images.CreateData( imagesDataIndex,
                           'IMAGES_FLOWER',
                           IMAGES_FORMAT[ self.format ],
                           self.width, self.height )

        eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
        Images.CopyToEgl( imagesDataIndex,
                          eglDataIndex )

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENGLES2_TEXTURE_2D', 'SCT_USAGE_OPENGLES2_RENDERBUFFER' ],
                                eglDataIndex )

        imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]
        
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, -1, 'EGL_NATIVE_PIXMAP_KHR', eglPixmapIndex, imageAttributes )

        # Create OpenGLES2 data/objects.
        mesh            = createPlane( self.gridx, self.gridy, 0.5, 0.5, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
        
        # Create framebuffer.
        framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        # Create a renderbuffer from egl image.
        renderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
        OpenGLES2.GenRenderbuffer( renderbufferIndex )
        OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', renderbufferIndex )
        
        OpenGLES2.EglImageTargetRenderbufferStorage( 'GL_RENDERBUFFER', eglImageIndex )
        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetRenderbufferStorage' )

        # Activate the renderbuffer.
        OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_RENDERBUFFER', renderbufferIndex )
        OpenGLES2.CheckFramebufferStatus( 'GL_FRAMEBUFFER' )

        # Clear lower left corner of the renderbuffer.
        OpenGLES2.Viewport( 0, 0, self.width, self.height );
        OpenGLES2.Scissor( 0, 0, self.width / 2, self.height / 2 )
        OpenGLES2.Enable( 'GL_SCISSOR_TEST' )
        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 0.3 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )       
        OpenGLES2.Disable( 'GL_SCISSOR_TEST' )

        # Unbind framebuffer
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )
        
        # Create a texture from the egl image and draw.
        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )
        
        textureSetup = TextureSetup( 0, textureIndex )
        useTexture( modules, textureSetup, 0, program )

        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 0.3 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )       
        drawVertexData( modules, vertexDataSetup )
        
        OpenGLES2.CheckError( '' )
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

        
######################################################################            
class TextureFromRenderbufferExt( vt.VisualTestCase ):
    
    ''' Test creating an EGL image from renderbuffer, and a texture from the EGL
    image. Test first draws into the renderbuffer and then draws the
    texture. Texture size and format varies. Requires EGL and EGL_KHR_image,
    EGL_KHR_image_base, EGL_KHR_gl_renderbuffer_image,
    EGL_KHR_gl_texture_2D_image, and GL_OES_EGL_image extensions.
    '''

    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'GL_RGB565' ), ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES2 texture from renderbuffer %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]

        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_renderbuffer_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_2D_image' )
        
        OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )        

        # Create OpenGLES2 data/objects.
        mesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        defaultFramebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.CurrentFramebuffer( defaultFramebufferIndex )
                
        # Create framebuffer and renderbuffer.
        framebufferIndex = indexTracker.allocIndex( 'OPENGLES2_FRAMEBUFFER_INDEX' )
        OpenGLES2.GenFramebuffer( framebufferIndex )
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', framebufferIndex )

        renderbufferIndex = indexTracker.allocIndex( 'OPENGLES2_RENDERBUFFER_INDEX' )
        OpenGLES2.GenRenderbuffer( renderbufferIndex )
        OpenGLES2.BindRenderbuffer( 'GL_RENDERBUFFER', renderbufferIndex )
        OpenGLES2.RenderbufferStorage( 'GL_RENDERBUFFER', self.format, self.width, self.width )

        OpenGLES2.FramebufferRenderbuffer( 'GL_FRAMEBUFFER', 'GL_COLOR_ATTACHMENT0', 'GL_RENDERBUFFER', renderbufferIndex )
        
        # Create egl image from renderbuffer.
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex, eglImageIndex, contextIndex, 'EGL_GL2_RENDERBUFFER_KHR', renderbufferIndex, [] )

        # Create texture from renderbuffer->egl image.
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImageIndex )
        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )
        
        # Create a gradient texture for drawing.
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             'OPENGLES2_RGB565' )

        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        # Draw the gradient texture to framebuffer/renderbuffer.
        OpenGLES2.Viewport( 0, 0, self.width, self.height );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )
        
        OpenGLES2.BindFramebuffer( 'GL_FRAMEBUFFER', defaultFramebufferIndex )

        # Draw the renderbuffer->egl image texture.
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )       
        
######################################################################            
class EglImageFromCubemapExt( vt.VisualTestCase ):
    
    ''' Test creating an EGL image from a cubemap texture. Texture size and
    format varies. Requires EGL and EGL_KHR_image, EGL_KHR_image_base,
    EGL_KHR_gl_texture_cubemap_image, and GL_OES_EGL_image extensions.
    '''

    testValues = { ( 'size', 'format' ) :
                           [ ( 256, 'OPENGLES2_LUMINANCE8' ),
                             ( 256, 'OPENGLES2_ALPHA8' ),
                             ( 64,  'OPENGLES2_RGB565' ),
                             ( 128, 'OPENGLES2_RGB565' ),
                             ( 255, 'OPENGLES2_RGB565' ),
                             ( 256, 'OPENGLES2_RGB565' ),
                             ( 257, 'OPENGLES2_RGB565' ),
                             ( 256, 'OPENGLES2_RGB888' ),
                             ( 256, 'OPENGLES2_RGBA4444' ),
                             ( 256, 'OPENGLES2_RGBA5551' ),
                             ( 256, 'OPENGLES2_RGBA8888' ), ]
                   }

    def __init__( self, size, format ):
        vt.VisualTestCase.__init__( self )
        self.size      = size
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES2 egl image from cubemap %dx%d %s' % ( self.size, self.size, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        Test      = modules[ 'Test' ]
        
        indexTracker = IndexTracker()
        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        contextIndex = state[ 3 ]        

        # Check extensions.
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image_base' )
        Egl.CheckExtension( displayIndex, 'EGL_KHR_gl_texture_cubemap_image' )
        
        OpenGLES2.CheckExtension( 'GL_OES_EGL_image' )
         
        colors = [ [ 1.0, 0.0, 0.0, 1 ],  ## positive x, RED
                   [ 0.0, 1.0, 0.0, 1 ],  ## negative x, GREEN
                   [ 0.0, 0.0, 1.0, 1 ],  ## positive y, BLUE
                   [ 1.0, 1.0, 0.0, 1 ],  ## negative y, CYAN
                   [ 1.0, 0.0, 1.0, 1 ],  ## positive z, MAGENDA
                   [ 1.0, 1.0, 1.0, 1 ] ] ## negative z, WHITE

        # Create cubemap.
        cubemapTextureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( cubemapTextureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', cubemapTextureIndex )

        faces = [ 'GL_TEXTURE_CUBE_MAP_POSITIVE_X',
                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_X',
                  'GL_TEXTURE_CUBE_MAP_POSITIVE_Y',
                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_Y',
                  'GL_TEXTURE_CUBE_MAP_POSITIVE_Z',
                  'GL_TEXTURE_CUBE_MAP_NEGATIVE_Z' ]

        nullTextureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateNullTextureData( nullTextureDataIndex,
                                         self.size,
                                         self.size,
                                         'OFF',
                                         self.format )
             
        # Set cubemap face texture data size. 
        for i in range( 6 ):
                OpenGLES2.TexImage2D( nullTextureDataIndex, faces[ i ] );

        OpenGLES2.TexParameter( 'GL_TEXTURE_CUBE_MAP', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE','GL_CLAMP_TO_EDGE' )
        OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', -1 )
        
        eglFaces = [ 'EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_X_KHR',
                     'EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR',
                     'EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR',
                     'EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR',
                     'EGL_GL2_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR',
                     'EGL_GL2_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR', ]

        # Create egl images from cubemap faces.
        eglImages = []        
        for i in range( 6 ):
                eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
                Egl.CreateImageExt( displayIndex,
                                    eglImageIndex,
                                    contextIndex,
                                    eglFaces[ i ],
                                    cubemapTextureIndex,
                                    [] )
                
                eglImages.append( eglImageIndex )

        # Create textures from egl images.
        textures = []
        for i in range( 6 ):
                textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
                OpenGLES2.GenTexture( textureIndex )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
                OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

                # Set texture data from egl image. 
                OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_2D', eglImages[ i ] )
                OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )

                textures.append( textureIndex )

        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', -1 )
                
        # Modify cubemap face data.
        OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', cubemapTextureIndex )
        for i in range( 6 ):
                textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
                OpenGLES2.CreateSolidTextureData( textureDataIndex,
                                                  colors[ i ],
                                                  self.size,
                                                  self.size,
                                                  'OFF',
                                                  self.format )
                OpenGLES2.TexSubImage2D( textureDataIndex, faces[ i ], 0, 0 );

        OpenGLES2.BindTexture( 'GL_TEXTURE_CUBE_MAP', -1 )
                
        # Create OpenGLES2 data/objects
        mesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )
                
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES2.Enable( 'GL_SCISSOR_TEST' )
        w = WIDTH / 6
        for i in range( 6 ):
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textures[ i ] )
                OpenGLES2.Scissor( i * w, 0, WIDTH - i * w, HEIGHT )
                drawVertexData( modules, vertexDataSetup )

        OpenGLES2.CheckError( '' )
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               
        
######################################################################            
class PbufferAsTexture( vt.VisualTestCase ):
    
    ''' Test using an EGL pbuffer surface as an OpenGLES2 texture. The test
    should draw back-and-white diagonal gradient to the surface; black on the
    lower-left, white on the upper-right. Requires EGL.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 255, 255, 'OPENGLES2_RGB565' ),
                         ( 256, 255, 'OPENGLES2_RGB565' ),
                         ( 255, 256, 'OPENGLES2_RGB565' ),
                         ( 256, 256, 'OPENGLES2_RGB565' ),                                                  
                         ( 257, 256, 'OPENGLES2_RGB565' ),
                         ( 257, 257, 'OPENGLES2_RGB565' ),                         
                         ( 256, 256, 'OPENGLES2_RGB888' ),
                         ( 256, 256, 'OPENGLES2_RGBA8888' ) ]
                   }
    
    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES2 egl pbuffer as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]
        
        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        if p[ 4 ] == 0:
            rgbTexture    = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        else:
            rgbaTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufSurfaceIndex,
                                  pbufConfigIndex,
                                  self.width, self.height,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, 2 )
        Egl.MakeCurrent( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )
       
        mesh              = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )
        vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
        
        program1 = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )               
        useVertexData( modules, indexTracker, vertexDataSetup, program1 )
        
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                             [ 0.0, 0.0, 0.0, 1.0 ],
                                             [ 1.0, 1.0, 1.0, 1.0 ],
                                             self.width, self.height,
                                             'OFF',
                                             self.format )
        
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )

        texture1Setup = TextureSetup( textureDataIndex, texture1Index )
        useTexture( modules, texture1Setup, 0, program1 )

        OpenGLES2.Viewport( 0, 0, self.width, self.height );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )               
        drawVertexData( modules, vertexDataSetup )
        
        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        program2 = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )               
        useVertexData( modules, indexTracker, vertexDataSetup, program2 )
       
        texture2Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture2Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture2Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        Egl.BindTexImage( displayIndex, pbufSurfaceIndex, 'EGL_BACK_BUFFER' )

        texture2Setup = TextureSetup( 0, texture2Index )
        useTexture( modules, texture2Setup, 0, program2 )
        
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )       

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################            
class PbufferWithMipmapsAsTexture( vt.VisualTestCase ):
    
    ''' Test using a EGL pbuffer surface with mipmaps as an OpenGLES2
    texture. Draws a solid green texture scaled across the surface. Requires
    EGL.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) : [ ( 512, 512, 'OPENGLES2_RGB565' ),
                                                       ( 512, 512, 'OPENGLES2_RGB888' ),
                                                       ( 512, 512, 'OPENGLES2_RGBA8888' ) ]
                   }
    
    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES2 egl pbuffer with mipmaps as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        WIDTH  = 240
        HEIGHT = 240
        
        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]
       
        textureW = self.width
        textureH = self.height
        
        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        if p[ 4 ] == 0:
            rgbTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        else:
            rgbaTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufSurfaceIndex,
                                  pbufConfigIndex,
                                  textureW, textureH,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'ON',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, 2 )
        Egl.MakeCurrent( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )

        w     = textureW
        h     = textureH
        level = 0
        
        while True:
                Egl.SurfaceAttrib( displayIndex, pbufSurfaceIndex, EGL_DEFINES[ 'EGL_MIPMAP_LEVEL' ], level )
                OpenGLES2.Viewport( 0, 0, w, h );
                if level == 1:
                        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                else:
                        OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                if w == 1 and h == 1:
                        break
                
                w     = w / 2
                h     = h / 2
                level = level + 1

                if w < 1:
                        w = 1
                        
                if h < 1:
                        h = 1

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        rx = 1.0
        ry = 1.0

        mesh              = createPlane( self.gridx, self.gridy, rx, ry, False, [ True ] )
        vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
        program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )               
        useVertexData( modules, indexTracker, vertexDataSetup, program )
               
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        Egl.BindTexImage( displayIndex, pbufSurfaceIndex, 'EGL_BACK_BUFFER' )
       
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )        
        drawVertexData( modules, vertexDataSetup )       

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################            
class NpotPbufferWithMipmapsAsTextureExt( vt.VisualTestCase ):
    
    ''' Test using a NPOT EGL pbuffer surface with mipmaps as an OpenGLES2
    texture. Draws a solid green texture scaled across the surface. Requires EGL
    and GL_OES_texture_npot extension.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) : [ ( 511, 512, 'OPENGLES2_RGB565' ),
                                                       ( 513, 512, 'OPENGLES2_RGB565' ),
                                                       ( 512, 513, 'OPENGLES2_RGB565' ),
                                                       ( 513, 513, 'OPENGLES2_RGB565' ),
                                                       ( 512, 511, 'OPENGLES2_RGB888' ),
                                                       ( 511, 513, 'OPENGLES2_RGBA8888' ) ]
                   }
    
    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES2 npot egl pbuffer with mipmaps as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        WIDTH  = 240
        HEIGHT = 240
        
        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
        
        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]
       
        textureW = self.width
        textureH = self.height
        
        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        if p[ 4 ] == 0:
            rgbTexture  = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        else:
            rgbaTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufSurfaceIndex,
                                  pbufConfigIndex,
                                  textureW, textureH,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'ON',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, 2 )
        Egl.MakeCurrent( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )

        w     = textureW
        h     = textureH
        level = 0
        
        while True:
                Egl.SurfaceAttrib( displayIndex, pbufSurfaceIndex, EGL_DEFINES[ 'EGL_MIPMAP_LEVEL' ], level )
                OpenGLES2.Viewport( 0, 0, w, h );
                if level == 1:
                        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
                else:
                        OpenGLES2.ClearColor( [ 1.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                if w == 1 and h == 1:
                        break
                
                w     = w / 2
                h     = h / 2
                level = level + 1

                if w < 1:
                        w = 1
                        
                if h < 1:
                        h = 1

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        rx = 1.0
        ry = 1.0

        mesh              = createPlane( self.gridx, self.gridy, rx, ry, False, [ True ] )
        vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
        program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )               
        useVertexData( modules, indexTracker, vertexDataSetup, program )
               
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        Egl.BindTexImage( displayIndex, pbufSurfaceIndex, 'EGL_BACK_BUFFER' )
       
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )        
        drawVertexData( modules, vertexDataSetup )       

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################            
class PbufferWithGeneratedMipmapsAsTexture( vt.VisualTestCase ):
    
    ''' Test using an EGL pbuffer surface with generated mipmaps as an OpenGLES2
    texture. Draws a solid green texture scaled across the surface. Requires
    EGL.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) : [ ( 255, 255, 'OPENGLES2_RGB565' ),
                                                       ( 255, 256, 'OPENGLES2_RGB565' ),
                                                       ( 256, 255, 'OPENGLES2_RGB565' ),
                                                       ( 256, 256, 'OPENGLES2_RGB565' ),
                                                       ( 257, 256, 'OPENGLES2_RGB565' ),
                                                       ( 256, 257, 'OPENGLES2_RGB565' ),
                                                       ( 257, 257, 'OPENGLES2_RGB565' ),
                                                       ( 256, 255, 'OPENGLES2_RGB888' ),
                                                       ( 255, 257, 'OPENGLES2_RGBA8888' ) ]
                   }
    
    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES2 egl pbuffer with generated mipmaps as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]
       
        textureW = self.width
        textureH = self.height
        
        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        if p[ 4 ] == 0:
            rgbTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        else:
            rgbaTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufSurfaceIndex,
                                  pbufConfigIndex,
                                  textureW, textureH,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'ON',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, 2 )
        Egl.MakeCurrent( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )

        w     = textureW
        h     = textureH
        
        OpenGLES2.Viewport( 0, 0, w, h );
        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        rx = 2.0 * float( textureW ) / float( WIDTH )
        ry = 2.0 * float( textureH ) / float( HEIGHT )

        mesh              = createPlane( self.gridx, self.gridy, rx, ry, False, [ True ] )
        vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
        program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )               
        useVertexData( modules, indexTracker, vertexDataSetup, program )
               
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        Egl.BindTexImage( displayIndex, pbufSurfaceIndex, 'EGL_BACK_BUFFER' )
        OpenGLES2.GenerateMipmap( 'GL_TEXTURE_2D' )
        
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )        
        drawVertexData( modules, vertexDataSetup )       

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################            
class NpotPbufferWithGeneratedMipmapsAsTextureExt( vt.VisualTestCase ):
    
    ''' Test using an NPOT EGL surface with generated mipmaps as an OpenGLES2
    texture. Draws a solid green texture scaled across the surface. Requires EGL
    and GL_OES_texture_npot extension.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) : [ ( 255, 255, 'OPENGLES2_RGB565' ),
                                                       ( 255, 256, 'OPENGLES2_RGB565' ),
                                                       ( 256, 255, 'OPENGLES2_RGB565' ),
                                                       ( 257, 256, 'OPENGLES2_RGB565' ),
                                                       ( 256, 257, 'OPENGLES2_RGB565' ),
                                                       ( 257, 257, 'OPENGLES2_RGB565' ),
                                                       ( 256, 255, 'OPENGLES2_RGB888' ),
                                                       ( 255, 257, 'OPENGLES2_RGBA8888' ) ]
                   }
    
    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.repeats   = 1
        self.name = 'OPENGLES2 npot egl pbuffer with generated mipmaps as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                uniform sampler2D gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        OpenGLES2.CheckExtension( 'GL_OES_texture_npot' )
        
        displayIndex = state[ 0 ]
        surfaceIndex = state[ 2 ]
        contextIndex = state[ 3 ]
       
        textureW = self.width
        textureH = self.height
        
        p = SURFACE_PARAMETERS[ self.format ]

        rgbTexture    = 'EGL_DONT_CARE'
        rgbaTexture   = 'EGL_DONT_CARE'        
        textureFormat = 'EGL_NO_TEXTURE'
        
        if p[ 4 ] == 0:
            rgbTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGB'
        else:
            rgbaTexture = 'EGL_TRUE'
            textureFormat = 'EGL_TEXTURE_RGBA'
        
        pbufConfigIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       pbufConfigIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       rgbTexture,
                       rgbaTexture,
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT'] )

        pbufSurfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  pbufSurfaceIndex,
                                  pbufConfigIndex,
                                  textureW, textureH,
                                  textureFormat,
                                  'EGL_TEXTURE_2D',
                                  'ON',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )

        pbufContextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, pbufContextIndex, pbufConfigIndex, -1, 2 )
        Egl.MakeCurrent( displayIndex, pbufSurfaceIndex, pbufSurfaceIndex, pbufContextIndex )

        w     = textureW
        h     = textureH
        
        OpenGLES2.Viewport( 0, 0, w, h );
        OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        rx = 2.0 * float( textureW ) / float( WIDTH )
        ry = 2.0 * float( textureH ) / float( HEIGHT )

        mesh              = createPlane( self.gridx, self.gridy, rx, ry, False, [ True ] )
        vertexDataSetup   = setupVertexData( modules, indexTracker, mesh )
        program           = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )               
        useVertexData( modules, indexTracker, vertexDataSetup, program )
               
        texture1Index = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( texture1Index )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture1Index )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST_MIPMAP_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        Egl.BindTexImage( displayIndex, pbufSurfaceIndex, 'EGL_BACK_BUFFER' )
        OpenGLES2.GenerateMipmap( 'GL_TEXTURE_2D' )
        
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()

        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )        
        drawVertexData( modules, vertexDataSetup )       

        OpenGLES2.CheckError( '' )                        
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################
class WindowSurface( vt.VisualTestCase ):
    
    ''' Test for window surfaces. Test draws a green surface. Requires EGL.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGB888',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENGLES2 window surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        windowIndex = 0
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, WIDTH, HEIGHT, 'SCT_COLOR_FORMAT_DEFAULT' )

        # Create pbuffer from the image.
        p = SURFACE_PARAMETERS2[ self.format ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT'] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )       
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, 2 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES2.ClearColor( [ 0, 1, 0, 1 ] )       
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES2.CheckError( '' )               
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class PbufferSurface( vt.VisualTestCase ):
    
    ''' Test for pbuffer surfaces. Test draws a green surface. Requires EGL.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGB888',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENGLES2 pbuffer surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        p = SURFACE_PARAMETERS2[ self.format ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_PBUFFER_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT'] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePbufferSurface( displayIndex,
                                  surfaceIndex,
                                  configIndex,
                                  WIDTH,
                                  HEIGHT,
                                  'EGL_NO_TEXTURE', 'EGL_NO_TEXTURE',
                                  'OFF',
                                  'OFF',
                                  'EGL_NONE',
                                  'EGL_NONE' )
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, 2 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )       

        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES2.ClearColor( [ 0, 1, 0, 1 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES2.CheckError( '' )               
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################
class PixmapSurface( vt.VisualTestCase ):
    
    ''' Test for pixmap surfaces. Test draws a green surface. Requires EGL.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGB888',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENGLES2 pixmap surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreatePixmap( pixmapIndex, WIDTH, HEIGHT, self.format, -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENGL_ES2_BIT' ]
                       ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex,  configIndex,  pixmapIndex, attributes )
        
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 pixmapIndex,
                                 'EGL_NONE',
                                 'EGL_NONE' )
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, 2 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES2.ClearColor( [ 0, 1, 0, 1 ] )        
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES2.CheckError( '' )               
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
        
######################################################################
class SharedPixmapSurface( vt.VisualTestCase ):
    
    ''' Test for shared pixmap surfaces. Test draws a green surface. Requires
    EGL. Requires EGL_NOK_pixmap_type_rsgimage extension in Symbian.'
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.repeats = 1        
        self.name = 'OPENGLES2 shared pixmap surface %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()

        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        # Check extensions.
        if target.supportFeature( 'SYMBIAN' ) == True:
            Egl.CheckExtension( displayIndex, 'EGL_NOK_pixmap_type_rsgimage' )

        pixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( pixmapIndex,
                                WIDTH, HEIGHT,
                                self.format,
                                [ 'SCT_USAGE_OPENGLES2_SURFACE' ],
                                -1 )

        attributes = [ EGL_DEFINES[ 'EGL_RENDERABLE_TYPE' ],   EGL_DEFINES[ 'EGL_OPENGL_ES2_BIT' ]
                       ]
        
        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.PixmapConfigExt( displayIndex, configIndex, pixmapIndex, attributes )
        
        attributes = []
        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )
        Egl.CreatePixmapSurfaceExt( displayIndex, surfaceIndex, configIndex, pixmapIndex, attributes )
        
        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, 2 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )
        
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES2.ClearColor( [ 0, 1, 0, 1 ] )
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

        OpenGLES2.CheckError( '' )               
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

        
######################################################################            
class EglImageExternalAsTextureExt( vt.VisualTestCase ):
    
    ''' Test creating an OpenGLES2 texture from an external EGL image,
    preserving EGL image buffer pixels. Draws a flower scaled to the
    surface. Image size and format varies. Requires EGL and
    EGL_KHR_gl_texture_2D_image, and GL_OES_EGL_image_external extensions.
    '''
   
    testValues = { ( 'width', 'height', 'format' ) :
                       [ ( 256, 256, 'SCT_COLOR_FORMAT_RGB565' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_RGBA8888' ),
                         ( 256, 256, 'SCT_COLOR_FORMAT_YUV12' ) ]
                   }

    def __init__( self, width, height, format ):
        vt.VisualTestCase.__init__( self )
        self.width     = width
        self.height    = height
        self.format    = format
        self.gridx     = 2
        self.gridy     = 2
        self.overdraw  = 1
        self.repeats   = 1
        self.name = 'OPENGLES2 egl image external as texture %dx%d %s' % ( self.width, self.height, self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Images    = modules[ 'Images' ]        
        Gltest    = modules[ 'Gltest' ]
        
        indexTracker = IndexTracker()

        vertexShaderSource = """
                precision mediump float;
                attribute vec4 gVertex;
                attribute vec4 gTexCoord0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                    gVSTexCoord0 = gTexCoord0;
                }"""

        fragmentShaderSource = """
                precision mediump float;
                \\n#extension GL_OES_EGL_image_external : require\\n
                uniform samplerExternalOES gTexture0;
                varying vec4 gVSTexCoord0;

                void main()
                {
                    gl_FragColor = texture2D( gTexture0, gVSTexCoord0.xy );
                }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        defaultAttributes = target.getDefaultAttributes( apis )
        state = target.setup( modules,
                              indexTracker,
                              WIDTH, HEIGHT,
                              apis,
                              defaultAttributes )

        displayIndex = state[ 0 ]

        # Check required extensions.        
        Egl.CheckExtension( displayIndex, 'EGL_KHR_image' )
        
        OpenGLES2.CheckExtension( 'GL_OES_EGL_image_external' )
        
        # Create image data, copy to egl, and create a shared pixmap from the
        # data.
        imagesDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )
        
        if self.format == 'SCT_COLOR_FORMAT_YUV12':
                imagesRawDataIndex = indexTracker.allocIndex( 'IMAGES_DATA_INDEX' )                
                Images.CreateData( imagesRawDataIndex,
                                   'IMAGES_FLOWER',
                                   'IMAGES_RGB8',
                                   self.width, self.height )
                Images.ConvertData( imagesRawDataIndex,
                                    imagesDataIndex,
                                    'IMAGES_YUV12' )
        else:
                Images.CreateData( imagesDataIndex,
                                   'IMAGES_FLOWER',
                                   IMAGES_FORMAT[ self.format ],
                                   self.width, self.height )
                
        eglDataIndex = indexTracker.allocIndex( 'EGL_DATA_INDEX' )
        Images.CopyToEgl( imagesDataIndex, eglDataIndex )

        # Create OpenGLES2 data/objects.
        mesh            = createPlane( self.gridx, self.gridy, 1.0, 1.0, False, [ True ] )        
        vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
        
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
        useVertexData( modules, indexTracker, vertexDataSetup, program )

        OpenGLES2.ActiveTexture( 0 )

        eglPixmapIndex = indexTracker.allocIndex( 'EGL_PIXMAP_INDEX' )
        Egl.CreateSharedPixmap( eglPixmapIndex,
                                self.width, self.height,
                                self.format,
                                [ 'SCT_USAGE_OPENGLES2_TEXTURE_2D' ],
                                eglDataIndex )

        # Create egl image from shared pixmap.
        imageAttributes = [ EGL_DEFINES[ 'EGL_IMAGE_PRESERVED_KHR' ], EGL_DEFINES[ 'EGL_TRUE' ] ]
        eglImageIndex = indexTracker.allocIndex( 'EGL_IMAGE_INDEX' )
        Egl.CreateImageExt( displayIndex,
                            eglImageIndex,
                            -1,
                            'EGL_NATIVE_BUFFER',
                            eglPixmapIndex,
                            imageAttributes )
        
        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_EXTERNAL_OES', textureIndex )
    
        # Set texture data from egl image. 
        OpenGLES2.EglImageTargetTexture2D( 'GL_TEXTURE_EXTERNAL_OES', eglImageIndex )

        OpenGLES2.TexParameter( 'GL_TEXTURE_EXTERNAL_OES', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )
        #OpenGLES2.TexParameter( 'GL_TEXTURE_2D', 'GL_NEAREST', 'GL_NEAREST', 'GL_CLAMP_TO_EDGE', 'GL_CLAMP_TO_EDGE' )

        OpenGLES2.CheckError( 'OpenGLES2.EglImageTargetTexture2D' )

        OpenGLES2.Uniformi( program.textureLocationIndices[ 0 ], 1, [ 0 ] )
        
        OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
        OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
        
        # ------------------------------------------------------------
        # Start benchmark actions
        self.beginBenchmarkActions()
                
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        drawVertexData( modules, vertexDataSetup )

        OpenGLES2.CheckError( '' )
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )               

        
######################################################################
def createPlane0( gridx, gridy ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh


######################################################################
class SwapBuffersPreserved( vt.VisualTestCase ):
    
    ''' Test egl swap buffers preserved. Test draws a blue-green-red
    surface. Requires EGL.
    '''

    testValues = { 'format' : [ 'SCT_COLOR_FORMAT_RGB565',
                                'SCT_COLOR_FORMAT_RGB888',
                                'SCT_COLOR_FORMAT_RGBX8888',
                                'SCT_COLOR_FORMAT_RGBA8888' ]
                   }
                   
    def __init__( self, format ):
        vt.VisualTestCase.__init__( self )
        self.format  = format
        self.gridx   = 2
        self.gridy   = 2
        self.repeats = 1        
        self.name = 'OPENGLES2 swap buffers preserved %s' % ( self.format, )
    
    def build( self, target, modules ):
        Egl       = modules[ 'Egl' ]
        OpenGLES2 = modules[ 'OpenGLES2' ]
        Gltest    = modules[ 'Gltest' ]

        indexTracker = IndexTracker()

        vertexShaderSource = """
            precision mediump float;
            attribute vec4 gVertex;
            attribute vec4 gColor;
            varying vec4 gVSColor;

            void main()
            {
                gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                gVSColor = gColor;
            }"""

        fragmentShaderSource = """
            precision mediump float;
            varying vec4 gVSColor;

            void main()
            {
                gl_FragColor = gVSColor;
            }"""

        vsSource = formatShader( vertexShaderSource )
        fsSource = formatShader( fragmentShaderSource )
        
        # ----------------------------------------------------------------------
        self.beginInitActions()

        apis = [ API_OPENGLES2 ]
        
        displayIndex = indexTracker.allocIndex( 'EGL_DISPLAY_INDEX' )
        Egl.GetDisplay( displayIndex, 'SCT_SCREEN_DEFAULT' );
        Egl.Initialize( displayIndex )
        
        windowIndex = 0
        Egl.CreateWindow( windowIndex, 'SCT_SCREEN_DEFAULT', 0, 0, WIDTH, HEIGHT, 'SCT_COLOR_FORMAT_DEFAULT' )

        # Create pbuffer from the image.
        p = SURFACE_PARAMETERS2[ self.format ]

        configIndex = indexTracker.allocIndex( 'EGL_CONFIG_INDEX' )
        Egl.RGBConfig( displayIndex,
                       configIndex,
                       p[ 0 ],
                       p[ 1 ],
                       p[ 2 ],
                       p[ 3 ],
                       p[ 4 ],
                       '-',
                       '-',
                       0,
                       '>=0',
                       '-',
                       'EGL_DONT_CARE',
                       'EGL_DONT_CARE',
                       [ 'EGL_WINDOW_BIT', 'EGL_SWAP_BEHAVIOR_PRESERVED_BIT' ],
                       [ 'EGL_OPENGL_ES2_BIT'] )

        surfaceIndex = indexTracker.allocIndex( 'EGL_SURFACE_INDEX' )       
        Egl.CreateWindowSurface( displayIndex,
                                 surfaceIndex,
                                 configIndex,
                                 windowIndex,
                                 'EGL_BACK_BUFFER',
                                 'EGL_NONE',
                                 'EGL_NONE' )

        contextIndex = indexTracker.allocIndex( 'EGL_CONTEXT_INDEX' )
        Egl.CreateContext( displayIndex, contextIndex, configIndex, -1, 2 )

        Egl.MakeCurrent( displayIndex, surfaceIndex, surfaceIndex, contextIndex )

        Egl.SurfaceAttrib( displayIndex,
                           surfaceIndex,
                           EGL_DEFINES[ 'EGL_SWAP_BEHAVIOR' ],
                           EGL_DEFINES[ 'EGL_BUFFER_PRESERVED' ] ) 

        mesh1 = createPlane0( self.gridx, self.gridy )
        mesh1.gradientColors( [ 0.0, 0.0, 1.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

        mesh2 = createPlane0( self.gridx, self.gridy )
        mesh2.gradientColors( [ 1.0, 0.0, 0.0, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                
        vertexDataSetup1 = setupVertexData( modules, indexTracker, mesh1 )
        vertexDataSetup2 = setupVertexData( modules, indexTracker, mesh2 )
                
        program = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup1 )
        useVertexData( modules, indexTracker, vertexDataSetup1, program )
       
        # ----------------------------------------------------------------------
        self.beginBenchmarkActions()

        OpenGLES2.ClearColor( [ 0, 1, 0, 1 ] )       
        OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )
        
        Egl.SwapBuffers( displayIndex, surfaceIndex )

        OpenGLES2.Scissor( 0, 0, WIDTH / 3, HEIGHT )
        OpenGLES2.Enable( 'GL_SCISSOR_TEST' )
        
        drawVertexData( modules, vertexDataSetup1 )

        Egl.SwapBuffers( displayIndex, surfaceIndex )

        OpenGLES2.Scissor( WIDTH * 2 / 3, 0, WIDTH, HEIGHT )
                
        useVertexData( modules, indexTracker, vertexDataSetup2, program )               
        drawVertexData( modules, vertexDataSetup2 )

        Egl.SwapBuffers( displayIndex, surfaceIndex )
                
        OpenGLES2.CheckError( '' )               
        Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

    
######################################################################
class Extensions( vt.VisualTestCase ):

        ''' The purpose of the test: test OpenGLES2 EGL extensions.

        Expected output: surface cleared to green if the particular OpenGLES2
        EGL extension is supported. Viewport size 320*240.
        '''
        
        testValues = { ( 'extension', 'functions' ) : [ ( 'EGL_KHR_lock_surface', [ 'eglLockSurfaceKHR',
                                                                                    'eglUnlockSurfaceKHR' ] ),
                                                        ( 'EGL_KHR_image', [] ),
                                                        ( 'EGL_KHR_vg_parent_image', [] ),
                                                        ( 'EGL_KHR_gl_texture_2D_image', [] ),
                                                        ( 'EGL_KHR_gl_texture_cubemap_image', [] ),
                                                        ( 'EGL_KHR_gl_renderbuffer_image', [] ),
                                                        ( 'EGL_KHR_image_base', [ 'eglCreateImageKHR',
                                                                                  'eglDestroyImageKHR' ] ),
                                                        ( 'EGL_KHR_image_pixmap', [] ),
                                                        ( 'EGL_KHR_lock_surface2', [ 'eglLockSurfaceKHR',
                                                                                     'eglUnlockSurfaceKHR' ] ),
                                                        ( 'EGL_NOK_resource_profiling', [ 'eglQueryProfilingDataNOK' ] ),
                                                        ( 'EGL_NOK_resource_profiling2', [ 'eglQueryProfilingDataNOK' ] ),
                                                        ( 'EGL_NOK_surface_scaling', [ 'eglSetSurfaceScalingNOK',
                                                                                       'eglQuerySurfaceScalingCapabilityNOK' ] ),
                                                        ( 'EGL_NOK_pixmap_type_rsgimage', [] ),
                                                        ( 'EGL_NOK_window_type_eglwindowbase', [] ),
                                                        ( 'EGL_SYMBIAN_COMPOSITION', [] ),
                                       ]
                       }
                                                                             
        def __init__( self, extension, functions ):
                vt.VisualTestCase.__init__( self )
                self.repeats = 1
                self.extension = extension
                self.functions = functions
                self.name    = "OPENGLES2 egl extensions, extension=%s" % ( self.extension, )
        
        def build( self, target, modules ):
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Egl       = modules[ 'Egl' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]                
                defaultAttributes = target.getDefaultAttributes( apis )
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                OpenGLES2.ClearColor( [ 0.0, 1.0, 0.0, 1.0 ] )                
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                                
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()               

                Egl.CheckExtension( state[ 0 ], self.extension )
                
                for f in self.functions:
                        Egl.CheckFunction( f )
                        
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.CheckError( '' )               
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
