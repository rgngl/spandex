# -*- mode: python -*-

#
# Spandex benchmark and test framework.
#
# Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Kari J. Kangas <kari.j.kangas@nokia.com>
#
#   This framework is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, version 2.1 of the License.
#
#   This framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
# for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
# along with this framework; if not, see <http://www.gnu.org/licenses/>.
#

import vt

from lib.common  import *
from lib.mesh    import *
from gles2common import *

WIDTH, HEIGHT = ( 320, 240, )
GRIDX, GRIDY  = ( 8, 8 )

######################################################################
def createPlane( gridx, gridy, tcx, tcy, colorAttributes, texCoordAttributes ):
        vertexAttribute   = MeshVertexAttribute( MESH_TYPE_FIXED, 3 )
        colorAttribute    = None
        texCoordAttribute = []
        indexAttribute    = MeshAttribute( MESH_TYPE_UNSIGNED_SHORT )
    
        if colorAttributes:
                colorAttribute = MeshVertexAttribute( MESH_TYPE_UNSIGNED_BYTE, 4 )

        if texCoordAttributes:
                for i in range( len( texCoordAttributes ) ):
                        texCoordAttribute.append( MeshTexCoordAttribute( MESH_TYPE_FIXED,
                                                                         2,
                                                                         [ float( tcx ),
                                                                           float( tcy ) ] ) )

        mesh = MeshStripPlane( gridx,
                               gridy,
                               vertexAttribute,
                               colorAttribute,
                               None,
                               texCoordAttribute,
                               indexAttribute,
                               None,
                               MESH_CCW_WINDING,
                               False )
        
        mesh.translate( [ -0.5, -0.5, 0 ] )
        mesh.scale( [ 2.0, 2.0 ] )

        return mesh
       
######################################################################
def setupTexture( modules, indexTracker, w, h, c1, c2 ):
        OpenGLES2 = modules[ 'OpenGLES2' ]
    
        textureDataIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_DATA_INDEX' )
        OpenGLES2.CreateGradientTextureData( textureDataIndex,
                                             'OPENGLES2_GRADIENT_DIAGONAL',
                                             c1,
                                             c2,
                                             w, h,
                                             'OFF',
                                             'OPENGLES2_RGBA8888' )

        textureIndex = indexTracker.allocIndex( 'OPENGLES2_TEXTURE_INDEX' )
        OpenGLES2.GenTexture( textureIndex )
        OpenGLES2.BindTexture( 'GL_TEXTURE_2D', textureIndex )
        OpenGLES2.TexImage2D( textureDataIndex, 'GL_TEXTURE_2D' )
        OpenGLES2.TexParameter( 'GL_TEXTURE_2D',
                                'GL_LINEAR',
                                'GL_LINEAR',
                                'GL_CLAMP_TO_EDGE',
                                'GL_CLAMP_TO_EDGE' )

        return TextureSetup( textureDataIndex, textureIndex )

######################################################################
class Points( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing points. SAMPLE_BUFFERS 0 is
        used to ensure non-multisample rendering.

        Expected output: 8x8 grid of points drawn using vertex color. Vertex
        colors are defined so that the lower-left vertex has color [0.0, 0.0,
        0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''
        testValues = { ( 'pointSize' ) : [ ( 1 ),
                                           ( 10 ),
                                           ( 20 ),                                           
                                           ]
                       }

        
        def __init__( self, pointSize ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.pointSize = pointSize
                self.gridx     = GRIDX
                self.gridy     = GRIDY
                self.overdraw  = 1
                self.name = "OPENGLES2 points, point size=%.2f" % ( self.pointSize, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                        gl_PointSize = %f;
                    }
                    """ % ( self.pointSize, )

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_SAMPLE_BUFFERS ] = 0
                defaultAttributes[ CONFIG_SAMPLES ]        = '>=0'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 0.9, 0.9 ] )
                
                mesh.indices = []
                for i in range( self.gridx * self.gridy ):
                        mesh.indices.append( i )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.DrawElements( 'GL_POINTS',
                                        vertexDataSetup.indexArrayLength,
                                        0,
                                        vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class RoundPoints( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing rounded points. Roundness is
        calculated at the fragment shader from the gl_PointCoord. SAMPLE_BUFFERS
        0 is used to ensure non-multisample rendering.

        Expected output: 8x8 grid of rounded points drawn using vertex
        color. Vertex colors are defined so that the lower-left vertex has color
        [0.0, 0.0, 0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0,
        1.0, 1.0]. Between those the vertex color is linearly interpolated from
        left to right, from bottom to top. Viewport size 320*240.
        '''
        testValues = { ( 'pointSize' ) : [ ( 10 ),
                                           ( 20 ),
                                           ]
                       }

        
        def __init__( self, pointSize ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.pointSize = pointSize
                self.gridx     = GRIDX
                self.gridy     = GRIDY
                self.overdraw  = 1
                self.name = "OPENGLES2 round points, point size=%.2f" % ( self.pointSize, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                        gl_PointSize = %f;
                    }
                    """ % ( self.pointSize, )

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;
                    void main()
                    {
                        float x, y, d;
                        x=gl_PointCoord.x-0.5;
                        y=gl_PointCoord.y-0.5;
                        d=x*x+y*y;
                        if( d>0.25 )
                        {
                            discard;
                        }
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_SAMPLE_BUFFERS ] = 0
                defaultAttributes[ CONFIG_SAMPLES ]        = '>=0'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 0.9, 0.9 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.DrawArrays( 'GL_POINTS',
                                      0,
                                      self.gridx * self.gridy )    

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class TexturedPoints( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing textured
        points. SAMPLE_BUFFERS 0 is used to ensure non-multisample rendering.

        Expected output: 8x8 grid of points drawn using a gradient texture. A
        gradient texture is scaled across each point. Top-left part of the point
        should be black, lower-right part should be white. Viewport size
        320*240.
        '''
        testValues = { ( 'pointSize' ) : [ ( 10 ),
                                           ( 20 ),
                                           ]
                       }
        
        def __init__( self, pointSize ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.pointSize = pointSize
                self.gridx     = GRIDX
                self.gridy     = GRIDY
                self.overdraw  = 1
                self.name = "OPENGLES2 textured points, point size=%.2f" % ( self.pointSize, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute float gSize;
                    uniform mat4 mvp;

                    void main()
                    {
                        gl_Position = mvp * gVertex;
                        gl_PointSize = gSize / gl_Position.w;
                    }"""
        
                fragmentShaderSource = """
                    precision mediump float;
                    uniform sampler2D gTexture0;
                    void main()
                    {
                        gl_FragColor = texture2D(gTexture0, gl_PointCoord.xy);
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_SAMPLE_BUFFERS ] = 0
                defaultAttributes[ CONFIG_SAMPLES ]        = '>=0'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 0.9, 0.9 ] )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )
                
                texture0Setup = setupTexture( modules,
                                              indexTracker,
                                              32, 32,
                                              [ 0.0, 0.0, 0.0, 1.0 ],
                                              [ 1.0, 1.0, 1.0, 1.0 ] )

                textureUnit = 0
                OpenGLES2.ActiveTexture( textureUnit )
                OpenGLES2.BindTexture( 'GL_TEXTURE_2D', texture0Setup.textureIndex )

                index = indexTracker.allocIndex( 'LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( index, program.programIndex, 'gTexture%d' % textureUnit )
    
                OpenGLES2.Uniformi( index, 1, [ textureUnit ] )
       
                sizes = [ self.pointSize ] * ( self.gridx * self.gridy )
                sizeArrayIndex  = indexTracker.allocIndex( 'OPENGLES2_ARRAY_INDEX' )
                OpenGLES2.CreateArray( sizeArrayIndex, 'GL_FLOAT' )
                OpenGLES2.AppendToArray( sizeArrayIndex, sizes )
        
                sizeLocationIndex = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetAttribLocation( sizeLocationIndex, program.programIndex, 'gSize' )

                OpenGLES2.VertexAttribPointer( sizeLocationIndex,
                                               sizeArrayIndex,
                                               1,
                                               'OFF',
                                               0 )
                OpenGLES2.EnableVertexAttribArray( sizeLocationIndex )
              
                modelViewProjectionLocationIndex = indexTracker.allocIndex( 'LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( modelViewProjectionLocationIndex,
                                              program.programIndex,
                                              'mvp' )

                OpenGLES2.UniformMatrix( modelViewProjectionLocationIndex,
                                         1,
                                         [ 1.0, 0.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0, 0.0,
                                           0.0, 0.0, 1.0, 0.0,
                                           0.0, 0.0, 0.0, 1.0 ] )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                # Clear the screen
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.DrawArrays( 'GL_POINTS',
                                      0,
                                      self.gridx * self.gridy )    

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                                
######################################################################
class Lines( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing lines. SAMPLE_BUFFERS 0 is
        used to ensure non-multisample rendering.

        Expected output: 8 lines spanning from left to right, spaced evenly from
        top to bottom. Vertex colors are defined so that the lower-left vertex
        has color [0.0, 0.0, 0.5, 1.0 ] and the upper-right vertex has color [
        1.0, 1.0, 1.0, 1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Viewport size
        320*240.
        '''
        testValues = { ( 'lineWidth' ) : [ ( 0.1 ),
                                           ( 1 ),
                                           ( 10 ),
                                           ( 20 ),                                           
                                           ]
                       }

        
        def __init__( self, lineWidth ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = lineWidth
                self.gridx     = GRIDX
                self.gridy     = GRIDY
                self.overdraw  = 1
                self.name = "OPENGLES2 lines, line width=%.2f" % ( self.lineWidth, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_SAMPLE_BUFFERS ] = 0
                defaultAttributes[ CONFIG_SAMPLES ]        = '>=0'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( 2, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 0.9, 0.9 ] )
                
                mesh.indices = []
                for i in range( self.gridy ):
                        mesh.indices.append( i * 2 )
                        mesh.indices.append( i * 2 + 1 )
                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.LineWidth( self.lineWidth );               
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                OpenGLES2.DrawElements( 'GL_LINES',
                                        vertexDataSetup.indexArrayLength,
                                        0,
                                        vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class LineStrips( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing line strips. SAMPLE_BUFFERS 0
        is used to ensure non-multisample rendering.

        Expected output: a continuous line snaking from the bottom-left corner
        doing left-left-right-right-right-left-repeat turns. Vertex colors are
        defined so that the lower-left vertex has color [0.0, 0.0, 0.5, 1.0 ]
        and the upper-right vertex has color [ 1.0, 1.0, 1.0, 1.0]. Between
        those the vertex color is linearly interpolated from left to right, from
        bottom to top. Viewport size 320*240.
        '''
        testValues = { ( 'lineWidth' ) : [ ( 1 ),
                                           ( 10 ),
                                           ( 20 ),                                           
                                           ]
                       }
        
        def __init__( self, lineWidth ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = lineWidth
                self.gridx     = 9
                self.gridy     = 9
                self.overdraw  = 1
                self.name = "OPENGLES2 line strips, line width=%.2f" % ( self.lineWidth, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_SAMPLE_BUFFERS ] = 0
                defaultAttributes[ CONFIG_SAMPLES ]        = '>=0'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 0.9, 0.9 ] )

                indicesPerStrip = 8
                mesh.indices = []
                for i in range( 1, self.gridy ):
                        if i % 2 == 1:
                                mesh.indices.append( i - 1 )                                
                                mesh.indices.append( i )
                                mesh.indices.append( i )                  # Add degerated segment
                                mesh.indices.append( i * self.gridx + i )
                                mesh.indices.append( i * self.gridx )
                        else:
                                mesh.indices.append( i * self.gridx )
                                mesh.indices.append( i * self.gridx + i )
                                mesh.indices.append( i )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.LineWidth( self.lineWidth );               
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                # Clear the screen
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerStrip ):
                        OpenGLES2.DrawElements( 'GL_LINE_STRIP',
                                                indicesPerStrip,
                                                i * indicesPerStrip,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class LineLoops( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing line loops. SAMPLE_BUFFERS 0
        is used to ensure non-multisample rendering.

        Expected output: Seven line loops each resembling a wedge pointing to
        upper-right corner. Vertex colors are defined so that the lower-left
        vertex has color [0.0, 0.0, 0.5, 1.0 ] and the upper-right vertex has
        color [ 1.0, 1.0, 1.0, 1.0]. Between those the vertex color is linearly
        interpolated from left to right, from bottom to top. Viewport size
        320*240.
        '''
        testValues = { ( 'lineWidth' ) : [ ( 1 ),
                                           ( 10 ),
                                           ( 20 ),                                           
                                           ]
                       }
        
        def __init__( self, lineWidth ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.lineWidth = lineWidth
                self.gridx     = 9
                self.gridy     = 9
                self.overdraw  = 1
                self.name = "OPENGLES2 line loops, line width=%.2f" % ( self.lineWidth, )
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_SAMPLE_BUFFERS ] = 0
                defaultAttributes[ CONFIG_SAMPLES ]        = '>=0'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )
                mesh.scale( [ 0.9, 0.9 ] )

                indicesPerLoop = 7
                mesh.indices = []
                for i in range( 1, self.gridy - 1 ):
                        mesh.indices.append( i )
                        mesh.indices.append( i + 1 )
                        mesh.indices.append( i + 1 )   # Denerated segment                        
                        mesh.indices.append( ( i + 1 ) * self.gridx + i + 1 )
                        mesh.indices.append( ( i + 1 ) * self.gridx )
                        mesh.indices.append( i * self.gridx )
                        mesh.indices.append( i * self.gridx + i )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.LineWidth( self.lineWidth );
                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                # Clear the screen
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerLoop ):
                        OpenGLES2.DrawElements( 'GL_LINE_LOOP',
                                                indicesPerLoop,
                                                i * indicesPerLoop,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class TriangleFans( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing triangle fans. SAMPLE_BUFFERS
        0 is used to ensure non-multisample rendering.

        Expected output: a full screen plane drawn with discrete triangle
        fans. Vertex colors are defined so that the lower-left vertex has color
        [0.0, 0.0, 0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0,
        1.0, 1.0]. Between those the vertex color is linearly interpolated from
        left to right, from bottom to top. Viewport size 320*240.
        '''
        
        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats   = 1
                self.gridx     = 9
                self.gridy     = 9
                self.overdraw  = 1
                self.name = "OPENGLES2 triangle fans"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()

                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z, gVertex.w );
                        gVSColor = gColor;
                    }
                    """

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }
                    """

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_SAMPLE_BUFFERS ] = 0
                defaultAttributes[ CONFIG_SAMPLES ]        = '>=0'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy, 1, 1, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                indicesPerFan = 11
                mesh.indices = []
                for i in range( 0, self.gridx - 2, 2 ):
                        for j in range( 0, self.gridy - 2, 2 ):
                                mesh.indices.append( j + ( i + 1 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx )   # Degenerated triangle
                                mesh.indices.append( j + ( i + 0 ) * self.gridx )
                                mesh.indices.append( j + ( i + 0 ) * self.gridx + 1 )
                                mesh.indices.append( j + ( i + 0 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 1 ) * self.gridx + 2 )
                                mesh.indices.append( j + ( i + 2 ) * self.gridx + 2 )
                                                
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                # Clear the screen
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                for i in range( vertexDataSetup.indexArrayLength / indicesPerFan ):
                        OpenGLES2.DrawElements( 'GL_TRIANGLE_FAN',
                                                indicesPerFan,
                                                i * indicesPerFan,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                        
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
######################################################################
class TriangleStrip( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing triangle
        strips. SAMPLE_BUFFERS 0 is used to ensure non-multisample rendering.

        Expected output: a full screen plane drawn with discrete triangle
        strips. The vertex colors are defined so that the lower-left vertex has
        color [0.0, 0.0, 0.5, 1.0 ] and the upper-right vertex has color [ 1.0,
        1.0, 1.0, 1.0]. Between those the vertex color is linearly interpolated
        from left to right, from bottom to top. Viewport size 320*240.
        '''

        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats  = 1
                self.gridx    = 9
                self.gridy    = 9
                self.overdraw = 1
                self.name = "OPENGLES2 triangle strips"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_SAMPLE_BUFFERS ] = 0
                defaultAttributes[ CONFIG_SAMPLES ]        = '>=0'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                indicesPerStrip = self.gridy * 2
                mesh.indices = []
                for i in range( self.gridx - 1 ):
                        for j in range( self.gridy ):
                                mesh.indices.append( j * self.gridy + i )
                                mesh.indices.append( j * self.gridy + i + 1 )
                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                # Clear the screen
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                offset  = 1.0
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
               
                for i in range( vertexDataSetup.indexArrayLength / indicesPerStrip ):
                        OpenGLES2.DrawElements( 'GL_TRIANGLE_STRIP',
                                                indicesPerStrip,
                                                i * indicesPerStrip,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )

######################################################################
class Triangles( vt.VisualTestCase ):

        ''' The purpose of the test: test drawing triangles. SAMPLE_BUFFERS 0 is
        used to ensure non-multisample rendering.

        Expected output: a full screen plane drawn with discrete triangles. The
        vertex colors are defined so that the lower-left vertex has color [0.0,
        0.0, 0.5, 1.0 ] and the upper-right vertex has color [ 1.0, 1.0, 1.0,
        1.0]. Between those the vertex color is linearly interpolated from left
        to right, from bottom to top. Viewport size 320*240.
        '''

        def __init__( self ):
                vt.VisualTestCase.__init__( self )
                self.repeats  = 1
                self.gridx    = 9
                self.gridy    = 9
                self.overdraw = 1
                self.name = "OPENGLES2 triangles"
        
        def build( self, target, modules ):       
                OpenGLES2 = modules[ 'OpenGLES2' ]
                Gltest    = modules[ 'Gltest' ]

                indexTracker = IndexTracker()
                
                vertexShaderSource = """
                    precision mediump float;
                    attribute vec4 gVertex;
                    attribute vec4 gColor;
                    varying vec4 gVSColor;
                    uniform float gOffset;

                    void main()
                    {
                        gl_Position = vec4( gVertex.x, gVertex.y, gVertex.z + gOffset, gVertex.w );
                        gVSColor = gColor;
                    }"""

                fragmentShaderSource = """
                    precision mediump float;
                    varying vec4 gVSColor;

                    void main()
                    {
                        gl_FragColor = gVSColor;
                    }"""

                vsSource = formatShader( vertexShaderSource )
                fsSource = formatShader( fragmentShaderSource )
                
                # ------------------------------------------------------------
                # Init actions
                self.beginInitActions()

                apis = [ API_OPENGLES2 ]
                defaultAttributes = target.getDefaultAttributes( apis )
                defaultAttributes[ CONFIG_SAMPLE_BUFFERS ] = 0
                defaultAttributes[ CONFIG_SAMPLES ]        = '>=0'
                state = target.setup( modules,
                                      indexTracker,
                                      WIDTH, HEIGHT,
                                      apis,
                                      defaultAttributes )

                mesh            = createPlane( self.gridx, self.gridy , self.gridx, self.gridy, True, [] )
                mesh.gradientColors( [ 0.0, 0.0, 0.5, 1.0 ], [ 1.0, 1.0, 1.0, 1.0 ] )

                indicesPerBatch = ( self.gridy - 1  ) * 6
                mesh.indices = []
                for i in range( self.gridx - 1 ):
                        for j in range( self.gridy - 1 ):
                                mesh.indices.append( j * self.gridy + i )
                                mesh.indices.append( j * self.gridy + i + 1 )
                                mesh.indices.append( ( j + 1 ) * self.gridy + i )
                                mesh.indices.append( j * self.gridy + i + 1 )                                
                                mesh.indices.append( ( j + 1 ) * self.gridy + i + 1 )
                                mesh.indices.append( ( j + 1 ) * self.gridy + i )                                
                                        
                vertexDataSetup = setupVertexData( modules, indexTracker, mesh )
                program         = createProgram( modules, indexTracker, vsSource, fsSource, vertexDataSetup )
                
                useVertexData( modules, indexTracker, vertexDataSetup, program )

                offsetLocationIndex     = indexTracker.allocIndex( 'OPENGLES2_LOCATION_INDEX' )
                OpenGLES2.GetUniformLocation( offsetLocationIndex, program.programIndex, 'gOffset' )

                OpenGLES2.Viewport( 0, 0, WIDTH, HEIGHT );
                OpenGLES2.ClearColor( [ 0.0, 0.0, 0.0, 1.0 ] )
                        
                # ------------------------------------------------------------
                # Start benchmark actions
                self.beginBenchmarkActions()
                
                # Clear the screen
                OpenGLES2.Clear( [ 'GL_COLOR_BUFFER_BIT' ] )

                offset  = 1.0
                OpenGLES2.Uniformf( offsetLocationIndex, 1, [ offset ] )
                
                for i in range( vertexDataSetup.indexArrayLength / indicesPerBatch ):
                        OpenGLES2.DrawElements( 'GL_TRIANGLES',
                                                indicesPerBatch,
                                                i * indicesPerBatch,
                                                vertexDataSetup.indexArrayIndex )    

                OpenGLES2.CheckError( '' )                
                Gltest.SaveTarga( -1, 0, 0, WIDTH, HEIGHT, target.outputPath( apis ) )
                
